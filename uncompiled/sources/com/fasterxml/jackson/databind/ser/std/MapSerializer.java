package com.fasterxml.jackson.databind.ser.std;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.d;
import com.fasterxml.jackson.databind.f;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.fasterxml.jackson.databind.jsontype.c;
import com.fasterxml.jackson.databind.k;
import com.fasterxml.jackson.databind.ser.ContainerSerializer;
import com.fasterxml.jackson.databind.ser.b;
import com.fasterxml.jackson.databind.ser.impl.a;
import com.fasterxml.jackson.databind.type.TypeFactory;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

@pt1
/* loaded from: classes.dex */
public class MapSerializer extends ContainerSerializer<Map<?, ?>> implements b {
    public static final JavaType UNSPECIFIED_TYPE = TypeFactory.unknownType();
    private static final long serialVersionUID = 1;
    public a _dynamicValueSerializers;
    public final Object _filterId;
    public final Set<String> _ignoredEntries;
    public f<Object> _keySerializer;
    public final JavaType _keyType;
    public final com.fasterxml.jackson.databind.a _property;
    public final boolean _sortKeys;
    public final Object _suppressableValue;
    public f<Object> _valueSerializer;
    public final JavaType _valueType;
    public final boolean _valueTypeIsStatic;
    public final c _valueTypeSerializer;

    public MapSerializer(Set<String> set, JavaType javaType, JavaType javaType2, boolean z, c cVar, f<?> fVar, f<?> fVar2) {
        super(Map.class, false);
        this._ignoredEntries = (set == null || set.isEmpty()) ? null : set;
        this._keyType = javaType;
        this._valueType = javaType2;
        this._valueTypeIsStatic = z;
        this._valueTypeSerializer = cVar;
        this._keySerializer = fVar;
        this._valueSerializer = fVar2;
        this._dynamicValueSerializers = a.a();
        this._property = null;
        this._filterId = null;
        this._sortKeys = false;
        this._suppressableValue = null;
    }

    @Deprecated
    public static MapSerializer construct(String[] strArr, JavaType javaType, boolean z, c cVar, f<Object> fVar, f<Object> fVar2, Object obj) {
        return construct((strArr == null || strArr.length == 0) ? null : lh.a(strArr), javaType, z, cVar, fVar, fVar2, obj);
    }

    public void _ensureOverride() {
        if (getClass() == MapSerializer.class) {
            return;
        }
        throw new IllegalStateException("Missing override in class " + getClass().getName());
    }

    public final f<Object> _findAndAddDynamic(a aVar, Class<?> cls, k kVar) throws JsonMappingException {
        a.d g = aVar.g(cls, kVar, this._property);
        a aVar2 = g.b;
        if (aVar != aVar2) {
            this._dynamicValueSerializers = aVar2;
        }
        return g.a;
    }

    public boolean _hasNullKey(Map<?, ?> map) {
        return (map instanceof HashMap) && map.containsKey(null);
    }

    public Map<?, ?> _orderEntries(Map<?, ?> map, JsonGenerator jsonGenerator, k kVar, Object obj) throws IOException {
        if (map instanceof SortedMap) {
            return map;
        }
        if (_hasNullKey(map)) {
            TreeMap treeMap = new TreeMap();
            for (Map.Entry<?, ?> entry : map.entrySet()) {
                Object key = entry.getKey();
                if (key == null) {
                    _writeNullKeyedEntry(jsonGenerator, kVar, obj, entry.getValue());
                } else {
                    treeMap.put(key, entry.getValue());
                }
            }
            return treeMap;
        }
        return new TreeMap(map);
    }

    public void _writeNullKeyedEntry(JsonGenerator jsonGenerator, k kVar, Object obj, Object obj2) throws IOException {
        f<Object> fVar;
        f<Object> findNullKeySerializer = kVar.findNullKeySerializer(this._keyType, this._property);
        if (obj2 != null) {
            f<Object> fVar2 = this._valueSerializer;
            if (fVar2 == null) {
                Class<?> cls = obj2.getClass();
                f<Object> i = this._dynamicValueSerializers.i(cls);
                if (i != null) {
                    fVar2 = i;
                } else if (this._valueType.hasGenericTypes()) {
                    fVar2 = _findAndAddDynamic(this._dynamicValueSerializers, kVar.constructSpecializedType(this._valueType, cls), kVar);
                } else {
                    fVar2 = _findAndAddDynamic(this._dynamicValueSerializers, cls, kVar);
                }
            }
            if (obj == JsonInclude.Include.NON_EMPTY && fVar2.isEmpty(kVar, obj2)) {
                return;
            }
            fVar = fVar2;
        } else if (obj != null) {
            return;
        } else {
            fVar = kVar.getDefaultNullValueSerializer();
        }
        try {
            findNullKeySerializer.serialize(null, jsonGenerator, kVar);
            fVar.serialize(obj2, jsonGenerator, kVar);
        } catch (Exception e) {
            wrapAndThrow(kVar, e, obj2, "");
        }
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public void acceptJsonFormatVisitor(com.fasterxml.jackson.databind.jsonFormatVisitors.b bVar, JavaType javaType) throws JsonMappingException {
        com.fasterxml.jackson.databind.jsonFormatVisitors.c o = bVar == null ? null : bVar.o(javaType);
        if (o != null) {
            o.a(this._keySerializer, this._keyType);
            f<Object> fVar = this._valueSerializer;
            if (fVar == null) {
                fVar = _findAndAddDynamic(this._dynamicValueSerializers, this._valueType, bVar.b());
            }
            o.k(fVar, this._valueType);
        }
    }

    @Override // com.fasterxml.jackson.databind.ser.b
    public f<?> createContextual(k kVar, com.fasterxml.jackson.databind.a aVar) throws JsonMappingException {
        f<?> fVar;
        f<?> handleSecondaryContextualization;
        AnnotatedMember member;
        Object findFilterId;
        Boolean feature;
        Set<String> findIgnoredForSerialization;
        AnnotationIntrospector annotationIntrospector = kVar.getAnnotationIntrospector();
        f<Object> fVar2 = null;
        AnnotatedMember member2 = aVar == null ? null : aVar.getMember();
        Object obj = this._suppressableValue;
        if (member2 == null || annotationIntrospector == null) {
            fVar = null;
        } else {
            Object findKeySerializer = annotationIntrospector.findKeySerializer(member2);
            fVar = findKeySerializer != null ? kVar.serializerInstance(member2, findKeySerializer) : null;
            Object findContentSerializer = annotationIntrospector.findContentSerializer(member2);
            if (findContentSerializer != null) {
                fVar2 = kVar.serializerInstance(member2, findContentSerializer);
            }
        }
        JsonInclude.Include contentInclusion = findIncludeOverrides(kVar, aVar, Map.class).getContentInclusion();
        if (contentInclusion != null && contentInclusion != JsonInclude.Include.USE_DEFAULTS) {
            obj = contentInclusion;
        }
        if (fVar2 == null) {
            fVar2 = this._valueSerializer;
        }
        f<?> findConvertingContentSerializer = findConvertingContentSerializer(kVar, aVar, fVar2);
        if (findConvertingContentSerializer == null) {
            if (this._valueTypeIsStatic && !this._valueType.isJavaLangObject()) {
                findConvertingContentSerializer = kVar.findValueSerializer(this._valueType, aVar);
            }
        } else {
            findConvertingContentSerializer = kVar.handleSecondaryContextualization(findConvertingContentSerializer, aVar);
        }
        f<?> fVar3 = findConvertingContentSerializer;
        if (fVar == null) {
            fVar = this._keySerializer;
        }
        if (fVar == null) {
            handleSecondaryContextualization = kVar.findKeySerializer(this._keyType, aVar);
        } else {
            handleSecondaryContextualization = kVar.handleSecondaryContextualization(fVar, aVar);
        }
        f<?> fVar4 = handleSecondaryContextualization;
        HashSet hashSet = this._ignoredEntries;
        boolean z = false;
        if (annotationIntrospector != null && member2 != null) {
            JsonIgnoreProperties.Value findPropertyIgnorals = annotationIntrospector.findPropertyIgnorals(member2);
            if (findPropertyIgnorals != null && (findIgnoredForSerialization = findPropertyIgnorals.findIgnoredForSerialization()) != null && !findIgnoredForSerialization.isEmpty()) {
                hashSet = hashSet == null ? new HashSet() : new HashSet(hashSet);
                for (String str : findIgnoredForSerialization) {
                    hashSet.add(str);
                }
            }
            Boolean findSerializationSortAlphabetically = annotationIntrospector.findSerializationSortAlphabetically(member2);
            if (findSerializationSortAlphabetically != null && findSerializationSortAlphabetically.booleanValue()) {
                z = true;
            }
        }
        Set<String> set = hashSet;
        JsonFormat.Value findFormatOverrides = findFormatOverrides(kVar, aVar, Map.class);
        MapSerializer withResolved = withResolved(aVar, fVar4, fVar3, set, (findFormatOverrides == null || (feature = findFormatOverrides.getFeature(JsonFormat.Feature.WRITE_SORTED_MAP_ENTRIES)) == null) ? z : feature.booleanValue());
        if (obj != this._suppressableValue) {
            withResolved = withResolved.withContentInclusion(obj);
        }
        return (aVar == null || (member = aVar.getMember()) == null || (findFilterId = annotationIntrospector.findFilterId(member)) == null) ? withResolved : withResolved.withFilterId(findFilterId);
    }

    @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
    public f<?> getContentSerializer() {
        return this._valueSerializer;
    }

    @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
    public JavaType getContentType() {
        return this._valueType;
    }

    public f<?> getKeySerializer() {
        return this._keySerializer;
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, defpackage.ld3
    public d getSchema(k kVar, Type type) {
        return createSchemaNode("object", true);
    }

    public void serializeFields(Map<?, ?> map, JsonGenerator jsonGenerator, k kVar) throws IOException {
        f<Object> _findAndAddDynamic;
        if (this._valueTypeSerializer != null) {
            serializeTypedFields(map, jsonGenerator, kVar, null);
            return;
        }
        f<Object> fVar = this._keySerializer;
        Set<String> set = this._ignoredEntries;
        a aVar = this._dynamicValueSerializers;
        for (Map.Entry<?, ?> entry : map.entrySet()) {
            Object value = entry.getValue();
            Object key = entry.getKey();
            if (key == null) {
                kVar.findNullKeySerializer(this._keyType, this._property).serialize(null, jsonGenerator, kVar);
            } else if (set == null || !set.contains(key)) {
                fVar.serialize(key, jsonGenerator, kVar);
            }
            if (value == null) {
                kVar.defaultSerializeNull(jsonGenerator);
            } else {
                f<Object> fVar2 = this._valueSerializer;
                if (fVar2 == null) {
                    Class<?> cls = value.getClass();
                    f<Object> i = aVar.i(cls);
                    if (i == null) {
                        if (this._valueType.hasGenericTypes()) {
                            _findAndAddDynamic = _findAndAddDynamic(aVar, kVar.constructSpecializedType(this._valueType, cls), kVar);
                        } else {
                            _findAndAddDynamic = _findAndAddDynamic(aVar, cls, kVar);
                        }
                        fVar2 = _findAndAddDynamic;
                        aVar = this._dynamicValueSerializers;
                    } else {
                        fVar2 = i;
                    }
                }
                try {
                    fVar2.serialize(value, jsonGenerator, kVar);
                } catch (Exception e) {
                    wrapAndThrow(kVar, e, map, "" + key);
                }
            }
        }
    }

    public void serializeFieldsUsing(Map<?, ?> map, JsonGenerator jsonGenerator, k kVar, f<Object> fVar) throws IOException {
        f<Object> fVar2 = this._keySerializer;
        Set<String> set = this._ignoredEntries;
        c cVar = this._valueTypeSerializer;
        for (Map.Entry<?, ?> entry : map.entrySet()) {
            Object key = entry.getKey();
            if (set == null || !set.contains(key)) {
                if (key == null) {
                    kVar.findNullKeySerializer(this._keyType, this._property).serialize(null, jsonGenerator, kVar);
                } else {
                    fVar2.serialize(key, jsonGenerator, kVar);
                }
                Object value = entry.getValue();
                if (value == null) {
                    kVar.defaultSerializeNull(jsonGenerator);
                } else if (cVar == null) {
                    try {
                        fVar.serialize(value, jsonGenerator, kVar);
                    } catch (Exception e) {
                        wrapAndThrow(kVar, e, map, "" + key);
                    }
                } else {
                    fVar.serializeWithType(value, jsonGenerator, kVar, cVar);
                }
            }
        }
    }

    public void serializeFilteredFields(Map<?, ?> map, JsonGenerator jsonGenerator, k kVar, com.fasterxml.jackson.databind.ser.d dVar, Object obj) throws IOException {
        f<Object> fVar;
        f<Object> defaultNullValueSerializer;
        f<Object> _findAndAddDynamic;
        Set<String> set = this._ignoredEntries;
        a aVar = this._dynamicValueSerializers;
        MapProperty mapProperty = new MapProperty(this._valueTypeSerializer, this._property);
        for (Map.Entry<?, ?> entry : map.entrySet()) {
            Object key = entry.getKey();
            if (set == null || !set.contains(key)) {
                if (key == null) {
                    fVar = kVar.findNullKeySerializer(this._keyType, this._property);
                } else {
                    fVar = this._keySerializer;
                }
                Object value = entry.getValue();
                if (value != null) {
                    defaultNullValueSerializer = this._valueSerializer;
                    if (defaultNullValueSerializer == null) {
                        Class<?> cls = value.getClass();
                        f<Object> i = aVar.i(cls);
                        if (i == null) {
                            if (this._valueType.hasGenericTypes()) {
                                _findAndAddDynamic = _findAndAddDynamic(aVar, kVar.constructSpecializedType(this._valueType, cls), kVar);
                            } else {
                                _findAndAddDynamic = _findAndAddDynamic(aVar, cls, kVar);
                            }
                            defaultNullValueSerializer = _findAndAddDynamic;
                            aVar = this._dynamicValueSerializers;
                        } else {
                            defaultNullValueSerializer = i;
                        }
                    }
                    if (obj == JsonInclude.Include.NON_EMPTY && defaultNullValueSerializer.isEmpty(kVar, value)) {
                    }
                } else if (obj == null) {
                    defaultNullValueSerializer = kVar.getDefaultNullValueSerializer();
                }
                mapProperty.reset(key, fVar, defaultNullValueSerializer);
                try {
                    dVar.b(value, jsonGenerator, kVar, mapProperty);
                } catch (Exception e) {
                    wrapAndThrow(kVar, e, map, "" + key);
                }
            }
        }
    }

    /* JADX WARN: Can't wrap try/catch for region: R(9:9|(2:49|50)(1:(1:15)(2:47|34))|16|(2:42|(2:46|34)(2:44|45))(5:18|19|(2:21|(3:23|(1:25)(1:27)|26)(1:28))|29|(2:33|34))|35|36|38|34|7) */
    /* JADX WARN: Code restructure failed: missing block: B:39:0x0086, code lost:
        r3 = move-exception;
     */
    /* JADX WARN: Code restructure failed: missing block: B:40:0x0087, code lost:
        wrapAndThrow(r12, r3, r10, "" + r4);
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void serializeOptionalFields(java.util.Map<?, ?> r10, com.fasterxml.jackson.core.JsonGenerator r11, com.fasterxml.jackson.databind.k r12, java.lang.Object r13) throws java.io.IOException {
        /*
            r9 = this;
            com.fasterxml.jackson.databind.jsontype.c r0 = r9._valueTypeSerializer
            if (r0 == 0) goto L8
            r9.serializeTypedFields(r10, r11, r12, r13)
            return
        L8:
            java.util.Set<java.lang.String> r0 = r9._ignoredEntries
            com.fasterxml.jackson.databind.ser.impl.a r1 = r9._dynamicValueSerializers
            java.util.Set r2 = r10.entrySet()
            java.util.Iterator r2 = r2.iterator()
        L14:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L9d
            java.lang.Object r3 = r2.next()
            java.util.Map$Entry r3 = (java.util.Map.Entry) r3
            java.lang.Object r4 = r3.getKey()
            if (r4 != 0) goto L2f
            com.fasterxml.jackson.databind.JavaType r5 = r9._keyType
            com.fasterxml.jackson.databind.a r6 = r9._property
            com.fasterxml.jackson.databind.f r5 = r12.findNullKeySerializer(r5, r6)
            goto L3a
        L2f:
            if (r0 == 0) goto L38
            boolean r5 = r0.contains(r4)
            if (r5 == 0) goto L38
            goto L14
        L38:
            com.fasterxml.jackson.databind.f<java.lang.Object> r5 = r9._keySerializer
        L3a:
            java.lang.Object r3 = r3.getValue()
            if (r3 != 0) goto L48
            if (r13 == 0) goto L43
            goto L14
        L43:
            com.fasterxml.jackson.databind.f r6 = r12.getDefaultNullValueSerializer()
            goto L7f
        L48:
            com.fasterxml.jackson.databind.f<java.lang.Object> r6 = r9._valueSerializer
            if (r6 != 0) goto L74
            java.lang.Class r6 = r3.getClass()
            com.fasterxml.jackson.databind.f r7 = r1.i(r6)
            if (r7 != 0) goto L73
            com.fasterxml.jackson.databind.JavaType r7 = r9._valueType
            boolean r7 = r7.hasGenericTypes()
            if (r7 == 0) goto L69
            com.fasterxml.jackson.databind.JavaType r7 = r9._valueType
            com.fasterxml.jackson.databind.JavaType r6 = r12.constructSpecializedType(r7, r6)
            com.fasterxml.jackson.databind.f r1 = r9._findAndAddDynamic(r1, r6, r12)
            goto L6d
        L69:
            com.fasterxml.jackson.databind.f r1 = r9._findAndAddDynamic(r1, r6, r12)
        L6d:
            com.fasterxml.jackson.databind.ser.impl.a r6 = r9._dynamicValueSerializers
            r8 = r6
            r6 = r1
            r1 = r8
            goto L74
        L73:
            r6 = r7
        L74:
            com.fasterxml.jackson.annotation.JsonInclude$Include r7 = com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY
            if (r13 != r7) goto L7f
            boolean r7 = r6.isEmpty(r12, r3)
            if (r7 == 0) goto L7f
            goto L14
        L7f:
            r5.serialize(r4, r11, r12)     // Catch: java.lang.Exception -> L86
            r6.serialize(r3, r11, r12)     // Catch: java.lang.Exception -> L86
            goto L14
        L86:
            r3 = move-exception
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = ""
            r5.append(r6)
            r5.append(r4)
            java.lang.String r4 = r5.toString()
            r9.wrapAndThrow(r12, r3, r10, r4)
            goto L14
        L9d:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fasterxml.jackson.databind.ser.std.MapSerializer.serializeOptionalFields(java.util.Map, com.fasterxml.jackson.core.JsonGenerator, com.fasterxml.jackson.databind.k, java.lang.Object):void");
    }

    /* JADX WARN: Can't wrap try/catch for region: R(10:4|(2:45|46)(1:(1:10)(2:43|30))|11|(2:38|(2:42|30)(2:40|41))(5:13|14|(3:16|(1:18)(1:36)|19)(1:37)|20|(2:34|30))|25|26|27|29|30|2) */
    /* JADX WARN: Code restructure failed: missing block: B:34:0x007c, code lost:
        r3 = move-exception;
     */
    /* JADX WARN: Code restructure failed: missing block: B:35:0x007d, code lost:
        wrapAndThrow(r12, r3, r10, "" + r4);
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void serializeTypedFields(java.util.Map<?, ?> r10, com.fasterxml.jackson.core.JsonGenerator r11, com.fasterxml.jackson.databind.k r12, java.lang.Object r13) throws java.io.IOException {
        /*
            r9 = this;
            java.util.Set<java.lang.String> r0 = r9._ignoredEntries
            com.fasterxml.jackson.databind.ser.impl.a r1 = r9._dynamicValueSerializers
            java.util.Set r2 = r10.entrySet()
            java.util.Iterator r2 = r2.iterator()
        Lc:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L93
            java.lang.Object r3 = r2.next()
            java.util.Map$Entry r3 = (java.util.Map.Entry) r3
            java.lang.Object r4 = r3.getKey()
            if (r4 != 0) goto L27
            com.fasterxml.jackson.databind.JavaType r5 = r9._keyType
            com.fasterxml.jackson.databind.a r6 = r9._property
            com.fasterxml.jackson.databind.f r5 = r12.findNullKeySerializer(r5, r6)
            goto L32
        L27:
            if (r0 == 0) goto L30
            boolean r5 = r0.contains(r4)
            if (r5 == 0) goto L30
            goto Lc
        L30:
            com.fasterxml.jackson.databind.f<java.lang.Object> r5 = r9._keySerializer
        L32:
            java.lang.Object r3 = r3.getValue()
            if (r3 != 0) goto L40
            if (r13 == 0) goto L3b
            goto Lc
        L3b:
            com.fasterxml.jackson.databind.f r6 = r12.getDefaultNullValueSerializer()
            goto L73
        L40:
            java.lang.Class r6 = r3.getClass()
            com.fasterxml.jackson.databind.f r7 = r1.i(r6)
            if (r7 != 0) goto L67
            com.fasterxml.jackson.databind.JavaType r7 = r9._valueType
            boolean r7 = r7.hasGenericTypes()
            if (r7 == 0) goto L5d
            com.fasterxml.jackson.databind.JavaType r7 = r9._valueType
            com.fasterxml.jackson.databind.JavaType r6 = r12.constructSpecializedType(r7, r6)
            com.fasterxml.jackson.databind.f r1 = r9._findAndAddDynamic(r1, r6, r12)
            goto L61
        L5d:
            com.fasterxml.jackson.databind.f r1 = r9._findAndAddDynamic(r1, r6, r12)
        L61:
            com.fasterxml.jackson.databind.ser.impl.a r6 = r9._dynamicValueSerializers
            r8 = r6
            r6 = r1
            r1 = r8
            goto L68
        L67:
            r6 = r7
        L68:
            com.fasterxml.jackson.annotation.JsonInclude$Include r7 = com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY
            if (r13 != r7) goto L73
            boolean r7 = r6.isEmpty(r12, r3)
            if (r7 == 0) goto L73
            goto Lc
        L73:
            r5.serialize(r4, r11, r12)
            com.fasterxml.jackson.databind.jsontype.c r5 = r9._valueTypeSerializer     // Catch: java.lang.Exception -> L7c
            r6.serializeWithType(r3, r11, r12, r5)     // Catch: java.lang.Exception -> L7c
            goto Lc
        L7c:
            r3 = move-exception
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = ""
            r5.append(r6)
            r5.append(r4)
            java.lang.String r4 = r5.toString()
            r9.wrapAndThrow(r12, r3, r10, r4)
            goto Lc
        L93:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fasterxml.jackson.databind.ser.std.MapSerializer.serializeTypedFields(java.util.Map, com.fasterxml.jackson.core.JsonGenerator, com.fasterxml.jackson.databind.k, java.lang.Object):void");
    }

    public MapSerializer withContentInclusion(Object obj) {
        if (obj == this._suppressableValue) {
            return this;
        }
        _ensureOverride();
        return new MapSerializer(this, this._valueTypeSerializer, obj);
    }

    public MapSerializer withResolved(com.fasterxml.jackson.databind.a aVar, f<?> fVar, f<?> fVar2, Set<String> set, boolean z) {
        _ensureOverride();
        MapSerializer mapSerializer = new MapSerializer(this, aVar, fVar, fVar2, set);
        return z != mapSerializer._sortKeys ? new MapSerializer(mapSerializer, this._filterId, z) : mapSerializer;
    }

    @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
    public MapSerializer _withValueTypeSerializer(c cVar) {
        if (this._valueTypeSerializer == cVar) {
            return this;
        }
        _ensureOverride();
        return new MapSerializer(this, cVar, (Object) null);
    }

    @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
    public boolean hasSingleElement(Map<?, ?> map) {
        return map.size() == 1;
    }

    @Override // com.fasterxml.jackson.databind.f
    public boolean isEmpty(k kVar, Map<?, ?> map) {
        if (map == null || map.isEmpty()) {
            return true;
        }
        Object obj = this._suppressableValue;
        if (obj == null || obj == JsonInclude.Include.ALWAYS) {
            return false;
        }
        f<Object> fVar = this._valueSerializer;
        if (fVar != null) {
            for (Object obj2 : map.values()) {
                if (obj2 != null && !fVar.isEmpty(kVar, obj2)) {
                    return false;
                }
            }
            return true;
        }
        a aVar = this._dynamicValueSerializers;
        for (Object obj3 : map.values()) {
            if (obj3 != null) {
                Class<?> cls = obj3.getClass();
                f<Object> i = aVar.i(cls);
                if (i == null) {
                    try {
                        i = _findAndAddDynamic(aVar, cls, kVar);
                        aVar = this._dynamicValueSerializers;
                    } catch (JsonMappingException unused) {
                        return false;
                    }
                }
                if (!i.isEmpty(kVar, obj3)) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public void serialize(Map<?, ?> map, JsonGenerator jsonGenerator, k kVar) throws IOException {
        com.fasterxml.jackson.databind.ser.d findPropertyFilter;
        jsonGenerator.k1(map);
        if (!map.isEmpty()) {
            Object obj = this._suppressableValue;
            if (obj == JsonInclude.Include.ALWAYS) {
                obj = null;
            } else if (obj == null && !kVar.isEnabled(SerializationFeature.WRITE_NULL_MAP_VALUES)) {
                obj = JsonInclude.Include.NON_NULL;
            }
            Object obj2 = obj;
            if (this._sortKeys || kVar.isEnabled(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS)) {
                map = _orderEntries(map, jsonGenerator, kVar, obj2);
            }
            Map<?, ?> map2 = map;
            Object obj3 = this._filterId;
            if (obj3 != null && (findPropertyFilter = findPropertyFilter(kVar, obj3, map2)) != null) {
                serializeFilteredFields(map2, jsonGenerator, kVar, findPropertyFilter, obj2);
            } else if (obj2 != null) {
                serializeOptionalFields(map2, jsonGenerator, kVar, obj2);
            } else {
                f<Object> fVar = this._valueSerializer;
                if (fVar != null) {
                    serializeFieldsUsing(map2, jsonGenerator, kVar, fVar);
                } else {
                    serializeFields(map2, jsonGenerator, kVar);
                }
            }
        }
        jsonGenerator.f0();
    }

    @Override // com.fasterxml.jackson.databind.f
    public void serializeWithType(Map<?, ?> map, JsonGenerator jsonGenerator, k kVar, c cVar) throws IOException {
        com.fasterxml.jackson.databind.ser.d findPropertyFilter;
        cVar.i(map, jsonGenerator);
        jsonGenerator.u(map);
        if (!map.isEmpty()) {
            Object obj = this._suppressableValue;
            if (obj == JsonInclude.Include.ALWAYS) {
                obj = null;
            } else if (obj == null && !kVar.isEnabled(SerializationFeature.WRITE_NULL_MAP_VALUES)) {
                obj = JsonInclude.Include.NON_NULL;
            }
            Object obj2 = obj;
            if (this._sortKeys || kVar.isEnabled(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS)) {
                map = _orderEntries(map, jsonGenerator, kVar, obj2);
            }
            Object obj3 = this._filterId;
            if (obj3 != null && (findPropertyFilter = findPropertyFilter(kVar, obj3, map)) != null) {
                serializeFilteredFields(map, jsonGenerator, kVar, findPropertyFilter, obj2);
            } else if (obj2 != null) {
                serializeOptionalFields(map, jsonGenerator, kVar, obj2);
            } else {
                f<Object> fVar = this._valueSerializer;
                if (fVar != null) {
                    serializeFieldsUsing(map, jsonGenerator, kVar, fVar);
                } else {
                    serializeFields(map, jsonGenerator, kVar);
                }
            }
        }
        cVar.m(map, jsonGenerator);
    }

    @Override // com.fasterxml.jackson.databind.f
    public MapSerializer withFilterId(Object obj) {
        if (this._filterId == obj) {
            return this;
        }
        _ensureOverride();
        return new MapSerializer(this, obj, this._sortKeys);
    }

    /* JADX WARN: Removed duplicated region for block: B:18:0x0036  */
    /* JADX WARN: Removed duplicated region for block: B:20:? A[RETURN, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static com.fasterxml.jackson.databind.ser.std.MapSerializer construct(java.util.Set<java.lang.String> r9, com.fasterxml.jackson.databind.JavaType r10, boolean r11, com.fasterxml.jackson.databind.jsontype.c r12, com.fasterxml.jackson.databind.f<java.lang.Object> r13, com.fasterxml.jackson.databind.f<java.lang.Object> r14, java.lang.Object r15) {
        /*
            if (r10 != 0) goto L7
            com.fasterxml.jackson.databind.JavaType r10 = com.fasterxml.jackson.databind.ser.std.MapSerializer.UNSPECIFIED_TYPE
            r3 = r10
            r4 = r3
            goto L11
        L7:
            com.fasterxml.jackson.databind.JavaType r0 = r10.getKeyType()
            com.fasterxml.jackson.databind.JavaType r10 = r10.getContentType()
            r4 = r10
            r3 = r0
        L11:
            r10 = 0
            if (r11 != 0) goto L1f
            if (r4 == 0) goto L1d
            boolean r11 = r4.isFinal()
            if (r11 == 0) goto L1d
            r10 = 1
        L1d:
            r11 = r10
            goto L29
        L1f:
            java.lang.Class r0 = r4.getRawClass()
            java.lang.Class<java.lang.Object> r1 = java.lang.Object.class
            if (r0 != r1) goto L29
            r5 = r10
            goto L2a
        L29:
            r5 = r11
        L2a:
            com.fasterxml.jackson.databind.ser.std.MapSerializer r10 = new com.fasterxml.jackson.databind.ser.std.MapSerializer
            r1 = r10
            r2 = r9
            r6 = r12
            r7 = r13
            r8 = r14
            r1.<init>(r2, r3, r4, r5, r6, r7, r8)
            if (r15 == 0) goto L3a
            com.fasterxml.jackson.databind.ser.std.MapSerializer r10 = r10.withFilterId(r15)
        L3a:
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fasterxml.jackson.databind.ser.std.MapSerializer.construct(java.util.Set, com.fasterxml.jackson.databind.JavaType, boolean, com.fasterxml.jackson.databind.jsontype.c, com.fasterxml.jackson.databind.f, com.fasterxml.jackson.databind.f, java.lang.Object):com.fasterxml.jackson.databind.ser.std.MapSerializer");
    }

    public final f<Object> _findAndAddDynamic(a aVar, JavaType javaType, k kVar) throws JsonMappingException {
        a.d f = aVar.f(javaType, kVar, this._property);
        a aVar2 = f.b;
        if (aVar != aVar2) {
            this._dynamicValueSerializers = aVar2;
        }
        return f.a;
    }

    public MapSerializer(MapSerializer mapSerializer, com.fasterxml.jackson.databind.a aVar, f<?> fVar, f<?> fVar2, Set<String> set) {
        super(Map.class, false);
        this._ignoredEntries = (set == null || set.isEmpty()) ? null : null;
        this._keyType = mapSerializer._keyType;
        this._valueType = mapSerializer._valueType;
        this._valueTypeIsStatic = mapSerializer._valueTypeIsStatic;
        this._valueTypeSerializer = mapSerializer._valueTypeSerializer;
        this._keySerializer = fVar;
        this._valueSerializer = fVar2;
        this._dynamicValueSerializers = mapSerializer._dynamicValueSerializers;
        this._property = aVar;
        this._filterId = mapSerializer._filterId;
        this._sortKeys = mapSerializer._sortKeys;
        this._suppressableValue = mapSerializer._suppressableValue;
    }

    @Deprecated
    public void serializeTypedFields(Map<?, ?> map, JsonGenerator jsonGenerator, k kVar) throws IOException {
        serializeTypedFields(map, jsonGenerator, kVar, kVar.isEnabled(SerializationFeature.WRITE_NULL_MAP_VALUES) ? null : JsonInclude.Include.NON_NULL);
    }

    @Deprecated
    public void serializeFilteredFields(Map<?, ?> map, JsonGenerator jsonGenerator, k kVar, com.fasterxml.jackson.databind.ser.d dVar) throws IOException {
        serializeFilteredFields(map, jsonGenerator, kVar, dVar, kVar.isEnabled(SerializationFeature.WRITE_NULL_MAP_VALUES) ? null : JsonInclude.Include.NON_NULL);
    }

    @Deprecated
    public MapSerializer(MapSerializer mapSerializer, c cVar) {
        this(mapSerializer, cVar, mapSerializer._suppressableValue);
    }

    public MapSerializer(MapSerializer mapSerializer, c cVar, Object obj) {
        super(Map.class, false);
        this._ignoredEntries = mapSerializer._ignoredEntries;
        this._keyType = mapSerializer._keyType;
        JavaType javaType = mapSerializer._valueType;
        this._valueType = javaType;
        this._valueTypeIsStatic = mapSerializer._valueTypeIsStatic;
        this._valueTypeSerializer = cVar;
        this._keySerializer = mapSerializer._keySerializer;
        this._valueSerializer = mapSerializer._valueSerializer;
        this._dynamicValueSerializers = mapSerializer._dynamicValueSerializers;
        this._property = mapSerializer._property;
        this._filterId = mapSerializer._filterId;
        this._sortKeys = mapSerializer._sortKeys;
        if (obj == JsonInclude.Include.NON_ABSENT) {
            obj = javaType.isReferenceType() ? JsonInclude.Include.NON_EMPTY : JsonInclude.Include.NON_NULL;
        }
        this._suppressableValue = obj;
    }

    public MapSerializer(MapSerializer mapSerializer, Object obj, boolean z) {
        super(Map.class, false);
        this._ignoredEntries = mapSerializer._ignoredEntries;
        this._keyType = mapSerializer._keyType;
        this._valueType = mapSerializer._valueType;
        this._valueTypeIsStatic = mapSerializer._valueTypeIsStatic;
        this._valueTypeSerializer = mapSerializer._valueTypeSerializer;
        this._keySerializer = mapSerializer._keySerializer;
        this._valueSerializer = mapSerializer._valueSerializer;
        this._dynamicValueSerializers = mapSerializer._dynamicValueSerializers;
        this._property = mapSerializer._property;
        this._filterId = obj;
        this._sortKeys = z;
        this._suppressableValue = mapSerializer._suppressableValue;
    }
}
