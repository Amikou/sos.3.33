package com.fasterxml.jackson.databind.ser.std;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.a;
import com.fasterxml.jackson.databind.d;
import com.fasterxml.jackson.databind.f;
import com.fasterxml.jackson.databind.k;
import com.fasterxml.jackson.databind.node.m;
import com.fasterxml.jackson.databind.ser.b;
import com.fasterxml.jackson.databind.util.EnumValues;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.LinkedHashSet;
import org.web3j.abi.datatypes.Utf8String;

@pt1
/* loaded from: classes.dex */
public class EnumSerializer extends StdScalarSerializer<Enum<?>> implements b {
    private static final long serialVersionUID = 1;
    public final Boolean _serializeAsIndex;
    public final EnumValues _values;

    @Deprecated
    public EnumSerializer(EnumValues enumValues) {
        this(enumValues, null);
    }

    public static Boolean _isShapeWrittenUsingIndex(Class<?> cls, JsonFormat.Value value, boolean z, Boolean bool) {
        JsonFormat.Shape shape = value == null ? null : value.getShape();
        if (shape == null || shape == JsonFormat.Shape.ANY || shape == JsonFormat.Shape.SCALAR) {
            return bool;
        }
        if (shape != JsonFormat.Shape.STRING && shape != JsonFormat.Shape.NATURAL) {
            if (!shape.isNumeric() && shape != JsonFormat.Shape.ARRAY) {
                Object[] objArr = new Object[3];
                objArr[0] = shape;
                objArr[1] = cls.getName();
                objArr[2] = z ? "class" : "property";
                throw new IllegalArgumentException(String.format("Unsupported serialization shape (%s) for Enum %s, not supported as %s annotation", objArr));
            }
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    public static EnumSerializer construct(Class<?> cls, SerializationConfig serializationConfig, so soVar, JsonFormat.Value value) {
        return new EnumSerializer(EnumValues.constructFromName(serializationConfig, cls), _isShapeWrittenUsingIndex(cls, value, true, null));
    }

    public final boolean _serializeAsIndex(k kVar) {
        Boolean bool = this._serializeAsIndex;
        if (bool != null) {
            return bool.booleanValue();
        }
        return kVar.isEnabled(SerializationFeature.WRITE_ENUMS_USING_INDEX);
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdScalarSerializer, com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public void acceptJsonFormatVisitor(com.fasterxml.jackson.databind.jsonFormatVisitors.b bVar, JavaType javaType) throws JsonMappingException {
        k b = bVar.b();
        if (_serializeAsIndex(b)) {
            visitIntFormat(bVar, javaType, JsonParser.NumberType.INT);
            return;
        }
        ew1 d = bVar.d(javaType);
        if (d != null) {
            LinkedHashSet linkedHashSet = new LinkedHashSet();
            if (b != null && b.isEnabled(SerializationFeature.WRITE_ENUMS_USING_TO_STRING)) {
                for (Enum<?> r1 : this._values.enums()) {
                    linkedHashSet.add(r1.toString());
                }
            } else {
                for (yl3 yl3Var : this._values.values()) {
                    linkedHashSet.add(yl3Var.getValue());
                }
            }
            d.b(linkedHashSet);
        }
    }

    @Override // com.fasterxml.jackson.databind.ser.b
    public f<?> createContextual(k kVar, a aVar) throws JsonMappingException {
        JsonFormat.Value findFormatOverrides;
        Boolean _isShapeWrittenUsingIndex;
        return (aVar == null || (findFormatOverrides = findFormatOverrides(kVar, aVar, handledType())) == null || (_isShapeWrittenUsingIndex = _isShapeWrittenUsingIndex(aVar.getType().getRawClass(), findFormatOverrides, false, this._serializeAsIndex)) == this._serializeAsIndex) ? this : new EnumSerializer(this._values, _isShapeWrittenUsingIndex);
    }

    public EnumValues getEnumValues() {
        return this._values;
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdScalarSerializer, com.fasterxml.jackson.databind.ser.std.StdSerializer, defpackage.ld3
    public d getSchema(k kVar, Type type) {
        if (_serializeAsIndex(kVar)) {
            return createSchemaNode("integer", true);
        }
        m createSchemaNode = createSchemaNode(Utf8String.TYPE_NAME, true);
        if (type != null && kVar.constructType(type).isEnumType()) {
            com.fasterxml.jackson.databind.node.a V = createSchemaNode.V("enum");
            for (yl3 yl3Var : this._values.values()) {
                V.T(yl3Var.getValue());
            }
        }
        return createSchemaNode;
    }

    public EnumSerializer(EnumValues enumValues, Boolean bool) {
        super(enumValues.getEnumClass(), false);
        this._values = enumValues;
        this._serializeAsIndex = bool;
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public final void serialize(Enum<?> r2, JsonGenerator jsonGenerator, k kVar) throws IOException {
        if (_serializeAsIndex(kVar)) {
            jsonGenerator.x0(r2.ordinal());
        } else if (kVar.isEnabled(SerializationFeature.WRITE_ENUMS_USING_TO_STRING)) {
            jsonGenerator.o1(r2.toString());
        } else {
            jsonGenerator.m1(this._values.serializedValueFor(r2));
        }
    }
}
