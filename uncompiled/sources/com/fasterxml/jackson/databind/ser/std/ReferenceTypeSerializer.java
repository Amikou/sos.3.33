package com.fasterxml.jackson.databind.ser.std;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.RuntimeJsonMappingException;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.f;
import com.fasterxml.jackson.databind.jsontype.c;
import com.fasterxml.jackson.databind.k;
import com.fasterxml.jackson.databind.ser.b;
import com.fasterxml.jackson.databind.ser.impl.a;
import com.fasterxml.jackson.databind.type.ReferenceType;
import com.fasterxml.jackson.databind.util.NameTransformer;
import java.io.IOException;

/* loaded from: classes.dex */
public abstract class ReferenceTypeSerializer<T> extends StdSerializer<T> implements b {
    private static final long serialVersionUID = 1;
    public final JsonInclude.Include _contentInclusion;
    public transient a _dynamicSerializers;
    public final com.fasterxml.jackson.databind.a _property;
    public final JavaType _referredType;
    public final NameTransformer _unwrapper;
    public final f<Object> _valueSerializer;
    public final c _valueTypeSerializer;

    public ReferenceTypeSerializer(ReferenceType referenceType, boolean z, c cVar, f<Object> fVar) {
        super(referenceType);
        this._referredType = referenceType.getReferencedType();
        this._property = null;
        this._valueTypeSerializer = cVar;
        this._valueSerializer = fVar;
        this._unwrapper = null;
        this._contentInclusion = null;
        this._dynamicSerializers = a.a();
    }

    public abstract Object _getReferenced(T t);

    public abstract Object _getReferencedIfPresent(T t);

    public abstract boolean _isValueEmpty(T t);

    public boolean _useStatic(k kVar, com.fasterxml.jackson.databind.a aVar, JavaType javaType) {
        if (javaType.isJavaLangObject()) {
            return false;
        }
        if (javaType.isFinal() || javaType.useStaticType()) {
            return true;
        }
        AnnotationIntrospector annotationIntrospector = kVar.getAnnotationIntrospector();
        if (annotationIntrospector != null && aVar != null && aVar.getMember() != null) {
            JsonSerialize.Typing findSerializationTyping = annotationIntrospector.findSerializationTyping(aVar.getMember());
            if (findSerializationTyping == JsonSerialize.Typing.STATIC) {
                return true;
            }
            if (findSerializationTyping == JsonSerialize.Typing.DYNAMIC) {
                return false;
            }
        }
        return kVar.isEnabled(MapperFeature.USE_STATIC_TYPING);
    }

    public final f<Object> a(k kVar, Class<?> cls) throws JsonMappingException {
        f<Object> i = this._dynamicSerializers.i(cls);
        if (i == null) {
            f<Object> c = c(kVar, cls, this._property);
            NameTransformer nameTransformer = this._unwrapper;
            if (nameTransformer != null) {
                c = c.unwrappingSerializer(nameTransformer);
            }
            f<Object> fVar = c;
            this._dynamicSerializers = this._dynamicSerializers.h(cls, fVar);
            return fVar;
        }
        return i;
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public void acceptJsonFormatVisitor(com.fasterxml.jackson.databind.jsonFormatVisitors.b bVar, JavaType javaType) throws JsonMappingException {
        f<Object> fVar = this._valueSerializer;
        if (fVar == null) {
            fVar = b(bVar.b(), this._referredType, this._property);
            NameTransformer nameTransformer = this._unwrapper;
            if (nameTransformer != null) {
                fVar = fVar.unwrappingSerializer(nameTransformer);
            }
        }
        fVar.acceptJsonFormatVisitor(bVar, this._referredType);
    }

    public final f<Object> b(k kVar, JavaType javaType, com.fasterxml.jackson.databind.a aVar) throws JsonMappingException {
        return kVar.findValueSerializer(javaType, aVar);
    }

    public final f<Object> c(k kVar, Class<?> cls, com.fasterxml.jackson.databind.a aVar) throws JsonMappingException {
        return kVar.findValueSerializer(cls, aVar);
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fasterxml.jackson.databind.ser.b
    public f<?> createContextual(k kVar, com.fasterxml.jackson.databind.a aVar) throws JsonMappingException {
        c cVar = this._valueTypeSerializer;
        if (cVar != null) {
            cVar = cVar.a(aVar);
        }
        c cVar2 = cVar;
        f<?> findAnnotatedContentSerializer = findAnnotatedContentSerializer(kVar, aVar);
        if (findAnnotatedContentSerializer == null) {
            findAnnotatedContentSerializer = this._valueSerializer;
            if (findAnnotatedContentSerializer == null) {
                if (_useStatic(kVar, aVar, this._referredType)) {
                    findAnnotatedContentSerializer = b(kVar, this._referredType, aVar);
                }
            } else {
                findAnnotatedContentSerializer = kVar.handlePrimaryContextualization(findAnnotatedContentSerializer, aVar);
            }
        }
        f<?> fVar = findAnnotatedContentSerializer;
        JsonInclude.Include include = this._contentInclusion;
        JsonInclude.Include contentInclusion = findIncludeOverrides(kVar, aVar, handledType()).getContentInclusion();
        return withResolved(aVar, cVar2, fVar, this._unwrapper, (contentInclusion == include || contentInclusion == JsonInclude.Include.USE_DEFAULTS) ? include : contentInclusion);
    }

    @Override // com.fasterxml.jackson.databind.f
    public boolean isEmpty(k kVar, T t) {
        if (t == null || _isValueEmpty(t)) {
            return true;
        }
        if (this._contentInclusion == null) {
            return false;
        }
        Object _getReferenced = _getReferenced(t);
        f<Object> fVar = this._valueSerializer;
        if (fVar == null) {
            try {
                fVar = a(kVar, _getReferenced.getClass());
            } catch (JsonMappingException e) {
                throw new RuntimeJsonMappingException(e);
            }
        }
        return fVar.isEmpty(kVar, _getReferenced);
    }

    @Override // com.fasterxml.jackson.databind.f
    public boolean isUnwrappingSerializer() {
        return this._unwrapper != null;
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public void serialize(T t, JsonGenerator jsonGenerator, k kVar) throws IOException {
        Object _getReferencedIfPresent = _getReferencedIfPresent(t);
        if (_getReferencedIfPresent == null) {
            if (this._unwrapper == null) {
                kVar.defaultSerializeNull(jsonGenerator);
                return;
            }
            return;
        }
        f<Object> fVar = this._valueSerializer;
        if (fVar == null) {
            fVar = a(kVar, _getReferencedIfPresent.getClass());
        }
        c cVar = this._valueTypeSerializer;
        if (cVar != null) {
            fVar.serializeWithType(_getReferencedIfPresent, jsonGenerator, kVar, cVar);
        } else {
            fVar.serialize(_getReferencedIfPresent, jsonGenerator, kVar);
        }
    }

    @Override // com.fasterxml.jackson.databind.f
    public void serializeWithType(T t, JsonGenerator jsonGenerator, k kVar, c cVar) throws IOException {
        Object _getReferencedIfPresent = _getReferencedIfPresent(t);
        if (_getReferencedIfPresent == null) {
            if (this._unwrapper == null) {
                kVar.defaultSerializeNull(jsonGenerator);
                return;
            }
            return;
        }
        f<Object> fVar = this._valueSerializer;
        if (fVar == null) {
            fVar = a(kVar, _getReferencedIfPresent.getClass());
        }
        fVar.serializeWithType(_getReferencedIfPresent, jsonGenerator, kVar, cVar);
    }

    @Override // com.fasterxml.jackson.databind.f
    public f<T> unwrappingSerializer(NameTransformer nameTransformer) {
        f<?> fVar = this._valueSerializer;
        if (fVar != null) {
            fVar = fVar.unwrappingSerializer(nameTransformer);
        }
        f<?> fVar2 = fVar;
        NameTransformer nameTransformer2 = this._unwrapper;
        if (nameTransformer2 != null) {
            nameTransformer = NameTransformer.chainedTransformer(nameTransformer, nameTransformer2);
        }
        return withResolved(this._property, this._valueTypeSerializer, fVar2, nameTransformer, this._contentInclusion);
    }

    public abstract ReferenceTypeSerializer<T> withResolved(com.fasterxml.jackson.databind.a aVar, c cVar, f<?> fVar, NameTransformer nameTransformer, JsonInclude.Include include);

    public ReferenceTypeSerializer(ReferenceTypeSerializer<?> referenceTypeSerializer, com.fasterxml.jackson.databind.a aVar, c cVar, f<?> fVar, NameTransformer nameTransformer, JsonInclude.Include include) {
        super(referenceTypeSerializer);
        this._referredType = referenceTypeSerializer._referredType;
        this._dynamicSerializers = referenceTypeSerializer._dynamicSerializers;
        this._property = aVar;
        this._valueTypeSerializer = cVar;
        this._valueSerializer = fVar;
        this._unwrapper = nameTransformer;
        if (include != JsonInclude.Include.USE_DEFAULTS && include != JsonInclude.Include.ALWAYS) {
            this._contentInclusion = include;
        } else {
            this._contentInclusion = null;
        }
    }
}
