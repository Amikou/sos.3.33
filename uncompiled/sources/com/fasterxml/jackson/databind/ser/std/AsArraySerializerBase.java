package com.fasterxml.jackson.databind.ser.std;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.d;
import com.fasterxml.jackson.databind.f;
import com.fasterxml.jackson.databind.jsontype.c;
import com.fasterxml.jackson.databind.k;
import com.fasterxml.jackson.databind.node.m;
import com.fasterxml.jackson.databind.ser.ContainerSerializer;
import com.fasterxml.jackson.databind.ser.b;
import com.fasterxml.jackson.databind.ser.impl.a;
import java.io.IOException;
import java.lang.reflect.Type;

/* loaded from: classes.dex */
public abstract class AsArraySerializerBase<T> extends ContainerSerializer<T> implements b {
    public a _dynamicSerializers;
    public final f<Object> _elementSerializer;
    public final JavaType _elementType;
    public final com.fasterxml.jackson.databind.a _property;
    public final boolean _staticTyping;
    public final Boolean _unwrapSingle;
    public final c _valueTypeSerializer;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AsArraySerializerBase(Class<?> cls, JavaType javaType, boolean z, c cVar, f<Object> fVar) {
        super(cls, false);
        boolean z2 = false;
        this._elementType = javaType;
        if (z || (javaType != null && javaType.isFinal())) {
            z2 = true;
        }
        this._staticTyping = z2;
        this._valueTypeSerializer = cVar;
        this._property = null;
        this._elementSerializer = fVar;
        this._dynamicSerializers = a.a();
        this._unwrapSingle = null;
    }

    public final f<Object> _findAndAddDynamic(a aVar, Class<?> cls, k kVar) throws JsonMappingException {
        a.d g = aVar.g(cls, kVar, this._property);
        a aVar2 = g.b;
        if (aVar != aVar2) {
            this._dynamicSerializers = aVar2;
        }
        return g.a;
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public void acceptJsonFormatVisitor(com.fasterxml.jackson.databind.jsonFormatVisitors.b bVar, JavaType javaType) throws JsonMappingException {
        f<Object> fVar = this._elementSerializer;
        if (fVar == null) {
            fVar = bVar.b().findValueSerializer(this._elementType, this._property);
        }
        visitArrayFormat(bVar, javaType, fVar, this._elementType);
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:15:0x002b  */
    /* JADX WARN: Removed duplicated region for block: B:17:0x0033  */
    /* JADX WARN: Removed duplicated region for block: B:20:0x003b  */
    /* JADX WARN: Removed duplicated region for block: B:27:0x0050  */
    @Override // com.fasterxml.jackson.databind.ser.b
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public com.fasterxml.jackson.databind.f<?> createContextual(com.fasterxml.jackson.databind.k r6, com.fasterxml.jackson.databind.a r7) throws com.fasterxml.jackson.databind.JsonMappingException {
        /*
            r5 = this;
            com.fasterxml.jackson.databind.jsontype.c r0 = r5._valueTypeSerializer
            if (r0 == 0) goto L8
            com.fasterxml.jackson.databind.jsontype.c r0 = r0.a(r7)
        L8:
            r1 = 0
            if (r7 == 0) goto L20
            com.fasterxml.jackson.databind.AnnotationIntrospector r2 = r6.getAnnotationIntrospector()
            com.fasterxml.jackson.databind.introspect.AnnotatedMember r3 = r7.getMember()
            if (r3 == 0) goto L20
            java.lang.Object r2 = r2.findContentSerializer(r3)
            if (r2 == 0) goto L20
            com.fasterxml.jackson.databind.f r2 = r6.serializerInstance(r3, r2)
            goto L21
        L20:
            r2 = r1
        L21:
            java.lang.Class r3 = r5.handledType()
            com.fasterxml.jackson.annotation.JsonFormat$Value r3 = r5.findFormatOverrides(r6, r7, r3)
            if (r3 == 0) goto L31
            com.fasterxml.jackson.annotation.JsonFormat$Feature r1 = com.fasterxml.jackson.annotation.JsonFormat.Feature.WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED
            java.lang.Boolean r1 = r3.getFeature(r1)
        L31:
            if (r2 != 0) goto L35
            com.fasterxml.jackson.databind.f<java.lang.Object> r2 = r5._elementSerializer
        L35:
            com.fasterxml.jackson.databind.f r2 = r5.findConvertingContentSerializer(r6, r7, r2)
            if (r2 != 0) goto L50
            com.fasterxml.jackson.databind.JavaType r3 = r5._elementType
            if (r3 == 0) goto L54
            boolean r4 = r5._staticTyping
            if (r4 == 0) goto L54
            boolean r3 = r3.isJavaLangObject()
            if (r3 != 0) goto L54
            com.fasterxml.jackson.databind.JavaType r2 = r5._elementType
            com.fasterxml.jackson.databind.f r2 = r6.findValueSerializer(r2, r7)
            goto L54
        L50:
            com.fasterxml.jackson.databind.f r2 = r6.handleSecondaryContextualization(r2, r7)
        L54:
            com.fasterxml.jackson.databind.f<java.lang.Object> r6 = r5._elementSerializer
            if (r2 != r6) goto L66
            com.fasterxml.jackson.databind.a r6 = r5._property
            if (r7 != r6) goto L66
            com.fasterxml.jackson.databind.jsontype.c r6 = r5._valueTypeSerializer
            if (r6 != r0) goto L66
            java.lang.Boolean r6 = r5._unwrapSingle
            if (r6 == r1) goto L65
            goto L66
        L65:
            return r5
        L66:
            com.fasterxml.jackson.databind.ser.std.AsArraySerializerBase r6 = r5.withResolved(r7, r0, r2, r1)
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fasterxml.jackson.databind.ser.std.AsArraySerializerBase.createContextual(com.fasterxml.jackson.databind.k, com.fasterxml.jackson.databind.a):com.fasterxml.jackson.databind.f");
    }

    @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
    public f<?> getContentSerializer() {
        return this._elementSerializer;
    }

    @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
    public JavaType getContentType() {
        return this._elementType;
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, defpackage.ld3
    public d getSchema(k kVar, Type type) throws JsonMappingException {
        m createSchemaNode = createSchemaNode("array", true);
        JavaType javaType = this._elementType;
        if (javaType != null) {
            d dVar = null;
            if (javaType.getRawClass() != Object.class) {
                f<Object> findValueSerializer = kVar.findValueSerializer(javaType, this._property);
                if (findValueSerializer instanceof ld3) {
                    dVar = ((ld3) findValueSerializer).getSchema(kVar, null);
                }
            }
            if (dVar == null) {
                dVar = aw1.a();
            }
            createSchemaNode.Z("items", dVar);
        }
        return createSchemaNode;
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public void serialize(T t, JsonGenerator jsonGenerator, k kVar) throws IOException {
        if (kVar.isEnabled(SerializationFeature.WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED) && hasSingleElement(t)) {
            serializeContents(t, jsonGenerator, kVar);
            return;
        }
        jsonGenerator.e1();
        jsonGenerator.u(t);
        serializeContents(t, jsonGenerator, kVar);
        jsonGenerator.e0();
    }

    public abstract void serializeContents(T t, JsonGenerator jsonGenerator, k kVar) throws IOException;

    @Override // com.fasterxml.jackson.databind.f
    public void serializeWithType(T t, JsonGenerator jsonGenerator, k kVar, c cVar) throws IOException {
        cVar.h(t, jsonGenerator);
        jsonGenerator.u(t);
        serializeContents(t, jsonGenerator, kVar);
        cVar.l(t, jsonGenerator);
    }

    @Deprecated
    public final AsArraySerializerBase<T> withResolved(com.fasterxml.jackson.databind.a aVar, c cVar, f<?> fVar) {
        return withResolved(aVar, cVar, fVar, this._unwrapSingle);
    }

    public abstract AsArraySerializerBase<T> withResolved(com.fasterxml.jackson.databind.a aVar, c cVar, f<?> fVar, Boolean bool);

    public final f<Object> _findAndAddDynamic(a aVar, JavaType javaType, k kVar) throws JsonMappingException {
        a.d f = aVar.f(javaType, kVar, this._property);
        a aVar2 = f.b;
        if (aVar != aVar2) {
            this._dynamicSerializers = aVar2;
        }
        return f.a;
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    @Deprecated
    public AsArraySerializerBase(Class<?> cls, JavaType javaType, boolean z, c cVar, com.fasterxml.jackson.databind.a aVar, f<Object> fVar) {
        super(cls, false);
        boolean z2 = false;
        this._elementType = javaType;
        if (z || (javaType != null && javaType.isFinal())) {
            z2 = true;
        }
        this._staticTyping = z2;
        this._valueTypeSerializer = cVar;
        this._property = aVar;
        this._elementSerializer = fVar;
        this._dynamicSerializers = a.a();
        this._unwrapSingle = null;
    }

    public AsArraySerializerBase(AsArraySerializerBase<?> asArraySerializerBase, com.fasterxml.jackson.databind.a aVar, c cVar, f<?> fVar, Boolean bool) {
        super(asArraySerializerBase);
        this._elementType = asArraySerializerBase._elementType;
        this._staticTyping = asArraySerializerBase._staticTyping;
        this._valueTypeSerializer = cVar;
        this._property = aVar;
        this._elementSerializer = fVar;
        this._dynamicSerializers = asArraySerializerBase._dynamicSerializers;
        this._unwrapSingle = bool;
    }

    @Deprecated
    public AsArraySerializerBase(AsArraySerializerBase<?> asArraySerializerBase, com.fasterxml.jackson.databind.a aVar, c cVar, f<?> fVar) {
        this(asArraySerializerBase, aVar, cVar, fVar, asArraySerializerBase._unwrapSingle);
    }
}
