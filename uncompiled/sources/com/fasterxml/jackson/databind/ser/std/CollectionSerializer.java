package com.fasterxml.jackson.databind.ser.std;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.a;
import com.fasterxml.jackson.databind.f;
import com.fasterxml.jackson.databind.jsontype.c;
import com.fasterxml.jackson.databind.k;
import com.fasterxml.jackson.databind.ser.ContainerSerializer;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;

/* loaded from: classes.dex */
public class CollectionSerializer extends AsArraySerializerBase<Collection<?>> {
    private static final long serialVersionUID = 1;

    public CollectionSerializer(JavaType javaType, boolean z, c cVar, f<Object> fVar) {
        super(Collection.class, javaType, z, cVar, fVar);
    }

    @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
    public ContainerSerializer<?> _withValueTypeSerializer(c cVar) {
        return new CollectionSerializer(this, this._property, cVar, this._elementSerializer, this._unwrapSingle);
    }

    public void serializeContentsUsing(Collection<?> collection, JsonGenerator jsonGenerator, k kVar, f<Object> fVar) throws IOException, JsonGenerationException {
        Iterator<?> it = collection.iterator();
        if (it.hasNext()) {
            c cVar = this._valueTypeSerializer;
            int i = 0;
            do {
                Object next = it.next();
                if (next == null) {
                    try {
                        kVar.defaultSerializeNull(jsonGenerator);
                    } catch (Exception e) {
                        wrapAndThrow(kVar, e, collection, i);
                    }
                } else if (cVar == null) {
                    fVar.serialize(next, jsonGenerator, kVar);
                } else {
                    fVar.serializeWithType(next, jsonGenerator, kVar, cVar);
                }
                i++;
            } while (it.hasNext());
        }
    }

    @Override // com.fasterxml.jackson.databind.ser.std.AsArraySerializerBase
    public /* bridge */ /* synthetic */ AsArraySerializerBase<Collection<?>> withResolved(a aVar, c cVar, f fVar, Boolean bool) {
        return withResolved2(aVar, cVar, (f<?>) fVar, bool);
    }

    @Deprecated
    public CollectionSerializer(JavaType javaType, boolean z, c cVar, a aVar, f<Object> fVar) {
        this(javaType, z, cVar, fVar);
    }

    @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
    public boolean hasSingleElement(Collection<?> collection) {
        Iterator<?> it = collection.iterator();
        if (it.hasNext()) {
            it.next();
            return !it.hasNext();
        }
        return false;
    }

    @Override // com.fasterxml.jackson.databind.f
    public boolean isEmpty(k kVar, Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }

    @Override // com.fasterxml.jackson.databind.ser.std.AsArraySerializerBase, com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public final void serialize(Collection<?> collection, JsonGenerator jsonGenerator, k kVar) throws IOException {
        int size = collection.size();
        if (size == 1 && ((this._unwrapSingle == null && kVar.isEnabled(SerializationFeature.WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED)) || this._unwrapSingle == Boolean.TRUE)) {
            serializeContents(collection, jsonGenerator, kVar);
            return;
        }
        jsonGenerator.f1(size);
        serializeContents(collection, jsonGenerator, kVar);
        jsonGenerator.e0();
    }

    @Override // com.fasterxml.jackson.databind.ser.std.AsArraySerializerBase
    public void serializeContents(Collection<?> collection, JsonGenerator jsonGenerator, k kVar) throws IOException {
        f<Object> _findAndAddDynamic;
        f<Object> fVar = this._elementSerializer;
        if (fVar != null) {
            serializeContentsUsing(collection, jsonGenerator, kVar, fVar);
            return;
        }
        Iterator<?> it = collection.iterator();
        if (it.hasNext()) {
            com.fasterxml.jackson.databind.ser.impl.a aVar = this._dynamicSerializers;
            c cVar = this._valueTypeSerializer;
            int i = 0;
            do {
                try {
                    Object next = it.next();
                    if (next == null) {
                        kVar.defaultSerializeNull(jsonGenerator);
                    } else {
                        Class<?> cls = next.getClass();
                        f<Object> i2 = aVar.i(cls);
                        if (i2 == null) {
                            if (this._elementType.hasGenericTypes()) {
                                _findAndAddDynamic = _findAndAddDynamic(aVar, kVar.constructSpecializedType(this._elementType, cls), kVar);
                            } else {
                                _findAndAddDynamic = _findAndAddDynamic(aVar, cls, kVar);
                            }
                            i2 = _findAndAddDynamic;
                            aVar = this._dynamicSerializers;
                        }
                        if (cVar == null) {
                            i2.serialize(next, jsonGenerator, kVar);
                        } else {
                            i2.serializeWithType(next, jsonGenerator, kVar, cVar);
                        }
                    }
                    i++;
                } catch (Exception e) {
                    wrapAndThrow(kVar, e, collection, i);
                    return;
                }
            } while (it.hasNext());
        }
    }

    @Override // com.fasterxml.jackson.databind.ser.std.AsArraySerializerBase
    /* renamed from: withResolved  reason: avoid collision after fix types in other method */
    public AsArraySerializerBase<Collection<?>> withResolved2(a aVar, c cVar, f<?> fVar, Boolean bool) {
        return new CollectionSerializer(this, aVar, cVar, fVar, bool);
    }

    public CollectionSerializer(CollectionSerializer collectionSerializer, a aVar, c cVar, f<?> fVar, Boolean bool) {
        super(collectionSerializer, aVar, cVar, fVar, bool);
    }
}
