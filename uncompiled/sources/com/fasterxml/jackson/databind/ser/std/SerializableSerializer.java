package com.fasterxml.jackson.databind.ser.std;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.e;
import com.fasterxml.jackson.databind.jsonFormatVisitors.b;
import com.fasterxml.jackson.databind.jsontype.c;
import com.fasterxml.jackson.databind.k;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicReference;

@pt1
/* loaded from: classes.dex */
public class SerializableSerializer extends StdSerializer<e> {
    public static final SerializableSerializer instance = new SerializableSerializer();
    public static final AtomicReference<ObjectMapper> f0 = new AtomicReference<>();

    public SerializableSerializer() {
        super(e.class);
    }

    public static final synchronized ObjectMapper a() {
        ObjectMapper objectMapper;
        synchronized (SerializableSerializer.class) {
            AtomicReference<ObjectMapper> atomicReference = f0;
            objectMapper = atomicReference.get();
            if (objectMapper == null) {
                objectMapper = new ObjectMapper();
                atomicReference.set(objectMapper);
            }
        }
        return objectMapper;
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public void acceptJsonFormatVisitor(b bVar, JavaType javaType) throws JsonMappingException {
        bVar.e(javaType);
    }

    /* JADX WARN: Removed duplicated region for block: B:25:0x0064 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:27:0x004d A[EXC_TOP_SPLITTER, SYNTHETIC] */
    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, defpackage.ld3
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public com.fasterxml.jackson.databind.d getSchema(com.fasterxml.jackson.databind.k r7, java.lang.reflect.Type r8) throws com.fasterxml.jackson.databind.JsonMappingException {
        /*
            r6 = this;
            com.fasterxml.jackson.databind.node.m r0 = r6.createObjectNode()
            r1 = 0
            if (r8 == 0) goto L42
            java.lang.Class r8 = com.fasterxml.jackson.databind.type.TypeFactory.rawClass(r8)
            java.lang.Class<bw1> r2 = defpackage.bw1.class
            boolean r2 = r8.isAnnotationPresent(r2)
            if (r2 == 0) goto L42
            java.lang.Class<bw1> r2 = defpackage.bw1.class
            java.lang.annotation.Annotation r8 = r8.getAnnotation(r2)
            bw1 r8 = (defpackage.bw1) r8
            java.lang.String r2 = r8.schemaType()
            java.lang.String r3 = r8.schemaObjectPropertiesDefinition()
            java.lang.String r4 = "##irrelevant"
            boolean r3 = r4.equals(r3)
            if (r3 != 0) goto L30
            java.lang.String r3 = r8.schemaObjectPropertiesDefinition()
            goto L31
        L30:
            r3 = r1
        L31:
            java.lang.String r5 = r8.schemaItemDefinition()
            boolean r4 = r4.equals(r5)
            if (r4 != 0) goto L3f
            java.lang.String r1 = r8.schemaItemDefinition()
        L3f:
            r8 = r1
            r1 = r3
            goto L45
        L42:
            java.lang.String r2 = "any"
            r8 = r1
        L45:
            java.lang.String r3 = "type"
            r0.T(r3, r2)
            r2 = 0
            if (r1 == 0) goto L62
            java.lang.String r3 = "properties"
            com.fasterxml.jackson.databind.ObjectMapper r4 = a()     // Catch: java.io.IOException -> L5b
            com.fasterxml.jackson.databind.d r1 = r4.readTree(r1)     // Catch: java.io.IOException -> L5b
            r0.Z(r3, r1)     // Catch: java.io.IOException -> L5b
            goto L62
        L5b:
            java.lang.Object[] r1 = new java.lang.Object[r2]
            java.lang.String r3 = "Failed to parse @JsonSerializableSchema.schemaObjectPropertiesDefinition value"
            r7.reportMappingProblem(r3, r1)
        L62:
            if (r8 == 0) goto L79
            java.lang.String r1 = "items"
            com.fasterxml.jackson.databind.ObjectMapper r3 = a()     // Catch: java.io.IOException -> L72
            com.fasterxml.jackson.databind.d r8 = r3.readTree(r8)     // Catch: java.io.IOException -> L72
            r0.Z(r1, r8)     // Catch: java.io.IOException -> L72
            goto L79
        L72:
            java.lang.Object[] r8 = new java.lang.Object[r2]
            java.lang.String r1 = "Failed to parse @JsonSerializableSchema.schemaItemDefinition value"
            r7.reportMappingProblem(r1, r8)
        L79:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fasterxml.jackson.databind.ser.std.SerializableSerializer.getSchema(com.fasterxml.jackson.databind.k, java.lang.reflect.Type):com.fasterxml.jackson.databind.d");
    }

    @Override // com.fasterxml.jackson.databind.f
    public boolean isEmpty(k kVar, e eVar) {
        if (eVar instanceof e.a) {
            return ((e.a) eVar).k(kVar);
        }
        return false;
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public void serialize(e eVar, JsonGenerator jsonGenerator, k kVar) throws IOException {
        eVar.serialize(jsonGenerator, kVar);
    }

    @Override // com.fasterxml.jackson.databind.f
    public final void serializeWithType(e eVar, JsonGenerator jsonGenerator, k kVar, c cVar) throws IOException {
        eVar.serializeWithType(jsonGenerator, kVar, cVar);
    }
}
