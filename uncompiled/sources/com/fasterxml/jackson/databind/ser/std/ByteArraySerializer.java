package com.fasterxml.jackson.databind.ser.std;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.d;
import com.fasterxml.jackson.databind.jsonFormatVisitors.JsonFormatTypes;
import com.fasterxml.jackson.databind.jsonFormatVisitors.a;
import com.fasterxml.jackson.databind.jsonFormatVisitors.b;
import com.fasterxml.jackson.databind.jsontype.c;
import com.fasterxml.jackson.databind.k;
import java.io.IOException;
import java.lang.reflect.Type;

@pt1
/* loaded from: classes.dex */
public class ByteArraySerializer extends StdSerializer<byte[]> {
    private static final long serialVersionUID = 1;

    public ByteArraySerializer() {
        super(byte[].class);
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public void acceptJsonFormatVisitor(b bVar, JavaType javaType) throws JsonMappingException {
        a j;
        if (bVar == null || (j = bVar.j(javaType)) == null) {
            return;
        }
        j.i(JsonFormatTypes.INTEGER);
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, defpackage.ld3
    public d getSchema(k kVar, Type type) {
        return createSchemaNode("array", true).Z("items", createSchemaNode("byte"));
    }

    @Override // com.fasterxml.jackson.databind.f
    public boolean isEmpty(k kVar, byte[] bArr) {
        return bArr == null || bArr.length == 0;
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public void serialize(byte[] bArr, JsonGenerator jsonGenerator, k kVar) throws IOException {
        jsonGenerator.S(kVar.getConfig().getBase64Variant(), bArr, 0, bArr.length);
    }

    @Override // com.fasterxml.jackson.databind.f
    public void serializeWithType(byte[] bArr, JsonGenerator jsonGenerator, k kVar, c cVar) throws IOException {
        cVar.j(bArr, jsonGenerator);
        jsonGenerator.S(kVar.getConfig().getBase64Variant(), bArr, 0, bArr.length);
        cVar.n(bArr, jsonGenerator);
    }
}
