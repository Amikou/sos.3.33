package com.fasterxml.jackson.databind.ser.std;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.a;
import com.fasterxml.jackson.databind.d;
import com.fasterxml.jackson.databind.f;
import com.fasterxml.jackson.databind.jsonFormatVisitors.JsonValueFormat;
import com.fasterxml.jackson.databind.k;
import com.fasterxml.jackson.databind.ser.b;
import com.fasterxml.jackson.databind.util.StdDateFormat;
import java.io.IOException;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;
import org.web3j.abi.datatypes.Utf8String;

/* loaded from: classes.dex */
public abstract class DateTimeSerializerBase<T> extends StdScalarSerializer<T> implements b {
    public final DateFormat _customFormat;
    public final Boolean _useTimestamp;

    public DateTimeSerializerBase(Class<T> cls, Boolean bool, DateFormat dateFormat) {
        super(cls);
        this._useTimestamp = bool;
        this._customFormat = dateFormat;
    }

    public void _acceptJsonFormatVisitor(com.fasterxml.jackson.databind.jsonFormatVisitors.b bVar, JavaType javaType, boolean z) throws JsonMappingException {
        if (z) {
            visitIntFormat(bVar, javaType, JsonParser.NumberType.LONG, JsonValueFormat.UTC_MILLISEC);
        } else {
            visitStringFormat(bVar, javaType, JsonValueFormat.DATE_TIME);
        }
    }

    public boolean _asTimestamp(k kVar) {
        Boolean bool = this._useTimestamp;
        if (bool != null) {
            return bool.booleanValue();
        }
        if (this._customFormat == null) {
            if (kVar != null) {
                return kVar.isEnabled(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
            }
            throw new IllegalArgumentException("Null SerializerProvider passed for " + handledType().getName());
        }
        return false;
    }

    public abstract long _timestamp(T t);

    @Override // com.fasterxml.jackson.databind.ser.std.StdScalarSerializer, com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public void acceptJsonFormatVisitor(com.fasterxml.jackson.databind.jsonFormatVisitors.b bVar, JavaType javaType) throws JsonMappingException {
        _acceptJsonFormatVisitor(bVar, javaType, _asTimestamp(bVar.b()));
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fasterxml.jackson.databind.ser.b
    public f<?> createContextual(k kVar, a aVar) throws JsonMappingException {
        JsonFormat.Value findFormatOverrides;
        SimpleDateFormat simpleDateFormat;
        Locale locale;
        if (aVar == null || (findFormatOverrides = findFormatOverrides(kVar, aVar, handledType())) == null) {
            return this;
        }
        JsonFormat.Shape shape = findFormatOverrides.getShape();
        if (shape.isNumeric()) {
            return withFormat(Boolean.TRUE, null);
        }
        if (findFormatOverrides.hasPattern()) {
            if (findFormatOverrides.hasLocale()) {
                locale = findFormatOverrides.getLocale();
            } else {
                locale = kVar.getLocale();
            }
            SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat(findFormatOverrides.getPattern(), locale);
            simpleDateFormat2.setTimeZone(findFormatOverrides.hasTimeZone() ? findFormatOverrides.getTimeZone() : kVar.getTimeZone());
            return withFormat(Boolean.FALSE, simpleDateFormat2);
        }
        boolean hasLocale = findFormatOverrides.hasLocale();
        boolean hasTimeZone = findFormatOverrides.hasTimeZone();
        boolean z = true;
        boolean z2 = shape == JsonFormat.Shape.STRING;
        if (hasLocale || hasTimeZone || z2) {
            DateFormat dateFormat = kVar.getConfig().getDateFormat();
            if (dateFormat instanceof StdDateFormat) {
                StdDateFormat stdDateFormat = (StdDateFormat) dateFormat;
                if (findFormatOverrides.hasLocale()) {
                    stdDateFormat = stdDateFormat.withLocale(findFormatOverrides.getLocale());
                }
                if (findFormatOverrides.hasTimeZone()) {
                    stdDateFormat = stdDateFormat.withTimeZone(findFormatOverrides.getTimeZone());
                }
                return withFormat(Boolean.FALSE, stdDateFormat);
            }
            if (!(dateFormat instanceof SimpleDateFormat)) {
                kVar.reportMappingProblem("Configured `DateFormat` (%s) not a `SimpleDateFormat`; can not configure `Locale` or `TimeZone`", dateFormat.getClass().getName());
            }
            SimpleDateFormat simpleDateFormat3 = (SimpleDateFormat) dateFormat;
            if (hasLocale) {
                simpleDateFormat = new SimpleDateFormat(simpleDateFormat3.toPattern(), findFormatOverrides.getLocale());
            } else {
                simpleDateFormat = (SimpleDateFormat) simpleDateFormat3.clone();
            }
            TimeZone timeZone = findFormatOverrides.getTimeZone();
            if (timeZone == null || timeZone.equals(simpleDateFormat.getTimeZone())) {
                z = false;
            }
            if (z) {
                simpleDateFormat.setTimeZone(timeZone);
            }
            return withFormat(Boolean.FALSE, simpleDateFormat);
        }
        return this;
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdScalarSerializer, com.fasterxml.jackson.databind.ser.std.StdSerializer, defpackage.ld3
    public d getSchema(k kVar, Type type) {
        return createSchemaNode(_asTimestamp(kVar) ? "number" : Utf8String.TYPE_NAME, true);
    }

    @Override // com.fasterxml.jackson.databind.f
    @Deprecated
    public boolean isEmpty(T t) {
        return t == null || _timestamp(t) == 0;
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public abstract void serialize(T t, JsonGenerator jsonGenerator, k kVar) throws IOException;

    public abstract DateTimeSerializerBase<T> withFormat(Boolean bool, DateFormat dateFormat);

    @Override // com.fasterxml.jackson.databind.f
    public boolean isEmpty(k kVar, T t) {
        return t == null || _timestamp(t) == 0;
    }
}
