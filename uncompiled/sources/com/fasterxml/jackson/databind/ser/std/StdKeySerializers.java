package com.fasterxml.jackson.databind.ser.std;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.f;
import com.fasterxml.jackson.databind.jsonFormatVisitors.b;
import com.fasterxml.jackson.databind.k;
import com.fasterxml.jackson.databind.ser.impl.a;
import com.fasterxml.jackson.databind.util.EnumValues;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

/* loaded from: classes.dex */
public class StdKeySerializers {
    public static final f<Object> a = new StdKeySerializer();
    public static final f<Object> b = new StringKeySerializer();

    /* loaded from: classes.dex */
    public static class Default extends StdSerializer<Object> {
        public static final int TYPE_CALENDAR = 2;
        public static final int TYPE_CLASS = 3;
        public static final int TYPE_DATE = 1;
        public static final int TYPE_ENUM = 4;
        public static final int TYPE_TO_STRING = 5;
        public final int _typeId;

        public Default(int i, Class<?> cls) {
            super(cls, false);
            this._typeId = i;
        }

        @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
        public void serialize(Object obj, JsonGenerator jsonGenerator, k kVar) throws IOException {
            String name;
            int i = this._typeId;
            if (i == 1) {
                kVar.defaultSerializeDateKey((Date) obj, jsonGenerator);
            } else if (i == 2) {
                kVar.defaultSerializeDateKey(((Calendar) obj).getTimeInMillis(), jsonGenerator);
            } else if (i == 3) {
                jsonGenerator.i0(((Class) obj).getName());
            } else if (i != 4) {
                jsonGenerator.i0(obj.toString());
            } else {
                if (kVar.isEnabled(SerializationFeature.WRITE_ENUMS_USING_TO_STRING)) {
                    name = obj.toString();
                } else {
                    Enum r3 = (Enum) obj;
                    if (kVar.isEnabled(SerializationFeature.WRITE_ENUMS_USING_INDEX)) {
                        name = String.valueOf(r3.ordinal());
                    } else {
                        name = r3.name();
                    }
                }
                jsonGenerator.i0(name);
            }
        }
    }

    /* loaded from: classes.dex */
    public static class Dynamic extends StdSerializer<Object> {
        public transient a _dynamicSerializers;

        public Dynamic() {
            super(String.class, false);
            this._dynamicSerializers = a.a();
        }

        public f<Object> _findAndAddDynamic(a aVar, Class<?> cls, k kVar) throws JsonMappingException {
            if (cls == Object.class) {
                Default r4 = new Default(5, cls);
                this._dynamicSerializers = aVar.h(cls, r4);
                return r4;
            }
            a.d c = aVar.c(cls, kVar, null);
            a aVar2 = c.b;
            if (aVar != aVar2) {
                this._dynamicSerializers = aVar2;
            }
            return c.a;
        }

        @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
        public void acceptJsonFormatVisitor(b bVar, JavaType javaType) throws JsonMappingException {
            visitStringFormat(bVar, javaType);
        }

        public Object readResolve() {
            this._dynamicSerializers = a.a();
            return this;
        }

        @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
        public void serialize(Object obj, JsonGenerator jsonGenerator, k kVar) throws IOException {
            Class<?> cls = obj.getClass();
            a aVar = this._dynamicSerializers;
            f<Object> i = aVar.i(cls);
            if (i == null) {
                i = _findAndAddDynamic(aVar, cls, kVar);
            }
            i.serialize(obj, jsonGenerator, kVar);
        }
    }

    /* loaded from: classes.dex */
    public static class EnumKeySerializer extends StdSerializer<Object> {
        public final EnumValues _values;

        public EnumKeySerializer(Class<?> cls, EnumValues enumValues) {
            super(cls, false);
            this._values = enumValues;
        }

        public static EnumKeySerializer construct(Class<?> cls, EnumValues enumValues) {
            return new EnumKeySerializer(cls, enumValues);
        }

        @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
        public void serialize(Object obj, JsonGenerator jsonGenerator, k kVar) throws IOException {
            if (kVar.isEnabled(SerializationFeature.WRITE_ENUMS_USING_TO_STRING)) {
                jsonGenerator.i0(obj.toString());
                return;
            }
            Enum<?> r2 = (Enum) obj;
            if (kVar.isEnabled(SerializationFeature.WRITE_ENUMS_USING_INDEX)) {
                jsonGenerator.i0(String.valueOf(r2.ordinal()));
            } else {
                jsonGenerator.g0(this._values.serializedValueFor(r2));
            }
        }
    }

    /* loaded from: classes.dex */
    public static class StringKeySerializer extends StdSerializer<Object> {
        public StringKeySerializer() {
            super(String.class, false);
        }

        @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
        public void serialize(Object obj, JsonGenerator jsonGenerator, k kVar) throws IOException {
            jsonGenerator.i0((String) obj);
        }
    }

    public static f<Object> a(SerializationConfig serializationConfig, Class<?> cls) {
        if (cls != null) {
            if (cls == Enum.class) {
                return new Dynamic();
            }
            if (cls.isEnum()) {
                return EnumKeySerializer.construct(cls, EnumValues.constructFromName(serializationConfig, cls));
            }
        }
        return a;
    }

    public static f<Object> b(SerializationConfig serializationConfig, Class<?> cls, boolean z) {
        if (cls != null && cls != Object.class) {
            if (cls == String.class) {
                return b;
            }
            if (!cls.isPrimitive() && !Number.class.isAssignableFrom(cls)) {
                if (cls == Class.class) {
                    return new Default(3, cls);
                }
                if (Date.class.isAssignableFrom(cls)) {
                    return new Default(1, cls);
                }
                if (Calendar.class.isAssignableFrom(cls)) {
                    return new Default(2, cls);
                }
                if (cls == UUID.class) {
                    return new Default(5, cls);
                }
                if (z) {
                    return a;
                }
                return null;
            }
            return new Default(5, cls);
        }
        return new Dynamic();
    }
}
