package com.fasterxml.jackson.databind.ser.std;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.a;
import com.fasterxml.jackson.databind.f;
import com.fasterxml.jackson.databind.jsontype.c;
import com.fasterxml.jackson.databind.k;
import com.fasterxml.jackson.databind.ser.ContainerSerializer;
import java.io.IOException;
import java.util.Iterator;

@pt1
/* loaded from: classes.dex */
public class IterableSerializer extends AsArraySerializerBase<Iterable<?>> {
    public IterableSerializer(JavaType javaType, boolean z, c cVar) {
        super(Iterable.class, javaType, z, cVar, (f<Object>) null);
    }

    @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
    public ContainerSerializer<?> _withValueTypeSerializer(c cVar) {
        return new IterableSerializer(this, this._property, cVar, this._elementSerializer, this._unwrapSingle);
    }

    @Override // com.fasterxml.jackson.databind.ser.std.AsArraySerializerBase
    public /* bridge */ /* synthetic */ AsArraySerializerBase<Iterable<?>> withResolved(a aVar, c cVar, f fVar, Boolean bool) {
        return withResolved2(aVar, cVar, (f<?>) fVar, bool);
    }

    public IterableSerializer(IterableSerializer iterableSerializer, a aVar, c cVar, f<?> fVar, Boolean bool) {
        super(iterableSerializer, aVar, cVar, fVar, bool);
    }

    @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
    public boolean hasSingleElement(Iterable<?> iterable) {
        if (iterable != null) {
            Iterator<?> it = iterable.iterator();
            if (it.hasNext()) {
                it.next();
                return !it.hasNext();
            }
            return false;
        }
        return false;
    }

    @Override // com.fasterxml.jackson.databind.f
    public boolean isEmpty(k kVar, Iterable<?> iterable) {
        return iterable == null || !iterable.iterator().hasNext();
    }

    @Override // com.fasterxml.jackson.databind.ser.std.AsArraySerializerBase, com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public final void serialize(Iterable<?> iterable, JsonGenerator jsonGenerator, k kVar) throws IOException {
        if (((this._unwrapSingle == null && kVar.isEnabled(SerializationFeature.WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED)) || this._unwrapSingle == Boolean.TRUE) && hasSingleElement(iterable)) {
            serializeContents(iterable, jsonGenerator, kVar);
            return;
        }
        jsonGenerator.e1();
        serializeContents(iterable, jsonGenerator, kVar);
        jsonGenerator.e0();
    }

    @Override // com.fasterxml.jackson.databind.ser.std.AsArraySerializerBase
    public void serializeContents(Iterable<?> iterable, JsonGenerator jsonGenerator, k kVar) throws IOException {
        f<Object> fVar;
        Iterator<?> it = iterable.iterator();
        if (it.hasNext()) {
            c cVar = this._valueTypeSerializer;
            Class<?> cls = null;
            f<Object> fVar2 = null;
            do {
                Object next = it.next();
                if (next == null) {
                    kVar.defaultSerializeNull(jsonGenerator);
                } else {
                    f<Object> fVar3 = this._elementSerializer;
                    if (fVar3 == null) {
                        Class<?> cls2 = next.getClass();
                        if (cls2 != cls) {
                            fVar2 = kVar.findValueSerializer(cls2, this._property);
                            cls = cls2;
                        }
                        fVar = fVar2;
                    } else {
                        fVar = fVar2;
                        fVar2 = fVar3;
                    }
                    if (cVar == null) {
                        fVar2.serialize(next, jsonGenerator, kVar);
                    } else {
                        fVar2.serializeWithType(next, jsonGenerator, kVar, cVar);
                    }
                    fVar2 = fVar;
                }
            } while (it.hasNext());
        }
    }

    @Override // com.fasterxml.jackson.databind.ser.std.AsArraySerializerBase
    /* renamed from: withResolved  reason: avoid collision after fix types in other method */
    public AsArraySerializerBase<Iterable<?>> withResolved2(a aVar, c cVar, f<?> fVar, Boolean bool) {
        return new IterableSerializer(this, aVar, cVar, fVar, bool);
    }
}
