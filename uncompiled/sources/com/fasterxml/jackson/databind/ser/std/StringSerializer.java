package com.fasterxml.jackson.databind.ser.std;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.d;
import com.fasterxml.jackson.databind.jsonFormatVisitors.b;
import com.fasterxml.jackson.databind.k;
import java.io.IOException;
import java.lang.reflect.Type;
import org.web3j.abi.datatypes.Utf8String;

@pt1
/* loaded from: classes.dex */
public final class StringSerializer extends NonTypedScalarSerializerBase<Object> {
    private static final long serialVersionUID = 1;

    public StringSerializer() {
        super(String.class, false);
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdScalarSerializer, com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public void acceptJsonFormatVisitor(b bVar, JavaType javaType) throws JsonMappingException {
        visitStringFormat(bVar, javaType);
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdScalarSerializer, com.fasterxml.jackson.databind.ser.std.StdSerializer, defpackage.ld3
    public d getSchema(k kVar, Type type) {
        return createSchemaNode(Utf8String.TYPE_NAME, true);
    }

    @Override // com.fasterxml.jackson.databind.f
    @Deprecated
    public boolean isEmpty(Object obj) {
        String str = (String) obj;
        return str == null || str.length() == 0;
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public void serialize(Object obj, JsonGenerator jsonGenerator, k kVar) throws IOException {
        jsonGenerator.o1((String) obj);
    }

    @Override // com.fasterxml.jackson.databind.f
    public boolean isEmpty(k kVar, Object obj) {
        String str = (String) obj;
        return str == null || str.length() == 0;
    }
}
