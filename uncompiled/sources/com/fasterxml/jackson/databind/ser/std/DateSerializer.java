package com.fasterxml.jackson.databind.ser.std;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.k;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;

@pt1
/* loaded from: classes.dex */
public class DateSerializer extends DateTimeSerializerBase<Date> {
    public static final DateSerializer instance = new DateSerializer();

    public DateSerializer() {
        this(null, null);
    }

    public DateSerializer(Boolean bool, DateFormat dateFormat) {
        super(Date.class, bool, dateFormat);
    }

    @Override // com.fasterxml.jackson.databind.ser.std.DateTimeSerializerBase
    public long _timestamp(Date date) {
        if (date == null) {
            return 0L;
        }
        return date.getTime();
    }

    @Override // com.fasterxml.jackson.databind.ser.std.DateTimeSerializerBase, com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public void serialize(Date date, JsonGenerator jsonGenerator, k kVar) throws IOException {
        if (_asTimestamp(kVar)) {
            jsonGenerator.y0(_timestamp(date));
            return;
        }
        DateFormat dateFormat = this._customFormat;
        if (dateFormat != null) {
            synchronized (dateFormat) {
                jsonGenerator.o1(this._customFormat.format(date));
            }
            return;
        }
        kVar.defaultSerializeDateValue(date, jsonGenerator);
    }

    @Override // com.fasterxml.jackson.databind.ser.std.DateTimeSerializerBase
    /* renamed from: withFormat */
    public DateTimeSerializerBase<Date> withFormat2(Boolean bool, DateFormat dateFormat) {
        return new DateSerializer(bool, dateFormat);
    }
}
