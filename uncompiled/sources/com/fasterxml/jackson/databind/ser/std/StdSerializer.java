package com.fasterxml.jackson.databind.ser.std;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.a;
import com.fasterxml.jackson.databind.f;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.fasterxml.jackson.databind.jsonFormatVisitors.JsonFormatTypes;
import com.fasterxml.jackson.databind.jsonFormatVisitors.JsonValueFormat;
import com.fasterxml.jackson.databind.jsonFormatVisitors.b;
import com.fasterxml.jackson.databind.k;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.m;
import com.fasterxml.jackson.databind.ser.d;
import com.fasterxml.jackson.databind.util.c;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import org.web3j.abi.datatypes.Utf8String;

/* loaded from: classes.dex */
public abstract class StdSerializer<T> extends f<T> implements zu1, ld3 {
    public static final Object a = new Object();
    private static final long serialVersionUID = 1;
    public final Class<T> _handledType;

    public StdSerializer(Class<T> cls) {
        this._handledType = cls;
    }

    @Override // com.fasterxml.jackson.databind.f
    public void acceptJsonFormatVisitor(b bVar, JavaType javaType) throws JsonMappingException {
        bVar.e(javaType);
    }

    public m createObjectNode() {
        return JsonNodeFactory.instance.objectNode();
    }

    public m createSchemaNode(String str) {
        m createObjectNode = createObjectNode();
        createObjectNode.T("type", str);
        return createObjectNode;
    }

    public f<?> findAnnotatedContentSerializer(k kVar, a aVar) throws JsonMappingException {
        Object findContentSerializer;
        if (aVar != null) {
            AnnotatedMember member = aVar.getMember();
            AnnotationIntrospector annotationIntrospector = kVar.getAnnotationIntrospector();
            if (member == null || (findContentSerializer = annotationIntrospector.findContentSerializer(member)) == null) {
                return null;
            }
            return kVar.serializerInstance(member, findContentSerializer);
        }
        return null;
    }

    public f<?> findConvertingContentSerializer(k kVar, a aVar, f<?> fVar) throws JsonMappingException {
        AnnotationIntrospector annotationIntrospector;
        AnnotatedMember member;
        Object obj = a;
        Object attribute = kVar.getAttribute(obj);
        if ((attribute == null || attribute != Boolean.TRUE) && (annotationIntrospector = kVar.getAnnotationIntrospector()) != null && aVar != null && (member = aVar.getMember()) != null) {
            kVar.setAttribute(obj, (Object) Boolean.TRUE);
            try {
                Object findSerializationContentConverter = annotationIntrospector.findSerializationContentConverter(member);
                kVar.setAttribute(obj, (Object) null);
                if (findSerializationContentConverter != null) {
                    o80<Object, Object> converterInstance = kVar.converterInstance(aVar.getMember(), findSerializationContentConverter);
                    JavaType c = converterInstance.c(kVar.getTypeFactory());
                    if (fVar == null && !c.isJavaLangObject()) {
                        fVar = kVar.findValueSerializer(c);
                    }
                    return new StdDelegatingSerializer(converterInstance, c, fVar);
                }
            } catch (Throwable th) {
                kVar.setAttribute(a, (Object) null);
                throw th;
            }
        }
        return fVar;
    }

    public Boolean findFormatFeature(k kVar, a aVar, Class<?> cls, JsonFormat.Feature feature) {
        JsonFormat.Value findFormatOverrides = findFormatOverrides(kVar, aVar, cls);
        if (findFormatOverrides != null) {
            return findFormatOverrides.getFeature(feature);
        }
        return null;
    }

    public JsonFormat.Value findFormatOverrides(k kVar, a aVar, Class<?> cls) {
        if (aVar != null) {
            return aVar.findPropertyFormat(kVar.getConfig(), cls);
        }
        return kVar.getDefaultPropertyFormat(cls);
    }

    public JsonInclude.Value findIncludeOverrides(k kVar, a aVar, Class<?> cls) {
        if (aVar != null) {
            return aVar.findPropertyInclusion(kVar.getConfig(), cls);
        }
        return kVar.getDefaultPropertyInclusion(cls);
    }

    public d findPropertyFilter(k kVar, Object obj, Object obj2) throws JsonMappingException {
        kVar.getFilterProvider();
        throw JsonMappingException.from(kVar, "Can not resolve PropertyFilter with id '" + obj + "'; no FilterProvider configured");
    }

    public com.fasterxml.jackson.databind.d getSchema(k kVar, Type type) throws JsonMappingException {
        return createSchemaNode(Utf8String.TYPE_NAME);
    }

    @Override // com.fasterxml.jackson.databind.f
    public Class<T> handledType() {
        return this._handledType;
    }

    public boolean isDefaultSerializer(f<?> fVar) {
        return c.J(fVar);
    }

    @Override // com.fasterxml.jackson.databind.f
    public abstract void serialize(T t, JsonGenerator jsonGenerator, k kVar) throws IOException;

    public void visitArrayFormat(b bVar, JavaType javaType, f<?> fVar, JavaType javaType2) throws JsonMappingException {
        com.fasterxml.jackson.databind.jsonFormatVisitors.a j;
        if (bVar == null || (j = bVar.j(javaType)) == null || fVar == null) {
            return;
        }
        j.n(fVar, javaType2);
    }

    public void visitFloatFormat(b bVar, JavaType javaType, JsonParser.NumberType numberType) throws JsonMappingException {
        fv1 f;
        if (bVar == null || (f = bVar.f(javaType)) == null) {
            return;
        }
        f.a(numberType);
    }

    public void visitIntFormat(b bVar, JavaType javaType, JsonParser.NumberType numberType) throws JsonMappingException {
        cv1 c;
        if (bVar == null || (c = bVar.c(javaType)) == null || numberType == null) {
            return;
        }
        c.a(numberType);
    }

    public void visitStringFormat(b bVar, JavaType javaType) throws JsonMappingException {
        if (bVar != null) {
            bVar.d(javaType);
        }
    }

    public void wrapAndThrow(k kVar, Throwable th, Object obj, String str) throws IOException {
        while ((th instanceof InvocationTargetException) && th.getCause() != null) {
            th = th.getCause();
        }
        if (!(th instanceof Error)) {
            boolean z = kVar == null || kVar.isEnabled(SerializationFeature.WRAP_EXCEPTIONS);
            if (th instanceof IOException) {
                if (!z || !(th instanceof JsonMappingException)) {
                    throw ((IOException) th);
                }
            } else if (!z && (th instanceof RuntimeException)) {
                throw ((RuntimeException) th);
            }
            throw JsonMappingException.wrapWithPath(th, obj, str);
        }
        throw ((Error) th);
    }

    public com.fasterxml.jackson.databind.d getSchema(k kVar, Type type, boolean z) throws JsonMappingException {
        m mVar = (m) getSchema(kVar, type);
        if (!z) {
            mVar.U("required", !z);
        }
        return mVar;
    }

    public void visitStringFormat(b bVar, JavaType javaType, JsonValueFormat jsonValueFormat) throws JsonMappingException {
        ew1 d;
        if (bVar == null || (d = bVar.d(javaType)) == null) {
            return;
        }
        d.c(jsonValueFormat);
    }

    public StdSerializer(JavaType javaType) {
        this._handledType = (Class<T>) javaType.getRawClass();
    }

    public m createSchemaNode(String str, boolean z) {
        m createSchemaNode = createSchemaNode(str);
        if (!z) {
            createSchemaNode.U("required", !z);
        }
        return createSchemaNode;
    }

    public void visitArrayFormat(b bVar, JavaType javaType, JsonFormatTypes jsonFormatTypes) throws JsonMappingException {
        com.fasterxml.jackson.databind.jsonFormatVisitors.a j;
        if (bVar == null || (j = bVar.j(javaType)) == null) {
            return;
        }
        j.i(jsonFormatTypes);
    }

    public void visitIntFormat(b bVar, JavaType javaType, JsonParser.NumberType numberType, JsonValueFormat jsonValueFormat) throws JsonMappingException {
        cv1 c;
        if (bVar == null || (c = bVar.c(javaType)) == null) {
            return;
        }
        if (numberType != null) {
            c.a(numberType);
        }
        if (jsonValueFormat != null) {
            c.c(jsonValueFormat);
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public StdSerializer(Class<?> cls, boolean z) {
        this._handledType = cls;
    }

    public StdSerializer(StdSerializer<?> stdSerializer) {
        this._handledType = (Class<T>) stdSerializer._handledType;
    }

    public void wrapAndThrow(k kVar, Throwable th, Object obj, int i) throws IOException {
        while ((th instanceof InvocationTargetException) && th.getCause() != null) {
            th = th.getCause();
        }
        if (!(th instanceof Error)) {
            boolean z = kVar == null || kVar.isEnabled(SerializationFeature.WRAP_EXCEPTIONS);
            if (th instanceof IOException) {
                if (!z || !(th instanceof JsonMappingException)) {
                    throw ((IOException) th);
                }
            } else if (!z && (th instanceof RuntimeException)) {
                throw ((RuntimeException) th);
            }
            throw JsonMappingException.wrapWithPath(th, obj, i);
        }
        throw ((Error) th);
    }
}
