package com.fasterxml.jackson.databind.ser.std;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.a;
import com.fasterxml.jackson.databind.f;
import com.fasterxml.jackson.databind.jsontype.c;
import com.fasterxml.jackson.databind.type.ReferenceType;
import com.fasterxml.jackson.databind.util.NameTransformer;
import java.util.concurrent.atomic.AtomicReference;

/* loaded from: classes.dex */
public class AtomicReferenceSerializer extends ReferenceTypeSerializer<AtomicReference<?>> {
    private static final long serialVersionUID = 1;

    public AtomicReferenceSerializer(ReferenceType referenceType, boolean z, c cVar, f<Object> fVar) {
        super(referenceType, z, cVar, fVar);
    }

    @Override // com.fasterxml.jackson.databind.ser.std.ReferenceTypeSerializer
    /* renamed from: withResolved  reason: avoid collision after fix types in other method */
    public /* bridge */ /* synthetic */ ReferenceTypeSerializer<AtomicReference<?>> withResolved2(a aVar, c cVar, f fVar, NameTransformer nameTransformer, JsonInclude.Include include) {
        return withResolved(aVar, cVar, (f<?>) fVar, nameTransformer, include);
    }

    public AtomicReferenceSerializer(AtomicReferenceSerializer atomicReferenceSerializer, a aVar, c cVar, f<?> fVar, NameTransformer nameTransformer, JsonInclude.Include include) {
        super(atomicReferenceSerializer, aVar, cVar, fVar, nameTransformer, include);
    }

    @Override // com.fasterxml.jackson.databind.ser.std.ReferenceTypeSerializer
    public Object _getReferenced(AtomicReference<?> atomicReference) {
        return atomicReference.get();
    }

    @Override // com.fasterxml.jackson.databind.ser.std.ReferenceTypeSerializer
    public Object _getReferencedIfPresent(AtomicReference<?> atomicReference) {
        return atomicReference.get();
    }

    @Override // com.fasterxml.jackson.databind.ser.std.ReferenceTypeSerializer
    public boolean _isValueEmpty(AtomicReference<?> atomicReference) {
        return atomicReference.get() == null;
    }

    @Override // com.fasterxml.jackson.databind.ser.std.ReferenceTypeSerializer
    public ReferenceTypeSerializer<AtomicReference<?>> withResolved(a aVar, c cVar, f<?> fVar, NameTransformer nameTransformer, JsonInclude.Include include) {
        return (this._property == aVar && include == this._contentInclusion && this._valueTypeSerializer == cVar && this._valueSerializer == fVar && this._unwrapper == nameTransformer) ? this : new AtomicReferenceSerializer(this, aVar, cVar, fVar, nameTransformer, include);
    }
}
