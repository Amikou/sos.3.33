package com.fasterxml.jackson.databind.ser.std;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.a;
import com.fasterxml.jackson.databind.d;
import com.fasterxml.jackson.databind.f;
import com.fasterxml.jackson.databind.jsontype.c;
import com.fasterxml.jackson.databind.k;
import com.fasterxml.jackson.databind.ser.b;
import com.fasterxml.jackson.databind.ser.e;
import java.io.IOException;
import java.lang.reflect.Type;

/* loaded from: classes.dex */
public class StdDelegatingSerializer extends StdSerializer<Object> implements b, e {
    public final o80<Object, ?> _converter;
    public final f<Object> _delegateSerializer;
    public final JavaType _delegateType;

    public StdDelegatingSerializer(o80<?, ?> o80Var) {
        super(Object.class);
        this._converter = o80Var;
        this._delegateType = null;
        this._delegateSerializer = null;
    }

    public f<Object> _findSerializer(Object obj, k kVar) throws JsonMappingException {
        return kVar.findValueSerializer(obj.getClass());
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public void acceptJsonFormatVisitor(com.fasterxml.jackson.databind.jsonFormatVisitors.b bVar, JavaType javaType) throws JsonMappingException {
        f<Object> fVar = this._delegateSerializer;
        if (fVar != null) {
            fVar.acceptJsonFormatVisitor(bVar, javaType);
        }
    }

    public Object convertValue(Object obj) {
        return this._converter.a(obj);
    }

    @Override // com.fasterxml.jackson.databind.ser.b
    public f<?> createContextual(k kVar, a aVar) throws JsonMappingException {
        f<?> fVar = this._delegateSerializer;
        JavaType javaType = this._delegateType;
        if (fVar == null) {
            if (javaType == null) {
                javaType = this._converter.c(kVar.getTypeFactory());
            }
            if (!javaType.isJavaLangObject()) {
                fVar = kVar.findValueSerializer(javaType);
            }
        }
        if (fVar instanceof b) {
            fVar = kVar.handleSecondaryContextualization(fVar, aVar);
        }
        return (fVar == this._delegateSerializer && javaType == this._delegateType) ? this : withDelegate(this._converter, javaType, fVar);
    }

    public o80<Object, ?> getConverter() {
        return this._converter;
    }

    @Override // com.fasterxml.jackson.databind.f
    public f<?> getDelegatee() {
        return this._delegateSerializer;
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, defpackage.ld3
    public d getSchema(k kVar, Type type) throws JsonMappingException {
        f<Object> fVar = this._delegateSerializer;
        if (fVar instanceof ld3) {
            return ((ld3) fVar).getSchema(kVar, type);
        }
        return super.getSchema(kVar, type);
    }

    @Override // com.fasterxml.jackson.databind.f
    @Deprecated
    public boolean isEmpty(Object obj) {
        return isEmpty(null, obj);
    }

    @Override // com.fasterxml.jackson.databind.ser.e
    public void resolve(k kVar) throws JsonMappingException {
        f<Object> fVar = this._delegateSerializer;
        if (fVar == null || !(fVar instanceof e)) {
            return;
        }
        ((e) fVar).resolve(kVar);
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public void serialize(Object obj, JsonGenerator jsonGenerator, k kVar) throws IOException {
        Object convertValue = convertValue(obj);
        if (convertValue == null) {
            kVar.defaultSerializeNull(jsonGenerator);
            return;
        }
        f<Object> fVar = this._delegateSerializer;
        if (fVar == null) {
            fVar = _findSerializer(convertValue, kVar);
        }
        fVar.serialize(convertValue, jsonGenerator, kVar);
    }

    @Override // com.fasterxml.jackson.databind.f
    public void serializeWithType(Object obj, JsonGenerator jsonGenerator, k kVar, c cVar) throws IOException {
        Object convertValue = convertValue(obj);
        f<Object> fVar = this._delegateSerializer;
        if (fVar == null) {
            fVar = _findSerializer(obj, kVar);
        }
        fVar.serializeWithType(convertValue, jsonGenerator, kVar, cVar);
    }

    public StdDelegatingSerializer withDelegate(o80<Object, ?> o80Var, JavaType javaType, f<?> fVar) {
        if (getClass() == StdDelegatingSerializer.class) {
            return new StdDelegatingSerializer(o80Var, javaType, fVar);
        }
        throw new IllegalStateException("Sub-class " + getClass().getName() + " must override 'withDelegate'");
    }

    @Override // com.fasterxml.jackson.databind.f
    public boolean isEmpty(k kVar, Object obj) {
        Object convertValue = convertValue(obj);
        f<Object> fVar = this._delegateSerializer;
        if (fVar == null) {
            return obj == null;
        }
        return fVar.isEmpty(kVar, convertValue);
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, defpackage.ld3
    public d getSchema(k kVar, Type type, boolean z) throws JsonMappingException {
        f<Object> fVar = this._delegateSerializer;
        if (fVar instanceof ld3) {
            return ((ld3) fVar).getSchema(kVar, type, z);
        }
        return super.getSchema(kVar, type);
    }

    public <T> StdDelegatingSerializer(Class<T> cls, o80<T, ?> o80Var) {
        super(cls, false);
        this._converter = o80Var;
        this._delegateType = null;
        this._delegateSerializer = null;
    }

    public StdDelegatingSerializer(o80<Object, ?> o80Var, JavaType javaType, f<?> fVar) {
        super(javaType);
        this._converter = o80Var;
        this._delegateType = javaType;
        this._delegateSerializer = fVar;
    }
}
