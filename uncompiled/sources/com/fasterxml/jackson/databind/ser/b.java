package com.fasterxml.jackson.databind.ser;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.k;

/* compiled from: ContextualSerializer.java */
/* loaded from: classes.dex */
public interface b {
    com.fasterxml.jackson.databind.f<?> createContextual(k kVar, com.fasterxml.jackson.databind.a aVar) throws JsonMappingException;
}
