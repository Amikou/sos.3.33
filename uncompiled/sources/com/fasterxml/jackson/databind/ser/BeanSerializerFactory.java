package com.fasterxml.jackson.databind.ser;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerator;
import com.fasterxml.jackson.annotation.ObjectIdGenerators$PropertyGenerator;
import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.PropertyMetadata;
import com.fasterxml.jackson.databind.PropertyName;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.a;
import com.fasterxml.jackson.databind.cfg.SerializerFactoryConfig;
import com.fasterxml.jackson.databind.introspect.AnnotatedField;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.fasterxml.jackson.databind.introspect.AnnotatedMethod;
import com.fasterxml.jackson.databind.k;
import com.fasterxml.jackson.databind.ser.impl.FilteredBeanPropertyWriter;
import com.fasterxml.jackson.databind.ser.impl.PropertyBasedObjectIdGenerator;
import com.fasterxml.jackson.databind.ser.std.AtomicReferenceSerializer;
import com.fasterxml.jackson.databind.ser.std.MapSerializer;
import com.fasterxml.jackson.databind.ser.std.StdDelegatingSerializer;
import com.fasterxml.jackson.databind.type.ReferenceType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

/* loaded from: classes.dex */
public class BeanSerializerFactory extends BasicSerializerFactory {
    public static final BeanSerializerFactory instance = new BeanSerializerFactory(null);
    private static final long serialVersionUID = 1;

    public BeanSerializerFactory(SerializerFactoryConfig serializerFactoryConfig) {
        super(serializerFactoryConfig);
    }

    public BeanPropertyWriter _constructWriter(k kVar, vo voVar, c cVar, boolean z, AnnotatedMember annotatedMember) throws JsonMappingException {
        PropertyName o = voVar.o();
        JavaType type = annotatedMember.getType();
        a.C0091a c0091a = new a.C0091a(o, type, voVar.y(), cVar.d(), annotatedMember, voVar.r());
        com.fasterxml.jackson.databind.f<?> findSerializerFromAnnotation = findSerializerFromAnnotation(kVar, annotatedMember);
        if (findSerializerFromAnnotation instanceof e) {
            ((e) findSerializerFromAnnotation).resolve(kVar);
        }
        com.fasterxml.jackson.databind.f<?> handlePrimaryContextualization = kVar.handlePrimaryContextualization(findSerializerFromAnnotation, c0091a);
        com.fasterxml.jackson.databind.jsontype.c cVar2 = null;
        if (type.isContainerType() || type.isReferenceType()) {
            cVar2 = findPropertyContentTypeSerializer(type, kVar.getConfig(), annotatedMember);
        }
        return cVar.b(kVar, voVar, type, handlePrimaryContextualization, findPropertyTypeSerializer(type, kVar.getConfig(), annotatedMember), cVar2, annotatedMember, z);
    }

    public com.fasterxml.jackson.databind.f<?> _createSerializer2(k kVar, JavaType javaType, so soVar, boolean z) throws JsonMappingException {
        com.fasterxml.jackson.databind.f<?> fVar;
        SerializationConfig config = kVar.getConfig();
        com.fasterxml.jackson.databind.f<?> fVar2 = null;
        if (javaType.isContainerType()) {
            if (!z) {
                z = usesStaticTyping(config, soVar, null);
            }
            fVar = buildContainerSerializer(kVar, javaType, soVar, z);
            if (fVar != null) {
                return fVar;
            }
        } else {
            if (javaType.isReferenceType()) {
                fVar = findReferenceSerializer(kVar, (ReferenceType) javaType, soVar, z);
            } else {
                Iterator<am3> it = customSerializers().iterator();
                while (it.hasNext() && (fVar2 = it.next().findSerializer(config, javaType, soVar)) == null) {
                }
                fVar = fVar2;
            }
            if (fVar == null) {
                fVar = findSerializerByAnnotations(kVar, javaType, soVar);
            }
        }
        if (fVar == null && (fVar = findSerializerByLookup(javaType, config, soVar, z)) == null && (fVar = findSerializerByPrimaryType(kVar, javaType, soVar, z)) == null && (fVar = findBeanSerializer(kVar, javaType, soVar)) == null && (fVar = findSerializerByAddonType(config, javaType, soVar, z)) == null) {
            fVar = kVar.getUnknownTypeSerializer(soVar.r());
        }
        if (fVar != null && this._factoryConfig.hasSerializerModifiers()) {
            for (xo xoVar : this._factoryConfig.serializerModifiers()) {
                fVar = xoVar.i(config, soVar, fVar);
            }
        }
        return fVar;
    }

    public com.fasterxml.jackson.databind.f<Object> constructBeanSerializer(k kVar, so soVar) throws JsonMappingException {
        List<BeanPropertyWriter> removeOverlappingTypeIds;
        if (soVar.r() == Object.class) {
            return kVar.getUnknownTypeSerializer(Object.class);
        }
        SerializationConfig config = kVar.getConfig();
        wo constructBeanSerializerBuilder = constructBeanSerializerBuilder(soVar);
        constructBeanSerializerBuilder.j(config);
        List<BeanPropertyWriter> findBeanProperties = findBeanProperties(kVar, soVar, constructBeanSerializerBuilder);
        if (findBeanProperties == null) {
            removeOverlappingTypeIds = new ArrayList<>();
        } else {
            removeOverlappingTypeIds = removeOverlappingTypeIds(kVar, soVar, constructBeanSerializerBuilder, findBeanProperties);
        }
        kVar.getAnnotationIntrospector().findAndAddVirtualProperties(config, soVar.t(), removeOverlappingTypeIds);
        if (this._factoryConfig.hasSerializerModifiers()) {
            for (xo xoVar : this._factoryConfig.serializerModifiers()) {
                removeOverlappingTypeIds = xoVar.a(config, soVar, removeOverlappingTypeIds);
            }
        }
        List<BeanPropertyWriter> filterBeanProperties = filterBeanProperties(config, soVar, removeOverlappingTypeIds);
        if (this._factoryConfig.hasSerializerModifiers()) {
            for (xo xoVar2 : this._factoryConfig.serializerModifiers()) {
                filterBeanProperties = xoVar2.j(config, soVar, filterBeanProperties);
            }
        }
        constructBeanSerializerBuilder.m(constructObjectIdHandler(kVar, soVar, filterBeanProperties));
        constructBeanSerializerBuilder.n(filterBeanProperties);
        constructBeanSerializerBuilder.k(findFilterId(config, soVar));
        AnnotatedMember a = soVar.a();
        if (a != null) {
            JavaType type = a.getType();
            boolean isEnabled = config.isEnabled(MapperFeature.USE_STATIC_TYPING);
            JavaType contentType = type.getContentType();
            com.fasterxml.jackson.databind.jsontype.c createTypeSerializer = createTypeSerializer(config, contentType);
            com.fasterxml.jackson.databind.f<Object> findSerializerFromAnnotation = findSerializerFromAnnotation(kVar, a);
            if (findSerializerFromAnnotation == null) {
                findSerializerFromAnnotation = MapSerializer.construct((Set<String>) null, type, isEnabled, createTypeSerializer, (com.fasterxml.jackson.databind.f<Object>) null, (com.fasterxml.jackson.databind.f<Object>) null, (Object) null);
            }
            constructBeanSerializerBuilder.i(new a(new a.C0091a(PropertyName.construct(a.getName()), contentType, null, soVar.s(), a, PropertyMetadata.STD_OPTIONAL), a, findSerializerFromAnnotation));
        }
        processViews(config, constructBeanSerializerBuilder);
        if (this._factoryConfig.hasSerializerModifiers()) {
            for (xo xoVar3 : this._factoryConfig.serializerModifiers()) {
                constructBeanSerializerBuilder = xoVar3.k(config, soVar, constructBeanSerializerBuilder);
            }
        }
        com.fasterxml.jackson.databind.f<?> a2 = constructBeanSerializerBuilder.a();
        return (a2 == null && soVar.z()) ? constructBeanSerializerBuilder.b() : a2;
    }

    public wo constructBeanSerializerBuilder(so soVar) {
        return new wo(soVar);
    }

    public BeanPropertyWriter constructFilteredBeanWriter(BeanPropertyWriter beanPropertyWriter, Class<?>[] clsArr) {
        return FilteredBeanPropertyWriter.a(beanPropertyWriter, clsArr);
    }

    public kl2 constructObjectIdHandler(k kVar, so soVar, List<BeanPropertyWriter> list) throws JsonMappingException {
        jl2 x = soVar.x();
        if (x == null) {
            return null;
        }
        Class<? extends ObjectIdGenerator<?>> c = x.c();
        if (c == ObjectIdGenerators$PropertyGenerator.class) {
            String simpleName = x.d().getSimpleName();
            int size = list.size();
            for (int i = 0; i != size; i++) {
                BeanPropertyWriter beanPropertyWriter = list.get(i);
                if (simpleName.equals(beanPropertyWriter.getName())) {
                    if (i > 0) {
                        list.remove(i);
                        list.add(0, beanPropertyWriter);
                    }
                    return kl2.a(beanPropertyWriter.getType(), null, new PropertyBasedObjectIdGenerator(x, beanPropertyWriter), x.b());
                }
            }
            throw new IllegalArgumentException("Invalid Object Id definition for " + soVar.r().getName() + ": can not find property with name '" + simpleName + "'");
        }
        return kl2.a(kVar.getTypeFactory().findTypeParameters(kVar.constructType(c), ObjectIdGenerator.class)[0], x.d(), kVar.objectIdGeneratorInstance(soVar.t(), x), x.b());
    }

    public c constructPropertyBuilder(SerializationConfig serializationConfig, so soVar) {
        return new c(serializationConfig, soVar);
    }

    @Override // com.fasterxml.jackson.databind.ser.BasicSerializerFactory, com.fasterxml.jackson.databind.ser.g
    public com.fasterxml.jackson.databind.f<Object> createSerializer(k kVar, JavaType javaType) throws JsonMappingException {
        JavaType refineSerializationType;
        SerializationConfig config = kVar.getConfig();
        so introspect = config.introspect(javaType);
        com.fasterxml.jackson.databind.f<?> findSerializerFromAnnotation = findSerializerFromAnnotation(kVar, introspect.t());
        if (findSerializerFromAnnotation != null) {
            return findSerializerFromAnnotation;
        }
        AnnotationIntrospector annotationIntrospector = config.getAnnotationIntrospector();
        boolean z = false;
        if (annotationIntrospector == null) {
            refineSerializationType = javaType;
        } else {
            try {
                refineSerializationType = annotationIntrospector.refineSerializationType(config, introspect.t(), javaType);
            } catch (JsonMappingException e) {
                return (com.fasterxml.jackson.databind.f) kVar.reportBadTypeDefinition(introspect, e.getMessage(), new Object[0]);
            }
        }
        if (refineSerializationType != javaType) {
            if (!refineSerializationType.hasRawClass(javaType.getRawClass())) {
                introspect = config.introspect(refineSerializationType);
            }
            z = true;
        }
        o80<Object, Object> p = introspect.p();
        if (p == null) {
            return _createSerializer2(kVar, refineSerializationType, introspect, z);
        }
        JavaType c = p.c(kVar.getTypeFactory());
        if (!c.hasRawClass(refineSerializationType.getRawClass())) {
            introspect = config.introspect(c);
            findSerializerFromAnnotation = findSerializerFromAnnotation(kVar, introspect.t());
        }
        if (findSerializerFromAnnotation == null && !c.isJavaLangObject()) {
            findSerializerFromAnnotation = _createSerializer2(kVar, c, introspect, true);
        }
        return new StdDelegatingSerializer(p, c, findSerializerFromAnnotation);
    }

    @Override // com.fasterxml.jackson.databind.ser.BasicSerializerFactory
    public Iterable<am3> customSerializers() {
        return this._factoryConfig.serializers();
    }

    public List<BeanPropertyWriter> filterBeanProperties(SerializationConfig serializationConfig, so soVar, List<BeanPropertyWriter> list) {
        JsonIgnoreProperties.Value defaultPropertyIgnorals = serializationConfig.getDefaultPropertyIgnorals(soVar.r(), soVar.t());
        if (defaultPropertyIgnorals != null) {
            Set<String> findIgnoredForSerialization = defaultPropertyIgnorals.findIgnoredForSerialization();
            if (!findIgnoredForSerialization.isEmpty()) {
                Iterator<BeanPropertyWriter> it = list.iterator();
                while (it.hasNext()) {
                    if (findIgnoredForSerialization.contains(it.next().getName())) {
                        it.remove();
                    }
                }
            }
        }
        return list;
    }

    public List<BeanPropertyWriter> findBeanProperties(k kVar, so soVar, wo woVar) throws JsonMappingException {
        List<vo> n = soVar.n();
        SerializationConfig config = kVar.getConfig();
        removeIgnorableTypes(config, soVar, n);
        if (config.isEnabled(MapperFeature.REQUIRE_SETTERS_FOR_GETTERS)) {
            removeSetterlessGetters(config, soVar, n);
        }
        if (n.isEmpty()) {
            return null;
        }
        boolean usesStaticTyping = usesStaticTyping(config, soVar, null);
        c constructPropertyBuilder = constructPropertyBuilder(config, soVar);
        ArrayList arrayList = new ArrayList(n.size());
        for (vo voVar : n) {
            AnnotatedMember j = voVar.j();
            if (!voVar.I()) {
                AnnotationIntrospector.ReferenceProperty g = voVar.g();
                if (g == null || !g.c()) {
                    if (j instanceof AnnotatedMethod) {
                        arrayList.add(_constructWriter(kVar, voVar, constructPropertyBuilder, usesStaticTyping, (AnnotatedMethod) j));
                    } else {
                        arrayList.add(_constructWriter(kVar, voVar, constructPropertyBuilder, usesStaticTyping, (AnnotatedField) j));
                    }
                }
            } else if (j != null) {
                woVar.o(j);
            }
        }
        return arrayList;
    }

    public com.fasterxml.jackson.databind.f<Object> findBeanSerializer(k kVar, JavaType javaType, so soVar) throws JsonMappingException {
        if (isPotentialBeanType(javaType.getRawClass()) || javaType.isEnumType()) {
            return constructBeanSerializer(kVar, soVar);
        }
        return null;
    }

    public com.fasterxml.jackson.databind.jsontype.c findPropertyContentTypeSerializer(JavaType javaType, SerializationConfig serializationConfig, AnnotatedMember annotatedMember) throws JsonMappingException {
        JavaType contentType = javaType.getContentType();
        vd4<?> findPropertyContentTypeResolver = serializationConfig.getAnnotationIntrospector().findPropertyContentTypeResolver(serializationConfig, annotatedMember, javaType);
        if (findPropertyContentTypeResolver == null) {
            return createTypeSerializer(serializationConfig, contentType);
        }
        return findPropertyContentTypeResolver.buildTypeSerializer(serializationConfig, contentType, serializationConfig.getSubtypeResolver().collectAndResolveSubtypesByClass(serializationConfig, annotatedMember, contentType));
    }

    public com.fasterxml.jackson.databind.jsontype.c findPropertyTypeSerializer(JavaType javaType, SerializationConfig serializationConfig, AnnotatedMember annotatedMember) throws JsonMappingException {
        vd4<?> findPropertyTypeResolver = serializationConfig.getAnnotationIntrospector().findPropertyTypeResolver(serializationConfig, annotatedMember, javaType);
        if (findPropertyTypeResolver == null) {
            return createTypeSerializer(serializationConfig, javaType);
        }
        return findPropertyTypeResolver.buildTypeSerializer(serializationConfig, javaType, serializationConfig.getSubtypeResolver().collectAndResolveSubtypesByClass(serializationConfig, annotatedMember, javaType));
    }

    public com.fasterxml.jackson.databind.f<?> findReferenceSerializer(k kVar, ReferenceType referenceType, so soVar, boolean z) throws JsonMappingException {
        JavaType contentType = referenceType.getContentType();
        com.fasterxml.jackson.databind.jsontype.c cVar = (com.fasterxml.jackson.databind.jsontype.c) contentType.getTypeHandler();
        SerializationConfig config = kVar.getConfig();
        if (cVar == null) {
            cVar = createTypeSerializer(config, contentType);
        }
        com.fasterxml.jackson.databind.f<Object> fVar = (com.fasterxml.jackson.databind.f) contentType.getValueHandler();
        for (am3 am3Var : customSerializers()) {
            com.fasterxml.jackson.databind.f<?> findReferenceSerializer = am3Var.findReferenceSerializer(config, referenceType, soVar, cVar, fVar);
            if (findReferenceSerializer != null) {
                return findReferenceSerializer;
            }
        }
        if (referenceType.isTypeOrSubTypeOf(AtomicReference.class)) {
            return new AtomicReferenceSerializer(referenceType, z, cVar, fVar);
        }
        return null;
    }

    public boolean isPotentialBeanType(Class<?> cls) {
        return com.fasterxml.jackson.databind.util.c.d(cls) == null && !com.fasterxml.jackson.databind.util.c.N(cls);
    }

    public void processViews(SerializationConfig serializationConfig, wo woVar) {
        List<BeanPropertyWriter> g = woVar.g();
        boolean isEnabled = serializationConfig.isEnabled(MapperFeature.DEFAULT_VIEW_INCLUSION);
        int size = g.size();
        BeanPropertyWriter[] beanPropertyWriterArr = new BeanPropertyWriter[size];
        int i = 0;
        for (int i2 = 0; i2 < size; i2++) {
            BeanPropertyWriter beanPropertyWriter = g.get(i2);
            Class<?>[] views = beanPropertyWriter.getViews();
            if (views != null) {
                i++;
                beanPropertyWriterArr[i2] = constructFilteredBeanWriter(beanPropertyWriter, views);
            } else if (isEnabled) {
                beanPropertyWriterArr[i2] = beanPropertyWriter;
            }
        }
        if (isEnabled && i == 0) {
            return;
        }
        woVar.l(beanPropertyWriterArr);
    }

    public void removeIgnorableTypes(SerializationConfig serializationConfig, so soVar, List<vo> list) {
        AnnotationIntrospector annotationIntrospector = serializationConfig.getAnnotationIntrospector();
        HashMap hashMap = new HashMap();
        Iterator<vo> it = list.iterator();
        while (it.hasNext()) {
            AnnotatedMember j = it.next().j();
            if (j == null) {
                it.remove();
            } else {
                Class<?> rawType = j.getRawType();
                Boolean bool = (Boolean) hashMap.get(rawType);
                if (bool == null) {
                    w40 findConfigOverride = serializationConfig.findConfigOverride(rawType);
                    if (findConfigOverride != null) {
                        bool = findConfigOverride.getIsIgnoredType();
                    }
                    if (bool == null && (bool = annotationIntrospector.isIgnorableType(serializationConfig.introspectClassAnnotations(rawType).t())) == null) {
                        bool = Boolean.FALSE;
                    }
                    hashMap.put(rawType, bool);
                }
                if (bool.booleanValue()) {
                    it.remove();
                }
            }
        }
    }

    public List<BeanPropertyWriter> removeOverlappingTypeIds(k kVar, so soVar, wo woVar, List<BeanPropertyWriter> list) {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            BeanPropertyWriter beanPropertyWriter = list.get(i);
            com.fasterxml.jackson.databind.jsontype.c typeSerializer = beanPropertyWriter.getTypeSerializer();
            if (typeSerializer != null && typeSerializer.c() == JsonTypeInfo.As.EXTERNAL_PROPERTY) {
                PropertyName construct = PropertyName.construct(typeSerializer.b());
                Iterator<BeanPropertyWriter> it = list.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    BeanPropertyWriter next = it.next();
                    if (next != beanPropertyWriter && next.wouldConflictWithName(construct)) {
                        beanPropertyWriter.assignTypeSerializer(null);
                        break;
                    }
                }
            }
        }
        return list;
    }

    public void removeSetterlessGetters(SerializationConfig serializationConfig, so soVar, List<vo> list) {
        Iterator<vo> it = list.iterator();
        while (it.hasNext()) {
            vo next = it.next();
            if (!next.a() && !next.G()) {
                it.remove();
            }
        }
    }

    @Override // com.fasterxml.jackson.databind.ser.BasicSerializerFactory
    public g withConfig(SerializerFactoryConfig serializerFactoryConfig) {
        if (this._factoryConfig == serializerFactoryConfig) {
            return this;
        }
        if (getClass() == BeanSerializerFactory.class) {
            return new BeanSerializerFactory(serializerFactoryConfig);
        }
        throw new IllegalStateException("Subtype of BeanSerializerFactory (" + getClass().getName() + ") has not properly overridden method 'withAdditionalSerializers': can not instantiate subtype with additional serializer definitions");
    }
}
