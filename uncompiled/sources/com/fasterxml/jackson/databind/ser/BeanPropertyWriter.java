package com.fasterxml.jackson.databind.ser;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.io.SerializedString;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.PropertyMetadata;
import com.fasterxml.jackson.databind.PropertyName;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.introspect.AnnotatedField;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.fasterxml.jackson.databind.introspect.AnnotatedMethod;
import com.fasterxml.jackson.databind.k;
import com.fasterxml.jackson.databind.node.m;
import com.fasterxml.jackson.databind.ser.impl.UnwrappingBeanPropertyWriter;
import com.fasterxml.jackson.databind.ser.impl.a;
import com.fasterxml.jackson.databind.ser.std.BeanSerializerBase;
import com.fasterxml.jackson.databind.util.NameTransformer;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.HashMap;

@pt1
/* loaded from: classes.dex */
public class BeanPropertyWriter extends PropertyWriter {
    public static final Object MARKER_FOR_EMPTY = JsonInclude.Include.NON_EMPTY;
    private static final long serialVersionUID = 1;
    public transient Method _accessorMethod;
    public final JavaType _cfgSerializationType;
    public final transient xe _contextAnnotations;
    public final JavaType _declaredType;
    public transient com.fasterxml.jackson.databind.ser.impl.a _dynamicSerializers;
    public transient Field _field;
    public final Class<?>[] _includeInViews;
    public transient HashMap<Object, Object> _internalSettings;
    public final AnnotatedMember _member;
    public final SerializedString _name;
    public JavaType _nonTrivialBaseType;
    public com.fasterxml.jackson.databind.f<Object> _nullSerializer;
    public com.fasterxml.jackson.databind.f<Object> _serializer;
    public final boolean _suppressNulls;
    public final Object _suppressableValue;
    public com.fasterxml.jackson.databind.jsontype.c _typeSerializer;
    public final PropertyName _wrapperName;

    public BeanPropertyWriter(vo voVar, AnnotatedMember annotatedMember, xe xeVar, JavaType javaType, com.fasterxml.jackson.databind.f<?> fVar, com.fasterxml.jackson.databind.jsontype.c cVar, JavaType javaType2, boolean z, Object obj) {
        super(voVar);
        this._member = annotatedMember;
        this._contextAnnotations = xeVar;
        this._name = new SerializedString(voVar.t());
        this._wrapperName = voVar.y();
        this._includeInViews = voVar.h();
        this._declaredType = javaType;
        this._serializer = fVar;
        this._dynamicSerializers = fVar == null ? com.fasterxml.jackson.databind.ser.impl.a.a() : null;
        this._typeSerializer = cVar;
        this._cfgSerializationType = javaType2;
        if (annotatedMember instanceof AnnotatedField) {
            this._accessorMethod = null;
            this._field = (Field) annotatedMember.getMember();
        } else if (annotatedMember instanceof AnnotatedMethod) {
            this._accessorMethod = (Method) annotatedMember.getMember();
            this._field = null;
        } else {
            this._accessorMethod = null;
            this._field = null;
        }
        this._suppressNulls = z;
        this._suppressableValue = obj;
        this._nullSerializer = null;
    }

    public void _depositSchemaProperty(m mVar, com.fasterxml.jackson.databind.d dVar) {
        mVar.Z(getName(), dVar);
    }

    public com.fasterxml.jackson.databind.f<Object> _findAndAddDynamic(com.fasterxml.jackson.databind.ser.impl.a aVar, Class<?> cls, k kVar) throws JsonMappingException {
        a.d e;
        JavaType javaType = this._nonTrivialBaseType;
        if (javaType != null) {
            e = aVar.d(kVar.constructSpecializedType(javaType, cls), kVar, this);
        } else {
            e = aVar.e(cls, kVar, this);
        }
        com.fasterxml.jackson.databind.ser.impl.a aVar2 = e.b;
        if (aVar != aVar2) {
            this._dynamicSerializers = aVar2;
        }
        return e.a;
    }

    public boolean _handleSelfReference(Object obj, JsonGenerator jsonGenerator, k kVar, com.fasterxml.jackson.databind.f<?> fVar) throws JsonMappingException {
        if (kVar.isEnabled(SerializationFeature.FAIL_ON_SELF_REFERENCES) && !fVar.usesObjectId() && (fVar instanceof BeanSerializerBase)) {
            kVar.reportMappingProblem("Direct self-reference leading to cycle", new Object[0]);
        }
        return false;
    }

    public BeanPropertyWriter _new(PropertyName propertyName) {
        return new BeanPropertyWriter(this, propertyName);
    }

    public void assignNullSerializer(com.fasterxml.jackson.databind.f<Object> fVar) {
        com.fasterxml.jackson.databind.f<Object> fVar2 = this._nullSerializer;
        if (fVar2 != null && fVar2 != fVar) {
            throw new IllegalStateException("Can not override null serializer");
        }
        this._nullSerializer = fVar;
    }

    public void assignSerializer(com.fasterxml.jackson.databind.f<Object> fVar) {
        com.fasterxml.jackson.databind.f<Object> fVar2 = this._serializer;
        if (fVar2 != null && fVar2 != fVar) {
            throw new IllegalStateException("Can not override serializer");
        }
        this._serializer = fVar;
    }

    public void assignTypeSerializer(com.fasterxml.jackson.databind.jsontype.c cVar) {
        this._typeSerializer = cVar;
    }

    @Override // com.fasterxml.jackson.databind.ser.PropertyWriter, com.fasterxml.jackson.databind.introspect.ConcreteBeanPropertyBase, com.fasterxml.jackson.databind.a
    public void depositSchemaProperty(com.fasterxml.jackson.databind.jsonFormatVisitors.d dVar, k kVar) throws JsonMappingException {
        if (dVar != null) {
            if (isRequired()) {
                dVar.q(this);
            } else {
                dVar.g(this);
            }
        }
    }

    public void fixAccess(SerializationConfig serializationConfig) {
        this._member.fixAccess(serializationConfig.isEnabled(MapperFeature.OVERRIDE_PUBLIC_ACCESS_MODIFIERS));
    }

    public final Object get(Object obj) throws Exception {
        Method method = this._accessorMethod;
        return method == null ? this._field.get(obj) : method.invoke(obj, new Object[0]);
    }

    @Override // com.fasterxml.jackson.databind.ser.PropertyWriter, com.fasterxml.jackson.databind.introspect.ConcreteBeanPropertyBase, com.fasterxml.jackson.databind.a
    public <A extends Annotation> A getAnnotation(Class<A> cls) {
        AnnotatedMember annotatedMember = this._member;
        if (annotatedMember == null) {
            return null;
        }
        return (A) annotatedMember.getAnnotation(cls);
    }

    @Override // com.fasterxml.jackson.databind.ser.PropertyWriter, com.fasterxml.jackson.databind.introspect.ConcreteBeanPropertyBase, com.fasterxml.jackson.databind.a
    public <A extends Annotation> A getContextAnnotation(Class<A> cls) {
        xe xeVar = this._contextAnnotations;
        if (xeVar == null) {
            return null;
        }
        return (A) xeVar.a(cls);
    }

    @Override // com.fasterxml.jackson.databind.ser.PropertyWriter, com.fasterxml.jackson.databind.introspect.ConcreteBeanPropertyBase
    public PropertyName getFullName() {
        return new PropertyName(this._name.getValue());
    }

    @Deprecated
    public Type getGenericPropertyType() {
        Method method = this._accessorMethod;
        if (method != null) {
            return method.getGenericReturnType();
        }
        Field field = this._field;
        if (field != null) {
            return field.getGenericType();
        }
        return null;
    }

    public Object getInternalSetting(Object obj) {
        HashMap<Object, Object> hashMap = this._internalSettings;
        if (hashMap == null) {
            return null;
        }
        return hashMap.get(obj);
    }

    @Override // com.fasterxml.jackson.databind.ser.PropertyWriter, com.fasterxml.jackson.databind.introspect.ConcreteBeanPropertyBase, com.fasterxml.jackson.databind.a
    public AnnotatedMember getMember() {
        return this._member;
    }

    @Override // com.fasterxml.jackson.databind.ser.PropertyWriter, com.fasterxml.jackson.databind.introspect.ConcreteBeanPropertyBase, com.fasterxml.jackson.databind.a
    public String getName() {
        return this._name.getValue();
    }

    @Deprecated
    public Class<?> getPropertyType() {
        Method method = this._accessorMethod;
        if (method != null) {
            return method.getReturnType();
        }
        Field field = this._field;
        if (field != null) {
            return field.getType();
        }
        return null;
    }

    public Class<?> getRawSerializationType() {
        JavaType javaType = this._cfgSerializationType;
        if (javaType == null) {
            return null;
        }
        return javaType.getRawClass();
    }

    public JavaType getSerializationType() {
        return this._cfgSerializationType;
    }

    public yl3 getSerializedName() {
        return this._name;
    }

    public com.fasterxml.jackson.databind.f<Object> getSerializer() {
        return this._serializer;
    }

    @Override // com.fasterxml.jackson.databind.ser.PropertyWriter, com.fasterxml.jackson.databind.introspect.ConcreteBeanPropertyBase, com.fasterxml.jackson.databind.a
    public JavaType getType() {
        return this._declaredType;
    }

    public com.fasterxml.jackson.databind.jsontype.c getTypeSerializer() {
        return this._typeSerializer;
    }

    public Class<?>[] getViews() {
        return this._includeInViews;
    }

    @Override // com.fasterxml.jackson.databind.ser.PropertyWriter, com.fasterxml.jackson.databind.introspect.ConcreteBeanPropertyBase, com.fasterxml.jackson.databind.a
    public PropertyName getWrapperName() {
        return this._wrapperName;
    }

    public boolean hasNullSerializer() {
        return this._nullSerializer != null;
    }

    public boolean hasSerializer() {
        return this._serializer != null;
    }

    public boolean isUnwrapping() {
        return false;
    }

    public Object readResolve() {
        AnnotatedMember annotatedMember = this._member;
        if (annotatedMember instanceof AnnotatedField) {
            this._accessorMethod = null;
            this._field = (Field) annotatedMember.getMember();
        } else if (annotatedMember instanceof AnnotatedMethod) {
            this._accessorMethod = (Method) annotatedMember.getMember();
            this._field = null;
        }
        if (this._serializer == null) {
            this._dynamicSerializers = com.fasterxml.jackson.databind.ser.impl.a.a();
        }
        return this;
    }

    public Object removeInternalSetting(Object obj) {
        HashMap<Object, Object> hashMap = this._internalSettings;
        if (hashMap != null) {
            Object remove = hashMap.remove(obj);
            if (this._internalSettings.size() == 0) {
                this._internalSettings = null;
            }
            return remove;
        }
        return null;
    }

    public BeanPropertyWriter rename(NameTransformer nameTransformer) {
        String transform = nameTransformer.transform(this._name.getValue());
        return transform.equals(this._name.toString()) ? this : _new(PropertyName.construct(transform));
    }

    @Override // com.fasterxml.jackson.databind.ser.PropertyWriter
    public void serializeAsElement(Object obj, JsonGenerator jsonGenerator, k kVar) throws Exception {
        Method method = this._accessorMethod;
        Object invoke = method == null ? this._field.get(obj) : method.invoke(obj, new Object[0]);
        if (invoke == null) {
            com.fasterxml.jackson.databind.f<Object> fVar = this._nullSerializer;
            if (fVar != null) {
                fVar.serialize(null, jsonGenerator, kVar);
                return;
            } else {
                jsonGenerator.m0();
                return;
            }
        }
        com.fasterxml.jackson.databind.f<?> fVar2 = this._serializer;
        if (fVar2 == null) {
            Class<?> cls = invoke.getClass();
            com.fasterxml.jackson.databind.ser.impl.a aVar = this._dynamicSerializers;
            com.fasterxml.jackson.databind.f<?> i = aVar.i(cls);
            fVar2 = i == null ? _findAndAddDynamic(aVar, cls, kVar) : i;
        }
        Object obj2 = this._suppressableValue;
        if (obj2 != null) {
            if (MARKER_FOR_EMPTY == obj2) {
                if (fVar2.isEmpty(kVar, invoke)) {
                    serializeAsPlaceholder(obj, jsonGenerator, kVar);
                    return;
                }
            } else if (obj2.equals(invoke)) {
                serializeAsPlaceholder(obj, jsonGenerator, kVar);
                return;
            }
        }
        if (invoke == obj && _handleSelfReference(obj, jsonGenerator, kVar, fVar2)) {
            return;
        }
        com.fasterxml.jackson.databind.jsontype.c cVar = this._typeSerializer;
        if (cVar == null) {
            fVar2.serialize(invoke, jsonGenerator, kVar);
        } else {
            fVar2.serializeWithType(invoke, jsonGenerator, kVar, cVar);
        }
    }

    @Override // com.fasterxml.jackson.databind.ser.PropertyWriter
    public void serializeAsField(Object obj, JsonGenerator jsonGenerator, k kVar) throws Exception {
        Method method = this._accessorMethod;
        Object invoke = method == null ? this._field.get(obj) : method.invoke(obj, new Object[0]);
        if (invoke == null) {
            if (this._nullSerializer != null) {
                jsonGenerator.g0(this._name);
                this._nullSerializer.serialize(null, jsonGenerator, kVar);
                return;
            }
            return;
        }
        com.fasterxml.jackson.databind.f<?> fVar = this._serializer;
        if (fVar == null) {
            Class<?> cls = invoke.getClass();
            com.fasterxml.jackson.databind.ser.impl.a aVar = this._dynamicSerializers;
            com.fasterxml.jackson.databind.f<?> i = aVar.i(cls);
            fVar = i == null ? _findAndAddDynamic(aVar, cls, kVar) : i;
        }
        Object obj2 = this._suppressableValue;
        if (obj2 != null) {
            if (MARKER_FOR_EMPTY == obj2) {
                if (fVar.isEmpty(kVar, invoke)) {
                    return;
                }
            } else if (obj2.equals(invoke)) {
                return;
            }
        }
        if (invoke == obj && _handleSelfReference(obj, jsonGenerator, kVar, fVar)) {
            return;
        }
        jsonGenerator.g0(this._name);
        com.fasterxml.jackson.databind.jsontype.c cVar = this._typeSerializer;
        if (cVar == null) {
            fVar.serialize(invoke, jsonGenerator, kVar);
        } else {
            fVar.serializeWithType(invoke, jsonGenerator, kVar, cVar);
        }
    }

    @Override // com.fasterxml.jackson.databind.ser.PropertyWriter
    public void serializeAsOmittedField(Object obj, JsonGenerator jsonGenerator, k kVar) throws Exception {
        if (jsonGenerator.e()) {
            return;
        }
        jsonGenerator.S0(this._name.getValue());
    }

    @Override // com.fasterxml.jackson.databind.ser.PropertyWriter
    public void serializeAsPlaceholder(Object obj, JsonGenerator jsonGenerator, k kVar) throws Exception {
        com.fasterxml.jackson.databind.f<Object> fVar = this._nullSerializer;
        if (fVar != null) {
            fVar.serialize(null, jsonGenerator, kVar);
        } else {
            jsonGenerator.m0();
        }
    }

    public Object setInternalSetting(Object obj, Object obj2) {
        if (this._internalSettings == null) {
            this._internalSettings = new HashMap<>();
        }
        return this._internalSettings.put(obj, obj2);
    }

    public void setNonTrivialBaseType(JavaType javaType) {
        this._nonTrivialBaseType = javaType;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(40);
        sb.append("property '");
        sb.append(getName());
        sb.append("' (");
        if (this._accessorMethod != null) {
            sb.append("via method ");
            sb.append(this._accessorMethod.getDeclaringClass().getName());
            sb.append("#");
            sb.append(this._accessorMethod.getName());
        } else if (this._field != null) {
            sb.append("field \"");
            sb.append(this._field.getDeclaringClass().getName());
            sb.append("#");
            sb.append(this._field.getName());
        } else {
            sb.append("virtual");
        }
        if (this._serializer == null) {
            sb.append(", no static serializer");
        } else {
            sb.append(", static serializer of type " + this._serializer.getClass().getName());
        }
        sb.append(')');
        return sb.toString();
    }

    public BeanPropertyWriter unwrappingWriter(NameTransformer nameTransformer) {
        return new UnwrappingBeanPropertyWriter(this, nameTransformer);
    }

    public boolean willSuppressNulls() {
        return this._suppressNulls;
    }

    public boolean wouldConflictWithName(PropertyName propertyName) {
        PropertyName propertyName2 = this._wrapperName;
        if (propertyName2 != null) {
            return propertyName2.equals(propertyName);
        }
        return propertyName.hasSimpleName(this._name.getValue()) && !propertyName.hasNamespace();
    }

    @Override // com.fasterxml.jackson.databind.ser.PropertyWriter
    @Deprecated
    public void depositSchemaProperty(m mVar, k kVar) throws JsonMappingException {
        com.fasterxml.jackson.databind.d a;
        JavaType serializationType = getSerializationType();
        Type type = serializationType == null ? getType() : serializationType.getRawClass();
        com.fasterxml.jackson.databind.f<Object> serializer = getSerializer();
        if (serializer == null) {
            serializer = kVar.findValueSerializer(getType(), this);
        }
        boolean z = !isRequired();
        if (serializer instanceof ld3) {
            a = ((ld3) serializer).getSchema(kVar, type, z);
        } else {
            a = aw1.a();
        }
        _depositSchemaProperty(mVar, a);
    }

    public BeanPropertyWriter() {
        super(PropertyMetadata.STD_REQUIRED_OR_OPTIONAL);
        this._member = null;
        this._contextAnnotations = null;
        this._name = null;
        this._wrapperName = null;
        this._includeInViews = null;
        this._declaredType = null;
        this._serializer = null;
        this._dynamicSerializers = null;
        this._typeSerializer = null;
        this._cfgSerializationType = null;
        this._accessorMethod = null;
        this._field = null;
        this._suppressNulls = false;
        this._suppressableValue = null;
        this._nullSerializer = null;
    }

    public BeanPropertyWriter(BeanPropertyWriter beanPropertyWriter) {
        this(beanPropertyWriter, beanPropertyWriter._name);
    }

    public BeanPropertyWriter(BeanPropertyWriter beanPropertyWriter, PropertyName propertyName) {
        super(beanPropertyWriter);
        this._name = new SerializedString(propertyName.getSimpleName());
        this._wrapperName = beanPropertyWriter._wrapperName;
        this._contextAnnotations = beanPropertyWriter._contextAnnotations;
        this._declaredType = beanPropertyWriter._declaredType;
        this._member = beanPropertyWriter._member;
        this._accessorMethod = beanPropertyWriter._accessorMethod;
        this._field = beanPropertyWriter._field;
        this._serializer = beanPropertyWriter._serializer;
        this._nullSerializer = beanPropertyWriter._nullSerializer;
        if (beanPropertyWriter._internalSettings != null) {
            this._internalSettings = new HashMap<>(beanPropertyWriter._internalSettings);
        }
        this._cfgSerializationType = beanPropertyWriter._cfgSerializationType;
        this._dynamicSerializers = beanPropertyWriter._dynamicSerializers;
        this._suppressNulls = beanPropertyWriter._suppressNulls;
        this._suppressableValue = beanPropertyWriter._suppressableValue;
        this._includeInViews = beanPropertyWriter._includeInViews;
        this._typeSerializer = beanPropertyWriter._typeSerializer;
        this._nonTrivialBaseType = beanPropertyWriter._nonTrivialBaseType;
    }

    public BeanPropertyWriter(BeanPropertyWriter beanPropertyWriter, SerializedString serializedString) {
        super(beanPropertyWriter);
        this._name = serializedString;
        this._wrapperName = beanPropertyWriter._wrapperName;
        this._member = beanPropertyWriter._member;
        this._contextAnnotations = beanPropertyWriter._contextAnnotations;
        this._declaredType = beanPropertyWriter._declaredType;
        this._accessorMethod = beanPropertyWriter._accessorMethod;
        this._field = beanPropertyWriter._field;
        this._serializer = beanPropertyWriter._serializer;
        this._nullSerializer = beanPropertyWriter._nullSerializer;
        if (beanPropertyWriter._internalSettings != null) {
            this._internalSettings = new HashMap<>(beanPropertyWriter._internalSettings);
        }
        this._cfgSerializationType = beanPropertyWriter._cfgSerializationType;
        this._dynamicSerializers = beanPropertyWriter._dynamicSerializers;
        this._suppressNulls = beanPropertyWriter._suppressNulls;
        this._suppressableValue = beanPropertyWriter._suppressableValue;
        this._includeInViews = beanPropertyWriter._includeInViews;
        this._typeSerializer = beanPropertyWriter._typeSerializer;
        this._nonTrivialBaseType = beanPropertyWriter._nonTrivialBaseType;
    }
}
