package com.fasterxml.jackson.databind.ser;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.PropertyName;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.databind.k;

/* loaded from: classes.dex */
public abstract class VirtualBeanPropertyWriter extends BeanPropertyWriter {
    private static final long serialVersionUID = 1;

    public VirtualBeanPropertyWriter(vo voVar, xe xeVar, JavaType javaType) {
        this(voVar, xeVar, javaType, null, null, null, voVar.e());
    }

    public static boolean _suppressNulls(JsonInclude.Value value) {
        JsonInclude.Include valueInclusion;
        return (value == null || (valueInclusion = value.getValueInclusion()) == JsonInclude.Include.ALWAYS || valueInclusion == JsonInclude.Include.USE_DEFAULTS) ? false : true;
    }

    public static Object _suppressableValue(JsonInclude.Value value) {
        if (value == null) {
            return Boolean.FALSE;
        }
        JsonInclude.Include valueInclusion = value.getValueInclusion();
        if (valueInclusion == JsonInclude.Include.ALWAYS || valueInclusion == JsonInclude.Include.NON_NULL || valueInclusion == JsonInclude.Include.USE_DEFAULTS) {
            return null;
        }
        return BeanPropertyWriter.MARKER_FOR_EMPTY;
    }

    @Override // com.fasterxml.jackson.databind.introspect.ConcreteBeanPropertyBase
    public boolean isVirtual() {
        return true;
    }

    @Override // com.fasterxml.jackson.databind.ser.BeanPropertyWriter, com.fasterxml.jackson.databind.ser.PropertyWriter
    public void serializeAsElement(Object obj, JsonGenerator jsonGenerator, k kVar) throws Exception {
        Object value = value(obj, jsonGenerator, kVar);
        if (value == null) {
            com.fasterxml.jackson.databind.f<Object> fVar = this._nullSerializer;
            if (fVar != null) {
                fVar.serialize(null, jsonGenerator, kVar);
                return;
            } else {
                jsonGenerator.m0();
                return;
            }
        }
        com.fasterxml.jackson.databind.f<?> fVar2 = this._serializer;
        if (fVar2 == null) {
            Class<?> cls = value.getClass();
            com.fasterxml.jackson.databind.ser.impl.a aVar = this._dynamicSerializers;
            com.fasterxml.jackson.databind.f<?> i = aVar.i(cls);
            fVar2 = i == null ? _findAndAddDynamic(aVar, cls, kVar) : i;
        }
        Object obj2 = this._suppressableValue;
        if (obj2 != null) {
            if (BeanPropertyWriter.MARKER_FOR_EMPTY == obj2) {
                if (fVar2.isEmpty(kVar, value)) {
                    serializeAsPlaceholder(obj, jsonGenerator, kVar);
                    return;
                }
            } else if (obj2.equals(value)) {
                serializeAsPlaceholder(obj, jsonGenerator, kVar);
                return;
            }
        }
        if (value == obj && _handleSelfReference(obj, jsonGenerator, kVar, fVar2)) {
            return;
        }
        com.fasterxml.jackson.databind.jsontype.c cVar = this._typeSerializer;
        if (cVar == null) {
            fVar2.serialize(value, jsonGenerator, kVar);
        } else {
            fVar2.serializeWithType(value, jsonGenerator, kVar, cVar);
        }
    }

    @Override // com.fasterxml.jackson.databind.ser.BeanPropertyWriter, com.fasterxml.jackson.databind.ser.PropertyWriter
    public void serializeAsField(Object obj, JsonGenerator jsonGenerator, k kVar) throws Exception {
        Object value = value(obj, jsonGenerator, kVar);
        if (value == null) {
            if (this._nullSerializer != null) {
                jsonGenerator.g0(this._name);
                this._nullSerializer.serialize(null, jsonGenerator, kVar);
                return;
            }
            return;
        }
        com.fasterxml.jackson.databind.f<?> fVar = this._serializer;
        if (fVar == null) {
            Class<?> cls = value.getClass();
            com.fasterxml.jackson.databind.ser.impl.a aVar = this._dynamicSerializers;
            com.fasterxml.jackson.databind.f<?> i = aVar.i(cls);
            fVar = i == null ? _findAndAddDynamic(aVar, cls, kVar) : i;
        }
        Object obj2 = this._suppressableValue;
        if (obj2 != null) {
            if (BeanPropertyWriter.MARKER_FOR_EMPTY == obj2) {
                if (fVar.isEmpty(kVar, value)) {
                    return;
                }
            } else if (obj2.equals(value)) {
                return;
            }
        }
        if (value == obj && _handleSelfReference(obj, jsonGenerator, kVar, fVar)) {
            return;
        }
        jsonGenerator.g0(this._name);
        com.fasterxml.jackson.databind.jsontype.c cVar = this._typeSerializer;
        if (cVar == null) {
            fVar.serialize(value, jsonGenerator, kVar);
        } else {
            fVar.serializeWithType(value, jsonGenerator, kVar, cVar);
        }
    }

    public abstract Object value(Object obj, JsonGenerator jsonGenerator, k kVar) throws Exception;

    public abstract VirtualBeanPropertyWriter withConfig(MapperConfig<?> mapperConfig, com.fasterxml.jackson.databind.introspect.a aVar, vo voVar, JavaType javaType);

    public VirtualBeanPropertyWriter() {
    }

    public VirtualBeanPropertyWriter(vo voVar, xe xeVar, JavaType javaType, com.fasterxml.jackson.databind.f<?> fVar, com.fasterxml.jackson.databind.jsontype.c cVar, JavaType javaType2, JsonInclude.Value value) {
        super(voVar, voVar.v(), xeVar, javaType, fVar, cVar, javaType2, _suppressNulls(value), _suppressableValue(value));
    }

    public VirtualBeanPropertyWriter(VirtualBeanPropertyWriter virtualBeanPropertyWriter) {
        super(virtualBeanPropertyWriter);
    }

    public VirtualBeanPropertyWriter(VirtualBeanPropertyWriter virtualBeanPropertyWriter, PropertyName propertyName) {
        super(virtualBeanPropertyWriter, propertyName);
    }
}
