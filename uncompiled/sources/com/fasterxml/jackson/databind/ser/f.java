package com.fasterxml.jackson.databind.ser;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.k;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicReference;

/* compiled from: SerializerCache.java */
/* loaded from: classes.dex */
public final class f {
    public final HashMap<rd4, com.fasterxml.jackson.databind.f<Object>> a = new HashMap<>(64);
    public final AtomicReference<a43> b = new AtomicReference<>();

    public final synchronized a43 a() {
        a43 a43Var;
        a43Var = this.b.get();
        if (a43Var == null) {
            a43Var = a43.b(this.a);
            this.b.set(a43Var);
        }
        return a43Var;
    }

    public void b(JavaType javaType, com.fasterxml.jackson.databind.f<Object> fVar, k kVar) throws JsonMappingException {
        synchronized (this) {
            if (this.a.put(new rd4(javaType, false), fVar) == null) {
                this.b.set(null);
            }
            if (fVar instanceof e) {
                ((e) fVar).resolve(kVar);
            }
        }
    }

    public void c(Class<?> cls, JavaType javaType, com.fasterxml.jackson.databind.f<Object> fVar, k kVar) throws JsonMappingException {
        synchronized (this) {
            com.fasterxml.jackson.databind.f<Object> put = this.a.put(new rd4(cls, false), fVar);
            com.fasterxml.jackson.databind.f<Object> put2 = this.a.put(new rd4(javaType, false), fVar);
            if (put == null || put2 == null) {
                this.b.set(null);
            }
            if (fVar instanceof e) {
                ((e) fVar).resolve(kVar);
            }
        }
    }

    public void d(JavaType javaType, com.fasterxml.jackson.databind.f<Object> fVar) {
        synchronized (this) {
            if (this.a.put(new rd4(javaType, true), fVar) == null) {
                this.b.set(null);
            }
        }
    }

    public void e(Class<?> cls, com.fasterxml.jackson.databind.f<Object> fVar) {
        synchronized (this) {
            if (this.a.put(new rd4(cls, true), fVar) == null) {
                this.b.set(null);
            }
        }
    }

    public synchronized void f() {
        this.a.clear();
    }

    public a43 g() {
        a43 a43Var = this.b.get();
        return a43Var != null ? a43Var : a();
    }

    public synchronized int h() {
        return this.a.size();
    }

    public com.fasterxml.jackson.databind.f<Object> i(JavaType javaType) {
        com.fasterxml.jackson.databind.f<Object> fVar;
        synchronized (this) {
            fVar = this.a.get(new rd4(javaType, true));
        }
        return fVar;
    }

    public com.fasterxml.jackson.databind.f<Object> j(Class<?> cls) {
        com.fasterxml.jackson.databind.f<Object> fVar;
        synchronized (this) {
            fVar = this.a.get(new rd4(cls, true));
        }
        return fVar;
    }

    public com.fasterxml.jackson.databind.f<Object> k(JavaType javaType) {
        com.fasterxml.jackson.databind.f<Object> fVar;
        synchronized (this) {
            fVar = this.a.get(new rd4(javaType, false));
        }
        return fVar;
    }

    public com.fasterxml.jackson.databind.f<Object> l(Class<?> cls) {
        com.fasterxml.jackson.databind.f<Object> fVar;
        synchronized (this) {
            fVar = this.a.get(new rd4(cls, false));
        }
        return fVar;
    }
}
