package com.fasterxml.jackson.databind.ser;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.fasterxml.jackson.databind.k;
import com.fasterxml.jackson.databind.ser.std.MapSerializer;
import java.util.Map;

/* compiled from: AnyGetterWriter.java */
/* loaded from: classes.dex */
public class a {
    public final com.fasterxml.jackson.databind.a a;
    public final AnnotatedMember b;
    public com.fasterxml.jackson.databind.f<Object> c;
    public MapSerializer d;

    public a(com.fasterxml.jackson.databind.a aVar, AnnotatedMember annotatedMember, com.fasterxml.jackson.databind.f<?> fVar) {
        this.b = annotatedMember;
        this.a = aVar;
        this.c = fVar;
        if (fVar instanceof MapSerializer) {
            this.d = (MapSerializer) fVar;
        }
    }

    public void a(SerializationConfig serializationConfig) {
        this.b.fixAccess(serializationConfig.isEnabled(MapperFeature.OVERRIDE_PUBLIC_ACCESS_MODIFIERS));
    }

    public void b(Object obj, JsonGenerator jsonGenerator, k kVar, d dVar) throws Exception {
        Object value = this.b.getValue(obj);
        if (value == null) {
            return;
        }
        if (!(value instanceof Map)) {
            kVar.reportMappingProblem("Value returned by 'any-getter' (%s()) not java.util.Map but %s", this.b.getName(), value.getClass().getName());
        }
        MapSerializer mapSerializer = this.d;
        if (mapSerializer != null) {
            mapSerializer.serializeFilteredFields((Map) value, jsonGenerator, kVar, dVar, null);
        } else {
            this.c.serialize(value, jsonGenerator, kVar);
        }
    }

    public void c(Object obj, JsonGenerator jsonGenerator, k kVar) throws Exception {
        Object value = this.b.getValue(obj);
        if (value == null) {
            return;
        }
        if (!(value instanceof Map)) {
            kVar.reportMappingProblem("Value returned by 'any-getter' %s() not java.util.Map but %s", this.b.getName(), value.getClass().getName());
        }
        MapSerializer mapSerializer = this.d;
        if (mapSerializer != null) {
            mapSerializer.serializeFields((Map) value, jsonGenerator, kVar);
        } else {
            this.c.serialize(value, jsonGenerator, kVar);
        }
    }

    public void d(k kVar) throws JsonMappingException {
        com.fasterxml.jackson.databind.f<?> fVar = this.c;
        if (fVar instanceof b) {
            com.fasterxml.jackson.databind.f<?> handlePrimaryContextualization = kVar.handlePrimaryContextualization(fVar, this.a);
            this.c = handlePrimaryContextualization;
            if (handlePrimaryContextualization instanceof MapSerializer) {
                this.d = (MapSerializer) handlePrimaryContextualization;
            }
        }
    }
}
