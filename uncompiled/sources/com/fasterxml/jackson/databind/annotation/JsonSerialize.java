package com.fasterxml.jackson.databind.annotation;

import com.fasterxml.jackson.databind.f;
import defpackage.o80;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.ANNOTATION_TYPE, ElementType.METHOD, ElementType.FIELD, ElementType.TYPE, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
/* loaded from: classes.dex */
public @interface JsonSerialize {

    @Deprecated
    /* loaded from: classes.dex */
    public enum Inclusion {
        ALWAYS,
        NON_NULL,
        NON_DEFAULT,
        NON_EMPTY,
        DEFAULT_INCLUSION
    }

    /* loaded from: classes.dex */
    public enum Typing {
        DYNAMIC,
        STATIC,
        DEFAULT_TYPING
    }

    Class<?> as() default Void.class;

    Class<?> contentAs() default Void.class;

    Class<? extends o80> contentConverter() default o80.a.class;

    Class<? extends f> contentUsing() default f.a.class;

    Class<? extends o80> converter() default o80.a.class;

    @Deprecated
    Inclusion include() default Inclusion.DEFAULT_INCLUSION;

    Class<?> keyAs() default Void.class;

    Class<? extends f> keyUsing() default f.a.class;

    Class<? extends f> nullsUsing() default f.a.class;

    Typing typing() default Typing.DEFAULT_TYPING;

    Class<? extends f> using() default f.a.class;
}
