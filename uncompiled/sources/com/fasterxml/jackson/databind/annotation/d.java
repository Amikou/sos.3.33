package com.fasterxml.jackson.databind.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/* compiled from: JsonPOJOBuilder.java */
@Target({ElementType.ANNOTATION_TYPE, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
/* loaded from: classes.dex */
public @interface d {

    /* compiled from: JsonPOJOBuilder.java */
    /* loaded from: classes.dex */
    public static class a {
        public final String a;
        public final String b;

        public a(d dVar) {
            this.a = dVar.buildMethodName();
            this.b = dVar.withPrefix();
        }
    }

    String buildMethodName() default "build";

    String withPrefix() default "with";
}
