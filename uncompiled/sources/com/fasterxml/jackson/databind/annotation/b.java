package com.fasterxml.jackson.databind.annotation;

import com.fasterxml.jackson.databind.c;
import com.fasterxml.jackson.databind.g;
import defpackage.o80;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/* compiled from: JsonDeserialize.java */
@Target({ElementType.ANNOTATION_TYPE, ElementType.METHOD, ElementType.FIELD, ElementType.TYPE, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
/* loaded from: classes.dex */
public @interface b {
    Class<?> as() default Void.class;

    Class<?> builder() default Void.class;

    Class<?> contentAs() default Void.class;

    Class<? extends o80> contentConverter() default o80.a.class;

    Class<? extends com.fasterxml.jackson.databind.c> contentUsing() default c.a.class;

    Class<? extends o80> converter() default o80.a.class;

    Class<?> keyAs() default Void.class;

    Class<? extends g> keyUsing() default g.a.class;

    Class<? extends com.fasterxml.jackson.databind.c> using() default c.a.class;
}
