package com.fasterxml.jackson.databind;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.jsontype.NamedType;

/* compiled from: Module.java */
/* loaded from: classes.dex */
public abstract class i {

    /* compiled from: Module.java */
    /* loaded from: classes.dex */
    public interface a {
        void a(com.fasterxml.jackson.databind.deser.f fVar);

        void b(ah4 ah4Var);

        void c(com.fasterxml.jackson.databind.deser.g gVar);

        void d(NamedType... namedTypeArr);

        void e(am3 am3Var);

        void f(o5 o5Var);

        void g(am3 am3Var);

        void h(PropertyNamingStrategy propertyNamingStrategy);

        void i(uo uoVar);

        void j(Class<?> cls, Class<?> cls2);

        void k(xo xoVar);
    }

    public abstract String getModuleName();

    public Object getTypeId() {
        return getClass().getName();
    }

    public abstract void setupModule(a aVar);

    public abstract Version version();
}
