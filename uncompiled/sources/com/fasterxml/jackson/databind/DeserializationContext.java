package com.fasterxml.jackson.databind;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.ObjectIdGenerator;
import com.fasterxml.jackson.annotation.t;
import com.fasterxml.jackson.core.Base64Variant;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.cfg.ContextAttributes;
import com.fasterxml.jackson.databind.deser.DeserializerCache;
import com.fasterxml.jackson.databind.deser.UnresolvedForwardReference;
import com.fasterxml.jackson.databind.deser.impl.ObjectIdReader;
import com.fasterxml.jackson.databind.deser.impl.TypeWrappedDeserializer;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.fasterxml.jackson.databind.exc.InvalidTypeIdException;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.type.TypeFactory;
import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicReference;
import okhttp3.HttpUrl;

/* loaded from: classes.dex */
public abstract class DeserializationContext extends b implements Serializable {
    private static final long serialVersionUID = 1;
    public transient lh _arrayBuilders;
    public transient ContextAttributes _attributes;
    public final DeserializerCache _cache;
    public final DeserializationConfig _config;
    public f02<JavaType> _currentType;
    public transient DateFormat _dateFormat;
    public final com.fasterxml.jackson.databind.deser.e _factory;
    public final int _featureFlags;
    public final sq1 _injectableValues;
    public transient gl2 _objectBuffer;
    public transient JsonParser _parser;
    public final Class<?> _view;

    public DeserializationContext(com.fasterxml.jackson.databind.deser.e eVar) {
        this(eVar, (DeserializerCache) null);
    }

    public String _calcName(Class<?> cls) {
        if (cls.isArray()) {
            return _calcName(cls.getComponentType()) + HttpUrl.PATH_SEGMENT_ENCODE_SET_URI;
        }
        return cls.getName();
    }

    public String _desc(String str) {
        if (str == null) {
            return "[N/A]";
        }
        if (str.length() > 500) {
            return str.substring(0, 500) + "]...[" + str.substring(str.length() - 500);
        }
        return str;
    }

    public boolean _isCompatible(Class<?> cls, Object obj) {
        if (obj == null || cls.isInstance(obj)) {
            return true;
        }
        return cls.isPrimitive() && com.fasterxml.jackson.databind.util.c.U(cls).isInstance(obj);
    }

    public String _quotedString(String str) {
        if (str == null) {
            return "[N/A]";
        }
        if (str.length() > 500) {
            return String.format("\"%s]...[%s\"", str.substring(0, 500), str.substring(str.length() - 500));
        }
        return "\"" + str + "\"";
    }

    public String _valueDesc() {
        try {
            return _desc(this._parser.X());
        } catch (Exception unused) {
            return "[N/A]";
        }
    }

    @Override // com.fasterxml.jackson.databind.b
    public final boolean canOverrideAccessModifiers() {
        return this._config.canOverrideAccessModifiers();
    }

    public abstract void checkUnresolvedObjectId() throws UnresolvedForwardReference;

    public Calendar constructCalendar(Date date) {
        Calendar calendar = Calendar.getInstance(getTimeZone());
        calendar.setTime(date);
        return calendar;
    }

    public final JavaType constructType(Class<?> cls) {
        return this._config.constructType(cls);
    }

    public abstract c<Object> deserializerInstance(ue ueVar, Object obj) throws JsonMappingException;

    public String determineClassName(Object obj) {
        return com.fasterxml.jackson.databind.util.c.v(obj);
    }

    @Deprecated
    public JsonMappingException endOfInputException(Class<?> cls) {
        JsonParser jsonParser = this._parser;
        return JsonMappingException.from(jsonParser, "Unexpected end-of-input when trying to deserialize a " + cls.getName());
    }

    public Class<?> findClass(String str) throws ClassNotFoundException {
        return getTypeFactory().findClass(str);
    }

    public final c<Object> findContextualValueDeserializer(JavaType javaType, a aVar) throws JsonMappingException {
        c<Object> findValueDeserializer = this._cache.findValueDeserializer(this, this._factory, javaType);
        return findValueDeserializer != null ? handleSecondaryContextualization(findValueDeserializer, aVar, javaType) : findValueDeserializer;
    }

    public final Object findInjectableValue(Object obj, a aVar, Object obj2) {
        throw new IllegalStateException("No 'injectableValues' configured, can not inject value with id [" + obj + "]");
    }

    public final g findKeyDeserializer(JavaType javaType, a aVar) throws JsonMappingException {
        g findKeyDeserializer = this._cache.findKeyDeserializer(this, this._factory, javaType);
        return findKeyDeserializer instanceof com.fasterxml.jackson.databind.deser.b ? ((com.fasterxml.jackson.databind.deser.b) findKeyDeserializer).createContextual(this, aVar) : findKeyDeserializer;
    }

    public final c<Object> findNonContextualValueDeserializer(JavaType javaType) throws JsonMappingException {
        return this._cache.findValueDeserializer(this, this._factory, javaType);
    }

    public abstract com.fasterxml.jackson.databind.deser.impl.e findObjectId(Object obj, ObjectIdGenerator<?> objectIdGenerator, t tVar);

    public final c<Object> findRootValueDeserializer(JavaType javaType) throws JsonMappingException {
        c<Object> findValueDeserializer = this._cache.findValueDeserializer(this, this._factory, javaType);
        if (findValueDeserializer == null) {
            return null;
        }
        c<?> handleSecondaryContextualization = handleSecondaryContextualization(findValueDeserializer, null, javaType);
        com.fasterxml.jackson.databind.jsontype.a findTypeDeserializer = this._factory.findTypeDeserializer(this._config, javaType);
        return findTypeDeserializer != null ? new TypeWrappedDeserializer(findTypeDeserializer.forProperty(null), handleSecondaryContextualization) : handleSecondaryContextualization;
    }

    @Override // com.fasterxml.jackson.databind.b
    public final Class<?> getActiveView() {
        return this._view;
    }

    @Override // com.fasterxml.jackson.databind.b
    public final AnnotationIntrospector getAnnotationIntrospector() {
        return this._config.getAnnotationIntrospector();
    }

    public final lh getArrayBuilders() {
        if (this._arrayBuilders == null) {
            this._arrayBuilders = new lh();
        }
        return this._arrayBuilders;
    }

    @Override // com.fasterxml.jackson.databind.b
    public Object getAttribute(Object obj) {
        return this._attributes.getAttribute(obj);
    }

    public final Base64Variant getBase64Variant() {
        return this._config.getBase64Variant();
    }

    public JavaType getContextualType() {
        f02<JavaType> f02Var = this._currentType;
        if (f02Var == null) {
            return null;
        }
        return f02Var.d();
    }

    public DateFormat getDateFormat() {
        DateFormat dateFormat = this._dateFormat;
        if (dateFormat != null) {
            return dateFormat;
        }
        DateFormat dateFormat2 = (DateFormat) this._config.getDateFormat().clone();
        this._dateFormat = dateFormat2;
        return dateFormat2;
    }

    @Override // com.fasterxml.jackson.databind.b
    public final JsonFormat.Value getDefaultPropertyFormat(Class<?> cls) {
        return this._config.getDefaultPropertyFormat(cls);
    }

    public final int getDeserializationFeatures() {
        return this._featureFlags;
    }

    public com.fasterxml.jackson.databind.deser.e getFactory() {
        return this._factory;
    }

    @Override // com.fasterxml.jackson.databind.b
    public Locale getLocale() {
        return this._config.getLocale();
    }

    public final JsonNodeFactory getNodeFactory() {
        return this._config.getNodeFactory();
    }

    public final JsonParser getParser() {
        return this._parser;
    }

    @Override // com.fasterxml.jackson.databind.b
    public TimeZone getTimeZone() {
        return this._config.getTimeZone();
    }

    @Override // com.fasterxml.jackson.databind.b
    public final TypeFactory getTypeFactory() {
        return this._config.getTypeFactory();
    }

    public Object handleInstantiationProblem(Class<?> cls, Object obj, Throwable th) throws IOException {
        for (f02<com.fasterxml.jackson.databind.deser.d> problemHandlers = this._config.getProblemHandlers(); problemHandlers != null; problemHandlers = problemHandlers.c()) {
            Object a = problemHandlers.d().a(this, cls, obj, th);
            if (a != com.fasterxml.jackson.databind.deser.d.a) {
                if (_isCompatible(cls, a)) {
                    return a;
                }
                throw instantiationException(cls, String.format("DeserializationProblemHandler.handleInstantiationProblem() for type %s returned value of type %s", cls, a.getClass()));
            }
        }
        if (th instanceof IOException) {
            throw ((IOException) th);
        }
        throw instantiationException(cls, th);
    }

    public Object handleMissingInstantiator(Class<?> cls, JsonParser jsonParser, String str, Object... objArr) throws IOException {
        if (objArr.length > 0) {
            str = String.format(str, objArr);
        }
        for (f02<com.fasterxml.jackson.databind.deser.d> problemHandlers = this._config.getProblemHandlers(); problemHandlers != null; problemHandlers = problemHandlers.c()) {
            Object b = problemHandlers.d().b(this, cls, jsonParser, str);
            if (b != com.fasterxml.jackson.databind.deser.d.a) {
                if (_isCompatible(cls, b)) {
                    return b;
                }
                throw instantiationException(cls, String.format("DeserializationProblemHandler.handleMissingInstantiator() for type %s returned value of type %s", cls, b.getClass()));
            }
        }
        throw instantiationException(cls, str);
    }

    public c<?> handlePrimaryContextualization(c<?> cVar, a aVar, JavaType javaType) throws JsonMappingException {
        if (cVar instanceof com.fasterxml.jackson.databind.deser.a) {
            this._currentType = new f02<>(javaType, this._currentType);
            try {
                cVar = ((com.fasterxml.jackson.databind.deser.a) cVar).createContextual(this, aVar);
            } finally {
                this._currentType = this._currentType.c();
            }
        }
        return cVar;
    }

    public c<?> handleSecondaryContextualization(c<?> cVar, a aVar, JavaType javaType) throws JsonMappingException {
        if (cVar instanceof com.fasterxml.jackson.databind.deser.a) {
            this._currentType = new f02<>(javaType, this._currentType);
            try {
                cVar = ((com.fasterxml.jackson.databind.deser.a) cVar).createContextual(this, aVar);
            } finally {
                this._currentType = this._currentType.c();
            }
        }
        return cVar;
    }

    public Object handleUnexpectedToken(Class<?> cls, JsonParser jsonParser) throws IOException {
        return handleUnexpectedToken(cls, jsonParser.u(), jsonParser, null, new Object[0]);
    }

    public boolean handleUnknownProperty(JsonParser jsonParser, c<?> cVar, Object obj, String str) throws IOException {
        for (f02<com.fasterxml.jackson.databind.deser.d> problemHandlers = this._config.getProblemHandlers(); problemHandlers != null; problemHandlers = problemHandlers.c()) {
            if (problemHandlers.d().d(this, jsonParser, cVar, obj, str)) {
                return true;
            }
        }
        if (!isEnabled(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)) {
            jsonParser.k1();
            return true;
        }
        throw UnrecognizedPropertyException.from(this._parser, obj, str, cVar == null ? null : cVar.getKnownPropertyNames());
    }

    public JavaType handleUnknownTypeId(JavaType javaType, String str, com.fasterxml.jackson.databind.jsontype.b bVar, String str2) throws IOException {
        for (f02<com.fasterxml.jackson.databind.deser.d> problemHandlers = this._config.getProblemHandlers(); problemHandlers != null; problemHandlers = problemHandlers.c()) {
            JavaType e = problemHandlers.d().e(this, javaType, str, bVar, str2);
            if (e != null) {
                if (e.hasRawClass(Void.class)) {
                    return null;
                }
                if (e.isTypeOrSubTypeOf(javaType.getRawClass())) {
                    return e;
                }
                throw unknownTypeIdException(javaType, str, "problem handler tried to resolve into non-subtype: " + e);
            }
        }
        if (isEnabled(DeserializationFeature.FAIL_ON_INVALID_SUBTYPE)) {
            throw unknownTypeIdException(javaType, str, str2);
        }
        return null;
    }

    public Object handleWeirdKey(Class<?> cls, String str, String str2, Object... objArr) throws IOException {
        if (objArr.length > 0) {
            str2 = String.format(str2, objArr);
        }
        for (f02<com.fasterxml.jackson.databind.deser.d> problemHandlers = this._config.getProblemHandlers(); problemHandlers != null; problemHandlers = problemHandlers.c()) {
            Object f = problemHandlers.d().f(this, cls, str, str2);
            if (f != com.fasterxml.jackson.databind.deser.d.a) {
                if (f == null || cls.isInstance(f)) {
                    return f;
                }
                throw weirdStringException(str, cls, String.format("DeserializationProblemHandler.handleWeirdStringValue() for type %s returned value of type %s", cls, f.getClass()));
            }
        }
        throw weirdKeyException(cls, str, str2);
    }

    public Object handleWeirdNumberValue(Class<?> cls, Number number, String str, Object... objArr) throws IOException {
        if (objArr.length > 0) {
            str = String.format(str, objArr);
        }
        for (f02<com.fasterxml.jackson.databind.deser.d> problemHandlers = this._config.getProblemHandlers(); problemHandlers != null; problemHandlers = problemHandlers.c()) {
            Object g = problemHandlers.d().g(this, cls, number, str);
            if (g != com.fasterxml.jackson.databind.deser.d.a) {
                if (_isCompatible(cls, g)) {
                    return g;
                }
                throw weirdNumberException(number, cls, String.format("DeserializationProblemHandler.handleWeirdNumberValue() for type %s returned value of type %s", cls, g.getClass()));
            }
        }
        throw weirdNumberException(number, cls, str);
    }

    public Object handleWeirdStringValue(Class<?> cls, String str, String str2, Object... objArr) throws IOException {
        if (objArr.length > 0) {
            str2 = String.format(str2, objArr);
        }
        for (f02<com.fasterxml.jackson.databind.deser.d> problemHandlers = this._config.getProblemHandlers(); problemHandlers != null; problemHandlers = problemHandlers.c()) {
            Object h = problemHandlers.d().h(this, cls, str, str2);
            if (h != com.fasterxml.jackson.databind.deser.d.a) {
                if (_isCompatible(cls, h)) {
                    return h;
                }
                throw weirdStringException(str, cls, String.format("DeserializationProblemHandler.handleWeirdStringValue() for type %s returned value of type %s", cls, h.getClass()));
            }
        }
        throw weirdStringException(str, cls, str2);
    }

    public final boolean hasDeserializationFeatures(int i) {
        return (this._featureFlags & i) == i;
    }

    public final boolean hasSomeOfFeatures(int i) {
        return (i & this._featureFlags) != 0;
    }

    public boolean hasValueDeserializerFor(JavaType javaType, AtomicReference<Throwable> atomicReference) {
        try {
            return this._cache.hasValueDeserializerFor(this, this._factory, javaType);
        } catch (JsonMappingException e) {
            if (atomicReference != null) {
                atomicReference.set(e);
                return false;
            }
            return false;
        } catch (RuntimeException e2) {
            if (atomicReference != null) {
                atomicReference.set(e2);
                return false;
            }
            throw e2;
        }
    }

    public JsonMappingException instantiationException(Class<?> cls, Throwable th) {
        return JsonMappingException.from(this._parser, String.format("Can not construct instance of %s, problem: %s", cls.getName(), th.getMessage()), th);
    }

    @Override // com.fasterxml.jackson.databind.b
    public final boolean isEnabled(MapperFeature mapperFeature) {
        return this._config.isEnabled(mapperFeature);
    }

    public abstract g keyDeserializerInstance(ue ueVar, Object obj) throws JsonMappingException;

    public final gl2 leaseObjectBuffer() {
        gl2 gl2Var = this._objectBuffer;
        if (gl2Var == null) {
            return new gl2();
        }
        this._objectBuffer = null;
        return gl2Var;
    }

    public JsonMappingException mappingException(String str) {
        return JsonMappingException.from(getParser(), str);
    }

    public Date parseDate(String str) throws IllegalArgumentException {
        try {
            return getDateFormat().parse(str);
        } catch (ParseException e) {
            throw new IllegalArgumentException(String.format("Failed to parse Date value '%s': %s", str, e.getMessage()));
        }
    }

    public <T> T readPropertyValue(JsonParser jsonParser, a aVar, Class<T> cls) throws IOException {
        return (T) readPropertyValue(jsonParser, aVar, getTypeFactory().constructType(cls));
    }

    public <T> T readValue(JsonParser jsonParser, Class<T> cls) throws IOException {
        return (T) readValue(jsonParser, getTypeFactory().constructType(cls));
    }

    public <T> T reportBadPropertyDefinition(so soVar, vo voVar, String str, Object... objArr) throws JsonMappingException {
        if (objArr != null && objArr.length > 0) {
            str = String.format(str, objArr);
        }
        String str2 = org.web3j.utils.b.MISSING_REASON;
        String _quotedString = voVar == null ? org.web3j.utils.b.MISSING_REASON : _quotedString(voVar.t());
        if (soVar != null) {
            str2 = _desc(soVar.y().getGenericSignature());
        }
        throw mappingException("Invalid definition for property %s (of type %s): %s", _quotedString, str2, str);
    }

    public <T> T reportBadTypeDefinition(so soVar, String str, Object... objArr) throws JsonMappingException {
        if (objArr != null && objArr.length > 0) {
            str = String.format(str, objArr);
        }
        throw mappingException("Invalid type definition for type %s: %s", soVar == null ? org.web3j.utils.b.MISSING_REASON : _desc(soVar.y().getGenericSignature()), str);
    }

    public void reportMappingException(String str, Object... objArr) throws JsonMappingException {
        if (objArr.length > 0) {
            str = String.format(str, objArr);
        }
        throw JsonMappingException.from(getParser(), str);
    }

    public void reportMissingContent(String str, Object... objArr) throws JsonMappingException {
        if (str == null) {
            str = "No content to map due to end-of-input";
        } else if (objArr.length > 0) {
            str = String.format(str, objArr);
        }
        throw JsonMappingException.from(getParser(), str);
    }

    @Deprecated
    public void reportUnknownProperty(Object obj, String str, c<?> cVar) throws JsonMappingException {
        if (isEnabled(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)) {
            throw UnrecognizedPropertyException.from(this._parser, obj, str, cVar == null ? null : cVar.getKnownPropertyNames());
        }
    }

    public void reportUnresolvedObjectId(ObjectIdReader objectIdReader, Object obj) throws JsonMappingException {
        throw JsonMappingException.from(getParser(), String.format("No Object Id found for an instance of %s, to assign to property '%s'", obj.getClass().getName(), objectIdReader.propertyName));
    }

    public void reportWrongTokenException(JsonParser jsonParser, JsonToken jsonToken, String str, Object... objArr) throws JsonMappingException {
        if (str != null && objArr.length > 0) {
            str = String.format(str, objArr);
        }
        throw wrongTokenException(jsonParser, jsonToken, str);
    }

    public final void returnObjectBuffer(gl2 gl2Var) {
        if (this._objectBuffer == null || gl2Var.h() >= this._objectBuffer.h()) {
            this._objectBuffer = gl2Var;
        }
    }

    @Deprecated
    public JsonMappingException unknownTypeException(JavaType javaType, String str, String str2) {
        String format = String.format("Could not resolve type id '%s' into a subtype of %s", str, javaType);
        if (str2 != null) {
            format = format + ": " + str2;
        }
        return JsonMappingException.from(this._parser, format);
    }

    public JsonMappingException unknownTypeIdException(JavaType javaType, String str, String str2) {
        String format = String.format("Could not resolve type id '%s' into a subtype of %s", str, javaType);
        if (str2 != null) {
            format = format + ": " + str2;
        }
        return InvalidTypeIdException.from(this._parser, format, javaType, str);
    }

    public JsonMappingException weirdKeyException(Class<?> cls, String str, String str2) {
        return InvalidFormatException.from(this._parser, String.format("Can not deserialize Map key of type %s from String %s: %s", cls.getName(), _quotedString(str), str2), str, cls);
    }

    public JsonMappingException weirdNumberException(Number number, Class<?> cls, String str) {
        return InvalidFormatException.from(this._parser, String.format("Can not deserialize value of type %s from number %s: %s", cls.getName(), String.valueOf(number), str), number, cls);
    }

    public JsonMappingException weirdStringException(String str, Class<?> cls, String str2) {
        return InvalidFormatException.from(this._parser, String.format("Can not deserialize value of type %s from String %s: %s", cls.getName(), _quotedString(str), str2), str, cls);
    }

    public JsonMappingException wrongTokenException(JsonParser jsonParser, JsonToken jsonToken, String str) {
        String format = String.format("Unexpected token (%s), expected %s", jsonParser.u(), jsonToken);
        if (str != null) {
            format = format + ": " + str;
        }
        return JsonMappingException.from(jsonParser, format);
    }

    public DeserializationContext(com.fasterxml.jackson.databind.deser.e eVar, DeserializerCache deserializerCache) {
        if (eVar != null) {
            this._factory = eVar;
            this._cache = deserializerCache == null ? new DeserializerCache() : deserializerCache;
            this._featureFlags = 0;
            this._config = null;
            this._view = null;
            this._attributes = null;
            return;
        }
        throw new IllegalArgumentException("Can not pass null DeserializerFactory");
    }

    @Override // com.fasterxml.jackson.databind.b
    public DeserializationConfig getConfig() {
        return this._config;
    }

    public Object handleUnexpectedToken(Class<?> cls, JsonToken jsonToken, JsonParser jsonParser, String str, Object... objArr) throws IOException {
        if (objArr.length > 0) {
            str = String.format(str, objArr);
        }
        for (f02<com.fasterxml.jackson.databind.deser.d> problemHandlers = this._config.getProblemHandlers(); problemHandlers != null; problemHandlers = problemHandlers.c()) {
            Object c = problemHandlers.d().c(this, cls, jsonToken, jsonParser, str);
            if (c != com.fasterxml.jackson.databind.deser.d.a) {
                if (_isCompatible(cls, c)) {
                    return c;
                }
                reportMappingException("DeserializationProblemHandler.handleUnexpectedToken() for type %s returned value of type %s", cls, c.getClass());
            }
        }
        if (str == null) {
            if (jsonToken == null) {
                str = String.format("Unexpected end-of-input when binding data into %s", _calcName(cls));
            } else {
                str = String.format("Can not deserialize instance of %s out of %s token", _calcName(cls), jsonToken);
            }
        }
        reportMappingException(str, new Object[0]);
        return null;
    }

    public final boolean isEnabled(DeserializationFeature deserializationFeature) {
        return (deserializationFeature.getMask() & this._featureFlags) != 0;
    }

    public JsonMappingException mappingException(String str, Object... objArr) {
        if (objArr != null && objArr.length > 0) {
            str = String.format(str, objArr);
        }
        return JsonMappingException.from(getParser(), str);
    }

    public <T> T readPropertyValue(JsonParser jsonParser, a aVar, JavaType javaType) throws IOException {
        String str;
        c<Object> findContextualValueDeserializer = findContextualValueDeserializer(javaType, aVar);
        if (findContextualValueDeserializer == null) {
            if (aVar == null) {
                str = "NULL";
            } else {
                str = "'" + aVar.getName() + "'";
            }
            reportMappingException("Could not find JsonDeserializer for type %s (via property %s)", javaType, str);
        }
        return (T) findContextualValueDeserializer.deserialize(jsonParser, this);
    }

    public <T> T readValue(JsonParser jsonParser, JavaType javaType) throws IOException {
        c<Object> findRootValueDeserializer = findRootValueDeserializer(javaType);
        if (findRootValueDeserializer == null) {
            reportMappingException("Could not find JsonDeserializer for type %s", javaType);
        }
        return (T) findRootValueDeserializer.deserialize(jsonParser, this);
    }

    @Override // com.fasterxml.jackson.databind.b
    public DeserializationContext setAttribute(Object obj, Object obj2) {
        this._attributes = this._attributes.withPerCallAttribute(obj, obj2);
        return this;
    }

    public JsonMappingException instantiationException(Class<?> cls, String str) {
        return JsonMappingException.from(this._parser, String.format("Can not construct instance of %s: %s", cls.getName(), str));
    }

    @Deprecated
    public JsonMappingException mappingException(Class<?> cls) {
        return mappingException(cls, this._parser.u());
    }

    @Deprecated
    public c<?> handlePrimaryContextualization(c<?> cVar, a aVar) throws JsonMappingException {
        return handlePrimaryContextualization(cVar, aVar, TypeFactory.unknownType());
    }

    @Deprecated
    public c<?> handleSecondaryContextualization(c<?> cVar, a aVar) throws JsonMappingException {
        return cVar instanceof com.fasterxml.jackson.databind.deser.a ? ((com.fasterxml.jackson.databind.deser.a) cVar).createContextual(this, aVar) : cVar;
    }

    @Deprecated
    public JsonMappingException mappingException(Class<?> cls, JsonToken jsonToken) {
        return JsonMappingException.from(this._parser, String.format("Can not deserialize instance of %s out of %s", _calcName(cls), jsonToken == null ? "<end of input>" : String.format("%s token", jsonToken)));
    }

    public DeserializationContext(DeserializationContext deserializationContext, com.fasterxml.jackson.databind.deser.e eVar) {
        this._cache = deserializationContext._cache;
        this._factory = eVar;
        this._config = deserializationContext._config;
        this._featureFlags = deserializationContext._featureFlags;
        this._view = deserializationContext._view;
        this._parser = deserializationContext._parser;
        this._attributes = deserializationContext._attributes;
    }

    public DeserializationContext(DeserializationContext deserializationContext, DeserializationConfig deserializationConfig, JsonParser jsonParser, sq1 sq1Var) {
        this._cache = deserializationContext._cache;
        this._factory = deserializationContext._factory;
        this._config = deserializationConfig;
        this._featureFlags = deserializationConfig.getDeserializationFeatures();
        this._view = deserializationConfig.getActiveView();
        this._parser = jsonParser;
        this._attributes = deserializationConfig.getAttributes();
    }

    public DeserializationContext(DeserializationContext deserializationContext) {
        this._cache = new DeserializerCache();
        this._factory = deserializationContext._factory;
        this._config = deserializationContext._config;
        this._featureFlags = deserializationContext._featureFlags;
        this._view = deserializationContext._view;
    }
}
