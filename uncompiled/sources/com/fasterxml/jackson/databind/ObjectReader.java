package com.fasterxml.jackson.databind;

import com.fasterxml.jackson.core.Base64Variant;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.cfg.ContextAttributes;
import com.fasterxml.jackson.databind.deser.DefaultDeserializationContext;
import com.fasterxml.jackson.databind.deser.c;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.l;
import com.fasterxml.jackson.databind.node.q;
import com.fasterxml.jackson.databind.type.SimpleType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import java.io.DataInput;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.Serializable;
import java.lang.reflect.Type;
import java.net.URL;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;

/* loaded from: classes.dex */
public class ObjectReader extends com.fasterxml.jackson.core.c implements Serializable {
    public static final JavaType a = SimpleType.constructUnsafe(d.class);
    private static final long serialVersionUID = 1;
    public final DeserializationConfig _config;
    public final DefaultDeserializationContext _context;
    public final com.fasterxml.jackson.databind.deser.c _dataFormatReaders;
    private final v64 _filter;
    public final sq1 _injectableValues;
    public final JsonFactory _parserFactory;
    public final c<Object> _rootDeserializer;
    public final ConcurrentHashMap<JavaType, c<Object>> _rootDeserializers;
    public final z81 _schema;
    public final boolean _unwrapRoot;
    public final Object _valueToUpdate;
    public final JavaType _valueType;

    public ObjectReader(ObjectMapper objectMapper, DeserializationConfig deserializationConfig) {
        this(objectMapper, deserializationConfig, null, null, null, null);
    }

    public Object _bind(JsonParser jsonParser, Object obj) throws IOException {
        DefaultDeserializationContext createDeserializationContext = createDeserializationContext(jsonParser);
        JsonToken _initForReading = _initForReading(createDeserializationContext, jsonParser);
        if (_initForReading == JsonToken.VALUE_NULL) {
            if (obj == null) {
                obj = _findRootDeserializer(createDeserializationContext).getNullValue(createDeserializationContext);
            }
        } else if (_initForReading != JsonToken.END_ARRAY && _initForReading != JsonToken.END_OBJECT) {
            c<Object> _findRootDeserializer = _findRootDeserializer(createDeserializationContext);
            if (this._unwrapRoot) {
                obj = _unwrapAndDeserialize(jsonParser, createDeserializationContext, this._valueType, _findRootDeserializer);
            } else if (obj == null) {
                obj = _findRootDeserializer.deserialize(jsonParser, createDeserializationContext);
            } else {
                _findRootDeserializer.deserialize(jsonParser, createDeserializationContext, obj);
            }
        }
        jsonParser.e();
        return obj;
    }

    public Object _bindAndClose(JsonParser jsonParser) throws IOException {
        Object obj;
        try {
            DefaultDeserializationContext createDeserializationContext = createDeserializationContext(jsonParser);
            JsonToken _initForReading = _initForReading(createDeserializationContext, jsonParser);
            if (_initForReading == JsonToken.VALUE_NULL) {
                obj = this._valueToUpdate;
                if (obj == null) {
                    obj = _findRootDeserializer(createDeserializationContext).getNullValue(createDeserializationContext);
                }
            } else {
                if (_initForReading != JsonToken.END_ARRAY && _initForReading != JsonToken.END_OBJECT) {
                    c<Object> _findRootDeserializer = _findRootDeserializer(createDeserializationContext);
                    if (this._unwrapRoot) {
                        obj = _unwrapAndDeserialize(jsonParser, createDeserializationContext, this._valueType, _findRootDeserializer);
                    } else {
                        Object obj2 = this._valueToUpdate;
                        if (obj2 == null) {
                            obj = _findRootDeserializer.deserialize(jsonParser, createDeserializationContext);
                        } else {
                            _findRootDeserializer.deserialize(jsonParser, createDeserializationContext, obj2);
                            obj = this._valueToUpdate;
                        }
                    }
                }
                obj = this._valueToUpdate;
            }
            if (jsonParser != null) {
                jsonParser.close();
            }
            return obj;
        } catch (Throwable th) {
            try {
                throw th;
            } catch (Throwable th2) {
                if (jsonParser != null) {
                    try {
                        jsonParser.close();
                    } catch (Throwable th3) {
                        th.addSuppressed(th3);
                    }
                }
                throw th2;
            }
        }
    }

    public d _bindAndCloseAsTree(JsonParser jsonParser) throws IOException {
        try {
            d _bindAsTree = _bindAsTree(jsonParser);
            if (jsonParser != null) {
                jsonParser.close();
            }
            return _bindAsTree;
        } catch (Throwable th) {
            try {
                throw th;
            } catch (Throwable th2) {
                if (jsonParser != null) {
                    try {
                        jsonParser.close();
                    } catch (Throwable th3) {
                        th.addSuppressed(th3);
                    }
                }
                throw th2;
            }
        }
    }

    public <T> h<T> _bindAndReadValues(JsonParser jsonParser) throws IOException {
        DefaultDeserializationContext createDeserializationContext = createDeserializationContext(jsonParser);
        _initForMultiRead(createDeserializationContext, jsonParser);
        jsonParser.T0();
        return _newIterator(jsonParser, createDeserializationContext, _findRootDeserializer(createDeserializationContext), true);
    }

    public d _bindAsTree(JsonParser jsonParser) throws IOException {
        d dVar;
        DefaultDeserializationContext createDeserializationContext = createDeserializationContext(jsonParser);
        JsonToken _initForReading = _initForReading(createDeserializationContext, jsonParser);
        if (_initForReading != JsonToken.VALUE_NULL && _initForReading != JsonToken.END_ARRAY && _initForReading != JsonToken.END_OBJECT) {
            c<Object> _findTreeDeserializer = _findTreeDeserializer(createDeserializationContext);
            if (this._unwrapRoot) {
                dVar = (d) _unwrapAndDeserialize(jsonParser, createDeserializationContext, a, _findTreeDeserializer);
            } else {
                dVar = (d) _findTreeDeserializer.deserialize(jsonParser, createDeserializationContext);
            }
        } else {
            dVar = l.a;
        }
        jsonParser.e();
        return dVar;
    }

    public JsonParser _considerFilter(JsonParser jsonParser, boolean z) {
        return (this._filter == null || m41.class.isInstance(jsonParser)) ? jsonParser : new m41(jsonParser, this._filter, false, z);
    }

    public Object _detectBindAndClose(byte[] bArr, int i, int i2) throws IOException {
        c.b c = this._dataFormatReaders.c(bArr, i, i2);
        if (!c.d()) {
            _reportUnkownFormat(this._dataFormatReaders, c);
        }
        return c.c()._bindAndClose(c.a());
    }

    public d _detectBindAndCloseAsTree(InputStream inputStream) throws IOException {
        c.b b = this._dataFormatReaders.b(inputStream);
        if (!b.d()) {
            _reportUnkownFormat(this._dataFormatReaders, b);
        }
        JsonParser a2 = b.a();
        a2.g(JsonParser.Feature.AUTO_CLOSE_SOURCE);
        return b.c()._bindAndCloseAsTree(a2);
    }

    public <T> h<T> _detectBindAndReadValues(c.b bVar, boolean z) throws IOException, JsonProcessingException {
        if (!bVar.d()) {
            _reportUnkownFormat(this._dataFormatReaders, bVar);
        }
        JsonParser a2 = bVar.a();
        if (z) {
            a2.g(JsonParser.Feature.AUTO_CLOSE_SOURCE);
        }
        return bVar.c()._bindAndReadValues(a2);
    }

    public c<Object> _findRootDeserializer(DeserializationContext deserializationContext) throws JsonMappingException {
        c<Object> cVar = this._rootDeserializer;
        if (cVar != null) {
            return cVar;
        }
        JavaType javaType = this._valueType;
        if (javaType == null) {
            deserializationContext.reportMappingException("No value type configured for ObjectReader", new Object[0]);
        }
        c<Object> cVar2 = this._rootDeserializers.get(javaType);
        if (cVar2 != null) {
            return cVar2;
        }
        c<Object> findRootValueDeserializer = deserializationContext.findRootValueDeserializer(javaType);
        if (findRootValueDeserializer == null) {
            deserializationContext.reportMappingException("Can not find a deserializer for type %s", javaType);
        }
        this._rootDeserializers.put(javaType, findRootValueDeserializer);
        return findRootValueDeserializer;
    }

    public c<Object> _findTreeDeserializer(DeserializationContext deserializationContext) throws JsonMappingException {
        ConcurrentHashMap<JavaType, c<Object>> concurrentHashMap = this._rootDeserializers;
        JavaType javaType = a;
        c<Object> cVar = concurrentHashMap.get(javaType);
        if (cVar == null) {
            cVar = deserializationContext.findRootValueDeserializer(javaType);
            if (cVar == null) {
                deserializationContext.reportMappingException("Can not find a deserializer for type %s", javaType);
            }
            this._rootDeserializers.put(javaType, cVar);
        }
        return cVar;
    }

    public void _initForMultiRead(DeserializationContext deserializationContext, JsonParser jsonParser) throws IOException {
        z81 z81Var = this._schema;
        if (z81Var != null) {
            jsonParser.i1(z81Var);
        }
        this._config.initialize(jsonParser);
    }

    public JsonToken _initForReading(DeserializationContext deserializationContext, JsonParser jsonParser) throws IOException {
        z81 z81Var = this._schema;
        if (z81Var != null) {
            jsonParser.i1(z81Var);
        }
        this._config.initialize(jsonParser);
        JsonToken u = jsonParser.u();
        if (u == null && (u = jsonParser.T0()) == null) {
            deserializationContext.reportMissingContent(null, new Object[0]);
        }
        return u;
    }

    public InputStream _inputStream(URL url) throws IOException {
        return url.openStream();
    }

    public ObjectReader _new(ObjectReader objectReader, JsonFactory jsonFactory) {
        return new ObjectReader(objectReader, jsonFactory);
    }

    public <T> h<T> _newIterator(JsonParser jsonParser, DeserializationContext deserializationContext, c<?> cVar, boolean z) {
        return new h<>(this._valueType, jsonParser, deserializationContext, cVar, z, this._valueToUpdate);
    }

    public c<Object> _prefetchRootDeserializer(JavaType javaType) {
        if (javaType == null || !this._config.isEnabled(DeserializationFeature.EAGER_DESERIALIZER_FETCH)) {
            return null;
        }
        c<Object> cVar = this._rootDeserializers.get(javaType);
        if (cVar == null) {
            try {
                cVar = createDeserializationContext(null).findRootValueDeserializer(javaType);
                if (cVar != null) {
                    this._rootDeserializers.put(javaType, cVar);
                }
            } catch (JsonProcessingException unused) {
            }
        }
        return cVar;
    }

    public void _reportUndetectableSource(Object obj) throws JsonProcessingException {
        throw new JsonParseException((JsonParser) null, "Can not use source of type " + obj.getClass().getName() + " with format auto-detection: must be byte- not char-based");
    }

    public void _reportUnkownFormat(com.fasterxml.jackson.databind.deser.c cVar, c.b bVar) throws JsonProcessingException {
        throw new JsonParseException((JsonParser) null, "Can not detect format from input, does not look like any of detectable formats " + cVar.toString());
    }

    public Object _unwrapAndDeserialize(JsonParser jsonParser, DeserializationContext deserializationContext, JavaType javaType, c<Object> cVar) throws IOException {
        Object obj;
        String simpleName = this._config.findRootName(javaType).getSimpleName();
        JsonToken u = jsonParser.u();
        JsonToken jsonToken = JsonToken.START_OBJECT;
        if (u != jsonToken) {
            deserializationContext.reportWrongTokenException(jsonParser, jsonToken, "Current token not START_OBJECT (needed to unwrap root name '%s'), but %s", simpleName, jsonParser.u());
        }
        JsonToken T0 = jsonParser.T0();
        JsonToken jsonToken2 = JsonToken.FIELD_NAME;
        if (T0 != jsonToken2) {
            deserializationContext.reportWrongTokenException(jsonParser, jsonToken2, "Current token not FIELD_NAME (to contain expected root name '%s'), but %s", simpleName, jsonParser.u());
        }
        Object r = jsonParser.r();
        if (!simpleName.equals(r)) {
            deserializationContext.reportMappingException("Root name '%s' does not match expected ('%s') for type %s", r, simpleName, javaType);
        }
        jsonParser.T0();
        Object obj2 = this._valueToUpdate;
        if (obj2 == null) {
            obj = cVar.deserialize(jsonParser, deserializationContext);
        } else {
            cVar.deserialize(jsonParser, deserializationContext, obj2);
            obj = this._valueToUpdate;
        }
        JsonToken T02 = jsonParser.T0();
        JsonToken jsonToken3 = JsonToken.END_OBJECT;
        if (T02 != jsonToken3) {
            deserializationContext.reportWrongTokenException(jsonParser, jsonToken3, "Current token not END_OBJECT (to match wrapper object with root name '%s'), but %s", simpleName, jsonParser.u());
        }
        return obj;
    }

    public void _verifySchemaType(z81 z81Var) {
        if (z81Var == null || this._parserFactory.canUseSchema(z81Var)) {
            return;
        }
        throw new IllegalArgumentException("Can not use FormatSchema of type " + z81Var.getClass().getName() + " for format " + this._parserFactory.getFormatName());
    }

    public ObjectReader _with(DeserializationConfig deserializationConfig) {
        if (deserializationConfig == this._config) {
            return this;
        }
        ObjectReader _new = _new(this, deserializationConfig);
        com.fasterxml.jackson.databind.deser.c cVar = this._dataFormatReaders;
        return cVar != null ? _new.withFormatDetection(cVar.d(deserializationConfig)) : _new;
    }

    public ObjectReader at(String str) {
        return new ObjectReader(this, new gv1(str));
    }

    public DefaultDeserializationContext createDeserializationContext(JsonParser jsonParser) {
        return this._context.createInstance(this._config, jsonParser, this._injectableValues);
    }

    public ObjectReader forType(JavaType javaType) {
        if (javaType == null || !javaType.equals(this._valueType)) {
            c<Object> _prefetchRootDeserializer = _prefetchRootDeserializer(javaType);
            com.fasterxml.jackson.databind.deser.c cVar = this._dataFormatReaders;
            if (cVar != null) {
                cVar = cVar.e(javaType);
            }
            return _new(this, this._config, javaType, _prefetchRootDeserializer, this._valueToUpdate, this._schema, this._injectableValues, cVar);
        }
        return this;
    }

    public ContextAttributes getAttributes() {
        return this._config.getAttributes();
    }

    public DeserializationConfig getConfig() {
        return this._config;
    }

    @Override // com.fasterxml.jackson.core.c
    public JsonFactory getFactory() {
        return this._parserFactory;
    }

    public sq1 getInjectableValues() {
        return this._injectableValues;
    }

    public TypeFactory getTypeFactory() {
        return this._config.getTypeFactory();
    }

    public boolean isEnabled(DeserializationFeature deserializationFeature) {
        return this._config.isEnabled(deserializationFeature);
    }

    @Override // com.fasterxml.jackson.core.c, com.fasterxml.jackson.core.e
    public <T extends com.fasterxml.jackson.core.f> T readTree(JsonParser jsonParser) throws IOException {
        return _bindAsTree(jsonParser);
    }

    public <T> T readValue(JsonParser jsonParser) throws IOException {
        return (T) _bind(jsonParser, this._valueToUpdate);
    }

    @Override // com.fasterxml.jackson.core.c
    public <T> Iterator<T> readValues(JsonParser jsonParser, Class<T> cls) throws IOException {
        return forType((Class<?>) cls).readValues(jsonParser);
    }

    @Override // com.fasterxml.jackson.core.c, com.fasterxml.jackson.core.e
    public JsonParser treeAsTokens(com.fasterxml.jackson.core.f fVar) {
        return new q((d) fVar, this);
    }

    @Override // com.fasterxml.jackson.core.c
    public <T> T treeToValue(com.fasterxml.jackson.core.f fVar, Class<T> cls) throws JsonProcessingException {
        try {
            return (T) readValue(treeAsTokens(fVar), cls);
        } catch (JsonProcessingException e) {
            throw e;
        } catch (IOException e2) {
            throw new IllegalArgumentException(e2.getMessage(), e2);
        }
    }

    @Override // com.fasterxml.jackson.core.c
    public Version version() {
        return wo2.a;
    }

    public ObjectReader with(DeserializationFeature deserializationFeature) {
        return _with(this._config.with(deserializationFeature));
    }

    public ObjectReader withAttribute(Object obj, Object obj2) {
        return _with(this._config.withAttribute(obj, obj2));
    }

    public ObjectReader withAttributes(Map<?, ?> map) {
        return _with(this._config.withAttributes(map));
    }

    public ObjectReader withFeatures(DeserializationFeature... deserializationFeatureArr) {
        return _with(this._config.withFeatures(deserializationFeatureArr));
    }

    public ObjectReader withFormatDetection(ObjectReader... objectReaderArr) {
        return withFormatDetection(new com.fasterxml.jackson.databind.deser.c(objectReaderArr));
    }

    public ObjectReader withHandler(com.fasterxml.jackson.databind.deser.d dVar) {
        return _with(this._config.withHandler(dVar));
    }

    public ObjectReader withRootName(String str) {
        return _with(this._config.withRootName(str));
    }

    @Deprecated
    public ObjectReader withType(JavaType javaType) {
        return forType(javaType);
    }

    public ObjectReader withValueToUpdate(Object obj) {
        if (obj == this._valueToUpdate) {
            return this;
        }
        if (obj == null) {
            return _new(this, this._config, this._valueType, this._rootDeserializer, null, this._schema, this._injectableValues, this._dataFormatReaders);
        }
        JavaType javaType = this._valueType;
        if (javaType == null) {
            javaType = this._config.constructType(obj.getClass());
        }
        return _new(this, this._config, javaType, this._rootDeserializer, obj, this._schema, this._injectableValues, this._dataFormatReaders);
    }

    public ObjectReader withView(Class<?> cls) {
        return _with(this._config.withView(cls));
    }

    public ObjectReader without(DeserializationFeature deserializationFeature) {
        return _with(this._config.without(deserializationFeature));
    }

    public ObjectReader withoutAttribute(Object obj) {
        return _with(this._config.withoutAttribute(obj));
    }

    public ObjectReader withoutFeatures(DeserializationFeature... deserializationFeatureArr) {
        return _with(this._config.withoutFeatures(deserializationFeatureArr));
    }

    public ObjectReader withoutRootName() {
        return _with(this._config.withRootName(PropertyName.NO_NAME));
    }

    @Override // com.fasterxml.jackson.core.c, com.fasterxml.jackson.core.e
    public void writeTree(JsonGenerator jsonGenerator, com.fasterxml.jackson.core.f fVar) {
        throw new UnsupportedOperationException();
    }

    @Override // com.fasterxml.jackson.core.c
    public void writeValue(JsonGenerator jsonGenerator, Object obj) throws IOException, JsonProcessingException {
        throw new UnsupportedOperationException("Not implemented for ObjectReader");
    }

    public ObjectReader(ObjectMapper objectMapper, DeserializationConfig deserializationConfig, JavaType javaType, Object obj, z81 z81Var, sq1 sq1Var) {
        this._config = deserializationConfig;
        this._context = objectMapper._deserializationContext;
        this._rootDeserializers = objectMapper._rootDeserializers;
        this._parserFactory = objectMapper._jsonFactory;
        this._valueType = javaType;
        this._valueToUpdate = obj;
        if (obj != null && javaType.isArrayType()) {
            throw new IllegalArgumentException("Can not update an array value");
        }
        this._schema = z81Var;
        this._unwrapRoot = deserializationConfig.useRootWrapping();
        this._rootDeserializer = _prefetchRootDeserializer(javaType);
        this._dataFormatReaders = null;
        this._filter = null;
    }

    public InputStream _inputStream(File file) throws IOException {
        return new FileInputStream(file);
    }

    public ObjectReader _new(ObjectReader objectReader, DeserializationConfig deserializationConfig) {
        return new ObjectReader(objectReader, deserializationConfig);
    }

    public ObjectReader at(com.fasterxml.jackson.core.b bVar) {
        return new ObjectReader(this, new gv1(bVar));
    }

    @Override // com.fasterxml.jackson.core.c, com.fasterxml.jackson.core.e
    public d createArrayNode() {
        return this._config.getNodeFactory().arrayNode();
    }

    @Override // com.fasterxml.jackson.core.c, com.fasterxml.jackson.core.e
    public d createObjectNode() {
        return this._config.getNodeFactory().objectNode();
    }

    public boolean isEnabled(MapperFeature mapperFeature) {
        return this._config.isEnabled(mapperFeature);
    }

    public d readTree(InputStream inputStream) throws IOException, JsonProcessingException {
        if (this._dataFormatReaders != null) {
            return _detectBindAndCloseAsTree(inputStream);
        }
        return _bindAndCloseAsTree(_considerFilter(this._parserFactory.createParser(inputStream), false));
    }

    @Override // com.fasterxml.jackson.core.c
    public <T> T readValue(JsonParser jsonParser, Class<T> cls) throws IOException {
        return (T) forType((Class<?>) cls).readValue(jsonParser);
    }

    @Override // com.fasterxml.jackson.core.c
    public <T> Iterator<T> readValues(JsonParser jsonParser, ud4<?> ud4Var) throws IOException {
        return forType(ud4Var).readValues(jsonParser);
    }

    public ObjectReader with(DeserializationFeature deserializationFeature, DeserializationFeature... deserializationFeatureArr) {
        return _with(this._config.with(deserializationFeature, deserializationFeatureArr));
    }

    public ObjectReader withFeatures(JsonParser.Feature... featureArr) {
        return _with(this._config.withFeatures(featureArr));
    }

    public ObjectReader withFormatDetection(com.fasterxml.jackson.databind.deser.c cVar) {
        return _new(this, this._config, this._valueType, this._rootDeserializer, this._valueToUpdate, this._schema, this._injectableValues, cVar);
    }

    public ObjectReader withRootName(PropertyName propertyName) {
        return _with(this._config.withRootName(propertyName));
    }

    @Deprecated
    public ObjectReader withType(Class<?> cls) {
        return forType(this._config.constructType(cls));
    }

    public ObjectReader without(DeserializationFeature deserializationFeature, DeserializationFeature... deserializationFeatureArr) {
        return _with(this._config.without(deserializationFeature, deserializationFeatureArr));
    }

    public ObjectReader withoutFeatures(JsonParser.Feature... featureArr) {
        return _with(this._config.withoutFeatures(featureArr));
    }

    public ObjectReader _new(ObjectReader objectReader, DeserializationConfig deserializationConfig, JavaType javaType, c<Object> cVar, Object obj, z81 z81Var, sq1 sq1Var, com.fasterxml.jackson.databind.deser.c cVar2) {
        return new ObjectReader(objectReader, deserializationConfig, javaType, cVar, obj, z81Var, sq1Var, cVar2);
    }

    public boolean isEnabled(JsonParser.Feature feature) {
        return this._parserFactory.isEnabled(feature);
    }

    @Override // com.fasterxml.jackson.core.c
    public <T> T readValue(JsonParser jsonParser, ud4<?> ud4Var) throws IOException {
        return (T) forType(ud4Var).readValue(jsonParser);
    }

    @Override // com.fasterxml.jackson.core.c
    public <T> Iterator<T> readValues(JsonParser jsonParser, r73 r73Var) throws IOException {
        return readValues(jsonParser, (JavaType) r73Var);
    }

    public ObjectReader with(JsonParser.Feature feature) {
        return _with(this._config.with(feature));
    }

    public ObjectReader withFeatures(x81... x81VarArr) {
        return _with(this._config.withFeatures(x81VarArr));
    }

    @Deprecated
    public ObjectReader withType(Type type) {
        return forType(this._config.getTypeFactory().constructType(type));
    }

    public ObjectReader without(JsonParser.Feature feature) {
        return _with(this._config.without(feature));
    }

    public ObjectReader withoutFeatures(x81... x81VarArr) {
        return _with(this._config.withoutFeatures(x81VarArr));
    }

    @Override // com.fasterxml.jackson.core.c
    public <T> T readValue(JsonParser jsonParser, r73 r73Var) throws IOException, JsonProcessingException {
        return (T) forType((JavaType) r73Var).readValue(jsonParser);
    }

    public <T> Iterator<T> readValues(JsonParser jsonParser, JavaType javaType) throws IOException {
        return forType(javaType).readValues(jsonParser);
    }

    public ObjectReader with(x81 x81Var) {
        return _with(this._config.with(x81Var));
    }

    @Deprecated
    public ObjectReader withType(ud4<?> ud4Var) {
        return forType(this._config.getTypeFactory().constructType(ud4Var.getType()));
    }

    public ObjectReader without(x81 x81Var) {
        return _with(this._config.without(x81Var));
    }

    public d readTree(Reader reader) throws IOException, JsonProcessingException {
        if (this._dataFormatReaders != null) {
            _reportUndetectableSource(reader);
        }
        return _bindAndCloseAsTree(_considerFilter(this._parserFactory.createParser(reader), false));
    }

    public <T> T readValue(JsonParser jsonParser, JavaType javaType) throws IOException {
        return (T) forType(javaType).readValue(jsonParser);
    }

    public <T> h<T> readValues(JsonParser jsonParser) throws IOException, JsonProcessingException {
        DefaultDeserializationContext createDeserializationContext = createDeserializationContext(jsonParser);
        return _newIterator(jsonParser, createDeserializationContext, _findRootDeserializer(createDeserializationContext), false);
    }

    public ObjectReader with(DeserializationConfig deserializationConfig) {
        return _with(deserializationConfig);
    }

    public Object _detectBindAndClose(c.b bVar, boolean z) throws IOException {
        if (!bVar.d()) {
            _reportUnkownFormat(this._dataFormatReaders, bVar);
        }
        JsonParser a2 = bVar.a();
        if (z) {
            a2.g(JsonParser.Feature.AUTO_CLOSE_SOURCE);
        }
        return bVar.c()._bindAndClose(a2);
    }

    public ObjectReader forType(Class<?> cls) {
        return forType(this._config.constructType(cls));
    }

    public <T> T readValue(InputStream inputStream) throws IOException, JsonProcessingException {
        com.fasterxml.jackson.databind.deser.c cVar = this._dataFormatReaders;
        if (cVar != null) {
            return (T) _detectBindAndClose(cVar.b(inputStream), false);
        }
        return (T) _bindAndClose(_considerFilter(this._parserFactory.createParser(inputStream), false));
    }

    public ObjectReader with(sq1 sq1Var) {
        return this._injectableValues == sq1Var ? this : _new(this, this._config, this._valueType, this._rootDeserializer, this._valueToUpdate, this._schema, sq1Var, this._dataFormatReaders);
    }

    public ObjectReader forType(ud4<?> ud4Var) {
        return forType(this._config.getTypeFactory().constructType(ud4Var.getType()));
    }

    public <T> h<T> readValues(InputStream inputStream) throws IOException, JsonProcessingException {
        com.fasterxml.jackson.databind.deser.c cVar = this._dataFormatReaders;
        if (cVar != null) {
            return _detectBindAndReadValues(cVar.b(inputStream), false);
        }
        return _bindAndReadValues(_considerFilter(this._parserFactory.createParser(inputStream), true));
    }

    public d readTree(String str) throws IOException, JsonProcessingException {
        if (this._dataFormatReaders != null) {
            _reportUndetectableSource(str);
        }
        return _bindAndCloseAsTree(_considerFilter(this._parserFactory.createParser(str), false));
    }

    public ObjectReader with(JsonNodeFactory jsonNodeFactory) {
        return _with(this._config.with(jsonNodeFactory));
    }

    public <T> T readValue(Reader reader) throws IOException, JsonProcessingException {
        if (this._dataFormatReaders != null) {
            _reportUndetectableSource(reader);
        }
        return (T) _bindAndClose(_considerFilter(this._parserFactory.createParser(reader), false));
    }

    public ObjectReader with(JsonFactory jsonFactory) {
        if (jsonFactory == this._parserFactory) {
            return this;
        }
        ObjectReader _new = _new(this, jsonFactory);
        if (jsonFactory.getCodec() == null) {
            jsonFactory.setCodec(_new);
        }
        return _new;
    }

    public <T> h<T> readValues(Reader reader) throws IOException, JsonProcessingException {
        if (this._dataFormatReaders != null) {
            _reportUndetectableSource(reader);
        }
        JsonParser _considerFilter = _considerFilter(this._parserFactory.createParser(reader), true);
        DefaultDeserializationContext createDeserializationContext = createDeserializationContext(_considerFilter);
        _initForMultiRead(createDeserializationContext, _considerFilter);
        _considerFilter.T0();
        return _newIterator(_considerFilter, createDeserializationContext, _findRootDeserializer(createDeserializationContext), true);
    }

    public d readTree(DataInput dataInput) throws IOException {
        if (this._dataFormatReaders != null) {
            _reportUndetectableSource(dataInput);
        }
        return _bindAndCloseAsTree(_considerFilter(this._parserFactory.createParser(dataInput), false));
    }

    public <T> T readValue(String str) throws IOException, JsonProcessingException {
        if (this._dataFormatReaders != null) {
            _reportUndetectableSource(str);
        }
        return (T) _bindAndClose(_considerFilter(this._parserFactory.createParser(str), false));
    }

    public ObjectReader with(z81 z81Var) {
        if (this._schema == z81Var) {
            return this;
        }
        _verifySchemaType(z81Var);
        return _new(this, this._config, this._valueType, this._rootDeserializer, this._valueToUpdate, z81Var, this._injectableValues, this._dataFormatReaders);
    }

    public <T> T readValue(byte[] bArr) throws IOException, JsonProcessingException {
        if (this._dataFormatReaders != null) {
            return (T) _detectBindAndClose(bArr, 0, bArr.length);
        }
        return (T) _bindAndClose(_considerFilter(this._parserFactory.createParser(bArr), false));
    }

    public ObjectReader(ObjectReader objectReader, DeserializationConfig deserializationConfig, JavaType javaType, c<Object> cVar, Object obj, z81 z81Var, sq1 sq1Var, com.fasterxml.jackson.databind.deser.c cVar2) {
        this._config = deserializationConfig;
        this._context = objectReader._context;
        this._rootDeserializers = objectReader._rootDeserializers;
        this._parserFactory = objectReader._parserFactory;
        this._valueType = javaType;
        this._rootDeserializer = cVar;
        this._valueToUpdate = obj;
        if (obj != null && javaType.isArrayType()) {
            throw new IllegalArgumentException("Can not update an array value");
        }
        this._schema = z81Var;
        this._unwrapRoot = deserializationConfig.useRootWrapping();
        this._dataFormatReaders = cVar2;
        this._filter = objectReader._filter;
    }

    public ObjectReader with(Locale locale) {
        return _with(this._config.with(locale));
    }

    public <T> h<T> readValues(String str) throws IOException, JsonProcessingException {
        if (this._dataFormatReaders != null) {
            _reportUndetectableSource(str);
        }
        JsonParser _considerFilter = _considerFilter(this._parserFactory.createParser(str), true);
        DefaultDeserializationContext createDeserializationContext = createDeserializationContext(_considerFilter);
        _initForMultiRead(createDeserializationContext, _considerFilter);
        _considerFilter.T0();
        return _newIterator(_considerFilter, createDeserializationContext, _findRootDeserializer(createDeserializationContext), true);
    }

    public ObjectReader with(TimeZone timeZone) {
        return _with(this._config.with(timeZone));
    }

    public <T> T readValue(byte[] bArr, int i, int i2) throws IOException, JsonProcessingException {
        if (this._dataFormatReaders != null) {
            return (T) _detectBindAndClose(bArr, i, i2);
        }
        return (T) _bindAndClose(_considerFilter(this._parserFactory.createParser(bArr, i, i2), false));
    }

    public ObjectReader with(Base64Variant base64Variant) {
        return _with(this._config.with(base64Variant));
    }

    public ObjectReader with(ContextAttributes contextAttributes) {
        return _with(this._config.with(contextAttributes));
    }

    public <T> T readValue(File file) throws IOException, JsonProcessingException {
        com.fasterxml.jackson.databind.deser.c cVar = this._dataFormatReaders;
        if (cVar != null) {
            return (T) _detectBindAndClose(cVar.b(_inputStream(file)), true);
        }
        return (T) _bindAndClose(_considerFilter(this._parserFactory.createParser(file), false));
    }

    public <T> T readValue(URL url) throws IOException, JsonProcessingException {
        com.fasterxml.jackson.databind.deser.c cVar = this._dataFormatReaders;
        if (cVar != null) {
            return (T) _detectBindAndClose(cVar.b(_inputStream(url)), true);
        }
        return (T) _bindAndClose(_considerFilter(this._parserFactory.createParser(url), false));
    }

    public <T> h<T> readValues(byte[] bArr, int i, int i2) throws IOException, JsonProcessingException {
        com.fasterxml.jackson.databind.deser.c cVar = this._dataFormatReaders;
        if (cVar != null) {
            return _detectBindAndReadValues(cVar.c(bArr, i, i2), false);
        }
        return _bindAndReadValues(_considerFilter(this._parserFactory.createParser(bArr, i, i2), true));
    }

    public <T> T readValue(d dVar) throws IOException, JsonProcessingException {
        if (this._dataFormatReaders != null) {
            _reportUndetectableSource(dVar);
        }
        return (T) _bindAndClose(_considerFilter(treeAsTokens(dVar), false));
    }

    public final <T> h<T> readValues(byte[] bArr) throws IOException, JsonProcessingException {
        return readValues(bArr, 0, bArr.length);
    }

    public <T> h<T> readValues(File file) throws IOException, JsonProcessingException {
        com.fasterxml.jackson.databind.deser.c cVar = this._dataFormatReaders;
        if (cVar != null) {
            return _detectBindAndReadValues(cVar.b(_inputStream(file)), false);
        }
        return _bindAndReadValues(_considerFilter(this._parserFactory.createParser(file), true));
    }

    public ObjectReader(ObjectReader objectReader, DeserializationConfig deserializationConfig) {
        this._config = deserializationConfig;
        this._context = objectReader._context;
        this._rootDeserializers = objectReader._rootDeserializers;
        this._parserFactory = objectReader._parserFactory;
        this._valueType = objectReader._valueType;
        this._rootDeserializer = objectReader._rootDeserializer;
        this._valueToUpdate = objectReader._valueToUpdate;
        this._schema = objectReader._schema;
        this._unwrapRoot = deserializationConfig.useRootWrapping();
        this._dataFormatReaders = objectReader._dataFormatReaders;
        this._filter = objectReader._filter;
    }

    public <T> T readValue(DataInput dataInput) throws IOException {
        if (this._dataFormatReaders != null) {
            _reportUndetectableSource(dataInput);
        }
        return (T) _bindAndClose(_considerFilter(this._parserFactory.createParser(dataInput), false));
    }

    public <T> h<T> readValues(URL url) throws IOException, JsonProcessingException {
        com.fasterxml.jackson.databind.deser.c cVar = this._dataFormatReaders;
        if (cVar != null) {
            return _detectBindAndReadValues(cVar.b(_inputStream(url)), true);
        }
        return _bindAndReadValues(_considerFilter(this._parserFactory.createParser(url), true));
    }

    public <T> h<T> readValues(DataInput dataInput) throws IOException {
        if (this._dataFormatReaders != null) {
            _reportUndetectableSource(dataInput);
        }
        return _bindAndReadValues(_considerFilter(this._parserFactory.createParser(dataInput), true));
    }

    public ObjectReader(ObjectReader objectReader, JsonFactory jsonFactory) {
        this._config = objectReader._config.with(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, jsonFactory.requiresPropertyOrdering());
        this._context = objectReader._context;
        this._rootDeserializers = objectReader._rootDeserializers;
        this._parserFactory = jsonFactory;
        this._valueType = objectReader._valueType;
        this._rootDeserializer = objectReader._rootDeserializer;
        this._valueToUpdate = objectReader._valueToUpdate;
        this._schema = objectReader._schema;
        this._unwrapRoot = objectReader._unwrapRoot;
        this._dataFormatReaders = objectReader._dataFormatReaders;
        this._filter = objectReader._filter;
    }

    public ObjectReader(ObjectReader objectReader, v64 v64Var) {
        this._config = objectReader._config;
        this._context = objectReader._context;
        this._rootDeserializers = objectReader._rootDeserializers;
        this._parserFactory = objectReader._parserFactory;
        this._valueType = objectReader._valueType;
        this._rootDeserializer = objectReader._rootDeserializer;
        this._valueToUpdate = objectReader._valueToUpdate;
        this._schema = objectReader._schema;
        this._unwrapRoot = objectReader._unwrapRoot;
        this._dataFormatReaders = objectReader._dataFormatReaders;
        this._filter = v64Var;
    }
}
