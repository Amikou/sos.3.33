package com.fasterxml.jackson.databind;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.ObjectIdGenerator;
import com.fasterxml.jackson.annotation.t;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.databind.type.TypeFactory;
import defpackage.o80;
import java.lang.reflect.Type;
import java.util.Locale;
import java.util.TimeZone;

/* compiled from: DatabindContext.java */
/* loaded from: classes.dex */
public abstract class b {
    public abstract boolean canOverrideAccessModifiers();

    public JavaType constructSpecializedType(JavaType javaType, Class<?> cls) {
        return javaType.getRawClass() == cls ? javaType : getConfig().constructSpecializedType(javaType, cls);
    }

    public JavaType constructType(Type type) {
        return getTypeFactory().constructType(type);
    }

    public o80<Object, Object> converterInstance(ue ueVar, Object obj) throws JsonMappingException {
        if (obj == null) {
            return null;
        }
        if (obj instanceof o80) {
            return (o80) obj;
        }
        if (obj instanceof Class) {
            Class cls = (Class) obj;
            if (cls == o80.a.class || com.fasterxml.jackson.databind.util.c.G(cls)) {
                return null;
            }
            if (o80.class.isAssignableFrom(cls)) {
                MapperConfig<?> config = getConfig();
                config.getHandlerInstantiator();
                return (o80) com.fasterxml.jackson.databind.util.c.i(cls, config.canOverrideAccessModifiers());
            }
            throw new IllegalStateException("AnnotationIntrospector returned Class " + cls.getName() + "; expected Class<Converter>");
        }
        throw new IllegalStateException("AnnotationIntrospector returned Converter definition of type " + obj.getClass().getName() + "; expected type Converter or Class<Converter> instead");
    }

    public abstract Class<?> getActiveView();

    public abstract AnnotationIntrospector getAnnotationIntrospector();

    public abstract Object getAttribute(Object obj);

    public abstract MapperConfig<?> getConfig();

    public abstract JsonFormat.Value getDefaultPropertyFormat(Class<?> cls);

    public abstract Locale getLocale();

    public abstract TimeZone getTimeZone();

    public abstract TypeFactory getTypeFactory();

    public abstract boolean isEnabled(MapperFeature mapperFeature);

    public ObjectIdGenerator<?> objectIdGeneratorInstance(ue ueVar, jl2 jl2Var) throws JsonMappingException {
        Class<? extends ObjectIdGenerator<?>> c = jl2Var.c();
        MapperConfig<?> config = getConfig();
        config.getHandlerInstantiator();
        return ((ObjectIdGenerator) com.fasterxml.jackson.databind.util.c.i(c, config.canOverrideAccessModifiers())).forScope(jl2Var.f());
    }

    public t objectIdResolverInstance(ue ueVar, jl2 jl2Var) {
        Class<? extends t> e = jl2Var.e();
        MapperConfig<?> config = getConfig();
        config.getHandlerInstantiator();
        return (t) com.fasterxml.jackson.databind.util.c.i(e, config.canOverrideAccessModifiers());
    }

    public abstract b setAttribute(Object obj, Object obj2);
}
