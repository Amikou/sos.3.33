package com.fasterxml.jackson.databind.jsontype;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.core.JsonGenerator;
import java.io.IOException;

/* compiled from: TypeSerializer.java */
/* loaded from: classes.dex */
public abstract class c {
    public abstract c a(com.fasterxml.jackson.databind.a aVar);

    public abstract String b();

    public abstract JsonTypeInfo.As c();

    public abstract void d(Object obj, JsonGenerator jsonGenerator, String str) throws IOException;

    public abstract void e(Object obj, JsonGenerator jsonGenerator, String str) throws IOException;

    public abstract void f(Object obj, JsonGenerator jsonGenerator, String str) throws IOException;

    public abstract void g(Object obj, JsonGenerator jsonGenerator, String str) throws IOException;

    public abstract void h(Object obj, JsonGenerator jsonGenerator) throws IOException;

    public abstract void i(Object obj, JsonGenerator jsonGenerator) throws IOException;

    public abstract void j(Object obj, JsonGenerator jsonGenerator) throws IOException;

    public void k(Object obj, JsonGenerator jsonGenerator, Class<?> cls) throws IOException {
        j(obj, jsonGenerator);
    }

    public abstract void l(Object obj, JsonGenerator jsonGenerator) throws IOException;

    public abstract void m(Object obj, JsonGenerator jsonGenerator) throws IOException;

    public abstract void n(Object obj, JsonGenerator jsonGenerator) throws IOException;
}
