package com.fasterxml.jackson.databind.jsontype.impl;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import java.io.IOException;

/* loaded from: classes.dex */
public class AsWrapperTypeDeserializer extends TypeDeserializerBase {
    private static final long serialVersionUID = 1;

    public AsWrapperTypeDeserializer(JavaType javaType, com.fasterxml.jackson.databind.jsontype.b bVar, String str, boolean z, JavaType javaType2) {
        super(javaType, bVar, str, z, javaType2);
    }

    public Object _deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        Object g0;
        if (jsonParser.d() && (g0 = jsonParser.g0()) != null) {
            return _deserializeWithNativeTypeId(jsonParser, deserializationContext, g0);
        }
        JsonToken u = jsonParser.u();
        JsonToken jsonToken = JsonToken.START_OBJECT;
        if (u == jsonToken) {
            JsonToken T0 = jsonParser.T0();
            JsonToken jsonToken2 = JsonToken.FIELD_NAME;
            if (T0 != jsonToken2) {
                deserializationContext.reportWrongTokenException(jsonParser, jsonToken2, "need JSON String that contains type id (for subtype of " + baseTypeName() + ")", new Object[0]);
            }
        } else if (u != JsonToken.FIELD_NAME) {
            deserializationContext.reportWrongTokenException(jsonParser, jsonToken, "need JSON Object to contain As.WRAPPER_OBJECT type information for class " + baseTypeName(), new Object[0]);
        }
        String X = jsonParser.X();
        com.fasterxml.jackson.databind.c<Object> _findDeserializer = _findDeserializer(deserializationContext, X);
        jsonParser.T0();
        if (this._typeIdVisible && jsonParser.u() == jsonToken) {
            com.fasterxml.jackson.databind.util.e eVar = new com.fasterxml.jackson.databind.util.e((com.fasterxml.jackson.core.c) null, false);
            eVar.i1();
            eVar.i0(this._typePropertyName);
            eVar.o1(X);
            jsonParser.e();
            jsonParser = com.fasterxml.jackson.core.util.b.o1(false, eVar.L1(jsonParser), jsonParser);
            jsonParser.T0();
        }
        Object deserialize = _findDeserializer.deserialize(jsonParser, deserializationContext);
        JsonToken T02 = jsonParser.T0();
        JsonToken jsonToken3 = JsonToken.END_OBJECT;
        if (T02 != jsonToken3) {
            deserializationContext.reportWrongTokenException(jsonParser, jsonToken3, "expected closing END_OBJECT after type information and deserialized value", new Object[0]);
        }
        return deserialize;
    }

    @Override // com.fasterxml.jackson.databind.jsontype.a
    public Object deserializeTypedFromAny(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        return _deserialize(jsonParser, deserializationContext);
    }

    @Override // com.fasterxml.jackson.databind.jsontype.a
    public Object deserializeTypedFromArray(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        return _deserialize(jsonParser, deserializationContext);
    }

    @Override // com.fasterxml.jackson.databind.jsontype.a
    public Object deserializeTypedFromObject(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        return _deserialize(jsonParser, deserializationContext);
    }

    @Override // com.fasterxml.jackson.databind.jsontype.a
    public Object deserializeTypedFromScalar(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        return _deserialize(jsonParser, deserializationContext);
    }

    @Override // com.fasterxml.jackson.databind.jsontype.impl.TypeDeserializerBase, com.fasterxml.jackson.databind.jsontype.a
    public com.fasterxml.jackson.databind.jsontype.a forProperty(com.fasterxml.jackson.databind.a aVar) {
        return aVar == this._property ? this : new AsWrapperTypeDeserializer(this, aVar);
    }

    @Override // com.fasterxml.jackson.databind.jsontype.impl.TypeDeserializerBase, com.fasterxml.jackson.databind.jsontype.a
    public JsonTypeInfo.As getTypeInclusion() {
        return JsonTypeInfo.As.WRAPPER_OBJECT;
    }

    public AsWrapperTypeDeserializer(AsWrapperTypeDeserializer asWrapperTypeDeserializer, com.fasterxml.jackson.databind.a aVar) {
        super(asWrapperTypeDeserializer, aVar);
    }
}
