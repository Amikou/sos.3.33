package com.fasterxml.jackson.databind.jsontype.impl;

import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.fasterxml.jackson.databind.jsontype.NamedType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* loaded from: classes.dex */
public class StdSubtypeResolver extends aw3 implements Serializable {
    private static final long serialVersionUID = 1;
    public LinkedHashSet<NamedType> _registeredSubtypes;

    public void _collectAndResolve(com.fasterxml.jackson.databind.introspect.a aVar, NamedType namedType, MapperConfig<?> mapperConfig, AnnotationIntrospector annotationIntrospector, HashMap<NamedType, NamedType> hashMap) {
        String findTypeName;
        if (!namedType.hasName() && (findTypeName = annotationIntrospector.findTypeName(aVar)) != null) {
            namedType = new NamedType(namedType.getType(), findTypeName);
        }
        if (hashMap.containsKey(namedType)) {
            if (!namedType.hasName() || hashMap.get(namedType).hasName()) {
                return;
            }
            hashMap.put(namedType, namedType);
            return;
        }
        hashMap.put(namedType, namedType);
        List<NamedType> findSubtypes = annotationIntrospector.findSubtypes(aVar);
        if (findSubtypes == null || findSubtypes.isEmpty()) {
            return;
        }
        for (NamedType namedType2 : findSubtypes) {
            _collectAndResolve(com.fasterxml.jackson.databind.introspect.a.J(namedType2.getType(), mapperConfig), namedType2, mapperConfig, annotationIntrospector, hashMap);
        }
    }

    public void _collectAndResolveByTypeId(com.fasterxml.jackson.databind.introspect.a aVar, NamedType namedType, MapperConfig<?> mapperConfig, Set<Class<?>> set, Map<String, NamedType> map) {
        List<NamedType> findSubtypes;
        String findTypeName;
        AnnotationIntrospector annotationIntrospector = mapperConfig.getAnnotationIntrospector();
        if (!namedType.hasName() && (findTypeName = annotationIntrospector.findTypeName(aVar)) != null) {
            namedType = new NamedType(namedType.getType(), findTypeName);
        }
        if (namedType.hasName()) {
            map.put(namedType.getName(), namedType);
        }
        if (!set.add(namedType.getType()) || (findSubtypes = annotationIntrospector.findSubtypes(aVar)) == null || findSubtypes.isEmpty()) {
            return;
        }
        for (NamedType namedType2 : findSubtypes) {
            _collectAndResolveByTypeId(com.fasterxml.jackson.databind.introspect.a.J(namedType2.getType(), mapperConfig), namedType2, mapperConfig, set, map);
        }
    }

    public Collection<NamedType> _combineNamedAndUnnamed(Set<Class<?>> set, Map<String, NamedType> map) {
        ArrayList arrayList = new ArrayList(map.values());
        for (NamedType namedType : map.values()) {
            set.remove(namedType.getType());
        }
        for (Class<?> cls : set) {
            arrayList.add(new NamedType(cls));
        }
        return arrayList;
    }

    @Override // defpackage.aw3
    public Collection<NamedType> collectAndResolveSubtypesByClass(MapperConfig<?> mapperConfig, AnnotatedMember annotatedMember, JavaType javaType) {
        AnnotationIntrospector annotationIntrospector = mapperConfig.getAnnotationIntrospector();
        Class<?> rawType = javaType == null ? annotatedMember.getRawType() : javaType.getRawClass();
        HashMap<NamedType, NamedType> hashMap = new HashMap<>();
        LinkedHashSet<NamedType> linkedHashSet = this._registeredSubtypes;
        if (linkedHashSet != null) {
            Iterator<NamedType> it = linkedHashSet.iterator();
            while (it.hasNext()) {
                NamedType next = it.next();
                if (rawType.isAssignableFrom(next.getType())) {
                    _collectAndResolve(com.fasterxml.jackson.databind.introspect.a.J(next.getType(), mapperConfig), next, mapperConfig, annotationIntrospector, hashMap);
                }
            }
        }
        List<NamedType> findSubtypes = annotationIntrospector.findSubtypes(annotatedMember);
        if (findSubtypes != null) {
            for (NamedType namedType : findSubtypes) {
                _collectAndResolve(com.fasterxml.jackson.databind.introspect.a.J(namedType.getType(), mapperConfig), namedType, mapperConfig, annotationIntrospector, hashMap);
            }
        }
        _collectAndResolve(com.fasterxml.jackson.databind.introspect.a.J(rawType, mapperConfig), new NamedType(rawType, null), mapperConfig, annotationIntrospector, hashMap);
        return new ArrayList(hashMap.values());
    }

    @Override // defpackage.aw3
    public Collection<NamedType> collectAndResolveSubtypesByTypeId(MapperConfig<?> mapperConfig, AnnotatedMember annotatedMember, JavaType javaType) {
        AnnotationIntrospector annotationIntrospector = mapperConfig.getAnnotationIntrospector();
        Class<?> rawType = javaType == null ? annotatedMember.getRawType() : javaType.getRawClass();
        HashSet hashSet = new HashSet();
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        _collectAndResolveByTypeId(com.fasterxml.jackson.databind.introspect.a.J(rawType, mapperConfig), new NamedType(rawType, null), mapperConfig, hashSet, linkedHashMap);
        List<NamedType> findSubtypes = annotationIntrospector.findSubtypes(annotatedMember);
        if (findSubtypes != null) {
            for (NamedType namedType : findSubtypes) {
                _collectAndResolveByTypeId(com.fasterxml.jackson.databind.introspect.a.J(namedType.getType(), mapperConfig), namedType, mapperConfig, hashSet, linkedHashMap);
            }
        }
        LinkedHashSet<NamedType> linkedHashSet = this._registeredSubtypes;
        if (linkedHashSet != null) {
            Iterator<NamedType> it = linkedHashSet.iterator();
            while (it.hasNext()) {
                NamedType next = it.next();
                if (rawType.isAssignableFrom(next.getType())) {
                    _collectAndResolveByTypeId(com.fasterxml.jackson.databind.introspect.a.J(next.getType(), mapperConfig), next, mapperConfig, hashSet, linkedHashMap);
                }
            }
        }
        return _combineNamedAndUnnamed(hashSet, linkedHashMap);
    }

    @Override // defpackage.aw3
    public void registerSubtypes(NamedType... namedTypeArr) {
        if (this._registeredSubtypes == null) {
            this._registeredSubtypes = new LinkedHashSet<>();
        }
        for (NamedType namedType : namedTypeArr) {
            this._registeredSubtypes.add(namedType);
        }
    }

    @Override // defpackage.aw3
    public void registerSubtypes(Class<?>... clsArr) {
        NamedType[] namedTypeArr = new NamedType[clsArr.length];
        int length = clsArr.length;
        for (int i = 0; i < length; i++) {
            namedTypeArr[i] = new NamedType(clsArr[i]);
        }
        registerSubtypes(namedTypeArr);
    }

    @Override // defpackage.aw3
    public Collection<NamedType> collectAndResolveSubtypesByClass(MapperConfig<?> mapperConfig, com.fasterxml.jackson.databind.introspect.a aVar) {
        AnnotationIntrospector annotationIntrospector = mapperConfig.getAnnotationIntrospector();
        HashMap<NamedType, NamedType> hashMap = new HashMap<>();
        if (this._registeredSubtypes != null) {
            Class<?> rawType = aVar.getRawType();
            Iterator<NamedType> it = this._registeredSubtypes.iterator();
            while (it.hasNext()) {
                NamedType next = it.next();
                if (rawType.isAssignableFrom(next.getType())) {
                    _collectAndResolve(com.fasterxml.jackson.databind.introspect.a.J(next.getType(), mapperConfig), next, mapperConfig, annotationIntrospector, hashMap);
                }
            }
        }
        _collectAndResolve(aVar, new NamedType(aVar.getRawType(), null), mapperConfig, annotationIntrospector, hashMap);
        return new ArrayList(hashMap.values());
    }

    @Override // defpackage.aw3
    public Collection<NamedType> collectAndResolveSubtypesByTypeId(MapperConfig<?> mapperConfig, com.fasterxml.jackson.databind.introspect.a aVar) {
        HashSet hashSet = new HashSet();
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        _collectAndResolveByTypeId(aVar, new NamedType(aVar.getRawType(), null), mapperConfig, hashSet, linkedHashMap);
        if (this._registeredSubtypes != null) {
            Class<?> rawType = aVar.getRawType();
            Iterator<NamedType> it = this._registeredSubtypes.iterator();
            while (it.hasNext()) {
                NamedType next = it.next();
                if (rawType.isAssignableFrom(next.getType())) {
                    _collectAndResolveByTypeId(com.fasterxml.jackson.databind.introspect.a.J(next.getType(), mapperConfig), next, mapperConfig, hashSet, linkedHashMap);
                }
            }
        }
        return _combineNamedAndUnnamed(hashSet, linkedHashMap);
    }
}
