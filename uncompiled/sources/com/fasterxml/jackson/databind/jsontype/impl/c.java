package com.fasterxml.jackson.databind.jsontype.impl;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.core.JsonGenerator;
import java.io.IOException;

/* compiled from: AsExternalTypeSerializer.java */
/* loaded from: classes.dex */
public class c extends wd4 {
    public final String c;

    public c(com.fasterxml.jackson.databind.jsontype.b bVar, com.fasterxml.jackson.databind.a aVar, String str) {
        super(bVar, aVar);
        this.c = str;
    }

    @Override // defpackage.wd4, com.fasterxml.jackson.databind.jsontype.c
    public String b() {
        return this.c;
    }

    @Override // com.fasterxml.jackson.databind.jsontype.c
    public JsonTypeInfo.As c() {
        return JsonTypeInfo.As.EXTERNAL_PROPERTY;
    }

    @Override // com.fasterxml.jackson.databind.jsontype.c
    public void d(Object obj, JsonGenerator jsonGenerator, String str) throws IOException {
        r(obj, jsonGenerator);
    }

    @Override // com.fasterxml.jackson.databind.jsontype.c
    public void e(Object obj, JsonGenerator jsonGenerator, String str) throws IOException {
        t(obj, jsonGenerator);
    }

    @Override // com.fasterxml.jackson.databind.jsontype.c
    public void f(Object obj, JsonGenerator jsonGenerator, String str) throws IOException {
        s(obj, jsonGenerator, str);
    }

    @Override // com.fasterxml.jackson.databind.jsontype.c
    public void g(Object obj, JsonGenerator jsonGenerator, String str) throws IOException {
        u(obj, jsonGenerator, str);
    }

    @Override // com.fasterxml.jackson.databind.jsontype.c
    public void h(Object obj, JsonGenerator jsonGenerator) throws IOException {
        r(obj, jsonGenerator);
    }

    @Override // com.fasterxml.jackson.databind.jsontype.c
    public void i(Object obj, JsonGenerator jsonGenerator) throws IOException {
        t(obj, jsonGenerator);
    }

    @Override // com.fasterxml.jackson.databind.jsontype.c
    public void j(Object obj, JsonGenerator jsonGenerator) throws IOException {
        v(obj, jsonGenerator);
    }

    @Override // com.fasterxml.jackson.databind.jsontype.c
    public void k(Object obj, JsonGenerator jsonGenerator, Class<?> cls) throws IOException {
        v(obj, jsonGenerator);
    }

    @Override // com.fasterxml.jackson.databind.jsontype.c
    public void l(Object obj, JsonGenerator jsonGenerator) throws IOException {
        s(obj, jsonGenerator, p(obj));
    }

    @Override // com.fasterxml.jackson.databind.jsontype.c
    public void m(Object obj, JsonGenerator jsonGenerator) throws IOException {
        u(obj, jsonGenerator, p(obj));
    }

    @Override // com.fasterxml.jackson.databind.jsontype.c
    public void n(Object obj, JsonGenerator jsonGenerator) throws IOException {
        w(obj, jsonGenerator, p(obj));
    }

    public final void r(Object obj, JsonGenerator jsonGenerator) throws IOException {
        jsonGenerator.e1();
    }

    public final void s(Object obj, JsonGenerator jsonGenerator, String str) throws IOException {
        jsonGenerator.e0();
        if (str != null) {
            jsonGenerator.r1(this.c, str);
        }
    }

    public final void t(Object obj, JsonGenerator jsonGenerator) throws IOException {
        jsonGenerator.i1();
    }

    public final void u(Object obj, JsonGenerator jsonGenerator, String str) throws IOException {
        jsonGenerator.f0();
        if (str != null) {
            jsonGenerator.r1(this.c, str);
        }
    }

    public final void v(Object obj, JsonGenerator jsonGenerator) throws IOException {
    }

    public final void w(Object obj, JsonGenerator jsonGenerator, String str) throws IOException {
        if (str != null) {
            jsonGenerator.r1(this.c, str);
        }
    }

    @Override // com.fasterxml.jackson.databind.jsontype.c
    /* renamed from: x */
    public c a(com.fasterxml.jackson.databind.a aVar) {
        return this.b == aVar ? this : new c(this.a, aVar, this.c);
    }
}
