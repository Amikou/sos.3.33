package com.fasterxml.jackson.databind.jsontype.impl;

import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;

/* compiled from: ClassNameIdResolver.java */
/* loaded from: classes.dex */
public class f extends pd4 {
    public f(JavaType javaType, TypeFactory typeFactory) {
        super(javaType, typeFactory);
    }

    public static boolean i(String str, String str2) {
        return str.startsWith("Collections$") ? str.indexOf(str2) > 0 && !str.contains("Unmodifiable") : str.startsWith("Arrays$") && str.indexOf(str2) > 0;
    }

    @Override // com.fasterxml.jackson.databind.jsontype.b
    public String a(Object obj) {
        return g(obj, obj.getClass(), this.a);
    }

    @Override // com.fasterxml.jackson.databind.jsontype.b
    public String b() {
        return "class name used as type id";
    }

    @Override // com.fasterxml.jackson.databind.jsontype.b
    public JavaType d(com.fasterxml.jackson.databind.b bVar, String str) throws IOException {
        return h(str, bVar);
    }

    @Override // com.fasterxml.jackson.databind.jsontype.b
    public String e(Object obj, Class<?> cls) {
        return g(obj, cls, this.a);
    }

    public String g(Object obj, Class<?> cls, TypeFactory typeFactory) {
        String name;
        if (Enum.class.isAssignableFrom(cls) && !cls.isEnum()) {
            cls = cls.getSuperclass();
        }
        String name2 = cls.getName();
        if (!name2.startsWith("java.util.")) {
            return (name2.indexOf(36) < 0 || com.fasterxml.jackson.databind.util.c.C(cls) == null || com.fasterxml.jackson.databind.util.c.C(this.b.getRawClass()) != null) ? name2 : this.b.getRawClass().getName();
        } else if (obj instanceof EnumSet) {
            return typeFactory.constructCollectionType(EnumSet.class, com.fasterxml.jackson.databind.util.c.q((EnumSet) obj)).toCanonical();
        } else {
            if (obj instanceof EnumMap) {
                return typeFactory.constructMapType(EnumMap.class, com.fasterxml.jackson.databind.util.c.p((EnumMap) obj), Object.class).toCanonical();
            }
            String substring = name2.substring(10);
            if (i(substring, "List")) {
                name = ArrayList.class.getName();
            } else if (i(substring, "Map")) {
                name = HashMap.class.getName();
            } else if (!i(substring, "Set")) {
                return name2;
            } else {
                name = HashSet.class.getName();
            }
            return name;
        }
    }

    public JavaType h(String str, com.fasterxml.jackson.databind.b bVar) throws IOException {
        TypeFactory typeFactory = bVar.getTypeFactory();
        if (str.indexOf(60) > 0) {
            JavaType constructFromCanonical = typeFactory.constructFromCanonical(str);
            if (constructFromCanonical.isTypeOrSubTypeOf(this.b.getRawClass())) {
                return constructFromCanonical;
            }
            throw new IllegalArgumentException(String.format("Class %s not subtype of %s", constructFromCanonical.getRawClass().getName(), this.b));
        }
        try {
            return typeFactory.constructSpecializedType(this.b, typeFactory.findClass(str));
        } catch (ClassNotFoundException unused) {
            if (bVar instanceof DeserializationContext) {
                return ((DeserializationContext) bVar).handleUnknownTypeId(this.b, str, this, "no such class found");
            }
            return null;
        } catch (Exception e) {
            throw new IllegalArgumentException("Invalid type id '" + str + "' (for id type 'Id.class'): " + e.getMessage(), e);
        }
    }
}
