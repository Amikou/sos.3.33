package com.fasterxml.jackson.databind.jsontype.impl;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.core.JsonGenerator;
import java.io.IOException;

/* compiled from: AsArrayTypeSerializer.java */
/* loaded from: classes.dex */
public class a extends wd4 {
    public a(com.fasterxml.jackson.databind.jsontype.b bVar, com.fasterxml.jackson.databind.a aVar) {
        super(bVar, aVar);
    }

    @Override // com.fasterxml.jackson.databind.jsontype.c
    public JsonTypeInfo.As c() {
        return JsonTypeInfo.As.WRAPPER_ARRAY;
    }

    @Override // com.fasterxml.jackson.databind.jsontype.c
    public void d(Object obj, JsonGenerator jsonGenerator, String str) throws IOException {
        if (!jsonGenerator.h()) {
            jsonGenerator.e1();
            jsonGenerator.o1(str);
        } else if (str != null) {
            jsonGenerator.s1(str);
        }
        jsonGenerator.e1();
    }

    @Override // com.fasterxml.jackson.databind.jsontype.c
    public void e(Object obj, JsonGenerator jsonGenerator, String str) throws IOException {
        if (!jsonGenerator.h()) {
            jsonGenerator.e1();
            jsonGenerator.o1(str);
        } else if (str != null) {
            jsonGenerator.s1(str);
        }
        jsonGenerator.i1();
    }

    @Override // com.fasterxml.jackson.databind.jsontype.c
    public void f(Object obj, JsonGenerator jsonGenerator, String str) throws IOException {
        if (jsonGenerator.h()) {
            return;
        }
        l(obj, jsonGenerator);
    }

    @Override // com.fasterxml.jackson.databind.jsontype.c
    public void g(Object obj, JsonGenerator jsonGenerator, String str) throws IOException {
        if (jsonGenerator.h()) {
            return;
        }
        m(obj, jsonGenerator);
    }

    @Override // com.fasterxml.jackson.databind.jsontype.c
    public void h(Object obj, JsonGenerator jsonGenerator) throws IOException {
        String p = p(obj);
        if (!jsonGenerator.h()) {
            jsonGenerator.e1();
            jsonGenerator.o1(p);
        } else if (p != null) {
            jsonGenerator.s1(p);
        }
        jsonGenerator.e1();
    }

    @Override // com.fasterxml.jackson.databind.jsontype.c
    public void i(Object obj, JsonGenerator jsonGenerator) throws IOException {
        String p = p(obj);
        if (!jsonGenerator.h()) {
            jsonGenerator.e1();
            jsonGenerator.o1(p);
        } else if (p != null) {
            jsonGenerator.s1(p);
        }
        jsonGenerator.i1();
    }

    @Override // com.fasterxml.jackson.databind.jsontype.c
    public void j(Object obj, JsonGenerator jsonGenerator) throws IOException {
        String p = p(obj);
        if (!jsonGenerator.h()) {
            jsonGenerator.e1();
            jsonGenerator.o1(p);
        } else if (p != null) {
            jsonGenerator.s1(p);
        }
    }

    @Override // com.fasterxml.jackson.databind.jsontype.c
    public void k(Object obj, JsonGenerator jsonGenerator, Class<?> cls) throws IOException {
        String q = q(obj, cls);
        if (!jsonGenerator.h()) {
            jsonGenerator.e1();
            jsonGenerator.o1(q);
        } else if (q != null) {
            jsonGenerator.s1(q);
        }
    }

    @Override // com.fasterxml.jackson.databind.jsontype.c
    public void l(Object obj, JsonGenerator jsonGenerator) throws IOException {
        jsonGenerator.e0();
        if (jsonGenerator.h()) {
            return;
        }
        jsonGenerator.e0();
    }

    @Override // com.fasterxml.jackson.databind.jsontype.c
    public void m(Object obj, JsonGenerator jsonGenerator) throws IOException {
        jsonGenerator.f0();
        if (jsonGenerator.h()) {
            return;
        }
        jsonGenerator.e0();
    }

    @Override // com.fasterxml.jackson.databind.jsontype.c
    public void n(Object obj, JsonGenerator jsonGenerator) throws IOException {
        if (jsonGenerator.h()) {
            return;
        }
        jsonGenerator.e0();
    }

    @Override // com.fasterxml.jackson.databind.jsontype.c
    /* renamed from: r */
    public a a(com.fasterxml.jackson.databind.a aVar) {
        return this.b == aVar ? this : new a(this.a, aVar);
    }
}
