package com.fasterxml.jackson.databind.jsontype.impl;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.core.JsonGenerator;
import java.io.IOException;

/* compiled from: AsPropertyTypeSerializer.java */
/* loaded from: classes.dex */
public class d extends a {
    public final String c;

    public d(com.fasterxml.jackson.databind.jsontype.b bVar, com.fasterxml.jackson.databind.a aVar, String str) {
        super(bVar, aVar);
        this.c = str;
    }

    @Override // defpackage.wd4, com.fasterxml.jackson.databind.jsontype.c
    public String b() {
        return this.c;
    }

    @Override // com.fasterxml.jackson.databind.jsontype.impl.a, com.fasterxml.jackson.databind.jsontype.c
    public JsonTypeInfo.As c() {
        return JsonTypeInfo.As.PROPERTY;
    }

    @Override // com.fasterxml.jackson.databind.jsontype.impl.a, com.fasterxml.jackson.databind.jsontype.c
    public void e(Object obj, JsonGenerator jsonGenerator, String str) throws IOException {
        if (str == null) {
            jsonGenerator.i1();
        } else if (jsonGenerator.h()) {
            jsonGenerator.s1(str);
            jsonGenerator.i1();
        } else {
            jsonGenerator.i1();
            jsonGenerator.r1(this.c, str);
        }
    }

    @Override // com.fasterxml.jackson.databind.jsontype.impl.a, com.fasterxml.jackson.databind.jsontype.c
    public void g(Object obj, JsonGenerator jsonGenerator, String str) throws IOException {
        jsonGenerator.f0();
    }

    @Override // com.fasterxml.jackson.databind.jsontype.impl.a, com.fasterxml.jackson.databind.jsontype.c
    public void i(Object obj, JsonGenerator jsonGenerator) throws IOException {
        String p = p(obj);
        if (p == null) {
            jsonGenerator.i1();
        } else if (jsonGenerator.h()) {
            jsonGenerator.s1(p);
            jsonGenerator.i1();
        } else {
            jsonGenerator.i1();
            jsonGenerator.r1(this.c, p);
        }
    }

    @Override // com.fasterxml.jackson.databind.jsontype.impl.a, com.fasterxml.jackson.databind.jsontype.c
    public void m(Object obj, JsonGenerator jsonGenerator) throws IOException {
        jsonGenerator.f0();
    }

    @Override // com.fasterxml.jackson.databind.jsontype.impl.a
    /* renamed from: s */
    public d r(com.fasterxml.jackson.databind.a aVar) {
        return this.b == aVar ? this : new d(this.a, aVar, this.c);
    }
}
