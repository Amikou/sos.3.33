package com.fasterxml.jackson.databind.jsontype.impl;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import java.io.IOException;

/* compiled from: MinimalClassNameIdResolver.java */
/* loaded from: classes.dex */
public class g extends f {
    public final String c;
    public final String d;

    public g(JavaType javaType, TypeFactory typeFactory) {
        super(javaType, typeFactory);
        String name = javaType.getRawClass().getName();
        int lastIndexOf = name.lastIndexOf(46);
        if (lastIndexOf < 0) {
            this.c = "";
            this.d = ".";
            return;
        }
        this.d = name.substring(0, lastIndexOf + 1);
        this.c = name.substring(0, lastIndexOf);
    }

    @Override // com.fasterxml.jackson.databind.jsontype.impl.f, com.fasterxml.jackson.databind.jsontype.b
    public String a(Object obj) {
        String name = obj.getClass().getName();
        return name.startsWith(this.d) ? name.substring(this.d.length() - 1) : name;
    }

    @Override // com.fasterxml.jackson.databind.jsontype.impl.f
    public JavaType h(String str, com.fasterxml.jackson.databind.b bVar) throws IOException {
        if (str.startsWith(".")) {
            StringBuilder sb = new StringBuilder(str.length() + this.c.length());
            if (this.c.length() == 0) {
                sb.append(str.substring(1));
            } else {
                sb.append(this.c);
                sb.append(str);
            }
            str = sb.toString();
        }
        return super.h(str, bVar);
    }
}
