package com.fasterxml.jackson.databind.jsontype.impl;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.deser.std.NullifyingDeserializer;
import java.io.IOException;
import java.io.Serializable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* loaded from: classes.dex */
public abstract class TypeDeserializerBase extends com.fasterxml.jackson.databind.jsontype.a implements Serializable {
    private static final long serialVersionUID = 1;
    public final JavaType _baseType;
    public final JavaType _defaultImpl;
    public com.fasterxml.jackson.databind.c<Object> _defaultImplDeserializer;
    public final Map<String, com.fasterxml.jackson.databind.c<Object>> _deserializers;
    public final com.fasterxml.jackson.databind.jsontype.b _idResolver;
    public final com.fasterxml.jackson.databind.a _property;
    public final boolean _typeIdVisible;
    public final String _typePropertyName;

    public TypeDeserializerBase(JavaType javaType, com.fasterxml.jackson.databind.jsontype.b bVar, String str, boolean z, JavaType javaType2) {
        this._baseType = javaType;
        this._idResolver = bVar;
        this._typePropertyName = str == null ? "" : str;
        this._typeIdVisible = z;
        this._deserializers = new ConcurrentHashMap(16, 0.75f, 2);
        this._defaultImpl = javaType2;
        this._property = null;
    }

    @Deprecated
    public Object _deserializeWithNativeTypeId(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        return _deserializeWithNativeTypeId(jsonParser, deserializationContext, jsonParser.g0());
    }

    public final com.fasterxml.jackson.databind.c<Object> _findDefaultImplDeserializer(DeserializationContext deserializationContext) throws IOException {
        com.fasterxml.jackson.databind.c<Object> cVar;
        JavaType javaType = this._defaultImpl;
        if (javaType == null) {
            if (deserializationContext.isEnabled(DeserializationFeature.FAIL_ON_INVALID_SUBTYPE)) {
                return null;
            }
            return NullifyingDeserializer.instance;
        } else if (com.fasterxml.jackson.databind.util.c.G(javaType.getRawClass())) {
            return NullifyingDeserializer.instance;
        } else {
            synchronized (this._defaultImpl) {
                if (this._defaultImplDeserializer == null) {
                    this._defaultImplDeserializer = deserializationContext.findContextualValueDeserializer(this._defaultImpl, this._property);
                }
                cVar = this._defaultImplDeserializer;
            }
            return cVar;
        }
    }

    public final com.fasterxml.jackson.databind.c<Object> _findDeserializer(DeserializationContext deserializationContext, String str) throws IOException {
        com.fasterxml.jackson.databind.c<Object> findContextualValueDeserializer;
        com.fasterxml.jackson.databind.c<Object> cVar = this._deserializers.get(str);
        if (cVar == null) {
            JavaType d = this._idResolver.d(deserializationContext, str);
            if (d == null) {
                cVar = _findDefaultImplDeserializer(deserializationContext);
                if (cVar == null) {
                    JavaType _handleUnknownTypeId = _handleUnknownTypeId(deserializationContext, str, this._idResolver, this._baseType);
                    if (_handleUnknownTypeId == null) {
                        return null;
                    }
                    findContextualValueDeserializer = deserializationContext.findContextualValueDeserializer(_handleUnknownTypeId, this._property);
                }
                this._deserializers.put(str, cVar);
            } else {
                JavaType javaType = this._baseType;
                if (javaType != null && javaType.getClass() == d.getClass() && !d.hasGenericTypes()) {
                    d = deserializationContext.getTypeFactory().constructSpecializedType(this._baseType, d.getRawClass());
                }
                findContextualValueDeserializer = deserializationContext.findContextualValueDeserializer(d, this._property);
            }
            cVar = findContextualValueDeserializer;
            this._deserializers.put(str, cVar);
        }
        return cVar;
    }

    public JavaType _handleUnknownTypeId(DeserializationContext deserializationContext, String str, com.fasterxml.jackson.databind.jsontype.b bVar, JavaType javaType) throws IOException {
        String str2;
        String b = bVar.b();
        if (b == null) {
            str2 = "known type ids are not statically known";
        } else {
            str2 = "known type ids = " + b;
        }
        return deserializationContext.handleUnknownTypeId(this._baseType, str, bVar, str2);
    }

    public String baseTypeName() {
        return this._baseType.getRawClass().getName();
    }

    @Override // com.fasterxml.jackson.databind.jsontype.a
    public abstract com.fasterxml.jackson.databind.jsontype.a forProperty(com.fasterxml.jackson.databind.a aVar);

    @Override // com.fasterxml.jackson.databind.jsontype.a
    public Class<?> getDefaultImpl() {
        JavaType javaType = this._defaultImpl;
        if (javaType == null) {
            return null;
        }
        return javaType.getRawClass();
    }

    @Override // com.fasterxml.jackson.databind.jsontype.a
    public final String getPropertyName() {
        return this._typePropertyName;
    }

    @Override // com.fasterxml.jackson.databind.jsontype.a
    public com.fasterxml.jackson.databind.jsontype.b getTypeIdResolver() {
        return this._idResolver;
    }

    @Override // com.fasterxml.jackson.databind.jsontype.a
    public abstract JsonTypeInfo.As getTypeInclusion();

    public String toString() {
        return '[' + getClass().getName() + "; base-type:" + this._baseType + "; id-resolver: " + this._idResolver + ']';
    }

    public Object _deserializeWithNativeTypeId(JsonParser jsonParser, DeserializationContext deserializationContext, Object obj) throws IOException {
        com.fasterxml.jackson.databind.c<Object> _findDeserializer;
        if (obj == null) {
            _findDeserializer = _findDefaultImplDeserializer(deserializationContext);
            if (_findDeserializer == null) {
                deserializationContext.reportMappingException("No (native) type id found when one was expected for polymorphic type handling", new Object[0]);
                return null;
            }
        } else {
            _findDeserializer = _findDeserializer(deserializationContext, obj instanceof String ? (String) obj : String.valueOf(obj));
        }
        return _findDeserializer.deserialize(jsonParser, deserializationContext);
    }

    public TypeDeserializerBase(TypeDeserializerBase typeDeserializerBase, com.fasterxml.jackson.databind.a aVar) {
        this._baseType = typeDeserializerBase._baseType;
        this._idResolver = typeDeserializerBase._idResolver;
        this._typePropertyName = typeDeserializerBase._typePropertyName;
        this._typeIdVisible = typeDeserializerBase._typeIdVisible;
        this._deserializers = typeDeserializerBase._deserializers;
        this._defaultImpl = typeDeserializerBase._defaultImpl;
        this._defaultImplDeserializer = typeDeserializerBase._defaultImplDeserializer;
        this._property = aVar;
    }
}
