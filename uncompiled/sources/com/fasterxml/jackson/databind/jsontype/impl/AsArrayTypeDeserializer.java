package com.fasterxml.jackson.databind.jsontype.impl;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import java.io.IOException;

/* loaded from: classes.dex */
public class AsArrayTypeDeserializer extends TypeDeserializerBase {
    private static final long serialVersionUID = 1;

    public AsArrayTypeDeserializer(JavaType javaType, com.fasterxml.jackson.databind.jsontype.b bVar, String str, boolean z, JavaType javaType2) {
        super(javaType, bVar, str, z, javaType2);
    }

    public Object _deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        Object g0;
        if (jsonParser.d() && (g0 = jsonParser.g0()) != null) {
            return _deserializeWithNativeTypeId(jsonParser, deserializationContext, g0);
        }
        boolean K0 = jsonParser.K0();
        String _locateTypeId = _locateTypeId(jsonParser, deserializationContext);
        com.fasterxml.jackson.databind.c<Object> _findDeserializer = _findDeserializer(deserializationContext, _locateTypeId);
        if (this._typeIdVisible && !_usesExternalId() && jsonParser.u() == JsonToken.START_OBJECT) {
            com.fasterxml.jackson.databind.util.e eVar = new com.fasterxml.jackson.databind.util.e((com.fasterxml.jackson.core.c) null, false);
            eVar.i1();
            eVar.i0(this._typePropertyName);
            eVar.o1(_locateTypeId);
            jsonParser.e();
            jsonParser = com.fasterxml.jackson.core.util.b.o1(false, eVar.L1(jsonParser), jsonParser);
            jsonParser.T0();
        }
        Object deserialize = _findDeserializer.deserialize(jsonParser, deserializationContext);
        if (K0) {
            JsonToken T0 = jsonParser.T0();
            JsonToken jsonToken = JsonToken.END_ARRAY;
            if (T0 != jsonToken) {
                deserializationContext.reportWrongTokenException(jsonParser, jsonToken, "expected closing END_ARRAY after type information and deserialized value", new Object[0]);
            }
        }
        return deserialize;
    }

    public String _locateTypeId(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        if (!jsonParser.K0()) {
            if (this._defaultImpl != null) {
                return this._idResolver.f();
            }
            JsonToken jsonToken = JsonToken.START_ARRAY;
            deserializationContext.reportWrongTokenException(jsonParser, jsonToken, "need JSON Array to contain As.WRAPPER_ARRAY type information for class " + baseTypeName(), new Object[0]);
            return null;
        }
        JsonToken T0 = jsonParser.T0();
        JsonToken jsonToken2 = JsonToken.VALUE_STRING;
        if (T0 == jsonToken2) {
            String X = jsonParser.X();
            jsonParser.T0();
            return X;
        } else if (this._defaultImpl != null) {
            return this._idResolver.f();
        } else {
            deserializationContext.reportWrongTokenException(jsonParser, jsonToken2, "need JSON String that contains type id (for subtype of " + baseTypeName() + ")", new Object[0]);
            return null;
        }
    }

    public boolean _usesExternalId() {
        return false;
    }

    @Override // com.fasterxml.jackson.databind.jsontype.a
    public Object deserializeTypedFromAny(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        return _deserialize(jsonParser, deserializationContext);
    }

    @Override // com.fasterxml.jackson.databind.jsontype.a
    public Object deserializeTypedFromArray(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        return _deserialize(jsonParser, deserializationContext);
    }

    @Override // com.fasterxml.jackson.databind.jsontype.a
    public Object deserializeTypedFromObject(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        return _deserialize(jsonParser, deserializationContext);
    }

    @Override // com.fasterxml.jackson.databind.jsontype.a
    public Object deserializeTypedFromScalar(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        return _deserialize(jsonParser, deserializationContext);
    }

    @Override // com.fasterxml.jackson.databind.jsontype.impl.TypeDeserializerBase, com.fasterxml.jackson.databind.jsontype.a
    public com.fasterxml.jackson.databind.jsontype.a forProperty(com.fasterxml.jackson.databind.a aVar) {
        return aVar == this._property ? this : new AsArrayTypeDeserializer(this, aVar);
    }

    @Override // com.fasterxml.jackson.databind.jsontype.impl.TypeDeserializerBase, com.fasterxml.jackson.databind.jsontype.a
    public JsonTypeInfo.As getTypeInclusion() {
        return JsonTypeInfo.As.WRAPPER_ARRAY;
    }

    public AsArrayTypeDeserializer(AsArrayTypeDeserializer asArrayTypeDeserializer, com.fasterxml.jackson.databind.a aVar) {
        super(asArrayTypeDeserializer, aVar);
    }
}
