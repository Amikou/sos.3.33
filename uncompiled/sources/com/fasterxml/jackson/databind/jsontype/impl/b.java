package com.fasterxml.jackson.databind.jsontype.impl;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.core.JsonGenerator;
import java.io.IOException;

/* compiled from: AsExistingPropertyTypeSerializer.java */
/* loaded from: classes.dex */
public class b extends d {
    public b(com.fasterxml.jackson.databind.jsontype.b bVar, com.fasterxml.jackson.databind.a aVar, String str) {
        super(bVar, aVar, str);
    }

    @Override // com.fasterxml.jackson.databind.jsontype.impl.d, com.fasterxml.jackson.databind.jsontype.impl.a, com.fasterxml.jackson.databind.jsontype.c
    public JsonTypeInfo.As c() {
        return JsonTypeInfo.As.EXISTING_PROPERTY;
    }

    @Override // com.fasterxml.jackson.databind.jsontype.impl.d, com.fasterxml.jackson.databind.jsontype.impl.a, com.fasterxml.jackson.databind.jsontype.c
    public void e(Object obj, JsonGenerator jsonGenerator, String str) throws IOException {
        if (str != null && jsonGenerator.h()) {
            jsonGenerator.s1(str);
        }
        jsonGenerator.i1();
    }

    @Override // com.fasterxml.jackson.databind.jsontype.impl.d, com.fasterxml.jackson.databind.jsontype.impl.a, com.fasterxml.jackson.databind.jsontype.c
    public void i(Object obj, JsonGenerator jsonGenerator) throws IOException {
        String p = p(obj);
        if (p != null && jsonGenerator.h()) {
            jsonGenerator.s1(p);
        }
        jsonGenerator.i1();
    }

    @Override // com.fasterxml.jackson.databind.jsontype.impl.d
    /* renamed from: t */
    public b s(com.fasterxml.jackson.databind.a aVar) {
        return this.b == aVar ? this : new b(this.a, aVar, this.c);
    }
}
