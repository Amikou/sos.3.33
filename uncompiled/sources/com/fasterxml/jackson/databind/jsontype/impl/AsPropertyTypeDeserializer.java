package com.fasterxml.jackson.databind.jsontype.impl;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import java.io.IOException;

/* loaded from: classes.dex */
public class AsPropertyTypeDeserializer extends AsArrayTypeDeserializer {
    private static final long serialVersionUID = 1;
    public final JsonTypeInfo.As _inclusion;

    public AsPropertyTypeDeserializer(JavaType javaType, com.fasterxml.jackson.databind.jsontype.b bVar, String str, boolean z, JavaType javaType2) {
        this(javaType, bVar, str, z, javaType2, JsonTypeInfo.As.PROPERTY);
    }

    public Object _deserializeTypedForId(JsonParser jsonParser, DeserializationContext deserializationContext, com.fasterxml.jackson.databind.util.e eVar) throws IOException {
        String X = jsonParser.X();
        com.fasterxml.jackson.databind.c<Object> _findDeserializer = _findDeserializer(deserializationContext, X);
        if (this._typeIdVisible) {
            if (eVar == null) {
                eVar = new com.fasterxml.jackson.databind.util.e(jsonParser, deserializationContext);
            }
            eVar.i0(jsonParser.r());
            eVar.o1(X);
        }
        if (eVar != null) {
            jsonParser.e();
            jsonParser = com.fasterxml.jackson.core.util.b.o1(false, eVar.L1(jsonParser), jsonParser);
        }
        jsonParser.T0();
        return _findDeserializer.deserialize(jsonParser, deserializationContext);
    }

    public Object _deserializeTypedUsingDefaultImpl(JsonParser jsonParser, DeserializationContext deserializationContext, com.fasterxml.jackson.databind.util.e eVar) throws IOException {
        com.fasterxml.jackson.databind.c<Object> _findDefaultImplDeserializer = _findDefaultImplDeserializer(deserializationContext);
        if (_findDefaultImplDeserializer != null) {
            if (eVar != null) {
                eVar.f0();
                jsonParser = eVar.L1(jsonParser);
                jsonParser.T0();
            }
            return _findDefaultImplDeserializer.deserialize(jsonParser, deserializationContext);
        }
        Object deserializeIfNatural = com.fasterxml.jackson.databind.jsontype.a.deserializeIfNatural(jsonParser, deserializationContext, this._baseType);
        if (deserializeIfNatural != null) {
            return deserializeIfNatural;
        }
        if (jsonParser.K0()) {
            return super.deserializeTypedFromAny(jsonParser, deserializationContext);
        }
        if (jsonParser.F0(JsonToken.VALUE_STRING) && deserializationContext.isEnabled(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT) && jsonParser.X().trim().isEmpty()) {
            return null;
        }
        JsonToken jsonToken = JsonToken.FIELD_NAME;
        deserializationContext.reportWrongTokenException(jsonParser, jsonToken, "missing property '" + this._typePropertyName + "' that is to contain type id  (for class " + baseTypeName() + ")", new Object[0]);
        return null;
    }

    @Override // com.fasterxml.jackson.databind.jsontype.impl.AsArrayTypeDeserializer, com.fasterxml.jackson.databind.jsontype.a
    public Object deserializeTypedFromAny(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        if (jsonParser.u() == JsonToken.START_ARRAY) {
            return super.deserializeTypedFromArray(jsonParser, deserializationContext);
        }
        return deserializeTypedFromObject(jsonParser, deserializationContext);
    }

    @Override // com.fasterxml.jackson.databind.jsontype.impl.AsArrayTypeDeserializer, com.fasterxml.jackson.databind.jsontype.a
    public Object deserializeTypedFromObject(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        Object g0;
        if (jsonParser.d() && (g0 = jsonParser.g0()) != null) {
            return _deserializeWithNativeTypeId(jsonParser, deserializationContext, g0);
        }
        JsonToken u = jsonParser.u();
        com.fasterxml.jackson.databind.util.e eVar = null;
        if (u == JsonToken.START_OBJECT) {
            u = jsonParser.T0();
        } else if (u != JsonToken.FIELD_NAME) {
            return _deserializeTypedUsingDefaultImpl(jsonParser, deserializationContext, null);
        }
        while (u == JsonToken.FIELD_NAME) {
            String r = jsonParser.r();
            jsonParser.T0();
            if (r.equals(this._typePropertyName)) {
                return _deserializeTypedForId(jsonParser, deserializationContext, eVar);
            }
            if (eVar == null) {
                eVar = new com.fasterxml.jackson.databind.util.e(jsonParser, deserializationContext);
            }
            eVar.i0(r);
            eVar.O1(jsonParser);
            u = jsonParser.T0();
        }
        return _deserializeTypedUsingDefaultImpl(jsonParser, deserializationContext, eVar);
    }

    @Override // com.fasterxml.jackson.databind.jsontype.impl.AsArrayTypeDeserializer, com.fasterxml.jackson.databind.jsontype.impl.TypeDeserializerBase, com.fasterxml.jackson.databind.jsontype.a
    public com.fasterxml.jackson.databind.jsontype.a forProperty(com.fasterxml.jackson.databind.a aVar) {
        return aVar == this._property ? this : new AsPropertyTypeDeserializer(this, aVar);
    }

    @Override // com.fasterxml.jackson.databind.jsontype.impl.AsArrayTypeDeserializer, com.fasterxml.jackson.databind.jsontype.impl.TypeDeserializerBase, com.fasterxml.jackson.databind.jsontype.a
    public JsonTypeInfo.As getTypeInclusion() {
        return this._inclusion;
    }

    public AsPropertyTypeDeserializer(JavaType javaType, com.fasterxml.jackson.databind.jsontype.b bVar, String str, boolean z, JavaType javaType2, JsonTypeInfo.As as) {
        super(javaType, bVar, str, z, javaType2);
        this._inclusion = as;
    }

    public AsPropertyTypeDeserializer(AsPropertyTypeDeserializer asPropertyTypeDeserializer, com.fasterxml.jackson.databind.a aVar) {
        super(asPropertyTypeDeserializer, aVar);
        this._inclusion = asPropertyTypeDeserializer._inclusion;
    }
}
