package com.fasterxml.jackson.databind.introspect;

import com.fasterxml.jackson.databind.JavaType;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

/* loaded from: classes.dex */
public abstract class AnnotatedWithParams extends AnnotatedMember {
    private static final long serialVersionUID = 1;
    public final we[] _paramAnnotations;

    public AnnotatedWithParams(d dVar, we weVar, we[] weVarArr) {
        super(dVar, weVar);
        this._paramAnnotations = weVarArr;
    }

    public final void addOrOverrideParam(int i, Annotation annotation) {
        we weVar = this._paramAnnotations[i];
        if (weVar == null) {
            weVar = new we();
            this._paramAnnotations[i] = weVar;
        }
        weVar.c(annotation);
    }

    public abstract Object call() throws Exception;

    public abstract Object call(Object[] objArr) throws Exception;

    public abstract Object call1(Object obj) throws Exception;

    public final int getAnnotationCount() {
        return this._annotations.i();
    }

    @Deprecated
    public abstract Type getGenericParameterType(int i);

    public final AnnotatedParameter getParameter(int i) {
        return new AnnotatedParameter(this, getParameterType(i), getParameterAnnotations(i), i);
    }

    public final we getParameterAnnotations(int i) {
        we[] weVarArr = this._paramAnnotations;
        if (weVarArr == null || i < 0 || i >= weVarArr.length) {
            return null;
        }
        return weVarArr[i];
    }

    public abstract int getParameterCount();

    public abstract JavaType getParameterType(int i);

    public abstract Class<?> getRawParameterType(int i);

    public AnnotatedParameter replaceParameterAnnotations(int i, we weVar) {
        this._paramAnnotations[i] = weVar;
        return getParameter(i);
    }

    public AnnotatedWithParams(AnnotatedWithParams annotatedWithParams, we[] weVarArr) {
        super(annotatedWithParams);
        this._paramAnnotations = weVarArr;
    }
}
