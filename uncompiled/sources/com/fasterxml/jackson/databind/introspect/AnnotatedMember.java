package com.fasterxml.jackson.databind.introspect;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Member;
import java.util.Collections;

/* loaded from: classes.dex */
public abstract class AnnotatedMember extends ue implements Serializable {
    private static final long serialVersionUID = 1;
    public final transient we _annotations;
    public final transient d _typeContext;

    public AnnotatedMember(d dVar, we weVar) {
        this._typeContext = dVar;
        this._annotations = weVar;
    }

    public final boolean addIfNotPresent(Annotation annotation) {
        return this._annotations.d(annotation);
    }

    public final boolean addOrOverride(Annotation annotation) {
        return this._annotations.c(annotation);
    }

    @Override // defpackage.ue
    public Iterable<Annotation> annotations() {
        we weVar = this._annotations;
        if (weVar == null) {
            return Collections.emptyList();
        }
        return weVar.e();
    }

    public final void fixAccess(boolean z) {
        Member member = getMember();
        if (member != null) {
            com.fasterxml.jackson.databind.util.c.f(member, z);
        }
    }

    @Override // defpackage.ue
    public we getAllAnnotations() {
        return this._annotations;
    }

    @Override // defpackage.ue
    public final <A extends Annotation> A getAnnotation(Class<A> cls) {
        we weVar = this._annotations;
        if (weVar == null) {
            return null;
        }
        return (A) weVar.a(cls);
    }

    public abstract Class<?> getDeclaringClass();

    public abstract Member getMember();

    public d getTypeContext() {
        return this._typeContext;
    }

    public abstract Object getValue(Object obj) throws UnsupportedOperationException, IllegalArgumentException;

    @Override // defpackage.ue
    public final boolean hasAnnotation(Class<?> cls) {
        we weVar = this._annotations;
        if (weVar == null) {
            return false;
        }
        return weVar.f(cls);
    }

    @Override // defpackage.ue
    public boolean hasOneOf(Class<? extends Annotation>[] clsArr) {
        we weVar = this._annotations;
        if (weVar == null) {
            return false;
        }
        return weVar.g(clsArr);
    }

    public abstract void setValue(Object obj, Object obj2) throws UnsupportedOperationException, IllegalArgumentException;

    @Deprecated
    public final void fixAccess() {
        fixAccess(true);
    }

    public AnnotatedMember(AnnotatedMember annotatedMember) {
        this._typeContext = annotatedMember._typeContext;
        this._annotations = annotatedMember._annotations;
    }
}
