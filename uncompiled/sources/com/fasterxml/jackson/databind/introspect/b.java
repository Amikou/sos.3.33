package com.fasterxml.jackson.databind.introspect;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.PropertyName;
import com.fasterxml.jackson.databind.annotation.d;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import defpackage.o80;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* compiled from: BasicBeanDescription.java */
/* loaded from: classes.dex */
public class b extends so {
    public final mo2 b;
    public final MapperConfig<?> c;
    public final AnnotationIntrospector d;
    public final a e;
    public List<vo> f;
    public jl2 g;

    public b(mo2 mo2Var, JavaType javaType, a aVar) {
        super(javaType);
        this.b = mo2Var;
        MapperConfig<?> z = mo2Var.z();
        this.c = z;
        if (z == null) {
            this.d = null;
        } else {
            this.d = z.getAnnotationIntrospector();
        }
        this.e = aVar;
    }

    public static b G(mo2 mo2Var) {
        return new b(mo2Var);
    }

    public static b H(MapperConfig<?> mapperConfig, JavaType javaType, a aVar) {
        return new b(mapperConfig, javaType, aVar, Collections.emptyList());
    }

    public static b I(mo2 mo2Var) {
        return new b(mo2Var);
    }

    @Override // defpackage.so
    public Object A(boolean z) {
        AnnotatedConstructor Q = this.e.Q();
        if (Q == null) {
            return null;
        }
        if (z) {
            Q.fixAccess(this.c.isEnabled(MapperFeature.OVERRIDE_PUBLIC_ACCESS_MODIFIERS));
        }
        try {
            return Q.getAnnotated().newInstance(new Object[0]);
        } catch (Exception e) {
            e = e;
            while (e.getCause() != null) {
                e = e.getCause();
            }
            if (!(e instanceof Error)) {
                if (e instanceof RuntimeException) {
                    throw ((RuntimeException) e);
                }
                throw new IllegalArgumentException("Failed to instantiate bean of type " + this.e.getAnnotated().getName() + ": (" + e.getClass().getName() + ") " + e.getMessage(), e);
            }
            throw ((Error) e);
        }
    }

    public o80<Object, Object> C(Object obj) {
        if (obj == null) {
            return null;
        }
        if (obj instanceof o80) {
            return (o80) obj;
        }
        if (obj instanceof Class) {
            Class cls = (Class) obj;
            if (cls == o80.a.class || com.fasterxml.jackson.databind.util.c.G(cls)) {
                return null;
            }
            if (o80.class.isAssignableFrom(cls)) {
                this.c.getHandlerInstantiator();
                return (o80) com.fasterxml.jackson.databind.util.c.i(cls, this.c.canOverrideAccessModifiers());
            }
            throw new IllegalStateException("AnnotationIntrospector returned Class " + cls.getName() + "; expected Class<Converter>");
        }
        throw new IllegalStateException("AnnotationIntrospector returned Converter definition of type " + obj.getClass().getName() + "; expected type Converter or Class<Converter> instead");
    }

    public List<vo> D() {
        if (this.f == null) {
            this.f = this.b.E();
        }
        return this.f;
    }

    public boolean E(vo voVar) {
        if (J(voVar.o())) {
            return false;
        }
        D().add(voVar);
        return true;
    }

    public vo F(PropertyName propertyName) {
        for (vo voVar : D()) {
            if (voVar.D(propertyName)) {
                return voVar;
            }
        }
        return null;
    }

    public boolean J(PropertyName propertyName) {
        return F(propertyName) != null;
    }

    public boolean K(AnnotatedMethod annotatedMethod) {
        Class<?> rawParameterType;
        if (r().isAssignableFrom(annotatedMethod.getRawReturnType())) {
            if (this.d.hasCreatorAnnotation(annotatedMethod)) {
                return true;
            }
            String name = annotatedMethod.getName();
            if ("valueOf".equals(name) && annotatedMethod.getParameterCount() == 1) {
                return true;
            }
            return "fromString".equals(name) && annotatedMethod.getParameterCount() == 1 && ((rawParameterType = annotatedMethod.getRawParameterType(0)) == String.class || CharSequence.class.isAssignableFrom(rawParameterType));
        }
        return false;
    }

    public boolean L(String str) {
        Iterator<vo> it = D().iterator();
        while (it.hasNext()) {
            if (it.next().t().equals(str)) {
                it.remove();
                return true;
            }
        }
        return false;
    }

    @Override // defpackage.so
    public AnnotatedMember a() throws IllegalArgumentException {
        mo2 mo2Var = this.b;
        AnnotatedMember v = mo2Var == null ? null : mo2Var.v();
        if (v == null || Map.class.isAssignableFrom(v.getRawType())) {
            return v;
        }
        throw new IllegalArgumentException("Invalid 'any-getter' annotation on method " + v.getName() + "(): return type is not instance of java.util.Map");
    }

    @Override // defpackage.so
    public AnnotatedMethod b() throws IllegalArgumentException {
        Class<?> rawParameterType;
        mo2 mo2Var = this.b;
        AnnotatedMethod x = mo2Var == null ? null : mo2Var.x();
        if (x == null || (rawParameterType = x.getRawParameterType(0)) == String.class || rawParameterType == Object.class) {
            return x;
        }
        throw new IllegalArgumentException("Invalid 'any-setter' annotation on method " + x.getName() + "(): first argument not of type String or Object, but " + rawParameterType.getName());
    }

    @Override // defpackage.so
    public AnnotatedMember c() throws IllegalArgumentException {
        mo2 mo2Var = this.b;
        AnnotatedMember w = mo2Var == null ? null : mo2Var.w();
        if (w == null || Map.class.isAssignableFrom(w.getRawType())) {
            return w;
        }
        throw new IllegalArgumentException("Invalid 'any-setter' annotation on field " + w.getName() + "(): type is not instance of java.util.Map");
    }

    @Override // defpackage.so
    public Map<String, AnnotatedMember> d() {
        AnnotationIntrospector.ReferenceProperty findReferenceType;
        HashMap hashMap = null;
        for (vo voVar : D()) {
            AnnotatedMember s = voVar.s();
            if (s != null && (findReferenceType = this.d.findReferenceType(s)) != null && findReferenceType.c()) {
                if (hashMap == null) {
                    hashMap = new HashMap();
                }
                String b = findReferenceType.b();
                if (hashMap.put(b, s) != null) {
                    throw new IllegalArgumentException("Multiple back-reference properties with name '" + b + "'");
                }
            }
        }
        return hashMap;
    }

    @Override // defpackage.so
    public AnnotatedConstructor e() {
        return this.e.Q();
    }

    @Override // defpackage.so
    public o80<Object, Object> f() {
        AnnotationIntrospector annotationIntrospector = this.d;
        if (annotationIntrospector == null) {
            return null;
        }
        return C(annotationIntrospector.findDeserializationConverter(this.e));
    }

    @Override // defpackage.so
    public JsonFormat.Value g(JsonFormat.Value value) {
        JsonFormat.Value findFormat;
        AnnotationIntrospector annotationIntrospector = this.d;
        if (annotationIntrospector != null && (findFormat = annotationIntrospector.findFormat(this.e)) != null) {
            value = value == null ? findFormat : value.withOverrides(findFormat);
        }
        JsonFormat.Value defaultPropertyFormat = this.c.getDefaultPropertyFormat(this.e.getRawType());
        return defaultPropertyFormat != null ? value == null ? defaultPropertyFormat : value.withOverrides(defaultPropertyFormat) : value;
    }

    @Override // defpackage.so
    public Method h(Class<?>... clsArr) {
        for (AnnotatedMethod annotatedMethod : this.e.R()) {
            if (K(annotatedMethod) && annotatedMethod.getParameterCount() == 1) {
                Class<?> rawParameterType = annotatedMethod.getRawParameterType(0);
                for (Class<?> cls : clsArr) {
                    if (rawParameterType.isAssignableFrom(cls)) {
                        return annotatedMethod.getAnnotated();
                    }
                }
                continue;
            }
        }
        return null;
    }

    @Override // defpackage.so
    public Map<Object, AnnotatedMember> i() {
        mo2 mo2Var = this.b;
        if (mo2Var != null) {
            return mo2Var.B();
        }
        return Collections.emptyMap();
    }

    @Override // defpackage.so
    public AnnotatedMethod j() {
        mo2 mo2Var = this.b;
        if (mo2Var == null) {
            return null;
        }
        return mo2Var.C();
    }

    @Override // defpackage.so
    public AnnotatedMethod k(String str, Class<?>[] clsArr) {
        return this.e.M(str, clsArr);
    }

    @Override // defpackage.so
    public Class<?> l() {
        AnnotationIntrospector annotationIntrospector = this.d;
        if (annotationIntrospector == null) {
            return null;
        }
        return annotationIntrospector.findPOJOBuilder(this.e);
    }

    @Override // defpackage.so
    public d.a m() {
        AnnotationIntrospector annotationIntrospector = this.d;
        if (annotationIntrospector == null) {
            return null;
        }
        return annotationIntrospector.findPOJOBuilderConfig(this.e);
    }

    @Override // defpackage.so
    public List<vo> n() {
        return D();
    }

    @Override // defpackage.so
    public JsonInclude.Value o(JsonInclude.Value value) {
        JsonInclude.Value findPropertyInclusion;
        AnnotationIntrospector annotationIntrospector = this.d;
        return (annotationIntrospector == null || (findPropertyInclusion = annotationIntrospector.findPropertyInclusion(this.e)) == null) ? value : value == null ? findPropertyInclusion : value.withOverrides(findPropertyInclusion);
    }

    @Override // defpackage.so
    public o80<Object, Object> p() {
        AnnotationIntrospector annotationIntrospector = this.d;
        if (annotationIntrospector == null) {
            return null;
        }
        return C(annotationIntrospector.findSerializationConverter(this.e));
    }

    @Override // defpackage.so
    public Constructor<?> q(Class<?>... clsArr) {
        for (AnnotatedConstructor annotatedConstructor : this.e.P()) {
            if (annotatedConstructor.getParameterCount() == 1) {
                Class<?> rawParameterType = annotatedConstructor.getRawParameterType(0);
                for (Class<?> cls : clsArr) {
                    if (cls == rawParameterType) {
                        return annotatedConstructor.getAnnotated();
                    }
                }
                continue;
            }
        }
        return null;
    }

    @Override // defpackage.so
    public xe s() {
        return this.e.O();
    }

    @Override // defpackage.so
    public a t() {
        return this.e;
    }

    @Override // defpackage.so
    public List<AnnotatedConstructor> u() {
        return this.e.P();
    }

    @Override // defpackage.so
    public List<AnnotatedMethod> v() {
        List<AnnotatedMethod> R = this.e.R();
        if (R.isEmpty()) {
            return R;
        }
        ArrayList arrayList = new ArrayList();
        for (AnnotatedMethod annotatedMethod : R) {
            if (K(annotatedMethod)) {
                arrayList.add(annotatedMethod);
            }
        }
        return arrayList;
    }

    @Override // defpackage.so
    public Set<String> w() {
        mo2 mo2Var = this.b;
        Set<String> A = mo2Var == null ? null : mo2Var.A();
        return A == null ? Collections.emptySet() : A;
    }

    @Override // defpackage.so
    public jl2 x() {
        return this.g;
    }

    @Override // defpackage.so
    public boolean z() {
        return this.e.S();
    }

    public b(MapperConfig<?> mapperConfig, JavaType javaType, a aVar, List<vo> list) {
        super(javaType);
        this.b = null;
        this.c = mapperConfig;
        if (mapperConfig == null) {
            this.d = null;
        } else {
            this.d = mapperConfig.getAnnotationIntrospector();
        }
        this.e = aVar;
        this.f = list;
    }

    public b(mo2 mo2Var) {
        this(mo2Var, mo2Var.G(), mo2Var.y());
        this.g = mo2Var.D();
    }
}
