package com.fasterxml.jackson.databind.introspect;

import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.databind.introspect.c;
import com.fasterxml.jackson.databind.introspect.d;
import com.fasterxml.jackson.databind.type.TypeBindings;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.databind.util.c;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/* compiled from: AnnotatedClass.java */
/* loaded from: classes.dex */
public final class a extends ue implements d {
    public static final we[] u0 = new we[0];
    public final JavaType a;
    public final Class<?> f0;
    public final TypeBindings g0;
    public final List<JavaType> h0;
    public final AnnotationIntrospector i0;
    public final TypeFactory j0;
    public final c.a k0;
    public final Class<?> l0;
    public final we m0;
    public boolean n0 = false;
    public AnnotatedConstructor o0;
    public List<AnnotatedConstructor> p0;
    public List<AnnotatedMethod> q0;
    public ve r0;
    public List<AnnotatedField> s0;
    public transient Boolean t0;

    public a(JavaType javaType, Class<?> cls, TypeBindings typeBindings, List<JavaType> list, AnnotationIntrospector annotationIntrospector, c.a aVar, TypeFactory typeFactory) {
        this.a = javaType;
        this.f0 = cls;
        this.g0 = typeBindings;
        this.h0 = list;
        this.i0 = annotationIntrospector;
        this.j0 = typeFactory;
        this.k0 = aVar;
        this.l0 = aVar == null ? null : aVar.findMixInClassFor(cls);
        this.m0 = F();
    }

    public static a H(JavaType javaType, MapperConfig<?> mapperConfig) {
        return new a(javaType, javaType.getRawClass(), javaType.getBindings(), com.fasterxml.jackson.databind.util.c.u(javaType, null, false), mapperConfig.isAnnotationProcessingEnabled() ? mapperConfig.getAnnotationIntrospector() : null, mapperConfig, mapperConfig.getTypeFactory());
    }

    public static a I(JavaType javaType, MapperConfig<?> mapperConfig, c.a aVar) {
        return new a(javaType, javaType.getRawClass(), javaType.getBindings(), com.fasterxml.jackson.databind.util.c.u(javaType, null, false), mapperConfig.isAnnotationProcessingEnabled() ? mapperConfig.getAnnotationIntrospector() : null, aVar, mapperConfig.getTypeFactory());
    }

    public static a J(Class<?> cls, MapperConfig<?> mapperConfig) {
        if (mapperConfig == null) {
            return new a(null, cls, TypeBindings.emptyBindings(), Collections.emptyList(), null, null, null);
        }
        return new a(null, cls, TypeBindings.emptyBindings(), Collections.emptyList(), mapperConfig.isAnnotationProcessingEnabled() ? mapperConfig.getAnnotationIntrospector() : null, mapperConfig, mapperConfig.getTypeFactory());
    }

    public static a K(Class<?> cls, MapperConfig<?> mapperConfig, c.a aVar) {
        if (mapperConfig == null) {
            return new a(null, cls, TypeBindings.emptyBindings(), Collections.emptyList(), null, null, null);
        }
        return new a(null, cls, TypeBindings.emptyBindings(), Collections.emptyList(), mapperConfig.isAnnotationProcessingEnabled() ? mapperConfig.getAnnotationIntrospector() : null, aVar, mapperConfig.getTypeFactory());
    }

    public Map<String, AnnotatedField> A(JavaType javaType, d dVar, Map<String, AnnotatedField> map) {
        Field[] x;
        Class<?> findMixInClassFor;
        JavaType superClass = javaType.getSuperClass();
        if (superClass != null) {
            Class<?> rawClass = javaType.getRawClass();
            map = A(superClass, new d.a(this.j0, superClass.getBindings()), map);
            for (Field field : com.fasterxml.jackson.databind.util.c.x(rawClass)) {
                if (D(field)) {
                    if (map == null) {
                        map = new LinkedHashMap<>();
                    }
                    map.put(field.getName(), u(field, dVar));
                }
            }
            c.a aVar = this.k0;
            if (aVar != null && (findMixInClassFor = aVar.findMixInClassFor(rawClass)) != null) {
                i(findMixInClassFor, rawClass, map);
            }
        }
        return map;
    }

    public final boolean B(Annotation annotation) {
        AnnotationIntrospector annotationIntrospector = this.i0;
        return annotationIntrospector != null && annotationIntrospector.isAnnotationBundle(annotation);
    }

    public final boolean C(Constructor<?> constructor) {
        return !constructor.isSynthetic();
    }

    public final boolean D(Field field) {
        return (field.isSynthetic() || Modifier.isStatic(field.getModifiers())) ? false : true;
    }

    public boolean E(Method method) {
        return (Modifier.isStatic(method.getModifiers()) || method.isSynthetic() || method.isBridge() || method.getParameterTypes().length > 2) ? false : true;
    }

    public final we F() {
        we weVar = new we();
        if (this.i0 != null) {
            Class<?> cls = this.l0;
            if (cls != null) {
                f(weVar, this.f0, cls);
            }
            b(weVar, com.fasterxml.jackson.databind.util.c.l(this.f0));
            for (JavaType javaType : this.h0) {
                d(weVar, javaType);
                b(weVar, com.fasterxml.jackson.databind.util.c.l(javaType.getRawClass()));
            }
            e(weVar, Object.class);
        }
        return weVar;
    }

    public final ve G() {
        Class<?> findMixInClassFor;
        ve veVar = new ve();
        ve veVar2 = new ve();
        k(this.f0, this, veVar, this.l0, veVar2);
        for (JavaType javaType : this.h0) {
            c.a aVar = this.k0;
            k(javaType.getRawClass(), new d.a(this.j0, javaType.getBindings()), veVar, aVar == null ? null : aVar.findMixInClassFor(javaType.getRawClass()), veVar2);
        }
        c.a aVar2 = this.k0;
        if (aVar2 != null && (findMixInClassFor = aVar2.findMixInClassFor(Object.class)) != null) {
            l(this.f0, veVar, findMixInClassFor, veVar2);
        }
        if (this.i0 != null && !veVar2.isEmpty()) {
            Iterator<AnnotatedMethod> it = veVar2.iterator();
            while (it.hasNext()) {
                AnnotatedMethod next = it.next();
                try {
                    Method declaredMethod = Object.class.getDeclaredMethod(next.getName(), next.getRawParameterTypes());
                    if (declaredMethod != null) {
                        AnnotatedMethod v = v(declaredMethod, this);
                        n(next.getAnnotated(), v, false);
                        veVar.e(v);
                    }
                } catch (Exception unused) {
                }
            }
        }
        return veVar;
    }

    public Iterable<AnnotatedField> L() {
        if (this.s0 == null) {
            W();
        }
        return this.s0;
    }

    public AnnotatedMethod M(String str, Class<?>[] clsArr) {
        if (this.r0 == null) {
            X();
        }
        return this.r0.i(str, clsArr);
    }

    @Override // defpackage.ue
    /* renamed from: N */
    public Class<?> getAnnotated() {
        return this.f0;
    }

    public xe O() {
        return this.m0;
    }

    public List<AnnotatedConstructor> P() {
        if (!this.n0) {
            V();
        }
        return this.p0;
    }

    public AnnotatedConstructor Q() {
        if (!this.n0) {
            V();
        }
        return this.o0;
    }

    public List<AnnotatedMethod> R() {
        if (!this.n0) {
            V();
        }
        return this.q0;
    }

    public boolean S() {
        return this.m0.i() > 0;
    }

    public boolean T() {
        Boolean bool = this.t0;
        if (bool == null) {
            bool = Boolean.valueOf(com.fasterxml.jackson.databind.util.c.L(this.f0));
            this.t0 = bool;
        }
        return bool.booleanValue();
    }

    public Iterable<AnnotatedMethod> U() {
        if (this.r0 == null) {
            X();
        }
        return this.r0;
    }

    public final void V() {
        ArrayList arrayList;
        Method[] z;
        ArrayList arrayList2 = null;
        if (this.a.isEnumType()) {
            arrayList = null;
        } else {
            c.b[] w = com.fasterxml.jackson.databind.util.c.w(this.f0);
            arrayList = null;
            for (c.b bVar : w) {
                if (C(bVar.a())) {
                    if (bVar.d() == 0) {
                        this.o0 = t(bVar, this);
                    } else {
                        if (arrayList == null) {
                            arrayList = new ArrayList(Math.max(10, w.length));
                        }
                        arrayList.add(w(bVar, this));
                    }
                }
            }
        }
        if (arrayList == null) {
            this.p0 = Collections.emptyList();
        } else {
            this.p0 = arrayList;
        }
        if (this.l0 != null && (this.o0 != null || !this.p0.isEmpty())) {
            g(this.l0);
        }
        AnnotationIntrospector annotationIntrospector = this.i0;
        if (annotationIntrospector != null) {
            AnnotatedConstructor annotatedConstructor = this.o0;
            if (annotatedConstructor != null && annotationIntrospector.hasIgnoreMarker(annotatedConstructor)) {
                this.o0 = null;
            }
            List<AnnotatedConstructor> list = this.p0;
            if (list != null) {
                int size = list.size();
                while (true) {
                    size--;
                    if (size < 0) {
                        break;
                    } else if (this.i0.hasIgnoreMarker(this.p0.get(size))) {
                        this.p0.remove(size);
                    }
                }
            }
        }
        for (Method method : z(this.f0)) {
            if (Modifier.isStatic(method.getModifiers())) {
                if (arrayList2 == null) {
                    arrayList2 = new ArrayList(8);
                }
                arrayList2.add(s(method, this));
            }
        }
        if (arrayList2 == null) {
            this.q0 = Collections.emptyList();
        } else {
            this.q0 = arrayList2;
            Class<?> cls = this.l0;
            if (cls != null) {
                h(cls);
            }
            if (this.i0 != null) {
                int size2 = this.q0.size();
                while (true) {
                    size2--;
                    if (size2 < 0) {
                        break;
                    } else if (this.i0.hasIgnoreMarker(this.q0.get(size2))) {
                        this.q0.remove(size2);
                    }
                }
            }
        }
        this.n0 = true;
    }

    public final void W() {
        List<AnnotatedField> emptyList;
        Map<String, AnnotatedField> A = A(this.a, this, null);
        if (A != null && A.size() != 0) {
            emptyList = new ArrayList<>(A.size());
            emptyList.addAll(A.values());
        } else {
            emptyList = Collections.emptyList();
        }
        this.s0 = emptyList;
    }

    public final void X() {
        this.r0 = G();
    }

    @Override // defpackage.ue
    /* renamed from: Y */
    public a withAnnotations(we weVar) {
        return new a(this, weVar);
    }

    @Override // com.fasterxml.jackson.databind.introspect.d
    public JavaType a(Type type) {
        return this.j0.constructType(type, this.g0);
    }

    @Override // defpackage.ue
    public Iterable<Annotation> annotations() {
        return this.m0.e();
    }

    public final we b(we weVar, Annotation[] annotationArr) {
        if (annotationArr != null) {
            List<Annotation> list = null;
            for (Annotation annotation : annotationArr) {
                if (weVar.d(annotation) && B(annotation)) {
                    list = j(annotation, list);
                }
            }
            if (list != null) {
                b(weVar, (Annotation[]) list.toArray(new Annotation[list.size()]));
            }
        }
        return weVar;
    }

    public final void c(AnnotatedMember annotatedMember, Annotation[] annotationArr) {
        if (annotationArr != null) {
            List<Annotation> list = null;
            for (Annotation annotation : annotationArr) {
                if (annotatedMember.addIfNotPresent(annotation) && B(annotation)) {
                    list = j(annotation, list);
                }
            }
            if (list != null) {
                c(annotatedMember, (Annotation[]) list.toArray(new Annotation[list.size()]));
            }
        }
    }

    public void d(we weVar, JavaType javaType) {
        if (this.k0 != null) {
            Class<?> rawClass = javaType.getRawClass();
            f(weVar, rawClass, this.k0.findMixInClassFor(rawClass));
        }
    }

    public void e(we weVar, Class<?> cls) {
        c.a aVar = this.k0;
        if (aVar != null) {
            f(weVar, cls, aVar.findMixInClassFor(cls));
        }
    }

    @Override // defpackage.ue
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        return obj != null && obj.getClass() == a.class && ((a) obj).f0 == this.f0;
    }

    public void f(we weVar, Class<?> cls, Class<?> cls2) {
        if (cls2 == null) {
            return;
        }
        b(weVar, com.fasterxml.jackson.databind.util.c.l(cls2));
        for (Class<?> cls3 : com.fasterxml.jackson.databind.util.c.t(cls2, cls, false)) {
            b(weVar, com.fasterxml.jackson.databind.util.c.l(cls3));
        }
    }

    public void g(Class<?> cls) {
        List<AnnotatedConstructor> list = this.p0;
        int size = list == null ? 0 : list.size();
        h72[] h72VarArr = null;
        for (c.b bVar : com.fasterxml.jackson.databind.util.c.w(cls)) {
            Constructor<?> a = bVar.a();
            if (a.getParameterTypes().length == 0) {
                AnnotatedConstructor annotatedConstructor = this.o0;
                if (annotatedConstructor != null) {
                    m(a, annotatedConstructor, false);
                }
            } else {
                if (h72VarArr == null) {
                    h72VarArr = new h72[size];
                    for (int i = 0; i < size; i++) {
                        h72VarArr[i] = new h72(this.p0.get(i).getAnnotated());
                    }
                }
                h72 h72Var = new h72(a);
                int i2 = 0;
                while (true) {
                    if (i2 >= size) {
                        break;
                    }
                    if (h72Var.equals(h72VarArr[i2])) {
                        m(a, this.p0.get(i2), true);
                        break;
                    }
                    i2++;
                }
            }
        }
    }

    @Override // defpackage.ue
    public we getAllAnnotations() {
        return this.m0;
    }

    @Override // defpackage.ue
    public <A extends Annotation> A getAnnotation(Class<A> cls) {
        return (A) this.m0.a(cls);
    }

    @Override // defpackage.ue
    public int getModifiers() {
        return this.f0.getModifiers();
    }

    @Override // defpackage.ue
    public String getName() {
        return this.f0.getName();
    }

    @Override // defpackage.ue
    public Class<?> getRawType() {
        return this.f0;
    }

    @Override // defpackage.ue
    public JavaType getType() {
        return this.a;
    }

    public void h(Class<?> cls) {
        Method[] y;
        int size = this.q0.size();
        h72[] h72VarArr = null;
        for (Method method : com.fasterxml.jackson.databind.util.c.y(cls)) {
            if (Modifier.isStatic(method.getModifiers()) && method.getParameterTypes().length != 0) {
                if (h72VarArr == null) {
                    h72VarArr = new h72[size];
                    for (int i = 0; i < size; i++) {
                        h72VarArr[i] = new h72(this.q0.get(i).getAnnotated());
                    }
                }
                h72 h72Var = new h72(method);
                int i2 = 0;
                while (true) {
                    if (i2 >= size) {
                        break;
                    }
                    if (h72Var.equals(h72VarArr[i2])) {
                        n(method, this.q0.get(i2), true);
                        break;
                    }
                    i2++;
                }
            }
        }
    }

    @Override // defpackage.ue
    public boolean hasAnnotation(Class<?> cls) {
        return this.m0.f(cls);
    }

    @Override // defpackage.ue
    public boolean hasOneOf(Class<? extends Annotation>[] clsArr) {
        return this.m0.g(clsArr);
    }

    @Override // defpackage.ue
    public int hashCode() {
        return this.f0.getName().hashCode();
    }

    public void i(Class<?> cls, Class<?> cls2, Map<String, AnnotatedField> map) {
        Field[] x;
        AnnotatedField annotatedField;
        for (Class<?> cls3 : com.fasterxml.jackson.databind.util.c.t(cls, cls2, true)) {
            for (Field field : com.fasterxml.jackson.databind.util.c.x(cls3)) {
                if (D(field) && (annotatedField = map.get(field.getName())) != null) {
                    p(annotatedField, field.getDeclaredAnnotations());
                }
            }
        }
    }

    public final List<Annotation> j(Annotation annotation, List<Annotation> list) {
        Annotation[] l;
        for (Annotation annotation2 : com.fasterxml.jackson.databind.util.c.l(annotation.annotationType())) {
            if (!(annotation2 instanceof Target) && !(annotation2 instanceof Retention)) {
                if (list == null) {
                    list = new ArrayList<>();
                }
                list.add(annotation2);
            }
        }
        return list;
    }

    public void k(Class<?> cls, d dVar, ve veVar, Class<?> cls2, ve veVar2) {
        Method[] z;
        if (cls2 != null) {
            l(cls, veVar, cls2, veVar2);
        }
        if (cls == null) {
            return;
        }
        for (Method method : z(cls)) {
            if (E(method)) {
                AnnotatedMethod k = veVar.k(method);
                if (k == null) {
                    AnnotatedMethod v = v(method, dVar);
                    veVar.e(v);
                    AnnotatedMethod m = veVar2.m(method);
                    if (m != null) {
                        n(m.getAnnotated(), v, false);
                    }
                } else {
                    o(method, k);
                    if (k.getDeclaringClass().isInterface() && !method.getDeclaringClass().isInterface()) {
                        veVar.e(k.withMethod(method));
                    }
                }
            }
        }
    }

    public void l(Class<?> cls, ve veVar, Class<?> cls2, ve veVar2) {
        Method[] y;
        for (Class<?> cls3 : com.fasterxml.jackson.databind.util.c.s(cls2, cls, true)) {
            for (Method method : com.fasterxml.jackson.databind.util.c.y(cls3)) {
                if (E(method)) {
                    AnnotatedMethod k = veVar.k(method);
                    if (k != null) {
                        o(method, k);
                    } else {
                        AnnotatedMethod k2 = veVar2.k(method);
                        if (k2 != null) {
                            o(method, k2);
                        } else {
                            veVar2.e(v(method, this));
                        }
                    }
                }
            }
        }
    }

    public void m(Constructor<?> constructor, AnnotatedConstructor annotatedConstructor, boolean z) {
        p(annotatedConstructor, constructor.getDeclaredAnnotations());
        if (z) {
            Annotation[][] parameterAnnotations = constructor.getParameterAnnotations();
            int length = parameterAnnotations.length;
            for (int i = 0; i < length; i++) {
                for (Annotation annotation : parameterAnnotations[i]) {
                    annotatedConstructor.addOrOverrideParam(i, annotation);
                }
            }
        }
    }

    public void n(Method method, AnnotatedMethod annotatedMethod, boolean z) {
        p(annotatedMethod, method.getDeclaredAnnotations());
        if (z) {
            Annotation[][] parameterAnnotations = method.getParameterAnnotations();
            int length = parameterAnnotations.length;
            for (int i = 0; i < length; i++) {
                for (Annotation annotation : parameterAnnotations[i]) {
                    annotatedMethod.addOrOverrideParam(i, annotation);
                }
            }
        }
    }

    public void o(Method method, AnnotatedMethod annotatedMethod) {
        c(annotatedMethod, method.getDeclaredAnnotations());
    }

    public final void p(AnnotatedMember annotatedMember, Annotation[] annotationArr) {
        if (annotationArr != null) {
            List<Annotation> list = null;
            for (Annotation annotation : annotationArr) {
                if (annotatedMember.addOrOverride(annotation) && B(annotation)) {
                    list = j(annotation, list);
                }
            }
            if (list != null) {
                p(annotatedMember, (Annotation[]) list.toArray(new Annotation[list.size()]));
            }
        }
    }

    public we q(Annotation[] annotationArr) {
        return b(new we(), annotationArr);
    }

    public we[] r(Annotation[][] annotationArr) {
        int length = annotationArr.length;
        we[] weVarArr = new we[length];
        for (int i = 0; i < length; i++) {
            weVarArr[i] = q(annotationArr[i]);
        }
        return weVarArr;
    }

    public AnnotatedMethod s(Method method, d dVar) {
        int length = method.getParameterTypes().length;
        if (this.i0 == null) {
            return new AnnotatedMethod(dVar, method, x(), y(length));
        }
        if (length == 0) {
            return new AnnotatedMethod(dVar, method, q(method.getDeclaredAnnotations()), u0);
        }
        return new AnnotatedMethod(dVar, method, q(method.getDeclaredAnnotations()), r(method.getParameterAnnotations()));
    }

    public AnnotatedConstructor t(c.b bVar, d dVar) {
        if (this.i0 == null) {
            return new AnnotatedConstructor(dVar, bVar.a(), x(), u0);
        }
        return new AnnotatedConstructor(dVar, bVar.a(), q(bVar.b()), u0);
    }

    @Override // defpackage.ue
    public String toString() {
        return "[AnnotedClass " + this.f0.getName() + "]";
    }

    public AnnotatedField u(Field field, d dVar) {
        if (this.i0 == null) {
            return new AnnotatedField(dVar, field, x());
        }
        return new AnnotatedField(dVar, field, q(field.getDeclaredAnnotations()));
    }

    public AnnotatedMethod v(Method method, d dVar) {
        if (this.i0 == null) {
            return new AnnotatedMethod(dVar, method, x(), null);
        }
        return new AnnotatedMethod(dVar, method, q(method.getDeclaredAnnotations()), null);
    }

    /* JADX WARN: Removed duplicated region for block: B:24:0x0073  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public com.fasterxml.jackson.databind.introspect.AnnotatedConstructor w(com.fasterxml.jackson.databind.util.c.b r9, com.fasterxml.jackson.databind.introspect.d r10) {
        /*
            r8 = this;
            int r0 = r9.d()
            com.fasterxml.jackson.databind.AnnotationIntrospector r1 = r8.i0
            if (r1 != 0) goto L1a
            com.fasterxml.jackson.databind.introspect.AnnotatedConstructor r1 = new com.fasterxml.jackson.databind.introspect.AnnotatedConstructor
            java.lang.reflect.Constructor r9 = r9.a()
            we r2 = r8.x()
            we[] r0 = r8.y(r0)
            r1.<init>(r10, r9, r2, r0)
            return r1
        L1a:
            if (r0 != 0) goto L30
            com.fasterxml.jackson.databind.introspect.AnnotatedConstructor r0 = new com.fasterxml.jackson.databind.introspect.AnnotatedConstructor
            java.lang.reflect.Constructor r1 = r9.a()
            java.lang.annotation.Annotation[] r9 = r9.b()
            we r9 = r8.q(r9)
            we[] r2 = com.fasterxml.jackson.databind.introspect.a.u0
            r0.<init>(r10, r1, r9, r2)
            return r0
        L30:
            java.lang.annotation.Annotation[][] r1 = r9.e()
            int r2 = r1.length
            if (r0 == r2) goto La8
            r2 = 0
            java.lang.Class r3 = r9.c()
            boolean r4 = r3.isEnum()
            r5 = 0
            if (r4 == 0) goto L58
            int r4 = r1.length
            r6 = 2
            int r4 = r4 + r6
            if (r0 != r4) goto L58
            int r2 = r1.length
            int r2 = r2 + r6
            java.lang.annotation.Annotation[][] r2 = new java.lang.annotation.Annotation[r2]
            int r3 = r1.length
            java.lang.System.arraycopy(r1, r5, r2, r6, r3)
            we[] r1 = r8.r(r2)
        L54:
            r7 = r2
            r2 = r1
            r1 = r7
            goto L70
        L58:
            boolean r3 = r3.isMemberClass()
            if (r3 == 0) goto L70
            int r3 = r1.length
            r4 = 1
            int r3 = r3 + r4
            if (r0 != r3) goto L70
            int r2 = r1.length
            int r2 = r2 + r4
            java.lang.annotation.Annotation[][] r2 = new java.lang.annotation.Annotation[r2]
            int r3 = r1.length
            java.lang.System.arraycopy(r1, r5, r2, r4, r3)
            we[] r1 = r8.r(r2)
            goto L54
        L70:
            if (r2 == 0) goto L73
            goto Lac
        L73:
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Internal error: constructor for "
            r2.append(r3)
            java.lang.Class r9 = r9.c()
            java.lang.String r9 = r9.getName()
            r2.append(r9)
            java.lang.String r9 = " has mismatch: "
            r2.append(r9)
            r2.append(r0)
            java.lang.String r9 = " parameters; "
            r2.append(r9)
            int r9 = r1.length
            r2.append(r9)
            java.lang.String r9 = " sets of annotations"
            r2.append(r9)
            java.lang.String r9 = r2.toString()
            r10.<init>(r9)
            throw r10
        La8:
            we[] r2 = r8.r(r1)
        Lac:
            com.fasterxml.jackson.databind.introspect.AnnotatedConstructor r0 = new com.fasterxml.jackson.databind.introspect.AnnotatedConstructor
            java.lang.reflect.Constructor r1 = r9.a()
            java.lang.annotation.Annotation[] r9 = r9.b()
            we r9 = r8.q(r9)
            r0.<init>(r10, r1, r9, r2)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fasterxml.jackson.databind.introspect.a.w(com.fasterxml.jackson.databind.util.c$b, com.fasterxml.jackson.databind.introspect.d):com.fasterxml.jackson.databind.introspect.AnnotatedConstructor");
    }

    public final we x() {
        return new we();
    }

    public final we[] y(int i) {
        if (i == 0) {
            return u0;
        }
        we[] weVarArr = new we[i];
        for (int i2 = 0; i2 < i; i2++) {
            weVarArr[i2] = x();
        }
        return weVarArr;
    }

    public Method[] z(Class<?> cls) {
        try {
            return com.fasterxml.jackson.databind.util.c.y(cls);
        } catch (NoClassDefFoundError e) {
            ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
            if (contextClassLoader != null) {
                try {
                    return contextClassLoader.loadClass(cls.getName()).getDeclaredMethods();
                } catch (ClassNotFoundException unused) {
                    throw e;
                }
            }
            throw e;
        }
    }

    public a(a aVar, we weVar) {
        this.a = aVar.a;
        this.f0 = aVar.f0;
        this.g0 = aVar.g0;
        this.h0 = aVar.h0;
        this.i0 = aVar.i0;
        this.j0 = aVar.j0;
        this.k0 = aVar.k0;
        this.l0 = aVar.l0;
        this.m0 = weVar;
    }
}
