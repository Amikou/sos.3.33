package com.fasterxml.jackson.databind.introspect;

import com.fasterxml.jackson.databind.introspect.c;
import com.fasterxml.jackson.databind.type.ClassKey;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/* loaded from: classes.dex */
public class SimpleMixInResolver implements c.a, Serializable {
    private static final long serialVersionUID = 1;
    public Map<ClassKey, Class<?>> _localMixIns;
    public final c.a _overrides;

    public SimpleMixInResolver(c.a aVar) {
        this._overrides = aVar;
    }

    public void addLocalDefinition(Class<?> cls, Class<?> cls2) {
        if (this._localMixIns == null) {
            this._localMixIns = new HashMap();
        }
        this._localMixIns.put(new ClassKey(cls), cls2);
    }

    @Override // com.fasterxml.jackson.databind.introspect.c.a
    public Class<?> findMixInClassFor(Class<?> cls) {
        Map<ClassKey, Class<?>> map;
        c.a aVar = this._overrides;
        Class<?> findMixInClassFor = aVar == null ? null : aVar.findMixInClassFor(cls);
        return (findMixInClassFor != null || (map = this._localMixIns) == null) ? findMixInClassFor : map.get(new ClassKey(cls));
    }

    public int localSize() {
        Map<ClassKey, Class<?>> map = this._localMixIns;
        if (map == null) {
            return 0;
        }
        return map.size();
    }

    public void setLocalDefinitions(Map<Class<?>, Class<?>> map) {
        if (map != null && !map.isEmpty()) {
            HashMap hashMap = new HashMap(map.size());
            for (Map.Entry<Class<?>, Class<?>> entry : map.entrySet()) {
                hashMap.put(new ClassKey(entry.getKey()), entry.getValue());
            }
            this._localMixIns = hashMap;
            return;
        }
        this._localMixIns = null;
    }

    public SimpleMixInResolver withOverrides(c.a aVar) {
        return new SimpleMixInResolver(aVar, this._localMixIns);
    }

    public SimpleMixInResolver withoutLocalDefinitions() {
        return new SimpleMixInResolver(this._overrides, null);
    }

    @Override // com.fasterxml.jackson.databind.introspect.c.a
    public SimpleMixInResolver copy() {
        c.a aVar = this._overrides;
        return new SimpleMixInResolver(aVar == null ? null : aVar.copy(), this._localMixIns != null ? new HashMap(this._localMixIns) : null);
    }

    public SimpleMixInResolver(c.a aVar, Map<ClassKey, Class<?>> map) {
        this._overrides = aVar;
        this._localMixIns = map;
    }
}
