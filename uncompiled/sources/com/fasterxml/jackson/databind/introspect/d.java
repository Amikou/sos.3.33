package com.fasterxml.jackson.databind.introspect;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.type.TypeBindings;
import com.fasterxml.jackson.databind.type.TypeFactory;
import java.lang.reflect.Type;

/* compiled from: TypeResolutionContext.java */
/* loaded from: classes.dex */
public interface d {

    /* compiled from: TypeResolutionContext.java */
    /* loaded from: classes.dex */
    public static class a implements d {
        public final TypeFactory a;
        public final TypeBindings f0;

        public a(TypeFactory typeFactory, TypeBindings typeBindings) {
            this.a = typeFactory;
            this.f0 = typeBindings;
        }

        @Override // com.fasterxml.jackson.databind.introspect.d
        public JavaType a(Type type) {
            return this.a.constructType(type, this.f0);
        }
    }

    JavaType a(Type type);
}
