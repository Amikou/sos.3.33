package com.fasterxml.jackson.databind.introspect;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators$None;
import com.fasterxml.jackson.annotation.e;
import com.fasterxml.jackson.annotation.g;
import com.fasterxml.jackson.annotation.h;
import com.fasterxml.jackson.annotation.i;
import com.fasterxml.jackson.annotation.j;
import com.fasterxml.jackson.annotation.k;
import com.fasterxml.jackson.annotation.l;
import com.fasterxml.jackson.annotation.m;
import com.fasterxml.jackson.annotation.n;
import com.fasterxml.jackson.annotation.o;
import com.fasterxml.jackson.annotation.p;
import com.fasterxml.jackson.annotation.q;
import com.fasterxml.jackson.annotation.r;
import com.fasterxml.jackson.annotation.s;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.PropertyMetadata;
import com.fasterxml.jackson.databind.PropertyName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.a;
import com.fasterxml.jackson.databind.annotation.d;
import com.fasterxml.jackson.databind.c;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.databind.f;
import com.fasterxml.jackson.databind.g;
import com.fasterxml.jackson.databind.jsontype.NamedType;
import com.fasterxml.jackson.databind.ser.BeanPropertyWriter;
import com.fasterxml.jackson.databind.ser.VirtualBeanPropertyWriter;
import com.fasterxml.jackson.databind.ser.impl.AttributePropertyWriter;
import com.fasterxml.jackson.databind.ser.std.RawSerializer;
import com.fasterxml.jackson.databind.util.LRUMap;
import com.fasterxml.jackson.databind.util.NameTransformer;
import defpackage.o80;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/* loaded from: classes.dex */
public class JacksonAnnotationIntrospector extends AnnotationIntrospector {
    public static final Class<? extends Annotation>[] a = {JsonSerialize.class, s.class, JsonFormat.class, JsonTypeInfo.class, l.class, q.class, com.fasterxml.jackson.annotation.b.class, i.class};
    public static final Class<? extends Annotation>[] f0 = {com.fasterxml.jackson.databind.annotation.b.class, s.class, JsonFormat.class, JsonTypeInfo.class, q.class, com.fasterxml.jackson.annotation.b.class, i.class};
    public static final qt1 g0;
    private static final long serialVersionUID = 1;
    public transient LRUMap<Class<?>, Boolean> _annotationsInside = new LRUMap<>(48, 48);
    public boolean _cfgConstructorPropertiesImpliesCreator = true;

    /* loaded from: classes.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[JsonSerialize.Inclusion.values().length];
            a = iArr;
            try {
                iArr[JsonSerialize.Inclusion.ALWAYS.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[JsonSerialize.Inclusion.NON_NULL.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[JsonSerialize.Inclusion.NON_DEFAULT.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                a[JsonSerialize.Inclusion.NON_EMPTY.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                a[JsonSerialize.Inclusion.DEFAULT_INCLUSION.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
        }
    }

    static {
        qt1 qt1Var;
        try {
            qt1Var = qt1.f();
        } catch (Throwable unused) {
            qt1Var = null;
        }
        g0 = qt1Var;
    }

    public Class<?> _classIfExplicit(Class<?> cls) {
        if (cls == null || com.fasterxml.jackson.databind.util.c.G(cls)) {
            return null;
        }
        return cls;
    }

    public pt3 _constructNoTypeResolverBuilder() {
        return pt3.noTypeInfoBuilder();
    }

    public pt3 _constructStdTypeResolverBuilder() {
        return new pt3();
    }

    public BeanPropertyWriter _constructVirtualProperty(a.InterfaceC0092a interfaceC0092a, MapperConfig<?> mapperConfig, com.fasterxml.jackson.databind.introspect.a aVar, JavaType javaType) {
        PropertyMetadata propertyMetadata = interfaceC0092a.required() ? PropertyMetadata.STD_REQUIRED : PropertyMetadata.STD_OPTIONAL;
        String value = interfaceC0092a.value();
        PropertyName _propertyName = _propertyName(interfaceC0092a.propName(), interfaceC0092a.propNamespace());
        if (!_propertyName.hasSimpleName()) {
            _propertyName = PropertyName.construct(value);
        }
        return AttributePropertyWriter.construct(value, wo3.K(mapperConfig, new VirtualAnnotatedMember(aVar, aVar.getRawType(), value, javaType), _propertyName, propertyMetadata, interfaceC0092a.include()), aVar.O(), javaType);
    }

    public PropertyName _findConstructorName(ue ueVar) {
        qt1 qt1Var;
        PropertyName a2;
        if (ueVar instanceof AnnotatedParameter) {
            AnnotatedParameter annotatedParameter = (AnnotatedParameter) ueVar;
            if (annotatedParameter.getOwner() == null || (qt1Var = g0) == null || (a2 = qt1Var.a(annotatedParameter)) == null) {
                return null;
            }
            return a2;
        }
        return null;
    }

    /* JADX WARN: Type inference failed for: r5v3, types: [vd4] */
    public vd4<?> _findTypeResolver(MapperConfig<?> mapperConfig, ue ueVar, JavaType javaType) {
        vd4<?> _constructStdTypeResolverBuilder;
        JsonTypeInfo jsonTypeInfo = (JsonTypeInfo) _findAnnotation(ueVar, JsonTypeInfo.class);
        hw1 hw1Var = (hw1) _findAnnotation(ueVar, hw1.class);
        if (hw1Var != null) {
            if (jsonTypeInfo == null) {
                return null;
            }
            _constructStdTypeResolverBuilder = mapperConfig.typeResolverBuilderInstance(ueVar, hw1Var.value());
        } else if (jsonTypeInfo == null) {
            return null;
        } else {
            if (jsonTypeInfo.use() == JsonTypeInfo.Id.NONE) {
                return _constructNoTypeResolverBuilder();
            }
            _constructStdTypeResolverBuilder = _constructStdTypeResolverBuilder();
        }
        gw1 gw1Var = (gw1) _findAnnotation(ueVar, gw1.class);
        com.fasterxml.jackson.databind.jsontype.b typeIdResolverInstance = gw1Var != null ? mapperConfig.typeIdResolverInstance(ueVar, gw1Var.value()) : null;
        if (typeIdResolverInstance != null) {
            typeIdResolverInstance.c(javaType);
        }
        ?? init = _constructStdTypeResolverBuilder.init(jsonTypeInfo.use(), typeIdResolverInstance);
        JsonTypeInfo.As include = jsonTypeInfo.include();
        if (include == JsonTypeInfo.As.EXTERNAL_PROPERTY && (ueVar instanceof com.fasterxml.jackson.databind.introspect.a)) {
            include = JsonTypeInfo.As.PROPERTY;
        }
        vd4 typeProperty = init.inclusion(include).typeProperty(jsonTypeInfo.property());
        Class<?> defaultImpl = jsonTypeInfo.defaultImpl();
        if (defaultImpl != JsonTypeInfo.a.class && !defaultImpl.isAnnotation()) {
            typeProperty = typeProperty.defaultImpl(defaultImpl);
        }
        return typeProperty.typeIdVisibility(jsonTypeInfo.visible());
    }

    public boolean _isIgnorable(ue ueVar) {
        Boolean b;
        g gVar = (g) _findAnnotation(ueVar, g.class);
        if (gVar != null) {
            return gVar.value();
        }
        qt1 qt1Var = g0;
        if (qt1Var == null || (b = qt1Var.b(ueVar)) == null) {
            return false;
        }
        return b.booleanValue();
    }

    public PropertyName _propertyName(String str, String str2) {
        if (str.isEmpty()) {
            return PropertyName.USE_DEFAULT;
        }
        if (str2 != null && !str2.isEmpty()) {
            return PropertyName.construct(str, str2);
        }
        return PropertyName.construct(str);
    }

    public final Boolean a(ue ueVar) {
        k kVar = (k) _findAnnotation(ueVar, k.class);
        if (kVar == null || !kVar.alphabetic()) {
            return null;
        }
        return Boolean.TRUE;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public void findAndAddVirtualProperties(MapperConfig<?> mapperConfig, com.fasterxml.jackson.databind.introspect.a aVar, List<BeanPropertyWriter> list) {
        com.fasterxml.jackson.databind.annotation.a aVar2 = (com.fasterxml.jackson.databind.annotation.a) _findAnnotation(aVar, com.fasterxml.jackson.databind.annotation.a.class);
        if (aVar2 == null) {
            return;
        }
        boolean prepend = aVar2.prepend();
        JavaType javaType = null;
        a.InterfaceC0092a[] attrs = aVar2.attrs();
        int length = attrs.length;
        for (int i = 0; i < length; i++) {
            if (javaType == null) {
                javaType = mapperConfig.constructType(Object.class);
            }
            BeanPropertyWriter _constructVirtualProperty = _constructVirtualProperty(attrs[i], mapperConfig, aVar, javaType);
            if (prepend) {
                list.add(i, _constructVirtualProperty);
            } else {
                list.add(_constructVirtualProperty);
            }
        }
        a.b[] props = aVar2.props();
        int length2 = props.length;
        for (int i2 = 0; i2 < length2; i2++) {
            BeanPropertyWriter _constructVirtualProperty2 = _constructVirtualProperty(props[i2], mapperConfig, aVar);
            if (prepend) {
                list.add(i2, _constructVirtualProperty2);
            } else {
                list.add(_constructVirtualProperty2);
            }
        }
    }

    /* JADX WARN: Type inference failed for: r3v1, types: [com.fasterxml.jackson.databind.introspect.VisibilityChecker, com.fasterxml.jackson.databind.introspect.VisibilityChecker<?>] */
    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public VisibilityChecker<?> findAutoDetectVisibility(com.fasterxml.jackson.databind.introspect.a aVar, VisibilityChecker<?> visibilityChecker) {
        JsonAutoDetect jsonAutoDetect = (JsonAutoDetect) _findAnnotation(aVar, JsonAutoDetect.class);
        return jsonAutoDetect == null ? visibilityChecker : visibilityChecker.with(jsonAutoDetect);
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public String findClassDescription(com.fasterxml.jackson.databind.introspect.a aVar) {
        com.fasterxml.jackson.annotation.c cVar = (com.fasterxml.jackson.annotation.c) _findAnnotation(aVar, com.fasterxml.jackson.annotation.c.class);
        if (cVar == null) {
            return null;
        }
        return cVar.value();
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Object findContentDeserializer(ue ueVar) {
        Class<? extends com.fasterxml.jackson.databind.c> contentUsing;
        com.fasterxml.jackson.databind.annotation.b bVar = (com.fasterxml.jackson.databind.annotation.b) _findAnnotation(ueVar, com.fasterxml.jackson.databind.annotation.b.class);
        if (bVar == null || (contentUsing = bVar.contentUsing()) == c.a.class) {
            return null;
        }
        return contentUsing;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Object findContentSerializer(ue ueVar) {
        Class<? extends f> contentUsing;
        JsonSerialize jsonSerialize = (JsonSerialize) _findAnnotation(ueVar, JsonSerialize.class);
        if (jsonSerialize == null || (contentUsing = jsonSerialize.contentUsing()) == f.a.class) {
            return null;
        }
        return contentUsing;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public JsonCreator.Mode findCreatorBinding(ue ueVar) {
        JsonCreator jsonCreator = (JsonCreator) _findAnnotation(ueVar, JsonCreator.class);
        if (jsonCreator == null) {
            return null;
        }
        return jsonCreator.mode();
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Enum<?> findDefaultEnumValue(Class<Enum<?>> cls) {
        return com.fasterxml.jackson.databind.util.c.r(cls, xu1.class);
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Object findDeserializationContentConverter(AnnotatedMember annotatedMember) {
        com.fasterxml.jackson.databind.annotation.b bVar = (com.fasterxml.jackson.databind.annotation.b) _findAnnotation(annotatedMember, com.fasterxml.jackson.databind.annotation.b.class);
        if (bVar == null) {
            return null;
        }
        return _classIfExplicit(bVar.contentConverter(), o80.a.class);
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    @Deprecated
    public Class<?> findDeserializationContentType(ue ueVar, JavaType javaType) {
        com.fasterxml.jackson.databind.annotation.b bVar = (com.fasterxml.jackson.databind.annotation.b) _findAnnotation(ueVar, com.fasterxml.jackson.databind.annotation.b.class);
        if (bVar == null) {
            return null;
        }
        return _classIfExplicit(bVar.contentAs());
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Object findDeserializationConverter(ue ueVar) {
        com.fasterxml.jackson.databind.annotation.b bVar = (com.fasterxml.jackson.databind.annotation.b) _findAnnotation(ueVar, com.fasterxml.jackson.databind.annotation.b.class);
        if (bVar == null) {
            return null;
        }
        return _classIfExplicit(bVar.converter(), o80.a.class);
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    @Deprecated
    public Class<?> findDeserializationKeyType(ue ueVar, JavaType javaType) {
        com.fasterxml.jackson.databind.annotation.b bVar = (com.fasterxml.jackson.databind.annotation.b) _findAnnotation(ueVar, com.fasterxml.jackson.databind.annotation.b.class);
        if (bVar == null) {
            return null;
        }
        return _classIfExplicit(bVar.keyAs());
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    @Deprecated
    public Class<?> findDeserializationType(ue ueVar, JavaType javaType) {
        com.fasterxml.jackson.databind.annotation.b bVar = (com.fasterxml.jackson.databind.annotation.b) _findAnnotation(ueVar, com.fasterxml.jackson.databind.annotation.b.class);
        if (bVar == null) {
            return null;
        }
        return _classIfExplicit(bVar.as());
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Object findDeserializer(ue ueVar) {
        Class<? extends com.fasterxml.jackson.databind.c> using;
        com.fasterxml.jackson.databind.annotation.b bVar = (com.fasterxml.jackson.databind.annotation.b) _findAnnotation(ueVar, com.fasterxml.jackson.databind.annotation.b.class);
        if (bVar == null || (using = bVar.using()) == c.a.class) {
            return null;
        }
        return using;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    @Deprecated
    public String findEnumValue(Enum<?> r3) {
        JsonProperty jsonProperty;
        String value;
        try {
            Field field = r3.getClass().getField(r3.name());
            if (field != null && (jsonProperty = (JsonProperty) field.getAnnotation(JsonProperty.class)) != null && (value = jsonProperty.value()) != null) {
                if (!value.isEmpty()) {
                    return value;
                }
            }
        } catch (NoSuchFieldException | SecurityException unused) {
        }
        return r3.name();
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public String[] findEnumValues(Class<?> cls, Enum<?>[] enumArr, String[] strArr) {
        Field[] x;
        JsonProperty jsonProperty;
        HashMap hashMap = null;
        for (Field field : com.fasterxml.jackson.databind.util.c.x(cls)) {
            if (field.isEnumConstant() && (jsonProperty = (JsonProperty) field.getAnnotation(JsonProperty.class)) != null) {
                String value = jsonProperty.value();
                if (!value.isEmpty()) {
                    if (hashMap == null) {
                        hashMap = new HashMap();
                    }
                    hashMap.put(field.getName(), value);
                }
            }
        }
        if (hashMap != null) {
            int length = enumArr.length;
            for (int i = 0; i < length; i++) {
                String str = (String) hashMap.get(enumArr[i].name());
                if (str != null) {
                    strArr[i] = str;
                }
            }
        }
        return strArr;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Object findFilterId(ue ueVar) {
        yu1 yu1Var = (yu1) _findAnnotation(ueVar, yu1.class);
        if (yu1Var != null) {
            String value = yu1Var.value();
            if (value.length() > 0) {
                return value;
            }
            return null;
        }
        return null;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public JsonFormat.Value findFormat(ue ueVar) {
        JsonFormat jsonFormat = (JsonFormat) _findAnnotation(ueVar, JsonFormat.class);
        if (jsonFormat == null) {
            return null;
        }
        return new JsonFormat.Value(jsonFormat);
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    @Deprecated
    public Boolean findIgnoreUnknownProperties(com.fasterxml.jackson.databind.introspect.a aVar) {
        JsonIgnoreProperties.Value findPropertyIgnorals = findPropertyIgnorals(aVar);
        if (findPropertyIgnorals == null) {
            return null;
        }
        return Boolean.valueOf(findPropertyIgnorals.getIgnoreUnknown());
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public String findImplicitPropertyName(AnnotatedMember annotatedMember) {
        PropertyName _findConstructorName = _findConstructorName(annotatedMember);
        if (_findConstructorName == null) {
            return null;
        }
        return _findConstructorName.getSimpleName();
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Object findInjectableValueId(AnnotatedMember annotatedMember) {
        com.fasterxml.jackson.annotation.a aVar = (com.fasterxml.jackson.annotation.a) _findAnnotation(annotatedMember, com.fasterxml.jackson.annotation.a.class);
        if (aVar == null) {
            return null;
        }
        String value = aVar.value();
        if (value.length() == 0) {
            if (!(annotatedMember instanceof AnnotatedMethod)) {
                return annotatedMember.getRawType().getName();
            }
            AnnotatedMethod annotatedMethod = (AnnotatedMethod) annotatedMember;
            if (annotatedMethod.getParameterCount() == 0) {
                return annotatedMember.getRawType().getName();
            }
            return annotatedMethod.getRawParameterType(0).getName();
        }
        return value;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Object findKeyDeserializer(ue ueVar) {
        Class<? extends com.fasterxml.jackson.databind.g> keyUsing;
        com.fasterxml.jackson.databind.annotation.b bVar = (com.fasterxml.jackson.databind.annotation.b) _findAnnotation(ueVar, com.fasterxml.jackson.databind.annotation.b.class);
        if (bVar == null || (keyUsing = bVar.keyUsing()) == g.a.class) {
            return null;
        }
        return keyUsing;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Object findKeySerializer(ue ueVar) {
        Class<? extends f> keyUsing;
        JsonSerialize jsonSerialize = (JsonSerialize) _findAnnotation(ueVar, JsonSerialize.class);
        if (jsonSerialize == null || (keyUsing = jsonSerialize.keyUsing()) == f.a.class) {
            return null;
        }
        return keyUsing;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public PropertyName findNameForDeserialization(ue ueVar) {
        n nVar = (n) _findAnnotation(ueVar, n.class);
        if (nVar != null) {
            return PropertyName.construct(nVar.value());
        }
        JsonProperty jsonProperty = (JsonProperty) _findAnnotation(ueVar, JsonProperty.class);
        if (jsonProperty != null) {
            return PropertyName.construct(jsonProperty.value());
        }
        if (_hasOneOf(ueVar, f0)) {
            return PropertyName.USE_DEFAULT;
        }
        return null;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public PropertyName findNameForSerialization(ue ueVar) {
        com.fasterxml.jackson.annotation.d dVar = (com.fasterxml.jackson.annotation.d) _findAnnotation(ueVar, com.fasterxml.jackson.annotation.d.class);
        if (dVar != null) {
            return PropertyName.construct(dVar.value());
        }
        JsonProperty jsonProperty = (JsonProperty) _findAnnotation(ueVar, JsonProperty.class);
        if (jsonProperty != null) {
            return PropertyName.construct(jsonProperty.value());
        }
        if (_hasOneOf(ueVar, a)) {
            return PropertyName.USE_DEFAULT;
        }
        return null;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Object findNamingStrategy(com.fasterxml.jackson.databind.introspect.a aVar) {
        com.fasterxml.jackson.databind.annotation.c cVar = (com.fasterxml.jackson.databind.annotation.c) _findAnnotation(aVar, com.fasterxml.jackson.databind.annotation.c.class);
        if (cVar == null) {
            return null;
        }
        return cVar.value();
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Object findNullSerializer(ue ueVar) {
        Class<? extends f> nullsUsing;
        JsonSerialize jsonSerialize = (JsonSerialize) _findAnnotation(ueVar, JsonSerialize.class);
        if (jsonSerialize == null || (nullsUsing = jsonSerialize.nullsUsing()) == f.a.class) {
            return null;
        }
        return nullsUsing;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public jl2 findObjectIdInfo(ue ueVar) {
        e eVar = (e) _findAnnotation(ueVar, e.class);
        if (eVar == null || eVar.generator() == ObjectIdGenerators$None.class) {
            return null;
        }
        return new jl2(PropertyName.construct(eVar.property()), eVar.scope(), eVar.generator(), eVar.resolver());
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public jl2 findObjectReferenceInfo(ue ueVar, jl2 jl2Var) {
        com.fasterxml.jackson.annotation.f fVar = (com.fasterxml.jackson.annotation.f) _findAnnotation(ueVar, com.fasterxml.jackson.annotation.f.class);
        if (fVar == null) {
            return jl2Var;
        }
        if (jl2Var == null) {
            jl2Var = jl2.a();
        }
        return jl2Var.g(fVar.alwaysAsId());
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Class<?> findPOJOBuilder(com.fasterxml.jackson.databind.introspect.a aVar) {
        com.fasterxml.jackson.databind.annotation.b bVar = (com.fasterxml.jackson.databind.annotation.b) _findAnnotation(aVar, com.fasterxml.jackson.databind.annotation.b.class);
        if (bVar == null) {
            return null;
        }
        return _classIfExplicit(bVar.builder());
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public d.a findPOJOBuilderConfig(com.fasterxml.jackson.databind.introspect.a aVar) {
        com.fasterxml.jackson.databind.annotation.d dVar = (com.fasterxml.jackson.databind.annotation.d) _findAnnotation(aVar, com.fasterxml.jackson.databind.annotation.d.class);
        if (dVar == null) {
            return null;
        }
        return new d.a(dVar);
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    @Deprecated
    public String[] findPropertiesToIgnore(ue ueVar, boolean z) {
        JsonIgnoreProperties.Value findPropertyIgnorals = findPropertyIgnorals(ueVar);
        if (findPropertyIgnorals == null) {
            return null;
        }
        if (z) {
            if (findPropertyIgnorals.getAllowGetters()) {
                return null;
            }
        } else if (findPropertyIgnorals.getAllowSetters()) {
            return null;
        }
        Set<String> ignored = findPropertyIgnorals.getIgnored();
        return (String[]) ignored.toArray(new String[ignored.size()]);
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public JsonProperty.Access findPropertyAccess(ue ueVar) {
        JsonProperty jsonProperty = (JsonProperty) _findAnnotation(ueVar, JsonProperty.class);
        if (jsonProperty != null) {
            return jsonProperty.access();
        }
        return null;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public vd4<?> findPropertyContentTypeResolver(MapperConfig<?> mapperConfig, AnnotatedMember annotatedMember, JavaType javaType) {
        if (javaType.getContentType() != null) {
            return _findTypeResolver(mapperConfig, annotatedMember, javaType);
        }
        throw new IllegalArgumentException("Must call method with a container or reference type (got " + javaType + ")");
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public String findPropertyDefaultValue(ue ueVar) {
        JsonProperty jsonProperty = (JsonProperty) _findAnnotation(ueVar, JsonProperty.class);
        if (jsonProperty == null) {
            return null;
        }
        String defaultValue = jsonProperty.defaultValue();
        if (defaultValue.isEmpty()) {
            return null;
        }
        return defaultValue;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public String findPropertyDescription(ue ueVar) {
        j jVar = (j) _findAnnotation(ueVar, j.class);
        if (jVar == null) {
            return null;
        }
        return jVar.value();
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public JsonIgnoreProperties.Value findPropertyIgnorals(ue ueVar) {
        JsonIgnoreProperties jsonIgnoreProperties = (JsonIgnoreProperties) _findAnnotation(ueVar, JsonIgnoreProperties.class);
        if (jsonIgnoreProperties == null) {
            return null;
        }
        return JsonIgnoreProperties.Value.from(jsonIgnoreProperties);
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public JsonInclude.Value findPropertyInclusion(ue ueVar) {
        JsonSerialize jsonSerialize;
        JsonInclude jsonInclude = (JsonInclude) _findAnnotation(ueVar, JsonInclude.class);
        JsonInclude.Include value = jsonInclude == null ? JsonInclude.Include.USE_DEFAULTS : jsonInclude.value();
        JsonInclude.Include include = JsonInclude.Include.USE_DEFAULTS;
        if (value == include && (jsonSerialize = (JsonSerialize) _findAnnotation(ueVar, JsonSerialize.class)) != null) {
            int i = a.a[jsonSerialize.include().ordinal()];
            if (i == 1) {
                value = JsonInclude.Include.ALWAYS;
            } else if (i == 2) {
                value = JsonInclude.Include.NON_NULL;
            } else if (i == 3) {
                value = JsonInclude.Include.NON_DEFAULT;
            } else if (i == 4) {
                value = JsonInclude.Include.NON_EMPTY;
            }
        }
        if (jsonInclude != null) {
            include = jsonInclude.content();
        }
        return JsonInclude.Value.construct(value, include);
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Integer findPropertyIndex(ue ueVar) {
        int index;
        JsonProperty jsonProperty = (JsonProperty) _findAnnotation(ueVar, JsonProperty.class);
        if (jsonProperty == null || (index = jsonProperty.index()) == -1) {
            return null;
        }
        return Integer.valueOf(index);
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public vd4<?> findPropertyTypeResolver(MapperConfig<?> mapperConfig, AnnotatedMember annotatedMember, JavaType javaType) {
        if (javaType.isContainerType() || javaType.isReferenceType()) {
            return null;
        }
        return _findTypeResolver(mapperConfig, annotatedMember, javaType);
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public AnnotationIntrospector.ReferenceProperty findReferenceType(AnnotatedMember annotatedMember) {
        i iVar = (i) _findAnnotation(annotatedMember, i.class);
        if (iVar != null) {
            return AnnotationIntrospector.ReferenceProperty.e(iVar.value());
        }
        com.fasterxml.jackson.annotation.b bVar = (com.fasterxml.jackson.annotation.b) _findAnnotation(annotatedMember, com.fasterxml.jackson.annotation.b.class);
        if (bVar != null) {
            return AnnotationIntrospector.ReferenceProperty.a(bVar.value());
        }
        return null;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public PropertyName findRootName(com.fasterxml.jackson.databind.introspect.a aVar) {
        m mVar = (m) _findAnnotation(aVar, m.class);
        String str = null;
        if (mVar == null) {
            return null;
        }
        String namespace = mVar.namespace();
        if (namespace == null || namespace.length() != 0) {
            str = namespace;
        }
        return PropertyName.construct(mVar.value(), str);
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Object findSerializationContentConverter(AnnotatedMember annotatedMember) {
        JsonSerialize jsonSerialize = (JsonSerialize) _findAnnotation(annotatedMember, JsonSerialize.class);
        if (jsonSerialize == null) {
            return null;
        }
        return _classIfExplicit(jsonSerialize.contentConverter(), o80.a.class);
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    @Deprecated
    public Class<?> findSerializationContentType(ue ueVar, JavaType javaType) {
        JsonSerialize jsonSerialize = (JsonSerialize) _findAnnotation(ueVar, JsonSerialize.class);
        if (jsonSerialize == null) {
            return null;
        }
        return _classIfExplicit(jsonSerialize.contentAs());
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Object findSerializationConverter(ue ueVar) {
        JsonSerialize jsonSerialize = (JsonSerialize) _findAnnotation(ueVar, JsonSerialize.class);
        if (jsonSerialize == null) {
            return null;
        }
        return _classIfExplicit(jsonSerialize.converter(), o80.a.class);
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public JsonInclude.Include findSerializationInclusion(ue ueVar, JsonInclude.Include include) {
        JsonInclude.Include value;
        JsonInclude jsonInclude = (JsonInclude) _findAnnotation(ueVar, JsonInclude.class);
        if (jsonInclude == null || (value = jsonInclude.value()) == JsonInclude.Include.USE_DEFAULTS) {
            JsonSerialize jsonSerialize = (JsonSerialize) _findAnnotation(ueVar, JsonSerialize.class);
            if (jsonSerialize != null) {
                int i = a.a[jsonSerialize.include().ordinal()];
                if (i == 1) {
                    return JsonInclude.Include.ALWAYS;
                }
                if (i == 2) {
                    return JsonInclude.Include.NON_NULL;
                }
                if (i == 3) {
                    return JsonInclude.Include.NON_DEFAULT;
                }
                if (i == 4) {
                    return JsonInclude.Include.NON_EMPTY;
                }
            }
            return include;
        }
        return value;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    @Deprecated
    public JsonInclude.Include findSerializationInclusionForContent(ue ueVar, JsonInclude.Include include) {
        JsonInclude.Include content;
        JsonInclude jsonInclude = (JsonInclude) _findAnnotation(ueVar, JsonInclude.class);
        return (jsonInclude == null || (content = jsonInclude.content()) == JsonInclude.Include.USE_DEFAULTS) ? include : content;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    @Deprecated
    public Class<?> findSerializationKeyType(ue ueVar, JavaType javaType) {
        JsonSerialize jsonSerialize = (JsonSerialize) _findAnnotation(ueVar, JsonSerialize.class);
        if (jsonSerialize == null) {
            return null;
        }
        return _classIfExplicit(jsonSerialize.keyAs());
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public String[] findSerializationPropertyOrder(com.fasterxml.jackson.databind.introspect.a aVar) {
        k kVar = (k) _findAnnotation(aVar, k.class);
        if (kVar == null) {
            return null;
        }
        return kVar.value();
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Boolean findSerializationSortAlphabetically(ue ueVar) {
        return a(ueVar);
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    @Deprecated
    public Class<?> findSerializationType(ue ueVar) {
        JsonSerialize jsonSerialize = (JsonSerialize) _findAnnotation(ueVar, JsonSerialize.class);
        if (jsonSerialize == null) {
            return null;
        }
        return _classIfExplicit(jsonSerialize.as());
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public JsonSerialize.Typing findSerializationTyping(ue ueVar) {
        JsonSerialize jsonSerialize = (JsonSerialize) _findAnnotation(ueVar, JsonSerialize.class);
        if (jsonSerialize == null) {
            return null;
        }
        return jsonSerialize.typing();
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Object findSerializer(ue ueVar) {
        Class<? extends f> using;
        JsonSerialize jsonSerialize = (JsonSerialize) _findAnnotation(ueVar, JsonSerialize.class);
        if (jsonSerialize == null || (using = jsonSerialize.using()) == f.a.class) {
            l lVar = (l) _findAnnotation(ueVar, l.class);
            if (lVar == null || !lVar.value()) {
                return null;
            }
            return new RawSerializer(ueVar.getRawType());
        }
        return using;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public List<NamedType> findSubtypes(ue ueVar) {
        o oVar = (o) _findAnnotation(ueVar, o.class);
        if (oVar == null) {
            return null;
        }
        o.a[] value = oVar.value();
        ArrayList arrayList = new ArrayList(value.length);
        for (o.a aVar : value) {
            arrayList.add(new NamedType(aVar.value(), aVar.name()));
        }
        return arrayList;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public String findTypeName(com.fasterxml.jackson.databind.introspect.a aVar) {
        p pVar = (p) _findAnnotation(aVar, p.class);
        if (pVar == null) {
            return null;
        }
        return pVar.value();
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public vd4<?> findTypeResolver(MapperConfig<?> mapperConfig, com.fasterxml.jackson.databind.introspect.a aVar, JavaType javaType) {
        return _findTypeResolver(mapperConfig, aVar, javaType);
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public NameTransformer findUnwrappingNameTransformer(AnnotatedMember annotatedMember) {
        q qVar = (q) _findAnnotation(annotatedMember, q.class);
        if (qVar == null || !qVar.enabled()) {
            return null;
        }
        return NameTransformer.simpleTransformer(qVar.prefix(), qVar.suffix());
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Object findValueInstantiator(com.fasterxml.jackson.databind.introspect.a aVar) {
        kw1 kw1Var = (kw1) _findAnnotation(aVar, kw1.class);
        if (kw1Var == null) {
            return null;
        }
        return kw1Var.value();
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Class<?>[] findViews(ue ueVar) {
        s sVar = (s) _findAnnotation(ueVar, s.class);
        if (sVar == null) {
            return null;
        }
        return sVar.value();
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public boolean hasAnyGetterAnnotation(AnnotatedMethod annotatedMethod) {
        return _hasAnnotation(annotatedMethod, qu1.class);
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public boolean hasAnySetterAnnotation(AnnotatedMethod annotatedMethod) {
        return _hasAnnotation(annotatedMethod, ru1.class);
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public boolean hasAsValueAnnotation(AnnotatedMethod annotatedMethod) {
        r rVar = (r) _findAnnotation(annotatedMethod, r.class);
        return rVar != null && rVar.value();
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public boolean hasCreatorAnnotation(ue ueVar) {
        qt1 qt1Var;
        Boolean e;
        JsonCreator jsonCreator = (JsonCreator) _findAnnotation(ueVar, JsonCreator.class);
        if (jsonCreator != null) {
            return jsonCreator.mode() != JsonCreator.Mode.DISABLED;
        } else if (!this._cfgConstructorPropertiesImpliesCreator || !(ueVar instanceof AnnotatedConstructor) || (qt1Var = g0) == null || (e = qt1Var.e(ueVar)) == null) {
            return false;
        } else {
            return e.booleanValue();
        }
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public boolean hasIgnoreMarker(AnnotatedMember annotatedMember) {
        return _isIgnorable(annotatedMember);
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Boolean hasRequiredMarker(AnnotatedMember annotatedMember) {
        JsonProperty jsonProperty = (JsonProperty) _findAnnotation(annotatedMember, JsonProperty.class);
        if (jsonProperty != null) {
            return Boolean.valueOf(jsonProperty.required());
        }
        return null;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public boolean isAnnotationBundle(Annotation annotation) {
        Class<? extends Annotation> annotationType = annotation.annotationType();
        Boolean bool = this._annotationsInside.get(annotationType);
        if (bool == null) {
            bool = Boolean.valueOf(annotationType.getAnnotation(ot1.class) != null);
            this._annotationsInside.putIfAbsent(annotationType, bool);
        }
        return bool.booleanValue();
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Boolean isIgnorableType(com.fasterxml.jackson.databind.introspect.a aVar) {
        h hVar = (h) _findAnnotation(aVar, h.class);
        if (hVar == null) {
            return null;
        }
        return Boolean.valueOf(hVar.value());
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Boolean isTypeId(AnnotatedMember annotatedMember) {
        return Boolean.valueOf(_hasAnnotation(annotatedMember, fw1.class));
    }

    public Object readResolve() {
        if (this._annotationsInside == null) {
            this._annotationsInside = new LRUMap<>(48, 48);
        }
        return this;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public AnnotatedMethod resolveSetterConflict(MapperConfig<?> mapperConfig, AnnotatedMethod annotatedMethod, AnnotatedMethod annotatedMethod2) {
        Class<?> rawParameterType = annotatedMethod.getRawParameterType(0);
        Class<?> rawParameterType2 = annotatedMethod2.getRawParameterType(0);
        if (rawParameterType.isPrimitive()) {
            if (!rawParameterType2.isPrimitive()) {
                return annotatedMethod;
            }
        } else if (rawParameterType2.isPrimitive()) {
            return annotatedMethod2;
        }
        if (rawParameterType == String.class) {
            if (rawParameterType2 != String.class) {
                return annotatedMethod;
            }
            return null;
        } else if (rawParameterType2 == String.class) {
            return annotatedMethod2;
        } else {
            return null;
        }
    }

    public JacksonAnnotationIntrospector setConstructorPropertiesImpliesCreator(boolean z) {
        this._cfgConstructorPropertiesImpliesCreator = z;
        return this;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Version version() {
        return wo2.a;
    }

    public Class<?> _classIfExplicit(Class<?> cls, Class<?> cls2) {
        Class<?> _classIfExplicit = _classIfExplicit(cls);
        if (_classIfExplicit == null || _classIfExplicit == cls2) {
            return null;
        }
        return _classIfExplicit;
    }

    public BeanPropertyWriter _constructVirtualProperty(a.b bVar, MapperConfig<?> mapperConfig, com.fasterxml.jackson.databind.introspect.a aVar) {
        PropertyMetadata propertyMetadata = bVar.required() ? PropertyMetadata.STD_REQUIRED : PropertyMetadata.STD_OPTIONAL;
        PropertyName _propertyName = _propertyName(bVar.name(), bVar.namespace());
        JavaType constructType = mapperConfig.constructType(bVar.type());
        wo3 K = wo3.K(mapperConfig, new VirtualAnnotatedMember(aVar, aVar.getRawType(), _propertyName.getSimpleName(), constructType), _propertyName, propertyMetadata, bVar.include());
        Class<? extends VirtualBeanPropertyWriter> value = bVar.value();
        mapperConfig.getHandlerInstantiator();
        return ((VirtualBeanPropertyWriter) com.fasterxml.jackson.databind.util.c.i(value, mapperConfig.canOverrideAccessModifiers())).withConfig(mapperConfig, aVar, K, constructType);
    }
}
