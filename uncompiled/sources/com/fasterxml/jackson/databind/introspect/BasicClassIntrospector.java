package com.fasterxml.jackson.databind.introspect;

import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.annotation.d;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.databind.introspect.c;
import com.fasterxml.jackson.databind.type.SimpleType;
import com.fasterxml.jackson.databind.util.LRUMap;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

/* loaded from: classes.dex */
public class BasicClassIntrospector extends c implements Serializable {
    public static final b BOOLEAN_DESC;
    public static final b INT_DESC;
    public static final b LONG_DESC;
    public static final b STRING_DESC = b.H(null, SimpleType.constructUnsafe(String.class), a.J(String.class, null));
    @Deprecated
    public static final BasicClassIntrospector instance;
    private static final long serialVersionUID = 1;
    public final LRUMap<JavaType, b> _cachedFCA = new LRUMap<>(16, 64);

    static {
        Class cls = Boolean.TYPE;
        BOOLEAN_DESC = b.H(null, SimpleType.constructUnsafe(cls), a.J(cls, null));
        Class cls2 = Integer.TYPE;
        INT_DESC = b.H(null, SimpleType.constructUnsafe(cls2), a.J(cls2, null));
        Class cls3 = Long.TYPE;
        LONG_DESC = b.H(null, SimpleType.constructUnsafe(cls3), a.J(cls3, null));
        instance = new BasicClassIntrospector();
    }

    public b _findStdJdkCollectionDesc(MapperConfig<?> mapperConfig, JavaType javaType) {
        if (_isStdJDKCollection(javaType)) {
            return b.H(mapperConfig, javaType, a.H(javaType, mapperConfig));
        }
        return null;
    }

    public b _findStdTypeDesc(JavaType javaType) {
        Class<?> rawClass = javaType.getRawClass();
        if (rawClass.isPrimitive()) {
            if (rawClass == Boolean.TYPE) {
                return BOOLEAN_DESC;
            }
            if (rawClass == Integer.TYPE) {
                return INT_DESC;
            }
            if (rawClass == Long.TYPE) {
                return LONG_DESC;
            }
            return null;
        } else if (rawClass == String.class) {
            return STRING_DESC;
        } else {
            return null;
        }
    }

    public boolean _isStdJDKCollection(JavaType javaType) {
        Class<?> rawClass;
        String D;
        return javaType.isContainerType() && !javaType.isArrayType() && (D = com.fasterxml.jackson.databind.util.c.D((rawClass = javaType.getRawClass()))) != null && (D.startsWith("java.lang") || D.startsWith("java.util")) && (Collection.class.isAssignableFrom(rawClass) || Map.class.isAssignableFrom(rawClass));
    }

    public mo2 collectProperties(MapperConfig<?> mapperConfig, JavaType javaType, c.a aVar, boolean z, String str) {
        return constructPropertyCollector(mapperConfig, a.I(javaType, mapperConfig, aVar), javaType, z, str);
    }

    public mo2 collectPropertiesWithBuilder(MapperConfig<?> mapperConfig, JavaType javaType, c.a aVar, boolean z) {
        AnnotationIntrospector annotationIntrospector = mapperConfig.isAnnotationProcessingEnabled() ? mapperConfig.getAnnotationIntrospector() : null;
        a I = a.I(javaType, mapperConfig, aVar);
        d.a findPOJOBuilderConfig = annotationIntrospector != null ? annotationIntrospector.findPOJOBuilderConfig(I) : null;
        return constructPropertyCollector(mapperConfig, I, javaType, z, findPOJOBuilderConfig == null ? "with" : findPOJOBuilderConfig.b);
    }

    public mo2 constructPropertyCollector(MapperConfig<?> mapperConfig, a aVar, JavaType javaType, boolean z, String str) {
        return new mo2(mapperConfig, z, javaType, aVar, str);
    }

    @Override // com.fasterxml.jackson.databind.introspect.c
    public /* bridge */ /* synthetic */ so forClassAnnotations(MapperConfig mapperConfig, JavaType javaType, c.a aVar) {
        return forClassAnnotations((MapperConfig<?>) mapperConfig, javaType, aVar);
    }

    @Override // com.fasterxml.jackson.databind.introspect.c
    public /* bridge */ /* synthetic */ so forDirectClassAnnotations(MapperConfig mapperConfig, JavaType javaType, c.a aVar) {
        return forDirectClassAnnotations((MapperConfig<?>) mapperConfig, javaType, aVar);
    }

    @Override // com.fasterxml.jackson.databind.introspect.c
    public b forClassAnnotations(MapperConfig<?> mapperConfig, JavaType javaType, c.a aVar) {
        b _findStdTypeDesc = _findStdTypeDesc(javaType);
        if (_findStdTypeDesc == null) {
            b bVar = this._cachedFCA.get(javaType);
            if (bVar == null) {
                b H = b.H(mapperConfig, javaType, a.I(javaType, mapperConfig, aVar));
                this._cachedFCA.put(javaType, H);
                return H;
            }
            return bVar;
        }
        return _findStdTypeDesc;
    }

    @Override // com.fasterxml.jackson.databind.introspect.c
    public b forCreation(DeserializationConfig deserializationConfig, JavaType javaType, c.a aVar) {
        b _findStdTypeDesc = _findStdTypeDesc(javaType);
        if (_findStdTypeDesc == null) {
            b _findStdJdkCollectionDesc = _findStdJdkCollectionDesc(deserializationConfig, javaType);
            return _findStdJdkCollectionDesc == null ? b.G(collectProperties(deserializationConfig, javaType, aVar, false, "set")) : _findStdJdkCollectionDesc;
        }
        return _findStdTypeDesc;
    }

    @Override // com.fasterxml.jackson.databind.introspect.c
    public b forDeserialization(DeserializationConfig deserializationConfig, JavaType javaType, c.a aVar) {
        b _findStdTypeDesc = _findStdTypeDesc(javaType);
        if (_findStdTypeDesc == null) {
            _findStdTypeDesc = _findStdJdkCollectionDesc(deserializationConfig, javaType);
            if (_findStdTypeDesc == null) {
                _findStdTypeDesc = b.G(collectProperties(deserializationConfig, javaType, aVar, false, "set"));
            }
            this._cachedFCA.putIfAbsent(javaType, _findStdTypeDesc);
        }
        return _findStdTypeDesc;
    }

    @Override // com.fasterxml.jackson.databind.introspect.c
    public b forDeserializationWithBuilder(DeserializationConfig deserializationConfig, JavaType javaType, c.a aVar) {
        b G = b.G(collectPropertiesWithBuilder(deserializationConfig, javaType, aVar, false));
        this._cachedFCA.putIfAbsent(javaType, G);
        return G;
    }

    @Override // com.fasterxml.jackson.databind.introspect.c
    public b forDirectClassAnnotations(MapperConfig<?> mapperConfig, JavaType javaType, c.a aVar) {
        b _findStdTypeDesc = _findStdTypeDesc(javaType);
        return _findStdTypeDesc == null ? b.H(mapperConfig, javaType, a.K(javaType.getRawClass(), mapperConfig, aVar)) : _findStdTypeDesc;
    }

    @Override // com.fasterxml.jackson.databind.introspect.c
    public b forSerialization(SerializationConfig serializationConfig, JavaType javaType, c.a aVar) {
        b _findStdTypeDesc = _findStdTypeDesc(javaType);
        if (_findStdTypeDesc == null) {
            _findStdTypeDesc = _findStdJdkCollectionDesc(serializationConfig, javaType);
            if (_findStdTypeDesc == null) {
                _findStdTypeDesc = b.I(collectProperties(serializationConfig, javaType, aVar, true, "set"));
            }
            this._cachedFCA.putIfAbsent(javaType, _findStdTypeDesc);
        }
        return _findStdTypeDesc;
    }
}
