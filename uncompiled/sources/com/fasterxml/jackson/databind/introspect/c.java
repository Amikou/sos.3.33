package com.fasterxml.jackson.databind.introspect;

import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.cfg.MapperConfig;

/* compiled from: ClassIntrospector.java */
/* loaded from: classes.dex */
public abstract class c {

    /* compiled from: ClassIntrospector.java */
    /* loaded from: classes.dex */
    public interface a {
        a copy();

        Class<?> findMixInClassFor(Class<?> cls);
    }

    public abstract so forClassAnnotations(MapperConfig<?> mapperConfig, JavaType javaType, a aVar);

    public abstract so forCreation(DeserializationConfig deserializationConfig, JavaType javaType, a aVar);

    public abstract so forDeserialization(DeserializationConfig deserializationConfig, JavaType javaType, a aVar);

    public abstract so forDeserializationWithBuilder(DeserializationConfig deserializationConfig, JavaType javaType, a aVar);

    public abstract so forDirectClassAnnotations(MapperConfig<?> mapperConfig, JavaType javaType, a aVar);

    public abstract so forSerialization(SerializationConfig serializationConfig, JavaType javaType, a aVar);
}
