package com.fasterxml.jackson.databind.introspect;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.PropertyName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.d;
import com.fasterxml.jackson.databind.c;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.databind.f;
import com.fasterxml.jackson.databind.g;
import com.fasterxml.jackson.databind.jsontype.NamedType;
import com.fasterxml.jackson.databind.ser.BeanPropertyWriter;
import com.fasterxml.jackson.databind.util.NameTransformer;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/* loaded from: classes.dex */
public class AnnotationIntrospectorPair extends AnnotationIntrospector {
    private static final long serialVersionUID = 1;
    public final AnnotationIntrospector _primary;
    public final AnnotationIntrospector _secondary;

    public AnnotationIntrospectorPair(AnnotationIntrospector annotationIntrospector, AnnotationIntrospector annotationIntrospector2) {
        this._primary = annotationIntrospector;
        this._secondary = annotationIntrospector2;
    }

    public static AnnotationIntrospector create(AnnotationIntrospector annotationIntrospector, AnnotationIntrospector annotationIntrospector2) {
        return annotationIntrospector == null ? annotationIntrospector2 : annotationIntrospector2 == null ? annotationIntrospector : new AnnotationIntrospectorPair(annotationIntrospector, annotationIntrospector2);
    }

    public boolean _isExplicitClassOrOb(Object obj, Class<?> cls) {
        if (obj == null) {
            return false;
        }
        if (obj instanceof Class) {
            Class<?> cls2 = (Class) obj;
            return (cls2 == cls || com.fasterxml.jackson.databind.util.c.G(cls2)) ? false : true;
        }
        return true;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Collection<AnnotationIntrospector> allIntrospectors() {
        return allIntrospectors(new ArrayList());
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public void findAndAddVirtualProperties(MapperConfig<?> mapperConfig, a aVar, List<BeanPropertyWriter> list) {
        this._primary.findAndAddVirtualProperties(mapperConfig, aVar, list);
        this._secondary.findAndAddVirtualProperties(mapperConfig, aVar, list);
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public VisibilityChecker<?> findAutoDetectVisibility(a aVar, VisibilityChecker<?> visibilityChecker) {
        return this._primary.findAutoDetectVisibility(aVar, this._secondary.findAutoDetectVisibility(aVar, visibilityChecker));
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public String findClassDescription(a aVar) {
        String findClassDescription = this._primary.findClassDescription(aVar);
        return (findClassDescription == null || findClassDescription.isEmpty()) ? this._secondary.findClassDescription(aVar) : findClassDescription;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Object findContentDeserializer(ue ueVar) {
        Object findContentDeserializer = this._primary.findContentDeserializer(ueVar);
        return _isExplicitClassOrOb(findContentDeserializer, c.a.class) ? findContentDeserializer : this._secondary.findContentDeserializer(ueVar);
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Object findContentSerializer(ue ueVar) {
        Object findContentSerializer = this._primary.findContentSerializer(ueVar);
        return _isExplicitClassOrOb(findContentSerializer, f.a.class) ? findContentSerializer : this._secondary.findContentSerializer(ueVar);
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public JsonCreator.Mode findCreatorBinding(ue ueVar) {
        JsonCreator.Mode findCreatorBinding = this._primary.findCreatorBinding(ueVar);
        return findCreatorBinding != null ? findCreatorBinding : this._secondary.findCreatorBinding(ueVar);
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Enum<?> findDefaultEnumValue(Class<Enum<?>> cls) {
        Enum<?> findDefaultEnumValue = this._primary.findDefaultEnumValue(cls);
        return findDefaultEnumValue == null ? this._secondary.findDefaultEnumValue(cls) : findDefaultEnumValue;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Object findDeserializationContentConverter(AnnotatedMember annotatedMember) {
        Object findDeserializationContentConverter = this._primary.findDeserializationContentConverter(annotatedMember);
        return findDeserializationContentConverter == null ? this._secondary.findDeserializationContentConverter(annotatedMember) : findDeserializationContentConverter;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    @Deprecated
    public Class<?> findDeserializationContentType(ue ueVar, JavaType javaType) {
        Class<?> findDeserializationContentType = this._primary.findDeserializationContentType(ueVar, javaType);
        return findDeserializationContentType == null ? this._secondary.findDeserializationContentType(ueVar, javaType) : findDeserializationContentType;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Object findDeserializationConverter(ue ueVar) {
        Object findDeserializationConverter = this._primary.findDeserializationConverter(ueVar);
        return findDeserializationConverter == null ? this._secondary.findDeserializationConverter(ueVar) : findDeserializationConverter;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    @Deprecated
    public Class<?> findDeserializationKeyType(ue ueVar, JavaType javaType) {
        Class<?> findDeserializationKeyType = this._primary.findDeserializationKeyType(ueVar, javaType);
        return findDeserializationKeyType == null ? this._secondary.findDeserializationKeyType(ueVar, javaType) : findDeserializationKeyType;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    @Deprecated
    public Class<?> findDeserializationType(ue ueVar, JavaType javaType) {
        Class<?> findDeserializationType = this._primary.findDeserializationType(ueVar, javaType);
        return findDeserializationType != null ? findDeserializationType : this._secondary.findDeserializationType(ueVar, javaType);
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Object findDeserializer(ue ueVar) {
        Object findDeserializer = this._primary.findDeserializer(ueVar);
        return _isExplicitClassOrOb(findDeserializer, c.a.class) ? findDeserializer : this._secondary.findDeserializer(ueVar);
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    @Deprecated
    public String findEnumValue(Enum<?> r2) {
        String findEnumValue = this._primary.findEnumValue(r2);
        return findEnumValue == null ? this._secondary.findEnumValue(r2) : findEnumValue;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public String[] findEnumValues(Class<?> cls, Enum<?>[] enumArr, String[] strArr) {
        return this._primary.findEnumValues(cls, enumArr, this._secondary.findEnumValues(cls, enumArr, strArr));
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Object findFilterId(ue ueVar) {
        Object findFilterId = this._primary.findFilterId(ueVar);
        return findFilterId == null ? this._secondary.findFilterId(ueVar) : findFilterId;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public JsonFormat.Value findFormat(ue ueVar) {
        JsonFormat.Value findFormat = this._primary.findFormat(ueVar);
        JsonFormat.Value findFormat2 = this._secondary.findFormat(ueVar);
        return findFormat2 == null ? findFormat : findFormat2.withOverrides(findFormat);
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    @Deprecated
    public Boolean findIgnoreUnknownProperties(a aVar) {
        Boolean findIgnoreUnknownProperties = this._primary.findIgnoreUnknownProperties(aVar);
        return findIgnoreUnknownProperties == null ? this._secondary.findIgnoreUnknownProperties(aVar) : findIgnoreUnknownProperties;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public String findImplicitPropertyName(AnnotatedMember annotatedMember) {
        String findImplicitPropertyName = this._primary.findImplicitPropertyName(annotatedMember);
        return findImplicitPropertyName == null ? this._secondary.findImplicitPropertyName(annotatedMember) : findImplicitPropertyName;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Object findInjectableValueId(AnnotatedMember annotatedMember) {
        Object findInjectableValueId = this._primary.findInjectableValueId(annotatedMember);
        return findInjectableValueId == null ? this._secondary.findInjectableValueId(annotatedMember) : findInjectableValueId;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Object findKeyDeserializer(ue ueVar) {
        Object findKeyDeserializer = this._primary.findKeyDeserializer(ueVar);
        return _isExplicitClassOrOb(findKeyDeserializer, g.a.class) ? findKeyDeserializer : this._secondary.findKeyDeserializer(ueVar);
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Object findKeySerializer(ue ueVar) {
        Object findKeySerializer = this._primary.findKeySerializer(ueVar);
        return _isExplicitClassOrOb(findKeySerializer, f.a.class) ? findKeySerializer : this._secondary.findKeySerializer(ueVar);
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public PropertyName findNameForDeserialization(ue ueVar) {
        PropertyName findNameForDeserialization;
        PropertyName findNameForDeserialization2 = this._primary.findNameForDeserialization(ueVar);
        if (findNameForDeserialization2 == null) {
            return this._secondary.findNameForDeserialization(ueVar);
        }
        return (findNameForDeserialization2 != PropertyName.USE_DEFAULT || (findNameForDeserialization = this._secondary.findNameForDeserialization(ueVar)) == null) ? findNameForDeserialization2 : findNameForDeserialization;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public PropertyName findNameForSerialization(ue ueVar) {
        PropertyName findNameForSerialization;
        PropertyName findNameForSerialization2 = this._primary.findNameForSerialization(ueVar);
        if (findNameForSerialization2 == null) {
            return this._secondary.findNameForSerialization(ueVar);
        }
        return (findNameForSerialization2 != PropertyName.USE_DEFAULT || (findNameForSerialization = this._secondary.findNameForSerialization(ueVar)) == null) ? findNameForSerialization2 : findNameForSerialization;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Object findNamingStrategy(a aVar) {
        Object findNamingStrategy = this._primary.findNamingStrategy(aVar);
        return findNamingStrategy == null ? this._secondary.findNamingStrategy(aVar) : findNamingStrategy;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Object findNullSerializer(ue ueVar) {
        Object findNullSerializer = this._primary.findNullSerializer(ueVar);
        return _isExplicitClassOrOb(findNullSerializer, f.a.class) ? findNullSerializer : this._secondary.findNullSerializer(ueVar);
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public jl2 findObjectIdInfo(ue ueVar) {
        jl2 findObjectIdInfo = this._primary.findObjectIdInfo(ueVar);
        return findObjectIdInfo == null ? this._secondary.findObjectIdInfo(ueVar) : findObjectIdInfo;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public jl2 findObjectReferenceInfo(ue ueVar, jl2 jl2Var) {
        return this._primary.findObjectReferenceInfo(ueVar, this._secondary.findObjectReferenceInfo(ueVar, jl2Var));
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Class<?> findPOJOBuilder(a aVar) {
        Class<?> findPOJOBuilder = this._primary.findPOJOBuilder(aVar);
        return findPOJOBuilder == null ? this._secondary.findPOJOBuilder(aVar) : findPOJOBuilder;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public d.a findPOJOBuilderConfig(a aVar) {
        d.a findPOJOBuilderConfig = this._primary.findPOJOBuilderConfig(aVar);
        return findPOJOBuilderConfig == null ? this._secondary.findPOJOBuilderConfig(aVar) : findPOJOBuilderConfig;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    @Deprecated
    public String[] findPropertiesToIgnore(ue ueVar) {
        String[] findPropertiesToIgnore = this._primary.findPropertiesToIgnore(ueVar);
        return findPropertiesToIgnore == null ? this._secondary.findPropertiesToIgnore(ueVar) : findPropertiesToIgnore;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public JsonProperty.Access findPropertyAccess(ue ueVar) {
        JsonProperty.Access findPropertyAccess = this._primary.findPropertyAccess(ueVar);
        if (findPropertyAccess == null || findPropertyAccess == JsonProperty.Access.AUTO) {
            JsonProperty.Access findPropertyAccess2 = this._secondary.findPropertyAccess(ueVar);
            return findPropertyAccess2 != null ? findPropertyAccess2 : JsonProperty.Access.AUTO;
        }
        return findPropertyAccess;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public vd4<?> findPropertyContentTypeResolver(MapperConfig<?> mapperConfig, AnnotatedMember annotatedMember, JavaType javaType) {
        vd4<?> findPropertyContentTypeResolver = this._primary.findPropertyContentTypeResolver(mapperConfig, annotatedMember, javaType);
        return findPropertyContentTypeResolver == null ? this._secondary.findPropertyContentTypeResolver(mapperConfig, annotatedMember, javaType) : findPropertyContentTypeResolver;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public String findPropertyDefaultValue(ue ueVar) {
        String findPropertyDefaultValue = this._primary.findPropertyDefaultValue(ueVar);
        return (findPropertyDefaultValue == null || findPropertyDefaultValue.isEmpty()) ? this._secondary.findPropertyDefaultValue(ueVar) : findPropertyDefaultValue;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public String findPropertyDescription(ue ueVar) {
        String findPropertyDescription = this._primary.findPropertyDescription(ueVar);
        return findPropertyDescription == null ? this._secondary.findPropertyDescription(ueVar) : findPropertyDescription;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public JsonIgnoreProperties.Value findPropertyIgnorals(ue ueVar) {
        JsonIgnoreProperties.Value findPropertyIgnorals = this._secondary.findPropertyIgnorals(ueVar);
        JsonIgnoreProperties.Value findPropertyIgnorals2 = this._primary.findPropertyIgnorals(ueVar);
        return findPropertyIgnorals == null ? findPropertyIgnorals2 : findPropertyIgnorals.withOverrides(findPropertyIgnorals2);
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public JsonInclude.Value findPropertyInclusion(ue ueVar) {
        JsonInclude.Value findPropertyInclusion = this._secondary.findPropertyInclusion(ueVar);
        JsonInclude.Value findPropertyInclusion2 = this._primary.findPropertyInclusion(ueVar);
        return findPropertyInclusion == null ? findPropertyInclusion2 : findPropertyInclusion.withOverrides(findPropertyInclusion2);
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Integer findPropertyIndex(ue ueVar) {
        Integer findPropertyIndex = this._primary.findPropertyIndex(ueVar);
        return findPropertyIndex == null ? this._secondary.findPropertyIndex(ueVar) : findPropertyIndex;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public vd4<?> findPropertyTypeResolver(MapperConfig<?> mapperConfig, AnnotatedMember annotatedMember, JavaType javaType) {
        vd4<?> findPropertyTypeResolver = this._primary.findPropertyTypeResolver(mapperConfig, annotatedMember, javaType);
        return findPropertyTypeResolver == null ? this._secondary.findPropertyTypeResolver(mapperConfig, annotatedMember, javaType) : findPropertyTypeResolver;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public AnnotationIntrospector.ReferenceProperty findReferenceType(AnnotatedMember annotatedMember) {
        AnnotationIntrospector.ReferenceProperty findReferenceType = this._primary.findReferenceType(annotatedMember);
        return findReferenceType == null ? this._secondary.findReferenceType(annotatedMember) : findReferenceType;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public PropertyName findRootName(a aVar) {
        PropertyName findRootName;
        PropertyName findRootName2 = this._primary.findRootName(aVar);
        if (findRootName2 == null) {
            return this._secondary.findRootName(aVar);
        }
        return (findRootName2.hasSimpleName() || (findRootName = this._secondary.findRootName(aVar)) == null) ? findRootName2 : findRootName;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Object findSerializationContentConverter(AnnotatedMember annotatedMember) {
        Object findSerializationContentConverter = this._primary.findSerializationContentConverter(annotatedMember);
        return findSerializationContentConverter == null ? this._secondary.findSerializationContentConverter(annotatedMember) : findSerializationContentConverter;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    @Deprecated
    public Class<?> findSerializationContentType(ue ueVar, JavaType javaType) {
        Class<?> findSerializationContentType = this._primary.findSerializationContentType(ueVar, javaType);
        return findSerializationContentType == null ? this._secondary.findSerializationContentType(ueVar, javaType) : findSerializationContentType;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Object findSerializationConverter(ue ueVar) {
        Object findSerializationConverter = this._primary.findSerializationConverter(ueVar);
        return findSerializationConverter == null ? this._secondary.findSerializationConverter(ueVar) : findSerializationConverter;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    @Deprecated
    public JsonInclude.Include findSerializationInclusion(ue ueVar, JsonInclude.Include include) {
        return this._primary.findSerializationInclusion(ueVar, this._secondary.findSerializationInclusion(ueVar, include));
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    @Deprecated
    public JsonInclude.Include findSerializationInclusionForContent(ue ueVar, JsonInclude.Include include) {
        return this._primary.findSerializationInclusionForContent(ueVar, this._secondary.findSerializationInclusionForContent(ueVar, include));
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    @Deprecated
    public Class<?> findSerializationKeyType(ue ueVar, JavaType javaType) {
        Class<?> findSerializationKeyType = this._primary.findSerializationKeyType(ueVar, javaType);
        return findSerializationKeyType == null ? this._secondary.findSerializationKeyType(ueVar, javaType) : findSerializationKeyType;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public String[] findSerializationPropertyOrder(a aVar) {
        String[] findSerializationPropertyOrder = this._primary.findSerializationPropertyOrder(aVar);
        return findSerializationPropertyOrder == null ? this._secondary.findSerializationPropertyOrder(aVar) : findSerializationPropertyOrder;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Boolean findSerializationSortAlphabetically(ue ueVar) {
        Boolean findSerializationSortAlphabetically = this._primary.findSerializationSortAlphabetically(ueVar);
        return findSerializationSortAlphabetically == null ? this._secondary.findSerializationSortAlphabetically(ueVar) : findSerializationSortAlphabetically;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    @Deprecated
    public Class<?> findSerializationType(ue ueVar) {
        Class<?> findSerializationType = this._primary.findSerializationType(ueVar);
        return findSerializationType == null ? this._secondary.findSerializationType(ueVar) : findSerializationType;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public JsonSerialize.Typing findSerializationTyping(ue ueVar) {
        JsonSerialize.Typing findSerializationTyping = this._primary.findSerializationTyping(ueVar);
        return findSerializationTyping == null ? this._secondary.findSerializationTyping(ueVar) : findSerializationTyping;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Object findSerializer(ue ueVar) {
        Object findSerializer = this._primary.findSerializer(ueVar);
        return _isExplicitClassOrOb(findSerializer, f.a.class) ? findSerializer : this._secondary.findSerializer(ueVar);
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public List<NamedType> findSubtypes(ue ueVar) {
        List<NamedType> findSubtypes = this._primary.findSubtypes(ueVar);
        List<NamedType> findSubtypes2 = this._secondary.findSubtypes(ueVar);
        if (findSubtypes == null || findSubtypes.isEmpty()) {
            return findSubtypes2;
        }
        if (findSubtypes2 == null || findSubtypes2.isEmpty()) {
            return findSubtypes;
        }
        ArrayList arrayList = new ArrayList(findSubtypes.size() + findSubtypes2.size());
        arrayList.addAll(findSubtypes);
        arrayList.addAll(findSubtypes2);
        return arrayList;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public String findTypeName(a aVar) {
        String findTypeName = this._primary.findTypeName(aVar);
        return (findTypeName == null || findTypeName.length() == 0) ? this._secondary.findTypeName(aVar) : findTypeName;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public vd4<?> findTypeResolver(MapperConfig<?> mapperConfig, a aVar, JavaType javaType) {
        vd4<?> findTypeResolver = this._primary.findTypeResolver(mapperConfig, aVar, javaType);
        return findTypeResolver == null ? this._secondary.findTypeResolver(mapperConfig, aVar, javaType) : findTypeResolver;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public NameTransformer findUnwrappingNameTransformer(AnnotatedMember annotatedMember) {
        NameTransformer findUnwrappingNameTransformer = this._primary.findUnwrappingNameTransformer(annotatedMember);
        return findUnwrappingNameTransformer == null ? this._secondary.findUnwrappingNameTransformer(annotatedMember) : findUnwrappingNameTransformer;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Object findValueInstantiator(a aVar) {
        Object findValueInstantiator = this._primary.findValueInstantiator(aVar);
        return findValueInstantiator == null ? this._secondary.findValueInstantiator(aVar) : findValueInstantiator;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Class<?>[] findViews(ue ueVar) {
        Class<?>[] findViews = this._primary.findViews(ueVar);
        return findViews == null ? this._secondary.findViews(ueVar) : findViews;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public PropertyName findWrapperName(ue ueVar) {
        PropertyName findWrapperName;
        PropertyName findWrapperName2 = this._primary.findWrapperName(ueVar);
        if (findWrapperName2 == null) {
            return this._secondary.findWrapperName(ueVar);
        }
        return (findWrapperName2 != PropertyName.USE_DEFAULT || (findWrapperName = this._secondary.findWrapperName(ueVar)) == null) ? findWrapperName2 : findWrapperName;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public boolean hasAnyGetterAnnotation(AnnotatedMethod annotatedMethod) {
        return this._primary.hasAnyGetterAnnotation(annotatedMethod) || this._secondary.hasAnyGetterAnnotation(annotatedMethod);
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public boolean hasAnySetterAnnotation(AnnotatedMethod annotatedMethod) {
        return this._primary.hasAnySetterAnnotation(annotatedMethod) || this._secondary.hasAnySetterAnnotation(annotatedMethod);
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public boolean hasAsValueAnnotation(AnnotatedMethod annotatedMethod) {
        return this._primary.hasAsValueAnnotation(annotatedMethod) || this._secondary.hasAsValueAnnotation(annotatedMethod);
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public boolean hasCreatorAnnotation(ue ueVar) {
        return this._primary.hasCreatorAnnotation(ueVar) || this._secondary.hasCreatorAnnotation(ueVar);
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public boolean hasIgnoreMarker(AnnotatedMember annotatedMember) {
        return this._primary.hasIgnoreMarker(annotatedMember) || this._secondary.hasIgnoreMarker(annotatedMember);
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Boolean hasRequiredMarker(AnnotatedMember annotatedMember) {
        Boolean hasRequiredMarker = this._primary.hasRequiredMarker(annotatedMember);
        return hasRequiredMarker == null ? this._secondary.hasRequiredMarker(annotatedMember) : hasRequiredMarker;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public boolean isAnnotationBundle(Annotation annotation) {
        return this._primary.isAnnotationBundle(annotation) || this._secondary.isAnnotationBundle(annotation);
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Boolean isIgnorableType(a aVar) {
        Boolean isIgnorableType = this._primary.isIgnorableType(aVar);
        return isIgnorableType == null ? this._secondary.isIgnorableType(aVar) : isIgnorableType;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Boolean isTypeId(AnnotatedMember annotatedMember) {
        Boolean isTypeId = this._primary.isTypeId(annotatedMember);
        return isTypeId == null ? this._secondary.isTypeId(annotatedMember) : isTypeId;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public JavaType refineDeserializationType(MapperConfig<?> mapperConfig, ue ueVar, JavaType javaType) throws JsonMappingException {
        return this._primary.refineDeserializationType(mapperConfig, ueVar, this._secondary.refineDeserializationType(mapperConfig, ueVar, javaType));
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public JavaType refineSerializationType(MapperConfig<?> mapperConfig, ue ueVar, JavaType javaType) throws JsonMappingException {
        return this._primary.refineSerializationType(mapperConfig, ueVar, this._secondary.refineSerializationType(mapperConfig, ueVar, javaType));
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public AnnotatedMethod resolveSetterConflict(MapperConfig<?> mapperConfig, AnnotatedMethod annotatedMethod, AnnotatedMethod annotatedMethod2) {
        AnnotatedMethod resolveSetterConflict = this._primary.resolveSetterConflict(mapperConfig, annotatedMethod, annotatedMethod2);
        return resolveSetterConflict == null ? this._secondary.resolveSetterConflict(mapperConfig, annotatedMethod, annotatedMethod2) : resolveSetterConflict;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Version version() {
        return this._primary.version();
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Collection<AnnotationIntrospector> allIntrospectors(Collection<AnnotationIntrospector> collection) {
        this._primary.allIntrospectors(collection);
        this._secondary.allIntrospectors(collection);
        return collection;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    @Deprecated
    public String[] findPropertiesToIgnore(ue ueVar, boolean z) {
        String[] findPropertiesToIgnore = this._primary.findPropertiesToIgnore(ueVar, z);
        return findPropertiesToIgnore == null ? this._secondary.findPropertiesToIgnore(ueVar, z) : findPropertiesToIgnore;
    }
}
