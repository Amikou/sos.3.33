package com.fasterxml.jackson.databind.introspect;

import com.fasterxml.jackson.databind.JavaType;
import java.lang.reflect.Field;
import java.lang.reflect.Member;

/* loaded from: classes.dex */
public class VirtualAnnotatedMember extends AnnotatedMember {
    private static final long serialVersionUID = 1;
    public final Class<?> _declaringClass;
    public final String _name;
    public final JavaType _type;

    public VirtualAnnotatedMember(d dVar, Class<?> cls, String str, JavaType javaType) {
        super(dVar, null);
        this._declaringClass = cls;
        this._type = javaType;
        this._name = str;
    }

    @Override // defpackage.ue
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }
        VirtualAnnotatedMember virtualAnnotatedMember = (VirtualAnnotatedMember) obj;
        return virtualAnnotatedMember._declaringClass == this._declaringClass && virtualAnnotatedMember._name.equals(this._name);
    }

    @Override // defpackage.ue
    public Field getAnnotated() {
        return null;
    }

    public int getAnnotationCount() {
        return 0;
    }

    @Override // com.fasterxml.jackson.databind.introspect.AnnotatedMember
    public Class<?> getDeclaringClass() {
        return this._declaringClass;
    }

    public String getFullName() {
        return getDeclaringClass().getName() + "#" + getName();
    }

    @Override // com.fasterxml.jackson.databind.introspect.AnnotatedMember
    public Member getMember() {
        return null;
    }

    @Override // defpackage.ue
    public int getModifiers() {
        return 0;
    }

    @Override // defpackage.ue
    public String getName() {
        return this._name;
    }

    @Override // defpackage.ue
    public Class<?> getRawType() {
        return this._type.getRawClass();
    }

    @Override // defpackage.ue
    public JavaType getType() {
        return this._type;
    }

    @Override // com.fasterxml.jackson.databind.introspect.AnnotatedMember
    public Object getValue(Object obj) throws IllegalArgumentException {
        throw new IllegalArgumentException("Can not get virtual property '" + this._name + "'");
    }

    @Override // defpackage.ue
    public int hashCode() {
        return this._name.hashCode();
    }

    @Override // com.fasterxml.jackson.databind.introspect.AnnotatedMember
    public void setValue(Object obj, Object obj2) throws IllegalArgumentException {
        throw new IllegalArgumentException("Can not set virtual property '" + this._name + "'");
    }

    @Override // defpackage.ue
    public String toString() {
        return "[field " + getFullName() + "]";
    }

    @Override // defpackage.ue
    public ue withAnnotations(we weVar) {
        return this;
    }

    @Deprecated
    public VirtualAnnotatedMember(d dVar, Class<?> cls, String str, Class<?> cls2) {
        this(dVar, cls, str, dVar.a(cls2));
    }
}
