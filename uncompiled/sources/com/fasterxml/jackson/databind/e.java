package com.fasterxml.jackson.databind;

import com.fasterxml.jackson.core.JsonGenerator;
import java.io.IOException;

/* compiled from: JsonSerializable.java */
/* loaded from: classes.dex */
public interface e {

    /* compiled from: JsonSerializable.java */
    /* loaded from: classes.dex */
    public static abstract class a implements e {
        public boolean k(k kVar) {
            return false;
        }
    }

    void serialize(JsonGenerator jsonGenerator, k kVar) throws IOException;

    void serializeWithType(JsonGenerator jsonGenerator, k kVar, com.fasterxml.jackson.databind.jsontype.c cVar) throws IOException;
}
