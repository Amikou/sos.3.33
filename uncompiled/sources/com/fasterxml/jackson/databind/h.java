package com.fasterxml.jackson.databind;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import java.io.Closeable;
import java.io.IOException;
import java.util.Iterator;
import java.util.NoSuchElementException;

/* compiled from: MappingIterator.java */
/* loaded from: classes.dex */
public class h<T> implements Iterator<T>, Closeable {
    public final DeserializationContext a;
    public final c<T> f0;
    public final JsonParser g0;
    public final cw1 h0;
    public final T i0;
    public final boolean j0;
    public int k0;

    static {
        new h(null, null, null, null, false, null);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public h(JavaType javaType, JsonParser jsonParser, DeserializationContext deserializationContext, c<?> cVar, boolean z, Object obj) {
        this.g0 = jsonParser;
        this.a = deserializationContext;
        this.f0 = cVar;
        this.j0 = z;
        if (obj == 0) {
            this.i0 = null;
        } else {
            this.i0 = obj;
        }
        if (jsonParser == null) {
            this.h0 = null;
            this.k0 = 0;
            return;
        }
        cw1 S = jsonParser.S();
        if (z && jsonParser.K0()) {
            jsonParser.e();
        } else {
            JsonToken u = jsonParser.u();
            if (u == JsonToken.START_OBJECT || u == JsonToken.START_ARRAY) {
                S = S.d();
            }
        }
        this.h0 = S;
        this.k0 = 2;
    }

    public <R> R a(IOException iOException) {
        throw new RuntimeException(iOException.getMessage(), iOException);
    }

    public <R> R b(JsonMappingException jsonMappingException) {
        throw new RuntimeJsonMappingException(jsonMappingException.getMessage(), jsonMappingException);
    }

    public void c() throws IOException {
        JsonParser jsonParser = this.g0;
        if (jsonParser.S() == this.h0) {
            return;
        }
        while (true) {
            JsonToken T0 = jsonParser.T0();
            if (T0 != JsonToken.END_ARRAY && T0 != JsonToken.END_OBJECT) {
                if (T0 == JsonToken.START_ARRAY || T0 == JsonToken.START_OBJECT) {
                    jsonParser.k1();
                } else if (T0 == null) {
                    return;
                }
            } else if (jsonParser.S() == this.h0) {
                jsonParser.e();
                return;
            }
        }
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        if (this.k0 != 0) {
            this.k0 = 0;
            JsonParser jsonParser = this.g0;
            if (jsonParser != null) {
                jsonParser.close();
            }
        }
    }

    public <R> R d() {
        throw new NoSuchElementException();
    }

    public boolean f() throws IOException {
        JsonToken T0;
        JsonParser jsonParser;
        int i = this.k0;
        if (i != 0) {
            if (i == 1) {
                c();
            } else if (i != 2) {
                return true;
            }
            if (this.g0.u() == null && ((T0 = this.g0.T0()) == null || T0 == JsonToken.END_ARRAY)) {
                this.k0 = 0;
                if (this.j0 && (jsonParser = this.g0) != null) {
                    jsonParser.close();
                }
                return false;
            }
            this.k0 = 3;
            return true;
        }
        return false;
    }

    public T g() throws IOException {
        T t;
        int i = this.k0;
        if (i != 0) {
            if ((i == 1 || i == 2) && !f()) {
                return (T) d();
            }
            try {
                T t2 = this.i0;
                if (t2 == null) {
                    t = this.f0.deserialize(this.g0, this.a);
                } else {
                    this.f0.deserialize(this.g0, this.a, t2);
                    t = this.i0;
                }
                this.k0 = 2;
                this.g0.e();
                return t;
            } catch (Throwable th) {
                this.k0 = 1;
                this.g0.e();
                throw th;
            }
        }
        return (T) d();
    }

    @Override // java.util.Iterator
    public boolean hasNext() {
        try {
            return f();
        } catch (JsonMappingException e) {
            return ((Boolean) b(e)).booleanValue();
        } catch (IOException e2) {
            return ((Boolean) a(e2)).booleanValue();
        }
    }

    @Override // java.util.Iterator
    public T next() {
        try {
            return g();
        } catch (JsonMappingException e) {
            throw new RuntimeJsonMappingException(e.getMessage(), e);
        } catch (IOException e2) {
            throw new RuntimeException(e2.getMessage(), e2);
        }
    }

    @Override // java.util.Iterator
    public void remove() {
        throw new UnsupportedOperationException();
    }
}
