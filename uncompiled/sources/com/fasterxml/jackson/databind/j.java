package com.fasterxml.jackson.databind;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.ser.DefaultSerializerProvider;
import java.io.Closeable;
import java.io.Flushable;
import java.io.IOException;

/* compiled from: SequenceWriter.java */
/* loaded from: classes.dex */
public class j implements Closeable, Flushable {
    public final SerializationConfig a;
    public final JsonGenerator f0;
    public final boolean g0;
    public boolean h0;
    public boolean i0;

    public j(DefaultSerializerProvider defaultSerializerProvider, JsonGenerator jsonGenerator, boolean z, ObjectWriter.Prefetch prefetch) throws IOException {
        this.f0 = jsonGenerator;
        this.g0 = z;
        prefetch.getValueSerializer();
        prefetch.getTypeSerializer();
        SerializationConfig config = defaultSerializerProvider.getConfig();
        this.a = config;
        config.isEnabled(SerializationFeature.FLUSH_AFTER_WRITE_VALUE);
        config.isEnabled(SerializationFeature.CLOSE_CLOSEABLE);
        com.fasterxml.jackson.databind.ser.impl.a.b();
    }

    public j a(boolean z) throws IOException {
        if (z) {
            this.f0.e1();
            this.h0 = true;
        }
        return this;
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        if (this.i0) {
            return;
        }
        this.i0 = true;
        if (this.h0) {
            this.h0 = false;
            this.f0.e0();
        }
        if (this.g0) {
            this.f0.close();
        }
    }

    @Override // java.io.Flushable
    public void flush() throws IOException {
        if (this.i0) {
            return;
        }
        this.f0.flush();
    }
}
