package com.fasterxml.jackson.databind;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.d;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.fasterxml.jackson.databind.introspect.AnnotatedMethod;
import com.fasterxml.jackson.databind.introspect.AnnotationIntrospectorPair;
import com.fasterxml.jackson.databind.introspect.NopAnnotationIntrospector;
import com.fasterxml.jackson.databind.introspect.VisibilityChecker;
import com.fasterxml.jackson.databind.jsontype.NamedType;
import com.fasterxml.jackson.databind.ser.BeanPropertyWriter;
import com.fasterxml.jackson.databind.type.MapLikeType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.databind.util.NameTransformer;
import java.io.Closeable;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/* loaded from: classes.dex */
public abstract class AnnotationIntrospector implements Serializable {

    /* loaded from: classes.dex */
    public static class ReferenceProperty {
        public final Type a;
        public final String b;

        /* loaded from: classes.dex */
        public enum Type {
            MANAGED_REFERENCE,
            BACK_REFERENCE
        }

        public ReferenceProperty(Type type, String str) {
            this.a = type;
            this.b = str;
        }

        public static ReferenceProperty a(String str) {
            return new ReferenceProperty(Type.BACK_REFERENCE, str);
        }

        public static ReferenceProperty e(String str) {
            return new ReferenceProperty(Type.MANAGED_REFERENCE, str);
        }

        public String b() {
            return this.b;
        }

        public boolean c() {
            return this.a == Type.BACK_REFERENCE;
        }

        public boolean d() {
            return this.a == Type.MANAGED_REFERENCE;
        }
    }

    public static AnnotationIntrospector nopInstance() {
        return NopAnnotationIntrospector.instance;
    }

    public static AnnotationIntrospector pair(AnnotationIntrospector annotationIntrospector, AnnotationIntrospector annotationIntrospector2) {
        return new AnnotationIntrospectorPair(annotationIntrospector, annotationIntrospector2);
    }

    public <A extends Annotation> A _findAnnotation(ue ueVar, Class<A> cls) {
        return (A) ueVar.getAnnotation(cls);
    }

    public boolean _hasAnnotation(ue ueVar, Class<? extends Annotation> cls) {
        return ueVar.hasAnnotation(cls);
    }

    public boolean _hasOneOf(ue ueVar, Class<? extends Annotation>[] clsArr) {
        return ueVar.hasOneOf(clsArr);
    }

    public Collection<AnnotationIntrospector> allIntrospectors() {
        return Collections.singletonList(this);
    }

    public void findAndAddVirtualProperties(MapperConfig<?> mapperConfig, com.fasterxml.jackson.databind.introspect.a aVar, List<BeanPropertyWriter> list) {
    }

    public VisibilityChecker<?> findAutoDetectVisibility(com.fasterxml.jackson.databind.introspect.a aVar, VisibilityChecker<?> visibilityChecker) {
        return visibilityChecker;
    }

    public String findClassDescription(com.fasterxml.jackson.databind.introspect.a aVar) {
        return null;
    }

    public Object findContentDeserializer(ue ueVar) {
        return null;
    }

    public Object findContentSerializer(ue ueVar) {
        return null;
    }

    public JsonCreator.Mode findCreatorBinding(ue ueVar) {
        return null;
    }

    public Enum<?> findDefaultEnumValue(Class<Enum<?>> cls) {
        return null;
    }

    public Object findDeserializationContentConverter(AnnotatedMember annotatedMember) {
        return null;
    }

    @Deprecated
    public Class<?> findDeserializationContentType(ue ueVar, JavaType javaType) {
        return null;
    }

    public Object findDeserializationConverter(ue ueVar) {
        return null;
    }

    @Deprecated
    public Class<?> findDeserializationKeyType(ue ueVar, JavaType javaType) {
        return null;
    }

    @Deprecated
    public Class<?> findDeserializationType(ue ueVar, JavaType javaType) {
        return null;
    }

    public Object findDeserializer(ue ueVar) {
        return null;
    }

    @Deprecated
    public String findEnumValue(Enum<?> r1) {
        return r1.name();
    }

    public String[] findEnumValues(Class<?> cls, Enum<?>[] enumArr, String[] strArr) {
        int length = enumArr.length;
        for (int i = 0; i < length; i++) {
            if (strArr[i] == null) {
                strArr[i] = findEnumValue(enumArr[i]);
            }
        }
        return strArr;
    }

    public Object findFilterId(ue ueVar) {
        return null;
    }

    public JsonFormat.Value findFormat(ue ueVar) {
        return null;
    }

    @Deprecated
    public Boolean findIgnoreUnknownProperties(com.fasterxml.jackson.databind.introspect.a aVar) {
        return null;
    }

    public String findImplicitPropertyName(AnnotatedMember annotatedMember) {
        return null;
    }

    public Object findInjectableValueId(AnnotatedMember annotatedMember) {
        return null;
    }

    public Object findKeyDeserializer(ue ueVar) {
        return null;
    }

    public Object findKeySerializer(ue ueVar) {
        return null;
    }

    public PropertyName findNameForDeserialization(ue ueVar) {
        return null;
    }

    public PropertyName findNameForSerialization(ue ueVar) {
        return null;
    }

    public Object findNamingStrategy(com.fasterxml.jackson.databind.introspect.a aVar) {
        return null;
    }

    public Object findNullSerializer(ue ueVar) {
        return null;
    }

    public jl2 findObjectIdInfo(ue ueVar) {
        return null;
    }

    public jl2 findObjectReferenceInfo(ue ueVar, jl2 jl2Var) {
        return jl2Var;
    }

    public Class<?> findPOJOBuilder(com.fasterxml.jackson.databind.introspect.a aVar) {
        return null;
    }

    public d.a findPOJOBuilderConfig(com.fasterxml.jackson.databind.introspect.a aVar) {
        return null;
    }

    @Deprecated
    public String[] findPropertiesToIgnore(ue ueVar) {
        return null;
    }

    @Deprecated
    public String[] findPropertiesToIgnore(ue ueVar, boolean z) {
        return null;
    }

    public JsonProperty.Access findPropertyAccess(ue ueVar) {
        return null;
    }

    public vd4<?> findPropertyContentTypeResolver(MapperConfig<?> mapperConfig, AnnotatedMember annotatedMember, JavaType javaType) {
        return null;
    }

    public String findPropertyDefaultValue(ue ueVar) {
        return null;
    }

    public String findPropertyDescription(ue ueVar) {
        return null;
    }

    public JsonIgnoreProperties.Value findPropertyIgnorals(ue ueVar) {
        JsonIgnoreProperties.Value forIgnoredProperties;
        String[] findPropertiesToIgnore = findPropertiesToIgnore(ueVar, true);
        Boolean findIgnoreUnknownProperties = ueVar instanceof com.fasterxml.jackson.databind.introspect.a ? findIgnoreUnknownProperties((com.fasterxml.jackson.databind.introspect.a) ueVar) : null;
        if (findPropertiesToIgnore != null) {
            forIgnoredProperties = JsonIgnoreProperties.Value.forIgnoredProperties(findPropertiesToIgnore);
        } else if (findIgnoreUnknownProperties == null) {
            return null;
        } else {
            forIgnoredProperties = JsonIgnoreProperties.Value.empty();
        }
        if (findIgnoreUnknownProperties != null) {
            return findIgnoreUnknownProperties.booleanValue() ? forIgnoredProperties.withIgnoreUnknown() : forIgnoredProperties.withoutIgnoreUnknown();
        }
        return forIgnoredProperties;
    }

    public JsonInclude.Value findPropertyInclusion(ue ueVar) {
        return JsonInclude.Value.empty();
    }

    public Integer findPropertyIndex(ue ueVar) {
        return null;
    }

    public vd4<?> findPropertyTypeResolver(MapperConfig<?> mapperConfig, AnnotatedMember annotatedMember, JavaType javaType) {
        return null;
    }

    public ReferenceProperty findReferenceType(AnnotatedMember annotatedMember) {
        return null;
    }

    public PropertyName findRootName(com.fasterxml.jackson.databind.introspect.a aVar) {
        return null;
    }

    public Object findSerializationContentConverter(AnnotatedMember annotatedMember) {
        return null;
    }

    @Deprecated
    public Class<?> findSerializationContentType(ue ueVar, JavaType javaType) {
        return null;
    }

    public Object findSerializationConverter(ue ueVar) {
        return null;
    }

    @Deprecated
    public JsonInclude.Include findSerializationInclusion(ue ueVar, JsonInclude.Include include) {
        return include;
    }

    @Deprecated
    public JsonInclude.Include findSerializationInclusionForContent(ue ueVar, JsonInclude.Include include) {
        return include;
    }

    @Deprecated
    public Class<?> findSerializationKeyType(ue ueVar, JavaType javaType) {
        return null;
    }

    public String[] findSerializationPropertyOrder(com.fasterxml.jackson.databind.introspect.a aVar) {
        return null;
    }

    public Boolean findSerializationSortAlphabetically(ue ueVar) {
        return null;
    }

    @Deprecated
    public Class<?> findSerializationType(ue ueVar) {
        return null;
    }

    public JsonSerialize.Typing findSerializationTyping(ue ueVar) {
        return null;
    }

    public Object findSerializer(ue ueVar) {
        return null;
    }

    public List<NamedType> findSubtypes(ue ueVar) {
        return null;
    }

    public String findTypeName(com.fasterxml.jackson.databind.introspect.a aVar) {
        return null;
    }

    public vd4<?> findTypeResolver(MapperConfig<?> mapperConfig, com.fasterxml.jackson.databind.introspect.a aVar, JavaType javaType) {
        return null;
    }

    public NameTransformer findUnwrappingNameTransformer(AnnotatedMember annotatedMember) {
        return null;
    }

    public Object findValueInstantiator(com.fasterxml.jackson.databind.introspect.a aVar) {
        return null;
    }

    public Class<?>[] findViews(ue ueVar) {
        return null;
    }

    public PropertyName findWrapperName(ue ueVar) {
        return null;
    }

    public boolean hasAnyGetterAnnotation(AnnotatedMethod annotatedMethod) {
        return false;
    }

    public boolean hasAnySetterAnnotation(AnnotatedMethod annotatedMethod) {
        return false;
    }

    public boolean hasAsValueAnnotation(AnnotatedMethod annotatedMethod) {
        return false;
    }

    public boolean hasCreatorAnnotation(ue ueVar) {
        return false;
    }

    public boolean hasIgnoreMarker(AnnotatedMember annotatedMember) {
        return false;
    }

    public Boolean hasRequiredMarker(AnnotatedMember annotatedMember) {
        return null;
    }

    public boolean isAnnotationBundle(Annotation annotation) {
        return false;
    }

    public Boolean isIgnorableType(com.fasterxml.jackson.databind.introspect.a aVar) {
        return null;
    }

    public Boolean isTypeId(AnnotatedMember annotatedMember) {
        return null;
    }

    public JavaType refineDeserializationType(MapperConfig<?> mapperConfig, ue ueVar, JavaType javaType) throws JsonMappingException {
        Class<?> findDeserializationContentType;
        JavaType keyType;
        Class<?> findDeserializationKeyType;
        TypeFactory typeFactory = mapperConfig.getTypeFactory();
        Class<?> findDeserializationType = findDeserializationType(ueVar, javaType);
        if (findDeserializationType != null && !javaType.hasRawClass(findDeserializationType)) {
            try {
                javaType = typeFactory.constructSpecializedType(javaType, findDeserializationType);
            } catch (IllegalArgumentException e) {
                throw new JsonMappingException((Closeable) null, String.format("Failed to narrow type %s with annotation (value %s), from '%s': %s", javaType, findDeserializationType.getName(), ueVar.getName(), e.getMessage()), e);
            }
        }
        if (javaType.isMapLikeType() && (findDeserializationKeyType = findDeserializationKeyType(ueVar, (keyType = javaType.getKeyType()))) != null) {
            try {
                javaType = ((MapLikeType) javaType).withKeyType(typeFactory.constructSpecializedType(keyType, findDeserializationKeyType));
            } catch (IllegalArgumentException e2) {
                throw new JsonMappingException((Closeable) null, String.format("Failed to narrow key type of %s with concrete-type annotation (value %s), from '%s': %s", javaType, findDeserializationKeyType.getName(), ueVar.getName(), e2.getMessage()), e2);
            }
        }
        JavaType contentType = javaType.getContentType();
        if (contentType == null || (findDeserializationContentType = findDeserializationContentType(ueVar, contentType)) == null) {
            return javaType;
        }
        try {
            return javaType.withContentType(typeFactory.constructSpecializedType(contentType, findDeserializationContentType));
        } catch (IllegalArgumentException e3) {
            throw new JsonMappingException((Closeable) null, String.format("Failed to narrow value type of %s with concrete-type annotation (value %s), from '%s': %s", javaType, findDeserializationContentType.getName(), ueVar.getName(), e3.getMessage()), e3);
        }
    }

    public JavaType refineSerializationType(MapperConfig<?> mapperConfig, ue ueVar, JavaType javaType) throws JsonMappingException {
        Class<?> findSerializationContentType;
        JavaType constructSpecializedType;
        JavaType keyType;
        Class<?> findSerializationKeyType;
        JavaType constructSpecializedType2;
        TypeFactory typeFactory = mapperConfig.getTypeFactory();
        Class<?> findSerializationType = findSerializationType(ueVar);
        if (findSerializationType != null) {
            if (javaType.hasRawClass(findSerializationType)) {
                javaType = javaType.withStaticTyping();
            } else {
                Class<?> rawClass = javaType.getRawClass();
                try {
                    if (findSerializationType.isAssignableFrom(rawClass)) {
                        javaType = typeFactory.constructGeneralizedType(javaType, findSerializationType);
                    } else if (rawClass.isAssignableFrom(findSerializationType)) {
                        javaType = typeFactory.constructSpecializedType(javaType, findSerializationType);
                    } else {
                        throw new JsonMappingException((Closeable) null, String.format("Can not refine serialization type %s into %s; types not related", javaType, findSerializationType.getName()));
                    }
                } catch (IllegalArgumentException e) {
                    throw new JsonMappingException((Closeable) null, String.format("Failed to widen type %s with annotation (value %s), from '%s': %s", javaType, findSerializationType.getName(), ueVar.getName(), e.getMessage()), e);
                }
            }
        }
        if (javaType.isMapLikeType() && (findSerializationKeyType = findSerializationKeyType(ueVar, (keyType = javaType.getKeyType()))) != null) {
            if (keyType.hasRawClass(findSerializationKeyType)) {
                constructSpecializedType2 = keyType.withStaticTyping();
            } else {
                Class<?> rawClass2 = keyType.getRawClass();
                try {
                    if (findSerializationKeyType.isAssignableFrom(rawClass2)) {
                        constructSpecializedType2 = typeFactory.constructGeneralizedType(keyType, findSerializationKeyType);
                    } else if (rawClass2.isAssignableFrom(findSerializationKeyType)) {
                        constructSpecializedType2 = typeFactory.constructSpecializedType(keyType, findSerializationKeyType);
                    } else {
                        throw new JsonMappingException((Closeable) null, String.format("Can not refine serialization key type %s into %s; types not related", keyType, findSerializationKeyType.getName()));
                    }
                } catch (IllegalArgumentException e2) {
                    throw new JsonMappingException((Closeable) null, String.format("Failed to widen key type of %s with concrete-type annotation (value %s), from '%s': %s", javaType, findSerializationKeyType.getName(), ueVar.getName(), e2.getMessage()), e2);
                }
            }
            javaType = ((MapLikeType) javaType).withKeyType(constructSpecializedType2);
        }
        JavaType contentType = javaType.getContentType();
        if (contentType == null || (findSerializationContentType = findSerializationContentType(ueVar, contentType)) == null) {
            return javaType;
        }
        if (contentType.hasRawClass(findSerializationContentType)) {
            constructSpecializedType = contentType.withStaticTyping();
        } else {
            Class<?> rawClass3 = contentType.getRawClass();
            try {
                if (findSerializationContentType.isAssignableFrom(rawClass3)) {
                    constructSpecializedType = typeFactory.constructGeneralizedType(contentType, findSerializationContentType);
                } else if (rawClass3.isAssignableFrom(findSerializationContentType)) {
                    constructSpecializedType = typeFactory.constructSpecializedType(contentType, findSerializationContentType);
                } else {
                    throw new JsonMappingException((Closeable) null, String.format("Can not refine serialization content type %s into %s; types not related", contentType, findSerializationContentType.getName()));
                }
            } catch (IllegalArgumentException e3) {
                throw new JsonMappingException((Closeable) null, String.format("Internal error: failed to refine value type of %s with concrete-type annotation (value %s), from '%s': %s", javaType, findSerializationContentType.getName(), ueVar.getName(), e3.getMessage()), e3);
            }
        }
        return javaType.withContentType(constructSpecializedType);
    }

    public AnnotatedMethod resolveSetterConflict(MapperConfig<?> mapperConfig, AnnotatedMethod annotatedMethod, AnnotatedMethod annotatedMethod2) {
        return null;
    }

    public abstract Version version();

    public Collection<AnnotationIntrospector> allIntrospectors(Collection<AnnotationIntrospector> collection) {
        collection.add(this);
        return collection;
    }
}
