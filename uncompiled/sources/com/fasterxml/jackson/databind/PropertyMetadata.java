package com.fasterxml.jackson.databind;

import java.io.Serializable;

/* loaded from: classes.dex */
public class PropertyMetadata implements Serializable {
    private static final long serialVersionUID = -1;
    public final String _defaultValue;
    public final String _description;
    public final Integer _index;
    public final Boolean _required;
    public static final PropertyMetadata STD_REQUIRED = new PropertyMetadata(Boolean.TRUE, null, null, null);
    public static final PropertyMetadata STD_OPTIONAL = new PropertyMetadata(Boolean.FALSE, null, null, null);
    public static final PropertyMetadata STD_REQUIRED_OR_OPTIONAL = new PropertyMetadata(null, null, null, null);

    @Deprecated
    public PropertyMetadata(Boolean bool, String str) {
        this(bool, str, null, null);
    }

    @Deprecated
    public static PropertyMetadata construct(boolean z, String str) {
        return construct(z, str, (Integer) null, (String) null);
    }

    public String getDefaultValue() {
        return this._defaultValue;
    }

    public String getDescription() {
        return this._description;
    }

    public Integer getIndex() {
        return this._index;
    }

    public Boolean getRequired() {
        return this._required;
    }

    public boolean hasDefaultValue() {
        return this._defaultValue != null;
    }

    @Deprecated
    public boolean hasDefuaultValue() {
        return hasDefaultValue();
    }

    public boolean hasIndex() {
        return this._index != null;
    }

    public boolean isRequired() {
        Boolean bool = this._required;
        return bool != null && bool.booleanValue();
    }

    public Object readResolve() {
        if (this._description == null && this._index == null && this._defaultValue == null) {
            Boolean bool = this._required;
            if (bool == null) {
                return STD_REQUIRED_OR_OPTIONAL;
            }
            return bool.booleanValue() ? STD_REQUIRED : STD_OPTIONAL;
        }
        return this;
    }

    public PropertyMetadata withDefaultValue(String str) {
        if (str != null && !str.isEmpty()) {
            if (this._defaultValue.equals(str)) {
                return this;
            }
        } else if (this._defaultValue == null) {
            return this;
        } else {
            str = null;
        }
        return new PropertyMetadata(this._required, this._description, this._index, str);
    }

    public PropertyMetadata withDescription(String str) {
        return new PropertyMetadata(this._required, str, this._index, this._defaultValue);
    }

    public PropertyMetadata withIndex(Integer num) {
        return new PropertyMetadata(this._required, this._description, num, this._defaultValue);
    }

    public PropertyMetadata withRequired(Boolean bool) {
        if (bool == null) {
            if (this._required == null) {
                return this;
            }
        } else {
            Boolean bool2 = this._required;
            if (bool2 != null && bool2.booleanValue() == bool.booleanValue()) {
                return this;
            }
        }
        return new PropertyMetadata(bool, this._description, this._index, this._defaultValue);
    }

    public PropertyMetadata(Boolean bool, String str, Integer num, String str2) {
        this._required = bool;
        this._description = str;
        this._index = num;
        this._defaultValue = (str2 == null || str2.isEmpty()) ? null : null;
    }

    public static PropertyMetadata construct(Boolean bool, String str, Integer num, String str2) {
        if (str == null && num == null && str2 == null) {
            if (bool == null) {
                return STD_REQUIRED_OR_OPTIONAL;
            }
            return bool.booleanValue() ? STD_REQUIRED : STD_OPTIONAL;
        }
        return new PropertyMetadata(bool, str, num, str2);
    }

    @Deprecated
    public static PropertyMetadata construct(boolean z, String str, Integer num, String str2) {
        if (str == null && num == null && str2 == null) {
            return z ? STD_REQUIRED : STD_OPTIONAL;
        }
        return new PropertyMetadata(Boolean.valueOf(z), str, num, str2);
    }
}
