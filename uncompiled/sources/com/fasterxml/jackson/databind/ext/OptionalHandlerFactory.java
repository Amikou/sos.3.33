package com.fasterxml.jackson.databind.ext;

import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.c;
import com.fasterxml.jackson.databind.deser.f;
import java.io.Serializable;

/* loaded from: classes.dex */
public class OptionalHandlerFactory implements Serializable {
    public static final Class<?> a;
    public static final Class<?> f0;
    public static final qt1 g0;
    public static final OptionalHandlerFactory instance;
    private static final long serialVersionUID = 1;

    /*  JADX ERROR: JadxRuntimeException in pass: BlockProcessor
        jadx.core.utils.exceptions.JadxRuntimeException: Unreachable block: B:6:0x0006
        	at jadx.core.dex.visitors.blocks.BlockProcessor.checkForUnreachableBlocks(BlockProcessor.java:81)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.processBlocksTree(BlockProcessor.java:47)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.visit(BlockProcessor.java:39)
        */
    static {
        /*
            r0 = 0
            java.lang.Class<org.w3c.dom.Node> r1 = org.w3c.dom.Node.class
            java.lang.Class<org.w3c.dom.Document> r2 = org.w3c.dom.Document.class
            goto L19
        L6:
            r1 = r0
        L7:
            java.lang.Class<com.fasterxml.jackson.databind.ext.OptionalHandlerFactory> r2 = com.fasterxml.jackson.databind.ext.OptionalHandlerFactory.class
            java.lang.String r2 = r2.getName()
            java.util.logging.Logger r2 = java.util.logging.Logger.getLogger(r2)
            java.util.logging.Level r3 = java.util.logging.Level.INFO
            java.lang.String r4 = "Could not load DOM `Node` and/or `Document` classes: no DOM support"
            r2.log(r3, r4)
            r2 = r0
        L19:
            com.fasterxml.jackson.databind.ext.OptionalHandlerFactory.a = r1
            com.fasterxml.jackson.databind.ext.OptionalHandlerFactory.f0 = r2
            qt1 r0 = defpackage.qt1.f()     // Catch: java.lang.Throwable -> L21
        L21:
            com.fasterxml.jackson.databind.ext.OptionalHandlerFactory.g0 = r0
            com.fasterxml.jackson.databind.ext.OptionalHandlerFactory r0 = new com.fasterxml.jackson.databind.ext.OptionalHandlerFactory
            r0.<init>()
            com.fasterxml.jackson.databind.ext.OptionalHandlerFactory.instance = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fasterxml.jackson.databind.ext.OptionalHandlerFactory.<clinit>():void");
    }

    public final boolean a(Class<?> cls, String str) {
        do {
            cls = cls.getSuperclass();
            if (cls == null || cls == Object.class) {
                return false;
            }
        } while (!cls.getName().startsWith(str));
        return true;
    }

    public final Object b(String str) {
        try {
            return Class.forName(str).newInstance();
        } catch (Exception | LinkageError unused) {
            return null;
        }
    }

    public c<?> findDeserializer(JavaType javaType, DeserializationConfig deserializationConfig, so soVar) throws JsonMappingException {
        Object b;
        c<?> c;
        Class<?> rawClass = javaType.getRawClass();
        qt1 qt1Var = g0;
        if (qt1Var == null || (c = qt1Var.c(rawClass)) == null) {
            Class<?> cls = a;
            if (cls != null && cls.isAssignableFrom(rawClass)) {
                return (c) b("com.fasterxml.jackson.databind.ext.DOMDeserializer$NodeDeserializer");
            }
            Class<?> cls2 = f0;
            if (cls2 != null && cls2.isAssignableFrom(rawClass)) {
                return (c) b("com.fasterxml.jackson.databind.ext.DOMDeserializer$DocumentDeserializer");
            }
            if ((rawClass.getName().startsWith("javax.xml.") || a(rawClass, "javax.xml.")) && (b = b("com.fasterxml.jackson.databind.ext.CoreXMLDeserializers")) != null) {
                return ((f) b).findBeanDeserializer(javaType, deserializationConfig, soVar);
            }
            return null;
        }
        return c;
    }

    public com.fasterxml.jackson.databind.f<?> findSerializer(SerializationConfig serializationConfig, JavaType javaType, so soVar) {
        Object b;
        com.fasterxml.jackson.databind.f<?> d;
        Class<?> rawClass = javaType.getRawClass();
        qt1 qt1Var = g0;
        if (qt1Var == null || (d = qt1Var.d(rawClass)) == null) {
            Class<?> cls = a;
            if (cls != null && cls.isAssignableFrom(rawClass)) {
                return (com.fasterxml.jackson.databind.f) b("com.fasterxml.jackson.databind.ext.DOMSerializer");
            }
            if ((rawClass.getName().startsWith("javax.xml.") || a(rawClass, "javax.xml.")) && (b = b("com.fasterxml.jackson.databind.ext.CoreXMLSerializers")) != null) {
                return ((am3) b).findSerializer(serializationConfig, javaType, soVar);
            }
            return null;
        }
        return d;
    }
}
