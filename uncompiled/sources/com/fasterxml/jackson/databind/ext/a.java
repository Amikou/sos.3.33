package com.fasterxml.jackson.databind.ext;

import com.fasterxml.jackson.databind.PropertyName;
import com.fasterxml.jackson.databind.c;
import com.fasterxml.jackson.databind.f;
import com.fasterxml.jackson.databind.introspect.AnnotatedParameter;
import com.fasterxml.jackson.databind.introspect.AnnotatedWithParams;
import java.beans.ConstructorProperties;
import java.beans.Transient;
import java.nio.file.Path;

/* compiled from: Java7SupportImpl.java */
/* loaded from: classes.dex */
public class a extends qt1 {
    @Override // defpackage.qt1
    public PropertyName a(AnnotatedParameter annotatedParameter) {
        ConstructorProperties annotation;
        AnnotatedWithParams owner = annotatedParameter.getOwner();
        if (owner == null || (annotation = owner.getAnnotation(ConstructorProperties.class)) == null) {
            return null;
        }
        String[] value = annotation.value();
        int index = annotatedParameter.getIndex();
        if (index < value.length) {
            return PropertyName.construct(value[index]);
        }
        return null;
    }

    @Override // defpackage.qt1
    public Boolean b(ue ueVar) {
        Transient annotation = ueVar.getAnnotation(Transient.class);
        if (annotation != null) {
            return Boolean.valueOf(annotation.value());
        }
        return null;
    }

    @Override // defpackage.qt1
    public c<?> c(Class<?> cls) {
        if (cls == Path.class) {
            return new NioPathDeserializer();
        }
        return null;
    }

    @Override // defpackage.qt1
    public f<?> d(Class<?> cls) {
        if (Path.class.isAssignableFrom(cls)) {
            return new NioPathSerializer();
        }
        return null;
    }

    @Override // defpackage.qt1
    public Boolean e(ue ueVar) {
        if (ueVar.getAnnotation(ConstructorProperties.class) != null) {
            return Boolean.TRUE;
        }
        return null;
    }
}
