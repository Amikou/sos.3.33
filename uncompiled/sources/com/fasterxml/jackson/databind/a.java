package com.fasterxml.jackson.databind;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import java.lang.annotation.Annotation;

/* compiled from: BeanProperty.java */
/* loaded from: classes.dex */
public interface a {
    public static final JsonFormat.Value V = new JsonFormat.Value();

    /* compiled from: BeanProperty.java */
    /* renamed from: com.fasterxml.jackson.databind.a$a  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static class C0091a implements a {
        public final PropertyName a;
        public final JavaType f0;
        public final PropertyName g0;
        public final PropertyMetadata h0;
        public final AnnotatedMember i0;
        public final xe j0;

        public C0091a(PropertyName propertyName, JavaType javaType, PropertyName propertyName2, xe xeVar, AnnotatedMember annotatedMember, PropertyMetadata propertyMetadata) {
            this.a = propertyName;
            this.f0 = javaType;
            this.g0 = propertyName2;
            this.h0 = propertyMetadata;
            this.i0 = annotatedMember;
            this.j0 = xeVar;
        }

        @Override // com.fasterxml.jackson.databind.a
        public void depositSchemaProperty(com.fasterxml.jackson.databind.jsonFormatVisitors.d dVar, k kVar) {
            throw new UnsupportedOperationException("Instances of " + getClass().getName() + " should not get visited");
        }

        @Override // com.fasterxml.jackson.databind.a
        public JsonFormat.Value findPropertyFormat(MapperConfig<?> mapperConfig, Class<?> cls) {
            AnnotatedMember annotatedMember;
            JsonFormat.Value findFormat;
            JsonFormat.Value defaultPropertyFormat = mapperConfig.getDefaultPropertyFormat(cls);
            AnnotationIntrospector annotationIntrospector = mapperConfig.getAnnotationIntrospector();
            return (annotationIntrospector == null || (annotatedMember = this.i0) == null || (findFormat = annotationIntrospector.findFormat(annotatedMember)) == null) ? defaultPropertyFormat : defaultPropertyFormat.withOverrides(findFormat);
        }

        @Override // com.fasterxml.jackson.databind.a
        public JsonInclude.Value findPropertyInclusion(MapperConfig<?> mapperConfig, Class<?> cls) {
            AnnotatedMember annotatedMember;
            JsonInclude.Value findPropertyInclusion;
            JsonInclude.Value defaultPropertyInclusion = mapperConfig.getDefaultPropertyInclusion(cls);
            AnnotationIntrospector annotationIntrospector = mapperConfig.getAnnotationIntrospector();
            return (annotationIntrospector == null || (annotatedMember = this.i0) == null || (findPropertyInclusion = annotationIntrospector.findPropertyInclusion(annotatedMember)) == null) ? defaultPropertyInclusion : defaultPropertyInclusion.withOverrides(findPropertyInclusion);
        }

        @Override // com.fasterxml.jackson.databind.a
        public <A extends Annotation> A getAnnotation(Class<A> cls) {
            AnnotatedMember annotatedMember = this.i0;
            if (annotatedMember == null) {
                return null;
            }
            return (A) annotatedMember.getAnnotation(cls);
        }

        @Override // com.fasterxml.jackson.databind.a
        public <A extends Annotation> A getContextAnnotation(Class<A> cls) {
            xe xeVar = this.j0;
            if (xeVar == null) {
                return null;
            }
            return (A) xeVar.a(cls);
        }

        @Override // com.fasterxml.jackson.databind.a
        public AnnotatedMember getMember() {
            return this.i0;
        }

        @Override // com.fasterxml.jackson.databind.a
        public PropertyMetadata getMetadata() {
            return this.h0;
        }

        @Override // com.fasterxml.jackson.databind.a
        public String getName() {
            return this.a.getSimpleName();
        }

        @Override // com.fasterxml.jackson.databind.a
        public JavaType getType() {
            return this.f0;
        }

        @Override // com.fasterxml.jackson.databind.a
        public PropertyName getWrapperName() {
            return this.g0;
        }
    }

    static {
        JsonInclude.Value.empty();
    }

    void depositSchemaProperty(com.fasterxml.jackson.databind.jsonFormatVisitors.d dVar, k kVar) throws JsonMappingException;

    JsonFormat.Value findPropertyFormat(MapperConfig<?> mapperConfig, Class<?> cls);

    JsonInclude.Value findPropertyInclusion(MapperConfig<?> mapperConfig, Class<?> cls);

    <A extends Annotation> A getAnnotation(Class<A> cls);

    <A extends Annotation> A getContextAnnotation(Class<A> cls);

    AnnotatedMember getMember();

    PropertyMetadata getMetadata();

    String getName();

    JavaType getType();

    PropertyName getWrapperName();
}
