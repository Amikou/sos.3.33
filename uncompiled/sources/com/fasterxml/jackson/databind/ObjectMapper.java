package com.fasterxml.jackson.databind;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.Base64Variant;
import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.core.io.CharacterEscapes;
import com.fasterxml.jackson.databind.cfg.BaseSettings;
import com.fasterxml.jackson.databind.cfg.ConfigOverrides;
import com.fasterxml.jackson.databind.cfg.ContextAttributes;
import com.fasterxml.jackson.databind.cfg.MutableConfigOverride;
import com.fasterxml.jackson.databind.deser.BeanDeserializerFactory;
import com.fasterxml.jackson.databind.deser.DefaultDeserializationContext;
import com.fasterxml.jackson.databind.i;
import com.fasterxml.jackson.databind.introspect.BasicClassIntrospector;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import com.fasterxml.jackson.databind.introspect.SimpleMixInResolver;
import com.fasterxml.jackson.databind.introspect.VisibilityChecker;
import com.fasterxml.jackson.databind.introspect.c;
import com.fasterxml.jackson.databind.jsontype.NamedType;
import com.fasterxml.jackson.databind.jsontype.impl.StdSubtypeResolver;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.l;
import com.fasterxml.jackson.databind.node.m;
import com.fasterxml.jackson.databind.node.n;
import com.fasterxml.jackson.databind.node.q;
import com.fasterxml.jackson.databind.ser.BeanSerializerFactory;
import com.fasterxml.jackson.databind.ser.DefaultSerializerProvider;
import com.fasterxml.jackson.databind.type.SimpleType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.databind.util.RootNameLookup;
import com.fasterxml.jackson.databind.util.StdDateFormat;
import java.io.Closeable;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.lang.reflect.Type;
import java.net.URL;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ServiceLoader;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;

/* loaded from: classes.dex */
public class ObjectMapper extends com.fasterxml.jackson.core.c implements Serializable {
    public static final AnnotationIntrospector DEFAULT_ANNOTATION_INTROSPECTOR;
    public static final BaseSettings DEFAULT_BASE;
    public static final VisibilityChecker<?> STD_VISIBILITY_CHECKER;
    public static final JavaType a = SimpleType.constructUnsafe(d.class);
    private static final long serialVersionUID = 1;
    public DeserializationConfig _deserializationConfig;
    public DefaultDeserializationContext _deserializationContext;
    public sq1 _injectableValues;
    public final JsonFactory _jsonFactory;
    public SimpleMixInResolver _mixIns;
    public ConfigOverrides _propertyOverrides;
    public Set<Object> _registeredModuleTypes;
    public final ConcurrentHashMap<JavaType, com.fasterxml.jackson.databind.c<Object>> _rootDeserializers;
    public SerializationConfig _serializationConfig;
    public com.fasterxml.jackson.databind.ser.g _serializerFactory;
    public DefaultSerializerProvider _serializerProvider;
    public aw3 _subtypeResolver;
    public TypeFactory _typeFactory;

    /* loaded from: classes.dex */
    public static class DefaultTypeResolverBuilder extends pt3 implements Serializable {
        private static final long serialVersionUID = 1;
        public final DefaultTyping _appliesFor;

        public DefaultTypeResolverBuilder(DefaultTyping defaultTyping) {
            this._appliesFor = defaultTyping;
        }

        @Override // defpackage.pt3, defpackage.vd4
        public com.fasterxml.jackson.databind.jsontype.a buildTypeDeserializer(DeserializationConfig deserializationConfig, JavaType javaType, Collection<NamedType> collection) {
            if (useForType(javaType)) {
                return super.buildTypeDeserializer(deserializationConfig, javaType, collection);
            }
            return null;
        }

        @Override // defpackage.pt3, defpackage.vd4
        public com.fasterxml.jackson.databind.jsontype.c buildTypeSerializer(SerializationConfig serializationConfig, JavaType javaType, Collection<NamedType> collection) {
            if (useForType(javaType)) {
                return super.buildTypeSerializer(serializationConfig, javaType, collection);
            }
            return null;
        }

        public boolean useForType(JavaType javaType) {
            if (javaType.isPrimitive()) {
                return false;
            }
            int i = c.a[this._appliesFor.ordinal()];
            if (i == 1) {
                while (javaType.isArrayType()) {
                    javaType = javaType.getContentType();
                }
            } else if (i != 2) {
                if (i != 3) {
                    return javaType.isJavaLangObject();
                }
                while (javaType.isArrayType()) {
                    javaType = javaType.getContentType();
                }
                while (javaType.isReferenceType()) {
                    javaType = javaType.getReferencedType();
                }
                return (javaType.isFinal() || com.fasterxml.jackson.core.f.class.isAssignableFrom(javaType.getRawClass())) ? false : true;
            }
            while (javaType.isReferenceType()) {
                javaType = javaType.getReferencedType();
            }
            return javaType.isJavaLangObject() || !(javaType.isConcrete() || com.fasterxml.jackson.core.f.class.isAssignableFrom(javaType.getRawClass()));
        }
    }

    /* loaded from: classes.dex */
    public enum DefaultTyping {
        JAVA_LANG_OBJECT,
        OBJECT_AND_NON_CONCRETE,
        NON_CONCRETE_AND_ARRAYS,
        NON_FINAL
    }

    /* loaded from: classes.dex */
    public class a implements i.a {
        public final /* synthetic */ ObjectMapper a;

        public a(ObjectMapper objectMapper, ObjectMapper objectMapper2) {
            this.a = objectMapper2;
        }

        @Override // com.fasterxml.jackson.databind.i.a
        public void a(com.fasterxml.jackson.databind.deser.f fVar) {
            com.fasterxml.jackson.databind.deser.e withAdditionalDeserializers = this.a._deserializationContext._factory.withAdditionalDeserializers(fVar);
            ObjectMapper objectMapper = this.a;
            objectMapper._deserializationContext = objectMapper._deserializationContext.with(withAdditionalDeserializers);
        }

        @Override // com.fasterxml.jackson.databind.i.a
        public void b(ah4 ah4Var) {
            com.fasterxml.jackson.databind.deser.e withValueInstantiators = this.a._deserializationContext._factory.withValueInstantiators(ah4Var);
            ObjectMapper objectMapper = this.a;
            objectMapper._deserializationContext = objectMapper._deserializationContext.with(withValueInstantiators);
        }

        @Override // com.fasterxml.jackson.databind.i.a
        public void c(com.fasterxml.jackson.databind.deser.g gVar) {
            com.fasterxml.jackson.databind.deser.e withAdditionalKeyDeserializers = this.a._deserializationContext._factory.withAdditionalKeyDeserializers(gVar);
            ObjectMapper objectMapper = this.a;
            objectMapper._deserializationContext = objectMapper._deserializationContext.with(withAdditionalKeyDeserializers);
        }

        @Override // com.fasterxml.jackson.databind.i.a
        public void d(NamedType... namedTypeArr) {
            this.a.registerSubtypes(namedTypeArr);
        }

        @Override // com.fasterxml.jackson.databind.i.a
        public void e(am3 am3Var) {
            ObjectMapper objectMapper = this.a;
            objectMapper._serializerFactory = objectMapper._serializerFactory.withAdditionalSerializers(am3Var);
        }

        @Override // com.fasterxml.jackson.databind.i.a
        public void f(o5 o5Var) {
            com.fasterxml.jackson.databind.deser.e withAbstractTypeResolver = this.a._deserializationContext._factory.withAbstractTypeResolver(o5Var);
            ObjectMapper objectMapper = this.a;
            objectMapper._deserializationContext = objectMapper._deserializationContext.with(withAbstractTypeResolver);
        }

        @Override // com.fasterxml.jackson.databind.i.a
        public void g(am3 am3Var) {
            ObjectMapper objectMapper = this.a;
            objectMapper._serializerFactory = objectMapper._serializerFactory.withAdditionalKeySerializers(am3Var);
        }

        @Override // com.fasterxml.jackson.databind.i.a
        public void h(PropertyNamingStrategy propertyNamingStrategy) {
            this.a.setPropertyNamingStrategy(propertyNamingStrategy);
        }

        @Override // com.fasterxml.jackson.databind.i.a
        public void i(uo uoVar) {
            com.fasterxml.jackson.databind.deser.e withDeserializerModifier = this.a._deserializationContext._factory.withDeserializerModifier(uoVar);
            ObjectMapper objectMapper = this.a;
            objectMapper._deserializationContext = objectMapper._deserializationContext.with(withDeserializerModifier);
        }

        @Override // com.fasterxml.jackson.databind.i.a
        public void j(Class<?> cls, Class<?> cls2) {
            this.a.addMixIn(cls, cls2);
        }

        @Override // com.fasterxml.jackson.databind.i.a
        public void k(xo xoVar) {
            ObjectMapper objectMapper = this.a;
            objectMapper._serializerFactory = objectMapper._serializerFactory.withSerializerModifier(xoVar);
        }
    }

    /* loaded from: classes.dex */
    public static class b implements PrivilegedAction<ServiceLoader<T>> {
        public final /* synthetic */ ClassLoader a;
        public final /* synthetic */ Class b;

        public b(ClassLoader classLoader, Class cls) {
            this.a = classLoader;
            this.b = cls;
        }

        @Override // java.security.PrivilegedAction
        /* renamed from: a */
        public ServiceLoader<T> run() {
            ClassLoader classLoader = this.a;
            return classLoader == null ? ServiceLoader.load(this.b) : ServiceLoader.load(this.b, classLoader);
        }
    }

    /* loaded from: classes.dex */
    public static /* synthetic */ class c {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[DefaultTyping.values().length];
            a = iArr;
            try {
                iArr[DefaultTyping.NON_CONCRETE_AND_ARRAYS.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[DefaultTyping.OBJECT_AND_NON_CONCRETE.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[DefaultTyping.NON_FINAL.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    static {
        JacksonAnnotationIntrospector jacksonAnnotationIntrospector = new JacksonAnnotationIntrospector();
        DEFAULT_ANNOTATION_INTROSPECTOR = jacksonAnnotationIntrospector;
        VisibilityChecker.Std defaultInstance = VisibilityChecker.Std.defaultInstance();
        STD_VISIBILITY_CHECKER = defaultInstance;
        DEFAULT_BASE = new BaseSettings(null, jacksonAnnotationIntrospector, defaultInstance, null, TypeFactory.defaultInstance(), null, StdDateFormat.instance, null, Locale.getDefault(), null, com.fasterxml.jackson.core.a.a());
    }

    public ObjectMapper() {
        this(null, null, null);
    }

    public static <T> ServiceLoader<T> c(Class<T> cls, ClassLoader classLoader) {
        if (System.getSecurityManager() == null) {
            return classLoader == null ? ServiceLoader.load(cls) : ServiceLoader.load(cls, classLoader);
        }
        return (ServiceLoader) AccessController.doPrivileged(new b(classLoader, cls));
    }

    public static List<i> findModules() {
        return findModules(null);
    }

    public void _checkInvalidCopy(Class<?> cls) {
        if (getClass() == cls) {
            return;
        }
        throw new IllegalStateException("Failed copy(): " + getClass().getName() + " (version: " + version() + ") does not override copy(); it has to");
    }

    public final void _configAndWriteValue(JsonGenerator jsonGenerator, Object obj) throws IOException {
        SerializationConfig serializationConfig = getSerializationConfig();
        serializationConfig.initialize(jsonGenerator);
        if (serializationConfig.isEnabled(SerializationFeature.CLOSE_CLOSEABLE) && (obj instanceof Closeable)) {
            a(jsonGenerator, obj, serializationConfig);
            return;
        }
        try {
            _serializerProvider(serializationConfig).serializeValue(jsonGenerator, obj);
            jsonGenerator.close();
        } catch (Exception e) {
            com.fasterxml.jackson.databind.util.c.h(jsonGenerator, e);
        }
    }

    public Object _convert(Object obj, JavaType javaType) throws IllegalArgumentException {
        Object obj2;
        Class<?> rawClass = javaType.getRawClass();
        if (rawClass == Object.class || javaType.hasGenericTypes() || !rawClass.isAssignableFrom(obj.getClass())) {
            com.fasterxml.jackson.databind.util.e eVar = new com.fasterxml.jackson.databind.util.e((com.fasterxml.jackson.core.c) this, false);
            if (isEnabled(DeserializationFeature.USE_BIG_DECIMAL_FOR_FLOATS)) {
                eVar = eVar.R1(true);
            }
            try {
                _serializerProvider(getSerializationConfig().without(SerializationFeature.WRAP_ROOT_VALUE)).serializeValue(eVar, obj);
                JsonParser K1 = eVar.K1();
                DeserializationConfig deserializationConfig = getDeserializationConfig();
                JsonToken _initForReading = _initForReading(K1);
                if (_initForReading == JsonToken.VALUE_NULL) {
                    DefaultDeserializationContext createDeserializationContext = createDeserializationContext(K1, deserializationConfig);
                    obj2 = _findRootDeserializer(createDeserializationContext, javaType).getNullValue(createDeserializationContext);
                } else {
                    if (_initForReading != JsonToken.END_ARRAY && _initForReading != JsonToken.END_OBJECT) {
                        DefaultDeserializationContext createDeserializationContext2 = createDeserializationContext(K1, deserializationConfig);
                        obj2 = _findRootDeserializer(createDeserializationContext2, javaType).deserialize(K1, createDeserializationContext2);
                    }
                    obj2 = null;
                }
                K1.close();
                return obj2;
            } catch (IOException e) {
                throw new IllegalArgumentException(e.getMessage(), e);
            }
        }
        return obj;
    }

    public com.fasterxml.jackson.databind.c<Object> _findRootDeserializer(DeserializationContext deserializationContext, JavaType javaType) throws JsonMappingException {
        com.fasterxml.jackson.databind.c<Object> cVar = this._rootDeserializers.get(javaType);
        if (cVar != null) {
            return cVar;
        }
        com.fasterxml.jackson.databind.c<Object> findRootValueDeserializer = deserializationContext.findRootValueDeserializer(javaType);
        if (findRootValueDeserializer != null) {
            this._rootDeserializers.put(javaType, findRootValueDeserializer);
            return findRootValueDeserializer;
        }
        throw JsonMappingException.from(deserializationContext, "Can not find a deserializer for type " + javaType);
    }

    public JsonToken _initForReading(JsonParser jsonParser) throws IOException {
        this._deserializationConfig.initialize(jsonParser);
        JsonToken u = jsonParser.u();
        if (u == null && (u = jsonParser.T0()) == null) {
            throw JsonMappingException.from(jsonParser, "No content to map due to end-of-input");
        }
        return u;
    }

    public ObjectReader _newReader(DeserializationConfig deserializationConfig) {
        return new ObjectReader(this, deserializationConfig);
    }

    public ObjectWriter _newWriter(SerializationConfig serializationConfig) {
        return new ObjectWriter(this, serializationConfig);
    }

    public Object _readMapAndClose(JsonParser jsonParser, JavaType javaType) throws IOException {
        Object obj;
        try {
            JsonToken _initForReading = _initForReading(jsonParser);
            if (_initForReading == JsonToken.VALUE_NULL) {
                DefaultDeserializationContext createDeserializationContext = createDeserializationContext(jsonParser, getDeserializationConfig());
                obj = _findRootDeserializer(createDeserializationContext, javaType).getNullValue(createDeserializationContext);
            } else {
                if (_initForReading != JsonToken.END_ARRAY && _initForReading != JsonToken.END_OBJECT) {
                    DeserializationConfig deserializationConfig = getDeserializationConfig();
                    DefaultDeserializationContext createDeserializationContext2 = createDeserializationContext(jsonParser, deserializationConfig);
                    com.fasterxml.jackson.databind.c<Object> _findRootDeserializer = _findRootDeserializer(createDeserializationContext2, javaType);
                    if (deserializationConfig.useRootWrapping()) {
                        obj = _unwrapAndDeserialize(jsonParser, createDeserializationContext2, deserializationConfig, javaType, _findRootDeserializer);
                    } else {
                        obj = _findRootDeserializer.deserialize(jsonParser, createDeserializationContext2);
                    }
                    createDeserializationContext2.checkUnresolvedObjectId();
                }
                obj = null;
            }
            jsonParser.e();
            jsonParser.close();
            return obj;
        } catch (Throwable th) {
            try {
                throw th;
            } catch (Throwable th2) {
                if (jsonParser != null) {
                    try {
                        jsonParser.close();
                    } catch (Throwable th3) {
                        th.addSuppressed(th3);
                    }
                }
                throw th2;
            }
        }
    }

    public Object _readValue(DeserializationConfig deserializationConfig, JsonParser jsonParser, JavaType javaType) throws IOException {
        Object obj;
        JsonToken _initForReading = _initForReading(jsonParser);
        if (_initForReading == JsonToken.VALUE_NULL) {
            DefaultDeserializationContext createDeserializationContext = createDeserializationContext(jsonParser, deserializationConfig);
            obj = _findRootDeserializer(createDeserializationContext, javaType).getNullValue(createDeserializationContext);
        } else if (_initForReading == JsonToken.END_ARRAY || _initForReading == JsonToken.END_OBJECT) {
            obj = null;
        } else {
            DefaultDeserializationContext createDeserializationContext2 = createDeserializationContext(jsonParser, deserializationConfig);
            com.fasterxml.jackson.databind.c<Object> _findRootDeserializer = _findRootDeserializer(createDeserializationContext2, javaType);
            if (deserializationConfig.useRootWrapping()) {
                obj = _unwrapAndDeserialize(jsonParser, createDeserializationContext2, deserializationConfig, javaType, _findRootDeserializer);
            } else {
                obj = _findRootDeserializer.deserialize(jsonParser, createDeserializationContext2);
            }
        }
        jsonParser.e();
        return obj;
    }

    public DefaultSerializerProvider _serializerProvider(SerializationConfig serializationConfig) {
        return this._serializerProvider.createInstance(serializationConfig, this._serializerFactory);
    }

    public Object _unwrapAndDeserialize(JsonParser jsonParser, DeserializationContext deserializationContext, DeserializationConfig deserializationConfig, JavaType javaType, com.fasterxml.jackson.databind.c<Object> cVar) throws IOException {
        String simpleName = deserializationConfig.findRootName(javaType).getSimpleName();
        JsonToken u = jsonParser.u();
        JsonToken jsonToken = JsonToken.START_OBJECT;
        if (u != jsonToken) {
            deserializationContext.reportWrongTokenException(jsonParser, jsonToken, "Current token not START_OBJECT (needed to unwrap root name '%s'), but %s", simpleName, jsonParser.u());
        }
        JsonToken T0 = jsonParser.T0();
        JsonToken jsonToken2 = JsonToken.FIELD_NAME;
        if (T0 != jsonToken2) {
            deserializationContext.reportWrongTokenException(jsonParser, jsonToken2, "Current token not FIELD_NAME (to contain expected root name '" + simpleName + "'), but " + jsonParser.u(), new Object[0]);
        }
        String r = jsonParser.r();
        if (!simpleName.equals(r)) {
            deserializationContext.reportMappingException("Root name '%s' does not match expected ('%s') for type %s", r, simpleName, javaType);
        }
        jsonParser.T0();
        Object deserialize = cVar.deserialize(jsonParser, deserializationContext);
        JsonToken T02 = jsonParser.T0();
        JsonToken jsonToken3 = JsonToken.END_OBJECT;
        if (T02 != jsonToken3) {
            deserializationContext.reportWrongTokenException(jsonParser, jsonToken3, "Current token not END_OBJECT (to match wrapper object with root name '%s'), but %s", simpleName, jsonParser.u());
        }
        return deserialize;
    }

    public void _verifySchemaType(z81 z81Var) {
        if (z81Var == null || this._jsonFactory.canUseSchema(z81Var)) {
            return;
        }
        throw new IllegalArgumentException("Can not use FormatSchema of type " + z81Var.getClass().getName() + " for format " + this._jsonFactory.getFormatName());
    }

    public final void a(JsonGenerator jsonGenerator, Object obj, SerializationConfig serializationConfig) throws IOException {
        Closeable closeable = (Closeable) obj;
        try {
            _serializerProvider(serializationConfig).serializeValue(jsonGenerator, obj);
            try {
                closeable.close();
                jsonGenerator.close();
            } catch (Exception e) {
                e = e;
                closeable = null;
                com.fasterxml.jackson.databind.util.c.g(jsonGenerator, closeable, e);
            }
        } catch (Exception e2) {
            e = e2;
        }
    }

    public void acceptJsonFormatVisitor(Class<?> cls, com.fasterxml.jackson.databind.jsonFormatVisitors.b bVar) throws JsonMappingException {
        acceptJsonFormatVisitor(this._typeFactory.constructType(cls), bVar);
    }

    public ObjectMapper addHandler(com.fasterxml.jackson.databind.deser.d dVar) {
        this._deserializationConfig = this._deserializationConfig.withHandler(dVar);
        return this;
    }

    public ObjectMapper addMixIn(Class<?> cls, Class<?> cls2) {
        this._mixIns.addLocalDefinition(cls, cls2);
        return this;
    }

    @Deprecated
    public final void addMixInAnnotations(Class<?> cls, Class<?> cls2) {
        addMixIn(cls, cls2);
    }

    public final void b(JsonGenerator jsonGenerator, Object obj, SerializationConfig serializationConfig) throws IOException {
        Closeable closeable = (Closeable) obj;
        try {
            _serializerProvider(serializationConfig).serializeValue(jsonGenerator, obj);
            if (serializationConfig.isEnabled(SerializationFeature.FLUSH_AFTER_WRITE_VALUE)) {
                jsonGenerator.flush();
            }
            closeable.close();
        } catch (Exception e) {
            com.fasterxml.jackson.databind.util.c.g(null, closeable, e);
        }
    }

    public boolean canDeserialize(JavaType javaType) {
        return createDeserializationContext(null, getDeserializationConfig()).hasValueDeserializerFor(javaType, null);
    }

    public boolean canSerialize(Class<?> cls) {
        return _serializerProvider(getSerializationConfig()).hasSerializerFor(cls, null);
    }

    public ObjectMapper clearProblemHandlers() {
        this._deserializationConfig = this._deserializationConfig.withNoProblemHandlers();
        return this;
    }

    public MutableConfigOverride configOverride(Class<?> cls) {
        return this._propertyOverrides.findOrCreateOverride(cls);
    }

    public ObjectMapper configure(MapperFeature mapperFeature, boolean z) {
        this._serializationConfig = z ? this._serializationConfig.with(mapperFeature) : this._serializationConfig.without(mapperFeature);
        this._deserializationConfig = z ? this._deserializationConfig.with(mapperFeature) : this._deserializationConfig.without(mapperFeature);
        return this;
    }

    public JavaType constructType(Type type) {
        return this._typeFactory.constructType(type);
    }

    public <T> T convertValue(Object obj, Class<T> cls) throws IllegalArgumentException {
        if (obj == null) {
            return null;
        }
        return (T) _convert(obj, this._typeFactory.constructType(cls));
    }

    public ObjectMapper copy() {
        _checkInvalidCopy(ObjectMapper.class);
        return new ObjectMapper(this);
    }

    public DefaultDeserializationContext createDeserializationContext(JsonParser jsonParser, DeserializationConfig deserializationConfig) {
        return this._deserializationContext.createInstance(deserializationConfig, jsonParser, this._injectableValues);
    }

    public com.fasterxml.jackson.databind.introspect.c defaultClassIntrospector() {
        return new BasicClassIntrospector();
    }

    public ObjectMapper disable(MapperFeature... mapperFeatureArr) {
        this._deserializationConfig = this._deserializationConfig.without(mapperFeatureArr);
        this._serializationConfig = this._serializationConfig.without(mapperFeatureArr);
        return this;
    }

    public ObjectMapper disableDefaultTyping() {
        return setDefaultTyping(null);
    }

    public ObjectMapper enable(MapperFeature... mapperFeatureArr) {
        this._deserializationConfig = this._deserializationConfig.with(mapperFeatureArr);
        this._serializationConfig = this._serializationConfig.with(mapperFeatureArr);
        return this;
    }

    public ObjectMapper enableDefaultTyping() {
        return enableDefaultTyping(DefaultTyping.OBJECT_AND_NON_CONCRETE);
    }

    public ObjectMapper enableDefaultTypingAsProperty(DefaultTyping defaultTyping, String str) {
        return setDefaultTyping(new DefaultTypeResolverBuilder(defaultTyping).init(JsonTypeInfo.Id.CLASS, (com.fasterxml.jackson.databind.jsontype.b) null).inclusion(JsonTypeInfo.As.PROPERTY).typeProperty(str));
    }

    public ObjectMapper findAndRegisterModules() {
        return registerModules(findModules());
    }

    public Class<?> findMixInClassFor(Class<?> cls) {
        return this._mixIns.findMixInClassFor(cls);
    }

    @Deprecated
    public aw1 generateJsonSchema(Class<?> cls) throws JsonMappingException {
        return _serializerProvider(getSerializationConfig()).generateJsonSchema(cls);
    }

    public DateFormat getDateFormat() {
        return this._serializationConfig.getDateFormat();
    }

    public DeserializationConfig getDeserializationConfig() {
        return this._deserializationConfig;
    }

    public DeserializationContext getDeserializationContext() {
        return this._deserializationContext;
    }

    @Override // com.fasterxml.jackson.core.c
    public JsonFactory getFactory() {
        return this._jsonFactory;
    }

    public sq1 getInjectableValues() {
        return this._injectableValues;
    }

    @Override // com.fasterxml.jackson.core.c
    @Deprecated
    public JsonFactory getJsonFactory() {
        return getFactory();
    }

    public JsonNodeFactory getNodeFactory() {
        return this._deserializationConfig.getNodeFactory();
    }

    public PropertyNamingStrategy getPropertyNamingStrategy() {
        return this._serializationConfig.getPropertyNamingStrategy();
    }

    public SerializationConfig getSerializationConfig() {
        return this._serializationConfig;
    }

    public com.fasterxml.jackson.databind.ser.g getSerializerFactory() {
        return this._serializerFactory;
    }

    public k getSerializerProvider() {
        return this._serializerProvider;
    }

    public k getSerializerProviderInstance() {
        return _serializerProvider(this._serializationConfig);
    }

    public aw3 getSubtypeResolver() {
        return this._subtypeResolver;
    }

    public TypeFactory getTypeFactory() {
        return this._typeFactory;
    }

    public VisibilityChecker<?> getVisibilityChecker() {
        return this._serializationConfig.getDefaultVisibilityChecker();
    }

    public boolean isEnabled(MapperFeature mapperFeature) {
        return this._serializationConfig.isEnabled(mapperFeature);
    }

    public int mixInCount() {
        return this._mixIns.localSize();
    }

    @Override // com.fasterxml.jackson.core.c, com.fasterxml.jackson.core.e
    public <T extends com.fasterxml.jackson.core.f> T readTree(JsonParser jsonParser) throws IOException, JsonProcessingException {
        DeserializationConfig deserializationConfig = getDeserializationConfig();
        if (jsonParser.u() == null && jsonParser.T0() == null) {
            return null;
        }
        d dVar = (d) _readValue(deserializationConfig, jsonParser, a);
        return dVar == null ? getNodeFactory().m4nullNode() : dVar;
    }

    @Override // com.fasterxml.jackson.core.c
    public <T> T readValue(JsonParser jsonParser, Class<T> cls) throws IOException, JsonParseException, JsonMappingException {
        return (T) _readValue(getDeserializationConfig(), jsonParser, this._typeFactory.constructType(cls));
    }

    public ObjectReader reader() {
        return _newReader(getDeserializationConfig()).with(this._injectableValues);
    }

    public ObjectReader readerFor(JavaType javaType) {
        return _newReader(getDeserializationConfig(), javaType, null, null, this._injectableValues);
    }

    public ObjectReader readerForUpdating(Object obj) {
        return _newReader(getDeserializationConfig(), this._typeFactory.constructType(obj.getClass()), obj, null, this._injectableValues);
    }

    public ObjectReader readerWithView(Class<?> cls) {
        return _newReader(getDeserializationConfig().withView(cls));
    }

    public ObjectMapper registerModule(i iVar) {
        Object typeId;
        if (isEnabled(MapperFeature.IGNORE_DUPLICATE_MODULE_REGISTRATIONS) && (typeId = iVar.getTypeId()) != null) {
            if (this._registeredModuleTypes == null) {
                this._registeredModuleTypes = new LinkedHashSet();
            }
            if (!this._registeredModuleTypes.add(typeId)) {
                return this;
            }
        }
        if (iVar.getModuleName() != null) {
            if (iVar.version() != null) {
                iVar.setupModule(new a(this, this));
                return this;
            }
            throw new IllegalArgumentException("Module without defined version");
        }
        throw new IllegalArgumentException("Module without defined name");
    }

    public ObjectMapper registerModules(i... iVarArr) {
        for (i iVar : iVarArr) {
            registerModule(iVar);
        }
        return this;
    }

    public void registerSubtypes(Class<?>... clsArr) {
        getSubtypeResolver().registerSubtypes(clsArr);
    }

    public ObjectMapper setAnnotationIntrospector(AnnotationIntrospector annotationIntrospector) {
        this._serializationConfig = this._serializationConfig.with(annotationIntrospector);
        this._deserializationConfig = this._deserializationConfig.with(annotationIntrospector);
        return this;
    }

    public ObjectMapper setAnnotationIntrospectors(AnnotationIntrospector annotationIntrospector, AnnotationIntrospector annotationIntrospector2) {
        this._serializationConfig = this._serializationConfig.with(annotationIntrospector);
        this._deserializationConfig = this._deserializationConfig.with(annotationIntrospector2);
        return this;
    }

    public ObjectMapper setBase64Variant(Base64Variant base64Variant) {
        this._serializationConfig = this._serializationConfig.with(base64Variant);
        this._deserializationConfig = this._deserializationConfig.with(base64Variant);
        return this;
    }

    public ObjectMapper setConfig(DeserializationConfig deserializationConfig) {
        this._deserializationConfig = deserializationConfig;
        return this;
    }

    public ObjectMapper setDateFormat(DateFormat dateFormat) {
        this._deserializationConfig = this._deserializationConfig.with(dateFormat);
        this._serializationConfig = this._serializationConfig.with(dateFormat);
        return this;
    }

    public ObjectMapper setDefaultPrettyPrinter(com.fasterxml.jackson.core.d dVar) {
        this._serializationConfig = this._serializationConfig.withDefaultPrettyPrinter(dVar);
        return this;
    }

    public ObjectMapper setDefaultTyping(vd4<?> vd4Var) {
        this._deserializationConfig = this._deserializationConfig.with(vd4Var);
        this._serializationConfig = this._serializationConfig.with(vd4Var);
        return this;
    }

    public ObjectMapper setFilterProvider(j41 j41Var) {
        this._serializationConfig = this._serializationConfig.withFilters(j41Var);
        return this;
    }

    @Deprecated
    public void setFilters(j41 j41Var) {
        this._serializationConfig = this._serializationConfig.withFilters(j41Var);
    }

    public Object setHandlerInstantiator(oj1 oj1Var) {
        this._deserializationConfig = this._deserializationConfig.with(oj1Var);
        this._serializationConfig = this._serializationConfig.with(oj1Var);
        return this;
    }

    public ObjectMapper setInjectableValues(sq1 sq1Var) {
        return this;
    }

    public ObjectMapper setLocale(Locale locale) {
        this._deserializationConfig = this._deserializationConfig.with(locale);
        this._serializationConfig = this._serializationConfig.with(locale);
        return this;
    }

    @Deprecated
    public void setMixInAnnotations(Map<Class<?>, Class<?>> map) {
        setMixIns(map);
    }

    public ObjectMapper setMixInResolver(c.a aVar) {
        SimpleMixInResolver withOverrides = this._mixIns.withOverrides(aVar);
        if (withOverrides != this._mixIns) {
            this._mixIns = withOverrides;
            this._deserializationConfig = new DeserializationConfig(this._deserializationConfig, withOverrides);
            this._serializationConfig = new SerializationConfig(this._serializationConfig, withOverrides);
        }
        return this;
    }

    public ObjectMapper setMixIns(Map<Class<?>, Class<?>> map) {
        this._mixIns.setLocalDefinitions(map);
        return this;
    }

    public ObjectMapper setNodeFactory(JsonNodeFactory jsonNodeFactory) {
        this._deserializationConfig = this._deserializationConfig.with(jsonNodeFactory);
        return this;
    }

    public ObjectMapper setPropertyInclusion(JsonInclude.Value value) {
        this._serializationConfig = this._serializationConfig.withPropertyInclusion(value);
        return this;
    }

    public ObjectMapper setPropertyNamingStrategy(PropertyNamingStrategy propertyNamingStrategy) {
        this._serializationConfig = this._serializationConfig.with(propertyNamingStrategy);
        this._deserializationConfig = this._deserializationConfig.with(propertyNamingStrategy);
        return this;
    }

    public ObjectMapper setSerializationInclusion(JsonInclude.Include include) {
        setPropertyInclusion(JsonInclude.Value.construct(include, JsonInclude.Include.USE_DEFAULTS));
        return this;
    }

    public ObjectMapper setSerializerFactory(com.fasterxml.jackson.databind.ser.g gVar) {
        this._serializerFactory = gVar;
        return this;
    }

    public ObjectMapper setSerializerProvider(DefaultSerializerProvider defaultSerializerProvider) {
        this._serializerProvider = defaultSerializerProvider;
        return this;
    }

    public ObjectMapper setSubtypeResolver(aw3 aw3Var) {
        this._subtypeResolver = aw3Var;
        this._deserializationConfig = this._deserializationConfig.with(aw3Var);
        this._serializationConfig = this._serializationConfig.with(aw3Var);
        return this;
    }

    public ObjectMapper setTimeZone(TimeZone timeZone) {
        this._deserializationConfig = this._deserializationConfig.with(timeZone);
        this._serializationConfig = this._serializationConfig.with(timeZone);
        return this;
    }

    public ObjectMapper setTypeFactory(TypeFactory typeFactory) {
        this._typeFactory = typeFactory;
        this._deserializationConfig = this._deserializationConfig.with(typeFactory);
        this._serializationConfig = this._serializationConfig.with(typeFactory);
        return this;
    }

    public ObjectMapper setVisibility(VisibilityChecker<?> visibilityChecker) {
        this._deserializationConfig = this._deserializationConfig.with(visibilityChecker);
        this._serializationConfig = this._serializationConfig.with(visibilityChecker);
        return this;
    }

    @Deprecated
    public void setVisibilityChecker(VisibilityChecker<?> visibilityChecker) {
        setVisibility(visibilityChecker);
    }

    @Override // com.fasterxml.jackson.core.c, com.fasterxml.jackson.core.e
    public JsonParser treeAsTokens(com.fasterxml.jackson.core.f fVar) {
        return new q((d) fVar, this);
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fasterxml.jackson.core.c
    public <T> T treeToValue(com.fasterxml.jackson.core.f fVar, Class<T> cls) throws JsonProcessingException {
        T t;
        if (cls != Object.class) {
            try {
                if (cls.isAssignableFrom(fVar.getClass())) {
                    return fVar;
                }
            } catch (JsonProcessingException e) {
                throw e;
            } catch (IOException e2) {
                throw new IllegalArgumentException(e2.getMessage(), e2);
            }
        }
        return (fVar.i() == JsonToken.VALUE_EMBEDDED_OBJECT && (fVar instanceof n) && ((t = (T) ((n) fVar).L()) == null || cls.isInstance(t))) ? t : (T) readValue(treeAsTokens(fVar), cls);
    }

    public <T extends d> T valueToTree(Object obj) throws IllegalArgumentException {
        if (obj == null) {
            return null;
        }
        com.fasterxml.jackson.databind.util.e eVar = new com.fasterxml.jackson.databind.util.e((com.fasterxml.jackson.core.c) this, false);
        if (isEnabled(DeserializationFeature.USE_BIG_DECIMAL_FOR_FLOATS)) {
            eVar = eVar.R1(true);
        }
        try {
            writeValue(eVar, obj);
            JsonParser K1 = eVar.K1();
            T t = (T) readTree(K1);
            K1.close();
            return t;
        } catch (IOException e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        }
    }

    @Override // com.fasterxml.jackson.core.c
    public Version version() {
        return wo2.a;
    }

    @Override // com.fasterxml.jackson.core.c, com.fasterxml.jackson.core.e
    public void writeTree(JsonGenerator jsonGenerator, com.fasterxml.jackson.core.f fVar) throws IOException, JsonProcessingException {
        SerializationConfig serializationConfig = getSerializationConfig();
        _serializerProvider(serializationConfig).serializeValue(jsonGenerator, fVar);
        if (serializationConfig.isEnabled(SerializationFeature.FLUSH_AFTER_WRITE_VALUE)) {
            jsonGenerator.flush();
        }
    }

    @Override // com.fasterxml.jackson.core.c
    public void writeValue(JsonGenerator jsonGenerator, Object obj) throws IOException, JsonGenerationException, JsonMappingException {
        SerializationConfig serializationConfig = getSerializationConfig();
        if (serializationConfig.isEnabled(SerializationFeature.INDENT_OUTPUT) && jsonGenerator.m() == null) {
            jsonGenerator.x(serializationConfig.constructDefaultPrettyPrinter());
        }
        if (serializationConfig.isEnabled(SerializationFeature.CLOSE_CLOSEABLE) && (obj instanceof Closeable)) {
            b(jsonGenerator, obj, serializationConfig);
            return;
        }
        _serializerProvider(serializationConfig).serializeValue(jsonGenerator, obj);
        if (serializationConfig.isEnabled(SerializationFeature.FLUSH_AFTER_WRITE_VALUE)) {
            jsonGenerator.flush();
        }
    }

    public byte[] writeValueAsBytes(Object obj) throws JsonProcessingException {
        ms msVar = new ms(this._jsonFactory._getBufferRecycler());
        try {
            _configAndWriteValue(this._jsonFactory.createGenerator(msVar, JsonEncoding.UTF8), obj);
            byte[] n = msVar.n();
            msVar.i();
            return n;
        } catch (JsonProcessingException e) {
            throw e;
        } catch (IOException e2) {
            throw JsonMappingException.fromUnexpectedIOE(e2);
        }
    }

    public String writeValueAsString(Object obj) throws JsonProcessingException {
        com.fasterxml.jackson.core.io.e eVar = new com.fasterxml.jackson.core.io.e(this._jsonFactory._getBufferRecycler());
        try {
            _configAndWriteValue(this._jsonFactory.createGenerator(eVar), obj);
            return eVar.a();
        } catch (JsonProcessingException e) {
            throw e;
        } catch (IOException e2) {
            throw JsonMappingException.fromUnexpectedIOE(e2);
        }
    }

    public ObjectWriter writer() {
        return _newWriter(getSerializationConfig());
    }

    public ObjectWriter writerFor(Class<?> cls) {
        return _newWriter(getSerializationConfig(), cls == null ? null : this._typeFactory.constructType(cls), null);
    }

    public ObjectWriter writerWithDefaultPrettyPrinter() {
        SerializationConfig serializationConfig = getSerializationConfig();
        return _newWriter(serializationConfig, null, serializationConfig.getDefaultPrettyPrinter());
    }

    @Deprecated
    public ObjectWriter writerWithType(Class<?> cls) {
        return _newWriter(getSerializationConfig(), cls == null ? null : this._typeFactory.constructType(cls), null);
    }

    public ObjectWriter writerWithView(Class<?> cls) {
        return _newWriter(getSerializationConfig().withView(cls));
    }

    public ObjectMapper(JsonFactory jsonFactory) {
        this(jsonFactory, null, null);
    }

    public static List<i> findModules(ClassLoader classLoader) {
        ArrayList arrayList = new ArrayList();
        Iterator it = c(i.class, classLoader).iterator();
        while (it.hasNext()) {
            arrayList.add((i) it.next());
        }
        return arrayList;
    }

    public ObjectReader _newReader(DeserializationConfig deserializationConfig, JavaType javaType, Object obj, z81 z81Var, sq1 sq1Var) {
        return new ObjectReader(this, deserializationConfig, javaType, obj, z81Var, sq1Var);
    }

    public ObjectWriter _newWriter(SerializationConfig serializationConfig, z81 z81Var) {
        return new ObjectWriter(this, serializationConfig, z81Var);
    }

    public void acceptJsonFormatVisitor(JavaType javaType, com.fasterxml.jackson.databind.jsonFormatVisitors.b bVar) throws JsonMappingException {
        if (javaType != null) {
            _serializerProvider(getSerializationConfig()).acceptJsonFormatVisitor(javaType, bVar);
            return;
        }
        throw new IllegalArgumentException("type must be provided");
    }

    public boolean canSerialize(Class<?> cls, AtomicReference<Throwable> atomicReference) {
        return _serializerProvider(getSerializationConfig()).hasSerializerFor(cls, atomicReference);
    }

    public <T> T convertValue(Object obj, ud4<?> ud4Var) throws IllegalArgumentException {
        return (T) convertValue(obj, this._typeFactory.constructType(ud4Var));
    }

    @Override // com.fasterxml.jackson.core.c, com.fasterxml.jackson.core.e
    public com.fasterxml.jackson.databind.node.a createArrayNode() {
        return this._deserializationConfig.getNodeFactory().arrayNode();
    }

    @Override // com.fasterxml.jackson.core.c, com.fasterxml.jackson.core.e
    public m createObjectNode() {
        return this._deserializationConfig.getNodeFactory().objectNode();
    }

    public ObjectMapper enableDefaultTyping(DefaultTyping defaultTyping) {
        return enableDefaultTyping(defaultTyping, JsonTypeInfo.As.WRAPPER_ARRAY);
    }

    public boolean isEnabled(SerializationFeature serializationFeature) {
        return this._serializationConfig.isEnabled(serializationFeature);
    }

    @Override // com.fasterxml.jackson.core.c
    public <T> T readValue(JsonParser jsonParser, ud4<?> ud4Var) throws IOException, JsonParseException, JsonMappingException {
        return (T) _readValue(getDeserializationConfig(), jsonParser, this._typeFactory.constructType(ud4Var));
    }

    @Override // com.fasterxml.jackson.core.c
    public /* bridge */ /* synthetic */ Iterator readValues(JsonParser jsonParser, ud4 ud4Var) throws IOException {
        return readValues(jsonParser, (ud4<?>) ud4Var);
    }

    public ObjectReader reader(DeserializationFeature deserializationFeature) {
        return _newReader(getDeserializationConfig().with(deserializationFeature));
    }

    public ObjectReader readerFor(Class<?> cls) {
        return _newReader(getDeserializationConfig(), this._typeFactory.constructType(cls), null, null, this._injectableValues);
    }

    public void registerSubtypes(NamedType... namedTypeArr) {
        getSubtypeResolver().registerSubtypes(namedTypeArr);
    }

    public ObjectMapper setConfig(SerializationConfig serializationConfig) {
        this._serializationConfig = serializationConfig;
        return this;
    }

    public ObjectWriter writer(SerializationFeature serializationFeature) {
        return _newWriter(getSerializationConfig().with(serializationFeature));
    }

    public ObjectMapper(ObjectMapper objectMapper) {
        this._rootDeserializers = new ConcurrentHashMap<>(64, 0.6f, 2);
        JsonFactory copy = objectMapper._jsonFactory.copy();
        this._jsonFactory = copy;
        copy.setCodec(this);
        this._subtypeResolver = objectMapper._subtypeResolver;
        this._typeFactory = objectMapper._typeFactory;
        this._propertyOverrides = objectMapper._propertyOverrides.copy();
        this._mixIns = objectMapper._mixIns.copy();
        RootNameLookup rootNameLookup = new RootNameLookup();
        this._serializationConfig = new SerializationConfig(objectMapper._serializationConfig, this._mixIns, rootNameLookup, this._propertyOverrides);
        this._deserializationConfig = new DeserializationConfig(objectMapper._deserializationConfig, this._mixIns, rootNameLookup, this._propertyOverrides);
        this._serializerProvider = objectMapper._serializerProvider.copy();
        this._deserializationContext = objectMapper._deserializationContext.copy();
        this._serializerFactory = objectMapper._serializerFactory;
        Set<Object> set = objectMapper._registeredModuleTypes;
        if (set == null) {
            this._registeredModuleTypes = null;
        } else {
            this._registeredModuleTypes = new LinkedHashSet(set);
        }
    }

    public ObjectWriter _newWriter(SerializationConfig serializationConfig, JavaType javaType, com.fasterxml.jackson.core.d dVar) {
        return new ObjectWriter(this, serializationConfig, javaType, dVar);
    }

    public <T> T convertValue(Object obj, JavaType javaType) throws IllegalArgumentException {
        if (obj == null) {
            return null;
        }
        return (T) _convert(obj, javaType);
    }

    public ObjectMapper disable(SerializationFeature serializationFeature) {
        this._serializationConfig = this._serializationConfig.without(serializationFeature);
        return this;
    }

    public ObjectMapper enable(SerializationFeature serializationFeature) {
        this._serializationConfig = this._serializationConfig.with(serializationFeature);
        return this;
    }

    public ObjectMapper enableDefaultTyping(DefaultTyping defaultTyping, JsonTypeInfo.As as) {
        if (as != JsonTypeInfo.As.EXTERNAL_PROPERTY) {
            return setDefaultTyping(new DefaultTypeResolverBuilder(defaultTyping).init(JsonTypeInfo.Id.CLASS, (com.fasterxml.jackson.databind.jsontype.b) null).inclusion(as));
        }
        throw new IllegalArgumentException("Can not use includeAs of " + as);
    }

    public boolean isEnabled(DeserializationFeature deserializationFeature) {
        return this._deserializationConfig.isEnabled(deserializationFeature);
    }

    @Override // com.fasterxml.jackson.core.c
    public final <T> T readValue(JsonParser jsonParser, r73 r73Var) throws IOException, JsonParseException, JsonMappingException {
        return (T) _readValue(getDeserializationConfig(), jsonParser, (JavaType) r73Var);
    }

    public ObjectReader reader(DeserializationFeature deserializationFeature, DeserializationFeature... deserializationFeatureArr) {
        return _newReader(getDeserializationConfig().with(deserializationFeature, deserializationFeatureArr));
    }

    public ObjectReader readerFor(ud4<?> ud4Var) {
        return _newReader(getDeserializationConfig(), this._typeFactory.constructType(ud4Var), null, null, this._injectableValues);
    }

    public ObjectMapper registerModules(Iterable<i> iterable) {
        for (i iVar : iterable) {
            registerModule(iVar);
        }
        return this;
    }

    public ObjectMapper setVisibility(PropertyAccessor propertyAccessor, JsonAutoDetect.Visibility visibility) {
        this._deserializationConfig = this._deserializationConfig.withVisibility(propertyAccessor, visibility);
        this._serializationConfig = this._serializationConfig.withVisibility(propertyAccessor, visibility);
        return this;
    }

    public ObjectWriter writer(SerializationFeature serializationFeature, SerializationFeature... serializationFeatureArr) {
        return _newWriter(getSerializationConfig().with(serializationFeature, serializationFeatureArr));
    }

    public boolean canDeserialize(JavaType javaType, AtomicReference<Throwable> atomicReference) {
        return createDeserializationContext(null, getDeserializationConfig()).hasValueDeserializerFor(javaType, atomicReference);
    }

    public ObjectMapper disable(SerializationFeature serializationFeature, SerializationFeature... serializationFeatureArr) {
        this._serializationConfig = this._serializationConfig.without(serializationFeature, serializationFeatureArr);
        return this;
    }

    public ObjectMapper enable(SerializationFeature serializationFeature, SerializationFeature... serializationFeatureArr) {
        this._serializationConfig = this._serializationConfig.with(serializationFeature, serializationFeatureArr);
        return this;
    }

    public boolean isEnabled(JsonParser.Feature feature) {
        return this._deserializationConfig.isEnabled(feature, this._jsonFactory);
    }

    public <T> T readValue(JsonParser jsonParser, JavaType javaType) throws IOException, JsonParseException, JsonMappingException {
        return (T) _readValue(getDeserializationConfig(), jsonParser, javaType);
    }

    @Override // com.fasterxml.jackson.core.c
    public <T> h<T> readValues(JsonParser jsonParser, r73 r73Var) throws IOException, JsonProcessingException {
        return readValues(jsonParser, (JavaType) r73Var);
    }

    public ObjectReader reader(JsonNodeFactory jsonNodeFactory) {
        return _newReader(getDeserializationConfig()).with(jsonNodeFactory);
    }

    public ObjectWriter writer(DateFormat dateFormat) {
        return _newWriter(getSerializationConfig().with(dateFormat));
    }

    public ObjectWriter writerFor(ud4<?> ud4Var) {
        return _newWriter(getSerializationConfig(), ud4Var == null ? null : this._typeFactory.constructType(ud4Var), null);
    }

    @Deprecated
    public ObjectWriter writerWithType(ud4<?> ud4Var) {
        return _newWriter(getSerializationConfig(), ud4Var == null ? null : this._typeFactory.constructType(ud4Var), null);
    }

    public ObjectMapper configure(SerializationFeature serializationFeature, boolean z) {
        this._serializationConfig = z ? this._serializationConfig.with(serializationFeature) : this._serializationConfig.without(serializationFeature);
        return this;
    }

    public ObjectMapper disable(DeserializationFeature deserializationFeature) {
        this._deserializationConfig = this._deserializationConfig.without(deserializationFeature);
        return this;
    }

    public ObjectMapper enable(DeserializationFeature deserializationFeature) {
        this._deserializationConfig = this._deserializationConfig.with(deserializationFeature);
        return this;
    }

    public boolean isEnabled(JsonGenerator.Feature feature) {
        return this._serializationConfig.isEnabled(feature, this._jsonFactory);
    }

    public <T> T readValue(File file, Class<T> cls) throws IOException, JsonParseException, JsonMappingException {
        return (T) _readMapAndClose(this._jsonFactory.createParser(file), this._typeFactory.constructType(cls));
    }

    public <T> h<T> readValues(JsonParser jsonParser, JavaType javaType) throws IOException, JsonProcessingException {
        DefaultDeserializationContext createDeserializationContext = createDeserializationContext(jsonParser, getDeserializationConfig());
        return new h<>(javaType, jsonParser, createDeserializationContext, _findRootDeserializer(createDeserializationContext, javaType), false, null);
    }

    public ObjectReader reader(z81 z81Var) {
        _verifySchemaType(z81Var);
        return _newReader(getDeserializationConfig(), null, null, z81Var, this._injectableValues);
    }

    public void writeTree(JsonGenerator jsonGenerator, d dVar) throws IOException, JsonProcessingException {
        SerializationConfig serializationConfig = getSerializationConfig();
        _serializerProvider(serializationConfig).serializeValue(jsonGenerator, dVar);
        if (serializationConfig.isEnabled(SerializationFeature.FLUSH_AFTER_WRITE_VALUE)) {
            jsonGenerator.flush();
        }
    }

    public ObjectWriter writer(com.fasterxml.jackson.core.d dVar) {
        if (dVar == null) {
            dVar = ObjectWriter.NULL_PRETTY_PRINTER;
        }
        return _newWriter(getSerializationConfig(), null, dVar);
    }

    public ObjectMapper disable(DeserializationFeature deserializationFeature, DeserializationFeature... deserializationFeatureArr) {
        this._deserializationConfig = this._deserializationConfig.without(deserializationFeature, deserializationFeatureArr);
        return this;
    }

    public ObjectMapper enable(DeserializationFeature deserializationFeature, DeserializationFeature... deserializationFeatureArr) {
        this._deserializationConfig = this._deserializationConfig.with(deserializationFeature, deserializationFeatureArr);
        return this;
    }

    public boolean isEnabled(JsonFactory.Feature feature) {
        return this._jsonFactory.isEnabled(feature);
    }

    public d readTree(InputStream inputStream) throws IOException, JsonProcessingException {
        d dVar = (d) _readMapAndClose(this._jsonFactory.createParser(inputStream), a);
        return dVar == null ? l.a : dVar;
    }

    public <T> T readValue(File file, ud4 ud4Var) throws IOException, JsonParseException, JsonMappingException {
        return (T) _readMapAndClose(this._jsonFactory.createParser(file), this._typeFactory.constructType(ud4Var));
    }

    public ObjectMapper configure(DeserializationFeature deserializationFeature, boolean z) {
        this._deserializationConfig = z ? this._deserializationConfig.with(deserializationFeature) : this._deserializationConfig.without(deserializationFeature);
        return this;
    }

    public ObjectMapper disable(JsonParser.Feature... featureArr) {
        for (JsonParser.Feature feature : featureArr) {
            this._jsonFactory.disable(feature);
        }
        return this;
    }

    public ObjectMapper enable(JsonParser.Feature... featureArr) {
        for (JsonParser.Feature feature : featureArr) {
            this._jsonFactory.enable(feature);
        }
        return this;
    }

    public <T> T readValue(File file, JavaType javaType) throws IOException, JsonParseException, JsonMappingException {
        return (T) _readMapAndClose(this._jsonFactory.createParser(file), javaType);
    }

    public ObjectReader reader(sq1 sq1Var) {
        return _newReader(getDeserializationConfig(), null, null, null, sq1Var);
    }

    public ObjectWriter writer(j41 j41Var) {
        return _newWriter(getSerializationConfig().withFilters(j41Var));
    }

    public ObjectWriter writerFor(JavaType javaType) {
        return _newWriter(getSerializationConfig(), javaType, null);
    }

    @Deprecated
    public ObjectWriter writerWithType(JavaType javaType) {
        return _newWriter(getSerializationConfig(), javaType, null);
    }

    public d readTree(Reader reader) throws IOException, JsonProcessingException {
        d dVar = (d) _readMapAndClose(this._jsonFactory.createParser(reader), a);
        return dVar == null ? l.a : dVar;
    }

    public <T> T readValue(URL url, Class<T> cls) throws IOException, JsonParseException, JsonMappingException {
        return (T) _readMapAndClose(this._jsonFactory.createParser(url), this._typeFactory.constructType(cls));
    }

    public ObjectReader reader(Base64Variant base64Variant) {
        return _newReader(getDeserializationConfig().with(base64Variant));
    }

    public ObjectWriter writer(z81 z81Var) {
        _verifySchemaType(z81Var);
        return _newWriter(getSerializationConfig(), z81Var);
    }

    public ObjectMapper configure(JsonParser.Feature feature, boolean z) {
        this._jsonFactory.configure(feature, z);
        return this;
    }

    public ObjectMapper disable(JsonGenerator.Feature... featureArr) {
        for (JsonGenerator.Feature feature : featureArr) {
            this._jsonFactory.disable(feature);
        }
        return this;
    }

    public ObjectMapper enable(JsonGenerator.Feature... featureArr) {
        for (JsonGenerator.Feature feature : featureArr) {
            this._jsonFactory.enable(feature);
        }
        return this;
    }

    public <T> T readValue(URL url, ud4 ud4Var) throws IOException, JsonParseException, JsonMappingException {
        return (T) _readMapAndClose(this._jsonFactory.createParser(url), this._typeFactory.constructType(ud4Var));
    }

    @Override // com.fasterxml.jackson.core.c
    public <T> h<T> readValues(JsonParser jsonParser, Class<T> cls) throws IOException, JsonProcessingException {
        return readValues(jsonParser, this._typeFactory.constructType(cls));
    }

    public ObjectReader reader(ContextAttributes contextAttributes) {
        return _newReader(getDeserializationConfig().with(contextAttributes));
    }

    public ObjectMapper configure(JsonGenerator.Feature feature, boolean z) {
        this._jsonFactory.configure(feature, z);
        return this;
    }

    public d readTree(String str) throws IOException, JsonProcessingException {
        d dVar = (d) _readMapAndClose(this._jsonFactory.createParser(str), a);
        return dVar == null ? l.a : dVar;
    }

    public <T> T readValue(URL url, JavaType javaType) throws IOException, JsonParseException, JsonMappingException {
        return (T) _readMapAndClose(this._jsonFactory.createParser(url), javaType);
    }

    @Override // com.fasterxml.jackson.core.c
    public <T> h<T> readValues(JsonParser jsonParser, ud4<?> ud4Var) throws IOException, JsonProcessingException {
        return readValues(jsonParser, this._typeFactory.constructType(ud4Var));
    }

    @Deprecated
    public ObjectReader reader(JavaType javaType) {
        return _newReader(getDeserializationConfig(), javaType, null, null, this._injectableValues);
    }

    public void writeValue(File file, Object obj) throws IOException, JsonGenerationException, JsonMappingException {
        _configAndWriteValue(this._jsonFactory.createGenerator(file, JsonEncoding.UTF8), obj);
    }

    public ObjectWriter writer(Base64Variant base64Variant) {
        return _newWriter(getSerializationConfig().with(base64Variant));
    }

    public <T> T readValue(String str, Class<T> cls) throws IOException, JsonParseException, JsonMappingException {
        return (T) _readMapAndClose(this._jsonFactory.createParser(str), this._typeFactory.constructType(cls));
    }

    @Deprecated
    public ObjectReader reader(Class<?> cls) {
        return _newReader(getDeserializationConfig(), this._typeFactory.constructType(cls), null, null, this._injectableValues);
    }

    public void writeValue(OutputStream outputStream, Object obj) throws IOException, JsonGenerationException, JsonMappingException {
        _configAndWriteValue(this._jsonFactory.createGenerator(outputStream, JsonEncoding.UTF8), obj);
    }

    public ObjectWriter writer(CharacterEscapes characterEscapes) {
        return _newWriter(getSerializationConfig()).with(characterEscapes);
    }

    public d readTree(byte[] bArr) throws IOException, JsonProcessingException {
        d dVar = (d) _readMapAndClose(this._jsonFactory.createParser(bArr), a);
        return dVar == null ? l.a : dVar;
    }

    public <T> T readValue(String str, ud4 ud4Var) throws IOException, JsonParseException, JsonMappingException {
        return (T) _readMapAndClose(this._jsonFactory.createParser(str), this._typeFactory.constructType(ud4Var));
    }

    @Deprecated
    public ObjectReader reader(ud4<?> ud4Var) {
        return _newReader(getDeserializationConfig(), this._typeFactory.constructType(ud4Var), null, null, this._injectableValues);
    }

    public void writeValue(DataOutput dataOutput, Object obj) throws IOException {
        _configAndWriteValue(this._jsonFactory.createGenerator(dataOutput, JsonEncoding.UTF8), obj);
    }

    public ObjectWriter writer(ContextAttributes contextAttributes) {
        return _newWriter(getSerializationConfig().with(contextAttributes));
    }

    public <T> T readValue(String str, JavaType javaType) throws IOException, JsonParseException, JsonMappingException {
        return (T) _readMapAndClose(this._jsonFactory.createParser(str), javaType);
    }

    public void writeValue(Writer writer, Object obj) throws IOException, JsonGenerationException, JsonMappingException {
        _configAndWriteValue(this._jsonFactory.createGenerator(writer), obj);
    }

    public d readTree(File file) throws IOException, JsonProcessingException {
        d dVar = (d) _readMapAndClose(this._jsonFactory.createParser(file), a);
        return dVar == null ? l.a : dVar;
    }

    public <T> T readValue(Reader reader, Class<T> cls) throws IOException, JsonParseException, JsonMappingException {
        return (T) _readMapAndClose(this._jsonFactory.createParser(reader), this._typeFactory.constructType(cls));
    }

    public <T> T readValue(Reader reader, ud4 ud4Var) throws IOException, JsonParseException, JsonMappingException {
        return (T) _readMapAndClose(this._jsonFactory.createParser(reader), this._typeFactory.constructType(ud4Var));
    }

    public d readTree(URL url) throws IOException, JsonProcessingException {
        d dVar = (d) _readMapAndClose(this._jsonFactory.createParser(url), a);
        return dVar == null ? l.a : dVar;
    }

    public <T> T readValue(Reader reader, JavaType javaType) throws IOException, JsonParseException, JsonMappingException {
        return (T) _readMapAndClose(this._jsonFactory.createParser(reader), javaType);
    }

    public <T> T readValue(InputStream inputStream, Class<T> cls) throws IOException, JsonParseException, JsonMappingException {
        return (T) _readMapAndClose(this._jsonFactory.createParser(inputStream), this._typeFactory.constructType(cls));
    }

    public <T> T readValue(InputStream inputStream, ud4 ud4Var) throws IOException, JsonParseException, JsonMappingException {
        return (T) _readMapAndClose(this._jsonFactory.createParser(inputStream), this._typeFactory.constructType(ud4Var));
    }

    public <T> T readValue(InputStream inputStream, JavaType javaType) throws IOException, JsonParseException, JsonMappingException {
        return (T) _readMapAndClose(this._jsonFactory.createParser(inputStream), javaType);
    }

    public ObjectMapper(JsonFactory jsonFactory, DefaultSerializerProvider defaultSerializerProvider, DefaultDeserializationContext defaultDeserializationContext) {
        this._rootDeserializers = new ConcurrentHashMap<>(64, 0.6f, 2);
        if (jsonFactory == null) {
            this._jsonFactory = new MappingJsonFactory(this);
        } else {
            this._jsonFactory = jsonFactory;
            if (jsonFactory.getCodec() == null) {
                jsonFactory.setCodec(this);
            }
        }
        this._subtypeResolver = new StdSubtypeResolver();
        RootNameLookup rootNameLookup = new RootNameLookup();
        this._typeFactory = TypeFactory.defaultInstance();
        SimpleMixInResolver simpleMixInResolver = new SimpleMixInResolver(null);
        this._mixIns = simpleMixInResolver;
        BaseSettings withClassIntrospector = DEFAULT_BASE.withClassIntrospector(defaultClassIntrospector());
        ConfigOverrides configOverrides = new ConfigOverrides();
        this._propertyOverrides = configOverrides;
        this._serializationConfig = new SerializationConfig(withClassIntrospector, this._subtypeResolver, simpleMixInResolver, rootNameLookup, configOverrides);
        this._deserializationConfig = new DeserializationConfig(withClassIntrospector, this._subtypeResolver, simpleMixInResolver, rootNameLookup, configOverrides);
        boolean requiresPropertyOrdering = this._jsonFactory.requiresPropertyOrdering();
        SerializationConfig serializationConfig = this._serializationConfig;
        MapperFeature mapperFeature = MapperFeature.SORT_PROPERTIES_ALPHABETICALLY;
        if (serializationConfig.isEnabled(mapperFeature) ^ requiresPropertyOrdering) {
            configure(mapperFeature, requiresPropertyOrdering);
        }
        this._serializerProvider = defaultSerializerProvider == null ? new DefaultSerializerProvider.Impl() : defaultSerializerProvider;
        this._deserializationContext = defaultDeserializationContext == null ? new DefaultDeserializationContext.Impl(BeanDeserializerFactory.instance) : defaultDeserializationContext;
        this._serializerFactory = BeanSerializerFactory.instance;
    }

    public <T> T readValue(byte[] bArr, Class<T> cls) throws IOException, JsonParseException, JsonMappingException {
        return (T) _readMapAndClose(this._jsonFactory.createParser(bArr), this._typeFactory.constructType(cls));
    }

    public <T> T readValue(byte[] bArr, int i, int i2, Class<T> cls) throws IOException, JsonParseException, JsonMappingException {
        return (T) _readMapAndClose(this._jsonFactory.createParser(bArr, i, i2), this._typeFactory.constructType(cls));
    }

    public <T> T readValue(byte[] bArr, ud4 ud4Var) throws IOException, JsonParseException, JsonMappingException {
        return (T) _readMapAndClose(this._jsonFactory.createParser(bArr), this._typeFactory.constructType(ud4Var));
    }

    public <T> T readValue(byte[] bArr, int i, int i2, ud4 ud4Var) throws IOException, JsonParseException, JsonMappingException {
        return (T) _readMapAndClose(this._jsonFactory.createParser(bArr, i, i2), this._typeFactory.constructType(ud4Var));
    }

    public <T> T readValue(byte[] bArr, JavaType javaType) throws IOException, JsonParseException, JsonMappingException {
        return (T) _readMapAndClose(this._jsonFactory.createParser(bArr), javaType);
    }

    public <T> T readValue(byte[] bArr, int i, int i2, JavaType javaType) throws IOException, JsonParseException, JsonMappingException {
        return (T) _readMapAndClose(this._jsonFactory.createParser(bArr, i, i2), javaType);
    }

    public <T> T readValue(DataInput dataInput, Class<T> cls) throws IOException {
        return (T) _readMapAndClose(this._jsonFactory.createParser(dataInput), this._typeFactory.constructType(cls));
    }

    public <T> T readValue(DataInput dataInput, JavaType javaType) throws IOException {
        return (T) _readMapAndClose(this._jsonFactory.createParser(dataInput), javaType);
    }
}
