package com.fasterxml.jackson.databind.cfg;

import com.fasterxml.jackson.databind.deser.f;
import com.fasterxml.jackson.databind.deser.g;
import com.fasterxml.jackson.databind.deser.std.StdKeyDeserializers;
import java.io.Serializable;

/* loaded from: classes.dex */
public class DeserializerFactoryConfig implements Serializable {
    private static final long serialVersionUID = 1;
    public final o5[] _abstractTypeResolvers;
    public final f[] _additionalDeserializers;
    public final g[] _additionalKeyDeserializers;
    public final uo[] _modifiers;
    public final ah4[] _valueInstantiators;
    public static final f[] NO_DESERIALIZERS = new f[0];
    public static final uo[] NO_MODIFIERS = new uo[0];
    public static final o5[] NO_ABSTRACT_TYPE_RESOLVERS = new o5[0];
    public static final ah4[] NO_VALUE_INSTANTIATORS = new ah4[0];
    public static final g[] DEFAULT_KEY_DESERIALIZERS = {new StdKeyDeserializers()};

    public DeserializerFactoryConfig() {
        this(null, null, null, null, null);
    }

    public Iterable<o5> abstractTypeResolvers() {
        return new ph(this._abstractTypeResolvers);
    }

    public Iterable<uo> deserializerModifiers() {
        return new ph(this._modifiers);
    }

    public Iterable<f> deserializers() {
        return new ph(this._additionalDeserializers);
    }

    public boolean hasAbstractTypeResolvers() {
        return this._abstractTypeResolvers.length > 0;
    }

    public boolean hasDeserializerModifiers() {
        return this._modifiers.length > 0;
    }

    public boolean hasDeserializers() {
        return this._additionalDeserializers.length > 0;
    }

    public boolean hasKeyDeserializers() {
        return this._additionalKeyDeserializers.length > 0;
    }

    public boolean hasValueInstantiators() {
        return this._valueInstantiators.length > 0;
    }

    public Iterable<g> keyDeserializers() {
        return new ph(this._additionalKeyDeserializers);
    }

    public Iterable<ah4> valueInstantiators() {
        return new ph(this._valueInstantiators);
    }

    public DeserializerFactoryConfig withAbstractTypeResolver(o5 o5Var) {
        if (o5Var != null) {
            return new DeserializerFactoryConfig(this._additionalDeserializers, this._additionalKeyDeserializers, this._modifiers, (o5[]) lh.j(this._abstractTypeResolvers, o5Var), this._valueInstantiators);
        }
        throw new IllegalArgumentException("Can not pass null resolver");
    }

    public DeserializerFactoryConfig withAdditionalDeserializers(f fVar) {
        if (fVar != null) {
            return new DeserializerFactoryConfig((f[]) lh.j(this._additionalDeserializers, fVar), this._additionalKeyDeserializers, this._modifiers, this._abstractTypeResolvers, this._valueInstantiators);
        }
        throw new IllegalArgumentException("Can not pass null Deserializers");
    }

    public DeserializerFactoryConfig withAdditionalKeyDeserializers(g gVar) {
        if (gVar != null) {
            return new DeserializerFactoryConfig(this._additionalDeserializers, (g[]) lh.j(this._additionalKeyDeserializers, gVar), this._modifiers, this._abstractTypeResolvers, this._valueInstantiators);
        }
        throw new IllegalArgumentException("Can not pass null KeyDeserializers");
    }

    public DeserializerFactoryConfig withDeserializerModifier(uo uoVar) {
        if (uoVar != null) {
            return new DeserializerFactoryConfig(this._additionalDeserializers, this._additionalKeyDeserializers, (uo[]) lh.j(this._modifiers, uoVar), this._abstractTypeResolvers, this._valueInstantiators);
        }
        throw new IllegalArgumentException("Can not pass null modifier");
    }

    public DeserializerFactoryConfig withValueInstantiators(ah4 ah4Var) {
        if (ah4Var != null) {
            return new DeserializerFactoryConfig(this._additionalDeserializers, this._additionalKeyDeserializers, this._modifiers, this._abstractTypeResolvers, (ah4[]) lh.j(this._valueInstantiators, ah4Var));
        }
        throw new IllegalArgumentException("Can not pass null resolver");
    }

    public DeserializerFactoryConfig(f[] fVarArr, g[] gVarArr, uo[] uoVarArr, o5[] o5VarArr, ah4[] ah4VarArr) {
        this._additionalDeserializers = fVarArr == null ? NO_DESERIALIZERS : fVarArr;
        this._additionalKeyDeserializers = gVarArr == null ? DEFAULT_KEY_DESERIALIZERS : gVarArr;
        this._modifiers = uoVarArr == null ? NO_MODIFIERS : uoVarArr;
        this._abstractTypeResolvers = o5VarArr == null ? NO_ABSTRACT_TYPE_RESOLVERS : o5VarArr;
        this._valueInstantiators = ah4VarArr == null ? NO_VALUE_INSTANTIATORS : ah4VarArr;
    }
}
