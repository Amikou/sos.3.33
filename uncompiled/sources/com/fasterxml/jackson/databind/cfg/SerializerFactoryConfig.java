package com.fasterxml.jackson.databind.cfg;

import java.io.Serializable;

/* loaded from: classes.dex */
public final class SerializerFactoryConfig implements Serializable {
    private static final long serialVersionUID = 1;
    public final am3[] _additionalKeySerializers;
    public final am3[] _additionalSerializers;
    public final xo[] _modifiers;
    public static final am3[] NO_SERIALIZERS = new am3[0];
    public static final xo[] NO_MODIFIERS = new xo[0];

    public SerializerFactoryConfig() {
        this(null, null, null);
    }

    public boolean hasKeySerializers() {
        return this._additionalKeySerializers.length > 0;
    }

    public boolean hasSerializerModifiers() {
        return this._modifiers.length > 0;
    }

    public boolean hasSerializers() {
        return this._additionalSerializers.length > 0;
    }

    public Iterable<am3> keySerializers() {
        return new ph(this._additionalKeySerializers);
    }

    public Iterable<xo> serializerModifiers() {
        return new ph(this._modifiers);
    }

    public Iterable<am3> serializers() {
        return new ph(this._additionalSerializers);
    }

    public SerializerFactoryConfig withAdditionalKeySerializers(am3 am3Var) {
        if (am3Var != null) {
            return new SerializerFactoryConfig(this._additionalSerializers, (am3[]) lh.j(this._additionalKeySerializers, am3Var), this._modifiers);
        }
        throw new IllegalArgumentException("Can not pass null Serializers");
    }

    public SerializerFactoryConfig withAdditionalSerializers(am3 am3Var) {
        if (am3Var != null) {
            return new SerializerFactoryConfig((am3[]) lh.j(this._additionalSerializers, am3Var), this._additionalKeySerializers, this._modifiers);
        }
        throw new IllegalArgumentException("Can not pass null Serializers");
    }

    public SerializerFactoryConfig withSerializerModifier(xo xoVar) {
        if (xoVar != null) {
            return new SerializerFactoryConfig(this._additionalSerializers, this._additionalKeySerializers, (xo[]) lh.j(this._modifiers, xoVar));
        }
        throw new IllegalArgumentException("Can not pass null modifier");
    }

    public SerializerFactoryConfig(am3[] am3VarArr, am3[] am3VarArr2, xo[] xoVarArr) {
        this._additionalSerializers = am3VarArr == null ? NO_SERIALIZERS : am3VarArr;
        this._additionalKeySerializers = am3VarArr2 == null ? NO_SERIALIZERS : am3VarArr2;
        this._modifiers = xoVarArr == null ? NO_MODIFIERS : xoVarArr;
    }
}
