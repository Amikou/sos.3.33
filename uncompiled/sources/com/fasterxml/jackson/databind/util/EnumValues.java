package com.fasterxml.jackson.databind.util;

import com.fasterxml.jackson.core.SerializableString;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.EnumMap;
import java.util.LinkedHashMap;
import java.util.List;

/* loaded from: classes.dex */
public final class EnumValues implements Serializable {
    private static final long serialVersionUID = 1;
    private final Class<Enum<?>> _enumClass;
    private final yl3[] _textual;
    private final Enum<?>[] _values;
    public transient EnumMap<?, yl3> a;

    public EnumValues(Class<Enum<?>> cls, SerializableString[] serializableStringArr) {
        this._enumClass = cls;
        this._values = cls.getEnumConstants();
        this._textual = serializableStringArr;
    }

    public static EnumValues construct(SerializationConfig serializationConfig, Class<Enum<?>> cls) {
        if (serializationConfig.isEnabled(SerializationFeature.WRITE_ENUMS_USING_TO_STRING)) {
            return constructFromToString(serializationConfig, cls);
        }
        return constructFromName(serializationConfig, cls);
    }

    public static EnumValues constructFromName(MapperConfig<?> mapperConfig, Class<Enum<?>> cls) {
        Class<? extends Enum<?>> n = c.n(cls);
        Enum<?>[] enumArr = (Enum[]) n.getEnumConstants();
        if (enumArr != null) {
            String[] findEnumValues = mapperConfig.getAnnotationIntrospector().findEnumValues(n, enumArr, new String[enumArr.length]);
            yl3[] yl3VarArr = new yl3[enumArr.length];
            int length = enumArr.length;
            for (int i = 0; i < length; i++) {
                Enum<?> r5 = enumArr[i];
                String str = findEnumValues[i];
                if (str == null) {
                    str = r5.name();
                }
                yl3VarArr[r5.ordinal()] = mapperConfig.compileString(str);
            }
            return new EnumValues(cls, yl3VarArr);
        }
        throw new IllegalArgumentException("Can not determine enum constants for Class " + cls.getName());
    }

    public static EnumValues constructFromToString(MapperConfig<?> mapperConfig, Class<Enum<?>> cls) {
        Enum[] enumArr = (Enum[]) c.n(cls).getEnumConstants();
        if (enumArr != null) {
            yl3[] yl3VarArr = new yl3[enumArr.length];
            for (Enum r4 : enumArr) {
                yl3VarArr[r4.ordinal()] = mapperConfig.compileString(r4.toString());
            }
            return new EnumValues(cls, yl3VarArr);
        }
        throw new IllegalArgumentException("Can not determine enum constants for Class " + cls.getName());
    }

    public List<Enum<?>> enums() {
        return Arrays.asList(this._values);
    }

    public Class<Enum<?>> getEnumClass() {
        return this._enumClass;
    }

    public EnumMap<?, yl3> internalMap() {
        Enum<?>[] enumArr;
        EnumMap<?, yl3> enumMap = this.a;
        if (enumMap == null) {
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            for (Enum<?> r4 : this._values) {
                linkedHashMap.put(r4, this._textual[r4.ordinal()]);
            }
            return new EnumMap<>(linkedHashMap);
        }
        return enumMap;
    }

    public yl3 serializedValueFor(Enum<?> r2) {
        return this._textual[r2.ordinal()];
    }

    public Collection<yl3> values() {
        return Arrays.asList(this._textual);
    }
}
