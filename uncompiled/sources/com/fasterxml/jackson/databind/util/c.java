package com.fasterxml.jackson.databind.util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.github.mikephil.charting.utils.Utils;
import java.io.Closeable;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

/* compiled from: ClassUtil.java */
/* loaded from: classes.dex */
public final class c {
    public static final Class<?> a = Object.class;
    public static final Annotation[] b = new Annotation[0];
    public static final b[] c = new b[0];
    public static final C0097c<?> d = new C0097c<>();

    /* compiled from: ClassUtil.java */
    /* loaded from: classes.dex */
    public static final class b {
        public final Constructor<?> a;
        public Annotation[] b;
        public Annotation[][] c;
        public int d = -1;

        public b(Constructor<?> constructor) {
            this.a = constructor;
        }

        public Constructor<?> a() {
            return this.a;
        }

        public Annotation[] b() {
            Annotation[] annotationArr = this.b;
            if (annotationArr == null) {
                Annotation[] declaredAnnotations = this.a.getDeclaredAnnotations();
                this.b = declaredAnnotations;
                return declaredAnnotations;
            }
            return annotationArr;
        }

        public Class<?> c() {
            return this.a.getDeclaringClass();
        }

        public int d() {
            int i = this.d;
            if (i < 0) {
                int length = this.a.getParameterTypes().length;
                this.d = length;
                return length;
            }
            return i;
        }

        public Annotation[][] e() {
            Annotation[][] annotationArr = this.c;
            if (annotationArr == null) {
                Annotation[][] parameterAnnotations = this.a.getParameterAnnotations();
                this.c = parameterAnnotations;
                return parameterAnnotations;
            }
            return annotationArr;
        }
    }

    /* compiled from: ClassUtil.java */
    /* renamed from: com.fasterxml.jackson.databind.util.c$c  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static final class C0097c<T> implements Iterator<T> {
        public C0097c() {
        }

        @Override // java.util.Iterator
        public boolean hasNext() {
            return false;
        }

        @Override // java.util.Iterator
        public T next() {
            throw new NoSuchElementException();
        }

        @Override // java.util.Iterator
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    /* compiled from: ClassUtil.java */
    /* loaded from: classes.dex */
    public static class d {
        public static final d c = new d();
        public final Field a = d(EnumSet.class, "elementType", Class.class);
        public final Field b = d(EnumMap.class, "elementType", Class.class);

        public static Field d(Class<?> cls, String str, Class<?> cls2) {
            Field field;
            Field[] x = c.x(cls);
            int length = x.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    field = null;
                    break;
                }
                field = x[i];
                if (str.equals(field.getName()) && field.getType() == cls2) {
                    break;
                }
                i++;
            }
            if (field == null) {
                for (Field field2 : x) {
                    if (field2.getType() == cls2) {
                        if (field != null) {
                            return null;
                        }
                        field = field2;
                    }
                }
            }
            if (field != null) {
                try {
                    field.setAccessible(true);
                } catch (Throwable unused) {
                }
            }
            return field;
        }

        public Class<? extends Enum<?>> a(EnumMap<?, ?> enumMap) {
            Field field = this.b;
            if (field != null) {
                return (Class) c(enumMap, field);
            }
            throw new IllegalStateException("Can not figure out type for EnumMap (odd JDK platform?)");
        }

        public Class<? extends Enum<?>> b(EnumSet<?> enumSet) {
            Field field = this.a;
            if (field != null) {
                return (Class) c(enumSet, field);
            }
            throw new IllegalStateException("Can not figure out type for EnumSet (odd JDK platform?)");
        }

        public final Object c(Object obj, Field field) {
            try {
                return field.get(obj);
            } catch (Exception e) {
                throw new IllegalArgumentException(e);
            }
        }
    }

    public static Type[] A(Class<?> cls) {
        return cls.getGenericInterfaces();
    }

    public static Type B(Class<?> cls) {
        return cls.getGenericSuperclass();
    }

    public static Class<?> C(Class<?> cls) {
        try {
            if (!F(cls) && !Modifier.isStatic(cls.getModifiers())) {
                return z(cls);
            }
        } catch (SecurityException unused) {
        }
        return null;
    }

    public static String D(Class<?> cls) {
        Package r0 = cls.getPackage();
        if (r0 == null) {
            return null;
        }
        return r0.getName();
    }

    public static Throwable E(Throwable th) {
        while (th.getCause() != null) {
            th = th.getCause();
        }
        return th;
    }

    public static boolean F(Class<?> cls) {
        return (M(cls) || cls.getEnclosingMethod() == null) ? false : true;
    }

    public static boolean G(Class<?> cls) {
        return cls == Void.class || cls == Void.TYPE || cls == ig2.class;
    }

    public static boolean H(Class<?> cls) {
        return (cls.getModifiers() & 1536) == 0;
    }

    public static boolean I(Class<?> cls) {
        return cls.getAnnotation(pt1.class) != null;
    }

    public static boolean J(Object obj) {
        return obj != null && I(obj.getClass());
    }

    public static String K(Class<?> cls, boolean z) {
        try {
            if (F(cls)) {
                return "local/anonymous";
            }
            if (z || Modifier.isStatic(cls.getModifiers())) {
                return null;
            }
            if (z(cls) != null) {
                return "non-static member class";
            }
            return null;
        } catch (NullPointerException | SecurityException unused) {
            return null;
        }
    }

    public static boolean L(Class<?> cls) {
        return (Modifier.isStatic(cls.getModifiers()) || z(cls) == null) ? false : true;
    }

    public static boolean M(Class<?> cls) {
        return cls == a || cls.isPrimitive();
    }

    public static boolean N(Class<?> cls) {
        String name = cls.getName();
        return name.startsWith("net.sf.cglib.proxy.") || name.startsWith("org.hibernate.proxy.");
    }

    public static Class<?> O(Class<?> cls) {
        if (cls.isPrimitive()) {
            return cls;
        }
        if (cls == Integer.class) {
            return Integer.TYPE;
        }
        if (cls == Long.class) {
            return Long.TYPE;
        }
        if (cls == Boolean.class) {
            return Boolean.TYPE;
        }
        if (cls == Double.class) {
            return Double.TYPE;
        }
        if (cls == Float.class) {
            return Float.TYPE;
        }
        if (cls == Byte.class) {
            return Byte.TYPE;
        }
        if (cls == Short.class) {
            return Short.TYPE;
        }
        if (cls == Character.class) {
            return Character.TYPE;
        }
        return null;
    }

    public static void P(Throwable th) {
        Q(th, th.getMessage());
    }

    public static void Q(Throwable th, String str) {
        if (!(th instanceof RuntimeException)) {
            if (th instanceof Error) {
                throw ((Error) th);
            }
            throw new IllegalArgumentException(str, th);
        }
        throw ((RuntimeException) th);
    }

    public static Throwable R(Throwable th) throws IOException {
        Throwable E = E(th);
        if (E instanceof IOException) {
            throw ((IOException) E);
        }
        return E;
    }

    public static void S(Throwable th) {
        P(E(th));
    }

    public static void T(Throwable th, String str) {
        Q(E(th), str);
    }

    public static Class<?> U(Class<?> cls) {
        if (cls == Integer.TYPE) {
            return Integer.class;
        }
        if (cls == Long.TYPE) {
            return Long.class;
        }
        if (cls == Boolean.TYPE) {
            return Boolean.class;
        }
        if (cls == Double.TYPE) {
            return Double.class;
        }
        if (cls == Float.TYPE) {
            return Float.class;
        }
        if (cls == Byte.TYPE) {
            return Byte.class;
        }
        if (cls == Short.TYPE) {
            return Short.class;
        }
        if (cls == Character.TYPE) {
            return Character.class;
        }
        throw new IllegalArgumentException("Class " + cls.getName() + " is not a primitive type");
    }

    public static void a(Class<?> cls, Class<?> cls2, Collection<Class<?>> collection, boolean z) {
        if (cls == cls2 || cls == null || cls == Object.class) {
            return;
        }
        if (z) {
            if (collection.contains(cls)) {
                return;
            }
            collection.add(cls);
        }
        for (Class<?> cls3 : c(cls)) {
            a(cls3, cls2, collection, true);
        }
        a(cls.getSuperclass(), cls2, collection, true);
    }

    public static void b(JavaType javaType, Class<?> cls, Collection<JavaType> collection, boolean z) {
        Class<?> rawClass;
        if (javaType == null || (rawClass = javaType.getRawClass()) == cls || rawClass == Object.class) {
            return;
        }
        if (z) {
            if (collection.contains(javaType)) {
                return;
            }
            collection.add(javaType);
        }
        for (JavaType javaType2 : javaType.getInterfaces()) {
            b(javaType2, cls, collection, true);
        }
        b(javaType.getSuperClass(), cls, collection, true);
    }

    public static Class<?>[] c(Class<?> cls) {
        return cls.getInterfaces();
    }

    public static String d(Class<?> cls) {
        if (cls.isAnnotation()) {
            return "annotation";
        }
        if (cls.isArray()) {
            return "array";
        }
        if (cls.isEnum()) {
            return "enum";
        }
        if (cls.isPrimitive()) {
            return "primitive";
        }
        return null;
    }

    @Deprecated
    public static void e(Member member) {
        f(member, false);
    }

    public static void f(Member member, boolean z) {
        AccessibleObject accessibleObject = (AccessibleObject) member;
        if (!z) {
            try {
                if (Modifier.isPublic(member.getModifiers()) && Modifier.isPublic(member.getDeclaringClass().getModifiers())) {
                    return;
                }
            } catch (SecurityException e) {
                if (accessibleObject.isAccessible()) {
                    return;
                }
                Class<?> declaringClass = member.getDeclaringClass();
                throw new IllegalArgumentException("Can not access " + member + " (from class " + declaringClass.getName() + "; failed to set access: " + e.getMessage());
            }
        }
        accessibleObject.setAccessible(true);
    }

    public static void g(JsonGenerator jsonGenerator, Closeable closeable, Exception exc) throws IOException {
        if (jsonGenerator != null) {
            jsonGenerator.i(JsonGenerator.Feature.AUTO_CLOSE_JSON_CONTENT);
            try {
                jsonGenerator.close();
            } catch (Exception e) {
                exc.addSuppressed(e);
            }
        }
        if (closeable != null) {
            try {
                closeable.close();
            } catch (Exception e2) {
                exc.addSuppressed(e2);
            }
        }
        if (!(exc instanceof IOException)) {
            if (exc instanceof RuntimeException) {
                throw ((RuntimeException) exc);
            }
            throw new RuntimeException(exc);
        }
        throw ((IOException) exc);
    }

    public static void h(JsonGenerator jsonGenerator, Exception exc) throws IOException {
        jsonGenerator.i(JsonGenerator.Feature.AUTO_CLOSE_JSON_CONTENT);
        try {
            jsonGenerator.close();
        } catch (Exception e) {
            exc.addSuppressed(e);
        }
        if (!(exc instanceof IOException)) {
            if (exc instanceof RuntimeException) {
                throw ((RuntimeException) exc);
            }
            throw new RuntimeException(exc);
        }
        throw ((IOException) exc);
    }

    public static <T> T i(Class<T> cls, boolean z) throws IllegalArgumentException {
        Constructor m = m(cls, z);
        if (m != null) {
            try {
                return (T) m.newInstance(new Object[0]);
            } catch (Exception e) {
                T(e, "Failed to instantiate class " + cls.getName() + ", problem: " + e.getMessage());
                return null;
            }
        }
        throw new IllegalArgumentException("Class " + cls.getName() + " has no default (no arg) constructor");
    }

    public static Object j(Class<?> cls) {
        if (cls == Integer.TYPE) {
            return 0;
        }
        if (cls == Long.TYPE) {
            return 0L;
        }
        if (cls == Boolean.TYPE) {
            return Boolean.FALSE;
        }
        if (cls == Double.TYPE) {
            return Double.valueOf((double) Utils.DOUBLE_EPSILON);
        }
        if (cls == Float.TYPE) {
            return Float.valueOf((float) Utils.FLOAT_EPSILON);
        }
        if (cls == Byte.TYPE) {
            return (byte) 0;
        }
        if (cls == Short.TYPE) {
            return (short) 0;
        }
        if (cls == Character.TYPE) {
            return (char) 0;
        }
        throw new IllegalArgumentException("Class " + cls.getName() + " is not a primitive type");
    }

    public static <T> Iterator<T> k() {
        return d;
    }

    public static Annotation[] l(Class<?> cls) {
        if (M(cls)) {
            return b;
        }
        return cls.getDeclaredAnnotations();
    }

    public static <T> Constructor<T> m(Class<T> cls, boolean z) throws IllegalArgumentException {
        try {
            Constructor<T> declaredConstructor = cls.getDeclaredConstructor(new Class[0]);
            if (z) {
                e(declaredConstructor);
            } else if (!Modifier.isPublic(declaredConstructor.getModifiers())) {
                throw new IllegalArgumentException("Default constructor for " + cls.getName() + " is not accessible (non-public?): not allowed to try modify access via Reflection: can not instantiate type");
            }
            return declaredConstructor;
        } catch (NoSuchMethodException unused) {
            return null;
        } catch (Exception e) {
            T(e, "Failed to find default constructor of class " + cls.getName() + ", problem: " + e.getMessage());
            return null;
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static Class<? extends Enum<?>> n(Class<?> cls) {
        return cls.getSuperclass() != Enum.class ? cls.getSuperclass() : cls;
    }

    public static Class<? extends Enum<?>> o(Enum<?> r2) {
        Class cls = r2.getClass();
        return cls.getSuperclass() != Enum.class ? cls.getSuperclass() : cls;
    }

    public static Class<? extends Enum<?>> p(EnumMap<?, ?> enumMap) {
        if (!enumMap.isEmpty()) {
            return o((Enum) enumMap.keySet().iterator().next());
        }
        return d.c.a(enumMap);
    }

    public static Class<? extends Enum<?>> q(EnumSet<?> enumSet) {
        if (!enumSet.isEmpty()) {
            return o((Enum) enumSet.iterator().next());
        }
        return d.c.b(enumSet);
    }

    public static <T extends Annotation> Enum<?> r(Class<Enum<?>> cls, Class<T> cls2) {
        Field[] x;
        Enum<?>[] enumConstants;
        for (Field field : x(cls)) {
            if (field.isEnumConstant() && field.getAnnotation(cls2) != null) {
                String name = field.getName();
                for (Enum<?> r8 : cls.getEnumConstants()) {
                    if (name.equals(r8.name())) {
                        return r8;
                    }
                }
                continue;
            }
        }
        return null;
    }

    public static List<Class<?>> s(Class<?> cls, Class<?> cls2, boolean z) {
        if (cls != null && cls != cls2 && cls != Object.class) {
            ArrayList arrayList = new ArrayList(8);
            a(cls, cls2, arrayList, z);
            return arrayList;
        }
        return Collections.emptyList();
    }

    public static List<Class<?>> t(Class<?> cls, Class<?> cls2, boolean z) {
        LinkedList linkedList = new LinkedList();
        if (cls != null && cls != cls2) {
            if (z) {
                linkedList.add(cls);
            }
            while (true) {
                cls = cls.getSuperclass();
                if (cls == null || cls == cls2) {
                    break;
                }
                linkedList.add(cls);
            }
        }
        return linkedList;
    }

    public static List<JavaType> u(JavaType javaType, Class<?> cls, boolean z) {
        if (javaType != null && !javaType.hasRawClass(cls) && !javaType.hasRawClass(Object.class)) {
            ArrayList arrayList = new ArrayList(8);
            b(javaType, cls, arrayList, z);
            return arrayList;
        }
        return Collections.emptyList();
    }

    public static String v(Object obj) {
        if (obj == null) {
            return "unknown";
        }
        return (obj instanceof Class ? (Class) obj : obj.getClass()).getName();
    }

    public static b[] w(Class<?> cls) {
        if (!cls.isInterface() && !M(cls)) {
            Constructor<?>[] declaredConstructors = cls.getDeclaredConstructors();
            int length = declaredConstructors.length;
            b[] bVarArr = new b[length];
            for (int i = 0; i < length; i++) {
                bVarArr[i] = new b(declaredConstructors[i]);
            }
            return bVarArr;
        }
        return c;
    }

    public static Field[] x(Class<?> cls) {
        return cls.getDeclaredFields();
    }

    public static Method[] y(Class<?> cls) {
        return cls.getDeclaredMethods();
    }

    public static Class<?> z(Class<?> cls) {
        if (M(cls)) {
            return null;
        }
        return cls.getEnclosingClass();
    }
}
