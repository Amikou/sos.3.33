package com.fasterxml.jackson.databind.util;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;

/* compiled from: ByteBufferBackedOutputStream.java */
/* loaded from: classes.dex */
public class b extends OutputStream {
    public final ByteBuffer a;

    public b(ByteBuffer byteBuffer) {
        this.a = byteBuffer;
    }

    @Override // java.io.OutputStream
    public void write(int i) throws IOException {
        this.a.put((byte) i);
    }

    @Override // java.io.OutputStream
    public void write(byte[] bArr, int i, int i2) throws IOException {
        this.a.put(bArr, i, i2);
    }
}
