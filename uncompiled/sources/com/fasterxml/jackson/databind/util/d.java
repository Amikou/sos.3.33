package com.fasterxml.jackson.databind.util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.k;
import java.io.IOException;

/* compiled from: RawValue.java */
/* loaded from: classes.dex */
public class d implements com.fasterxml.jackson.databind.e {
    public Object a;

    public d(String str) {
        this.a = str;
    }

    public void a(JsonGenerator jsonGenerator) throws IOException {
        Object obj = this.a;
        if (obj instanceof yl3) {
            jsonGenerator.Z0((yl3) obj);
        } else {
            jsonGenerator.c1(String.valueOf(obj));
        }
    }

    public void b(JsonGenerator jsonGenerator) throws IOException {
        Object obj = this.a;
        if (obj instanceof com.fasterxml.jackson.databind.e) {
            jsonGenerator.J0(obj);
        } else {
            a(jsonGenerator);
        }
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof d) {
            Object obj2 = this.a;
            Object obj3 = ((d) obj).a;
            if (obj2 == obj3) {
                return true;
            }
            return obj2 != null && obj2.equals(obj3);
        }
        return false;
    }

    public int hashCode() {
        Object obj = this.a;
        if (obj == null) {
            return 0;
        }
        return obj.hashCode();
    }

    @Override // com.fasterxml.jackson.databind.e
    public void serialize(JsonGenerator jsonGenerator, k kVar) throws IOException {
        Object obj = this.a;
        if (obj instanceof com.fasterxml.jackson.databind.e) {
            ((com.fasterxml.jackson.databind.e) obj).serialize(jsonGenerator, kVar);
        } else {
            a(jsonGenerator);
        }
    }

    @Override // com.fasterxml.jackson.databind.e
    public void serializeWithType(JsonGenerator jsonGenerator, k kVar, com.fasterxml.jackson.databind.jsontype.c cVar) throws IOException {
        Object obj = this.a;
        if (obj instanceof com.fasterxml.jackson.databind.e) {
            ((com.fasterxml.jackson.databind.e) obj).serializeWithType(jsonGenerator, kVar, cVar);
        } else if (obj instanceof yl3) {
            serialize(jsonGenerator, kVar);
        }
    }

    public String toString() {
        Object[] objArr = new Object[1];
        Object obj = this.a;
        objArr[0] = obj == null ? "NULL" : obj.getClass().getName();
        return String.format("[RawValue of type %s]", objArr);
    }
}
