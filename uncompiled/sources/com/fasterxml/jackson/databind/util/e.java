package com.fasterxml.jackson.databind.util;

import com.fasterxml.jackson.core.Base64Variant;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonLocation;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.DeserializationFeature;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.TreeMap;

/* compiled from: TokenBuffer.java */
/* loaded from: classes.dex */
public class e extends JsonGenerator {
    public static final int s0 = JsonGenerator.Feature.collectDefaults();
    public com.fasterxml.jackson.core.c f0;
    public int g0;
    public boolean h0;
    public boolean i0;
    public boolean j0;
    public boolean k0;
    public c l0;
    public c m0;
    public int n0;
    public Object o0;
    public Object p0;
    public boolean q0;
    public mw1 r0;

    /* compiled from: TokenBuffer.java */
    /* loaded from: classes.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;
        public static final /* synthetic */ int[] b;

        static {
            int[] iArr = new int[JsonParser.NumberType.values().length];
            b = iArr;
            try {
                iArr[JsonParser.NumberType.INT.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                b[JsonParser.NumberType.BIG_INTEGER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                b[JsonParser.NumberType.BIG_DECIMAL.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                b[JsonParser.NumberType.FLOAT.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                b[JsonParser.NumberType.LONG.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            int[] iArr2 = new int[JsonToken.values().length];
            a = iArr2;
            try {
                iArr2[JsonToken.START_OBJECT.ordinal()] = 1;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                a[JsonToken.END_OBJECT.ordinal()] = 2;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                a[JsonToken.START_ARRAY.ordinal()] = 3;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                a[JsonToken.END_ARRAY.ordinal()] = 4;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                a[JsonToken.FIELD_NAME.ordinal()] = 5;
            } catch (NoSuchFieldError unused10) {
            }
            try {
                a[JsonToken.VALUE_STRING.ordinal()] = 6;
            } catch (NoSuchFieldError unused11) {
            }
            try {
                a[JsonToken.VALUE_NUMBER_INT.ordinal()] = 7;
            } catch (NoSuchFieldError unused12) {
            }
            try {
                a[JsonToken.VALUE_NUMBER_FLOAT.ordinal()] = 8;
            } catch (NoSuchFieldError unused13) {
            }
            try {
                a[JsonToken.VALUE_TRUE.ordinal()] = 9;
            } catch (NoSuchFieldError unused14) {
            }
            try {
                a[JsonToken.VALUE_FALSE.ordinal()] = 10;
            } catch (NoSuchFieldError unused15) {
            }
            try {
                a[JsonToken.VALUE_NULL.ordinal()] = 11;
            } catch (NoSuchFieldError unused16) {
            }
            try {
                a[JsonToken.VALUE_EMBEDDED_OBJECT.ordinal()] = 12;
            } catch (NoSuchFieldError unused17) {
            }
        }
    }

    /* compiled from: TokenBuffer.java */
    /* loaded from: classes.dex */
    public static final class b extends rp2 {
        public com.fasterxml.jackson.core.c h0;
        public final boolean i0;
        public final boolean j0;
        public c k0;
        public int l0;
        public hv1 m0;
        public boolean n0;
        public transient ms o0;
        public JsonLocation p0;

        public b(c cVar, com.fasterxml.jackson.core.c cVar2, boolean z, boolean z2) {
            super(0);
            this.p0 = null;
            this.k0 = cVar;
            this.l0 = -1;
            this.h0 = cVar2;
            this.m0 = hv1.n(null);
            this.i0 = z;
            this.j0 = z2;
        }

        @Override // com.fasterxml.jackson.core.JsonParser
        public float A() throws IOException {
            return Q().floatValue();
        }

        @Override // com.fasterxml.jackson.core.JsonParser
        public boolean B0() {
            return false;
        }

        @Override // com.fasterxml.jackson.core.JsonParser
        public int F() throws IOException {
            if (this.g0 == JsonToken.VALUE_NUMBER_INT) {
                return ((Number) O1()).intValue();
            }
            return Q().intValue();
        }

        @Override // com.fasterxml.jackson.core.JsonParser
        public long M() throws IOException {
            return Q().longValue();
        }

        @Override // com.fasterxml.jackson.core.JsonParser
        public JsonParser.NumberType N() throws IOException {
            Number Q = Q();
            if (Q instanceof Integer) {
                return JsonParser.NumberType.INT;
            }
            if (Q instanceof Long) {
                return JsonParser.NumberType.LONG;
            }
            if (Q instanceof Double) {
                return JsonParser.NumberType.DOUBLE;
            }
            if (Q instanceof BigDecimal) {
                return JsonParser.NumberType.BIG_DECIMAL;
            }
            if (Q instanceof BigInteger) {
                return JsonParser.NumberType.BIG_INTEGER;
            }
            if (Q instanceof Float) {
                return JsonParser.NumberType.FLOAT;
            }
            if (Q instanceof Short) {
                return JsonParser.NumberType.INT;
            }
            return null;
        }

        public final void N1() throws JsonParseException {
            JsonToken jsonToken = this.g0;
            if (jsonToken == null || !jsonToken.isNumeric()) {
                throw a("Current token (" + this.g0 + ") not numeric, can not use numeric value accessors");
            }
        }

        @Override // com.fasterxml.jackson.core.JsonParser
        public String O0() throws IOException {
            c cVar;
            if (this.n0 || (cVar = this.k0) == null) {
                return null;
            }
            int i = this.l0 + 1;
            if (i < 16 && cVar.q(i) == JsonToken.FIELD_NAME) {
                this.l0 = i;
                Object j = this.k0.j(i);
                String obj = j instanceof String ? (String) j : j.toString();
                this.m0.t(obj);
                return obj;
            } else if (T0() == JsonToken.FIELD_NAME) {
                return r();
            } else {
                return null;
            }
        }

        public final Object O1() {
            return this.k0.j(this.l0);
        }

        public void P1(JsonLocation jsonLocation) {
            this.p0 = jsonLocation;
        }

        @Override // com.fasterxml.jackson.core.JsonParser
        public final Number Q() throws IOException {
            N1();
            Object O1 = O1();
            if (O1 instanceof Number) {
                return (Number) O1;
            }
            if (O1 instanceof String) {
                String str = (String) O1;
                if (str.indexOf(46) >= 0) {
                    return Double.valueOf(Double.parseDouble(str));
                }
                return Long.valueOf(Long.parseLong(str));
            } else if (O1 == null) {
                return null;
            } else {
                throw new IllegalStateException("Internal error: entry should be a Number, but is of type " + O1.getClass().getName());
            }
        }

        @Override // com.fasterxml.jackson.core.JsonParser
        public Object R() {
            return this.k0.h(this.l0);
        }

        @Override // com.fasterxml.jackson.core.JsonParser
        public cw1 S() {
            return this.m0;
        }

        @Override // com.fasterxml.jackson.core.JsonParser
        public JsonToken T0() throws IOException {
            c cVar;
            if (this.n0 || (cVar = this.k0) == null) {
                return null;
            }
            int i = this.l0 + 1;
            this.l0 = i;
            if (i >= 16) {
                this.l0 = 0;
                c l = cVar.l();
                this.k0 = l;
                if (l == null) {
                    return null;
                }
            }
            JsonToken q = this.k0.q(this.l0);
            this.g0 = q;
            if (q == JsonToken.FIELD_NAME) {
                Object O1 = O1();
                this.m0.t(O1 instanceof String ? (String) O1 : O1.toString());
            } else if (q == JsonToken.START_OBJECT) {
                this.m0 = this.m0.m(-1, -1);
            } else if (q == JsonToken.START_ARRAY) {
                this.m0 = this.m0.l(-1, -1);
            } else if (q == JsonToken.END_OBJECT || q == JsonToken.END_ARRAY) {
                hv1 d = this.m0.d();
                this.m0 = d;
                if (d == null) {
                    this.m0 = hv1.n(null);
                }
            }
            return this.g0;
        }

        @Override // com.fasterxml.jackson.core.JsonParser
        public String X() {
            JsonToken jsonToken = this.g0;
            if (jsonToken == JsonToken.VALUE_STRING || jsonToken == JsonToken.FIELD_NAME) {
                Object O1 = O1();
                if (O1 instanceof String) {
                    return (String) O1;
                }
                if (O1 == null) {
                    return null;
                }
                return O1.toString();
            } else if (jsonToken == null) {
                return null;
            } else {
                int i = a.a[jsonToken.ordinal()];
                if (i != 7 && i != 8) {
                    return this.g0.asString();
                }
                Object O12 = O1();
                if (O12 == null) {
                    return null;
                }
                return O12.toString();
            }
        }

        @Override // com.fasterxml.jackson.core.JsonParser
        public int Z0(Base64Variant base64Variant, OutputStream outputStream) throws IOException {
            byte[] j = j(base64Variant);
            if (j != null) {
                outputStream.write(j, 0, j.length);
                return j.length;
            }
            return 0;
        }

        @Override // com.fasterxml.jackson.core.JsonParser
        public char[] a0() {
            String X = X();
            if (X == null) {
                return null;
            }
            return X.toCharArray();
        }

        @Override // com.fasterxml.jackson.core.JsonParser
        public int b0() {
            String X = X();
            if (X == null) {
                return 0;
            }
            return X.length();
        }

        @Override // com.fasterxml.jackson.core.JsonParser
        public boolean c() {
            return this.j0;
        }

        @Override // com.fasterxml.jackson.core.JsonParser, java.io.Closeable, java.lang.AutoCloseable
        public void close() throws IOException {
            if (this.n0) {
                return;
            }
            this.n0 = true;
        }

        @Override // com.fasterxml.jackson.core.JsonParser
        public boolean d() {
            return this.i0;
        }

        @Override // com.fasterxml.jackson.core.JsonParser
        public int e0() {
            return 0;
        }

        @Override // com.fasterxml.jackson.core.JsonParser
        public JsonLocation f0() {
            return q();
        }

        @Override // com.fasterxml.jackson.core.JsonParser
        public Object g0() {
            return this.k0.i(this.l0);
        }

        @Override // com.fasterxml.jackson.core.JsonParser
        public BigInteger h() throws IOException {
            Number Q = Q();
            if (Q instanceof BigInteger) {
                return (BigInteger) Q;
            }
            if (N() == JsonParser.NumberType.BIG_DECIMAL) {
                return ((BigDecimal) Q).toBigInteger();
            }
            return BigInteger.valueOf(Q.longValue());
        }

        @Override // com.fasterxml.jackson.core.JsonParser
        public byte[] j(Base64Variant base64Variant) throws IOException, JsonParseException {
            if (this.g0 == JsonToken.VALUE_EMBEDDED_OBJECT) {
                Object O1 = O1();
                if (O1 instanceof byte[]) {
                    return (byte[]) O1;
                }
            }
            if (this.g0 == JsonToken.VALUE_STRING) {
                String X = X();
                if (X == null) {
                    return null;
                }
                ms msVar = this.o0;
                if (msVar == null) {
                    msVar = new ms(100);
                    this.o0 = msVar;
                } else {
                    msVar.j();
                }
                o1(X, msVar, base64Variant);
                return msVar.n();
            }
            throw a("Current token (" + this.g0 + ") not VALUE_STRING (or VALUE_EMBEDDED_OBJECT with byte[]), can not access as binary");
        }

        @Override // com.fasterxml.jackson.core.JsonParser
        public com.fasterxml.jackson.core.c n() {
            return this.h0;
        }

        @Override // com.fasterxml.jackson.core.JsonParser
        public JsonLocation q() {
            JsonLocation jsonLocation = this.p0;
            return jsonLocation == null ? JsonLocation.NA : jsonLocation;
        }

        @Override // com.fasterxml.jackson.core.JsonParser
        public String r() {
            JsonToken jsonToken = this.g0;
            if (jsonToken != JsonToken.START_OBJECT && jsonToken != JsonToken.START_ARRAY) {
                return this.m0.b();
            }
            return this.m0.d().b();
        }

        @Override // defpackage.rp2
        public void r1() throws JsonParseException {
            J1();
        }

        @Override // com.fasterxml.jackson.core.JsonParser
        public BigDecimal w() throws IOException {
            Number Q = Q();
            if (Q instanceof BigDecimal) {
                return (BigDecimal) Q;
            }
            int i = a.b[N().ordinal()];
            if (i != 1) {
                if (i == 2) {
                    return new BigDecimal((BigInteger) Q);
                }
                if (i != 5) {
                    return BigDecimal.valueOf(Q.doubleValue());
                }
            }
            return BigDecimal.valueOf(Q.longValue());
        }

        @Override // com.fasterxml.jackson.core.JsonParser
        public double x() throws IOException {
            return Q().doubleValue();
        }

        @Override // com.fasterxml.jackson.core.JsonParser
        public Object z() {
            if (this.g0 == JsonToken.VALUE_EMBEDDED_OBJECT) {
                return O1();
            }
            return null;
        }
    }

    /* compiled from: TokenBuffer.java */
    /* loaded from: classes.dex */
    public static final class c {
        public static final JsonToken[] e;
        public c a;
        public long b;
        public final Object[] c = new Object[16];
        public TreeMap<Integer, Object> d;

        static {
            JsonToken[] jsonTokenArr = new JsonToken[16];
            e = jsonTokenArr;
            JsonToken[] values = JsonToken.values();
            System.arraycopy(values, 1, jsonTokenArr, 1, Math.min(15, values.length - 1));
        }

        public final int a(int i) {
            return i + i + 1;
        }

        public final int b(int i) {
            return i + i;
        }

        public c c(int i, JsonToken jsonToken) {
            if (i < 16) {
                m(i, jsonToken);
                return null;
            }
            c cVar = new c();
            this.a = cVar;
            cVar.m(0, jsonToken);
            return this.a;
        }

        public c d(int i, JsonToken jsonToken, Object obj) {
            if (i < 16) {
                n(i, jsonToken, obj);
                return null;
            }
            c cVar = new c();
            this.a = cVar;
            cVar.n(0, jsonToken, obj);
            return this.a;
        }

        public c e(int i, JsonToken jsonToken, Object obj, Object obj2) {
            if (i < 16) {
                o(i, jsonToken, obj, obj2);
                return null;
            }
            c cVar = new c();
            this.a = cVar;
            cVar.o(0, jsonToken, obj, obj2);
            return this.a;
        }

        public c f(int i, JsonToken jsonToken, Object obj, Object obj2, Object obj3) {
            if (i < 16) {
                p(i, jsonToken, obj, obj2, obj3);
                return null;
            }
            c cVar = new c();
            this.a = cVar;
            cVar.p(0, jsonToken, obj, obj2, obj3);
            return this.a;
        }

        public final void g(int i, Object obj, Object obj2) {
            if (this.d == null) {
                this.d = new TreeMap<>();
            }
            if (obj != null) {
                this.d.put(Integer.valueOf(a(i)), obj);
            }
            if (obj2 != null) {
                this.d.put(Integer.valueOf(b(i)), obj2);
            }
        }

        public Object h(int i) {
            TreeMap<Integer, Object> treeMap = this.d;
            if (treeMap == null) {
                return null;
            }
            return treeMap.get(Integer.valueOf(a(i)));
        }

        public Object i(int i) {
            TreeMap<Integer, Object> treeMap = this.d;
            if (treeMap == null) {
                return null;
            }
            return treeMap.get(Integer.valueOf(b(i)));
        }

        public Object j(int i) {
            return this.c[i];
        }

        public boolean k() {
            return this.d != null;
        }

        public c l() {
            return this.a;
        }

        public final void m(int i, JsonToken jsonToken) {
            long ordinal = jsonToken.ordinal();
            if (i > 0) {
                ordinal <<= i << 2;
            }
            this.b |= ordinal;
        }

        public final void n(int i, JsonToken jsonToken, Object obj) {
            this.c[i] = obj;
            long ordinal = jsonToken.ordinal();
            if (i > 0) {
                ordinal <<= i << 2;
            }
            this.b |= ordinal;
        }

        public final void o(int i, JsonToken jsonToken, Object obj, Object obj2) {
            long ordinal = jsonToken.ordinal();
            if (i > 0) {
                ordinal <<= i << 2;
            }
            this.b = ordinal | this.b;
            g(i, obj, obj2);
        }

        public final void p(int i, JsonToken jsonToken, Object obj, Object obj2, Object obj3) {
            this.c[i] = obj;
            long ordinal = jsonToken.ordinal();
            if (i > 0) {
                ordinal <<= i << 2;
            }
            this.b = ordinal | this.b;
            g(i, obj2, obj3);
        }

        public JsonToken q(int i) {
            long j = this.b;
            if (i > 0) {
                j >>= i << 2;
            }
            return e[((int) j) & 15];
        }
    }

    public e(com.fasterxml.jackson.core.c cVar, boolean z) {
        this.q0 = false;
        this.f0 = cVar;
        this.g0 = s0;
        this.r0 = mw1.o(null);
        c cVar2 = new c();
        this.m0 = cVar2;
        this.l0 = cVar2;
        this.n0 = 0;
        this.h0 = z;
        this.i0 = z;
        this.j0 = z | z;
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void B0(BigDecimal bigDecimal) throws IOException {
        if (bigDecimal == null) {
            m0();
        } else {
            F1(JsonToken.VALUE_NUMBER_FLOAT, bigDecimal);
        }
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void F0(BigInteger bigInteger) throws IOException {
        if (bigInteger == null) {
            m0();
        } else {
            F1(JsonToken.VALUE_NUMBER_INT, bigInteger);
        }
    }

    public final void F1(JsonToken jsonToken, Object obj) {
        c d;
        this.r0.u();
        if (this.q0) {
            d = this.m0.f(this.n0, jsonToken, obj, this.p0, this.o0);
        } else {
            d = this.m0.d(this.n0, jsonToken, obj);
        }
        if (d == null) {
            this.n0++;
            return;
        }
        this.m0 = d;
        this.n0 = 1;
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void H0(short s) throws IOException {
        F1(JsonToken.VALUE_NUMBER_INT, Short.valueOf(s));
    }

    public final void H1(JsonParser jsonParser) throws IOException {
        Object g0 = jsonParser.g0();
        this.o0 = g0;
        if (g0 != null) {
            this.q0 = true;
        }
        Object R = jsonParser.R();
        this.p0 = R;
        if (R != null) {
            this.q0 = true;
        }
    }

    public void I1() {
        throw new UnsupportedOperationException("Called operation not supported for TokenBuffer");
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void J0(Object obj) throws IOException {
        if (obj == null) {
            m0();
        } else if (obj.getClass() != byte[].class && !(obj instanceof d)) {
            com.fasterxml.jackson.core.c cVar = this.f0;
            if (cVar == null) {
                F1(JsonToken.VALUE_EMBEDDED_OBJECT, obj);
            } else {
                cVar.writeValue(this, obj);
            }
        } else {
            F1(JsonToken.VALUE_EMBEDDED_OBJECT, obj);
        }
    }

    public e J1(e eVar) throws IOException {
        if (!this.h0) {
            this.h0 = eVar.h();
        }
        if (!this.i0) {
            this.i0 = eVar.g();
        }
        this.j0 = this.h0 | this.i0;
        JsonParser K1 = eVar.K1();
        while (K1.T0() != null) {
            O1(K1);
        }
        return this;
    }

    public JsonParser K1() {
        return M1(this.f0);
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void L0(Object obj) {
        this.p0 = obj;
        this.q0 = true;
    }

    public JsonParser L1(JsonParser jsonParser) {
        b bVar = new b(this.l0, jsonParser.n(), this.h0, this.i0);
        bVar.P1(jsonParser.f0());
        return bVar;
    }

    public JsonParser M1(com.fasterxml.jackson.core.c cVar) {
        return new b(this.l0, cVar, this.h0, this.i0);
    }

    public void N1(JsonParser jsonParser) throws IOException {
        if (this.j0) {
            H1(jsonParser);
        }
        switch (a.a[jsonParser.u().ordinal()]) {
            case 1:
                i1();
                return;
            case 2:
                f0();
                return;
            case 3:
                e1();
                return;
            case 4:
                e0();
                return;
            case 5:
                i0(jsonParser.r());
                return;
            case 6:
                if (jsonParser.B0()) {
                    p1(jsonParser.a0(), jsonParser.e0(), jsonParser.b0());
                    return;
                } else {
                    o1(jsonParser.X());
                    return;
                }
            case 7:
                int i = a.b[jsonParser.N().ordinal()];
                if (i == 1) {
                    x0(jsonParser.F());
                    return;
                } else if (i != 2) {
                    y0(jsonParser.M());
                    return;
                } else {
                    F0(jsonParser.h());
                    return;
                }
            case 8:
                if (this.k0) {
                    B0(jsonParser.w());
                    return;
                }
                int i2 = a.b[jsonParser.N().ordinal()];
                if (i2 == 3) {
                    B0(jsonParser.w());
                    return;
                } else if (i2 != 4) {
                    r0(jsonParser.x());
                    return;
                } else {
                    w0(jsonParser.A());
                    return;
                }
            case 9:
                a0(true);
                return;
            case 10:
                a0(false);
                return;
            case 11:
                m0();
                return;
            case 12:
                J0(jsonParser.z());
                return;
            default:
                throw new RuntimeException("Internal error: should never end up through this code path");
        }
    }

    public void O1(JsonParser jsonParser) throws IOException {
        JsonToken u = jsonParser.u();
        if (u == JsonToken.FIELD_NAME) {
            if (this.j0) {
                H1(jsonParser);
            }
            i0(jsonParser.r());
            u = jsonParser.T0();
        }
        if (this.j0) {
            H1(jsonParser);
        }
        int i = a.a[u.ordinal()];
        if (i == 1) {
            i1();
            while (jsonParser.T0() != JsonToken.END_OBJECT) {
                O1(jsonParser);
            }
            f0();
        } else if (i != 3) {
            N1(jsonParser);
        } else {
            e1();
            while (jsonParser.T0() != JsonToken.END_ARRAY) {
                O1(jsonParser);
            }
            e0();
        }
    }

    public e P1(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        JsonToken T0;
        if (jsonParser.v() != JsonToken.FIELD_NAME.id()) {
            O1(jsonParser);
            return this;
        }
        i1();
        do {
            O1(jsonParser);
            T0 = jsonParser.T0();
        } while (T0 == JsonToken.FIELD_NAME);
        JsonToken jsonToken = JsonToken.END_OBJECT;
        if (T0 != jsonToken) {
            deserializationContext.reportWrongTokenException(jsonParser, jsonToken, "Expected END_OBJECT after copying contents of a JsonParser into TokenBuffer, got " + T0, new Object[0]);
        }
        f0();
        return this;
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public int Q(Base64Variant base64Variant, InputStream inputStream, int i) {
        throw new UnsupportedOperationException();
    }

    public JsonToken Q1() {
        c cVar = this.l0;
        if (cVar != null) {
            return cVar.q(0);
        }
        return null;
    }

    public e R1(boolean z) {
        this.k0 = z;
        return this;
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void S(Base64Variant base64Variant, byte[] bArr, int i, int i2) throws IOException {
        byte[] bArr2 = new byte[i2];
        System.arraycopy(bArr, i, bArr2, 0, i2);
        J0(bArr2);
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    /* renamed from: S1 */
    public final mw1 l() {
        return this.r0;
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void T0(char c2) throws IOException {
        I1();
    }

    public void T1(JsonGenerator jsonGenerator) throws IOException {
        c cVar = this.l0;
        boolean z = this.j0;
        boolean z2 = z && cVar.k();
        int i = -1;
        while (true) {
            i++;
            if (i >= 16) {
                cVar = cVar.l();
                if (cVar == null) {
                    return;
                }
                z2 = z && cVar.k();
                i = 0;
            }
            JsonToken q = cVar.q(i);
            if (q == null) {
                return;
            }
            if (z2) {
                Object h = cVar.h(i);
                if (h != null) {
                    jsonGenerator.L0(h);
                }
                Object i2 = cVar.i(i);
                if (i2 != null) {
                    jsonGenerator.s1(i2);
                }
            }
            switch (a.a[q.ordinal()]) {
                case 1:
                    jsonGenerator.i1();
                    break;
                case 2:
                    jsonGenerator.f0();
                    break;
                case 3:
                    jsonGenerator.e1();
                    break;
                case 4:
                    jsonGenerator.e0();
                    break;
                case 5:
                    Object j = cVar.j(i);
                    if (j instanceof yl3) {
                        jsonGenerator.g0((yl3) j);
                        break;
                    } else {
                        jsonGenerator.i0((String) j);
                        break;
                    }
                case 6:
                    Object j2 = cVar.j(i);
                    if (j2 instanceof yl3) {
                        jsonGenerator.m1((yl3) j2);
                        break;
                    } else {
                        jsonGenerator.o1((String) j2);
                        break;
                    }
                case 7:
                    Object j3 = cVar.j(i);
                    if (j3 instanceof Integer) {
                        jsonGenerator.x0(((Integer) j3).intValue());
                        break;
                    } else if (j3 instanceof BigInteger) {
                        jsonGenerator.F0((BigInteger) j3);
                        break;
                    } else if (j3 instanceof Long) {
                        jsonGenerator.y0(((Long) j3).longValue());
                        break;
                    } else if (j3 instanceof Short) {
                        jsonGenerator.H0(((Short) j3).shortValue());
                        break;
                    } else {
                        jsonGenerator.x0(((Number) j3).intValue());
                        break;
                    }
                case 8:
                    Object j4 = cVar.j(i);
                    if (j4 instanceof Double) {
                        jsonGenerator.r0(((Double) j4).doubleValue());
                        break;
                    } else if (j4 instanceof BigDecimal) {
                        jsonGenerator.B0((BigDecimal) j4);
                        break;
                    } else if (j4 instanceof Float) {
                        jsonGenerator.w0(((Float) j4).floatValue());
                        break;
                    } else if (j4 == null) {
                        jsonGenerator.m0();
                        break;
                    } else if (j4 instanceof String) {
                        jsonGenerator.z0((String) j4);
                        break;
                    } else {
                        throw new JsonGenerationException(String.format("Unrecognized value type for VALUE_NUMBER_FLOAT: %s, can not serialize", j4.getClass().getName()), jsonGenerator);
                    }
                case 9:
                    jsonGenerator.a0(true);
                    break;
                case 10:
                    jsonGenerator.a0(false);
                    break;
                case 11:
                    jsonGenerator.m0();
                    break;
                case 12:
                    Object j5 = cVar.j(i);
                    if (j5 instanceof d) {
                        ((d) j5).b(jsonGenerator);
                        break;
                    } else if (j5 instanceof com.fasterxml.jackson.databind.e) {
                        jsonGenerator.J0(j5);
                        break;
                    } else {
                        jsonGenerator.b0(j5);
                        break;
                    }
                default:
                    throw new RuntimeException("Internal error: should never end up through this code path");
            }
        }
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void U0(yl3 yl3Var) throws IOException {
        I1();
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void W0(String str) throws IOException {
        I1();
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void X0(char[] cArr, int i, int i2) throws IOException {
        I1();
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void a0(boolean z) throws IOException {
        z1(z ? JsonToken.VALUE_TRUE : JsonToken.VALUE_FALSE);
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void b0(Object obj) throws IOException {
        F1(JsonToken.VALUE_EMBEDDED_OBJECT, obj);
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void c1(String str) throws IOException {
        F1(JsonToken.VALUE_EMBEDDED_OBJECT, new d(str));
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator, java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public final void e0() throws IOException {
        v1(JsonToken.END_ARRAY);
        mw1 d = this.r0.d();
        if (d != null) {
            this.r0 = d;
        }
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public final void e1() throws IOException {
        this.r0.u();
        v1(JsonToken.START_ARRAY);
        this.r0 = this.r0.m();
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public boolean f() {
        return true;
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public final void f0() throws IOException {
        v1(JsonToken.END_OBJECT);
        mw1 d = this.r0.d();
        if (d != null) {
            this.r0 = d;
        }
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator, java.io.Flushable
    public void flush() throws IOException {
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public boolean g() {
        return this.i0;
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void g0(yl3 yl3Var) throws IOException {
        this.r0.t(yl3Var.getValue());
        w1(JsonToken.FIELD_NAME, yl3Var);
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public boolean h() {
        return this.h0;
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public JsonGenerator i(JsonGenerator.Feature feature) {
        this.g0 = (~feature.getMask()) & this.g0;
        return this;
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public final void i0(String str) throws IOException {
        this.r0.t(str);
        w1(JsonToken.FIELD_NAME, str);
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public final void i1() throws IOException {
        this.r0.u();
        v1(JsonToken.START_OBJECT);
        this.r0 = this.r0.n();
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public int j() {
        return this.g0;
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void k1(Object obj) throws IOException {
        this.r0.u();
        v1(JsonToken.START_OBJECT);
        mw1 n = this.r0.n();
        this.r0 = n;
        if (obj != null) {
            n.h(obj);
        }
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void m0() throws IOException {
        z1(JsonToken.VALUE_NULL);
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void m1(yl3 yl3Var) throws IOException {
        if (yl3Var == null) {
            m0();
        } else {
            F1(JsonToken.VALUE_STRING, yl3Var);
        }
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void o1(String str) throws IOException {
        if (str == null) {
            m0();
        } else {
            F1(JsonToken.VALUE_STRING, str);
        }
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void p1(char[] cArr, int i, int i2) throws IOException {
        o1(new String(cArr, i, i2));
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public JsonGenerator q(int i, int i2) {
        this.g0 = (i & i2) | (j() & (~i2));
        return this;
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void r0(double d) throws IOException {
        F1(JsonToken.VALUE_NUMBER_FLOAT, Double.valueOf(d));
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void s1(Object obj) {
        this.o0 = obj;
        this.q0 = true;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[TokenBuffer: ");
        JsonParser K1 = K1();
        int i = 0;
        boolean z = this.h0 || this.i0;
        while (true) {
            try {
                JsonToken T0 = K1.T0();
                if (T0 == null) {
                    break;
                }
                if (z) {
                    y1(sb);
                }
                if (i < 100) {
                    if (i > 0) {
                        sb.append(", ");
                    }
                    sb.append(T0.toString());
                    if (T0 == JsonToken.FIELD_NAME) {
                        sb.append('(');
                        sb.append(K1.r());
                        sb.append(')');
                    }
                }
                i++;
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }
        if (i >= 100) {
            sb.append(" ... (truncated ");
            sb.append(i - 100);
            sb.append(" entries)");
        }
        sb.append(']');
        return sb.toString();
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    @Deprecated
    public JsonGenerator v(int i) {
        this.g0 = i;
        return this;
    }

    public final void v1(JsonToken jsonToken) {
        c c2;
        if (this.q0) {
            c2 = this.m0.e(this.n0, jsonToken, this.p0, this.o0);
        } else {
            c2 = this.m0.c(this.n0, jsonToken);
        }
        if (c2 == null) {
            this.n0++;
            return;
        }
        this.m0 = c2;
        this.n0 = 1;
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void w0(float f) throws IOException {
        F1(JsonToken.VALUE_NUMBER_FLOAT, Float.valueOf(f));
    }

    public final void w1(JsonToken jsonToken, Object obj) {
        c d;
        if (this.q0) {
            d = this.m0.f(this.n0, jsonToken, obj, this.p0, this.o0);
        } else {
            d = this.m0.d(this.n0, jsonToken, obj);
        }
        if (d == null) {
            this.n0++;
            return;
        }
        this.m0 = d;
        this.n0 = 1;
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void x0(int i) throws IOException {
        F1(JsonToken.VALUE_NUMBER_INT, Integer.valueOf(i));
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void y0(long j) throws IOException {
        F1(JsonToken.VALUE_NUMBER_INT, Long.valueOf(j));
    }

    public final void y1(StringBuilder sb) {
        Object h = this.m0.h(this.n0 - 1);
        if (h != null) {
            sb.append("[objectId=");
            sb.append(String.valueOf(h));
            sb.append(']');
        }
        Object i = this.m0.i(this.n0 - 1);
        if (i != null) {
            sb.append("[typeId=");
            sb.append(String.valueOf(i));
            sb.append(']');
        }
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void z0(String str) throws IOException {
        F1(JsonToken.VALUE_NUMBER_FLOAT, str);
    }

    public final void z1(JsonToken jsonToken) {
        c c2;
        this.r0.u();
        if (this.q0) {
            c2 = this.m0.e(this.n0, jsonToken, this.p0, this.o0);
        } else {
            c2 = this.m0.c(this.n0, jsonToken);
        }
        if (c2 == null) {
            this.n0++;
            return;
        }
        this.m0 = c2;
        this.n0 = 1;
    }

    public e(JsonParser jsonParser) {
        this(jsonParser, (DeserializationContext) null);
    }

    public e(JsonParser jsonParser, DeserializationContext deserializationContext) {
        this.q0 = false;
        this.f0 = jsonParser.n();
        this.g0 = s0;
        this.r0 = mw1.o(null);
        c cVar = new c();
        this.m0 = cVar;
        this.l0 = cVar;
        this.n0 = 0;
        this.h0 = jsonParser.d();
        boolean c2 = jsonParser.c();
        this.i0 = c2;
        this.j0 = c2 | this.h0;
        this.k0 = deserializationContext != null ? deserializationContext.isEnabled(DeserializationFeature.USE_BIG_DECIMAL_FOR_FLOATS) : false;
    }
}
