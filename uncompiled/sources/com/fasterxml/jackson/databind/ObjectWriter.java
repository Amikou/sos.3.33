package com.fasterxml.jackson.databind;

import com.fasterxml.jackson.core.Base64Variant;
import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.core.io.CharacterEscapes;
import com.fasterxml.jackson.core.io.SerializedString;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import com.fasterxml.jackson.databind.cfg.ContextAttributes;
import com.fasterxml.jackson.databind.ser.DefaultSerializerProvider;
import com.fasterxml.jackson.databind.type.TypeFactory;
import java.io.Closeable;
import java.io.DataOutput;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.io.Writer;
import java.text.DateFormat;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicReference;

/* loaded from: classes.dex */
public class ObjectWriter implements Serializable {
    public static final com.fasterxml.jackson.core.d NULL_PRETTY_PRINTER = new MinimalPrettyPrinter();
    private static final long serialVersionUID = 1;
    public final SerializationConfig _config;
    public final JsonFactory _generatorFactory;
    public final GeneratorSettings _generatorSettings;
    public final Prefetch _prefetch;
    public final com.fasterxml.jackson.databind.ser.g _serializerFactory;
    public final DefaultSerializerProvider _serializerProvider;

    /* loaded from: classes.dex */
    public static final class Prefetch implements Serializable {
        public static final Prefetch empty = new Prefetch(null, null, null);
        private static final long serialVersionUID = 1;
        private final JavaType rootType;
        private final com.fasterxml.jackson.databind.jsontype.c typeSerializer;
        private final f<Object> valueSerializer;

        public Prefetch(JavaType javaType, f<Object> fVar, com.fasterxml.jackson.databind.jsontype.c cVar) {
            this.rootType = javaType;
            this.valueSerializer = fVar;
            this.typeSerializer = cVar;
        }

        public Prefetch forRootType(ObjectWriter objectWriter, JavaType javaType) {
            if (javaType == null || javaType.isJavaLangObject()) {
                return (this.rootType == null || this.valueSerializer == null) ? this : new Prefetch(null, null, this.typeSerializer);
            } else if (javaType.equals(this.rootType)) {
                return this;
            } else {
                if (objectWriter.isEnabled(SerializationFeature.EAGER_SERIALIZER_FETCH)) {
                    try {
                        f<Object> findTypedValueSerializer = objectWriter._serializerProvider().findTypedValueSerializer(javaType, true, (a) null);
                        if (findTypedValueSerializer instanceof com.fasterxml.jackson.databind.ser.impl.b) {
                            return new Prefetch(javaType, null, ((com.fasterxml.jackson.databind.ser.impl.b) findTypedValueSerializer).a());
                        }
                        return new Prefetch(javaType, findTypedValueSerializer, null);
                    } catch (JsonProcessingException unused) {
                    }
                }
                return new Prefetch(javaType, null, this.typeSerializer);
            }
        }

        public final com.fasterxml.jackson.databind.jsontype.c getTypeSerializer() {
            return this.typeSerializer;
        }

        public final f<Object> getValueSerializer() {
            return this.valueSerializer;
        }

        public boolean hasSerializer() {
            return (this.valueSerializer == null && this.typeSerializer == null) ? false : true;
        }

        public void serialize(JsonGenerator jsonGenerator, Object obj, DefaultSerializerProvider defaultSerializerProvider) throws IOException {
            com.fasterxml.jackson.databind.jsontype.c cVar = this.typeSerializer;
            if (cVar != null) {
                defaultSerializerProvider.serializePolymorphic(jsonGenerator, obj, this.rootType, this.valueSerializer, cVar);
                return;
            }
            f<Object> fVar = this.valueSerializer;
            if (fVar != null) {
                defaultSerializerProvider.serializeValue(jsonGenerator, obj, this.rootType, fVar);
                return;
            }
            JavaType javaType = this.rootType;
            if (javaType != null) {
                defaultSerializerProvider.serializeValue(jsonGenerator, obj, javaType);
            } else {
                defaultSerializerProvider.serializeValue(jsonGenerator, obj);
            }
        }
    }

    public ObjectWriter(ObjectMapper objectMapper, SerializationConfig serializationConfig, JavaType javaType, com.fasterxml.jackson.core.d dVar) {
        this._config = serializationConfig;
        this._serializerProvider = objectMapper._serializerProvider;
        this._serializerFactory = objectMapper._serializerFactory;
        this._generatorFactory = objectMapper._jsonFactory;
        this._generatorSettings = dVar == null ? GeneratorSettings.empty : new GeneratorSettings(dVar, null, null, null);
        if (javaType != null && !javaType.hasRawClass(Object.class)) {
            this._prefetch = Prefetch.empty.forRootType(this, javaType.withStaticTyping());
        } else {
            this._prefetch = Prefetch.empty;
        }
    }

    public final void _configAndWriteValue(JsonGenerator jsonGenerator, Object obj) throws IOException {
        _configureGenerator(jsonGenerator);
        if (this._config.isEnabled(SerializationFeature.CLOSE_CLOSEABLE) && (obj instanceof Closeable)) {
            a(jsonGenerator, obj);
            return;
        }
        try {
            this._prefetch.serialize(jsonGenerator, obj, _serializerProvider());
            jsonGenerator.close();
        } catch (Exception e) {
            com.fasterxml.jackson.databind.util.c.h(jsonGenerator, e);
        }
    }

    public final void _configureGenerator(JsonGenerator jsonGenerator) {
        this._config.initialize(jsonGenerator);
        this._generatorSettings.initialize(jsonGenerator);
    }

    public ObjectWriter _new(ObjectWriter objectWriter, JsonFactory jsonFactory) {
        return new ObjectWriter(objectWriter, jsonFactory);
    }

    public j _newSequenceWriter(boolean z, JsonGenerator jsonGenerator, boolean z2) throws IOException {
        _configureGenerator(jsonGenerator);
        return new j(_serializerProvider(), jsonGenerator, z2, this._prefetch).a(z);
    }

    public DefaultSerializerProvider _serializerProvider() {
        return this._serializerProvider.createInstance(this._config, this._serializerFactory);
    }

    public void _verifySchemaType(z81 z81Var) {
        if (z81Var == null || this._generatorFactory.canUseSchema(z81Var)) {
            return;
        }
        throw new IllegalArgumentException("Can not use FormatSchema of type " + z81Var.getClass().getName() + " for format " + this._generatorFactory.getFormatName());
    }

    public final void a(JsonGenerator jsonGenerator, Object obj) throws IOException {
        Exception e;
        Closeable closeable;
        Closeable closeable2 = (Closeable) obj;
        try {
            this._prefetch.serialize(jsonGenerator, obj, _serializerProvider());
            closeable = null;
            try {
                closeable2.close();
                jsonGenerator.close();
            } catch (Exception e2) {
                e = e2;
                com.fasterxml.jackson.databind.util.c.g(jsonGenerator, closeable, e);
            }
        } catch (Exception e3) {
            e = e3;
            closeable = closeable2;
        }
    }

    public void acceptJsonFormatVisitor(JavaType javaType, com.fasterxml.jackson.databind.jsonFormatVisitors.b bVar) throws JsonMappingException {
        if (javaType != null) {
            _serializerProvider().acceptJsonFormatVisitor(javaType, bVar);
            return;
        }
        throw new IllegalArgumentException("type must be provided");
    }

    public boolean canSerialize(Class<?> cls) {
        return _serializerProvider().hasSerializerFor(cls, null);
    }

    public ObjectWriter forType(JavaType javaType) {
        Prefetch forRootType = this._prefetch.forRootType(this, javaType);
        return forRootType == this._prefetch ? this : _new(this._generatorSettings, forRootType);
    }

    public ContextAttributes getAttributes() {
        return this._config.getAttributes();
    }

    public SerializationConfig getConfig() {
        return this._config;
    }

    public JsonFactory getFactory() {
        return this._generatorFactory;
    }

    public TypeFactory getTypeFactory() {
        return this._config.getTypeFactory();
    }

    public boolean hasPrefetchedSerializer() {
        return this._prefetch.hasSerializer();
    }

    public boolean isEnabled(SerializationFeature serializationFeature) {
        return this._config.isEnabled(serializationFeature);
    }

    public Version version() {
        return wo2.a;
    }

    public ObjectWriter with(SerializationFeature serializationFeature) {
        SerializationConfig with = this._config.with(serializationFeature);
        return with == this._config ? this : _new(this, with);
    }

    public ObjectWriter withAttribute(Object obj, Object obj2) {
        SerializationConfig withAttribute = this._config.withAttribute(obj, obj2);
        return withAttribute == this._config ? this : _new(this, withAttribute);
    }

    public ObjectWriter withAttributes(Map<?, ?> map) {
        SerializationConfig withAttributes = this._config.withAttributes(map);
        return withAttributes == this._config ? this : _new(this, withAttributes);
    }

    public ObjectWriter withDefaultPrettyPrinter() {
        return with(this._config.getDefaultPrettyPrinter());
    }

    public ObjectWriter withFeatures(SerializationFeature... serializationFeatureArr) {
        SerializationConfig withFeatures = this._config.withFeatures(serializationFeatureArr);
        return withFeatures == this._config ? this : _new(this, withFeatures);
    }

    public ObjectWriter withRootName(String str) {
        SerializationConfig withRootName = this._config.withRootName(str);
        return withRootName == this._config ? this : _new(this, withRootName);
    }

    public ObjectWriter withRootValueSeparator(String str) {
        GeneratorSettings withRootValueSeparator = this._generatorSettings.withRootValueSeparator(str);
        return withRootValueSeparator == this._generatorSettings ? this : _new(withRootValueSeparator, this._prefetch);
    }

    @Deprecated
    public ObjectWriter withSchema(z81 z81Var) {
        return with(z81Var);
    }

    @Deprecated
    public ObjectWriter withType(JavaType javaType) {
        return forType(javaType);
    }

    public ObjectWriter withView(Class<?> cls) {
        SerializationConfig withView = this._config.withView(cls);
        return withView == this._config ? this : _new(this, withView);
    }

    public ObjectWriter without(SerializationFeature serializationFeature) {
        SerializationConfig without = this._config.without(serializationFeature);
        return without == this._config ? this : _new(this, without);
    }

    public ObjectWriter withoutAttribute(Object obj) {
        SerializationConfig withoutAttribute = this._config.withoutAttribute(obj);
        return withoutAttribute == this._config ? this : _new(this, withoutAttribute);
    }

    public ObjectWriter withoutFeatures(SerializationFeature... serializationFeatureArr) {
        SerializationConfig withoutFeatures = this._config.withoutFeatures(serializationFeatureArr);
        return withoutFeatures == this._config ? this : _new(this, withoutFeatures);
    }

    public ObjectWriter withoutRootName() {
        SerializationConfig withRootName = this._config.withRootName(PropertyName.NO_NAME);
        return withRootName == this._config ? this : _new(this, withRootName);
    }

    public void writeValue(JsonGenerator jsonGenerator, Object obj) throws IOException {
        _configureGenerator(jsonGenerator);
        if (this._config.isEnabled(SerializationFeature.CLOSE_CLOSEABLE) && (obj instanceof Closeable)) {
            Closeable closeable = (Closeable) obj;
            try {
                this._prefetch.serialize(jsonGenerator, obj, _serializerProvider());
                if (this._config.isEnabled(SerializationFeature.FLUSH_AFTER_WRITE_VALUE)) {
                    jsonGenerator.flush();
                }
                closeable.close();
                return;
            } catch (Exception e) {
                com.fasterxml.jackson.databind.util.c.g(null, closeable, e);
                return;
            }
        }
        this._prefetch.serialize(jsonGenerator, obj, _serializerProvider());
        if (this._config.isEnabled(SerializationFeature.FLUSH_AFTER_WRITE_VALUE)) {
            jsonGenerator.flush();
        }
    }

    public byte[] writeValueAsBytes(Object obj) throws JsonProcessingException {
        ms msVar = new ms(this._generatorFactory._getBufferRecycler());
        try {
            _configAndWriteValue(this._generatorFactory.createGenerator(msVar, JsonEncoding.UTF8), obj);
            byte[] n = msVar.n();
            msVar.i();
            return n;
        } catch (JsonProcessingException e) {
            throw e;
        } catch (IOException e2) {
            throw JsonMappingException.fromUnexpectedIOE(e2);
        }
    }

    public String writeValueAsString(Object obj) throws JsonProcessingException {
        com.fasterxml.jackson.core.io.e eVar = new com.fasterxml.jackson.core.io.e(this._generatorFactory._getBufferRecycler());
        try {
            _configAndWriteValue(this._generatorFactory.createGenerator(eVar), obj);
            return eVar.a();
        } catch (JsonProcessingException e) {
            throw e;
        } catch (IOException e2) {
            throw JsonMappingException.fromUnexpectedIOE(e2);
        }
    }

    public j writeValues(File file) throws IOException {
        return _newSequenceWriter(false, this._generatorFactory.createGenerator(file, JsonEncoding.UTF8), true);
    }

    public j writeValuesAsArray(File file) throws IOException {
        return _newSequenceWriter(true, this._generatorFactory.createGenerator(file, JsonEncoding.UTF8), true);
    }

    /* loaded from: classes.dex */
    public static final class GeneratorSettings implements Serializable {
        public static final GeneratorSettings empty = new GeneratorSettings(null, null, null, null);
        private static final long serialVersionUID = 1;
        public final CharacterEscapes characterEscapes;
        public final com.fasterxml.jackson.core.d prettyPrinter;
        public final yl3 rootValueSeparator;
        public final z81 schema;

        public GeneratorSettings(com.fasterxml.jackson.core.d dVar, z81 z81Var, CharacterEscapes characterEscapes, yl3 yl3Var) {
            this.prettyPrinter = dVar;
            this.schema = z81Var;
            this.characterEscapes = characterEscapes;
            this.rootValueSeparator = yl3Var;
        }

        public void initialize(JsonGenerator jsonGenerator) {
            com.fasterxml.jackson.core.d dVar = this.prettyPrinter;
            if (dVar != null) {
                if (dVar == ObjectWriter.NULL_PRETTY_PRINTER) {
                    jsonGenerator.x(null);
                } else {
                    if (dVar instanceof ir1) {
                        dVar = (com.fasterxml.jackson.core.d) ((ir1) dVar).createInstance();
                    }
                    jsonGenerator.x(dVar);
                }
            }
            CharacterEscapes characterEscapes = this.characterEscapes;
            if (characterEscapes != null) {
                jsonGenerator.r(characterEscapes);
            }
            z81 z81Var = this.schema;
            if (z81Var != null) {
                jsonGenerator.A(z81Var);
            }
            yl3 yl3Var = this.rootValueSeparator;
            if (yl3Var != null) {
                jsonGenerator.z(yl3Var);
            }
        }

        public GeneratorSettings with(com.fasterxml.jackson.core.d dVar) {
            if (dVar == null) {
                dVar = ObjectWriter.NULL_PRETTY_PRINTER;
            }
            return dVar == this.prettyPrinter ? this : new GeneratorSettings(dVar, this.schema, this.characterEscapes, this.rootValueSeparator);
        }

        public GeneratorSettings withRootValueSeparator(String str) {
            if (str == null) {
                if (this.rootValueSeparator == null) {
                    return this;
                }
            } else if (str.equals(this.rootValueSeparator)) {
                return this;
            }
            return new GeneratorSettings(this.prettyPrinter, this.schema, this.characterEscapes, str == null ? null : new SerializedString(str));
        }

        public GeneratorSettings with(z81 z81Var) {
            return this.schema == z81Var ? this : new GeneratorSettings(this.prettyPrinter, z81Var, this.characterEscapes, this.rootValueSeparator);
        }

        public GeneratorSettings with(CharacterEscapes characterEscapes) {
            return this.characterEscapes == characterEscapes ? this : new GeneratorSettings(this.prettyPrinter, this.schema, characterEscapes, this.rootValueSeparator);
        }

        public GeneratorSettings withRootValueSeparator(yl3 yl3Var) {
            if (yl3Var == null) {
                if (this.rootValueSeparator == null) {
                    return this;
                }
            } else if (this.rootValueSeparator != null && yl3Var.getValue().equals(this.rootValueSeparator.getValue())) {
                return this;
            }
            return new GeneratorSettings(this.prettyPrinter, this.schema, this.characterEscapes, yl3Var);
        }
    }

    public ObjectWriter _new(ObjectWriter objectWriter, SerializationConfig serializationConfig) {
        return new ObjectWriter(objectWriter, serializationConfig);
    }

    public boolean canSerialize(Class<?> cls, AtomicReference<Throwable> atomicReference) {
        return _serializerProvider().hasSerializerFor(cls, atomicReference);
    }

    public boolean isEnabled(MapperFeature mapperFeature) {
        return this._config.isEnabled(mapperFeature);
    }

    @Deprecated
    public ObjectWriter withType(Class<?> cls) {
        return forType(cls);
    }

    public ObjectWriter _new(GeneratorSettings generatorSettings, Prefetch prefetch) {
        return new ObjectWriter(this, this._config, generatorSettings, prefetch);
    }

    public void acceptJsonFormatVisitor(Class<?> cls, com.fasterxml.jackson.databind.jsonFormatVisitors.b bVar) throws JsonMappingException {
        acceptJsonFormatVisitor(this._config.constructType(cls), bVar);
    }

    public ObjectWriter forType(Class<?> cls) {
        if (cls == Object.class) {
            return forType((JavaType) null);
        }
        return forType(this._config.constructType(cls));
    }

    @Deprecated
    public boolean isEnabled(JsonParser.Feature feature) {
        return this._generatorFactory.isEnabled(feature);
    }

    public ObjectWriter with(SerializationFeature serializationFeature, SerializationFeature... serializationFeatureArr) {
        SerializationConfig with = this._config.with(serializationFeature, serializationFeatureArr);
        return with == this._config ? this : _new(this, with);
    }

    public ObjectWriter withFeatures(JsonGenerator.Feature... featureArr) {
        SerializationConfig withFeatures = this._config.withFeatures(featureArr);
        return withFeatures == this._config ? this : _new(this, withFeatures);
    }

    public ObjectWriter withRootName(PropertyName propertyName) {
        SerializationConfig withRootName = this._config.withRootName(propertyName);
        return withRootName == this._config ? this : _new(this, withRootName);
    }

    @Deprecated
    public ObjectWriter withType(ud4<?> ud4Var) {
        return forType(ud4Var);
    }

    public ObjectWriter without(SerializationFeature serializationFeature, SerializationFeature... serializationFeatureArr) {
        SerializationConfig without = this._config.without(serializationFeature, serializationFeatureArr);
        return without == this._config ? this : _new(this, without);
    }

    public ObjectWriter withoutFeatures(JsonGenerator.Feature... featureArr) {
        SerializationConfig withoutFeatures = this._config.withoutFeatures(featureArr);
        return withoutFeatures == this._config ? this : _new(this, withoutFeatures);
    }

    public boolean isEnabled(JsonGenerator.Feature feature) {
        return this._generatorFactory.isEnabled(feature);
    }

    public ObjectWriter withRootValueSeparator(yl3 yl3Var) {
        GeneratorSettings withRootValueSeparator = this._generatorSettings.withRootValueSeparator(yl3Var);
        return withRootValueSeparator == this._generatorSettings ? this : _new(withRootValueSeparator, this._prefetch);
    }

    public j writeValues(JsonGenerator jsonGenerator) throws IOException {
        _configureGenerator(jsonGenerator);
        return _newSequenceWriter(false, jsonGenerator, false);
    }

    public j writeValuesAsArray(JsonGenerator jsonGenerator) throws IOException {
        return _newSequenceWriter(true, jsonGenerator, false);
    }

    public ObjectWriter with(JsonGenerator.Feature feature) {
        SerializationConfig with = this._config.with(feature);
        return with == this._config ? this : _new(this, with);
    }

    public ObjectWriter withFeatures(x81... x81VarArr) {
        SerializationConfig withFeatures = this._config.withFeatures(x81VarArr);
        return withFeatures == this._config ? this : _new(this, withFeatures);
    }

    public ObjectWriter without(JsonGenerator.Feature feature) {
        SerializationConfig without = this._config.without(feature);
        return without == this._config ? this : _new(this, without);
    }

    public ObjectWriter withoutFeatures(x81... x81VarArr) {
        SerializationConfig withoutFeatures = this._config.withoutFeatures(x81VarArr);
        return withoutFeatures == this._config ? this : _new(this, withoutFeatures);
    }

    public j writeValuesAsArray(Writer writer) throws IOException {
        return _newSequenceWriter(true, this._generatorFactory.createGenerator(writer), true);
    }

    public ObjectWriter forType(ud4<?> ud4Var) {
        return forType(this._config.getTypeFactory().constructType(ud4Var.getType()));
    }

    public j writeValues(Writer writer) throws IOException {
        return _newSequenceWriter(false, this._generatorFactory.createGenerator(writer), true);
    }

    public j writeValuesAsArray(OutputStream outputStream) throws IOException {
        return _newSequenceWriter(true, this._generatorFactory.createGenerator(outputStream, JsonEncoding.UTF8), true);
    }

    public ObjectWriter with(x81 x81Var) {
        SerializationConfig with = this._config.with(x81Var);
        return with == this._config ? this : _new(this, with);
    }

    public ObjectWriter without(x81 x81Var) {
        SerializationConfig without = this._config.without(x81Var);
        return without == this._config ? this : _new(this, without);
    }

    public ObjectWriter with(DateFormat dateFormat) {
        SerializationConfig with = this._config.with(dateFormat);
        return with == this._config ? this : _new(this, with);
    }

    public j writeValues(OutputStream outputStream) throws IOException {
        return _newSequenceWriter(false, this._generatorFactory.createGenerator(outputStream, JsonEncoding.UTF8), true);
    }

    public j writeValuesAsArray(DataOutput dataOutput) throws IOException {
        return _newSequenceWriter(true, this._generatorFactory.createGenerator(dataOutput), true);
    }

    public ObjectWriter(ObjectMapper objectMapper, SerializationConfig serializationConfig) {
        this._config = serializationConfig;
        this._serializerProvider = objectMapper._serializerProvider;
        this._serializerFactory = objectMapper._serializerFactory;
        this._generatorFactory = objectMapper._jsonFactory;
        this._generatorSettings = GeneratorSettings.empty;
        this._prefetch = Prefetch.empty;
    }

    public ObjectWriter with(j41 j41Var) {
        return j41Var == this._config.getFilterProvider() ? this : _new(this, this._config.withFilters(j41Var));
    }

    public void writeValue(File file, Object obj) throws IOException, JsonGenerationException, JsonMappingException {
        _configAndWriteValue(this._generatorFactory.createGenerator(file, JsonEncoding.UTF8), obj);
    }

    public j writeValues(DataOutput dataOutput) throws IOException {
        return _newSequenceWriter(false, this._generatorFactory.createGenerator(dataOutput), true);
    }

    public ObjectWriter with(com.fasterxml.jackson.core.d dVar) {
        GeneratorSettings with = this._generatorSettings.with(dVar);
        return with == this._generatorSettings ? this : _new(with, this._prefetch);
    }

    public void writeValue(OutputStream outputStream, Object obj) throws IOException, JsonGenerationException, JsonMappingException {
        _configAndWriteValue(this._generatorFactory.createGenerator(outputStream, JsonEncoding.UTF8), obj);
    }

    public void writeValue(Writer writer, Object obj) throws IOException, JsonGenerationException, JsonMappingException {
        _configAndWriteValue(this._generatorFactory.createGenerator(writer), obj);
    }

    public void writeValue(DataOutput dataOutput, Object obj) throws IOException {
        _configAndWriteValue(this._generatorFactory.createGenerator(dataOutput), obj);
    }

    public ObjectWriter with(z81 z81Var) {
        GeneratorSettings with = this._generatorSettings.with(z81Var);
        if (with == this._generatorSettings) {
            return this;
        }
        _verifySchemaType(z81Var);
        return _new(with, this._prefetch);
    }

    public ObjectWriter(ObjectMapper objectMapper, SerializationConfig serializationConfig, z81 z81Var) {
        this._config = serializationConfig;
        this._serializerProvider = objectMapper._serializerProvider;
        this._serializerFactory = objectMapper._serializerFactory;
        this._generatorFactory = objectMapper._jsonFactory;
        this._generatorSettings = z81Var == null ? GeneratorSettings.empty : new GeneratorSettings(null, z81Var, null, null);
        this._prefetch = Prefetch.empty;
    }

    public ObjectWriter with(Locale locale) {
        SerializationConfig with = this._config.with(locale);
        return with == this._config ? this : _new(this, with);
    }

    public ObjectWriter with(TimeZone timeZone) {
        SerializationConfig with = this._config.with(timeZone);
        return with == this._config ? this : _new(this, with);
    }

    public ObjectWriter with(Base64Variant base64Variant) {
        SerializationConfig with = this._config.with(base64Variant);
        return with == this._config ? this : _new(this, with);
    }

    public ObjectWriter(ObjectWriter objectWriter, SerializationConfig serializationConfig, GeneratorSettings generatorSettings, Prefetch prefetch) {
        this._config = serializationConfig;
        this._serializerProvider = objectWriter._serializerProvider;
        this._serializerFactory = objectWriter._serializerFactory;
        this._generatorFactory = objectWriter._generatorFactory;
        this._generatorSettings = generatorSettings;
        this._prefetch = prefetch;
    }

    public ObjectWriter with(CharacterEscapes characterEscapes) {
        GeneratorSettings with = this._generatorSettings.with(characterEscapes);
        return with == this._generatorSettings ? this : _new(with, this._prefetch);
    }

    public ObjectWriter with(JsonFactory jsonFactory) {
        return jsonFactory == this._generatorFactory ? this : _new(this, jsonFactory);
    }

    public ObjectWriter with(ContextAttributes contextAttributes) {
        SerializationConfig with = this._config.with(contextAttributes);
        return with == this._config ? this : _new(this, with);
    }

    public ObjectWriter(ObjectWriter objectWriter, SerializationConfig serializationConfig) {
        this._config = serializationConfig;
        this._serializerProvider = objectWriter._serializerProvider;
        this._serializerFactory = objectWriter._serializerFactory;
        this._generatorFactory = objectWriter._generatorFactory;
        this._generatorSettings = objectWriter._generatorSettings;
        this._prefetch = objectWriter._prefetch;
    }

    public ObjectWriter(ObjectWriter objectWriter, JsonFactory jsonFactory) {
        this._config = objectWriter._config.with(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, jsonFactory.requiresPropertyOrdering());
        this._serializerProvider = objectWriter._serializerProvider;
        this._serializerFactory = objectWriter._serializerFactory;
        this._generatorFactory = jsonFactory;
        this._generatorSettings = objectWriter._generatorSettings;
        this._prefetch = objectWriter._prefetch;
    }
}
