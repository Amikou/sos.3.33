package com.fasterxml.jackson.databind.jsonFormatVisitors;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;

/* compiled from: JsonArrayFormatVisitor.java */
/* loaded from: classes.dex */
public interface a extends av1 {
    void i(JsonFormatTypes jsonFormatTypes) throws JsonMappingException;

    void n(zu1 zu1Var, JavaType javaType) throws JsonMappingException;
}
