package com.fasterxml.jackson.databind.jsonFormatVisitors;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.r;
import java.util.HashMap;
import java.util.Map;

/* loaded from: classes.dex */
public enum JsonFormatTypes {
    STRING,
    NUMBER,
    INTEGER,
    BOOLEAN,
    OBJECT,
    ARRAY,
    NULL,
    ANY;
    
    public static final Map<String, JsonFormatTypes> a = new HashMap();

    static {
        JsonFormatTypes[] values;
        for (JsonFormatTypes jsonFormatTypes : values()) {
            a.put(jsonFormatTypes.name().toLowerCase(), jsonFormatTypes);
        }
    }

    @JsonCreator
    public static JsonFormatTypes forValue(String str) {
        return a.get(str);
    }

    @r
    public String value() {
        return name().toLowerCase();
    }
}
