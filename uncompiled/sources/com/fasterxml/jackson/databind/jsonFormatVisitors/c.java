package com.fasterxml.jackson.databind.jsonFormatVisitors;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;

/* compiled from: JsonMapFormatVisitor.java */
/* loaded from: classes.dex */
public interface c extends av1 {
    void a(zu1 zu1Var, JavaType javaType) throws JsonMappingException;

    void k(zu1 zu1Var, JavaType javaType) throws JsonMappingException;
}
