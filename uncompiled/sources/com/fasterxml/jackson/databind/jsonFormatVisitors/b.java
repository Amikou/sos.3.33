package com.fasterxml.jackson.databind.jsonFormatVisitors;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.k;

/* compiled from: JsonFormatVisitorWrapper.java */
/* loaded from: classes.dex */
public interface b extends av1 {

    /* compiled from: JsonFormatVisitorWrapper.java */
    /* loaded from: classes.dex */
    public static class a implements b {
        public k a;

        public a(k kVar) {
            this.a = kVar;
        }

        @Override // defpackage.av1
        public k b() {
            return this.a;
        }

        @Override // com.fasterxml.jackson.databind.jsonFormatVisitors.b
        public cv1 c(JavaType javaType) throws JsonMappingException {
            return null;
        }

        @Override // com.fasterxml.jackson.databind.jsonFormatVisitors.b
        public ew1 d(JavaType javaType) throws JsonMappingException {
            return null;
        }

        @Override // com.fasterxml.jackson.databind.jsonFormatVisitors.b
        public pu1 e(JavaType javaType) throws JsonMappingException {
            return null;
        }

        @Override // com.fasterxml.jackson.databind.jsonFormatVisitors.b
        public fv1 f(JavaType javaType) throws JsonMappingException {
            return null;
        }

        @Override // defpackage.av1
        public void h(k kVar) {
            this.a = kVar;
        }

        @Override // com.fasterxml.jackson.databind.jsonFormatVisitors.b
        public com.fasterxml.jackson.databind.jsonFormatVisitors.a j(JavaType javaType) throws JsonMappingException {
            return null;
        }

        @Override // com.fasterxml.jackson.databind.jsonFormatVisitors.b
        public su1 m(JavaType javaType) throws JsonMappingException {
            return null;
        }

        @Override // com.fasterxml.jackson.databind.jsonFormatVisitors.b
        public c o(JavaType javaType) throws JsonMappingException {
            return null;
        }

        @Override // com.fasterxml.jackson.databind.jsonFormatVisitors.b
        public ev1 p(JavaType javaType) throws JsonMappingException {
            return null;
        }
    }

    cv1 c(JavaType javaType) throws JsonMappingException;

    ew1 d(JavaType javaType) throws JsonMappingException;

    pu1 e(JavaType javaType) throws JsonMappingException;

    fv1 f(JavaType javaType) throws JsonMappingException;

    com.fasterxml.jackson.databind.jsonFormatVisitors.a j(JavaType javaType) throws JsonMappingException;

    d l(JavaType javaType) throws JsonMappingException;

    su1 m(JavaType javaType) throws JsonMappingException;

    c o(JavaType javaType) throws JsonMappingException;

    ev1 p(JavaType javaType) throws JsonMappingException;
}
