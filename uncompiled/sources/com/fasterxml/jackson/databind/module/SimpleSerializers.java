package com.fasterxml.jackson.databind.module;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.f;
import com.fasterxml.jackson.databind.jsontype.c;
import com.fasterxml.jackson.databind.type.ArrayType;
import com.fasterxml.jackson.databind.type.ClassKey;
import com.fasterxml.jackson.databind.type.CollectionLikeType;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.MapLikeType;
import com.fasterxml.jackson.databind.type.MapType;
import defpackage.am3;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

/* loaded from: classes.dex */
public class SimpleSerializers extends am3.a implements Serializable {
    private static final long serialVersionUID = 8531646511998456779L;
    public HashMap<ClassKey, f<?>> _classMappings = null;
    public HashMap<ClassKey, f<?>> _interfaceMappings = null;
    public boolean _hasEnumSerializer = false;

    public SimpleSerializers() {
    }

    public void _addSerializer(Class<?> cls, f<?> fVar) {
        ClassKey classKey = new ClassKey(cls);
        if (cls.isInterface()) {
            if (this._interfaceMappings == null) {
                this._interfaceMappings = new HashMap<>();
            }
            this._interfaceMappings.put(classKey, fVar);
            return;
        }
        if (this._classMappings == null) {
            this._classMappings = new HashMap<>();
        }
        this._classMappings.put(classKey, fVar);
        if (cls == Enum.class) {
            this._hasEnumSerializer = true;
        }
    }

    public f<?> _findInterfaceMapping(Class<?> cls, ClassKey classKey) {
        Class<?>[] interfaces;
        for (Class<?> cls2 : cls.getInterfaces()) {
            classKey.reset(cls2);
            f<?> fVar = this._interfaceMappings.get(classKey);
            if (fVar != null) {
                return fVar;
            }
            f<?> _findInterfaceMapping = _findInterfaceMapping(cls2, classKey);
            if (_findInterfaceMapping != null) {
                return _findInterfaceMapping;
            }
        }
        return null;
    }

    public void addSerializer(f<?> fVar) {
        Class<?> handledType = fVar.handledType();
        if (handledType != null && handledType != Object.class) {
            _addSerializer(handledType, fVar);
            return;
        }
        throw new IllegalArgumentException("JsonSerializer of type " + fVar.getClass().getName() + " does not define valid handledType() -- must either register with method that takes type argument  or make serializer extend 'com.fasterxml.jackson.databind.ser.std.StdSerializer'");
    }

    public void addSerializers(List<f<?>> list) {
        for (f<?> fVar : list) {
            addSerializer(fVar);
        }
    }

    @Override // defpackage.am3.a, defpackage.am3
    public f<?> findArraySerializer(SerializationConfig serializationConfig, ArrayType arrayType, so soVar, c cVar, f<Object> fVar) {
        return findSerializer(serializationConfig, arrayType, soVar);
    }

    @Override // defpackage.am3.a, defpackage.am3
    public f<?> findCollectionLikeSerializer(SerializationConfig serializationConfig, CollectionLikeType collectionLikeType, so soVar, c cVar, f<Object> fVar) {
        return findSerializer(serializationConfig, collectionLikeType, soVar);
    }

    @Override // defpackage.am3.a, defpackage.am3
    public f<?> findCollectionSerializer(SerializationConfig serializationConfig, CollectionType collectionType, so soVar, c cVar, f<Object> fVar) {
        return findSerializer(serializationConfig, collectionType, soVar);
    }

    @Override // defpackage.am3.a, defpackage.am3
    public f<?> findMapLikeSerializer(SerializationConfig serializationConfig, MapLikeType mapLikeType, so soVar, f<Object> fVar, c cVar, f<Object> fVar2) {
        return findSerializer(serializationConfig, mapLikeType, soVar);
    }

    @Override // defpackage.am3.a, defpackage.am3
    public f<?> findMapSerializer(SerializationConfig serializationConfig, MapType mapType, so soVar, f<Object> fVar, c cVar, f<Object> fVar2) {
        return findSerializer(serializationConfig, mapType, soVar);
    }

    @Override // defpackage.am3.a, defpackage.am3
    public f<?> findSerializer(SerializationConfig serializationConfig, JavaType javaType, so soVar) {
        f<?> _findInterfaceMapping;
        f<?> fVar;
        Class<?> rawClass = javaType.getRawClass();
        ClassKey classKey = new ClassKey(rawClass);
        if (rawClass.isInterface()) {
            HashMap<ClassKey, f<?>> hashMap = this._interfaceMappings;
            if (hashMap != null && (fVar = hashMap.get(classKey)) != null) {
                return fVar;
            }
        } else {
            HashMap<ClassKey, f<?>> hashMap2 = this._classMappings;
            if (hashMap2 != null) {
                f<?> fVar2 = hashMap2.get(classKey);
                if (fVar2 != null) {
                    return fVar2;
                }
                if (this._hasEnumSerializer && javaType.isEnumType()) {
                    classKey.reset(Enum.class);
                    f<?> fVar3 = this._classMappings.get(classKey);
                    if (fVar3 != null) {
                        return fVar3;
                    }
                }
                for (Class<?> cls = rawClass; cls != null; cls = cls.getSuperclass()) {
                    classKey.reset(cls);
                    f<?> fVar4 = this._classMappings.get(classKey);
                    if (fVar4 != null) {
                        return fVar4;
                    }
                }
            }
        }
        if (this._interfaceMappings != null) {
            f<?> _findInterfaceMapping2 = _findInterfaceMapping(rawClass, classKey);
            if (_findInterfaceMapping2 != null) {
                return _findInterfaceMapping2;
            }
            if (rawClass.isInterface()) {
                return null;
            }
            do {
                rawClass = rawClass.getSuperclass();
                if (rawClass == null) {
                    return null;
                }
                _findInterfaceMapping = _findInterfaceMapping(rawClass, classKey);
            } while (_findInterfaceMapping == null);
            return _findInterfaceMapping;
        }
        return null;
    }

    public SimpleSerializers(List<f<?>> list) {
        addSerializers(list);
    }

    public <T> void addSerializer(Class<? extends T> cls, f<T> fVar) {
        _addSerializer(cls, fVar);
    }
}
