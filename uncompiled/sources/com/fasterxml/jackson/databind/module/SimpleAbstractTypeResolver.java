package com.fasterxml.jackson.databind.module;

import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.type.ClassKey;
import java.io.Serializable;
import java.lang.reflect.Modifier;
import java.util.HashMap;

/* loaded from: classes.dex */
public class SimpleAbstractTypeResolver extends o5 implements Serializable {
    private static final long serialVersionUID = 8635483102371490919L;
    public final HashMap<ClassKey, Class<?>> _mappings = new HashMap<>();

    public <T> SimpleAbstractTypeResolver addMapping(Class<T> cls, Class<? extends T> cls2) {
        if (cls != cls2) {
            if (cls.isAssignableFrom(cls2)) {
                if (Modifier.isAbstract(cls.getModifiers())) {
                    this._mappings.put(new ClassKey(cls), cls2);
                    return this;
                }
                throw new IllegalArgumentException("Can not add mapping from class " + cls.getName() + " since it is not abstract");
            }
            throw new IllegalArgumentException("Can not add mapping from class " + cls.getName() + " to " + cls2.getName() + ", as latter is not a subtype of former");
        }
        throw new IllegalArgumentException("Can not add mapping from class to itself");
    }

    @Override // defpackage.o5
    public JavaType findTypeMapping(DeserializationConfig deserializationConfig, JavaType javaType) {
        Class<?> cls = this._mappings.get(new ClassKey(javaType.getRawClass()));
        if (cls == null) {
            return null;
        }
        return deserializationConfig.getTypeFactory().constructSpecializedType(javaType, cls);
    }

    @Override // defpackage.o5
    @Deprecated
    public JavaType resolveAbstractType(DeserializationConfig deserializationConfig, JavaType javaType) {
        return null;
    }

    @Override // defpackage.o5
    public JavaType resolveAbstractType(DeserializationConfig deserializationConfig, so soVar) {
        return null;
    }
}
