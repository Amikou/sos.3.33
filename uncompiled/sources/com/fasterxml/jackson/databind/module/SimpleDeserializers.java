package com.fasterxml.jackson.databind.module;

import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.c;
import com.fasterxml.jackson.databind.d;
import com.fasterxml.jackson.databind.deser.f;
import com.fasterxml.jackson.databind.g;
import com.fasterxml.jackson.databind.jsontype.a;
import com.fasterxml.jackson.databind.type.ArrayType;
import com.fasterxml.jackson.databind.type.ClassKey;
import com.fasterxml.jackson.databind.type.CollectionLikeType;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.MapLikeType;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.databind.type.ReferenceType;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/* loaded from: classes.dex */
public class SimpleDeserializers implements f, Serializable {
    private static final long serialVersionUID = 1;
    public HashMap<ClassKey, c<?>> _classMappings = null;
    public boolean _hasEnumDeserializer = false;

    public SimpleDeserializers() {
    }

    public <T> void addDeserializer(Class<T> cls, c<? extends T> cVar) {
        ClassKey classKey = new ClassKey(cls);
        if (this._classMappings == null) {
            this._classMappings = new HashMap<>();
        }
        this._classMappings.put(classKey, cVar);
        if (cls == Enum.class) {
            this._hasEnumDeserializer = true;
        }
    }

    public void addDeserializers(Map<Class<?>, c<?>> map) {
        for (Map.Entry<Class<?>, c<?>> entry : map.entrySet()) {
            addDeserializer(entry.getKey(), entry.getValue());
        }
    }

    @Override // com.fasterxml.jackson.databind.deser.f
    public c<?> findArrayDeserializer(ArrayType arrayType, DeserializationConfig deserializationConfig, so soVar, a aVar, c<?> cVar) throws JsonMappingException {
        HashMap<ClassKey, c<?>> hashMap = this._classMappings;
        if (hashMap == null) {
            return null;
        }
        return hashMap.get(new ClassKey(arrayType.getRawClass()));
    }

    @Override // com.fasterxml.jackson.databind.deser.f
    public c<?> findBeanDeserializer(JavaType javaType, DeserializationConfig deserializationConfig, so soVar) throws JsonMappingException {
        HashMap<ClassKey, c<?>> hashMap = this._classMappings;
        if (hashMap == null) {
            return null;
        }
        return hashMap.get(new ClassKey(javaType.getRawClass()));
    }

    @Override // com.fasterxml.jackson.databind.deser.f
    public c<?> findCollectionDeserializer(CollectionType collectionType, DeserializationConfig deserializationConfig, so soVar, a aVar, c<?> cVar) throws JsonMappingException {
        HashMap<ClassKey, c<?>> hashMap = this._classMappings;
        if (hashMap == null) {
            return null;
        }
        return hashMap.get(new ClassKey(collectionType.getRawClass()));
    }

    @Override // com.fasterxml.jackson.databind.deser.f
    public c<?> findCollectionLikeDeserializer(CollectionLikeType collectionLikeType, DeserializationConfig deserializationConfig, so soVar, a aVar, c<?> cVar) throws JsonMappingException {
        HashMap<ClassKey, c<?>> hashMap = this._classMappings;
        if (hashMap == null) {
            return null;
        }
        return hashMap.get(new ClassKey(collectionLikeType.getRawClass()));
    }

    @Override // com.fasterxml.jackson.databind.deser.f
    public c<?> findEnumDeserializer(Class<?> cls, DeserializationConfig deserializationConfig, so soVar) throws JsonMappingException {
        HashMap<ClassKey, c<?>> hashMap = this._classMappings;
        if (hashMap == null) {
            return null;
        }
        c<?> cVar = hashMap.get(new ClassKey(cls));
        return (cVar == null && this._hasEnumDeserializer && cls.isEnum()) ? this._classMappings.get(new ClassKey(Enum.class)) : cVar;
    }

    @Override // com.fasterxml.jackson.databind.deser.f
    public c<?> findMapDeserializer(MapType mapType, DeserializationConfig deserializationConfig, so soVar, g gVar, a aVar, c<?> cVar) throws JsonMappingException {
        HashMap<ClassKey, c<?>> hashMap = this._classMappings;
        if (hashMap == null) {
            return null;
        }
        return hashMap.get(new ClassKey(mapType.getRawClass()));
    }

    @Override // com.fasterxml.jackson.databind.deser.f
    public c<?> findMapLikeDeserializer(MapLikeType mapLikeType, DeserializationConfig deserializationConfig, so soVar, g gVar, a aVar, c<?> cVar) throws JsonMappingException {
        HashMap<ClassKey, c<?>> hashMap = this._classMappings;
        if (hashMap == null) {
            return null;
        }
        return hashMap.get(new ClassKey(mapLikeType.getRawClass()));
    }

    @Override // com.fasterxml.jackson.databind.deser.f
    public c<?> findReferenceDeserializer(ReferenceType referenceType, DeserializationConfig deserializationConfig, so soVar, a aVar, c<?> cVar) throws JsonMappingException {
        HashMap<ClassKey, c<?>> hashMap = this._classMappings;
        if (hashMap == null) {
            return null;
        }
        return hashMap.get(new ClassKey(referenceType.getRawClass()));
    }

    @Override // com.fasterxml.jackson.databind.deser.f
    public c<?> findTreeNodeDeserializer(Class<? extends d> cls, DeserializationConfig deserializationConfig, so soVar) throws JsonMappingException {
        HashMap<ClassKey, c<?>> hashMap = this._classMappings;
        if (hashMap == null) {
            return null;
        }
        return hashMap.get(new ClassKey(cls));
    }

    public SimpleDeserializers(Map<Class<?>, c<?>> map) {
        addDeserializers(map);
    }
}
