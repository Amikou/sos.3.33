package com.fasterxml.jackson.databind.module;

import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.deser.i;
import com.fasterxml.jackson.databind.type.ClassKey;
import defpackage.ah4;
import java.io.Serializable;
import java.util.HashMap;

/* loaded from: classes.dex */
public class SimpleValueInstantiators extends ah4.a implements Serializable {
    private static final long serialVersionUID = -8929386427526115130L;
    public HashMap<ClassKey, i> _classMappings = new HashMap<>();

    public SimpleValueInstantiators addValueInstantiator(Class<?> cls, i iVar) {
        this._classMappings.put(new ClassKey(cls), iVar);
        return this;
    }

    @Override // defpackage.ah4.a, defpackage.ah4
    public i findValueInstantiator(DeserializationConfig deserializationConfig, so soVar, i iVar) {
        i iVar2 = this._classMappings.get(new ClassKey(soVar.r()));
        return iVar2 == null ? iVar : iVar2;
    }
}
