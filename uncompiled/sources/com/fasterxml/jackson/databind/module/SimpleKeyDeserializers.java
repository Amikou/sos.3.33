package com.fasterxml.jackson.databind.module;

import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.deser.g;
import com.fasterxml.jackson.databind.type.ClassKey;
import java.io.Serializable;
import java.util.HashMap;

/* loaded from: classes.dex */
public class SimpleKeyDeserializers implements g, Serializable {
    private static final long serialVersionUID = 1;
    public HashMap<ClassKey, com.fasterxml.jackson.databind.g> _classMappings = null;

    public SimpleKeyDeserializers addDeserializer(Class<?> cls, com.fasterxml.jackson.databind.g gVar) {
        if (this._classMappings == null) {
            this._classMappings = new HashMap<>();
        }
        this._classMappings.put(new ClassKey(cls), gVar);
        return this;
    }

    @Override // com.fasterxml.jackson.databind.deser.g
    public com.fasterxml.jackson.databind.g findKeyDeserializer(JavaType javaType, DeserializationConfig deserializationConfig, so soVar) {
        HashMap<ClassKey, com.fasterxml.jackson.databind.g> hashMap = this._classMappings;
        if (hashMap == null) {
            return null;
        }
        return hashMap.get(new ClassKey(javaType.getRawClass()));
    }
}
