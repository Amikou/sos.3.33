package com.fasterxml.jackson.databind;

import com.fasterxml.jackson.core.JsonProcessingException;
import java.io.IOException;

/* compiled from: KeyDeserializer.java */
/* loaded from: classes.dex */
public abstract class g {

    /* compiled from: KeyDeserializer.java */
    /* loaded from: classes.dex */
    public static abstract class a extends g {
    }

    public abstract Object deserializeKey(String str, DeserializationContext deserializationContext) throws IOException, JsonProcessingException;
}
