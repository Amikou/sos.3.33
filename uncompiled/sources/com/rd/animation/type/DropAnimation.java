package com.rd.animation.type;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.view.animation.AccelerateDecelerateInterpolator;
import defpackage.xg4;
import java.util.Iterator;

/* loaded from: classes2.dex */
public class DropAnimation extends nm<AnimatorSet> {
    public int d;
    public int e;
    public int f;
    public int g;
    public int h;
    public as0 i;

    /* loaded from: classes2.dex */
    public enum AnimationType {
        Width,
        Height,
        Radius
    }

    /* loaded from: classes2.dex */
    public class a implements ValueAnimator.AnimatorUpdateListener {
        public final /* synthetic */ AnimationType a;

        public a(AnimationType animationType) {
            this.a = animationType;
        }

        @Override // android.animation.ValueAnimator.AnimatorUpdateListener
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            DropAnimation.this.k(valueAnimator, this.a);
        }
    }

    /* loaded from: classes2.dex */
    public static /* synthetic */ class b {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[AnimationType.values().length];
            a = iArr;
            try {
                iArr[AnimationType.Width.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[AnimationType.Height.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[AnimationType.Radius.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    public DropAnimation(xg4.a aVar) {
        super(aVar);
        this.i = new as0();
    }

    @Override // defpackage.nm
    /* renamed from: g */
    public AnimatorSet a() {
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setInterpolator(new AccelerateDecelerateInterpolator());
        return animatorSet;
    }

    public final ValueAnimator h(int i, int i2, long j, AnimationType animationType) {
        ValueAnimator ofInt = ValueAnimator.ofInt(i, i2);
        ofInt.setInterpolator(new AccelerateDecelerateInterpolator());
        ofInt.setDuration(j);
        ofInt.addUpdateListener(new a(animationType));
        return ofInt;
    }

    public DropAnimation i(long j) {
        super.b(j);
        return this;
    }

    public final boolean j(int i, int i2, int i3, int i4, int i5) {
        return (this.d == i && this.e == i2 && this.f == i3 && this.g == i4 && this.h == i5) ? false : true;
    }

    public final void k(ValueAnimator valueAnimator, AnimationType animationType) {
        int intValue = ((Integer) valueAnimator.getAnimatedValue()).intValue();
        int i = b.a[animationType.ordinal()];
        if (i == 1) {
            this.i.f(intValue);
        } else if (i == 2) {
            this.i.d(intValue);
        } else if (i == 3) {
            this.i.e(intValue);
        }
        xg4.a aVar = this.b;
        if (aVar != null) {
            aVar.a(this.i);
        }
    }

    @Override // defpackage.nm
    /* renamed from: l */
    public DropAnimation d(float f) {
        T t = this.c;
        if (t != 0) {
            long j = f * ((float) this.a);
            boolean z = false;
            Iterator<Animator> it = ((AnimatorSet) t).getChildAnimations().iterator();
            while (it.hasNext()) {
                ValueAnimator valueAnimator = (ValueAnimator) it.next();
                long duration = valueAnimator.getDuration();
                long j2 = z ? j - duration : j;
                if (j2 >= 0) {
                    if (j2 >= duration) {
                        j2 = duration;
                    }
                    if (valueAnimator.getValues() != null && valueAnimator.getValues().length > 0) {
                        valueAnimator.setCurrentPlayTime(j2);
                    }
                    if (!z && duration >= this.a) {
                        z = true;
                    }
                }
            }
        }
        return this;
    }

    public DropAnimation m(int i, int i2, int i3, int i4, int i5) {
        if (j(i, i2, i3, i4, i5)) {
            this.c = a();
            this.d = i;
            this.e = i2;
            this.f = i3;
            this.g = i4;
            this.h = i5;
            int i6 = (int) (i5 / 1.5d);
            long j = this.a;
            long j2 = j / 2;
            ValueAnimator h = h(i, i2, j, AnimationType.Width);
            AnimationType animationType = AnimationType.Height;
            ValueAnimator h2 = h(i3, i4, j2, animationType);
            AnimationType animationType2 = AnimationType.Radius;
            ValueAnimator h3 = h(i5, i6, j2, animationType2);
            ValueAnimator h4 = h(i4, i3, j2, animationType);
            ((AnimatorSet) this.c).play(h2).with(h3).with(h).before(h4).before(h(i6, i5, j2, animationType2));
        }
        return this;
    }
}
