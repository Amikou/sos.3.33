package com.rd;

import defpackage.xg4;

/* compiled from: IndicatorManager.java */
/* loaded from: classes2.dex */
public class a implements xg4.a {
    public pq0 a;
    public me b;
    public InterfaceC0162a c;

    /* compiled from: IndicatorManager.java */
    /* renamed from: com.rd.a$a  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public interface InterfaceC0162a {
        void c();
    }

    public a(InterfaceC0162a interfaceC0162a) {
        this.c = interfaceC0162a;
        pq0 pq0Var = new pq0();
        this.a = pq0Var;
        this.b = new me(pq0Var.b(), this);
    }

    @Override // defpackage.xg4.a
    public void a(wg4 wg4Var) {
        this.a.g(wg4Var);
        InterfaceC0162a interfaceC0162a = this.c;
        if (interfaceC0162a != null) {
            interfaceC0162a.c();
        }
    }

    public me b() {
        return this.b;
    }

    public pq0 c() {
        return this.a;
    }

    public mq1 d() {
        return this.a.b();
    }
}
