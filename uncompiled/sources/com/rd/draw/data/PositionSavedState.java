package com.rd.draw.data;

import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;

/* loaded from: classes2.dex */
public class PositionSavedState extends View.BaseSavedState {
    public static final Parcelable.Creator<PositionSavedState> CREATOR = new a();
    public int a;
    public int f0;
    public int g0;

    /* loaded from: classes2.dex */
    public static class a implements Parcelable.Creator<PositionSavedState> {
        @Override // android.os.Parcelable.Creator
        /* renamed from: a */
        public PositionSavedState createFromParcel(Parcel parcel) {
            return new PositionSavedState(parcel, null);
        }

        @Override // android.os.Parcelable.Creator
        /* renamed from: b */
        public PositionSavedState[] newArray(int i) {
            return new PositionSavedState[i];
        }
    }

    public /* synthetic */ PositionSavedState(Parcel parcel, a aVar) {
        this(parcel);
    }

    public int a() {
        return this.g0;
    }

    public int b() {
        return this.a;
    }

    public int c() {
        return this.f0;
    }

    public void d(int i) {
        this.g0 = i;
    }

    public void e(int i) {
        this.a = i;
    }

    public void f(int i) {
        this.f0 = i;
    }

    @Override // android.view.View.BaseSavedState, android.view.AbsSavedState, android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.a);
        parcel.writeInt(this.f0);
        parcel.writeInt(this.g0);
    }

    public PositionSavedState(Parcelable parcelable) {
        super(parcelable);
    }

    public PositionSavedState(Parcel parcel) {
        super(parcel);
        this.a = parcel.readInt();
        this.f0 = parcel.readInt();
        this.g0 = parcel.readInt();
    }
}
