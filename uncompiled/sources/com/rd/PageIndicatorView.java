package com.rd;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Canvas;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import androidx.viewpager.widget.ViewPager;
import com.github.mikephil.charting.utils.Utils;
import com.rd.a;
import com.rd.animation.type.AnimationType;
import com.rd.draw.data.Orientation;
import com.rd.draw.data.PositionSavedState;
import com.rd.draw.data.RtlMode;
import defpackage.oq0;

/* loaded from: classes2.dex */
public class PageIndicatorView extends View implements ViewPager.i, a.InterfaceC0162a, ViewPager.h, View.OnTouchListener {
    public static final Handler j0 = new Handler(Looper.getMainLooper());
    public com.rd.a a;
    public DataSetObserver f0;
    public ViewPager g0;
    public boolean h0;
    public Runnable i0;

    /* loaded from: classes2.dex */
    public class a extends DataSetObserver {
        public a() {
        }

        @Override // android.database.DataSetObserver
        public void onChanged() {
            PageIndicatorView.this.z();
        }
    }

    /* loaded from: classes2.dex */
    public class b implements Runnable {
        public b() {
        }

        @Override // java.lang.Runnable
        public void run() {
            PageIndicatorView.this.a.d().H(true);
            PageIndicatorView.this.m();
        }
    }

    /* loaded from: classes2.dex */
    public static /* synthetic */ class c {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[RtlMode.values().length];
            a = iArr;
            try {
                iArr[RtlMode.On.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[RtlMode.Off.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[RtlMode.Auto.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    public PageIndicatorView(Context context) {
        super(context);
        this.i0 = new b();
        n(null);
    }

    public final void A() {
        if (this.a.d().w()) {
            int c2 = this.a.d().c();
            int visibility = getVisibility();
            if (visibility != 0 && c2 > 1) {
                setVisibility(0);
            } else if (visibility == 4 || c2 > 1) {
            } else {
                setVisibility(4);
            }
        }
    }

    @Override // androidx.viewpager.widget.ViewPager.i
    public void a(int i, float f, int i2) {
        r(i, f);
    }

    @Override // androidx.viewpager.widget.ViewPager.h
    public void b(ViewPager viewPager, dp2 dp2Var, dp2 dp2Var2) {
        DataSetObserver dataSetObserver;
        if (this.a.d().x()) {
            if (dp2Var != null && (dataSetObserver = this.f0) != null) {
                dp2Var.t(dataSetObserver);
                this.f0 = null;
            }
            t();
        }
        z();
    }

    @Override // com.rd.a.InterfaceC0162a
    public void c() {
        invalidate();
    }

    @Override // androidx.viewpager.widget.ViewPager.i
    public void d(int i) {
        if (i == 0) {
            this.a.d().J(this.h0);
        }
    }

    @Override // androidx.viewpager.widget.ViewPager.i
    public void e(int i) {
        s(i);
    }

    public long getAnimationDuration() {
        return this.a.d().a();
    }

    public int getCount() {
        return this.a.d().c();
    }

    public int getPadding() {
        return this.a.d().h();
    }

    public int getRadius() {
        return this.a.d().m();
    }

    public float getScaleFactor() {
        return this.a.d().o();
    }

    public int getSelectedColor() {
        return this.a.d().p();
    }

    public int getSelection() {
        return this.a.d().q();
    }

    public int getStrokeWidth() {
        return this.a.d().s();
    }

    public int getUnselectedColor() {
        return this.a.d().t();
    }

    public final int i(int i) {
        int c2 = this.a.d().c() - 1;
        if (i < 0) {
            return 0;
        }
        return i > c2 ? c2 : i;
    }

    public final void j() {
        animate().cancel();
        animate().alpha(1.0f).setDuration(250L);
    }

    public final ViewPager k(ViewGroup viewGroup, int i) {
        View findViewById;
        if (viewGroup.getChildCount() > 0 && (findViewById = viewGroup.findViewById(i)) != null && (findViewById instanceof ViewPager)) {
            return (ViewPager) findViewById;
        }
        return null;
    }

    public final void l(ViewParent viewParent) {
        if (viewParent != null && (viewParent instanceof ViewGroup) && ((ViewGroup) viewParent).getChildCount() > 0) {
            ViewPager k = k((ViewGroup) viewParent, this.a.d().u());
            if (k != null) {
                setViewPager(k);
            } else {
                l(viewParent.getParent());
            }
        }
    }

    public final void m() {
        animate().cancel();
        animate().alpha(Utils.FLOAT_EPSILON).setDuration(250L);
    }

    public final void n(AttributeSet attributeSet) {
        v();
        o(attributeSet);
        if (this.a.d().y()) {
            w();
        }
    }

    public final void o(AttributeSet attributeSet) {
        com.rd.a aVar = new com.rd.a(this);
        this.a = aVar;
        aVar.c().c(getContext(), attributeSet);
        mq1 d = this.a.d();
        d.O(getPaddingLeft());
        d.Q(getPaddingTop());
        d.P(getPaddingRight());
        d.N(getPaddingBottom());
        this.h0 = d.z();
    }

    @Override // android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        l(getParent());
    }

    @Override // android.view.View
    public void onDetachedFromWindow() {
        y();
        super.onDetachedFromWindow();
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        this.a.c().a(canvas);
    }

    @Override // android.view.View
    public void onMeasure(int i, int i2) {
        Pair<Integer, Integer> d = this.a.c().d(i, i2);
        setMeasuredDimension(((Integer) d.first).intValue(), ((Integer) d.second).intValue());
    }

    @Override // android.view.View
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (parcelable instanceof PositionSavedState) {
            mq1 d = this.a.d();
            PositionSavedState positionSavedState = (PositionSavedState) parcelable;
            d.V(positionSavedState.b());
            d.W(positionSavedState.c());
            d.K(positionSavedState.a());
            super.onRestoreInstanceState(positionSavedState.getSuperState());
            return;
        }
        super.onRestoreInstanceState(parcelable);
    }

    @Override // android.view.View
    public Parcelable onSaveInstanceState() {
        mq1 d = this.a.d();
        PositionSavedState positionSavedState = new PositionSavedState(super.onSaveInstanceState());
        positionSavedState.e(d.q());
        positionSavedState.f(d.r());
        positionSavedState.d(d.f());
        return positionSavedState;
    }

    @Override // android.view.View.OnTouchListener
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (this.a.d().y()) {
            int action = motionEvent.getAction();
            if (action == 0) {
                x();
            } else if (action == 1) {
                w();
            }
            return false;
        }
        return false;
    }

    @Override // android.view.View
    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        this.a.c().f(motionEvent);
        return true;
    }

    public final boolean p() {
        int i = c.a[this.a.d().n().ordinal()];
        if (i != 1) {
            return i == 3 && r44.b(getContext().getResources().getConfiguration().locale) == 1;
        }
        return true;
    }

    public final boolean q() {
        return (getMeasuredHeight() == 0 && getMeasuredWidth() == 0) ? false : true;
    }

    public final void r(int i, float f) {
        mq1 d = this.a.d();
        if (q() && d.z() && d.b() != AnimationType.NONE) {
            Pair<Integer, Float> e = r80.e(d, i, f, p());
            setProgress(((Integer) e.first).intValue(), ((Float) e.second).floatValue());
        }
    }

    public final void s(int i) {
        mq1 d = this.a.d();
        boolean q = q();
        int c2 = d.c();
        if (q) {
            if (p()) {
                i = (c2 - 1) - i;
            }
            setSelection(i);
        }
    }

    public void setAnimationDuration(long j) {
        this.a.d().A(j);
    }

    public void setAnimationType(AnimationType animationType) {
        this.a.a(null);
        if (animationType != null) {
            this.a.d().B(animationType);
        } else {
            this.a.d().B(AnimationType.NONE);
        }
        invalidate();
    }

    public void setAutoVisibility(boolean z) {
        if (!z) {
            setVisibility(0);
        }
        this.a.d().C(z);
        A();
    }

    public void setClickListener(oq0.b bVar) {
        this.a.c().e(bVar);
    }

    public void setCount(int i) {
        if (i < 0 || this.a.d().c() == i) {
            return;
        }
        this.a.d().D(i);
        A();
        requestLayout();
    }

    public void setDynamicCount(boolean z) {
        this.a.d().E(z);
        if (z) {
            t();
        } else {
            y();
        }
    }

    public void setFadeOnIdle(boolean z) {
        this.a.d().F(z);
        if (z) {
            w();
        } else {
            x();
        }
    }

    public void setIdleDuration(long j) {
        this.a.d().I(j);
        if (this.a.d().y()) {
            w();
        } else {
            x();
        }
    }

    public void setInteractiveAnimation(boolean z) {
        this.a.d().J(z);
        this.h0 = z;
    }

    public void setOrientation(Orientation orientation) {
        if (orientation != null) {
            this.a.d().L(orientation);
            requestLayout();
        }
    }

    public void setPadding(int i) {
        if (i < 0) {
            i = 0;
        }
        this.a.d().M(fm0.a(i));
        invalidate();
    }

    public void setProgress(int i, float f) {
        mq1 d = this.a.d();
        if (d.z()) {
            int c2 = d.c();
            if (c2 <= 0 || i < 0) {
                i = 0;
            } else {
                int i2 = c2 - 1;
                if (i > i2) {
                    i = i2;
                }
            }
            if (f < Utils.FLOAT_EPSILON) {
                f = 0.0f;
            } else if (f > 1.0f) {
                f = 1.0f;
            }
            if (f == 1.0f) {
                d.K(d.q());
                d.V(i);
            }
            d.W(i);
            this.a.b().c(f);
        }
    }

    public void setRadius(int i) {
        if (i < 0) {
            i = 0;
        }
        this.a.d().R(fm0.a(i));
        invalidate();
    }

    public void setRtlMode(RtlMode rtlMode) {
        mq1 d = this.a.d();
        if (rtlMode == null) {
            d.S(RtlMode.Off);
        } else {
            d.S(rtlMode);
        }
        if (this.g0 == null) {
            return;
        }
        int q = d.q();
        if (p()) {
            q = (d.c() - 1) - q;
        } else {
            ViewPager viewPager = this.g0;
            if (viewPager != null) {
                q = viewPager.getCurrentItem();
            }
        }
        d.K(q);
        d.W(q);
        d.V(q);
        invalidate();
    }

    public void setScaleFactor(float f) {
        if (f > 1.0f) {
            f = 1.0f;
        } else if (f < 0.3f) {
            f = 0.3f;
        }
        this.a.d().T(f);
    }

    public void setSelected(int i) {
        mq1 d = this.a.d();
        AnimationType b2 = d.b();
        d.B(AnimationType.NONE);
        setSelection(i);
        d.B(b2);
    }

    public void setSelectedColor(int i) {
        this.a.d().U(i);
        invalidate();
    }

    public void setSelection(int i) {
        mq1 d = this.a.d();
        int i2 = i(i);
        if (i2 == d.q() || i2 == d.r()) {
            return;
        }
        d.J(false);
        d.K(d.q());
        d.W(i2);
        d.V(i2);
        this.a.b().a();
    }

    public void setStrokeWidth(float f) {
        int m = this.a.d().m();
        if (f < Utils.FLOAT_EPSILON) {
            f = 0.0f;
        } else {
            float f2 = m;
            if (f > f2) {
                f = f2;
            }
        }
        this.a.d().X((int) f);
        invalidate();
    }

    public void setUnselectedColor(int i) {
        this.a.d().Y(i);
        invalidate();
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public void setViewPager(ViewPager viewPager) {
        u();
        if (viewPager == null) {
            return;
        }
        this.g0 = viewPager;
        viewPager.c(this);
        this.g0.b(this);
        this.g0.setOnTouchListener(this);
        this.a.d().Z(this.g0.getId());
        setDynamicCount(this.a.d().x());
        z();
    }

    public final void t() {
        ViewPager viewPager;
        if (this.f0 != null || (viewPager = this.g0) == null || viewPager.getAdapter() == null) {
            return;
        }
        this.f0 = new a();
        try {
            this.g0.getAdapter().l(this.f0);
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    public void u() {
        ViewPager viewPager = this.g0;
        if (viewPager != null) {
            viewPager.M(this);
            this.g0.L(this);
            this.g0 = null;
        }
    }

    public final void v() {
        if (getId() == -1) {
            setId(mn1.b());
        }
    }

    public final void w() {
        Handler handler = j0;
        handler.removeCallbacks(this.i0);
        handler.postDelayed(this.i0, this.a.d().e());
    }

    public final void x() {
        j0.removeCallbacks(this.i0);
        j();
    }

    public final void y() {
        ViewPager viewPager;
        if (this.f0 == null || (viewPager = this.g0) == null || viewPager.getAdapter() == null) {
            return;
        }
        try {
            this.g0.getAdapter().t(this.f0);
            this.f0 = null;
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    public final void z() {
        ViewPager viewPager = this.g0;
        if (viewPager == null || viewPager.getAdapter() == null) {
            return;
        }
        int e = this.g0.getAdapter().e();
        int currentItem = p() ? (e - 1) - this.g0.getCurrentItem() : this.g0.getCurrentItem();
        this.a.d().V(currentItem);
        this.a.d().W(currentItem);
        this.a.d().K(currentItem);
        this.a.d().D(e);
        this.a.b().b();
        A();
        requestLayout();
    }

    public PageIndicatorView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.i0 = new b();
        n(attributeSet);
    }

    public void setPadding(float f) {
        if (f < Utils.FLOAT_EPSILON) {
            f = 0.0f;
        }
        this.a.d().M((int) f);
        invalidate();
    }

    public void setRadius(float f) {
        if (f < Utils.FLOAT_EPSILON) {
            f = 0.0f;
        }
        this.a.d().R((int) f);
        invalidate();
    }

    public void setStrokeWidth(int i) {
        int a2 = fm0.a(i);
        int m = this.a.d().m();
        if (a2 < 0) {
            a2 = 0;
        } else if (a2 > m) {
            a2 = m;
        }
        this.a.d().X(a2);
        invalidate();
    }

    public PageIndicatorView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.i0 = new b();
        n(attributeSet);
    }
}
