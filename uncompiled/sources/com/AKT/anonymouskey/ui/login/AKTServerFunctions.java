package com.AKT.anonymouskey.ui.login;

import android.content.Context;
import android.os.Bundle;
import androidx.lifecycle.l;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import java.util.Locale;
import net.safemoon.androidwallet.MyApplicationClass;
import net.safemoon.androidwallet.activity.common.BasicActivity;
import net.safemoon.androidwallet.dialogs.ProgressLoading;
import net.safemoon.androidwallet.viewmodels.MultiWalletViewModel;

/* loaded from: classes.dex */
public abstract class AKTServerFunctions extends BasicActivity {
    public ProgressLoading j0;
    public MultiWalletViewModel k0;
    public qy4 l0 = MyApplicationClass.c().l0;

    public AKTServerFunctions() {
        System.loadLibrary("TrustWalletCore");
    }

    public static String L(String str) {
        String f = ay1.f(la3.a(ay1.a(str.toLowerCase())));
        return lm.d(f) + "@akt.com";
    }

    public static String N(Context context, String str) {
        String i = bo3.i(context, "PBU5");
        String str2 = "|AKTRequestID=CHANGEEMAIL|PBU5=" + i;
        String str3 = str2 + "|EMAIL=" + L(str) + "|AKTAPIID=CHANGE|";
        return ("server=" + s.e + ",port=" + s.f) + ",00#" + str3.length() + "#" + str3;
    }

    public static byte[] S(String str, String str2) {
        String replace = str.toLowerCase().replace(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR, "");
        String replace2 = str2.toLowerCase().replace(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR, "");
        int length = replace.length();
        if (length > 32) {
            length = 32;
        }
        byte[] bArr = new byte[32];
        for (int i = 0; i < 32; i++) {
            if (i < length) {
                bArr[i] = (byte) (((byte) replace.charAt(i)) ^ 202);
            } else {
                bArr[i] = -54;
            }
        }
        int length2 = replace2.length();
        int i2 = length2 <= 32 ? length2 : 32;
        for (int i3 = 0; i3 < i2; i3++) {
            int i4 = 31 - i3;
            bArr[i4] = (byte) (bArr[i4] ^ ((byte) replace2.charAt(i3)));
        }
        ay1.g(ay1.f(bArr));
        ay1.g(ay1.f(bArr));
        return la3.a(bArr);
    }

    public static String T(Context context) {
        String str = "|AKTRequestID=GETBLOB|PBU5=" + bo3.i(context, "PBU5") + "|AKTAPIID=getblob|";
        return ("server=" + s.e + ",port=" + s.f) + ",00#" + str.length() + "#" + str;
    }

    public static String Z(Context context, String str) {
        String str2 = "|AKTRequestID=PUTBLOB|PBU5=" + bo3.i(context, "PBU5") + "|BLOB=" + str + "|AKTAPIID=putblob|";
        return ("server=" + s.e + ",port=" + s.f) + ",00#" + str2.length() + "#" + str2;
    }

    public static String a0(Context context, String str) {
        return q.j(bo3.i(context, "ICEPRIVKEY"), s.g, str);
    }

    public static String e0(Context context, String str) {
        String[] m = i2.m();
        bo3.o(context, "ICEPRIVKEY", m[0]);
        bo3.o(context, "ICEPUBKEY", m[1]);
        String k = q.k(m[0], s.g, str);
        return m[1] + "|LSON=" + k + "|";
    }

    public String K(qy4 qy4Var, String str, boolean z) {
        String e;
        String str2 = s.g;
        String[] m = i2.m();
        String str3 = m[0];
        String str4 = m[1];
        bo3.o(this, "ICEPRIVKEY", str3);
        bo3.o(this, "ICEPUBKEY", str4);
        String l = q.l(str3, str2);
        if (z) {
            e = q.e(l, L(str));
        } else {
            e = q.e(l, str);
        }
        String[] strArr = new String[5];
        String f = b30.a.f(this, null);
        String[] strArr2 = {"AKTRequestID", "EMAIL", "PUBKEY", "LANG", "AKTAPIID"};
        strArr[0] = "AKTSFMICE03";
        strArr[1] = e;
        strArr[2] = str4;
        strArr[3] = f;
        if (z) {
            strArr[4] = "FORGOT";
        } else {
            strArr[4] = "AKTSFMICE03";
        }
        return i2.t(strArr2, strArr, s.e, s.f);
    }

    public String M(qy4 qy4Var, String str, String str2) {
        dz4 c = q.c(str, str2);
        String e = c.e();
        String g = c.g();
        qy4Var.c("U5", g);
        qy4Var.c("K5", e);
        String str3 = "AKTRequestID=AKTAUTHUSER|U5=" + g + "|AKTAPIID=login02|";
        String g2 = qy4Var.g("SERVER");
        return "port=" + qy4Var.g("SOCKET") + ",server=" + g2 + ",00#" + str3.length() + "#|" + str3;
    }

    public String O(String str, String str2) {
        dz4 dz4Var = new dz4();
        dz4Var.p(str2);
        dz4Var.q(str);
        dz4Var.o();
        lm.a(dz4Var.e());
        String str3 = "|AKTRequestID=CHECKFORU5|U5=" + dz4Var.g() + "|AKTAPIID=checkU5|";
        String str4 = "server=" + s.e + ",port=" + s.f;
        String str5 = str4 + ",00#" + str3.length() + "#" + str3;
        y54.a("Lynn").a("667 AKTS toSend=" + str3, new Object[0]);
        return str5;
    }

    public void P() {
        lp4.a(getApplicationContext()).execute();
        bo3.a(this);
        jv0.a(this);
    }

    public abstract void Q();

    public byte[] R(String str, String str2) {
        String replace = str.toLowerCase().replace(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR, "");
        String replace2 = str2.toLowerCase().replace(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR, "");
        byte[] bArr = new byte[20];
        for (int i = 0; i < 20; i++) {
            bArr[i] = -96;
        }
        int length = replace.length();
        if (length > 20) {
            length = 20;
        }
        for (int i2 = 0; i2 < length; i2++) {
            bArr[i2] = (byte) replace.charAt(i2);
        }
        int length2 = replace2.length();
        int i3 = length2 <= 20 ? length2 : 20;
        for (int i4 = 0; i4 < i3; i4++) {
            bArr[(20 - i4) - 1] = (byte) replace2.charAt(i4);
        }
        return la3.a(bArr);
    }

    public String U(String str, String str2, String str3) {
        byte[] S = S(str2, str3);
        ay1.g(ay1.f(S));
        dz4 dz4Var = new dz4();
        dz4Var.t(ay1.f(S));
        dz4Var.m(true);
        String b = dz4Var.b(str);
        if (b.length() != 32) {
            byte[] R = R(str2, str3);
            ay1.g(ay1.f(R));
            dz4Var.t(ay1.f(R));
            dz4Var.m(true);
            b = dz4Var.b(str);
        }
        return ay1.g(b);
    }

    public String V(String str) {
        return q.j(bo3.i(this, "ICEPRIVKEY"), s.g, str);
    }

    public boolean W(String str) {
        boolean equals = str.toUpperCase(Locale.ROOT).equals("CONNECTION_REFUSED");
        if (equals) {
            ProgressLoading progressLoading = this.j0;
            if (progressLoading != null) {
                progressLoading.h();
            }
            J(true);
            Q();
        }
        return equals;
    }

    public void X(qy4 qy4Var, String str) {
        if (W(str)) {
            return;
        }
        String[] v = i2.v(b30.a.v(this, str), "|");
        int length = v.length;
        String[] split = v[0].split("=");
        String upperCase = split.length > 1 ? split[1].toUpperCase() : "";
        upperCase.hashCode();
        if (upperCase.equals("AKTSERVERERROR")) {
            Q();
            Y(v, v[length - 1].split("=")[1]);
        } else if (!upperCase.equals("PUBKEY02")) {
            Q();
        } else {
            Q();
            String[] split2 = v[1].split("=");
            if (split2.length > 1) {
                bo3.o(this, "SERPUBKEY", q.g(split2[1]));
            }
            ProgressLoading progressLoading = this.j0;
            if (progressLoading != null) {
                progressLoading.h();
            }
        }
    }

    public void Y(String[] strArr, String str) {
        ProgressLoading progressLoading = this.j0;
        if (progressLoading != null) {
            progressLoading.h();
        }
    }

    public String b0(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10) {
        String[] strArr;
        String[] strArr2;
        dz4 dz4Var = new dz4();
        dz4Var.p(str3);
        dz4Var.q(str2);
        dz4Var.o();
        String e = dz4Var.e();
        String a = lm.a(e);
        String g = dz4Var.g();
        String f = ay1.f(r.b(str10));
        String g2 = ay1.g(f);
        byte[] h = ay1.h(ay1.a(i2.q(str2)), i2.q(str3));
        String f2 = ay1.f(h);
        String d = lm.d(ay1.f(h));
        dz4Var.t(f2);
        dz4Var.m(true);
        String c = dz4Var.c(f);
        byte[] S = S(str5, str7);
        dz4 dz4Var2 = new dz4();
        dz4Var2.t(ay1.f(S));
        dz4Var2.m(true);
        String c2 = dz4Var2.c(f);
        dz4Var2.s();
        dz4Var2.t(f);
        dz4Var2.m(true);
        String c3 = dz4Var2.c(str2 + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + str3);
        String b = ez4.b(f);
        String d2 = lm.d(b);
        byte[] a2 = ay1.a(f);
        byte[] bArr = new byte[32];
        for (int i = 0; i < 32; i++) {
            bArr[31 - i] = a2[i];
        }
        String d3 = lm.d(ay1.f(la3.a(bArr)));
        bo3.o(this, "U5", g);
        bo3.o(this, "KA", c);
        bo3.o(this, "PBU5", d2);
        bo3.o(this, "PB5K", d3);
        bo3.o(this, "EMAIL", str);
        this.l0.c("U5", g);
        this.l0.c("K5", e);
        this.l0.c("UK5", d);
        this.l0.c("KA", c);
        this.l0.c("PBU5", d2);
        this.l0.c("PB5K", d3);
        this.l0.c("PBHEX", g2);
        this.l0.c("EMAIL", str);
        c0("K5", e);
        c0("UK5", d);
        boolean z = s.a;
        if (z) {
            strArr = new String[22];
            strArr2 = new String[22];
        } else {
            strArr = new String[17];
            strArr2 = new String[17];
        }
        strArr[0] = "AKTRequestID";
        strArr[0] = "AKTRequestID";
        strArr[1] = "EMAIL";
        strArr[2] = "KA";
        strArr[3] = "SECURITYLEVEL";
        strArr[4] = "KS";
        strArr[5] = "U5";
        strArr[6] = "PBU5";
        strArr[7] = "PB5K";
        strArr[8] = "UPKEY";
        strArr[9] = "UPKEYH";
        strArr[10] = "AKTAPIID";
        strArr[11] = "FN";
        strArr[12] = "QN1";
        strArr[13] = "QN2";
        strArr[14] = "PSK";
        strArr[15] = "Q1";
        strArr[16] = "Q2";
        if (z) {
            strArr[17] = "K5";
            strArr[18] = "AN1";
            strArr[19] = "AN2";
            strArr[20] = "U";
            strArr[21] = "P";
        }
        StringBuffer stringBuffer = new StringBuffer("");
        for (int i2 = 0; i2 < f.length(); i2++) {
            stringBuffer.append(f.charAt((f.length() - i2) - 1));
        }
        String f3 = ay1.f(la3.a(ay1.a("" + ((Object) stringBuffer))));
        String f4 = ay1.f(la3.a(ay1.a(c3)));
        String b2 = ay1.b(32);
        String f5 = ay1.f(la3.a(ay1.a(str.toLowerCase())));
        String str11 = lm.d(f5) + "@akt.com";
        bo3.o(this, "EMAIL", str11);
        bo3.o(this, "EMAILRAW", str);
        String[] m = i2.m();
        String str12 = m[0];
        String str13 = m[1];
        bo3.o(this, "ICEPRIVKEY", str12);
        bo3.o(this, "ICEPUBKEY", str13);
        byte[] b3 = r.b(q.l(str12, s.g));
        String f6 = ay1.f(b3);
        bz4 bz4Var = new bz4(false);
        bz4Var.g(b3);
        bz4Var.i(false);
        dz4 dz4Var3 = new dz4();
        dz4Var3.t(f6);
        dz4Var3.m(false);
        String c4 = dz4Var3.c(str11);
        dz4Var3.c(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
        String c5 = dz4Var3.c(g);
        String c6 = dz4Var3.c(a);
        String c7 = dz4Var3.c(b);
        ay1.g(dz4Var3.b(c7));
        String c8 = dz4Var3.c(f3);
        String c9 = dz4Var3.c(c3);
        String c10 = dz4Var3.c(f4);
        String c11 = dz4Var3.c(c);
        String c12 = dz4Var3.c(c2);
        String c13 = dz4Var3.c("ZecureIT.uky");
        String c14 = dz4Var3.c(str4);
        String c15 = dz4Var3.c(str6);
        String[] strArr3 = strArr;
        String c16 = dz4Var3.c("1");
        String c17 = dz4Var3.c(b2);
        String c18 = dz4Var3.c(str5);
        String c19 = dz4Var3.c(str7);
        String c20 = dz4Var3.c(str8);
        String c21 = dz4Var3.c(str9);
        String c22 = dz4Var3.c(str2);
        String c23 = dz4Var3.c(str3);
        strArr2[0] = "AKTSFMReg01";
        strArr2[1] = c4;
        strArr2[2] = c11;
        strArr2[3] = c16;
        strArr2[4] = c12;
        strArr2[5] = c5;
        strArr2[6] = c7;
        strArr2[7] = c8;
        strArr2[8] = c9;
        strArr2[9] = c10;
        strArr2[10] = "GR8Reg01";
        strArr2[11] = c13;
        strArr2[12] = c14;
        strArr2[13] = c15;
        strArr2[14] = c17;
        strArr2[15] = c20;
        strArr2[16] = c21;
        if (s.a) {
            strArr2[17] = c6;
            strArr2[18] = c18;
            strArr2[19] = c19;
            strArr2[20] = c22;
            strArr2[21] = c23;
        }
        String t = i2.t(strArr3, strArr2, s.e, s.f);
        int indexOf = t.indexOf("|");
        return t.substring(0, indexOf) + "," + str13 + t.substring(indexOf);
    }

    public void c0(String str, String str2) {
        jv0.e(this, str, str2);
    }

    public void d0(String str, String str2) {
        bo3.o(this, str, str2);
    }

    public String f0(String str, String str2, String str3) {
        dz4 dz4Var = new dz4();
        dz4Var.p(str3);
        dz4Var.q(str2);
        dz4Var.o();
        String e = dz4Var.e();
        lm.a(e);
        String g = dz4Var.g();
        String f = ay1.f(r.b(str));
        byte[] h = ay1.h(ay1.a(i2.q(str2)), i2.q(str3));
        String f2 = ay1.f(h);
        String d = lm.d(ay1.f(h));
        dz4Var.t(f2);
        dz4Var.m(true);
        String c = dz4Var.c(f);
        String str4 = ("|AKTRequestID=UPDATEKA|PBU5=" + lm.d(ay1.f(ay1.a(ez4.b(f)))) + "|U5=" + g + "|KA=" + c + "|AKTAPIID=reset02|") + "U=" + str2 + "|P=" + str3 + "|";
        String str5 = "server=" + s.e + ",port=" + s.f;
        String str6 = str5 + ",00#" + str4.length() + "#" + str4;
        this.l0.c("tempU5", g);
        this.l0.c("tempKA", c);
        this.l0.c("tempK5", e);
        this.l0.c("tempUK5", d);
        return str6;
    }

    @Override // net.safemoon.androidwallet.activity.common.BasicActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.k0 = (MultiWalletViewModel) new l(this).a(MultiWalletViewModel.class);
    }
}
