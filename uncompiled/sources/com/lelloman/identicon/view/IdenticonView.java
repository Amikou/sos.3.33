package com.lelloman.identicon.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.ImageView;

/* compiled from: IdenticonView.kt */
@SuppressLint({"AppCompatCustomView"})
/* loaded from: classes2.dex */
public abstract class IdenticonView extends ImageView {
    public int a;
    public on1 f0;

    /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
    public IdenticonView(Context context) {
        this(context, null, 2, null);
        fs1.f(context, "context");
    }

    public /* synthetic */ IdenticonView(Context context, AttributeSet attributeSet, int i, qi0 qi0Var) {
        this(context, (i & 2) != 0 ? null : attributeSet);
    }

    public abstract on1 a(int i, int i2, int i3);

    public final int getHash() {
        return this.a;
    }

    @Override // android.view.View
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        if (i <= 0 || i2 <= 0) {
            return;
        }
        on1 a = a(getWidth(), getHeight(), this.a);
        this.f0 = a;
        setImageDrawable(a);
    }

    public final void setHash(int i) {
        this.a = i;
        on1 on1Var = this.f0;
        if (on1Var == null) {
            return;
        }
        on1Var.e(i);
        invalidate();
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public IdenticonView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        fs1.f(context, "context");
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, q23.IdenticonView);
            fs1.e(obtainStyledAttributes, "context.obtainStyledAttr….styleable.IdenticonView)");
            try {
                try {
                    setHash(obtainStyledAttributes.getInt(q23.IdenticonView_hash, this.a));
                } catch (Throwable unused) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Something went wrong when initializing ");
                    sb.append((Object) getClass().getName());
                    sb.append(' ');
                    sb.append(this);
                }
            } finally {
                obtainStyledAttributes.recycle();
            }
        }
    }
}
