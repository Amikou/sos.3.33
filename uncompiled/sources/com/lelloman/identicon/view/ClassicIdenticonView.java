package com.lelloman.identicon.view;

import android.content.Context;
import android.util.AttributeSet;

/* compiled from: ClassicIdenticonView.kt */
/* loaded from: classes2.dex */
public final class ClassicIdenticonView extends IdenticonView {
    /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
    public ClassicIdenticonView(Context context) {
        this(context, null, 2, null);
        fs1.f(context, "context");
    }

    public /* synthetic */ ClassicIdenticonView(Context context, AttributeSet attributeSet, int i, qi0 qi0Var) {
        this(context, (i & 2) != 0 ? null : attributeSet);
    }

    @Override // com.lelloman.identicon.view.IdenticonView
    /* renamed from: b */
    public ez a(int i, int i2, int i3) {
        return new ez(i, i2, i3);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ClassicIdenticonView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        fs1.f(context, "context");
    }
}
