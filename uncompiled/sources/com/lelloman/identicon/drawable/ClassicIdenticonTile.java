package com.lelloman.identicon.drawable;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import com.github.mikephil.charting.utils.Utils;
import kotlin.jvm.internal.Lambda;

/* compiled from: ClassicIdenticonTile.kt */
/* loaded from: classes2.dex */
public final class ClassicIdenticonTile {
    public static final ClassicIdenticonTile a = new ClassicIdenticonTile();
    public static Tiles[] b = {Tiles.FOUR_SQUARES, Tiles.HALF_SQUARE_TRIANGLE, Tiles.BIG_TRIANGLE, Tiles.HALF_SQUARE_RECTANGLE, Tiles.ROTATED_SQUARE, Tiles.SPEAR_TIP, Tiles.THREE_TRIANGLES, Tiles.SLIM_TRIANGLE, Tiles.LITTLE_SQUARE, Tiles.TWO_TRIANGLES, Tiles.LITTLE_SIDE_SQUARE, Tiles.MARLBORO_TRIANGLE, Tiles.FOUR_TRIANGLES, Tiles.LITTLE_TRIANGLE_INSIDE, Tiles.LITTLE_TRIANGLE_TIP, Tiles.TWO_SQUARES_DIAGONAL, Tiles.LITTLE_ROTATED_SQUARE, Tiles.SHIFTED_MARLBORO_TRIANGLE, Tiles.TUNNELING_TRIANGLES, Tiles.TWO_TIPS_TRIANGLES, Tiles.FOUR_TRIANGLES_FACING_CENTER, Tiles.TWO_SQUARES_STRAIGHT_LINE, Tiles.TWO_TRAPEZOIDS, Tiles.ARROW, Tiles.ROTATE_SQUARE_WITH_HOLE, Tiles.TWO_OPPOSITE_TRIANGLES, Tiles.TRIANGLE_SANDWICH, Tiles.SPIKE, Tiles.FOUR_TRIANGLES_STAR, Tiles.BIG_TRIANGLE_TIP, Tiles.DIAMOND, Tiles.TWO_OPPOSITE_TRIANGLES_BIG};

    /* compiled from: ClassicIdenticonTile.kt */
    /* loaded from: classes2.dex */
    public enum Tiles {
        FOUR_SQUARES(AnonymousClass1.INSTANCE),
        HALF_SQUARE_TRIANGLE(AnonymousClass2.INSTANCE),
        BIG_TRIANGLE(AnonymousClass3.INSTANCE),
        HALF_SQUARE_RECTANGLE(AnonymousClass4.INSTANCE),
        ROTATED_SQUARE(AnonymousClass5.INSTANCE),
        SPEAR_TIP(AnonymousClass6.INSTANCE),
        THREE_TRIANGLES(AnonymousClass7.INSTANCE),
        SLIM_TRIANGLE(AnonymousClass8.INSTANCE),
        LITTLE_SQUARE(AnonymousClass9.INSTANCE),
        TWO_TRIANGLES(AnonymousClass10.INSTANCE),
        LITTLE_SIDE_SQUARE(AnonymousClass11.INSTANCE),
        MARLBORO_TRIANGLE(AnonymousClass12.INSTANCE),
        FOUR_TRIANGLES(AnonymousClass13.INSTANCE),
        LITTLE_TRIANGLE_INSIDE(AnonymousClass14.INSTANCE),
        LITTLE_TRIANGLE_TIP(AnonymousClass15.INSTANCE),
        TWO_SQUARES_DIAGONAL(AnonymousClass16.INSTANCE),
        LITTLE_ROTATED_SQUARE(AnonymousClass17.INSTANCE),
        SHIFTED_MARLBORO_TRIANGLE(AnonymousClass18.INSTANCE),
        TUNNELING_TRIANGLES(AnonymousClass19.INSTANCE),
        TWO_TIPS_TRIANGLES(AnonymousClass20.INSTANCE),
        FOUR_TRIANGLES_FACING_CENTER(AnonymousClass21.INSTANCE),
        TWO_SQUARES_STRAIGHT_LINE(AnonymousClass22.INSTANCE),
        TWO_TRAPEZOIDS(AnonymousClass23.INSTANCE),
        ARROW(AnonymousClass24.INSTANCE),
        ROTATE_SQUARE_WITH_HOLE(AnonymousClass25.INSTANCE),
        TWO_OPPOSITE_TRIANGLES(AnonymousClass26.INSTANCE),
        TRIANGLE_SANDWICH(AnonymousClass27.INSTANCE),
        SPIKE(AnonymousClass28.INSTANCE),
        FOUR_TRIANGLES_STAR(AnonymousClass29.INSTANCE),
        BIG_TRIANGLE_TIP(AnonymousClass30.INSTANCE),
        DIAMOND(AnonymousClass31.INSTANCE),
        TWO_OPPOSITE_TRIANGLES_BIG(AnonymousClass32.INSTANCE);
        
        private final w54 tileDrawer;

        /* compiled from: ClassicIdenticonTile.kt */
        /* renamed from: com.lelloman.identicon.drawable.ClassicIdenticonTile$Tiles$1  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass1 extends Lambda implements hd1<Path, x54, te4> {
            public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

            public AnonymousClass1() {
                super(2);
            }

            @Override // defpackage.hd1
            public /* bridge */ /* synthetic */ te4 invoke(Path path, x54 x54Var) {
                invoke2(path, x54Var);
                return te4.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(Path path, x54 x54Var) {
                fs1.f(path, "path");
                fs1.f(x54Var, "meas");
                wp2.b(path, 0, 0, x54Var.h(), x54Var.a());
                wp2.b(path, x54Var.i(), 0, x54Var.m(), x54Var.a());
                wp2.b(path, 0, x54Var.b(), x54Var.h(), x54Var.f());
                wp2.b(path, x54Var.i(), x54Var.b(), x54Var.m(), x54Var.f());
            }
        }

        /* compiled from: ClassicIdenticonTile.kt */
        /* renamed from: com.lelloman.identicon.drawable.ClassicIdenticonTile$Tiles$10  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass10 extends Lambda implements hd1<Path, x54, te4> {
            public static final AnonymousClass10 INSTANCE = new AnonymousClass10();

            public AnonymousClass10() {
                super(2);
            }

            @Override // defpackage.hd1
            public /* bridge */ /* synthetic */ te4 invoke(Path path, x54 x54Var) {
                invoke2(path, x54Var);
                return te4.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(Path path, x54 x54Var) {
                fs1.f(path, "path");
                fs1.f(x54Var, "meas");
                wp2.c(path, 0, x54Var.f(), 0, x54Var.e(), x54Var.l(), x54Var.e());
                wp2.c(path, x54Var.m(), 0, x54Var.l(), 0, x54Var.l(), x54Var.e());
            }
        }

        /* compiled from: ClassicIdenticonTile.kt */
        /* renamed from: com.lelloman.identicon.drawable.ClassicIdenticonTile$Tiles$11  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass11 extends Lambda implements hd1<Path, x54, te4> {
            public static final AnonymousClass11 INSTANCE = new AnonymousClass11();

            public AnonymousClass11() {
                super(2);
            }

            @Override // defpackage.hd1
            public /* bridge */ /* synthetic */ te4 invoke(Path path, x54 x54Var) {
                invoke2(path, x54Var);
                return te4.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(Path path, x54 x54Var) {
                fs1.f(path, "path");
                fs1.f(x54Var, "meas");
                wp2.b(path, 0, 0, x54Var.l(), x54Var.e());
            }
        }

        /* compiled from: ClassicIdenticonTile.kt */
        /* renamed from: com.lelloman.identicon.drawable.ClassicIdenticonTile$Tiles$12  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass12 extends Lambda implements hd1<Path, x54, te4> {
            public static final AnonymousClass12 INSTANCE = new AnonymousClass12();

            public AnonymousClass12() {
                super(2);
            }

            @Override // defpackage.hd1
            public /* bridge */ /* synthetic */ te4 invoke(Path path, x54 x54Var) {
                invoke2(path, x54Var);
                return te4.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(Path path, x54 x54Var) {
                fs1.f(path, "path");
                fs1.f(x54Var, "meas");
                wp2.c(path, 0, x54Var.f(), x54Var.m(), x54Var.f(), x54Var.l(), x54Var.e());
            }
        }

        /* compiled from: ClassicIdenticonTile.kt */
        /* renamed from: com.lelloman.identicon.drawable.ClassicIdenticonTile$Tiles$13  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass13 extends Lambda implements hd1<Path, x54, te4> {
            public static final AnonymousClass13 INSTANCE = new AnonymousClass13();

            public AnonymousClass13() {
                super(2);
            }

            @Override // defpackage.hd1
            public /* bridge */ /* synthetic */ te4 invoke(Path path, x54 x54Var) {
                invoke2(path, x54Var);
                return te4.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(Path path, x54 x54Var) {
                fs1.f(path, "path");
                fs1.f(x54Var, "meas");
                wp2.c(path, x54Var.l(), 0, x54Var.j(), x54Var.c(), x54Var.k(), x54Var.c());
                wp2.c(path, x54Var.m(), x54Var.e(), x54Var.k(), x54Var.d(), x54Var.k(), x54Var.c());
                wp2.c(path, x54Var.l(), x54Var.f(), x54Var.j(), x54Var.d(), x54Var.k(), x54Var.d());
                wp2.c(path, 0, x54Var.e(), x54Var.j(), x54Var.c(), x54Var.j(), x54Var.d());
            }
        }

        /* compiled from: ClassicIdenticonTile.kt */
        /* renamed from: com.lelloman.identicon.drawable.ClassicIdenticonTile$Tiles$14  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass14 extends Lambda implements hd1<Path, x54, te4> {
            public static final AnonymousClass14 INSTANCE = new AnonymousClass14();

            public AnonymousClass14() {
                super(2);
            }

            @Override // defpackage.hd1
            public /* bridge */ /* synthetic */ te4 invoke(Path path, x54 x54Var) {
                invoke2(path, x54Var);
                return te4.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(Path path, x54 x54Var) {
                fs1.f(path, "path");
                fs1.f(x54Var, "meas");
                wp2.c(path, x54Var.l(), 0, x54Var.l(), x54Var.e(), 0, x54Var.e());
            }
        }

        /* compiled from: ClassicIdenticonTile.kt */
        /* renamed from: com.lelloman.identicon.drawable.ClassicIdenticonTile$Tiles$15  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass15 extends Lambda implements hd1<Path, x54, te4> {
            public static final AnonymousClass15 INSTANCE = new AnonymousClass15();

            public AnonymousClass15() {
                super(2);
            }

            @Override // defpackage.hd1
            public /* bridge */ /* synthetic */ te4 invoke(Path path, x54 x54Var) {
                invoke2(path, x54Var);
                return te4.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(Path path, x54 x54Var) {
                fs1.f(path, "path");
                fs1.f(x54Var, "meas");
                wp2.c(path, x54Var.l(), 0, 0, x54Var.e(), 0, 0);
            }
        }

        /* compiled from: ClassicIdenticonTile.kt */
        /* renamed from: com.lelloman.identicon.drawable.ClassicIdenticonTile$Tiles$16  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass16 extends Lambda implements hd1<Path, x54, te4> {
            public static final AnonymousClass16 INSTANCE = new AnonymousClass16();

            public AnonymousClass16() {
                super(2);
            }

            @Override // defpackage.hd1
            public /* bridge */ /* synthetic */ te4 invoke(Path path, x54 x54Var) {
                invoke2(path, x54Var);
                return te4.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(Path path, x54 x54Var) {
                fs1.f(path, "path");
                fs1.f(x54Var, "meas");
                wp2.b(path, 0, 0, x54Var.l(), x54Var.e());
                wp2.b(path, x54Var.l(), x54Var.e(), x54Var.m(), x54Var.f());
            }
        }

        /* compiled from: ClassicIdenticonTile.kt */
        /* renamed from: com.lelloman.identicon.drawable.ClassicIdenticonTile$Tiles$17  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass17 extends Lambda implements hd1<Path, x54, te4> {
            public static final AnonymousClass17 INSTANCE = new AnonymousClass17();

            public AnonymousClass17() {
                super(2);
            }

            @Override // defpackage.hd1
            public /* bridge */ /* synthetic */ te4 invoke(Path path, x54 x54Var) {
                invoke2(path, x54Var);
                return te4.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(Path path, x54 x54Var) {
                fs1.f(path, "path");
                fs1.f(x54Var, "meas");
                wp2.a(path, x54Var.l(), x54Var.c(), x54Var.k(), x54Var.e(), x54Var.l(), x54Var.d(), x54Var.j(), x54Var.e());
            }
        }

        /* compiled from: ClassicIdenticonTile.kt */
        /* renamed from: com.lelloman.identicon.drawable.ClassicIdenticonTile$Tiles$18  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass18 extends Lambda implements hd1<Path, x54, te4> {
            public static final AnonymousClass18 INSTANCE = new AnonymousClass18();

            public AnonymousClass18() {
                super(2);
            }

            @Override // defpackage.hd1
            public /* bridge */ /* synthetic */ te4 invoke(Path path, x54 x54Var) {
                invoke2(path, x54Var);
                return te4.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(Path path, x54 x54Var) {
                fs1.f(path, "path");
                fs1.f(x54Var, "meas");
                wp2.c(path, x54Var.l(), 0, x54Var.m(), x54Var.e(), 0, x54Var.e());
            }
        }

        /* compiled from: ClassicIdenticonTile.kt */
        /* renamed from: com.lelloman.identicon.drawable.ClassicIdenticonTile$Tiles$19  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass19 extends Lambda implements hd1<Path, x54, te4> {
            public static final AnonymousClass19 INSTANCE = new AnonymousClass19();

            public AnonymousClass19() {
                super(2);
            }

            @Override // defpackage.hd1
            public /* bridge */ /* synthetic */ te4 invoke(Path path, x54 x54Var) {
                invoke2(path, x54Var);
                return te4.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(Path path, x54 x54Var) {
                fs1.f(path, "path");
                fs1.f(x54Var, "meas");
                wp2.c(path, 0, 0, x54Var.m(), 0, 0, x54Var.e());
                wp2.c(path, x54Var.m(), x54Var.f(), 0, x54Var.f(), x54Var.m(), x54Var.e());
            }
        }

        /* compiled from: ClassicIdenticonTile.kt */
        /* renamed from: com.lelloman.identicon.drawable.ClassicIdenticonTile$Tiles$2  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass2 extends Lambda implements hd1<Path, x54, te4> {
            public static final AnonymousClass2 INSTANCE = new AnonymousClass2();

            public AnonymousClass2() {
                super(2);
            }

            @Override // defpackage.hd1
            public /* bridge */ /* synthetic */ te4 invoke(Path path, x54 x54Var) {
                invoke2(path, x54Var);
                return te4.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(Path path, x54 x54Var) {
                fs1.f(path, "path");
                fs1.f(x54Var, "meas");
                wp2.c(path, 0, 0, x54Var.m(), 0, 0, x54Var.f());
            }
        }

        /* compiled from: ClassicIdenticonTile.kt */
        /* renamed from: com.lelloman.identicon.drawable.ClassicIdenticonTile$Tiles$20  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass20 extends Lambda implements hd1<Path, x54, te4> {
            public static final AnonymousClass20 INSTANCE = new AnonymousClass20();

            public AnonymousClass20() {
                super(2);
            }

            @Override // defpackage.hd1
            public /* bridge */ /* synthetic */ te4 invoke(Path path, x54 x54Var) {
                invoke2(path, x54Var);
                return te4.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(Path path, x54 x54Var) {
                fs1.f(path, "path");
                fs1.f(x54Var, "meas");
                wp2.c(path, x54Var.l(), 0, 0, x54Var.e(), 0, 0);
                wp2.c(path, x54Var.l(), x54Var.f(), x54Var.m(), x54Var.e(), x54Var.m(), x54Var.f());
            }
        }

        /* compiled from: ClassicIdenticonTile.kt */
        /* renamed from: com.lelloman.identicon.drawable.ClassicIdenticonTile$Tiles$21  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass21 extends Lambda implements hd1<Path, x54, te4> {
            public static final AnonymousClass21 INSTANCE = new AnonymousClass21();

            public AnonymousClass21() {
                super(2);
            }

            @Override // defpackage.hd1
            public /* bridge */ /* synthetic */ te4 invoke(Path path, x54 x54Var) {
                invoke2(path, x54Var);
                return te4.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(Path path, x54 x54Var) {
                fs1.f(path, "path");
                fs1.f(x54Var, "meas");
                wp2.c(path, 0, 0, x54Var.m(), 0, x54Var.l(), x54Var.a());
                wp2.c(path, x54Var.m(), 0, x54Var.m(), x54Var.f(), x54Var.i(), x54Var.e());
                wp2.c(path, x54Var.m(), x54Var.f(), 0, x54Var.f(), x54Var.l(), x54Var.b());
                wp2.c(path, 0, x54Var.f(), 0, 0, x54Var.h(), x54Var.e());
            }
        }

        /* compiled from: ClassicIdenticonTile.kt */
        /* renamed from: com.lelloman.identicon.drawable.ClassicIdenticonTile$Tiles$22  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass22 extends Lambda implements hd1<Path, x54, te4> {
            public static final AnonymousClass22 INSTANCE = new AnonymousClass22();

            public AnonymousClass22() {
                super(2);
            }

            @Override // defpackage.hd1
            public /* bridge */ /* synthetic */ te4 invoke(Path path, x54 x54Var) {
                invoke2(path, x54Var);
                return te4.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(Path path, x54 x54Var) {
                fs1.f(path, "path");
                fs1.f(x54Var, "meas");
                wp2.a(path, x54Var.l(), 0, x54Var.k(), x54Var.c(), x54Var.l(), x54Var.e(), x54Var.j(), x54Var.c());
                wp2.a(path, x54Var.l(), x54Var.e(), x54Var.k(), x54Var.d(), x54Var.l(), x54Var.f(), x54Var.j(), x54Var.d());
            }
        }

        /* compiled from: ClassicIdenticonTile.kt */
        /* renamed from: com.lelloman.identicon.drawable.ClassicIdenticonTile$Tiles$23  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass23 extends Lambda implements hd1<Path, x54, te4> {
            public static final AnonymousClass23 INSTANCE = new AnonymousClass23();

            public AnonymousClass23() {
                super(2);
            }

            @Override // defpackage.hd1
            public /* bridge */ /* synthetic */ te4 invoke(Path path, x54 x54Var) {
                invoke2(path, x54Var);
                return te4.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(Path path, x54 x54Var) {
                fs1.f(path, "path");
                fs1.f(x54Var, "meas");
                wp2.a(path, 0, 0, x54Var.l(), 0, x54Var.l(), x54Var.c(), 0, x54Var.e());
                wp2.a(path, x54Var.m(), x54Var.f(), x54Var.m(), x54Var.e(), x54Var.l(), x54Var.d(), x54Var.l(), x54Var.f());
            }
        }

        /* compiled from: ClassicIdenticonTile.kt */
        /* renamed from: com.lelloman.identicon.drawable.ClassicIdenticonTile$Tiles$24  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass24 extends Lambda implements hd1<Path, x54, te4> {
            public static final AnonymousClass24 INSTANCE = new AnonymousClass24();

            public AnonymousClass24() {
                super(2);
            }

            @Override // defpackage.hd1
            public /* bridge */ /* synthetic */ te4 invoke(Path path, x54 x54Var) {
                invoke2(path, x54Var);
                return te4.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(Path path, x54 x54Var) {
                fs1.f(path, "path");
                fs1.f(x54Var, "meas");
                wp2.a(path, x54Var.m(), 0, x54Var.l(), x54Var.e(), x54Var.m(), x54Var.f(), 0, x54Var.e());
            }
        }

        /* compiled from: ClassicIdenticonTile.kt */
        /* renamed from: com.lelloman.identicon.drawable.ClassicIdenticonTile$Tiles$25  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass25 extends Lambda implements hd1<Path, x54, te4> {
            public static final AnonymousClass25 INSTANCE = new AnonymousClass25();

            public AnonymousClass25() {
                super(2);
            }

            @Override // defpackage.hd1
            public /* bridge */ /* synthetic */ te4 invoke(Path path, x54 x54Var) {
                invoke2(path, x54Var);
                return te4.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(Path path, x54 x54Var) {
                fs1.f(path, "path");
                fs1.f(x54Var, "meas");
                path.setFillType(Path.FillType.EVEN_ODD);
                path.moveTo(x54Var.l(), Utils.FLOAT_EPSILON);
                path.lineTo(x54Var.m(), x54Var.e());
                path.lineTo(x54Var.l(), x54Var.f());
                path.lineTo(Utils.FLOAT_EPSILON, x54Var.e());
                path.lineTo(x54Var.l(), Utils.FLOAT_EPSILON);
                path.close();
                path.moveTo(x54Var.l(), x54Var.c());
                path.lineTo(x54Var.k(), x54Var.e());
                path.lineTo(x54Var.l(), x54Var.d());
                path.lineTo(x54Var.j(), x54Var.e());
                path.lineTo(x54Var.l(), x54Var.c());
                path.close();
            }
        }

        /* compiled from: ClassicIdenticonTile.kt */
        /* renamed from: com.lelloman.identicon.drawable.ClassicIdenticonTile$Tiles$26  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass26 extends Lambda implements hd1<Path, x54, te4> {
            public static final AnonymousClass26 INSTANCE = new AnonymousClass26();

            public AnonymousClass26() {
                super(2);
            }

            @Override // defpackage.hd1
            public /* bridge */ /* synthetic */ te4 invoke(Path path, x54 x54Var) {
                invoke2(path, x54Var);
                return te4.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(Path path, x54 x54Var) {
                fs1.f(path, "path");
                fs1.f(x54Var, "meas");
                wp2.c(path, 0, 0, x54Var.m(), 0, x54Var.l(), x54Var.c());
                wp2.c(path, x54Var.m(), x54Var.f(), 0, x54Var.f(), x54Var.l(), x54Var.d());
            }
        }

        /* compiled from: ClassicIdenticonTile.kt */
        /* renamed from: com.lelloman.identicon.drawable.ClassicIdenticonTile$Tiles$27  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass27 extends Lambda implements hd1<Path, x54, te4> {
            public static final AnonymousClass27 INSTANCE = new AnonymousClass27();

            public AnonymousClass27() {
                super(2);
            }

            @Override // defpackage.hd1
            public /* bridge */ /* synthetic */ te4 invoke(Path path, x54 x54Var) {
                invoke2(path, x54Var);
                return te4.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(Path path, x54 x54Var) {
                fs1.f(path, "path");
                fs1.f(x54Var, "meas");
                wp2.c(path, 0, 0, x54Var.m(), 0, x54Var.l(), x54Var.c());
                wp2.a(path, x54Var.l(), x54Var.c(), x54Var.j(), x54Var.e(), x54Var.l(), x54Var.d(), x54Var.k(), x54Var.e());
                wp2.c(path, x54Var.m(), x54Var.f(), 0, x54Var.f(), x54Var.l(), x54Var.d());
            }
        }

        /* compiled from: ClassicIdenticonTile.kt */
        /* renamed from: com.lelloman.identicon.drawable.ClassicIdenticonTile$Tiles$28  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass28 extends Lambda implements hd1<Path, x54, te4> {
            public static final AnonymousClass28 INSTANCE = new AnonymousClass28();

            public AnonymousClass28() {
                super(2);
            }

            @Override // defpackage.hd1
            public /* bridge */ /* synthetic */ te4 invoke(Path path, x54 x54Var) {
                invoke2(path, x54Var);
                return te4.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(Path path, x54 x54Var) {
                fs1.f(path, "path");
                fs1.f(x54Var, "meas");
                wp2.c(path, 0, 0, x54Var.m(), x54Var.e(), x54Var.m(), x54Var.f());
            }
        }

        /* compiled from: ClassicIdenticonTile.kt */
        /* renamed from: com.lelloman.identicon.drawable.ClassicIdenticonTile$Tiles$29  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass29 extends Lambda implements hd1<Path, x54, te4> {
            public static final AnonymousClass29 INSTANCE = new AnonymousClass29();

            public AnonymousClass29() {
                super(2);
            }

            @Override // defpackage.hd1
            public /* bridge */ /* synthetic */ te4 invoke(Path path, x54 x54Var) {
                invoke2(path, x54Var);
                return te4.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(Path path, x54 x54Var) {
                fs1.f(path, "path");
                fs1.f(x54Var, "meas");
                wp2.c(path, 0, 0, x54Var.l(), x54Var.c(), x54Var.j(), x54Var.e());
                wp2.c(path, x54Var.m(), 0, x54Var.k(), x54Var.e(), x54Var.l(), x54Var.c());
                wp2.c(path, x54Var.m(), x54Var.f(), x54Var.l(), x54Var.d(), x54Var.k(), x54Var.e());
                wp2.c(path, 0, x54Var.f(), x54Var.j(), x54Var.e(), x54Var.l(), x54Var.d());
            }
        }

        /* compiled from: ClassicIdenticonTile.kt */
        /* renamed from: com.lelloman.identicon.drawable.ClassicIdenticonTile$Tiles$3  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass3 extends Lambda implements hd1<Path, x54, te4> {
            public static final AnonymousClass3 INSTANCE = new AnonymousClass3();

            public AnonymousClass3() {
                super(2);
            }

            @Override // defpackage.hd1
            public /* bridge */ /* synthetic */ te4 invoke(Path path, x54 x54Var) {
                invoke2(path, x54Var);
                return te4.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(Path path, x54 x54Var) {
                fs1.f(path, "path");
                fs1.f(x54Var, "meas");
                wp2.c(path, x54Var.l(), 0, 0, x54Var.f(), x54Var.m(), x54Var.f());
            }
        }

        /* compiled from: ClassicIdenticonTile.kt */
        /* renamed from: com.lelloman.identicon.drawable.ClassicIdenticonTile$Tiles$30  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass30 extends Lambda implements hd1<Path, x54, te4> {
            public static final AnonymousClass30 INSTANCE = new AnonymousClass30();

            public AnonymousClass30() {
                super(2);
            }

            @Override // defpackage.hd1
            public /* bridge */ /* synthetic */ te4 invoke(Path path, x54 x54Var) {
                invoke2(path, x54Var);
                return te4.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(Path path, x54 x54Var) {
                fs1.f(path, "path");
                fs1.f(x54Var, "meas");
                wp2.c(path, 0, 0, x54Var.m(), 0, 0, x54Var.e());
            }
        }

        /* compiled from: ClassicIdenticonTile.kt */
        /* renamed from: com.lelloman.identicon.drawable.ClassicIdenticonTile$Tiles$31  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass31 extends Lambda implements hd1<Path, x54, te4> {
            public static final AnonymousClass31 INSTANCE = new AnonymousClass31();

            public AnonymousClass31() {
                super(2);
            }

            @Override // defpackage.hd1
            public /* bridge */ /* synthetic */ te4 invoke(Path path, x54 x54Var) {
                invoke2(path, x54Var);
                return te4.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(Path path, x54 x54Var) {
                fs1.f(path, "path");
                fs1.f(x54Var, "meas");
                wp2.a(path, 0, x54Var.e(), x54Var.l(), x54Var.c(), x54Var.m(), x54Var.e(), x54Var.l(), x54Var.d());
            }
        }

        /* compiled from: ClassicIdenticonTile.kt */
        /* renamed from: com.lelloman.identicon.drawable.ClassicIdenticonTile$Tiles$32  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass32 extends Lambda implements hd1<Path, x54, te4> {
            public static final AnonymousClass32 INSTANCE = new AnonymousClass32();

            public AnonymousClass32() {
                super(2);
            }

            @Override // defpackage.hd1
            public /* bridge */ /* synthetic */ te4 invoke(Path path, x54 x54Var) {
                invoke2(path, x54Var);
                return te4.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(Path path, x54 x54Var) {
                fs1.f(path, "path");
                fs1.f(x54Var, "meas");
                wp2.c(path, 0, 0, x54Var.l(), x54Var.e(), 0, x54Var.f());
                wp2.c(path, x54Var.m(), 0, x54Var.l(), x54Var.e(), x54Var.m(), x54Var.f());
            }
        }

        /* compiled from: ClassicIdenticonTile.kt */
        /* renamed from: com.lelloman.identicon.drawable.ClassicIdenticonTile$Tiles$4  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass4 extends Lambda implements hd1<Path, x54, te4> {
            public static final AnonymousClass4 INSTANCE = new AnonymousClass4();

            public AnonymousClass4() {
                super(2);
            }

            @Override // defpackage.hd1
            public /* bridge */ /* synthetic */ te4 invoke(Path path, x54 x54Var) {
                invoke2(path, x54Var);
                return te4.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(Path path, x54 x54Var) {
                fs1.f(path, "path");
                fs1.f(x54Var, "meas");
                wp2.b(path, 0, 0, x54Var.l(), x54Var.f());
            }
        }

        /* compiled from: ClassicIdenticonTile.kt */
        /* renamed from: com.lelloman.identicon.drawable.ClassicIdenticonTile$Tiles$5  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass5 extends Lambda implements hd1<Path, x54, te4> {
            public static final AnonymousClass5 INSTANCE = new AnonymousClass5();

            public AnonymousClass5() {
                super(2);
            }

            @Override // defpackage.hd1
            public /* bridge */ /* synthetic */ te4 invoke(Path path, x54 x54Var) {
                invoke2(path, x54Var);
                return te4.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(Path path, x54 x54Var) {
                fs1.f(path, "path");
                fs1.f(x54Var, "meas");
                wp2.a(path, x54Var.l(), 0, x54Var.m(), x54Var.e(), x54Var.l(), x54Var.f(), 0, x54Var.e());
            }
        }

        /* compiled from: ClassicIdenticonTile.kt */
        /* renamed from: com.lelloman.identicon.drawable.ClassicIdenticonTile$Tiles$6  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass6 extends Lambda implements hd1<Path, x54, te4> {
            public static final AnonymousClass6 INSTANCE = new AnonymousClass6();

            public AnonymousClass6() {
                super(2);
            }

            @Override // defpackage.hd1
            public /* bridge */ /* synthetic */ te4 invoke(Path path, x54 x54Var) {
                invoke2(path, x54Var);
                return te4.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(Path path, x54 x54Var) {
                fs1.f(path, "path");
                fs1.f(x54Var, "meas");
                wp2.a(path, 0, 0, x54Var.m(), x54Var.e(), x54Var.m(), x54Var.f(), x54Var.l(), x54Var.f());
            }
        }

        /* compiled from: ClassicIdenticonTile.kt */
        /* renamed from: com.lelloman.identicon.drawable.ClassicIdenticonTile$Tiles$7  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass7 extends Lambda implements hd1<Path, x54, te4> {
            public static final AnonymousClass7 INSTANCE = new AnonymousClass7();

            public AnonymousClass7() {
                super(2);
            }

            @Override // defpackage.hd1
            public /* bridge */ /* synthetic */ te4 invoke(Path path, x54 x54Var) {
                invoke2(path, x54Var);
                return te4.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(Path path, x54 x54Var) {
                fs1.f(path, "path");
                fs1.f(x54Var, "meas");
                wp2.c(path, x54Var.l(), 0, x54Var.k(), x54Var.e(), x54Var.j(), x54Var.e());
                wp2.c(path, 0, x54Var.f(), x54Var.j(), x54Var.e(), x54Var.l(), x54Var.f());
                wp2.c(path, x54Var.m(), x54Var.f(), x54Var.k(), x54Var.e(), x54Var.l(), x54Var.f());
            }
        }

        /* compiled from: ClassicIdenticonTile.kt */
        /* renamed from: com.lelloman.identicon.drawable.ClassicIdenticonTile$Tiles$8  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass8 extends Lambda implements hd1<Path, x54, te4> {
            public static final AnonymousClass8 INSTANCE = new AnonymousClass8();

            public AnonymousClass8() {
                super(2);
            }

            @Override // defpackage.hd1
            public /* bridge */ /* synthetic */ te4 invoke(Path path, x54 x54Var) {
                invoke2(path, x54Var);
                return te4.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(Path path, x54 x54Var) {
                fs1.f(path, "path");
                fs1.f(x54Var, "meas");
                wp2.c(path, 0, 0, x54Var.m(), x54Var.e(), x54Var.l(), x54Var.f());
            }
        }

        /* compiled from: ClassicIdenticonTile.kt */
        /* renamed from: com.lelloman.identicon.drawable.ClassicIdenticonTile$Tiles$9  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass9 extends Lambda implements hd1<Path, x54, te4> {
            public static final AnonymousClass9 INSTANCE = new AnonymousClass9();

            public AnonymousClass9() {
                super(2);
            }

            @Override // defpackage.hd1
            public /* bridge */ /* synthetic */ te4 invoke(Path path, x54 x54Var) {
                invoke2(path, x54Var);
                return te4.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(Path path, x54 x54Var) {
                fs1.f(path, "path");
                fs1.f(x54Var, "meas");
                wp2.b(path, x54Var.j(), x54Var.c(), x54Var.k(), x54Var.d());
            }
        }

        Tiles(hd1 hd1Var) {
            this.tileDrawer = new w54(hd1Var);
        }

        public final void draw(Canvas canvas, x54 x54Var, int i, Paint paint, Paint paint2) {
            fs1.f(canvas, "canvas");
            fs1.f(x54Var, "measures");
            fs1.f(paint, "bgPaint");
            fs1.f(paint2, "fgPaint");
            this.tileDrawer.a(canvas, x54Var, i, paint, paint2);
        }
    }

    public final Tiles[] a() {
        return b;
    }
}
