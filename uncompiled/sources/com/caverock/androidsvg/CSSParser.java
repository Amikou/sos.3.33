package com.caverock.androidsvg;

import androidx.media3.common.PlaybackException;
import com.caverock.androidsvg.SVG;
import com.caverock.androidsvg.SVGParser;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.slf4j.Marker;

/* loaded from: classes.dex */
public class CSSParser {
    public MediaType a;
    public Source b;
    public boolean c;

    /* loaded from: classes.dex */
    public enum AttribOp {
        EXISTS,
        EQUALS,
        INCLUDES,
        DASHMATCH
    }

    /* loaded from: classes.dex */
    public enum Combinator {
        DESCENDANT,
        CHILD,
        FOLLOWS
    }

    /* loaded from: classes.dex */
    public enum MediaType {
        all,
        aural,
        braille,
        embossed,
        handheld,
        print,
        projection,
        screen,
        speech,
        tty,
        tv
    }

    /* loaded from: classes.dex */
    public enum PseudoClassIdents {
        target,
        root,
        nth_child,
        nth_last_child,
        nth_of_type,
        nth_last_of_type,
        first_child,
        last_child,
        first_of_type,
        last_of_type,
        only_child,
        only_of_type,
        empty,
        not,
        lang,
        link,
        visited,
        hover,
        active,
        focus,
        enabled,
        disabled,
        checked,
        indeterminate,
        UNSUPPORTED;
        
        public static final Map<String, PseudoClassIdents> a = new HashMap();

        static {
            PseudoClassIdents[] values;
            for (PseudoClassIdents pseudoClassIdents : values()) {
                if (pseudoClassIdents != UNSUPPORTED) {
                    a.put(pseudoClassIdents.name().replace('_', '-'), pseudoClassIdents);
                }
            }
        }

        public static PseudoClassIdents fromString(String str) {
            PseudoClassIdents pseudoClassIdents = a.get(str);
            return pseudoClassIdents != null ? pseudoClassIdents : UNSUPPORTED;
        }
    }

    /* loaded from: classes.dex */
    public enum Source {
        Document,
        RenderOptions
    }

    /* loaded from: classes.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;
        public static final /* synthetic */ int[] b;

        static {
            int[] iArr = new int[PseudoClassIdents.values().length];
            b = iArr;
            try {
                iArr[PseudoClassIdents.first_child.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                b[PseudoClassIdents.last_child.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                b[PseudoClassIdents.only_child.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                b[PseudoClassIdents.first_of_type.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                b[PseudoClassIdents.last_of_type.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                b[PseudoClassIdents.only_of_type.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                b[PseudoClassIdents.root.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                b[PseudoClassIdents.empty.ordinal()] = 8;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                b[PseudoClassIdents.nth_child.ordinal()] = 9;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                b[PseudoClassIdents.nth_last_child.ordinal()] = 10;
            } catch (NoSuchFieldError unused10) {
            }
            try {
                b[PseudoClassIdents.nth_of_type.ordinal()] = 11;
            } catch (NoSuchFieldError unused11) {
            }
            try {
                b[PseudoClassIdents.nth_last_of_type.ordinal()] = 12;
            } catch (NoSuchFieldError unused12) {
            }
            try {
                b[PseudoClassIdents.not.ordinal()] = 13;
            } catch (NoSuchFieldError unused13) {
            }
            try {
                b[PseudoClassIdents.target.ordinal()] = 14;
            } catch (NoSuchFieldError unused14) {
            }
            try {
                b[PseudoClassIdents.lang.ordinal()] = 15;
            } catch (NoSuchFieldError unused15) {
            }
            try {
                b[PseudoClassIdents.link.ordinal()] = 16;
            } catch (NoSuchFieldError unused16) {
            }
            try {
                b[PseudoClassIdents.visited.ordinal()] = 17;
            } catch (NoSuchFieldError unused17) {
            }
            try {
                b[PseudoClassIdents.hover.ordinal()] = 18;
            } catch (NoSuchFieldError unused18) {
            }
            try {
                b[PseudoClassIdents.active.ordinal()] = 19;
            } catch (NoSuchFieldError unused19) {
            }
            try {
                b[PseudoClassIdents.focus.ordinal()] = 20;
            } catch (NoSuchFieldError unused20) {
            }
            try {
                b[PseudoClassIdents.enabled.ordinal()] = 21;
            } catch (NoSuchFieldError unused21) {
            }
            try {
                b[PseudoClassIdents.disabled.ordinal()] = 22;
            } catch (NoSuchFieldError unused22) {
            }
            try {
                b[PseudoClassIdents.checked.ordinal()] = 23;
            } catch (NoSuchFieldError unused23) {
            }
            try {
                b[PseudoClassIdents.indeterminate.ordinal()] = 24;
            } catch (NoSuchFieldError unused24) {
            }
            int[] iArr2 = new int[AttribOp.values().length];
            a = iArr2;
            try {
                iArr2[AttribOp.EQUALS.ordinal()] = 1;
            } catch (NoSuchFieldError unused25) {
            }
            try {
                a[AttribOp.INCLUDES.ordinal()] = 2;
            } catch (NoSuchFieldError unused26) {
            }
            try {
                a[AttribOp.DASHMATCH.ordinal()] = 3;
            } catch (NoSuchFieldError unused27) {
            }
        }
    }

    /* loaded from: classes.dex */
    public static class b {
        public final String a;
        public final AttribOp b;
        public final String c;

        public b(String str, AttribOp attribOp, String str2) {
            this.a = str;
            this.b = attribOp;
            this.c = str2;
        }
    }

    /* loaded from: classes.dex */
    public static class c extends SVGParser.g {

        /* loaded from: classes.dex */
        public static class a {
            public int a;
            public int b;

            public a(int i, int i2) {
                this.a = i;
                this.b = i2;
            }
        }

        public c(String str) {
            super(str.replaceAll("(?s)/\\*.*?\\*/", ""));
        }

        public final int C(int i) {
            if (i < 48 || i > 57) {
                int i2 = 65;
                if (i < 65 || i > 70) {
                    i2 = 97;
                    if (i < 97 || i > 102) {
                        return -1;
                    }
                }
                return (i - i2) + 10;
            }
            return i - 48;
        }

        public final a D() throws CSSParseException {
            xr1 xr1Var;
            a aVar;
            if (h()) {
                return null;
            }
            int i = this.b;
            if (f('(')) {
                A();
                int i2 = 1;
                if (g("odd")) {
                    aVar = new a(2, 1);
                } else {
                    if (g("even")) {
                        aVar = new a(2, 0);
                    } else {
                        int i3 = (!f('+') && f('-')) ? -1 : 1;
                        xr1 c = xr1.c(this.a, this.b, this.c, false);
                        if (c != null) {
                            this.b = c.a();
                        }
                        if (f('n') || f('N')) {
                            if (c == null) {
                                c = new xr1(1L, this.b);
                            }
                            A();
                            boolean f = f('+');
                            if (!f && (f = f('-'))) {
                                i2 = -1;
                            }
                            if (f) {
                                A();
                                xr1Var = xr1.c(this.a, this.b, this.c, false);
                                if (xr1Var != null) {
                                    this.b = xr1Var.a();
                                } else {
                                    this.b = i;
                                    return null;
                                }
                            } else {
                                xr1Var = null;
                            }
                            int i4 = i2;
                            i2 = i3;
                            i3 = i4;
                        } else {
                            xr1Var = c;
                            c = null;
                        }
                        aVar = new a(c == null ? 0 : i2 * c.d(), xr1Var != null ? i3 * xr1Var.d() : 0);
                    }
                }
                A();
                if (f(')')) {
                    return aVar;
                }
                this.b = i;
                return null;
            }
            return null;
        }

        public final String E() {
            if (h()) {
                return null;
            }
            String q = q();
            return q != null ? q : H();
        }

        public String F() {
            int C;
            if (h()) {
                return null;
            }
            char charAt = this.a.charAt(this.b);
            if (charAt == '\'' || charAt == '\"') {
                StringBuilder sb = new StringBuilder();
                this.b++;
                int intValue = l().intValue();
                while (intValue != -1 && intValue != charAt) {
                    if (intValue == 92) {
                        intValue = l().intValue();
                        if (intValue != -1) {
                            if (intValue != 10 && intValue != 13 && intValue != 12) {
                                int C2 = C(intValue);
                                if (C2 != -1) {
                                    for (int i = 1; i <= 5 && (C = C((intValue = l().intValue()))) != -1; i++) {
                                        C2 = (C2 * 16) + C;
                                    }
                                    sb.append((char) C2);
                                }
                            } else {
                                intValue = l().intValue();
                            }
                        }
                    }
                    sb.append((char) intValue);
                    intValue = l().intValue();
                }
                return sb.toString();
            }
            return null;
        }

        public final List<String> G() throws CSSParseException {
            if (h()) {
                return null;
            }
            int i = this.b;
            if (f('(')) {
                A();
                ArrayList arrayList = null;
                do {
                    String H = H();
                    if (H == null) {
                        this.b = i;
                        return null;
                    }
                    if (arrayList == null) {
                        arrayList = new ArrayList();
                    }
                    arrayList.add(H);
                    A();
                } while (z());
                if (f(')')) {
                    return arrayList;
                }
                this.b = i;
                return null;
            }
            return null;
        }

        public String H() {
            int P = P();
            int i = this.b;
            if (P == i) {
                return null;
            }
            String substring = this.a.substring(i, P);
            this.b = P;
            return substring;
        }

        public String I() {
            char charAt;
            int C;
            StringBuilder sb = new StringBuilder();
            while (!h() && (charAt = this.a.charAt(this.b)) != '\'' && charAt != '\"' && charAt != '(' && charAt != ')' && !k(charAt) && !Character.isISOControl((int) charAt)) {
                this.b++;
                if (charAt == '\\') {
                    if (!h()) {
                        String str = this.a;
                        int i = this.b;
                        this.b = i + 1;
                        charAt = str.charAt(i);
                        if (charAt != '\n' && charAt != '\r' && charAt != '\f') {
                            int C2 = C(charAt);
                            if (C2 != -1) {
                                for (int i2 = 1; i2 <= 5 && !h() && (C = C(this.a.charAt(this.b))) != -1; i2++) {
                                    this.b++;
                                    C2 = (C2 * 16) + C;
                                }
                                sb.append((char) C2);
                            }
                        }
                    }
                }
                sb.append(charAt);
            }
            if (sb.length() == 0) {
                return null;
            }
            return sb.toString();
        }

        public String J() {
            if (h()) {
                return null;
            }
            int i = this.b;
            int charAt = this.a.charAt(i);
            int i2 = i;
            while (charAt != -1 && charAt != 59 && charAt != 125 && charAt != 33 && !j(charAt)) {
                if (!k(charAt)) {
                    i2 = this.b + 1;
                }
                charAt = a();
            }
            if (this.b > i) {
                return this.a.substring(i, i2);
            }
            this.b = i;
            return null;
        }

        public final List<o> K() throws CSSParseException {
            List<p> list;
            List<d> list2;
            if (h()) {
                return null;
            }
            int i = this.b;
            if (f('(')) {
                A();
                List<o> L = L();
                if (L == null) {
                    this.b = i;
                    return null;
                } else if (!f(')')) {
                    this.b = i;
                    return null;
                } else {
                    Iterator<o> it = L.iterator();
                    while (it.hasNext() && (list = it.next().a) != null) {
                        Iterator<p> it2 = list.iterator();
                        while (it2.hasNext() && (list2 = it2.next().d) != null) {
                            for (d dVar : list2) {
                                if (dVar instanceof g) {
                                    return null;
                                }
                            }
                        }
                    }
                    return L;
                }
            }
            return null;
        }

        public final List<o> L() throws CSSParseException {
            if (h()) {
                return null;
            }
            ArrayList arrayList = new ArrayList(1);
            o oVar = new o(null);
            while (!h() && M(oVar)) {
                if (z()) {
                    arrayList.add(oVar);
                    oVar = new o(null);
                }
            }
            if (!oVar.f()) {
                arrayList.add(oVar);
            }
            return arrayList;
        }

        /* JADX WARN: Removed duplicated region for block: B:16:0x0036  */
        /* JADX WARN: Removed duplicated region for block: B:17:0x003c  */
        /* JADX WARN: Removed duplicated region for block: B:23:0x0053  */
        /* JADX WARN: Removed duplicated region for block: B:79:0x012e  */
        /* JADX WARN: Removed duplicated region for block: B:81:0x0133  */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public boolean M(com.caverock.androidsvg.CSSParser.o r11) throws com.caverock.androidsvg.CSSParseException {
            /*
                Method dump skipped, instructions count: 310
                To view this dump change 'Code comments level' option to 'DEBUG'
            */
            throw new UnsupportedOperationException("Method not decompiled: com.caverock.androidsvg.CSSParser.c.M(com.caverock.androidsvg.CSSParser$o):boolean");
        }

        public String N() {
            if (h()) {
                return null;
            }
            int i = this.b;
            if (g("url(")) {
                A();
                String F = F();
                if (F == null) {
                    F = I();
                }
                if (F == null) {
                    this.b = i;
                    return null;
                }
                A();
                if (h() || g(")")) {
                    return F;
                }
                this.b = i;
                return null;
            }
            return null;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public final void O(o oVar, p pVar) throws CSSParseException {
            d eVar;
            e eVar2;
            String H = H();
            if (H != null) {
                PseudoClassIdents fromString = PseudoClassIdents.fromString(H);
                switch (a.b[fromString.ordinal()]) {
                    case 1:
                        eVar = new e(0, 1, true, false, null);
                        oVar.b();
                        break;
                    case 2:
                        eVar = new e(0, 1, false, false, null);
                        oVar.b();
                        break;
                    case 3:
                        eVar = new i(false, null);
                        oVar.b();
                        break;
                    case 4:
                        eVar = new e(0, 1, true, true, pVar.b);
                        oVar.b();
                        break;
                    case 5:
                        eVar = new e(0, 1, false, true, pVar.b);
                        oVar.b();
                        break;
                    case 6:
                        eVar = new i(true, pVar.b);
                        oVar.b();
                        break;
                    case 7:
                        eVar = new j(null);
                        oVar.b();
                        break;
                    case 8:
                        eVar = new f(null);
                        oVar.b();
                        break;
                    case 9:
                    case 10:
                    case 11:
                    case 12:
                        boolean z = fromString == PseudoClassIdents.nth_child || fromString == PseudoClassIdents.nth_of_type;
                        boolean z2 = fromString == PseudoClassIdents.nth_of_type || fromString == PseudoClassIdents.nth_last_of_type;
                        a D = D();
                        if (D != null) {
                            e eVar3 = new e(D.a, D.b, z, z2, pVar.b);
                            oVar.b();
                            eVar2 = eVar3;
                            eVar = eVar2;
                            break;
                        } else {
                            throw new CSSParseException("Invalid or missing parameter section for pseudo class: " + H);
                        }
                        break;
                    case 13:
                        List<o> K = K();
                        if (K != null) {
                            g gVar = new g(K);
                            oVar.b = gVar.b();
                            eVar2 = gVar;
                            eVar = eVar2;
                            break;
                        } else {
                            throw new CSSParseException("Invalid or missing parameter section for pseudo class: " + H);
                        }
                    case 14:
                        eVar = new k(null);
                        oVar.b();
                        break;
                    case 15:
                        G();
                        eVar = new h(H);
                        oVar.b();
                        break;
                    case 16:
                    case 17:
                    case 18:
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                    case 23:
                    case 24:
                        eVar = new h(H);
                        oVar.b();
                        break;
                    default:
                        throw new CSSParseException("Unsupported pseudo class: " + H);
                }
                pVar.b(eVar);
                return;
            }
            throw new CSSParseException("Invalid pseudo class");
        }

        public final int P() {
            int i;
            if (h()) {
                return this.b;
            }
            int i2 = this.b;
            int charAt = this.a.charAt(i2);
            if (charAt == 45) {
                charAt = a();
            }
            if ((charAt < 65 || charAt > 90) && ((charAt < 97 || charAt > 122) && charAt != 95)) {
                i = i2;
            } else {
                int a2 = a();
                while (true) {
                    if ((a2 < 65 || a2 > 90) && ((a2 < 97 || a2 > 122) && !((a2 >= 48 && a2 <= 57) || a2 == 45 || a2 == 95))) {
                        break;
                    }
                    a2 = a();
                }
                i = this.b;
            }
            this.b = i2;
            return i;
        }
    }

    /* loaded from: classes.dex */
    public interface d {
        boolean a(m mVar, SVG.j0 j0Var);
    }

    /* loaded from: classes.dex */
    public static class e implements d {
        public int a;
        public int b;
        public boolean c;
        public boolean d;
        public String e;

        public e(int i, int i2, boolean z, boolean z2, String str) {
            this.a = i;
            this.b = i2;
            this.c = z;
            this.d = z2;
            this.e = str;
        }

        @Override // com.caverock.androidsvg.CSSParser.d
        public boolean a(m mVar, SVG.j0 j0Var) {
            int i;
            int i2;
            String o = (this.d && this.e == null) ? j0Var.o() : this.e;
            SVG.h0 h0Var = j0Var.b;
            if (h0Var != null) {
                Iterator<SVG.l0> it = h0Var.a().iterator();
                i = 0;
                i2 = 0;
                while (it.hasNext()) {
                    SVG.j0 j0Var2 = (SVG.j0) it.next();
                    if (j0Var2 == j0Var) {
                        i = i2;
                    }
                    if (o == null || j0Var2.o().equals(o)) {
                        i2++;
                    }
                }
            } else {
                i = 0;
                i2 = 1;
            }
            int i3 = this.c ? i + 1 : i2 - i;
            int i4 = this.a;
            if (i4 == 0) {
                return i3 == this.b;
            }
            int i5 = this.b;
            if ((i3 - i5) % i4 == 0) {
                return Integer.signum(i3 - i5) == 0 || Integer.signum(i3 - this.b) == Integer.signum(this.a);
            }
            return false;
        }

        public String toString() {
            String str = this.c ? "" : "last-";
            return this.d ? String.format("nth-%schild(%dn%+d of type <%s>)", str, Integer.valueOf(this.a), Integer.valueOf(this.b), this.e) : String.format("nth-%schild(%dn%+d)", str, Integer.valueOf(this.a), Integer.valueOf(this.b));
        }
    }

    /* loaded from: classes.dex */
    public static class f implements d {
        public f() {
        }

        @Override // com.caverock.androidsvg.CSSParser.d
        public boolean a(m mVar, SVG.j0 j0Var) {
            return !(j0Var instanceof SVG.h0) || ((SVG.h0) j0Var).a().size() == 0;
        }

        public String toString() {
            return "empty";
        }

        public /* synthetic */ f(a aVar) {
            this();
        }
    }

    /* loaded from: classes.dex */
    public static class g implements d {
        public List<o> a;

        public g(List<o> list) {
            this.a = list;
        }

        @Override // com.caverock.androidsvg.CSSParser.d
        public boolean a(m mVar, SVG.j0 j0Var) {
            for (o oVar : this.a) {
                if (CSSParser.l(mVar, oVar, j0Var)) {
                    return false;
                }
            }
            return true;
        }

        public int b() {
            int i = Integer.MIN_VALUE;
            for (o oVar : this.a) {
                int i2 = oVar.b;
                if (i2 > i) {
                    i = i2;
                }
            }
            return i;
        }

        public String toString() {
            return "not(" + this.a + ")";
        }
    }

    /* loaded from: classes.dex */
    public static class h implements d {
        public String a;

        public h(String str) {
            this.a = str;
        }

        @Override // com.caverock.androidsvg.CSSParser.d
        public boolean a(m mVar, SVG.j0 j0Var) {
            return false;
        }

        public String toString() {
            return this.a;
        }
    }

    /* loaded from: classes.dex */
    public static class i implements d {
        public boolean a;
        public String b;

        public i(boolean z, String str) {
            this.a = z;
            this.b = str;
        }

        @Override // com.caverock.androidsvg.CSSParser.d
        public boolean a(m mVar, SVG.j0 j0Var) {
            int i;
            String o = (this.a && this.b == null) ? j0Var.o() : this.b;
            SVG.h0 h0Var = j0Var.b;
            if (h0Var != null) {
                Iterator<SVG.l0> it = h0Var.a().iterator();
                i = 0;
                while (it.hasNext()) {
                    SVG.j0 j0Var2 = (SVG.j0) it.next();
                    if (o == null || j0Var2.o().equals(o)) {
                        i++;
                    }
                }
            } else {
                i = 1;
            }
            return i == 1;
        }

        public String toString() {
            return this.a ? String.format("only-of-type <%s>", this.b) : String.format("only-child", new Object[0]);
        }
    }

    /* loaded from: classes.dex */
    public static class j implements d {
        public j() {
        }

        @Override // com.caverock.androidsvg.CSSParser.d
        public boolean a(m mVar, SVG.j0 j0Var) {
            return j0Var.b == null;
        }

        public String toString() {
            return "root";
        }

        public /* synthetic */ j(a aVar) {
            this();
        }
    }

    /* loaded from: classes.dex */
    public static class k implements d {
        public k() {
        }

        @Override // com.caverock.androidsvg.CSSParser.d
        public boolean a(m mVar, SVG.j0 j0Var) {
            return mVar != null && j0Var == mVar.a;
        }

        public String toString() {
            return "target";
        }

        public /* synthetic */ k(a aVar) {
            this();
        }
    }

    /* loaded from: classes.dex */
    public static class l {
        public o a;
        public SVG.Style b;
        public Source c;

        public l(o oVar, SVG.Style style, Source source) {
            this.a = null;
            this.b = null;
            this.a = oVar;
            this.b = style;
            this.c = source;
        }

        public String toString() {
            return String.valueOf(this.a) + " {...} (src=" + this.c + ")";
        }
    }

    /* loaded from: classes.dex */
    public static class m {
        public SVG.j0 a;

        public String toString() {
            SVG.j0 j0Var = this.a;
            return j0Var != null ? String.format("<%s id=\"%s\">", j0Var.o(), this.a.c) : "";
        }
    }

    /* loaded from: classes.dex */
    public static class n {
        public List<l> a = null;

        public void a(l lVar) {
            if (this.a == null) {
                this.a = new ArrayList();
            }
            for (int i = 0; i < this.a.size(); i++) {
                if (this.a.get(i).a.b > lVar.a.b) {
                    this.a.add(i, lVar);
                    return;
                }
            }
            this.a.add(lVar);
        }

        public void b(n nVar) {
            if (nVar.a == null) {
                return;
            }
            if (this.a == null) {
                this.a = new ArrayList(nVar.a.size());
            }
            for (l lVar : nVar.a) {
                a(lVar);
            }
        }

        public List<l> c() {
            return this.a;
        }

        public boolean d() {
            List<l> list = this.a;
            return list == null || list.isEmpty();
        }

        public void e(Source source) {
            List<l> list = this.a;
            if (list == null) {
                return;
            }
            Iterator<l> it = list.iterator();
            while (it.hasNext()) {
                if (it.next().c == source) {
                    it.remove();
                }
            }
        }

        public int f() {
            List<l> list = this.a;
            if (list != null) {
                return list.size();
            }
            return 0;
        }

        public String toString() {
            if (this.a == null) {
                return "";
            }
            StringBuilder sb = new StringBuilder();
            for (l lVar : this.a) {
                sb.append(lVar.toString());
                sb.append('\n');
            }
            return sb.toString();
        }
    }

    /* loaded from: classes.dex */
    public static class p {
        public Combinator a;
        public String b;
        public List<b> c = null;
        public List<d> d = null;

        public p(Combinator combinator, String str) {
            this.a = null;
            this.b = null;
            this.a = combinator == null ? Combinator.DESCENDANT : combinator;
            this.b = str;
        }

        public void a(String str, AttribOp attribOp, String str2) {
            if (this.c == null) {
                this.c = new ArrayList();
            }
            this.c.add(new b(str, attribOp, str2));
        }

        public void b(d dVar) {
            if (this.d == null) {
                this.d = new ArrayList();
            }
            this.d.add(dVar);
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            Combinator combinator = this.a;
            if (combinator == Combinator.CHILD) {
                sb.append("> ");
            } else if (combinator == Combinator.FOLLOWS) {
                sb.append("+ ");
            }
            String str = this.b;
            if (str == null) {
                str = Marker.ANY_MARKER;
            }
            sb.append(str);
            List<b> list = this.c;
            if (list != null) {
                for (b bVar : list) {
                    sb.append('[');
                    sb.append(bVar.a);
                    int i = a.a[bVar.b.ordinal()];
                    if (i == 1) {
                        sb.append('=');
                        sb.append(bVar.c);
                    } else if (i == 2) {
                        sb.append("~=");
                        sb.append(bVar.c);
                    } else if (i == 3) {
                        sb.append("|=");
                        sb.append(bVar.c);
                    }
                    sb.append(']');
                }
            }
            List<d> list2 = this.d;
            if (list2 != null) {
                for (d dVar : list2) {
                    sb.append(':');
                    sb.append(dVar);
                }
            }
            return sb.toString();
        }
    }

    public CSSParser(Source source) {
        this(MediaType.screen, source);
    }

    public static int a(List<SVG.h0> list, int i2, SVG.j0 j0Var) {
        int i3 = 0;
        if (i2 < 0) {
            return 0;
        }
        SVG.h0 h0Var = list.get(i2);
        SVG.h0 h0Var2 = j0Var.b;
        if (h0Var != h0Var2) {
            return -1;
        }
        for (SVG.l0 l0Var : h0Var2.a()) {
            if (l0Var == j0Var) {
                return i3;
            }
            i3++;
        }
        return -1;
    }

    public static boolean b(String str, MediaType mediaType) {
        c cVar = new c(str);
        cVar.A();
        return c(h(cVar), mediaType);
    }

    public static boolean c(List<MediaType> list, MediaType mediaType) {
        for (MediaType mediaType2 : list) {
            if (mediaType2 == MediaType.all) {
                return true;
            }
            if (mediaType2 == mediaType) {
                return true;
            }
        }
        return false;
    }

    public static List<String> f(String str) {
        c cVar = new c(str);
        ArrayList arrayList = null;
        while (!cVar.h()) {
            String r = cVar.r();
            if (r != null) {
                if (arrayList == null) {
                    arrayList = new ArrayList();
                }
                arrayList.add(r);
                cVar.A();
            }
        }
        return arrayList;
    }

    public static List<MediaType> h(c cVar) {
        String w;
        ArrayList arrayList = new ArrayList();
        while (!cVar.h() && (w = cVar.w()) != null) {
            try {
                arrayList.add(MediaType.valueOf(w));
            } catch (IllegalArgumentException unused) {
            }
            if (!cVar.z()) {
                break;
            }
        }
        return arrayList;
    }

    public static boolean k(m mVar, o oVar, int i2, List<SVG.h0> list, int i3, SVG.j0 j0Var) {
        p e2 = oVar.e(i2);
        if (n(mVar, e2, list, i3, j0Var)) {
            Combinator combinator = e2.a;
            if (combinator == Combinator.DESCENDANT) {
                if (i2 == 0) {
                    return true;
                }
                while (i3 >= 0) {
                    if (m(mVar, oVar, i2 - 1, list, i3)) {
                        return true;
                    }
                    i3--;
                }
                return false;
            } else if (combinator == Combinator.CHILD) {
                return m(mVar, oVar, i2 - 1, list, i3);
            } else {
                int a2 = a(list, i3, j0Var);
                if (a2 <= 0) {
                    return false;
                }
                return k(mVar, oVar, i2 - 1, list, i3, (SVG.j0) j0Var.b.a().get(a2 - 1));
            }
        }
        return false;
    }

    public static boolean l(m mVar, o oVar, SVG.j0 j0Var) {
        ArrayList arrayList = new ArrayList();
        for (SVG.h0 h0Var = j0Var.b; h0Var != null; h0Var = ((SVG.l0) h0Var).b) {
            arrayList.add(0, h0Var);
        }
        int size = arrayList.size() - 1;
        if (oVar.g() == 1) {
            return n(mVar, oVar.e(0), arrayList, size, j0Var);
        }
        return k(mVar, oVar, oVar.g() - 1, arrayList, size, j0Var);
    }

    public static boolean m(m mVar, o oVar, int i2, List<SVG.h0> list, int i3) {
        p e2 = oVar.e(i2);
        SVG.j0 j0Var = (SVG.j0) list.get(i3);
        if (n(mVar, e2, list, i3, j0Var)) {
            Combinator combinator = e2.a;
            if (combinator == Combinator.DESCENDANT) {
                if (i2 == 0) {
                    return true;
                }
                while (i3 > 0) {
                    i3--;
                    if (m(mVar, oVar, i2 - 1, list, i3)) {
                        return true;
                    }
                }
                return false;
            } else if (combinator == Combinator.CHILD) {
                return m(mVar, oVar, i2 - 1, list, i3 - 1);
            } else {
                int a2 = a(list, i3, j0Var);
                if (a2 <= 0) {
                    return false;
                }
                return k(mVar, oVar, i2 - 1, list, i3, (SVG.j0) j0Var.b.a().get(a2 - 1));
            }
        }
        return false;
    }

    public static boolean n(m mVar, p pVar, List<SVG.h0> list, int i2, SVG.j0 j0Var) {
        List<String> list2;
        String str = pVar.b;
        if (str == null || str.equals(j0Var.o().toLowerCase(Locale.US))) {
            List<b> list3 = pVar.c;
            if (list3 != null) {
                for (b bVar : list3) {
                    String str2 = bVar.a;
                    str2.hashCode();
                    if (!str2.equals("id")) {
                        if (!str2.equals("class") || (list2 = j0Var.g) == null || !list2.contains(bVar.c)) {
                            return false;
                        }
                    } else if (!bVar.c.equals(j0Var.c)) {
                        return false;
                    }
                }
            }
            List<d> list4 = pVar.d;
            if (list4 != null) {
                for (d dVar : list4) {
                    if (!dVar.a(mVar, j0Var)) {
                        return false;
                    }
                }
                return true;
            }
            return true;
        }
        return false;
    }

    public static void p(String str, Object... objArr) {
        String.format(str, objArr);
    }

    public n d(String str) {
        c cVar = new c(str);
        cVar.A();
        return j(cVar);
    }

    public final void e(n nVar, c cVar) throws CSSParseException {
        String H = cVar.H();
        cVar.A();
        if (H != null) {
            if (!this.c && H.equals("media")) {
                List<MediaType> h2 = h(cVar);
                if (cVar.f('{')) {
                    cVar.A();
                    if (c(h2, this.a)) {
                        this.c = true;
                        nVar.b(j(cVar));
                        this.c = false;
                    } else {
                        j(cVar);
                    }
                    if (!cVar.h() && !cVar.f('}')) {
                        throw new CSSParseException("Invalid @media rule: expected '}' at end of rule set");
                    }
                } else {
                    throw new CSSParseException("Invalid @media rule: missing rule set");
                }
            } else if (!this.c && H.equals("import")) {
                String N = cVar.N();
                if (N == null) {
                    N = cVar.F();
                }
                if (N != null) {
                    cVar.A();
                    h(cVar);
                    if (!cVar.h() && !cVar.f(';')) {
                        throw new CSSParseException("Invalid @media rule: expected '}' at end of rule set");
                    }
                    SVG.g();
                } else {
                    throw new CSSParseException("Invalid @import rule: expected string or url()");
                }
            } else {
                p("Ignoring @%s rule", H);
                o(cVar);
            }
            cVar.A();
            return;
        }
        throw new CSSParseException("Invalid '@' rule");
    }

    public final SVG.Style g(c cVar) throws CSSParseException {
        SVG.Style style = new SVG.Style();
        do {
            String H = cVar.H();
            cVar.A();
            if (cVar.f(':')) {
                cVar.A();
                String J = cVar.J();
                if (J != null) {
                    cVar.A();
                    if (cVar.f('!')) {
                        cVar.A();
                        if (cVar.g("important")) {
                            cVar.A();
                        } else {
                            throw new CSSParseException("Malformed rule set: found unexpected '!'");
                        }
                    }
                    cVar.f(';');
                    SVGParser.S0(style, H, J);
                    cVar.A();
                    if (cVar.h()) {
                        break;
                    }
                } else {
                    throw new CSSParseException("Expected property value");
                }
            } else {
                throw new CSSParseException("Expected ':'");
            }
        } while (!cVar.f('}'));
        return style;
    }

    public final boolean i(n nVar, c cVar) throws CSSParseException {
        List<o> L = cVar.L();
        if (L == null || L.isEmpty()) {
            return false;
        }
        if (cVar.f('{')) {
            cVar.A();
            SVG.Style g2 = g(cVar);
            cVar.A();
            for (o oVar : L) {
                nVar.a(new l(oVar, g2, this.b));
            }
            return true;
        }
        throw new CSSParseException("Malformed rule block: expected '{'");
    }

    public final n j(c cVar) {
        n nVar = new n();
        while (!cVar.h()) {
            try {
                if (!cVar.g("<!--") && !cVar.g("-->")) {
                    if (cVar.f('@')) {
                        e(nVar, cVar);
                    } else if (!i(nVar, cVar)) {
                        break;
                    }
                }
            } catch (CSSParseException e2) {
                StringBuilder sb = new StringBuilder();
                sb.append("CSS parser terminated early due to error: ");
                sb.append(e2.getMessage());
            }
        }
        return nVar;
    }

    public final void o(c cVar) {
        int i2 = 0;
        while (!cVar.h()) {
            int intValue = cVar.l().intValue();
            if (intValue == 59 && i2 == 0) {
                return;
            }
            if (intValue == 123) {
                i2++;
            } else if (intValue == 125 && i2 > 0 && i2 - 1 == 0) {
                return;
            }
        }
    }

    public CSSParser(MediaType mediaType, Source source) {
        this.a = null;
        this.b = null;
        this.c = false;
        this.a = mediaType;
        this.b = source;
    }

    /* loaded from: classes.dex */
    public static class o {
        public List<p> a;
        public int b;

        public o() {
            this.a = null;
            this.b = 0;
        }

        public void a(p pVar) {
            if (this.a == null) {
                this.a = new ArrayList();
            }
            this.a.add(pVar);
        }

        public void b() {
            this.b += 1000;
        }

        public void c() {
            this.b++;
        }

        public void d() {
            this.b += PlaybackException.CUSTOM_ERROR_CODE_BASE;
        }

        public p e(int i) {
            return this.a.get(i);
        }

        public boolean f() {
            List<p> list = this.a;
            return list == null || list.isEmpty();
        }

        public int g() {
            List<p> list = this.a;
            if (list == null) {
                return 0;
            }
            return list.size();
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            for (p pVar : this.a) {
                sb.append(pVar);
                sb.append(' ');
            }
            sb.append('[');
            sb.append(this.b);
            sb.append(']');
            return sb.toString();
        }

        public /* synthetic */ o(a aVar) {
            this();
        }
    }
}
