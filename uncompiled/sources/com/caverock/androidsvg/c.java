package com.caverock.androidsvg;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.DashPathEffect;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RadialGradient;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.os.Build;
import android.util.Base64;
import com.caverock.androidsvg.CSSParser;
import com.caverock.androidsvg.PreserveAspectRatio;
import com.caverock.androidsvg.SVG;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import com.github.mikephil.charting.utils.Utils;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;
import java.util.Stack;
import okhttp3.internal.http2.Http2Stream;
import okhttp3.internal.ws.RealWebSocket;

/* compiled from: SVGAndroidRenderer.java */
/* loaded from: classes.dex */
public class c {
    public static HashSet<String> i;
    public Canvas a;
    public float b;
    public SVG c;
    public h d;
    public Stack<h> e;
    public Stack<SVG.h0> f;
    public Stack<Matrix> g;
    public CSSParser.m h = null;

    /* compiled from: SVGAndroidRenderer.java */
    /* loaded from: classes.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;
        public static final /* synthetic */ int[] b;
        public static final /* synthetic */ int[] c;

        static {
            int[] iArr = new int[SVG.Style.LineJoin.values().length];
            c = iArr;
            try {
                iArr[SVG.Style.LineJoin.Miter.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                c[SVG.Style.LineJoin.Round.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                c[SVG.Style.LineJoin.Bevel.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            int[] iArr2 = new int[SVG.Style.LineCap.values().length];
            b = iArr2;
            try {
                iArr2[SVG.Style.LineCap.Butt.ordinal()] = 1;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                b[SVG.Style.LineCap.Round.ordinal()] = 2;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                b[SVG.Style.LineCap.Square.ordinal()] = 3;
            } catch (NoSuchFieldError unused6) {
            }
            int[] iArr3 = new int[PreserveAspectRatio.Alignment.values().length];
            a = iArr3;
            try {
                iArr3[PreserveAspectRatio.Alignment.xMidYMin.ordinal()] = 1;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                a[PreserveAspectRatio.Alignment.xMidYMid.ordinal()] = 2;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                a[PreserveAspectRatio.Alignment.xMidYMax.ordinal()] = 3;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                a[PreserveAspectRatio.Alignment.xMaxYMin.ordinal()] = 4;
            } catch (NoSuchFieldError unused10) {
            }
            try {
                a[PreserveAspectRatio.Alignment.xMaxYMid.ordinal()] = 5;
            } catch (NoSuchFieldError unused11) {
            }
            try {
                a[PreserveAspectRatio.Alignment.xMaxYMax.ordinal()] = 6;
            } catch (NoSuchFieldError unused12) {
            }
            try {
                a[PreserveAspectRatio.Alignment.xMinYMid.ordinal()] = 7;
            } catch (NoSuchFieldError unused13) {
            }
            try {
                a[PreserveAspectRatio.Alignment.xMinYMax.ordinal()] = 8;
            } catch (NoSuchFieldError unused14) {
            }
        }
    }

    /* compiled from: SVGAndroidRenderer.java */
    /* loaded from: classes.dex */
    public class b implements SVG.w {
        public float b;
        public float c;
        public boolean h;
        public List<C0080c> a = new ArrayList();
        public C0080c d = null;
        public boolean e = false;
        public boolean f = true;
        public int g = -1;

        public b(SVG.v vVar) {
            if (vVar == null) {
                return;
            }
            vVar.h(this);
            if (this.h) {
                this.d.b(this.a.get(this.g));
                this.a.set(this.g, this.d);
                this.h = false;
            }
            C0080c c0080c = this.d;
            if (c0080c != null) {
                this.a.add(c0080c);
            }
        }

        @Override // com.caverock.androidsvg.SVG.w
        public void a(float f, float f2, float f3, float f4) {
            this.d.a(f, f2);
            this.a.add(this.d);
            this.d = new C0080c(c.this, f3, f4, f3 - f, f4 - f2);
            this.h = false;
        }

        @Override // com.caverock.androidsvg.SVG.w
        public void b(float f, float f2) {
            if (this.h) {
                this.d.b(this.a.get(this.g));
                this.a.set(this.g, this.d);
                this.h = false;
            }
            C0080c c0080c = this.d;
            if (c0080c != null) {
                this.a.add(c0080c);
            }
            this.b = f;
            this.c = f2;
            this.d = new C0080c(c.this, f, f2, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON);
            this.g = this.a.size();
        }

        @Override // com.caverock.androidsvg.SVG.w
        public void c(float f, float f2, float f3, float f4, float f5, float f6) {
            if (this.f || this.e) {
                this.d.a(f, f2);
                this.a.add(this.d);
                this.e = false;
            }
            this.d = new C0080c(c.this, f5, f6, f5 - f3, f6 - f4);
            this.h = false;
        }

        @Override // com.caverock.androidsvg.SVG.w
        public void close() {
            this.a.add(this.d);
            e(this.b, this.c);
            this.h = true;
        }

        @Override // com.caverock.androidsvg.SVG.w
        public void d(float f, float f2, float f3, boolean z, boolean z2, float f4, float f5) {
            this.e = true;
            this.f = false;
            C0080c c0080c = this.d;
            c.m(c0080c.a, c0080c.b, f, f2, f3, z, z2, f4, f5, this);
            this.f = true;
            this.h = false;
        }

        @Override // com.caverock.androidsvg.SVG.w
        public void e(float f, float f2) {
            this.d.a(f, f2);
            this.a.add(this.d);
            c cVar = c.this;
            C0080c c0080c = this.d;
            this.d = new C0080c(cVar, f, f2, f - c0080c.a, f2 - c0080c.b);
            this.h = false;
        }

        public List<C0080c> f() {
            return this.a;
        }
    }

    /* compiled from: SVGAndroidRenderer.java */
    /* renamed from: com.caverock.androidsvg.c$c  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public class C0080c {
        public float a;
        public float b;
        public float c;
        public float d;
        public boolean e = false;

        public C0080c(c cVar, float f, float f2, float f3, float f4) {
            this.c = Utils.FLOAT_EPSILON;
            this.d = Utils.FLOAT_EPSILON;
            this.a = f;
            this.b = f2;
            double sqrt = Math.sqrt((f3 * f3) + (f4 * f4));
            if (sqrt != Utils.DOUBLE_EPSILON) {
                this.c = (float) (f3 / sqrt);
                this.d = (float) (f4 / sqrt);
            }
        }

        public void a(float f, float f2) {
            float f3 = f - this.a;
            float f4 = f2 - this.b;
            double sqrt = Math.sqrt((f3 * f3) + (f4 * f4));
            if (sqrt != Utils.DOUBLE_EPSILON) {
                f3 = (float) (f3 / sqrt);
                f4 = (float) (f4 / sqrt);
            }
            float f5 = this.c;
            if (f3 == (-f5) && f4 == (-this.d)) {
                this.e = true;
                this.c = -f4;
                this.d = f3;
                return;
            }
            this.c = f5 + f3;
            this.d += f4;
        }

        public void b(C0080c c0080c) {
            float f = c0080c.c;
            float f2 = this.c;
            if (f == (-f2)) {
                float f3 = c0080c.d;
                if (f3 == (-this.d)) {
                    this.e = true;
                    this.c = -f3;
                    this.d = c0080c.c;
                    return;
                }
            }
            this.c = f2 + f;
            this.d += c0080c.d;
        }

        public String toString() {
            return "(" + this.a + "," + this.b + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + this.c + "," + this.d + ")";
        }
    }

    /* compiled from: SVGAndroidRenderer.java */
    /* loaded from: classes.dex */
    public class d implements SVG.w {
        public Path a = new Path();
        public float b;
        public float c;

        public d(c cVar, SVG.v vVar) {
            if (vVar == null) {
                return;
            }
            vVar.h(this);
        }

        @Override // com.caverock.androidsvg.SVG.w
        public void a(float f, float f2, float f3, float f4) {
            this.a.quadTo(f, f2, f3, f4);
            this.b = f3;
            this.c = f4;
        }

        @Override // com.caverock.androidsvg.SVG.w
        public void b(float f, float f2) {
            this.a.moveTo(f, f2);
            this.b = f;
            this.c = f2;
        }

        @Override // com.caverock.androidsvg.SVG.w
        public void c(float f, float f2, float f3, float f4, float f5, float f6) {
            this.a.cubicTo(f, f2, f3, f4, f5, f6);
            this.b = f5;
            this.c = f6;
        }

        @Override // com.caverock.androidsvg.SVG.w
        public void close() {
            this.a.close();
        }

        @Override // com.caverock.androidsvg.SVG.w
        public void d(float f, float f2, float f3, boolean z, boolean z2, float f4, float f5) {
            c.m(this.b, this.c, f, f2, f3, z, z2, f4, f5, this);
            this.b = f4;
            this.c = f5;
        }

        @Override // com.caverock.androidsvg.SVG.w
        public void e(float f, float f2) {
            this.a.lineTo(f, f2);
            this.b = f;
            this.c = f2;
        }

        public Path f() {
            return this.a;
        }
    }

    /* compiled from: SVGAndroidRenderer.java */
    /* loaded from: classes.dex */
    public class e extends f {
        public Path d;

        public e(Path path, float f, float f2) {
            super(f, f2);
            this.d = path;
        }

        @Override // com.caverock.androidsvg.c.f, com.caverock.androidsvg.c.j
        public void b(String str) {
            if (c.this.g1()) {
                if (c.this.d.b) {
                    c.this.a.drawTextOnPath(str, this.d, this.a, this.b, c.this.d.d);
                }
                if (c.this.d.c) {
                    c.this.a.drawTextOnPath(str, this.d, this.a, this.b, c.this.d.e);
                }
            }
            this.a += c.this.d.d.measureText(str);
        }
    }

    /* compiled from: SVGAndroidRenderer.java */
    /* loaded from: classes.dex */
    public class f extends j {
        public float a;
        public float b;

        public f(float f, float f2) {
            super(c.this, null);
            this.a = f;
            this.b = f2;
        }

        @Override // com.caverock.androidsvg.c.j
        public void b(String str) {
            c.G("TextSequence render", new Object[0]);
            if (c.this.g1()) {
                if (c.this.d.b) {
                    c.this.a.drawText(str, this.a, this.b, c.this.d.d);
                }
                if (c.this.d.c) {
                    c.this.a.drawText(str, this.a, this.b, c.this.d.e);
                }
            }
            this.a += c.this.d.d.measureText(str);
        }
    }

    /* compiled from: SVGAndroidRenderer.java */
    /* loaded from: classes.dex */
    public class g extends j {
        public float a;
        public float b;
        public Path c;

        public g(float f, float f2, Path path) {
            super(c.this, null);
            this.a = f;
            this.b = f2;
            this.c = path;
        }

        @Override // com.caverock.androidsvg.c.j
        public boolean a(SVG.w0 w0Var) {
            if (w0Var instanceof SVG.x0) {
                c.h1("Using <textPath> elements in a clip path is not supported.", new Object[0]);
                return false;
            }
            return true;
        }

        @Override // com.caverock.androidsvg.c.j
        public void b(String str) {
            if (c.this.g1()) {
                Path path = new Path();
                c.this.d.d.getTextPath(str, 0, str.length(), this.a, this.b, path);
                this.c.addPath(path);
            }
            this.a += c.this.d.d.measureText(str);
        }
    }

    /* compiled from: SVGAndroidRenderer.java */
    /* loaded from: classes.dex */
    public class i extends j {
        public float a;
        public float b;
        public RectF c;

        public i(float f, float f2) {
            super(c.this, null);
            this.c = new RectF();
            this.a = f;
            this.b = f2;
        }

        @Override // com.caverock.androidsvg.c.j
        public boolean a(SVG.w0 w0Var) {
            if (w0Var instanceof SVG.x0) {
                SVG.x0 x0Var = (SVG.x0) w0Var;
                SVG.l0 q = w0Var.a.q(x0Var.n);
                if (q == null) {
                    c.N("TextPath path reference '%s' not found", x0Var.n);
                    return false;
                }
                SVG.u uVar = (SVG.u) q;
                Path f = new d(c.this, uVar.o).f();
                Matrix matrix = uVar.n;
                if (matrix != null) {
                    f.transform(matrix);
                }
                RectF rectF = new RectF();
                f.computeBounds(rectF, true);
                this.c.union(rectF);
                return false;
            }
            return true;
        }

        @Override // com.caverock.androidsvg.c.j
        public void b(String str) {
            if (c.this.g1()) {
                Rect rect = new Rect();
                c.this.d.d.getTextBounds(str, 0, str.length(), rect);
                RectF rectF = new RectF(rect);
                rectF.offset(this.a, this.b);
                this.c.union(rectF);
            }
            this.a += c.this.d.d.measureText(str);
        }
    }

    /* compiled from: SVGAndroidRenderer.java */
    /* loaded from: classes.dex */
    public abstract class j {
        public j(c cVar) {
        }

        public boolean a(SVG.w0 w0Var) {
            return true;
        }

        public abstract void b(String str);

        public /* synthetic */ j(c cVar, a aVar) {
            this(cVar);
        }
    }

    public c(Canvas canvas, float f2) {
        this.a = canvas;
        this.b = f2;
    }

    public static double B(double d2) {
        if (d2 < -1.0d) {
            return 3.141592653589793d;
        }
        return d2 > 1.0d ? Utils.DOUBLE_EPSILON : Math.acos(d2);
    }

    public static int C(float f2) {
        int i2 = (int) (f2 * 256.0f);
        if (i2 < 0) {
            return 0;
        }
        if (i2 > 255) {
            return 255;
        }
        return i2;
    }

    public static int F(int i2, float f2) {
        int i3 = 255;
        int round = Math.round(((i2 >> 24) & 255) * f2);
        if (round < 0) {
            i3 = 0;
        } else if (round <= 255) {
            i3 = round;
        }
        return (i2 & 16777215) | (i3 << 24);
    }

    public static void G(String str, Object... objArr) {
    }

    public static void N(String str, Object... objArr) {
        String.format(str, objArr);
    }

    public static synchronized void d0() {
        synchronized (c.class) {
            HashSet<String> hashSet = new HashSet<>();
            i = hashSet;
            hashSet.add("Structure");
            i.add("BasicStructure");
            i.add("ConditionalProcessing");
            i.add("Image");
            i.add("Style");
            i.add("ViewportAttribute");
            i.add("Shape");
            i.add("BasicText");
            i.add("PaintAttribute");
            i.add("BasicPaintAttribute");
            i.add("OpacityAttribute");
            i.add("BasicGraphicsAttribute");
            i.add("Marker");
            i.add("Gradient");
            i.add("Pattern");
            i.add("Clip");
            i.add("BasicClip");
            i.add("Mask");
            i.add("View");
        }
    }

    public static void h1(String str, Object... objArr) {
        String.format(str, objArr);
    }

    public static void m(float f2, float f3, float f4, float f5, float f6, boolean z, boolean z2, float f7, float f8, SVG.w wVar) {
        float f9;
        SVG.w wVar2;
        if (f2 == f7 && f3 == f8) {
            return;
        }
        if (f4 == Utils.FLOAT_EPSILON) {
            f9 = f7;
            wVar2 = wVar;
        } else if (f5 != Utils.FLOAT_EPSILON) {
            float abs = Math.abs(f4);
            float abs2 = Math.abs(f5);
            double radians = Math.toRadians(f6 % 360.0d);
            double cos = Math.cos(radians);
            double sin = Math.sin(radians);
            double d2 = (f2 - f7) / 2.0d;
            double d3 = (f3 - f8) / 2.0d;
            double d4 = (cos * d2) + (sin * d3);
            double d5 = ((-sin) * d2) + (d3 * cos);
            double d6 = abs * abs;
            double d7 = abs2 * abs2;
            double d8 = d4 * d4;
            double d9 = d5 * d5;
            double d10 = (d8 / d6) + (d9 / d7);
            if (d10 > 0.99999d) {
                double sqrt = Math.sqrt(d10) * 1.00001d;
                abs = (float) (abs * sqrt);
                abs2 = (float) (sqrt * abs2);
                d6 = abs * abs;
                d7 = abs2 * abs2;
            }
            double d11 = z == z2 ? -1.0d : 1.0d;
            double d12 = d6 * d7;
            double d13 = d6 * d9;
            double d14 = d7 * d8;
            double d15 = ((d12 - d13) - d14) / (d13 + d14);
            if (d15 < Utils.DOUBLE_EPSILON) {
                d15 = 0.0d;
            }
            double sqrt2 = d11 * Math.sqrt(d15);
            double d16 = abs;
            double d17 = abs2;
            double d18 = ((d16 * d5) / d17) * sqrt2;
            float f10 = abs;
            float f11 = abs2;
            double d19 = sqrt2 * (-((d17 * d4) / d16));
            double d20 = ((f2 + f7) / 2.0d) + ((cos * d18) - (sin * d19));
            double d21 = ((f3 + f8) / 2.0d) + (sin * d18) + (cos * d19);
            double d22 = (d4 - d18) / d16;
            double d23 = (d5 - d19) / d17;
            double d24 = ((-d4) - d18) / d16;
            double d25 = ((-d5) - d19) / d17;
            double d26 = (d22 * d22) + (d23 * d23);
            double acos = (d23 < Utils.DOUBLE_EPSILON ? -1.0d : 1.0d) * Math.acos(d22 / Math.sqrt(d26));
            double B = ((d22 * d25) - (d23 * d24) >= Utils.DOUBLE_EPSILON ? 1.0d : -1.0d) * B(((d22 * d24) + (d23 * d25)) / Math.sqrt(d26 * ((d24 * d24) + (d25 * d25))));
            if (!z2 && B > Utils.DOUBLE_EPSILON) {
                B -= 6.283185307179586d;
            } else if (z2 && B < Utils.DOUBLE_EPSILON) {
                B += 6.283185307179586d;
            }
            float[] n = n(acos % 6.283185307179586d, B % 6.283185307179586d);
            Matrix matrix = new Matrix();
            matrix.postScale(f10, f11);
            matrix.postRotate(f6);
            matrix.postTranslate((float) d20, (float) d21);
            matrix.mapPoints(n);
            n[n.length - 2] = f7;
            n[n.length - 1] = f8;
            for (int i2 = 0; i2 < n.length; i2 += 6) {
                wVar.c(n[i2], n[i2 + 1], n[i2 + 2], n[i2 + 3], n[i2 + 4], n[i2 + 5]);
            }
            return;
        } else {
            wVar2 = wVar;
            f9 = f7;
        }
        wVar2.e(f9, f8);
    }

    public static float[] n(double d2, double d3) {
        int ceil = (int) Math.ceil((Math.abs(d3) * 2.0d) / 3.141592653589793d);
        double d4 = d3 / ceil;
        double d5 = d4 / 2.0d;
        double sin = (Math.sin(d5) * 1.3333333333333333d) / (Math.cos(d5) + 1.0d);
        float[] fArr = new float[ceil * 6];
        int i2 = 0;
        for (int i3 = 0; i3 < ceil; i3++) {
            double d6 = d2 + (i3 * d4);
            double cos = Math.cos(d6);
            double sin2 = Math.sin(d6);
            int i4 = i2 + 1;
            fArr[i2] = (float) (cos - (sin * sin2));
            int i5 = i4 + 1;
            fArr[i4] = (float) (sin2 + (cos * sin));
            d4 = d4;
            double d7 = d6 + d4;
            double cos2 = Math.cos(d7);
            double sin3 = Math.sin(d7);
            int i6 = i5 + 1;
            fArr[i5] = (float) ((sin * sin3) + cos2);
            int i7 = i6 + 1;
            fArr[i6] = (float) (sin3 - (sin * cos2));
            int i8 = i7 + 1;
            fArr[i7] = (float) cos2;
            i2 = i8 + 1;
            fArr[i8] = (float) sin3;
        }
        return fArr;
    }

    public final void A(SVG.l0 l0Var) {
        Boolean bool;
        if ((l0Var instanceof SVG.j0) && (bool = ((SVG.j0) l0Var).d) != null) {
            this.d.h = bool.booleanValue();
        }
    }

    public final void A0(SVG.p pVar) {
        G("Line render", new Object[0]);
        e1(this.d, pVar);
        if (I() && g1() && this.d.c) {
            Matrix matrix = pVar.n;
            if (matrix != null) {
                this.a.concat(matrix);
            }
            Path i0 = i0(pVar);
            c1(pVar);
            x(pVar);
            u(pVar);
            boolean u0 = u0();
            K(i0);
            Q0(pVar);
            if (u0) {
                r0(pVar);
            }
        }
    }

    public final void B0(SVG.u uVar) {
        G("Path render", new Object[0]);
        if (uVar.o == null) {
            return;
        }
        e1(this.d, uVar);
        if (I() && g1()) {
            h hVar = this.d;
            if (hVar.c || hVar.b) {
                Matrix matrix = uVar.n;
                if (matrix != null) {
                    this.a.concat(matrix);
                }
                Path f2 = new d(this, uVar.o).f();
                if (uVar.h == null) {
                    uVar.h = r(f2);
                }
                c1(uVar);
                x(uVar);
                u(uVar);
                boolean u0 = u0();
                if (this.d.b) {
                    f2.setFillType(c0());
                    J(uVar, f2);
                }
                if (this.d.c) {
                    K(f2);
                }
                Q0(uVar);
                if (u0) {
                    r0(uVar);
                }
            }
        }
    }

    public final void C0(SVG.y yVar) {
        G("PolyLine render", new Object[0]);
        e1(this.d, yVar);
        if (I() && g1()) {
            h hVar = this.d;
            if (hVar.c || hVar.b) {
                Matrix matrix = yVar.n;
                if (matrix != null) {
                    this.a.concat(matrix);
                }
                if (yVar.o.length < 2) {
                    return;
                }
                Path j0 = j0(yVar);
                c1(yVar);
                j0.setFillType(c0());
                x(yVar);
                u(yVar);
                boolean u0 = u0();
                if (this.d.b) {
                    J(yVar, j0);
                }
                if (this.d.c) {
                    K(j0);
                }
                Q0(yVar);
                if (u0) {
                    r0(yVar);
                }
            }
        }
    }

    public final void D() {
        this.a.restore();
        this.d = this.e.pop();
    }

    public final void D0(SVG.z zVar) {
        G("Polygon render", new Object[0]);
        e1(this.d, zVar);
        if (I() && g1()) {
            h hVar = this.d;
            if (hVar.c || hVar.b) {
                Matrix matrix = zVar.n;
                if (matrix != null) {
                    this.a.concat(matrix);
                }
                if (zVar.o.length < 2) {
                    return;
                }
                Path j0 = j0(zVar);
                c1(zVar);
                x(zVar);
                u(zVar);
                boolean u0 = u0();
                if (this.d.b) {
                    J(zVar, j0);
                }
                if (this.d.c) {
                    K(j0);
                }
                Q0(zVar);
                if (u0) {
                    r0(zVar);
                }
            }
        }
    }

    public final void E() {
        com.caverock.androidsvg.a.a(this.a, com.caverock.androidsvg.a.a);
        this.e.push(this.d);
        this.d = new h(this, this.d);
    }

    public final void E0(SVG.a0 a0Var) {
        G("Rect render", new Object[0]);
        SVG.o oVar = a0Var.q;
        if (oVar == null || a0Var.r == null || oVar.j() || a0Var.r.j()) {
            return;
        }
        e1(this.d, a0Var);
        if (I() && g1()) {
            Matrix matrix = a0Var.n;
            if (matrix != null) {
                this.a.concat(matrix);
            }
            Path k0 = k0(a0Var);
            c1(a0Var);
            x(a0Var);
            u(a0Var);
            boolean u0 = u0();
            if (this.d.b) {
                J(a0Var, k0);
            }
            if (this.d.c) {
                K(k0);
            }
            if (u0) {
                r0(a0Var);
            }
        }
    }

    public final void F0(SVG.d0 d0Var) {
        H0(d0Var, n0(d0Var.p, d0Var.q, d0Var.r, d0Var.s), d0Var.o, d0Var.n);
    }

    public final void G0(SVG.d0 d0Var, SVG.b bVar) {
        H0(d0Var, bVar, d0Var.o, d0Var.n);
    }

    public final void H(boolean z, SVG.b bVar, SVG.t tVar) {
        SVG.l0 q = this.c.q(tVar.a);
        if (q == null) {
            Object[] objArr = new Object[2];
            objArr[0] = z ? "Fill" : "Stroke";
            objArr[1] = tVar.a;
            N("%s reference '%s' not found", objArr);
            SVG.m0 m0Var = tVar.f0;
            if (m0Var != null) {
                X0(this.d, z, m0Var);
            } else if (z) {
                this.d.b = false;
            } else {
                this.d.c = false;
            }
        } else if (q instanceof SVG.k0) {
            f0(z, bVar, (SVG.k0) q);
        } else if (q instanceof SVG.o0) {
            m0(z, bVar, (SVG.o0) q);
        } else if (q instanceof SVG.b0) {
            Y0(z, (SVG.b0) q);
        }
    }

    public final void H0(SVG.d0 d0Var, SVG.b bVar, SVG.b bVar2, PreserveAspectRatio preserveAspectRatio) {
        G("Svg render", new Object[0]);
        if (bVar.c == Utils.FLOAT_EPSILON || bVar.d == Utils.FLOAT_EPSILON) {
            return;
        }
        if (preserveAspectRatio == null && (preserveAspectRatio = d0Var.n) == null) {
            preserveAspectRatio = PreserveAspectRatio.d;
        }
        e1(this.d, d0Var);
        if (I()) {
            h hVar = this.d;
            hVar.f = bVar;
            if (!hVar.a.z0.booleanValue()) {
                SVG.b bVar3 = this.d.f;
                W0(bVar3.a, bVar3.b, bVar3.c, bVar3.d);
            }
            v(d0Var, this.d.f);
            if (bVar2 != null) {
                this.a.concat(t(this.d.f, bVar2, preserveAspectRatio));
                this.d.g = d0Var.o;
            } else {
                Canvas canvas = this.a;
                SVG.b bVar4 = this.d.f;
                canvas.translate(bVar4.a, bVar4.b);
            }
            boolean u0 = u0();
            f1();
            N0(d0Var, true);
            if (u0) {
                r0(d0Var);
            }
            c1(d0Var);
        }
    }

    public final boolean I() {
        Boolean bool = this.d.a.E0;
        if (bool != null) {
            return bool.booleanValue();
        }
        return true;
    }

    public final void I0(SVG.l0 l0Var) {
        if (l0Var instanceof SVG.s) {
            return;
        }
        a1();
        A(l0Var);
        if (l0Var instanceof SVG.d0) {
            F0((SVG.d0) l0Var);
        } else if (l0Var instanceof SVG.b1) {
            M0((SVG.b1) l0Var);
        } else if (l0Var instanceof SVG.q0) {
            J0((SVG.q0) l0Var);
        } else if (l0Var instanceof SVG.l) {
            y0((SVG.l) l0Var);
        } else if (l0Var instanceof SVG.n) {
            z0((SVG.n) l0Var);
        } else if (l0Var instanceof SVG.u) {
            B0((SVG.u) l0Var);
        } else if (l0Var instanceof SVG.a0) {
            E0((SVG.a0) l0Var);
        } else if (l0Var instanceof SVG.d) {
            w0((SVG.d) l0Var);
        } else if (l0Var instanceof SVG.i) {
            x0((SVG.i) l0Var);
        } else if (l0Var instanceof SVG.p) {
            A0((SVG.p) l0Var);
        } else if (l0Var instanceof SVG.z) {
            D0((SVG.z) l0Var);
        } else if (l0Var instanceof SVG.y) {
            C0((SVG.y) l0Var);
        } else if (l0Var instanceof SVG.u0) {
            L0((SVG.u0) l0Var);
        }
        Z0();
    }

    public final void J(SVG.i0 i0Var, Path path) {
        SVG.m0 m0Var = this.d.a.f0;
        if (m0Var instanceof SVG.t) {
            SVG.l0 q = this.c.q(((SVG.t) m0Var).a);
            if (q instanceof SVG.x) {
                T(i0Var, path, (SVG.x) q);
                return;
            }
        }
        this.a.drawPath(path, this.d.d);
    }

    public final void J0(SVG.q0 q0Var) {
        G("Switch render", new Object[0]);
        e1(this.d, q0Var);
        if (I()) {
            Matrix matrix = q0Var.n;
            if (matrix != null) {
                this.a.concat(matrix);
            }
            u(q0Var);
            boolean u0 = u0();
            S0(q0Var);
            if (u0) {
                r0(q0Var);
            }
            c1(q0Var);
        }
    }

    public final void K(Path path) {
        h hVar = this.d;
        if (hVar.a.P0 == SVG.Style.VectorEffect.NonScalingStroke) {
            Matrix matrix = this.a.getMatrix();
            Path path2 = new Path();
            path.transform(matrix, path2);
            this.a.setMatrix(new Matrix());
            Shader shader = this.d.e.getShader();
            Matrix matrix2 = new Matrix();
            if (shader != null) {
                shader.getLocalMatrix(matrix2);
                Matrix matrix3 = new Matrix(matrix2);
                matrix3.postConcat(matrix);
                shader.setLocalMatrix(matrix3);
            }
            this.a.drawPath(path2, this.d.e);
            this.a.setMatrix(matrix);
            if (shader != null) {
                shader.setLocalMatrix(matrix2);
                return;
            }
            return;
        }
        this.a.drawPath(path, hVar.e);
    }

    public final void K0(SVG.r0 r0Var, SVG.b bVar) {
        G("Symbol render", new Object[0]);
        if (bVar.c == Utils.FLOAT_EPSILON || bVar.d == Utils.FLOAT_EPSILON) {
            return;
        }
        PreserveAspectRatio preserveAspectRatio = r0Var.n;
        if (preserveAspectRatio == null) {
            preserveAspectRatio = PreserveAspectRatio.d;
        }
        e1(this.d, r0Var);
        h hVar = this.d;
        hVar.f = bVar;
        if (!hVar.a.z0.booleanValue()) {
            SVG.b bVar2 = this.d.f;
            W0(bVar2.a, bVar2.b, bVar2.c, bVar2.d);
        }
        SVG.b bVar3 = r0Var.o;
        if (bVar3 != null) {
            this.a.concat(t(this.d.f, bVar3, preserveAspectRatio));
            this.d.g = r0Var.o;
        } else {
            Canvas canvas = this.a;
            SVG.b bVar4 = this.d.f;
            canvas.translate(bVar4.a, bVar4.b);
        }
        boolean u0 = u0();
        N0(r0Var, true);
        if (u0) {
            r0(r0Var);
        }
        c1(r0Var);
    }

    public final float L(float f2, float f3, float f4, float f5) {
        return (f2 * f4) + (f3 * f5);
    }

    public final void L0(SVG.u0 u0Var) {
        G("Text render", new Object[0]);
        e1(this.d, u0Var);
        if (I()) {
            Matrix matrix = u0Var.r;
            if (matrix != null) {
                this.a.concat(matrix);
            }
            List<SVG.o> list = u0Var.n;
            float f2 = Utils.FLOAT_EPSILON;
            float f3 = (list == null || list.size() == 0) ? 0.0f : u0Var.n.get(0).f(this);
            List<SVG.o> list2 = u0Var.o;
            float g2 = (list2 == null || list2.size() == 0) ? 0.0f : u0Var.o.get(0).g(this);
            List<SVG.o> list3 = u0Var.p;
            float f4 = (list3 == null || list3.size() == 0) ? 0.0f : u0Var.p.get(0).f(this);
            List<SVG.o> list4 = u0Var.q;
            if (list4 != null && list4.size() != 0) {
                f2 = u0Var.q.get(0).g(this);
            }
            SVG.Style.TextAnchor W = W();
            if (W != SVG.Style.TextAnchor.Start) {
                float s = s(u0Var);
                if (W == SVG.Style.TextAnchor.Middle) {
                    s /= 2.0f;
                }
                f3 -= s;
            }
            if (u0Var.h == null) {
                i iVar = new i(f3, g2);
                M(u0Var, iVar);
                RectF rectF = iVar.c;
                u0Var.h = new SVG.b(rectF.left, rectF.top, rectF.width(), iVar.c.height());
            }
            c1(u0Var);
            x(u0Var);
            u(u0Var);
            boolean u0 = u0();
            M(u0Var, new f(f3 + f4, g2 + f2));
            if (u0) {
                r0(u0Var);
            }
        }
    }

    public final void M(SVG.w0 w0Var, j jVar) {
        if (I()) {
            Iterator<SVG.l0> it = w0Var.i.iterator();
            boolean z = true;
            while (it.hasNext()) {
                SVG.l0 next = it.next();
                if (next instanceof SVG.a1) {
                    jVar.b(b1(((SVG.a1) next).c, z, !it.hasNext()));
                } else {
                    t0(next, jVar);
                }
                z = false;
            }
        }
    }

    public final void M0(SVG.b1 b1Var) {
        G("Use render", new Object[0]);
        SVG.o oVar = b1Var.r;
        if (oVar == null || !oVar.j()) {
            SVG.o oVar2 = b1Var.s;
            if (oVar2 == null || !oVar2.j()) {
                e1(this.d, b1Var);
                if (I()) {
                    SVG.l0 q = b1Var.a.q(b1Var.o);
                    if (q == null) {
                        N("Use reference '%s' not found", b1Var.o);
                        return;
                    }
                    Matrix matrix = b1Var.n;
                    if (matrix != null) {
                        this.a.concat(matrix);
                    }
                    SVG.o oVar3 = b1Var.p;
                    float f2 = Utils.FLOAT_EPSILON;
                    float f3 = oVar3 != null ? oVar3.f(this) : 0.0f;
                    SVG.o oVar4 = b1Var.q;
                    if (oVar4 != null) {
                        f2 = oVar4.g(this);
                    }
                    this.a.translate(f3, f2);
                    u(b1Var);
                    boolean u0 = u0();
                    q0(b1Var);
                    if (q instanceof SVG.d0) {
                        SVG.b n0 = n0(null, null, b1Var.r, b1Var.s);
                        a1();
                        G0((SVG.d0) q, n0);
                        Z0();
                    } else if (q instanceof SVG.r0) {
                        SVG.o oVar5 = b1Var.r;
                        if (oVar5 == null) {
                            oVar5 = new SVG.o(100.0f, SVG.Unit.percent);
                        }
                        SVG.o oVar6 = b1Var.s;
                        if (oVar6 == null) {
                            oVar6 = new SVG.o(100.0f, SVG.Unit.percent);
                        }
                        SVG.b n02 = n0(null, null, oVar5, oVar6);
                        a1();
                        K0((SVG.r0) q, n02);
                        Z0();
                    } else {
                        I0(q);
                    }
                    p0();
                    if (u0) {
                        r0(b1Var);
                    }
                    c1(b1Var);
                }
            }
        }
    }

    public final void N0(SVG.h0 h0Var, boolean z) {
        if (z) {
            q0(h0Var);
        }
        for (SVG.l0 l0Var : h0Var.a()) {
            I0(l0Var);
        }
        if (z) {
            p0();
        }
    }

    public final void O(SVG.w0 w0Var, StringBuilder sb) {
        Iterator<SVG.l0> it = w0Var.i.iterator();
        boolean z = true;
        while (it.hasNext()) {
            SVG.l0 next = it.next();
            if (next instanceof SVG.w0) {
                O((SVG.w0) next, sb);
            } else if (next instanceof SVG.a1) {
                sb.append(b1(((SVG.a1) next).c, z, !it.hasNext()));
            }
            z = false;
        }
    }

    public void O0(SVG svg, com.caverock.androidsvg.b bVar) {
        SVG.b bVar2;
        PreserveAspectRatio preserveAspectRatio;
        Objects.requireNonNull(bVar, "renderOptions shouldn't be null");
        this.c = svg;
        SVG.d0 l = svg.l();
        if (l == null) {
            h1("Nothing to render. Document is empty.", new Object[0]);
            return;
        }
        if (bVar.e()) {
            SVG.j0 f2 = this.c.f(bVar.e);
            if (f2 == null || !(f2 instanceof SVG.c1)) {
                String.format("View element with id \"%s\" not found.", bVar.e);
                return;
            }
            SVG.c1 c1Var = (SVG.c1) f2;
            bVar2 = c1Var.o;
            if (bVar2 == null) {
                String.format("View element with id \"%s\" is missing a viewBox attribute.", bVar.e);
                return;
            }
            preserveAspectRatio = c1Var.n;
        } else {
            bVar2 = bVar.f() ? bVar.d : l.o;
            preserveAspectRatio = bVar.c() ? bVar.b : l.n;
        }
        if (bVar.b()) {
            svg.a(bVar.a);
        }
        if (bVar.d()) {
            CSSParser.m mVar = new CSSParser.m();
            this.h = mVar;
            mVar.a = svg.f(bVar.c);
        }
        V0();
        A(l);
        a1();
        SVG.b bVar3 = new SVG.b(bVar.f);
        SVG.o oVar = l.r;
        if (oVar != null) {
            bVar3.c = oVar.e(this, bVar3.c);
        }
        SVG.o oVar2 = l.s;
        if (oVar2 != null) {
            bVar3.d = oVar2.e(this, bVar3.d);
        }
        H0(l, bVar3, bVar2, preserveAspectRatio);
        Z0();
        if (bVar.b()) {
            svg.b();
        }
    }

    public final void P(SVG.j jVar, String str) {
        SVG.l0 q = jVar.a.q(str);
        if (q == null) {
            h1("Gradient reference '%s' not found", str);
        } else if (!(q instanceof SVG.j)) {
            N("Gradient href attributes must point to other gradient elements", new Object[0]);
        } else if (q == jVar) {
            N("Circular reference in gradient href attribute '%s'", str);
        } else {
            SVG.j jVar2 = (SVG.j) q;
            if (jVar.i == null) {
                jVar.i = jVar2.i;
            }
            if (jVar.j == null) {
                jVar.j = jVar2.j;
            }
            if (jVar.k == null) {
                jVar.k = jVar2.k;
            }
            if (jVar.h.isEmpty()) {
                jVar.h = jVar2.h;
            }
            try {
                if (jVar instanceof SVG.k0) {
                    Q((SVG.k0) jVar, (SVG.k0) q);
                } else {
                    R((SVG.o0) jVar, (SVG.o0) q);
                }
            } catch (ClassCastException unused) {
            }
            String str2 = jVar2.l;
            if (str2 != null) {
                P(jVar, str2);
            }
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:62:0x00ff, code lost:
        if (r7 != 8) goto L52;
     */
    /* JADX WARN: Removed duplicated region for block: B:15:0x0038  */
    /* JADX WARN: Removed duplicated region for block: B:16:0x003b  */
    /* JADX WARN: Removed duplicated region for block: B:19:0x0063  */
    /* JADX WARN: Removed duplicated region for block: B:20:0x0068  */
    /* JADX WARN: Removed duplicated region for block: B:23:0x006d  */
    /* JADX WARN: Removed duplicated region for block: B:24:0x0072  */
    /* JADX WARN: Removed duplicated region for block: B:27:0x0079  */
    /* JADX WARN: Removed duplicated region for block: B:28:0x007e  */
    /* JADX WARN: Removed duplicated region for block: B:31:0x0083  */
    /* JADX WARN: Removed duplicated region for block: B:34:0x008b  */
    /* JADX WARN: Removed duplicated region for block: B:69:0x0115  */
    /* JADX WARN: Removed duplicated region for block: B:71:0x0124  */
    /* JADX WARN: Removed duplicated region for block: B:76:0x0147  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void P0(com.caverock.androidsvg.SVG.q r12, com.caverock.androidsvg.c.C0080c r13) {
        /*
            Method dump skipped, instructions count: 350
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.caverock.androidsvg.c.P0(com.caverock.androidsvg.SVG$q, com.caverock.androidsvg.c$c):void");
    }

    public final void Q(SVG.k0 k0Var, SVG.k0 k0Var2) {
        if (k0Var.m == null) {
            k0Var.m = k0Var2.m;
        }
        if (k0Var.n == null) {
            k0Var.n = k0Var2.n;
        }
        if (k0Var.o == null) {
            k0Var.o = k0Var2.o;
        }
        if (k0Var.p == null) {
            k0Var.p = k0Var2.p;
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:18:0x0039  */
    /* JADX WARN: Removed duplicated region for block: B:25:0x005a  */
    /* JADX WARN: Removed duplicated region for block: B:32:0x0077  */
    /* JADX WARN: Removed duplicated region for block: B:33:0x0085  */
    /* JADX WARN: Removed duplicated region for block: B:38:0x0098 A[ADDED_TO_REGION, RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:39:0x0099  */
    /* JADX WARN: Removed duplicated region for block: B:44:0x00ac  */
    /* JADX WARN: Removed duplicated region for block: B:51:0x00cf  */
    /* JADX WARN: Removed duplicated region for block: B:57:0x00e8  */
    /* JADX WARN: Removed duplicated region for block: B:62:? A[RETURN, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void Q0(com.caverock.androidsvg.SVG.k r10) {
        /*
            Method dump skipped, instructions count: 243
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.caverock.androidsvg.c.Q0(com.caverock.androidsvg.SVG$k):void");
    }

    public final void R(SVG.o0 o0Var, SVG.o0 o0Var2) {
        if (o0Var.m == null) {
            o0Var.m = o0Var2.m;
        }
        if (o0Var.n == null) {
            o0Var.n = o0Var2.n;
        }
        if (o0Var.o == null) {
            o0Var.o = o0Var2.o;
        }
        if (o0Var.p == null) {
            o0Var.p = o0Var2.p;
        }
        if (o0Var.q == null) {
            o0Var.q = o0Var2.q;
        }
    }

    public final void R0(SVG.r rVar, SVG.i0 i0Var, SVG.b bVar) {
        float f2;
        float f3;
        G("Mask render", new Object[0]);
        Boolean bool = rVar.n;
        boolean z = true;
        if (bool != null && bool.booleanValue()) {
            SVG.o oVar = rVar.r;
            f2 = oVar != null ? oVar.f(this) : bVar.c;
            SVG.o oVar2 = rVar.s;
            f3 = oVar2 != null ? oVar2.g(this) : bVar.d;
        } else {
            SVG.o oVar3 = rVar.r;
            float e2 = oVar3 != null ? oVar3.e(this, 1.0f) : 1.2f;
            SVG.o oVar4 = rVar.s;
            float e3 = oVar4 != null ? oVar4.e(this, 1.0f) : 1.2f;
            f2 = e2 * bVar.c;
            f3 = e3 * bVar.d;
        }
        if (f2 == Utils.FLOAT_EPSILON || f3 == Utils.FLOAT_EPSILON) {
            return;
        }
        a1();
        h U = U(rVar);
        this.d = U;
        U.a.q0 = Float.valueOf(1.0f);
        boolean u0 = u0();
        this.a.save();
        Boolean bool2 = rVar.o;
        if (bool2 != null && !bool2.booleanValue()) {
            z = false;
        }
        if (!z) {
            this.a.translate(bVar.a, bVar.b);
            this.a.scale(bVar.c, bVar.d);
        }
        N0(rVar, false);
        this.a.restore();
        if (u0) {
            s0(i0Var, bVar);
        }
        Z0();
    }

    public final void S(SVG.x xVar, String str) {
        SVG.l0 q = xVar.a.q(str);
        if (q == null) {
            h1("Pattern reference '%s' not found", str);
        } else if (!(q instanceof SVG.x)) {
            N("Pattern href attributes must point to other pattern elements", new Object[0]);
        } else if (q == xVar) {
            N("Circular reference in pattern href attribute '%s'", str);
        } else {
            SVG.x xVar2 = (SVG.x) q;
            if (xVar.p == null) {
                xVar.p = xVar2.p;
            }
            if (xVar.q == null) {
                xVar.q = xVar2.q;
            }
            if (xVar.r == null) {
                xVar.r = xVar2.r;
            }
            if (xVar.s == null) {
                xVar.s = xVar2.s;
            }
            if (xVar.t == null) {
                xVar.t = xVar2.t;
            }
            if (xVar.u == null) {
                xVar.u = xVar2.u;
            }
            if (xVar.v == null) {
                xVar.v = xVar2.v;
            }
            if (xVar.i.isEmpty()) {
                xVar.i = xVar2.i;
            }
            if (xVar.o == null) {
                xVar.o = xVar2.o;
            }
            if (xVar.n == null) {
                xVar.n = xVar2.n;
            }
            String str2 = xVar2.w;
            if (str2 != null) {
                S(xVar, str2);
            }
        }
    }

    public final void S0(SVG.q0 q0Var) {
        Set<String> b2;
        String language = Locale.getDefault().getLanguage();
        SVG.g();
        for (SVG.l0 l0Var : q0Var.a()) {
            if (l0Var instanceof SVG.e0) {
                SVG.e0 e0Var = (SVG.e0) l0Var;
                if (e0Var.e() == null && ((b2 = e0Var.b()) == null || (!b2.isEmpty() && b2.contains(language)))) {
                    Set<String> i2 = e0Var.i();
                    if (i2 != null) {
                        if (i == null) {
                            d0();
                        }
                        if (!i2.isEmpty() && i.containsAll(i2)) {
                        }
                    }
                    Set<String> m = e0Var.m();
                    if (m != null) {
                        m.isEmpty();
                    } else {
                        Set<String> n = e0Var.n();
                        if (n != null) {
                            n.isEmpty();
                        } else {
                            I0(l0Var);
                            return;
                        }
                    }
                }
            }
        }
    }

    public final void T(SVG.i0 i0Var, Path path, SVG.x xVar) {
        float f2;
        float f3;
        float f4;
        float f5;
        float f6;
        Boolean bool = xVar.p;
        boolean z = bool != null && bool.booleanValue();
        String str = xVar.w;
        if (str != null) {
            S(xVar, str);
        }
        if (z) {
            SVG.o oVar = xVar.s;
            f2 = oVar != null ? oVar.f(this) : 0.0f;
            SVG.o oVar2 = xVar.t;
            f4 = oVar2 != null ? oVar2.g(this) : 0.0f;
            SVG.o oVar3 = xVar.u;
            f5 = oVar3 != null ? oVar3.f(this) : 0.0f;
            SVG.o oVar4 = xVar.v;
            f3 = oVar4 != null ? oVar4.g(this) : 0.0f;
        } else {
            SVG.o oVar5 = xVar.s;
            float e2 = oVar5 != null ? oVar5.e(this, 1.0f) : 0.0f;
            SVG.o oVar6 = xVar.t;
            float e3 = oVar6 != null ? oVar6.e(this, 1.0f) : 0.0f;
            SVG.o oVar7 = xVar.u;
            float e4 = oVar7 != null ? oVar7.e(this, 1.0f) : 0.0f;
            SVG.o oVar8 = xVar.v;
            float e5 = oVar8 != null ? oVar8.e(this, 1.0f) : 0.0f;
            SVG.b bVar = i0Var.h;
            float f7 = bVar.a;
            float f8 = bVar.c;
            f2 = (e2 * f8) + f7;
            float f9 = bVar.b;
            float f10 = bVar.d;
            float f11 = e4 * f8;
            f3 = e5 * f10;
            f4 = (e3 * f10) + f9;
            f5 = f11;
        }
        if (f5 == Utils.FLOAT_EPSILON || f3 == Utils.FLOAT_EPSILON) {
            return;
        }
        PreserveAspectRatio preserveAspectRatio = xVar.n;
        if (preserveAspectRatio == null) {
            preserveAspectRatio = PreserveAspectRatio.d;
        }
        a1();
        this.a.clipPath(path);
        h hVar = new h(this);
        d1(hVar, SVG.Style.a());
        hVar.a.z0 = Boolean.FALSE;
        this.d = V(xVar, hVar);
        SVG.b bVar2 = i0Var.h;
        Matrix matrix = xVar.r;
        if (matrix != null) {
            this.a.concat(matrix);
            Matrix matrix2 = new Matrix();
            if (xVar.r.invert(matrix2)) {
                SVG.b bVar3 = i0Var.h;
                SVG.b bVar4 = i0Var.h;
                SVG.b bVar5 = i0Var.h;
                float[] fArr = {bVar3.a, bVar3.b, bVar3.b(), bVar4.b, bVar4.b(), i0Var.h.c(), bVar5.a, bVar5.c()};
                matrix2.mapPoints(fArr);
                RectF rectF = new RectF(fArr[0], fArr[1], fArr[0], fArr[1]);
                for (int i2 = 2; i2 <= 6; i2 += 2) {
                    if (fArr[i2] < rectF.left) {
                        rectF.left = fArr[i2];
                    }
                    if (fArr[i2] > rectF.right) {
                        rectF.right = fArr[i2];
                    }
                    int i3 = i2 + 1;
                    if (fArr[i3] < rectF.top) {
                        rectF.top = fArr[i3];
                    }
                    if (fArr[i3] > rectF.bottom) {
                        rectF.bottom = fArr[i3];
                    }
                }
                float f12 = rectF.left;
                float f13 = rectF.top;
                bVar2 = new SVG.b(f12, f13, rectF.right - f12, rectF.bottom - f13);
            }
        }
        float floor = f2 + (((float) Math.floor((bVar2.a - f2) / f5)) * f5);
        float b2 = bVar2.b();
        float c = bVar2.c();
        SVG.b bVar6 = new SVG.b(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, f5, f3);
        boolean u0 = u0();
        for (float floor2 = f4 + (((float) Math.floor((bVar2.b - f4) / f3)) * f3); floor2 < c; floor2 += f3) {
            float f14 = floor;
            while (f14 < b2) {
                bVar6.a = f14;
                bVar6.b = floor2;
                a1();
                if (this.d.a.z0.booleanValue()) {
                    f6 = floor;
                } else {
                    f6 = floor;
                    W0(bVar6.a, bVar6.b, bVar6.c, bVar6.d);
                }
                SVG.b bVar7 = xVar.o;
                if (bVar7 != null) {
                    this.a.concat(t(bVar6, bVar7, preserveAspectRatio));
                } else {
                    Boolean bool2 = xVar.q;
                    boolean z2 = bool2 == null || bool2.booleanValue();
                    this.a.translate(f14, floor2);
                    if (!z2) {
                        Canvas canvas = this.a;
                        SVG.b bVar8 = i0Var.h;
                        canvas.scale(bVar8.c, bVar8.d);
                    }
                }
                for (SVG.l0 l0Var : xVar.i) {
                    I0(l0Var);
                }
                Z0();
                f14 += f5;
                floor = f6;
            }
        }
        if (u0) {
            r0(xVar);
        }
        Z0();
    }

    public final void T0(SVG.x0 x0Var) {
        G("TextPath render", new Object[0]);
        e1(this.d, x0Var);
        if (I() && g1()) {
            SVG.l0 q = x0Var.a.q(x0Var.n);
            if (q == null) {
                N("TextPath reference '%s' not found", x0Var.n);
                return;
            }
            SVG.u uVar = (SVG.u) q;
            Path f2 = new d(this, uVar.o).f();
            Matrix matrix = uVar.n;
            if (matrix != null) {
                f2.transform(matrix);
            }
            PathMeasure pathMeasure = new PathMeasure(f2, false);
            SVG.o oVar = x0Var.o;
            float e2 = oVar != null ? oVar.e(this, pathMeasure.getLength()) : 0.0f;
            SVG.Style.TextAnchor W = W();
            if (W != SVG.Style.TextAnchor.Start) {
                float s = s(x0Var);
                if (W == SVG.Style.TextAnchor.Middle) {
                    s /= 2.0f;
                }
                e2 -= s;
            }
            x((SVG.i0) x0Var.g());
            boolean u0 = u0();
            M(x0Var, new e(f2, e2, Utils.FLOAT_EPSILON));
            if (u0) {
                r0(x0Var);
            }
        }
    }

    public final h U(SVG.l0 l0Var) {
        h hVar = new h(this);
        d1(hVar, SVG.Style.a());
        return V(l0Var, hVar);
    }

    public final boolean U0() {
        return this.d.a.q0.floatValue() < 1.0f || this.d.a.K0 != null;
    }

    public final h V(SVG.l0 l0Var, h hVar) {
        ArrayList<SVG.j0> arrayList = new ArrayList();
        while (true) {
            if (l0Var instanceof SVG.j0) {
                arrayList.add(0, (SVG.j0) l0Var);
            }
            SVG.h0 h0Var = l0Var.b;
            if (h0Var == null) {
                break;
            }
            l0Var = (SVG.l0) h0Var;
        }
        for (SVG.j0 j0Var : arrayList) {
            e1(hVar, j0Var);
        }
        h hVar2 = this.d;
        hVar.g = hVar2.g;
        hVar.f = hVar2.f;
        return hVar;
    }

    public final void V0() {
        this.d = new h(this);
        this.e = new Stack<>();
        d1(this.d, SVG.Style.a());
        h hVar = this.d;
        hVar.f = null;
        hVar.h = false;
        this.e.push(new h(this, hVar));
        this.g = new Stack<>();
        this.f = new Stack<>();
    }

    public final SVG.Style.TextAnchor W() {
        SVG.Style.TextAnchor textAnchor;
        SVG.Style style = this.d.a;
        if (style.x0 != SVG.Style.TextDirection.LTR && (textAnchor = style.y0) != SVG.Style.TextAnchor.Middle) {
            SVG.Style.TextAnchor textAnchor2 = SVG.Style.TextAnchor.Start;
            return textAnchor == textAnchor2 ? SVG.Style.TextAnchor.End : textAnchor2;
        }
        return style.y0;
    }

    public final void W0(float f2, float f3, float f4, float f5) {
        float f6 = f4 + f2;
        float f7 = f5 + f3;
        SVG.c cVar = this.d.a.A0;
        if (cVar != null) {
            f2 += cVar.d.f(this);
            f3 += this.d.a.A0.a.g(this);
            f6 -= this.d.a.A0.b.f(this);
            f7 -= this.d.a.A0.c.g(this);
        }
        this.a.clipRect(f2, f3, f6, f7);
    }

    public final Path.FillType X() {
        SVG.Style.FillRule fillRule = this.d.a.J0;
        if (fillRule != null && fillRule == SVG.Style.FillRule.EvenOdd) {
            return Path.FillType.EVEN_ODD;
        }
        return Path.FillType.WINDING;
    }

    public final void X0(h hVar, boolean z, SVG.m0 m0Var) {
        int i2;
        SVG.Style style = hVar.a;
        float floatValue = (z ? style.h0 : style.j0).floatValue();
        if (m0Var instanceof SVG.f) {
            i2 = ((SVG.f) m0Var).a;
        } else if (!(m0Var instanceof SVG.g)) {
            return;
        } else {
            i2 = hVar.a.r0.a;
        }
        int F = F(i2, floatValue);
        if (z) {
            hVar.d.setColor(F);
        } else {
            hVar.e.setColor(F);
        }
    }

    public float Y() {
        return this.d.d.getTextSize();
    }

    public final void Y0(boolean z, SVG.b0 b0Var) {
        if (z) {
            if (e0(b0Var.e, 2147483648L)) {
                h hVar = this.d;
                SVG.Style style = hVar.a;
                SVG.m0 m0Var = b0Var.e.L0;
                style.f0 = m0Var;
                hVar.b = m0Var != null;
            }
            if (e0(b0Var.e, 4294967296L)) {
                this.d.a.h0 = b0Var.e.M0;
            }
            if (e0(b0Var.e, 6442450944L)) {
                h hVar2 = this.d;
                X0(hVar2, z, hVar2.a.f0);
                return;
            }
            return;
        }
        if (e0(b0Var.e, 2147483648L)) {
            h hVar3 = this.d;
            SVG.Style style2 = hVar3.a;
            SVG.m0 m0Var2 = b0Var.e.L0;
            style2.i0 = m0Var2;
            hVar3.c = m0Var2 != null;
        }
        if (e0(b0Var.e, 4294967296L)) {
            this.d.a.j0 = b0Var.e.M0;
        }
        if (e0(b0Var.e, 6442450944L)) {
            h hVar4 = this.d;
            X0(hVar4, z, hVar4.a.i0);
        }
    }

    public float Z() {
        return this.d.d.getTextSize() / 2.0f;
    }

    public final void Z0() {
        this.a.restore();
        this.d = this.e.pop();
    }

    public SVG.b a0() {
        h hVar = this.d;
        SVG.b bVar = hVar.g;
        return bVar != null ? bVar : hVar.f;
    }

    public final void a1() {
        this.a.save();
        this.e.push(this.d);
        this.d = new h(this, this.d);
    }

    public float b0() {
        return this.b;
    }

    public final String b1(String str, boolean z, boolean z2) {
        if (this.d.h) {
            return str.replaceAll("[\\n\\t]", MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
        }
        String replaceAll = str.replaceAll("\\n", "").replaceAll("\\t", MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
        if (z) {
            replaceAll = replaceAll.replaceAll("^\\s+", "");
        }
        if (z2) {
            replaceAll = replaceAll.replaceAll("\\s+$", "");
        }
        return replaceAll.replaceAll("\\s{2,}", MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
    }

    public final Path.FillType c0() {
        SVG.Style.FillRule fillRule = this.d.a.g0;
        if (fillRule != null && fillRule == SVG.Style.FillRule.EvenOdd) {
            return Path.FillType.EVEN_ODD;
        }
        return Path.FillType.WINDING;
    }

    public final void c1(SVG.i0 i0Var) {
        if (i0Var.b == null || i0Var.h == null) {
            return;
        }
        Matrix matrix = new Matrix();
        if (this.g.peek().invert(matrix)) {
            SVG.b bVar = i0Var.h;
            SVG.b bVar2 = i0Var.h;
            SVG.b bVar3 = i0Var.h;
            float[] fArr = {bVar.a, bVar.b, bVar.b(), bVar2.b, bVar2.b(), i0Var.h.c(), bVar3.a, bVar3.c()};
            matrix.preConcat(this.a.getMatrix());
            matrix.mapPoints(fArr);
            RectF rectF = new RectF(fArr[0], fArr[1], fArr[0], fArr[1]);
            for (int i2 = 2; i2 <= 6; i2 += 2) {
                if (fArr[i2] < rectF.left) {
                    rectF.left = fArr[i2];
                }
                if (fArr[i2] > rectF.right) {
                    rectF.right = fArr[i2];
                }
                int i3 = i2 + 1;
                if (fArr[i3] < rectF.top) {
                    rectF.top = fArr[i3];
                }
                if (fArr[i3] > rectF.bottom) {
                    rectF.bottom = fArr[i3];
                }
            }
            SVG.i0 i0Var2 = (SVG.i0) this.f.peek();
            SVG.b bVar4 = i0Var2.h;
            if (bVar4 == null) {
                i0Var2.h = SVG.b.a(rectF.left, rectF.top, rectF.right, rectF.bottom);
            } else {
                bVar4.d(SVG.b.a(rectF.left, rectF.top, rectF.right, rectF.bottom));
            }
        }
    }

    public final void d1(h hVar, SVG.Style style) {
        if (e0(style, 4096L)) {
            hVar.a.r0 = style.r0;
        }
        if (e0(style, 2048L)) {
            hVar.a.q0 = style.q0;
        }
        if (e0(style, 1L)) {
            hVar.a.f0 = style.f0;
            SVG.m0 m0Var = style.f0;
            hVar.b = (m0Var == null || m0Var == SVG.f.g0) ? false : true;
        }
        if (e0(style, 4L)) {
            hVar.a.h0 = style.h0;
        }
        if (e0(style, 6149L)) {
            X0(hVar, true, hVar.a.f0);
        }
        if (e0(style, 2L)) {
            hVar.a.g0 = style.g0;
        }
        if (e0(style, 8L)) {
            hVar.a.i0 = style.i0;
            SVG.m0 m0Var2 = style.i0;
            hVar.c = (m0Var2 == null || m0Var2 == SVG.f.g0) ? false : true;
        }
        if (e0(style, 16L)) {
            hVar.a.j0 = style.j0;
        }
        if (e0(style, 6168L)) {
            X0(hVar, false, hVar.a.i0);
        }
        if (e0(style, 34359738368L)) {
            hVar.a.P0 = style.P0;
        }
        if (e0(style, 32L)) {
            SVG.Style style2 = hVar.a;
            SVG.o oVar = style.k0;
            style2.k0 = oVar;
            hVar.e.setStrokeWidth(oVar.d(this));
        }
        if (e0(style, 64L)) {
            hVar.a.l0 = style.l0;
            int i2 = a.b[style.l0.ordinal()];
            if (i2 == 1) {
                hVar.e.setStrokeCap(Paint.Cap.BUTT);
            } else if (i2 == 2) {
                hVar.e.setStrokeCap(Paint.Cap.ROUND);
            } else if (i2 == 3) {
                hVar.e.setStrokeCap(Paint.Cap.SQUARE);
            }
        }
        if (e0(style, 128L)) {
            hVar.a.m0 = style.m0;
            int i3 = a.c[style.m0.ordinal()];
            if (i3 == 1) {
                hVar.e.setStrokeJoin(Paint.Join.MITER);
            } else if (i3 == 2) {
                hVar.e.setStrokeJoin(Paint.Join.ROUND);
            } else if (i3 == 3) {
                hVar.e.setStrokeJoin(Paint.Join.BEVEL);
            }
        }
        if (e0(style, 256L)) {
            hVar.a.n0 = style.n0;
            hVar.e.setStrokeMiter(style.n0.floatValue());
        }
        if (e0(style, 512L)) {
            hVar.a.o0 = style.o0;
        }
        if (e0(style, RealWebSocket.DEFAULT_MINIMUM_DEFLATE_SIZE)) {
            hVar.a.p0 = style.p0;
        }
        Typeface typeface = null;
        if (e0(style, 1536L)) {
            SVG.o[] oVarArr = hVar.a.o0;
            if (oVarArr == null) {
                hVar.e.setPathEffect(null);
            } else {
                int length = oVarArr.length;
                int i4 = length % 2 == 0 ? length : length * 2;
                float[] fArr = new float[i4];
                float f2 = 0.0f;
                for (int i5 = 0; i5 < i4; i5++) {
                    fArr[i5] = hVar.a.o0[i5 % length].d(this);
                    f2 += fArr[i5];
                }
                if (f2 == Utils.FLOAT_EPSILON) {
                    hVar.e.setPathEffect(null);
                } else {
                    float d2 = hVar.a.p0.d(this);
                    if (d2 < Utils.FLOAT_EPSILON) {
                        d2 = (d2 % f2) + f2;
                    }
                    hVar.e.setPathEffect(new DashPathEffect(fArr, d2));
                }
            }
        }
        if (e0(style, Http2Stream.EMIT_BUFFER_SIZE)) {
            float Y = Y();
            hVar.a.t0 = style.t0;
            hVar.d.setTextSize(style.t0.e(this, Y));
            hVar.e.setTextSize(style.t0.e(this, Y));
        }
        if (e0(style, 8192L)) {
            hVar.a.s0 = style.s0;
        }
        if (e0(style, 32768L)) {
            if (style.u0.intValue() == -1 && hVar.a.u0.intValue() > 100) {
                SVG.Style style3 = hVar.a;
                style3.u0 = Integer.valueOf(style3.u0.intValue() - 100);
            } else if (style.u0.intValue() == 1 && hVar.a.u0.intValue() < 900) {
                SVG.Style style4 = hVar.a;
                style4.u0 = Integer.valueOf(style4.u0.intValue() + 100);
            } else {
                hVar.a.u0 = style.u0;
            }
        }
        if (e0(style, 65536L)) {
            hVar.a.v0 = style.v0;
        }
        if (e0(style, 106496L)) {
            if (hVar.a.s0 != null && this.c != null) {
                SVG.g();
                for (String str : hVar.a.s0) {
                    SVG.Style style5 = hVar.a;
                    typeface = z(str, style5.u0, style5.v0);
                    if (typeface != null) {
                        break;
                    }
                }
            }
            if (typeface == null) {
                SVG.Style style6 = hVar.a;
                typeface = z("serif", style6.u0, style6.v0);
            }
            hVar.d.setTypeface(typeface);
            hVar.e.setTypeface(typeface);
        }
        if (e0(style, 131072L)) {
            hVar.a.w0 = style.w0;
            Paint paint = hVar.d;
            SVG.Style.TextDecoration textDecoration = style.w0;
            SVG.Style.TextDecoration textDecoration2 = SVG.Style.TextDecoration.LineThrough;
            paint.setStrikeThruText(textDecoration == textDecoration2);
            Paint paint2 = hVar.d;
            SVG.Style.TextDecoration textDecoration3 = style.w0;
            SVG.Style.TextDecoration textDecoration4 = SVG.Style.TextDecoration.Underline;
            paint2.setUnderlineText(textDecoration3 == textDecoration4);
            if (Build.VERSION.SDK_INT >= 17) {
                hVar.e.setStrikeThruText(style.w0 == textDecoration2);
                hVar.e.setUnderlineText(style.w0 == textDecoration4);
            }
        }
        if (e0(style, 68719476736L)) {
            hVar.a.x0 = style.x0;
        }
        if (e0(style, 262144L)) {
            hVar.a.y0 = style.y0;
        }
        if (e0(style, 524288L)) {
            hVar.a.z0 = style.z0;
        }
        if (e0(style, 2097152L)) {
            hVar.a.B0 = style.B0;
        }
        if (e0(style, 4194304L)) {
            hVar.a.C0 = style.C0;
        }
        if (e0(style, 8388608L)) {
            hVar.a.D0 = style.D0;
        }
        if (e0(style, 16777216L)) {
            hVar.a.E0 = style.E0;
        }
        if (e0(style, 33554432L)) {
            hVar.a.F0 = style.F0;
        }
        if (e0(style, 1048576L)) {
            hVar.a.A0 = style.A0;
        }
        if (e0(style, 268435456L)) {
            hVar.a.I0 = style.I0;
        }
        if (e0(style, 536870912L)) {
            hVar.a.J0 = style.J0;
        }
        if (e0(style, 1073741824L)) {
            hVar.a.K0 = style.K0;
        }
        if (e0(style, 67108864L)) {
            hVar.a.G0 = style.G0;
        }
        if (e0(style, 134217728L)) {
            hVar.a.H0 = style.H0;
        }
        if (e0(style, 8589934592L)) {
            hVar.a.N0 = style.N0;
        }
        if (e0(style, 17179869184L)) {
            hVar.a.O0 = style.O0;
        }
        if (e0(style, 137438953472L)) {
            hVar.a.Q0 = style.Q0;
        }
    }

    public final boolean e0(SVG.Style style, long j2) {
        return (style.a & j2) != 0;
    }

    public final void e1(h hVar, SVG.j0 j0Var) {
        hVar.a.b(j0Var.b == null);
        SVG.Style style = j0Var.e;
        if (style != null) {
            d1(hVar, style);
        }
        if (this.c.m()) {
            for (CSSParser.l lVar : this.c.d()) {
                if (CSSParser.l(this.h, lVar.a, j0Var)) {
                    d1(hVar, lVar.b);
                }
            }
        }
        SVG.Style style2 = j0Var.f;
        if (style2 != null) {
            d1(hVar, style2);
        }
    }

    public final void f0(boolean z, SVG.b bVar, SVG.k0 k0Var) {
        float f2;
        float e2;
        float f3;
        float f4;
        String str = k0Var.l;
        if (str != null) {
            P(k0Var, str);
        }
        Boolean bool = k0Var.i;
        int i2 = 0;
        boolean z2 = bool != null && bool.booleanValue();
        h hVar = this.d;
        Paint paint = z ? hVar.d : hVar.e;
        if (z2) {
            SVG.b a0 = a0();
            SVG.o oVar = k0Var.m;
            float f5 = oVar != null ? oVar.f(this) : Utils.FLOAT_EPSILON;
            SVG.o oVar2 = k0Var.n;
            float g2 = oVar2 != null ? oVar2.g(this) : Utils.FLOAT_EPSILON;
            SVG.o oVar3 = k0Var.o;
            float f6 = oVar3 != null ? oVar3.f(this) : a0.c;
            SVG.o oVar4 = k0Var.p;
            f4 = f6;
            f2 = f5;
            f3 = g2;
            e2 = oVar4 != null ? oVar4.g(this) : Utils.FLOAT_EPSILON;
        } else {
            SVG.o oVar5 = k0Var.m;
            float e3 = oVar5 != null ? oVar5.e(this, 1.0f) : Utils.FLOAT_EPSILON;
            SVG.o oVar6 = k0Var.n;
            float e4 = oVar6 != null ? oVar6.e(this, 1.0f) : Utils.FLOAT_EPSILON;
            SVG.o oVar7 = k0Var.o;
            float e5 = oVar7 != null ? oVar7.e(this, 1.0f) : 1.0f;
            SVG.o oVar8 = k0Var.p;
            f2 = e3;
            e2 = oVar8 != null ? oVar8.e(this, 1.0f) : Utils.FLOAT_EPSILON;
            f3 = e4;
            f4 = e5;
        }
        a1();
        this.d = U(k0Var);
        Matrix matrix = new Matrix();
        if (!z2) {
            matrix.preTranslate(bVar.a, bVar.b);
            matrix.preScale(bVar.c, bVar.d);
        }
        Matrix matrix2 = k0Var.j;
        if (matrix2 != null) {
            matrix.preConcat(matrix2);
        }
        int size = k0Var.h.size();
        if (size == 0) {
            Z0();
            if (z) {
                this.d.b = false;
                return;
            } else {
                this.d.c = false;
                return;
            }
        }
        int[] iArr = new int[size];
        float[] fArr = new float[size];
        float f7 = -1.0f;
        Iterator<SVG.l0> it = k0Var.h.iterator();
        while (it.hasNext()) {
            SVG.c0 c0Var = (SVG.c0) it.next();
            Float f8 = c0Var.h;
            float floatValue = f8 != null ? f8.floatValue() : Utils.FLOAT_EPSILON;
            if (i2 != 0 && floatValue < f7) {
                fArr[i2] = f7;
            } else {
                fArr[i2] = floatValue;
                f7 = floatValue;
            }
            a1();
            e1(this.d, c0Var);
            SVG.Style style = this.d.a;
            SVG.f fVar = (SVG.f) style.G0;
            if (fVar == null) {
                fVar = SVG.f.f0;
            }
            iArr[i2] = F(fVar.a, style.H0.floatValue());
            i2++;
            Z0();
        }
        if ((f2 == f4 && f3 == e2) || size == 1) {
            Z0();
            paint.setColor(iArr[size - 1]);
            return;
        }
        Shader.TileMode tileMode = Shader.TileMode.CLAMP;
        SVG.GradientSpread gradientSpread = k0Var.k;
        if (gradientSpread != null) {
            if (gradientSpread == SVG.GradientSpread.reflect) {
                tileMode = Shader.TileMode.MIRROR;
            } else if (gradientSpread == SVG.GradientSpread.repeat) {
                tileMode = Shader.TileMode.REPEAT;
            }
        }
        Z0();
        LinearGradient linearGradient = new LinearGradient(f2, f3, f4, e2, iArr, fArr, tileMode);
        linearGradient.setLocalMatrix(matrix);
        paint.setShader(linearGradient);
        paint.setAlpha(C(this.d.a.h0.floatValue()));
    }

    public final void f1() {
        int i2;
        SVG.Style style = this.d.a;
        SVG.m0 m0Var = style.N0;
        if (m0Var instanceof SVG.f) {
            i2 = ((SVG.f) m0Var).a;
        } else if (!(m0Var instanceof SVG.g)) {
            return;
        } else {
            i2 = style.r0.a;
        }
        Float f2 = style.O0;
        if (f2 != null) {
            i2 = F(i2, f2.floatValue());
        }
        this.a.drawColor(i2);
    }

    public final Path g0(SVG.d dVar) {
        SVG.o oVar = dVar.o;
        float f2 = Utils.FLOAT_EPSILON;
        float f3 = oVar != null ? oVar.f(this) : 0.0f;
        SVG.o oVar2 = dVar.p;
        if (oVar2 != null) {
            f2 = oVar2.g(this);
        }
        float d2 = dVar.q.d(this);
        float f4 = f3 - d2;
        float f5 = f2 - d2;
        float f6 = f3 + d2;
        float f7 = f2 + d2;
        if (dVar.h == null) {
            float f8 = 2.0f * d2;
            dVar.h = new SVG.b(f4, f5, f8, f8);
        }
        float f9 = 0.5522848f * d2;
        Path path = new Path();
        path.moveTo(f3, f5);
        float f10 = f3 + f9;
        float f11 = f2 - f9;
        path.cubicTo(f10, f5, f6, f11, f6, f2);
        float f12 = f2 + f9;
        path.cubicTo(f6, f12, f10, f7, f3, f7);
        float f13 = f3 - f9;
        path.cubicTo(f13, f7, f4, f12, f4, f2);
        path.cubicTo(f4, f11, f13, f5, f3, f5);
        path.close();
        return path;
    }

    public final boolean g1() {
        Boolean bool = this.d.a.F0;
        if (bool != null) {
            return bool.booleanValue();
        }
        return true;
    }

    public final void h(SVG.k kVar, Path path, Matrix matrix) {
        Path j0;
        e1(this.d, kVar);
        if (I() && g1()) {
            Matrix matrix2 = kVar.n;
            if (matrix2 != null) {
                matrix.preConcat(matrix2);
            }
            if (kVar instanceof SVG.a0) {
                j0 = k0((SVG.a0) kVar);
            } else if (kVar instanceof SVG.d) {
                j0 = g0((SVG.d) kVar);
            } else if (kVar instanceof SVG.i) {
                j0 = h0((SVG.i) kVar);
            } else if (!(kVar instanceof SVG.y)) {
                return;
            } else {
                j0 = j0((SVG.y) kVar);
            }
            u(kVar);
            path.setFillType(X());
            path.addPath(j0, matrix);
        }
    }

    public final Path h0(SVG.i iVar) {
        SVG.o oVar = iVar.o;
        float f2 = Utils.FLOAT_EPSILON;
        float f3 = oVar != null ? oVar.f(this) : 0.0f;
        SVG.o oVar2 = iVar.p;
        if (oVar2 != null) {
            f2 = oVar2.g(this);
        }
        float f4 = iVar.q.f(this);
        float g2 = iVar.r.g(this);
        float f5 = f3 - f4;
        float f6 = f2 - g2;
        float f7 = f3 + f4;
        float f8 = f2 + g2;
        if (iVar.h == null) {
            iVar.h = new SVG.b(f5, f6, f4 * 2.0f, 2.0f * g2);
        }
        float f9 = f4 * 0.5522848f;
        float f10 = 0.5522848f * g2;
        Path path = new Path();
        path.moveTo(f3, f6);
        float f11 = f3 + f9;
        float f12 = f2 - f10;
        path.cubicTo(f11, f6, f7, f12, f7, f2);
        float f13 = f10 + f2;
        path.cubicTo(f7, f13, f11, f8, f3, f8);
        float f14 = f3 - f9;
        path.cubicTo(f14, f8, f5, f13, f5, f2);
        path.cubicTo(f5, f12, f14, f6, f3, f6);
        path.close();
        return path;
    }

    public final void i(SVG.u uVar, Path path, Matrix matrix) {
        e1(this.d, uVar);
        if (I() && g1()) {
            Matrix matrix2 = uVar.n;
            if (matrix2 != null) {
                matrix.preConcat(matrix2);
            }
            Path f2 = new d(this, uVar.o).f();
            if (uVar.h == null) {
                uVar.h = r(f2);
            }
            u(uVar);
            path.setFillType(X());
            path.addPath(f2, matrix);
        }
    }

    public final Path i0(SVG.p pVar) {
        SVG.o oVar = pVar.o;
        float f2 = Utils.FLOAT_EPSILON;
        float f3 = oVar == null ? 0.0f : oVar.f(this);
        SVG.o oVar2 = pVar.p;
        float g2 = oVar2 == null ? 0.0f : oVar2.g(this);
        SVG.o oVar3 = pVar.q;
        float f4 = oVar3 == null ? 0.0f : oVar3.f(this);
        SVG.o oVar4 = pVar.r;
        if (oVar4 != null) {
            f2 = oVar4.g(this);
        }
        if (pVar.h == null) {
            pVar.h = new SVG.b(Math.min(f3, f4), Math.min(g2, f2), Math.abs(f4 - f3), Math.abs(f2 - g2));
        }
        Path path = new Path();
        path.moveTo(f3, g2);
        path.lineTo(f4, f2);
        return path;
    }

    public final void j(SVG.l0 l0Var, boolean z, Path path, Matrix matrix) {
        if (I()) {
            E();
            if (l0Var instanceof SVG.b1) {
                if (z) {
                    l((SVG.b1) l0Var, path, matrix);
                } else {
                    N("<use> elements inside a <clipPath> cannot reference another <use>", new Object[0]);
                }
            } else if (l0Var instanceof SVG.u) {
                i((SVG.u) l0Var, path, matrix);
            } else if (l0Var instanceof SVG.u0) {
                k((SVG.u0) l0Var, path, matrix);
            } else if (l0Var instanceof SVG.k) {
                h((SVG.k) l0Var, path, matrix);
            } else {
                N("Invalid %s element found in clipPath definition", l0Var.toString());
            }
            D();
        }
    }

    public final Path j0(SVG.y yVar) {
        Path path = new Path();
        float[] fArr = yVar.o;
        path.moveTo(fArr[0], fArr[1]);
        int i2 = 2;
        while (true) {
            float[] fArr2 = yVar.o;
            if (i2 >= fArr2.length) {
                break;
            }
            path.lineTo(fArr2[i2], fArr2[i2 + 1]);
            i2 += 2;
        }
        if (yVar instanceof SVG.z) {
            path.close();
        }
        if (yVar.h == null) {
            yVar.h = r(path);
        }
        return path;
    }

    public final void k(SVG.u0 u0Var, Path path, Matrix matrix) {
        e1(this.d, u0Var);
        if (I()) {
            Matrix matrix2 = u0Var.r;
            if (matrix2 != null) {
                matrix.preConcat(matrix2);
            }
            List<SVG.o> list = u0Var.n;
            float f2 = Utils.FLOAT_EPSILON;
            float f3 = (list == null || list.size() == 0) ? 0.0f : u0Var.n.get(0).f(this);
            List<SVG.o> list2 = u0Var.o;
            float g2 = (list2 == null || list2.size() == 0) ? 0.0f : u0Var.o.get(0).g(this);
            List<SVG.o> list3 = u0Var.p;
            float f4 = (list3 == null || list3.size() == 0) ? 0.0f : u0Var.p.get(0).f(this);
            List<SVG.o> list4 = u0Var.q;
            if (list4 != null && list4.size() != 0) {
                f2 = u0Var.q.get(0).g(this);
            }
            if (this.d.a.y0 != SVG.Style.TextAnchor.Start) {
                float s = s(u0Var);
                if (this.d.a.y0 == SVG.Style.TextAnchor.Middle) {
                    s /= 2.0f;
                }
                f3 -= s;
            }
            if (u0Var.h == null) {
                i iVar = new i(f3, g2);
                M(u0Var, iVar);
                RectF rectF = iVar.c;
                u0Var.h = new SVG.b(rectF.left, rectF.top, rectF.width(), iVar.c.height());
            }
            u(u0Var);
            Path path2 = new Path();
            M(u0Var, new g(f3 + f4, g2 + f2, path2));
            path.setFillType(X());
            path.addPath(path2, matrix);
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:16:0x0048  */
    /* JADX WARN: Removed duplicated region for block: B:17:0x004d  */
    /* JADX WARN: Removed duplicated region for block: B:20:0x0052  */
    /* JADX WARN: Removed duplicated region for block: B:21:0x0058  */
    /* JADX WARN: Removed duplicated region for block: B:24:0x0069  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final android.graphics.Path k0(com.caverock.androidsvg.SVG.a0 r24) {
        /*
            Method dump skipped, instructions count: 241
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.caverock.androidsvg.c.k0(com.caverock.androidsvg.SVG$a0):android.graphics.Path");
    }

    public final void l(SVG.b1 b1Var, Path path, Matrix matrix) {
        e1(this.d, b1Var);
        if (I() && g1()) {
            Matrix matrix2 = b1Var.n;
            if (matrix2 != null) {
                matrix.preConcat(matrix2);
            }
            SVG.l0 q = b1Var.a.q(b1Var.o);
            if (q == null) {
                N("Use reference '%s' not found", b1Var.o);
                return;
            }
            u(b1Var);
            j(q, false, path, matrix);
        }
    }

    public final Path l0(SVG.u0 u0Var) {
        List<SVG.o> list = u0Var.n;
        float f2 = Utils.FLOAT_EPSILON;
        float f3 = (list == null || list.size() == 0) ? 0.0f : u0Var.n.get(0).f(this);
        List<SVG.o> list2 = u0Var.o;
        float g2 = (list2 == null || list2.size() == 0) ? 0.0f : u0Var.o.get(0).g(this);
        List<SVG.o> list3 = u0Var.p;
        float f4 = (list3 == null || list3.size() == 0) ? 0.0f : u0Var.p.get(0).f(this);
        List<SVG.o> list4 = u0Var.q;
        if (list4 != null && list4.size() != 0) {
            f2 = u0Var.q.get(0).g(this);
        }
        if (this.d.a.y0 != SVG.Style.TextAnchor.Start) {
            float s = s(u0Var);
            if (this.d.a.y0 == SVG.Style.TextAnchor.Middle) {
                s /= 2.0f;
            }
            f3 -= s;
        }
        if (u0Var.h == null) {
            i iVar = new i(f3, g2);
            M(u0Var, iVar);
            RectF rectF = iVar.c;
            u0Var.h = new SVG.b(rectF.left, rectF.top, rectF.width(), iVar.c.height());
        }
        Path path = new Path();
        M(u0Var, new g(f3 + f4, g2 + f2, path));
        return path;
    }

    public final void m0(boolean z, SVG.b bVar, SVG.o0 o0Var) {
        float f2;
        float e2;
        float f3;
        String str = o0Var.l;
        if (str != null) {
            P(o0Var, str);
        }
        Boolean bool = o0Var.i;
        int i2 = 0;
        boolean z2 = bool != null && bool.booleanValue();
        h hVar = this.d;
        Paint paint = z ? hVar.d : hVar.e;
        if (z2) {
            SVG.o oVar = new SVG.o(50.0f, SVG.Unit.percent);
            SVG.o oVar2 = o0Var.m;
            float f4 = oVar2 != null ? oVar2.f(this) : oVar.f(this);
            SVG.o oVar3 = o0Var.n;
            float g2 = oVar3 != null ? oVar3.g(this) : oVar.g(this);
            SVG.o oVar4 = o0Var.o;
            e2 = oVar4 != null ? oVar4.d(this) : oVar.d(this);
            f2 = f4;
            f3 = g2;
        } else {
            SVG.o oVar5 = o0Var.m;
            float e3 = oVar5 != null ? oVar5.e(this, 1.0f) : 0.5f;
            SVG.o oVar6 = o0Var.n;
            float e4 = oVar6 != null ? oVar6.e(this, 1.0f) : 0.5f;
            SVG.o oVar7 = o0Var.o;
            f2 = e3;
            e2 = oVar7 != null ? oVar7.e(this, 1.0f) : 0.5f;
            f3 = e4;
        }
        a1();
        this.d = U(o0Var);
        Matrix matrix = new Matrix();
        if (!z2) {
            matrix.preTranslate(bVar.a, bVar.b);
            matrix.preScale(bVar.c, bVar.d);
        }
        Matrix matrix2 = o0Var.j;
        if (matrix2 != null) {
            matrix.preConcat(matrix2);
        }
        int size = o0Var.h.size();
        if (size == 0) {
            Z0();
            if (z) {
                this.d.b = false;
                return;
            } else {
                this.d.c = false;
                return;
            }
        }
        int[] iArr = new int[size];
        float[] fArr = new float[size];
        float f5 = -1.0f;
        Iterator<SVG.l0> it = o0Var.h.iterator();
        while (true) {
            boolean hasNext = it.hasNext();
            float f6 = Utils.FLOAT_EPSILON;
            if (!hasNext) {
                break;
            }
            SVG.c0 c0Var = (SVG.c0) it.next();
            Float f7 = c0Var.h;
            if (f7 != null) {
                f6 = f7.floatValue();
            }
            if (i2 != 0 && f6 < f5) {
                fArr[i2] = f5;
            } else {
                fArr[i2] = f6;
                f5 = f6;
            }
            a1();
            e1(this.d, c0Var);
            SVG.Style style = this.d.a;
            SVG.f fVar = (SVG.f) style.G0;
            if (fVar == null) {
                fVar = SVG.f.f0;
            }
            iArr[i2] = F(fVar.a, style.H0.floatValue());
            i2++;
            Z0();
        }
        if (e2 != Utils.FLOAT_EPSILON && size != 1) {
            Shader.TileMode tileMode = Shader.TileMode.CLAMP;
            SVG.GradientSpread gradientSpread = o0Var.k;
            if (gradientSpread != null) {
                if (gradientSpread == SVG.GradientSpread.reflect) {
                    tileMode = Shader.TileMode.MIRROR;
                } else if (gradientSpread == SVG.GradientSpread.repeat) {
                    tileMode = Shader.TileMode.REPEAT;
                }
            }
            Z0();
            RadialGradient radialGradient = new RadialGradient(f2, f3, e2, iArr, fArr, tileMode);
            radialGradient.setLocalMatrix(matrix);
            paint.setShader(radialGradient);
            paint.setAlpha(C(this.d.a.h0.floatValue()));
            return;
        }
        Z0();
        paint.setColor(iArr[size - 1]);
    }

    public final SVG.b n0(SVG.o oVar, SVG.o oVar2, SVG.o oVar3, SVG.o oVar4) {
        float f2 = Utils.FLOAT_EPSILON;
        float f3 = oVar != null ? oVar.f(this) : 0.0f;
        if (oVar2 != null) {
            f2 = oVar2.g(this);
        }
        SVG.b a0 = a0();
        return new SVG.b(f3, f2, oVar3 != null ? oVar3.f(this) : a0.c, oVar4 != null ? oVar4.g(this) : a0.d);
    }

    @TargetApi(19)
    public final Path o(SVG.i0 i0Var, SVG.b bVar) {
        Path o0;
        SVG.l0 q = i0Var.a.q(this.d.a.I0);
        boolean z = false;
        if (q == null) {
            N("ClipPath reference '%s' not found", this.d.a.I0);
            return null;
        }
        SVG.e eVar = (SVG.e) q;
        this.e.push(this.d);
        this.d = U(eVar);
        Boolean bool = eVar.o;
        z = (bool == null || bool.booleanValue()) ? true : true;
        Matrix matrix = new Matrix();
        if (!z) {
            matrix.preTranslate(bVar.a, bVar.b);
            matrix.preScale(bVar.c, bVar.d);
        }
        Matrix matrix2 = eVar.n;
        if (matrix2 != null) {
            matrix.preConcat(matrix2);
        }
        Path path = new Path();
        for (SVG.l0 l0Var : eVar.i) {
            if ((l0Var instanceof SVG.i0) && (o0 = o0((SVG.i0) l0Var, true)) != null) {
                path.op(o0, Path.Op.UNION);
            }
        }
        if (this.d.a.I0 != null) {
            if (eVar.h == null) {
                eVar.h = r(path);
            }
            Path o = o(eVar, eVar.h);
            if (o != null) {
                path.op(o, Path.Op.INTERSECT);
            }
        }
        path.transform(matrix);
        this.d = this.e.pop();
        return path;
    }

    @TargetApi(19)
    public final Path o0(SVG.i0 i0Var, boolean z) {
        Path l0;
        Path o;
        this.e.push(this.d);
        h hVar = new h(this, this.d);
        this.d = hVar;
        e1(hVar, i0Var);
        if (I() && g1()) {
            if (i0Var instanceof SVG.b1) {
                if (!z) {
                    N("<use> elements inside a <clipPath> cannot reference another <use>", new Object[0]);
                }
                SVG.b1 b1Var = (SVG.b1) i0Var;
                SVG.l0 q = i0Var.a.q(b1Var.o);
                if (q == null) {
                    N("Use reference '%s' not found", b1Var.o);
                    this.d = this.e.pop();
                    return null;
                } else if (!(q instanceof SVG.i0)) {
                    this.d = this.e.pop();
                    return null;
                } else {
                    l0 = o0((SVG.i0) q, false);
                    if (l0 == null) {
                        return null;
                    }
                    if (b1Var.h == null) {
                        b1Var.h = r(l0);
                    }
                    Matrix matrix = b1Var.n;
                    if (matrix != null) {
                        l0.transform(matrix);
                    }
                }
            } else if (i0Var instanceof SVG.k) {
                SVG.k kVar = (SVG.k) i0Var;
                if (i0Var instanceof SVG.u) {
                    l0 = new d(this, ((SVG.u) i0Var).o).f();
                    if (i0Var.h == null) {
                        i0Var.h = r(l0);
                    }
                } else {
                    l0 = i0Var instanceof SVG.a0 ? k0((SVG.a0) i0Var) : i0Var instanceof SVG.d ? g0((SVG.d) i0Var) : i0Var instanceof SVG.i ? h0((SVG.i) i0Var) : i0Var instanceof SVG.y ? j0((SVG.y) i0Var) : null;
                }
                if (l0 == null) {
                    return null;
                }
                if (kVar.h == null) {
                    kVar.h = r(l0);
                }
                Matrix matrix2 = kVar.n;
                if (matrix2 != null) {
                    l0.transform(matrix2);
                }
                l0.setFillType(X());
            } else if (!(i0Var instanceof SVG.u0)) {
                N("Invalid %s element found in clipPath definition", i0Var.o());
                return null;
            } else {
                SVG.u0 u0Var = (SVG.u0) i0Var;
                l0 = l0(u0Var);
                if (l0 == null) {
                    return null;
                }
                Matrix matrix3 = u0Var.r;
                if (matrix3 != null) {
                    l0.transform(matrix3);
                }
                l0.setFillType(X());
            }
            if (this.d.a.I0 != null && (o = o(i0Var, i0Var.h)) != null) {
                l0.op(o, Path.Op.INTERSECT);
            }
            this.d = this.e.pop();
            return l0;
        }
        this.d = this.e.pop();
        return null;
    }

    public final List<C0080c> p(SVG.p pVar) {
        SVG.o oVar = pVar.o;
        float f2 = Utils.FLOAT_EPSILON;
        float f3 = oVar != null ? oVar.f(this) : 0.0f;
        SVG.o oVar2 = pVar.p;
        float g2 = oVar2 != null ? oVar2.g(this) : 0.0f;
        SVG.o oVar3 = pVar.q;
        float f4 = oVar3 != null ? oVar3.f(this) : 0.0f;
        SVG.o oVar4 = pVar.r;
        if (oVar4 != null) {
            f2 = oVar4.g(this);
        }
        float f5 = f2;
        ArrayList arrayList = new ArrayList(2);
        float f6 = f4 - f3;
        float f7 = f5 - g2;
        arrayList.add(new C0080c(this, f3, g2, f6, f7));
        arrayList.add(new C0080c(this, f4, f5, f6, f7));
        return arrayList;
    }

    public final void p0() {
        this.f.pop();
        this.g.pop();
    }

    public final List<C0080c> q(SVG.y yVar) {
        int length = yVar.o.length;
        int i2 = 2;
        if (length < 2) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        float[] fArr = yVar.o;
        C0080c c0080c = new C0080c(this, fArr[0], fArr[1], Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON);
        float f2 = Utils.FLOAT_EPSILON;
        float f3 = 0.0f;
        while (i2 < length) {
            float[] fArr2 = yVar.o;
            float f4 = fArr2[i2];
            float f5 = fArr2[i2 + 1];
            c0080c.a(f4, f5);
            arrayList.add(c0080c);
            i2 += 2;
            c0080c = new C0080c(this, f4, f5, f4 - c0080c.a, f5 - c0080c.b);
            f3 = f5;
            f2 = f4;
        }
        if (yVar instanceof SVG.z) {
            float[] fArr3 = yVar.o;
            if (f2 != fArr3[0] && f3 != fArr3[1]) {
                float f6 = fArr3[0];
                float f7 = fArr3[1];
                c0080c.a(f6, f7);
                arrayList.add(c0080c);
                C0080c c0080c2 = new C0080c(this, f6, f7, f6 - c0080c.a, f7 - c0080c.b);
                c0080c2.b((C0080c) arrayList.get(0));
                arrayList.add(c0080c2);
                arrayList.set(0, c0080c2);
            }
        } else {
            arrayList.add(c0080c);
        }
        return arrayList;
    }

    public final void q0(SVG.h0 h0Var) {
        this.f.push(h0Var);
        this.g.push(this.a.getMatrix());
    }

    public final SVG.b r(Path path) {
        RectF rectF = new RectF();
        path.computeBounds(rectF, true);
        return new SVG.b(rectF.left, rectF.top, rectF.width(), rectF.height());
    }

    public final void r0(SVG.i0 i0Var) {
        s0(i0Var, i0Var.h);
    }

    public final float s(SVG.w0 w0Var) {
        k kVar = new k(this, null);
        M(w0Var, kVar);
        return kVar.a;
    }

    public final void s0(SVG.i0 i0Var, SVG.b bVar) {
        if (this.d.a.K0 != null) {
            Paint paint = new Paint();
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
            this.a.saveLayer(null, paint, 31);
            Paint paint2 = new Paint();
            paint2.setColorFilter(new ColorMatrixColorFilter(new ColorMatrix(new float[]{Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, 0.2127f, 0.7151f, 0.0722f, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON})));
            this.a.saveLayer(null, paint2, 31);
            SVG.r rVar = (SVG.r) this.c.q(this.d.a.K0);
            R0(rVar, i0Var, bVar);
            this.a.restore();
            Paint paint3 = new Paint();
            paint3.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
            this.a.saveLayer(null, paint3, 31);
            R0(rVar, i0Var, bVar);
            this.a.restore();
            this.a.restore();
        }
        Z0();
    }

    /* JADX WARN: Code restructure failed: missing block: B:32:0x0082, code lost:
        if (r12 != 8) goto L30;
     */
    /* JADX WARN: Removed duplicated region for block: B:23:0x0074  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final android.graphics.Matrix t(com.caverock.androidsvg.SVG.b r10, com.caverock.androidsvg.SVG.b r11, com.caverock.androidsvg.PreserveAspectRatio r12) {
        /*
            r9 = this;
            android.graphics.Matrix r0 = new android.graphics.Matrix
            r0.<init>()
            if (r12 == 0) goto L9b
            com.caverock.androidsvg.PreserveAspectRatio$Alignment r1 = r12.a()
            if (r1 != 0) goto Lf
            goto L9b
        Lf:
            float r1 = r10.c
            float r2 = r11.c
            float r1 = r1 / r2
            float r2 = r10.d
            float r3 = r11.d
            float r2 = r2 / r3
            float r3 = r11.a
            float r3 = -r3
            float r4 = r11.b
            float r4 = -r4
            com.caverock.androidsvg.PreserveAspectRatio r5 = com.caverock.androidsvg.PreserveAspectRatio.c
            boolean r5 = r12.equals(r5)
            if (r5 == 0) goto L35
            float r11 = r10.a
            float r10 = r10.b
            r0.preTranslate(r11, r10)
            r0.preScale(r1, r2)
            r0.preTranslate(r3, r4)
            return r0
        L35:
            com.caverock.androidsvg.PreserveAspectRatio$Scale r5 = r12.b()
            com.caverock.androidsvg.PreserveAspectRatio$Scale r6 = com.caverock.androidsvg.PreserveAspectRatio.Scale.slice
            if (r5 != r6) goto L42
            float r1 = java.lang.Math.max(r1, r2)
            goto L46
        L42:
            float r1 = java.lang.Math.min(r1, r2)
        L46:
            float r2 = r10.c
            float r2 = r2 / r1
            float r5 = r10.d
            float r5 = r5 / r1
            int[] r6 = com.caverock.androidsvg.c.a.a
            com.caverock.androidsvg.PreserveAspectRatio$Alignment r7 = r12.a()
            int r7 = r7.ordinal()
            r7 = r6[r7]
            r8 = 1073741824(0x40000000, float:2.0)
            switch(r7) {
                case 1: goto L62;
                case 2: goto L62;
                case 3: goto L62;
                case 4: goto L5e;
                case 5: goto L5e;
                case 6: goto L5e;
                default: goto L5d;
            }
        L5d:
            goto L67
        L5e:
            float r7 = r11.c
            float r7 = r7 - r2
            goto L66
        L62:
            float r7 = r11.c
            float r7 = r7 - r2
            float r7 = r7 / r8
        L66:
            float r3 = r3 - r7
        L67:
            com.caverock.androidsvg.PreserveAspectRatio$Alignment r12 = r12.a()
            int r12 = r12.ordinal()
            r12 = r6[r12]
            r2 = 2
            if (r12 == r2) goto L89
            r2 = 3
            if (r12 == r2) goto L85
            r2 = 5
            if (r12 == r2) goto L89
            r2 = 6
            if (r12 == r2) goto L85
            r2 = 7
            if (r12 == r2) goto L89
            r2 = 8
            if (r12 == r2) goto L85
            goto L8e
        L85:
            float r11 = r11.d
            float r11 = r11 - r5
            goto L8d
        L89:
            float r11 = r11.d
            float r11 = r11 - r5
            float r11 = r11 / r8
        L8d:
            float r4 = r4 - r11
        L8e:
            float r11 = r10.a
            float r10 = r10.b
            r0.preTranslate(r11, r10)
            r0.preScale(r1, r1)
            r0.preTranslate(r3, r4)
        L9b:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.caverock.androidsvg.c.t(com.caverock.androidsvg.SVG$b, com.caverock.androidsvg.SVG$b, com.caverock.androidsvg.PreserveAspectRatio):android.graphics.Matrix");
    }

    public final void t0(SVG.l0 l0Var, j jVar) {
        float f2;
        float f3;
        float f4;
        SVG.Style.TextAnchor W;
        if (jVar.a((SVG.w0) l0Var)) {
            if (l0Var instanceof SVG.x0) {
                a1();
                T0((SVG.x0) l0Var);
                Z0();
                return;
            }
            boolean z = true;
            if (l0Var instanceof SVG.t0) {
                G("TSpan render", new Object[0]);
                a1();
                SVG.t0 t0Var = (SVG.t0) l0Var;
                e1(this.d, t0Var);
                if (I()) {
                    List<SVG.o> list = t0Var.n;
                    if (list == null || list.size() <= 0) {
                        z = false;
                    }
                    boolean z2 = jVar instanceof f;
                    float f5 = Utils.FLOAT_EPSILON;
                    if (z2) {
                        float f6 = !z ? ((f) jVar).a : t0Var.n.get(0).f(this);
                        List<SVG.o> list2 = t0Var.o;
                        f3 = (list2 == null || list2.size() == 0) ? ((f) jVar).b : t0Var.o.get(0).g(this);
                        List<SVG.o> list3 = t0Var.p;
                        f4 = (list3 == null || list3.size() == 0) ? 0.0f : t0Var.p.get(0).f(this);
                        List<SVG.o> list4 = t0Var.q;
                        if (list4 != null && list4.size() != 0) {
                            f5 = t0Var.q.get(0).g(this);
                        }
                        f2 = f5;
                        f5 = f6;
                    } else {
                        f2 = 0.0f;
                        f3 = 0.0f;
                        f4 = 0.0f;
                    }
                    if (z && (W = W()) != SVG.Style.TextAnchor.Start) {
                        float s = s(t0Var);
                        if (W == SVG.Style.TextAnchor.Middle) {
                            s /= 2.0f;
                        }
                        f5 -= s;
                    }
                    x((SVG.i0) t0Var.g());
                    if (z2) {
                        f fVar = (f) jVar;
                        fVar.a = f5 + f4;
                        fVar.b = f3 + f2;
                    }
                    boolean u0 = u0();
                    M(t0Var, jVar);
                    if (u0) {
                        r0(t0Var);
                    }
                }
                Z0();
            } else if (l0Var instanceof SVG.s0) {
                a1();
                SVG.s0 s0Var = (SVG.s0) l0Var;
                e1(this.d, s0Var);
                if (I()) {
                    x((SVG.i0) s0Var.g());
                    SVG.l0 q = l0Var.a.q(s0Var.n);
                    if (q == null || !(q instanceof SVG.w0)) {
                        N("Tref reference '%s' not found", s0Var.n);
                    } else {
                        StringBuilder sb = new StringBuilder();
                        O((SVG.w0) q, sb);
                        if (sb.length() > 0) {
                            jVar.b(sb.toString());
                        }
                    }
                }
                Z0();
            }
        }
    }

    public final void u(SVG.i0 i0Var) {
        v(i0Var, i0Var.h);
    }

    public final boolean u0() {
        SVG.l0 q;
        if (U0()) {
            this.a.saveLayerAlpha(null, C(this.d.a.q0.floatValue()), 31);
            this.e.push(this.d);
            h hVar = new h(this, this.d);
            this.d = hVar;
            String str = hVar.a.K0;
            if (str != null && ((q = this.c.q(str)) == null || !(q instanceof SVG.r))) {
                N("Mask reference '%s' not found", this.d.a.K0);
                this.d.a.K0 = null;
            }
            return true;
        }
        return false;
    }

    public final void v(SVG.i0 i0Var, SVG.b bVar) {
        if (this.d.a.I0 == null) {
            return;
        }
        if (Build.VERSION.SDK_INT >= 19) {
            Path o = o(i0Var, bVar);
            if (o != null) {
                this.a.clipPath(o);
                return;
            }
            return;
        }
        w(i0Var, bVar);
    }

    public final C0080c v0(C0080c c0080c, C0080c c0080c2, C0080c c0080c3) {
        float L = L(c0080c2.c, c0080c2.d, c0080c2.a - c0080c.a, c0080c2.b - c0080c.b);
        if (L == Utils.FLOAT_EPSILON) {
            L = L(c0080c2.c, c0080c2.d, c0080c3.a - c0080c2.a, c0080c3.b - c0080c2.b);
        }
        int i2 = (L > Utils.FLOAT_EPSILON ? 1 : (L == Utils.FLOAT_EPSILON ? 0 : -1));
        if (i2 > 0) {
            return c0080c2;
        }
        if (i2 != 0 || (c0080c2.c <= Utils.FLOAT_EPSILON && c0080c2.d < Utils.FLOAT_EPSILON)) {
            c0080c2.c = -c0080c2.c;
            c0080c2.d = -c0080c2.d;
            return c0080c2;
        }
        return c0080c2;
    }

    public final void w(SVG.i0 i0Var, SVG.b bVar) {
        SVG.l0 q = i0Var.a.q(this.d.a.I0);
        if (q == null) {
            N("ClipPath reference '%s' not found", this.d.a.I0);
            return;
        }
        SVG.e eVar = (SVG.e) q;
        if (eVar.i.isEmpty()) {
            this.a.clipRect(0, 0, 0, 0);
            return;
        }
        Boolean bool = eVar.o;
        boolean z = bool == null || bool.booleanValue();
        if ((i0Var instanceof SVG.l) && !z) {
            h1("<clipPath clipPathUnits=\"objectBoundingBox\"> is not supported when referenced from container elements (like %s)", i0Var.o());
            return;
        }
        E();
        if (!z) {
            Matrix matrix = new Matrix();
            matrix.preTranslate(bVar.a, bVar.b);
            matrix.preScale(bVar.c, bVar.d);
            this.a.concat(matrix);
        }
        Matrix matrix2 = eVar.n;
        if (matrix2 != null) {
            this.a.concat(matrix2);
        }
        this.d = U(eVar);
        u(eVar);
        Path path = new Path();
        for (SVG.l0 l0Var : eVar.i) {
            j(l0Var, true, path, new Matrix());
        }
        this.a.clipPath(path);
        D();
    }

    public final void w0(SVG.d dVar) {
        G("Circle render", new Object[0]);
        SVG.o oVar = dVar.q;
        if (oVar == null || oVar.j()) {
            return;
        }
        e1(this.d, dVar);
        if (I() && g1()) {
            Matrix matrix = dVar.n;
            if (matrix != null) {
                this.a.concat(matrix);
            }
            Path g0 = g0(dVar);
            c1(dVar);
            x(dVar);
            u(dVar);
            boolean u0 = u0();
            if (this.d.b) {
                J(dVar, g0);
            }
            if (this.d.c) {
                K(g0);
            }
            if (u0) {
                r0(dVar);
            }
        }
    }

    public final void x(SVG.i0 i0Var) {
        SVG.m0 m0Var = this.d.a.f0;
        if (m0Var instanceof SVG.t) {
            H(true, i0Var.h, (SVG.t) m0Var);
        }
        SVG.m0 m0Var2 = this.d.a.i0;
        if (m0Var2 instanceof SVG.t) {
            H(false, i0Var.h, (SVG.t) m0Var2);
        }
    }

    public final void x0(SVG.i iVar) {
        G("Ellipse render", new Object[0]);
        SVG.o oVar = iVar.q;
        if (oVar == null || iVar.r == null || oVar.j() || iVar.r.j()) {
            return;
        }
        e1(this.d, iVar);
        if (I() && g1()) {
            Matrix matrix = iVar.n;
            if (matrix != null) {
                this.a.concat(matrix);
            }
            Path h0 = h0(iVar);
            c1(iVar);
            x(iVar);
            u(iVar);
            boolean u0 = u0();
            if (this.d.b) {
                J(iVar, h0);
            }
            if (this.d.c) {
                K(h0);
            }
            if (u0) {
                r0(iVar);
            }
        }
    }

    public final Bitmap y(String str) {
        int indexOf;
        if (str.startsWith("data:") && str.length() >= 14 && (indexOf = str.indexOf(44)) >= 12 && ";base64".equals(str.substring(indexOf - 7, indexOf))) {
            try {
                byte[] decode = Base64.decode(str.substring(indexOf + 1), 0);
                return BitmapFactory.decodeByteArray(decode, 0, decode.length);
            } catch (Exception unused) {
                return null;
            }
        }
        return null;
    }

    public final void y0(SVG.l lVar) {
        G("Group render", new Object[0]);
        e1(this.d, lVar);
        if (I()) {
            Matrix matrix = lVar.n;
            if (matrix != null) {
                this.a.concat(matrix);
            }
            u(lVar);
            boolean u0 = u0();
            N0(lVar, true);
            if (u0) {
                r0(lVar);
            }
            c1(lVar);
        }
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARN: Code restructure failed: missing block: B:30:0x0052, code lost:
        if (r6.equals("monospace") == false) goto L10;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final android.graphics.Typeface z(java.lang.String r6, java.lang.Integer r7, com.caverock.androidsvg.SVG.Style.FontStyle r8) {
        /*
            r5 = this;
            com.caverock.androidsvg.SVG$Style$FontStyle r0 = com.caverock.androidsvg.SVG.Style.FontStyle.Italic
            r1 = 1
            r2 = 0
            if (r8 != r0) goto L8
            r8 = r1
            goto L9
        L8:
            r8 = r2
        L9:
            int r7 = r7.intValue()
            r0 = 500(0x1f4, float:7.0E-43)
            r3 = 3
            r4 = 2
            if (r7 <= r0) goto L19
            if (r8 == 0) goto L17
            r7 = r3
            goto L1e
        L17:
            r7 = r1
            goto L1e
        L19:
            if (r8 == 0) goto L1d
            r7 = r4
            goto L1e
        L1d:
            r7 = r2
        L1e:
            r6.hashCode()
            r8 = -1
            int r0 = r6.hashCode()
            switch(r0) {
                case -1536685117: goto L55;
                case -1431958525: goto L4c;
                case -1081737434: goto L41;
                case 109326717: goto L36;
                case 1126973893: goto L2b;
                default: goto L29;
            }
        L29:
            r1 = r8
            goto L5f
        L2b:
            java.lang.String r0 = "cursive"
            boolean r6 = r6.equals(r0)
            if (r6 != 0) goto L34
            goto L29
        L34:
            r1 = 4
            goto L5f
        L36:
            java.lang.String r0 = "serif"
            boolean r6 = r6.equals(r0)
            if (r6 != 0) goto L3f
            goto L29
        L3f:
            r1 = r3
            goto L5f
        L41:
            java.lang.String r0 = "fantasy"
            boolean r6 = r6.equals(r0)
            if (r6 != 0) goto L4a
            goto L29
        L4a:
            r1 = r4
            goto L5f
        L4c:
            java.lang.String r0 = "monospace"
            boolean r6 = r6.equals(r0)
            if (r6 != 0) goto L5f
            goto L29
        L55:
            java.lang.String r0 = "sans-serif"
            boolean r6 = r6.equals(r0)
            if (r6 != 0) goto L5e
            goto L29
        L5e:
            r1 = r2
        L5f:
            switch(r1) {
                case 0: goto L80;
                case 1: goto L79;
                case 2: goto L72;
                case 3: goto L6b;
                case 4: goto L64;
                default: goto L62;
            }
        L62:
            r6 = 0
            goto L86
        L64:
            android.graphics.Typeface r6 = android.graphics.Typeface.SANS_SERIF
            android.graphics.Typeface r6 = android.graphics.Typeface.create(r6, r7)
            goto L86
        L6b:
            android.graphics.Typeface r6 = android.graphics.Typeface.SERIF
            android.graphics.Typeface r6 = android.graphics.Typeface.create(r6, r7)
            goto L86
        L72:
            android.graphics.Typeface r6 = android.graphics.Typeface.SANS_SERIF
            android.graphics.Typeface r6 = android.graphics.Typeface.create(r6, r7)
            goto L86
        L79:
            android.graphics.Typeface r6 = android.graphics.Typeface.MONOSPACE
            android.graphics.Typeface r6 = android.graphics.Typeface.create(r6, r7)
            goto L86
        L80:
            android.graphics.Typeface r6 = android.graphics.Typeface.SANS_SERIF
            android.graphics.Typeface r6 = android.graphics.Typeface.create(r6, r7)
        L86:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.caverock.androidsvg.c.z(java.lang.String, java.lang.Integer, com.caverock.androidsvg.SVG$Style$FontStyle):android.graphics.Typeface");
    }

    public final void z0(SVG.n nVar) {
        SVG.o oVar;
        String str;
        G("Image render", new Object[0]);
        SVG.o oVar2 = nVar.r;
        if (oVar2 == null || oVar2.j() || (oVar = nVar.s) == null || oVar.j() || (str = nVar.o) == null) {
            return;
        }
        PreserveAspectRatio preserveAspectRatio = nVar.n;
        if (preserveAspectRatio == null) {
            preserveAspectRatio = PreserveAspectRatio.d;
        }
        Bitmap y = y(str);
        if (y == null) {
            SVG.g();
            return;
        }
        SVG.b bVar = new SVG.b(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, y.getWidth(), y.getHeight());
        e1(this.d, nVar);
        if (I() && g1()) {
            Matrix matrix = nVar.t;
            if (matrix != null) {
                this.a.concat(matrix);
            }
            SVG.o oVar3 = nVar.p;
            float f2 = oVar3 != null ? oVar3.f(this) : 0.0f;
            SVG.o oVar4 = nVar.q;
            this.d.f = new SVG.b(f2, oVar4 != null ? oVar4.g(this) : 0.0f, nVar.r.f(this), nVar.s.f(this));
            if (!this.d.a.z0.booleanValue()) {
                SVG.b bVar2 = this.d.f;
                W0(bVar2.a, bVar2.b, bVar2.c, bVar2.d);
            }
            nVar.h = this.d.f;
            c1(nVar);
            u(nVar);
            boolean u0 = u0();
            f1();
            this.a.save();
            this.a.concat(t(this.d.f, bVar, preserveAspectRatio));
            this.a.drawBitmap(y, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, new Paint(this.d.a.Q0 != SVG.Style.RenderQuality.optimizeSpeed ? 2 : 0));
            this.a.restore();
            if (u0) {
                r0(nVar);
            }
        }
    }

    /* compiled from: SVGAndroidRenderer.java */
    /* loaded from: classes.dex */
    public class k extends j {
        public float a;

        public k() {
            super(c.this, null);
            this.a = Utils.FLOAT_EPSILON;
        }

        @Override // com.caverock.androidsvg.c.j
        public void b(String str) {
            this.a += c.this.d.d.measureText(str);
        }

        public /* synthetic */ k(c cVar, a aVar) {
            this();
        }
    }

    /* compiled from: SVGAndroidRenderer.java */
    /* loaded from: classes.dex */
    public class h {
        public SVG.Style a;
        public boolean b;
        public boolean c;
        public Paint d;
        public Paint e;
        public SVG.b f;
        public SVG.b g;
        public boolean h;

        public h(c cVar) {
            Paint paint = new Paint();
            this.d = paint;
            paint.setFlags(193);
            int i = Build.VERSION.SDK_INT;
            if (i >= 14) {
                this.d.setHinting(0);
            }
            this.d.setStyle(Paint.Style.FILL);
            this.d.setTypeface(Typeface.DEFAULT);
            Paint paint2 = new Paint();
            this.e = paint2;
            paint2.setFlags(193);
            if (i >= 14) {
                this.e.setHinting(0);
            }
            this.e.setStyle(Paint.Style.STROKE);
            this.e.setTypeface(Typeface.DEFAULT);
            this.a = SVG.Style.a();
        }

        public h(c cVar, h hVar) {
            this.b = hVar.b;
            this.c = hVar.c;
            this.d = new Paint(hVar.d);
            this.e = new Paint(hVar.e);
            SVG.b bVar = hVar.f;
            if (bVar != null) {
                this.f = new SVG.b(bVar);
            }
            SVG.b bVar2 = hVar.g;
            if (bVar2 != null) {
                this.g = new SVG.b(bVar2);
            }
            this.h = hVar.h;
            try {
                this.a = (SVG.Style) hVar.a.clone();
            } catch (CloneNotSupportedException unused) {
                this.a = SVG.Style.a();
            }
        }
    }
}
