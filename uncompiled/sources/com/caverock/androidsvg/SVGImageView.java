package com.caverock.androidsvg;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.graphics.Picture;
import android.graphics.drawable.PictureDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;

/* loaded from: classes.dex */
public class SVGImageView extends ImageView {
    public static Method g0;
    public SVG a;
    public com.caverock.androidsvg.b f0;

    /* loaded from: classes.dex */
    public class b extends AsyncTask<Integer, Integer, SVG> {
        public Context a;
        public int b;

        public b(Context context, int i) {
            this.a = context;
            this.b = i;
        }

        @Override // android.os.AsyncTask
        /* renamed from: a */
        public SVG doInBackground(Integer... numArr) {
            try {
                return SVG.i(this.a, this.b);
            } catch (SVGParseException e) {
                String.format("Error loading resource 0x%x: %s", Integer.valueOf(this.b), e.getMessage());
                return null;
            }
        }

        @Override // android.os.AsyncTask
        /* renamed from: b */
        public void onPostExecute(SVG svg) {
            SVGImageView.this.a = svg;
            SVGImageView.this.c();
        }
    }

    /* loaded from: classes.dex */
    public class c extends AsyncTask<InputStream, Integer, SVG> {
        public c() {
        }

        @Override // android.os.AsyncTask
        /* renamed from: a */
        public SVG doInBackground(InputStream... inputStreamArr) {
            try {
                try {
                    SVG h = SVG.h(inputStreamArr[0]);
                    try {
                        inputStreamArr[0].close();
                    } catch (IOException unused) {
                    }
                    return h;
                } catch (Throwable th) {
                    try {
                        inputStreamArr[0].close();
                    } catch (IOException unused2) {
                    }
                    throw th;
                }
            } catch (SVGParseException e) {
                StringBuilder sb = new StringBuilder();
                sb.append("Parse error loading URI: ");
                sb.append(e.getMessage());
                try {
                    inputStreamArr[0].close();
                    return null;
                } catch (IOException unused3) {
                    return null;
                }
            }
        }

        @Override // android.os.AsyncTask
        /* renamed from: b */
        public void onPostExecute(SVG svg) {
            SVGImageView.this.a = svg;
            SVGImageView.this.c();
        }
    }

    static {
        try {
            g0 = View.class.getMethod("setLayerType", Integer.TYPE, Paint.class);
        } catch (NoSuchMethodException unused) {
        }
    }

    public SVGImageView(Context context) {
        super(context);
        this.a = null;
        this.f0 = new com.caverock.androidsvg.b();
    }

    private void setFromString(String str) {
        try {
            this.a = SVG.k(str);
            c();
        } catch (SVGParseException unused) {
            StringBuilder sb = new StringBuilder();
            sb.append("Could not find SVG at: ");
            sb.append(str);
        }
    }

    public final void c() {
        SVG svg = this.a;
        if (svg == null) {
            return;
        }
        Picture p = svg.p(this.f0);
        g();
        setImageDrawable(new PictureDrawable(p));
    }

    public final void d(AttributeSet attributeSet, int i) {
        if (isInEditMode()) {
            return;
        }
        TypedArray obtainStyledAttributes = getContext().getTheme().obtainStyledAttributes(attributeSet, i23.SVGImageView, i, 0);
        try {
            String string = obtainStyledAttributes.getString(i23.SVGImageView_css);
            if (string != null) {
                this.f0.a(string);
            }
            int i2 = i23.SVGImageView_svg;
            int resourceId = obtainStyledAttributes.getResourceId(i2, -1);
            if (resourceId != -1) {
                setImageResource(resourceId);
                return;
            }
            String string2 = obtainStyledAttributes.getString(i2);
            if (string2 != null) {
                if (f(Uri.parse(string2))) {
                    return;
                }
                if (e(string2)) {
                    return;
                }
                setFromString(string2);
            }
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    public final boolean e(String str) {
        try {
            new c().execute(getContext().getAssets().open(str));
            return true;
        } catch (IOException unused) {
            return false;
        }
    }

    public final boolean f(Uri uri) {
        try {
            new c().execute(getContext().getContentResolver().openInputStream(uri));
            return true;
        } catch (FileNotFoundException unused) {
            return false;
        }
    }

    public final void g() {
        if (g0 == null) {
            return;
        }
        try {
            g0.invoke(this, Integer.valueOf(View.class.getField("LAYER_TYPE_SOFTWARE").getInt(new View(getContext()))), null);
        } catch (Exception unused) {
        }
    }

    public void setCSS(String str) {
        this.f0.a(str);
        c();
    }

    public void setImageAsset(String str) {
        if (e(str)) {
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("File not found: ");
        sb.append(str);
    }

    @Override // android.widget.ImageView
    public void setImageResource(int i) {
        new b(getContext(), i).execute(new Integer[0]);
    }

    @Override // android.widget.ImageView
    public void setImageURI(Uri uri) {
        if (f(uri)) {
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("File not found: ");
        sb.append(uri);
    }

    public void setSVG(SVG svg) {
        if (svg != null) {
            this.a = svg;
            c();
            return;
        }
        throw new IllegalArgumentException("Null value passed to setSVG()");
    }

    public SVGImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 0);
        this.a = null;
        this.f0 = new com.caverock.androidsvg.b();
        d(attributeSet, 0);
    }

    public void setSVG(SVG svg, String str) {
        if (svg != null) {
            this.a = svg;
            this.f0.a(str);
            c();
            return;
        }
        throw new IllegalArgumentException("Null value passed to setSVG()");
    }

    public SVGImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.a = null;
        this.f0 = new com.caverock.androidsvg.b();
        d(attributeSet, i);
    }
}
