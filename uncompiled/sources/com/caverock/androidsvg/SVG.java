package com.caverock.androidsvg;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Picture;
import androidx.recyclerview.widget.RecyclerView;
import com.caverock.androidsvg.CSSParser;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import com.github.mikephil.charting.utils.Utils;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.web3j.ens.contracts.generated.PublicResolver;

/* loaded from: classes.dex */
public class SVG {
    public static sb3 e = null;
    public static boolean f = true;
    public d0 a = null;
    public float b = 96.0f;
    public CSSParser.n c = new CSSParser.n();
    public Map<String, j0> d = new HashMap();

    /* loaded from: classes.dex */
    public enum GradientSpread {
        pad,
        reflect,
        repeat
    }

    /* loaded from: classes.dex */
    public static class Style implements Cloneable {
        public c A0;
        public String B0;
        public String C0;
        public String D0;
        public Boolean E0;
        public Boolean F0;
        public m0 G0;
        public Float H0;
        public String I0;
        public FillRule J0;
        public String K0;
        public m0 L0;
        public Float M0;
        public m0 N0;
        public Float O0;
        public VectorEffect P0;
        public RenderQuality Q0;
        public long a = 0;
        public m0 f0;
        public FillRule g0;
        public Float h0;
        public m0 i0;
        public Float j0;
        public o k0;
        public LineCap l0;
        public LineJoin m0;
        public Float n0;
        public o[] o0;
        public o p0;
        public Float q0;
        public f r0;
        public List<String> s0;
        public o t0;
        public Integer u0;
        public FontStyle v0;
        public TextDecoration w0;
        public TextDirection x0;
        public TextAnchor y0;
        public Boolean z0;

        /* loaded from: classes.dex */
        public enum FillRule {
            NonZero,
            EvenOdd
        }

        /* loaded from: classes.dex */
        public enum FontStyle {
            Normal,
            Italic,
            Oblique
        }

        /* loaded from: classes.dex */
        public enum LineCap {
            Butt,
            Round,
            Square
        }

        /* loaded from: classes.dex */
        public enum LineJoin {
            Miter,
            Round,
            Bevel
        }

        /* loaded from: classes.dex */
        public enum RenderQuality {
            auto,
            optimizeQuality,
            optimizeSpeed
        }

        /* loaded from: classes.dex */
        public enum TextAnchor {
            Start,
            Middle,
            End
        }

        /* loaded from: classes.dex */
        public enum TextDecoration {
            None,
            Underline,
            Overline,
            LineThrough,
            Blink
        }

        /* loaded from: classes.dex */
        public enum TextDirection {
            LTR,
            RTL
        }

        /* loaded from: classes.dex */
        public enum VectorEffect {
            None,
            NonScalingStroke
        }

        public static Style a() {
            Style style = new Style();
            style.a = -1L;
            f fVar = f.f0;
            style.f0 = fVar;
            FillRule fillRule = FillRule.NonZero;
            style.g0 = fillRule;
            Float valueOf = Float.valueOf(1.0f);
            style.h0 = valueOf;
            style.i0 = null;
            style.j0 = valueOf;
            style.k0 = new o(1.0f);
            style.l0 = LineCap.Butt;
            style.m0 = LineJoin.Miter;
            style.n0 = Float.valueOf(4.0f);
            style.o0 = null;
            style.p0 = new o(Utils.FLOAT_EPSILON);
            style.q0 = valueOf;
            style.r0 = fVar;
            style.s0 = null;
            style.t0 = new o(12.0f, Unit.pt);
            style.u0 = 400;
            style.v0 = FontStyle.Normal;
            style.w0 = TextDecoration.None;
            style.x0 = TextDirection.LTR;
            style.y0 = TextAnchor.Start;
            Boolean bool = Boolean.TRUE;
            style.z0 = bool;
            style.A0 = null;
            style.B0 = null;
            style.C0 = null;
            style.D0 = null;
            style.E0 = bool;
            style.F0 = bool;
            style.G0 = fVar;
            style.H0 = valueOf;
            style.I0 = null;
            style.J0 = fillRule;
            style.K0 = null;
            style.L0 = null;
            style.M0 = valueOf;
            style.N0 = null;
            style.O0 = valueOf;
            style.P0 = VectorEffect.None;
            style.Q0 = RenderQuality.auto;
            return style;
        }

        public void b(boolean z) {
            Boolean bool = Boolean.TRUE;
            this.E0 = bool;
            if (!z) {
                bool = Boolean.FALSE;
            }
            this.z0 = bool;
            this.A0 = null;
            this.I0 = null;
            this.q0 = Float.valueOf(1.0f);
            this.G0 = f.f0;
            this.H0 = Float.valueOf(1.0f);
            this.K0 = null;
            this.L0 = null;
            this.M0 = Float.valueOf(1.0f);
            this.N0 = null;
            this.O0 = Float.valueOf(1.0f);
            this.P0 = VectorEffect.None;
        }

        public Object clone() throws CloneNotSupportedException {
            Style style = (Style) super.clone();
            o[] oVarArr = this.o0;
            if (oVarArr != null) {
                style.o0 = (o[]) oVarArr.clone();
            }
            return style;
        }
    }

    /* loaded from: classes.dex */
    public enum Unit {
        px,
        em,
        ex,
        in,
        cm,
        mm,
        pt,
        pc,
        percent
    }

    /* loaded from: classes.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[Unit.values().length];
            a = iArr;
            try {
                iArr[Unit.px.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[Unit.em.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[Unit.ex.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                a[Unit.in.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                a[Unit.cm.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                a[Unit.mm.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                a[Unit.pt.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                a[Unit.pc.ordinal()] = 8;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                a[Unit.percent.ordinal()] = 9;
            } catch (NoSuchFieldError unused9) {
            }
        }
    }

    /* loaded from: classes.dex */
    public static class a0 extends k {
        public o o;
        public o p;
        public o q;
        public o r;
        public o s;
        public o t;

        @Override // com.caverock.androidsvg.SVG.l0
        public String o() {
            return "rect";
        }
    }

    /* loaded from: classes.dex */
    public static class a1 extends l0 implements v0 {
        public String c;
        public z0 d;

        public a1(String str) {
            this.c = str;
        }

        @Override // com.caverock.androidsvg.SVG.v0
        public z0 g() {
            return this.d;
        }

        public String toString() {
            return "TextChild: '" + this.c + "'";
        }
    }

    /* loaded from: classes.dex */
    public static class b0 extends j0 implements h0 {
        @Override // com.caverock.androidsvg.SVG.h0
        public List<l0> a() {
            return Collections.emptyList();
        }

        @Override // com.caverock.androidsvg.SVG.h0
        public void c(l0 l0Var) {
        }

        @Override // com.caverock.androidsvg.SVG.l0
        public String o() {
            return "solidColor";
        }
    }

    /* loaded from: classes.dex */
    public static class b1 extends l {
        public String o;
        public o p;
        public o q;
        public o r;
        public o s;

        @Override // com.caverock.androidsvg.SVG.l, com.caverock.androidsvg.SVG.l0
        public String o() {
            return "use";
        }
    }

    /* loaded from: classes.dex */
    public static class c {
        public o a;
        public o b;
        public o c;
        public o d;

        public c(o oVar, o oVar2, o oVar3, o oVar4) {
            this.a = oVar;
            this.b = oVar2;
            this.c = oVar3;
            this.d = oVar4;
        }
    }

    /* loaded from: classes.dex */
    public static class c0 extends j0 implements h0 {
        public Float h;

        @Override // com.caverock.androidsvg.SVG.h0
        public List<l0> a() {
            return Collections.emptyList();
        }

        @Override // com.caverock.androidsvg.SVG.h0
        public void c(l0 l0Var) {
        }

        @Override // com.caverock.androidsvg.SVG.l0
        public String o() {
            return "stop";
        }
    }

    /* loaded from: classes.dex */
    public static class c1 extends p0 implements s {
        @Override // com.caverock.androidsvg.SVG.l0
        public String o() {
            return "view";
        }
    }

    /* loaded from: classes.dex */
    public static class d extends k {
        public o o;
        public o p;
        public o q;

        @Override // com.caverock.androidsvg.SVG.l0
        public String o() {
            return "circle";
        }
    }

    /* loaded from: classes.dex */
    public static class d0 extends p0 {
        public o p;
        public o q;
        public o r;
        public o s;
        public String t;

        @Override // com.caverock.androidsvg.SVG.l0
        public String o() {
            return "svg";
        }
    }

    /* loaded from: classes.dex */
    public static class e extends l implements s {
        public Boolean o;

        @Override // com.caverock.androidsvg.SVG.l, com.caverock.androidsvg.SVG.l0
        public String o() {
            return "clipPath";
        }
    }

    /* loaded from: classes.dex */
    public interface e0 {
        Set<String> b();

        void d(Set<String> set);

        String e();

        void f(Set<String> set);

        void h(Set<String> set);

        Set<String> i();

        void j(String str);

        void l(Set<String> set);

        Set<String> m();

        Set<String> n();
    }

    /* loaded from: classes.dex */
    public static class f extends m0 {
        public static final f f0 = new f(-16777216);
        public static final f g0 = new f(0);
        public int a;

        public f(int i) {
            this.a = i;
        }

        public String toString() {
            return String.format("#%08x", Integer.valueOf(this.a));
        }
    }

    /* loaded from: classes.dex */
    public static abstract class f0 extends i0 implements h0, e0 {
        public List<l0> i = new ArrayList();
        public Set<String> j = null;
        public String k = null;
        public Set<String> l = null;
        public Set<String> m = null;

        @Override // com.caverock.androidsvg.SVG.h0
        public List<l0> a() {
            return this.i;
        }

        @Override // com.caverock.androidsvg.SVG.e0
        public Set<String> b() {
            return null;
        }

        @Override // com.caverock.androidsvg.SVG.h0
        public void c(l0 l0Var) throws SVGParseException {
            this.i.add(l0Var);
        }

        @Override // com.caverock.androidsvg.SVG.e0
        public void d(Set<String> set) {
            this.l = set;
        }

        @Override // com.caverock.androidsvg.SVG.e0
        public String e() {
            return this.k;
        }

        @Override // com.caverock.androidsvg.SVG.e0
        public void f(Set<String> set) {
            this.m = set;
        }

        @Override // com.caverock.androidsvg.SVG.e0
        public void h(Set<String> set) {
            this.j = set;
        }

        @Override // com.caverock.androidsvg.SVG.e0
        public Set<String> i() {
            return this.j;
        }

        @Override // com.caverock.androidsvg.SVG.e0
        public void j(String str) {
            this.k = str;
        }

        @Override // com.caverock.androidsvg.SVG.e0
        public void l(Set<String> set) {
        }

        @Override // com.caverock.androidsvg.SVG.e0
        public Set<String> m() {
            return this.l;
        }

        @Override // com.caverock.androidsvg.SVG.e0
        public Set<String> n() {
            return this.m;
        }
    }

    /* loaded from: classes.dex */
    public static class g extends m0 {
        public static g a = new g();

        public static g a() {
            return a;
        }
    }

    /* loaded from: classes.dex */
    public static abstract class g0 extends i0 implements e0 {
        public Set<String> i = null;
        public String j = null;
        public Set<String> k = null;
        public Set<String> l = null;
        public Set<String> m = null;

        @Override // com.caverock.androidsvg.SVG.e0
        public Set<String> b() {
            return this.k;
        }

        @Override // com.caverock.androidsvg.SVG.e0
        public void d(Set<String> set) {
            this.l = set;
        }

        @Override // com.caverock.androidsvg.SVG.e0
        public String e() {
            return this.j;
        }

        @Override // com.caverock.androidsvg.SVG.e0
        public void f(Set<String> set) {
            this.m = set;
        }

        @Override // com.caverock.androidsvg.SVG.e0
        public void h(Set<String> set) {
            this.i = set;
        }

        @Override // com.caverock.androidsvg.SVG.e0
        public Set<String> i() {
            return this.i;
        }

        @Override // com.caverock.androidsvg.SVG.e0
        public void j(String str) {
            this.j = str;
        }

        @Override // com.caverock.androidsvg.SVG.e0
        public void l(Set<String> set) {
            this.k = set;
        }

        @Override // com.caverock.androidsvg.SVG.e0
        public Set<String> m() {
            return this.l;
        }

        @Override // com.caverock.androidsvg.SVG.e0
        public Set<String> n() {
            return this.m;
        }
    }

    /* loaded from: classes.dex */
    public static class h extends l implements s {
        @Override // com.caverock.androidsvg.SVG.l, com.caverock.androidsvg.SVG.l0
        public String o() {
            return "defs";
        }
    }

    /* loaded from: classes.dex */
    public interface h0 {
        List<l0> a();

        void c(l0 l0Var) throws SVGParseException;
    }

    /* loaded from: classes.dex */
    public static class i extends k {
        public o o;
        public o p;
        public o q;
        public o r;

        @Override // com.caverock.androidsvg.SVG.l0
        public String o() {
            return "ellipse";
        }
    }

    /* loaded from: classes.dex */
    public static abstract class i0 extends j0 {
        public b h = null;
    }

    /* loaded from: classes.dex */
    public static abstract class j extends j0 implements h0 {
        public List<l0> h = new ArrayList();
        public Boolean i;
        public Matrix j;
        public GradientSpread k;
        public String l;

        @Override // com.caverock.androidsvg.SVG.h0
        public List<l0> a() {
            return this.h;
        }

        @Override // com.caverock.androidsvg.SVG.h0
        public void c(l0 l0Var) throws SVGParseException {
            if (l0Var instanceof c0) {
                this.h.add(l0Var);
                return;
            }
            throw new SVGParseException("Gradient elements cannot contain " + l0Var + " elements.");
        }
    }

    /* loaded from: classes.dex */
    public static abstract class j0 extends l0 {
        public String c = null;
        public Boolean d = null;
        public Style e = null;
        public Style f = null;
        public List<String> g = null;

        public String toString() {
            return o();
        }
    }

    /* loaded from: classes.dex */
    public static abstract class k extends g0 implements m {
        public Matrix n;

        @Override // com.caverock.androidsvg.SVG.m
        public void k(Matrix matrix) {
            this.n = matrix;
        }
    }

    /* loaded from: classes.dex */
    public static class k0 extends j {
        public o m;
        public o n;
        public o o;
        public o p;

        @Override // com.caverock.androidsvg.SVG.l0
        public String o() {
            return "linearGradient";
        }
    }

    /* loaded from: classes.dex */
    public static class l extends f0 implements m {
        public Matrix n;

        @Override // com.caverock.androidsvg.SVG.m
        public void k(Matrix matrix) {
            this.n = matrix;
        }

        @Override // com.caverock.androidsvg.SVG.l0
        public String o() {
            return "group";
        }
    }

    /* loaded from: classes.dex */
    public static class l0 {
        public SVG a;
        public h0 b;

        public String o() {
            return "";
        }
    }

    /* loaded from: classes.dex */
    public interface m {
        void k(Matrix matrix);
    }

    /* loaded from: classes.dex */
    public static abstract class m0 implements Cloneable {
    }

    /* loaded from: classes.dex */
    public static class n extends n0 implements m {
        public String o;
        public o p;
        public o q;
        public o r;
        public o s;
        public Matrix t;

        @Override // com.caverock.androidsvg.SVG.m
        public void k(Matrix matrix) {
            this.t = matrix;
        }

        @Override // com.caverock.androidsvg.SVG.l0
        public String o() {
            return "image";
        }
    }

    /* loaded from: classes.dex */
    public static abstract class n0 extends f0 {
        public PreserveAspectRatio n = null;
    }

    /* loaded from: classes.dex */
    public static class o0 extends j {
        public o m;
        public o n;
        public o o;
        public o p;
        public o q;

        @Override // com.caverock.androidsvg.SVG.l0
        public String o() {
            return "radialGradient";
        }
    }

    /* loaded from: classes.dex */
    public static class p extends k {
        public o o;
        public o p;
        public o q;
        public o r;

        @Override // com.caverock.androidsvg.SVG.l0
        public String o() {
            return "line";
        }
    }

    /* loaded from: classes.dex */
    public static abstract class p0 extends n0 {
        public b o;
    }

    /* loaded from: classes.dex */
    public static class q extends p0 implements s {
        public boolean p;
        public o q;
        public o r;
        public o s;
        public o t;
        public Float u;

        @Override // com.caverock.androidsvg.SVG.l0
        public String o() {
            return "marker";
        }
    }

    /* loaded from: classes.dex */
    public static class q0 extends l {
        @Override // com.caverock.androidsvg.SVG.l, com.caverock.androidsvg.SVG.l0
        public String o() {
            return "switch";
        }
    }

    /* loaded from: classes.dex */
    public static class r extends f0 implements s {
        public Boolean n;
        public Boolean o;
        public o p;
        public o q;
        public o r;
        public o s;

        @Override // com.caverock.androidsvg.SVG.l0
        public String o() {
            return "mask";
        }
    }

    /* loaded from: classes.dex */
    public static class r0 extends p0 implements s {
        @Override // com.caverock.androidsvg.SVG.l0
        public String o() {
            return "symbol";
        }
    }

    /* loaded from: classes.dex */
    public interface s {
    }

    /* loaded from: classes.dex */
    public static class s0 extends w0 implements v0 {
        public String n;
        public z0 o;

        @Override // com.caverock.androidsvg.SVG.v0
        public z0 g() {
            return this.o;
        }

        @Override // com.caverock.androidsvg.SVG.l0
        public String o() {
            return "tref";
        }

        public void p(z0 z0Var) {
            this.o = z0Var;
        }
    }

    /* loaded from: classes.dex */
    public static class t extends m0 {
        public String a;
        public m0 f0;

        public t(String str, m0 m0Var) {
            this.a = str;
            this.f0 = m0Var;
        }

        public String toString() {
            return this.a + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + this.f0;
        }
    }

    /* loaded from: classes.dex */
    public static class t0 extends y0 implements v0 {
        public z0 r;

        @Override // com.caverock.androidsvg.SVG.v0
        public z0 g() {
            return this.r;
        }

        @Override // com.caverock.androidsvg.SVG.l0
        public String o() {
            return "tspan";
        }

        public void p(z0 z0Var) {
            this.r = z0Var;
        }
    }

    /* loaded from: classes.dex */
    public static class u extends k {
        public v o;
        public Float p;

        @Override // com.caverock.androidsvg.SVG.l0
        public String o() {
            return "path";
        }
    }

    /* loaded from: classes.dex */
    public static class u0 extends y0 implements z0, m {
        public Matrix r;

        @Override // com.caverock.androidsvg.SVG.m
        public void k(Matrix matrix) {
            this.r = matrix;
        }

        @Override // com.caverock.androidsvg.SVG.l0
        public String o() {
            return PublicResolver.FUNC_TEXT;
        }
    }

    /* loaded from: classes.dex */
    public static class v implements w {
        public int b = 0;
        public int d = 0;
        public byte[] a = new byte[8];
        public float[] c = new float[16];

        @Override // com.caverock.androidsvg.SVG.w
        public void a(float f, float f2, float f3, float f4) {
            f((byte) 3);
            g(4);
            float[] fArr = this.c;
            int i = this.d;
            int i2 = i + 1;
            this.d = i2;
            fArr[i] = f;
            int i3 = i2 + 1;
            this.d = i3;
            fArr[i2] = f2;
            int i4 = i3 + 1;
            this.d = i4;
            fArr[i3] = f3;
            this.d = i4 + 1;
            fArr[i4] = f4;
        }

        @Override // com.caverock.androidsvg.SVG.w
        public void b(float f, float f2) {
            f((byte) 0);
            g(2);
            float[] fArr = this.c;
            int i = this.d;
            int i2 = i + 1;
            this.d = i2;
            fArr[i] = f;
            this.d = i2 + 1;
            fArr[i2] = f2;
        }

        @Override // com.caverock.androidsvg.SVG.w
        public void c(float f, float f2, float f3, float f4, float f5, float f6) {
            f((byte) 2);
            g(6);
            float[] fArr = this.c;
            int i = this.d;
            int i2 = i + 1;
            this.d = i2;
            fArr[i] = f;
            int i3 = i2 + 1;
            this.d = i3;
            fArr[i2] = f2;
            int i4 = i3 + 1;
            this.d = i4;
            fArr[i3] = f3;
            int i5 = i4 + 1;
            this.d = i5;
            fArr[i4] = f4;
            int i6 = i5 + 1;
            this.d = i6;
            fArr[i5] = f5;
            this.d = i6 + 1;
            fArr[i6] = f6;
        }

        @Override // com.caverock.androidsvg.SVG.w
        public void close() {
            f((byte) 8);
        }

        @Override // com.caverock.androidsvg.SVG.w
        public void d(float f, float f2, float f3, boolean z, boolean z2, float f4, float f5) {
            f((byte) ((z ? 2 : 0) | 4 | (z2 ? 1 : 0)));
            g(5);
            float[] fArr = this.c;
            int i = this.d;
            int i2 = i + 1;
            this.d = i2;
            fArr[i] = f;
            int i3 = i2 + 1;
            this.d = i3;
            fArr[i2] = f2;
            int i4 = i3 + 1;
            this.d = i4;
            fArr[i3] = f3;
            int i5 = i4 + 1;
            this.d = i5;
            fArr[i4] = f4;
            this.d = i5 + 1;
            fArr[i5] = f5;
        }

        @Override // com.caverock.androidsvg.SVG.w
        public void e(float f, float f2) {
            f((byte) 1);
            g(2);
            float[] fArr = this.c;
            int i = this.d;
            int i2 = i + 1;
            this.d = i2;
            fArr[i] = f;
            this.d = i2 + 1;
            fArr[i2] = f2;
        }

        public final void f(byte b) {
            int i = this.b;
            byte[] bArr = this.a;
            if (i == bArr.length) {
                byte[] bArr2 = new byte[bArr.length * 2];
                System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
                this.a = bArr2;
            }
            byte[] bArr3 = this.a;
            int i2 = this.b;
            this.b = i2 + 1;
            bArr3[i2] = b;
        }

        public final void g(int i) {
            float[] fArr = this.c;
            if (fArr.length < this.d + i) {
                float[] fArr2 = new float[fArr.length * 2];
                System.arraycopy(fArr, 0, fArr2, 0, fArr.length);
                this.c = fArr2;
            }
        }

        public void h(w wVar) {
            int i;
            int i2 = 0;
            for (int i3 = 0; i3 < this.b; i3++) {
                byte b = this.a[i3];
                if (b == 0) {
                    float[] fArr = this.c;
                    int i4 = i2 + 1;
                    i = i4 + 1;
                    wVar.b(fArr[i2], fArr[i4]);
                } else if (b != 1) {
                    if (b == 2) {
                        float[] fArr2 = this.c;
                        int i5 = i2 + 1;
                        float f = fArr2[i2];
                        int i6 = i5 + 1;
                        float f2 = fArr2[i5];
                        int i7 = i6 + 1;
                        float f3 = fArr2[i6];
                        int i8 = i7 + 1;
                        float f4 = fArr2[i7];
                        int i9 = i8 + 1;
                        float f5 = fArr2[i8];
                        i2 = i9 + 1;
                        wVar.c(f, f2, f3, f4, f5, fArr2[i9]);
                    } else if (b == 3) {
                        float[] fArr3 = this.c;
                        int i10 = i2 + 1;
                        int i11 = i10 + 1;
                        int i12 = i11 + 1;
                        wVar.a(fArr3[i2], fArr3[i10], fArr3[i11], fArr3[i12]);
                        i2 = i12 + 1;
                    } else if (b != 8) {
                        boolean z = (b & 2) != 0;
                        boolean z2 = (b & 1) != 0;
                        float[] fArr4 = this.c;
                        int i13 = i2 + 1;
                        float f6 = fArr4[i2];
                        int i14 = i13 + 1;
                        float f7 = fArr4[i13];
                        int i15 = i14 + 1;
                        float f8 = fArr4[i14];
                        int i16 = i15 + 1;
                        wVar.d(f6, f7, f8, z, z2, fArr4[i15], fArr4[i16]);
                        i2 = i16 + 1;
                    } else {
                        wVar.close();
                    }
                } else {
                    float[] fArr5 = this.c;
                    int i17 = i2 + 1;
                    i = i17 + 1;
                    wVar.e(fArr5[i2], fArr5[i17]);
                }
                i2 = i;
            }
        }

        public boolean i() {
            return this.b == 0;
        }
    }

    /* loaded from: classes.dex */
    public interface v0 {
        z0 g();
    }

    /* loaded from: classes.dex */
    public interface w {
        void a(float f, float f2, float f3, float f4);

        void b(float f, float f2);

        void c(float f, float f2, float f3, float f4, float f5, float f6);

        void close();

        void d(float f, float f2, float f3, boolean z, boolean z2, float f4, float f5);

        void e(float f, float f2);
    }

    /* loaded from: classes.dex */
    public static abstract class w0 extends f0 {
        @Override // com.caverock.androidsvg.SVG.f0, com.caverock.androidsvg.SVG.h0
        public void c(l0 l0Var) throws SVGParseException {
            if (l0Var instanceof v0) {
                this.i.add(l0Var);
                return;
            }
            throw new SVGParseException("Text content elements cannot contain " + l0Var + " elements.");
        }
    }

    /* loaded from: classes.dex */
    public static class x extends p0 implements s {
        public Boolean p;
        public Boolean q;
        public Matrix r;
        public o s;
        public o t;
        public o u;
        public o v;
        public String w;

        @Override // com.caverock.androidsvg.SVG.l0
        public String o() {
            return "pattern";
        }
    }

    /* loaded from: classes.dex */
    public static class x0 extends w0 implements v0 {
        public String n;
        public o o;
        public z0 p;

        @Override // com.caverock.androidsvg.SVG.v0
        public z0 g() {
            return this.p;
        }

        @Override // com.caverock.androidsvg.SVG.l0
        public String o() {
            return "textPath";
        }

        public void p(z0 z0Var) {
            this.p = z0Var;
        }
    }

    /* loaded from: classes.dex */
    public static class y extends k {
        public float[] o;

        @Override // com.caverock.androidsvg.SVG.l0
        public String o() {
            return "polyline";
        }
    }

    /* loaded from: classes.dex */
    public static abstract class y0 extends w0 {
        public List<o> n;
        public List<o> o;
        public List<o> p;
        public List<o> q;
    }

    /* loaded from: classes.dex */
    public static class z extends y {
        @Override // com.caverock.androidsvg.SVG.y, com.caverock.androidsvg.SVG.l0
        public String o() {
            return "polygon";
        }
    }

    /* loaded from: classes.dex */
    public interface z0 {
    }

    public static sb3 g() {
        return e;
    }

    public static SVG h(InputStream inputStream) throws SVGParseException {
        return new SVGParser().z(inputStream, f);
    }

    public static SVG i(Context context, int i2) throws SVGParseException {
        return j(context.getResources(), i2);
    }

    public static SVG j(Resources resources, int i2) throws SVGParseException {
        SVGParser sVGParser = new SVGParser();
        InputStream openRawResource = resources.openRawResource(i2);
        try {
            return sVGParser.z(openRawResource, f);
        } finally {
            try {
                openRawResource.close();
            } catch (IOException unused) {
            }
        }
    }

    public static SVG k(String str) throws SVGParseException {
        return new SVGParser().z(new ByteArrayInputStream(str.getBytes()), f);
    }

    public void a(CSSParser.n nVar) {
        this.c.b(nVar);
    }

    public void b() {
        this.c.e(CSSParser.Source.RenderOptions);
    }

    public final String c(String str) {
        if (str.startsWith("\"") && str.endsWith("\"")) {
            str = str.substring(1, str.length() - 1).replace("\\\"", "\"");
        } else if (str.startsWith("'") && str.endsWith("'")) {
            str = str.substring(1, str.length() - 1).replace("\\'", "'");
        }
        return str.replace("\\\n", "").replace("\\A", "\n");
    }

    public List<CSSParser.l> d() {
        return this.c.c();
    }

    public final j0 e(h0 h0Var, String str) {
        j0 e2;
        j0 j0Var = (j0) h0Var;
        if (str.equals(j0Var.c)) {
            return j0Var;
        }
        for (l0 l0Var : h0Var.a()) {
            if (l0Var instanceof j0) {
                j0 j0Var2 = (j0) l0Var;
                if (str.equals(j0Var2.c)) {
                    return j0Var2;
                }
                if ((l0Var instanceof h0) && (e2 = e((h0) l0Var, str)) != null) {
                    return e2;
                }
            }
        }
        return null;
    }

    public j0 f(String str) {
        if (str == null || str.length() == 0) {
            return null;
        }
        if (str.equals(this.a.c)) {
            return this.a;
        }
        if (this.d.containsKey(str)) {
            return this.d.get(str);
        }
        j0 e2 = e(this.a, str);
        this.d.put(str, e2);
        return e2;
    }

    public d0 l() {
        return this.a;
    }

    public boolean m() {
        return !this.c.d();
    }

    public Picture n() {
        return p(null);
    }

    public Picture o(int i2, int i3, com.caverock.androidsvg.b bVar) {
        Picture picture = new Picture();
        Canvas beginRecording = picture.beginRecording(i2, i3);
        if (bVar == null || bVar.f == null) {
            bVar = bVar == null ? new com.caverock.androidsvg.b() : new com.caverock.androidsvg.b(bVar);
            bVar.h(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, i2, i3);
        }
        new com.caverock.androidsvg.c(beginRecording, this.b).O0(this, bVar);
        picture.endRecording();
        return picture;
    }

    public Picture p(com.caverock.androidsvg.b bVar) {
        o oVar;
        b bVar2 = (bVar == null || !bVar.f()) ? this.a.o : bVar.d;
        if (bVar != null && bVar.g()) {
            return o((int) Math.ceil(bVar.f.b()), (int) Math.ceil(bVar.f.c()), bVar);
        }
        d0 d0Var = this.a;
        o oVar2 = d0Var.r;
        if (oVar2 != null) {
            Unit unit = oVar2.f0;
            Unit unit2 = Unit.percent;
            if (unit != unit2 && (oVar = d0Var.s) != null && oVar.f0 != unit2) {
                return o((int) Math.ceil(oVar2.b(this.b)), (int) Math.ceil(this.a.s.b(this.b)), bVar);
            }
        }
        if (oVar2 != null && bVar2 != null) {
            float b2 = oVar2.b(this.b);
            return o((int) Math.ceil(b2), (int) Math.ceil((bVar2.d * b2) / bVar2.c), bVar);
        }
        o oVar3 = d0Var.s;
        if (oVar3 != null && bVar2 != null) {
            float b3 = oVar3.b(this.b);
            return o((int) Math.ceil((bVar2.c * b3) / bVar2.d), (int) Math.ceil(b3), bVar);
        }
        return o(RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN, RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN, bVar);
    }

    public l0 q(String str) {
        if (str == null) {
            return null;
        }
        String c2 = c(str);
        if (c2.length() <= 1 || !c2.startsWith("#")) {
            return null;
        }
        return f(c2.substring(1));
    }

    public void r(String str) {
    }

    public void s(float f2) {
        d0 d0Var = this.a;
        if (d0Var != null) {
            d0Var.s = new o(f2);
            return;
        }
        throw new IllegalArgumentException("SVG document is empty");
    }

    public void t(float f2) {
        d0 d0Var = this.a;
        if (d0Var != null) {
            d0Var.r = new o(f2);
            return;
        }
        throw new IllegalArgumentException("SVG document is empty");
    }

    public void u(d0 d0Var) {
        this.a = d0Var;
    }

    public void v(String str) {
    }

    /* loaded from: classes.dex */
    public static class o implements Cloneable {
        public float a;
        public Unit f0;

        public o(float f, Unit unit) {
            this.a = f;
            this.f0 = unit;
        }

        public float a() {
            return this.a;
        }

        public float b(float f) {
            int i = a.a[this.f0.ordinal()];
            if (i != 1) {
                switch (i) {
                    case 4:
                        return this.a * f;
                    case 5:
                        return (this.a * f) / 2.54f;
                    case 6:
                        return (this.a * f) / 25.4f;
                    case 7:
                        return (this.a * f) / 72.0f;
                    case 8:
                        return (this.a * f) / 6.0f;
                    default:
                        return this.a;
                }
            }
            return this.a;
        }

        public float d(com.caverock.androidsvg.c cVar) {
            if (this.f0 == Unit.percent) {
                b a0 = cVar.a0();
                if (a0 == null) {
                    return this.a;
                }
                float f = a0.c;
                float f2 = a0.d;
                if (f == f2) {
                    return (this.a * f) / 100.0f;
                }
                return (this.a * ((float) (Math.sqrt((f * f) + (f2 * f2)) / 1.414213562373095d))) / 100.0f;
            }
            return f(cVar);
        }

        public float e(com.caverock.androidsvg.c cVar, float f) {
            if (this.f0 == Unit.percent) {
                return (this.a * f) / 100.0f;
            }
            return f(cVar);
        }

        public float f(com.caverock.androidsvg.c cVar) {
            switch (a.a[this.f0.ordinal()]) {
                case 1:
                    return this.a;
                case 2:
                    return this.a * cVar.Y();
                case 3:
                    return this.a * cVar.Z();
                case 4:
                    return this.a * cVar.b0();
                case 5:
                    return (this.a * cVar.b0()) / 2.54f;
                case 6:
                    return (this.a * cVar.b0()) / 25.4f;
                case 7:
                    return (this.a * cVar.b0()) / 72.0f;
                case 8:
                    return (this.a * cVar.b0()) / 6.0f;
                case 9:
                    b a0 = cVar.a0();
                    if (a0 == null) {
                        return this.a;
                    }
                    return (this.a * a0.c) / 100.0f;
                default:
                    return this.a;
            }
        }

        public float g(com.caverock.androidsvg.c cVar) {
            if (this.f0 == Unit.percent) {
                b a0 = cVar.a0();
                if (a0 == null) {
                    return this.a;
                }
                return (this.a * a0.d) / 100.0f;
            }
            return f(cVar);
        }

        public boolean h() {
            return this.a < Utils.FLOAT_EPSILON;
        }

        public boolean j() {
            return this.a == Utils.FLOAT_EPSILON;
        }

        public String toString() {
            return String.valueOf(this.a) + this.f0;
        }

        public o(float f) {
            this.a = f;
            this.f0 = Unit.px;
        }
    }

    /* loaded from: classes.dex */
    public static class b {
        public float a;
        public float b;
        public float c;
        public float d;

        public b(float f, float f2, float f3, float f4) {
            this.a = f;
            this.b = f2;
            this.c = f3;
            this.d = f4;
        }

        public static b a(float f, float f2, float f3, float f4) {
            return new b(f, f2, f3 - f, f4 - f2);
        }

        public float b() {
            return this.a + this.c;
        }

        public float c() {
            return this.b + this.d;
        }

        public void d(b bVar) {
            float f = bVar.a;
            if (f < this.a) {
                this.a = f;
            }
            float f2 = bVar.b;
            if (f2 < this.b) {
                this.b = f2;
            }
            if (bVar.b() > b()) {
                this.c = bVar.b() - this.a;
            }
            if (bVar.c() > c()) {
                this.d = bVar.c() - this.b;
            }
        }

        public String toString() {
            return "[" + this.a + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + this.b + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + this.c + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + this.d + "]";
        }

        public b(b bVar) {
            this.a = bVar.a;
            this.b = bVar.b;
            this.c = bVar.c;
            this.d = bVar.d;
        }
    }
}
