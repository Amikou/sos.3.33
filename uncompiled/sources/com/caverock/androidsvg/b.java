package com.caverock.androidsvg;

import com.caverock.androidsvg.CSSParser;
import com.caverock.androidsvg.SVG;

/* compiled from: RenderOptions.java */
/* loaded from: classes.dex */
public class b {
    public CSSParser.n a;
    public PreserveAspectRatio b;
    public String c;
    public SVG.b d;
    public String e;
    public SVG.b f;

    public b() {
        this.a = null;
        this.b = null;
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = null;
    }

    public b a(String str) {
        this.a = new CSSParser(CSSParser.Source.RenderOptions).d(str);
        return this;
    }

    public boolean b() {
        CSSParser.n nVar = this.a;
        return nVar != null && nVar.f() > 0;
    }

    public boolean c() {
        return this.b != null;
    }

    public boolean d() {
        return this.c != null;
    }

    public boolean e() {
        return this.e != null;
    }

    public boolean f() {
        return this.d != null;
    }

    public boolean g() {
        return this.f != null;
    }

    public b h(float f, float f2, float f3, float f4) {
        this.f = new SVG.b(f, f2, f3, f4);
        return this;
    }

    public b(b bVar) {
        this.a = null;
        this.b = null;
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = null;
        if (bVar == null) {
            return;
        }
        this.a = bVar.a;
        this.b = bVar.b;
        this.d = bVar.d;
        this.e = bVar.e;
        this.f = bVar.f;
    }
}
