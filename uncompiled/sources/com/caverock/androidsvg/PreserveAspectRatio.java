package com.caverock.androidsvg;

import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;

/* loaded from: classes.dex */
public class PreserveAspectRatio {
    public static final PreserveAspectRatio c;
    public static final PreserveAspectRatio d;
    public Alignment a;
    public Scale b;

    /* loaded from: classes.dex */
    public enum Alignment {
        none,
        xMinYMin,
        xMidYMin,
        xMaxYMin,
        xMinYMid,
        xMidYMid,
        xMaxYMid,
        xMinYMax,
        xMidYMax,
        xMaxYMax
    }

    /* loaded from: classes.dex */
    public enum Scale {
        meet,
        slice
    }

    static {
        new PreserveAspectRatio(null, null);
        c = new PreserveAspectRatio(Alignment.none, null);
        Alignment alignment = Alignment.xMidYMid;
        Scale scale = Scale.meet;
        d = new PreserveAspectRatio(alignment, scale);
        Alignment alignment2 = Alignment.xMinYMin;
        new PreserveAspectRatio(alignment2, scale);
        new PreserveAspectRatio(Alignment.xMaxYMax, scale);
        new PreserveAspectRatio(Alignment.xMidYMin, scale);
        new PreserveAspectRatio(Alignment.xMidYMax, scale);
        Scale scale2 = Scale.slice;
        new PreserveAspectRatio(alignment, scale2);
        new PreserveAspectRatio(alignment2, scale2);
    }

    public PreserveAspectRatio(Alignment alignment, Scale scale) {
        this.a = alignment;
        this.b = scale;
    }

    public Alignment a() {
        return this.a;
    }

    public Scale b() {
        return this.b;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && PreserveAspectRatio.class == obj.getClass()) {
            PreserveAspectRatio preserveAspectRatio = (PreserveAspectRatio) obj;
            return this.a == preserveAspectRatio.a && this.b == preserveAspectRatio.b;
        }
        return false;
    }

    public String toString() {
        return this.a + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + this.b;
    }
}
