package com.trustwallet.walletconnect;

import kotlin.jvm.internal.Lambda;

/* compiled from: WCClient.kt */
/* loaded from: classes2.dex */
public final class WCClient$onDisconnect$1 extends Lambda implements hd1<Integer, String, te4> {
    public static final WCClient$onDisconnect$1 INSTANCE = new WCClient$onDisconnect$1();

    public WCClient$onDisconnect$1() {
        super(2);
    }

    @Override // defpackage.hd1
    public /* bridge */ /* synthetic */ te4 invoke(Integer num, String str) {
        invoke(num.intValue(), str);
        return te4.a;
    }

    public final void invoke(int i, String str) {
        fs1.f(str, "$noName_1");
    }
}
