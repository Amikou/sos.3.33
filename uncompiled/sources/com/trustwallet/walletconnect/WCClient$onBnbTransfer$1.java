package com.trustwallet.walletconnect;

import com.trustwallet.walletconnect.models.binance.WCBinanceTransferOrder;
import kotlin.jvm.internal.Lambda;

/* compiled from: WCClient.kt */
/* loaded from: classes2.dex */
public final class WCClient$onBnbTransfer$1 extends Lambda implements hd1<Long, WCBinanceTransferOrder, te4> {
    public static final WCClient$onBnbTransfer$1 INSTANCE = new WCClient$onBnbTransfer$1();

    public WCClient$onBnbTransfer$1() {
        super(2);
    }

    @Override // defpackage.hd1
    public /* bridge */ /* synthetic */ te4 invoke(Long l, WCBinanceTransferOrder wCBinanceTransferOrder) {
        invoke(l.longValue(), wCBinanceTransferOrder);
        return te4.a;
    }

    public final void invoke(long j, WCBinanceTransferOrder wCBinanceTransferOrder) {
        fs1.f(wCBinanceTransferOrder, "$noName_1");
    }
}
