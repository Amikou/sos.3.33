package com.trustwallet.walletconnect;

import com.fasterxml.jackson.databind.deser.std.ThrowableDeserializer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.trustwallet.walletconnect.exceptions.InvalidJsonRpcParamsException;
import com.trustwallet.walletconnect.extensions.StringKt;
import com.trustwallet.walletconnect.jsonrpc.JsonRpcError;
import com.trustwallet.walletconnect.jsonrpc.JsonRpcErrorResponse;
import com.trustwallet.walletconnect.jsonrpc.JsonRpcRequest;
import com.trustwallet.walletconnect.jsonrpc.JsonRpcResponse;
import com.trustwallet.walletconnect.models.MessageType;
import com.trustwallet.walletconnect.models.WCMethod;
import com.trustwallet.walletconnect.models.WCPeerMeta;
import com.trustwallet.walletconnect.models.WCSignTransaction;
import com.trustwallet.walletconnect.models.WCSocketMessage;
import com.trustwallet.walletconnect.models.binance.WCBinanceCancelOrder;
import com.trustwallet.walletconnect.models.binance.WCBinanceTradeOrder;
import com.trustwallet.walletconnect.models.binance.WCBinanceTransferOrder;
import com.trustwallet.walletconnect.models.binance.WCBinanceTxConfirmParam;
import com.trustwallet.walletconnect.models.ethereum.WCEthereumSignMessage;
import com.trustwallet.walletconnect.models.ethereum.WCEthereumTransaction;
import com.trustwallet.walletconnect.models.session.WCApproveSessionResponse;
import com.trustwallet.walletconnect.models.session.WCSession;
import com.trustwallet.walletconnect.models.session.WCSessionUpdate;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: WCClient.kt */
/* loaded from: classes2.dex */
public class WCClient extends WebSocketListener {
    private final String TAG;
    private String chainId;
    private final Gson gson;
    private long handshakeId;
    private final OkHttpClient httpClient;
    private boolean isConnected;
    private final Set<WebSocketListener> listeners;
    private hd1<? super Long, ? super WCBinanceCancelOrder, te4> onBnbCancel;
    private hd1<? super Long, ? super WCBinanceTradeOrder, te4> onBnbTrade;
    private hd1<? super Long, ? super WCBinanceTransferOrder, te4> onBnbTransfer;
    private hd1<? super Long, ? super WCBinanceTxConfirmParam, te4> onBnbTxConfirm;
    private hd1<? super Long, ? super String, te4> onCustomRequest;
    private hd1<? super Integer, ? super String, te4> onDisconnect;
    private hd1<? super Long, ? super WCEthereumTransaction, te4> onEthSendTransaction;
    private hd1<? super Long, ? super WCEthereumSignMessage, te4> onEthSign;
    private hd1<? super Long, ? super WCEthereumTransaction, te4> onEthSignTransaction;
    private tc1<? super Throwable, te4> onFailure;
    private tc1<? super Long, te4> onGetAccounts;
    private hd1<? super Long, ? super WCPeerMeta, te4> onSessionRequest;
    private hd1<? super Long, ? super WCSignTransaction, te4> onSignTransaction;
    private String peerId;
    private WCPeerMeta peerMeta;
    private String remotePeerId;
    private WCSession session;
    private WebSocket socket;

    /* compiled from: WCClient.kt */
    /* loaded from: classes2.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[WCMethod.values().length];
            iArr[WCMethod.SESSION_REQUEST.ordinal()] = 1;
            iArr[WCMethod.SESSION_UPDATE.ordinal()] = 2;
            iArr[WCMethod.ETH_SIGN.ordinal()] = 3;
            iArr[WCMethod.ETH_PERSONAL_SIGN.ordinal()] = 4;
            iArr[WCMethod.ETH_SIGN_TYPE_DATA.ordinal()] = 5;
            iArr[WCMethod.ETH_SIGN_TYPE_DATA_V4.ordinal()] = 6;
            iArr[WCMethod.ETH_SIGN_TRANSACTION.ordinal()] = 7;
            iArr[WCMethod.ETH_SEND_TRANSACTION.ordinal()] = 8;
            iArr[WCMethod.BNB_SIGN.ordinal()] = 9;
            iArr[WCMethod.BNB_TRANSACTION_CONFIRM.ordinal()] = 10;
            iArr[WCMethod.GET_ACCOUNTS.ordinal()] = 11;
            iArr[WCMethod.SIGN_TRANSACTION.ordinal()] = 12;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    public /* synthetic */ WCClient(GsonBuilder gsonBuilder, OkHttpClient okHttpClient, int i, qi0 qi0Var) {
        this((i & 1) != 0 ? new GsonBuilder() : gsonBuilder, okHttpClient);
    }

    public static /* synthetic */ void connect$default(WCClient wCClient, WCSession wCSession, WCPeerMeta wCPeerMeta, String str, String str2, int i, Object obj) {
        if (obj != null) {
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: connect");
        }
        if ((i & 4) != 0) {
            str = UUID.randomUUID().toString();
            fs1.e(str, "randomUUID().toString()");
        }
        if ((i & 8) != 0) {
            str2 = null;
        }
        wCClient.connect(wCSession, wCPeerMeta, str, str2);
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0054  */
    /* JADX WARN: Removed duplicated region for block: B:16:0x0076  */
    /* JADX WARN: Removed duplicated region for block: B:18:0x008c  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    private final java.lang.String decryptMessage(java.lang.String r8) {
        /*
            r7 = this;
            com.google.gson.Gson r0 = r7.gson
            java.lang.String r1 = "gson"
            defpackage.fs1.e(r0, r1)
            com.trustwallet.walletconnect.WCClient$decryptMessage$$inlined$fromJson$1 r2 = new com.trustwallet.walletconnect.WCClient$decryptMessage$$inlined$fromJson$1
            r2.<init>()
            java.lang.reflect.Type r2 = r2.getType()
            java.lang.String r3 = "object : TypeToken<T>() {} .type"
            defpackage.fs1.c(r2, r3)
            boolean r4 = r2 instanceof java.lang.reflect.ParameterizedType
            java.lang.String r5 = "type.rawType"
            if (r4 == 0) goto L2c
            r4 = r2
            java.lang.reflect.ParameterizedType r4 = (java.lang.reflect.ParameterizedType) r4
            boolean r6 = defpackage.xi1.a(r4)
            if (r6 == 0) goto L2c
            java.lang.reflect.Type r2 = r4.getRawType()
            defpackage.fs1.c(r2, r5)
            goto L30
        L2c:
            java.lang.reflect.Type r2 = defpackage.xi1.d(r2)
        L30:
            java.lang.Object r8 = r0.fromJson(r8, r2)
            java.lang.String r0 = "fromJson(json, typeToken<T>())"
            defpackage.fs1.c(r8, r0)
            com.trustwallet.walletconnect.models.WCSocketMessage r8 = (com.trustwallet.walletconnect.models.WCSocketMessage) r8
            com.google.gson.Gson r2 = r7.gson
            defpackage.fs1.e(r2, r1)
            java.lang.String r8 = r8.getPayload()
            com.trustwallet.walletconnect.WCClient$decryptMessage$$inlined$fromJson$2 r1 = new com.trustwallet.walletconnect.WCClient$decryptMessage$$inlined$fromJson$2
            r1.<init>()
            java.lang.reflect.Type r1 = r1.getType()
            defpackage.fs1.c(r1, r3)
            boolean r3 = r1 instanceof java.lang.reflect.ParameterizedType
            if (r3 == 0) goto L65
            r3 = r1
            java.lang.reflect.ParameterizedType r3 = (java.lang.reflect.ParameterizedType) r3
            boolean r4 = defpackage.xi1.a(r3)
            if (r4 == 0) goto L65
            java.lang.reflect.Type r1 = r3.getRawType()
            defpackage.fs1.c(r1, r5)
            goto L69
        L65:
            java.lang.reflect.Type r1 = defpackage.xi1.d(r1)
        L69:
            java.lang.Object r8 = r2.fromJson(r8, r1)
            defpackage.fs1.c(r8, r0)
            com.trustwallet.walletconnect.models.WCEncryptionPayload r8 = (com.trustwallet.walletconnect.models.WCEncryptionPayload) r8
            com.trustwallet.walletconnect.models.session.WCSession r0 = r7.session
            if (r0 == 0) goto L8c
            com.trustwallet.walletconnect.WCCipher r1 = com.trustwallet.walletconnect.WCCipher.INSTANCE
            java.lang.String r0 = r0.getKey()
            byte[] r0 = com.trustwallet.walletconnect.extensions.StringKt.hexStringToByteArray(r0)
            byte[] r8 = r1.decrypt(r8, r0)
            java.nio.charset.Charset r0 = defpackage.by.a
            java.lang.String r1 = new java.lang.String
            r1.<init>(r8, r0)
            return r1
        L8c:
            java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
            java.lang.String r0 = "session can't be null on message receive"
            r8.<init>(r0)
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.trustwallet.walletconnect.WCClient.decryptMessage(java.lang.String):java.lang.String");
    }

    private final boolean encryptAndSend(String str) {
        fs1.l("==> message ", str);
        WCSession wCSession = this.session;
        if (wCSession != null) {
            Gson gson = this.gson;
            WCCipher wCCipher = WCCipher.INSTANCE;
            Charset charset = by.a;
            Objects.requireNonNull(str, "null cannot be cast to non-null type java.lang.String");
            byte[] bytes = str.getBytes(charset);
            fs1.e(bytes, "(this as java.lang.String).getBytes(charset)");
            String json = gson.toJson(wCCipher.encrypt(bytes, StringKt.hexStringToByteArray(wCSession.getKey())));
            String str2 = this.remotePeerId;
            if (str2 == null) {
                str2 = wCSession.getTopic();
            }
            MessageType messageType = MessageType.PUB;
            fs1.e(json, "payload");
            String json2 = this.gson.toJson(new WCSocketMessage(str2, messageType, json));
            fs1.l("==> encrypted ", json2);
            WebSocket webSocket = this.socket;
            if (webSocket == null) {
                return false;
            }
            fs1.e(json2, "json");
            return webSocket.send(json2);
        }
        throw new IllegalStateException("session can't be null on message send");
    }

    private final void handleMessage(String str) {
        Type d;
        try {
            Gson gson = this.gson;
            Type type = new TypeToken<JsonRpcRequest<JsonArray>>() { // from class: com.trustwallet.walletconnect.WCClient$handleMessage$$inlined$typeToken$1
            }.getType();
            fs1.c(type, "object : TypeToken<T>() {} .type");
            if ((type instanceof ParameterizedType) && xi1.a((ParameterizedType) type)) {
                d = ((ParameterizedType) type).getRawType();
                fs1.c(d, "type.rawType");
            } else {
                d = xi1.d(type);
            }
            JsonRpcRequest<JsonArray> jsonRpcRequest = (JsonRpcRequest) gson.fromJson(str, d);
            if (jsonRpcRequest.getMethod() != null) {
                fs1.e(jsonRpcRequest, "request");
                handleRequest(jsonRpcRequest);
                return;
            }
            this.onCustomRequest.invoke(Long.valueOf(jsonRpcRequest.getId()), str);
        } catch (InvalidJsonRpcParamsException e) {
            invalidParams(e.getRequestId());
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:106:0x0345  */
    /* JADX WARN: Removed duplicated region for block: B:107:0x035b  */
    /* JADX WARN: Removed duplicated region for block: B:117:0x03a4  */
    /* JADX WARN: Removed duplicated region for block: B:118:0x03ba  */
    /* JADX WARN: Removed duplicated region for block: B:128:0x0403  */
    /* JADX WARN: Removed duplicated region for block: B:129:0x0419  */
    /* JADX WARN: Removed duplicated region for block: B:139:0x0464  */
    /* JADX WARN: Removed duplicated region for block: B:142:0x046f  */
    /* JADX WARN: Removed duplicated region for block: B:152:0x04ba  */
    /* JADX WARN: Removed duplicated region for block: B:153:0x04de  */
    /* JADX WARN: Removed duplicated region for block: B:17:0x005f  */
    /* JADX WARN: Removed duplicated region for block: B:18:0x006e  */
    /* JADX WARN: Removed duplicated region for block: B:29:0x00c8  */
    /* JADX WARN: Removed duplicated region for block: B:30:0x00d7  */
    /* JADX WARN: Removed duplicated region for block: B:73:0x0234  */
    /* JADX WARN: Removed duplicated region for block: B:74:0x0243  */
    /* JADX WARN: Removed duplicated region for block: B:84:0x028e  */
    /* JADX WARN: Removed duplicated region for block: B:85:0x029d  */
    /* JADX WARN: Removed duplicated region for block: B:95:0x02e6  */
    /* JADX WARN: Removed duplicated region for block: B:96:0x02fc  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    private final void handleRequest(com.trustwallet.walletconnect.jsonrpc.JsonRpcRequest<com.google.gson.JsonArray> r9) {
        /*
            Method dump skipped, instructions count: 1286
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.trustwallet.walletconnect.WCClient.handleRequest(com.trustwallet.walletconnect.jsonrpc.JsonRpcRequest):void");
    }

    private final boolean invalidParams(long j) {
        String json = this.gson.toJson(new JsonRpcErrorResponse(null, j, JsonRpcError.Companion.invalidParams("Invalid parameters"), 1, null));
        fs1.e(json, "gson.toJson(response)");
        return encryptAndSend(json);
    }

    public static /* synthetic */ boolean rejectRequest$default(WCClient wCClient, long j, String str, int i, Object obj) {
        if (obj == null) {
            if ((i & 2) != 0) {
                str = "Reject by the user";
            }
            return wCClient.rejectRequest(j, str);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: rejectRequest");
    }

    public static /* synthetic */ boolean rejectSession$default(WCClient wCClient, String str, int i, Object obj) {
        if (obj == null) {
            if ((i & 1) != 0) {
                str = "Session rejected";
            }
            return wCClient.rejectSession(str);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: rejectSession");
    }

    private final void resetState() {
        this.handshakeId = -1L;
        this.isConnected = false;
        this.session = null;
        this.peerId = null;
        this.remotePeerId = null;
        this.peerMeta = null;
    }

    private final boolean subscribe(String str) {
        WCSocketMessage wCSocketMessage = new WCSocketMessage(str, MessageType.SUB, "");
        fs1.l("==> subscribe ", this.gson.toJson(wCSocketMessage));
        WebSocket webSocket = this.socket;
        if (webSocket == null) {
            return false;
        }
        String json = this.gson.toJson(wCSocketMessage);
        fs1.e(json, "gson.toJson(message)");
        return webSocket.send(json);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ boolean updateSession$default(WCClient wCClient, List list, Integer num, boolean z, int i, Object obj) {
        if (obj == null) {
            if ((i & 1) != 0) {
                list = null;
            }
            if ((i & 2) != 0) {
                num = null;
            }
            if ((i & 4) != 0) {
                z = true;
            }
            return wCClient.updateSession(list, num, z);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: updateSession");
    }

    public final void addSocketListener(WebSocketListener webSocketListener) {
        fs1.f(webSocketListener, "listener");
        this.listeners.add(webSocketListener);
    }

    public final <T> boolean approveRequest(long j, T t) {
        String json = this.gson.toJson(new JsonRpcResponse(null, j, t, 1, null));
        fs1.e(json, "gson.toJson(response)");
        return encryptAndSend(json);
    }

    public final boolean approveSession(List<String> list, int i) {
        fs1.f(list, "accounts");
        if (this.handshakeId > 0) {
            this.chainId = String.valueOf(i);
            String json = this.gson.toJson(new JsonRpcResponse(null, this.handshakeId, new WCApproveSessionResponse(false, i, list, this.peerId, this.peerMeta, 1, null), 1, null));
            fs1.e(json, "gson.toJson(response)");
            return encryptAndSend(json);
        }
        throw new IllegalStateException("handshakeId must be greater than 0 on session approve".toString());
    }

    public final void connect(WCSession wCSession, WCPeerMeta wCPeerMeta, String str, String str2) {
        fs1.f(wCSession, "session");
        fs1.f(wCPeerMeta, "peerMeta");
        fs1.f(str, "peerId");
        WCSession wCSession2 = this.session;
        if (wCSession2 != null) {
            if (!fs1.b(wCSession2 == null ? null : wCSession2.getTopic(), wCSession.getTopic())) {
                killSession();
            }
        }
        this.session = wCSession;
        this.peerMeta = wCPeerMeta;
        this.peerId = str;
        this.remotePeerId = str2;
        this.socket = this.httpClient.newWebSocket(new Request.Builder().url(wCSession.getBridge()).build(), this);
    }

    public final boolean disconnect() {
        WebSocket webSocket = this.socket;
        if (webSocket == null) {
            return false;
        }
        return webSocket.close(1000, null);
    }

    public final String getChainId() {
        return this.chainId;
    }

    public final hd1<Long, WCBinanceCancelOrder, te4> getOnBnbCancel() {
        return this.onBnbCancel;
    }

    public final hd1<Long, WCBinanceTradeOrder, te4> getOnBnbTrade() {
        return this.onBnbTrade;
    }

    public final hd1<Long, WCBinanceTransferOrder, te4> getOnBnbTransfer() {
        return this.onBnbTransfer;
    }

    public final hd1<Long, WCBinanceTxConfirmParam, te4> getOnBnbTxConfirm() {
        return this.onBnbTxConfirm;
    }

    public final hd1<Long, String, te4> getOnCustomRequest() {
        return this.onCustomRequest;
    }

    public final hd1<Integer, String, te4> getOnDisconnect() {
        return this.onDisconnect;
    }

    public final hd1<Long, WCEthereumTransaction, te4> getOnEthSendTransaction() {
        return this.onEthSendTransaction;
    }

    public final hd1<Long, WCEthereumSignMessage, te4> getOnEthSign() {
        return this.onEthSign;
    }

    public final hd1<Long, WCEthereumTransaction, te4> getOnEthSignTransaction() {
        return this.onEthSignTransaction;
    }

    public final tc1<Throwable, te4> getOnFailure() {
        return this.onFailure;
    }

    public final tc1<Long, te4> getOnGetAccounts() {
        return this.onGetAccounts;
    }

    public final hd1<Long, WCPeerMeta, te4> getOnSessionRequest() {
        return this.onSessionRequest;
    }

    public final hd1<Long, WCSignTransaction, te4> getOnSignTransaction() {
        return this.onSignTransaction;
    }

    public final String getPeerId() {
        return this.peerId;
    }

    public final WCPeerMeta getPeerMeta() {
        return this.peerMeta;
    }

    public final String getRemotePeerId() {
        return this.remotePeerId;
    }

    public final WCSession getSession() {
        return this.session;
    }

    public final boolean isConnected() {
        return this.isConnected;
    }

    public final boolean killSession() {
        updateSession$default(this, null, null, false, 3, null);
        return disconnect();
    }

    @Override // okhttp3.WebSocketListener
    public void onClosed(WebSocket webSocket, int i, String str) {
        fs1.f(webSocket, "webSocket");
        fs1.f(str, "reason");
        for (WebSocketListener webSocketListener : this.listeners) {
            webSocketListener.onClosed(webSocket, i, str);
        }
    }

    @Override // okhttp3.WebSocketListener
    public void onClosing(WebSocket webSocket, int i, String str) {
        fs1.f(webSocket, "webSocket");
        fs1.f(str, "reason");
        resetState();
        this.onDisconnect.invoke(Integer.valueOf(i), str);
        for (WebSocketListener webSocketListener : this.listeners) {
            webSocketListener.onClosing(webSocket, i, str);
        }
    }

    @Override // okhttp3.WebSocketListener
    public void onFailure(WebSocket webSocket, Throwable th, Response response) {
        fs1.f(webSocket, "webSocket");
        fs1.f(th, "t");
        resetState();
        this.onFailure.invoke(th);
        for (WebSocketListener webSocketListener : this.listeners) {
            webSocketListener.onFailure(webSocket, th, response);
        }
    }

    @Override // okhttp3.WebSocketListener
    public void onMessage(WebSocket webSocket, String str) {
        fs1.f(webSocket, "webSocket");
        fs1.f(str, PublicResolver.FUNC_TEXT);
        String str2 = null;
        try {
            try {
                fs1.l("<== message ", str);
                str2 = decryptMessage(str);
                fs1.l("<== decrypted ", str2);
                handleMessage(str2);
                for (WebSocketListener webSocketListener : this.listeners) {
                    webSocketListener.onMessage(webSocket, str2 == null ? str : str2);
                }
            } catch (Exception e) {
                this.onFailure.invoke(e);
                for (WebSocketListener webSocketListener2 : this.listeners) {
                    webSocketListener2.onMessage(webSocket, str2 == null ? str : str2);
                }
            }
        } catch (Throwable th) {
            for (WebSocketListener webSocketListener3 : this.listeners) {
                webSocketListener3.onMessage(webSocket, str2 == null ? str : str2);
            }
            throw th;
        }
    }

    @Override // okhttp3.WebSocketListener
    public void onOpen(WebSocket webSocket, Response response) {
        fs1.f(webSocket, "webSocket");
        fs1.f(response, "response");
        this.isConnected = true;
        for (WebSocketListener webSocketListener : this.listeners) {
            webSocketListener.onOpen(webSocket, response);
        }
        WCSession wCSession = this.session;
        if (wCSession != null) {
            String str = this.peerId;
            if (str != null) {
                subscribe(wCSession.getTopic());
                subscribe(str);
                return;
            }
            throw new IllegalStateException("peerId can't be null on connection open");
        }
        throw new IllegalStateException("session can't be null on connection open");
    }

    public final boolean rejectRequest(long j, String str) {
        fs1.f(str, ThrowableDeserializer.PROP_NAME_MESSAGE);
        String json = this.gson.toJson(new JsonRpcErrorResponse(null, j, JsonRpcError.Companion.serverError(str), 1, null));
        fs1.e(json, "gson.toJson(response)");
        return encryptAndSend(json);
    }

    public final boolean rejectSession(String str) {
        fs1.f(str, ThrowableDeserializer.PROP_NAME_MESSAGE);
        long j = this.handshakeId;
        if (j > 0) {
            String json = this.gson.toJson(new JsonRpcErrorResponse(null, j, JsonRpcError.Companion.serverError(str), 1, null));
            fs1.e(json, "gson.toJson(response)");
            return encryptAndSend(json);
        }
        throw new IllegalStateException("handshakeId must be greater than 0 on session reject".toString());
    }

    public final void removeSocketListener(WebSocketListener webSocketListener) {
        fs1.f(webSocketListener, "listener");
        this.listeners.remove(webSocketListener);
    }

    public final void setOnBnbCancel(hd1<? super Long, ? super WCBinanceCancelOrder, te4> hd1Var) {
        fs1.f(hd1Var, "<set-?>");
        this.onBnbCancel = hd1Var;
    }

    public final void setOnBnbTrade(hd1<? super Long, ? super WCBinanceTradeOrder, te4> hd1Var) {
        fs1.f(hd1Var, "<set-?>");
        this.onBnbTrade = hd1Var;
    }

    public final void setOnBnbTransfer(hd1<? super Long, ? super WCBinanceTransferOrder, te4> hd1Var) {
        fs1.f(hd1Var, "<set-?>");
        this.onBnbTransfer = hd1Var;
    }

    public final void setOnBnbTxConfirm(hd1<? super Long, ? super WCBinanceTxConfirmParam, te4> hd1Var) {
        fs1.f(hd1Var, "<set-?>");
        this.onBnbTxConfirm = hd1Var;
    }

    public final void setOnCustomRequest(hd1<? super Long, ? super String, te4> hd1Var) {
        fs1.f(hd1Var, "<set-?>");
        this.onCustomRequest = hd1Var;
    }

    public final void setOnDisconnect(hd1<? super Integer, ? super String, te4> hd1Var) {
        fs1.f(hd1Var, "<set-?>");
        this.onDisconnect = hd1Var;
    }

    public final void setOnEthSendTransaction(hd1<? super Long, ? super WCEthereumTransaction, te4> hd1Var) {
        fs1.f(hd1Var, "<set-?>");
        this.onEthSendTransaction = hd1Var;
    }

    public final void setOnEthSign(hd1<? super Long, ? super WCEthereumSignMessage, te4> hd1Var) {
        fs1.f(hd1Var, "<set-?>");
        this.onEthSign = hd1Var;
    }

    public final void setOnEthSignTransaction(hd1<? super Long, ? super WCEthereumTransaction, te4> hd1Var) {
        fs1.f(hd1Var, "<set-?>");
        this.onEthSignTransaction = hd1Var;
    }

    public final void setOnFailure(tc1<? super Throwable, te4> tc1Var) {
        fs1.f(tc1Var, "<set-?>");
        this.onFailure = tc1Var;
    }

    public final void setOnGetAccounts(tc1<? super Long, te4> tc1Var) {
        fs1.f(tc1Var, "<set-?>");
        this.onGetAccounts = tc1Var;
    }

    public final void setOnSessionRequest(hd1<? super Long, ? super WCPeerMeta, te4> hd1Var) {
        fs1.f(hd1Var, "<set-?>");
        this.onSessionRequest = hd1Var;
    }

    public final void setOnSignTransaction(hd1<? super Long, ? super WCSignTransaction, te4> hd1Var) {
        fs1.f(hd1Var, "<set-?>");
        this.onSignTransaction = hd1Var;
    }

    public final boolean updateSession(List<String> list, Integer num, boolean z) {
        long generateId;
        generateId = WCClientKt.generateId();
        WCMethod wCMethod = WCMethod.SESSION_UPDATE;
        if (num == null) {
            String str = this.chainId;
            num = str == null ? null : cv3.l(str);
        }
        String json = this.gson.toJson(new JsonRpcRequest(generateId, null, wCMethod, a20.b(new WCSessionUpdate(z, num, list)), 2, null));
        fs1.e(json, "gson.toJson(request)");
        return encryptAndSend(json);
    }

    /* JADX WARN: Removed duplicated region for block: B:12:0x005c  */
    /* JADX WARN: Removed duplicated region for block: B:20:0x008e  */
    /* JADX WARN: Removed duplicated region for block: B:28:0x00c0  */
    /* JADX WARN: Removed duplicated region for block: B:36:0x00f2  */
    /* JADX WARN: Removed duplicated region for block: B:44:0x0124  */
    /* JADX WARN: Removed duplicated region for block: B:52:0x0156  */
    /* JADX WARN: Removed duplicated region for block: B:60:0x01c8  */
    /* JADX WARN: Removed duplicated region for block: B:62:0x01ce  */
    /* JADX WARN: Removed duplicated region for block: B:64:0x01d4  */
    /* JADX WARN: Removed duplicated region for block: B:66:0x01da  */
    /* JADX WARN: Removed duplicated region for block: B:68:0x01e0  */
    /* JADX WARN: Removed duplicated region for block: B:70:0x01e6  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public WCClient(com.google.gson.GsonBuilder r8, okhttp3.OkHttpClient r9) {
        /*
            Method dump skipped, instructions count: 498
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.trustwallet.walletconnect.WCClient.<init>(com.google.gson.GsonBuilder, okhttp3.OkHttpClient):void");
    }

    @Override // okhttp3.WebSocketListener
    public void onMessage(WebSocket webSocket, ByteString byteString) {
        fs1.f(webSocket, "webSocket");
        fs1.f(byteString, "bytes");
        for (WebSocketListener webSocketListener : this.listeners) {
            webSocketListener.onMessage(webSocket, byteString);
        }
    }
}
