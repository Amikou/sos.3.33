package com.trustwallet.walletconnect;

import com.trustwallet.walletconnect.exceptions.InvalidHmacException;
import com.trustwallet.walletconnect.extensions.ByteArrayKt;
import com.trustwallet.walletconnect.extensions.StringKt;
import com.trustwallet.walletconnect.models.WCEncryptionPayload;
import java.security.SecureRandom;
import java.util.Locale;
import java.util.Objects;
import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/* compiled from: WCCipher.kt */
/* loaded from: classes2.dex */
public final class WCCipher {
    public static final WCCipher INSTANCE = new WCCipher();

    private WCCipher() {
    }

    private final String computeHmac(byte[] bArr, byte[] bArr2, byte[] bArr3) {
        String str;
        String str2;
        str = WCCipherKt.MAC_ALGORITHM;
        Mac mac = Mac.getInstance(str);
        byte[] n = zh.n(bArr, bArr2);
        str2 = WCCipherKt.MAC_ALGORITHM;
        mac.init(new SecretKeySpec(bArr3, str2));
        byte[] doFinal = mac.doFinal(n);
        fs1.e(doFinal, "mac.doFinal(payload)");
        return ByteArrayKt.toHex(doFinal);
    }

    private final byte[] randomBytes(int i) {
        byte[] bArr = new byte[i];
        new SecureRandom().nextBytes(bArr);
        return bArr;
    }

    public final byte[] decrypt(WCEncryptionPayload wCEncryptionPayload, byte[] bArr) {
        String str;
        fs1.f(wCEncryptionPayload, "payload");
        fs1.f(bArr, "key");
        byte[] hexStringToByteArray = StringKt.hexStringToByteArray(wCEncryptionPayload.getData());
        byte[] hexStringToByteArray2 = StringKt.hexStringToByteArray(wCEncryptionPayload.getIv());
        String computeHmac = computeHmac(hexStringToByteArray, hexStringToByteArray2, bArr);
        String hmac = wCEncryptionPayload.getHmac();
        Locale locale = Locale.getDefault();
        fs1.e(locale, "getDefault()");
        Objects.requireNonNull(hmac, "null cannot be cast to non-null type java.lang.String");
        String lowerCase = hmac.toLowerCase(locale);
        fs1.e(lowerCase, "(this as java.lang.String).toLowerCase(locale)");
        if (fs1.b(computeHmac, lowerCase)) {
            SecretKeySpec secretKeySpec = new SecretKeySpec(bArr, "AES");
            IvParameterSpec ivParameterSpec = new IvParameterSpec(hexStringToByteArray2);
            str = WCCipherKt.CIPHER_ALGORITHM;
            Cipher cipher = Cipher.getInstance(str);
            cipher.init(2, secretKeySpec, ivParameterSpec);
            byte[] doFinal = cipher.doFinal(hexStringToByteArray);
            fs1.e(doFinal, "cipher.doFinal(data)");
            return doFinal;
        }
        throw new InvalidHmacException();
    }

    public final WCEncryptionPayload encrypt(byte[] bArr, byte[] bArr2) {
        String str;
        fs1.f(bArr, "data");
        fs1.f(bArr2, "key");
        byte[] randomBytes = randomBytes(16);
        SecretKeySpec secretKeySpec = new SecretKeySpec(bArr2, "AES");
        IvParameterSpec ivParameterSpec = new IvParameterSpec(randomBytes);
        str = WCCipherKt.CIPHER_ALGORITHM;
        Cipher cipher = Cipher.getInstance(str);
        cipher.init(1, secretKeySpec, ivParameterSpec);
        byte[] doFinal = cipher.doFinal(bArr);
        fs1.e(doFinal, "encryptedData");
        return new WCEncryptionPayload(ByteArrayKt.toHex(doFinal), computeHmac(doFinal, randomBytes, bArr2), ByteArrayKt.toHex(randomBytes));
    }
}
