package com.trustwallet.walletconnect.extensions;

import java.util.Locale;
import kotlin.text.StringsKt__StringsKt;

/* compiled from: String.kt */
/* loaded from: classes2.dex */
public final class StringKt {
    private static final String HEX_CHARS = "0123456789abcdef";

    public static final byte[] hexStringToByteArray(String str) {
        fs1.f(str, "<this>");
        Locale locale = Locale.getDefault();
        fs1.e(locale, "getDefault()");
        String lowerCase = str.toLowerCase(locale);
        fs1.e(lowerCase, "(this as java.lang.String).toLowerCase(locale)");
        byte[] bArr = new byte[str.length() / 2];
        qr1 j = u33.j(u33.k(0, lowerCase.length()), 2);
        int m = j.m();
        int n = j.n();
        int o = j.o();
        if ((o > 0 && m <= n) || (o < 0 && n <= m)) {
            while (true) {
                int i = m + o;
                String str2 = HEX_CHARS;
                bArr[m >> 1] = (byte) (StringsKt__StringsKt.Y(str2, lowerCase.charAt(m + 1), 0, false, 6, null) | (StringsKt__StringsKt.Y(str2, lowerCase.charAt(m), 0, false, 6, null) << 4));
                if (m == n) {
                    break;
                }
                m = i;
            }
        }
        return bArr;
    }
}
