package com.trustwallet.walletconnect.extensions;

/* compiled from: ByteArray.kt */
/* loaded from: classes2.dex */
public final class ByteArrayKt {
    private static final char[] HEX_CHARS;

    static {
        char[] charArray = "0123456789abcdef".toCharArray();
        fs1.e(charArray, "(this as java.lang.String).toCharArray()");
        HEX_CHARS = charArray;
    }

    public static final String toHex(byte[] bArr) {
        fs1.f(bArr, "<this>");
        StringBuffer stringBuffer = new StringBuffer();
        for (byte b : bArr) {
            char[] cArr = HEX_CHARS;
            stringBuffer.append(cArr[(b & 240) >>> 4]);
            stringBuffer.append(cArr[b & 15]);
        }
        String stringBuffer2 = stringBuffer.toString();
        fs1.e(stringBuffer2, "result.toString()");
        return stringBuffer2;
    }
}
