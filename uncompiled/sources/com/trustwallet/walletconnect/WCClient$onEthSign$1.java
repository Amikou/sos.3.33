package com.trustwallet.walletconnect;

import com.trustwallet.walletconnect.models.ethereum.WCEthereumSignMessage;
import kotlin.jvm.internal.Lambda;

/* compiled from: WCClient.kt */
/* loaded from: classes2.dex */
public final class WCClient$onEthSign$1 extends Lambda implements hd1<Long, WCEthereumSignMessage, te4> {
    public static final WCClient$onEthSign$1 INSTANCE = new WCClient$onEthSign$1();

    public WCClient$onEthSign$1() {
        super(2);
    }

    @Override // defpackage.hd1
    public /* bridge */ /* synthetic */ te4 invoke(Long l, WCEthereumSignMessage wCEthereumSignMessage) {
        invoke(l.longValue(), wCEthereumSignMessage);
        return te4.a;
    }

    public final void invoke(long j, WCEthereumSignMessage wCEthereumSignMessage) {
        fs1.f(wCEthereumSignMessage, "$noName_1");
    }
}
