package com.trustwallet.walletconnect.models.ethereum;

/* compiled from: WCEthereumTransaction.kt */
/* loaded from: classes2.dex */
public final class WCEthereumTransaction {
    private final String data;
    private final String from;
    private final String gas;
    private final String gasLimit;
    private final String gasPrice;
    private final String maxFeePerGas;
    private final String maxPriorityFeePerGas;
    private final String nonce;
    private final String to;
    private final String value;

    public WCEthereumTransaction(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10) {
        fs1.f(str, "from");
        fs1.f(str10, "data");
        this.from = str;
        this.to = str2;
        this.nonce = str3;
        this.gasPrice = str4;
        this.maxFeePerGas = str5;
        this.maxPriorityFeePerGas = str6;
        this.gas = str7;
        this.gasLimit = str8;
        this.value = str9;
        this.data = str10;
    }

    public final String component1() {
        return this.from;
    }

    public final String component10() {
        return this.data;
    }

    public final String component2() {
        return this.to;
    }

    public final String component3() {
        return this.nonce;
    }

    public final String component4() {
        return this.gasPrice;
    }

    public final String component5() {
        return this.maxFeePerGas;
    }

    public final String component6() {
        return this.maxPriorityFeePerGas;
    }

    public final String component7() {
        return this.gas;
    }

    public final String component8() {
        return this.gasLimit;
    }

    public final String component9() {
        return this.value;
    }

    public final WCEthereumTransaction copy(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10) {
        fs1.f(str, "from");
        fs1.f(str10, "data");
        return new WCEthereumTransaction(str, str2, str3, str4, str5, str6, str7, str8, str9, str10);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof WCEthereumTransaction) {
            WCEthereumTransaction wCEthereumTransaction = (WCEthereumTransaction) obj;
            return fs1.b(this.from, wCEthereumTransaction.from) && fs1.b(this.to, wCEthereumTransaction.to) && fs1.b(this.nonce, wCEthereumTransaction.nonce) && fs1.b(this.gasPrice, wCEthereumTransaction.gasPrice) && fs1.b(this.maxFeePerGas, wCEthereumTransaction.maxFeePerGas) && fs1.b(this.maxPriorityFeePerGas, wCEthereumTransaction.maxPriorityFeePerGas) && fs1.b(this.gas, wCEthereumTransaction.gas) && fs1.b(this.gasLimit, wCEthereumTransaction.gasLimit) && fs1.b(this.value, wCEthereumTransaction.value) && fs1.b(this.data, wCEthereumTransaction.data);
        }
        return false;
    }

    public final String getData() {
        return this.data;
    }

    public final String getFrom() {
        return this.from;
    }

    public final String getGas() {
        return this.gas;
    }

    public final String getGasLimit() {
        return this.gasLimit;
    }

    public final String getGasPrice() {
        return this.gasPrice;
    }

    public final String getMaxFeePerGas() {
        return this.maxFeePerGas;
    }

    public final String getMaxPriorityFeePerGas() {
        return this.maxPriorityFeePerGas;
    }

    public final String getNonce() {
        return this.nonce;
    }

    public final String getTo() {
        return this.to;
    }

    public final String getValue() {
        return this.value;
    }

    public int hashCode() {
        int hashCode = this.from.hashCode() * 31;
        String str = this.to;
        int hashCode2 = (hashCode + (str == null ? 0 : str.hashCode())) * 31;
        String str2 = this.nonce;
        int hashCode3 = (hashCode2 + (str2 == null ? 0 : str2.hashCode())) * 31;
        String str3 = this.gasPrice;
        int hashCode4 = (hashCode3 + (str3 == null ? 0 : str3.hashCode())) * 31;
        String str4 = this.maxFeePerGas;
        int hashCode5 = (hashCode4 + (str4 == null ? 0 : str4.hashCode())) * 31;
        String str5 = this.maxPriorityFeePerGas;
        int hashCode6 = (hashCode5 + (str5 == null ? 0 : str5.hashCode())) * 31;
        String str6 = this.gas;
        int hashCode7 = (hashCode6 + (str6 == null ? 0 : str6.hashCode())) * 31;
        String str7 = this.gasLimit;
        int hashCode8 = (hashCode7 + (str7 == null ? 0 : str7.hashCode())) * 31;
        String str8 = this.value;
        return ((hashCode8 + (str8 != null ? str8.hashCode() : 0)) * 31) + this.data.hashCode();
    }

    public String toString() {
        return "WCEthereumTransaction(from=" + this.from + ", to=" + ((Object) this.to) + ", nonce=" + ((Object) this.nonce) + ", gasPrice=" + ((Object) this.gasPrice) + ", maxFeePerGas=" + ((Object) this.maxFeePerGas) + ", maxPriorityFeePerGas=" + ((Object) this.maxPriorityFeePerGas) + ", gas=" + ((Object) this.gas) + ", gasLimit=" + ((Object) this.gasLimit) + ", value=" + ((Object) this.value) + ", data=" + this.data + ')';
    }
}
