package com.trustwallet.walletconnect.models.ethereum;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import defpackage.om0;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import kotlin.jvm.internal.Lambda;

/* compiled from: WCEthereumTransaction.kt */
/* loaded from: classes2.dex */
public final class WCEthereumTransactionKt$ethTransactionSerializer$1 extends Lambda implements tc1<om0, List<? extends WCEthereumTransaction>> {
    public static final WCEthereumTransactionKt$ethTransactionSerializer$1 INSTANCE = new WCEthereumTransactionKt$ethTransactionSerializer$1();

    public WCEthereumTransactionKt$ethTransactionSerializer$1() {
        super(1);
    }

    @Override // defpackage.tc1
    public final List<WCEthereumTransaction> invoke(om0 om0Var) {
        Type d;
        fs1.f(om0Var, "it");
        ArrayList arrayList = new ArrayList();
        JsonArray asJsonArray = om0Var.b().getAsJsonArray();
        fs1.e(asJsonArray, "it.json.asJsonArray");
        for (JsonElement jsonElement : asJsonArray) {
            if (jsonElement.isJsonObject()) {
                om0.a a = om0Var.a();
                fs1.e(jsonElement, "tx");
                JsonDeserializationContext a2 = a.a();
                Type type = new TypeToken<WCEthereumTransaction>() { // from class: com.trustwallet.walletconnect.models.ethereum.WCEthereumTransactionKt$ethTransactionSerializer$1$invoke$lambda-0$$inlined$deserialize$1
                }.getType();
                fs1.c(type, "object : TypeToken<T>() {} .type");
                if (type instanceof ParameterizedType) {
                    ParameterizedType parameterizedType = (ParameterizedType) type;
                    if (xi1.a(parameterizedType)) {
                        d = parameterizedType.getRawType();
                        fs1.c(d, "type.rawType");
                        Object deserialize = a2.deserialize(jsonElement, d);
                        fs1.e(deserialize, "it.context.deserialize(tx)");
                        arrayList.add(deserialize);
                    }
                }
                d = xi1.d(type);
                Object deserialize2 = a2.deserialize(jsonElement, d);
                fs1.e(deserialize2, "it.context.deserialize(tx)");
                arrayList.add(deserialize2);
            }
        }
        return arrayList;
    }
}
