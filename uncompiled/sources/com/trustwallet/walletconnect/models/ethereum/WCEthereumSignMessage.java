package com.trustwallet.walletconnect.models.ethereum;

import java.util.List;
import kotlin.NoWhenBranchMatchedException;

/* compiled from: WCEthereumSignMessage.kt */
/* loaded from: classes2.dex */
public final class WCEthereumSignMessage {
    private final List<String> raw;
    private final WCSignType type;

    /* compiled from: WCEthereumSignMessage.kt */
    /* loaded from: classes2.dex */
    public enum WCSignType {
        MESSAGE,
        PERSONAL_MESSAGE,
        TYPED_MESSAGE,
        ETH_SIGN_TYPE_DATA_V4
    }

    /* compiled from: WCEthereumSignMessage.kt */
    /* loaded from: classes2.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[WCSignType.values().length];
            iArr[WCSignType.MESSAGE.ordinal()] = 1;
            iArr[WCSignType.TYPED_MESSAGE.ordinal()] = 2;
            iArr[WCSignType.ETH_SIGN_TYPE_DATA_V4.ordinal()] = 3;
            iArr[WCSignType.PERSONAL_MESSAGE.ordinal()] = 4;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    public WCEthereumSignMessage(List<String> list, WCSignType wCSignType) {
        fs1.f(list, "raw");
        fs1.f(wCSignType, "type");
        this.raw = list;
        this.type = wCSignType;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ WCEthereumSignMessage copy$default(WCEthereumSignMessage wCEthereumSignMessage, List list, WCSignType wCSignType, int i, Object obj) {
        if ((i & 1) != 0) {
            list = wCEthereumSignMessage.raw;
        }
        if ((i & 2) != 0) {
            wCSignType = wCEthereumSignMessage.type;
        }
        return wCEthereumSignMessage.copy(list, wCSignType);
    }

    public final List<String> component1() {
        return this.raw;
    }

    public final WCSignType component2() {
        return this.type;
    }

    public final WCEthereumSignMessage copy(List<String> list, WCSignType wCSignType) {
        fs1.f(list, "raw");
        fs1.f(wCSignType, "type");
        return new WCEthereumSignMessage(list, wCSignType);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof WCEthereumSignMessage) {
            WCEthereumSignMessage wCEthereumSignMessage = (WCEthereumSignMessage) obj;
            return fs1.b(this.raw, wCEthereumSignMessage.raw) && this.type == wCEthereumSignMessage.type;
        }
        return false;
    }

    public final String getData() {
        int i = WhenMappings.$EnumSwitchMapping$0[this.type.ordinal()];
        if (i != 1) {
            if (i != 2) {
                if (i != 3) {
                    if (i == 4) {
                        return this.raw.get(0);
                    }
                    throw new NoWhenBranchMatchedException();
                }
                return this.raw.get(1);
            }
            return this.raw.get(1);
        }
        return this.raw.get(1);
    }

    public final List<String> getRaw() {
        return this.raw;
    }

    public final WCSignType getType() {
        return this.type;
    }

    public int hashCode() {
        return (this.raw.hashCode() * 31) + this.type.hashCode();
    }

    public String toString() {
        return "WCEthereumSignMessage(raw=" + this.raw + ", type=" + this.type + ')';
    }
}
