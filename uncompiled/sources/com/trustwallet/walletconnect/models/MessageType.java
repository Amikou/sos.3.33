package com.trustwallet.walletconnect.models;

/* compiled from: MessageType.kt */
/* loaded from: classes2.dex */
public enum MessageType {
    PUB,
    SUB
}
