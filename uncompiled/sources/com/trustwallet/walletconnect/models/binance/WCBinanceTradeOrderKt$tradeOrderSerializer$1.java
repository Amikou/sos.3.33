package com.trustwallet.walletconnect.models.binance;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.trustwallet.walletconnect.models.binance.WCBinanceTradeOrder;
import kotlin.jvm.internal.Lambda;

/* compiled from: WCBinanceTradeOrder.kt */
/* loaded from: classes2.dex */
public final class WCBinanceTradeOrderKt$tradeOrderSerializer$1 extends Lambda implements tc1<zl3<WCBinanceTradeOrder.Message>, JsonElement> {
    public static final WCBinanceTradeOrderKt$tradeOrderSerializer$1 INSTANCE = new WCBinanceTradeOrderKt$tradeOrderSerializer$1();

    public WCBinanceTradeOrderKt$tradeOrderSerializer$1() {
        super(1);
    }

    @Override // defpackage.tc1
    public final JsonElement invoke(zl3<WCBinanceTradeOrder.Message> zl3Var) {
        fs1.f(zl3Var, "it");
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty(WCBinanceTradeOrder.MessageKey.ID.getKey(), zl3Var.b().getId());
        jsonObject.addProperty(WCBinanceTradeOrder.MessageKey.ORDER_TYPE.getKey(), Integer.valueOf(zl3Var.b().getOrderType()));
        jsonObject.addProperty(WCBinanceTradeOrder.MessageKey.PRICE.getKey(), Long.valueOf(zl3Var.b().getPrice()));
        jsonObject.addProperty(WCBinanceTradeOrder.MessageKey.QUANTITY.getKey(), Long.valueOf(zl3Var.b().getQuantity()));
        jsonObject.addProperty(WCBinanceTradeOrder.MessageKey.SENDER.getKey(), zl3Var.b().getSender());
        jsonObject.addProperty(WCBinanceTradeOrder.MessageKey.SIDE.getKey(), Integer.valueOf(zl3Var.b().getSide()));
        jsonObject.addProperty(WCBinanceTradeOrder.MessageKey.SYMBOL.getKey(), zl3Var.b().getSymbol());
        jsonObject.addProperty(WCBinanceTradeOrder.MessageKey.TIME_INFORCE.getKey(), Integer.valueOf(zl3Var.b().getTimeInforce()));
        return jsonObject;
    }
}
