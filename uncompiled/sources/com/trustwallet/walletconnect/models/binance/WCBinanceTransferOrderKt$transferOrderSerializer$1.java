package com.trustwallet.walletconnect.models.binance;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.trustwallet.walletconnect.models.binance.WCBinanceTransferOrder;
import kotlin.jvm.internal.Lambda;

/* compiled from: WCBinanceTransferOrder.kt */
/* loaded from: classes2.dex */
public final class WCBinanceTransferOrderKt$transferOrderSerializer$1 extends Lambda implements tc1<zl3<WCBinanceTransferOrder.Message>, JsonElement> {
    public static final WCBinanceTransferOrderKt$transferOrderSerializer$1 INSTANCE = new WCBinanceTransferOrderKt$transferOrderSerializer$1();

    public WCBinanceTransferOrderKt$transferOrderSerializer$1() {
        super(1);
    }

    @Override // defpackage.tc1
    public final JsonElement invoke(zl3<WCBinanceTransferOrder.Message> zl3Var) {
        fs1.f(zl3Var, "it");
        JsonObject jsonObject = new JsonObject();
        ju0.a(jsonObject, WCBinanceTransferOrder.MessageKey.INPUTS.getKey(), zl3Var.a().serialize(zl3Var.b().getInputs()));
        ju0.a(jsonObject, WCBinanceTransferOrder.MessageKey.OUTPUTS.getKey(), zl3Var.a().serialize(zl3Var.b().getOutputs()));
        return jsonObject;
    }
}
