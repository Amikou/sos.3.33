package com.trustwallet.walletconnect.models.binance;

/* compiled from: WCBinanceOrder.kt */
/* loaded from: classes2.dex */
public final class WCBinanceTxConfirmParam {
    private final String errorMsg;
    private final boolean ok;

    public WCBinanceTxConfirmParam(boolean z, String str) {
        this.ok = z;
        this.errorMsg = str;
    }

    public static /* synthetic */ WCBinanceTxConfirmParam copy$default(WCBinanceTxConfirmParam wCBinanceTxConfirmParam, boolean z, String str, int i, Object obj) {
        if ((i & 1) != 0) {
            z = wCBinanceTxConfirmParam.ok;
        }
        if ((i & 2) != 0) {
            str = wCBinanceTxConfirmParam.errorMsg;
        }
        return wCBinanceTxConfirmParam.copy(z, str);
    }

    public final boolean component1() {
        return this.ok;
    }

    public final String component2() {
        return this.errorMsg;
    }

    public final WCBinanceTxConfirmParam copy(boolean z, String str) {
        return new WCBinanceTxConfirmParam(z, str);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof WCBinanceTxConfirmParam) {
            WCBinanceTxConfirmParam wCBinanceTxConfirmParam = (WCBinanceTxConfirmParam) obj;
            return this.ok == wCBinanceTxConfirmParam.ok && fs1.b(this.errorMsg, wCBinanceTxConfirmParam.errorMsg);
        }
        return false;
    }

    public final String getErrorMsg() {
        return this.errorMsg;
    }

    public final boolean getOk() {
        return this.ok;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v1, types: [int] */
    /* JADX WARN: Type inference failed for: r0v4 */
    /* JADX WARN: Type inference failed for: r0v5 */
    public int hashCode() {
        boolean z = this.ok;
        ?? r0 = z;
        if (z) {
            r0 = 1;
        }
        int i = r0 * 31;
        String str = this.errorMsg;
        return i + (str == null ? 0 : str.hashCode());
    }

    public String toString() {
        return "WCBinanceTxConfirmParam(ok=" + this.ok + ", errorMsg=" + ((Object) this.errorMsg) + ')';
    }
}
