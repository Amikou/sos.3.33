package com.trustwallet.walletconnect.models.binance;

import com.trustwallet.walletconnect.models.binance.WCBinanceCancelOrder;
import kotlin.jvm.internal.Lambda;

/* compiled from: WCBinanceCancelOrder.kt */
/* loaded from: classes2.dex */
public final class WCBinanceCancelOrderKt$cancelOrderDeserializer$1 extends Lambda implements tc1<om0, WCBinanceCancelOrder.Message> {
    public static final WCBinanceCancelOrderKt$cancelOrderDeserializer$1 INSTANCE = new WCBinanceCancelOrderKt$cancelOrderDeserializer$1();

    public WCBinanceCancelOrderKt$cancelOrderDeserializer$1() {
        super(1);
    }

    @Override // defpackage.tc1
    public final WCBinanceCancelOrder.Message invoke(om0 om0Var) {
        fs1.f(om0Var, "it");
        return new WCBinanceCancelOrder.Message(ju0.h(ju0.b(om0Var.b(), WCBinanceCancelOrder.MessageKey.REFID.getKey())), ju0.h(ju0.b(om0Var.b(), WCBinanceCancelOrder.MessageKey.SENDER.getKey())), ju0.h(ju0.b(om0Var.b(), WCBinanceCancelOrder.MessageKey.SYMBOL.getKey())));
    }
}
