package com.trustwallet.walletconnect.models.binance;

import com.google.gson.annotations.SerializedName;
import java.util.List;

/* compiled from: WCBinanceOrder.kt */
/* loaded from: classes2.dex */
public class WCBinanceOrder<T> {
    @SerializedName("account_number")
    private final String accountNumber;
    @SerializedName("chain_id")
    private final String chainId;
    private final String data;
    private final String memo;
    private final List<T> msgs;
    private final String sequence;
    private final String source;

    /* JADX WARN: Multi-variable type inference failed */
    public WCBinanceOrder(String str, String str2, String str3, String str4, String str5, String str6, List<? extends T> list) {
        fs1.f(str, "accountNumber");
        fs1.f(str2, "chainId");
        fs1.f(str5, "sequence");
        fs1.f(str6, "source");
        fs1.f(list, "msgs");
        this.accountNumber = str;
        this.chainId = str2;
        this.data = str3;
        this.memo = str4;
        this.sequence = str5;
        this.source = str6;
        this.msgs = list;
    }

    public final String getAccountNumber() {
        return this.accountNumber;
    }

    public final String getChainId() {
        return this.chainId;
    }

    public final String getData() {
        return this.data;
    }

    public final String getMemo() {
        return this.memo;
    }

    public final List<T> getMsgs() {
        return this.msgs;
    }

    public final String getSequence() {
        return this.sequence;
    }

    public final String getSource() {
        return this.source;
    }
}
