package com.trustwallet.walletconnect.models.binance;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.trustwallet.walletconnect.models.binance.WCBinanceCancelOrder;
import kotlin.jvm.internal.Lambda;

/* compiled from: WCBinanceCancelOrder.kt */
/* loaded from: classes2.dex */
public final class WCBinanceCancelOrderKt$cancelOrderSerializer$1 extends Lambda implements tc1<zl3<WCBinanceCancelOrder.Message>, JsonElement> {
    public static final WCBinanceCancelOrderKt$cancelOrderSerializer$1 INSTANCE = new WCBinanceCancelOrderKt$cancelOrderSerializer$1();

    public WCBinanceCancelOrderKt$cancelOrderSerializer$1() {
        super(1);
    }

    @Override // defpackage.tc1
    public final JsonElement invoke(zl3<WCBinanceCancelOrder.Message> zl3Var) {
        fs1.f(zl3Var, "it");
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("refid", zl3Var.b().getRefid());
        jsonObject.addProperty("sender", zl3Var.b().getSender());
        jsonObject.addProperty("symbol", zl3Var.b().getSymbol());
        return jsonObject;
    }
}
