package com.trustwallet.walletconnect.models.binance;

import java.util.List;
import org.web3j.abi.datatypes.Address;

/* compiled from: WCBinanceTransferOrder.kt */
/* loaded from: classes2.dex */
public final class WCBinanceTransferOrder extends WCBinanceOrder<Message> {

    /* compiled from: WCBinanceTransferOrder.kt */
    /* loaded from: classes2.dex */
    public static final class Message {
        private final List<Item> inputs;
        private final List<Item> outputs;

        /* compiled from: WCBinanceTransferOrder.kt */
        /* loaded from: classes2.dex */
        public static final class Item {
            private final String address;
            private final List<Coin> coins;

            /* compiled from: WCBinanceTransferOrder.kt */
            /* loaded from: classes2.dex */
            public static final class Coin {
                private final long amount;
                private final String denom;

                public Coin(long j, String str) {
                    fs1.f(str, "denom");
                    this.amount = j;
                    this.denom = str;
                }

                public static /* synthetic */ Coin copy$default(Coin coin, long j, String str, int i, Object obj) {
                    if ((i & 1) != 0) {
                        j = coin.amount;
                    }
                    if ((i & 2) != 0) {
                        str = coin.denom;
                    }
                    return coin.copy(j, str);
                }

                public final long component1() {
                    return this.amount;
                }

                public final String component2() {
                    return this.denom;
                }

                public final Coin copy(long j, String str) {
                    fs1.f(str, "denom");
                    return new Coin(j, str);
                }

                public boolean equals(Object obj) {
                    if (this == obj) {
                        return true;
                    }
                    if (obj instanceof Coin) {
                        Coin coin = (Coin) obj;
                        return this.amount == coin.amount && fs1.b(this.denom, coin.denom);
                    }
                    return false;
                }

                public final long getAmount() {
                    return this.amount;
                }

                public final String getDenom() {
                    return this.denom;
                }

                public int hashCode() {
                    return (p80.a(this.amount) * 31) + this.denom.hashCode();
                }

                public String toString() {
                    return "Coin(amount=" + this.amount + ", denom=" + this.denom + ')';
                }
            }

            public Item(String str, List<Coin> list) {
                fs1.f(str, Address.TYPE_NAME);
                fs1.f(list, "coins");
                this.address = str;
                this.coins = list;
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ Item copy$default(Item item, String str, List list, int i, Object obj) {
                if ((i & 1) != 0) {
                    str = item.address;
                }
                if ((i & 2) != 0) {
                    list = item.coins;
                }
                return item.copy(str, list);
            }

            public final String component1() {
                return this.address;
            }

            public final List<Coin> component2() {
                return this.coins;
            }

            public final Item copy(String str, List<Coin> list) {
                fs1.f(str, Address.TYPE_NAME);
                fs1.f(list, "coins");
                return new Item(str, list);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (obj instanceof Item) {
                    Item item = (Item) obj;
                    return fs1.b(this.address, item.address) && fs1.b(this.coins, item.coins);
                }
                return false;
            }

            public final String getAddress() {
                return this.address;
            }

            public final List<Coin> getCoins() {
                return this.coins;
            }

            public int hashCode() {
                return (this.address.hashCode() * 31) + this.coins.hashCode();
            }

            public String toString() {
                return "Item(address=" + this.address + ", coins=" + this.coins + ')';
            }
        }

        public Message(List<Item> list, List<Item> list2) {
            fs1.f(list, "inputs");
            fs1.f(list2, "outputs");
            this.inputs = list;
            this.outputs = list2;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ Message copy$default(Message message, List list, List list2, int i, Object obj) {
            if ((i & 1) != 0) {
                list = message.inputs;
            }
            if ((i & 2) != 0) {
                list2 = message.outputs;
            }
            return message.copy(list, list2);
        }

        public final List<Item> component1() {
            return this.inputs;
        }

        public final List<Item> component2() {
            return this.outputs;
        }

        public final Message copy(List<Item> list, List<Item> list2) {
            fs1.f(list, "inputs");
            fs1.f(list2, "outputs");
            return new Message(list, list2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof Message) {
                Message message = (Message) obj;
                return fs1.b(this.inputs, message.inputs) && fs1.b(this.outputs, message.outputs);
            }
            return false;
        }

        public final List<Item> getInputs() {
            return this.inputs;
        }

        public final List<Item> getOutputs() {
            return this.outputs;
        }

        public int hashCode() {
            return (this.inputs.hashCode() * 31) + this.outputs.hashCode();
        }

        public String toString() {
            return "Message(inputs=" + this.inputs + ", outputs=" + this.outputs + ')';
        }
    }

    /* compiled from: WCBinanceTransferOrder.kt */
    /* loaded from: classes2.dex */
    public enum MessageKey {
        INPUTS("inputs"),
        OUTPUTS("outputs");
        
        private final String key;

        MessageKey(String str) {
            this.key = str;
        }

        public final String getKey() {
            return this.key;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WCBinanceTransferOrder(String str, String str2, String str3, String str4, String str5, String str6, List<Message> list) {
        super(str, str2, str3, str4, str5, str6, list);
        fs1.f(str, "account_number");
        fs1.f(str2, "chain_id");
        fs1.f(str5, "sequence");
        fs1.f(str6, "source");
        fs1.f(list, "msgs");
    }
}
