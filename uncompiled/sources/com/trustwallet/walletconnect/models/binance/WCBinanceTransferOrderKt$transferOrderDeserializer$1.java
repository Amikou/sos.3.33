package com.trustwallet.walletconnect.models.binance;

import com.trustwallet.walletconnect.models.binance.WCBinanceTransferOrder;
import kotlin.jvm.internal.Lambda;

/* compiled from: WCBinanceTransferOrder.kt */
/* loaded from: classes2.dex */
public final class WCBinanceTransferOrderKt$transferOrderDeserializer$1 extends Lambda implements tc1<om0, WCBinanceTransferOrder.Message> {
    public static final WCBinanceTransferOrderKt$transferOrderDeserializer$1 INSTANCE = new WCBinanceTransferOrderKt$transferOrderDeserializer$1();

    public WCBinanceTransferOrderKt$transferOrderDeserializer$1() {
        super(1);
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x007f  */
    @Override // defpackage.tc1
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final com.trustwallet.walletconnect.models.binance.WCBinanceTransferOrder.Message invoke(defpackage.om0 r9) {
        /*
            r8 = this;
            java.lang.String r0 = "it"
            defpackage.fs1.f(r9, r0)
            com.trustwallet.walletconnect.models.binance.WCBinanceTransferOrder$Message r0 = new com.trustwallet.walletconnect.models.binance.WCBinanceTransferOrder$Message
            om0$a r1 = r9.a()
            com.google.gson.JsonElement r2 = r9.b()
            com.trustwallet.walletconnect.models.binance.WCBinanceTransferOrder$MessageKey r3 = com.trustwallet.walletconnect.models.binance.WCBinanceTransferOrder.MessageKey.INPUTS
            java.lang.String r3 = r3.getKey()
            com.google.gson.JsonElement r2 = defpackage.ju0.b(r2, r3)
            com.google.gson.JsonArray r2 = defpackage.ju0.c(r2)
            com.google.gson.JsonDeserializationContext r1 = r1.a()
            com.trustwallet.walletconnect.models.binance.WCBinanceTransferOrderKt$transferOrderDeserializer$1$invoke$$inlined$deserialize$1 r3 = new com.trustwallet.walletconnect.models.binance.WCBinanceTransferOrderKt$transferOrderDeserializer$1$invoke$$inlined$deserialize$1
            r3.<init>()
            java.lang.reflect.Type r3 = r3.getType()
            java.lang.String r4 = "object : TypeToken<T>() {} .type"
            defpackage.fs1.c(r3, r4)
            boolean r5 = r3 instanceof java.lang.reflect.ParameterizedType
            java.lang.String r6 = "type.rawType"
            if (r5 == 0) goto L46
            r5 = r3
            java.lang.reflect.ParameterizedType r5 = (java.lang.reflect.ParameterizedType) r5
            boolean r7 = defpackage.xi1.a(r5)
            if (r7 == 0) goto L46
            java.lang.reflect.Type r3 = r5.getRawType()
            defpackage.fs1.c(r3, r6)
            goto L4a
        L46:
            java.lang.reflect.Type r3 = defpackage.xi1.d(r3)
        L4a:
            java.lang.Object r1 = r1.deserialize(r2, r3)
            java.lang.String r2 = "it.context.deserialize(i…ageKey.INPUTS.key].array)"
            defpackage.fs1.e(r1, r2)
            java.util.List r1 = (java.util.List) r1
            om0$a r2 = r9.a()
            com.google.gson.JsonElement r9 = r9.b()
            com.trustwallet.walletconnect.models.binance.WCBinanceTransferOrder$MessageKey r3 = com.trustwallet.walletconnect.models.binance.WCBinanceTransferOrder.MessageKey.OUTPUTS
            java.lang.String r3 = r3.getKey()
            com.google.gson.JsonElement r9 = defpackage.ju0.b(r9, r3)
            com.google.gson.JsonArray r9 = defpackage.ju0.c(r9)
            com.google.gson.JsonDeserializationContext r2 = r2.a()
            com.trustwallet.walletconnect.models.binance.WCBinanceTransferOrderKt$transferOrderDeserializer$1$invoke$$inlined$deserialize$2 r3 = new com.trustwallet.walletconnect.models.binance.WCBinanceTransferOrderKt$transferOrderDeserializer$1$invoke$$inlined$deserialize$2
            r3.<init>()
            java.lang.reflect.Type r3 = r3.getType()
            defpackage.fs1.c(r3, r4)
            boolean r4 = r3 instanceof java.lang.reflect.ParameterizedType
            if (r4 == 0) goto L90
            r4 = r3
            java.lang.reflect.ParameterizedType r4 = (java.lang.reflect.ParameterizedType) r4
            boolean r5 = defpackage.xi1.a(r4)
            if (r5 == 0) goto L90
            java.lang.reflect.Type r3 = r4.getRawType()
            defpackage.fs1.c(r3, r6)
            goto L94
        L90:
            java.lang.reflect.Type r3 = defpackage.xi1.d(r3)
        L94:
            java.lang.Object r9 = r2.deserialize(r9, r3)
            java.lang.String r2 = "it.context.deserialize(i…geKey.OUTPUTS.key].array)"
            defpackage.fs1.e(r9, r2)
            java.util.List r9 = (java.util.List) r9
            r0.<init>(r1, r9)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.trustwallet.walletconnect.models.binance.WCBinanceTransferOrderKt$transferOrderDeserializer$1.invoke(om0):com.trustwallet.walletconnect.models.binance.WCBinanceTransferOrder$Message");
    }
}
