package com.trustwallet.walletconnect.models.binance;

import java.util.List;

/* compiled from: WCBinanceCancelOrder.kt */
/* loaded from: classes2.dex */
public final class WCBinanceCancelOrder extends WCBinanceOrder<Message> {

    /* compiled from: WCBinanceCancelOrder.kt */
    /* loaded from: classes2.dex */
    public static final class Message {
        private final String refid;
        private final String sender;
        private final String symbol;

        public Message(String str, String str2, String str3) {
            fs1.f(str, "refid");
            fs1.f(str2, "sender");
            fs1.f(str3, "symbol");
            this.refid = str;
            this.sender = str2;
            this.symbol = str3;
        }

        public static /* synthetic */ Message copy$default(Message message, String str, String str2, String str3, int i, Object obj) {
            if ((i & 1) != 0) {
                str = message.refid;
            }
            if ((i & 2) != 0) {
                str2 = message.sender;
            }
            if ((i & 4) != 0) {
                str3 = message.symbol;
            }
            return message.copy(str, str2, str3);
        }

        public final String component1() {
            return this.refid;
        }

        public final String component2() {
            return this.sender;
        }

        public final String component3() {
            return this.symbol;
        }

        public final Message copy(String str, String str2, String str3) {
            fs1.f(str, "refid");
            fs1.f(str2, "sender");
            fs1.f(str3, "symbol");
            return new Message(str, str2, str3);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof Message) {
                Message message = (Message) obj;
                return fs1.b(this.refid, message.refid) && fs1.b(this.sender, message.sender) && fs1.b(this.symbol, message.symbol);
            }
            return false;
        }

        public final String getRefid() {
            return this.refid;
        }

        public final String getSender() {
            return this.sender;
        }

        public final String getSymbol() {
            return this.symbol;
        }

        public int hashCode() {
            return (((this.refid.hashCode() * 31) + this.sender.hashCode()) * 31) + this.symbol.hashCode();
        }

        public String toString() {
            return "Message(refid=" + this.refid + ", sender=" + this.sender + ", symbol=" + this.symbol + ')';
        }
    }

    /* compiled from: WCBinanceCancelOrder.kt */
    /* loaded from: classes2.dex */
    public enum MessageKey {
        REFID("refid"),
        SENDER("sender"),
        SYMBOL("symbol");
        
        private final String key;

        MessageKey(String str) {
            this.key = str;
        }

        public final String getKey() {
            return this.key;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WCBinanceCancelOrder(String str, String str2, String str3, String str4, String str5, String str6, List<Message> list) {
        super(str, str2, str3, str4, str5, str6, list);
        fs1.f(str, "account_number");
        fs1.f(str2, "chain_id");
        fs1.f(str5, "sequence");
        fs1.f(str6, "source");
        fs1.f(list, "msgs");
    }
}
