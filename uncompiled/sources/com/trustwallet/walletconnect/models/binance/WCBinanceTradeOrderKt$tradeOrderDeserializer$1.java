package com.trustwallet.walletconnect.models.binance;

import com.trustwallet.walletconnect.models.binance.WCBinanceTradeOrder;
import kotlin.jvm.internal.Lambda;

/* compiled from: WCBinanceTradeOrder.kt */
/* loaded from: classes2.dex */
public final class WCBinanceTradeOrderKt$tradeOrderDeserializer$1 extends Lambda implements tc1<om0, WCBinanceTradeOrder.Message> {
    public static final WCBinanceTradeOrderKt$tradeOrderDeserializer$1 INSTANCE = new WCBinanceTradeOrderKt$tradeOrderDeserializer$1();

    public WCBinanceTradeOrderKt$tradeOrderDeserializer$1() {
        super(1);
    }

    @Override // defpackage.tc1
    public final WCBinanceTradeOrder.Message invoke(om0 om0Var) {
        fs1.f(om0Var, "it");
        return new WCBinanceTradeOrder.Message(ju0.h(ju0.b(om0Var.b(), WCBinanceTradeOrder.MessageKey.ID.getKey())), ju0.d(ju0.b(om0Var.b(), WCBinanceTradeOrder.MessageKey.ORDER_TYPE.getKey())), ju0.e(ju0.b(om0Var.b(), WCBinanceTradeOrder.MessageKey.PRICE.getKey())), ju0.e(ju0.b(om0Var.b(), WCBinanceTradeOrder.MessageKey.QUANTITY.getKey())), ju0.h(ju0.b(om0Var.b(), WCBinanceTradeOrder.MessageKey.SENDER.getKey())), ju0.d(ju0.b(om0Var.b(), WCBinanceTradeOrder.MessageKey.SIDE.getKey())), ju0.h(ju0.b(om0Var.b(), WCBinanceTradeOrder.MessageKey.SYMBOL.getKey())), ju0.d(ju0.b(om0Var.b(), WCBinanceTradeOrder.MessageKey.TIME_INFORCE.getKey())));
    }
}
