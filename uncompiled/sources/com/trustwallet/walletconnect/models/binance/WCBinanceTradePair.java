package com.trustwallet.walletconnect.models.binance;

import java.util.List;
import kotlin.text.StringsKt__StringsKt;

/* compiled from: WCBinanceTradePair.kt */
/* loaded from: classes2.dex */
public final class WCBinanceTradePair {
    public static final Companion Companion = new Companion(null);
    private final String from;
    private final String to;

    /* compiled from: WCBinanceTradePair.kt */
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(qi0 qi0Var) {
            this();
        }

        public final WCBinanceTradePair from(String str) {
            fs1.f(str, "symbol");
            List w0 = StringsKt__StringsKt.w0(str, new String[]{"_"}, false, 0, 6, null);
            if (w0.size() > 1) {
                return new WCBinanceTradePair((String) StringsKt__StringsKt.w0((CharSequence) w0.get(0), new String[]{"-"}, false, 0, 6, null).get(0), (String) StringsKt__StringsKt.w0((CharSequence) w0.get(1), new String[]{"-"}, false, 0, 6, null).get(0));
            }
            return null;
        }
    }

    public WCBinanceTradePair(String str, String str2) {
        fs1.f(str, "from");
        fs1.f(str2, "to");
        this.from = str;
        this.to = str2;
    }

    public static /* synthetic */ WCBinanceTradePair copy$default(WCBinanceTradePair wCBinanceTradePair, String str, String str2, int i, Object obj) {
        if ((i & 1) != 0) {
            str = wCBinanceTradePair.from;
        }
        if ((i & 2) != 0) {
            str2 = wCBinanceTradePair.to;
        }
        return wCBinanceTradePair.copy(str, str2);
    }

    public final String component1() {
        return this.from;
    }

    public final String component2() {
        return this.to;
    }

    public final WCBinanceTradePair copy(String str, String str2) {
        fs1.f(str, "from");
        fs1.f(str2, "to");
        return new WCBinanceTradePair(str, str2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof WCBinanceTradePair) {
            WCBinanceTradePair wCBinanceTradePair = (WCBinanceTradePair) obj;
            return fs1.b(this.from, wCBinanceTradePair.from) && fs1.b(this.to, wCBinanceTradePair.to);
        }
        return false;
    }

    public final String getFrom() {
        return this.from;
    }

    public final String getTo() {
        return this.to;
    }

    public int hashCode() {
        return (this.from.hashCode() * 31) + this.to.hashCode();
    }

    public String toString() {
        return "WCBinanceTradePair(from=" + this.from + ", to=" + this.to + ')';
    }
}
