package com.trustwallet.walletconnect.models.binance;

import java.util.List;

/* compiled from: WCBinanceTradeOrder.kt */
/* loaded from: classes2.dex */
public final class WCBinanceTradeOrder extends WCBinanceOrder<Message> {

    /* compiled from: WCBinanceTradeOrder.kt */
    /* loaded from: classes2.dex */
    public static final class Message {
        private final String id;
        private final int orderType;
        private final long price;
        private final long quantity;
        private final String sender;
        private final int side;
        private final String symbol;
        private final int timeInforce;

        public Message(String str, int i, long j, long j2, String str2, int i2, String str3, int i3) {
            fs1.f(str, "id");
            fs1.f(str2, "sender");
            fs1.f(str3, "symbol");
            this.id = str;
            this.orderType = i;
            this.price = j;
            this.quantity = j2;
            this.sender = str2;
            this.side = i2;
            this.symbol = str3;
            this.timeInforce = i3;
        }

        public final String component1() {
            return this.id;
        }

        public final int component2() {
            return this.orderType;
        }

        public final long component3() {
            return this.price;
        }

        public final long component4() {
            return this.quantity;
        }

        public final String component5() {
            return this.sender;
        }

        public final int component6() {
            return this.side;
        }

        public final String component7() {
            return this.symbol;
        }

        public final int component8() {
            return this.timeInforce;
        }

        public final Message copy(String str, int i, long j, long j2, String str2, int i2, String str3, int i3) {
            fs1.f(str, "id");
            fs1.f(str2, "sender");
            fs1.f(str3, "symbol");
            return new Message(str, i, j, j2, str2, i2, str3, i3);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof Message) {
                Message message = (Message) obj;
                return fs1.b(this.id, message.id) && this.orderType == message.orderType && this.price == message.price && this.quantity == message.quantity && fs1.b(this.sender, message.sender) && this.side == message.side && fs1.b(this.symbol, message.symbol) && this.timeInforce == message.timeInforce;
            }
            return false;
        }

        public final String getId() {
            return this.id;
        }

        public final int getOrderType() {
            return this.orderType;
        }

        public final long getPrice() {
            return this.price;
        }

        public final long getQuantity() {
            return this.quantity;
        }

        public final String getSender() {
            return this.sender;
        }

        public final int getSide() {
            return this.side;
        }

        public final String getSymbol() {
            return this.symbol;
        }

        public final int getTimeInforce() {
            return this.timeInforce;
        }

        public int hashCode() {
            return (((((((((((((this.id.hashCode() * 31) + this.orderType) * 31) + p80.a(this.price)) * 31) + p80.a(this.quantity)) * 31) + this.sender.hashCode()) * 31) + this.side) * 31) + this.symbol.hashCode()) * 31) + this.timeInforce;
        }

        public String toString() {
            return "Message(id=" + this.id + ", orderType=" + this.orderType + ", price=" + this.price + ", quantity=" + this.quantity + ", sender=" + this.sender + ", side=" + this.side + ", symbol=" + this.symbol + ", timeInforce=" + this.timeInforce + ')';
        }
    }

    /* compiled from: WCBinanceTradeOrder.kt */
    /* loaded from: classes2.dex */
    public enum MessageKey {
        ID("id"),
        ORDER_TYPE("ordertype"),
        PRICE("price"),
        QUANTITY("quantity"),
        SENDER("sender"),
        SIDE("side"),
        SYMBOL("symbol"),
        TIME_INFORCE("timeinforce");
        
        private final String key;

        MessageKey(String str) {
            this.key = str;
        }

        public final String getKey() {
            return this.key;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WCBinanceTradeOrder(String str, String str2, String str3, String str4, String str5, String str6, List<Message> list) {
        super(str, str2, str3, str4, str5, str6, list);
        fs1.f(str, "account_number");
        fs1.f(str2, "chain_id");
        fs1.f(str5, "sequence");
        fs1.f(str6, "source");
        fs1.f(list, "msgs");
    }
}
