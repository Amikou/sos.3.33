package com.trustwallet.walletconnect.models;

/* compiled from: WCSignTransaction.kt */
/* loaded from: classes2.dex */
public final class WCSignTransaction {
    private final int network;
    private final String transaction;

    public WCSignTransaction(int i, String str) {
        fs1.f(str, "transaction");
        this.network = i;
        this.transaction = str;
    }

    public static /* synthetic */ WCSignTransaction copy$default(WCSignTransaction wCSignTransaction, int i, String str, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = wCSignTransaction.network;
        }
        if ((i2 & 2) != 0) {
            str = wCSignTransaction.transaction;
        }
        return wCSignTransaction.copy(i, str);
    }

    public final int component1() {
        return this.network;
    }

    public final String component2() {
        return this.transaction;
    }

    public final WCSignTransaction copy(int i, String str) {
        fs1.f(str, "transaction");
        return new WCSignTransaction(i, str);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof WCSignTransaction) {
            WCSignTransaction wCSignTransaction = (WCSignTransaction) obj;
            return this.network == wCSignTransaction.network && fs1.b(this.transaction, wCSignTransaction.transaction);
        }
        return false;
    }

    public final int getNetwork() {
        return this.network;
    }

    public final String getTransaction() {
        return this.transaction;
    }

    public int hashCode() {
        return (this.network * 31) + this.transaction.hashCode();
    }

    public String toString() {
        return "WCSignTransaction(network=" + this.network + ", transaction=" + this.transaction + ')';
    }
}
