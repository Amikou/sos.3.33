package com.trustwallet.walletconnect.models;

import java.util.List;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: WCPeerMeta.kt */
/* loaded from: classes2.dex */
public final class WCPeerMeta {
    private final String description;
    private final List<String> icons;
    private final String name;
    private final String url;

    public WCPeerMeta(String str, String str2, String str3, List<String> list) {
        fs1.f(str, PublicResolver.FUNC_NAME);
        fs1.f(str2, "url");
        fs1.f(list, "icons");
        this.name = str;
        this.url = str2;
        this.description = str3;
        this.icons = list;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ WCPeerMeta copy$default(WCPeerMeta wCPeerMeta, String str, String str2, String str3, List list, int i, Object obj) {
        if ((i & 1) != 0) {
            str = wCPeerMeta.name;
        }
        if ((i & 2) != 0) {
            str2 = wCPeerMeta.url;
        }
        if ((i & 4) != 0) {
            str3 = wCPeerMeta.description;
        }
        if ((i & 8) != 0) {
            list = wCPeerMeta.icons;
        }
        return wCPeerMeta.copy(str, str2, str3, list);
    }

    public final String component1() {
        return this.name;
    }

    public final String component2() {
        return this.url;
    }

    public final String component3() {
        return this.description;
    }

    public final List<String> component4() {
        return this.icons;
    }

    public final WCPeerMeta copy(String str, String str2, String str3, List<String> list) {
        fs1.f(str, PublicResolver.FUNC_NAME);
        fs1.f(str2, "url");
        fs1.f(list, "icons");
        return new WCPeerMeta(str, str2, str3, list);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof WCPeerMeta) {
            WCPeerMeta wCPeerMeta = (WCPeerMeta) obj;
            return fs1.b(this.name, wCPeerMeta.name) && fs1.b(this.url, wCPeerMeta.url) && fs1.b(this.description, wCPeerMeta.description) && fs1.b(this.icons, wCPeerMeta.icons);
        }
        return false;
    }

    public final String getDescription() {
        return this.description;
    }

    public final List<String> getIcons() {
        return this.icons;
    }

    public final String getName() {
        return this.name;
    }

    public final String getUrl() {
        return this.url;
    }

    public int hashCode() {
        int hashCode = ((this.name.hashCode() * 31) + this.url.hashCode()) * 31;
        String str = this.description;
        return ((hashCode + (str == null ? 0 : str.hashCode())) * 31) + this.icons.hashCode();
    }

    public String toString() {
        return "WCPeerMeta(name=" + this.name + ", url=" + this.url + ", description=" + ((Object) this.description) + ", icons=" + this.icons + ')';
    }

    public /* synthetic */ WCPeerMeta(String str, String str2, String str3, List list, int i, qi0 qi0Var) {
        this(str, str2, (i & 4) != 0 ? null : str3, (i & 8) != 0 ? a20.b("") : list);
    }
}
