package com.trustwallet.walletconnect.models;

/* compiled from: WCSocketMessage.kt */
/* loaded from: classes2.dex */
public final class WCSocketMessage {
    private final String payload;
    private final String topic;
    private final MessageType type;

    public WCSocketMessage(String str, MessageType messageType, String str2) {
        fs1.f(str, "topic");
        fs1.f(messageType, "type");
        fs1.f(str2, "payload");
        this.topic = str;
        this.type = messageType;
        this.payload = str2;
    }

    public static /* synthetic */ WCSocketMessage copy$default(WCSocketMessage wCSocketMessage, String str, MessageType messageType, String str2, int i, Object obj) {
        if ((i & 1) != 0) {
            str = wCSocketMessage.topic;
        }
        if ((i & 2) != 0) {
            messageType = wCSocketMessage.type;
        }
        if ((i & 4) != 0) {
            str2 = wCSocketMessage.payload;
        }
        return wCSocketMessage.copy(str, messageType, str2);
    }

    public final String component1() {
        return this.topic;
    }

    public final MessageType component2() {
        return this.type;
    }

    public final String component3() {
        return this.payload;
    }

    public final WCSocketMessage copy(String str, MessageType messageType, String str2) {
        fs1.f(str, "topic");
        fs1.f(messageType, "type");
        fs1.f(str2, "payload");
        return new WCSocketMessage(str, messageType, str2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof WCSocketMessage) {
            WCSocketMessage wCSocketMessage = (WCSocketMessage) obj;
            return fs1.b(this.topic, wCSocketMessage.topic) && this.type == wCSocketMessage.type && fs1.b(this.payload, wCSocketMessage.payload);
        }
        return false;
    }

    public final String getPayload() {
        return this.payload;
    }

    public final String getTopic() {
        return this.topic;
    }

    public final MessageType getType() {
        return this.type;
    }

    public int hashCode() {
        return (((this.topic.hashCode() * 31) + this.type.hashCode()) * 31) + this.payload.hashCode();
    }

    public String toString() {
        return "WCSocketMessage(topic=" + this.topic + ", type=" + this.type + ", payload=" + this.payload + ')';
    }
}
