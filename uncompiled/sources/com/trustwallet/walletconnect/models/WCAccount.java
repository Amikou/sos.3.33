package com.trustwallet.walletconnect.models;

import org.web3j.abi.datatypes.Address;

/* compiled from: WCAccount.kt */
/* loaded from: classes2.dex */
public final class WCAccount {
    private final String address;
    private final int network;

    public WCAccount(int i, String str) {
        fs1.f(str, Address.TYPE_NAME);
        this.network = i;
        this.address = str;
    }

    public static /* synthetic */ WCAccount copy$default(WCAccount wCAccount, int i, String str, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = wCAccount.network;
        }
        if ((i2 & 2) != 0) {
            str = wCAccount.address;
        }
        return wCAccount.copy(i, str);
    }

    public final int component1() {
        return this.network;
    }

    public final String component2() {
        return this.address;
    }

    public final WCAccount copy(int i, String str) {
        fs1.f(str, Address.TYPE_NAME);
        return new WCAccount(i, str);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof WCAccount) {
            WCAccount wCAccount = (WCAccount) obj;
            return this.network == wCAccount.network && fs1.b(this.address, wCAccount.address);
        }
        return false;
    }

    public final String getAddress() {
        return this.address;
    }

    public final int getNetwork() {
        return this.network;
    }

    public int hashCode() {
        return (this.network * 31) + this.address.hashCode();
    }

    public String toString() {
        return "WCAccount(network=" + this.network + ", address=" + this.address + ')';
    }
}
