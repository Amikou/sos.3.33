package com.trustwallet.walletconnect.models;

/* compiled from: WCEncryptionPayload.kt */
/* loaded from: classes2.dex */
public final class WCEncryptionPayload {
    private final String data;
    private final String hmac;
    private final String iv;

    public WCEncryptionPayload(String str, String str2, String str3) {
        fs1.f(str, "data");
        fs1.f(str2, "hmac");
        fs1.f(str3, "iv");
        this.data = str;
        this.hmac = str2;
        this.iv = str3;
    }

    public static /* synthetic */ WCEncryptionPayload copy$default(WCEncryptionPayload wCEncryptionPayload, String str, String str2, String str3, int i, Object obj) {
        if ((i & 1) != 0) {
            str = wCEncryptionPayload.data;
        }
        if ((i & 2) != 0) {
            str2 = wCEncryptionPayload.hmac;
        }
        if ((i & 4) != 0) {
            str3 = wCEncryptionPayload.iv;
        }
        return wCEncryptionPayload.copy(str, str2, str3);
    }

    public final String component1() {
        return this.data;
    }

    public final String component2() {
        return this.hmac;
    }

    public final String component3() {
        return this.iv;
    }

    public final WCEncryptionPayload copy(String str, String str2, String str3) {
        fs1.f(str, "data");
        fs1.f(str2, "hmac");
        fs1.f(str3, "iv");
        return new WCEncryptionPayload(str, str2, str3);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof WCEncryptionPayload) {
            WCEncryptionPayload wCEncryptionPayload = (WCEncryptionPayload) obj;
            return fs1.b(this.data, wCEncryptionPayload.data) && fs1.b(this.hmac, wCEncryptionPayload.hmac) && fs1.b(this.iv, wCEncryptionPayload.iv);
        }
        return false;
    }

    public final String getData() {
        return this.data;
    }

    public final String getHmac() {
        return this.hmac;
    }

    public final String getIv() {
        return this.iv;
    }

    public int hashCode() {
        return (((this.data.hashCode() * 31) + this.hmac.hashCode()) * 31) + this.iv.hashCode();
    }

    public String toString() {
        return "WCEncryptionPayload(data=" + this.data + ", hmac=" + this.hmac + ", iv=" + this.iv + ')';
    }
}
