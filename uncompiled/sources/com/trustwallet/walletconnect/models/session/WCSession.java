package com.trustwallet.walletconnect.models.session;

import android.net.Uri;

/* compiled from: WCSession.kt */
/* loaded from: classes2.dex */
public final class WCSession {
    public static final Companion Companion = new Companion(null);
    private final String bridge;
    private final String key;
    private final String topic;
    private final String version;

    /* compiled from: WCSession.kt */
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(qi0 qi0Var) {
            this();
        }

        public final WCSession from(String str) {
            fs1.f(str, "from");
            if (dv3.H(str, "wc:", false, 2, null)) {
                Uri parse = Uri.parse(dv3.D(str, "wc:", "wc://", false, 4, null));
                String queryParameter = parse.getQueryParameter("bridge");
                String queryParameter2 = parse.getQueryParameter("key");
                String userInfo = parse.getUserInfo();
                String host = parse.getHost();
                if (queryParameter == null || queryParameter2 == null || userInfo == null || host == null) {
                    return null;
                }
                return new WCSession(userInfo, host, queryParameter, queryParameter2);
            }
            return null;
        }
    }

    public WCSession(String str, String str2, String str3, String str4) {
        fs1.f(str, "topic");
        fs1.f(str2, "version");
        fs1.f(str3, "bridge");
        fs1.f(str4, "key");
        this.topic = str;
        this.version = str2;
        this.bridge = str3;
        this.key = str4;
    }

    public static /* synthetic */ WCSession copy$default(WCSession wCSession, String str, String str2, String str3, String str4, int i, Object obj) {
        if ((i & 1) != 0) {
            str = wCSession.topic;
        }
        if ((i & 2) != 0) {
            str2 = wCSession.version;
        }
        if ((i & 4) != 0) {
            str3 = wCSession.bridge;
        }
        if ((i & 8) != 0) {
            str4 = wCSession.key;
        }
        return wCSession.copy(str, str2, str3, str4);
    }

    public final String component1() {
        return this.topic;
    }

    public final String component2() {
        return this.version;
    }

    public final String component3() {
        return this.bridge;
    }

    public final String component4() {
        return this.key;
    }

    public final WCSession copy(String str, String str2, String str3, String str4) {
        fs1.f(str, "topic");
        fs1.f(str2, "version");
        fs1.f(str3, "bridge");
        fs1.f(str4, "key");
        return new WCSession(str, str2, str3, str4);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof WCSession) {
            WCSession wCSession = (WCSession) obj;
            return fs1.b(this.topic, wCSession.topic) && fs1.b(this.version, wCSession.version) && fs1.b(this.bridge, wCSession.bridge) && fs1.b(this.key, wCSession.key);
        }
        return false;
    }

    public final String getBridge() {
        return this.bridge;
    }

    public final String getKey() {
        return this.key;
    }

    public final String getTopic() {
        return this.topic;
    }

    public final String getVersion() {
        return this.version;
    }

    public int hashCode() {
        return (((((this.topic.hashCode() * 31) + this.version.hashCode()) * 31) + this.bridge.hashCode()) * 31) + this.key.hashCode();
    }

    public String toString() {
        return "WCSession(topic=" + this.topic + ", version=" + this.version + ", bridge=" + this.bridge + ", key=" + this.key + ')';
    }

    public final String toUri() {
        return "wc:" + this.topic + '@' + this.version + "?bridge=" + this.bridge + "&key=" + this.key;
    }
}
