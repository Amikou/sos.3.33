package com.trustwallet.walletconnect.models.session;

import com.trustwallet.walletconnect.models.WCPeerMeta;
import java.util.List;

/* compiled from: WCApproveSessionResponse.kt */
/* loaded from: classes2.dex */
public final class WCApproveSessionResponse {
    private final List<String> accounts;
    private final boolean approved;
    private final int chainId;
    private final String peerId;
    private final WCPeerMeta peerMeta;

    public WCApproveSessionResponse(boolean z, int i, List<String> list, String str, WCPeerMeta wCPeerMeta) {
        fs1.f(list, "accounts");
        this.approved = z;
        this.chainId = i;
        this.accounts = list;
        this.peerId = str;
        this.peerMeta = wCPeerMeta;
    }

    public static /* synthetic */ WCApproveSessionResponse copy$default(WCApproveSessionResponse wCApproveSessionResponse, boolean z, int i, List list, String str, WCPeerMeta wCPeerMeta, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            z = wCApproveSessionResponse.approved;
        }
        if ((i2 & 2) != 0) {
            i = wCApproveSessionResponse.chainId;
        }
        int i3 = i;
        List<String> list2 = list;
        if ((i2 & 4) != 0) {
            list2 = wCApproveSessionResponse.accounts;
        }
        List list3 = list2;
        if ((i2 & 8) != 0) {
            str = wCApproveSessionResponse.peerId;
        }
        String str2 = str;
        if ((i2 & 16) != 0) {
            wCPeerMeta = wCApproveSessionResponse.peerMeta;
        }
        return wCApproveSessionResponse.copy(z, i3, list3, str2, wCPeerMeta);
    }

    public final boolean component1() {
        return this.approved;
    }

    public final int component2() {
        return this.chainId;
    }

    public final List<String> component3() {
        return this.accounts;
    }

    public final String component4() {
        return this.peerId;
    }

    public final WCPeerMeta component5() {
        return this.peerMeta;
    }

    public final WCApproveSessionResponse copy(boolean z, int i, List<String> list, String str, WCPeerMeta wCPeerMeta) {
        fs1.f(list, "accounts");
        return new WCApproveSessionResponse(z, i, list, str, wCPeerMeta);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof WCApproveSessionResponse) {
            WCApproveSessionResponse wCApproveSessionResponse = (WCApproveSessionResponse) obj;
            return this.approved == wCApproveSessionResponse.approved && this.chainId == wCApproveSessionResponse.chainId && fs1.b(this.accounts, wCApproveSessionResponse.accounts) && fs1.b(this.peerId, wCApproveSessionResponse.peerId) && fs1.b(this.peerMeta, wCApproveSessionResponse.peerMeta);
        }
        return false;
    }

    public final List<String> getAccounts() {
        return this.accounts;
    }

    public final boolean getApproved() {
        return this.approved;
    }

    public final int getChainId() {
        return this.chainId;
    }

    public final String getPeerId() {
        return this.peerId;
    }

    public final WCPeerMeta getPeerMeta() {
        return this.peerMeta;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v1, types: [int] */
    /* JADX WARN: Type inference failed for: r0v10 */
    /* JADX WARN: Type inference failed for: r0v11 */
    public int hashCode() {
        boolean z = this.approved;
        ?? r0 = z;
        if (z) {
            r0 = 1;
        }
        int hashCode = ((((r0 * 31) + this.chainId) * 31) + this.accounts.hashCode()) * 31;
        String str = this.peerId;
        int hashCode2 = (hashCode + (str == null ? 0 : str.hashCode())) * 31;
        WCPeerMeta wCPeerMeta = this.peerMeta;
        return hashCode2 + (wCPeerMeta != null ? wCPeerMeta.hashCode() : 0);
    }

    public String toString() {
        return "WCApproveSessionResponse(approved=" + this.approved + ", chainId=" + this.chainId + ", accounts=" + this.accounts + ", peerId=" + ((Object) this.peerId) + ", peerMeta=" + this.peerMeta + ')';
    }

    public /* synthetic */ WCApproveSessionResponse(boolean z, int i, List list, String str, WCPeerMeta wCPeerMeta, int i2, qi0 qi0Var) {
        this((i2 & 1) != 0 ? true : z, i, list, str, wCPeerMeta);
    }
}
