package com.trustwallet.walletconnect.models.session;

import com.trustwallet.walletconnect.models.WCPeerMeta;

/* compiled from: WCSessionRequest.kt */
/* loaded from: classes2.dex */
public final class WCSessionRequest {
    private final String chainId;
    private final String peerId;
    private final WCPeerMeta peerMeta;

    public WCSessionRequest(String str, WCPeerMeta wCPeerMeta, String str2) {
        fs1.f(str, "peerId");
        fs1.f(wCPeerMeta, "peerMeta");
        this.peerId = str;
        this.peerMeta = wCPeerMeta;
        this.chainId = str2;
    }

    public static /* synthetic */ WCSessionRequest copy$default(WCSessionRequest wCSessionRequest, String str, WCPeerMeta wCPeerMeta, String str2, int i, Object obj) {
        if ((i & 1) != 0) {
            str = wCSessionRequest.peerId;
        }
        if ((i & 2) != 0) {
            wCPeerMeta = wCSessionRequest.peerMeta;
        }
        if ((i & 4) != 0) {
            str2 = wCSessionRequest.chainId;
        }
        return wCSessionRequest.copy(str, wCPeerMeta, str2);
    }

    public final String component1() {
        return this.peerId;
    }

    public final WCPeerMeta component2() {
        return this.peerMeta;
    }

    public final String component3() {
        return this.chainId;
    }

    public final WCSessionRequest copy(String str, WCPeerMeta wCPeerMeta, String str2) {
        fs1.f(str, "peerId");
        fs1.f(wCPeerMeta, "peerMeta");
        return new WCSessionRequest(str, wCPeerMeta, str2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof WCSessionRequest) {
            WCSessionRequest wCSessionRequest = (WCSessionRequest) obj;
            return fs1.b(this.peerId, wCSessionRequest.peerId) && fs1.b(this.peerMeta, wCSessionRequest.peerMeta) && fs1.b(this.chainId, wCSessionRequest.chainId);
        }
        return false;
    }

    public final String getChainId() {
        return this.chainId;
    }

    public final String getPeerId() {
        return this.peerId;
    }

    public final WCPeerMeta getPeerMeta() {
        return this.peerMeta;
    }

    public int hashCode() {
        int hashCode = ((this.peerId.hashCode() * 31) + this.peerMeta.hashCode()) * 31;
        String str = this.chainId;
        return hashCode + (str == null ? 0 : str.hashCode());
    }

    public String toString() {
        return "WCSessionRequest(peerId=" + this.peerId + ", peerMeta=" + this.peerMeta + ", chainId=" + ((Object) this.chainId) + ')';
    }
}
