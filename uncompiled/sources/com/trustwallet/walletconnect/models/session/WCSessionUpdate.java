package com.trustwallet.walletconnect.models.session;

import java.util.List;

/* compiled from: WCSessionUpdate.kt */
/* loaded from: classes2.dex */
public final class WCSessionUpdate {
    private final List<String> accounts;
    private final boolean approved;
    private final Integer chainId;

    public WCSessionUpdate(boolean z, Integer num, List<String> list) {
        this.approved = z;
        this.chainId = num;
        this.accounts = list;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ WCSessionUpdate copy$default(WCSessionUpdate wCSessionUpdate, boolean z, Integer num, List list, int i, Object obj) {
        if ((i & 1) != 0) {
            z = wCSessionUpdate.approved;
        }
        if ((i & 2) != 0) {
            num = wCSessionUpdate.chainId;
        }
        if ((i & 4) != 0) {
            list = wCSessionUpdate.accounts;
        }
        return wCSessionUpdate.copy(z, num, list);
    }

    public final boolean component1() {
        return this.approved;
    }

    public final Integer component2() {
        return this.chainId;
    }

    public final List<String> component3() {
        return this.accounts;
    }

    public final WCSessionUpdate copy(boolean z, Integer num, List<String> list) {
        return new WCSessionUpdate(z, num, list);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof WCSessionUpdate) {
            WCSessionUpdate wCSessionUpdate = (WCSessionUpdate) obj;
            return this.approved == wCSessionUpdate.approved && fs1.b(this.chainId, wCSessionUpdate.chainId) && fs1.b(this.accounts, wCSessionUpdate.accounts);
        }
        return false;
    }

    public final List<String> getAccounts() {
        return this.accounts;
    }

    public final boolean getApproved() {
        return this.approved;
    }

    public final Integer getChainId() {
        return this.chainId;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v1, types: [int] */
    /* JADX WARN: Type inference failed for: r0v6 */
    /* JADX WARN: Type inference failed for: r0v7 */
    public int hashCode() {
        boolean z = this.approved;
        ?? r0 = z;
        if (z) {
            r0 = 1;
        }
        int i = r0 * 31;
        Integer num = this.chainId;
        int hashCode = (i + (num == null ? 0 : num.hashCode())) * 31;
        List<String> list = this.accounts;
        return hashCode + (list != null ? list.hashCode() : 0);
    }

    public String toString() {
        return "WCSessionUpdate(approved=" + this.approved + ", chainId=" + this.chainId + ", accounts=" + this.accounts + ')';
    }
}
