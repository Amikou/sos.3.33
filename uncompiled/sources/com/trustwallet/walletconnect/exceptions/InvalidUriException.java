package com.trustwallet.walletconnect.exceptions;

/* compiled from: InvalidUriException.kt */
/* loaded from: classes2.dex */
public final class InvalidUriException extends Exception {
    public InvalidUriException() {
        super("Invalid Wallet Connect URI");
    }
}
