package com.trustwallet.walletconnect.exceptions;

/* compiled from: InvalidPayloadException.kt */
/* loaded from: classes2.dex */
public final class InvalidPayloadException extends Exception {
    public InvalidPayloadException() {
        super("Invalid WCEncryptionPayload");
    }
}
