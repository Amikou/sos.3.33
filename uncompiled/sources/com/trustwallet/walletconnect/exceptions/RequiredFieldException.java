package com.trustwallet.walletconnect.exceptions;

import com.google.gson.JsonParseException;

/* compiled from: RequiredFieldException.kt */
/* loaded from: classes2.dex */
public final class RequiredFieldException extends JsonParseException {
    private final String field;

    public RequiredFieldException() {
        this(null, 1, null);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public RequiredFieldException(String str) {
        super('\'' + str + "' is required");
        fs1.f(str, "field");
        this.field = str;
    }

    public final String getField() {
        return this.field;
    }

    public /* synthetic */ RequiredFieldException(String str, int i, qi0 qi0Var) {
        this((i & 1) != 0 ? "" : str);
    }
}
