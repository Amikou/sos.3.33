package com.trustwallet.walletconnect.exceptions;

/* compiled from: InvalidMessageException.kt */
/* loaded from: classes2.dex */
public final class InvalidMessageException extends Exception {
    public InvalidMessageException() {
        super("Invalid message");
    }
}
