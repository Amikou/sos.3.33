package com.trustwallet.walletconnect;

import com.trustwallet.walletconnect.models.ethereum.WCEthereumTransaction;
import kotlin.jvm.internal.Lambda;

/* compiled from: WCClient.kt */
/* loaded from: classes2.dex */
public final class WCClient$onEthSignTransaction$1 extends Lambda implements hd1<Long, WCEthereumTransaction, te4> {
    public static final WCClient$onEthSignTransaction$1 INSTANCE = new WCClient$onEthSignTransaction$1();

    public WCClient$onEthSignTransaction$1() {
        super(2);
    }

    @Override // defpackage.hd1
    public /* bridge */ /* synthetic */ te4 invoke(Long l, WCEthereumTransaction wCEthereumTransaction) {
        invoke(l.longValue(), wCEthereumTransaction);
        return te4.a;
    }

    public final void invoke(long j, WCEthereumTransaction wCEthereumTransaction) {
        fs1.f(wCEthereumTransaction, "$noName_1");
    }
}
