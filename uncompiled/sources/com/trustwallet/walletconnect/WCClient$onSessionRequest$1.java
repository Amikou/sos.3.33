package com.trustwallet.walletconnect;

import com.trustwallet.walletconnect.models.WCPeerMeta;
import kotlin.jvm.internal.Lambda;

/* compiled from: WCClient.kt */
/* loaded from: classes2.dex */
public final class WCClient$onSessionRequest$1 extends Lambda implements hd1<Long, WCPeerMeta, te4> {
    public static final WCClient$onSessionRequest$1 INSTANCE = new WCClient$onSessionRequest$1();

    public WCClient$onSessionRequest$1() {
        super(2);
    }

    @Override // defpackage.hd1
    public /* bridge */ /* synthetic */ te4 invoke(Long l, WCPeerMeta wCPeerMeta) {
        invoke(l.longValue(), wCPeerMeta);
        return te4.a;
    }

    public final void invoke(long j, WCPeerMeta wCPeerMeta) {
        fs1.f(wCPeerMeta, "$noName_1");
    }
}
