package com.trustwallet.walletconnect;

import com.trustwallet.walletconnect.models.binance.WCBinanceCancelOrder;
import kotlin.jvm.internal.Lambda;

/* compiled from: WCClient.kt */
/* loaded from: classes2.dex */
public final class WCClient$onBnbCancel$1 extends Lambda implements hd1<Long, WCBinanceCancelOrder, te4> {
    public static final WCClient$onBnbCancel$1 INSTANCE = new WCClient$onBnbCancel$1();

    public WCClient$onBnbCancel$1() {
        super(2);
    }

    @Override // defpackage.hd1
    public /* bridge */ /* synthetic */ te4 invoke(Long l, WCBinanceCancelOrder wCBinanceCancelOrder) {
        invoke(l.longValue(), wCBinanceCancelOrder);
        return te4.a;
    }

    public final void invoke(long j, WCBinanceCancelOrder wCBinanceCancelOrder) {
        fs1.f(wCBinanceCancelOrder, "$noName_1");
    }
}
