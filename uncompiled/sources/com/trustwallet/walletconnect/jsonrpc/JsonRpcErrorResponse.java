package com.trustwallet.walletconnect.jsonrpc;

import com.trustwallet.walletconnect.WCClientKt;

/* compiled from: JsonRpcResponse.kt */
/* loaded from: classes2.dex */
public final class JsonRpcErrorResponse {
    private final JsonRpcError error;
    private final long id;
    private final String jsonrpc;

    public JsonRpcErrorResponse(String str, long j, JsonRpcError jsonRpcError) {
        fs1.f(str, "jsonrpc");
        fs1.f(jsonRpcError, "error");
        this.jsonrpc = str;
        this.id = j;
        this.error = jsonRpcError;
    }

    public static /* synthetic */ JsonRpcErrorResponse copy$default(JsonRpcErrorResponse jsonRpcErrorResponse, String str, long j, JsonRpcError jsonRpcError, int i, Object obj) {
        if ((i & 1) != 0) {
            str = jsonRpcErrorResponse.jsonrpc;
        }
        if ((i & 2) != 0) {
            j = jsonRpcErrorResponse.id;
        }
        if ((i & 4) != 0) {
            jsonRpcError = jsonRpcErrorResponse.error;
        }
        return jsonRpcErrorResponse.copy(str, j, jsonRpcError);
    }

    public final String component1() {
        return this.jsonrpc;
    }

    public final long component2() {
        return this.id;
    }

    public final JsonRpcError component3() {
        return this.error;
    }

    public final JsonRpcErrorResponse copy(String str, long j, JsonRpcError jsonRpcError) {
        fs1.f(str, "jsonrpc");
        fs1.f(jsonRpcError, "error");
        return new JsonRpcErrorResponse(str, j, jsonRpcError);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof JsonRpcErrorResponse) {
            JsonRpcErrorResponse jsonRpcErrorResponse = (JsonRpcErrorResponse) obj;
            return fs1.b(this.jsonrpc, jsonRpcErrorResponse.jsonrpc) && this.id == jsonRpcErrorResponse.id && fs1.b(this.error, jsonRpcErrorResponse.error);
        }
        return false;
    }

    public final JsonRpcError getError() {
        return this.error;
    }

    public final long getId() {
        return this.id;
    }

    public final String getJsonrpc() {
        return this.jsonrpc;
    }

    public int hashCode() {
        return (((this.jsonrpc.hashCode() * 31) + p80.a(this.id)) * 31) + this.error.hashCode();
    }

    public String toString() {
        return "JsonRpcErrorResponse(jsonrpc=" + this.jsonrpc + ", id=" + this.id + ", error=" + this.error + ')';
    }

    public /* synthetic */ JsonRpcErrorResponse(String str, long j, JsonRpcError jsonRpcError, int i, qi0 qi0Var) {
        this((i & 1) != 0 ? WCClientKt.JSONRPC_VERSION : str, j, jsonRpcError);
    }
}
