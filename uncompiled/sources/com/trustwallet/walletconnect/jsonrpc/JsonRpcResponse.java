package com.trustwallet.walletconnect.jsonrpc;

import com.trustwallet.walletconnect.WCClientKt;

/* compiled from: JsonRpcResponse.kt */
/* loaded from: classes2.dex */
public final class JsonRpcResponse<T> {
    private final long id;
    private final String jsonrpc;
    private final T result;

    public JsonRpcResponse(String str, long j, T t) {
        fs1.f(str, "jsonrpc");
        this.jsonrpc = str;
        this.id = j;
        this.result = t;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ JsonRpcResponse copy$default(JsonRpcResponse jsonRpcResponse, String str, long j, Object obj, int i, Object obj2) {
        if ((i & 1) != 0) {
            str = jsonRpcResponse.jsonrpc;
        }
        if ((i & 2) != 0) {
            j = jsonRpcResponse.id;
        }
        if ((i & 4) != 0) {
            obj = jsonRpcResponse.result;
        }
        return jsonRpcResponse.copy(str, j, obj);
    }

    public final String component1() {
        return this.jsonrpc;
    }

    public final long component2() {
        return this.id;
    }

    public final T component3() {
        return this.result;
    }

    public final JsonRpcResponse<T> copy(String str, long j, T t) {
        fs1.f(str, "jsonrpc");
        return new JsonRpcResponse<>(str, j, t);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof JsonRpcResponse) {
            JsonRpcResponse jsonRpcResponse = (JsonRpcResponse) obj;
            return fs1.b(this.jsonrpc, jsonRpcResponse.jsonrpc) && this.id == jsonRpcResponse.id && fs1.b(this.result, jsonRpcResponse.result);
        }
        return false;
    }

    public final long getId() {
        return this.id;
    }

    public final String getJsonrpc() {
        return this.jsonrpc;
    }

    public final T getResult() {
        return this.result;
    }

    public int hashCode() {
        int hashCode = ((this.jsonrpc.hashCode() * 31) + p80.a(this.id)) * 31;
        T t = this.result;
        return hashCode + (t == null ? 0 : t.hashCode());
    }

    public String toString() {
        return "JsonRpcResponse(jsonrpc=" + this.jsonrpc + ", id=" + this.id + ", result=" + this.result + ')';
    }

    public /* synthetic */ JsonRpcResponse(String str, long j, Object obj, int i, qi0 qi0Var) {
        this((i & 1) != 0 ? WCClientKt.JSONRPC_VERSION : str, j, obj);
    }
}
