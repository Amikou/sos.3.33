package com.trustwallet.walletconnect.jsonrpc;

import com.trustwallet.walletconnect.WCClientKt;
import com.trustwallet.walletconnect.models.WCMethod;

/* compiled from: JsonRpcRequest.kt */
/* loaded from: classes2.dex */
public final class JsonRpcRequest<T> {
    private final long id;
    private final String jsonrpc;
    private final WCMethod method;
    private final T params;

    public JsonRpcRequest(long j, String str, WCMethod wCMethod, T t) {
        fs1.f(str, "jsonrpc");
        this.id = j;
        this.jsonrpc = str;
        this.method = wCMethod;
        this.params = t;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ JsonRpcRequest copy$default(JsonRpcRequest jsonRpcRequest, long j, String str, WCMethod wCMethod, Object obj, int i, Object obj2) {
        if ((i & 1) != 0) {
            j = jsonRpcRequest.id;
        }
        long j2 = j;
        if ((i & 2) != 0) {
            str = jsonRpcRequest.jsonrpc;
        }
        String str2 = str;
        if ((i & 4) != 0) {
            wCMethod = jsonRpcRequest.method;
        }
        WCMethod wCMethod2 = wCMethod;
        T t = obj;
        if ((i & 8) != 0) {
            t = jsonRpcRequest.params;
        }
        return jsonRpcRequest.copy(j2, str2, wCMethod2, t);
    }

    public final long component1() {
        return this.id;
    }

    public final String component2() {
        return this.jsonrpc;
    }

    public final WCMethod component3() {
        return this.method;
    }

    public final T component4() {
        return this.params;
    }

    public final JsonRpcRequest<T> copy(long j, String str, WCMethod wCMethod, T t) {
        fs1.f(str, "jsonrpc");
        return new JsonRpcRequest<>(j, str, wCMethod, t);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof JsonRpcRequest) {
            JsonRpcRequest jsonRpcRequest = (JsonRpcRequest) obj;
            return this.id == jsonRpcRequest.id && fs1.b(this.jsonrpc, jsonRpcRequest.jsonrpc) && this.method == jsonRpcRequest.method && fs1.b(this.params, jsonRpcRequest.params);
        }
        return false;
    }

    public final long getId() {
        return this.id;
    }

    public final String getJsonrpc() {
        return this.jsonrpc;
    }

    public final WCMethod getMethod() {
        return this.method;
    }

    public final T getParams() {
        return this.params;
    }

    public int hashCode() {
        int a = ((p80.a(this.id) * 31) + this.jsonrpc.hashCode()) * 31;
        WCMethod wCMethod = this.method;
        int hashCode = (a + (wCMethod == null ? 0 : wCMethod.hashCode())) * 31;
        T t = this.params;
        return hashCode + (t != null ? t.hashCode() : 0);
    }

    public String toString() {
        return "JsonRpcRequest(id=" + this.id + ", jsonrpc=" + this.jsonrpc + ", method=" + this.method + ", params=" + this.params + ')';
    }

    public /* synthetic */ JsonRpcRequest(long j, String str, WCMethod wCMethod, Object obj, int i, qi0 qi0Var) {
        this(j, (i & 2) != 0 ? WCClientKt.JSONRPC_VERSION : str, wCMethod, obj);
    }
}
