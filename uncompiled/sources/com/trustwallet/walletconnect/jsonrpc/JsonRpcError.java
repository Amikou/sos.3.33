package com.trustwallet.walletconnect.jsonrpc;

import com.fasterxml.jackson.databind.deser.std.ThrowableDeserializer;

/* compiled from: JsonRpcError.kt */
/* loaded from: classes2.dex */
public final class JsonRpcError {
    public static final Companion Companion = new Companion(null);
    private final int code;
    private final String message;

    /* compiled from: JsonRpcError.kt */
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(qi0 qi0Var) {
            this();
        }

        public final JsonRpcError invalidParams(String str) {
            fs1.f(str, ThrowableDeserializer.PROP_NAME_MESSAGE);
            return new JsonRpcError(-32602, str);
        }

        public final JsonRpcError invalidRequest(String str) {
            fs1.f(str, ThrowableDeserializer.PROP_NAME_MESSAGE);
            return new JsonRpcError(-32600, str);
        }

        public final JsonRpcError methodNotFound(String str) {
            fs1.f(str, ThrowableDeserializer.PROP_NAME_MESSAGE);
            return new JsonRpcError(-32601, str);
        }

        public final JsonRpcError parseError(String str) {
            fs1.f(str, ThrowableDeserializer.PROP_NAME_MESSAGE);
            return new JsonRpcError(-32700, str);
        }

        public final JsonRpcError serverError(String str) {
            fs1.f(str, ThrowableDeserializer.PROP_NAME_MESSAGE);
            return new JsonRpcError(-32000, str);
        }
    }

    public JsonRpcError(int i, String str) {
        fs1.f(str, ThrowableDeserializer.PROP_NAME_MESSAGE);
        this.code = i;
        this.message = str;
    }

    public static /* synthetic */ JsonRpcError copy$default(JsonRpcError jsonRpcError, int i, String str, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = jsonRpcError.code;
        }
        if ((i2 & 2) != 0) {
            str = jsonRpcError.message;
        }
        return jsonRpcError.copy(i, str);
    }

    public final int component1() {
        return this.code;
    }

    public final String component2() {
        return this.message;
    }

    public final JsonRpcError copy(int i, String str) {
        fs1.f(str, ThrowableDeserializer.PROP_NAME_MESSAGE);
        return new JsonRpcError(i, str);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof JsonRpcError) {
            JsonRpcError jsonRpcError = (JsonRpcError) obj;
            return this.code == jsonRpcError.code && fs1.b(this.message, jsonRpcError.message);
        }
        return false;
    }

    public final int getCode() {
        return this.code;
    }

    public final String getMessage() {
        return this.message;
    }

    public int hashCode() {
        return (this.code * 31) + this.message.hashCode();
    }

    public String toString() {
        return "JsonRpcError(code=" + this.code + ", message=" + this.message + ')';
    }
}
