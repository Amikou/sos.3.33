package com.trustwallet.walletconnect;

import kotlin.jvm.internal.Lambda;

/* compiled from: WCClient.kt */
/* loaded from: classes2.dex */
public final class WCClient$onFailure$1 extends Lambda implements tc1<Throwable, te4> {
    public static final WCClient$onFailure$1 INSTANCE = new WCClient$onFailure$1();

    public WCClient$onFailure$1() {
        super(1);
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(Throwable th) {
        invoke2(th);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Throwable th) {
        fs1.f(th, "$noName_0");
    }
}
