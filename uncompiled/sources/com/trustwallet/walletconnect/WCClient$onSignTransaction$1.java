package com.trustwallet.walletconnect;

import com.trustwallet.walletconnect.models.WCSignTransaction;
import kotlin.jvm.internal.Lambda;

/* compiled from: WCClient.kt */
/* loaded from: classes2.dex */
public final class WCClient$onSignTransaction$1 extends Lambda implements hd1<Long, WCSignTransaction, te4> {
    public static final WCClient$onSignTransaction$1 INSTANCE = new WCClient$onSignTransaction$1();

    public WCClient$onSignTransaction$1() {
        super(2);
    }

    @Override // defpackage.hd1
    public /* bridge */ /* synthetic */ te4 invoke(Long l, WCSignTransaction wCSignTransaction) {
        invoke(l.longValue(), wCSignTransaction);
        return te4.a;
    }

    public final void invoke(long j, WCSignTransaction wCSignTransaction) {
        fs1.f(wCSignTransaction, "$noName_1");
    }
}
