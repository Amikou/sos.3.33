package com.trustwallet.walletconnect;

import com.trustwallet.walletconnect.models.WCPeerMeta;
import com.trustwallet.walletconnect.models.session.WCSession;
import java.util.Date;

/* compiled from: WCSessionStore.kt */
/* loaded from: classes2.dex */
public final class WCSessionStoreItem {
    private final int chainId;
    private final Date date;
    private final boolean isAutoSign;
    private final String peerId;
    private final String remotePeerId;
    private final WCPeerMeta remotePeerMeta;
    private final WCSession session;

    public WCSessionStoreItem(WCSession wCSession, int i, String str, String str2, WCPeerMeta wCPeerMeta, boolean z, Date date) {
        fs1.f(wCSession, "session");
        fs1.f(str, "peerId");
        fs1.f(str2, "remotePeerId");
        fs1.f(wCPeerMeta, "remotePeerMeta");
        fs1.f(date, "date");
        this.session = wCSession;
        this.chainId = i;
        this.peerId = str;
        this.remotePeerId = str2;
        this.remotePeerMeta = wCPeerMeta;
        this.isAutoSign = z;
        this.date = date;
    }

    public static /* synthetic */ WCSessionStoreItem copy$default(WCSessionStoreItem wCSessionStoreItem, WCSession wCSession, int i, String str, String str2, WCPeerMeta wCPeerMeta, boolean z, Date date, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            wCSession = wCSessionStoreItem.session;
        }
        if ((i2 & 2) != 0) {
            i = wCSessionStoreItem.chainId;
        }
        int i3 = i;
        if ((i2 & 4) != 0) {
            str = wCSessionStoreItem.peerId;
        }
        String str3 = str;
        if ((i2 & 8) != 0) {
            str2 = wCSessionStoreItem.remotePeerId;
        }
        String str4 = str2;
        if ((i2 & 16) != 0) {
            wCPeerMeta = wCSessionStoreItem.remotePeerMeta;
        }
        WCPeerMeta wCPeerMeta2 = wCPeerMeta;
        if ((i2 & 32) != 0) {
            z = wCSessionStoreItem.isAutoSign;
        }
        boolean z2 = z;
        if ((i2 & 64) != 0) {
            date = wCSessionStoreItem.date;
        }
        return wCSessionStoreItem.copy(wCSession, i3, str3, str4, wCPeerMeta2, z2, date);
    }

    public final WCSession component1() {
        return this.session;
    }

    public final int component2() {
        return this.chainId;
    }

    public final String component3() {
        return this.peerId;
    }

    public final String component4() {
        return this.remotePeerId;
    }

    public final WCPeerMeta component5() {
        return this.remotePeerMeta;
    }

    public final boolean component6() {
        return this.isAutoSign;
    }

    public final Date component7() {
        return this.date;
    }

    public final WCSessionStoreItem copy(WCSession wCSession, int i, String str, String str2, WCPeerMeta wCPeerMeta, boolean z, Date date) {
        fs1.f(wCSession, "session");
        fs1.f(str, "peerId");
        fs1.f(str2, "remotePeerId");
        fs1.f(wCPeerMeta, "remotePeerMeta");
        fs1.f(date, "date");
        return new WCSessionStoreItem(wCSession, i, str, str2, wCPeerMeta, z, date);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof WCSessionStoreItem) {
            WCSessionStoreItem wCSessionStoreItem = (WCSessionStoreItem) obj;
            return fs1.b(this.session, wCSessionStoreItem.session) && this.chainId == wCSessionStoreItem.chainId && fs1.b(this.peerId, wCSessionStoreItem.peerId) && fs1.b(this.remotePeerId, wCSessionStoreItem.remotePeerId) && fs1.b(this.remotePeerMeta, wCSessionStoreItem.remotePeerMeta) && this.isAutoSign == wCSessionStoreItem.isAutoSign && fs1.b(this.date, wCSessionStoreItem.date);
        }
        return false;
    }

    public final int getChainId() {
        return this.chainId;
    }

    public final Date getDate() {
        return this.date;
    }

    public final String getPeerId() {
        return this.peerId;
    }

    public final String getRemotePeerId() {
        return this.remotePeerId;
    }

    public final WCPeerMeta getRemotePeerMeta() {
        return this.remotePeerMeta;
    }

    public final WCSession getSession() {
        return this.session;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public int hashCode() {
        int hashCode = ((((((((this.session.hashCode() * 31) + this.chainId) * 31) + this.peerId.hashCode()) * 31) + this.remotePeerId.hashCode()) * 31) + this.remotePeerMeta.hashCode()) * 31;
        boolean z = this.isAutoSign;
        int i = z;
        if (z != 0) {
            i = 1;
        }
        return ((hashCode + i) * 31) + this.date.hashCode();
    }

    public final boolean isAutoSign() {
        return this.isAutoSign;
    }

    public String toString() {
        return "WCSessionStoreItem(session=" + this.session + ", chainId=" + this.chainId + ", peerId=" + this.peerId + ", remotePeerId=" + this.remotePeerId + ", remotePeerMeta=" + this.remotePeerMeta + ", isAutoSign=" + this.isAutoSign + ", date=" + this.date + ')';
    }

    public /* synthetic */ WCSessionStoreItem(WCSession wCSession, int i, String str, String str2, WCPeerMeta wCPeerMeta, boolean z, Date date, int i2, qi0 qi0Var) {
        this(wCSession, i, str, str2, wCPeerMeta, (i2 & 32) != 0 ? false : z, (i2 & 64) != 0 ? new Date() : date);
    }
}
