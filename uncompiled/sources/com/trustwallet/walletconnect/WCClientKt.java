package com.trustwallet.walletconnect;

import java.util.Date;

/* compiled from: WCClient.kt */
/* loaded from: classes2.dex */
public final class WCClientKt {
    public static final String JSONRPC_VERSION = "2.0";
    public static final int WS_CLOSE_NORMAL = 1000;

    /* JADX INFO: Access modifiers changed from: private */
    public static final long generateId() {
        return new Date().getTime();
    }
}
