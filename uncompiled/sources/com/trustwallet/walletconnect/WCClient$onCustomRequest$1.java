package com.trustwallet.walletconnect;

import kotlin.jvm.internal.Lambda;

/* compiled from: WCClient.kt */
/* loaded from: classes2.dex */
public final class WCClient$onCustomRequest$1 extends Lambda implements hd1<Long, String, te4> {
    public static final WCClient$onCustomRequest$1 INSTANCE = new WCClient$onCustomRequest$1();

    public WCClient$onCustomRequest$1() {
        super(2);
    }

    @Override // defpackage.hd1
    public /* bridge */ /* synthetic */ te4 invoke(Long l, String str) {
        invoke(l.longValue(), str);
        return te4.a;
    }

    public final void invoke(long j, String str) {
        fs1.f(str, "$noName_1");
    }
}
