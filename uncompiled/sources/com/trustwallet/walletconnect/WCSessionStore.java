package com.trustwallet.walletconnect;

/* compiled from: WCSessionStore.kt */
/* loaded from: classes2.dex */
public interface WCSessionStore {
    WCSessionStoreItem getSession();

    void setSession(WCSessionStoreItem wCSessionStoreItem);
}
