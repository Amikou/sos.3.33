package com.trustwallet.walletconnect;

import com.trustwallet.walletconnect.models.binance.WCBinanceTxConfirmParam;
import kotlin.jvm.internal.Lambda;

/* compiled from: WCClient.kt */
/* loaded from: classes2.dex */
public final class WCClient$onBnbTxConfirm$1 extends Lambda implements hd1<Long, WCBinanceTxConfirmParam, te4> {
    public static final WCClient$onBnbTxConfirm$1 INSTANCE = new WCClient$onBnbTxConfirm$1();

    public WCClient$onBnbTxConfirm$1() {
        super(2);
    }

    @Override // defpackage.hd1
    public /* bridge */ /* synthetic */ te4 invoke(Long l, WCBinanceTxConfirmParam wCBinanceTxConfirmParam) {
        invoke(l.longValue(), wCBinanceTxConfirmParam);
        return te4.a;
    }

    public final void invoke(long j, WCBinanceTxConfirmParam wCBinanceTxConfirmParam) {
        fs1.f(wCBinanceTxConfirmParam, "$noName_1");
    }
}
