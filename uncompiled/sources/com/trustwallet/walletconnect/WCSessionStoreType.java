package com.trustwallet.walletconnect;

import android.content.SharedPreferences;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/* compiled from: WCSessionStoreType.kt */
/* loaded from: classes2.dex */
public final class WCSessionStoreType implements WCSessionStore {
    public static final Companion Companion = new Companion(null);
    private static final String SESSION_KEY = "org.walletconnect.session";
    private final Gson gson;
    private final SharedPreferences sharedPreferences;

    /* compiled from: WCSessionStoreType.kt */
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(qi0 qi0Var) {
            this();
        }
    }

    public WCSessionStoreType(SharedPreferences sharedPreferences, GsonBuilder gsonBuilder) {
        fs1.f(sharedPreferences, "sharedPreferences");
        fs1.f(gsonBuilder, "builder");
        this.sharedPreferences = sharedPreferences;
        this.gson = gsonBuilder.serializeNulls().create();
    }

    private final WCSessionStoreItem load() {
        Type d;
        String string = this.sharedPreferences.getString(SESSION_KEY, null);
        if (string == null) {
            return null;
        }
        Gson gson = this.gson;
        fs1.e(gson, "gson");
        Type type = new TypeToken<WCSessionStoreItem>() { // from class: com.trustwallet.walletconnect.WCSessionStoreType$load$$inlined$fromJson$1
        }.getType();
        fs1.c(type, "object : TypeToken<T>() {} .type");
        if (type instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) type;
            if (xi1.a(parameterizedType)) {
                d = parameterizedType.getRawType();
                fs1.c(d, "type.rawType");
                Object fromJson = gson.fromJson(string, d);
                fs1.c(fromJson, "fromJson(json, typeToken<T>())");
                return (WCSessionStoreItem) fromJson;
            }
        }
        d = xi1.d(type);
        Object fromJson2 = gson.fromJson(string, d);
        fs1.c(fromJson2, "fromJson(json, typeToken<T>())");
        return (WCSessionStoreItem) fromJson2;
    }

    private final void store(WCSessionStoreItem wCSessionStoreItem) {
        if (wCSessionStoreItem != null) {
            this.sharedPreferences.edit().putString(SESSION_KEY, this.gson.toJson(wCSessionStoreItem)).apply();
        } else {
            this.sharedPreferences.edit().remove(SESSION_KEY).apply();
        }
    }

    @Override // com.trustwallet.walletconnect.WCSessionStore
    public WCSessionStoreItem getSession() {
        return load();
    }

    @Override // com.trustwallet.walletconnect.WCSessionStore
    public void setSession(WCSessionStoreItem wCSessionStoreItem) {
        store(wCSessionStoreItem);
    }

    public /* synthetic */ WCSessionStoreType(SharedPreferences sharedPreferences, GsonBuilder gsonBuilder, int i, qi0 qi0Var) {
        this(sharedPreferences, (i & 2) != 0 ? new GsonBuilder() : gsonBuilder);
    }
}
