package com.trustwallet.walletconnect;

import com.trustwallet.walletconnect.models.binance.WCBinanceTradeOrder;
import kotlin.jvm.internal.Lambda;

/* compiled from: WCClient.kt */
/* loaded from: classes2.dex */
public final class WCClient$onBnbTrade$1 extends Lambda implements hd1<Long, WCBinanceTradeOrder, te4> {
    public static final WCClient$onBnbTrade$1 INSTANCE = new WCClient$onBnbTrade$1();

    public WCClient$onBnbTrade$1() {
        super(2);
    }

    @Override // defpackage.hd1
    public /* bridge */ /* synthetic */ te4 invoke(Long l, WCBinanceTradeOrder wCBinanceTradeOrder) {
        invoke(l.longValue(), wCBinanceTradeOrder);
        return te4.a;
    }

    public final void invoke(long j, WCBinanceTradeOrder wCBinanceTradeOrder) {
        fs1.f(wCBinanceTradeOrder, "$noName_1");
    }
}
