package com.github.mikephil.charting.data;

import android.annotation.SuppressLint;
import com.github.mikephil.charting.utils.Utils;

@SuppressLint({"ParcelCreator"})
/* loaded from: classes.dex */
public class RadarEntry extends Entry {
    public RadarEntry(float f) {
        super(Utils.FLOAT_EPSILON, f);
    }

    public float getValue() {
        return getY();
    }

    @Override // com.github.mikephil.charting.data.Entry
    @Deprecated
    public float getX() {
        return super.getX();
    }

    @Override // com.github.mikephil.charting.data.Entry
    @Deprecated
    public void setX(float f) {
        super.setX(f);
    }

    public RadarEntry(float f, Object obj) {
        super((float) Utils.FLOAT_EPSILON, f, obj);
    }

    @Override // com.github.mikephil.charting.data.Entry
    public RadarEntry copy() {
        return new RadarEntry(getY(), getData());
    }
}
