package com.squareup.picasso;

import android.content.Context;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.p;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: ContentStreamRequestHandler.java */
/* loaded from: classes2.dex */
public class e extends p {
    public final Context a;

    public e(Context context) {
        this.a = context;
    }

    @Override // com.squareup.picasso.p
    public boolean c(n nVar) {
        return PublicResolver.FUNC_CONTENT.equals(nVar.d.getScheme());
    }

    @Override // com.squareup.picasso.p
    public p.a f(n nVar, int i) throws IOException {
        return new p.a(okio.k.l(j(nVar)), Picasso.LoadedFrom.DISK);
    }

    public InputStream j(n nVar) throws FileNotFoundException {
        return this.a.getContentResolver().openInputStream(nVar.d);
    }
}
