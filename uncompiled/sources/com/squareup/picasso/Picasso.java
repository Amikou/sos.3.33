package com.squareup.picasso;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.widget.ImageView;
import com.squareup.picasso.a;
import java.io.File;
import java.lang.ref.ReferenceQueue;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;

/* loaded from: classes2.dex */
public class Picasso {
    public static final Handler p = new a(Looper.getMainLooper());
    @SuppressLint({"StaticFieldLeak"})
    public static volatile Picasso q = null;
    public final d a;
    public final e b;
    public final c c;
    public final List<p> d;
    public final Context e;
    public final f f;
    public final tt g;
    public final kt3 h;
    public final Map<Object, com.squareup.picasso.a> i;
    public final Map<ImageView, vl0> j;
    public final ReferenceQueue<Object> k;
    public final Bitmap.Config l;
    public boolean m;
    public volatile boolean n;
    public boolean o;

    /* loaded from: classes2.dex */
    public enum LoadedFrom {
        MEMORY(-16711936),
        DISK(-16776961),
        NETWORK(-65536);
        
        public final int debugColor;

        LoadedFrom(int i) {
            this.debugColor = i;
        }
    }

    /* loaded from: classes2.dex */
    public enum Priority {
        LOW,
        NORMAL,
        HIGH
    }

    /* loaded from: classes2.dex */
    public static class a extends Handler {
        public a(Looper looper) {
            super(looper);
        }

        @Override // android.os.Handler
        public void handleMessage(Message message) {
            int i = message.what;
            if (i == 3) {
                com.squareup.picasso.a aVar = (com.squareup.picasso.a) message.obj;
                if (aVar.g().n) {
                    t.t("Main", "canceled", aVar.b.d(), "target got garbage collected");
                }
                aVar.a.a(aVar.k());
                return;
            }
            int i2 = 0;
            if (i == 8) {
                List list = (List) message.obj;
                int size = list.size();
                while (i2 < size) {
                    com.squareup.picasso.c cVar = (com.squareup.picasso.c) list.get(i2);
                    cVar.f0.d(cVar);
                    i2++;
                }
            } else if (i == 13) {
                List list2 = (List) message.obj;
                int size2 = list2.size();
                while (i2 < size2) {
                    com.squareup.picasso.a aVar2 = (com.squareup.picasso.a) list2.get(i2);
                    aVar2.a.n(aVar2);
                    i2++;
                }
            } else {
                throw new AssertionError("Unknown handler message received: " + message.what);
            }
        }
    }

    /* loaded from: classes2.dex */
    public static class b {
        public final Context a;
        public g b;
        public ExecutorService c;
        public tt d;
        public d e;
        public e f;
        public List<p> g;
        public Bitmap.Config h;
        public boolean i;
        public boolean j;

        public b(Context context) {
            if (context != null) {
                this.a = context.getApplicationContext();
                return;
            }
            throw new IllegalArgumentException("Context must not be null.");
        }

        public Picasso a() {
            Context context = this.a;
            if (this.b == null) {
                this.b = new k(context);
            }
            if (this.d == null) {
                this.d = new r22(context);
            }
            if (this.c == null) {
                this.c = new m();
            }
            if (this.f == null) {
                this.f = e.a;
            }
            kt3 kt3Var = new kt3(this.d);
            return new Picasso(context, new f(context, this.c, Picasso.p, this.b, this.d, kt3Var), this.d, this.e, this.f, this.g, kt3Var, this.h, this.i, this.j);
        }

        public b b(Bitmap.Config config) {
            if (config != null) {
                this.h = config;
                return this;
            }
            throw new IllegalArgumentException("Bitmap config must not be null.");
        }

        public b c(g gVar) {
            if (gVar != null) {
                if (this.b == null) {
                    this.b = gVar;
                    return this;
                }
                throw new IllegalStateException("Downloader already set.");
            }
            throw new IllegalArgumentException("Downloader must not be null.");
        }

        public b d(ExecutorService executorService) {
            if (executorService != null) {
                if (this.c == null) {
                    this.c = executorService;
                    return this;
                }
                throw new IllegalStateException("Executor service already set.");
            }
            throw new IllegalArgumentException("Executor service must not be null.");
        }
    }

    /* loaded from: classes2.dex */
    public static class c extends Thread {
        public final ReferenceQueue<Object> a;
        public final Handler f0;

        /* loaded from: classes2.dex */
        public class a implements Runnable {
            public final /* synthetic */ Exception a;

            public a(c cVar, Exception exc) {
                this.a = exc;
            }

            @Override // java.lang.Runnable
            public void run() {
                throw new RuntimeException(this.a);
            }
        }

        public c(ReferenceQueue<Object> referenceQueue, Handler handler) {
            this.a = referenceQueue;
            this.f0 = handler;
            setDaemon(true);
            setName("Picasso-refQueue");
        }

        @Override // java.lang.Thread, java.lang.Runnable
        public void run() {
            Process.setThreadPriority(10);
            while (true) {
                try {
                    a.C0163a c0163a = (a.C0163a) this.a.remove(1000L);
                    Message obtainMessage = this.f0.obtainMessage();
                    if (c0163a != null) {
                        obtainMessage.what = 3;
                        obtainMessage.obj = c0163a.a;
                        this.f0.sendMessage(obtainMessage);
                    } else {
                        obtainMessage.recycle();
                    }
                } catch (InterruptedException unused) {
                    return;
                } catch (Exception e) {
                    this.f0.post(new a(this, e));
                    return;
                }
            }
        }
    }

    /* loaded from: classes2.dex */
    public interface d {
        void a(Picasso picasso, Uri uri, Exception exc);
    }

    /* loaded from: classes2.dex */
    public interface e {
        public static final e a = new a();

        /* loaded from: classes2.dex */
        public static class a implements e {
            @Override // com.squareup.picasso.Picasso.e
            public n a(n nVar) {
                return nVar;
            }
        }

        n a(n nVar);
    }

    public Picasso(Context context, f fVar, tt ttVar, d dVar, e eVar, List<p> list, kt3 kt3Var, Bitmap.Config config, boolean z, boolean z2) {
        this.e = context;
        this.f = fVar;
        this.g = ttVar;
        this.a = dVar;
        this.b = eVar;
        this.l = config;
        ArrayList arrayList = new ArrayList((list != null ? list.size() : 0) + 7);
        arrayList.add(new q(context));
        if (list != null) {
            arrayList.addAll(list);
        }
        arrayList.add(new com.squareup.picasso.d(context));
        arrayList.add(new MediaStoreRequestHandler(context));
        arrayList.add(new com.squareup.picasso.e(context));
        arrayList.add(new com.squareup.picasso.b(context));
        arrayList.add(new h(context));
        arrayList.add(new NetworkRequestHandler(fVar.d, kt3Var));
        this.d = Collections.unmodifiableList(arrayList);
        this.h = kt3Var;
        this.i = new WeakHashMap();
        this.j = new WeakHashMap();
        this.m = z;
        this.n = z2;
        ReferenceQueue<Object> referenceQueue = new ReferenceQueue<>();
        this.k = referenceQueue;
        c cVar = new c(referenceQueue, p);
        this.c = cVar;
        cVar.start();
    }

    public static Picasso h() {
        if (q == null) {
            synchronized (Picasso.class) {
                if (q == null) {
                    Context context = PicassoProvider.a;
                    if (context != null) {
                        q = new b(context).a();
                    } else {
                        throw new IllegalStateException("context == null");
                    }
                }
            }
        }
        return q;
    }

    public void a(Object obj) {
        t.c();
        com.squareup.picasso.a remove = this.i.remove(obj);
        if (remove != null) {
            remove.a();
            this.f.c(remove);
        }
        if (obj instanceof ImageView) {
            vl0 remove2 = this.j.remove((ImageView) obj);
            if (remove2 != null) {
                remove2.a();
            }
        }
    }

    public void b(ImageView imageView) {
        if (imageView != null) {
            a(imageView);
            return;
        }
        throw new IllegalArgumentException("view cannot be null.");
    }

    public void c(r rVar) {
        if (rVar != null) {
            a(rVar);
            return;
        }
        throw new IllegalArgumentException("target cannot be null.");
    }

    public void d(com.squareup.picasso.c cVar) {
        com.squareup.picasso.a h = cVar.h();
        List<com.squareup.picasso.a> i = cVar.i();
        boolean z = true;
        boolean z2 = (i == null || i.isEmpty()) ? false : true;
        if (h == null && !z2) {
            z = false;
        }
        if (z) {
            Uri uri = cVar.j().d;
            Exception k = cVar.k();
            Bitmap s = cVar.s();
            LoadedFrom o = cVar.o();
            if (h != null) {
                f(s, o, h, k);
            }
            if (z2) {
                int size = i.size();
                for (int i2 = 0; i2 < size; i2++) {
                    f(s, o, i.get(i2), k);
                }
            }
            d dVar = this.a;
            if (dVar == null || k == null) {
                return;
            }
            dVar.a(this, uri, k);
        }
    }

    public void e(ImageView imageView, vl0 vl0Var) {
        if (this.j.containsKey(imageView)) {
            a(imageView);
        }
        this.j.put(imageView, vl0Var);
    }

    public final void f(Bitmap bitmap, LoadedFrom loadedFrom, com.squareup.picasso.a aVar, Exception exc) {
        if (aVar.l()) {
            return;
        }
        if (!aVar.m()) {
            this.i.remove(aVar.k());
        }
        if (bitmap == null) {
            aVar.c(exc);
            if (this.n) {
                t.t("Main", "errored", aVar.b.d(), exc.getMessage());
            }
        } else if (loadedFrom != null) {
            aVar.b(bitmap, loadedFrom);
            if (this.n) {
                String d2 = aVar.b.d();
                t.t("Main", "completed", d2, "from " + loadedFrom);
            }
        } else {
            throw new AssertionError("LoadedFrom cannot be null.");
        }
    }

    public void g(com.squareup.picasso.a aVar) {
        Object k = aVar.k();
        if (k != null && this.i.get(k) != aVar) {
            a(k);
            this.i.put(k, aVar);
        }
        o(aVar);
    }

    public List<p> i() {
        return this.d;
    }

    public o j(Uri uri) {
        return new o(this, uri, 0);
    }

    public o k(File file) {
        if (file == null) {
            return new o(this, null, 0);
        }
        return j(Uri.fromFile(file));
    }

    public o l(String str) {
        if (str == null) {
            return new o(this, null, 0);
        }
        if (str.trim().length() != 0) {
            return j(Uri.parse(str));
        }
        throw new IllegalArgumentException("Path must not be empty.");
    }

    public Bitmap m(String str) {
        Bitmap bitmap = this.g.get(str);
        if (bitmap != null) {
            this.h.d();
        } else {
            this.h.e();
        }
        return bitmap;
    }

    public void n(com.squareup.picasso.a aVar) {
        Bitmap m = MemoryPolicy.shouldReadFromMemoryCache(aVar.e) ? m(aVar.d()) : null;
        if (m != null) {
            LoadedFrom loadedFrom = LoadedFrom.MEMORY;
            f(m, loadedFrom, aVar, null);
            if (this.n) {
                String d2 = aVar.b.d();
                t.t("Main", "completed", d2, "from " + loadedFrom);
                return;
            }
            return;
        }
        g(aVar);
        if (this.n) {
            t.s("Main", "resumed", aVar.b.d());
        }
    }

    public void o(com.squareup.picasso.a aVar) {
        this.f.h(aVar);
    }

    public n p(n nVar) {
        n a2 = this.b.a(nVar);
        if (a2 != null) {
            return a2;
        }
        throw new IllegalStateException("Request transformer " + this.b.getClass().getCanonicalName() + " returned null for " + nVar);
    }
}
