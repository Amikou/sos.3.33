package com.squareup.picasso;

import android.net.NetworkInfo;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.p;
import java.io.IOException;
import okhttp3.CacheControl;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

/* loaded from: classes2.dex */
public class NetworkRequestHandler extends p {
    public final g a;
    public final kt3 b;

    /* loaded from: classes2.dex */
    public static class ContentLengthException extends IOException {
        public ContentLengthException(String str) {
            super(str);
        }
    }

    /* loaded from: classes2.dex */
    public static final class ResponseException extends IOException {
        public final int code;
        public final int networkPolicy;

        public ResponseException(int i, int i2) {
            super("HTTP " + i);
            this.code = i;
            this.networkPolicy = i2;
        }
    }

    public NetworkRequestHandler(g gVar, kt3 kt3Var) {
        this.a = gVar;
        this.b = kt3Var;
    }

    public static Request j(n nVar, int i) {
        CacheControl cacheControl;
        if (i == 0) {
            cacheControl = null;
        } else if (NetworkPolicy.isOfflineOnly(i)) {
            cacheControl = CacheControl.FORCE_CACHE;
        } else {
            CacheControl.Builder builder = new CacheControl.Builder();
            if (!NetworkPolicy.shouldReadFromDiskCache(i)) {
                builder.noCache();
            }
            if (!NetworkPolicy.shouldWriteToDiskCache(i)) {
                builder.noStore();
            }
            cacheControl = builder.build();
        }
        Request.Builder url = new Request.Builder().url(nVar.d.toString());
        if (cacheControl != null) {
            url.cacheControl(cacheControl);
        }
        return url.build();
    }

    @Override // com.squareup.picasso.p
    public boolean c(n nVar) {
        String scheme = nVar.d.getScheme();
        return "http".equals(scheme) || "https".equals(scheme);
    }

    @Override // com.squareup.picasso.p
    public int e() {
        return 2;
    }

    @Override // com.squareup.picasso.p
    public p.a f(n nVar, int i) throws IOException {
        Response a = this.a.a(j(nVar, i));
        ResponseBody body = a.body();
        if (a.isSuccessful()) {
            Picasso.LoadedFrom loadedFrom = a.cacheResponse() == null ? Picasso.LoadedFrom.NETWORK : Picasso.LoadedFrom.DISK;
            if (loadedFrom == Picasso.LoadedFrom.DISK && body.contentLength() == 0) {
                body.close();
                throw new ContentLengthException("Received response with 0 content-length header.");
            }
            if (loadedFrom == Picasso.LoadedFrom.NETWORK && body.contentLength() > 0) {
                this.b.f(body.contentLength());
            }
            return new p.a(body.source(), loadedFrom);
        }
        body.close();
        throw new ResponseException(a.code(), nVar.c);
    }

    @Override // com.squareup.picasso.p
    public boolean h(boolean z, NetworkInfo networkInfo) {
        return networkInfo == null || networkInfo.isConnected();
    }

    @Override // com.squareup.picasso.p
    public boolean i() {
        return true;
    }
}
