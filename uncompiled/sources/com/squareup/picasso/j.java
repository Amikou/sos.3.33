package com.squareup.picasso;

import androidx.recyclerview.widget.RecyclerView;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

/* compiled from: MarkableInputStream.java */
/* loaded from: classes2.dex */
public final class j extends InputStream {
    public final InputStream a;
    public long f0;
    public long g0;
    public long h0;
    public long i0;
    public boolean j0;
    public int k0;

    public j(InputStream inputStream) {
        this(inputStream, 4096);
    }

    public void a(boolean z) {
        this.j0 = z;
    }

    @Override // java.io.InputStream
    public int available() throws IOException {
        return this.a.available();
    }

    public void b(long j) throws IOException {
        if (this.f0 <= this.h0 && j >= this.g0) {
            this.a.reset();
            e(this.g0, j);
            this.f0 = j;
            return;
        }
        throw new IOException("Cannot reset");
    }

    public long c(int i) {
        long j = this.f0 + i;
        if (this.h0 < j) {
            d(j);
        }
        return this.f0;
    }

    @Override // java.io.InputStream, java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        this.a.close();
    }

    public final void d(long j) {
        try {
            long j2 = this.g0;
            long j3 = this.f0;
            if (j2 < j3 && j3 <= this.h0) {
                this.a.reset();
                this.a.mark((int) (j - this.g0));
                e(this.g0, this.f0);
            } else {
                this.g0 = j3;
                this.a.mark((int) (j - j3));
            }
            this.h0 = j;
        } catch (IOException e) {
            throw new IllegalStateException("Unable to mark: " + e);
        }
    }

    public final void e(long j, long j2) throws IOException {
        while (j < j2) {
            long skip = this.a.skip(j2 - j);
            if (skip == 0) {
                if (read() == -1) {
                    return;
                }
                skip = 1;
            }
            j += skip;
        }
    }

    @Override // java.io.InputStream
    public void mark(int i) {
        this.i0 = c(i);
    }

    @Override // java.io.InputStream
    public boolean markSupported() {
        return this.a.markSupported();
    }

    @Override // java.io.InputStream
    public int read() throws IOException {
        if (!this.j0) {
            long j = this.h0;
            if (this.f0 + 1 > j) {
                d(j + this.k0);
            }
        }
        int read = this.a.read();
        if (read != -1) {
            this.f0++;
        }
        return read;
    }

    @Override // java.io.InputStream
    public void reset() throws IOException {
        b(this.i0);
    }

    @Override // java.io.InputStream
    public long skip(long j) throws IOException {
        if (!this.j0) {
            long j2 = this.f0;
            if (j2 + j > this.h0) {
                d(j2 + j + this.k0);
            }
        }
        long skip = this.a.skip(j);
        this.f0 += skip;
        return skip;
    }

    public j(InputStream inputStream, int i) {
        this(inputStream, i, RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE);
    }

    public j(InputStream inputStream, int i, int i2) {
        this.i0 = -1L;
        this.j0 = true;
        this.k0 = -1;
        this.a = inputStream.markSupported() ? inputStream : new BufferedInputStream(inputStream, i);
        this.k0 = i2;
    }

    @Override // java.io.InputStream
    public int read(byte[] bArr) throws IOException {
        if (!this.j0) {
            long j = this.f0;
            if (bArr.length + j > this.h0) {
                d(j + bArr.length + this.k0);
            }
        }
        int read = this.a.read(bArr);
        if (read != -1) {
            this.f0 += read;
        }
        return read;
    }

    @Override // java.io.InputStream
    public int read(byte[] bArr, int i, int i2) throws IOException {
        if (!this.j0) {
            long j = this.f0;
            long j2 = i2;
            if (j + j2 > this.h0) {
                d(j + j2 + this.k0);
            }
        }
        int read = this.a.read(bArr, i, i2);
        if (read != -1) {
            this.f0 += read;
        }
        return read;
    }
}
