package com.squareup.picasso;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.NetworkInfo;
import android.os.Build;
import androidx.recyclerview.widget.RecyclerView;
import com.squareup.picasso.NetworkRequestHandler;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.p;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: BitmapHunter.java */
/* loaded from: classes2.dex */
public class c implements Runnable {
    public final int a = z0.incrementAndGet();
    public final Picasso f0;
    public final com.squareup.picasso.f g0;
    public final tt h0;
    public final kt3 i0;
    public final String j0;
    public final n k0;
    public final int l0;
    public int m0;
    public final p n0;
    public com.squareup.picasso.a o0;
    public List<com.squareup.picasso.a> p0;
    public Bitmap q0;
    public Future<?> r0;
    public Picasso.LoadedFrom s0;
    public Exception t0;
    public int u0;
    public int v0;
    public Picasso.Priority w0;
    public static final Object x0 = new Object();
    public static final ThreadLocal<StringBuilder> y0 = new a();
    public static final AtomicInteger z0 = new AtomicInteger();
    public static final p A0 = new b();

    /* compiled from: BitmapHunter.java */
    /* loaded from: classes2.dex */
    public static class a extends ThreadLocal<StringBuilder> {
        @Override // java.lang.ThreadLocal
        /* renamed from: a */
        public StringBuilder initialValue() {
            return new StringBuilder("Picasso-");
        }
    }

    /* compiled from: BitmapHunter.java */
    /* loaded from: classes2.dex */
    public static class b extends p {
        @Override // com.squareup.picasso.p
        public boolean c(n nVar) {
            return true;
        }

        @Override // com.squareup.picasso.p
        public p.a f(n nVar, int i) throws IOException {
            throw new IllegalStateException("Unrecognized type of request: " + nVar);
        }
    }

    /* compiled from: BitmapHunter.java */
    /* renamed from: com.squareup.picasso.c$c  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public static class RunnableC0164c implements Runnable {
        public final /* synthetic */ ya4 a;
        public final /* synthetic */ RuntimeException f0;

        public RunnableC0164c(ya4 ya4Var, RuntimeException runtimeException) {
            this.a = ya4Var;
            this.f0 = runtimeException;
        }

        @Override // java.lang.Runnable
        public void run() {
            throw new RuntimeException("Transformation " + this.a.key() + " crashed with exception.", this.f0);
        }
    }

    /* compiled from: BitmapHunter.java */
    /* loaded from: classes2.dex */
    public static class d implements Runnable {
        public final /* synthetic */ StringBuilder a;

        public d(StringBuilder sb) {
            this.a = sb;
        }

        @Override // java.lang.Runnable
        public void run() {
            throw new NullPointerException(this.a.toString());
        }
    }

    /* compiled from: BitmapHunter.java */
    /* loaded from: classes2.dex */
    public static class e implements Runnable {
        public final /* synthetic */ ya4 a;

        public e(ya4 ya4Var) {
            this.a = ya4Var;
        }

        @Override // java.lang.Runnable
        public void run() {
            throw new IllegalStateException("Transformation " + this.a.key() + " returned input Bitmap but recycled it.");
        }
    }

    /* compiled from: BitmapHunter.java */
    /* loaded from: classes2.dex */
    public static class f implements Runnable {
        public final /* synthetic */ ya4 a;

        public f(ya4 ya4Var) {
            this.a = ya4Var;
        }

        @Override // java.lang.Runnable
        public void run() {
            throw new IllegalStateException("Transformation " + this.a.key() + " mutated input Bitmap but failed to recycle the original.");
        }
    }

    public c(Picasso picasso, com.squareup.picasso.f fVar, tt ttVar, kt3 kt3Var, com.squareup.picasso.a aVar, p pVar) {
        this.f0 = picasso;
        this.g0 = fVar;
        this.h0 = ttVar;
        this.i0 = kt3Var;
        this.o0 = aVar;
        this.j0 = aVar.d();
        this.k0 = aVar.i();
        this.w0 = aVar.h();
        this.l0 = aVar.e();
        this.m0 = aVar.f();
        this.n0 = pVar;
        this.v0 = pVar.e();
    }

    public static Bitmap a(List<ya4> list, Bitmap bitmap) {
        int size = list.size();
        int i = 0;
        while (i < size) {
            ya4 ya4Var = list.get(i);
            try {
                Bitmap transform = ya4Var.transform(bitmap);
                if (transform == null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Transformation ");
                    sb.append(ya4Var.key());
                    sb.append(" returned null after ");
                    sb.append(i);
                    sb.append(" previous transformation(s).\n\nTransformation list:\n");
                    for (ya4 ya4Var2 : list) {
                        sb.append(ya4Var2.key());
                        sb.append('\n');
                    }
                    Picasso.p.post(new d(sb));
                    return null;
                } else if (transform == bitmap && bitmap.isRecycled()) {
                    Picasso.p.post(new e(ya4Var));
                    return null;
                } else if (transform != bitmap && !bitmap.isRecycled()) {
                    Picasso.p.post(new f(ya4Var));
                    return null;
                } else {
                    i++;
                    bitmap = transform;
                }
            } catch (RuntimeException e2) {
                Picasso.p.post(new RunnableC0164c(ya4Var, e2));
                return null;
            }
        }
        return bitmap;
    }

    public static Bitmap e(okio.n nVar, n nVar2) throws IOException {
        okio.d d2 = okio.k.d(nVar);
        boolean r = t.r(d2);
        boolean z = nVar2.r && Build.VERSION.SDK_INT < 21;
        BitmapFactory.Options d3 = p.d(nVar2);
        boolean g = p.g(d3);
        if (!r && !z) {
            InputStream G1 = d2.G1();
            if (g) {
                j jVar = new j(G1);
                jVar.a(false);
                long c = jVar.c(RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE);
                BitmapFactory.decodeStream(jVar, null, d3);
                p.b(nVar2.h, nVar2.i, d3, nVar2);
                jVar.b(c);
                jVar.a(true);
                G1 = jVar;
            }
            Bitmap decodeStream = BitmapFactory.decodeStream(G1, null, d3);
            if (decodeStream != null) {
                return decodeStream;
            }
            throw new IOException("Failed to decode stream.");
        }
        byte[] Z = d2.Z();
        if (g) {
            BitmapFactory.decodeByteArray(Z, 0, Z.length, d3);
            p.b(nVar2.h, nVar2.i, d3, nVar2);
        }
        return BitmapFactory.decodeByteArray(Z, 0, Z.length, d3);
    }

    public static c g(Picasso picasso, com.squareup.picasso.f fVar, tt ttVar, kt3 kt3Var, com.squareup.picasso.a aVar) {
        n i = aVar.i();
        List<p> i2 = picasso.i();
        int size = i2.size();
        for (int i3 = 0; i3 < size; i3++) {
            p pVar = i2.get(i3);
            if (pVar.c(i)) {
                return new c(picasso, fVar, ttVar, kt3Var, aVar, pVar);
            }
        }
        return new c(picasso, fVar, ttVar, kt3Var, aVar, A0);
    }

    public static int l(int i) {
        switch (i) {
            case 3:
            case 4:
                return 180;
            case 5:
            case 6:
                return 90;
            case 7:
            case 8:
                return 270;
            default:
                return 0;
        }
    }

    public static int m(int i) {
        return (i == 2 || i == 7 || i == 4 || i == 5) ? -1 : 1;
    }

    public static boolean v(boolean z, int i, int i2, int i3, int i4) {
        return !z || (i3 != 0 && i > i3) || (i4 != 0 && i2 > i4);
    }

    /* JADX WARN: Removed duplicated region for block: B:91:0x024b  */
    /* JADX WARN: Removed duplicated region for block: B:92:0x024f  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static android.graphics.Bitmap y(com.squareup.picasso.n r26, android.graphics.Bitmap r27, int r28) {
        /*
            Method dump skipped, instructions count: 593
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.squareup.picasso.c.y(com.squareup.picasso.n, android.graphics.Bitmap, int):android.graphics.Bitmap");
    }

    public static void z(n nVar) {
        String a2 = nVar.a();
        StringBuilder sb = y0.get();
        sb.ensureCapacity(a2.length() + 8);
        sb.replace(8, sb.length(), a2);
        Thread.currentThread().setName(sb.toString());
    }

    public void b(com.squareup.picasso.a aVar) {
        boolean z = this.f0.n;
        n nVar = aVar.b;
        if (this.o0 == null) {
            this.o0 = aVar;
            if (z) {
                List<com.squareup.picasso.a> list = this.p0;
                if (list != null && !list.isEmpty()) {
                    t.t("Hunter", "joined", nVar.d(), t.k(this, "to "));
                    return;
                } else {
                    t.t("Hunter", "joined", nVar.d(), "to empty hunter");
                    return;
                }
            }
            return;
        }
        if (this.p0 == null) {
            this.p0 = new ArrayList(3);
        }
        this.p0.add(aVar);
        if (z) {
            t.t("Hunter", "joined", nVar.d(), t.k(this, "to "));
        }
        Picasso.Priority h = aVar.h();
        if (h.ordinal() > this.w0.ordinal()) {
            this.w0 = h;
        }
    }

    public boolean c() {
        Future<?> future;
        if (this.o0 == null) {
            List<com.squareup.picasso.a> list = this.p0;
            return (list == null || list.isEmpty()) && (future = this.r0) != null && future.cancel(false);
        }
        return false;
    }

    public final Picasso.Priority d() {
        Picasso.Priority priority = Picasso.Priority.LOW;
        List<com.squareup.picasso.a> list = this.p0;
        boolean z = true;
        boolean z2 = (list == null || list.isEmpty()) ? false : true;
        com.squareup.picasso.a aVar = this.o0;
        if (aVar == null && !z2) {
            z = false;
        }
        if (z) {
            if (aVar != null) {
                priority = aVar.h();
            }
            if (z2) {
                int size = this.p0.size();
                for (int i = 0; i < size; i++) {
                    Picasso.Priority h = this.p0.get(i).h();
                    if (h.ordinal() > priority.ordinal()) {
                        priority = h;
                    }
                }
            }
            return priority;
        }
        return priority;
    }

    public void f(com.squareup.picasso.a aVar) {
        boolean remove;
        if (this.o0 == aVar) {
            this.o0 = null;
            remove = true;
        } else {
            List<com.squareup.picasso.a> list = this.p0;
            remove = list != null ? list.remove(aVar) : false;
        }
        if (remove && aVar.h() == this.w0) {
            this.w0 = d();
        }
        if (this.f0.n) {
            t.t("Hunter", "removed", aVar.b.d(), t.k(this, "from "));
        }
    }

    public com.squareup.picasso.a h() {
        return this.o0;
    }

    public List<com.squareup.picasso.a> i() {
        return this.p0;
    }

    public n j() {
        return this.k0;
    }

    public Exception k() {
        return this.t0;
    }

    public String n() {
        return this.j0;
    }

    public Picasso.LoadedFrom o() {
        return this.s0;
    }

    public int p() {
        return this.l0;
    }

    public Picasso q() {
        return this.f0;
    }

    public Picasso.Priority r() {
        return this.w0;
    }

    @Override // java.lang.Runnable
    public void run() {
        try {
            try {
                try {
                    z(this.k0);
                    if (this.f0.n) {
                        t.s("Hunter", "executing", t.j(this));
                    }
                    Bitmap t = t();
                    this.q0 = t;
                    if (t == null) {
                        this.g0.e(this);
                    } else {
                        this.g0.d(this);
                    }
                } catch (NetworkRequestHandler.ResponseException e2) {
                    if (!NetworkPolicy.isOfflineOnly(e2.networkPolicy) || e2.code != 504) {
                        this.t0 = e2;
                    }
                    this.g0.e(this);
                } catch (IOException e3) {
                    this.t0 = e3;
                    this.g0.g(this);
                }
            } catch (Exception e4) {
                this.t0 = e4;
                this.g0.e(this);
            } catch (OutOfMemoryError e5) {
                StringWriter stringWriter = new StringWriter();
                this.i0.a().a(new PrintWriter(stringWriter));
                this.t0 = new RuntimeException(stringWriter.toString(), e5);
                this.g0.e(this);
            }
        } finally {
            Thread.currentThread().setName("Picasso-Idle");
        }
    }

    public Bitmap s() {
        return this.q0;
    }

    public Bitmap t() throws IOException {
        Bitmap bitmap;
        if (MemoryPolicy.shouldReadFromMemoryCache(this.l0)) {
            bitmap = this.h0.get(this.j0);
            if (bitmap != null) {
                this.i0.d();
                this.s0 = Picasso.LoadedFrom.MEMORY;
                if (this.f0.n) {
                    t.t("Hunter", "decoded", this.k0.d(), "from cache");
                }
                return bitmap;
            }
        } else {
            bitmap = null;
        }
        int i = this.v0 == 0 ? NetworkPolicy.OFFLINE.index : this.m0;
        this.m0 = i;
        p.a f2 = this.n0.f(this.k0, i);
        if (f2 != null) {
            this.s0 = f2.c();
            this.u0 = f2.b();
            bitmap = f2.a();
            if (bitmap == null) {
                okio.n d2 = f2.d();
                try {
                    bitmap = e(d2, this.k0);
                } finally {
                    try {
                        d2.close();
                    } catch (IOException unused) {
                    }
                }
            }
        }
        if (bitmap != null) {
            if (this.f0.n) {
                t.s("Hunter", "decoded", this.k0.d());
            }
            this.i0.b(bitmap);
            if (this.k0.f() || this.u0 != 0) {
                synchronized (x0) {
                    if (this.k0.e() || this.u0 != 0) {
                        bitmap = y(this.k0, bitmap, this.u0);
                        if (this.f0.n) {
                            t.s("Hunter", "transformed", this.k0.d());
                        }
                    }
                    if (this.k0.b()) {
                        bitmap = a(this.k0.g, bitmap);
                        if (this.f0.n) {
                            t.t("Hunter", "transformed", this.k0.d(), "from custom transformations");
                        }
                    }
                }
                if (bitmap != null) {
                    this.i0.c(bitmap);
                }
            }
        }
        return bitmap;
    }

    public boolean u() {
        Future<?> future = this.r0;
        return future != null && future.isCancelled();
    }

    public boolean w(boolean z, NetworkInfo networkInfo) {
        int i = this.v0;
        if (i > 0) {
            this.v0 = i - 1;
            return this.n0.h(z, networkInfo);
        }
        return false;
    }

    public boolean x() {
        return this.n0.i();
    }
}
