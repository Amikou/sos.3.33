package com.squareup.picasso;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.p;
import java.io.IOException;

/* compiled from: ResourceRequestHandler.java */
/* loaded from: classes2.dex */
public class q extends p {
    public final Context a;

    public q(Context context) {
        this.a = context;
    }

    public static Bitmap j(Resources resources, int i, n nVar) {
        BitmapFactory.Options d = p.d(nVar);
        if (p.g(d)) {
            BitmapFactory.decodeResource(resources, i, d);
            p.b(nVar.h, nVar.i, d, nVar);
        }
        return BitmapFactory.decodeResource(resources, i, d);
    }

    @Override // com.squareup.picasso.p
    public boolean c(n nVar) {
        if (nVar.e != 0) {
            return true;
        }
        return "android.resource".equals(nVar.d.getScheme());
    }

    @Override // com.squareup.picasso.p
    public p.a f(n nVar, int i) throws IOException {
        Resources m = t.m(this.a, nVar);
        return new p.a(j(m, t.l(m, nVar), nVar), Picasso.LoadedFrom.DISK);
    }
}
