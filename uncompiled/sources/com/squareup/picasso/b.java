package com.squareup.picasso;

import android.content.Context;
import android.content.res.AssetManager;
import android.net.Uri;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.p;
import java.io.IOException;

/* compiled from: AssetRequestHandler.java */
/* loaded from: classes2.dex */
public class b extends p {
    public static final int d = 22;
    public final Context a;
    public final Object b = new Object();
    public AssetManager c;

    public b(Context context) {
        this.a = context;
    }

    public static String j(n nVar) {
        return nVar.d.toString().substring(d);
    }

    @Override // com.squareup.picasso.p
    public boolean c(n nVar) {
        Uri uri = nVar.d;
        return "file".equals(uri.getScheme()) && !uri.getPathSegments().isEmpty() && "android_asset".equals(uri.getPathSegments().get(0));
    }

    @Override // com.squareup.picasso.p
    public p.a f(n nVar, int i) throws IOException {
        if (this.c == null) {
            synchronized (this.b) {
                if (this.c == null) {
                    this.c = this.a.getAssets();
                }
            }
        }
        return new p.a(okio.k.l(this.c.open(j(nVar))), Picasso.LoadedFrom.DISK);
    }
}
