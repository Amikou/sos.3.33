package com.squareup.picasso;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.os.StatFs;
import android.provider.Settings;
import com.github.mikephil.charting.utils.Utils;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ThreadFactory;
import okio.ByteString;

/* compiled from: Utils.java */
/* loaded from: classes2.dex */
public final class t {
    public static final StringBuilder a = new StringBuilder();
    public static final ByteString b = ByteString.encodeUtf8("RIFF");
    public static final ByteString c = ByteString.encodeUtf8("WEBP");

    /* compiled from: Utils.java */
    /* loaded from: classes2.dex */
    public static class a extends Handler {
        public a(Looper looper) {
            super(looper);
        }

        @Override // android.os.Handler
        public void handleMessage(Message message) {
            sendMessageDelayed(obtainMessage(), 1000L);
        }
    }

    /* compiled from: Utils.java */
    /* loaded from: classes2.dex */
    public static class b extends Thread {
        public b(Runnable runnable) {
            super(runnable);
        }

        @Override // java.lang.Thread, java.lang.Runnable
        public void run() {
            Process.setThreadPriority(10);
            super.run();
        }
    }

    /* compiled from: Utils.java */
    /* loaded from: classes2.dex */
    public static class c implements ThreadFactory {
        @Override // java.util.concurrent.ThreadFactory
        public Thread newThread(Runnable runnable) {
            return new b(runnable);
        }
    }

    @TargetApi(18)
    public static long a(File file) {
        long j;
        try {
            StatFs statFs = new StatFs(file.getAbsolutePath());
            int i = Build.VERSION.SDK_INT;
            j = ((i < 18 ? statFs.getBlockCount() : statFs.getBlockCountLong()) * (i < 18 ? statFs.getBlockSize() : statFs.getBlockSizeLong())) / 50;
        } catch (IllegalArgumentException unused) {
            j = 5242880;
        }
        return Math.max(Math.min(j, 52428800L), 5242880L);
    }

    public static int b(Context context) {
        ActivityManager activityManager = (ActivityManager) n(context, "activity");
        return (int) ((((context.getApplicationInfo().flags & 1048576) != 0 ? activityManager.getLargeMemoryClass() : activityManager.getMemoryClass()) * 1048576) / 7);
    }

    public static void c() {
        if (!q()) {
            throw new IllegalStateException("Method call should happen from the main thread.");
        }
    }

    public static <T> T d(T t, String str) {
        Objects.requireNonNull(t, str);
        return t;
    }

    public static File e(Context context) {
        File file = new File(context.getApplicationContext().getCacheDir(), "picasso-cache");
        if (!file.exists()) {
            file.mkdirs();
        }
        return file;
    }

    public static String f(n nVar) {
        StringBuilder sb = a;
        String g = g(nVar, sb);
        sb.setLength(0);
        return g;
    }

    public static String g(n nVar, StringBuilder sb) {
        String str = nVar.f;
        if (str != null) {
            sb.ensureCapacity(str.length() + 50);
            sb.append(nVar.f);
        } else {
            Uri uri = nVar.d;
            if (uri != null) {
                String uri2 = uri.toString();
                sb.ensureCapacity(uri2.length() + 50);
                sb.append(uri2);
            } else {
                sb.ensureCapacity(50);
                sb.append(nVar.e);
            }
        }
        sb.append('\n');
        if (nVar.n != Utils.FLOAT_EPSILON) {
            sb.append("rotation:");
            sb.append(nVar.n);
            if (nVar.q) {
                sb.append('@');
                sb.append(nVar.o);
                sb.append('x');
                sb.append(nVar.p);
            }
            sb.append('\n');
        }
        if (nVar.c()) {
            sb.append("resize:");
            sb.append(nVar.h);
            sb.append('x');
            sb.append(nVar.i);
            sb.append('\n');
        }
        if (nVar.j) {
            sb.append("centerCrop:");
            sb.append(nVar.k);
            sb.append('\n');
        } else if (nVar.l) {
            sb.append("centerInside");
            sb.append('\n');
        }
        List<ya4> list = nVar.g;
        if (list != null) {
            int size = list.size();
            for (int i = 0; i < size; i++) {
                sb.append(nVar.g.get(i).key());
                sb.append('\n');
            }
        }
        return sb.toString();
    }

    public static void h(Looper looper) {
        a aVar = new a(looper);
        aVar.sendMessageDelayed(aVar.obtainMessage(), 1000L);
    }

    public static int i(Bitmap bitmap) {
        int allocationByteCount = Build.VERSION.SDK_INT >= 19 ? bitmap.getAllocationByteCount() : bitmap.getByteCount();
        if (allocationByteCount >= 0) {
            return allocationByteCount;
        }
        throw new IllegalStateException("Negative size: " + bitmap);
    }

    public static String j(com.squareup.picasso.c cVar) {
        return k(cVar, "");
    }

    public static String k(com.squareup.picasso.c cVar, String str) {
        StringBuilder sb = new StringBuilder(str);
        com.squareup.picasso.a h = cVar.h();
        if (h != null) {
            sb.append(h.b.d());
        }
        List<com.squareup.picasso.a> i = cVar.i();
        if (i != null) {
            int size = i.size();
            for (int i2 = 0; i2 < size; i2++) {
                if (i2 > 0 || h != null) {
                    sb.append(", ");
                }
                sb.append(i.get(i2).b.d());
            }
        }
        return sb.toString();
    }

    public static int l(Resources resources, n nVar) throws FileNotFoundException {
        Uri uri;
        int i = nVar.e;
        if (i != 0 || (uri = nVar.d) == null) {
            return i;
        }
        String authority = uri.getAuthority();
        if (authority != null) {
            List<String> pathSegments = nVar.d.getPathSegments();
            if (pathSegments != null && !pathSegments.isEmpty()) {
                if (pathSegments.size() == 1) {
                    try {
                        return Integer.parseInt(pathSegments.get(0));
                    } catch (NumberFormatException unused) {
                        throw new FileNotFoundException("Last path segment is not a resource ID: " + nVar.d);
                    }
                } else if (pathSegments.size() == 2) {
                    return resources.getIdentifier(pathSegments.get(1), pathSegments.get(0), authority);
                } else {
                    throw new FileNotFoundException("More than two path segments: " + nVar.d);
                }
            }
            throw new FileNotFoundException("No path segments: " + nVar.d);
        }
        throw new FileNotFoundException("No package provided: " + nVar.d);
    }

    public static Resources m(Context context, n nVar) throws FileNotFoundException {
        Uri uri;
        if (nVar.e == 0 && (uri = nVar.d) != null) {
            String authority = uri.getAuthority();
            if (authority != null) {
                try {
                    return context.getPackageManager().getResourcesForApplication(authority);
                } catch (PackageManager.NameNotFoundException unused) {
                    throw new FileNotFoundException("Unable to obtain resources for package: " + nVar.d);
                }
            }
            throw new FileNotFoundException("No package provided: " + nVar.d);
        }
        return context.getResources();
    }

    public static <T> T n(Context context, String str) {
        return (T) context.getSystemService(str);
    }

    public static boolean o(Context context, String str) {
        return context.checkCallingOrSelfPermission(str) == 0;
    }

    public static boolean p(Context context) {
        ContentResolver contentResolver = context.getContentResolver();
        try {
            return Build.VERSION.SDK_INT < 17 ? Settings.System.getInt(contentResolver, "airplane_mode_on", 0) != 0 : Settings.Global.getInt(contentResolver, "airplane_mode_on", 0) != 0;
        } catch (NullPointerException | SecurityException unused) {
            return false;
        }
    }

    public static boolean q() {
        return Looper.getMainLooper().getThread() == Thread.currentThread();
    }

    public static boolean r(okio.d dVar) throws IOException {
        return dVar.G0(0L, b) && dVar.G0(8L, c);
    }

    public static void s(String str, String str2, String str3) {
        t(str, str2, str3, "");
    }

    public static void t(String str, String str2, String str3, String str4) {
        String.format("%1$-11s %2$-12s %3$s %4$s", str, str2, str3, str4);
    }
}
