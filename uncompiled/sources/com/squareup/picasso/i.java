package com.squareup.picasso;

import android.graphics.Bitmap;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.squareup.picasso.Picasso;

/* compiled from: ImageViewAction.java */
/* loaded from: classes2.dex */
public class i extends a<ImageView> {
    public xu m;

    public i(Picasso picasso, ImageView imageView, n nVar, int i, int i2, int i3, Drawable drawable, String str, Object obj, xu xuVar, boolean z) {
        super(picasso, imageView, nVar, i, i2, i3, drawable, str, obj, z);
        this.m = xuVar;
    }

    @Override // com.squareup.picasso.a
    public void a() {
        super.a();
        if (this.m != null) {
            this.m = null;
        }
    }

    @Override // com.squareup.picasso.a
    public void b(Bitmap bitmap, Picasso.LoadedFrom loadedFrom) {
        if (bitmap != null) {
            ImageView imageView = (ImageView) this.c.get();
            if (imageView == null) {
                return;
            }
            Picasso picasso = this.a;
            l.c(imageView, picasso.e, bitmap, loadedFrom, this.d, picasso.m);
            xu xuVar = this.m;
            if (xuVar != null) {
                xuVar.onSuccess();
                return;
            }
            return;
        }
        throw new AssertionError(String.format("Attempted to complete action with no result!\n%s", this));
    }

    @Override // com.squareup.picasso.a
    public void c(Exception exc) {
        ImageView imageView = (ImageView) this.c.get();
        if (imageView == null) {
            return;
        }
        Drawable drawable = imageView.getDrawable();
        if (drawable instanceof Animatable) {
            ((Animatable) drawable).stop();
        }
        int i = this.g;
        if (i != 0) {
            imageView.setImageResource(i);
        } else {
            Drawable drawable2 = this.h;
            if (drawable2 != null) {
                imageView.setImageDrawable(drawable2);
            }
        }
        xu xuVar = this.m;
        if (xuVar != null) {
            xuVar.onError(exc);
        }
    }
}
