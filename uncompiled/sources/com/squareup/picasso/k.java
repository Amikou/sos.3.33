package com.squareup.picasso;

import android.content.Context;
import java.io.File;
import java.io.IOException;
import okhttp3.Cache;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/* compiled from: OkHttp3Downloader.java */
/* loaded from: classes2.dex */
public final class k implements g {
    public final Call.Factory a;

    public k(Context context) {
        this(t.e(context));
    }

    @Override // com.squareup.picasso.g
    public Response a(Request request) throws IOException {
        return this.a.newCall(request).execute();
    }

    public k(File file) {
        this(file, t.a(file));
    }

    public k(File file, long j) {
        this(new OkHttpClient.Builder().cache(new Cache(file, j)).build());
    }

    public k(OkHttpClient okHttpClient) {
        this.a = okHttpClient;
        okHttpClient.cache();
    }
}
