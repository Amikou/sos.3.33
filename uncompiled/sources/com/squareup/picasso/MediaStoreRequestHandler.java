package com.squareup.picasso;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import androidx.recyclerview.widget.RecyclerView;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.p;
import java.io.IOException;
import org.web3j.ens.contracts.generated.PublicResolver;

/* loaded from: classes2.dex */
public class MediaStoreRequestHandler extends e {
    public static final String[] b = {"orientation"};

    /* loaded from: classes2.dex */
    public enum PicassoKind {
        MICRO(3, 96, 96),
        MINI(1, RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN, 384),
        FULL(2, -1, -1);
        
        public final int androidKind;
        public final int height;
        public final int width;

        PicassoKind(int i, int i2, int i3) {
            this.androidKind = i;
            this.width = i2;
            this.height = i3;
        }
    }

    public MediaStoreRequestHandler(Context context) {
        super(context);
    }

    /* JADX WARN: Code restructure failed: missing block: B:12:0x0020, code lost:
        r1.close();
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static int k(android.content.ContentResolver r8, android.net.Uri r9) {
        /*
            r0 = 0
            r1 = 0
            java.lang.String[] r4 = com.squareup.picasso.MediaStoreRequestHandler.b     // Catch: java.lang.Throwable -> L24 java.lang.RuntimeException -> L2b
            r5 = 0
            r6 = 0
            r7 = 0
            r2 = r8
            r3 = r9
            android.database.Cursor r1 = r2.query(r3, r4, r5, r6, r7)     // Catch: java.lang.Throwable -> L24 java.lang.RuntimeException -> L2b
            if (r1 == 0) goto L1e
            boolean r8 = r1.moveToFirst()     // Catch: java.lang.Throwable -> L24 java.lang.RuntimeException -> L2b
            if (r8 != 0) goto L16
            goto L1e
        L16:
            int r8 = r1.getInt(r0)     // Catch: java.lang.Throwable -> L24 java.lang.RuntimeException -> L2b
            r1.close()
            return r8
        L1e:
            if (r1 == 0) goto L23
            r1.close()
        L23:
            return r0
        L24:
            r8 = move-exception
            if (r1 == 0) goto L2a
            r1.close()
        L2a:
            throw r8
        L2b:
            if (r1 == 0) goto L30
            r1.close()
        L30:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.squareup.picasso.MediaStoreRequestHandler.k(android.content.ContentResolver, android.net.Uri):int");
    }

    public static PicassoKind l(int i, int i2) {
        PicassoKind picassoKind = PicassoKind.MICRO;
        if (i > picassoKind.width || i2 > picassoKind.height) {
            PicassoKind picassoKind2 = PicassoKind.MINI;
            return (i > picassoKind2.width || i2 > picassoKind2.height) ? PicassoKind.FULL : picassoKind2;
        }
        return picassoKind;
    }

    @Override // com.squareup.picasso.e, com.squareup.picasso.p
    public boolean c(n nVar) {
        Uri uri = nVar.d;
        return PublicResolver.FUNC_CONTENT.equals(uri.getScheme()) && "media".equals(uri.getAuthority());
    }

    @Override // com.squareup.picasso.e, com.squareup.picasso.p
    public p.a f(n nVar, int i) throws IOException {
        Bitmap thumbnail;
        ContentResolver contentResolver = this.a.getContentResolver();
        int k = k(contentResolver, nVar.d);
        String type = contentResolver.getType(nVar.d);
        boolean z = type != null && type.startsWith("video/");
        if (nVar.c()) {
            PicassoKind l = l(nVar.h, nVar.i);
            if (!z && l == PicassoKind.FULL) {
                return new p.a(null, okio.k.l(j(nVar)), Picasso.LoadedFrom.DISK, k);
            }
            long parseId = ContentUris.parseId(nVar.d);
            BitmapFactory.Options d = p.d(nVar);
            d.inJustDecodeBounds = true;
            p.a(nVar.h, nVar.i, l.width, l.height, d, nVar);
            if (z) {
                thumbnail = MediaStore.Video.Thumbnails.getThumbnail(contentResolver, parseId, l == PicassoKind.FULL ? 1 : l.androidKind, d);
            } else {
                thumbnail = MediaStore.Images.Thumbnails.getThumbnail(contentResolver, parseId, l.androidKind, d);
            }
            if (thumbnail != null) {
                return new p.a(thumbnail, null, Picasso.LoadedFrom.DISK, k);
            }
        }
        return new p.a(null, okio.k.l(j(nVar)), Picasso.LoadedFrom.DISK, k);
    }
}
