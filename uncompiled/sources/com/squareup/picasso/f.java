package com.squareup.picasso;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import com.squareup.picasso.NetworkRequestHandler;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;

/* compiled from: Dispatcher.java */
/* loaded from: classes2.dex */
public class f {
    public final b a;
    public final Context b;
    public final ExecutorService c;
    public final g d;
    public final Map<String, com.squareup.picasso.c> e;
    public final Map<Object, com.squareup.picasso.a> f;
    public final Map<Object, com.squareup.picasso.a> g;
    public final Set<Object> h;
    public final Handler i;
    public final Handler j;
    public final tt k;
    public final kt3 l;
    public final List<com.squareup.picasso.c> m;
    public final c n;
    public final boolean o;
    public boolean p;

    /* compiled from: Dispatcher.java */
    /* loaded from: classes2.dex */
    public static class a extends Handler {
        public final f a;

        /* compiled from: Dispatcher.java */
        /* renamed from: com.squareup.picasso.f$a$a  reason: collision with other inner class name */
        /* loaded from: classes2.dex */
        public class RunnableC0165a implements Runnable {
            public final /* synthetic */ Message a;

            public RunnableC0165a(a aVar, Message message) {
                this.a = message;
            }

            @Override // java.lang.Runnable
            public void run() {
                throw new AssertionError("Unknown handler message received: " + this.a.what);
            }
        }

        public a(Looper looper, f fVar) {
            super(looper);
            this.a = fVar;
        }

        @Override // android.os.Handler
        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    this.a.v((com.squareup.picasso.a) message.obj);
                    return;
                case 2:
                    this.a.o((com.squareup.picasso.a) message.obj);
                    return;
                case 3:
                case 8:
                default:
                    Picasso.p.post(new RunnableC0165a(this, message));
                    return;
                case 4:
                    this.a.p((com.squareup.picasso.c) message.obj);
                    return;
                case 5:
                    this.a.u((com.squareup.picasso.c) message.obj);
                    return;
                case 6:
                    this.a.q((com.squareup.picasso.c) message.obj, false);
                    return;
                case 7:
                    this.a.n();
                    return;
                case 9:
                    this.a.r((NetworkInfo) message.obj);
                    return;
                case 10:
                    this.a.m(message.arg1 == 1);
                    return;
                case 11:
                    this.a.s(message.obj);
                    return;
                case 12:
                    this.a.t(message.obj);
                    return;
            }
        }
    }

    /* compiled from: Dispatcher.java */
    /* loaded from: classes2.dex */
    public static class b extends HandlerThread {
        public b() {
            super("Picasso-Dispatcher", 10);
        }
    }

    /* compiled from: Dispatcher.java */
    /* loaded from: classes2.dex */
    public static class c extends BroadcastReceiver {
        public final f a;

        public c(f fVar) {
            this.a = fVar;
        }

        public void a() {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.AIRPLANE_MODE");
            if (this.a.o) {
                intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
            }
            this.a.b.registerReceiver(this, intentFilter);
        }

        @Override // android.content.BroadcastReceiver
        @SuppressLint({"MissingPermission"})
        public void onReceive(Context context, Intent intent) {
            if (intent == null) {
                return;
            }
            String action = intent.getAction();
            if ("android.intent.action.AIRPLANE_MODE".equals(action)) {
                if (intent.hasExtra("state")) {
                    this.a.b(intent.getBooleanExtra("state", false));
                }
            } else if ("android.net.conn.CONNECTIVITY_CHANGE".equals(action)) {
                this.a.f(((ConnectivityManager) t.n(context, "connectivity")).getActiveNetworkInfo());
            }
        }
    }

    public f(Context context, ExecutorService executorService, Handler handler, g gVar, tt ttVar, kt3 kt3Var) {
        b bVar = new b();
        this.a = bVar;
        bVar.start();
        t.h(bVar.getLooper());
        this.b = context;
        this.c = executorService;
        this.e = new LinkedHashMap();
        this.f = new WeakHashMap();
        this.g = new WeakHashMap();
        this.h = new LinkedHashSet();
        this.i = new a(bVar.getLooper(), this);
        this.d = gVar;
        this.j = handler;
        this.k = ttVar;
        this.l = kt3Var;
        this.m = new ArrayList(4);
        this.p = t.p(context);
        this.o = t.o(context, "android.permission.ACCESS_NETWORK_STATE");
        c cVar = new c(this);
        this.n = cVar;
        cVar.a();
    }

    public final void a(com.squareup.picasso.c cVar) {
        if (cVar.u()) {
            return;
        }
        Bitmap bitmap = cVar.q0;
        if (bitmap != null) {
            bitmap.prepareToDraw();
        }
        this.m.add(cVar);
        if (this.i.hasMessages(7)) {
            return;
        }
        this.i.sendEmptyMessageDelayed(7, 200L);
    }

    public void b(boolean z) {
        Handler handler = this.i;
        handler.sendMessage(handler.obtainMessage(10, z ? 1 : 0, 0));
    }

    public void c(com.squareup.picasso.a aVar) {
        Handler handler = this.i;
        handler.sendMessage(handler.obtainMessage(2, aVar));
    }

    public void d(com.squareup.picasso.c cVar) {
        Handler handler = this.i;
        handler.sendMessage(handler.obtainMessage(4, cVar));
    }

    public void e(com.squareup.picasso.c cVar) {
        Handler handler = this.i;
        handler.sendMessage(handler.obtainMessage(6, cVar));
    }

    public void f(NetworkInfo networkInfo) {
        Handler handler = this.i;
        handler.sendMessage(handler.obtainMessage(9, networkInfo));
    }

    public void g(com.squareup.picasso.c cVar) {
        Handler handler = this.i;
        handler.sendMessageDelayed(handler.obtainMessage(5, cVar), 500L);
    }

    public void h(com.squareup.picasso.a aVar) {
        Handler handler = this.i;
        handler.sendMessage(handler.obtainMessage(1, aVar));
    }

    public final void i() {
        if (this.f.isEmpty()) {
            return;
        }
        Iterator<com.squareup.picasso.a> it = this.f.values().iterator();
        while (it.hasNext()) {
            com.squareup.picasso.a next = it.next();
            it.remove();
            if (next.g().n) {
                t.s("Dispatcher", "replaying", next.i().d());
            }
            w(next, false);
        }
    }

    public final void j(List<com.squareup.picasso.c> list) {
        if (list == null || list.isEmpty() || !list.get(0).q().n) {
            return;
        }
        StringBuilder sb = new StringBuilder();
        for (com.squareup.picasso.c cVar : list) {
            if (sb.length() > 0) {
                sb.append(", ");
            }
            sb.append(t.j(cVar));
        }
        t.s("Dispatcher", "delivered", sb.toString());
    }

    public final void k(com.squareup.picasso.a aVar) {
        Object k = aVar.k();
        if (k != null) {
            aVar.k = true;
            this.f.put(k, aVar);
        }
    }

    public final void l(com.squareup.picasso.c cVar) {
        com.squareup.picasso.a h = cVar.h();
        if (h != null) {
            k(h);
        }
        List<com.squareup.picasso.a> i = cVar.i();
        if (i != null) {
            int size = i.size();
            for (int i2 = 0; i2 < size; i2++) {
                k(i.get(i2));
            }
        }
    }

    public void m(boolean z) {
        this.p = z;
    }

    public void n() {
        ArrayList arrayList = new ArrayList(this.m);
        this.m.clear();
        Handler handler = this.j;
        handler.sendMessage(handler.obtainMessage(8, arrayList));
        j(arrayList);
    }

    public void o(com.squareup.picasso.a aVar) {
        String d = aVar.d();
        com.squareup.picasso.c cVar = this.e.get(d);
        if (cVar != null) {
            cVar.f(aVar);
            if (cVar.c()) {
                this.e.remove(d);
                if (aVar.g().n) {
                    t.s("Dispatcher", "canceled", aVar.i().d());
                }
            }
        }
        if (this.h.contains(aVar.j())) {
            this.g.remove(aVar.k());
            if (aVar.g().n) {
                t.t("Dispatcher", "canceled", aVar.i().d(), "because paused request got canceled");
            }
        }
        com.squareup.picasso.a remove = this.f.remove(aVar.k());
        if (remove == null || !remove.g().n) {
            return;
        }
        t.t("Dispatcher", "canceled", remove.i().d(), "from replaying");
    }

    public void p(com.squareup.picasso.c cVar) {
        if (MemoryPolicy.shouldWriteToMemoryCache(cVar.p())) {
            this.k.b(cVar.n(), cVar.s());
        }
        this.e.remove(cVar.n());
        a(cVar);
        if (cVar.q().n) {
            t.t("Dispatcher", "batched", t.j(cVar), "for completion");
        }
    }

    public void q(com.squareup.picasso.c cVar, boolean z) {
        if (cVar.q().n) {
            String j = t.j(cVar);
            StringBuilder sb = new StringBuilder();
            sb.append("for error");
            sb.append(z ? " (will replay)" : "");
            t.t("Dispatcher", "batched", j, sb.toString());
        }
        this.e.remove(cVar.n());
        a(cVar);
    }

    public void r(NetworkInfo networkInfo) {
        ExecutorService executorService = this.c;
        if (executorService instanceof m) {
            ((m) executorService).a(networkInfo);
        }
        if (networkInfo == null || !networkInfo.isConnected()) {
            return;
        }
        i();
    }

    public void s(Object obj) {
        if (this.h.add(obj)) {
            Iterator<com.squareup.picasso.c> it = this.e.values().iterator();
            while (it.hasNext()) {
                com.squareup.picasso.c next = it.next();
                boolean z = next.q().n;
                com.squareup.picasso.a h = next.h();
                List<com.squareup.picasso.a> i = next.i();
                boolean z2 = (i == null || i.isEmpty()) ? false : true;
                if (h != null || z2) {
                    if (h != null && h.j().equals(obj)) {
                        next.f(h);
                        this.g.put(h.k(), h);
                        if (z) {
                            t.t("Dispatcher", "paused", h.b.d(), "because tag '" + obj + "' was paused");
                        }
                    }
                    if (z2) {
                        for (int size = i.size() - 1; size >= 0; size--) {
                            com.squareup.picasso.a aVar = i.get(size);
                            if (aVar.j().equals(obj)) {
                                next.f(aVar);
                                this.g.put(aVar.k(), aVar);
                                if (z) {
                                    t.t("Dispatcher", "paused", aVar.b.d(), "because tag '" + obj + "' was paused");
                                }
                            }
                        }
                    }
                    if (next.c()) {
                        it.remove();
                        if (z) {
                            t.t("Dispatcher", "canceled", t.j(next), "all actions paused");
                        }
                    }
                }
            }
        }
    }

    public void t(Object obj) {
        if (this.h.remove(obj)) {
            ArrayList arrayList = null;
            Iterator<com.squareup.picasso.a> it = this.g.values().iterator();
            while (it.hasNext()) {
                com.squareup.picasso.a next = it.next();
                if (next.j().equals(obj)) {
                    if (arrayList == null) {
                        arrayList = new ArrayList();
                    }
                    arrayList.add(next);
                    it.remove();
                }
            }
            if (arrayList != null) {
                Handler handler = this.j;
                handler.sendMessage(handler.obtainMessage(13, arrayList));
            }
        }
    }

    @SuppressLint({"MissingPermission"})
    public void u(com.squareup.picasso.c cVar) {
        if (cVar.u()) {
            return;
        }
        boolean z = false;
        if (this.c.isShutdown()) {
            q(cVar, false);
            return;
        }
        if (cVar.w(this.p, this.o ? ((ConnectivityManager) t.n(this.b, "connectivity")).getActiveNetworkInfo() : null)) {
            if (cVar.q().n) {
                t.s("Dispatcher", "retrying", t.j(cVar));
            }
            if (cVar.k() instanceof NetworkRequestHandler.ContentLengthException) {
                cVar.m0 |= NetworkPolicy.NO_CACHE.index;
            }
            cVar.r0 = this.c.submit(cVar);
            return;
        }
        if (this.o && cVar.x()) {
            z = true;
        }
        q(cVar, z);
        if (z) {
            l(cVar);
        }
    }

    public void v(com.squareup.picasso.a aVar) {
        w(aVar, true);
    }

    public void w(com.squareup.picasso.a aVar, boolean z) {
        if (this.h.contains(aVar.j())) {
            this.g.put(aVar.k(), aVar);
            if (aVar.g().n) {
                String d = aVar.b.d();
                t.t("Dispatcher", "paused", d, "because tag '" + aVar.j() + "' is paused");
                return;
            }
            return;
        }
        com.squareup.picasso.c cVar = this.e.get(aVar.d());
        if (cVar != null) {
            cVar.b(aVar);
        } else if (this.c.isShutdown()) {
            if (aVar.g().n) {
                t.t("Dispatcher", "ignored", aVar.b.d(), "because shut down");
            }
        } else {
            com.squareup.picasso.c g = com.squareup.picasso.c.g(aVar.g(), this, this.k, this.l, aVar);
            g.r0 = this.c.submit(g);
            this.e.put(aVar.d(), g);
            if (z) {
                this.f.remove(aVar.k());
            }
            if (aVar.g().n) {
                t.s("Dispatcher", "enqueued", aVar.b.d());
            }
        }
    }
}
