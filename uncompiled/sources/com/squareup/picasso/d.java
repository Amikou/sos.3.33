package com.squareup.picasso;

import android.content.ContentResolver;
import android.content.Context;
import android.content.UriMatcher;
import android.net.Uri;
import android.provider.ContactsContract;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.p;
import java.io.IOException;
import java.io.InputStream;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: ContactsPhotoRequestHandler.java */
/* loaded from: classes2.dex */
public class d extends p {
    public static final UriMatcher b;
    public final Context a;

    static {
        UriMatcher uriMatcher = new UriMatcher(-1);
        b = uriMatcher;
        uriMatcher.addURI("com.android.contacts", "contacts/lookup/*/#", 1);
        uriMatcher.addURI("com.android.contacts", "contacts/lookup/*", 1);
        uriMatcher.addURI("com.android.contacts", "contacts/#/photo", 2);
        uriMatcher.addURI("com.android.contacts", "contacts/#", 3);
        uriMatcher.addURI("com.android.contacts", "display_photo/#", 4);
    }

    public d(Context context) {
        this.a = context;
    }

    @Override // com.squareup.picasso.p
    public boolean c(n nVar) {
        Uri uri = nVar.d;
        return PublicResolver.FUNC_CONTENT.equals(uri.getScheme()) && ContactsContract.Contacts.CONTENT_URI.getHost().equals(uri.getHost()) && b.match(nVar.d) != -1;
    }

    @Override // com.squareup.picasso.p
    public p.a f(n nVar, int i) throws IOException {
        InputStream j = j(nVar);
        if (j == null) {
            return null;
        }
        return new p.a(okio.k.l(j), Picasso.LoadedFrom.DISK);
    }

    public final InputStream j(n nVar) throws IOException {
        ContentResolver contentResolver = this.a.getContentResolver();
        Uri uri = nVar.d;
        int match = b.match(uri);
        if (match != 1) {
            if (match != 2) {
                if (match != 3) {
                    if (match != 4) {
                        throw new IllegalStateException("Invalid uri: " + uri);
                    }
                }
            }
            return contentResolver.openInputStream(uri);
        }
        uri = ContactsContract.Contacts.lookupContact(contentResolver, uri);
        if (uri == null) {
            return null;
        }
        return ContactsContract.Contacts.openContactPhotoInputStream(contentResolver, uri, true);
    }
}
