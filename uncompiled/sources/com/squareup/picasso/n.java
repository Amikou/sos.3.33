package com.squareup.picasso;

import android.graphics.Bitmap;
import android.net.Uri;
import com.github.mikephil.charting.utils.Utils;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

/* compiled from: Request.java */
/* loaded from: classes2.dex */
public final class n {
    public static final long u = TimeUnit.SECONDS.toNanos(5);
    public int a;
    public long b;
    public int c;
    public final Uri d;
    public final int e;
    public final String f;
    public final List<ya4> g;
    public final int h;
    public final int i;
    public final boolean j;
    public final int k;
    public final boolean l;
    public final boolean m;
    public final float n;
    public final float o;
    public final float p;
    public final boolean q;
    public final boolean r;
    public final Bitmap.Config s;
    public final Picasso.Priority t;

    /* compiled from: Request.java */
    /* loaded from: classes2.dex */
    public static final class b {
        public Uri a;
        public int b;
        public String c;
        public int d;
        public int e;
        public boolean f;
        public int g;
        public boolean h;
        public boolean i;
        public float j;
        public float k;
        public float l;
        public boolean m;
        public boolean n;
        public List<ya4> o;
        public Bitmap.Config p;
        public Picasso.Priority q;

        public b(Uri uri, int i, Bitmap.Config config) {
            this.a = uri;
            this.b = i;
            this.p = config;
        }

        public n a() {
            boolean z = this.h;
            if (z && this.f) {
                throw new IllegalStateException("Center crop and center inside can not be used together.");
            }
            if (this.f && this.d == 0 && this.e == 0) {
                throw new IllegalStateException("Center crop requires calling resize with positive width and height.");
            }
            if (z && this.d == 0 && this.e == 0) {
                throw new IllegalStateException("Center inside requires calling resize with positive width and height.");
            }
            if (this.q == null) {
                this.q = Picasso.Priority.NORMAL;
            }
            return new n(this.a, this.b, this.c, this.o, this.d, this.e, this.f, this.h, this.g, this.i, this.j, this.k, this.l, this.m, this.n, this.p, this.q);
        }

        public b b(int i) {
            if (!this.h) {
                this.f = true;
                this.g = i;
                return this;
            }
            throw new IllegalStateException("Center crop can not be used after calling centerInside");
        }

        public boolean c() {
            return (this.a == null && this.b == 0) ? false : true;
        }

        public boolean d() {
            return (this.d == 0 && this.e == 0) ? false : true;
        }

        public b e(int i, int i2) {
            if (i >= 0) {
                if (i2 >= 0) {
                    if (i2 == 0 && i == 0) {
                        throw new IllegalArgumentException("At least one dimension has to be positive number.");
                    }
                    this.d = i;
                    this.e = i2;
                    return this;
                }
                throw new IllegalArgumentException("Height must be positive number or 0.");
            }
            throw new IllegalArgumentException("Width must be positive number or 0.");
        }

        public b f(ya4 ya4Var) {
            if (ya4Var != null) {
                if (ya4Var.key() != null) {
                    if (this.o == null) {
                        this.o = new ArrayList(2);
                    }
                    this.o.add(ya4Var);
                    return this;
                }
                throw new IllegalArgumentException("Transformation key must not be null.");
            }
            throw new IllegalArgumentException("Transformation must not be null.");
        }
    }

    public String a() {
        Uri uri = this.d;
        if (uri != null) {
            return String.valueOf(uri.getPath());
        }
        return Integer.toHexString(this.e);
    }

    public boolean b() {
        return this.g != null;
    }

    public boolean c() {
        return (this.h == 0 && this.i == 0) ? false : true;
    }

    public String d() {
        long nanoTime = System.nanoTime() - this.b;
        if (nanoTime > u) {
            return g() + '+' + TimeUnit.NANOSECONDS.toSeconds(nanoTime) + 's';
        }
        return g() + '+' + TimeUnit.NANOSECONDS.toMillis(nanoTime) + "ms";
    }

    public boolean e() {
        return c() || this.n != Utils.FLOAT_EPSILON;
    }

    public boolean f() {
        return e() || b();
    }

    public String g() {
        return "[R" + this.a + ']';
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Request{");
        int i = this.e;
        if (i > 0) {
            sb.append(i);
        } else {
            sb.append(this.d);
        }
        List<ya4> list = this.g;
        if (list != null && !list.isEmpty()) {
            for (ya4 ya4Var : this.g) {
                sb.append(' ');
                sb.append(ya4Var.key());
            }
        }
        if (this.f != null) {
            sb.append(" stableKey(");
            sb.append(this.f);
            sb.append(')');
        }
        if (this.h > 0) {
            sb.append(" resize(");
            sb.append(this.h);
            sb.append(',');
            sb.append(this.i);
            sb.append(')');
        }
        if (this.j) {
            sb.append(" centerCrop");
        }
        if (this.l) {
            sb.append(" centerInside");
        }
        if (this.n != Utils.FLOAT_EPSILON) {
            sb.append(" rotation(");
            sb.append(this.n);
            if (this.q) {
                sb.append(" @ ");
                sb.append(this.o);
                sb.append(',');
                sb.append(this.p);
            }
            sb.append(')');
        }
        if (this.r) {
            sb.append(" purgeable");
        }
        if (this.s != null) {
            sb.append(' ');
            sb.append(this.s);
        }
        sb.append('}');
        return sb.toString();
    }

    public n(Uri uri, int i, String str, List<ya4> list, int i2, int i3, boolean z, boolean z2, int i4, boolean z3, float f, float f2, float f3, boolean z4, boolean z5, Bitmap.Config config, Picasso.Priority priority) {
        this.d = uri;
        this.e = i;
        this.f = str;
        if (list == null) {
            this.g = null;
        } else {
            this.g = Collections.unmodifiableList(list);
        }
        this.h = i2;
        this.i = i3;
        this.j = z;
        this.l = z2;
        this.k = i4;
        this.m = z3;
        this.n = f;
        this.o = f2;
        this.p = f3;
        this.q = z4;
        this.r = z5;
        this.s = config;
        this.t = priority;
    }
}
