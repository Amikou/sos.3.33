package com.squareup.picasso;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.util.TypedValue;
import android.widget.ImageView;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.n;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: RequestCreator.java */
/* loaded from: classes2.dex */
public class o {
    public static final AtomicInteger m = new AtomicInteger();
    public final Picasso a;
    public final n.b b;
    public boolean c;
    public boolean d;
    public boolean e = true;
    public int f;
    public int g;
    public int h;
    public int i;
    public Drawable j;
    public Drawable k;
    public Object l;

    public o(Picasso picasso, Uri uri, int i) {
        if (!picasso.o) {
            this.a = picasso;
            this.b = new n.b(uri, i, picasso.l);
            return;
        }
        throw new IllegalStateException("Picasso instance already shut down. Cannot submit new requests.");
    }

    public o a() {
        this.b.b(17);
        return this;
    }

    public o b() {
        this.l = null;
        return this;
    }

    public final n c(long j) {
        int andIncrement = m.getAndIncrement();
        n a = this.b.a();
        a.a = andIncrement;
        a.b = j;
        boolean z = this.a.n;
        if (z) {
            t.t("Main", "created", a.g(), a.toString());
        }
        n p = this.a.p(a);
        if (p != a) {
            p.a = andIncrement;
            p.b = j;
            if (z) {
                String d = p.d();
                t.t("Main", "changed", d, "into " + p);
            }
        }
        return p;
    }

    public o d() {
        this.d = true;
        return this;
    }

    public final Drawable e() {
        int i = this.f;
        if (i != 0) {
            int i2 = Build.VERSION.SDK_INT;
            if (i2 >= 21) {
                return this.a.e.getDrawable(i);
            }
            if (i2 >= 16) {
                return this.a.e.getResources().getDrawable(this.f);
            }
            TypedValue typedValue = new TypedValue();
            this.a.e.getResources().getValue(this.f, typedValue, true);
            return this.a.e.getResources().getDrawable(typedValue.resourceId);
        }
        return this.j;
    }

    public void f(ImageView imageView) {
        g(imageView, null);
    }

    public void g(ImageView imageView, xu xuVar) {
        Bitmap m2;
        long nanoTime = System.nanoTime();
        t.c();
        if (imageView != null) {
            if (!this.b.c()) {
                this.a.b(imageView);
                if (this.e) {
                    l.d(imageView, e());
                    return;
                }
                return;
            }
            if (this.d) {
                if (!this.b.d()) {
                    int width = imageView.getWidth();
                    int height = imageView.getHeight();
                    if (width != 0 && height != 0) {
                        this.b.e(width, height);
                    } else {
                        if (this.e) {
                            l.d(imageView, e());
                        }
                        this.a.e(imageView, new vl0(this, imageView, xuVar));
                        return;
                    }
                } else {
                    throw new IllegalStateException("Fit cannot be used with resize.");
                }
            }
            n c = c(nanoTime);
            String f = t.f(c);
            if (MemoryPolicy.shouldReadFromMemoryCache(this.h) && (m2 = this.a.m(f)) != null) {
                this.a.b(imageView);
                Picasso picasso = this.a;
                Context context = picasso.e;
                Picasso.LoadedFrom loadedFrom = Picasso.LoadedFrom.MEMORY;
                l.c(imageView, context, m2, loadedFrom, this.c, picasso.m);
                if (this.a.n) {
                    String g = c.g();
                    t.t("Main", "completed", g, "from " + loadedFrom);
                }
                if (xuVar != null) {
                    xuVar.onSuccess();
                    return;
                }
                return;
            }
            if (this.e) {
                l.d(imageView, e());
            }
            this.a.g(new i(this.a, imageView, c, this.h, this.i, this.g, this.k, f, this.l, xuVar, this.c));
            return;
        }
        throw new IllegalArgumentException("Target must not be null.");
    }

    public void h(r rVar) {
        Bitmap m2;
        long nanoTime = System.nanoTime();
        t.c();
        if (rVar != null) {
            if (!this.d) {
                if (!this.b.c()) {
                    this.a.c(rVar);
                    rVar.onPrepareLoad(this.e ? e() : null);
                    return;
                }
                n c = c(nanoTime);
                String f = t.f(c);
                if (MemoryPolicy.shouldReadFromMemoryCache(this.h) && (m2 = this.a.m(f)) != null) {
                    this.a.c(rVar);
                    rVar.onBitmapLoaded(m2, Picasso.LoadedFrom.MEMORY);
                    return;
                }
                rVar.onPrepareLoad(this.e ? e() : null);
                this.a.g(new s(this.a, rVar, c, this.h, this.i, this.k, f, this.l, this.g));
                return;
            }
            throw new IllegalStateException("Fit cannot be used with a Target.");
        }
        throw new IllegalArgumentException("Target must not be null.");
    }

    public o i() {
        this.c = true;
        return this;
    }

    public o j() {
        if (this.f == 0) {
            if (this.j == null) {
                this.e = false;
                return this;
            }
            throw new IllegalStateException("Placeholder image already set.");
        }
        throw new IllegalStateException("Placeholder resource already set.");
    }

    public o k(int i, int i2) {
        this.b.e(i, i2);
        return this;
    }

    public o l(ya4 ya4Var) {
        this.b.f(ya4Var);
        return this;
    }

    public o m() {
        this.d = false;
        return this;
    }
}
