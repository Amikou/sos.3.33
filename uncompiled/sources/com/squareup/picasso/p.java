package com.squareup.picasso;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.NetworkInfo;
import com.squareup.picasso.Picasso;
import java.io.IOException;

/* compiled from: RequestHandler.java */
/* loaded from: classes2.dex */
public abstract class p {

    /* compiled from: RequestHandler.java */
    /* loaded from: classes2.dex */
    public static final class a {
        public final Picasso.LoadedFrom a;
        public final Bitmap b;
        public final okio.n c;
        public final int d;

        public a(Bitmap bitmap, Picasso.LoadedFrom loadedFrom) {
            this((Bitmap) t.d(bitmap, "bitmap == null"), null, loadedFrom, 0);
        }

        public Bitmap a() {
            return this.b;
        }

        public int b() {
            return this.d;
        }

        public Picasso.LoadedFrom c() {
            return this.a;
        }

        public okio.n d() {
            return this.c;
        }

        public a(okio.n nVar, Picasso.LoadedFrom loadedFrom) {
            this(null, (okio.n) t.d(nVar, "source == null"), loadedFrom, 0);
        }

        public a(Bitmap bitmap, okio.n nVar, Picasso.LoadedFrom loadedFrom, int i) {
            if ((bitmap != null) != (nVar != null)) {
                this.b = bitmap;
                this.c = nVar;
                this.a = (Picasso.LoadedFrom) t.d(loadedFrom, "loadedFrom == null");
                this.d = i;
                return;
            }
            throw new AssertionError();
        }
    }

    public static void a(int i, int i2, int i3, int i4, BitmapFactory.Options options, n nVar) {
        int min;
        double floor;
        if (i4 > i2 || i3 > i) {
            if (i2 == 0) {
                floor = Math.floor(i3 / i);
            } else if (i == 0) {
                floor = Math.floor(i4 / i2);
            } else {
                int floor2 = (int) Math.floor(i4 / i2);
                int floor3 = (int) Math.floor(i3 / i);
                if (nVar.l) {
                    min = Math.max(floor2, floor3);
                } else {
                    min = Math.min(floor2, floor3);
                }
            }
            min = (int) floor;
        } else {
            min = 1;
        }
        options.inSampleSize = min;
        options.inJustDecodeBounds = false;
    }

    public static void b(int i, int i2, BitmapFactory.Options options, n nVar) {
        a(i, i2, options.outWidth, options.outHeight, options, nVar);
    }

    public static BitmapFactory.Options d(n nVar) {
        boolean c = nVar.c();
        boolean z = nVar.s != null;
        BitmapFactory.Options options = null;
        if (c || z || nVar.r) {
            options = new BitmapFactory.Options();
            options.inJustDecodeBounds = c;
            boolean z2 = nVar.r;
            options.inInputShareable = z2;
            options.inPurgeable = z2;
            if (z) {
                options.inPreferredConfig = nVar.s;
            }
        }
        return options;
    }

    public static boolean g(BitmapFactory.Options options) {
        return options != null && options.inJustDecodeBounds;
    }

    public abstract boolean c(n nVar);

    public int e() {
        return 0;
    }

    public abstract a f(n nVar, int i) throws IOException;

    public boolean h(boolean z, NetworkInfo networkInfo) {
        return false;
    }

    public boolean i() {
        return false;
    }
}
