package com.squareup.picasso;

import android.content.Context;
import android.net.Uri;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.p;
import java.io.IOException;

/* compiled from: FileRequestHandler.java */
/* loaded from: classes2.dex */
public class h extends e {
    public h(Context context) {
        super(context);
    }

    public static int k(Uri uri) throws IOException {
        return new az0(uri.getPath()).f("Orientation", 1);
    }

    @Override // com.squareup.picasso.e, com.squareup.picasso.p
    public boolean c(n nVar) {
        return "file".equals(nVar.d.getScheme());
    }

    @Override // com.squareup.picasso.e, com.squareup.picasso.p
    public p.a f(n nVar, int i) throws IOException {
        return new p.a(null, okio.k.l(j(nVar)), Picasso.LoadedFrom.DISK, k(nVar.d));
    }
}
