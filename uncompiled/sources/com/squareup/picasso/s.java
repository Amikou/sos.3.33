package com.squareup.picasso;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import com.squareup.picasso.Picasso;

/* compiled from: TargetAction.java */
/* loaded from: classes2.dex */
public final class s extends a<r> {
    public s(Picasso picasso, r rVar, n nVar, int i, int i2, Drawable drawable, String str, Object obj, int i3) {
        super(picasso, rVar, nVar, i, i2, i3, drawable, str, obj, false);
    }

    @Override // com.squareup.picasso.a
    public void b(Bitmap bitmap, Picasso.LoadedFrom loadedFrom) {
        if (bitmap != null) {
            r k = k();
            if (k != null) {
                k.onBitmapLoaded(bitmap, loadedFrom);
                if (bitmap.isRecycled()) {
                    throw new IllegalStateException("Target callback must not recycle bitmap!");
                }
                return;
            }
            return;
        }
        throw new AssertionError(String.format("Attempted to complete action with no result!\n%s", this));
    }

    @Override // com.squareup.picasso.a
    public void c(Exception exc) {
        r k = k();
        if (k != null) {
            if (this.g != 0) {
                k.onBitmapFailed(exc, this.a.e.getResources().getDrawable(this.g));
            } else {
                k.onBitmapFailed(exc, this.h);
            }
        }
    }
}
