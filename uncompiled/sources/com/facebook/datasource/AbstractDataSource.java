package com.facebook.datasource;

import android.util.Pair;
import com.github.mikephil.charting.utils.Utils;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executor;

/* loaded from: classes.dex */
public abstract class AbstractDataSource<T> implements ge0<T> {
    public static volatile c h;
    public Map<String, Object> a;
    public T d = null;
    public Throwable e = null;
    public float f = Utils.FLOAT_EPSILON;
    public boolean c = false;
    public DataSourceStatus b = DataSourceStatus.IN_PROGRESS;
    public final ConcurrentLinkedQueue<Pair<ke0<T>, Executor>> g = new ConcurrentLinkedQueue<>();

    /* loaded from: classes.dex */
    public enum DataSourceStatus {
        IN_PROGRESS,
        SUCCESS,
        FAILURE
    }

    /* loaded from: classes.dex */
    public class a implements Runnable {
        public final /* synthetic */ boolean a;
        public final /* synthetic */ ke0 f0;
        public final /* synthetic */ boolean g0;

        public a(boolean z, ke0 ke0Var, boolean z2) {
            this.a = z;
            this.f0 = ke0Var;
            this.g0 = z2;
        }

        @Override // java.lang.Runnable
        public void run() {
            if (this.a) {
                this.f0.b(AbstractDataSource.this);
            } else if (this.g0) {
                this.f0.c(AbstractDataSource.this);
            } else {
                this.f0.d(AbstractDataSource.this);
            }
        }
    }

    /* loaded from: classes.dex */
    public class b implements Runnable {
        public final /* synthetic */ ke0 a;

        public b(ke0 ke0Var) {
            this.a = ke0Var;
        }

        @Override // java.lang.Runnable
        public void run() {
            this.a.a(AbstractDataSource.this);
        }
    }

    /* loaded from: classes.dex */
    public interface c {
        Runnable a(Runnable runnable, String str);
    }

    public static c i() {
        return h;
    }

    @Override // defpackage.ge0
    public synchronized boolean a() {
        return this.d != null;
    }

    @Override // defpackage.ge0
    public synchronized boolean b() {
        return this.b != DataSourceStatus.IN_PROGRESS;
    }

    @Override // defpackage.ge0
    public synchronized Throwable c() {
        return this.e;
    }

    @Override // defpackage.ge0
    public boolean close() {
        synchronized (this) {
            if (this.c) {
                return false;
            }
            this.c = true;
            T t = this.d;
            this.d = null;
            if (t != null) {
                h(t);
            }
            if (!b()) {
                m();
            }
            synchronized (this) {
                this.g.clear();
            }
            return true;
        }
    }

    @Override // defpackage.ge0
    public synchronized float d() {
        return this.f;
    }

    /* JADX WARN: Removed duplicated region for block: B:22:0x0035  */
    /* JADX WARN: Removed duplicated region for block: B:28:? A[RETURN, SYNTHETIC] */
    @Override // defpackage.ge0
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void e(defpackage.ke0<T> r3, java.util.concurrent.Executor r4) {
        /*
            r2 = this;
            defpackage.xt2.g(r3)
            defpackage.xt2.g(r4)
            monitor-enter(r2)
            boolean r0 = r2.c     // Catch: java.lang.Throwable -> L41
            if (r0 == 0) goto Ld
            monitor-exit(r2)     // Catch: java.lang.Throwable -> L41
            return
        Ld:
            com.facebook.datasource.AbstractDataSource$DataSourceStatus r0 = r2.b     // Catch: java.lang.Throwable -> L41
            com.facebook.datasource.AbstractDataSource$DataSourceStatus r1 = com.facebook.datasource.AbstractDataSource.DataSourceStatus.IN_PROGRESS     // Catch: java.lang.Throwable -> L41
            if (r0 != r1) goto L1c
            java.util.concurrent.ConcurrentLinkedQueue<android.util.Pair<ke0<T>, java.util.concurrent.Executor>> r0 = r2.g     // Catch: java.lang.Throwable -> L41
            android.util.Pair r1 = android.util.Pair.create(r3, r4)     // Catch: java.lang.Throwable -> L41
            r0.add(r1)     // Catch: java.lang.Throwable -> L41
        L1c:
            boolean r0 = r2.a()     // Catch: java.lang.Throwable -> L41
            if (r0 != 0) goto L31
            boolean r0 = r2.b()     // Catch: java.lang.Throwable -> L41
            if (r0 != 0) goto L31
            boolean r0 = r2.w()     // Catch: java.lang.Throwable -> L41
            if (r0 == 0) goto L2f
            goto L31
        L2f:
            r0 = 0
            goto L32
        L31:
            r0 = 1
        L32:
            monitor-exit(r2)     // Catch: java.lang.Throwable -> L41
            if (r0 == 0) goto L40
            boolean r0 = r2.j()
            boolean r1 = r2.w()
            r2.l(r3, r4, r0, r1)
        L40:
            return
        L41:
            r3 = move-exception
            monitor-exit(r2)     // Catch: java.lang.Throwable -> L41
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.datasource.AbstractDataSource.e(ke0, java.util.concurrent.Executor):void");
    }

    @Override // defpackage.ge0
    public boolean f() {
        return false;
    }

    @Override // defpackage.ge0
    public synchronized T g() {
        return this.d;
    }

    @Override // defpackage.ge0
    public Map<String, Object> getExtras() {
        return this.a;
    }

    public void h(T t) {
    }

    public synchronized boolean j() {
        return this.b == DataSourceStatus.FAILURE;
    }

    public synchronized boolean k() {
        return this.c;
    }

    public void l(ke0<T> ke0Var, Executor executor, boolean z, boolean z2) {
        Runnable aVar = new a(z, ke0Var, z2);
        c i = i();
        if (i != null) {
            aVar = i.a(aVar, "AbstractDataSource_notifyDataSubscriber");
        }
        executor.execute(aVar);
    }

    public final void m() {
        boolean j = j();
        boolean w = w();
        Iterator<Pair<ke0<T>, Executor>> it = this.g.iterator();
        while (it.hasNext()) {
            Pair<ke0<T>, Executor> next = it.next();
            l((ke0) next.first, (Executor) next.second, j, w);
        }
    }

    public void n() {
        Iterator<Pair<ke0<T>, Executor>> it = this.g.iterator();
        while (it.hasNext()) {
            Pair<ke0<T>, Executor> next = it.next();
            ((Executor) next.second).execute(new b((ke0) next.first));
        }
    }

    public void o(Map<String, Object> map) {
        this.a = map;
    }

    public boolean p(Throwable th) {
        return q(th, null);
    }

    public boolean q(Throwable th, Map<String, Object> map) {
        boolean r = r(th, map);
        if (r) {
            m();
        }
        return r;
    }

    public final synchronized boolean r(Throwable th, Map<String, Object> map) {
        if (!this.c && this.b == DataSourceStatus.IN_PROGRESS) {
            this.b = DataSourceStatus.FAILURE;
            this.e = th;
            this.a = map;
            return true;
        }
        return false;
    }

    public boolean s(float f) {
        boolean t = t(f);
        if (t) {
            n();
        }
        return t;
    }

    public final synchronized boolean t(float f) {
        if (!this.c && this.b == DataSourceStatus.IN_PROGRESS) {
            if (f < this.f) {
                return false;
            }
            this.f = f;
            return true;
        }
        return false;
    }

    public boolean u(T t, boolean z, Map<String, Object> map) {
        o(map);
        boolean v = v(t, z);
        if (v) {
            m();
        }
        return v;
    }

    public final boolean v(T t, boolean z) {
        T t2;
        T t3 = null;
        try {
            synchronized (this) {
                try {
                    try {
                        if (!this.c && this.b == DataSourceStatus.IN_PROGRESS) {
                            if (z) {
                                this.b = DataSourceStatus.SUCCESS;
                                this.f = 1.0f;
                            }
                            T t4 = this.d;
                            if (t4 != t) {
                                try {
                                    this.d = t;
                                    t2 = t4;
                                } catch (Throwable th) {
                                    th = th;
                                    t3 = t4;
                                    throw th;
                                }
                            } else {
                                t2 = null;
                            }
                            return true;
                        }
                        if (t != null) {
                            h(t);
                        }
                        return false;
                    } catch (Throwable th2) {
                        t3 = t;
                        th = th2;
                    }
                } catch (Throwable th3) {
                    th = th3;
                }
            }
        } finally {
            if (t3 != null) {
                h(t3);
            }
        }
    }

    public final synchronized boolean w() {
        boolean z;
        if (k()) {
            z = b() ? false : true;
        }
        return z;
    }
}
