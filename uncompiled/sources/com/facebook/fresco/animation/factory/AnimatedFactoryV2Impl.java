package com.facebook.fresco.animation.factory;

import android.content.Context;
import android.graphics.Rect;
import com.facebook.common.time.RealtimeSinceBootClock;
import java.util.concurrent.ExecutorService;

@cq0
/* loaded from: classes.dex */
public class AnimatedFactoryV2Impl implements qd {
    public final br2 a;
    public final wy0 b;
    public final i90<wt, com.facebook.imagepipeline.image.a> c;
    public final boolean d;
    public vd e;
    public od f;
    public pd g;
    public uq0 h;
    public xl3 i;

    /* loaded from: classes.dex */
    public class a implements tn1 {
        public a() {
        }

        @Override // defpackage.tn1
        public com.facebook.imagepipeline.image.a a(zu0 zu0Var, int i, xw2 xw2Var, rn1 rn1Var) {
            return AnimatedFactoryV2Impl.this.k().b(zu0Var, rn1Var, rn1Var.h);
        }
    }

    /* loaded from: classes.dex */
    public class b implements tn1 {
        public b() {
        }

        @Override // defpackage.tn1
        public com.facebook.imagepipeline.image.a a(zu0 zu0Var, int i, xw2 xw2Var, rn1 rn1Var) {
            return AnimatedFactoryV2Impl.this.k().a(zu0Var, rn1Var, rn1Var.h);
        }
    }

    /* loaded from: classes.dex */
    public class c implements fw3<Integer> {
        public c(AnimatedFactoryV2Impl animatedFactoryV2Impl) {
        }

        @Override // defpackage.fw3
        /* renamed from: a */
        public Integer get() {
            return 2;
        }
    }

    /* loaded from: classes.dex */
    public class d implements fw3<Integer> {
        public d(AnimatedFactoryV2Impl animatedFactoryV2Impl) {
        }

        @Override // defpackage.fw3
        /* renamed from: a */
        public Integer get() {
            return 3;
        }
    }

    /* loaded from: classes.dex */
    public class e implements od {
        public e() {
        }

        @Override // defpackage.od
        public kd a(yd ydVar, Rect rect) {
            return new nd(AnimatedFactoryV2Impl.this.j(), ydVar, rect, AnimatedFactoryV2Impl.this.d);
        }
    }

    /* loaded from: classes.dex */
    public class f implements od {
        public f() {
        }

        @Override // defpackage.od
        public kd a(yd ydVar, Rect rect) {
            return new nd(AnimatedFactoryV2Impl.this.j(), ydVar, rect, AnimatedFactoryV2Impl.this.d);
        }
    }

    @cq0
    public AnimatedFactoryV2Impl(br2 br2Var, wy0 wy0Var, i90<wt, com.facebook.imagepipeline.image.a> i90Var, boolean z, xl3 xl3Var) {
        this.a = br2Var;
        this.b = wy0Var;
        this.c = i90Var;
        this.d = z;
        this.i = xl3Var;
    }

    @Override // defpackage.qd
    public uq0 a(Context context) {
        if (this.h == null) {
            this.h = h();
        }
        return this.h;
    }

    @Override // defpackage.qd
    public tn1 b() {
        return new a();
    }

    @Override // defpackage.qd
    public tn1 c() {
        return new b();
    }

    public final vd g() {
        return new wd(new f(), this.a);
    }

    public final y01 h() {
        c cVar = new c(this);
        ExecutorService executorService = this.i;
        if (executorService == null) {
            executorService = new uk0(this.b.a());
        }
        d dVar = new d(this);
        fw3<Boolean> fw3Var = gw3.a;
        return new y01(i(), me4.g(), executorService, RealtimeSinceBootClock.get(), this.a, this.c, cVar, dVar, fw3Var);
    }

    public final od i() {
        if (this.f == null) {
            this.f = new e();
        }
        return this.f;
    }

    public final pd j() {
        if (this.g == null) {
            this.g = new pd();
        }
        return this.g;
    }

    public final vd k() {
        if (this.e == null) {
            this.e = g();
        }
        return this.e;
    }
}
