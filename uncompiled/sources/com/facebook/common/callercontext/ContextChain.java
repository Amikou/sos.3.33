package com.facebook.common.callercontext;

import android.os.Parcel;
import android.os.Parcelable;

/* loaded from: classes.dex */
public class ContextChain implements Parcelable {
    public static final Parcelable.Creator<ContextChain> CREATOR = new a();
    public static boolean j0 = false;
    public final String a;
    public final String f0;
    public final int g0;
    public final ContextChain h0;
    public String i0;

    /* loaded from: classes.dex */
    public static class a implements Parcelable.Creator<ContextChain> {
        @Override // android.os.Parcelable.Creator
        /* renamed from: a */
        public ContextChain createFromParcel(Parcel parcel) {
            return new ContextChain(parcel);
        }

        @Override // android.os.Parcelable.Creator
        /* renamed from: b */
        public ContextChain[] newArray(int i) {
            return new ContextChain[i];
        }
    }

    public ContextChain(Parcel parcel) {
        this.a = parcel.readString();
        this.f0 = parcel.readString();
        this.g0 = parcel.readInt();
        this.h0 = (ContextChain) parcel.readParcelable(ContextChain.class.getClassLoader());
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (j0) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ContextChain contextChain = (ContextChain) obj;
            if (ol2.a(this.a, contextChain.a) && ol2.a(this.f0, contextChain.f0) && this.g0 == contextChain.g0) {
                ContextChain contextChain2 = this.h0;
                ContextChain contextChain3 = contextChain.h0;
                if (contextChain2 == contextChain3) {
                    return true;
                }
                if (contextChain2 != null && contextChain2.equals(contextChain3)) {
                    return true;
                }
            }
            return false;
        }
        return super.equals(obj);
    }

    public int hashCode() {
        if (j0) {
            int hashCode = super.hashCode() * 31;
            String str = this.a;
            int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
            String str2 = this.f0;
            int hashCode3 = (((hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31) + this.g0) * 31;
            ContextChain contextChain = this.h0;
            return hashCode3 + (contextChain != null ? contextChain.hashCode() : 0);
        }
        return super.hashCode();
    }

    public String toString() {
        if (this.i0 == null) {
            this.i0 = this.a + ":" + this.f0;
            if (this.h0 != null) {
                this.i0 = this.h0.toString() + '/' + this.i0;
            }
        }
        return this.i0;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.a);
        parcel.writeString(this.f0);
        parcel.writeInt(this.g0);
        parcel.writeParcelable(this.h0, i);
    }
}
