package com.facebook.common.time;

import android.os.SystemClock;

@cq0
/* loaded from: classes.dex */
public class RealtimeSinceBootClock implements o92 {
    public static final RealtimeSinceBootClock a = new RealtimeSinceBootClock();

    private RealtimeSinceBootClock() {
    }

    @cq0
    public static RealtimeSinceBootClock get() {
        return a;
    }

    @Override // defpackage.o92
    public long now() {
        return SystemClock.elapsedRealtime();
    }
}
