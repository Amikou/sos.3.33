package com.facebook.common.time;

import android.os.SystemClock;

@cq0
/* loaded from: classes.dex */
public class AwakeTimeSinceBootClock implements o92 {
    @cq0
    private static final AwakeTimeSinceBootClock INSTANCE = new AwakeTimeSinceBootClock();

    private AwakeTimeSinceBootClock() {
    }

    @cq0
    public static AwakeTimeSinceBootClock get() {
        return INSTANCE;
    }

    @Override // defpackage.o92
    @cq0
    public long now() {
        return SystemClock.uptimeMillis();
    }

    @cq0
    public long nowNanos() {
        return System.nanoTime();
    }
}
