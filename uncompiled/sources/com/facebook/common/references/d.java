package com.facebook.common.references;

import com.facebook.common.references.a;

/* compiled from: NoOpCloseableReference.java */
/* loaded from: classes.dex */
public class d<T> extends a<T> {
    public d(T t, d83<T> d83Var, a.c cVar, Throwable th) {
        super(t, d83Var, cVar, th);
    }

    @Override // com.facebook.common.references.a
    /* renamed from: b */
    public a<T> clone() {
        return this;
    }

    @Override // com.facebook.common.references.a, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
    }
}
