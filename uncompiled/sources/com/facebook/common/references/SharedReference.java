package com.facebook.common.references;

import android.graphics.Bitmap;
import java.util.IdentityHashMap;
import java.util.Map;

/* loaded from: classes.dex */
public class SharedReference<T> {
    public static final Map<Object, Integer> d = new IdentityHashMap();
    public T a;
    public int b = 1;
    public final d83<T> c;

    /* loaded from: classes.dex */
    public static class NullReferenceException extends RuntimeException {
        public NullReferenceException() {
            super("Null shared reference");
        }
    }

    public SharedReference(T t, d83<T> d83Var) {
        this.a = (T) xt2.g(t);
        this.c = (d83) xt2.g(d83Var);
        a(t);
    }

    public static void a(Object obj) {
        if (a.Q() && ((obj instanceof Bitmap) || (obj instanceof yj1))) {
            return;
        }
        Map<Object, Integer> map = d;
        synchronized (map) {
            Integer num = map.get(obj);
            if (num == null) {
                map.put(obj, 1);
            } else {
                map.put(obj, Integer.valueOf(num.intValue() + 1));
            }
        }
    }

    public static boolean h(SharedReference<?> sharedReference) {
        return sharedReference != null && sharedReference.g();
    }

    public static void i(Object obj) {
        Map<Object, Integer> map = d;
        synchronized (map) {
            Integer num = map.get(obj);
            if (num == null) {
                v11.A("SharedReference", "No entry in sLiveObjects for value of type %s", obj.getClass());
            } else if (num.intValue() == 1) {
                map.remove(obj);
            } else {
                map.put(obj, Integer.valueOf(num.intValue() - 1));
            }
        }
    }

    public synchronized void b() {
        e();
        this.b++;
    }

    public final synchronized int c() {
        int i;
        e();
        xt2.b(Boolean.valueOf(this.b > 0));
        i = this.b - 1;
        this.b = i;
        return i;
    }

    public void d() {
        T t;
        if (c() == 0) {
            synchronized (this) {
                t = this.a;
                this.a = null;
            }
            if (t != null) {
                this.c.a(t);
                i(t);
            }
        }
    }

    public final void e() {
        if (!h(this)) {
            throw new NullReferenceException();
        }
    }

    public synchronized T f() {
        return this.a;
    }

    public synchronized boolean g() {
        return this.b > 0;
    }
}
