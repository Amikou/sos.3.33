package com.facebook.common.references;

import com.facebook.common.references.a;

/* compiled from: FinalizerCloseableReference.java */
/* loaded from: classes.dex */
public class c<T> extends a<T> {
    public c(T t, d83<T> d83Var, a.c cVar, Throwable th) {
        super(t, d83Var, cVar, th);
    }

    @Override // com.facebook.common.references.a
    /* renamed from: b */
    public a<T> clone() {
        return this;
    }

    @Override // com.facebook.common.references.a, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
    }

    public void finalize() throws Throwable {
        try {
            synchronized (this) {
                if (this.a) {
                    return;
                }
                T f = this.f0.f();
                Object[] objArr = new Object[3];
                objArr[0] = Integer.valueOf(System.identityHashCode(this));
                objArr[1] = Integer.valueOf(System.identityHashCode(this.f0));
                objArr[2] = f == null ? null : f.getClass().getName();
                v11.x("FinalizerCloseableReference", "Finalized without closing: %x %x (type = %s)", objArr);
                this.f0.d();
            }
        } finally {
            super.finalize();
        }
    }
}
