package com.facebook.common.references;

import android.graphics.Bitmap;
import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/* compiled from: CloseableReference.java */
/* loaded from: classes.dex */
public abstract class a<T> implements Cloneable, Closeable {
    public static Class<a> i0 = a.class;
    public static int j0 = 0;
    public static final d83<Closeable> k0 = new C0084a();
    public static final c l0 = new b();
    public boolean a = false;
    public final SharedReference<T> f0;
    public final c g0;
    public final Throwable h0;

    /* compiled from: CloseableReference.java */
    /* renamed from: com.facebook.common.references.a$a  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static class C0084a implements d83<Closeable> {
        @Override // defpackage.d83
        /* renamed from: b */
        public void a(Closeable closeable) {
            try {
                com.facebook.common.internal.b.a(closeable, true);
            } catch (IOException unused) {
            }
        }
    }

    /* compiled from: CloseableReference.java */
    /* loaded from: classes.dex */
    public static class b implements c {
        @Override // com.facebook.common.references.a.c
        public void a(SharedReference<Object> sharedReference, Throwable th) {
            Object f = sharedReference.f();
            Class cls = a.i0;
            Object[] objArr = new Object[3];
            objArr[0] = Integer.valueOf(System.identityHashCode(this));
            objArr[1] = Integer.valueOf(System.identityHashCode(sharedReference));
            objArr[2] = f == null ? null : f.getClass().getName();
            v11.v(cls, "Finalized without closing: %x %x (type = %s)", objArr);
        }

        @Override // com.facebook.common.references.a.c
        public boolean b() {
            return false;
        }
    }

    /* compiled from: CloseableReference.java */
    /* loaded from: classes.dex */
    public interface c {
        void a(SharedReference<Object> sharedReference, Throwable th);

        boolean b();
    }

    public a(SharedReference<T> sharedReference, c cVar, Throwable th) {
        this.f0 = (SharedReference) xt2.g(sharedReference);
        sharedReference.b();
        this.g0 = cVar;
        this.h0 = th;
    }

    public static <T> a<T> A(T t, d83<T> d83Var) {
        return C(t, d83Var, l0);
    }

    public static <T> a<T> C(T t, d83<T> d83Var, c cVar) {
        if (t == null) {
            return null;
        }
        return M(t, d83Var, cVar, cVar.b() ? new Throwable() : null);
    }

    public static <T> a<T> M(T t, d83<T> d83Var, c cVar, Throwable th) {
        if (t == null) {
            return null;
        }
        if ((t instanceof Bitmap) || (t instanceof yj1)) {
            int i = j0;
            if (i == 1) {
                return new com.facebook.common.references.c(t, d83Var, cVar, th);
            }
            if (i == 2) {
                return new e(t, d83Var, cVar, th);
            }
            if (i == 3) {
                return new d(t, d83Var, cVar, th);
            }
        }
        return new com.facebook.common.references.b(t, d83Var, cVar, th);
    }

    public static void N(int i) {
        j0 = i;
    }

    public static boolean Q() {
        return j0 == 3;
    }

    public static <T> a<T> e(a<T> aVar) {
        if (aVar != null) {
            return aVar.d();
        }
        return null;
    }

    public static <T> List<a<T>> f(Collection<a<T>> collection) {
        if (collection == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList(collection.size());
        for (a<T> aVar : collection) {
            arrayList.add(e(aVar));
        }
        return arrayList;
    }

    public static void g(a<?> aVar) {
        if (aVar != null) {
            aVar.close();
        }
    }

    public static void h(Iterable<? extends a<?>> iterable) {
        if (iterable != null) {
            for (a<?> aVar : iterable) {
                g(aVar);
            }
        }
    }

    public static boolean u(a<?> aVar) {
        return aVar != null && aVar.r();
    }

    /* JADX WARN: Incorrect types in method signature: <T::Ljava/io/Closeable;>(TT;)Lcom/facebook/common/references/a<TT;>; */
    public static a v(Closeable closeable) {
        return A(closeable, k0);
    }

    /* JADX WARN: Incorrect types in method signature: <T::Ljava/io/Closeable;>(TT;Lcom/facebook/common/references/a$c;)Lcom/facebook/common/references/a<TT;>; */
    public static a x(Closeable closeable, c cVar) {
        if (closeable == null) {
            return null;
        }
        return M(closeable, k0, cVar, cVar.b() ? new Throwable() : null);
    }

    @Override // 
    /* renamed from: b */
    public abstract a<T> clone();

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        synchronized (this) {
            if (this.a) {
                return;
            }
            this.a = true;
            this.f0.d();
        }
    }

    public synchronized a<T> d() {
        if (r()) {
            return clone();
        }
        return null;
    }

    public synchronized T j() {
        xt2.i(!this.a);
        return (T) xt2.g(this.f0.f());
    }

    public int l() {
        if (r()) {
            return System.identityHashCode(this.f0.f());
        }
        return 0;
    }

    public synchronized boolean r() {
        return !this.a;
    }

    public a(T t, d83<T> d83Var, c cVar, Throwable th) {
        this.f0 = new SharedReference<>(t, d83Var);
        this.g0 = cVar;
        this.h0 = th;
    }
}
