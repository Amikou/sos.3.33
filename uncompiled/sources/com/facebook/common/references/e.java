package com.facebook.common.references;

import com.facebook.common.references.a;

/* compiled from: RefCountCloseableReference.java */
/* loaded from: classes.dex */
public class e<T> extends a<T> {
    public e(SharedReference<T> sharedReference, a.c cVar, Throwable th) {
        super(sharedReference, cVar, th);
    }

    @Override // com.facebook.common.references.a
    /* renamed from: b */
    public a<T> clone() {
        xt2.i(r());
        return new e(this.f0, this.g0, this.h0);
    }

    public e(T t, d83<T> d83Var, a.c cVar, Throwable th) {
        super(t, d83Var, cVar, th);
    }
}
