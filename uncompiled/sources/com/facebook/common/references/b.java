package com.facebook.common.references;

import com.facebook.common.references.a;

/* compiled from: DefaultCloseableReference.java */
/* loaded from: classes.dex */
public class b<T> extends a<T> {
    public b(SharedReference<T> sharedReference, a.c cVar, Throwable th) {
        super(sharedReference, cVar, th);
    }

    @Override // com.facebook.common.references.a
    /* renamed from: b */
    public a<T> clone() {
        xt2.i(r());
        return new b(this.f0, this.g0, this.h0 != null ? new Throwable(this.h0) : null);
    }

    public void finalize() throws Throwable {
        try {
            synchronized (this) {
                if (this.a) {
                    return;
                }
                T f = this.f0.f();
                Object[] objArr = new Object[3];
                objArr[0] = Integer.valueOf(System.identityHashCode(this));
                objArr[1] = Integer.valueOf(System.identityHashCode(this.f0));
                objArr[2] = f == null ? null : f.getClass().getName();
                v11.x("DefaultCloseableReference", "Finalized without closing: %x %x (type = %s)", objArr);
                this.g0.a(this.f0, this.h0);
                close();
            }
        } finally {
            super.finalize();
        }
    }

    public b(T t, d83<T> d83Var, a.c cVar, Throwable th) {
        super(t, d83Var, cVar, th);
    }
}
