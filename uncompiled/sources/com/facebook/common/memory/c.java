package com.facebook.common.memory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import okhttp3.internal.http2.Http2;

/* compiled from: PooledByteStreams.java */
/* loaded from: classes.dex */
public class c {
    public final int a;
    public final os b;

    public c(os osVar) {
        this(osVar, Http2.INITIAL_MAX_FRAME_SIZE);
    }

    public long a(InputStream inputStream, OutputStream outputStream) throws IOException {
        byte[] bArr = this.b.get(this.a);
        long j = 0;
        while (true) {
            try {
                int read = inputStream.read(bArr, 0, this.a);
                if (read == -1) {
                    return j;
                }
                outputStream.write(bArr, 0, read);
                j += read;
            } finally {
                this.b.a(bArr);
            }
        }
    }

    public c(os osVar, int i) {
        xt2.b(Boolean.valueOf(i > 0));
        this.a = i;
        this.b = osVar;
    }
}
