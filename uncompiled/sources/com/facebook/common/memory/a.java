package com.facebook.common.memory;

import java.io.IOException;
import java.io.InputStream;

/* compiled from: PooledByteArrayBufferedInputStream.java */
/* loaded from: classes.dex */
public class a extends InputStream {
    public final InputStream a;
    public final byte[] f0;
    public final d83<byte[]> g0;
    public int h0 = 0;
    public int i0 = 0;
    public boolean j0 = false;

    public a(InputStream inputStream, byte[] bArr, d83<byte[]> d83Var) {
        this.a = (InputStream) xt2.g(inputStream);
        this.f0 = (byte[]) xt2.g(bArr);
        this.g0 = (d83) xt2.g(d83Var);
    }

    public final boolean a() throws IOException {
        if (this.i0 < this.h0) {
            return true;
        }
        int read = this.a.read(this.f0);
        if (read <= 0) {
            return false;
        }
        this.h0 = read;
        this.i0 = 0;
        return true;
    }

    @Override // java.io.InputStream
    public int available() throws IOException {
        xt2.i(this.i0 <= this.h0);
        b();
        return (this.h0 - this.i0) + this.a.available();
    }

    public final void b() throws IOException {
        if (this.j0) {
            throw new IOException("stream already closed");
        }
    }

    @Override // java.io.InputStream, java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        if (this.j0) {
            return;
        }
        this.j0 = true;
        this.g0.a(this.f0);
        super.close();
    }

    public void finalize() throws Throwable {
        if (!this.j0) {
            v11.h("PooledByteInputStream", "Finalized without closing");
            close();
        }
        super.finalize();
    }

    @Override // java.io.InputStream
    public int read() throws IOException {
        xt2.i(this.i0 <= this.h0);
        b();
        if (a()) {
            byte[] bArr = this.f0;
            int i = this.i0;
            this.i0 = i + 1;
            return bArr[i] & 255;
        }
        return -1;
    }

    @Override // java.io.InputStream
    public long skip(long j) throws IOException {
        xt2.i(this.i0 <= this.h0);
        b();
        int i = this.h0;
        int i2 = this.i0;
        long j2 = i - i2;
        if (j2 >= j) {
            this.i0 = (int) (i2 + j);
            return j;
        }
        this.i0 = i;
        return j2 + this.a.skip(j - j2);
    }

    @Override // java.io.InputStream
    public int read(byte[] bArr, int i, int i2) throws IOException {
        xt2.i(this.i0 <= this.h0);
        b();
        if (a()) {
            int min = Math.min(this.h0 - this.i0, i2);
            System.arraycopy(this.f0, this.i0, bArr, i, min);
            this.i0 += min;
            return min;
        }
        return -1;
    }
}
