package com.facebook.common.memory;

import java.io.Closeable;
import java.nio.ByteBuffer;

/* loaded from: classes.dex */
public interface PooledByteBuffer extends Closeable {

    /* loaded from: classes.dex */
    public static class ClosedException extends RuntimeException {
        public ClosedException() {
            super("Invalid bytebuf. Already closed");
        }
    }

    boolean isClosed();

    byte p(int i);

    int s(int i, byte[] bArr, int i2, int i3);

    int size();

    ByteBuffer t();

    long y();
}
