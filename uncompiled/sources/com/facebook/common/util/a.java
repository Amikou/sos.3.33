package com.facebook.common.util;

import java.io.IOException;
import java.io.InputStream;

/* compiled from: StreamUtil.java */
/* loaded from: classes.dex */
public class a {
    public static long a(InputStream inputStream, long j) throws IOException {
        xt2.g(inputStream);
        xt2.b(Boolean.valueOf(j >= 0));
        long j2 = j;
        while (j2 > 0) {
            long skip = inputStream.skip(j2);
            if (skip <= 0) {
                if (inputStream.read() == -1) {
                    return j - j2;
                }
                skip = 1;
            }
            j2 -= skip;
        }
        return j;
    }
}
