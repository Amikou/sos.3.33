package com.facebook.drawee.generic;

import android.content.res.Resources;
import android.graphics.PointF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.os.Build;
import com.facebook.drawee.drawable.RoundedCornersDrawable;
import com.facebook.drawee.generic.RoundingParams;
import defpackage.qc3;

/* compiled from: WrappingUtils.java */
/* loaded from: classes.dex */
public class a {
    public static final Drawable a = new ColorDrawable(0);

    public static Drawable a(Drawable drawable, RoundingParams roundingParams, Resources resources) {
        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            t93 t93Var = new t93(resources, bitmapDrawable.getBitmap(), bitmapDrawable.getPaint());
            b(t93Var, roundingParams);
            return t93Var;
        } else if (drawable instanceof NinePatchDrawable) {
            x93 x93Var = new x93((NinePatchDrawable) drawable);
            b(x93Var, roundingParams);
            return x93Var;
        } else if ((drawable instanceof ColorDrawable) && Build.VERSION.SDK_INT >= 11) {
            u93 c = u93.c((ColorDrawable) drawable);
            b(c, roundingParams);
            return c;
        } else {
            v11.x("WrappingUtils", "Don't know how to round that drawable: %s", drawable);
            return drawable;
        }
    }

    public static void b(s93 s93Var, RoundingParams roundingParams) {
        s93Var.b(roundingParams.h());
        s93Var.l(roundingParams.c());
        s93Var.a(roundingParams.a(), roundingParams.b());
        s93Var.h(roundingParams.f());
        s93Var.e(roundingParams.j());
        s93Var.d(roundingParams.g());
    }

    public static wq0 c(wq0 wq0Var) {
        while (true) {
            Drawable i = wq0Var.i();
            if (i == wq0Var || !(i instanceof wq0)) {
                break;
            }
            wq0Var = (wq0) i;
        }
        return wq0Var;
    }

    public static Drawable d(Drawable drawable, RoundingParams roundingParams, Resources resources) {
        try {
            if (nc1.d()) {
                nc1.a("WrappingUtils#maybeApplyLeafRounding");
            }
            if (drawable != null && roundingParams != null && roundingParams.i() == RoundingParams.RoundingMethod.BITMAP_ONLY) {
                if (drawable instanceof d91) {
                    wq0 c = c((d91) drawable);
                    c.f(a(c.f(a), roundingParams, resources));
                    return drawable;
                }
                Drawable a2 = a(drawable, roundingParams, resources);
                if (nc1.d()) {
                    nc1.b();
                }
                return a2;
            }
            if (nc1.d()) {
                nc1.b();
            }
            return drawable;
        } finally {
            if (nc1.d()) {
                nc1.b();
            }
        }
    }

    public static Drawable e(Drawable drawable, RoundingParams roundingParams) {
        try {
            if (nc1.d()) {
                nc1.a("WrappingUtils#maybeWrapWithRoundedOverlayColor");
            }
            if (drawable != null && roundingParams != null && roundingParams.i() == RoundingParams.RoundingMethod.OVERLAY_COLOR) {
                RoundedCornersDrawable roundedCornersDrawable = new RoundedCornersDrawable(drawable);
                b(roundedCornersDrawable, roundingParams);
                roundedCornersDrawable.q(roundingParams.e());
                return roundedCornersDrawable;
            }
            if (nc1.d()) {
                nc1.b();
            }
            return drawable;
        } finally {
            if (nc1.d()) {
                nc1.b();
            }
        }
    }

    public static Drawable f(Drawable drawable, qc3.b bVar) {
        return g(drawable, bVar, null);
    }

    public static Drawable g(Drawable drawable, qc3.b bVar, PointF pointF) {
        if (nc1.d()) {
            nc1.a("WrappingUtils#maybeWrapWithScaleType");
        }
        if (drawable != null && bVar != null) {
            oc3 oc3Var = new oc3(drawable, bVar);
            if (pointF != null) {
                oc3Var.t(pointF);
            }
            if (nc1.d()) {
                nc1.b();
            }
            return oc3Var;
        }
        if (nc1.d()) {
            nc1.b();
        }
        return drawable;
    }

    public static oc3 h(wq0 wq0Var, qc3.b bVar) {
        Drawable f = f(wq0Var.f(a), bVar);
        wq0Var.f(f);
        xt2.h(f, "Parent has no child drawable!");
        return (oc3) f;
    }
}
