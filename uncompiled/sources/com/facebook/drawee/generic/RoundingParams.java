package com.facebook.drawee.generic;

import com.github.mikephil.charting.utils.Utils;
import java.util.Arrays;

/* loaded from: classes.dex */
public class RoundingParams {
    public RoundingMethod a = RoundingMethod.BITMAP_ONLY;
    public boolean b = false;
    public float[] c = null;
    public int d = 0;
    public float e = Utils.FLOAT_EPSILON;
    public int f = 0;
    public float g = Utils.FLOAT_EPSILON;
    public boolean h = false;
    public boolean i = false;

    /* loaded from: classes.dex */
    public enum RoundingMethod {
        OVERLAY_COLOR,
        BITMAP_ONLY
    }

    public int a() {
        return this.f;
    }

    public float b() {
        return this.e;
    }

    public float[] c() {
        return this.c;
    }

    public final float[] d() {
        if (this.c == null) {
            this.c = new float[8];
        }
        return this.c;
    }

    public int e() {
        return this.d;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || RoundingParams.class != obj.getClass()) {
            return false;
        }
        RoundingParams roundingParams = (RoundingParams) obj;
        if (this.b == roundingParams.b && this.d == roundingParams.d && Float.compare(roundingParams.e, this.e) == 0 && this.f == roundingParams.f && Float.compare(roundingParams.g, this.g) == 0 && this.a == roundingParams.a && this.h == roundingParams.h && this.i == roundingParams.i) {
            return Arrays.equals(this.c, roundingParams.c);
        }
        return false;
    }

    public float f() {
        return this.g;
    }

    public boolean g() {
        return this.i;
    }

    public boolean h() {
        return this.b;
    }

    public int hashCode() {
        RoundingMethod roundingMethod = this.a;
        int hashCode = (((roundingMethod != null ? roundingMethod.hashCode() : 0) * 31) + (this.b ? 1 : 0)) * 31;
        float[] fArr = this.c;
        int hashCode2 = (((hashCode + (fArr != null ? Arrays.hashCode(fArr) : 0)) * 31) + this.d) * 31;
        float f = this.e;
        int floatToIntBits = (((hashCode2 + (f != Utils.FLOAT_EPSILON ? Float.floatToIntBits(f) : 0)) * 31) + this.f) * 31;
        float f2 = this.g;
        return ((((floatToIntBits + (f2 != Utils.FLOAT_EPSILON ? Float.floatToIntBits(f2) : 0)) * 31) + (this.h ? 1 : 0)) * 31) + (this.i ? 1 : 0);
    }

    public RoundingMethod i() {
        return this.a;
    }

    public boolean j() {
        return this.h;
    }

    public RoundingParams k(int i) {
        this.f = i;
        return this;
    }

    public RoundingParams l(float f) {
        xt2.c(f >= Utils.FLOAT_EPSILON, "the border width cannot be < 0");
        this.e = f;
        return this;
    }

    public RoundingParams m(float f, float f2, float f3, float f4) {
        float[] d = d();
        d[1] = f;
        d[0] = f;
        d[3] = f2;
        d[2] = f2;
        d[5] = f3;
        d[4] = f3;
        d[7] = f4;
        d[6] = f4;
        return this;
    }

    public RoundingParams n(int i) {
        this.d = i;
        this.a = RoundingMethod.OVERLAY_COLOR;
        return this;
    }

    public RoundingParams o(float f) {
        xt2.c(f >= Utils.FLOAT_EPSILON, "the padding cannot be < 0");
        this.g = f;
        return this;
    }

    public RoundingParams p(boolean z) {
        this.b = z;
        return this;
    }
}
