package com.facebook.drawee.controller;

import android.content.Context;
import android.graphics.drawable.Animatable;
import com.facebook.drawee.controller.AbstractDraweeControllerBuilder;
import java.util.ArrayList;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

/* loaded from: classes.dex */
public abstract class AbstractDraweeControllerBuilder<BUILDER extends AbstractDraweeControllerBuilder<BUILDER, REQUEST, IMAGE, INFO>, REQUEST, IMAGE, INFO> implements bp3 {
    public static final m80<Object> q = new a();
    public static final NullPointerException r = new NullPointerException("No image request was specified!");
    public static final AtomicLong s = new AtomicLong();
    public final Context a;
    public final Set<m80> b;
    public final Set<l80> c;
    public Object d;
    public REQUEST e;
    public REQUEST f;
    public REQUEST[] g;
    public boolean h;
    public fw3<ge0<IMAGE>> i;
    public m80<? super INFO> j;
    public n80 k;
    public boolean l;
    public boolean m;
    public boolean n;
    public String o;
    public ir0 p;

    /* loaded from: classes.dex */
    public enum CacheLevel {
        FULL_FETCH,
        DISK_CACHE,
        BITMAP_MEMORY_CACHE
    }

    /* loaded from: classes.dex */
    public static class a extends en<Object> {
        @Override // defpackage.en, defpackage.m80
        public void b(String str, Object obj, Animatable animatable) {
            if (animatable != null) {
                animatable.start();
            }
        }
    }

    /* loaded from: classes.dex */
    public class b implements fw3<ge0<IMAGE>> {
        public final /* synthetic */ ir0 a;
        public final /* synthetic */ String b;
        public final /* synthetic */ Object c;
        public final /* synthetic */ Object d;
        public final /* synthetic */ CacheLevel e;

        public b(ir0 ir0Var, String str, Object obj, Object obj2, CacheLevel cacheLevel) {
            this.a = ir0Var;
            this.b = str;
            this.c = obj;
            this.d = obj2;
            this.e = cacheLevel;
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // defpackage.fw3
        /* renamed from: a */
        public ge0<IMAGE> get() {
            return AbstractDraweeControllerBuilder.this.i(this.a, this.b, this.c, this.d, this.e);
        }

        public String toString() {
            return ol2.c(this).b("request", this.c.toString()).toString();
        }
    }

    public AbstractDraweeControllerBuilder(Context context, Set<m80> set, Set<l80> set2) {
        this.a = context;
        this.b = set;
        this.c = set2;
        s();
    }

    public static String e() {
        return String.valueOf(s.getAndIncrement());
    }

    public BUILDER A(m80<? super INFO> m80Var) {
        this.j = m80Var;
        return r();
    }

    public BUILDER B(REQUEST request) {
        this.e = request;
        return r();
    }

    @Override // defpackage.bp3
    /* renamed from: C */
    public BUILDER b(ir0 ir0Var) {
        this.p = ir0Var;
        return r();
    }

    public void D() {
        boolean z = false;
        xt2.j(this.g == null || this.e == null, "Cannot specify both ImageRequest and FirstAvailableImageRequests!");
        if (this.i == null || (this.g == null && this.e == null && this.f == null)) {
            z = true;
        }
        xt2.j(z, "Cannot specify DataSourceSupplier with other ImageRequests! Use one or the other.");
    }

    @Override // defpackage.bp3
    /* renamed from: c */
    public w4 build() {
        REQUEST request;
        D();
        if (this.e == null && this.g == null && (request = this.f) != null) {
            this.e = request;
            this.f = null;
        }
        return d();
    }

    public w4 d() {
        if (nc1.d()) {
            nc1.a("AbstractDraweeControllerBuilder#buildController");
        }
        w4 w = w();
        w.d0(q());
        w.Z(g());
        w.b0(h());
        v(w);
        t(w);
        if (nc1.d()) {
            nc1.b();
        }
        return w;
    }

    public Object f() {
        return this.d;
    }

    public String g() {
        return this.o;
    }

    public n80 h() {
        return this.k;
    }

    public abstract ge0<IMAGE> i(ir0 ir0Var, String str, REQUEST request, Object obj, CacheLevel cacheLevel);

    public fw3<ge0<IMAGE>> j(ir0 ir0Var, String str, REQUEST request) {
        return k(ir0Var, str, request, CacheLevel.FULL_FETCH);
    }

    public fw3<ge0<IMAGE>> k(ir0 ir0Var, String str, REQUEST request, CacheLevel cacheLevel) {
        return new b(ir0Var, str, request, f(), cacheLevel);
    }

    public fw3<ge0<IMAGE>> l(ir0 ir0Var, String str, REQUEST[] requestArr, boolean z) {
        ArrayList arrayList = new ArrayList(requestArr.length * 2);
        if (z) {
            for (REQUEST request : requestArr) {
                arrayList.add(k(ir0Var, str, request, CacheLevel.BITMAP_MEMORY_CACHE));
            }
        }
        for (REQUEST request2 : requestArr) {
            arrayList.add(j(ir0Var, str, request2));
        }
        return z51.b(arrayList);
    }

    public REQUEST[] m() {
        return this.g;
    }

    public REQUEST n() {
        return this.e;
    }

    public REQUEST o() {
        return this.f;
    }

    public ir0 p() {
        return this.p;
    }

    public boolean q() {
        return this.n;
    }

    public final BUILDER r() {
        return this;
    }

    public final void s() {
        this.d = null;
        this.e = null;
        this.f = null;
        this.g = null;
        this.h = true;
        this.j = null;
        this.k = null;
        this.l = false;
        this.m = false;
        this.p = null;
        this.o = null;
    }

    public void t(w4 w4Var) {
        Set<m80> set = this.b;
        if (set != null) {
            for (m80 m80Var : set) {
                w4Var.l(m80Var);
            }
        }
        Set<l80> set2 = this.c;
        if (set2 != null) {
            for (l80 l80Var : set2) {
                w4Var.m(l80Var);
            }
        }
        m80<? super INFO> m80Var2 = this.j;
        if (m80Var2 != null) {
            w4Var.l(m80Var2);
        }
        if (this.m) {
            w4Var.l(q);
        }
    }

    public void u(w4 w4Var) {
        if (w4Var.w() == null) {
            w4Var.c0(ff1.c(this.a));
        }
    }

    public void v(w4 w4Var) {
        if (this.l) {
            w4Var.C().d(this.l);
            u(w4Var);
        }
    }

    public abstract w4 w();

    public fw3<ge0<IMAGE>> x(ir0 ir0Var, String str) {
        fw3<ge0<IMAGE>> fw3Var = this.i;
        if (fw3Var != null) {
            return fw3Var;
        }
        fw3<ge0<IMAGE>> fw3Var2 = null;
        REQUEST request = this.e;
        if (request != null) {
            fw3Var2 = j(ir0Var, str, request);
        } else {
            REQUEST[] requestArr = this.g;
            if (requestArr != null) {
                fw3Var2 = l(ir0Var, str, requestArr, this.h);
            }
        }
        if (fw3Var2 != null && this.f != null) {
            ArrayList arrayList = new ArrayList(2);
            arrayList.add(fw3Var2);
            arrayList.add(j(ir0Var, str, this.f));
            fw3Var2 = fq1.c(arrayList, false);
        }
        return fw3Var2 == null ? ie0.a(r) : fw3Var2;
    }

    public BUILDER y(boolean z) {
        this.m = z;
        return r();
    }

    public BUILDER z(Object obj) {
        this.d = obj;
        return r();
    }
}
