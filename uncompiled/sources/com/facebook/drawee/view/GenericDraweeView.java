package com.facebook.drawee.view;

import android.content.Context;
import android.util.AttributeSet;

/* loaded from: classes.dex */
public class GenericDraweeView extends DraweeView<ye1> {
    public GenericDraweeView(Context context) {
        super(context);
        g(context, null);
    }

    public void g(Context context, AttributeSet attributeSet) {
        if (nc1.d()) {
            nc1.a("GenericDraweeView#inflateHierarchy");
        }
        ze1 d = af1.d(context, attributeSet);
        setAspectRatio(d.f());
        setHierarchy(d.a());
        if (nc1.d()) {
            nc1.b();
        }
    }

    public GenericDraweeView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        g(context, attributeSet);
    }

    public GenericDraweeView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        g(context, attributeSet);
    }
}
