package com.facebook.drawee.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.net.Uri;
import android.util.AttributeSet;
import com.facebook.drawee.controller.AbstractDraweeControllerBuilder;
import com.facebook.imagepipeline.request.ImageRequest;

/* loaded from: classes.dex */
public class SimpleDraweeView extends GenericDraweeView {
    public static fw3<? extends AbstractDraweeControllerBuilder> l0;
    public AbstractDraweeControllerBuilder k0;

    public SimpleDraweeView(Context context) {
        super(context);
        h(context, null);
    }

    public static void i(fw3<? extends AbstractDraweeControllerBuilder> fw3Var) {
        l0 = fw3Var;
    }

    public AbstractDraweeControllerBuilder getControllerBuilder() {
        return this.k0;
    }

    public final void h(Context context, AttributeSet attributeSet) {
        int resourceId;
        try {
            if (nc1.d()) {
                nc1.a("SimpleDraweeView#init");
            }
            if (isInEditMode()) {
                getTopLevelDrawable().setVisible(true, false);
                getTopLevelDrawable().invalidateSelf();
            } else {
                xt2.h(l0, "SimpleDraweeView was not initialized!");
                this.k0 = l0.get();
            }
            if (attributeSet != null) {
                TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, k23.SimpleDraweeView);
                int i = k23.SimpleDraweeView_actualImageUri;
                if (obtainStyledAttributes.hasValue(i)) {
                    setImageURI(Uri.parse(obtainStyledAttributes.getString(i)), (Object) null);
                } else {
                    int i2 = k23.SimpleDraweeView_actualImageResource;
                    if (obtainStyledAttributes.hasValue(i2) && (resourceId = obtainStyledAttributes.getResourceId(i2, -1)) != -1) {
                        if (isInEditMode()) {
                            setImageResource(resourceId);
                        } else {
                            setActualImageResource(resourceId);
                        }
                    }
                }
                obtainStyledAttributes.recycle();
            }
        } finally {
            if (nc1.d()) {
                nc1.b();
            }
        }
    }

    public void setActualImageResource(int i) {
        setActualImageResource(i, null);
    }

    public void setImageRequest(ImageRequest imageRequest) {
        setController(this.k0.B(imageRequest).b(getController()).build());
    }

    @Override // com.facebook.drawee.view.DraweeView, android.widget.ImageView
    public void setImageResource(int i) {
        super.setImageResource(i);
    }

    @Override // com.facebook.drawee.view.DraweeView, android.widget.ImageView
    public void setImageURI(Uri uri) {
        setImageURI(uri, (Object) null);
    }

    public void setActualImageResource(int i, Object obj) {
        setImageURI(qf4.d(i), obj);
    }

    public void setImageURI(String str) {
        setImageURI(str, (Object) null);
    }

    public SimpleDraweeView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        h(context, attributeSet);
    }

    public void setImageURI(Uri uri, Object obj) {
        setController(this.k0.z(obj).a(uri).b(getController()).build());
    }

    public SimpleDraweeView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        h(context, attributeSet);
    }

    public void setImageURI(String str, Object obj) {
        setImageURI(str != null ? Uri.parse(str) : null, obj);
    }
}
