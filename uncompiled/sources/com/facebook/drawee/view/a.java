package com.facebook.drawee.view;

import android.view.View;
import android.view.ViewGroup;
import com.github.mikephil.charting.utils.Utils;

/* compiled from: AspectRatioMeasure.java */
/* loaded from: classes.dex */
public class a {

    /* compiled from: AspectRatioMeasure.java */
    /* renamed from: com.facebook.drawee.view.a$a  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static class C0085a {
        public int a;
        public int b;
    }

    public static boolean a(int i) {
        return i == 0 || i == -2;
    }

    public static void b(C0085a c0085a, float f, ViewGroup.LayoutParams layoutParams, int i, int i2) {
        if (f <= Utils.FLOAT_EPSILON || layoutParams == null) {
            return;
        }
        if (a(layoutParams.height)) {
            c0085a.b = View.MeasureSpec.makeMeasureSpec(View.resolveSize((int) (((View.MeasureSpec.getSize(c0085a.a) - i) / f) + i2), c0085a.b), 1073741824);
        } else if (a(layoutParams.width)) {
            c0085a.a = View.MeasureSpec.makeMeasureSpec(View.resolveSize((int) (((View.MeasureSpec.getSize(c0085a.b) - i2) * f) + i), c0085a.a), 1073741824);
        }
    }
}
