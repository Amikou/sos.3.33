package com.facebook.drawee.view;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import com.facebook.drawee.view.a;
import com.github.mikephil.charting.utils.Utils;
import defpackage.jr0;
import defpackage.ol2;

/* loaded from: classes.dex */
public class DraweeView<DH extends jr0> extends ImageView {
    public static boolean j0 = false;
    public final a.C0085a a;
    public float f0;
    public kr0<DH> g0;
    public boolean h0;
    public boolean i0;

    public DraweeView(Context context) {
        super(context);
        this.a = new a.C0085a();
        this.f0 = Utils.FLOAT_EPSILON;
        this.h0 = false;
        this.i0 = false;
        c(context);
    }

    public static void setGlobalLegacyVisibilityHandlingEnabled(boolean z) {
        j0 = z;
    }

    public void a() {
        this.g0.k();
    }

    public void b() {
        this.g0.l();
    }

    public final void c(Context context) {
        boolean d;
        try {
            if (nc1.d()) {
                nc1.a("DraweeView#init");
            }
            if (this.h0) {
                if (d) {
                    return;
                }
                return;
            }
            boolean z = true;
            this.h0 = true;
            this.g0 = kr0.e(null, context);
            if (Build.VERSION.SDK_INT >= 21) {
                ColorStateList imageTintList = getImageTintList();
                if (imageTintList == null) {
                    if (nc1.d()) {
                        nc1.b();
                        return;
                    }
                    return;
                }
                setColorFilter(imageTintList.getDefaultColor());
            }
            if (!j0 || context.getApplicationInfo().targetSdkVersion < 24) {
                z = false;
            }
            this.i0 = z;
            if (nc1.d()) {
                nc1.b();
            }
        } finally {
            if (nc1.d()) {
                nc1.b();
            }
        }
    }

    public final void d() {
        Drawable drawable;
        if (!this.i0 || (drawable = getDrawable()) == null) {
            return;
        }
        drawable.setVisible(getVisibility() == 0, false);
    }

    public void e() {
        a();
    }

    public void f() {
        b();
    }

    public float getAspectRatio() {
        return this.f0;
    }

    public ir0 getController() {
        return this.g0.g();
    }

    public DH getHierarchy() {
        return this.g0.h();
    }

    public Drawable getTopLevelDrawable() {
        return this.g0.i();
    }

    @Override // android.widget.ImageView, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        d();
        e();
    }

    @Override // android.widget.ImageView, android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        d();
        f();
    }

    @Override // android.view.View
    public void onFinishTemporaryDetach() {
        super.onFinishTemporaryDetach();
        d();
        e();
    }

    @Override // android.widget.ImageView, android.view.View
    public void onMeasure(int i, int i2) {
        a.C0085a c0085a = this.a;
        c0085a.a = i;
        c0085a.b = i2;
        a.b(c0085a, this.f0, getLayoutParams(), getPaddingLeft() + getPaddingRight(), getPaddingTop() + getPaddingBottom());
        a.C0085a c0085a2 = this.a;
        super.onMeasure(c0085a2.a, c0085a2.b);
    }

    @Override // android.view.View
    public void onStartTemporaryDetach() {
        super.onStartTemporaryDetach();
        d();
        f();
    }

    @Override // android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.g0.m(motionEvent)) {
            return true;
        }
        return super.onTouchEvent(motionEvent);
    }

    @Override // android.view.View
    public void onVisibilityChanged(View view, int i) {
        super.onVisibilityChanged(view, i);
        d();
    }

    public void setAspectRatio(float f) {
        if (f == this.f0) {
            return;
        }
        this.f0 = f;
        requestLayout();
    }

    public void setController(ir0 ir0Var) {
        this.g0.o(ir0Var);
        super.setImageDrawable(this.g0.i());
    }

    public void setHierarchy(DH dh) {
        this.g0.p(dh);
        super.setImageDrawable(this.g0.i());
    }

    @Override // android.widget.ImageView
    @Deprecated
    public void setImageBitmap(Bitmap bitmap) {
        c(getContext());
        this.g0.o(null);
        super.setImageBitmap(bitmap);
    }

    @Override // android.widget.ImageView
    @Deprecated
    public void setImageDrawable(Drawable drawable) {
        c(getContext());
        this.g0.o(null);
        super.setImageDrawable(drawable);
    }

    @Override // android.widget.ImageView
    @Deprecated
    public void setImageResource(int i) {
        c(getContext());
        this.g0.o(null);
        super.setImageResource(i);
    }

    @Override // android.widget.ImageView
    @Deprecated
    public void setImageURI(Uri uri) {
        c(getContext());
        this.g0.o(null);
        super.setImageURI(uri);
    }

    public void setLegacyVisibilityHandlingEnabled(boolean z) {
        this.i0 = z;
    }

    @Override // android.view.View
    public String toString() {
        ol2.b c = ol2.c(this);
        kr0<DH> kr0Var = this.g0;
        return c.b("holder", kr0Var != null ? kr0Var.toString() : "<no holder set>").toString();
    }

    public DraweeView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.a = new a.C0085a();
        this.f0 = Utils.FLOAT_EPSILON;
        this.h0 = false;
        this.i0 = false;
        c(context);
    }

    public DraweeView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.a = new a.C0085a();
        this.f0 = Utils.FLOAT_EPSILON;
        this.h0 = false;
        this.i0 = false;
        c(context);
    }
}
