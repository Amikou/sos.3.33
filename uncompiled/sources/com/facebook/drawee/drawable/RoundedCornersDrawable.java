package com.facebook.drawee.drawable;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import com.github.mikephil.charting.utils.Utils;
import java.util.Arrays;

/* loaded from: classes.dex */
public class RoundedCornersDrawable extends d91 implements s93 {
    public Type h0;
    public final RectF i0;
    public RectF j0;
    public Matrix k0;
    public final float[] l0;
    public final float[] m0;
    public final Paint n0;
    public boolean o0;
    public float p0;
    public int q0;
    public int r0;
    public float s0;
    public boolean t0;
    public boolean u0;
    public final Path v0;
    public final Path w0;
    public final RectF x0;

    /* loaded from: classes.dex */
    public enum Type {
        OVERLAY_COLOR,
        CLIPPING
    }

    /* loaded from: classes.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[Type.values().length];
            a = iArr;
            try {
                iArr[Type.CLIPPING.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[Type.OVERLAY_COLOR.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
        }
    }

    public RoundedCornersDrawable(Drawable drawable) {
        super((Drawable) xt2.g(drawable));
        this.h0 = Type.OVERLAY_COLOR;
        this.i0 = new RectF();
        this.l0 = new float[8];
        this.m0 = new float[8];
        this.n0 = new Paint(1);
        this.o0 = false;
        this.p0 = Utils.FLOAT_EPSILON;
        this.q0 = 0;
        this.r0 = 0;
        this.s0 = Utils.FLOAT_EPSILON;
        this.t0 = false;
        this.u0 = false;
        this.v0 = new Path();
        this.w0 = new Path();
        this.x0 = new RectF();
    }

    @Override // defpackage.s93
    public void a(int i, float f) {
        this.q0 = i;
        this.p0 = f;
        r();
        invalidateSelf();
    }

    @Override // defpackage.s93
    public void b(boolean z) {
        this.o0 = z;
        r();
        invalidateSelf();
    }

    @Override // defpackage.s93
    public void d(boolean z) {
        if (this.u0 != z) {
            this.u0 = z;
            invalidateSelf();
        }
    }

    @Override // defpackage.d91, android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        this.i0.set(getBounds());
        int i = a.a[this.h0.ordinal()];
        if (i == 1) {
            int save = canvas.save();
            canvas.clipPath(this.v0);
            super.draw(canvas);
            canvas.restoreToCount(save);
        } else if (i == 2) {
            if (this.t0) {
                RectF rectF = this.j0;
                if (rectF == null) {
                    this.j0 = new RectF(this.i0);
                    this.k0 = new Matrix();
                } else {
                    rectF.set(this.i0);
                }
                RectF rectF2 = this.j0;
                float f = this.p0;
                rectF2.inset(f, f);
                this.k0.setRectToRect(this.i0, this.j0, Matrix.ScaleToFit.FILL);
                int save2 = canvas.save();
                canvas.clipRect(this.i0);
                canvas.concat(this.k0);
                super.draw(canvas);
                canvas.restoreToCount(save2);
            } else {
                super.draw(canvas);
            }
            this.n0.setStyle(Paint.Style.FILL);
            this.n0.setColor(this.r0);
            this.n0.setStrokeWidth(Utils.FLOAT_EPSILON);
            this.n0.setFilterBitmap(p());
            this.v0.setFillType(Path.FillType.EVEN_ODD);
            canvas.drawPath(this.v0, this.n0);
            if (this.o0) {
                float width = ((this.i0.width() - this.i0.height()) + this.p0) / 2.0f;
                float height = ((this.i0.height() - this.i0.width()) + this.p0) / 2.0f;
                if (width > Utils.FLOAT_EPSILON) {
                    RectF rectF3 = this.i0;
                    float f2 = rectF3.left;
                    canvas.drawRect(f2, rectF3.top, f2 + width, rectF3.bottom, this.n0);
                    RectF rectF4 = this.i0;
                    float f3 = rectF4.right;
                    canvas.drawRect(f3 - width, rectF4.top, f3, rectF4.bottom, this.n0);
                }
                if (height > Utils.FLOAT_EPSILON) {
                    RectF rectF5 = this.i0;
                    float f4 = rectF5.left;
                    float f5 = rectF5.top;
                    canvas.drawRect(f4, f5, rectF5.right, f5 + height, this.n0);
                    RectF rectF6 = this.i0;
                    float f6 = rectF6.left;
                    float f7 = rectF6.bottom;
                    canvas.drawRect(f6, f7 - height, rectF6.right, f7, this.n0);
                }
            }
        }
        if (this.q0 != 0) {
            this.n0.setStyle(Paint.Style.STROKE);
            this.n0.setColor(this.q0);
            this.n0.setStrokeWidth(this.p0);
            this.v0.setFillType(Path.FillType.EVEN_ODD);
            canvas.drawPath(this.w0, this.n0);
        }
    }

    @Override // defpackage.s93
    public void e(boolean z) {
        this.t0 = z;
        r();
        invalidateSelf();
    }

    @Override // defpackage.s93
    public void h(float f) {
        this.s0 = f;
        r();
        invalidateSelf();
    }

    @Override // defpackage.s93
    public void l(float[] fArr) {
        if (fArr == null) {
            Arrays.fill(this.l0, (float) Utils.FLOAT_EPSILON);
        } else {
            xt2.c(fArr.length == 8, "radii should have exactly 8 values");
            System.arraycopy(fArr, 0, this.l0, 0, 8);
        }
        r();
        invalidateSelf();
    }

    @Override // defpackage.d91, android.graphics.drawable.Drawable
    public void onBoundsChange(Rect rect) {
        super.onBoundsChange(rect);
        r();
    }

    public boolean p() {
        return this.u0;
    }

    public void q(int i) {
        this.r0 = i;
        invalidateSelf();
    }

    public final void r() {
        float[] fArr;
        this.v0.reset();
        this.w0.reset();
        this.x0.set(getBounds());
        RectF rectF = this.x0;
        float f = this.s0;
        rectF.inset(f, f);
        if (this.h0 == Type.OVERLAY_COLOR) {
            this.v0.addRect(this.x0, Path.Direction.CW);
        }
        if (this.o0) {
            this.v0.addCircle(this.x0.centerX(), this.x0.centerY(), Math.min(this.x0.width(), this.x0.height()) / 2.0f, Path.Direction.CW);
        } else {
            this.v0.addRoundRect(this.x0, this.l0, Path.Direction.CW);
        }
        RectF rectF2 = this.x0;
        float f2 = this.s0;
        rectF2.inset(-f2, -f2);
        RectF rectF3 = this.x0;
        float f3 = this.p0;
        rectF3.inset(f3 / 2.0f, f3 / 2.0f);
        if (this.o0) {
            this.w0.addCircle(this.x0.centerX(), this.x0.centerY(), Math.min(this.x0.width(), this.x0.height()) / 2.0f, Path.Direction.CW);
        } else {
            int i = 0;
            while (true) {
                fArr = this.m0;
                if (i >= fArr.length) {
                    break;
                }
                fArr[i] = (this.l0[i] + this.s0) - (this.p0 / 2.0f);
                i++;
            }
            this.w0.addRoundRect(this.x0, fArr, Path.Direction.CW);
        }
        RectF rectF4 = this.x0;
        float f4 = this.p0;
        rectF4.inset((-f4) / 2.0f, (-f4) / 2.0f);
    }
}
