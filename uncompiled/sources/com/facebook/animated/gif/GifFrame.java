package com.facebook.animated.gif;

import android.graphics.Bitmap;

/* loaded from: classes.dex */
public class GifFrame implements xd {
    @cq0
    private long mNativeContext;

    @cq0
    public GifFrame(long j) {
        this.mNativeContext = j;
    }

    @cq0
    private native void nativeDispose();

    @cq0
    private native void nativeFinalize();

    @cq0
    private native int nativeGetDisposalMode();

    @cq0
    private native int nativeGetDurationMs();

    @cq0
    private native int nativeGetHeight();

    @cq0
    private native int nativeGetTransparentPixelColor();

    @cq0
    private native int nativeGetWidth();

    @cq0
    private native int nativeGetXOffset();

    @cq0
    private native int nativeGetYOffset();

    @cq0
    private native boolean nativeHasTransparency();

    @cq0
    private native void nativeRenderFrame(int i, int i2, Bitmap bitmap);

    @Override // defpackage.xd
    public void a() {
        nativeDispose();
    }

    @Override // defpackage.xd
    public void b(int i, int i2, Bitmap bitmap) {
        nativeRenderFrame(i, i2, bitmap);
    }

    @Override // defpackage.xd
    public int c() {
        return nativeGetXOffset();
    }

    @Override // defpackage.xd
    public int d() {
        return nativeGetYOffset();
    }

    public int e() {
        return nativeGetDisposalMode();
    }

    public void finalize() {
        nativeFinalize();
    }

    @Override // defpackage.xd
    public int getHeight() {
        return nativeGetHeight();
    }

    @Override // defpackage.xd
    public int getWidth() {
        return nativeGetWidth();
    }
}
