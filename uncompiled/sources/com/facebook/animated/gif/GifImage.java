package com.facebook.animated.gif;

import android.graphics.Bitmap;
import com.facebook.imagepipeline.animated.base.AnimatedDrawableFrameInfo;
import java.nio.ByteBuffer;

@cq0
/* loaded from: classes.dex */
public class GifImage implements td, ud {
    public static volatile boolean b;
    public Bitmap.Config a = null;
    @cq0
    private long mNativeContext;

    @cq0
    public GifImage() {
    }

    public static GifImage k(ByteBuffer byteBuffer, rn1 rn1Var) {
        m();
        byteBuffer.rewind();
        GifImage nativeCreateFromDirectByteBuffer = nativeCreateFromDirectByteBuffer(byteBuffer, rn1Var.b, rn1Var.f);
        nativeCreateFromDirectByteBuffer.a = rn1Var.h;
        return nativeCreateFromDirectByteBuffer;
    }

    public static GifImage l(long j, int i, rn1 rn1Var) {
        m();
        xt2.b(Boolean.valueOf(j != 0));
        GifImage nativeCreateFromNativeMemory = nativeCreateFromNativeMemory(j, i, rn1Var.b, rn1Var.f);
        nativeCreateFromNativeMemory.a = rn1Var.h;
        return nativeCreateFromNativeMemory;
    }

    public static synchronized void m() {
        synchronized (GifImage.class) {
            if (!b) {
                b = true;
                pd2.d("gifimage");
            }
        }
    }

    public static AnimatedDrawableFrameInfo.DisposalMethod n(int i) {
        if (i == 0) {
            return AnimatedDrawableFrameInfo.DisposalMethod.DISPOSE_DO_NOT;
        }
        if (i == 1) {
            return AnimatedDrawableFrameInfo.DisposalMethod.DISPOSE_DO_NOT;
        }
        if (i == 2) {
            return AnimatedDrawableFrameInfo.DisposalMethod.DISPOSE_TO_BACKGROUND;
        }
        if (i == 3) {
            return AnimatedDrawableFrameInfo.DisposalMethod.DISPOSE_TO_PREVIOUS;
        }
        return AnimatedDrawableFrameInfo.DisposalMethod.DISPOSE_DO_NOT;
    }

    @cq0
    private static native GifImage nativeCreateFromDirectByteBuffer(ByteBuffer byteBuffer, int i, boolean z);

    @cq0
    private static native GifImage nativeCreateFromFileDescriptor(int i, int i2, boolean z);

    @cq0
    private static native GifImage nativeCreateFromNativeMemory(long j, int i, int i2, boolean z);

    @cq0
    private native void nativeDispose();

    @cq0
    private native void nativeFinalize();

    @cq0
    private native int nativeGetDuration();

    @cq0
    private native GifFrame nativeGetFrame(int i);

    @cq0
    private native int nativeGetFrameCount();

    @cq0
    private native int[] nativeGetFrameDurations();

    @cq0
    private native int nativeGetHeight();

    @cq0
    private native int nativeGetLoopCount();

    @cq0
    private native int nativeGetSizeInBytes();

    @cq0
    private native int nativeGetWidth();

    @cq0
    private native boolean nativeIsAnimated();

    @Override // defpackage.td
    public int a() {
        return nativeGetFrameCount();
    }

    @Override // defpackage.td
    public int b() {
        int nativeGetLoopCount = nativeGetLoopCount();
        if (nativeGetLoopCount != -1) {
            if (nativeGetLoopCount != 0) {
                return nativeGetLoopCount + 1;
            }
            return 0;
        }
        return 1;
    }

    @Override // defpackage.td
    public AnimatedDrawableFrameInfo c(int i) {
        GifFrame g = g(i);
        try {
            return new AnimatedDrawableFrameInfo(i, g.c(), g.d(), g.getWidth(), g.getHeight(), AnimatedDrawableFrameInfo.BlendOperation.BLEND_WITH_PREVIOUS, n(g.e()));
        } finally {
            g.a();
        }
    }

    @Override // defpackage.ud
    public td d(long j, int i, rn1 rn1Var) {
        return l(j, i, rn1Var);
    }

    @Override // defpackage.ud
    public td e(ByteBuffer byteBuffer, rn1 rn1Var) {
        return k(byteBuffer, rn1Var);
    }

    @Override // defpackage.td
    public Bitmap.Config f() {
        return this.a;
    }

    public void finalize() {
        nativeFinalize();
    }

    @Override // defpackage.td
    public int getHeight() {
        return nativeGetHeight();
    }

    @Override // defpackage.td
    public int getWidth() {
        return nativeGetWidth();
    }

    @Override // defpackage.td
    public boolean h() {
        return false;
    }

    @Override // defpackage.td
    public int[] i() {
        return nativeGetFrameDurations();
    }

    @Override // defpackage.td
    public int j() {
        return nativeGetSizeInBytes();
    }

    @Override // defpackage.td
    /* renamed from: o */
    public GifFrame g(int i) {
        return nativeGetFrame(i);
    }

    @cq0
    public GifImage(long j) {
        this.mNativeContext = j;
    }
}
