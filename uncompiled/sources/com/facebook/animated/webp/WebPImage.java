package com.facebook.animated.webp;

import android.graphics.Bitmap;
import com.facebook.imagepipeline.animated.base.AnimatedDrawableFrameInfo;
import java.nio.ByteBuffer;

@cq0
/* loaded from: classes.dex */
public class WebPImage implements td, ud {
    public Bitmap.Config a = null;
    @cq0
    private long mNativeContext;

    @cq0
    public WebPImage() {
    }

    public static WebPImage k(ByteBuffer byteBuffer, rn1 rn1Var) {
        jt3.a();
        byteBuffer.rewind();
        WebPImage nativeCreateFromDirectByteBuffer = nativeCreateFromDirectByteBuffer(byteBuffer);
        if (rn1Var != null) {
            nativeCreateFromDirectByteBuffer.a = rn1Var.h;
        }
        return nativeCreateFromDirectByteBuffer;
    }

    public static WebPImage l(long j, int i, rn1 rn1Var) {
        jt3.a();
        xt2.b(Boolean.valueOf(j != 0));
        WebPImage nativeCreateFromNativeMemory = nativeCreateFromNativeMemory(j, i);
        if (rn1Var != null) {
            nativeCreateFromNativeMemory.a = rn1Var.h;
        }
        return nativeCreateFromNativeMemory;
    }

    private static native WebPImage nativeCreateFromDirectByteBuffer(ByteBuffer byteBuffer);

    private static native WebPImage nativeCreateFromNativeMemory(long j, int i);

    private native void nativeDispose();

    private native void nativeFinalize();

    private native int nativeGetDuration();

    private native WebPFrame nativeGetFrame(int i);

    private native int nativeGetFrameCount();

    private native int[] nativeGetFrameDurations();

    private native int nativeGetHeight();

    private native int nativeGetLoopCount();

    private native int nativeGetSizeInBytes();

    private native int nativeGetWidth();

    @Override // defpackage.td
    public int a() {
        return nativeGetFrameCount();
    }

    @Override // defpackage.td
    public int b() {
        return nativeGetLoopCount();
    }

    @Override // defpackage.td
    public AnimatedDrawableFrameInfo c(int i) {
        WebPFrame g = g(i);
        try {
            return new AnimatedDrawableFrameInfo(i, g.c(), g.d(), g.getWidth(), g.getHeight(), g.e() ? AnimatedDrawableFrameInfo.BlendOperation.BLEND_WITH_PREVIOUS : AnimatedDrawableFrameInfo.BlendOperation.NO_BLEND, g.f() ? AnimatedDrawableFrameInfo.DisposalMethod.DISPOSE_TO_BACKGROUND : AnimatedDrawableFrameInfo.DisposalMethod.DISPOSE_DO_NOT);
        } finally {
            g.a();
        }
    }

    @Override // defpackage.ud
    public td d(long j, int i, rn1 rn1Var) {
        return l(j, i, rn1Var);
    }

    @Override // defpackage.ud
    public td e(ByteBuffer byteBuffer, rn1 rn1Var) {
        return k(byteBuffer, rn1Var);
    }

    @Override // defpackage.td
    public Bitmap.Config f() {
        return this.a;
    }

    public void finalize() {
        nativeFinalize();
    }

    @Override // defpackage.td
    public int getHeight() {
        return nativeGetHeight();
    }

    @Override // defpackage.td
    public int getWidth() {
        return nativeGetWidth();
    }

    @Override // defpackage.td
    public boolean h() {
        return true;
    }

    @Override // defpackage.td
    public int[] i() {
        return nativeGetFrameDurations();
    }

    @Override // defpackage.td
    public int j() {
        return nativeGetSizeInBytes();
    }

    @Override // defpackage.td
    /* renamed from: m */
    public WebPFrame g(int i) {
        return nativeGetFrame(i);
    }

    @cq0
    public WebPImage(long j) {
        this.mNativeContext = j;
    }
}
