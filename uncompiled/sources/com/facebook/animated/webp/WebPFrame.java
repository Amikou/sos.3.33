package com.facebook.animated.webp;

import android.graphics.Bitmap;

/* loaded from: classes.dex */
public class WebPFrame implements xd {
    @cq0
    private long mNativeContext;

    @cq0
    public WebPFrame(long j) {
        this.mNativeContext = j;
    }

    private native void nativeDispose();

    private native void nativeFinalize();

    private native int nativeGetDurationMs();

    private native int nativeGetHeight();

    private native int nativeGetWidth();

    private native int nativeGetXOffset();

    private native int nativeGetYOffset();

    private native boolean nativeIsBlendWithPreviousFrame();

    private native void nativeRenderFrame(int i, int i2, Bitmap bitmap);

    private native boolean nativeShouldDisposeToBackgroundColor();

    @Override // defpackage.xd
    public void a() {
        nativeDispose();
    }

    @Override // defpackage.xd
    public void b(int i, int i2, Bitmap bitmap) {
        nativeRenderFrame(i, i2, bitmap);
    }

    @Override // defpackage.xd
    public int c() {
        return nativeGetXOffset();
    }

    @Override // defpackage.xd
    public int d() {
        return nativeGetYOffset();
    }

    public boolean e() {
        return nativeIsBlendWithPreviousFrame();
    }

    public boolean f() {
        return nativeShouldDisposeToBackgroundColor();
    }

    public void finalize() {
        nativeFinalize();
    }

    @Override // defpackage.xd
    public int getHeight() {
        return nativeGetHeight();
    }

    @Override // defpackage.xd
    public int getWidth() {
        return nativeGetWidth();
    }
}
