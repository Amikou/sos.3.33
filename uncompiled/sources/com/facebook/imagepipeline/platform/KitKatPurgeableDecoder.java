package com.facebook.imagepipeline.platform;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.facebook.common.memory.PooledByteBuffer;
import com.facebook.imagepipeline.nativecode.DalvikPurgeableDecoder;

@cq0
@TargetApi(19)
/* loaded from: classes.dex */
public class KitKatPurgeableDecoder extends DalvikPurgeableDecoder {
    public static final /* synthetic */ int d = 0;
    public final y61 c;

    @cq0
    public KitKatPurgeableDecoder(y61 y61Var) {
        this.c = y61Var;
    }

    public static void i(byte[] bArr, int i) {
        bArr[i] = -1;
        bArr[i + 1] = -39;
    }

    @Override // com.facebook.imagepipeline.nativecode.DalvikPurgeableDecoder
    public Bitmap d(com.facebook.common.references.a<PooledByteBuffer> aVar, BitmapFactory.Options options) {
        PooledByteBuffer j = aVar.j();
        int size = j.size();
        com.facebook.common.references.a<byte[]> a = this.c.a(size);
        try {
            byte[] j2 = a.j();
            j.s(0, j2, 0, size);
            return (Bitmap) xt2.h(BitmapFactory.decodeByteArray(j2, 0, size, options), "BitmapFactory returned null");
        } finally {
            com.facebook.common.references.a.g(a);
        }
    }

    @Override // com.facebook.imagepipeline.nativecode.DalvikPurgeableDecoder
    public Bitmap e(com.facebook.common.references.a<PooledByteBuffer> aVar, int i, BitmapFactory.Options options) {
        byte[] bArr = DalvikPurgeableDecoder.f(aVar, i) ? null : DalvikPurgeableDecoder.b;
        PooledByteBuffer j = aVar.j();
        xt2.b(Boolean.valueOf(i <= j.size()));
        int i2 = i + 2;
        com.facebook.common.references.a<byte[]> a = this.c.a(i2);
        try {
            byte[] j2 = a.j();
            j.s(0, j2, 0, i);
            if (bArr != null) {
                i(j2, i);
                i = i2;
            }
            return (Bitmap) xt2.h(BitmapFactory.decodeByteArray(j2, 0, i, options), "BitmapFactory returned null");
        } finally {
            com.facebook.common.references.a.g(a);
        }
    }
}
