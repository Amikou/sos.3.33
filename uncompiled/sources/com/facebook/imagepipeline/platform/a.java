package com.facebook.imagepipeline.platform;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ColorSpace;
import android.graphics.Rect;
import android.os.Build;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import okhttp3.internal.http2.Http2;

/* compiled from: DefaultDecoder.java */
@TargetApi(11)
/* loaded from: classes.dex */
public abstract class a implements dr2 {
    public static final Class<?> d = a.class;
    public static final byte[] e = {-1, -39};
    public final iq a;
    public final PreverificationHelper b;
    public final it2<ByteBuffer> c;

    public a(iq iqVar, int i, it2 it2Var) {
        this.b = Build.VERSION.SDK_INT >= 26 ? new PreverificationHelper() : null;
        this.a = iqVar;
        this.c = it2Var;
        for (int i2 = 0; i2 < i; i2++) {
            this.c.a(ByteBuffer.allocate(Http2.INITIAL_MAX_FRAME_SIZE));
        }
    }

    public static BitmapFactory.Options f(zu0 zu0Var, Bitmap.Config config) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = zu0Var.r();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(zu0Var.m(), null, options);
        if (options.outWidth != -1 && options.outHeight != -1) {
            options.inJustDecodeBounds = false;
            options.inDither = true;
            options.inPreferredConfig = config;
            options.inMutable = true;
            return options;
        }
        throw new IllegalArgumentException();
    }

    @Override // defpackage.dr2
    public com.facebook.common.references.a<Bitmap> a(zu0 zu0Var, Bitmap.Config config, Rect rect, int i) {
        return c(zu0Var, config, rect, i, null);
    }

    @Override // defpackage.dr2
    public com.facebook.common.references.a<Bitmap> b(zu0 zu0Var, Bitmap.Config config, Rect rect, ColorSpace colorSpace) {
        BitmapFactory.Options f = f(zu0Var, config);
        boolean z = f.inPreferredConfig != Bitmap.Config.ARGB_8888;
        try {
            return d((InputStream) xt2.g(zu0Var.m()), f, rect, colorSpace);
        } catch (RuntimeException e2) {
            if (z) {
                return b(zu0Var, Bitmap.Config.ARGB_8888, rect, colorSpace);
            }
            throw e2;
        }
    }

    @Override // defpackage.dr2
    public com.facebook.common.references.a<Bitmap> c(zu0 zu0Var, Bitmap.Config config, Rect rect, int i, ColorSpace colorSpace) {
        boolean z = zu0Var.z(i);
        BitmapFactory.Options f = f(zu0Var, config);
        h34 m = zu0Var.m();
        xt2.g(m);
        if (zu0Var.u() > i) {
            m = new vz1(m, i);
        }
        if (!z) {
            m = new h34(m, e);
        }
        boolean z2 = f.inPreferredConfig != Bitmap.Config.ARGB_8888;
        try {
            try {
                com.facebook.common.references.a<Bitmap> d2 = d(m, f, rect, colorSpace);
                try {
                    m.close();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
                return d2;
            } catch (Throwable th) {
                try {
                    m.close();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
                throw th;
            }
        } catch (RuntimeException e4) {
            if (z2) {
                com.facebook.common.references.a<Bitmap> c = c(zu0Var, Bitmap.Config.ARGB_8888, rect, i, colorSpace);
                try {
                    m.close();
                } catch (IOException e5) {
                    e5.printStackTrace();
                }
                return c;
            }
            throw e4;
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:51:0x00ac A[Catch: all -> 0x00cf, RuntimeException -> 0x00d1, IllegalArgumentException -> 0x00da, TRY_LEAVE, TryCatch #8 {IllegalArgumentException -> 0x00da, RuntimeException -> 0x00d1, blocks: (B:29:0x006e, B:36:0x0089, B:51:0x00ac, B:43:0x009d, B:47:0x00a5, B:48:0x00a8), top: B:83:0x006e, outer: #7 }] */
    /* JADX WARN: Removed duplicated region for block: B:54:0x00b7 A[ADDED_TO_REGION] */
    /* JADX WARN: Type inference failed for: r0v1, types: [int] */
    /* JADX WARN: Type inference failed for: r0v7 */
    /* JADX WARN: Type inference failed for: r0v8 */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final com.facebook.common.references.a<android.graphics.Bitmap> d(java.io.InputStream r10, android.graphics.BitmapFactory.Options r11, android.graphics.Rect r12, android.graphics.ColorSpace r13) {
        /*
            Method dump skipped, instructions count: 257
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.imagepipeline.platform.a.d(java.io.InputStream, android.graphics.BitmapFactory$Options, android.graphics.Rect, android.graphics.ColorSpace):com.facebook.common.references.a");
    }

    public abstract int e(int i, int i2, BitmapFactory.Options options);
}
