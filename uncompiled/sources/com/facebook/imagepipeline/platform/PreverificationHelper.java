package com.facebook.imagepipeline.platform;

import android.annotation.TargetApi;
import android.graphics.Bitmap;

@bq0
/* loaded from: classes.dex */
class PreverificationHelper {
    @TargetApi(26)
    @bq0
    public boolean shouldUseHardwareBitmapConfig(Bitmap.Config config) {
        return config == Bitmap.Config.HARDWARE;
    }
}
