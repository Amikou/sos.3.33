package com.facebook.imagepipeline.platform;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.MemoryFile;
import com.facebook.common.internal.b;
import com.facebook.common.internal.d;
import com.facebook.common.memory.PooledByteBuffer;
import com.facebook.imagepipeline.nativecode.DalvikPurgeableDecoder;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Method;

@cq0
/* loaded from: classes.dex */
public class GingerbreadPurgeableDecoder extends DalvikPurgeableDecoder {
    public static Method d;
    public final oo4 c = po4.i();

    public static MemoryFile i(com.facebook.common.references.a<PooledByteBuffer> aVar, int i, byte[] bArr) throws IOException {
        OutputStream outputStream;
        vz1 vz1Var;
        bt2 bt2Var = null;
        OutputStream outputStream2 = null;
        MemoryFile memoryFile = new MemoryFile(null, (bArr == null ? 0 : bArr.length) + i);
        memoryFile.allowPurging(false);
        try {
            bt2 bt2Var2 = new bt2(aVar.j());
            try {
                vz1Var = new vz1(bt2Var2, i);
            } catch (Throwable th) {
                th = th;
                outputStream = null;
                vz1Var = null;
            }
            try {
                outputStream2 = memoryFile.getOutputStream();
                com.facebook.common.internal.a.a(vz1Var, outputStream2);
                if (bArr != null) {
                    memoryFile.writeBytes(bArr, 0, i, bArr.length);
                }
                com.facebook.common.references.a.g(aVar);
                b.b(bt2Var2);
                b.b(vz1Var);
                b.a(outputStream2, true);
                return memoryFile;
            } catch (Throwable th2) {
                th = th2;
                outputStream = outputStream2;
                bt2Var = bt2Var2;
                com.facebook.common.references.a.g(aVar);
                b.b(bt2Var);
                b.b(vz1Var);
                b.a(outputStream, true);
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            outputStream = null;
            vz1Var = null;
        }
    }

    @Override // com.facebook.imagepipeline.nativecode.DalvikPurgeableDecoder
    public Bitmap d(com.facebook.common.references.a<PooledByteBuffer> aVar, BitmapFactory.Options options) {
        return j(aVar, aVar.j().size(), null, options);
    }

    @Override // com.facebook.imagepipeline.nativecode.DalvikPurgeableDecoder
    public Bitmap e(com.facebook.common.references.a<PooledByteBuffer> aVar, int i, BitmapFactory.Options options) {
        return j(aVar, i, DalvikPurgeableDecoder.f(aVar, i) ? null : DalvikPurgeableDecoder.b, options);
    }

    public final Bitmap j(com.facebook.common.references.a<PooledByteBuffer> aVar, int i, byte[] bArr, BitmapFactory.Options options) {
        MemoryFile memoryFile = null;
        try {
            try {
                MemoryFile i2 = i(aVar, i, bArr);
                try {
                    FileDescriptor l = l(i2);
                    oo4 oo4Var = this.c;
                    if (oo4Var != null) {
                        Bitmap bitmap = (Bitmap) xt2.h(oo4Var.a(l, null, options), "BitmapFactory returned null");
                        if (i2 != null) {
                            i2.close();
                        }
                        return bitmap;
                    }
                    throw new IllegalStateException("WebpBitmapFactory is null");
                } catch (IOException e) {
                    e = e;
                    memoryFile = i2;
                    throw d.a(e);
                } catch (Throwable th) {
                    th = th;
                    memoryFile = i2;
                    if (memoryFile != null) {
                        memoryFile.close();
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
            }
        } catch (IOException e2) {
            e = e2;
        }
    }

    public final synchronized Method k() {
        if (d == null) {
            try {
                d = MemoryFile.class.getDeclaredMethod("getFileDescriptor", new Class[0]);
            } catch (Exception e) {
                throw d.a(e);
            }
        }
        return d;
    }

    public final FileDescriptor l(MemoryFile memoryFile) {
        try {
            return (FileDescriptor) xt2.g(k().invoke(memoryFile, new Object[0]));
        } catch (Exception e) {
            throw d.a(e);
        }
    }
}
