package com.facebook.imagepipeline.producers;

import com.facebook.common.internal.ImmutableMap;
import com.facebook.imagepipeline.request.ImageRequest;
import java.util.Map;
import java.util.concurrent.CancellationException;
import java.util.concurrent.atomic.AtomicBoolean;

/* compiled from: DiskCacheReadProducer.java */
/* loaded from: classes.dex */
public class c implements dv2<zu0> {
    public final xr a;
    public final xr b;
    public final xt c;
    public final dv2<zu0> d;

    /* compiled from: DiskCacheReadProducer.java */
    /* loaded from: classes.dex */
    public class a implements bolts.a<zu0, Void> {
        public final /* synthetic */ iv2 a;
        public final /* synthetic */ ev2 b;
        public final /* synthetic */ l60 c;

        public a(iv2 iv2Var, ev2 ev2Var, l60 l60Var) {
            this.a = iv2Var;
            this.b = ev2Var;
            this.c = l60Var;
        }

        @Override // bolts.a
        /* renamed from: b */
        public Void a(bolts.b<zu0> bVar) throws Exception {
            if (c.e(bVar)) {
                this.a.d(this.b, "DiskCacheProducer", null);
                this.c.b();
            } else if (bVar.n()) {
                this.a.c(this.b, "DiskCacheProducer", bVar.i(), null);
                c.this.d.a(this.c, this.b);
            } else {
                zu0 j = bVar.j();
                if (j != null) {
                    iv2 iv2Var = this.a;
                    ev2 ev2Var = this.b;
                    iv2Var.a(ev2Var, "DiskCacheProducer", c.d(iv2Var, ev2Var, true, j.u()));
                    this.a.e(this.b, "DiskCacheProducer", true);
                    this.b.k("disk");
                    this.c.c(1.0f);
                    this.c.d(j, 1);
                    j.close();
                } else {
                    iv2 iv2Var2 = this.a;
                    ev2 ev2Var2 = this.b;
                    iv2Var2.a(ev2Var2, "DiskCacheProducer", c.d(iv2Var2, ev2Var2, false, 0));
                    c.this.d.a(this.c, this.b);
                }
            }
            return null;
        }
    }

    /* compiled from: DiskCacheReadProducer.java */
    /* loaded from: classes.dex */
    public class b extends vn {
        public final /* synthetic */ AtomicBoolean a;

        public b(c cVar, AtomicBoolean atomicBoolean) {
            this.a = atomicBoolean;
        }

        @Override // defpackage.fv2
        public void a() {
            this.a.set(true);
        }
    }

    public c(xr xrVar, xr xrVar2, xt xtVar, dv2<zu0> dv2Var) {
        this.a = xrVar;
        this.b = xrVar2;
        this.c = xtVar;
        this.d = dv2Var;
    }

    public static Map<String, String> d(iv2 iv2Var, ev2 ev2Var, boolean z, int i) {
        if (iv2Var.j(ev2Var, "DiskCacheProducer")) {
            if (z) {
                return ImmutableMap.of("cached_value_found", String.valueOf(z), "encodedImageSize", String.valueOf(i));
            }
            return ImmutableMap.of("cached_value_found", String.valueOf(z));
        }
        return null;
    }

    public static boolean e(bolts.b<?> bVar) {
        return bVar.l() || (bVar.n() && (bVar.i() instanceof CancellationException));
    }

    @Override // defpackage.dv2
    public void a(l60<zu0> l60Var, ev2 ev2Var) {
        ImageRequest c = ev2Var.c();
        if (!ev2Var.c().x(16)) {
            f(l60Var, ev2Var);
            return;
        }
        ev2Var.l().k(ev2Var, "DiskCacheProducer");
        wt d = this.c.d(c, ev2Var.a());
        xr xrVar = c.d() == ImageRequest.CacheChoice.SMALL ? this.b : this.a;
        AtomicBoolean atomicBoolean = new AtomicBoolean(false);
        xrVar.j(d, atomicBoolean).e(g(l60Var, ev2Var));
        h(atomicBoolean, ev2Var);
    }

    public final void f(l60<zu0> l60Var, ev2 ev2Var) {
        if (ev2Var.n().getValue() >= ImageRequest.RequestLevel.DISK_CACHE.getValue()) {
            ev2Var.f("disk", "nil-result_read");
            l60Var.d(null, 1);
            return;
        }
        this.d.a(l60Var, ev2Var);
    }

    public final bolts.a<zu0, Void> g(l60<zu0> l60Var, ev2 ev2Var) {
        return new a(ev2Var.l(), ev2Var, l60Var);
    }

    public final void h(AtomicBoolean atomicBoolean, ev2 ev2Var) {
        ev2Var.o(new b(this, atomicBoolean));
    }
}
