package com.facebook.imagepipeline.producers;

import com.facebook.imagepipeline.request.ImageRequest;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.Executor;

/* compiled from: LocalFetchProducer.java */
/* loaded from: classes.dex */
public abstract class h implements dv2<zu0> {
    public final Executor a;
    public final com.facebook.common.memory.b b;

    /* compiled from: LocalFetchProducer.java */
    /* loaded from: classes.dex */
    public class a extends gt3<zu0> {
        public final /* synthetic */ ImageRequest j0;
        public final /* synthetic */ iv2 k0;
        public final /* synthetic */ ev2 l0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(l60 l60Var, iv2 iv2Var, ev2 ev2Var, String str, ImageRequest imageRequest, iv2 iv2Var2, ev2 ev2Var2) {
            super(l60Var, iv2Var, ev2Var, str);
            this.j0 = imageRequest;
            this.k0 = iv2Var2;
            this.l0 = ev2Var2;
        }

        @Override // defpackage.ht3
        /* renamed from: j */
        public void b(zu0 zu0Var) {
            zu0.c(zu0Var);
        }

        @Override // defpackage.ht3
        /* renamed from: k */
        public zu0 c() throws Exception {
            zu0 c = h.this.c(this.j0);
            if (c == null) {
                this.k0.e(this.l0, h.this.e(), false);
                this.l0.k("local");
                return null;
            }
            c.M();
            this.k0.e(this.l0, h.this.e(), true);
            this.l0.k("local");
            return c;
        }
    }

    /* compiled from: LocalFetchProducer.java */
    /* loaded from: classes.dex */
    public class b extends vn {
        public final /* synthetic */ gt3 a;

        public b(h hVar, gt3 gt3Var) {
            this.a = gt3Var;
        }

        @Override // defpackage.fv2
        public void a() {
            this.a.a();
        }
    }

    public h(Executor executor, com.facebook.common.memory.b bVar) {
        this.a = executor;
        this.b = bVar;
    }

    @Override // defpackage.dv2
    public void a(l60<zu0> l60Var, ev2 ev2Var) {
        iv2 l = ev2Var.l();
        ImageRequest c = ev2Var.c();
        ev2Var.f("local", "fetch");
        a aVar = new a(l60Var, l, ev2Var, e(), c, l, ev2Var);
        ev2Var.o(new b(this, aVar));
        this.a.execute(aVar);
    }

    public zu0 b(InputStream inputStream, int i) throws IOException {
        com.facebook.common.references.a v;
        com.facebook.common.references.a aVar = null;
        try {
            if (i <= 0) {
                v = com.facebook.common.references.a.v(this.b.c(inputStream));
            } else {
                v = com.facebook.common.references.a.v(this.b.d(inputStream, i));
            }
            aVar = v;
            return new zu0(aVar);
        } finally {
            com.facebook.common.internal.b.b(inputStream);
            com.facebook.common.references.a.g(aVar);
        }
    }

    public abstract zu0 c(ImageRequest imageRequest) throws IOException;

    public zu0 d(InputStream inputStream, int i) throws IOException {
        return b(inputStream, i);
    }

    public abstract String e();
}
