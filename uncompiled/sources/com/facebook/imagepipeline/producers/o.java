package com.facebook.imagepipeline.producers;

import android.net.Uri;
import com.facebook.common.internal.ImmutableMap;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;
import java.util.concurrent.CancellationException;
import java.util.concurrent.atomic.AtomicBoolean;
import okhttp3.internal.http2.Http2;

/* compiled from: PartialDiskCacheProducer.java */
/* loaded from: classes.dex */
public class o implements dv2<zu0> {
    public final xr a;
    public final xt b;
    public final com.facebook.common.memory.b c;
    public final os d;
    public final dv2<zu0> e;

    /* compiled from: PartialDiskCacheProducer.java */
    /* loaded from: classes.dex */
    public class a implements bolts.a<zu0, Void> {
        public final /* synthetic */ iv2 a;
        public final /* synthetic */ ev2 b;
        public final /* synthetic */ l60 c;
        public final /* synthetic */ wt d;

        public a(iv2 iv2Var, ev2 ev2Var, l60 l60Var, wt wtVar) {
            this.a = iv2Var;
            this.b = ev2Var;
            this.c = l60Var;
            this.d = wtVar;
        }

        @Override // bolts.a
        /* renamed from: b */
        public Void a(bolts.b<zu0> bVar) throws Exception {
            if (o.f(bVar)) {
                this.a.d(this.b, "PartialDiskCacheProducer", null);
                this.c.b();
            } else if (bVar.n()) {
                this.a.c(this.b, "PartialDiskCacheProducer", bVar.i(), null);
                o.this.h(this.c, this.b, this.d, null);
            } else {
                zu0 j = bVar.j();
                if (j != null) {
                    iv2 iv2Var = this.a;
                    ev2 ev2Var = this.b;
                    iv2Var.a(ev2Var, "PartialDiskCacheProducer", o.e(iv2Var, ev2Var, true, j.u()));
                    ct c = ct.c(j.u() - 1);
                    j.S(c);
                    int u = j.u();
                    ImageRequest c2 = this.b.c();
                    if (c.a(c2.c())) {
                        this.b.f("disk", "partial");
                        this.a.e(this.b, "PartialDiskCacheProducer", true);
                        this.c.d(j, 9);
                    } else {
                        this.c.d(j, 8);
                        o.this.h(this.c, new xm3(ImageRequestBuilder.b(c2).v(ct.b(u - 1)).a(), this.b), this.d, j);
                    }
                } else {
                    iv2 iv2Var2 = this.a;
                    ev2 ev2Var2 = this.b;
                    iv2Var2.a(ev2Var2, "PartialDiskCacheProducer", o.e(iv2Var2, ev2Var2, false, 0));
                    o.this.h(this.c, this.b, this.d, j);
                }
            }
            return null;
        }
    }

    /* compiled from: PartialDiskCacheProducer.java */
    /* loaded from: classes.dex */
    public class b extends vn {
        public final /* synthetic */ AtomicBoolean a;

        public b(o oVar, AtomicBoolean atomicBoolean) {
            this.a = atomicBoolean;
        }

        @Override // defpackage.fv2
        public void a() {
            this.a.set(true);
        }
    }

    /* compiled from: PartialDiskCacheProducer.java */
    /* loaded from: classes.dex */
    public static class c extends bm0<zu0, zu0> {
        public final xr c;
        public final wt d;
        public final com.facebook.common.memory.b e;
        public final os f;
        public final zu0 g;
        public final boolean h;

        public /* synthetic */ c(l60 l60Var, xr xrVar, wt wtVar, com.facebook.common.memory.b bVar, os osVar, zu0 zu0Var, boolean z, a aVar) {
            this(l60Var, xrVar, wtVar, bVar, osVar, zu0Var, z);
        }

        public final void q(InputStream inputStream, OutputStream outputStream, int i) throws IOException {
            byte[] bArr = this.f.get(Http2.INITIAL_MAX_FRAME_SIZE);
            int i2 = i;
            while (i2 > 0) {
                try {
                    int read = inputStream.read(bArr, 0, Math.min((int) Http2.INITIAL_MAX_FRAME_SIZE, i2));
                    if (read < 0) {
                        break;
                    } else if (read > 0) {
                        outputStream.write(bArr, 0, read);
                        i2 -= read;
                    }
                } finally {
                    this.f.a(bArr);
                }
            }
            if (i2 > 0) {
                throw new IOException(String.format(null, "Failed to read %d bytes - finished %d short", Integer.valueOf(i), Integer.valueOf(i2)));
            }
        }

        public final ct2 r(zu0 zu0Var, zu0 zu0Var2) throws IOException {
            int i = ((ct) xt2.g(zu0Var2.f())).a;
            ct2 e = this.e.e(zu0Var2.u() + i);
            q(zu0Var.n(), e, i);
            q(zu0Var2.n(), e, zu0Var2.u());
            return e;
        }

        @Override // defpackage.qm
        /* renamed from: s */
        public void i(zu0 zu0Var, int i) {
            if (qm.f(i)) {
                return;
            }
            if (this.g != null && zu0Var != null && zu0Var.f() != null) {
                try {
                    try {
                        t(r(this.g, zu0Var));
                    } catch (IOException e) {
                        v11.i("PartialDiskCacheProducer", "Error while merging image data", e);
                        p().a(e);
                    }
                    this.c.n(this.d);
                } finally {
                    zu0Var.close();
                    this.g.close();
                }
            } else if (this.h && qm.n(i, 8) && qm.e(i) && zu0Var != null && zu0Var.l() != wn1.b) {
                this.c.l(this.d, zu0Var);
                p().d(zu0Var, i);
            } else {
                p().d(zu0Var, i);
            }
        }

        public final void t(ct2 ct2Var) {
            zu0 zu0Var;
            Throwable th;
            com.facebook.common.references.a v = com.facebook.common.references.a.v(ct2Var.a());
            try {
                zu0Var = new zu0(v);
            } catch (Throwable th2) {
                zu0Var = null;
                th = th2;
            }
            try {
                zu0Var.M();
                p().d(zu0Var, 1);
                zu0.c(zu0Var);
                com.facebook.common.references.a.g(v);
            } catch (Throwable th3) {
                th = th3;
                zu0.c(zu0Var);
                com.facebook.common.references.a.g(v);
                throw th;
            }
        }

        public c(l60<zu0> l60Var, xr xrVar, wt wtVar, com.facebook.common.memory.b bVar, os osVar, zu0 zu0Var, boolean z) {
            super(l60Var);
            this.c = xrVar;
            this.d = wtVar;
            this.e = bVar;
            this.f = osVar;
            this.g = zu0Var;
            this.h = z;
        }
    }

    public o(xr xrVar, xt xtVar, com.facebook.common.memory.b bVar, os osVar, dv2<zu0> dv2Var) {
        this.a = xrVar;
        this.b = xtVar;
        this.c = bVar;
        this.d = osVar;
        this.e = dv2Var;
    }

    public static Uri d(ImageRequest imageRequest) {
        return imageRequest.u().buildUpon().appendQueryParameter("fresco_partial", "true").build();
    }

    public static Map<String, String> e(iv2 iv2Var, ev2 ev2Var, boolean z, int i) {
        if (iv2Var.j(ev2Var, "PartialDiskCacheProducer")) {
            if (z) {
                return ImmutableMap.of("cached_value_found", String.valueOf(z), "encodedImageSize", String.valueOf(i));
            }
            return ImmutableMap.of("cached_value_found", String.valueOf(z));
        }
        return null;
    }

    public static boolean f(bolts.b<?> bVar) {
        return bVar.l() || (bVar.n() && (bVar.i() instanceof CancellationException));
    }

    @Override // defpackage.dv2
    public void a(l60<zu0> l60Var, ev2 ev2Var) {
        ImageRequest c2 = ev2Var.c();
        boolean x = ev2Var.c().x(16);
        iv2 l = ev2Var.l();
        l.k(ev2Var, "PartialDiskCacheProducer");
        wt b2 = this.b.b(c2, d(c2), ev2Var.a());
        if (!x) {
            l.a(ev2Var, "PartialDiskCacheProducer", e(l, ev2Var, false, 0));
            h(l60Var, ev2Var, b2, null);
            return;
        }
        AtomicBoolean atomicBoolean = new AtomicBoolean(false);
        this.a.j(b2, atomicBoolean).e(g(l60Var, ev2Var, b2));
        i(atomicBoolean, ev2Var);
    }

    public final bolts.a<zu0, Void> g(l60<zu0> l60Var, ev2 ev2Var, wt wtVar) {
        return new a(ev2Var.l(), ev2Var, l60Var, wtVar);
    }

    public final void h(l60<zu0> l60Var, ev2 ev2Var, wt wtVar, zu0 zu0Var) {
        this.e.a(new c(l60Var, this.a, wtVar, this.c, this.d, zu0Var, ev2Var.c().x(32), null), ev2Var);
    }

    public final void i(AtomicBoolean atomicBoolean, ev2 ev2Var) {
        ev2Var.o(new b(this, atomicBoolean));
    }
}
