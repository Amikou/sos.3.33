package com.facebook.imagepipeline.producers;

import com.facebook.common.util.TriState;
import java.io.InputStream;
import java.util.concurrent.Executor;

/* compiled from: WebpTranscodeProducer.java */
/* loaded from: classes.dex */
public class s implements dv2<zu0> {
    public final Executor a;
    public final com.facebook.common.memory.b b;
    public final dv2<zu0> c;

    /* compiled from: WebpTranscodeProducer.java */
    /* loaded from: classes.dex */
    public class a extends gt3<zu0> {
        public final /* synthetic */ zu0 j0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(l60 l60Var, iv2 iv2Var, ev2 ev2Var, String str, zu0 zu0Var) {
            super(l60Var, iv2Var, ev2Var, str);
            this.j0 = zu0Var;
        }

        @Override // defpackage.gt3, defpackage.ht3
        public void d() {
            zu0.c(this.j0);
            super.d();
        }

        @Override // defpackage.gt3, defpackage.ht3
        public void e(Exception exc) {
            zu0.c(this.j0);
            super.e(exc);
        }

        @Override // defpackage.ht3
        /* renamed from: j */
        public void b(zu0 zu0Var) {
            zu0.c(zu0Var);
        }

        @Override // defpackage.ht3
        /* renamed from: k */
        public zu0 c() throws Exception {
            ct2 a = s.this.b.a();
            try {
                s.f(this.j0, a);
                com.facebook.common.references.a v = com.facebook.common.references.a.v(a.a());
                zu0 zu0Var = new zu0(v);
                zu0Var.d(this.j0);
                com.facebook.common.references.a.g(v);
                return zu0Var;
            } finally {
                a.close();
            }
        }

        @Override // defpackage.gt3, defpackage.ht3
        /* renamed from: l */
        public void f(zu0 zu0Var) {
            zu0.c(this.j0);
            super.f(zu0Var);
        }
    }

    /* compiled from: WebpTranscodeProducer.java */
    /* loaded from: classes.dex */
    public class b extends bm0<zu0, zu0> {
        public final ev2 c;
        public TriState d;

        public b(l60<zu0> l60Var, ev2 ev2Var) {
            super(l60Var);
            this.c = ev2Var;
            this.d = TriState.UNSET;
        }

        @Override // defpackage.qm
        /* renamed from: q */
        public void i(zu0 zu0Var, int i) {
            if (this.d == TriState.UNSET && zu0Var != null) {
                this.d = s.g(zu0Var);
            }
            if (this.d == TriState.NO) {
                p().d(zu0Var, i);
            } else if (qm.e(i)) {
                if (this.d == TriState.YES && zu0Var != null) {
                    s.this.h(zu0Var, p(), this.c);
                } else {
                    p().d(zu0Var, i);
                }
            }
        }
    }

    public s(Executor executor, com.facebook.common.memory.b bVar, dv2<zu0> dv2Var) {
        this.a = (Executor) xt2.g(executor);
        this.b = (com.facebook.common.memory.b) xt2.g(bVar);
        this.c = (dv2) xt2.g(dv2Var);
    }

    public static void f(zu0 zu0Var, ct2 ct2Var) throws Exception {
        InputStream inputStream = (InputStream) xt2.g(zu0Var.m());
        wn1 c = xn1.c(inputStream);
        if (c != wj0.f && c != wj0.h) {
            if (c != wj0.g && c != wj0.i) {
                throw new IllegalArgumentException("Wrong image format");
            }
            qo4.a().b(inputStream, ct2Var);
            zu0Var.a0(wj0.b);
            return;
        }
        qo4.a().a(inputStream, ct2Var, 80);
        zu0Var.a0(wj0.a);
    }

    public static TriState g(zu0 zu0Var) {
        xt2.g(zu0Var);
        wn1 c = xn1.c((InputStream) xt2.g(zu0Var.m()));
        if (wj0.a(c)) {
            com.facebook.imagepipeline.nativecode.a a2 = qo4.a();
            if (a2 == null) {
                return TriState.NO;
            }
            return TriState.valueOf(!a2.c(c));
        } else if (c == wn1.b) {
            return TriState.UNSET;
        } else {
            return TriState.NO;
        }
    }

    @Override // defpackage.dv2
    public void a(l60<zu0> l60Var, ev2 ev2Var) {
        this.c.a(new b(l60Var, ev2Var), ev2Var);
    }

    public final void h(zu0 zu0Var, l60<zu0> l60Var, ev2 ev2Var) {
        xt2.g(zu0Var);
        this.a.execute(new a(l60Var, ev2Var.l(), ev2Var, "WebpTranscodeProducer", zu0.b(zu0Var)));
    }
}
