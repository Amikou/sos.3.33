package com.facebook.imagepipeline.producers;

import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.os.CancellationSignal;
import android.util.Size;
import com.facebook.common.internal.ImmutableMap;
import com.facebook.imagepipeline.request.ImageRequest;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.Executor;

/* compiled from: LocalThumbnailBitmapProducer.java */
/* loaded from: classes.dex */
public class k implements dv2<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> {
    public final Executor a;
    public final ContentResolver b;

    /* compiled from: LocalThumbnailBitmapProducer.java */
    /* loaded from: classes.dex */
    public class a extends gt3<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> {
        public final /* synthetic */ iv2 j0;
        public final /* synthetic */ ev2 k0;
        public final /* synthetic */ ImageRequest l0;
        public final /* synthetic */ CancellationSignal m0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(l60 l60Var, iv2 iv2Var, ev2 ev2Var, String str, iv2 iv2Var2, ev2 ev2Var2, ImageRequest imageRequest, CancellationSignal cancellationSignal) {
            super(l60Var, iv2Var, ev2Var, str);
            this.j0 = iv2Var2;
            this.k0 = ev2Var2;
            this.l0 = imageRequest;
            this.m0 = cancellationSignal;
        }

        @Override // defpackage.gt3, defpackage.ht3
        public void d() {
            super.d();
            this.m0.cancel();
        }

        @Override // defpackage.gt3, defpackage.ht3
        public void e(Exception exc) {
            super.e(exc);
            this.j0.e(this.k0, "LocalThumbnailBitmapProducer", false);
            this.k0.k("local");
        }

        @Override // defpackage.ht3
        /* renamed from: j */
        public void b(com.facebook.common.references.a<com.facebook.imagepipeline.image.a> aVar) {
            com.facebook.common.references.a.g(aVar);
        }

        @Override // defpackage.gt3
        /* renamed from: k */
        public Map<String, String> i(com.facebook.common.references.a<com.facebook.imagepipeline.image.a> aVar) {
            return ImmutableMap.of("createdThumbnail", String.valueOf(aVar != null));
        }

        @Override // defpackage.ht3
        /* renamed from: l */
        public com.facebook.common.references.a<com.facebook.imagepipeline.image.a> c() throws IOException {
            Bitmap loadThumbnail = k.this.b.loadThumbnail(this.l0.u(), new Size(this.l0.m(), this.l0.l()), this.m0);
            if (loadThumbnail == null) {
                return null;
            }
            c00 c00Var = new c00(loadThumbnail, yo3.b(), jp1.d, 0);
            this.k0.b("image_format", "thumbnail");
            c00Var.e(this.k0.getExtras());
            return com.facebook.common.references.a.v(c00Var);
        }

        @Override // defpackage.gt3, defpackage.ht3
        /* renamed from: m */
        public void f(com.facebook.common.references.a<com.facebook.imagepipeline.image.a> aVar) {
            super.f(aVar);
            this.j0.e(this.k0, "LocalThumbnailBitmapProducer", aVar != null);
            this.k0.k("local");
        }
    }

    /* compiled from: LocalThumbnailBitmapProducer.java */
    /* loaded from: classes.dex */
    public class b extends vn {
        public final /* synthetic */ gt3 a;

        public b(k kVar, gt3 gt3Var) {
            this.a = gt3Var;
        }

        @Override // defpackage.fv2
        public void a() {
            this.a.a();
        }
    }

    public k(Executor executor, ContentResolver contentResolver) {
        this.a = executor;
        this.b = contentResolver;
    }

    @Override // defpackage.dv2
    public void a(l60<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> l60Var, ev2 ev2Var) {
        iv2 l = ev2Var.l();
        ImageRequest c = ev2Var.c();
        ev2Var.f("local", "thumbnail_bitmap");
        a aVar = new a(l60Var, l, ev2Var, "LocalThumbnailBitmapProducer", l, ev2Var, c, new CancellationSignal());
        ev2Var.o(new b(this, aVar));
        this.a.execute(aVar);
    }
}
