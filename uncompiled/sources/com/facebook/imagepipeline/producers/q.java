package com.facebook.imagepipeline.producers;

import com.facebook.common.internal.ImmutableMap;
import com.facebook.common.util.TriState;
import com.facebook.imagepipeline.producers.JobScheduler;
import com.facebook.imagepipeline.request.ImageRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;

/* compiled from: ResizeAndRotateProducer.java */
/* loaded from: classes.dex */
public class q implements dv2<zu0> {
    public final Executor a;
    public final com.facebook.common.memory.b b;
    public final dv2<zu0> c;
    public final boolean d;
    public final bp1 e;

    /* compiled from: ResizeAndRotateProducer.java */
    /* loaded from: classes.dex */
    public class a extends bm0<zu0, zu0> {
        public final boolean c;
        public final bp1 d;
        public final ev2 e;
        public boolean f;
        public final JobScheduler g;

        /* compiled from: ResizeAndRotateProducer.java */
        /* renamed from: com.facebook.imagepipeline.producers.q$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public class C0088a implements JobScheduler.d {
            public C0088a(q qVar) {
            }

            @Override // com.facebook.imagepipeline.producers.JobScheduler.d
            public void a(zu0 zu0Var, int i) {
                a aVar = a.this;
                aVar.w(zu0Var, i, (ap1) xt2.g(aVar.d.createImageTranscoder(zu0Var.l(), a.this.c)));
            }
        }

        /* compiled from: ResizeAndRotateProducer.java */
        /* loaded from: classes.dex */
        public class b extends vn {
            public final /* synthetic */ l60 a;

            public b(q qVar, l60 l60Var) {
                this.a = l60Var;
            }

            @Override // defpackage.fv2
            public void a() {
                a.this.g.c();
                a.this.f = true;
                this.a.b();
            }

            @Override // defpackage.vn, defpackage.fv2
            public void b() {
                if (a.this.e.m()) {
                    a.this.g.h();
                }
            }
        }

        public a(l60<zu0> l60Var, ev2 ev2Var, boolean z, bp1 bp1Var) {
            super(l60Var);
            this.f = false;
            this.e = ev2Var;
            Boolean r = ev2Var.c().r();
            this.c = r != null ? r.booleanValue() : z;
            this.d = bp1Var;
            this.g = new JobScheduler(q.this.a, new C0088a(q.this), 100);
            ev2Var.o(new b(q.this, l60Var));
        }

        public final zu0 A(zu0 zu0Var) {
            p93 s = this.e.c().s();
            return (s.f() || !s.e()) ? zu0Var : y(zu0Var, s.d());
        }

        public final zu0 B(zu0 zu0Var) {
            return (this.e.c().s().c() || zu0Var.q() == 0 || zu0Var.q() == -1) ? zu0Var : y(zu0Var, 0);
        }

        @Override // defpackage.qm
        /* renamed from: C */
        public void i(zu0 zu0Var, int i) {
            if (this.f) {
                return;
            }
            boolean e = qm.e(i);
            if (zu0Var == null) {
                if (e) {
                    p().d(null, 1);
                    return;
                }
                return;
            }
            wn1 l = zu0Var.l();
            TriState g = q.g(this.e.c(), zu0Var, (ap1) xt2.g(this.d.createImageTranscoder(l, this.c)));
            if (e || g != TriState.UNSET) {
                if (g != TriState.YES) {
                    x(zu0Var, i, l);
                } else if (this.g.k(zu0Var, i)) {
                    if (e || this.e.m()) {
                        this.g.h();
                    }
                }
            }
        }

        public final void w(zu0 zu0Var, int i, ap1 ap1Var) {
            this.e.l().k(this.e, "ResizeAndRotateProducer");
            ImageRequest c = this.e.c();
            ct2 a = q.this.b.a();
            try {
                zo1 d = ap1Var.d(zu0Var, a, c.s(), c.q(), null, 85);
                if (d.a() != 2) {
                    Map<String, String> z = z(zu0Var, c.q(), d, ap1Var.a());
                    com.facebook.common.references.a v = com.facebook.common.references.a.v(a.a());
                    try {
                        zu0 zu0Var2 = new zu0(v);
                        zu0Var2.a0(wj0.a);
                        zu0Var2.M();
                        this.e.l().a(this.e, "ResizeAndRotateProducer", z);
                        if (d.a() != 1) {
                            i |= 16;
                        }
                        p().d(zu0Var2, i);
                        zu0.c(zu0Var2);
                        return;
                    } finally {
                        com.facebook.common.references.a.g(v);
                    }
                }
                throw new RuntimeException("Error while transcoding the image");
            } catch (Exception e) {
                this.e.l().c(this.e, "ResizeAndRotateProducer", e, null);
                if (qm.e(i)) {
                    p().a(e);
                }
            } finally {
                a.close();
            }
        }

        public final void x(zu0 zu0Var, int i, wn1 wn1Var) {
            zu0 B;
            if (wn1Var != wj0.a && wn1Var != wj0.k) {
                B = A(zu0Var);
            } else {
                B = B(zu0Var);
            }
            p().d(B, i);
        }

        public final zu0 y(zu0 zu0Var, int i) {
            zu0 b2 = zu0.b(zu0Var);
            if (b2 != null) {
                b2.b0(i);
            }
            return b2;
        }

        public final Map<String, String> z(zu0 zu0Var, p73 p73Var, zo1 zo1Var, String str) {
            if (this.e.l().j(this.e, "ResizeAndRotateProducer")) {
                HashMap hashMap = new HashMap();
                hashMap.put("Image format", String.valueOf(zu0Var.l()));
                hashMap.put("Original size", zu0Var.v() + "x" + zu0Var.j());
                hashMap.put("Requested size", "Unspecified");
                hashMap.put("queueTime", String.valueOf(this.g.f()));
                hashMap.put("Transcoder id", str);
                hashMap.put("Transcoding result", String.valueOf(zo1Var));
                return ImmutableMap.copyOf((Map) hashMap);
            }
            return null;
        }
    }

    public q(Executor executor, com.facebook.common.memory.b bVar, dv2<zu0> dv2Var, boolean z, bp1 bp1Var) {
        this.a = (Executor) xt2.g(executor);
        this.b = (com.facebook.common.memory.b) xt2.g(bVar);
        this.c = (dv2) xt2.g(dv2Var);
        this.e = (bp1) xt2.g(bp1Var);
        this.d = z;
    }

    public static boolean e(p93 p93Var, zu0 zu0Var) {
        return !p93Var.c() && (ou1.d(p93Var, zu0Var) != 0 || f(p93Var, zu0Var));
    }

    public static boolean f(p93 p93Var, zu0 zu0Var) {
        if (p93Var.e() && !p93Var.c()) {
            return ou1.a.contains(Integer.valueOf(zu0Var.h()));
        }
        zu0Var.W(0);
        return false;
    }

    public static TriState g(ImageRequest imageRequest, zu0 zu0Var, ap1 ap1Var) {
        if (zu0Var != null && zu0Var.l() != wn1.b) {
            if (!ap1Var.b(zu0Var.l())) {
                return TriState.NO;
            }
            return TriState.valueOf(e(imageRequest.s(), zu0Var) || ap1Var.c(zu0Var, imageRequest.s(), imageRequest.q()));
        }
        return TriState.UNSET;
    }

    @Override // defpackage.dv2
    public void a(l60<zu0> l60Var, ev2 ev2Var) {
        this.c.a(new a(l60Var, ev2Var, this.d, this.e), ev2Var);
    }
}
