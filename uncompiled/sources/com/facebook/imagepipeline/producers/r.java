package com.facebook.imagepipeline.producers;

import android.os.Looper;

/* compiled from: ThreadHandoffProducer.java */
/* loaded from: classes.dex */
public class r<T> implements dv2<T> {
    public final dv2<T> a;
    public final h54 b;

    /* compiled from: ThreadHandoffProducer.java */
    /* loaded from: classes.dex */
    public class a extends gt3<T> {
        public final /* synthetic */ iv2 j0;
        public final /* synthetic */ ev2 k0;
        public final /* synthetic */ l60 l0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(l60 l60Var, iv2 iv2Var, ev2 ev2Var, String str, iv2 iv2Var2, ev2 ev2Var2, l60 l60Var2) {
            super(l60Var, iv2Var, ev2Var, str);
            this.j0 = iv2Var2;
            this.k0 = ev2Var2;
            this.l0 = l60Var2;
        }

        @Override // defpackage.ht3
        public void b(T t) {
        }

        @Override // defpackage.ht3
        public T c() throws Exception {
            return null;
        }

        @Override // defpackage.gt3, defpackage.ht3
        public void f(T t) {
            this.j0.a(this.k0, "BackgroundThreadHandoffProducer", null);
            r.this.a.a(this.l0, this.k0);
        }
    }

    /* compiled from: ThreadHandoffProducer.java */
    /* loaded from: classes.dex */
    public class b extends vn {
        public final /* synthetic */ gt3 a;

        public b(gt3 gt3Var) {
            this.a = gt3Var;
        }

        @Override // defpackage.fv2
        public void a() {
            this.a.a();
            r.this.b.a(this.a);
        }
    }

    public r(dv2<T> dv2Var, h54 h54Var) {
        this.a = (dv2) xt2.g(dv2Var);
        this.b = h54Var;
    }

    public static String d(ev2 ev2Var) {
        if (mc1.b()) {
            return "ThreadHandoffProducer_produceResults_" + ev2Var.getId();
        }
        return null;
    }

    public static boolean e(ev2 ev2Var) {
        return ev2Var.d().C().o() && Looper.getMainLooper().getThread() != Thread.currentThread();
    }

    @Override // defpackage.dv2
    public void a(l60<T> l60Var, ev2 ev2Var) {
        boolean d;
        try {
            if (nc1.d()) {
                nc1.a("ThreadHandoffProducer#produceResults");
            }
            iv2 l = ev2Var.l();
            if (e(ev2Var)) {
                l.k(ev2Var, "BackgroundThreadHandoffProducer");
                l.a(ev2Var, "BackgroundThreadHandoffProducer", null);
                this.a.a(l60Var, ev2Var);
                if (d) {
                    return;
                }
                return;
            }
            a aVar = new a(l60Var, l, ev2Var, "BackgroundThreadHandoffProducer", l, ev2Var, l60Var);
            ev2Var.o(new b(aVar));
            this.b.b(mc1.a(aVar, d(ev2Var)));
            if (nc1.d()) {
                nc1.b();
            }
        } finally {
            if (nc1.d()) {
                nc1.b();
            }
        }
    }
}
