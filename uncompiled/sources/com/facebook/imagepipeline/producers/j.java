package com.facebook.imagepipeline.producers;

import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import com.facebook.imagepipeline.request.ImageRequest;
import java.io.IOException;
import java.util.concurrent.Executor;

/* compiled from: LocalResourceFetchProducer.java */
/* loaded from: classes.dex */
public class j extends h {
    public final Resources c;

    public j(Executor executor, com.facebook.common.memory.b bVar, Resources resources) {
        super(executor, bVar);
        this.c = resources;
    }

    public static int g(ImageRequest imageRequest) {
        String path = imageRequest.u().getPath();
        xt2.g(path);
        return Integer.parseInt(path.substring(1));
    }

    @Override // com.facebook.imagepipeline.producers.h
    public zu0 c(ImageRequest imageRequest) throws IOException {
        return d(this.c.openRawResource(g(imageRequest)), f(imageRequest));
    }

    @Override // com.facebook.imagepipeline.producers.h
    public String e() {
        return "LocalResourceFetchProducer";
    }

    public final int f(ImageRequest imageRequest) {
        AssetFileDescriptor assetFileDescriptor = null;
        try {
            assetFileDescriptor = this.c.openRawResourceFd(g(imageRequest));
            int length = (int) assetFileDescriptor.getLength();
            try {
                assetFileDescriptor.close();
            } catch (IOException unused) {
            }
            return length;
        } catch (Resources.NotFoundException unused2) {
            if (assetFileDescriptor != null) {
                try {
                    assetFileDescriptor.close();
                } catch (IOException unused3) {
                }
            }
            return -1;
        } catch (Throwable th) {
            if (assetFileDescriptor != null) {
                try {
                    assetFileDescriptor.close();
                } catch (IOException unused4) {
                }
            }
            throw th;
        }
    }
}
