package com.facebook.imagepipeline.producers;

import android.content.ContentResolver;
import android.content.res.AssetFileDescriptor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.provider.ContactsContract;
import com.facebook.imagepipeline.request.ImageRequest;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.Executor;

/* compiled from: LocalContentUriFetchProducer.java */
/* loaded from: classes.dex */
public class f extends h {
    public final ContentResolver c;

    public f(Executor executor, com.facebook.common.memory.b bVar, ContentResolver contentResolver) {
        super(executor, bVar);
        this.c = contentResolver;
    }

    @Override // com.facebook.imagepipeline.producers.h
    public zu0 c(ImageRequest imageRequest) throws IOException {
        zu0 f;
        InputStream createInputStream;
        Uri u = imageRequest.u();
        if (!qf4.h(u)) {
            return (!qf4.g(u) || (f = f(u)) == null) ? d((InputStream) xt2.g(this.c.openInputStream(u)), -1) : f;
        }
        if (u.toString().endsWith("/photo")) {
            createInputStream = this.c.openInputStream(u);
        } else if (u.toString().endsWith("/display_photo")) {
            try {
                AssetFileDescriptor openAssetFileDescriptor = this.c.openAssetFileDescriptor(u, "r");
                xt2.g(openAssetFileDescriptor);
                createInputStream = openAssetFileDescriptor.createInputStream();
            } catch (IOException unused) {
                throw new IOException("Contact photo does not exist: " + u);
            }
        } else {
            InputStream openContactPhotoInputStream = ContactsContract.Contacts.openContactPhotoInputStream(this.c, u);
            if (openContactPhotoInputStream == null) {
                throw new IOException("Contact photo does not exist: " + u);
            }
            createInputStream = openContactPhotoInputStream;
        }
        xt2.g(createInputStream);
        return d(createInputStream, -1);
    }

    @Override // com.facebook.imagepipeline.producers.h
    public String e() {
        return "LocalContentUriFetchProducer";
    }

    public final zu0 f(Uri uri) throws IOException {
        try {
            ParcelFileDescriptor openFileDescriptor = this.c.openFileDescriptor(uri, "r");
            xt2.g(openFileDescriptor);
            return d(new FileInputStream(openFileDescriptor.getFileDescriptor()), (int) openFileDescriptor.getStatSize());
        } catch (FileNotFoundException unused) {
            return null;
        }
    }
}
