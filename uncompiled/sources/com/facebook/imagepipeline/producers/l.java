package com.facebook.imagepipeline.producers;

import android.content.ContentResolver;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import com.facebook.common.internal.ImmutableMap;
import com.facebook.imagepipeline.request.ImageRequest;
import java.io.FileNotFoundException;
import java.util.Map;
import java.util.concurrent.Executor;

/* compiled from: LocalVideoThumbnailProducer.java */
/* loaded from: classes.dex */
public class l implements dv2<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> {
    public final Executor a;
    public final ContentResolver b;

    /* compiled from: LocalVideoThumbnailProducer.java */
    /* loaded from: classes.dex */
    public class a extends gt3<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> {
        public final /* synthetic */ iv2 j0;
        public final /* synthetic */ ev2 k0;
        public final /* synthetic */ ImageRequest l0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(l60 l60Var, iv2 iv2Var, ev2 ev2Var, String str, iv2 iv2Var2, ev2 ev2Var2, ImageRequest imageRequest) {
            super(l60Var, iv2Var, ev2Var, str);
            this.j0 = iv2Var2;
            this.k0 = ev2Var2;
            this.l0 = imageRequest;
        }

        @Override // defpackage.gt3, defpackage.ht3
        public void e(Exception exc) {
            super.e(exc);
            this.j0.e(this.k0, "VideoThumbnailProducer", false);
            this.k0.k("local");
        }

        @Override // defpackage.ht3
        /* renamed from: j */
        public void b(com.facebook.common.references.a<com.facebook.imagepipeline.image.a> aVar) {
            com.facebook.common.references.a.g(aVar);
        }

        @Override // defpackage.gt3
        /* renamed from: k */
        public Map<String, String> i(com.facebook.common.references.a<com.facebook.imagepipeline.image.a> aVar) {
            return ImmutableMap.of("createdThumbnail", String.valueOf(aVar != null));
        }

        @Override // defpackage.ht3
        /* renamed from: l */
        public com.facebook.common.references.a<com.facebook.imagepipeline.image.a> c() throws Exception {
            String str;
            try {
                str = l.this.h(this.l0);
            } catch (IllegalArgumentException unused) {
                str = null;
            }
            Bitmap createVideoThumbnail = str != null ? ThumbnailUtils.createVideoThumbnail(str, l.f(this.l0)) : l.g(l.this.b, this.l0.u());
            if (createVideoThumbnail == null) {
                return null;
            }
            c00 c00Var = new c00(createVideoThumbnail, yo3.b(), jp1.d, 0);
            this.k0.b("image_format", "thumbnail");
            c00Var.e(this.k0.getExtras());
            return com.facebook.common.references.a.v(c00Var);
        }

        @Override // defpackage.gt3, defpackage.ht3
        /* renamed from: m */
        public void f(com.facebook.common.references.a<com.facebook.imagepipeline.image.a> aVar) {
            super.f(aVar);
            this.j0.e(this.k0, "VideoThumbnailProducer", aVar != null);
            this.k0.k("local");
        }
    }

    /* compiled from: LocalVideoThumbnailProducer.java */
    /* loaded from: classes.dex */
    public class b extends vn {
        public final /* synthetic */ gt3 a;

        public b(l lVar, gt3 gt3Var) {
            this.a = gt3Var;
        }

        @Override // defpackage.fv2
        public void a() {
            this.a.a();
        }
    }

    public l(Executor executor, ContentResolver contentResolver) {
        this.a = executor;
        this.b = contentResolver;
    }

    public static int f(ImageRequest imageRequest) {
        return (imageRequest.m() > 96 || imageRequest.l() > 96) ? 1 : 3;
    }

    public static Bitmap g(ContentResolver contentResolver, Uri uri) {
        if (Build.VERSION.SDK_INT >= 10) {
            try {
                ParcelFileDescriptor openFileDescriptor = contentResolver.openFileDescriptor(uri, "r");
                xt2.g(openFileDescriptor);
                MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
                mediaMetadataRetriever.setDataSource(openFileDescriptor.getFileDescriptor());
                return mediaMetadataRetriever.getFrameAtTime(-1L);
            } catch (FileNotFoundException unused) {
            }
        }
        return null;
    }

    @Override // defpackage.dv2
    public void a(l60<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> l60Var, ev2 ev2Var) {
        iv2 l = ev2Var.l();
        ImageRequest c = ev2Var.c();
        ev2Var.f("local", "video");
        a aVar = new a(l60Var, l, ev2Var, "VideoThumbnailProducer", l, ev2Var, c);
        ev2Var.o(new b(this, aVar));
        this.a.execute(aVar);
    }

    public final String h(ImageRequest imageRequest) {
        Uri uri;
        String str;
        String[] strArr;
        Uri u = imageRequest.u();
        if (qf4.j(u)) {
            return imageRequest.t().getPath();
        }
        if (qf4.i(u)) {
            if (Build.VERSION.SDK_INT < 19 || !"com.android.providers.media.documents".equals(u.getAuthority())) {
                uri = u;
                str = null;
                strArr = null;
            } else {
                String documentId = DocumentsContract.getDocumentId(u);
                xt2.g(documentId);
                String[] strArr2 = {documentId.split(":")[1]};
                str = "_id=?";
                uri = (Uri) xt2.g(MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
                strArr = strArr2;
            }
            Cursor query = this.b.query(uri, new String[]{"_data"}, str, strArr, null);
            if (query != null) {
                try {
                    if (query.moveToFirst()) {
                        return query.getString(query.getColumnIndexOrThrow("_data"));
                    }
                } finally {
                    query.close();
                }
            }
            if (query != null) {
            }
        }
        return null;
    }
}
