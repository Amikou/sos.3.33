package com.facebook.imagepipeline.producers;

import android.content.ContentResolver;
import android.graphics.Rect;
import android.net.Uri;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.imagepipeline.request.ImageRequest;
import java.io.IOException;
import java.util.concurrent.Executor;

/* compiled from: LocalContentUriThumbnailFetchProducer.java */
/* loaded from: classes.dex */
public class g extends h implements s54<zu0> {
    public static final Class<?> d = g.class;
    public static final String[] e = {"_id", "_data"};
    public static final String[] f = {"_data"};
    public static final Rect g = new Rect(0, 0, RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN, 384);
    public static final Rect h = new Rect(0, 0, 96, 96);
    public final ContentResolver c;

    public g(Executor executor, com.facebook.common.memory.b bVar, ContentResolver contentResolver) {
        super(executor, bVar);
        this.c = contentResolver;
    }

    @Override // com.facebook.imagepipeline.producers.h
    public zu0 c(ImageRequest imageRequest) throws IOException {
        Uri u = imageRequest.u();
        if (qf4.g(u)) {
            return f(u, imageRequest.q());
        }
        return null;
    }

    @Override // com.facebook.imagepipeline.producers.h
    public String e() {
        return "LocalContentUriThumbnailFetchProducer";
    }

    public final zu0 f(Uri uri, p73 p73Var) throws IOException {
        return null;
    }
}
