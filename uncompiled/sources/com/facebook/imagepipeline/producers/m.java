package com.facebook.imagepipeline.producers;

import android.os.SystemClock;
import com.facebook.imagepipeline.image.EncodedImageOrigin;
import com.facebook.imagepipeline.producers.n;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import okhttp3.internal.http2.Http2;

/* compiled from: NetworkFetchProducer.java */
/* loaded from: classes.dex */
public class m implements dv2<zu0> {
    public final com.facebook.common.memory.b a;
    public final os b;
    public final n c;

    /* compiled from: NetworkFetchProducer.java */
    /* loaded from: classes.dex */
    public class a implements n.a {
        public final /* synthetic */ t21 a;

        public a(t21 t21Var) {
            this.a = t21Var;
        }

        @Override // com.facebook.imagepipeline.producers.n.a
        public void a(Throwable th) {
            m.this.k(this.a, th);
        }

        @Override // com.facebook.imagepipeline.producers.n.a
        public void b() {
            m.this.j(this.a);
        }

        @Override // com.facebook.imagepipeline.producers.n.a
        public void c(InputStream inputStream, int i) throws IOException {
            if (nc1.d()) {
                nc1.a("NetworkFetcher->onResponse");
            }
            m.this.l(this.a, inputStream, i);
            if (nc1.d()) {
                nc1.b();
            }
        }
    }

    public m(com.facebook.common.memory.b bVar, os osVar, n nVar) {
        this.a = bVar;
        this.b = osVar;
        this.c = nVar;
    }

    public static float d(int i, int i2) {
        return i2 > 0 ? i / i2 : 1.0f - ((float) Math.exp((-i) / 50000.0d));
    }

    public static void i(ct2 ct2Var, int i, ct ctVar, l60<zu0> l60Var, ev2 ev2Var) {
        zu0 zu0Var;
        com.facebook.common.references.a v = com.facebook.common.references.a.v(ct2Var.a());
        zu0 zu0Var2 = null;
        try {
            zu0Var = new zu0(v);
        } catch (Throwable th) {
            th = th;
        }
        try {
            zu0Var.S(ctVar);
            zu0Var.M();
            ev2Var.e(EncodedImageOrigin.NETWORK);
            l60Var.d(zu0Var, i);
            zu0.c(zu0Var);
            com.facebook.common.references.a.g(v);
        } catch (Throwable th2) {
            th = th2;
            zu0Var2 = zu0Var;
            zu0.c(zu0Var2);
            com.facebook.common.references.a.g(v);
            throw th;
        }
    }

    @Override // defpackage.dv2
    public void a(l60<zu0> l60Var, ev2 ev2Var) {
        ev2Var.l().k(ev2Var, "NetworkFetchProducer");
        t21 b = this.c.b(l60Var, ev2Var);
        this.c.e(b, new a(b));
    }

    public final Map<String, String> e(t21 t21Var, int i) {
        if (t21Var.d().j(t21Var.b(), "NetworkFetchProducer")) {
            return this.c.d(t21Var, i);
        }
        return null;
    }

    public long f() {
        return SystemClock.uptimeMillis();
    }

    public void g(ct2 ct2Var, t21 t21Var) {
        Map<String, String> e = e(t21Var, ct2Var.size());
        iv2 d = t21Var.d();
        d.a(t21Var.b(), "NetworkFetchProducer", e);
        d.e(t21Var.b(), "NetworkFetchProducer", true);
        t21Var.b().k("network");
        i(ct2Var, t21Var.e() | 1, t21Var.f(), t21Var.a(), t21Var.b());
    }

    public void h(ct2 ct2Var, t21 t21Var) {
        long f = f();
        if (!m(t21Var) || f - t21Var.c() < 100) {
            return;
        }
        t21Var.h(f);
        t21Var.d().i(t21Var.b(), "NetworkFetchProducer", "intermediate_result");
        i(ct2Var, t21Var.e(), t21Var.f(), t21Var.a(), t21Var.b());
    }

    public final void j(t21 t21Var) {
        t21Var.d().d(t21Var.b(), "NetworkFetchProducer", null);
        t21Var.a().b();
    }

    public final void k(t21 t21Var, Throwable th) {
        t21Var.d().c(t21Var.b(), "NetworkFetchProducer", th, null);
        t21Var.d().e(t21Var.b(), "NetworkFetchProducer", false);
        t21Var.b().k("network");
        t21Var.a().a(th);
    }

    public void l(t21 t21Var, InputStream inputStream, int i) throws IOException {
        ct2 a2;
        if (i > 0) {
            a2 = this.a.e(i);
        } else {
            a2 = this.a.a();
        }
        byte[] bArr = this.b.get(Http2.INITIAL_MAX_FRAME_SIZE);
        while (true) {
            try {
                int read = inputStream.read(bArr);
                if (read < 0) {
                    this.c.a(t21Var, a2.size());
                    g(a2, t21Var);
                    return;
                } else if (read > 0) {
                    a2.write(bArr, 0, read);
                    h(a2, t21Var);
                    t21Var.a().c(d(a2.size(), i));
                }
            } finally {
                this.b.a(bArr);
                a2.close();
            }
        }
    }

    public final boolean m(t21 t21Var) {
        if (t21Var.b().m()) {
            return this.c.c(t21Var);
        }
        return false;
    }
}
