package com.facebook.imagepipeline.producers;

import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import com.facebook.imagepipeline.request.ImageRequest;
import java.io.IOException;
import java.util.concurrent.Executor;

/* compiled from: LocalAssetFetchProducer.java */
/* loaded from: classes.dex */
public class e extends h {
    public final AssetManager c;

    public e(Executor executor, com.facebook.common.memory.b bVar, AssetManager assetManager) {
        super(executor, bVar);
        this.c = assetManager;
    }

    public static String f(ImageRequest imageRequest) {
        return imageRequest.u().getPath().substring(1);
    }

    @Override // com.facebook.imagepipeline.producers.h
    public zu0 c(ImageRequest imageRequest) throws IOException {
        return d(this.c.open(f(imageRequest), 2), g(imageRequest));
    }

    @Override // com.facebook.imagepipeline.producers.h
    public String e() {
        return "LocalAssetFetchProducer";
    }

    public final int g(ImageRequest imageRequest) {
        AssetFileDescriptor assetFileDescriptor = null;
        try {
            assetFileDescriptor = this.c.openFd(f(imageRequest));
            int length = (int) assetFileDescriptor.getLength();
            try {
                assetFileDescriptor.close();
            } catch (IOException unused) {
            }
            return length;
        } catch (IOException unused2) {
            if (assetFileDescriptor != null) {
                try {
                    assetFileDescriptor.close();
                } catch (IOException unused3) {
                }
            }
            return -1;
        } catch (Throwable th) {
            if (assetFileDescriptor != null) {
                try {
                    assetFileDescriptor.close();
                } catch (IOException unused4) {
                }
            }
            throw th;
        }
    }
}
