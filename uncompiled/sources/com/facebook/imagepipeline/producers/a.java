package com.facebook.imagepipeline.producers;

import android.net.Uri;
import android.util.Base64;
import com.facebook.imagepipeline.request.ImageRequest;
import java.io.ByteArrayInputStream;
import java.io.IOException;

/* compiled from: DataFetchProducer.java */
/* loaded from: classes.dex */
public class a extends h {
    public a(com.facebook.common.memory.b bVar) {
        super(bv.a(), bVar);
    }

    public static byte[] f(String str) {
        xt2.b(Boolean.valueOf(str.substring(0, 5).equals("data:")));
        int indexOf = str.indexOf(44);
        String substring = str.substring(indexOf + 1, str.length());
        if (g(str.substring(0, indexOf))) {
            return Base64.decode(substring, 0);
        }
        String decode = Uri.decode(substring);
        xt2.g(decode);
        return decode.getBytes();
    }

    public static boolean g(String str) {
        if (str.contains(";")) {
            String[] split = str.split(";");
            return split[split.length - 1].equals("base64");
        }
        return false;
    }

    @Override // com.facebook.imagepipeline.producers.h
    public zu0 c(ImageRequest imageRequest) throws IOException {
        byte[] f = f(imageRequest.u().toString());
        return b(new ByteArrayInputStream(f), f.length);
    }

    @Override // com.facebook.imagepipeline.producers.h
    public String e() {
        return "DataFetchProducer";
    }
}
