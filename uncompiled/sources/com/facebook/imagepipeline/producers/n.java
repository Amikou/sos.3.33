package com.facebook.imagepipeline.producers;

import defpackage.t21;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

/* compiled from: NetworkFetcher.java */
/* loaded from: classes.dex */
public interface n<FETCH_STATE extends t21> {

    /* compiled from: NetworkFetcher.java */
    /* loaded from: classes.dex */
    public interface a {
        void a(Throwable th);

        void b();

        void c(InputStream inputStream, int i) throws IOException;
    }

    void a(FETCH_STATE fetch_state, int i);

    FETCH_STATE b(l60<zu0> l60Var, ev2 ev2Var);

    boolean c(FETCH_STATE fetch_state);

    Map<String, String> d(FETCH_STATE fetch_state, int i);

    void e(FETCH_STATE fetch_state, a aVar);
}
