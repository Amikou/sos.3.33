package com.facebook.imagepipeline.producers;

import android.net.Uri;
import com.facebook.common.time.RealtimeSinceBootClock;
import com.facebook.imagepipeline.producers.n;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import zendesk.core.Constants;

/* compiled from: HttpUrlConnectionNetworkFetcher.java */
/* loaded from: classes.dex */
public class d extends rn<c> {
    public int a;
    public String b;
    public final Map<String, String> c;
    public final ExecutorService d;
    public final o92 e;

    /* compiled from: HttpUrlConnectionNetworkFetcher.java */
    /* loaded from: classes.dex */
    public class a implements Runnable {
        public final /* synthetic */ c a;
        public final /* synthetic */ n.a f0;

        public a(c cVar, n.a aVar) {
            this.a = cVar;
            this.f0 = aVar;
        }

        @Override // java.lang.Runnable
        public void run() {
            d.this.j(this.a, this.f0);
        }
    }

    /* compiled from: HttpUrlConnectionNetworkFetcher.java */
    /* loaded from: classes.dex */
    public class b extends vn {
        public final /* synthetic */ Future a;
        public final /* synthetic */ n.a b;

        public b(d dVar, Future future, n.a aVar) {
            this.a = future;
            this.b = aVar;
        }

        @Override // defpackage.fv2
        public void a() {
            if (this.a.cancel(false)) {
                this.b.b();
            }
        }
    }

    /* compiled from: HttpUrlConnectionNetworkFetcher.java */
    /* loaded from: classes.dex */
    public static class c extends t21 {
        public long f;
        public long g;
        public long h;

        public c(l60<zu0> l60Var, ev2 ev2Var) {
            super(l60Var, ev2Var);
        }
    }

    public d(int i) {
        this(null, null, RealtimeSinceBootClock.get());
        this.a = i;
    }

    public static String h(String str, Object... objArr) {
        return String.format(Locale.getDefault(), str, objArr);
    }

    public static boolean l(int i) {
        if (i == 307 || i == 308) {
            return true;
        }
        switch (i) {
            case 300:
            case 301:
            case 302:
            case 303:
                return true;
            default:
                return false;
        }
    }

    public static boolean m(int i) {
        return i >= 200 && i < 300;
    }

    public static HttpURLConnection o(Uri uri) throws IOException {
        return (HttpURLConnection) qf4.n(uri).openConnection();
    }

    @Override // com.facebook.imagepipeline.producers.n
    /* renamed from: f */
    public c b(l60<zu0> l60Var, ev2 ev2Var) {
        return new c(l60Var, ev2Var);
    }

    public final HttpURLConnection g(Uri uri, int i) throws IOException {
        HttpURLConnection o = o(uri);
        String str = this.b;
        if (str != null) {
            o.setRequestProperty(Constants.USER_AGENT_HEADER_KEY, str);
        }
        Map<String, String> map = this.c;
        if (map != null) {
            for (Map.Entry<String, String> entry : map.entrySet()) {
                o.setRequestProperty(entry.getKey(), entry.getValue());
            }
        }
        o.setConnectTimeout(this.a);
        int responseCode = o.getResponseCode();
        if (m(responseCode)) {
            return o;
        }
        if (l(responseCode)) {
            String headerField = o.getHeaderField("Location");
            o.disconnect();
            Uri parse = headerField == null ? null : Uri.parse(headerField);
            String scheme = uri.getScheme();
            if (i > 0 && parse != null && !ol2.a(parse.getScheme(), scheme)) {
                return g(parse, i - 1);
            }
            throw new IOException(i == 0 ? h("URL %s follows too many redirects", uri.toString()) : h("URL %s returned %d without a valid redirect", uri.toString(), Integer.valueOf(responseCode)));
        }
        o.disconnect();
        throw new IOException(String.format("Image URL %s returned HTTP code %d", uri.toString(), Integer.valueOf(responseCode)));
    }

    @Override // com.facebook.imagepipeline.producers.n
    /* renamed from: i */
    public void e(c cVar, n.a aVar) {
        cVar.f = this.e.now();
        cVar.b().o(new b(this, this.d.submit(new a(cVar, aVar)), aVar));
    }

    /* JADX WARN: Removed duplicated region for block: B:27:0x0042  */
    /* JADX WARN: Removed duplicated region for block: B:39:0x003d A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void j(com.facebook.imagepipeline.producers.d.c r5, com.facebook.imagepipeline.producers.n.a r6) {
        /*
            r4 = this;
            r0 = 0
            android.net.Uri r1 = r5.g()     // Catch: java.lang.Throwable -> L27 java.io.IOException -> L2a
            r2 = 5
            java.net.HttpURLConnection r1 = r4.g(r1, r2)     // Catch: java.lang.Throwable -> L27 java.io.IOException -> L2a
            o92 r2 = r4.e     // Catch: java.io.IOException -> L25 java.lang.Throwable -> L3a
            long r2 = r2.now()     // Catch: java.io.IOException -> L25 java.lang.Throwable -> L3a
            com.facebook.imagepipeline.producers.d.c.l(r5, r2)     // Catch: java.io.IOException -> L25 java.lang.Throwable -> L3a
            if (r1 == 0) goto L1d
            java.io.InputStream r0 = r1.getInputStream()     // Catch: java.io.IOException -> L25 java.lang.Throwable -> L3a
            r5 = -1
            r6.c(r0, r5)     // Catch: java.io.IOException -> L25 java.lang.Throwable -> L3a
        L1d:
            if (r0 == 0) goto L22
            r0.close()     // Catch: java.io.IOException -> L22
        L22:
            if (r1 == 0) goto L39
            goto L36
        L25:
            r5 = move-exception
            goto L2c
        L27:
            r5 = move-exception
            r1 = r0
            goto L3b
        L2a:
            r5 = move-exception
            r1 = r0
        L2c:
            r6.a(r5)     // Catch: java.lang.Throwable -> L3a
            if (r0 == 0) goto L34
            r0.close()     // Catch: java.io.IOException -> L34
        L34:
            if (r1 == 0) goto L39
        L36:
            r1.disconnect()
        L39:
            return
        L3a:
            r5 = move-exception
        L3b:
            if (r0 == 0) goto L40
            r0.close()     // Catch: java.io.IOException -> L40
        L40:
            if (r1 == 0) goto L45
            r1.disconnect()
        L45:
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.imagepipeline.producers.d.j(com.facebook.imagepipeline.producers.d$c, com.facebook.imagepipeline.producers.n$a):void");
    }

    @Override // com.facebook.imagepipeline.producers.n
    /* renamed from: k */
    public Map<String, String> d(c cVar, int i) {
        HashMap hashMap = new HashMap(4);
        hashMap.put("queue_time", Long.toString(cVar.g - cVar.f));
        hashMap.put("fetch_time", Long.toString(cVar.h - cVar.g));
        hashMap.put("total_time", Long.toString(cVar.h - cVar.f));
        hashMap.put("image_size", Integer.toString(i));
        return hashMap;
    }

    @Override // com.facebook.imagepipeline.producers.n
    /* renamed from: n */
    public void a(c cVar, int i) {
        cVar.h = this.e.now();
    }

    public d(String str, Map<String, String> map, o92 o92Var) {
        this.d = Executors.newFixedThreadPool(3);
        this.e = o92Var;
        this.c = map;
        this.b = str;
    }
}
