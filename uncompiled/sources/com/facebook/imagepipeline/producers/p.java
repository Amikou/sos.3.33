package com.facebook.imagepipeline.producers;

import android.content.ContentResolver;
import com.facebook.imagepipeline.request.ImageRequest;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.Executor;

/* compiled from: QualifiedResourceFetchProducer.java */
/* loaded from: classes.dex */
public class p extends h {
    public final ContentResolver c;

    public p(Executor executor, com.facebook.common.memory.b bVar, ContentResolver contentResolver) {
        super(executor, bVar);
        this.c = contentResolver;
    }

    @Override // com.facebook.imagepipeline.producers.h
    public zu0 c(ImageRequest imageRequest) throws IOException {
        InputStream openInputStream = this.c.openInputStream(imageRequest.u());
        xt2.h(openInputStream, "ContentResolver returned null InputStream");
        return d(openInputStream, -1);
    }

    @Override // com.facebook.imagepipeline.producers.h
    public String e() {
        return "QualifiedResourceFetchProducer";
    }
}
