package com.facebook.imagepipeline.producers;

import com.facebook.imagepipeline.request.ImageRequest;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.concurrent.Executor;

/* compiled from: LocalFileFetchProducer.java */
/* loaded from: classes.dex */
public class i extends h {
    public i(Executor executor, com.facebook.common.memory.b bVar) {
        super(executor, bVar);
    }

    @Override // com.facebook.imagepipeline.producers.h
    public zu0 c(ImageRequest imageRequest) throws IOException {
        return d(new FileInputStream(imageRequest.t().toString()), (int) imageRequest.t().length());
    }

    @Override // com.facebook.imagepipeline.producers.h
    public String e() {
        return "LocalFileFetchProducer";
    }
}
