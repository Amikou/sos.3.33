package com.facebook.imagepipeline.producers;

import android.graphics.Bitmap;
import android.os.Build;
import com.facebook.common.internal.ImmutableMap;
import com.facebook.common.util.ExceptionWithNoStacktrace;
import com.facebook.imagepipeline.producers.JobScheduler;
import com.facebook.imagepipeline.request.ImageRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;

/* compiled from: DecodeProducer.java */
/* loaded from: classes.dex */
public class b implements dv2<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> {
    public final os a;
    public final Executor b;
    public final tn1 c;
    public final qv2 d;
    public final dv2<zu0> e;
    public final boolean f;
    public final boolean g;
    public final boolean h;
    public final int i;
    public final a00 j;
    public final Runnable k;
    public final fw3<Boolean> l;

    /* compiled from: DecodeProducer.java */
    /* loaded from: classes.dex */
    public class a extends c {
        public a(b bVar, l60<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> l60Var, ev2 ev2Var, boolean z, int i) {
            super(l60Var, ev2Var, z, i);
        }

        @Override // com.facebook.imagepipeline.producers.b.c
        public synchronized boolean I(zu0 zu0Var, int i) {
            if (qm.f(i)) {
                return false;
            }
            return super.I(zu0Var, i);
        }

        @Override // com.facebook.imagepipeline.producers.b.c
        public int x(zu0 zu0Var) {
            return zu0Var.u();
        }

        @Override // com.facebook.imagepipeline.producers.b.c
        public xw2 y() {
            return jp1.d(0, false, false);
        }
    }

    /* compiled from: DecodeProducer.java */
    /* renamed from: com.facebook.imagepipeline.producers.b$b  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public class C0086b extends c {
        public final rv2 i;
        public final qv2 j;
        public int k;

        public C0086b(b bVar, l60<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> l60Var, ev2 ev2Var, rv2 rv2Var, qv2 qv2Var, boolean z, int i) {
            super(l60Var, ev2Var, z, i);
            this.i = (rv2) xt2.g(rv2Var);
            this.j = (qv2) xt2.g(qv2Var);
            this.k = 0;
        }

        @Override // com.facebook.imagepipeline.producers.b.c
        public synchronized boolean I(zu0 zu0Var, int i) {
            boolean I = super.I(zu0Var, i);
            if ((qm.f(i) || qm.n(i, 8)) && !qm.n(i, 4) && zu0.F(zu0Var) && zu0Var.l() == wj0.a) {
                if (!this.i.g(zu0Var)) {
                    return false;
                }
                int d = this.i.d();
                int i2 = this.k;
                if (d <= i2) {
                    return false;
                }
                if (d < this.j.b(i2) && !this.i.e()) {
                    return false;
                }
                this.k = d;
            }
            return I;
        }

        @Override // com.facebook.imagepipeline.producers.b.c
        public int x(zu0 zu0Var) {
            return this.i.c();
        }

        @Override // com.facebook.imagepipeline.producers.b.c
        public xw2 y() {
            return this.j.a(this.i.d());
        }
    }

    /* compiled from: DecodeProducer.java */
    /* loaded from: classes.dex */
    public abstract class c extends bm0<zu0, com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> {
        public final ev2 c;
        public final iv2 d;
        public final rn1 e;
        public boolean f;
        public final JobScheduler g;

        /* compiled from: DecodeProducer.java */
        /* loaded from: classes.dex */
        public class a implements JobScheduler.d {
            public final /* synthetic */ ev2 a;
            public final /* synthetic */ int b;

            public a(b bVar, ev2 ev2Var, int i) {
                this.a = ev2Var;
                this.b = i;
            }

            @Override // com.facebook.imagepipeline.producers.JobScheduler.d
            public void a(zu0 zu0Var, int i) {
                if (zu0Var != null) {
                    c.this.c.b("image_format", zu0Var.l().a());
                    if (b.this.f || !qm.n(i, 16)) {
                        ImageRequest c = this.a.c();
                        if (b.this.g || !qf4.l(c.u())) {
                            zu0Var.e0(mq0.b(c.s(), c.q(), zu0Var, this.b));
                        }
                    }
                    if (this.a.d().C().A()) {
                        c.this.F(zu0Var);
                    }
                    c.this.v(zu0Var, i);
                }
            }
        }

        /* compiled from: DecodeProducer.java */
        /* renamed from: com.facebook.imagepipeline.producers.b$c$b  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public class C0087b extends vn {
            public final /* synthetic */ boolean a;

            public C0087b(b bVar, boolean z) {
                this.a = z;
            }

            @Override // defpackage.fv2
            public void a() {
                if (this.a) {
                    c.this.z();
                }
            }

            @Override // defpackage.vn, defpackage.fv2
            public void b() {
                if (c.this.c.m()) {
                    c.this.g.h();
                }
            }
        }

        public c(l60<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> l60Var, ev2 ev2Var, boolean z, int i) {
            super(l60Var);
            this.c = ev2Var;
            this.d = ev2Var.l();
            rn1 g = ev2Var.c().g();
            this.e = g;
            this.f = false;
            this.g = new JobScheduler(b.this.b, new a(b.this, ev2Var, i), g.a);
            ev2Var.o(new C0087b(b.this, z));
        }

        public final void A(Throwable th) {
            E(true);
            p().a(th);
        }

        public final void B(com.facebook.imagepipeline.image.a aVar, int i) {
            com.facebook.common.references.a<com.facebook.imagepipeline.image.a> b = b.this.j.b(aVar);
            try {
                E(qm.e(i));
                p().d(b, i);
            } finally {
                com.facebook.common.references.a.g(b);
            }
        }

        public final com.facebook.imagepipeline.image.a C(zu0 zu0Var, int i, xw2 xw2Var) {
            boolean z = b.this.k != null && ((Boolean) b.this.l.get()).booleanValue();
            try {
                return b.this.c.a(zu0Var, i, xw2Var, this.e);
            } catch (OutOfMemoryError e) {
                if (z) {
                    b.this.k.run();
                    System.gc();
                    return b.this.c.a(zu0Var, i, xw2Var, this.e);
                }
                throw e;
            }
        }

        public final synchronized boolean D() {
            return this.f;
        }

        public final void E(boolean z) {
            synchronized (this) {
                if (z) {
                    if (!this.f) {
                        p().c(1.0f);
                        this.f = true;
                        this.g.c();
                    }
                }
            }
        }

        public final void F(zu0 zu0Var) {
            if (zu0Var.l() != wj0.a) {
                return;
            }
            zu0Var.e0(mq0.c(zu0Var, rq.c(this.e.g), 104857600));
        }

        @Override // defpackage.qm
        /* renamed from: G */
        public void i(zu0 zu0Var, int i) {
            boolean d;
            try {
                if (nc1.d()) {
                    nc1.a("DecodeProducer#onNewResultImpl");
                }
                boolean e = qm.e(i);
                if (e) {
                    if (zu0Var == null) {
                        A(new ExceptionWithNoStacktrace("Encoded image is null."));
                        if (d) {
                            return;
                        }
                        return;
                    } else if (!zu0Var.C()) {
                        A(new ExceptionWithNoStacktrace("Encoded image is not valid."));
                        if (nc1.d()) {
                            nc1.b();
                            return;
                        }
                        return;
                    }
                }
                if (!I(zu0Var, i)) {
                    if (nc1.d()) {
                        nc1.b();
                        return;
                    }
                    return;
                }
                boolean n = qm.n(i, 4);
                if (e || n || this.c.m()) {
                    this.g.h();
                }
                if (nc1.d()) {
                    nc1.b();
                }
            } finally {
                if (nc1.d()) {
                    nc1.b();
                }
            }
        }

        public final void H(zu0 zu0Var, com.facebook.imagepipeline.image.a aVar) {
            this.c.b("encoded_width", Integer.valueOf(zu0Var.v()));
            this.c.b("encoded_height", Integer.valueOf(zu0Var.j()));
            this.c.b("encoded_size", Integer.valueOf(zu0Var.u()));
            if (aVar instanceof wz) {
                Bitmap f = ((wz) aVar).f();
                this.c.b("bitmap_config", String.valueOf(f == null ? null : f.getConfig()));
            }
            if (aVar != null) {
                aVar.e(this.c.getExtras());
            }
        }

        public boolean I(zu0 zu0Var, int i) {
            return this.g.k(zu0Var, i);
        }

        @Override // defpackage.bm0, defpackage.qm
        public void g() {
            z();
        }

        @Override // defpackage.bm0, defpackage.qm
        public void h(Throwable th) {
            A(th);
        }

        @Override // defpackage.bm0, defpackage.qm
        public void j(float f) {
            super.j(f * 0.99f);
        }

        /* JADX WARN: Can't wrap try/catch for region: R(3:(13:(18:25|26|(15:30|31|32|33|34|35|36|(1:38)|39|40|41|42|43|44|45)|66|31|32|33|34|35|36|(0)|39|40|41|42|43|44|45)|(15:30|31|32|33|34|35|36|(0)|39|40|41|42|43|44|45)|34|35|36|(0)|39|40|41|42|43|44|45)|32|33) */
        /* JADX WARN: Code restructure failed: missing block: B:51:0x00e2, code lost:
            r0 = e;
         */
        /* JADX WARN: Code restructure failed: missing block: B:52:0x00e3, code lost:
            r2 = null;
         */
        /* JADX WARN: Removed duplicated region for block: B:40:0x00bb  */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public final void v(defpackage.zu0 r20, int r21) {
            /*
                Method dump skipped, instructions count: 306
                To view this dump change 'Code comments level' option to 'DEBUG'
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.imagepipeline.producers.b.c.v(zu0, int):void");
        }

        public final Map<String, String> w(com.facebook.imagepipeline.image.a aVar, long j, xw2 xw2Var, boolean z, String str, String str2, String str3, String str4) {
            Bitmap f;
            if (this.d.j(this.c, "DecodeProducer")) {
                String valueOf = String.valueOf(j);
                String valueOf2 = String.valueOf(xw2Var.b());
                String valueOf3 = String.valueOf(z);
                if (aVar instanceof c00) {
                    xt2.g(((c00) aVar).f());
                    HashMap hashMap = new HashMap(8);
                    hashMap.put("bitmapSize", f.getWidth() + "x" + f.getHeight());
                    hashMap.put("queueTime", valueOf);
                    hashMap.put("hasGoodQuality", valueOf2);
                    hashMap.put("isFinal", valueOf3);
                    hashMap.put("encodedImageSize", str2);
                    hashMap.put("imageFormat", str);
                    hashMap.put("requestedImageSize", str3);
                    hashMap.put("sampleSize", str4);
                    if (Build.VERSION.SDK_INT >= 12) {
                        hashMap.put("byteCount", f.getByteCount() + "");
                    }
                    return ImmutableMap.copyOf((Map) hashMap);
                }
                HashMap hashMap2 = new HashMap(7);
                hashMap2.put("queueTime", valueOf);
                hashMap2.put("hasGoodQuality", valueOf2);
                hashMap2.put("isFinal", valueOf3);
                hashMap2.put("encodedImageSize", str2);
                hashMap2.put("imageFormat", str);
                hashMap2.put("requestedImageSize", str3);
                hashMap2.put("sampleSize", str4);
                return ImmutableMap.copyOf((Map) hashMap2);
            }
            return null;
        }

        public abstract int x(zu0 zu0Var);

        public abstract xw2 y();

        public final void z() {
            E(true);
            p().b();
        }
    }

    public b(os osVar, Executor executor, tn1 tn1Var, qv2 qv2Var, boolean z, boolean z2, boolean z3, dv2<zu0> dv2Var, int i, a00 a00Var, Runnable runnable, fw3<Boolean> fw3Var) {
        this.a = (os) xt2.g(osVar);
        this.b = (Executor) xt2.g(executor);
        this.c = (tn1) xt2.g(tn1Var);
        this.d = (qv2) xt2.g(qv2Var);
        this.f = z;
        this.g = z2;
        this.e = (dv2) xt2.g(dv2Var);
        this.h = z3;
        this.i = i;
        this.j = a00Var;
        this.k = runnable;
        this.l = fw3Var;
    }

    @Override // defpackage.dv2
    public void a(l60<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> l60Var, ev2 ev2Var) {
        l60<zu0> c0086b;
        try {
            if (nc1.d()) {
                nc1.a("DecodeProducer#produceResults");
            }
            if (!qf4.l(ev2Var.c().u())) {
                c0086b = new a(this, l60Var, ev2Var, this.h, this.i);
            } else {
                c0086b = new C0086b(this, l60Var, ev2Var, new rv2(this.a), this.d, this.h, this.i);
            }
            this.e.a(c0086b, ev2Var);
        } finally {
            if (nc1.d()) {
                nc1.b();
            }
        }
    }
}
