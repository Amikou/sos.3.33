package com.facebook.imagepipeline.producers;

import android.content.ContentResolver;
import android.content.res.AssetFileDescriptor;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.util.Pair;
import com.facebook.common.internal.ImmutableMap;
import com.facebook.common.memory.PooledByteBuffer;
import com.facebook.imagepipeline.request.ImageRequest;
import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.Executor;

/* loaded from: classes.dex */
public class LocalExifThumbnailProducer implements s54<zu0> {
    public final Executor a;
    public final com.facebook.common.memory.b b;
    public final ContentResolver c;

    @bq0
    /* loaded from: classes.dex */
    public class Api24Utils {
        public Api24Utils(LocalExifThumbnailProducer localExifThumbnailProducer) {
        }

        public ExifInterface a(FileDescriptor fileDescriptor) throws IOException {
            if (Build.VERSION.SDK_INT >= 24) {
                return new ExifInterface(fileDescriptor);
            }
            return null;
        }

        public /* synthetic */ Api24Utils(LocalExifThumbnailProducer localExifThumbnailProducer, a aVar) {
            this(localExifThumbnailProducer);
        }
    }

    /* loaded from: classes.dex */
    public class a extends gt3<zu0> {
        public final /* synthetic */ ImageRequest j0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(l60 l60Var, iv2 iv2Var, ev2 ev2Var, String str, ImageRequest imageRequest) {
            super(l60Var, iv2Var, ev2Var, str);
            this.j0 = imageRequest;
        }

        @Override // defpackage.ht3
        /* renamed from: j */
        public void b(zu0 zu0Var) {
            zu0.c(zu0Var);
        }

        @Override // defpackage.gt3
        /* renamed from: k */
        public Map<String, String> i(zu0 zu0Var) {
            return ImmutableMap.of("createdThumbnail", Boolean.toString(zu0Var != null));
        }

        @Override // defpackage.ht3
        /* renamed from: l */
        public zu0 c() throws Exception {
            ExifInterface f = LocalExifThumbnailProducer.this.f(this.j0.u());
            if (f == null || !f.hasThumbnail()) {
                return null;
            }
            return LocalExifThumbnailProducer.this.d(LocalExifThumbnailProducer.this.b.b((byte[]) xt2.g(f.getThumbnail())), f);
        }
    }

    /* loaded from: classes.dex */
    public class b extends vn {
        public final /* synthetic */ gt3 a;

        public b(LocalExifThumbnailProducer localExifThumbnailProducer, gt3 gt3Var) {
            this.a = gt3Var;
        }

        @Override // defpackage.fv2
        public void a() {
            this.a.a();
        }
    }

    public LocalExifThumbnailProducer(Executor executor, com.facebook.common.memory.b bVar, ContentResolver contentResolver) {
        this.a = executor;
        this.b = bVar;
        this.c = contentResolver;
    }

    @Override // defpackage.dv2
    public void a(l60<zu0> l60Var, ev2 ev2Var) {
        iv2 l = ev2Var.l();
        ImageRequest c = ev2Var.c();
        ev2Var.f("local", "exif");
        a aVar = new a(l60Var, l, ev2Var, "LocalExifThumbnailProducer", c);
        ev2Var.o(new b(this, aVar));
        this.a.execute(aVar);
    }

    public final zu0 d(PooledByteBuffer pooledByteBuffer, ExifInterface exifInterface) {
        Pair<Integer, Integer> a2 = rq.a(new bt2(pooledByteBuffer));
        int g = g(exifInterface);
        int intValue = a2 != null ? ((Integer) a2.first).intValue() : -1;
        int intValue2 = a2 != null ? ((Integer) a2.second).intValue() : -1;
        com.facebook.common.references.a v = com.facebook.common.references.a.v(pooledByteBuffer);
        try {
            zu0 zu0Var = new zu0(v);
            com.facebook.common.references.a.g(v);
            zu0Var.a0(wj0.a);
            zu0Var.b0(g);
            zu0Var.f0(intValue);
            zu0Var.X(intValue2);
            return zu0Var;
        } catch (Throwable th) {
            com.facebook.common.references.a.g(v);
            throw th;
        }
    }

    public boolean e(String str) throws IOException {
        if (str == null) {
            return false;
        }
        File file = new File(str);
        return file.exists() && file.canRead();
    }

    public ExifInterface f(Uri uri) {
        String b2 = qf4.b(this.c, uri);
        if (b2 == null) {
            return null;
        }
        try {
        } catch (IOException unused) {
        } catch (StackOverflowError unused2) {
            v11.d(LocalExifThumbnailProducer.class, "StackOverflowError in ExifInterface constructor");
        }
        if (e(b2)) {
            return new ExifInterface(b2);
        }
        AssetFileDescriptor a2 = qf4.a(this.c, uri);
        if (a2 != null && Build.VERSION.SDK_INT >= 24) {
            ExifInterface a3 = new Api24Utils(this, null).a(a2.getFileDescriptor());
            a2.close();
            return a3;
        }
        return null;
    }

    public final int g(ExifInterface exifInterface) {
        return com.facebook.imageutils.a.a(Integer.parseInt((String) xt2.g(exifInterface.getAttribute("Orientation"))));
    }
}
