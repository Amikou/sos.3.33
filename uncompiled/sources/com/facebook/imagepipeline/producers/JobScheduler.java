package com.facebook.imagepipeline.producers;

import android.os.SystemClock;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/* loaded from: classes.dex */
public class JobScheduler {
    public final Executor a;
    public final d b;
    public final int e;
    public final Runnable c = new a();
    public final Runnable d = new b();
    public zu0 f = null;
    public int g = 0;
    public JobState h = JobState.IDLE;
    public long i = 0;
    public long j = 0;

    /* loaded from: classes.dex */
    public enum JobState {
        IDLE,
        QUEUED,
        RUNNING,
        RUNNING_AND_PENDING
    }

    /* loaded from: classes.dex */
    public class a implements Runnable {
        public a() {
        }

        @Override // java.lang.Runnable
        public void run() {
            JobScheduler.this.d();
        }
    }

    /* loaded from: classes.dex */
    public class b implements Runnable {
        public b() {
        }

        @Override // java.lang.Runnable
        public void run() {
            JobScheduler.this.j();
        }
    }

    /* loaded from: classes.dex */
    public static /* synthetic */ class c {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[JobState.values().length];
            a = iArr;
            try {
                iArr[JobState.IDLE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[JobState.QUEUED.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[JobState.RUNNING.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                a[JobState.RUNNING_AND_PENDING.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
        }
    }

    /* loaded from: classes.dex */
    public interface d {
        void a(zu0 zu0Var, int i);
    }

    /* loaded from: classes.dex */
    public static class e {
        public static ScheduledExecutorService a;

        public static ScheduledExecutorService a() {
            if (a == null) {
                a = Executors.newSingleThreadScheduledExecutor();
            }
            return a;
        }
    }

    public JobScheduler(Executor executor, d dVar, int i) {
        this.a = executor;
        this.b = dVar;
        this.e = i;
    }

    public static boolean i(zu0 zu0Var, int i) {
        return qm.e(i) || qm.n(i, 4) || zu0.F(zu0Var);
    }

    public void c() {
        zu0 zu0Var;
        synchronized (this) {
            zu0Var = this.f;
            this.f = null;
            this.g = 0;
        }
        zu0.c(zu0Var);
    }

    public final void d() {
        zu0 zu0Var;
        int i;
        long uptimeMillis = SystemClock.uptimeMillis();
        synchronized (this) {
            zu0Var = this.f;
            i = this.g;
            this.f = null;
            this.g = 0;
            this.h = JobState.RUNNING;
            this.j = uptimeMillis;
        }
        try {
            if (i(zu0Var, i)) {
                this.b.a(zu0Var, i);
            }
        } finally {
            zu0.c(zu0Var);
            g();
        }
    }

    public final void e(long j) {
        Runnable a2 = mc1.a(this.d, "JobScheduler_enqueueJob");
        if (j > 0) {
            e.a().schedule(a2, j, TimeUnit.MILLISECONDS);
        } else {
            a2.run();
        }
    }

    public synchronized long f() {
        return this.j - this.i;
    }

    public final void g() {
        long j;
        boolean z;
        long uptimeMillis = SystemClock.uptimeMillis();
        synchronized (this) {
            if (this.h == JobState.RUNNING_AND_PENDING) {
                j = Math.max(this.j + this.e, uptimeMillis);
                z = true;
                this.i = uptimeMillis;
                this.h = JobState.QUEUED;
            } else {
                this.h = JobState.IDLE;
                j = 0;
                z = false;
            }
        }
        if (z) {
            e(j - uptimeMillis);
        }
    }

    public boolean h() {
        long max;
        long uptimeMillis = SystemClock.uptimeMillis();
        synchronized (this) {
            boolean z = false;
            if (i(this.f, this.g)) {
                int i = c.a[this.h.ordinal()];
                if (i != 1) {
                    if (i == 3) {
                        this.h = JobState.RUNNING_AND_PENDING;
                    }
                    max = 0;
                } else {
                    max = Math.max(this.j + this.e, uptimeMillis);
                    this.i = uptimeMillis;
                    this.h = JobState.QUEUED;
                    z = true;
                }
                if (z) {
                    e(max - uptimeMillis);
                }
                return true;
            }
            return false;
        }
    }

    public final void j() {
        this.a.execute(mc1.a(this.c, "JobScheduler_submitJob"));
    }

    public boolean k(zu0 zu0Var, int i) {
        zu0 zu0Var2;
        if (i(zu0Var, i)) {
            synchronized (this) {
                zu0Var2 = this.f;
                this.f = zu0.b(zu0Var);
                this.g = i;
            }
            zu0.c(zu0Var2);
            return true;
        }
        return false;
    }
}
