package com.facebook.imagepipeline.decoder;

/* loaded from: classes.dex */
public class DecodeException extends RuntimeException {
    private final zu0 mEncodedImage;

    public DecodeException(String str, zu0 zu0Var) {
        super(str);
        this.mEncodedImage = zu0Var;
    }

    public zu0 getEncodedImage() {
        return this.mEncodedImage;
    }

    public DecodeException(String str, Throwable th, zu0 zu0Var) {
        super(str, th);
        this.mEncodedImage = zu0Var;
    }
}
