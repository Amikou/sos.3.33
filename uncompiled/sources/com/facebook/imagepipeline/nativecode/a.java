package com.facebook.imagepipeline.nativecode;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/* compiled from: WebpTranscoder.java */
/* loaded from: classes.dex */
public interface a {
    void a(InputStream inputStream, OutputStream outputStream, int i) throws IOException;

    void b(InputStream inputStream, OutputStream outputStream) throws IOException;

    boolean c(wn1 wn1Var);
}
