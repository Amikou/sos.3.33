package com.facebook.imagepipeline.nativecode;

import com.facebook.common.internal.b;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

@cq0
/* loaded from: classes.dex */
public class NativeJpegTranscoder implements ap1 {
    public boolean a;
    public int b;
    public boolean c;

    public NativeJpegTranscoder(boolean z, int i, boolean z2, boolean z3) {
        this.a = z;
        this.b = i;
        this.c = z2;
        if (z3) {
            od2.a();
        }
    }

    public static void e(InputStream inputStream, OutputStream outputStream, int i, int i2, int i3) throws IOException {
        od2.a();
        boolean z = false;
        xt2.b(Boolean.valueOf(i2 >= 1));
        xt2.b(Boolean.valueOf(i2 <= 16));
        xt2.b(Boolean.valueOf(i3 >= 0));
        xt2.b(Boolean.valueOf(i3 <= 100));
        xt2.b(Boolean.valueOf(ou1.i(i)));
        if (i2 != 8 || i != 0) {
            z = true;
        }
        xt2.c(z, "no transformation requested");
        nativeTranscodeJpeg((InputStream) xt2.g(inputStream), (OutputStream) xt2.g(outputStream), i, i2, i3);
    }

    public static void f(InputStream inputStream, OutputStream outputStream, int i, int i2, int i3) throws IOException {
        od2.a();
        boolean z = false;
        xt2.b(Boolean.valueOf(i2 >= 1));
        xt2.b(Boolean.valueOf(i2 <= 16));
        xt2.b(Boolean.valueOf(i3 >= 0));
        xt2.b(Boolean.valueOf(i3 <= 100));
        xt2.b(Boolean.valueOf(ou1.h(i)));
        if (i2 != 8 || i != 1) {
            z = true;
        }
        xt2.c(z, "no transformation requested");
        nativeTranscodeJpegWithExifOrientation((InputStream) xt2.g(inputStream), (OutputStream) xt2.g(outputStream), i, i2, i3);
    }

    @cq0
    private static native void nativeTranscodeJpeg(InputStream inputStream, OutputStream outputStream, int i, int i2, int i3) throws IOException;

    @cq0
    private static native void nativeTranscodeJpegWithExifOrientation(InputStream inputStream, OutputStream outputStream, int i, int i2, int i3) throws IOException;

    @Override // defpackage.ap1
    public String a() {
        return "NativeJpegTranscoder";
    }

    @Override // defpackage.ap1
    public boolean b(wn1 wn1Var) {
        return wn1Var == wj0.a;
    }

    @Override // defpackage.ap1
    public boolean c(zu0 zu0Var, p93 p93Var, p73 p73Var) {
        if (p93Var == null) {
            p93Var = p93.a();
        }
        return ou1.e(p93Var, p73Var, zu0Var, this.a) < 8;
    }

    @Override // defpackage.ap1
    public zo1 d(zu0 zu0Var, OutputStream outputStream, p93 p93Var, p73 p73Var, wn1 wn1Var, Integer num) throws IOException {
        if (num == null) {
            num = 85;
        }
        if (p93Var == null) {
            p93Var = p93.a();
        }
        int b = mq0.b(p93Var, p73Var, zu0Var, this.b);
        try {
            int e = ou1.e(p93Var, p73Var, zu0Var, this.a);
            int a = ou1.a(b);
            if (this.c) {
                e = a;
            }
            InputStream m = zu0Var.m();
            if (ou1.a.contains(Integer.valueOf(zu0Var.h()))) {
                f((InputStream) xt2.h(m, "Cannot transcode from null input stream!"), outputStream, ou1.c(p93Var, zu0Var), e, num.intValue());
            } else {
                e((InputStream) xt2.h(m, "Cannot transcode from null input stream!"), outputStream, ou1.d(p93Var, zu0Var), e, num.intValue());
            }
            b.b(m);
            return new zo1(b != 1 ? 0 : 1);
        } catch (Throwable th) {
            b.b(null);
            throw th;
        }
    }
}
