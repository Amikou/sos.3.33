package com.facebook.imagepipeline.nativecode;

@cq0
/* loaded from: classes.dex */
public class NativeJpegTranscoderFactory implements bp1 {
    public final int a;
    public final boolean b;
    public final boolean c;

    @cq0
    public NativeJpegTranscoderFactory(int i, boolean z, boolean z2) {
        this.a = i;
        this.b = z;
        this.c = z2;
    }

    @Override // defpackage.bp1
    @cq0
    public ap1 createImageTranscoder(wn1 wn1Var, boolean z) {
        if (wn1Var != wj0.a) {
            return null;
        }
        return new NativeJpegTranscoder(z, this.a, this.b, this.c);
    }
}
