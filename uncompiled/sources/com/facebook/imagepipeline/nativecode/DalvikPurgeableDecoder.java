package com.facebook.imagepipeline.nativecode;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ColorSpace;
import android.graphics.Rect;
import android.os.Build;
import com.facebook.common.internal.d;
import com.facebook.common.memory.PooledByteBuffer;
import com.facebook.imagepipeline.common.TooManyBitmapsException;
import java.util.Locale;

@cq0
/* loaded from: classes.dex */
public abstract class DalvikPurgeableDecoder implements dr2 {
    public static final byte[] b;
    public final rp a = sp.a();

    @bq0
    /* loaded from: classes.dex */
    public static class OreoUtils {
        private OreoUtils() {
        }

        @TargetApi(26)
        public static void a(BitmapFactory.Options options, ColorSpace colorSpace) {
            if (colorSpace == null) {
                colorSpace = ColorSpace.get(ColorSpace.Named.SRGB);
            }
            options.inPreferredColorSpace = colorSpace;
        }
    }

    static {
        vo1.a();
        b = new byte[]{-1, -39};
    }

    public static boolean f(com.facebook.common.references.a<PooledByteBuffer> aVar, int i) {
        PooledByteBuffer j = aVar.j();
        return i >= 2 && j.p(i + (-2)) == -1 && j.p(i - 1) == -39;
    }

    public static BitmapFactory.Options g(int i, Bitmap.Config config) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inDither = true;
        options.inPreferredConfig = config;
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inSampleSize = i;
        if (Build.VERSION.SDK_INT >= 11) {
            options.inMutable = true;
        }
        return options;
    }

    @cq0
    private static native void nativePinBitmap(Bitmap bitmap);

    @Override // defpackage.dr2
    public com.facebook.common.references.a<Bitmap> a(zu0 zu0Var, Bitmap.Config config, Rect rect, int i) {
        return c(zu0Var, config, rect, i, null);
    }

    @Override // defpackage.dr2
    public com.facebook.common.references.a<Bitmap> b(zu0 zu0Var, Bitmap.Config config, Rect rect, ColorSpace colorSpace) {
        BitmapFactory.Options g = g(zu0Var.r(), config);
        if (Build.VERSION.SDK_INT >= 26) {
            OreoUtils.a(g, colorSpace);
        }
        com.facebook.common.references.a<PooledByteBuffer> e = zu0Var.e();
        xt2.g(e);
        try {
            return h(d(e, g));
        } finally {
            com.facebook.common.references.a.g(e);
        }
    }

    @Override // defpackage.dr2
    public com.facebook.common.references.a<Bitmap> c(zu0 zu0Var, Bitmap.Config config, Rect rect, int i, ColorSpace colorSpace) {
        BitmapFactory.Options g = g(zu0Var.r(), config);
        if (Build.VERSION.SDK_INT >= 26) {
            OreoUtils.a(g, colorSpace);
        }
        com.facebook.common.references.a<PooledByteBuffer> e = zu0Var.e();
        xt2.g(e);
        try {
            return h(e(e, i, g));
        } finally {
            com.facebook.common.references.a.g(e);
        }
    }

    public abstract Bitmap d(com.facebook.common.references.a<PooledByteBuffer> aVar, BitmapFactory.Options options);

    public abstract Bitmap e(com.facebook.common.references.a<PooledByteBuffer> aVar, int i, BitmapFactory.Options options);

    public com.facebook.common.references.a<Bitmap> h(Bitmap bitmap) {
        xt2.g(bitmap);
        try {
            nativePinBitmap(bitmap);
            if (this.a.g(bitmap)) {
                return com.facebook.common.references.a.A(bitmap, this.a.e());
            }
            int e = rq.e(bitmap);
            bitmap.recycle();
            throw new TooManyBitmapsException(String.format(Locale.US, "Attempted to pin a bitmap of size %d bytes. The current pool count is %d, the current pool size is %d bytes. The current pool max count is %d, the current pool max size is %d bytes.", Integer.valueOf(e), Integer.valueOf(this.a.b()), Long.valueOf(this.a.f()), Integer.valueOf(this.a.c()), Integer.valueOf(this.a.d())));
        } catch (Exception e2) {
            bitmap.recycle();
            throw d.a(e2);
        }
    }
}
