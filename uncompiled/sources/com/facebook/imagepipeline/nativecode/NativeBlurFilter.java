package com.facebook.imagepipeline.nativecode;

import android.graphics.Bitmap;

@cq0
/* loaded from: classes.dex */
public class NativeBlurFilter {
    static {
        md2.a();
    }

    @cq0
    private static native void nativeIterativeBoxBlur(Bitmap bitmap, int i, int i2);
}
