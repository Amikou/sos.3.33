package com.facebook.imagepipeline.nativecode;

import android.graphics.Bitmap;

@cq0
/* loaded from: classes.dex */
public class NativeRoundingFilter {
    static {
        md2.a();
    }

    @cq0
    private static native void nativeAddRoundedCornersFilter(Bitmap bitmap, int i, int i2, int i3, int i4);

    @cq0
    private static native void nativeToCircleFastFilter(Bitmap bitmap, boolean z);

    @cq0
    private static native void nativeToCircleFilter(Bitmap bitmap, boolean z);

    @cq0
    private static native void nativeToCircleWithBorderFilter(Bitmap bitmap, int i, int i2, boolean z);

    @cq0
    public static void toCircle(Bitmap bitmap, boolean z) {
        xt2.g(bitmap);
        if (bitmap.getWidth() < 3 || bitmap.getHeight() < 3) {
            return;
        }
        nativeToCircleFilter(bitmap, z);
    }

    @cq0
    public static void toCircleFast(Bitmap bitmap, boolean z) {
        xt2.g(bitmap);
        if (bitmap.getWidth() < 3 || bitmap.getHeight() < 3) {
            return;
        }
        nativeToCircleFastFilter(bitmap, z);
    }
}
