package com.facebook.imagepipeline.nativecode;

import android.graphics.Bitmap;

@cq0
/* loaded from: classes.dex */
public class Bitmaps {
    static {
        vo1.a();
    }

    @cq0
    public static void copyBitmap(Bitmap bitmap, Bitmap bitmap2) {
        xt2.b(Boolean.valueOf(bitmap2.getConfig() == bitmap.getConfig()));
        xt2.b(Boolean.valueOf(bitmap.isMutable()));
        xt2.b(Boolean.valueOf(bitmap.getWidth() == bitmap2.getWidth()));
        xt2.b(Boolean.valueOf(bitmap.getHeight() == bitmap2.getHeight()));
        nativeCopyBitmap(bitmap, bitmap.getRowBytes(), bitmap2, bitmap2.getRowBytes(), bitmap.getHeight());
    }

    @cq0
    private static native void nativeCopyBitmap(Bitmap bitmap, int i, Bitmap bitmap2, int i2, int i3);
}
