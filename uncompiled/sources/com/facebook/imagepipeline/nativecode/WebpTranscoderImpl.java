package com.facebook.imagepipeline.nativecode;

import android.os.Build;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

@cq0
/* loaded from: classes.dex */
public class WebpTranscoderImpl implements a {
    @cq0
    private static native void nativeTranscodeWebpToJpeg(InputStream inputStream, OutputStream outputStream, int i) throws IOException;

    @cq0
    private static native void nativeTranscodeWebpToPng(InputStream inputStream, OutputStream outputStream) throws IOException;

    @Override // com.facebook.imagepipeline.nativecode.a
    public void a(InputStream inputStream, OutputStream outputStream, int i) throws IOException {
        jt3.a();
        nativeTranscodeWebpToJpeg((InputStream) xt2.g(inputStream), (OutputStream) xt2.g(outputStream), i);
    }

    @Override // com.facebook.imagepipeline.nativecode.a
    public void b(InputStream inputStream, OutputStream outputStream) throws IOException {
        jt3.a();
        nativeTranscodeWebpToPng((InputStream) xt2.g(inputStream), (OutputStream) xt2.g(outputStream));
    }

    @Override // com.facebook.imagepipeline.nativecode.a
    public boolean c(wn1 wn1Var) {
        if (wn1Var == wj0.f) {
            return Build.VERSION.SDK_INT >= 14;
        } else if (wn1Var != wj0.g && wn1Var != wj0.h && wn1Var != wj0.i) {
            if (wn1Var == wj0.j) {
                return false;
            }
            throw new IllegalArgumentException("Image format is not a WebP.");
        } else {
            return po4.b;
        }
    }
}
