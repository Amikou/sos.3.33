package com.facebook.imagepipeline.image;

import java.io.Closeable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/* compiled from: CloseableImage.java */
/* loaded from: classes.dex */
public abstract class a implements Closeable, ao1 {
    public static final Set<String> f0 = new HashSet(Arrays.asList("encoded_size", "encoded_width", "encoded_height", "uri_source", "image_format", "bitmap_config", "is_rounded"));
    public Map<String, Object> a = new HashMap();

    public xw2 a() {
        return jp1.d;
    }

    public abstract int b();

    public boolean c() {
        return false;
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public abstract void close();

    public void d(String str, Object obj) {
        if (f0.contains(str)) {
            this.a.put(str, obj);
        }
    }

    public void e(Map<String, Object> map) {
        if (map == null) {
            return;
        }
        for (String str : f0) {
            Object obj = map.get(str);
            if (obj != null) {
                this.a.put(str, obj);
            }
        }
    }

    public void finalize() throws Throwable {
        if (isClosed()) {
            return;
        }
        v11.x("CloseableImage", "finalize: %s %x still open.", getClass().getSimpleName(), Integer.valueOf(System.identityHashCode(this)));
        try {
            close();
        } finally {
            super.finalize();
        }
    }

    @Override // defpackage.zj1
    public Map<String, Object> getExtras() {
        return this.a;
    }

    public abstract boolean isClosed();
}
