package com.facebook.imagepipeline.memory;

import java.io.IOException;

/* loaded from: classes.dex */
public class MemoryPooledByteBufferOutputStream extends ct2 {
    public final c a;
    public com.facebook.common.references.a<b> f0;
    public int g0;

    /* loaded from: classes.dex */
    public static class InvalidStreamException extends RuntimeException {
        public InvalidStreamException() {
            super("OutputStream no longer valid");
        }
    }

    public MemoryPooledByteBufferOutputStream(c cVar) {
        this(cVar, cVar.C());
    }

    public final void b() {
        if (!com.facebook.common.references.a.u(this.f0)) {
            throw new InvalidStreamException();
        }
    }

    public void c(int i) {
        b();
        xt2.g(this.f0);
        if (i <= this.f0.j().a()) {
            return;
        }
        b bVar = this.a.get(i);
        xt2.g(this.f0);
        this.f0.j().b(0, bVar, 0, this.g0);
        this.f0.close();
        this.f0 = com.facebook.common.references.a.A(bVar, this.a);
    }

    @Override // defpackage.ct2, java.io.OutputStream, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        com.facebook.common.references.a.g(this.f0);
        this.f0 = null;
        this.g0 = -1;
        super.close();
    }

    @Override // defpackage.ct2
    /* renamed from: d */
    public d a() {
        b();
        return new d((com.facebook.common.references.a) xt2.g(this.f0), this.g0);
    }

    @Override // defpackage.ct2
    public int size() {
        return this.g0;
    }

    @Override // java.io.OutputStream
    public void write(int i) throws IOException {
        write(new byte[]{(byte) i});
    }

    public MemoryPooledByteBufferOutputStream(c cVar, int i) {
        xt2.b(Boolean.valueOf(i > 0));
        c cVar2 = (c) xt2.g(cVar);
        this.a = cVar2;
        this.g0 = 0;
        this.f0 = com.facebook.common.references.a.A(cVar2.get(i), cVar2);
    }

    @Override // java.io.OutputStream
    public void write(byte[] bArr, int i, int i2) throws IOException {
        if (i >= 0 && i2 >= 0 && i + i2 <= bArr.length) {
            b();
            c(this.g0 + i2);
            ((b) ((com.facebook.common.references.a) xt2.g(this.f0)).j()).c(this.g0, bArr, i, i2);
            this.g0 += i2;
            return;
        }
        throw new ArrayIndexOutOfBoundsException("length=" + bArr.length + "; regionStart=" + i + "; regionLength=" + i2);
    }
}
