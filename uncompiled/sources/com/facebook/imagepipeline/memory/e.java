package com.facebook.imagepipeline.memory;

import java.io.IOException;
import java.io.InputStream;

/* compiled from: MemoryPooledByteBufferFactory.java */
/* loaded from: classes.dex */
public class e implements com.facebook.common.memory.b {
    public final com.facebook.common.memory.c a;
    public final c b;

    public e(c cVar, com.facebook.common.memory.c cVar2) {
        this.b = cVar;
        this.a = cVar2;
    }

    public d f(InputStream inputStream, MemoryPooledByteBufferOutputStream memoryPooledByteBufferOutputStream) throws IOException {
        this.a.a(inputStream, memoryPooledByteBufferOutputStream);
        return memoryPooledByteBufferOutputStream.a();
    }

    @Override // com.facebook.common.memory.b
    /* renamed from: g */
    public d c(InputStream inputStream) throws IOException {
        MemoryPooledByteBufferOutputStream memoryPooledByteBufferOutputStream = new MemoryPooledByteBufferOutputStream(this.b);
        try {
            return f(inputStream, memoryPooledByteBufferOutputStream);
        } finally {
            memoryPooledByteBufferOutputStream.close();
        }
    }

    @Override // com.facebook.common.memory.b
    /* renamed from: h */
    public d d(InputStream inputStream, int i) throws IOException {
        MemoryPooledByteBufferOutputStream memoryPooledByteBufferOutputStream = new MemoryPooledByteBufferOutputStream(this.b, i);
        try {
            return f(inputStream, memoryPooledByteBufferOutputStream);
        } finally {
            memoryPooledByteBufferOutputStream.close();
        }
    }

    @Override // com.facebook.common.memory.b
    /* renamed from: i */
    public d b(byte[] bArr) {
        MemoryPooledByteBufferOutputStream memoryPooledByteBufferOutputStream = new MemoryPooledByteBufferOutputStream(this.b, bArr.length);
        try {
            try {
                memoryPooledByteBufferOutputStream.write(bArr, 0, bArr.length);
                return memoryPooledByteBufferOutputStream.a();
            } catch (IOException e) {
                throw com.facebook.common.internal.d.a(e);
            }
        } finally {
            memoryPooledByteBufferOutputStream.close();
        }
    }

    @Override // com.facebook.common.memory.b
    /* renamed from: j */
    public MemoryPooledByteBufferOutputStream a() {
        return new MemoryPooledByteBufferOutputStream(this.b);
    }

    @Override // com.facebook.common.memory.b
    /* renamed from: k */
    public MemoryPooledByteBufferOutputStream e(int i) {
        return new MemoryPooledByteBufferOutputStream(this.b, i);
    }
}
