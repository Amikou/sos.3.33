package com.facebook.imagepipeline.memory;

import java.io.Closeable;
import java.nio.ByteBuffer;

@cq0
/* loaded from: classes.dex */
public class NativeMemoryChunk implements b, Closeable {
    public final long a;
    public final int f0;
    public boolean g0;

    static {
        pd2.d("imagepipeline");
    }

    public NativeMemoryChunk(int i) {
        xt2.b(Boolean.valueOf(i > 0));
        this.f0 = i;
        this.a = nativeAllocate(i);
        this.g0 = false;
    }

    @cq0
    private static native long nativeAllocate(int i);

    @cq0
    private static native void nativeCopyFromByteArray(long j, byte[] bArr, int i, int i2);

    @cq0
    private static native void nativeCopyToByteArray(long j, byte[] bArr, int i, int i2);

    @cq0
    private static native void nativeFree(long j);

    @cq0
    private static native void nativeMemcpy(long j, long j2, int i);

    @cq0
    private static native byte nativeReadByte(long j);

    @Override // com.facebook.imagepipeline.memory.b
    public int a() {
        return this.f0;
    }

    @Override // com.facebook.imagepipeline.memory.b
    public void b(int i, b bVar, int i2, int i3) {
        xt2.g(bVar);
        if (bVar.getUniqueId() == getUniqueId()) {
            StringBuilder sb = new StringBuilder();
            sb.append("Copying from NativeMemoryChunk ");
            sb.append(Integer.toHexString(System.identityHashCode(this)));
            sb.append(" to NativeMemoryChunk ");
            sb.append(Integer.toHexString(System.identityHashCode(bVar)));
            sb.append(" which share the same address ");
            sb.append(Long.toHexString(this.a));
            xt2.b(Boolean.FALSE);
        }
        if (bVar.getUniqueId() < getUniqueId()) {
            synchronized (bVar) {
                synchronized (this) {
                    d(i, bVar, i2, i3);
                }
            }
            return;
        }
        synchronized (this) {
            synchronized (bVar) {
                d(i, bVar, i2, i3);
            }
        }
    }

    @Override // com.facebook.imagepipeline.memory.b
    public synchronized int c(int i, byte[] bArr, int i2, int i3) {
        int a;
        xt2.g(bArr);
        xt2.i(!isClosed());
        a = o72.a(i, i3, this.f0);
        o72.b(i, bArr.length, i2, a, this.f0);
        nativeCopyFromByteArray(this.a + i, bArr, i2, a);
        return a;
    }

    @Override // com.facebook.imagepipeline.memory.b, java.io.Closeable, java.lang.AutoCloseable
    public synchronized void close() {
        if (!this.g0) {
            this.g0 = true;
            nativeFree(this.a);
        }
    }

    public final void d(int i, b bVar, int i2, int i3) {
        if (bVar instanceof NativeMemoryChunk) {
            xt2.i(!isClosed());
            xt2.i(!bVar.isClosed());
            o72.b(i, bVar.a(), i2, i3, this.f0);
            nativeMemcpy(bVar.y() + i2, this.a + i, i3);
            return;
        }
        throw new IllegalArgumentException("Cannot copy two incompatible MemoryChunks");
    }

    public void finalize() throws Throwable {
        if (isClosed()) {
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("finalize: Chunk ");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" still active. ");
        try {
            close();
        } finally {
            super.finalize();
        }
    }

    @Override // com.facebook.imagepipeline.memory.b
    public long getUniqueId() {
        return this.a;
    }

    @Override // com.facebook.imagepipeline.memory.b
    public synchronized boolean isClosed() {
        return this.g0;
    }

    @Override // com.facebook.imagepipeline.memory.b
    public synchronized byte p(int i) {
        boolean z = true;
        xt2.i(!isClosed());
        xt2.b(Boolean.valueOf(i >= 0));
        if (i >= this.f0) {
            z = false;
        }
        xt2.b(Boolean.valueOf(z));
        return nativeReadByte(this.a + i);
    }

    @Override // com.facebook.imagepipeline.memory.b
    public synchronized int s(int i, byte[] bArr, int i2, int i3) {
        int a;
        xt2.g(bArr);
        xt2.i(!isClosed());
        a = o72.a(i, i3, this.f0);
        o72.b(i, bArr.length, i2, a, this.f0);
        nativeCopyToByteArray(this.a + i, bArr, i2, a);
        return a;
    }

    @Override // com.facebook.imagepipeline.memory.b
    public ByteBuffer t() {
        return null;
    }

    @Override // com.facebook.imagepipeline.memory.b
    public long y() {
        return this.a;
    }

    public NativeMemoryChunk() {
        this.f0 = 0;
        this.a = 0L;
        this.g0 = true;
    }
}
