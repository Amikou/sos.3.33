package com.facebook.imagepipeline.memory;

import android.util.SparseIntArray;
import com.facebook.imagepipeline.memory.BasePool;

/* compiled from: MemoryChunkPool.java */
/* loaded from: classes.dex */
public abstract class c extends BasePool<b> {
    public final int[] k;

    public c(r72 r72Var, ys2 ys2Var, zs2 zs2Var) {
        super(r72Var, ys2Var, zs2Var);
        SparseIntArray sparseIntArray = (SparseIntArray) xt2.g(ys2Var.c);
        this.k = new int[sparseIntArray.size()];
        int i = 0;
        while (true) {
            int[] iArr = this.k;
            if (i < iArr.length) {
                iArr[i] = sparseIntArray.keyAt(i);
                i++;
            } else {
                r();
                return;
            }
        }
    }

    @Override // com.facebook.imagepipeline.memory.BasePool
    /* renamed from: A */
    public void j(b bVar) {
        xt2.g(bVar);
        bVar.close();
    }

    @Override // com.facebook.imagepipeline.memory.BasePool
    /* renamed from: B */
    public int n(b bVar) {
        xt2.g(bVar);
        return bVar.a();
    }

    public int C() {
        return this.k[0];
    }

    @Override // com.facebook.imagepipeline.memory.BasePool
    /* renamed from: D */
    public boolean t(b bVar) {
        xt2.g(bVar);
        return !bVar.isClosed();
    }

    @Override // com.facebook.imagepipeline.memory.BasePool
    public int m(int i) {
        int[] iArr;
        if (i > 0) {
            for (int i2 : this.k) {
                if (i2 >= i) {
                    return i2;
                }
            }
            return i;
        }
        throw new BasePool.InvalidSizeException(Integer.valueOf(i));
    }

    @Override // com.facebook.imagepipeline.memory.BasePool
    public int o(int i) {
        return i;
    }

    @Override // com.facebook.imagepipeline.memory.BasePool
    /* renamed from: z */
    public abstract b f(int i);
}
