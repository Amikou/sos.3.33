package com.facebook.imagepipeline.memory;

import java.nio.ByteBuffer;

/* compiled from: MemoryChunk.java */
/* loaded from: classes.dex */
public interface b {
    int a();

    void b(int i, b bVar, int i2, int i3);

    int c(int i, byte[] bArr, int i2, int i3);

    void close();

    long getUniqueId();

    boolean isClosed();

    byte p(int i);

    int s(int i, byte[] bArr, int i2, int i3);

    ByteBuffer t();

    long y() throws UnsupportedOperationException;
}
