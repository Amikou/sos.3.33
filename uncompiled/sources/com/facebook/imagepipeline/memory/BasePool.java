package com.facebook.imagepipeline.memory;

import android.annotation.SuppressLint;
import android.util.SparseArray;
import android.util.SparseIntArray;
import java.util.Set;

/* loaded from: classes.dex */
public abstract class BasePool<V> implements us2<V> {
    public final Class<?> a;
    public final r72 b;
    public final ys2 c;
    public final SparseArray<or<V>> d;
    public final Set<V> e;
    public boolean f;
    public final a g;
    public final a h;
    public final zs2 i;
    public boolean j;

    /* loaded from: classes.dex */
    public static class InvalidSizeException extends RuntimeException {
        public InvalidSizeException(Object obj) {
            super("Invalid size: " + obj.toString());
        }
    }

    /* loaded from: classes.dex */
    public static class PoolSizeViolationException extends RuntimeException {
        public PoolSizeViolationException(int i, int i2, int i3, int i4) {
            super("Pool hard cap violation? Hard cap = " + i + " Used size = " + i2 + " Free size = " + i3 + " Request size = " + i4);
        }
    }

    /* loaded from: classes.dex */
    public static class a {
        public int a;
        public int b;

        public void a(int i) {
            int i2;
            int i3 = this.b;
            if (i3 >= i && (i2 = this.a) > 0) {
                this.a = i2 - 1;
                this.b = i3 - i;
                return;
            }
            v11.A("com.facebook.imagepipeline.memory.BasePool.Counter", "Unexpected decrement of %d. Current numBytes = %d, count = %d", Integer.valueOf(i), Integer.valueOf(this.b), Integer.valueOf(this.a));
        }

        public void b(int i) {
            this.a++;
            this.b += i;
        }
    }

    public BasePool(r72 r72Var, ys2 ys2Var, zs2 zs2Var) {
        this.a = getClass();
        this.b = (r72) xt2.g(r72Var);
        ys2 ys2Var2 = (ys2) xt2.g(ys2Var);
        this.c = ys2Var2;
        this.i = (zs2) xt2.g(zs2Var);
        this.d = new SparseArray<>();
        if (ys2Var2.d) {
            q();
        } else {
            u(new SparseIntArray(0));
        }
        this.e = rm3.b();
        this.h = new a();
        this.g = new a();
    }

    /* JADX WARN: Code restructure failed: missing block: B:19:0x0080, code lost:
        r2.b();
     */
    @Override // defpackage.us2, defpackage.d83
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void a(V r8) {
        /*
            r7 = this;
            defpackage.xt2.g(r8)
            int r0 = r7.n(r8)
            int r1 = r7.o(r0)
            monitor-enter(r7)
            or r2 = r7.l(r0)     // Catch: java.lang.Throwable -> Lae
            java.util.Set<V> r3 = r7.e     // Catch: java.lang.Throwable -> Lae
            boolean r3 = r3.remove(r8)     // Catch: java.lang.Throwable -> Lae
            r4 = 2
            if (r3 != 0) goto L3d
            java.lang.Class<?> r2 = r7.a     // Catch: java.lang.Throwable -> Lae
            java.lang.String r3 = "release (free, value unrecognized) (object, size) = (%x, %s)"
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch: java.lang.Throwable -> Lae
            r5 = 0
            int r6 = java.lang.System.identityHashCode(r8)     // Catch: java.lang.Throwable -> Lae
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch: java.lang.Throwable -> Lae
            r4[r5] = r6     // Catch: java.lang.Throwable -> Lae
            r5 = 1
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch: java.lang.Throwable -> Lae
            r4[r5] = r0     // Catch: java.lang.Throwable -> Lae
            defpackage.v11.f(r2, r3, r4)     // Catch: java.lang.Throwable -> Lae
            r7.j(r8)     // Catch: java.lang.Throwable -> Lae
            zs2 r8 = r7.i     // Catch: java.lang.Throwable -> Lae
            r8.e(r1)     // Catch: java.lang.Throwable -> Lae
            goto La9
        L3d:
            if (r2 == 0) goto L7e
            boolean r3 = r2.f()     // Catch: java.lang.Throwable -> Lae
            if (r3 != 0) goto L7e
            boolean r3 = r7.s()     // Catch: java.lang.Throwable -> Lae
            if (r3 != 0) goto L7e
            boolean r3 = r7.t(r8)     // Catch: java.lang.Throwable -> Lae
            if (r3 != 0) goto L52
            goto L7e
        L52:
            r2.h(r8)     // Catch: java.lang.Throwable -> Lae
            com.facebook.imagepipeline.memory.BasePool$a r2 = r7.h     // Catch: java.lang.Throwable -> Lae
            r2.b(r1)     // Catch: java.lang.Throwable -> Lae
            com.facebook.imagepipeline.memory.BasePool$a r2 = r7.g     // Catch: java.lang.Throwable -> Lae
            r2.a(r1)     // Catch: java.lang.Throwable -> Lae
            zs2 r2 = r7.i     // Catch: java.lang.Throwable -> Lae
            r2.g(r1)     // Catch: java.lang.Throwable -> Lae
            boolean r1 = defpackage.v11.m(r4)     // Catch: java.lang.Throwable -> Lae
            if (r1 == 0) goto La9
            java.lang.Class<?> r1 = r7.a     // Catch: java.lang.Throwable -> Lae
            java.lang.String r2 = "release (reuse) (object, size) = (%x, %s)"
            int r8 = java.lang.System.identityHashCode(r8)     // Catch: java.lang.Throwable -> Lae
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch: java.lang.Throwable -> Lae
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch: java.lang.Throwable -> Lae
            defpackage.v11.p(r1, r2, r8, r0)     // Catch: java.lang.Throwable -> Lae
            goto La9
        L7e:
            if (r2 == 0) goto L83
            r2.b()     // Catch: java.lang.Throwable -> Lae
        L83:
            boolean r2 = defpackage.v11.m(r4)     // Catch: java.lang.Throwable -> Lae
            if (r2 == 0) goto L9c
            java.lang.Class<?> r2 = r7.a     // Catch: java.lang.Throwable -> Lae
            java.lang.String r3 = "release (free) (object, size) = (%x, %s)"
            int r4 = java.lang.System.identityHashCode(r8)     // Catch: java.lang.Throwable -> Lae
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch: java.lang.Throwable -> Lae
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch: java.lang.Throwable -> Lae
            defpackage.v11.p(r2, r3, r4, r0)     // Catch: java.lang.Throwable -> Lae
        L9c:
            r7.j(r8)     // Catch: java.lang.Throwable -> Lae
            com.facebook.imagepipeline.memory.BasePool$a r8 = r7.g     // Catch: java.lang.Throwable -> Lae
            r8.a(r1)     // Catch: java.lang.Throwable -> Lae
            zs2 r8 = r7.i     // Catch: java.lang.Throwable -> Lae
            r8.e(r1)     // Catch: java.lang.Throwable -> Lae
        La9:
            r7.v()     // Catch: java.lang.Throwable -> Lae
            monitor-exit(r7)     // Catch: java.lang.Throwable -> Lae
            return
        Lae:
            r8 = move-exception
            monitor-exit(r7)     // Catch: java.lang.Throwable -> Lae
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.imagepipeline.memory.BasePool.a(java.lang.Object):void");
    }

    public abstract V f(int i);

    public synchronized boolean g(int i) {
        if (this.j) {
            return true;
        }
        ys2 ys2Var = this.c;
        int i2 = ys2Var.a;
        int i3 = this.g.b;
        if (i > i2 - i3) {
            this.i.f();
            return false;
        }
        int i4 = ys2Var.b;
        if (i > i4 - (i3 + this.h.b)) {
            x(i4 - i);
        }
        if (i > i2 - (this.g.b + this.h.b)) {
            this.i.f();
            return false;
        }
        return true;
    }

    @Override // defpackage.us2
    public V get(int i) {
        V p;
        h();
        int m = m(i);
        synchronized (this) {
            or<V> k = k(m);
            if (k != null && (p = p(k)) != null) {
                xt2.i(this.e.add(p));
                int n = n(p);
                int o = o(n);
                this.g.b(o);
                this.h.a(o);
                this.i.b(o);
                v();
                if (v11.m(2)) {
                    v11.p(this.a, "get (reuse) (object, size) = (%x, %s)", Integer.valueOf(System.identityHashCode(p)), Integer.valueOf(n));
                }
                return p;
            }
            int o2 = o(m);
            if (g(o2)) {
                this.g.b(o2);
                if (k != null) {
                    k.e();
                }
                V v = null;
                try {
                    v = f(m);
                } catch (Throwable th) {
                    synchronized (this) {
                        this.g.a(o2);
                        or<V> k2 = k(m);
                        if (k2 != null) {
                            k2.b();
                        }
                        com.facebook.common.internal.d.c(th);
                    }
                }
                synchronized (this) {
                    xt2.i(this.e.add(v));
                    y();
                    this.i.a(o2);
                    v();
                    if (v11.m(2)) {
                        v11.p(this.a, "get (alloc) (object, size) = (%x, %s)", Integer.valueOf(System.identityHashCode(v)), Integer.valueOf(m));
                    }
                }
                return v;
            }
            throw new PoolSizeViolationException(this.c.a, this.g.b, this.h.b, o2);
        }
    }

    public final synchronized void h() {
        boolean z;
        if (s() && this.h.b != 0) {
            z = false;
            xt2.i(z);
        }
        z = true;
        xt2.i(z);
    }

    public final void i(SparseIntArray sparseIntArray) {
        this.d.clear();
        for (int i = 0; i < sparseIntArray.size(); i++) {
            int keyAt = sparseIntArray.keyAt(i);
            this.d.put(keyAt, new or<>(o(keyAt), sparseIntArray.valueAt(i), 0, this.c.d));
        }
    }

    public abstract void j(V v);

    public synchronized or<V> k(int i) {
        or<V> orVar = this.d.get(i);
        if (orVar == null && this.f) {
            if (v11.m(2)) {
                v11.o(this.a, "creating new bucket %s", Integer.valueOf(i));
            }
            or<V> w = w(i);
            this.d.put(i, w);
            return w;
        }
        return orVar;
    }

    public final synchronized or<V> l(int i) {
        return this.d.get(i);
    }

    public abstract int m(int i);

    public abstract int n(V v);

    public abstract int o(int i);

    public synchronized V p(or<V> orVar) {
        return orVar.c();
    }

    public final synchronized void q() {
        SparseIntArray sparseIntArray = this.c.c;
        if (sparseIntArray != null) {
            i(sparseIntArray);
            this.f = false;
        } else {
            this.f = true;
        }
    }

    public void r() {
        this.b.a(this);
        this.i.c(this);
    }

    public synchronized boolean s() {
        boolean z;
        z = this.g.b + this.h.b > this.c.b;
        if (z) {
            this.i.d();
        }
        return z;
    }

    public boolean t(V v) {
        xt2.g(v);
        return true;
    }

    public final synchronized void u(SparseIntArray sparseIntArray) {
        xt2.g(sparseIntArray);
        this.d.clear();
        SparseIntArray sparseIntArray2 = this.c.c;
        if (sparseIntArray2 != null) {
            for (int i = 0; i < sparseIntArray2.size(); i++) {
                int keyAt = sparseIntArray2.keyAt(i);
                this.d.put(keyAt, new or<>(o(keyAt), sparseIntArray2.valueAt(i), sparseIntArray.get(keyAt, 0), this.c.d));
            }
            this.f = false;
        } else {
            this.f = true;
        }
    }

    @SuppressLint({"InvalidAccessToGuardedField"})
    public final void v() {
        if (v11.m(2)) {
            v11.r(this.a, "Used = (%d, %d); Free = (%d, %d)", Integer.valueOf(this.g.a), Integer.valueOf(this.g.b), Integer.valueOf(this.h.a), Integer.valueOf(this.h.b));
        }
    }

    public or<V> w(int i) {
        return new or<>(o(i), Integer.MAX_VALUE, 0, this.c.d);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public synchronized void x(int i) {
        int i2 = this.g.b;
        int i3 = this.h.b;
        int min = Math.min((i2 + i3) - i, i3);
        if (min <= 0) {
            return;
        }
        if (v11.m(2)) {
            v11.q(this.a, "trimToSize: TargetSize = %d; Initial Size = %d; Bytes to free = %d", Integer.valueOf(i), Integer.valueOf(this.g.b + this.h.b), Integer.valueOf(min));
        }
        v();
        for (int i4 = 0; i4 < this.d.size() && min > 0; i4++) {
            or orVar = (or) xt2.g(this.d.valueAt(i4));
            while (min > 0) {
                Object g = orVar.g();
                if (g == null) {
                    break;
                }
                j(g);
                int i5 = orVar.a;
                min -= i5;
                this.h.a(i5);
            }
        }
        v();
        if (v11.m(2)) {
            v11.p(this.a, "trimToSize: TargetSize = %d; Final Size = %d", Integer.valueOf(i), Integer.valueOf(this.g.b + this.h.b));
        }
    }

    public synchronized void y() {
        if (s()) {
            x(this.c.b);
        }
    }

    public BasePool(r72 r72Var, ys2 ys2Var, zs2 zs2Var, boolean z) {
        this(r72Var, ys2Var, zs2Var);
        this.j = z;
    }
}
