package com.facebook.imagepipeline.memory;

@cq0
/* loaded from: classes.dex */
public class NativeMemoryChunkPool extends c {
    @cq0
    public NativeMemoryChunkPool(r72 r72Var, ys2 ys2Var, zs2 zs2Var) {
        super(r72Var, ys2Var, zs2Var);
    }

    @Override // com.facebook.imagepipeline.memory.c
    /* renamed from: E */
    public NativeMemoryChunk z(int i) {
        return new NativeMemoryChunk(i);
    }
}
