package com.facebook.imagepipeline.memory;

import android.annotation.TargetApi;

@cq0
@TargetApi(27)
/* loaded from: classes.dex */
public class AshmemMemoryChunkPool extends c {
    @cq0
    public AshmemMemoryChunkPool(r72 r72Var, ys2 ys2Var, zs2 zs2Var) {
        super(r72Var, ys2Var, zs2Var);
    }

    @Override // com.facebook.imagepipeline.memory.c
    /* renamed from: E */
    public fi z(int i) {
        return new fi(i);
    }
}
