package com.facebook.imagepipeline.memory;

import com.facebook.common.memory.PooledByteBuffer;
import java.nio.ByteBuffer;

/* compiled from: MemoryPooledByteBuffer.java */
/* loaded from: classes.dex */
public class d implements PooledByteBuffer {
    public final int a;
    public com.facebook.common.references.a<b> f0;

    public d(com.facebook.common.references.a<b> aVar, int i) {
        xt2.g(aVar);
        xt2.b(Boolean.valueOf(i >= 0 && i <= aVar.j().a()));
        this.f0 = aVar.clone();
        this.a = i;
    }

    public synchronized void a() {
        if (isClosed()) {
            throw new PooledByteBuffer.ClosedException();
        }
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public synchronized void close() {
        com.facebook.common.references.a.g(this.f0);
        this.f0 = null;
    }

    @Override // com.facebook.common.memory.PooledByteBuffer
    public synchronized boolean isClosed() {
        return !com.facebook.common.references.a.u(this.f0);
    }

    @Override // com.facebook.common.memory.PooledByteBuffer
    public synchronized byte p(int i) {
        a();
        boolean z = true;
        xt2.b(Boolean.valueOf(i >= 0));
        if (i >= this.a) {
            z = false;
        }
        xt2.b(Boolean.valueOf(z));
        return this.f0.j().p(i);
    }

    @Override // com.facebook.common.memory.PooledByteBuffer
    public synchronized int s(int i, byte[] bArr, int i2, int i3) {
        a();
        xt2.b(Boolean.valueOf(i + i3 <= this.a));
        return this.f0.j().s(i, bArr, i2, i3);
    }

    @Override // com.facebook.common.memory.PooledByteBuffer
    public synchronized int size() {
        a();
        return this.a;
    }

    @Override // com.facebook.common.memory.PooledByteBuffer
    public synchronized ByteBuffer t() {
        return this.f0.j().t();
    }

    @Override // com.facebook.common.memory.PooledByteBuffer
    public synchronized long y() throws UnsupportedOperationException {
        a();
        return this.f0.j().y();
    }
}
