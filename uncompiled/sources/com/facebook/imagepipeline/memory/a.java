package com.facebook.imagepipeline.memory;

import android.util.SparseIntArray;
import com.facebook.imagepipeline.memory.BasePool;

/* compiled from: GenericByteArrayPool.java */
/* loaded from: classes.dex */
public class a extends BasePool<byte[]> implements os {
    public final int[] k;

    public a(r72 r72Var, ys2 ys2Var, zs2 zs2Var) {
        super(r72Var, ys2Var, zs2Var);
        SparseIntArray sparseIntArray = (SparseIntArray) xt2.g(ys2Var.c);
        this.k = new int[sparseIntArray.size()];
        for (int i = 0; i < sparseIntArray.size(); i++) {
            this.k[i] = sparseIntArray.keyAt(i);
        }
        r();
    }

    @Override // com.facebook.imagepipeline.memory.BasePool
    /* renamed from: A */
    public void j(byte[] bArr) {
        xt2.g(bArr);
    }

    @Override // com.facebook.imagepipeline.memory.BasePool
    /* renamed from: B */
    public int n(byte[] bArr) {
        xt2.g(bArr);
        return bArr.length;
    }

    @Override // com.facebook.imagepipeline.memory.BasePool
    public int m(int i) {
        int[] iArr;
        if (i > 0) {
            for (int i2 : this.k) {
                if (i2 >= i) {
                    return i2;
                }
            }
            return i;
        }
        throw new BasePool.InvalidSizeException(Integer.valueOf(i));
    }

    @Override // com.facebook.imagepipeline.memory.BasePool
    public int o(int i) {
        return i;
    }

    @Override // com.facebook.imagepipeline.memory.BasePool
    /* renamed from: z */
    public byte[] f(int i) {
        return new byte[i];
    }
}
