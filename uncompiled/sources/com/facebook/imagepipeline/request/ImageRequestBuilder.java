package com.facebook.imagepipeline.request;

import android.net.Uri;
import com.facebook.imagepipeline.common.Priority;
import com.facebook.imagepipeline.request.ImageRequest;

/* loaded from: classes.dex */
public class ImageRequestBuilder {
    public p73 d;
    public h73 n;
    public int q;
    public Uri a = null;
    public ImageRequest.RequestLevel b = ImageRequest.RequestLevel.FULL_FETCH;
    public int c = 0;
    public p93 e = null;
    public rn1 f = rn1.a();
    public ImageRequest.CacheChoice g = ImageRequest.CacheChoice.DEFAULT;
    public boolean h = ro1.F().a();
    public boolean i = false;
    public boolean j = false;
    public Priority k = Priority.HIGH;
    public rt2 l = null;
    public Boolean m = null;
    public ct o = null;
    public Boolean p = null;

    /* loaded from: classes.dex */
    public static class BuilderException extends RuntimeException {
        public BuilderException(String str) {
            super("Invalid request builder: " + str);
        }
    }

    public static ImageRequestBuilder b(ImageRequest imageRequest) {
        return u(imageRequest.u()).z(imageRequest.g()).v(imageRequest.c()).w(imageRequest.d()).B(imageRequest.i()).A(imageRequest.h()).C(imageRequest.j()).x(imageRequest.e()).D(imageRequest.k()).E(imageRequest.o()).G(imageRequest.n()).H(imageRequest.q()).F(imageRequest.p()).I(imageRequest.s()).J(imageRequest.y()).y(imageRequest.f());
    }

    public static ImageRequestBuilder u(Uri uri) {
        return new ImageRequestBuilder().K(uri);
    }

    public ImageRequestBuilder A(boolean z) {
        this.j = z;
        return this;
    }

    public ImageRequestBuilder B(boolean z) {
        this.i = z;
        return this;
    }

    public ImageRequestBuilder C(ImageRequest.RequestLevel requestLevel) {
        this.b = requestLevel;
        return this;
    }

    public ImageRequestBuilder D(rt2 rt2Var) {
        this.l = rt2Var;
        return this;
    }

    public ImageRequestBuilder E(boolean z) {
        this.h = z;
        return this;
    }

    public ImageRequestBuilder F(h73 h73Var) {
        this.n = h73Var;
        return this;
    }

    public ImageRequestBuilder G(Priority priority) {
        this.k = priority;
        return this;
    }

    public ImageRequestBuilder H(p73 p73Var) {
        return this;
    }

    public ImageRequestBuilder I(p93 p93Var) {
        this.e = p93Var;
        return this;
    }

    public ImageRequestBuilder J(Boolean bool) {
        this.m = bool;
        return this;
    }

    public ImageRequestBuilder K(Uri uri) {
        xt2.g(uri);
        this.a = uri;
        return this;
    }

    public Boolean L() {
        return this.m;
    }

    public void M() {
        Uri uri = this.a;
        if (uri != null) {
            if (qf4.k(uri)) {
                if (this.a.isAbsolute()) {
                    if (!this.a.getPath().isEmpty()) {
                        try {
                            Integer.parseInt(this.a.getPath().substring(1));
                        } catch (NumberFormatException unused) {
                            throw new BuilderException("Resource URI path must be a resource id.");
                        }
                    } else {
                        throw new BuilderException("Resource URI must not be empty");
                    }
                } else {
                    throw new BuilderException("Resource URI path must be absolute.");
                }
            }
            if (qf4.f(this.a) && !this.a.isAbsolute()) {
                throw new BuilderException("Asset URI path must be absolute.");
            }
            return;
        }
        throw new BuilderException("Source must be set!");
    }

    public ImageRequest a() {
        M();
        return new ImageRequest(this);
    }

    public ct c() {
        return this.o;
    }

    public ImageRequest.CacheChoice d() {
        return this.g;
    }

    public int e() {
        return this.c;
    }

    public int f() {
        return this.q;
    }

    public rn1 g() {
        return this.f;
    }

    public boolean h() {
        return this.j;
    }

    public ImageRequest.RequestLevel i() {
        return this.b;
    }

    public rt2 j() {
        return this.l;
    }

    public h73 k() {
        return this.n;
    }

    public Priority l() {
        return this.k;
    }

    public p73 m() {
        return this.d;
    }

    public Boolean n() {
        return this.p;
    }

    public p93 o() {
        return this.e;
    }

    public Uri p() {
        return this.a;
    }

    public boolean q() {
        return (this.c & 48) == 0 && qf4.l(this.a);
    }

    public boolean r() {
        return this.i;
    }

    public boolean s() {
        return (this.c & 15) == 0;
    }

    public boolean t() {
        return this.h;
    }

    public ImageRequestBuilder v(ct ctVar) {
        this.o = ctVar;
        return this;
    }

    public ImageRequestBuilder w(ImageRequest.CacheChoice cacheChoice) {
        this.g = cacheChoice;
        return this;
    }

    public final ImageRequestBuilder x(int i) {
        this.c = i;
        return this;
    }

    public ImageRequestBuilder y(int i) {
        this.q = i;
        return this;
    }

    public ImageRequestBuilder z(rn1 rn1Var) {
        this.f = rn1Var;
        return this;
    }
}
