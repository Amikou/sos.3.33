package com.facebook.imagepipeline.request;

import android.net.Uri;
import com.facebook.imagepipeline.common.Priority;
import java.io.File;

/* loaded from: classes.dex */
public class ImageRequest {
    public static boolean w;
    public static boolean x;
    public static final i81<ImageRequest, Uri> y = new a();
    public int a;
    public final CacheChoice b;
    public final Uri c;
    public final int d;
    public File e;
    public final boolean f;
    public final boolean g;
    public final boolean h;
    public final rn1 i;
    public final p73 j;
    public final p93 k;
    public final ct l;
    public final Priority m;
    public final RequestLevel n;
    public final int o;
    public final boolean p;
    public final boolean q;
    public final Boolean r;
    public final rt2 s;
    public final h73 t;
    public final Boolean u;
    public final int v;

    /* loaded from: classes.dex */
    public enum CacheChoice {
        SMALL,
        DEFAULT
    }

    /* loaded from: classes.dex */
    public enum RequestLevel {
        FULL_FETCH(1),
        DISK_CACHE(2),
        ENCODED_MEMORY_CACHE(3),
        BITMAP_MEMORY_CACHE(4);
        
        private int mValue;

        RequestLevel(int i) {
            this.mValue = i;
        }

        public static RequestLevel getMax(RequestLevel requestLevel, RequestLevel requestLevel2) {
            return requestLevel.getValue() > requestLevel2.getValue() ? requestLevel : requestLevel2;
        }

        public int getValue() {
            return this.mValue;
        }
    }

    /* loaded from: classes.dex */
    public static class a implements i81<ImageRequest, Uri> {
        @Override // defpackage.i81
        /* renamed from: a */
        public Uri apply(ImageRequest imageRequest) {
            if (imageRequest != null) {
                return imageRequest.u();
            }
            return null;
        }
    }

    public ImageRequest(ImageRequestBuilder imageRequestBuilder) {
        p93 o;
        this.b = imageRequestBuilder.d();
        Uri p = imageRequestBuilder.p();
        this.c = p;
        this.d = w(p);
        this.f = imageRequestBuilder.t();
        this.g = imageRequestBuilder.r();
        this.h = imageRequestBuilder.h();
        this.i = imageRequestBuilder.g();
        imageRequestBuilder.m();
        if (imageRequestBuilder.o() == null) {
            o = p93.a();
        } else {
            o = imageRequestBuilder.o();
        }
        this.k = o;
        this.l = imageRequestBuilder.c();
        this.m = imageRequestBuilder.l();
        this.n = imageRequestBuilder.i();
        this.o = imageRequestBuilder.e();
        this.p = imageRequestBuilder.q();
        this.q = imageRequestBuilder.s();
        this.r = imageRequestBuilder.L();
        this.s = imageRequestBuilder.j();
        this.t = imageRequestBuilder.k();
        this.u = imageRequestBuilder.n();
        this.v = imageRequestBuilder.f();
    }

    public static ImageRequest a(Uri uri) {
        if (uri == null) {
            return null;
        }
        return ImageRequestBuilder.u(uri).a();
    }

    public static ImageRequest b(String str) {
        if (str == null || str.length() == 0) {
            return null;
        }
        return a(Uri.parse(str));
    }

    public static int w(Uri uri) {
        if (uri == null) {
            return -1;
        }
        if (qf4.l(uri)) {
            return 0;
        }
        if (qf4.j(uri)) {
            return f72.c(f72.b(uri.getPath())) ? 2 : 3;
        } else if (qf4.i(uri)) {
            return 4;
        } else {
            if (qf4.f(uri)) {
                return 5;
            }
            if (qf4.k(uri)) {
                return 6;
            }
            if (qf4.e(uri)) {
                return 7;
            }
            return qf4.m(uri) ? 8 : -1;
        }
    }

    public ct c() {
        return this.l;
    }

    public CacheChoice d() {
        return this.b;
    }

    public int e() {
        return this.o;
    }

    public boolean equals(Object obj) {
        if (obj instanceof ImageRequest) {
            ImageRequest imageRequest = (ImageRequest) obj;
            if (w) {
                int i = this.a;
                int i2 = imageRequest.a;
                if (i != 0 && i2 != 0 && i != i2) {
                    return false;
                }
            }
            if (this.g == imageRequest.g && this.p == imageRequest.p && this.q == imageRequest.q && ol2.a(this.c, imageRequest.c) && ol2.a(this.b, imageRequest.b) && ol2.a(this.e, imageRequest.e) && ol2.a(this.l, imageRequest.l) && ol2.a(this.i, imageRequest.i) && ol2.a(this.j, imageRequest.j) && ol2.a(this.m, imageRequest.m) && ol2.a(this.n, imageRequest.n) && ol2.a(Integer.valueOf(this.o), Integer.valueOf(imageRequest.o)) && ol2.a(this.r, imageRequest.r) && ol2.a(this.u, imageRequest.u) && ol2.a(this.k, imageRequest.k) && this.h == imageRequest.h) {
                rt2 rt2Var = this.s;
                wt c = rt2Var != null ? rt2Var.c() : null;
                rt2 rt2Var2 = imageRequest.s;
                return ol2.a(c, rt2Var2 != null ? rt2Var2.c() : null) && this.v == imageRequest.v;
            }
            return false;
        }
        return false;
    }

    public int f() {
        return this.v;
    }

    public rn1 g() {
        return this.i;
    }

    public boolean h() {
        return this.h;
    }

    public int hashCode() {
        boolean z = x;
        int i = z ? this.a : 0;
        if (i == 0) {
            rt2 rt2Var = this.s;
            i = ol2.b(this.b, this.c, Boolean.valueOf(this.g), this.l, this.m, this.n, Integer.valueOf(this.o), Boolean.valueOf(this.p), Boolean.valueOf(this.q), this.i, this.r, this.j, this.k, rt2Var != null ? rt2Var.c() : null, this.u, Integer.valueOf(this.v), Boolean.valueOf(this.h));
            if (z) {
                this.a = i;
            }
        }
        return i;
    }

    public boolean i() {
        return this.g;
    }

    public RequestLevel j() {
        return this.n;
    }

    public rt2 k() {
        return this.s;
    }

    public int l() {
        return 2048;
    }

    public int m() {
        return 2048;
    }

    public Priority n() {
        return this.m;
    }

    public boolean o() {
        return this.f;
    }

    public h73 p() {
        return this.t;
    }

    public p73 q() {
        return this.j;
    }

    public Boolean r() {
        return this.u;
    }

    public p93 s() {
        return this.k;
    }

    public synchronized File t() {
        if (this.e == null) {
            this.e = new File(this.c.getPath());
        }
        return this.e;
    }

    public String toString() {
        return ol2.c(this).b("uri", this.c).b("cacheChoice", this.b).b("decodeOptions", this.i).b("postprocessor", this.s).b("priority", this.m).b("resizeOptions", this.j).b("rotationOptions", this.k).b("bytesRange", this.l).b("resizingAllowedOverride", this.u).c("progressiveRenderingEnabled", this.f).c("localThumbnailPreviewsEnabled", this.g).c("loadThumbnailOnly", this.h).b("lowestPermittedRequestLevel", this.n).a("cachesDisabled", this.o).c("isDiskCacheEnabled", this.p).c("isMemoryCacheEnabled", this.q).b("decodePrefetches", this.r).a("delayMs", this.v).toString();
    }

    public Uri u() {
        return this.c;
    }

    public int v() {
        return this.d;
    }

    public boolean x(int i) {
        return (i & e()) == 0;
    }

    public Boolean y() {
        return this.r;
    }
}
