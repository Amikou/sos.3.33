package com.facebook.imagepipeline.animated.base;

/* loaded from: classes.dex */
public class AnimatedDrawableFrameInfo {
    public final int a;
    public final int b;
    public final int c;
    public final int d;
    public final BlendOperation e;
    public final DisposalMethod f;

    /* loaded from: classes.dex */
    public enum BlendOperation {
        BLEND_WITH_PREVIOUS,
        NO_BLEND
    }

    /* loaded from: classes.dex */
    public enum DisposalMethod {
        DISPOSE_DO_NOT,
        DISPOSE_TO_BACKGROUND,
        DISPOSE_TO_PREVIOUS
    }

    public AnimatedDrawableFrameInfo(int i, int i2, int i3, int i4, int i5, BlendOperation blendOperation, DisposalMethod disposalMethod) {
        this.a = i2;
        this.b = i3;
        this.c = i4;
        this.d = i5;
        this.e = blendOperation;
        this.f = disposalMethod;
    }
}
