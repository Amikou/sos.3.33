package com.facebook.imagepipeline.animated.impl;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import com.facebook.imagepipeline.animated.base.AnimatedDrawableFrameInfo;
import com.github.mikephil.charting.utils.Utils;

/* loaded from: classes.dex */
public class AnimatedImageCompositor {
    public final kd a;
    public final b b;
    public final Paint c;

    /* loaded from: classes.dex */
    public enum FrameNeededResult {
        REQUIRED,
        NOT_REQUIRED,
        SKIP,
        ABORT
    }

    /* loaded from: classes.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[FrameNeededResult.values().length];
            a = iArr;
            try {
                iArr[FrameNeededResult.REQUIRED.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[FrameNeededResult.NOT_REQUIRED.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[FrameNeededResult.ABORT.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                a[FrameNeededResult.SKIP.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
        }
    }

    /* loaded from: classes.dex */
    public interface b {
        void a(int i, Bitmap bitmap);

        com.facebook.common.references.a<Bitmap> b(int i);
    }

    public AnimatedImageCompositor(kd kdVar, b bVar) {
        this.a = kdVar;
        this.b = bVar;
        Paint paint = new Paint();
        this.c = paint;
        paint.setColor(0);
        paint.setStyle(Paint.Style.FILL);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC));
    }

    public final void a(Canvas canvas, AnimatedDrawableFrameInfo animatedDrawableFrameInfo) {
        int i = animatedDrawableFrameInfo.a;
        int i2 = animatedDrawableFrameInfo.b;
        canvas.drawRect(i, i2, i + animatedDrawableFrameInfo.c, i2 + animatedDrawableFrameInfo.d, this.c);
    }

    public final FrameNeededResult b(int i) {
        AnimatedDrawableFrameInfo c = this.a.c(i);
        AnimatedDrawableFrameInfo.DisposalMethod disposalMethod = c.f;
        if (disposalMethod == AnimatedDrawableFrameInfo.DisposalMethod.DISPOSE_DO_NOT) {
            return FrameNeededResult.REQUIRED;
        }
        if (disposalMethod == AnimatedDrawableFrameInfo.DisposalMethod.DISPOSE_TO_BACKGROUND) {
            if (c(c)) {
                return FrameNeededResult.NOT_REQUIRED;
            }
            return FrameNeededResult.REQUIRED;
        } else if (disposalMethod == AnimatedDrawableFrameInfo.DisposalMethod.DISPOSE_TO_PREVIOUS) {
            return FrameNeededResult.SKIP;
        } else {
            return FrameNeededResult.ABORT;
        }
    }

    public final boolean c(AnimatedDrawableFrameInfo animatedDrawableFrameInfo) {
        return animatedDrawableFrameInfo.a == 0 && animatedDrawableFrameInfo.b == 0 && animatedDrawableFrameInfo.c == this.a.h() && animatedDrawableFrameInfo.d == this.a.g();
    }

    public final boolean d(int i) {
        if (i == 0) {
            return true;
        }
        AnimatedDrawableFrameInfo c = this.a.c(i);
        AnimatedDrawableFrameInfo c2 = this.a.c(i - 1);
        if (c.e == AnimatedDrawableFrameInfo.BlendOperation.NO_BLEND && c(c)) {
            return true;
        }
        return c2.f == AnimatedDrawableFrameInfo.DisposalMethod.DISPOSE_TO_BACKGROUND && c(c2);
    }

    public final void e(Bitmap bitmap) {
        pq c;
        yd i = this.a.i();
        if (i == null || (c = i.c()) == null) {
            return;
        }
        c.transform(bitmap);
    }

    public final int f(int i, Canvas canvas) {
        while (i >= 0) {
            int i2 = a.a[b(i).ordinal()];
            if (i2 == 1) {
                AnimatedDrawableFrameInfo c = this.a.c(i);
                com.facebook.common.references.a<Bitmap> b2 = this.b.b(i);
                if (b2 != null) {
                    try {
                        canvas.drawBitmap(b2.j(), Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, (Paint) null);
                        if (c.f == AnimatedDrawableFrameInfo.DisposalMethod.DISPOSE_TO_BACKGROUND) {
                            a(canvas, c);
                        }
                        return i + 1;
                    } finally {
                        b2.close();
                    }
                } else if (d(i)) {
                    return i;
                }
            } else if (i2 == 2) {
                return i + 1;
            } else {
                if (i2 == 3) {
                    return i;
                }
            }
            i--;
        }
        return 0;
    }

    public void g(int i, Bitmap bitmap) {
        Canvas canvas = new Canvas(bitmap);
        canvas.drawColor(0, PorterDuff.Mode.SRC);
        for (int f = !d(i) ? f(i - 1, canvas) : i; f < i; f++) {
            AnimatedDrawableFrameInfo c = this.a.c(f);
            AnimatedDrawableFrameInfo.DisposalMethod disposalMethod = c.f;
            if (disposalMethod != AnimatedDrawableFrameInfo.DisposalMethod.DISPOSE_TO_PREVIOUS) {
                if (c.e == AnimatedDrawableFrameInfo.BlendOperation.NO_BLEND) {
                    a(canvas, c);
                }
                this.a.d(f, canvas);
                this.b.a(f, bitmap);
                if (disposalMethod == AnimatedDrawableFrameInfo.DisposalMethod.DISPOSE_TO_BACKGROUND) {
                    a(canvas, c);
                }
            }
        }
        AnimatedDrawableFrameInfo c2 = this.a.c(i);
        if (c2.e == AnimatedDrawableFrameInfo.BlendOperation.NO_BLEND) {
            a(canvas, c2);
        }
        this.a.d(i, canvas);
        e(bitmap);
    }
}
