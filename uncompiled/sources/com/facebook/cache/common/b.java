package com.facebook.cache.common;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/* compiled from: CacheKeyUtil.java */
/* loaded from: classes.dex */
public final class b {
    public static String a(wt wtVar) {
        try {
            if (wtVar instanceof ma2) {
                return c(((ma2) wtVar).c().get(0));
            }
            return c(wtVar);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public static List<String> b(wt wtVar) {
        ArrayList arrayList;
        try {
            if (wtVar instanceof ma2) {
                List<wt> c = ((ma2) wtVar).c();
                arrayList = new ArrayList(c.size());
                for (int i = 0; i < c.size(); i++) {
                    arrayList.add(c(c.get(i)));
                }
            } else {
                arrayList = new ArrayList(1);
                arrayList.add(wtVar.a() ? wtVar.b() : c(wtVar));
            }
            return arrayList;
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public static String c(wt wtVar) throws UnsupportedEncodingException {
        return yh3.a(wtVar.b().getBytes("UTF-8"));
    }
}
