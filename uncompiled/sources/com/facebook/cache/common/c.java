package com.facebook.cache.common;

import com.facebook.cache.common.CacheErrorLogger;

/* compiled from: NoOpCacheErrorLogger.java */
/* loaded from: classes.dex */
public class c implements CacheErrorLogger {
    public static c a;

    public static synchronized c b() {
        c cVar;
        synchronized (c.class) {
            if (a == null) {
                a = new c();
            }
            cVar = a;
        }
        return cVar;
    }

    @Override // com.facebook.cache.common.CacheErrorLogger
    public void a(CacheErrorLogger.CacheErrorCategory cacheErrorCategory, Class<?> cls, String str, Throwable th) {
    }
}
