package com.facebook.cache.disk;

import com.facebook.cache.common.CacheErrorLogger;
import com.facebook.cache.disk.b;
import com.facebook.common.file.FileUtils;
import java.io.File;
import java.io.IOException;
import java.util.Collection;

/* compiled from: DynamicDefaultDiskStorage.java */
/* loaded from: classes.dex */
public class d implements b {
    public static final Class<?> f = d.class;
    public final int a;
    public final fw3<File> b;
    public final String c;
    public final CacheErrorLogger d;
    public volatile a e = new a(null, null);

    /* compiled from: DynamicDefaultDiskStorage.java */
    /* loaded from: classes.dex */
    public static class a {
        public final b a;
        public final File b;

        public a(File file, b bVar) {
            this.a = bVar;
            this.b = file;
        }
    }

    public d(int i, fw3<File> fw3Var, String str, CacheErrorLogger cacheErrorLogger) {
        this.a = i;
        this.d = cacheErrorLogger;
        this.b = fw3Var;
        this.c = str;
    }

    @Override // com.facebook.cache.disk.b
    public void a() {
        try {
            j().a();
        } catch (IOException e) {
            v11.e(f, "purgeUnexpectedResources", e);
        }
    }

    @Override // com.facebook.cache.disk.b
    public b.InterfaceC0082b b(String str, Object obj) throws IOException {
        return j().b(str, obj);
    }

    @Override // com.facebook.cache.disk.b
    public boolean c(String str, Object obj) throws IOException {
        return j().c(str, obj);
    }

    @Override // com.facebook.cache.disk.b
    public kp d(String str, Object obj) throws IOException {
        return j().d(str, obj);
    }

    @Override // com.facebook.cache.disk.b
    public Collection<b.a> e() throws IOException {
        return j().e();
    }

    @Override // com.facebook.cache.disk.b
    public long f(b.a aVar) throws IOException {
        return j().f(aVar);
    }

    public void g(File file) throws IOException {
        try {
            FileUtils.a(file);
            v11.a(f, "Created cache directory %s", file.getAbsolutePath());
        } catch (FileUtils.CreateDirectoryException e) {
            this.d.a(CacheErrorLogger.CacheErrorCategory.WRITE_CREATE_DIR, f, "createRootDirectoryIfNecessary", e);
            throw e;
        }
    }

    public final void h() throws IOException {
        File file = new File(this.b.get(), this.c);
        g(file);
        this.e = new a(file, new DefaultDiskStorage(file, this.a, this.d));
    }

    public void i() {
        if (this.e.a == null || this.e.b == null) {
            return;
        }
        u31.b(this.e.b);
    }

    @Override // com.facebook.cache.disk.b
    public boolean isExternal() {
        try {
            return j().isExternal();
        } catch (IOException unused) {
            return false;
        }
    }

    public synchronized b j() throws IOException {
        if (k()) {
            i();
            h();
        }
        return (b) xt2.g(this.e.a);
    }

    public final boolean k() {
        File file;
        a aVar = this.e;
        return aVar.a == null || (file = aVar.b) == null || !file.exists();
    }

    @Override // com.facebook.cache.disk.b
    public long remove(String str) throws IOException {
        return j().remove(str);
    }
}
