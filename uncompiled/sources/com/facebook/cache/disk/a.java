package com.facebook.cache.disk;

import com.facebook.cache.disk.b;

/* compiled from: DefaultEntryEvictionComparatorSupplier.java */
/* loaded from: classes.dex */
public class a implements aw0 {

    /* compiled from: DefaultEntryEvictionComparatorSupplier.java */
    /* renamed from: com.facebook.cache.disk.a$a  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public class C0081a implements e {
        public C0081a(a aVar) {
        }

        @Override // java.util.Comparator
        /* renamed from: a */
        public int compare(b.a aVar, b.a aVar2) {
            long timestamp = aVar.getTimestamp();
            long timestamp2 = aVar2.getTimestamp();
            if (timestamp < timestamp2) {
                return -1;
            }
            return timestamp2 == timestamp ? 0 : 1;
        }
    }

    @Override // defpackage.aw0
    public e get() {
        return new C0081a(this);
    }
}
