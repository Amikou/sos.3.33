package com.facebook.cache.disk;

import com.facebook.cache.common.CacheErrorLogger;
import com.facebook.cache.common.CacheEventListener;
import com.facebook.cache.disk.b;
import com.facebook.common.statfs.StatFsHelper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

/* compiled from: DiskStorageCache.java */
/* loaded from: classes.dex */
public class c implements f, kp0 {
    public static final Class<?> q = c.class;
    public static final long r = TimeUnit.HOURS.toMillis(2);
    public static final long s = TimeUnit.MINUTES.toMillis(30);
    public final long a;
    public final long b;
    public final CountDownLatch c;
    public long d;
    public final CacheEventListener e;
    public final Set<String> f;
    public long g;
    public final StatFsHelper h;
    public final com.facebook.cache.disk.b i;
    public final aw0 j;
    public final CacheErrorLogger k;
    public final boolean l;
    public final b m;
    public final pz n;
    public final Object o = new Object();
    public boolean p;

    /* compiled from: DiskStorageCache.java */
    /* loaded from: classes.dex */
    public class a implements Runnable {
        public a() {
        }

        @Override // java.lang.Runnable
        public void run() {
            synchronized (c.this.o) {
                c.this.m();
            }
            c.this.p = true;
            c.this.c.countDown();
        }
    }

    /* compiled from: DiskStorageCache.java */
    /* loaded from: classes.dex */
    public static class b {
        public boolean a = false;
        public long b = -1;
        public long c = -1;

        public synchronized long a() {
            return this.c;
        }

        public synchronized long b() {
            return this.b;
        }

        public synchronized void c(long j, long j2) {
            if (this.a) {
                this.b += j;
                this.c += j2;
            }
        }

        public synchronized boolean d() {
            return this.a;
        }

        public synchronized void e() {
            this.a = false;
            this.c = -1L;
            this.b = -1L;
        }

        public synchronized void f(long j, long j2) {
            this.c = j2;
            this.b = j;
            this.a = true;
        }
    }

    /* compiled from: DiskStorageCache.java */
    /* renamed from: com.facebook.cache.disk.c$c  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static class C0083c {
        public final long a;
        public final long b;
        public final long c;

        public C0083c(long j, long j2, long j3) {
            this.a = j;
            this.b = j2;
            this.c = j3;
        }
    }

    public c(com.facebook.cache.disk.b bVar, aw0 aw0Var, C0083c c0083c, CacheEventListener cacheEventListener, CacheErrorLogger cacheErrorLogger, lp0 lp0Var, Executor executor, boolean z) {
        this.a = c0083c.b;
        long j = c0083c.c;
        this.b = j;
        this.d = j;
        this.h = StatFsHelper.d();
        this.i = bVar;
        this.j = aw0Var;
        this.g = -1L;
        this.e = cacheEventListener;
        this.k = cacheErrorLogger;
        this.m = new b();
        this.n = o24.a();
        this.l = z;
        this.f = new HashSet();
        if (lp0Var != null) {
            lp0Var.a(this);
        }
        if (z) {
            this.c = new CountDownLatch(1);
            executor.execute(new a());
            return;
        }
        this.c = new CountDownLatch(0);
    }

    @Override // com.facebook.cache.disk.f
    public boolean a(wt wtVar) {
        String str;
        IOException e;
        String str2 = null;
        try {
            try {
                synchronized (this.o) {
                    try {
                        List<String> b2 = com.facebook.cache.common.b.b(wtVar);
                        int i = 0;
                        while (i < b2.size()) {
                            String str3 = b2.get(i);
                            if (this.i.c(str3, wtVar)) {
                                this.f.add(str3);
                                return true;
                            }
                            i++;
                            str2 = str3;
                        }
                        return false;
                    } catch (Throwable th) {
                        str = str2;
                        th = th;
                        try {
                            throw th;
                        } catch (IOException e2) {
                            e = e2;
                            um3 h = um3.a().d(wtVar).j(str).h(e);
                            this.e.f(h);
                            h.b();
                            return false;
                        }
                    }
                }
            } catch (Throwable th2) {
                th = th2;
            }
        } catch (IOException e3) {
            str = null;
            e = e3;
        }
    }

    @Override // com.facebook.cache.disk.f
    public kp b(wt wtVar, com.facebook.cache.common.d dVar) throws IOException {
        String a2;
        um3 d = um3.a().d(wtVar);
        this.e.g(d);
        synchronized (this.o) {
            a2 = com.facebook.cache.common.b.a(wtVar);
        }
        d.j(a2);
        try {
            try {
                b.InterfaceC0082b o = o(a2, wtVar);
                try {
                    o.a(dVar, wtVar);
                    kp i = i(o, wtVar, a2);
                    d.i(i.size()).f(this.m.b());
                    this.e.e(d);
                    return i;
                } finally {
                    if (!o.k()) {
                        v11.d(q, "Failed to delete temp file");
                    }
                }
            } catch (IOException e) {
                d.h(e);
                this.e.c(d);
                v11.e(q, "Failed inserting a file into the cache", e);
                throw e;
            }
        } finally {
            d.b();
        }
    }

    @Override // com.facebook.cache.disk.f
    public kp c(wt wtVar) {
        kp kpVar;
        um3 d = um3.a().d(wtVar);
        try {
            synchronized (this.o) {
                List<String> b2 = com.facebook.cache.common.b.b(wtVar);
                String str = null;
                kpVar = null;
                for (int i = 0; i < b2.size(); i++) {
                    str = b2.get(i);
                    d.j(str);
                    kpVar = this.i.d(str, wtVar);
                    if (kpVar != null) {
                        break;
                    }
                }
                if (kpVar == null) {
                    this.e.a(d);
                    this.f.remove(str);
                } else {
                    xt2.g(str);
                    this.e.d(d);
                    this.f.add(str);
                }
            }
            return kpVar;
        } catch (IOException e) {
            this.k.a(CacheErrorLogger.CacheErrorCategory.GENERIC_IO, q, "getResource", e);
            d.h(e);
            this.e.f(d);
            return null;
        } finally {
            d.b();
        }
    }

    @Override // com.facebook.cache.disk.f
    public void d(wt wtVar) {
        synchronized (this.o) {
            try {
                List<String> b2 = com.facebook.cache.common.b.b(wtVar);
                for (int i = 0; i < b2.size(); i++) {
                    String str = b2.get(i);
                    this.i.remove(str);
                    this.f.remove(str);
                }
            } catch (IOException e) {
                CacheErrorLogger cacheErrorLogger = this.k;
                CacheErrorLogger.CacheErrorCategory cacheErrorCategory = CacheErrorLogger.CacheErrorCategory.DELETE_FILE;
                Class<?> cls = q;
                cacheErrorLogger.a(cacheErrorCategory, cls, "delete: " + e.getMessage(), e);
            }
        }
    }

    public final kp i(b.InterfaceC0082b interfaceC0082b, wt wtVar, String str) throws IOException {
        kp b2;
        synchronized (this.o) {
            b2 = interfaceC0082b.b(wtVar);
            this.f.add(str);
            this.m.c(b2.size(), 1L);
        }
        return b2;
    }

    public final void j(long j, CacheEventListener.EvictionReason evictionReason) throws IOException {
        try {
            Collection<b.a> k = k(this.i.e());
            long b2 = this.m.b();
            long j2 = b2 - j;
            int i = 0;
            long j3 = 0;
            for (b.a aVar : k) {
                if (j3 > j2) {
                    break;
                }
                long f = this.i.f(aVar);
                this.f.remove(aVar.getId());
                if (f > 0) {
                    i++;
                    j3 += f;
                    um3 e = um3.a().j(aVar.getId()).g(evictionReason).i(f).f(b2 - j3).e(j);
                    this.e.b(e);
                    e.b();
                }
            }
            this.m.c(-j3, -i);
            this.i.a();
        } catch (IOException e2) {
            CacheErrorLogger cacheErrorLogger = this.k;
            CacheErrorLogger.CacheErrorCategory cacheErrorCategory = CacheErrorLogger.CacheErrorCategory.EVICTION;
            Class<?> cls = q;
            cacheErrorLogger.a(cacheErrorCategory, cls, "evictAboveSize: " + e2.getMessage(), e2);
            throw e2;
        }
    }

    public final Collection<b.a> k(Collection<b.a> collection) {
        long now = this.n.now() + r;
        ArrayList arrayList = new ArrayList(collection.size());
        ArrayList arrayList2 = new ArrayList(collection.size());
        for (b.a aVar : collection) {
            if (aVar.getTimestamp() > now) {
                arrayList.add(aVar);
            } else {
                arrayList2.add(aVar);
            }
        }
        Collections.sort(arrayList2, this.j.get());
        arrayList.addAll(arrayList2);
        return arrayList;
    }

    public final void l() throws IOException {
        synchronized (this.o) {
            boolean m = m();
            p();
            long b2 = this.m.b();
            if (b2 > this.d && !m) {
                this.m.e();
                m();
            }
            long j = this.d;
            if (b2 > j) {
                j((j * 9) / 10, CacheEventListener.EvictionReason.CACHE_FULL);
            }
        }
    }

    public final boolean m() {
        long now = this.n.now();
        if (this.m.d()) {
            long j = this.g;
            if (j != -1 && now - j <= s) {
                return false;
            }
        }
        return n();
    }

    public final boolean n() {
        Set<String> hashSet;
        long j;
        long now = this.n.now();
        long j2 = r + now;
        if (this.l && this.f.isEmpty()) {
            hashSet = this.f;
        } else {
            hashSet = this.l ? new HashSet<>() : null;
        }
        try {
            long j3 = 0;
            long j4 = -1;
            int i = 0;
            boolean z = false;
            int i2 = 0;
            int i3 = 0;
            for (b.a aVar : this.i.e()) {
                i2++;
                j3 += aVar.a();
                if (aVar.getTimestamp() > j2) {
                    i3++;
                    i = (int) (i + aVar.a());
                    j = j2;
                    j4 = Math.max(aVar.getTimestamp() - now, j4);
                    z = true;
                } else {
                    j = j2;
                    if (this.l) {
                        xt2.g(hashSet);
                        hashSet.add(aVar.getId());
                    }
                }
                j2 = j;
            }
            if (z) {
                this.k.a(CacheErrorLogger.CacheErrorCategory.READ_INVALID_ENTRY, q, "Future timestamp found in " + i3 + " files , with a total size of " + i + " bytes, and a maximum time delta of " + j4 + "ms", null);
            }
            long j5 = i2;
            if (this.m.a() != j5 || this.m.b() != j3) {
                if (this.l && this.f != hashSet) {
                    xt2.g(hashSet);
                    this.f.clear();
                    this.f.addAll(hashSet);
                }
                this.m.f(j3, j5);
            }
            this.g = now;
            return true;
        } catch (IOException e) {
            this.k.a(CacheErrorLogger.CacheErrorCategory.GENERIC_IO, q, "calcFileCacheSize: " + e.getMessage(), e);
            return false;
        }
    }

    public final b.InterfaceC0082b o(String str, wt wtVar) throws IOException {
        l();
        return this.i.b(str, wtVar);
    }

    public final void p() {
        if (this.h.f(this.i.isExternal() ? StatFsHelper.StorageType.EXTERNAL : StatFsHelper.StorageType.INTERNAL, this.b - this.m.b())) {
            this.d = this.a;
        } else {
            this.d = this.b;
        }
    }
}
