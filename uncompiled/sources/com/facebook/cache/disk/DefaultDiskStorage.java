package com.facebook.cache.disk;

import android.os.Environment;
import com.facebook.cache.common.CacheErrorLogger;
import com.facebook.cache.disk.b;
import com.facebook.common.file.FileUtils;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

/* loaded from: classes.dex */
public class DefaultDiskStorage implements com.facebook.cache.disk.b {
    public static final Class<?> f = DefaultDiskStorage.class;
    public static final long g = TimeUnit.MINUTES.toMillis(30);
    public final File a;
    public final boolean b;
    public final File c;
    public final CacheErrorLogger d;
    public final pz e;

    /* loaded from: classes.dex */
    public static class IncompleteFileException extends IOException {
        public IncompleteFileException(long j, long j2) {
            super("File was not written completely. Expected: " + j + ", found: " + j2);
        }
    }

    /* loaded from: classes.dex */
    public class b implements v31 {
        public final List<b.a> a;

        public b() {
            this.a = new ArrayList();
        }

        @Override // defpackage.v31
        public void a(File file) {
            d s = DefaultDiskStorage.this.s(file);
            if (s == null || s.a != ".cnt") {
                return;
            }
            this.a.add(new c(s.b, file));
        }

        @Override // defpackage.v31
        public void b(File file) {
        }

        @Override // defpackage.v31
        public void c(File file) {
        }

        public List<b.a> d() {
            return Collections.unmodifiableList(this.a);
        }
    }

    /* loaded from: classes.dex */
    public static class c implements b.a {
        public final String a;
        public final k31 b;
        public long c;
        public long d;

        @Override // com.facebook.cache.disk.b.a
        public long a() {
            if (this.c < 0) {
                this.c = this.b.size();
            }
            return this.c;
        }

        public k31 b() {
            return this.b;
        }

        @Override // com.facebook.cache.disk.b.a
        public String getId() {
            return this.a;
        }

        @Override // com.facebook.cache.disk.b.a
        public long getTimestamp() {
            if (this.d < 0) {
                this.d = this.b.d().lastModified();
            }
            return this.d;
        }

        public c(String str, File file) {
            xt2.g(file);
            this.a = (String) xt2.g(str);
            this.b = k31.b(file);
            this.c = -1L;
            this.d = -1L;
        }
    }

    /* loaded from: classes.dex */
    public static class d {
        public final String a;
        public final String b;

        public static d b(File file) {
            String q;
            String name = file.getName();
            int lastIndexOf = name.lastIndexOf(46);
            if (lastIndexOf > 0 && (q = DefaultDiskStorage.q(name.substring(lastIndexOf))) != null) {
                String substring = name.substring(0, lastIndexOf);
                if (q.equals(".tmp")) {
                    int lastIndexOf2 = substring.lastIndexOf(46);
                    if (lastIndexOf2 <= 0) {
                        return null;
                    }
                    substring = substring.substring(0, lastIndexOf2);
                }
                return new d(q, substring);
            }
            return null;
        }

        public File a(File file) throws IOException {
            return File.createTempFile(this.b + ".", ".tmp", file);
        }

        public String c(String str) {
            return str + File.separator + this.b + this.a;
        }

        public String toString() {
            return this.a + "(" + this.b + ")";
        }

        public d(String str, String str2) {
            this.a = str;
            this.b = str2;
        }
    }

    /* loaded from: classes.dex */
    public class e implements b.InterfaceC0082b {
        public final String a;
        public final File b;

        public e(String str, File file) {
            this.a = str;
            this.b = file;
        }

        @Override // com.facebook.cache.disk.b.InterfaceC0082b
        public void a(com.facebook.cache.common.d dVar, Object obj) throws IOException {
            try {
                FileOutputStream fileOutputStream = new FileOutputStream(this.b);
                try {
                    com.facebook.common.internal.c cVar = new com.facebook.common.internal.c(fileOutputStream);
                    dVar.a(cVar);
                    cVar.flush();
                    long a = cVar.a();
                    fileOutputStream.close();
                    if (this.b.length() != a) {
                        throw new IncompleteFileException(a, this.b.length());
                    }
                } catch (Throwable th) {
                    fileOutputStream.close();
                    throw th;
                }
            } catch (FileNotFoundException e) {
                DefaultDiskStorage.this.d.a(CacheErrorLogger.CacheErrorCategory.WRITE_UPDATE_FILE_NOT_FOUND, DefaultDiskStorage.f, "updateResource", e);
                throw e;
            }
        }

        @Override // com.facebook.cache.disk.b.InterfaceC0082b
        public kp b(Object obj) throws IOException {
            return c(obj, DefaultDiskStorage.this.e.now());
        }

        public kp c(Object obj, long j) throws IOException {
            CacheErrorLogger.CacheErrorCategory cacheErrorCategory;
            File o = DefaultDiskStorage.this.o(this.a);
            try {
                FileUtils.b(this.b, o);
                if (o.exists()) {
                    o.setLastModified(j);
                }
                return k31.b(o);
            } catch (FileUtils.RenameException e) {
                Throwable cause = e.getCause();
                if (cause != null) {
                    if (!(cause instanceof FileUtils.ParentDirNotFoundException)) {
                        if (cause instanceof FileNotFoundException) {
                            cacheErrorCategory = CacheErrorLogger.CacheErrorCategory.WRITE_RENAME_FILE_TEMPFILE_NOT_FOUND;
                        } else {
                            cacheErrorCategory = CacheErrorLogger.CacheErrorCategory.WRITE_RENAME_FILE_OTHER;
                        }
                    } else {
                        cacheErrorCategory = CacheErrorLogger.CacheErrorCategory.WRITE_RENAME_FILE_TEMPFILE_PARENT_NOT_FOUND;
                    }
                } else {
                    cacheErrorCategory = CacheErrorLogger.CacheErrorCategory.WRITE_RENAME_FILE_OTHER;
                }
                DefaultDiskStorage.this.d.a(cacheErrorCategory, DefaultDiskStorage.f, "commit", e);
                throw e;
            }
        }

        @Override // com.facebook.cache.disk.b.InterfaceC0082b
        public boolean k() {
            return !this.b.exists() || this.b.delete();
        }
    }

    /* loaded from: classes.dex */
    public class f implements v31 {
        public boolean a;

        public f() {
        }

        @Override // defpackage.v31
        public void a(File file) {
            if (this.a && d(file)) {
                return;
            }
            file.delete();
        }

        @Override // defpackage.v31
        public void b(File file) {
            if (this.a || !file.equals(DefaultDiskStorage.this.c)) {
                return;
            }
            this.a = true;
        }

        @Override // defpackage.v31
        public void c(File file) {
            if (!DefaultDiskStorage.this.a.equals(file) && !this.a) {
                file.delete();
            }
            if (this.a && file.equals(DefaultDiskStorage.this.c)) {
                this.a = false;
            }
        }

        public final boolean d(File file) {
            d s = DefaultDiskStorage.this.s(file);
            if (s == null) {
                return false;
            }
            String str = s.a;
            if (str == ".tmp") {
                return e(file);
            }
            xt2.i(str == ".cnt");
            return true;
        }

        public final boolean e(File file) {
            return file.lastModified() > DefaultDiskStorage.this.e.now() - DefaultDiskStorage.g;
        }
    }

    public DefaultDiskStorage(File file, int i, CacheErrorLogger cacheErrorLogger) {
        xt2.g(file);
        this.a = file;
        this.b = w(file, cacheErrorLogger);
        this.c = new File(file, v(i));
        this.d = cacheErrorLogger;
        z();
        this.e = o24.a();
    }

    public static String q(String str) {
        if (".cnt".equals(str)) {
            return ".cnt";
        }
        if (".tmp".equals(str)) {
            return ".tmp";
        }
        return null;
    }

    public static String v(int i) {
        return String.format(null, "%s.ols%d.%d", "v2", 100, Integer.valueOf(i));
    }

    public static boolean w(File file, CacheErrorLogger cacheErrorLogger) {
        try {
            File externalStorageDirectory = Environment.getExternalStorageDirectory();
            if (externalStorageDirectory == null) {
                return false;
            }
            try {
                try {
                    return file.getCanonicalPath().contains(externalStorageDirectory.toString());
                } catch (IOException e2) {
                    e = e2;
                    CacheErrorLogger.CacheErrorCategory cacheErrorCategory = CacheErrorLogger.CacheErrorCategory.OTHER;
                    Class<?> cls = f;
                    cacheErrorLogger.a(cacheErrorCategory, cls, "failed to read folder to check if external: " + ((String) null), e);
                    return false;
                }
            } catch (IOException e3) {
                e = e3;
            }
        } catch (Exception e4) {
            cacheErrorLogger.a(CacheErrorLogger.CacheErrorCategory.OTHER, f, "failed to get the external storage directory!", e4);
            return false;
        }
    }

    @Override // com.facebook.cache.disk.b
    public void a() {
        u31.c(this.a, new f());
    }

    @Override // com.facebook.cache.disk.b
    public b.InterfaceC0082b b(String str, Object obj) throws IOException {
        d dVar = new d(".tmp", str);
        File t = t(dVar.b);
        if (!t.exists()) {
            x(t, "insert");
        }
        try {
            return new e(str, dVar.a(t));
        } catch (IOException e2) {
            this.d.a(CacheErrorLogger.CacheErrorCategory.WRITE_CREATE_TEMPFILE, f, "insert", e2);
            throw e2;
        }
    }

    @Override // com.facebook.cache.disk.b
    public boolean c(String str, Object obj) {
        return y(str, true);
    }

    @Override // com.facebook.cache.disk.b
    public kp d(String str, Object obj) {
        File o = o(str);
        if (o.exists()) {
            o.setLastModified(this.e.now());
            return k31.c(o);
        }
        return null;
    }

    @Override // com.facebook.cache.disk.b
    public long f(b.a aVar) {
        return n(((c) aVar).b().d());
    }

    @Override // com.facebook.cache.disk.b
    public boolean isExternal() {
        return this.b;
    }

    public final long n(File file) {
        if (file.exists()) {
            long length = file.length();
            if (file.delete()) {
                return length;
            }
            return -1L;
        }
        return 0L;
    }

    public File o(String str) {
        return new File(r(str));
    }

    @Override // com.facebook.cache.disk.b
    /* renamed from: p */
    public List<b.a> e() throws IOException {
        b bVar = new b();
        u31.c(this.c, bVar);
        return bVar.d();
    }

    public final String r(String str) {
        d dVar = new d(".cnt", str);
        return dVar.c(u(dVar.b));
    }

    @Override // com.facebook.cache.disk.b
    public long remove(String str) {
        return n(o(str));
    }

    public final d s(File file) {
        d b2 = d.b(file);
        if (b2 != null && t(b2.b).equals(file.getParentFile())) {
            return b2;
        }
        return null;
    }

    public final File t(String str) {
        return new File(u(str));
    }

    public final String u(String str) {
        String valueOf = String.valueOf(Math.abs(str.hashCode() % 100));
        return this.c + File.separator + valueOf;
    }

    public final void x(File file, String str) throws IOException {
        try {
            FileUtils.a(file);
        } catch (FileUtils.CreateDirectoryException e2) {
            this.d.a(CacheErrorLogger.CacheErrorCategory.WRITE_CREATE_DIR, f, str, e2);
            throw e2;
        }
    }

    public final boolean y(String str, boolean z) {
        File o = o(str);
        boolean exists = o.exists();
        if (z && exists) {
            o.setLastModified(this.e.now());
        }
        return exists;
    }

    public final void z() {
        boolean z = true;
        if (this.a.exists()) {
            if (this.c.exists()) {
                z = false;
            } else {
                u31.b(this.a);
            }
        }
        if (z) {
            try {
                FileUtils.a(this.c);
            } catch (FileUtils.CreateDirectoryException unused) {
                CacheErrorLogger cacheErrorLogger = this.d;
                CacheErrorLogger.CacheErrorCategory cacheErrorCategory = CacheErrorLogger.CacheErrorCategory.WRITE_CREATE_DIR;
                Class<?> cls = f;
                cacheErrorLogger.a(cacheErrorCategory, cls, "version directory could not be created: " + this.c, null);
            }
        }
    }
}
