package com.facebook.cache.disk;

import java.io.IOException;
import java.util.Collection;

/* compiled from: DiskStorage.java */
/* loaded from: classes.dex */
public interface b {

    /* compiled from: DiskStorage.java */
    /* loaded from: classes.dex */
    public interface a {
        long a();

        String getId();

        long getTimestamp();
    }

    /* compiled from: DiskStorage.java */
    /* renamed from: com.facebook.cache.disk.b$b  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public interface InterfaceC0082b {
        void a(com.facebook.cache.common.d dVar, Object obj) throws IOException;

        kp b(Object obj) throws IOException;

        boolean k();
    }

    void a();

    InterfaceC0082b b(String str, Object obj) throws IOException;

    boolean c(String str, Object obj) throws IOException;

    kp d(String str, Object obj) throws IOException;

    Collection<a> e() throws IOException;

    long f(a aVar) throws IOException;

    boolean isExternal();

    long remove(String str) throws IOException;
}
