package com.facebook.soloader;

import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileLock;

/* compiled from: FileLocker.java */
/* loaded from: classes.dex */
public final class f implements Closeable {
    public final FileOutputStream a;
    public final FileLock f0;

    public f(File file) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        this.a = fileOutputStream;
        try {
            FileLock lock = fileOutputStream.getChannel().lock();
            if (lock == null) {
                fileOutputStream.close();
            }
            this.f0 = lock;
        } catch (Throwable th) {
            this.a.close();
            throw th;
        }
    }

    public static f a(File file) throws IOException {
        return new f(file);
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        try {
            FileLock fileLock = this.f0;
            if (fileLock != null) {
                fileLock.release();
            }
        } finally {
            this.a.close();
        }
    }
}
