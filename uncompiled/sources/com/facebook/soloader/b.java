package com.facebook.soloader;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.StrictMode;
import java.io.File;
import java.io.IOException;

/* compiled from: ApplicationSoSource.java */
/* loaded from: classes.dex */
public class b extends g {
    public Context a;
    public int b;
    public c c;

    public b(Context context, int i) {
        Context applicationContext = context.getApplicationContext();
        this.a = applicationContext;
        if (applicationContext == null) {
            this.a = context;
        }
        this.b = i;
        this.c = new c(new File(this.a.getApplicationInfo().nativeLibraryDir), i);
    }

    public static File d(Context context) {
        return new File(context.getApplicationInfo().nativeLibraryDir);
    }

    @Override // com.facebook.soloader.g
    public int a(String str, int i, StrictMode.ThreadPolicy threadPolicy) throws IOException {
        return this.c.a(str, i, threadPolicy);
    }

    @Override // com.facebook.soloader.g
    public void b(int i) throws IOException {
        this.c.b(i);
    }

    public boolean c() throws IOException {
        File file = this.c.a;
        Context e = e();
        File d = d(e);
        if (file.equals(d)) {
            return false;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Native library directory updated from ");
        sb.append(file);
        sb.append(" to ");
        sb.append(d);
        int i = this.b | 1;
        this.b = i;
        c cVar = new c(d, i);
        this.c = cVar;
        cVar.b(this.b);
        this.a = e;
        return true;
    }

    public Context e() {
        try {
            Context context = this.a;
            return context.createPackageContext(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    @Override // com.facebook.soloader.g
    public String toString() {
        return this.c.toString();
    }
}
