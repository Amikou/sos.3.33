package com.facebook.soloader;

import android.content.Context;
import com.facebook.soloader.h;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/* compiled from: ExtractFromZipSoSource.java */
/* loaded from: classes.dex */
public class e extends h {
    public final File f;
    public final String g;

    /* compiled from: ExtractFromZipSoSource.java */
    /* loaded from: classes.dex */
    public static final class b extends h.b implements Comparable {
        public final ZipEntry g0;
        public final int h0;

        public b(String str, ZipEntry zipEntry, int i) {
            super(str, a(zipEntry));
            this.g0 = zipEntry;
            this.h0 = i;
        }

        public static String a(ZipEntry zipEntry) {
            return String.format("pseudo-zip-hash-1-%s-%s-%s-%s", zipEntry.getName(), Long.valueOf(zipEntry.getSize()), Long.valueOf(zipEntry.getCompressedSize()), Long.valueOf(zipEntry.getCrc()));
        }

        @Override // java.lang.Comparable
        public int compareTo(Object obj) {
            return this.a.compareTo(((b) obj).a);
        }
    }

    /* compiled from: ExtractFromZipSoSource.java */
    /* loaded from: classes.dex */
    public class c extends h.f {
        public b[] a;
        public final ZipFile f0;
        public final h g0;

        /* compiled from: ExtractFromZipSoSource.java */
        /* loaded from: classes.dex */
        public final class a extends h.e {
            public int a;

            public a() {
            }

            @Override // com.facebook.soloader.h.e
            public boolean a() {
                c.this.e();
                return this.a < c.this.a.length;
            }

            @Override // com.facebook.soloader.h.e
            public h.d b() throws IOException {
                c.this.e();
                b[] bVarArr = c.this.a;
                int i = this.a;
                this.a = i + 1;
                b bVar = bVarArr[i];
                InputStream inputStream = c.this.f0.getInputStream(bVar.g0);
                try {
                    return new h.d(bVar, inputStream);
                } catch (Throwable th) {
                    if (inputStream != null) {
                        inputStream.close();
                    }
                    throw th;
                }
            }
        }

        public c(h hVar) throws IOException {
            this.f0 = new ZipFile(e.this.f);
            this.g0 = hVar;
        }

        @Override // com.facebook.soloader.h.f
        public final h.c a() throws IOException {
            return new h.c(e());
        }

        @Override // com.facebook.soloader.h.f
        public final h.e b() throws IOException {
            return new a();
        }

        @Override // com.facebook.soloader.h.f, java.io.Closeable, java.lang.AutoCloseable
        public void close() throws IOException {
            this.f0.close();
        }

        public final b[] e() {
            if (this.a == null) {
                LinkedHashSet linkedHashSet = new LinkedHashSet();
                HashMap hashMap = new HashMap();
                Pattern compile = Pattern.compile(e.this.g);
                String[] h = SysUtil.h();
                Enumeration<? extends ZipEntry> entries = this.f0.entries();
                while (entries.hasMoreElements()) {
                    ZipEntry nextElement = entries.nextElement();
                    Matcher matcher = compile.matcher(nextElement.getName());
                    if (matcher.matches()) {
                        String group = matcher.group(1);
                        String group2 = matcher.group(2);
                        int e = SysUtil.e(h, group);
                        if (e >= 0) {
                            linkedHashSet.add(group);
                            b bVar = (b) hashMap.get(group2);
                            if (bVar == null || e < bVar.h0) {
                                hashMap.put(group2, new b(group2, nextElement, e));
                            }
                        }
                    }
                }
                this.g0.q((String[]) linkedHashSet.toArray(new String[linkedHashSet.size()]));
                b[] bVarArr = (b[]) hashMap.values().toArray(new b[hashMap.size()]);
                Arrays.sort(bVarArr);
                int i = 0;
                for (int i2 = 0; i2 < bVarArr.length; i2++) {
                    b bVar2 = bVarArr[i2];
                    if (f(bVar2.g0, bVar2.a)) {
                        i++;
                    } else {
                        bVarArr[i2] = null;
                    }
                }
                b[] bVarArr2 = new b[i];
                int i3 = 0;
                for (b bVar3 : bVarArr) {
                    if (bVar3 != null) {
                        bVarArr2[i3] = bVar3;
                        i3++;
                    }
                }
                this.a = bVarArr2;
            }
            return this.a;
        }

        public boolean f(ZipEntry zipEntry, String str) {
            throw null;
        }
    }

    public e(Context context, String str, File file, String str2) {
        super(context, str);
        this.f = file;
        this.g = str2;
    }
}
