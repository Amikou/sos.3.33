package com.facebook.soloader;

import android.os.StrictMode;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

/* compiled from: DirectorySoSource.java */
/* loaded from: classes.dex */
public class c extends g {
    public final File a;
    public final int b;

    public c(File file, int i) {
        this.a = file;
        this.b = i;
    }

    public static String[] c(File file) throws IOException {
        boolean z = SoLoader.a;
        if (z) {
            Api18TraceUtils.a("SoLoader.getElfDependencies[", file.getName(), "]");
        }
        try {
            String[] a = MinElf.a(file);
            if (z) {
                Api18TraceUtils.b();
            }
            return a;
        } catch (Throwable th) {
            if (SoLoader.a) {
                Api18TraceUtils.b();
            }
            throw th;
        }
    }

    public static void d(File file, int i, StrictMode.ThreadPolicy threadPolicy) throws IOException {
        String[] c = c(file);
        StringBuilder sb = new StringBuilder();
        sb.append("Loading lib dependencies: ");
        sb.append(Arrays.toString(c));
        for (String str : c) {
            if (!str.startsWith("/")) {
                SoLoader.j(str, i | 1, threadPolicy);
            }
        }
    }

    @Override // com.facebook.soloader.g
    public int a(String str, int i, StrictMode.ThreadPolicy threadPolicy) throws IOException {
        return e(str, i, this.a, threadPolicy);
    }

    public int e(String str, int i, File file, StrictMode.ThreadPolicy threadPolicy) throws IOException {
        File file2 = new File(file, str);
        if (!file2.exists()) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(" not found on ");
            sb.append(file.getCanonicalPath());
            return 0;
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append(str);
        sb2.append(" found on ");
        sb2.append(file.getCanonicalPath());
        if ((i & 1) != 0 && (this.b & 2) != 0) {
            StringBuilder sb3 = new StringBuilder();
            sb3.append(str);
            sb3.append(" loaded implicitly");
            return 2;
        }
        if ((this.b & 1) != 0) {
            d(file2, i, threadPolicy);
        } else {
            StringBuilder sb4 = new StringBuilder();
            sb4.append("Not resolving dependencies for ");
            sb4.append(str);
        }
        try {
            SoLoader.b.a(file2.getAbsolutePath(), i);
            return 1;
        } catch (UnsatisfiedLinkError e) {
            if (e.getMessage().contains("bad ELF magic")) {
                return 3;
            }
            throw e;
        }
    }

    @Override // com.facebook.soloader.g
    public String toString() {
        String name;
        try {
            name = String.valueOf(this.a.getCanonicalPath());
        } catch (IOException unused) {
            name = this.a.getName();
        }
        return getClass().getName() + "[root = " + name + " flags = " + this.b + ']';
    }
}
