package com.facebook.soloader;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;
import okhttp3.internal.ws.WebSocketProtocol;

/* loaded from: classes.dex */
public final class MinElf {

    /* loaded from: classes.dex */
    public static class ElfError extends RuntimeException {
        public ElfError(String str) {
            super(str);
        }
    }

    /* loaded from: classes.dex */
    public enum ISA {
        NOT_SO("not_so"),
        X86("x86"),
        ARM("armeabi-v7a"),
        X86_64("x86_64"),
        AARCH64("arm64-v8a"),
        OTHERS("others");
        
        private final String value;

        ISA(String str) {
            this.value = str;
        }

        @Override // java.lang.Enum
        public String toString() {
            return this.value;
        }
    }

    /* JADX WARN: Can't wrap try/catch for region: R(3:13|14|15) */
    /* JADX WARN: Code restructure failed: missing block: B:10:0x0015, code lost:
        r0 = r0 + 1;
     */
    /* JADX WARN: Code restructure failed: missing block: B:11:0x0018, code lost:
        if (r0 <= 3) goto L13;
     */
    /* JADX WARN: Code restructure failed: missing block: B:12:0x001a, code lost:
        java.lang.Thread.interrupted();
     */
    /* JADX WARN: Code restructure failed: missing block: B:14:0x0021, code lost:
        throw r2;
     */
    /* JADX WARN: Code restructure failed: missing block: B:15:0x0022, code lost:
        r1.close();
     */
    /* JADX WARN: Code restructure failed: missing block: B:16:0x0025, code lost:
        throw r4;
     */
    /* JADX WARN: Code restructure failed: missing block: B:7:0x0012, code lost:
        r4 = move-exception;
     */
    /* JADX WARN: Code restructure failed: missing block: B:9:0x0014, code lost:
        r2 = move-exception;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static java.lang.String[] a(java.io.File r4) throws java.io.IOException {
        /*
            r0 = 0
        L1:
            java.io.FileInputStream r1 = new java.io.FileInputStream
            r1.<init>(r4)
            java.nio.channels.FileChannel r2 = r1.getChannel()     // Catch: java.lang.Throwable -> L12 java.nio.channels.ClosedByInterruptException -> L14
            java.lang.String[] r4 = b(r2)     // Catch: java.lang.Throwable -> L12 java.nio.channels.ClosedByInterruptException -> L14
            r1.close()
            return r4
        L12:
            r4 = move-exception
            goto L22
        L14:
            r2 = move-exception
            int r0 = r0 + 1
            r3 = 3
            if (r0 > r3) goto L21
            java.lang.Thread.interrupted()     // Catch: java.lang.Throwable -> L12
            r1.close()
            goto L1
        L21:
            throw r2     // Catch: java.lang.Throwable -> L12
        L22:
            r1.close()
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.soloader.MinElf.a(java.io.File):java.lang.String[]");
    }

    public static String[] b(FileChannel fileChannel) throws IOException {
        long f;
        long j;
        long j2;
        long c;
        String str;
        long j3;
        String str2;
        long f2;
        long c2;
        long c3;
        long c4;
        long f3;
        long f4;
        ByteBuffer allocate = ByteBuffer.allocate(8);
        allocate.order(ByteOrder.LITTLE_ENDIAN);
        if (f(fileChannel, allocate, 0L) == 1179403647) {
            boolean z = g(fileChannel, allocate, 4L) == 1;
            if (g(fileChannel, allocate, 5L) == 2) {
                allocate.order(ByteOrder.BIG_ENDIAN);
            }
            long f5 = z ? f(fileChannel, allocate, 28L) : c(fileChannel, allocate, 32L);
            long e = z ? e(fileChannel, allocate, 44L) : e(fileChannel, allocate, 56L);
            int e2 = e(fileChannel, allocate, z ? 42L : 54L);
            if (e == WebSocketProtocol.PAYLOAD_SHORT_MAX) {
                long f6 = z ? f(fileChannel, allocate, 32L) : c(fileChannel, allocate, 40L);
                if (z) {
                    f4 = f(fileChannel, allocate, f6 + 28);
                } else {
                    f4 = f(fileChannel, allocate, f6 + 44);
                }
                e = f4;
            }
            long j4 = f5;
            long j5 = 0;
            while (true) {
                if (j5 >= e) {
                    j = 0;
                    break;
                }
                if (z) {
                    f3 = f(fileChannel, allocate, j4 + 0);
                } else {
                    f3 = f(fileChannel, allocate, j4 + 0);
                }
                if (f3 != 2) {
                    j4 += e2;
                    j5++;
                } else if (z) {
                    j = f(fileChannel, allocate, j4 + 4);
                } else {
                    j = c(fileChannel, allocate, j4 + 8);
                }
            }
            long j6 = 0;
            if (j == 0) {
                throw new ElfError("ELF file does not contain dynamic linking information");
            }
            long j7 = j;
            long j8 = 0;
            int i = 0;
            while (true) {
                boolean z2 = z;
                long f7 = z ? f(fileChannel, allocate, j7 + j6) : c(fileChannel, allocate, j7 + j6);
                long j9 = j;
                String str3 = "malformed DT_NEEDED section";
                if (f7 == 1) {
                    if (i == Integer.MAX_VALUE) {
                        throw new ElfError("malformed DT_NEEDED section");
                    }
                    i++;
                    j2 = f7;
                } else if (f7 == 5) {
                    if (z2) {
                        j2 = f7;
                        c = f(fileChannel, allocate, j7 + 4);
                    } else {
                        j2 = f7;
                        c = c(fileChannel, allocate, j7 + 8);
                    }
                    j8 = c;
                } else {
                    j2 = f7;
                }
                long j10 = 16;
                j7 += z2 ? 8L : 16L;
                j6 = 0;
                if (j2 != 0) {
                    z = z2;
                    j = j9;
                } else if (j8 == 0) {
                    throw new ElfError("Dynamic section string-table not found");
                } else {
                    int i2 = 0;
                    while (true) {
                        if (i2 >= e) {
                            str = str3;
                            j3 = 0;
                            break;
                        }
                        if (z2) {
                            f2 = f(fileChannel, allocate, f5 + j6);
                        } else {
                            f2 = f(fileChannel, allocate, f5 + j6);
                        }
                        if (f2 == 1) {
                            if (z2) {
                                c2 = f(fileChannel, allocate, f5 + 8);
                            } else {
                                c2 = c(fileChannel, allocate, f5 + j10);
                            }
                            if (z2) {
                                str = str3;
                                c3 = f(fileChannel, allocate, f5 + 20);
                            } else {
                                str = str3;
                                c3 = c(fileChannel, allocate, f5 + 40);
                            }
                            if (c2 <= j8 && j8 < c3 + c2) {
                                if (z2) {
                                    c4 = f(fileChannel, allocate, f5 + 4);
                                } else {
                                    c4 = c(fileChannel, allocate, f5 + 8);
                                }
                                j3 = c4 + (j8 - c2);
                            }
                        } else {
                            str = str3;
                        }
                        f5 += e2;
                        i2++;
                        str3 = str;
                        j10 = 16;
                        j6 = 0;
                    }
                    long j11 = 0;
                    if (j3 != 0) {
                        String[] strArr = new String[i];
                        long j12 = j9;
                        int i3 = 0;
                        while (true) {
                            long j13 = j12 + j11;
                            long f8 = z2 ? f(fileChannel, allocate, j13) : c(fileChannel, allocate, j13);
                            if (f8 == 1) {
                                strArr[i3] = d(fileChannel, allocate, (z2 ? f(fileChannel, allocate, j12 + 4) : c(fileChannel, allocate, j12 + 8)) + j3);
                                if (i3 == Integer.MAX_VALUE) {
                                    throw new ElfError(str);
                                }
                                i3++;
                                str2 = str;
                            } else {
                                str2 = str;
                            }
                            j12 += z2 ? 8L : 16L;
                            if (f8 == 0) {
                                if (i3 == i) {
                                    return strArr;
                                }
                                throw new ElfError(str2);
                            }
                            str = str2;
                            j11 = 0;
                        }
                    } else {
                        throw new ElfError("did not find file offset of DT_STRTAB table");
                    }
                }
            }
        } else {
            throw new ElfError("file is not ELF: 0x" + Long.toHexString(f));
        }
    }

    public static long c(FileChannel fileChannel, ByteBuffer byteBuffer, long j) throws IOException {
        h(fileChannel, byteBuffer, 8, j);
        return byteBuffer.getLong();
    }

    public static String d(FileChannel fileChannel, ByteBuffer byteBuffer, long j) throws IOException {
        StringBuilder sb = new StringBuilder();
        while (true) {
            long j2 = 1 + j;
            short g = g(fileChannel, byteBuffer, j);
            if (g != 0) {
                sb.append((char) g);
                j = j2;
            } else {
                return sb.toString();
            }
        }
    }

    public static int e(FileChannel fileChannel, ByteBuffer byteBuffer, long j) throws IOException {
        h(fileChannel, byteBuffer, 2, j);
        return byteBuffer.getShort() & 65535;
    }

    public static long f(FileChannel fileChannel, ByteBuffer byteBuffer, long j) throws IOException {
        h(fileChannel, byteBuffer, 4, j);
        return byteBuffer.getInt() & 4294967295L;
    }

    public static short g(FileChannel fileChannel, ByteBuffer byteBuffer, long j) throws IOException {
        h(fileChannel, byteBuffer, 1, j);
        return (short) (byteBuffer.get() & 255);
    }

    public static void h(FileChannel fileChannel, ByteBuffer byteBuffer, int i, long j) throws IOException {
        int read;
        byteBuffer.position(0);
        byteBuffer.limit(i);
        while (byteBuffer.remaining() > 0 && (read = fileChannel.read(byteBuffer, j)) != -1) {
            j += read;
        }
        if (byteBuffer.remaining() <= 0) {
            byteBuffer.position(0);
            return;
        }
        throw new ElfError("ELF file truncated");
    }
}
