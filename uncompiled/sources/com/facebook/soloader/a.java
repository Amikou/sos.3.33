package com.facebook.soloader;

import android.content.Context;
import android.os.Parcel;
import com.facebook.soloader.e;
import com.facebook.soloader.h;
import java.io.File;
import java.io.IOException;
import java.util.zip.ZipEntry;

/* compiled from: ApkSoSource.java */
/* loaded from: classes.dex */
public class a extends e {
    public final int h;

    /* compiled from: ApkSoSource.java */
    /* renamed from: com.facebook.soloader.a$a  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public class C0089a extends e.c {
        public File i0;
        public final int j0;

        public C0089a(e eVar) throws IOException {
            super(eVar);
            this.i0 = new File(a.this.c.getApplicationInfo().nativeLibraryDir);
            this.j0 = a.this.h;
        }

        @Override // com.facebook.soloader.e.c
        public boolean f(ZipEntry zipEntry, String str) {
            String name = zipEntry.getName();
            if (str.equals(a.this.d)) {
                a.this.d = null;
                String.format("allowing consideration of corrupted lib %s", str);
            } else if ((this.j0 & 1) == 0) {
                StringBuilder sb = new StringBuilder();
                sb.append("allowing consideration of ");
                sb.append(name);
                sb.append(": self-extraction preferred");
            } else {
                File file = new File(this.i0, str);
                if (!file.isFile()) {
                    String.format("allowing considering of %s: %s not in system lib dir", name, str);
                } else {
                    long length = file.length();
                    long size = zipEntry.getSize();
                    if (length != size) {
                        String.format("allowing consideration of %s: sysdir file length is %s, but the file is %s bytes long in the APK", file, Long.valueOf(length), Long.valueOf(size));
                    } else {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("not allowing consideration of ");
                        sb2.append(name);
                        sb2.append(": deferring to libdir");
                        return false;
                    }
                }
            }
            return true;
        }
    }

    public a(Context context, File file, String str, int i) {
        super(context, str, file, "^lib/([^/]+)/([^/]+\\.so)$");
        this.h = i;
    }

    @Override // com.facebook.soloader.h
    public byte[] j() throws IOException {
        File canonicalFile = this.f.getCanonicalFile();
        Parcel obtain = Parcel.obtain();
        try {
            obtain.writeByte((byte) 2);
            obtain.writeString(canonicalFile.getPath());
            obtain.writeLong(canonicalFile.lastModified());
            obtain.writeInt(SysUtil.g(this.c));
            if ((this.h & 1) == 0) {
                obtain.writeByte((byte) 0);
                return obtain.marshall();
            }
            String str = this.c.getApplicationInfo().nativeLibraryDir;
            if (str == null) {
                obtain.writeByte((byte) 1);
                return obtain.marshall();
            }
            File canonicalFile2 = new File(str).getCanonicalFile();
            if (!canonicalFile2.exists()) {
                obtain.writeByte((byte) 1);
                return obtain.marshall();
            }
            obtain.writeByte((byte) 2);
            obtain.writeString(canonicalFile2.getPath());
            obtain.writeLong(canonicalFile2.lastModified());
            return obtain.marshall();
        } finally {
            obtain.recycle();
        }
    }

    @Override // com.facebook.soloader.h
    public h.f m() throws IOException {
        return new C0089a(this);
    }
}
