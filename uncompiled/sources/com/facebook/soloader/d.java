package com.facebook.soloader;

import android.content.Context;
import com.facebook.soloader.h;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/* compiled from: ExoSoSource.java */
/* loaded from: classes.dex */
public final class d extends h {

    /* compiled from: ExoSoSource.java */
    /* loaded from: classes.dex */
    public final class b extends h.f {
        public final c[] a;

        /* compiled from: ExoSoSource.java */
        /* loaded from: classes.dex */
        public final class a extends h.e {
            public int a;

            public a() {
            }

            @Override // com.facebook.soloader.h.e
            public boolean a() {
                return this.a < b.this.a.length;
            }

            @Override // com.facebook.soloader.h.e
            public h.d b() throws IOException {
                c[] cVarArr = b.this.a;
                int i = this.a;
                this.a = i + 1;
                c cVar = cVarArr[i];
                FileInputStream fileInputStream = new FileInputStream(cVar.g0);
                try {
                    return new h.d(cVar, fileInputStream);
                } catch (Throwable th) {
                    fileInputStream.close();
                    throw th;
                }
            }
        }

        /* JADX WARN: Code restructure failed: missing block: B:30:0x00dc, code lost:
            throw new java.lang.RuntimeException("illegal line in exopackage metadata: [" + r10 + "]");
         */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public b(com.facebook.soloader.d r17, com.facebook.soloader.h r18) throws java.io.IOException {
            /*
                Method dump skipped, instructions count: 295
                To view this dump change 'Code comments level' option to 'DEBUG'
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.soloader.d.b.<init>(com.facebook.soloader.d, com.facebook.soloader.h):void");
        }

        @Override // com.facebook.soloader.h.f
        public h.c a() throws IOException {
            return new h.c(this.a);
        }

        @Override // com.facebook.soloader.h.f
        public h.e b() throws IOException {
            return new a();
        }
    }

    /* compiled from: ExoSoSource.java */
    /* loaded from: classes.dex */
    public static final class c extends h.b {
        public final File g0;

        public c(String str, String str2, File file) {
            super(str, str2);
            this.g0 = file;
        }
    }

    public d(Context context, String str) {
        super(context, str);
    }

    @Override // com.facebook.soloader.h
    public h.f m() throws IOException {
        return new b(this, this);
    }
}
