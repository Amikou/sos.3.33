package com.facebook.soloader;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.StrictMode;
import android.text.TextUtils;
import dalvik.system.BaseDexClassLoader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/* loaded from: classes.dex */
public class SoLoader {
    public static final boolean a;
    public static ar3 b;
    public static h[] f;
    public static b g;
    public static int l;
    public static boolean m;
    public static final ReentrantReadWriteLock c = new ReentrantReadWriteLock();
    public static g[] d = null;
    public static volatile int e = 0;
    public static final HashSet<String> h = new HashSet<>();
    public static final Map<String, Object> i = new HashMap();
    public static final Set<String> j = Collections.newSetFromMap(new ConcurrentHashMap());
    public static z24 k = null;

    @TargetApi(14)
    @bq0
    /* loaded from: classes.dex */
    public static class Api14Utils {
        private Api14Utils() {
        }

        public static String a() {
            ClassLoader classLoader = SoLoader.class.getClassLoader();
            if (classLoader != null && !(classLoader instanceof BaseDexClassLoader)) {
                throw new IllegalStateException("ClassLoader " + classLoader.getClass().getName() + " should be of type BaseDexClassLoader");
            }
            try {
                return (String) BaseDexClassLoader.class.getMethod("getLdLibraryPath", new Class[0]).invoke((BaseDexClassLoader) classLoader, new Object[0]);
            } catch (Exception e) {
                throw new RuntimeException("Cannot call getLdLibraryPath", e);
            }
        }
    }

    /* loaded from: classes.dex */
    public static final class WrongAbiError extends UnsatisfiedLinkError {
        public WrongAbiError(Throwable th, String str) {
            super("APK was built for a different platform. Supported ABIs: " + Arrays.toString(SysUtil.h()) + " error: " + str);
            initCause(th);
        }
    }

    /* loaded from: classes.dex */
    public static class a implements ar3 {
        public final /* synthetic */ boolean a;
        public final /* synthetic */ String b;
        public final /* synthetic */ String c;
        public final /* synthetic */ Runtime d;
        public final /* synthetic */ Method e;

        public a(boolean z, String str, String str2, Runtime runtime, Method method) {
            this.a = z;
            this.b = str;
            this.c = str2;
            this.d = runtime;
            this.e = method;
        }

        /* JADX WARN: Code restructure failed: missing block: B:17:0x0035, code lost:
            if (r1 == null) goto L21;
         */
        /* JADX WARN: Code restructure failed: missing block: B:18:0x0037, code lost:
            r0 = new java.lang.StringBuilder();
            r0.append("Error when loading lib: ");
            r0.append(r1);
            r0.append(" lib hash: ");
            r0.append(b(r9));
            r0.append(" search path is ");
            r0.append(r10);
         */
        /* JADX WARN: Code restructure failed: missing block: B:56:?, code lost:
            return;
         */
        /* JADX WARN: Code restructure failed: missing block: B:57:?, code lost:
            return;
         */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Removed duplicated region for block: B:45:0x0095  */
        /* JADX WARN: Type inference failed for: r1v0 */
        /* JADX WARN: Type inference failed for: r1v2 */
        @Override // defpackage.ar3
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public void a(java.lang.String r9, int r10) {
            /*
                Method dump skipped, instructions count: 187
                To view this dump change 'Code comments level' option to 'DEBUG'
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.soloader.SoLoader.a.a(java.lang.String, int):void");
        }

        public final String b(String str) {
            try {
                File file = new File(str);
                MessageDigest messageDigest = MessageDigest.getInstance("MD5");
                FileInputStream fileInputStream = new FileInputStream(file);
                try {
                    byte[] bArr = new byte[4096];
                    while (true) {
                        int read = fileInputStream.read(bArr);
                        if (read > 0) {
                            messageDigest.update(bArr, 0, read);
                        } else {
                            String format = String.format("%32x", new BigInteger(1, messageDigest.digest()));
                            fileInputStream.close();
                            return format;
                        }
                    }
                } catch (Throwable th) {
                    try {
                        throw th;
                    } catch (Throwable th2) {
                        try {
                            fileInputStream.close();
                        } catch (Throwable th3) {
                            th.addSuppressed(th3);
                        }
                        throw th2;
                    }
                }
            } catch (IOException e) {
                return e.toString();
            } catch (SecurityException e2) {
                return e2.toString();
            } catch (NoSuchAlgorithmException e3) {
                return e3.toString();
            }
        }
    }

    static {
        boolean z = false;
        try {
            if (Build.VERSION.SDK_INT >= 18) {
                z = true;
            }
        } catch (NoClassDefFoundError | UnsatisfiedLinkError unused) {
        }
        a = z;
    }

    public static void a() {
        if (!h()) {
            throw new IllegalStateException("SoLoader.init() not yet called");
        }
    }

    public static boolean b(Context context, int i2) {
        return ((i2 & 32) != 0 || context == null || (context.getApplicationInfo().flags & 129) == 0) ? false : true;
    }

    public static void c(String str, int i2, StrictMode.ThreadPolicy threadPolicy) throws UnsatisfiedLinkError {
        boolean z;
        ReentrantReadWriteLock reentrantReadWriteLock;
        h[] hVarArr;
        ReentrantReadWriteLock reentrantReadWriteLock2 = c;
        reentrantReadWriteLock2.readLock().lock();
        try {
            if (d != null) {
                reentrantReadWriteLock2.readLock().unlock();
                int i3 = 0;
                if (threadPolicy == null) {
                    threadPolicy = StrictMode.allowThreadDiskReads();
                    z = true;
                } else {
                    z = false;
                }
                if (a) {
                    Api18TraceUtils.a("SoLoader.loadLibrary[", str, "]");
                }
                try {
                    reentrantReadWriteLock2.readLock().lock();
                    int i4 = 0;
                    int i5 = 0;
                    while (i4 == 0) {
                        g[] gVarArr = d;
                        if (i5 < gVarArr.length) {
                            i4 = gVarArr[i5].a(str, i2, threadPolicy);
                            if (i4 != 3 || f == null) {
                                i5++;
                            } else {
                                StringBuilder sb = new StringBuilder();
                                sb.append("Trying backup SoSource for ");
                                sb.append(str);
                                for (h hVar : f) {
                                    hVar.n(str);
                                    int a2 = hVar.a(str, i2, threadPolicy);
                                    if (a2 == 1) {
                                        i4 = a2;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    try {
                        if (a) {
                            Api18TraceUtils.b();
                        }
                        if (z) {
                            StrictMode.setThreadPolicy(threadPolicy);
                        }
                        if (i4 == 0 || i4 == 3) {
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append("couldn't find DSO to load: ");
                            sb2.append(str);
                            reentrantReadWriteLock.readLock().lock();
                            while (i3 < d.length) {
                                sb2.append("\n\tSoSource ");
                                sb2.append(i3);
                                sb2.append(": ");
                                sb2.append(d[i3].toString());
                                i3++;
                            }
                            b bVar = g;
                            if (bVar != null) {
                                File d2 = b.d(bVar.e());
                                sb2.append("\n\tNative lib dir: ");
                                sb2.append(d2.getAbsolutePath());
                                sb2.append("\n");
                            }
                            c.readLock().unlock();
                            sb2.append(" result: ");
                            sb2.append(i4);
                            throw new UnsatisfiedLinkError(sb2.toString());
                        }
                    } catch (Throwable th) {
                        th = th;
                        i3 = i4;
                        if (a) {
                            Api18TraceUtils.b();
                        }
                        if (z) {
                            StrictMode.setThreadPolicy(threadPolicy);
                        }
                        if (i3 == 0 || i3 == 3) {
                            StringBuilder sb3 = new StringBuilder();
                            sb3.append("couldn't find DSO to load: ");
                            sb3.append(str);
                            String message = th.getMessage();
                            if (message == null) {
                                message = th.toString();
                            }
                            sb3.append(" caused by: ");
                            sb3.append(message);
                            th.printStackTrace();
                            sb3.append(" result: ");
                            sb3.append(i3);
                            UnsatisfiedLinkError unsatisfiedLinkError = new UnsatisfiedLinkError(sb3.toString());
                            unsatisfiedLinkError.initCause(th);
                            throw unsatisfiedLinkError;
                        }
                    }
                } catch (Throwable th2) {
                    th = th2;
                }
            } else {
                StringBuilder sb4 = new StringBuilder();
                sb4.append("Could not load: ");
                sb4.append(str);
                sb4.append(" because no SO source exists");
                throw new UnsatisfiedLinkError("couldn't find DSO to load: " + str);
            }
        } finally {
            c.readLock().unlock();
        }
    }

    public static Method d() {
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 23 && i2 <= 27) {
            try {
                Method declaredMethod = Runtime.class.getDeclaredMethod("nativeLoad", String.class, ClassLoader.class, String.class);
                declaredMethod.setAccessible(true);
                return declaredMethod;
            } catch (NoSuchMethodException | SecurityException unused) {
            }
        }
        return null;
    }

    public static void e(Context context, int i2, ar3 ar3Var) throws IOException {
        StrictMode.ThreadPolicy allowThreadDiskWrites = StrictMode.allowThreadDiskWrites();
        try {
            m = b(context, i2);
            f(ar3Var);
            g(context, i2, ar3Var);
            pd2.b(new rd2());
        } finally {
            StrictMode.setThreadPolicy(allowThreadDiskWrites);
        }
    }

    public static synchronized void f(ar3 ar3Var) {
        synchronized (SoLoader.class) {
            if (ar3Var != null) {
                b = ar3Var;
                return;
            }
            Runtime runtime = Runtime.getRuntime();
            Method d2 = d();
            boolean z = d2 != null;
            String a2 = z ? Api14Utils.a() : null;
            b = new a(z, a2, m(a2), runtime, d2);
        }
    }

    public static void g(Context context, int i2, ar3 ar3Var) throws IOException {
        String[] split;
        int i3;
        c.writeLock().lock();
        try {
            if (d == null) {
                l = i2;
                ArrayList arrayList = new ArrayList();
                String str = System.getenv("LD_LIBRARY_PATH");
                if (str == null) {
                    str = SysUtil.i() ? "/vendor/lib64:/system/lib64" : "/vendor/lib:/system/lib";
                }
                for (String str2 : str.split(":")) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("adding system library source: ");
                    sb.append(str2);
                    arrayList.add(new c(new File(str2), 2));
                }
                if (context != null) {
                    if ((i2 & 1) != 0) {
                        f = null;
                        arrayList.add(0, new d(context, "lib-main"));
                    } else {
                        if (m) {
                            i3 = 0;
                        } else {
                            g = new b(context, Build.VERSION.SDK_INT <= 17 ? 1 : 0);
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append("adding application source: ");
                            sb2.append(g.toString());
                            arrayList.add(0, g);
                            i3 = 1;
                        }
                        if ((l & 8) != 0) {
                            f = null;
                        } else {
                            File file = new File(context.getApplicationInfo().sourceDir);
                            ArrayList arrayList2 = new ArrayList();
                            com.facebook.soloader.a aVar = new com.facebook.soloader.a(context, file, "lib-main", i3);
                            arrayList2.add(aVar);
                            StringBuilder sb3 = new StringBuilder();
                            sb3.append("adding backup source from : ");
                            sb3.append(aVar.toString());
                            if (Build.VERSION.SDK_INT >= 21 && context.getApplicationInfo().splitSourceDirs != null) {
                                String[] strArr = context.getApplicationInfo().splitSourceDirs;
                                int length = strArr.length;
                                int i4 = 0;
                                int i5 = 0;
                                while (i4 < length) {
                                    File file2 = new File(strArr[i4]);
                                    StringBuilder sb4 = new StringBuilder();
                                    sb4.append("lib-");
                                    int i6 = i5 + 1;
                                    sb4.append(i5);
                                    com.facebook.soloader.a aVar2 = new com.facebook.soloader.a(context, file2, sb4.toString(), i3);
                                    StringBuilder sb5 = new StringBuilder();
                                    sb5.append("adding backup source: ");
                                    sb5.append(aVar2.toString());
                                    arrayList2.add(aVar2);
                                    i4++;
                                    i5 = i6;
                                }
                            }
                            f = (h[]) arrayList2.toArray(new h[arrayList2.size()]);
                            arrayList.addAll(0, arrayList2);
                        }
                    }
                }
                g[] gVarArr = (g[]) arrayList.toArray(new g[arrayList.size()]);
                int n = n();
                int length2 = gVarArr.length;
                while (true) {
                    int i7 = length2 - 1;
                    if (length2 <= 0) {
                        break;
                    }
                    StringBuilder sb6 = new StringBuilder();
                    sb6.append("Preparing SO source: ");
                    sb6.append(gVarArr[i7]);
                    gVarArr[i7].b(n);
                    length2 = i7;
                }
                d = gVarArr;
                e++;
                StringBuilder sb7 = new StringBuilder();
                sb7.append("init finish: ");
                sb7.append(d.length);
                sb7.append(" SO sources prepared");
            }
        } finally {
            c.writeLock().unlock();
        }
    }

    public static boolean h() {
        ReentrantReadWriteLock reentrantReadWriteLock = c;
        reentrantReadWriteLock.readLock().lock();
        try {
            boolean z = d != null;
            reentrantReadWriteLock.readLock().unlock();
            return z;
        } catch (Throwable th) {
            c.readLock().unlock();
            throw th;
        }
    }

    public static boolean i(String str, int i2) throws UnsatisfiedLinkError {
        z24 z24Var;
        boolean z;
        ReentrantReadWriteLock reentrantReadWriteLock = c;
        reentrantReadWriteLock.readLock().lock();
        try {
            if (d == null) {
                if ("http://www.android.com/".equals(System.getProperty("java.vendor.url"))) {
                    a();
                } else {
                    synchronized (SoLoader.class) {
                        z = !h.contains(str);
                        if (z) {
                            z24 z24Var2 = k;
                            if (z24Var2 != null) {
                                z24Var2.a(str);
                            } else {
                                System.loadLibrary(str);
                            }
                        }
                    }
                    reentrantReadWriteLock.readLock().unlock();
                    return z;
                }
            }
            reentrantReadWriteLock.readLock().unlock();
            if (m && (z24Var = k) != null) {
                z24Var.a(str);
                return true;
            }
            String b2 = x72.b(str);
            return k(System.mapLibraryName(b2 != null ? b2 : str), str, b2, i2, null);
        } catch (Throwable th) {
            c.readLock().unlock();
            throw th;
        }
    }

    public static void init(Context context, int i2) throws IOException {
        e(context, i2, null);
    }

    public static void j(String str, int i2, StrictMode.ThreadPolicy threadPolicy) {
        l(str, null, null, i2, threadPolicy);
    }

    public static boolean k(String str, String str2, String str3, int i2, StrictMode.ThreadPolicy threadPolicy) {
        boolean z;
        boolean z2 = false;
        do {
            try {
                z2 = l(str, str2, str3, i2, threadPolicy);
                z = false;
                continue;
            } catch (UnsatisfiedLinkError e2) {
                int i3 = e;
                c.writeLock().lock();
                try {
                    try {
                        z = true;
                        if (g == null || !g.c()) {
                            z = false;
                        } else {
                            StringBuilder sb = new StringBuilder();
                            sb.append("sApplicationSoSource updated during load: ");
                            sb.append(str);
                            sb.append(", attempting load again.");
                            e++;
                        }
                        c.writeLock().unlock();
                        if (e == i3) {
                            throw e2;
                        }
                    } catch (IOException e3) {
                        throw new RuntimeException(e3);
                    }
                } catch (Throwable th) {
                    c.writeLock().unlock();
                    throw th;
                }
            }
        } while (z);
        return z2;
    }

    public static boolean l(String str, String str2, String str3, int i2, StrictMode.ThreadPolicy threadPolicy) {
        boolean z;
        Object obj;
        boolean z2 = false;
        if (TextUtils.isEmpty(str2) || !j.contains(str2)) {
            synchronized (SoLoader.class) {
                HashSet<String> hashSet = h;
                if (!hashSet.contains(str)) {
                    z = false;
                } else if (str3 == null) {
                    return false;
                } else {
                    z = true;
                }
                Map<String, Object> map = i;
                if (map.containsKey(str)) {
                    obj = map.get(str);
                } else {
                    Object obj2 = new Object();
                    map.put(str, obj2);
                    obj = obj2;
                }
                ReentrantReadWriteLock reentrantReadWriteLock = c;
                reentrantReadWriteLock.readLock().lock();
                try {
                    synchronized (obj) {
                        if (!z) {
                            synchronized (SoLoader.class) {
                                if (hashSet.contains(str)) {
                                    if (str3 == null) {
                                        reentrantReadWriteLock.readLock().unlock();
                                        return false;
                                    }
                                    z = true;
                                }
                                if (!z) {
                                    try {
                                        StringBuilder sb = new StringBuilder();
                                        sb.append("About to load: ");
                                        sb.append(str);
                                        c(str, i2, threadPolicy);
                                        synchronized (SoLoader.class) {
                                            StringBuilder sb2 = new StringBuilder();
                                            sb2.append("Loaded: ");
                                            sb2.append(str);
                                            hashSet.add(str);
                                        }
                                    } catch (UnsatisfiedLinkError e2) {
                                        String message = e2.getMessage();
                                        if (message != null && message.contains("unexpected e_machine:")) {
                                            throw new WrongAbiError(e2, message.substring(message.lastIndexOf("unexpected e_machine:")));
                                        }
                                        throw e2;
                                    }
                                }
                            }
                        }
                        if ((i2 & 16) == 0) {
                            if (!TextUtils.isEmpty(str2) && j.contains(str2)) {
                                z2 = true;
                            }
                            if (str3 != null && !z2) {
                                boolean z3 = a;
                                if (z3) {
                                    Api18TraceUtils.a("MergedSoMapping.invokeJniOnload[", str2, "]");
                                }
                                try {
                                    StringBuilder sb3 = new StringBuilder();
                                    sb3.append("About to merge: ");
                                    sb3.append(str2);
                                    sb3.append(" / ");
                                    sb3.append(str);
                                    x72.a(str2);
                                    j.add(str2);
                                    if (z3) {
                                        Api18TraceUtils.b();
                                    }
                                } catch (UnsatisfiedLinkError e3) {
                                    throw new RuntimeException("Failed to call JNI_OnLoad from '" + str2 + "', which has been merged into '" + str + "'.  See comment for details.", e3);
                                }
                            }
                        }
                        reentrantReadWriteLock.readLock().unlock();
                        return !z;
                    }
                } catch (Throwable th) {
                    c.readLock().unlock();
                    throw th;
                }
            }
        }
        return false;
    }

    public static String m(String str) {
        if (str == null) {
            return null;
        }
        String[] split = str.split(":");
        ArrayList arrayList = new ArrayList(split.length);
        for (String str2 : split) {
            if (!str2.contains("!")) {
                arrayList.add(str2);
            }
        }
        return TextUtils.join(":", arrayList);
    }

    public static int n() {
        ReentrantReadWriteLock reentrantReadWriteLock = c;
        reentrantReadWriteLock.writeLock().lock();
        try {
            int i2 = (l & 2) != 0 ? 1 : 0;
            reentrantReadWriteLock.writeLock().unlock();
            return i2;
        } catch (Throwable th) {
            c.writeLock().unlock();
            throw th;
        }
    }
}
