package com.facebook.soloader;

import android.os.StrictMode;
import java.io.IOException;

/* compiled from: SoSource.java */
/* loaded from: classes.dex */
public abstract class g {
    public abstract int a(String str, int i, StrictMode.ThreadPolicy threadPolicy) throws IOException;

    public void b(int i) throws IOException {
    }

    public String toString() {
        return getClass().getName();
    }
}
