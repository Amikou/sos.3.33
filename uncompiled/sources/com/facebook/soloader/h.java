package com.facebook.soloader;

import android.content.Context;
import android.os.Parcel;
import android.os.StrictMode;
import java.io.Closeable;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.util.HashMap;
import java.util.Map;

/* compiled from: UnpackingSoSource.java */
/* loaded from: classes.dex */
public abstract class h extends com.facebook.soloader.c {
    public final Context c;
    public String d;
    public final Map<String, Object> e;

    /* compiled from: UnpackingSoSource.java */
    /* loaded from: classes.dex */
    public class a implements Runnable {
        public final /* synthetic */ File a;
        public final /* synthetic */ byte[] f0;
        public final /* synthetic */ c g0;
        public final /* synthetic */ File h0;
        public final /* synthetic */ com.facebook.soloader.f i0;

        public a(File file, byte[] bArr, c cVar, File file2, com.facebook.soloader.f fVar) {
            this.a = file;
            this.f0 = bArr;
            this.g0 = cVar;
            this.h0 = file2;
            this.i0 = fVar;
        }

        @Override // java.lang.Runnable
        public void run() {
            try {
                RandomAccessFile randomAccessFile = new RandomAccessFile(this.a, "rw");
                try {
                    randomAccessFile.write(this.f0);
                    randomAccessFile.setLength(randomAccessFile.getFilePointer());
                    randomAccessFile.close();
                    RandomAccessFile randomAccessFile2 = new RandomAccessFile(new File(h.this.a, "dso_manifest"), "rw");
                    try {
                        this.g0.b(randomAccessFile2);
                        randomAccessFile2.close();
                        SysUtil.f(h.this.a);
                        h.r(this.h0, (byte) 1);
                        StringBuilder sb = new StringBuilder();
                        sb.append("releasing dso store lock for ");
                        sb.append(h.this.a);
                        sb.append(" (from syncer thread)");
                        this.i0.close();
                    } finally {
                    }
                } catch (Throwable th) {
                    try {
                        throw th;
                    } catch (Throwable th2) {
                        try {
                            randomAccessFile.close();
                        } catch (Throwable th3) {
                            th.addSuppressed(th3);
                        }
                        throw th2;
                    }
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    /* compiled from: UnpackingSoSource.java */
    /* loaded from: classes.dex */
    public static class b {
        public final String a;
        public final String f0;

        public b(String str, String str2) {
            this.a = str;
            this.f0 = str2;
        }
    }

    /* compiled from: UnpackingSoSource.java */
    /* loaded from: classes.dex */
    public static final class c {
        public final b[] a;

        public c(b[] bVarArr) {
            this.a = bVarArr;
        }

        public static final c a(DataInput dataInput) throws IOException {
            if (dataInput.readByte() == 1) {
                int readInt = dataInput.readInt();
                if (readInt >= 0) {
                    b[] bVarArr = new b[readInt];
                    for (int i = 0; i < readInt; i++) {
                        bVarArr[i] = new b(dataInput.readUTF(), dataInput.readUTF());
                    }
                    return new c(bVarArr);
                }
                throw new RuntimeException("illegal number of shared libraries");
            }
            throw new RuntimeException("wrong dso manifest version");
        }

        public final void b(DataOutput dataOutput) throws IOException {
            dataOutput.writeByte(1);
            dataOutput.writeInt(this.a.length);
            int i = 0;
            while (true) {
                b[] bVarArr = this.a;
                if (i >= bVarArr.length) {
                    return;
                }
                dataOutput.writeUTF(bVarArr[i].a);
                dataOutput.writeUTF(this.a[i].f0);
                i++;
            }
        }
    }

    /* compiled from: UnpackingSoSource.java */
    /* loaded from: classes.dex */
    public static final class d implements Closeable {
        public final b a;
        public final InputStream f0;

        public d(b bVar, InputStream inputStream) {
            this.a = bVar;
            this.f0 = inputStream;
        }

        @Override // java.io.Closeable, java.lang.AutoCloseable
        public void close() throws IOException {
            this.f0.close();
        }
    }

    /* compiled from: UnpackingSoSource.java */
    /* loaded from: classes.dex */
    public static abstract class e implements Closeable {
        public abstract boolean a();

        public abstract d b() throws IOException;

        @Override // java.io.Closeable, java.lang.AutoCloseable
        public void close() throws IOException {
        }
    }

    /* compiled from: UnpackingSoSource.java */
    /* loaded from: classes.dex */
    public static abstract class f implements Closeable {
        public abstract c a() throws IOException;

        public abstract e b() throws IOException;

        @Override // java.io.Closeable, java.lang.AutoCloseable
        public void close() throws IOException {
        }
    }

    public h(Context context, String str) {
        super(l(context, str), 1);
        this.e = new HashMap();
        this.c = context;
    }

    public static File l(Context context, String str) {
        return new File(context.getApplicationInfo().dataDir + "/" + str);
    }

    public static void r(File file, byte b2) throws IOException {
        RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
        try {
            randomAccessFile.seek(0L);
            randomAccessFile.write(b2);
            randomAccessFile.setLength(randomAccessFile.getFilePointer());
            randomAccessFile.getFD().sync();
            randomAccessFile.close();
        } catch (Throwable th) {
            try {
                throw th;
            } catch (Throwable th2) {
                try {
                    randomAccessFile.close();
                } catch (Throwable th3) {
                    th.addSuppressed(th3);
                }
                throw th2;
            }
        }
    }

    @Override // com.facebook.soloader.c, com.facebook.soloader.g
    public int a(String str, int i, StrictMode.ThreadPolicy threadPolicy) throws IOException {
        int e2;
        synchronized (k(str)) {
            e2 = e(str, i, this.a, threadPolicy);
        }
        return e2;
    }

    @Override // com.facebook.soloader.g
    public void b(int i) throws IOException {
        SysUtil.j(this.a);
        com.facebook.soloader.f a2 = com.facebook.soloader.f.a(new File(this.a, "dso_lock"));
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("locked dso store ");
            sb.append(this.a);
            if (o(a2, i, j())) {
                a2 = null;
            } else {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("dso store is up-to-date: ");
                sb2.append(this.a);
            }
        } finally {
            if (a2 != null) {
                StringBuilder sb3 = new StringBuilder();
                sb3.append("releasing dso store lock for ");
                sb3.append(this.a);
                a2.close();
            } else {
                StringBuilder sb4 = new StringBuilder();
                sb4.append("not releasing dso store lock for ");
                sb4.append(this.a);
                sb4.append(" (syncer thread started)");
            }
        }
    }

    public final void g(b[] bVarArr) throws IOException {
        String[] list = this.a.list();
        if (list != null) {
            for (String str : list) {
                if (!str.equals("dso_state") && !str.equals("dso_lock") && !str.equals("dso_deps") && !str.equals("dso_manifest")) {
                    boolean z = false;
                    for (int i = 0; !z && i < bVarArr.length; i++) {
                        if (bVarArr[i].a.equals(str)) {
                            z = true;
                        }
                    }
                    if (!z) {
                        File file = new File(this.a, str);
                        StringBuilder sb = new StringBuilder();
                        sb.append("deleting unaccounted-for file ");
                        sb.append(file);
                        SysUtil.c(file);
                    }
                }
            }
            return;
        }
        throw new IOException("unable to list directory " + this.a);
    }

    public final void h(d dVar, byte[] bArr) throws IOException {
        boolean writable;
        StringBuilder sb = new StringBuilder();
        sb.append("extracting DSO ");
        sb.append(dVar.a.a);
        try {
            if (this.a.setWritable(true)) {
                i(dVar, bArr);
                if (writable) {
                    return;
                }
                return;
            }
            throw new IOException("cannot make directory writable for us: " + this.a);
        } finally {
            if (!this.a.setWritable(false)) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("error removing ");
                sb2.append(this.a.getCanonicalPath());
                sb2.append(" write permission");
            }
        }
    }

    public final void i(d dVar, byte[] bArr) throws IOException {
        RandomAccessFile randomAccessFile;
        File file = new File(this.a, dVar.a.a);
        RandomAccessFile randomAccessFile2 = null;
        try {
            try {
                if (file.exists() && !file.setWritable(true)) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("error adding write permission to: ");
                    sb.append(file);
                }
                try {
                    randomAccessFile = new RandomAccessFile(file, "rw");
                } catch (IOException unused) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("error overwriting ");
                    sb2.append(file);
                    sb2.append(" trying to delete and start over");
                    SysUtil.c(file);
                    randomAccessFile = new RandomAccessFile(file, "rw");
                }
                randomAccessFile2 = randomAccessFile;
                int available = dVar.f0.available();
                if (available > 1) {
                    SysUtil.d(randomAccessFile2.getFD(), available);
                }
                SysUtil.a(randomAccessFile2, dVar.f0, Integer.MAX_VALUE, bArr);
                randomAccessFile2.setLength(randomAccessFile2.getFilePointer());
                if (file.setExecutable(true, false)) {
                    if (!file.setWritable(false)) {
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append("error removing ");
                        sb3.append(file);
                        sb3.append(" write permission");
                    }
                    randomAccessFile2.close();
                    return;
                }
                throw new IOException("cannot make file executable: " + file);
            } catch (IOException e2) {
                SysUtil.c(file);
                throw e2;
            }
        } catch (Throwable th) {
            if (!file.setWritable(false)) {
                StringBuilder sb4 = new StringBuilder();
                sb4.append("error removing ");
                sb4.append(file);
                sb4.append(" write permission");
            }
            if (randomAccessFile2 != null) {
                randomAccessFile2.close();
            }
            throw th;
        }
    }

    public byte[] j() throws IOException {
        Parcel obtain = Parcel.obtain();
        f m = m();
        try {
            b[] bVarArr = m.a().a;
            obtain.writeByte((byte) 1);
            obtain.writeInt(bVarArr.length);
            for (int i = 0; i < bVarArr.length; i++) {
                obtain.writeString(bVarArr[i].a);
                obtain.writeString(bVarArr[i].f0);
            }
            m.close();
            byte[] marshall = obtain.marshall();
            obtain.recycle();
            return marshall;
        } catch (Throwable th) {
            try {
                throw th;
            } catch (Throwable th2) {
                if (m != null) {
                    try {
                        m.close();
                    } catch (Throwable th3) {
                        th.addSuppressed(th3);
                    }
                }
                throw th2;
            }
        }
    }

    public final Object k(String str) {
        Object obj;
        synchronized (this.e) {
            obj = this.e.get(str);
            if (obj == null) {
                obj = new Object();
                this.e.put(str, obj);
            }
        }
        return obj;
    }

    public abstract f m() throws IOException;

    public synchronized void n(String str) throws IOException {
        synchronized (k(str)) {
            this.d = str;
            b(2);
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:36:0x008a A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:37:0x008b  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final boolean o(com.facebook.soloader.f r11, int r12, byte[] r13) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 230
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.soloader.h.o(com.facebook.soloader.f, int, byte[]):boolean");
    }

    /* JADX WARN: Removed duplicated region for block: B:11:0x0034 A[Catch: all -> 0x002d, TRY_ENTER, TryCatch #5 {all -> 0x002d, blocks: (B:4:0x0028, B:11:0x0034, B:12:0x003b, B:13:0x0045, B:15:0x004b, B:40:0x0091, B:18:0x0053, B:20:0x0058, B:22:0x0066, B:25:0x0077, B:29:0x007e), top: B:52:0x0028 }] */
    /* JADX WARN: Removed duplicated region for block: B:15:0x004b A[Catch: all -> 0x002d, TRY_LEAVE, TryCatch #5 {all -> 0x002d, blocks: (B:4:0x0028, B:11:0x0034, B:12:0x003b, B:13:0x0045, B:15:0x004b, B:40:0x0091, B:18:0x0053, B:20:0x0058, B:22:0x0066, B:25:0x0077, B:29:0x007e), top: B:52:0x0028 }] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void p(byte r9, com.facebook.soloader.h.c r10, com.facebook.soloader.h.e r11) throws java.io.IOException {
        /*
            r8 = this;
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "regenerating DSO store "
            r0.append(r1)
            java.lang.Class r1 = r8.getClass()
            java.lang.String r1 = r1.getName()
            r0.append(r1)
            java.io.File r0 = new java.io.File
            java.io.File r1 = r8.a
            java.lang.String r2 = "dso_manifest"
            r0.<init>(r1, r2)
            java.io.RandomAccessFile r1 = new java.io.RandomAccessFile
            java.lang.String r2 = "rw"
            r1.<init>(r0, r2)
            r0 = 1
            if (r9 != r0) goto L30
            com.facebook.soloader.h$c r9 = com.facebook.soloader.h.c.a(r1)     // Catch: java.lang.Throwable -> L2d java.lang.Exception -> L30
            goto L31
        L2d:
            r9 = move-exception
            goto Lae
        L30:
            r9 = 0
        L31:
            r2 = 0
            if (r9 != 0) goto L3b
            com.facebook.soloader.h$c r9 = new com.facebook.soloader.h$c     // Catch: java.lang.Throwable -> L2d
            com.facebook.soloader.h$b[] r3 = new com.facebook.soloader.h.b[r2]     // Catch: java.lang.Throwable -> L2d
            r9.<init>(r3)     // Catch: java.lang.Throwable -> L2d
        L3b:
            com.facebook.soloader.h$b[] r10 = r10.a     // Catch: java.lang.Throwable -> L2d
            r8.g(r10)     // Catch: java.lang.Throwable -> L2d
            r10 = 32768(0x8000, float:4.5918E-41)
            byte[] r10 = new byte[r10]     // Catch: java.lang.Throwable -> L2d
        L45:
            boolean r3 = r11.a()     // Catch: java.lang.Throwable -> L2d
            if (r3 == 0) goto L95
            com.facebook.soloader.h$d r3 = r11.b()     // Catch: java.lang.Throwable -> L2d
            r4 = r0
            r5 = r2
        L51:
            if (r4 == 0) goto L7c
            com.facebook.soloader.h$b[] r6 = r9.a     // Catch: java.lang.Throwable -> L7a
            int r7 = r6.length     // Catch: java.lang.Throwable -> L7a
            if (r5 >= r7) goto L7c
            r6 = r6[r5]     // Catch: java.lang.Throwable -> L7a
            java.lang.String r6 = r6.a     // Catch: java.lang.Throwable -> L7a
            com.facebook.soloader.h$b r7 = r3.a     // Catch: java.lang.Throwable -> L7a
            java.lang.String r7 = r7.a     // Catch: java.lang.Throwable -> L7a
            boolean r6 = r6.equals(r7)     // Catch: java.lang.Throwable -> L7a
            if (r6 == 0) goto L77
            com.facebook.soloader.h$b[] r6 = r9.a     // Catch: java.lang.Throwable -> L7a
            r6 = r6[r5]     // Catch: java.lang.Throwable -> L7a
            java.lang.String r6 = r6.f0     // Catch: java.lang.Throwable -> L7a
            com.facebook.soloader.h$b r7 = r3.a     // Catch: java.lang.Throwable -> L7a
            java.lang.String r7 = r7.f0     // Catch: java.lang.Throwable -> L7a
            boolean r6 = r6.equals(r7)     // Catch: java.lang.Throwable -> L7a
            if (r6 == 0) goto L77
            r4 = r2
        L77:
            int r5 = r5 + 1
            goto L51
        L7a:
            r9 = move-exception
            goto L82
        L7c:
            if (r4 == 0) goto L8f
            r8.h(r3, r10)     // Catch: java.lang.Throwable -> L7a
            goto L8f
        L82:
            throw r9     // Catch: java.lang.Throwable -> L83
        L83:
            r10 = move-exception
            if (r3 == 0) goto L8e
            r3.close()     // Catch: java.lang.Throwable -> L8a
            goto L8e
        L8a:
            r11 = move-exception
            r9.addSuppressed(r11)     // Catch: java.lang.Throwable -> L2d
        L8e:
            throw r10     // Catch: java.lang.Throwable -> L2d
        L8f:
            if (r3 == 0) goto L45
            r3.close()     // Catch: java.lang.Throwable -> L2d
            goto L45
        L95:
            r1.close()
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r10 = "Finished regenerating DSO store "
            r9.append(r10)
            java.lang.Class r10 = r8.getClass()
            java.lang.String r10 = r10.getName()
            r9.append(r10)
            return
        Lae:
            throw r9     // Catch: java.lang.Throwable -> Laf
        Laf:
            r10 = move-exception
            r1.close()     // Catch: java.lang.Throwable -> Lb4
            goto Lb8
        Lb4:
            r11 = move-exception
            r9.addSuppressed(r11)
        Lb8:
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.soloader.h.p(byte, com.facebook.soloader.h$c, com.facebook.soloader.h$e):void");
    }

    public void q(String[] strArr) {
    }
}
