package com.yariksoffice.lingver;

import android.app.Application;
import android.content.res.Configuration;
import kotlin.jvm.internal.Lambda;

/* compiled from: Lingver.kt */
/* loaded from: classes2.dex */
public final class Lingver$initialize$2 extends Lambda implements tc1<Configuration, te4> {
    public final /* synthetic */ Application $application;
    public final /* synthetic */ Lingver this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public Lingver$initialize$2(Lingver lingver, Application application) {
        super(1);
        this.this$0 = lingver;
        this.$application = application;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(Configuration configuration) {
        invoke2(configuration);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Configuration configuration) {
        fs1.g(configuration, "it");
        this.this$0.j(this.$application, configuration);
    }
}
