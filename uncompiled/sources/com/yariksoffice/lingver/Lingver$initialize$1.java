package com.yariksoffice.lingver;

import android.app.Activity;
import kotlin.jvm.internal.Lambda;

/* compiled from: Lingver.kt */
/* loaded from: classes2.dex */
public final class Lingver$initialize$1 extends Lambda implements tc1<Activity, te4> {
    public final /* synthetic */ Lingver this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public Lingver$initialize$1(Lingver lingver) {
        super(1);
        this.this$0 = lingver;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(Activity activity) {
        invoke2(activity);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Activity activity) {
        fs1.g(activity, "it");
        this.this$0.e(activity);
    }
}
