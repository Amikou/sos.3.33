package com.yariksoffice.lingver;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import java.util.Locale;

/* compiled from: Lingver.kt */
/* loaded from: classes2.dex */
public final class Lingver {
    @SuppressLint({"ConstantLocale"})
    public static final Locale d;
    public static Lingver e;
    public static final a f = new a(null);
    public Locale a;
    public final g12 b;
    public final gf4 c;

    /* compiled from: Lingver.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public final Lingver b() {
            if (Lingver.e != null) {
                Lingver lingver = Lingver.e;
                if (lingver == null) {
                    fs1.r("instance");
                }
                return lingver;
            }
            throw new IllegalStateException("Lingver should be initialized first".toString());
        }

        public final Lingver c(Application application, g12 g12Var) {
            fs1.g(application, "application");
            fs1.g(g12Var, "store");
            if (Lingver.e == null) {
                Lingver lingver = new Lingver(g12Var, new gf4(), null);
                lingver.h(application);
                Lingver.e = lingver;
                return lingver;
            }
            throw new IllegalStateException("Already initialized".toString());
        }

        public final Lingver d(Application application, String str) {
            fs1.g(application, "application");
            fs1.g(str, "defaultLanguage");
            return e(application, new Locale(str));
        }

        public final Lingver e(Application application, Locale locale) {
            fs1.g(application, "application");
            fs1.g(locale, "defaultLocale");
            return c(application, new ku2(application, locale, null, 4, null));
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    static {
        Locale locale = Locale.getDefault();
        fs1.c(locale, "Locale.getDefault()");
        d = locale;
    }

    public Lingver(g12 g12Var, gf4 gf4Var) {
        this.b = g12Var;
        this.c = gf4Var;
        this.a = d;
    }

    public static final Lingver g(Application application, String str) {
        return f.d(application, str);
    }

    public static /* synthetic */ void m(Lingver lingver, Context context, String str, String str2, String str3, int i, Object obj) {
        if ((i & 4) != 0) {
            str2 = "";
        }
        if ((i & 8) != 0) {
            str3 = "";
        }
        lingver.k(context, str, str2, str3);
    }

    public final void e(Activity activity) {
        f(activity);
        m11.c(activity);
    }

    public final void f(Context context) {
        this.c.a(context, this.b.d());
    }

    public final void h(Application application) {
        Locale d2;
        fs1.g(application, "application");
        application.registerActivityLifecycleCallbacks(new c02(new Lingver$initialize$1(this)));
        application.registerComponentCallbacks(new d02(new Lingver$initialize$2(this, application)));
        if (this.b.a()) {
            d2 = this.a;
        } else {
            d2 = this.b.d();
        }
        i(application, d2);
    }

    public final void i(Context context, Locale locale) {
        this.b.c(locale);
        this.c.a(context, locale);
    }

    public final void j(Context context, Configuration configuration) {
        this.a = m11.a(configuration);
        if (this.b.a()) {
            i(context, this.a);
        } else {
            f(context);
        }
    }

    public final void k(Context context, String str, String str2, String str3) {
        fs1.g(context, "context");
        fs1.g(str, "language");
        fs1.g(str2, "country");
        fs1.g(str3, "variant");
        l(context, new Locale(str, str2, str3));
    }

    public final void l(Context context, Locale locale) {
        fs1.g(context, "context");
        fs1.g(locale, "locale");
        this.b.b(false);
        i(context, locale);
    }

    public /* synthetic */ Lingver(g12 g12Var, gf4 gf4Var, qi0 qi0Var) {
        this(g12Var, gf4Var);
    }
}
