package com.bumptech.glide;

import android.content.ComponentCallbacks2;
import android.content.ContentResolver;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import com.bumptech.glide.b;
import com.bumptech.glide.load.ImageHeaderParser;
import com.bumptech.glide.load.data.ParcelFileDescriptorRewinder;
import com.bumptech.glide.load.data.k;
import com.bumptech.glide.load.resource.bitmap.DefaultImageHeaderParser;
import com.bumptech.glide.load.resource.bitmap.VideoDecoder;
import com.bumptech.glide.load.resource.bitmap.h;
import com.bumptech.glide.load.resource.bitmap.i;
import com.bumptech.glide.load.resource.bitmap.j;
import com.bumptech.glide.load.resource.bitmap.k;
import com.bumptech.glide.load.resource.bitmap.m;
import defpackage.a83;
import defpackage.b72;
import defpackage.c72;
import defpackage.e72;
import defpackage.il1;
import defpackage.mi;
import defpackage.n31;
import defpackage.ns;
import defpackage.nu3;
import defpackage.oe0;
import defpackage.pf4;
import defpackage.qs;
import defpackage.ss;
import defpackage.sw2;
import defpackage.tf4;
import defpackage.ve4;
import defpackage.vf4;
import java.io.File;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* compiled from: Glide.java */
/* loaded from: classes.dex */
public class a implements ComponentCallbacks2 {
    public static volatile a m0;
    public static volatile boolean n0;
    public final jq a;
    public final k72 f0;
    public final c g0;
    public final Registry h0;
    public final sh i0;
    public final l73 j0;
    public final y50 k0;
    public final List<k73> l0 = new ArrayList();

    /* compiled from: Glide.java */
    /* renamed from: com.bumptech.glide.a$a  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public interface InterfaceC0074a {
        n73 build();
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r17v2, types: [com.bumptech.glide.load.resource.bitmap.d] */
    public a(Context context, com.bumptech.glide.load.engine.f fVar, k72 k72Var, jq jqVar, sh shVar, l73 l73Var, y50 y50Var, int i, InterfaceC0074a interfaceC0074a, Map<Class<?>, f<?, ?>> map, List<j73<Object>> list, d dVar) {
        Object obj;
        com.bumptech.glide.load.b kVar;
        com.bumptech.glide.load.resource.bitmap.c cVar;
        int i2;
        MemoryCategory memoryCategory = MemoryCategory.NORMAL;
        this.a = jqVar;
        this.i0 = shVar;
        this.f0 = k72Var;
        this.j0 = l73Var;
        this.k0 = y50Var;
        Resources resources = context.getResources();
        Registry registry = new Registry();
        this.h0 = registry;
        registry.o(new DefaultImageHeaderParser());
        int i3 = Build.VERSION.SDK_INT;
        if (i3 >= 27) {
            registry.o(new com.bumptech.glide.load.resource.bitmap.f());
        }
        List<ImageHeaderParser> g = registry.g();
        rs rsVar = new rs(context, g, jqVar, shVar);
        com.bumptech.glide.load.b<ParcelFileDescriptor, Bitmap> h = VideoDecoder.h(jqVar);
        com.bumptech.glide.load.resource.bitmap.e eVar = new com.bumptech.glide.load.resource.bitmap.e(registry.g(), resources.getDisplayMetrics(), jqVar, shVar);
        if (i3 >= 28 && dVar.a(b.c.class)) {
            kVar = new h();
            cVar = new com.bumptech.glide.load.resource.bitmap.d();
            obj = String.class;
        } else {
            com.bumptech.glide.load.resource.bitmap.c cVar2 = new com.bumptech.glide.load.resource.bitmap.c(eVar);
            obj = String.class;
            kVar = new k(eVar, shVar);
            cVar = cVar2;
        }
        if (i3 < 28 || !dVar.a(b.C0075b.class)) {
            i2 = i3;
        } else {
            i2 = i3;
            registry.e("Animation", InputStream.class, Drawable.class, ce.f(g, shVar));
            registry.e("Animation", ByteBuffer.class, Drawable.class, ce.a(g, shVar));
        }
        x73 x73Var = new x73(context);
        a83.c cVar3 = new a83.c(resources);
        a83.d dVar2 = new a83.d(resources);
        a83.b bVar = new a83.b(resources);
        a83.a aVar = new a83.a(resources);
        wp wpVar = new wp(shVar);
        qp qpVar = new qp();
        vf1 vf1Var = new vf1();
        ContentResolver contentResolver = context.getContentResolver();
        registry.a(ByteBuffer.class, new ps()).a(InputStream.class, new bu3(shVar)).e("Bitmap", ByteBuffer.class, Bitmap.class, cVar).e("Bitmap", InputStream.class, Bitmap.class, kVar);
        if (ParcelFileDescriptorRewinder.c()) {
            registry.e("Bitmap", ParcelFileDescriptor.class, Bitmap.class, new i(eVar));
        }
        registry.e("Bitmap", ParcelFileDescriptor.class, Bitmap.class, h).e("Bitmap", AssetFileDescriptor.class, Bitmap.class, VideoDecoder.c(jqVar)).c(Bitmap.class, Bitmap.class, ve4.a.b()).e("Bitmap", Bitmap.class, Bitmap.class, new m()).b(Bitmap.class, wpVar).e("BitmapDrawable", ByteBuffer.class, BitmapDrawable.class, new com.bumptech.glide.load.resource.bitmap.a(resources, cVar)).e("BitmapDrawable", InputStream.class, BitmapDrawable.class, new com.bumptech.glide.load.resource.bitmap.a(resources, kVar)).e("BitmapDrawable", ParcelFileDescriptor.class, BitmapDrawable.class, new com.bumptech.glide.load.resource.bitmap.a(resources, h)).b(BitmapDrawable.class, new up(jqVar, wpVar)).e("Animation", InputStream.class, uf1.class, new du3(g, rsVar, shVar)).e("Animation", ByteBuffer.class, uf1.class, rsVar).b(uf1.class, new wf1()).c(tf1.class, tf1.class, ve4.a.b()).e("Bitmap", tf1.class, Bitmap.class, new bg1(jqVar)).d(Uri.class, Drawable.class, x73Var).d(Uri.class, Bitmap.class, new j(x73Var, jqVar)).p(new ss.a()).c(File.class, ByteBuffer.class, new qs.b()).c(File.class, InputStream.class, new n31.e()).d(File.class, File.class, new m31()).c(File.class, ParcelFileDescriptor.class, new n31.b()).c(File.class, File.class, ve4.a.b()).p(new k.a(shVar));
        if (ParcelFileDescriptorRewinder.c()) {
            registry.p(new ParcelFileDescriptorRewinder.a());
        }
        Class cls = Integer.TYPE;
        Object obj2 = obj;
        registry.c(cls, InputStream.class, cVar3).c(cls, ParcelFileDescriptor.class, bVar).c(Integer.class, InputStream.class, cVar3).c(Integer.class, ParcelFileDescriptor.class, bVar).c(Integer.class, Uri.class, dVar2).c(cls, AssetFileDescriptor.class, aVar).c(Integer.class, AssetFileDescriptor.class, aVar).c(cls, Uri.class, dVar2).c(obj2, InputStream.class, new oe0.c()).c(Uri.class, InputStream.class, new oe0.c()).c(obj2, InputStream.class, new nu3.c()).c(obj2, ParcelFileDescriptor.class, new nu3.b()).c(obj2, AssetFileDescriptor.class, new nu3.a()).c(Uri.class, InputStream.class, new mi.c(context.getAssets())).c(Uri.class, AssetFileDescriptor.class, new mi.b(context.getAssets())).c(Uri.class, InputStream.class, new c72.a(context)).c(Uri.class, InputStream.class, new e72.a(context));
        int i4 = i2;
        if (i4 >= 29) {
            registry.c(Uri.class, InputStream.class, new sw2.c(context));
            registry.c(Uri.class, ParcelFileDescriptor.class, new sw2.b(context));
        }
        registry.c(Uri.class, InputStream.class, new pf4.d(contentResolver)).c(Uri.class, ParcelFileDescriptor.class, new pf4.b(contentResolver)).c(Uri.class, AssetFileDescriptor.class, new pf4.a(contentResolver)).c(Uri.class, InputStream.class, new vf4.a()).c(URL.class, InputStream.class, new tf4.a()).c(Uri.class, File.class, new b72.a(context)).c(ng1.class, InputStream.class, new il1.a()).c(byte[].class, ByteBuffer.class, new ns.a()).c(byte[].class, InputStream.class, new ns.d()).c(Uri.class, Uri.class, ve4.a.b()).c(Drawable.class, Drawable.class, ve4.a.b()).d(Drawable.class, Drawable.class, new ue4()).q(Bitmap.class, BitmapDrawable.class, new vp(resources)).q(Bitmap.class, byte[].class, qpVar).q(Drawable.class, byte[].class, new rq0(jqVar, qpVar, vf1Var)).q(uf1.class, byte[].class, vf1Var);
        if (i4 >= 23) {
            com.bumptech.glide.load.b<ByteBuffer, Bitmap> d = VideoDecoder.d(jqVar);
            registry.d(ByteBuffer.class, Bitmap.class, d);
            registry.d(ByteBuffer.class, BitmapDrawable.class, new com.bumptech.glide.load.resource.bitmap.a(resources, d));
        }
        this.g0 = new c(context, shVar, registry, new fp1(), interfaceC0074a, map, list, fVar, dVar, i);
    }

    public static void a(Context context, GeneratedAppGlideModule generatedAppGlideModule) {
        if (!n0) {
            n0 = true;
            m(context, generatedAppGlideModule);
            n0 = false;
            return;
        }
        throw new IllegalStateException("You cannot call Glide.get() in registerComponents(), use the provided Glide instance instead");
    }

    public static a c(Context context) {
        if (m0 == null) {
            GeneratedAppGlideModule d = d(context.getApplicationContext());
            synchronized (a.class) {
                if (m0 == null) {
                    a(context, d);
                }
            }
        }
        return m0;
    }

    public static GeneratedAppGlideModule d(Context context) {
        try {
            return (GeneratedAppGlideModule) GeneratedAppGlideModuleImpl.class.getDeclaredConstructor(Context.class).newInstance(context.getApplicationContext());
        } catch (ClassNotFoundException unused) {
            return null;
        } catch (IllegalAccessException e) {
            q(e);
            return null;
        } catch (InstantiationException e2) {
            q(e2);
            return null;
        } catch (NoSuchMethodException e3) {
            q(e3);
            return null;
        } catch (InvocationTargetException e4) {
            q(e4);
            return null;
        }
    }

    public static l73 l(Context context) {
        wt2.e(context, "You cannot start a load on a not yet attached View or a Fragment where getActivity() returns null (which usually occurs when getActivity() is called before the Fragment is attached or after the Fragment is destroyed).");
        return c(context).k();
    }

    public static void m(Context context, GeneratedAppGlideModule generatedAppGlideModule) {
        n(context, new b(), generatedAppGlideModule);
    }

    public static void n(Context context, b bVar, GeneratedAppGlideModule generatedAppGlideModule) {
        Context applicationContext = context.getApplicationContext();
        List<jg1> emptyList = Collections.emptyList();
        if (generatedAppGlideModule == null || generatedAppGlideModule.c()) {
            emptyList = new p32(applicationContext).a();
        }
        if (generatedAppGlideModule != null && !generatedAppGlideModule.d().isEmpty()) {
            Set<Class<?>> d = generatedAppGlideModule.d();
            Iterator<jg1> it = emptyList.iterator();
            while (it.hasNext()) {
                jg1 next = it.next();
                if (d.contains(next.getClass())) {
                    if (Log.isLoggable("Glide", 3)) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("AppGlideModule excludes manifest GlideModule: ");
                        sb.append(next);
                    }
                    it.remove();
                }
            }
        }
        if (Log.isLoggable("Glide", 3)) {
            for (jg1 jg1Var : emptyList) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Discovered GlideModule from manifest: ");
                sb2.append(jg1Var.getClass());
            }
        }
        bVar.b(generatedAppGlideModule != null ? generatedAppGlideModule.e() : null);
        for (jg1 jg1Var2 : emptyList) {
            jg1Var2.a(applicationContext, bVar);
        }
        if (generatedAppGlideModule != null) {
            generatedAppGlideModule.a(applicationContext, bVar);
        }
        a a = bVar.a(applicationContext);
        for (jg1 jg1Var3 : emptyList) {
            try {
                jg1Var3.b(applicationContext, a, a.h0);
            } catch (AbstractMethodError e) {
                throw new IllegalStateException("Attempting to register a Glide v3 module. If you see this, you or one of your dependencies may be including Glide v3 even though you're using Glide v4. You'll need to find and remove (or update) the offending dependency. The v3 module name is: " + jg1Var3.getClass().getName(), e);
            }
        }
        if (generatedAppGlideModule != null) {
            generatedAppGlideModule.b(applicationContext, a, a.h0);
        }
        applicationContext.registerComponentCallbacks(a);
        m0 = a;
    }

    public static void q(Exception exc) {
        throw new IllegalStateException("GeneratedAppGlideModuleImpl is implemented incorrectly. If you've manually implemented this class, remove your implementation. The Annotation processor will generate a correct implementation.", exc);
    }

    public static k73 t(Context context) {
        return l(context).l(context);
    }

    public static k73 u(View view) {
        return l(view.getContext()).m(view);
    }

    public static k73 v(Fragment fragment) {
        return l(fragment.getContext()).n(fragment);
    }

    public static k73 w(FragmentActivity fragmentActivity) {
        return l(fragmentActivity).o(fragmentActivity);
    }

    public void b() {
        mg4.b();
        this.f0.b();
        this.a.b();
        this.i0.b();
    }

    public sh e() {
        return this.i0;
    }

    public jq f() {
        return this.a;
    }

    public y50 g() {
        return this.k0;
    }

    public Context h() {
        return this.g0.getBaseContext();
    }

    public c i() {
        return this.g0;
    }

    public Registry j() {
        return this.h0;
    }

    public l73 k() {
        return this.j0;
    }

    public void o(k73 k73Var) {
        synchronized (this.l0) {
            if (!this.l0.contains(k73Var)) {
                this.l0.add(k73Var);
            } else {
                throw new IllegalStateException("Cannot register already registered manager");
            }
        }
    }

    @Override // android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
    }

    @Override // android.content.ComponentCallbacks
    public void onLowMemory() {
        b();
    }

    @Override // android.content.ComponentCallbacks2
    public void onTrimMemory(int i) {
        r(i);
    }

    public boolean p(i34<?> i34Var) {
        synchronized (this.l0) {
            for (k73 k73Var : this.l0) {
                if (k73Var.G(i34Var)) {
                    return true;
                }
            }
            return false;
        }
    }

    public void r(int i) {
        mg4.b();
        synchronized (this.l0) {
            for (k73 k73Var : this.l0) {
                k73Var.onTrimMemory(i);
            }
        }
        this.f0.a(i);
        this.a.a(i);
        this.i0.a(i);
    }

    public void s(k73 k73Var) {
        synchronized (this.l0) {
            if (this.l0.contains(k73Var)) {
                this.l0.remove(k73Var);
            } else {
                throw new IllegalStateException("Cannot unregister not yet registered manager");
            }
        }
    }
}
