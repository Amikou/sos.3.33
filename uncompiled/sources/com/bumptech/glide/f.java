package com.bumptech.glide;

import com.bumptech.glide.f;

/* compiled from: TransitionOptions.java */
/* loaded from: classes.dex */
public abstract class f<CHILD extends f<CHILD, TranscodeType>, TranscodeType> implements Cloneable {
    public ib4<? super TranscodeType> a = sg2.b();

    /* renamed from: a */
    public final CHILD clone() {
        try {
            return (CHILD) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    public final ib4<? super TranscodeType> b() {
        return this.a;
    }
}
