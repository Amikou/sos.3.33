package com.bumptech.glide;

import android.content.Context;
import java.util.Collections;
import java.util.Set;
import net.safemoon.androidwallet.utils.svg.SvgModule;

/* JADX INFO: Access modifiers changed from: package-private */
/* loaded from: classes.dex */
public final class GeneratedAppGlideModuleImpl extends GeneratedAppGlideModule {
    public final SvgModule a = new SvgModule();

    public GeneratedAppGlideModuleImpl(Context context) {
    }

    @Override // defpackage.pf, defpackage.eh
    public void a(Context context, b bVar) {
        this.a.a(context, bVar);
    }

    @Override // defpackage.gz1, defpackage.h63
    public void b(Context context, a aVar, Registry registry) {
        new com.bumptech.glide.integration.okhttp3.a().b(context, aVar, registry);
        this.a.b(context, aVar, registry);
    }

    @Override // defpackage.pf
    public boolean c() {
        return this.a.c();
    }

    @Override // com.bumptech.glide.GeneratedAppGlideModule
    public Set<Class<?>> d() {
        return Collections.emptySet();
    }

    @Override // com.bumptech.glide.GeneratedAppGlideModule
    /* renamed from: f */
    public te1 e() {
        return new te1();
    }
}
