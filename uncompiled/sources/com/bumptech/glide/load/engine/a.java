package com.bumptech.glide.load.engine;

import android.os.Process;
import com.bumptech.glide.load.engine.h;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

/* compiled from: ActiveResources.java */
/* loaded from: classes.dex */
public final class a {
    public final boolean a;
    public final Executor b;
    public final Map<fx1, d> c;
    public final ReferenceQueue<h<?>> d;
    public h.a e;
    public volatile boolean f;
    public volatile c g;

    /* compiled from: ActiveResources.java */
    /* renamed from: com.bumptech.glide.load.engine.a$a  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public class ThreadFactoryC0077a implements ThreadFactory {

        /* compiled from: ActiveResources.java */
        /* renamed from: com.bumptech.glide.load.engine.a$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public class RunnableC0078a implements Runnable {
            public final /* synthetic */ Runnable a;

            public RunnableC0078a(ThreadFactoryC0077a threadFactoryC0077a, Runnable runnable) {
                this.a = runnable;
            }

            @Override // java.lang.Runnable
            public void run() {
                Process.setThreadPriority(10);
                this.a.run();
            }
        }

        @Override // java.util.concurrent.ThreadFactory
        public Thread newThread(Runnable runnable) {
            return new Thread(new RunnableC0078a(this, runnable), "glide-active-resources");
        }
    }

    /* compiled from: ActiveResources.java */
    /* loaded from: classes.dex */
    public class b implements Runnable {
        public b() {
        }

        @Override // java.lang.Runnable
        public void run() {
            a.this.b();
        }
    }

    /* compiled from: ActiveResources.java */
    /* loaded from: classes.dex */
    public interface c {
        void a();
    }

    /* compiled from: ActiveResources.java */
    /* loaded from: classes.dex */
    public static final class d extends WeakReference<h<?>> {
        public final fx1 a;
        public final boolean b;
        public s73<?> c;

        public d(fx1 fx1Var, h<?> hVar, ReferenceQueue<? super h<?>> referenceQueue, boolean z) {
            super(hVar, referenceQueue);
            this.a = (fx1) wt2.d(fx1Var);
            this.c = (hVar.f() && z) ? (s73) wt2.d(hVar.e()) : null;
            this.b = hVar.f();
        }

        public void a() {
            this.c = null;
            clear();
        }
    }

    public a(boolean z) {
        this(z, Executors.newSingleThreadExecutor(new ThreadFactoryC0077a()));
    }

    public synchronized void a(fx1 fx1Var, h<?> hVar) {
        d put = this.c.put(fx1Var, new d(fx1Var, hVar, this.d, this.a));
        if (put != null) {
            put.a();
        }
    }

    public void b() {
        while (!this.f) {
            try {
                c((d) this.d.remove());
                c cVar = this.g;
                if (cVar != null) {
                    cVar.a();
                }
            } catch (InterruptedException unused) {
                Thread.currentThread().interrupt();
            }
        }
    }

    public void c(d dVar) {
        s73<?> s73Var;
        synchronized (this) {
            this.c.remove(dVar.a);
            if (dVar.b && (s73Var = dVar.c) != null) {
                this.e.b(dVar.a, new h<>(s73Var, true, false, dVar.a, this.e));
            }
        }
    }

    public synchronized void d(fx1 fx1Var) {
        d remove = this.c.remove(fx1Var);
        if (remove != null) {
            remove.a();
        }
    }

    public synchronized h<?> e(fx1 fx1Var) {
        d dVar = this.c.get(fx1Var);
        if (dVar == null) {
            return null;
        }
        h<?> hVar = dVar.get();
        if (hVar == null) {
            c(dVar);
        }
        return hVar;
    }

    public void f(h.a aVar) {
        synchronized (aVar) {
            synchronized (this) {
                this.e = aVar;
            }
        }
    }

    public a(boolean z, Executor executor) {
        this.c = new HashMap();
        this.d = new ReferenceQueue<>();
        this.a = z;
        this.b = executor;
        executor.execute(new b());
    }
}
