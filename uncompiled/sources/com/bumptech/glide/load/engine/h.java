package com.bumptech.glide.load.engine;

/* compiled from: EngineResource.java */
/* loaded from: classes.dex */
public class h<Z> implements s73<Z> {
    public final boolean a;
    public final boolean f0;
    public final s73<Z> g0;
    public final a h0;
    public final fx1 i0;
    public int j0;
    public boolean k0;

    /* compiled from: EngineResource.java */
    /* loaded from: classes.dex */
    public interface a {
        void b(fx1 fx1Var, h<?> hVar);
    }

    public h(s73<Z> s73Var, boolean z, boolean z2, fx1 fx1Var, a aVar) {
        this.g0 = (s73) wt2.d(s73Var);
        this.a = z;
        this.f0 = z2;
        this.i0 = fx1Var;
        this.h0 = (a) wt2.d(aVar);
    }

    @Override // defpackage.s73
    public int a() {
        return this.g0.a();
    }

    @Override // defpackage.s73
    public synchronized void b() {
        if (this.j0 <= 0) {
            if (!this.k0) {
                this.k0 = true;
                if (this.f0) {
                    this.g0.b();
                }
            } else {
                throw new IllegalStateException("Cannot recycle a resource that has already been recycled");
            }
        } else {
            throw new IllegalStateException("Cannot recycle a resource while it is still acquired");
        }
    }

    public synchronized void c() {
        if (!this.k0) {
            this.j0++;
        } else {
            throw new IllegalStateException("Cannot acquire a recycled resource");
        }
    }

    @Override // defpackage.s73
    public Class<Z> d() {
        return this.g0.d();
    }

    public s73<Z> e() {
        return this.g0;
    }

    public boolean f() {
        return this.a;
    }

    public void g() {
        boolean z;
        synchronized (this) {
            int i = this.j0;
            if (i > 0) {
                z = true;
                int i2 = i - 1;
                this.j0 = i2;
                if (i2 != 0) {
                    z = false;
                }
            } else {
                throw new IllegalStateException("Cannot release a recycled or not yet acquired resource");
            }
        }
        if (z) {
            this.h0.b(this.i0, this);
        }
    }

    @Override // defpackage.s73
    public Z get() {
        return this.g0.get();
    }

    public synchronized String toString() {
        return "EngineResource{isMemoryCacheable=" + this.a + ", listener=" + this.h0 + ", key=" + this.i0 + ", acquired=" + this.j0 + ", isRecycled=" + this.k0 + ", resource=" + this.g0 + '}';
    }
}
