package com.bumptech.glide.load.engine;

import android.util.Log;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.data.d;
import com.bumptech.glide.load.engine.c;
import defpackage.j92;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

/* compiled from: SourceGenerator.java */
/* loaded from: classes.dex */
public class k implements c, c.a {
    public final d<?> a;
    public final c.a f0;
    public volatile int g0;
    public volatile b h0;
    public volatile Object i0;
    public volatile j92.a<?> j0;
    public volatile zd0 k0;

    /* compiled from: SourceGenerator.java */
    /* loaded from: classes.dex */
    public class a implements d.a<Object> {
        public final /* synthetic */ j92.a a;

        public a(j92.a aVar) {
            this.a = aVar;
        }

        @Override // com.bumptech.glide.load.data.d.a
        public void c(Exception exc) {
            if (k.this.g(this.a)) {
                k.this.i(this.a, exc);
            }
        }

        @Override // com.bumptech.glide.load.data.d.a
        public void f(Object obj) {
            if (k.this.g(this.a)) {
                k.this.h(this.a, obj);
            }
        }
    }

    public k(d<?> dVar, c.a aVar) {
        this.a = dVar;
        this.f0 = aVar;
    }

    @Override // com.bumptech.glide.load.engine.c
    public boolean a() {
        if (this.i0 != null) {
            Object obj = this.i0;
            this.i0 = null;
            try {
                if (!b(obj)) {
                    return true;
                }
            } catch (IOException unused) {
            }
        }
        if (this.h0 == null || !this.h0.a()) {
            this.h0 = null;
            this.j0 = null;
            boolean z = false;
            while (!z && c()) {
                List<j92.a<?>> g = this.a.g();
                int i = this.g0;
                this.g0 = i + 1;
                this.j0 = g.get(i);
                if (this.j0 != null && (this.a.e().c(this.j0.c.d()) || this.a.u(this.j0.c.a()))) {
                    j(this.j0);
                    z = true;
                }
            }
            return z;
        }
        return true;
    }

    public final boolean b(Object obj) throws IOException {
        long b = t12.b();
        boolean z = true;
        try {
            com.bumptech.glide.load.data.e<T> o = this.a.o(obj);
            Object a2 = o.a();
            ev0<X> q = this.a.q(a2);
            ae0 ae0Var = new ae0(q, a2, this.a.k());
            zd0 zd0Var = new zd0(this.j0.a, this.a.p());
            yo0 d = this.a.d();
            d.b(zd0Var, ae0Var);
            if (Log.isLoggable("SourceGenerator", 2)) {
                StringBuilder sb = new StringBuilder();
                sb.append("Finished encoding source to cache, key: ");
                sb.append(zd0Var);
                sb.append(", data: ");
                sb.append(obj);
                sb.append(", encoder: ");
                sb.append(q);
                sb.append(", duration: ");
                sb.append(t12.a(b));
            }
            if (d.a(zd0Var) != null) {
                this.k0 = zd0Var;
                this.h0 = new b(Collections.singletonList(this.j0.a), this.a, this);
                this.j0.c.b();
                return true;
            }
            if (Log.isLoggable("SourceGenerator", 3)) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Attempt to write: ");
                sb2.append(this.k0);
                sb2.append(", data: ");
                sb2.append(obj);
                sb2.append(" to the disk cache failed, maybe the disk cache is disabled? Trying to decode the data directly...");
            }
            try {
                this.f0.e(this.j0.a, o.a(), this.j0.c, this.j0.c.d(), this.j0.a);
                return false;
            } catch (Throwable th) {
                th = th;
                if (!z) {
                    this.j0.c.b();
                }
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            z = false;
        }
    }

    public final boolean c() {
        return this.g0 < this.a.g().size();
    }

    @Override // com.bumptech.glide.load.engine.c
    public void cancel() {
        j92.a<?> aVar = this.j0;
        if (aVar != null) {
            aVar.c.cancel();
        }
    }

    @Override // com.bumptech.glide.load.engine.c.a
    public void d(fx1 fx1Var, Exception exc, com.bumptech.glide.load.data.d<?> dVar, DataSource dataSource) {
        this.f0.d(fx1Var, exc, dVar, this.j0.c.d());
    }

    @Override // com.bumptech.glide.load.engine.c.a
    public void e(fx1 fx1Var, Object obj, com.bumptech.glide.load.data.d<?> dVar, DataSource dataSource, fx1 fx1Var2) {
        this.f0.e(fx1Var, obj, dVar, this.j0.c.d(), fx1Var);
    }

    @Override // com.bumptech.glide.load.engine.c.a
    public void f() {
        throw new UnsupportedOperationException();
    }

    public boolean g(j92.a<?> aVar) {
        j92.a<?> aVar2 = this.j0;
        return aVar2 != null && aVar2 == aVar;
    }

    public void h(j92.a<?> aVar, Object obj) {
        bp0 e = this.a.e();
        if (obj != null && e.c(aVar.c.d())) {
            this.i0 = obj;
            this.f0.f();
            return;
        }
        c.a aVar2 = this.f0;
        fx1 fx1Var = aVar.a;
        com.bumptech.glide.load.data.d<?> dVar = aVar.c;
        aVar2.e(fx1Var, obj, dVar, dVar.d(), this.k0);
    }

    public void i(j92.a<?> aVar, Exception exc) {
        c.a aVar2 = this.f0;
        zd0 zd0Var = this.k0;
        com.bumptech.glide.load.data.d<?> dVar = aVar.c;
        aVar2.d(zd0Var, exc, dVar, dVar.d());
    }

    public final void j(j92.a<?> aVar) {
        this.j0.c.e(this.a.l(), new a(aVar));
    }
}
