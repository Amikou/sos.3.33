package com.bumptech.glide.load.engine;

import com.bumptech.glide.load.engine.e;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* compiled from: LoadPath.java */
/* loaded from: classes.dex */
public class i<Data, ResourceType, Transcode> {
    public final et2<List<Throwable>> a;
    public final List<? extends e<Data, ResourceType, Transcode>> b;
    public final String c;

    public i(Class<Data> cls, Class<ResourceType> cls2, Class<Transcode> cls3, List<e<Data, ResourceType, Transcode>> list, et2<List<Throwable>> et2Var) {
        this.a = et2Var;
        this.b = (List) wt2.c(list);
        this.c = "Failed LoadPath{" + cls.getSimpleName() + "->" + cls2.getSimpleName() + "->" + cls3.getSimpleName() + "}";
    }

    public s73<Transcode> a(com.bumptech.glide.load.data.e<Data> eVar, vn2 vn2Var, int i, int i2, e.a<ResourceType> aVar) throws GlideException {
        List<Throwable> list = (List) wt2.d(this.a.b());
        try {
            return b(eVar, vn2Var, i, i2, aVar, list);
        } finally {
            this.a.a(list);
        }
    }

    public final s73<Transcode> b(com.bumptech.glide.load.data.e<Data> eVar, vn2 vn2Var, int i, int i2, e.a<ResourceType> aVar, List<Throwable> list) throws GlideException {
        int size = this.b.size();
        s73<Transcode> s73Var = null;
        for (int i3 = 0; i3 < size; i3++) {
            try {
                s73Var = this.b.get(i3).a(eVar, i, i2, vn2Var, aVar);
            } catch (GlideException e) {
                list.add(e);
            }
            if (s73Var != null) {
                break;
            }
        }
        if (s73Var != null) {
            return s73Var;
        }
        throw new GlideException(this.c, new ArrayList(list));
    }

    public String toString() {
        return "LoadPath{decodePaths=" + Arrays.toString(this.b.toArray()) + '}';
    }
}
