package com.bumptech.glide.load.engine;

import android.util.Log;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DecodeJob;
import com.bumptech.glide.load.engine.h;
import defpackage.a21;
import defpackage.k72;
import defpackage.yo0;
import java.util.Map;
import java.util.concurrent.Executor;

/* compiled from: Engine.java */
/* loaded from: classes.dex */
public class f implements mv0, k72.a, h.a {
    public static final boolean i = Log.isLoggable("Engine", 2);
    public final du1 a;
    public final ov0 b;
    public final k72 c;
    public final b d;
    public final c83 e;
    public final c f;
    public final a g;
    public final com.bumptech.glide.load.engine.a h;

    /* compiled from: Engine.java */
    /* loaded from: classes.dex */
    public static class a {
        public final DecodeJob.e a;
        public final et2<DecodeJob<?>> b = a21.d(150, new C0079a());
        public int c;

        /* compiled from: Engine.java */
        /* renamed from: com.bumptech.glide.load.engine.f$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public class C0079a implements a21.d<DecodeJob<?>> {
            public C0079a() {
            }

            @Override // defpackage.a21.d
            /* renamed from: b */
            public DecodeJob<?> a() {
                a aVar = a.this;
                return new DecodeJob<>(aVar.a, aVar.b);
            }
        }

        public a(DecodeJob.e eVar) {
            this.a = eVar;
        }

        public <R> DecodeJob<R> a(com.bumptech.glide.c cVar, Object obj, nv0 nv0Var, fx1 fx1Var, int i, int i2, Class<?> cls, Class<R> cls2, Priority priority, bp0 bp0Var, Map<Class<?>, za4<?>> map, boolean z, boolean z2, boolean z3, vn2 vn2Var, DecodeJob.b<R> bVar) {
            DecodeJob decodeJob = (DecodeJob) wt2.d(this.b.b());
            int i3 = this.c;
            this.c = i3 + 1;
            return decodeJob.t(cVar, obj, nv0Var, fx1Var, i, i2, cls, cls2, priority, bp0Var, map, z, z2, z3, vn2Var, bVar, i3);
        }
    }

    /* compiled from: Engine.java */
    /* loaded from: classes.dex */
    public static class b {
        public final ig1 a;
        public final ig1 b;
        public final ig1 c;
        public final ig1 d;
        public final mv0 e;
        public final h.a f;
        public final et2<g<?>> g = a21.d(150, new a());

        /* compiled from: Engine.java */
        /* loaded from: classes.dex */
        public class a implements a21.d<g<?>> {
            public a() {
            }

            @Override // defpackage.a21.d
            /* renamed from: b */
            public g<?> a() {
                b bVar = b.this;
                return new g<>(bVar.a, bVar.b, bVar.c, bVar.d, bVar.e, bVar.f, bVar.g);
            }
        }

        public b(ig1 ig1Var, ig1 ig1Var2, ig1 ig1Var3, ig1 ig1Var4, mv0 mv0Var, h.a aVar) {
            this.a = ig1Var;
            this.b = ig1Var2;
            this.c = ig1Var3;
            this.d = ig1Var4;
            this.e = mv0Var;
            this.f = aVar;
        }

        public <R> g<R> a(fx1 fx1Var, boolean z, boolean z2, boolean z3, boolean z4) {
            return ((g) wt2.d(this.g.b())).l(fx1Var, z, z2, z3, z4);
        }
    }

    /* compiled from: Engine.java */
    /* loaded from: classes.dex */
    public static class c implements DecodeJob.e {
        public final yo0.a a;
        public volatile yo0 b;

        public c(yo0.a aVar) {
            this.a = aVar;
        }

        @Override // com.bumptech.glide.load.engine.DecodeJob.e
        public yo0 a() {
            if (this.b == null) {
                synchronized (this) {
                    if (this.b == null) {
                        this.b = this.a.build();
                    }
                    if (this.b == null) {
                        this.b = new zo0();
                    }
                }
            }
            return this.b;
        }
    }

    /* compiled from: Engine.java */
    /* loaded from: classes.dex */
    public class d {
        public final g<?> a;
        public final u73 b;

        public d(u73 u73Var, g<?> gVar) {
            this.b = u73Var;
            this.a = gVar;
        }

        public void a() {
            synchronized (f.this) {
                this.a.r(this.b);
            }
        }
    }

    public f(k72 k72Var, yo0.a aVar, ig1 ig1Var, ig1 ig1Var2, ig1 ig1Var3, ig1 ig1Var4, boolean z) {
        this(k72Var, aVar, ig1Var, ig1Var2, ig1Var3, ig1Var4, null, null, null, null, null, null, z);
    }

    public static void j(String str, long j, fx1 fx1Var) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(" in ");
        sb.append(t12.a(j));
        sb.append("ms, key: ");
        sb.append(fx1Var);
    }

    @Override // defpackage.mv0
    public synchronized void a(g<?> gVar, fx1 fx1Var) {
        this.a.d(fx1Var, gVar);
    }

    @Override // com.bumptech.glide.load.engine.h.a
    public void b(fx1 fx1Var, h<?> hVar) {
        this.h.d(fx1Var);
        if (hVar.f()) {
            this.c.c(fx1Var, hVar);
        } else {
            this.e.a(hVar, false);
        }
    }

    @Override // defpackage.mv0
    public synchronized void c(g<?> gVar, fx1 fx1Var, h<?> hVar) {
        if (hVar != null) {
            if (hVar.f()) {
                this.h.a(fx1Var, hVar);
            }
        }
        this.a.d(fx1Var, gVar);
    }

    @Override // defpackage.k72.a
    public void d(s73<?> s73Var) {
        this.e.a(s73Var, true);
    }

    public final h<?> e(fx1 fx1Var) {
        s73<?> e = this.c.e(fx1Var);
        if (e == null) {
            return null;
        }
        if (e instanceof h) {
            return (h) e;
        }
        return new h<>(e, true, true, fx1Var, this);
    }

    public <R> d f(com.bumptech.glide.c cVar, Object obj, fx1 fx1Var, int i2, int i3, Class<?> cls, Class<R> cls2, Priority priority, bp0 bp0Var, Map<Class<?>, za4<?>> map, boolean z, boolean z2, vn2 vn2Var, boolean z3, boolean z4, boolean z5, boolean z6, u73 u73Var, Executor executor) {
        long b2 = i ? t12.b() : 0L;
        nv0 a2 = this.b.a(obj, fx1Var, i2, i3, map, cls, cls2, vn2Var);
        synchronized (this) {
            h<?> i4 = i(a2, z3, b2);
            if (i4 == null) {
                return l(cVar, obj, fx1Var, i2, i3, cls, cls2, priority, bp0Var, map, z, z2, vn2Var, z3, z4, z5, z6, u73Var, executor, a2, b2);
            }
            u73Var.b(i4, DataSource.MEMORY_CACHE, false);
            return null;
        }
    }

    public final h<?> g(fx1 fx1Var) {
        h<?> e = this.h.e(fx1Var);
        if (e != null) {
            e.c();
        }
        return e;
    }

    public final h<?> h(fx1 fx1Var) {
        h<?> e = e(fx1Var);
        if (e != null) {
            e.c();
            this.h.a(fx1Var, e);
        }
        return e;
    }

    public final h<?> i(nv0 nv0Var, boolean z, long j) {
        if (z) {
            h<?> g = g(nv0Var);
            if (g != null) {
                if (i) {
                    j("Loaded resource from active resources", j, nv0Var);
                }
                return g;
            }
            h<?> h = h(nv0Var);
            if (h != null) {
                if (i) {
                    j("Loaded resource from cache", j, nv0Var);
                }
                return h;
            }
            return null;
        }
        return null;
    }

    public void k(s73<?> s73Var) {
        if (s73Var instanceof h) {
            ((h) s73Var).g();
            return;
        }
        throw new IllegalArgumentException("Cannot release anything but an EngineResource");
    }

    public final <R> d l(com.bumptech.glide.c cVar, Object obj, fx1 fx1Var, int i2, int i3, Class<?> cls, Class<R> cls2, Priority priority, bp0 bp0Var, Map<Class<?>, za4<?>> map, boolean z, boolean z2, vn2 vn2Var, boolean z3, boolean z4, boolean z5, boolean z6, u73 u73Var, Executor executor, nv0 nv0Var, long j) {
        g<?> a2 = this.a.a(nv0Var, z6);
        if (a2 != null) {
            a2.d(u73Var, executor);
            if (i) {
                j("Added to existing load", j, nv0Var);
            }
            return new d(u73Var, a2);
        }
        g<R> a3 = this.d.a(nv0Var, z3, z4, z5, z6);
        DecodeJob<R> a4 = this.g.a(cVar, obj, nv0Var, fx1Var, i2, i3, cls, cls2, priority, bp0Var, map, z, z2, z6, vn2Var, a3);
        this.a.c(nv0Var, a3);
        a3.d(u73Var, executor);
        a3.s(a4);
        if (i) {
            j("Started new load", j, nv0Var);
        }
        return new d(u73Var, a3);
    }

    public f(k72 k72Var, yo0.a aVar, ig1 ig1Var, ig1 ig1Var2, ig1 ig1Var3, ig1 ig1Var4, du1 du1Var, ov0 ov0Var, com.bumptech.glide.load.engine.a aVar2, b bVar, a aVar3, c83 c83Var, boolean z) {
        this.c = k72Var;
        c cVar = new c(aVar);
        this.f = cVar;
        com.bumptech.glide.load.engine.a aVar4 = aVar2 == null ? new com.bumptech.glide.load.engine.a(z) : aVar2;
        this.h = aVar4;
        aVar4.f(this);
        this.b = ov0Var == null ? new ov0() : ov0Var;
        this.a = du1Var == null ? new du1() : du1Var;
        this.d = bVar == null ? new b(ig1Var, ig1Var2, ig1Var3, ig1Var4, this, this) : bVar;
        this.g = aVar3 == null ? new a(cVar) : aVar3;
        this.e = c83Var == null ? new c83() : c83Var;
        k72Var.d(this);
    }
}
