package com.bumptech.glide.load.engine;

import com.bumptech.glide.Priority;
import com.bumptech.glide.Registry;
import com.bumptech.glide.load.engine.DecodeJob;
import defpackage.j92;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: DecodeHelper.java */
/* loaded from: classes.dex */
public final class d<Transcode> {
    public final List<j92.a<?>> a = new ArrayList();
    public final List<fx1> b = new ArrayList();
    public com.bumptech.glide.c c;
    public Object d;
    public int e;
    public int f;
    public Class<?> g;
    public DecodeJob.e h;
    public vn2 i;
    public Map<Class<?>, za4<?>> j;
    public Class<Transcode> k;
    public boolean l;
    public boolean m;
    public fx1 n;
    public Priority o;
    public bp0 p;
    public boolean q;
    public boolean r;

    public void a() {
        this.c = null;
        this.d = null;
        this.n = null;
        this.g = null;
        this.k = null;
        this.i = null;
        this.o = null;
        this.j = null;
        this.p = null;
        this.a.clear();
        this.l = false;
        this.b.clear();
        this.m = false;
    }

    public sh b() {
        return this.c.b();
    }

    public List<fx1> c() {
        if (!this.m) {
            this.m = true;
            this.b.clear();
            List<j92.a<?>> g = g();
            int size = g.size();
            for (int i = 0; i < size; i++) {
                j92.a<?> aVar = g.get(i);
                if (!this.b.contains(aVar.a)) {
                    this.b.add(aVar.a);
                }
                for (int i2 = 0; i2 < aVar.b.size(); i2++) {
                    if (!this.b.contains(aVar.b.get(i2))) {
                        this.b.add(aVar.b.get(i2));
                    }
                }
            }
        }
        return this.b;
    }

    public yo0 d() {
        return this.h.a();
    }

    public bp0 e() {
        return this.p;
    }

    public int f() {
        return this.f;
    }

    public List<j92.a<?>> g() {
        if (!this.l) {
            this.l = true;
            this.a.clear();
            List i = this.c.i().i(this.d);
            int size = i.size();
            for (int i2 = 0; i2 < size; i2++) {
                j92.a<?> b = ((j92) i.get(i2)).b(this.d, this.e, this.f, this.i);
                if (b != null) {
                    this.a.add(b);
                }
            }
        }
        return this.a;
    }

    public <Data> i<Data, ?, Transcode> h(Class<Data> cls) {
        return this.c.i().h(cls, this.g, this.k);
    }

    public Class<?> i() {
        return this.d.getClass();
    }

    public List<j92<File, ?>> j(File file) throws Registry.NoModelLoaderAvailableException {
        return this.c.i().i(file);
    }

    public vn2 k() {
        return this.i;
    }

    public Priority l() {
        return this.o;
    }

    public List<Class<?>> m() {
        return this.c.i().j(this.d.getClass(), this.g, this.k);
    }

    public <Z> y73<Z> n(s73<Z> s73Var) {
        return this.c.i().k(s73Var);
    }

    public <T> com.bumptech.glide.load.data.e<T> o(T t) {
        return this.c.i().l(t);
    }

    public fx1 p() {
        return this.n;
    }

    public <X> ev0<X> q(X x) throws Registry.NoSourceEncoderAvailableException {
        return this.c.i().m(x);
    }

    public Class<?> r() {
        return (Class<Transcode>) this.k;
    }

    public <Z> za4<Z> s(Class<Z> cls) {
        za4<Z> za4Var = (za4<Z>) this.j.get(cls);
        if (za4Var == null) {
            Iterator<Map.Entry<Class<?>, za4<?>>> it = this.j.entrySet().iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                Map.Entry<Class<?>, za4<?>> next = it.next();
                if (next.getKey().isAssignableFrom(cls)) {
                    za4Var = (za4<Z>) next.getValue();
                    break;
                }
            }
        }
        if (za4Var == null) {
            if (this.j.isEmpty() && this.q) {
                throw new IllegalArgumentException("Missing transformation for " + cls + ". If you wish to ignore unknown resource types, use the optional transformation methods.");
            }
            return xe4.c();
        }
        return za4Var;
    }

    public int t() {
        return this.e;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public boolean u(Class<?> cls) {
        return h(cls) != null;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public <R> void v(com.bumptech.glide.c cVar, Object obj, fx1 fx1Var, int i, int i2, bp0 bp0Var, Class<?> cls, Class<R> cls2, Priority priority, vn2 vn2Var, Map<Class<?>, za4<?>> map, boolean z, boolean z2, DecodeJob.e eVar) {
        this.c = cVar;
        this.d = obj;
        this.n = fx1Var;
        this.e = i;
        this.f = i2;
        this.p = bp0Var;
        this.g = cls;
        this.h = eVar;
        this.k = cls2;
        this.o = priority;
        this.i = vn2Var;
        this.j = map;
        this.q = z;
        this.r = z2;
    }

    public boolean w(s73<?> s73Var) {
        return this.c.i().n(s73Var);
    }

    public boolean x() {
        return this.r;
    }

    public boolean y(fx1 fx1Var) {
        List<j92.a<?>> g = g();
        int size = g.size();
        for (int i = 0; i < size; i++) {
            if (g.get(i).a.equals(fx1Var)) {
                return true;
            }
        }
        return false;
    }
}
