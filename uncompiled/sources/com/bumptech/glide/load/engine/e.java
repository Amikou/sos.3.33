package com.bumptech.glide.load.engine;

import android.util.Log;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/* compiled from: DecodePath.java */
/* loaded from: classes.dex */
public class e<DataType, ResourceType, Transcode> {
    public final Class<DataType> a;
    public final List<? extends com.bumptech.glide.load.b<DataType, ResourceType>> b;
    public final e83<ResourceType, Transcode> c;
    public final et2<List<Throwable>> d;
    public final String e;

    /* compiled from: DecodePath.java */
    /* loaded from: classes.dex */
    public interface a<ResourceType> {
        s73<ResourceType> a(s73<ResourceType> s73Var);
    }

    public e(Class<DataType> cls, Class<ResourceType> cls2, Class<Transcode> cls3, List<? extends com.bumptech.glide.load.b<DataType, ResourceType>> list, e83<ResourceType, Transcode> e83Var, et2<List<Throwable>> et2Var) {
        this.a = cls;
        this.b = list;
        this.c = e83Var;
        this.d = et2Var;
        this.e = "Failed DecodePath{" + cls.getSimpleName() + "->" + cls2.getSimpleName() + "->" + cls3.getSimpleName() + "}";
    }

    public s73<Transcode> a(com.bumptech.glide.load.data.e<DataType> eVar, int i, int i2, vn2 vn2Var, a<ResourceType> aVar) throws GlideException {
        return this.c.a(aVar.a(b(eVar, i, i2, vn2Var)), vn2Var);
    }

    public final s73<ResourceType> b(com.bumptech.glide.load.data.e<DataType> eVar, int i, int i2, vn2 vn2Var) throws GlideException {
        List<Throwable> list = (List) wt2.d(this.d.b());
        try {
            return c(eVar, i, i2, vn2Var, list);
        } finally {
            this.d.a(list);
        }
    }

    public final s73<ResourceType> c(com.bumptech.glide.load.data.e<DataType> eVar, int i, int i2, vn2 vn2Var, List<Throwable> list) throws GlideException {
        int size = this.b.size();
        s73<ResourceType> s73Var = null;
        for (int i3 = 0; i3 < size; i3++) {
            com.bumptech.glide.load.b<DataType, ResourceType> bVar = this.b.get(i3);
            try {
                if (bVar.a(eVar.a(), vn2Var)) {
                    s73Var = bVar.b(eVar.a(), i, i2, vn2Var);
                }
            } catch (IOException | OutOfMemoryError | RuntimeException e) {
                if (Log.isLoggable("DecodePath", 2)) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Failed to decode data for ");
                    sb.append(bVar);
                }
                list.add(e);
            }
            if (s73Var != null) {
                break;
            }
        }
        if (s73Var != null) {
            return s73Var;
        }
        throw new GlideException(this.e, new ArrayList(list));
    }

    public String toString() {
        return "DecodePath{ dataClass=" + this.a + ", decoders=" + this.b + ", transcoder=" + this.c + '}';
    }
}
