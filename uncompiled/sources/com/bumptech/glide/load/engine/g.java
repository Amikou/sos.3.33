package com.bumptech.glide.load.engine;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DecodeJob;
import com.bumptech.glide.load.engine.h;
import defpackage.a21;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: EngineJob.java */
/* loaded from: classes.dex */
public class g<R> implements DecodeJob.b<R>, a21.f {
    public static final c D0 = new c();
    public DecodeJob<R> A0;
    public volatile boolean B0;
    public boolean C0;
    public final e a;
    public final et3 f0;
    public final h.a g0;
    public final et2<g<?>> h0;
    public final c i0;
    public final mv0 j0;
    public final ig1 k0;
    public final ig1 l0;
    public final ig1 m0;
    public final ig1 n0;
    public final AtomicInteger o0;
    public fx1 p0;
    public boolean q0;
    public boolean r0;
    public boolean s0;
    public boolean t0;
    public s73<?> u0;
    public DataSource v0;
    public boolean w0;
    public GlideException x0;
    public boolean y0;
    public h<?> z0;

    /* compiled from: EngineJob.java */
    /* loaded from: classes.dex */
    public class a implements Runnable {
        public final u73 a;

        public a(u73 u73Var) {
            this.a = u73Var;
        }

        @Override // java.lang.Runnable
        public void run() {
            synchronized (this.a.g()) {
                synchronized (g.this) {
                    if (g.this.a.i(this.a)) {
                        g.this.e(this.a);
                    }
                    g.this.i();
                }
            }
        }
    }

    /* compiled from: EngineJob.java */
    /* loaded from: classes.dex */
    public class b implements Runnable {
        public final u73 a;

        public b(u73 u73Var) {
            this.a = u73Var;
        }

        @Override // java.lang.Runnable
        public void run() {
            synchronized (this.a.g()) {
                synchronized (g.this) {
                    if (g.this.a.i(this.a)) {
                        g.this.z0.c();
                        g.this.f(this.a);
                        g.this.r(this.a);
                    }
                    g.this.i();
                }
            }
        }
    }

    /* compiled from: EngineJob.java */
    /* loaded from: classes.dex */
    public static class c {
        public <R> h<R> a(s73<R> s73Var, boolean z, fx1 fx1Var, h.a aVar) {
            return new h<>(s73Var, z, true, fx1Var, aVar);
        }
    }

    /* compiled from: EngineJob.java */
    /* loaded from: classes.dex */
    public static final class d {
        public final u73 a;
        public final Executor b;

        public d(u73 u73Var, Executor executor) {
            this.a = u73Var;
            this.b = executor;
        }

        public boolean equals(Object obj) {
            if (obj instanceof d) {
                return this.a.equals(((d) obj).a);
            }
            return false;
        }

        public int hashCode() {
            return this.a.hashCode();
        }
    }

    /* compiled from: EngineJob.java */
    /* loaded from: classes.dex */
    public static final class e implements Iterable<d> {
        public final List<d> a;

        public e() {
            this(new ArrayList(2));
        }

        public static d m(u73 u73Var) {
            return new d(u73Var, yy0.a());
        }

        public void clear() {
            this.a.clear();
        }

        public void e(u73 u73Var, Executor executor) {
            this.a.add(new d(u73Var, executor));
        }

        public boolean i(u73 u73Var) {
            return this.a.contains(m(u73Var));
        }

        public boolean isEmpty() {
            return this.a.isEmpty();
        }

        @Override // java.lang.Iterable
        public Iterator<d> iterator() {
            return this.a.iterator();
        }

        public e k() {
            return new e(new ArrayList(this.a));
        }

        public void n(u73 u73Var) {
            this.a.remove(m(u73Var));
        }

        public int size() {
            return this.a.size();
        }

        public e(List<d> list) {
            this.a = list;
        }
    }

    public g(ig1 ig1Var, ig1 ig1Var2, ig1 ig1Var3, ig1 ig1Var4, mv0 mv0Var, h.a aVar, et2<g<?>> et2Var) {
        this(ig1Var, ig1Var2, ig1Var3, ig1Var4, mv0Var, aVar, et2Var, D0);
    }

    @Override // com.bumptech.glide.load.engine.DecodeJob.b
    public void a(GlideException glideException) {
        synchronized (this) {
            this.x0 = glideException;
        }
        n();
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.bumptech.glide.load.engine.DecodeJob.b
    public void b(s73<R> s73Var, DataSource dataSource, boolean z) {
        synchronized (this) {
            this.u0 = s73Var;
            this.v0 = dataSource;
            this.C0 = z;
        }
        o();
    }

    @Override // com.bumptech.glide.load.engine.DecodeJob.b
    public void c(DecodeJob<?> decodeJob) {
        j().execute(decodeJob);
    }

    public synchronized void d(u73 u73Var, Executor executor) {
        this.f0.c();
        this.a.e(u73Var, executor);
        boolean z = true;
        if (this.w0) {
            k(1);
            executor.execute(new b(u73Var));
        } else if (this.y0) {
            k(1);
            executor.execute(new a(u73Var));
        } else {
            if (this.B0) {
                z = false;
            }
            wt2.a(z, "Cannot add callbacks to a cancelled EngineJob");
        }
    }

    public void e(u73 u73Var) {
        try {
            u73Var.a(this.x0);
        } catch (Throwable th) {
            throw new CallbackException(th);
        }
    }

    public void f(u73 u73Var) {
        try {
            u73Var.b(this.z0, this.v0, this.C0);
        } catch (Throwable th) {
            throw new CallbackException(th);
        }
    }

    @Override // defpackage.a21.f
    public et3 g() {
        return this.f0;
    }

    public void h() {
        if (m()) {
            return;
        }
        this.B0 = true;
        this.A0.a();
        this.j0.a(this, this.p0);
    }

    public void i() {
        h<?> hVar;
        synchronized (this) {
            this.f0.c();
            wt2.a(m(), "Not yet complete!");
            int decrementAndGet = this.o0.decrementAndGet();
            wt2.a(decrementAndGet >= 0, "Can't decrement below 0");
            if (decrementAndGet == 0) {
                hVar = this.z0;
                q();
            } else {
                hVar = null;
            }
        }
        if (hVar != null) {
            hVar.g();
        }
    }

    public final ig1 j() {
        if (this.r0) {
            return this.m0;
        }
        return this.s0 ? this.n0 : this.l0;
    }

    public synchronized void k(int i) {
        h<?> hVar;
        wt2.a(m(), "Not yet complete!");
        if (this.o0.getAndAdd(i) == 0 && (hVar = this.z0) != null) {
            hVar.c();
        }
    }

    public synchronized g<R> l(fx1 fx1Var, boolean z, boolean z2, boolean z3, boolean z4) {
        this.p0 = fx1Var;
        this.q0 = z;
        this.r0 = z2;
        this.s0 = z3;
        this.t0 = z4;
        return this;
    }

    public final boolean m() {
        return this.y0 || this.w0 || this.B0;
    }

    public void n() {
        synchronized (this) {
            this.f0.c();
            if (this.B0) {
                q();
            } else if (!this.a.isEmpty()) {
                if (!this.y0) {
                    this.y0 = true;
                    fx1 fx1Var = this.p0;
                    e k = this.a.k();
                    k(k.size() + 1);
                    this.j0.c(this, fx1Var, null);
                    Iterator<d> it = k.iterator();
                    while (it.hasNext()) {
                        d next = it.next();
                        next.b.execute(new a(next.a));
                    }
                    i();
                    return;
                }
                throw new IllegalStateException("Already failed once");
            } else {
                throw new IllegalStateException("Received an exception without any callbacks to notify");
            }
        }
    }

    public void o() {
        synchronized (this) {
            this.f0.c();
            if (this.B0) {
                this.u0.b();
                q();
            } else if (!this.a.isEmpty()) {
                if (!this.w0) {
                    this.z0 = this.i0.a(this.u0, this.q0, this.p0, this.g0);
                    this.w0 = true;
                    e k = this.a.k();
                    k(k.size() + 1);
                    this.j0.c(this, this.p0, this.z0);
                    Iterator<d> it = k.iterator();
                    while (it.hasNext()) {
                        d next = it.next();
                        next.b.execute(new b(next.a));
                    }
                    i();
                    return;
                }
                throw new IllegalStateException("Already have resource");
            } else {
                throw new IllegalStateException("Received a resource without any callbacks to notify");
            }
        }
    }

    public boolean p() {
        return this.t0;
    }

    public final synchronized void q() {
        if (this.p0 != null) {
            this.a.clear();
            this.p0 = null;
            this.z0 = null;
            this.u0 = null;
            this.y0 = false;
            this.B0 = false;
            this.w0 = false;
            this.C0 = false;
            this.A0.E(false);
            this.A0 = null;
            this.x0 = null;
            this.v0 = null;
            this.h0.a(this);
        } else {
            throw new IllegalArgumentException();
        }
    }

    public synchronized void r(u73 u73Var) {
        boolean z;
        this.f0.c();
        this.a.n(u73Var);
        if (this.a.isEmpty()) {
            h();
            if (!this.w0 && !this.y0) {
                z = false;
                if (z && this.o0.get() == 0) {
                    q();
                }
            }
            z = true;
            if (z) {
                q();
            }
        }
    }

    public synchronized void s(DecodeJob<R> decodeJob) {
        this.A0 = decodeJob;
        (decodeJob.L() ? this.k0 : j()).execute(decodeJob);
    }

    public g(ig1 ig1Var, ig1 ig1Var2, ig1 ig1Var3, ig1 ig1Var4, mv0 mv0Var, h.a aVar, et2<g<?>> et2Var, c cVar) {
        this.a = new e();
        this.f0 = et3.a();
        this.o0 = new AtomicInteger();
        this.k0 = ig1Var;
        this.l0 = ig1Var2;
        this.m0 = ig1Var3;
        this.n0 = ig1Var4;
        this.j0 = mv0Var;
        this.g0 = aVar;
        this.h0 = et2Var;
        this.i0 = cVar;
    }
}
