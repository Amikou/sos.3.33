package com.bumptech.glide.load.engine;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.data.d;
import com.bumptech.glide.load.engine.c;
import defpackage.j92;
import java.io.File;
import java.util.List;

/* compiled from: ResourceCacheGenerator.java */
/* loaded from: classes.dex */
public class j implements c, d.a<Object> {
    public final c.a a;
    public final d<?> f0;
    public int g0;
    public int h0 = -1;
    public fx1 i0;
    public List<j92<File, ?>> j0;
    public int k0;
    public volatile j92.a<?> l0;
    public File m0;
    public t73 n0;

    public j(d<?> dVar, c.a aVar) {
        this.f0 = dVar;
        this.a = aVar;
    }

    @Override // com.bumptech.glide.load.engine.c
    public boolean a() {
        mg1.a("ResourceCacheGenerator.startNext");
        try {
            List<fx1> c = this.f0.c();
            boolean z = false;
            if (c.isEmpty()) {
                return false;
            }
            List<Class<?>> m = this.f0.m();
            if (m.isEmpty()) {
                if (File.class.equals(this.f0.r())) {
                    return false;
                }
                throw new IllegalStateException("Failed to find any load path from " + this.f0.i() + " to " + this.f0.r());
            }
            while (true) {
                if (this.j0 != null && b()) {
                    this.l0 = null;
                    while (!z && b()) {
                        List<j92<File, ?>> list = this.j0;
                        int i = this.k0;
                        this.k0 = i + 1;
                        this.l0 = list.get(i).b(this.m0, this.f0.t(), this.f0.f(), this.f0.k());
                        if (this.l0 != null && this.f0.u(this.l0.c.a())) {
                            this.l0.c.e(this.f0.l(), this);
                            z = true;
                        }
                    }
                    return z;
                }
                int i2 = this.h0 + 1;
                this.h0 = i2;
                if (i2 >= m.size()) {
                    int i3 = this.g0 + 1;
                    this.g0 = i3;
                    if (i3 >= c.size()) {
                        return false;
                    }
                    this.h0 = 0;
                }
                fx1 fx1Var = c.get(this.g0);
                Class<?> cls = m.get(this.h0);
                this.n0 = new t73(this.f0.b(), fx1Var, this.f0.p(), this.f0.t(), this.f0.f(), this.f0.s(cls), cls, this.f0.k());
                File a = this.f0.d().a(this.n0);
                this.m0 = a;
                if (a != null) {
                    this.i0 = fx1Var;
                    this.j0 = this.f0.j(a);
                    this.k0 = 0;
                }
            }
        } finally {
            mg1.e();
        }
    }

    public final boolean b() {
        return this.k0 < this.j0.size();
    }

    @Override // com.bumptech.glide.load.data.d.a
    public void c(Exception exc) {
        this.a.d(this.n0, exc, this.l0.c, DataSource.RESOURCE_DISK_CACHE);
    }

    @Override // com.bumptech.glide.load.engine.c
    public void cancel() {
        j92.a<?> aVar = this.l0;
        if (aVar != null) {
            aVar.c.cancel();
        }
    }

    @Override // com.bumptech.glide.load.data.d.a
    public void f(Object obj) {
        this.a.e(this.i0, obj, this.l0.c, DataSource.RESOURCE_DISK_CACHE, this.n0);
    }
}
