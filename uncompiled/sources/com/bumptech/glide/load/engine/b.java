package com.bumptech.glide.load.engine;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.data.d;
import com.bumptech.glide.load.engine.c;
import defpackage.j92;
import java.io.File;
import java.util.List;

/* compiled from: DataCacheGenerator.java */
/* loaded from: classes.dex */
public class b implements c, d.a<Object> {
    public final List<fx1> a;
    public final d<?> f0;
    public final c.a g0;
    public int h0;
    public fx1 i0;
    public List<j92<File, ?>> j0;
    public int k0;
    public volatile j92.a<?> l0;
    public File m0;

    public b(d<?> dVar, c.a aVar) {
        this(dVar.c(), dVar, aVar);
    }

    /* JADX WARN: Code restructure failed: missing block: B:11:0x001b, code lost:
        if (b() == false) goto L25;
     */
    /* JADX WARN: Code restructure failed: missing block: B:12:0x001d, code lost:
        r0 = r7.j0;
        r3 = r7.k0;
        r7.k0 = r3 + 1;
        r7.l0 = r0.get(r3).b(r7.m0, r7.f0.t(), r7.f0.f(), r7.f0.k());
     */
    /* JADX WARN: Code restructure failed: missing block: B:13:0x0047, code lost:
        if (r7.l0 == null) goto L24;
     */
    /* JADX WARN: Code restructure failed: missing block: B:15:0x0057, code lost:
        if (r7.f0.u(r7.l0.c.a()) == false) goto L23;
     */
    /* JADX WARN: Code restructure failed: missing block: B:16:0x0059, code lost:
        r7.l0.c.e(r7.f0.l(), r7);
     */
    /* JADX WARN: Code restructure failed: missing block: B:17:0x0066, code lost:
        r1 = true;
     */
    /* JADX WARN: Code restructure failed: missing block: B:19:0x006b, code lost:
        return r1;
     */
    /* JADX WARN: Code restructure failed: missing block: B:8:0x0012, code lost:
        r7.l0 = null;
     */
    /* JADX WARN: Code restructure failed: missing block: B:9:0x0015, code lost:
        if (r1 != false) goto L30;
     */
    @Override // com.bumptech.glide.load.engine.c
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public boolean a() {
        /*
            r7 = this;
            java.lang.String r0 = "DataCacheGenerator.startNext"
            defpackage.mg1.a(r0)
        L5:
            java.util.List<j92<java.io.File, ?>> r0 = r7.j0     // Catch: java.lang.Throwable -> Lae
            r1 = 0
            r2 = 1
            if (r0 == 0) goto L6c
            boolean r0 = r7.b()     // Catch: java.lang.Throwable -> Lae
            if (r0 != 0) goto L12
            goto L6c
        L12:
            r0 = 0
            r7.l0 = r0     // Catch: java.lang.Throwable -> Lae
        L15:
            if (r1 != 0) goto L68
            boolean r0 = r7.b()     // Catch: java.lang.Throwable -> Lae
            if (r0 == 0) goto L68
            java.util.List<j92<java.io.File, ?>> r0 = r7.j0     // Catch: java.lang.Throwable -> Lae
            int r3 = r7.k0     // Catch: java.lang.Throwable -> Lae
            int r4 = r3 + 1
            r7.k0 = r4     // Catch: java.lang.Throwable -> Lae
            java.lang.Object r0 = r0.get(r3)     // Catch: java.lang.Throwable -> Lae
            j92 r0 = (defpackage.j92) r0     // Catch: java.lang.Throwable -> Lae
            java.io.File r3 = r7.m0     // Catch: java.lang.Throwable -> Lae
            com.bumptech.glide.load.engine.d<?> r4 = r7.f0     // Catch: java.lang.Throwable -> Lae
            int r4 = r4.t()     // Catch: java.lang.Throwable -> Lae
            com.bumptech.glide.load.engine.d<?> r5 = r7.f0     // Catch: java.lang.Throwable -> Lae
            int r5 = r5.f()     // Catch: java.lang.Throwable -> Lae
            com.bumptech.glide.load.engine.d<?> r6 = r7.f0     // Catch: java.lang.Throwable -> Lae
            vn2 r6 = r6.k()     // Catch: java.lang.Throwable -> Lae
            j92$a r0 = r0.b(r3, r4, r5, r6)     // Catch: java.lang.Throwable -> Lae
            r7.l0 = r0     // Catch: java.lang.Throwable -> Lae
            j92$a<?> r0 = r7.l0     // Catch: java.lang.Throwable -> Lae
            if (r0 == 0) goto L15
            com.bumptech.glide.load.engine.d<?> r0 = r7.f0     // Catch: java.lang.Throwable -> Lae
            j92$a<?> r3 = r7.l0     // Catch: java.lang.Throwable -> Lae
            com.bumptech.glide.load.data.d<Data> r3 = r3.c     // Catch: java.lang.Throwable -> Lae
            java.lang.Class r3 = r3.a()     // Catch: java.lang.Throwable -> Lae
            boolean r0 = r0.u(r3)     // Catch: java.lang.Throwable -> Lae
            if (r0 == 0) goto L15
            j92$a<?> r0 = r7.l0     // Catch: java.lang.Throwable -> Lae
            com.bumptech.glide.load.data.d<Data> r0 = r0.c     // Catch: java.lang.Throwable -> Lae
            com.bumptech.glide.load.engine.d<?> r1 = r7.f0     // Catch: java.lang.Throwable -> Lae
            com.bumptech.glide.Priority r1 = r1.l()     // Catch: java.lang.Throwable -> Lae
            r0.e(r1, r7)     // Catch: java.lang.Throwable -> Lae
            r1 = r2
            goto L15
        L68:
            defpackage.mg1.e()
            return r1
        L6c:
            int r0 = r7.h0     // Catch: java.lang.Throwable -> Lae
            int r0 = r0 + r2
            r7.h0 = r0     // Catch: java.lang.Throwable -> Lae
            java.util.List<fx1> r2 = r7.a     // Catch: java.lang.Throwable -> Lae
            int r2 = r2.size()     // Catch: java.lang.Throwable -> Lae
            if (r0 < r2) goto L7d
            defpackage.mg1.e()
            return r1
        L7d:
            java.util.List<fx1> r0 = r7.a     // Catch: java.lang.Throwable -> Lae
            int r2 = r7.h0     // Catch: java.lang.Throwable -> Lae
            java.lang.Object r0 = r0.get(r2)     // Catch: java.lang.Throwable -> Lae
            fx1 r0 = (defpackage.fx1) r0     // Catch: java.lang.Throwable -> Lae
            zd0 r2 = new zd0     // Catch: java.lang.Throwable -> Lae
            com.bumptech.glide.load.engine.d<?> r3 = r7.f0     // Catch: java.lang.Throwable -> Lae
            fx1 r3 = r3.p()     // Catch: java.lang.Throwable -> Lae
            r2.<init>(r0, r3)     // Catch: java.lang.Throwable -> Lae
            com.bumptech.glide.load.engine.d<?> r3 = r7.f0     // Catch: java.lang.Throwable -> Lae
            yo0 r3 = r3.d()     // Catch: java.lang.Throwable -> Lae
            java.io.File r2 = r3.a(r2)     // Catch: java.lang.Throwable -> Lae
            r7.m0 = r2     // Catch: java.lang.Throwable -> Lae
            if (r2 == 0) goto L5
            r7.i0 = r0     // Catch: java.lang.Throwable -> Lae
            com.bumptech.glide.load.engine.d<?> r0 = r7.f0     // Catch: java.lang.Throwable -> Lae
            java.util.List r0 = r0.j(r2)     // Catch: java.lang.Throwable -> Lae
            r7.j0 = r0     // Catch: java.lang.Throwable -> Lae
            r7.k0 = r1     // Catch: java.lang.Throwable -> Lae
            goto L5
        Lae:
            r0 = move-exception
            defpackage.mg1.e()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.load.engine.b.a():boolean");
    }

    public final boolean b() {
        return this.k0 < this.j0.size();
    }

    @Override // com.bumptech.glide.load.data.d.a
    public void c(Exception exc) {
        this.g0.d(this.i0, exc, this.l0.c, DataSource.DATA_DISK_CACHE);
    }

    @Override // com.bumptech.glide.load.engine.c
    public void cancel() {
        j92.a<?> aVar = this.l0;
        if (aVar != null) {
            aVar.c.cancel();
        }
    }

    @Override // com.bumptech.glide.load.data.d.a
    public void f(Object obj) {
        this.g0.e(this.i0, obj, this.l0.c, DataSource.DATA_DISK_CACHE, this.i0);
    }

    public b(List<fx1> list, d<?> dVar, c.a aVar) {
        this.h0 = -1;
        this.a = list;
        this.f0 = dVar;
        this.g0 = aVar;
    }
}
