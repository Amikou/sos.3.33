package com.bumptech.glide.load.engine;

import android.os.Build;
import android.util.Log;
import com.bumptech.glide.Priority;
import com.bumptech.glide.Registry;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.EncodeStrategy;
import com.bumptech.glide.load.engine.c;
import com.bumptech.glide.load.engine.e;
import defpackage.a21;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/* loaded from: classes.dex */
public class DecodeJob<R> implements c.a, Runnable, Comparable<DecodeJob<?>>, a21.f {
    public Thread A0;
    public fx1 B0;
    public fx1 C0;
    public Object D0;
    public DataSource E0;
    public com.bumptech.glide.load.data.d<?> F0;
    public volatile com.bumptech.glide.load.engine.c G0;
    public volatile boolean H0;
    public volatile boolean I0;
    public boolean J0;
    public final e h0;
    public final et2<DecodeJob<?>> i0;
    public com.bumptech.glide.c l0;
    public fx1 m0;
    public Priority n0;
    public nv0 o0;
    public int p0;
    public int q0;
    public bp0 r0;
    public vn2 s0;
    public b<R> t0;
    public int u0;
    public Stage v0;
    public RunReason w0;
    public long x0;
    public boolean y0;
    public Object z0;
    public final com.bumptech.glide.load.engine.d<R> a = new com.bumptech.glide.load.engine.d<>();
    public final List<Throwable> f0 = new ArrayList();
    public final et3 g0 = et3.a();
    public final d<?> j0 = new d<>();
    public final f k0 = new f();

    /* loaded from: classes.dex */
    public enum RunReason {
        INITIALIZE,
        SWITCH_TO_SOURCE_SERVICE,
        DECODE_DATA
    }

    /* loaded from: classes.dex */
    public enum Stage {
        INITIALIZE,
        RESOURCE_CACHE,
        DATA_CACHE,
        SOURCE,
        ENCODE,
        FINISHED
    }

    /* loaded from: classes.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;
        public static final /* synthetic */ int[] b;
        public static final /* synthetic */ int[] c;

        static {
            int[] iArr = new int[EncodeStrategy.values().length];
            c = iArr;
            try {
                iArr[EncodeStrategy.SOURCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                c[EncodeStrategy.TRANSFORMED.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            int[] iArr2 = new int[Stage.values().length];
            b = iArr2;
            try {
                iArr2[Stage.RESOURCE_CACHE.ordinal()] = 1;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                b[Stage.DATA_CACHE.ordinal()] = 2;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                b[Stage.SOURCE.ordinal()] = 3;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                b[Stage.FINISHED.ordinal()] = 4;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                b[Stage.INITIALIZE.ordinal()] = 5;
            } catch (NoSuchFieldError unused7) {
            }
            int[] iArr3 = new int[RunReason.values().length];
            a = iArr3;
            try {
                iArr3[RunReason.INITIALIZE.ordinal()] = 1;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                a[RunReason.SWITCH_TO_SOURCE_SERVICE.ordinal()] = 2;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                a[RunReason.DECODE_DATA.ordinal()] = 3;
            } catch (NoSuchFieldError unused10) {
            }
        }
    }

    /* loaded from: classes.dex */
    public interface b<R> {
        void a(GlideException glideException);

        void b(s73<R> s73Var, DataSource dataSource, boolean z);

        void c(DecodeJob<?> decodeJob);
    }

    /* loaded from: classes.dex */
    public final class c<Z> implements e.a<Z> {
        public final DataSource a;

        public c(DataSource dataSource) {
            this.a = dataSource;
        }

        @Override // com.bumptech.glide.load.engine.e.a
        public s73<Z> a(s73<Z> s73Var) {
            return DecodeJob.this.D(this.a, s73Var);
        }
    }

    /* loaded from: classes.dex */
    public static class d<Z> {
        public fx1 a;
        public y73<Z> b;
        public o12<Z> c;

        public void a() {
            this.a = null;
            this.b = null;
            this.c = null;
        }

        public void b(e eVar, vn2 vn2Var) {
            mg1.a("DecodeJob.encode");
            try {
                eVar.a().b(this.a, new ae0(this.b, this.c, vn2Var));
            } finally {
                this.c.h();
                mg1.e();
            }
        }

        public boolean c() {
            return this.c != null;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public <X> void d(fx1 fx1Var, y73<X> y73Var, o12<X> o12Var) {
            this.a = fx1Var;
            this.b = y73Var;
            this.c = o12Var;
        }
    }

    /* loaded from: classes.dex */
    public interface e {
        yo0 a();
    }

    /* loaded from: classes.dex */
    public static class f {
        public boolean a;
        public boolean b;
        public boolean c;

        public final boolean a(boolean z) {
            return (this.c || z || this.b) && this.a;
        }

        public synchronized boolean b() {
            this.b = true;
            return a(false);
        }

        public synchronized boolean c() {
            this.c = true;
            return a(false);
        }

        public synchronized boolean d(boolean z) {
            this.a = true;
            return a(z);
        }

        public synchronized void e() {
            this.b = false;
            this.a = false;
            this.c = false;
        }
    }

    public DecodeJob(e eVar, et2<DecodeJob<?>> et2Var) {
        this.h0 = eVar;
        this.i0 = et2Var;
    }

    public final void A() {
        K();
        this.t0.a(new GlideException("Failed to load resource", new ArrayList(this.f0)));
        C();
    }

    public final void B() {
        if (this.k0.b()) {
            G();
        }
    }

    public final void C() {
        if (this.k0.c()) {
            G();
        }
    }

    public <Z> s73<Z> D(DataSource dataSource, s73<Z> s73Var) {
        s73<Z> s73Var2;
        za4<Z> za4Var;
        EncodeStrategy encodeStrategy;
        fx1 zd0Var;
        Class<?> cls = s73Var.get().getClass();
        y73<Z> y73Var = null;
        if (dataSource != DataSource.RESOURCE_DISK_CACHE) {
            za4<Z> s = this.a.s(cls);
            za4Var = s;
            s73Var2 = s.a(this.l0, s73Var, this.p0, this.q0);
        } else {
            s73Var2 = s73Var;
            za4Var = null;
        }
        if (!s73Var.equals(s73Var2)) {
            s73Var.b();
        }
        if (this.a.w(s73Var2)) {
            y73Var = this.a.n(s73Var2);
            encodeStrategy = y73Var.b(this.s0);
        } else {
            encodeStrategy = EncodeStrategy.NONE;
        }
        y73 y73Var2 = y73Var;
        if (this.r0.d(!this.a.y(this.B0), dataSource, encodeStrategy)) {
            if (y73Var2 != null) {
                int i = a.c[encodeStrategy.ordinal()];
                if (i == 1) {
                    zd0Var = new zd0(this.B0, this.m0);
                } else if (i == 2) {
                    zd0Var = new t73(this.a.b(), this.B0, this.m0, this.p0, this.q0, za4Var, cls, this.s0);
                } else {
                    throw new IllegalArgumentException("Unknown strategy: " + encodeStrategy);
                }
                o12 e2 = o12.e(s73Var2);
                this.j0.d(zd0Var, y73Var2, e2);
                return e2;
            }
            throw new Registry.NoResultEncoderAvailableException(s73Var2.get().getClass());
        }
        return s73Var2;
    }

    public void E(boolean z) {
        if (this.k0.d(z)) {
            G();
        }
    }

    public final void G() {
        this.k0.e();
        this.j0.a();
        this.a.a();
        this.H0 = false;
        this.l0 = null;
        this.m0 = null;
        this.s0 = null;
        this.n0 = null;
        this.o0 = null;
        this.t0 = null;
        this.v0 = null;
        this.G0 = null;
        this.A0 = null;
        this.B0 = null;
        this.D0 = null;
        this.E0 = null;
        this.F0 = null;
        this.x0 = 0L;
        this.I0 = false;
        this.z0 = null;
        this.f0.clear();
        this.i0.a(this);
    }

    public final void H() {
        this.A0 = Thread.currentThread();
        this.x0 = t12.b();
        boolean z = false;
        while (!this.I0 && this.G0 != null && !(z = this.G0.a())) {
            this.v0 = p(this.v0);
            this.G0 = o();
            if (this.v0 == Stage.SOURCE) {
                f();
                return;
            }
        }
        if ((this.v0 == Stage.FINISHED || this.I0) && !z) {
            A();
        }
    }

    public final <Data, ResourceType> s73<R> I(Data data, DataSource dataSource, i<Data, ResourceType, R> iVar) throws GlideException {
        vn2 r = r(dataSource);
        com.bumptech.glide.load.data.e<Data> l = this.l0.i().l(data);
        try {
            return iVar.a(l, r, this.p0, this.q0, new c(dataSource));
        } finally {
            l.b();
        }
    }

    public final void J() {
        int i = a.a[this.w0.ordinal()];
        if (i == 1) {
            this.v0 = p(Stage.INITIALIZE);
            this.G0 = o();
            H();
        } else if (i == 2) {
            H();
        } else if (i == 3) {
            l();
        } else {
            throw new IllegalStateException("Unrecognized run reason: " + this.w0);
        }
    }

    public final void K() {
        Throwable th;
        this.g0.c();
        if (this.H0) {
            if (this.f0.isEmpty()) {
                th = null;
            } else {
                List<Throwable> list = this.f0;
                th = list.get(list.size() - 1);
            }
            throw new IllegalStateException("Already notified", th);
        }
        this.H0 = true;
    }

    public boolean L() {
        Stage p = p(Stage.INITIALIZE);
        return p == Stage.RESOURCE_CACHE || p == Stage.DATA_CACHE;
    }

    public void a() {
        this.I0 = true;
        com.bumptech.glide.load.engine.c cVar = this.G0;
        if (cVar != null) {
            cVar.cancel();
        }
    }

    @Override // com.bumptech.glide.load.engine.c.a
    public void d(fx1 fx1Var, Exception exc, com.bumptech.glide.load.data.d<?> dVar, DataSource dataSource) {
        dVar.b();
        GlideException glideException = new GlideException("Fetching data failed", exc);
        glideException.setLoggingDetails(fx1Var, dataSource, dVar.a());
        this.f0.add(glideException);
        if (Thread.currentThread() != this.A0) {
            this.w0 = RunReason.SWITCH_TO_SOURCE_SERVICE;
            this.t0.c(this);
            return;
        }
        H();
    }

    @Override // com.bumptech.glide.load.engine.c.a
    public void e(fx1 fx1Var, Object obj, com.bumptech.glide.load.data.d<?> dVar, DataSource dataSource, fx1 fx1Var2) {
        this.B0 = fx1Var;
        this.D0 = obj;
        this.F0 = dVar;
        this.E0 = dataSource;
        this.C0 = fx1Var2;
        this.J0 = fx1Var != this.a.c().get(0);
        if (Thread.currentThread() != this.A0) {
            this.w0 = RunReason.DECODE_DATA;
            this.t0.c(this);
            return;
        }
        mg1.a("DecodeJob.decodeFromRetrievedData");
        try {
            l();
        } finally {
            mg1.e();
        }
    }

    @Override // com.bumptech.glide.load.engine.c.a
    public void f() {
        this.w0 = RunReason.SWITCH_TO_SOURCE_SERVICE;
        this.t0.c(this);
    }

    @Override // defpackage.a21.f
    public et3 g() {
        return this.g0;
    }

    @Override // java.lang.Comparable
    /* renamed from: h */
    public int compareTo(DecodeJob<?> decodeJob) {
        int s = s() - decodeJob.s();
        return s == 0 ? this.u0 - decodeJob.u0 : s;
    }

    public final <Data> s73<R> j(com.bumptech.glide.load.data.d<?> dVar, Data data, DataSource dataSource) throws GlideException {
        if (data == null) {
            return null;
        }
        try {
            long b2 = t12.b();
            s73<R> k = k(data, dataSource);
            if (Log.isLoggable("DecodeJob", 2)) {
                u("Decoded result " + k, b2);
            }
            return k;
        } finally {
            dVar.b();
        }
    }

    public final <Data> s73<R> k(Data data, DataSource dataSource) throws GlideException {
        return I(data, dataSource, (i<Data, ?, R>) this.a.h(data.getClass()));
    }

    public final void l() {
        if (Log.isLoggable("DecodeJob", 2)) {
            long j = this.x0;
            v("Retrieved data", j, "data: " + this.D0 + ", cache key: " + this.B0 + ", fetcher: " + this.F0);
        }
        s73<R> s73Var = null;
        try {
            s73Var = j(this.F0, this.D0, this.E0);
        } catch (GlideException e2) {
            e2.setLoggingDetails(this.C0, this.E0);
            this.f0.add(e2);
        }
        if (s73Var != null) {
            y(s73Var, this.E0, this.J0);
        } else {
            H();
        }
    }

    public final com.bumptech.glide.load.engine.c o() {
        int i = a.b[this.v0.ordinal()];
        if (i != 1) {
            if (i != 2) {
                if (i != 3) {
                    if (i == 4) {
                        return null;
                    }
                    throw new IllegalStateException("Unrecognized stage: " + this.v0);
                }
                return new k(this.a, this);
            }
            return new com.bumptech.glide.load.engine.b(this.a, this);
        }
        return new j(this.a, this);
    }

    public final Stage p(Stage stage) {
        int i = a.b[stage.ordinal()];
        if (i == 1) {
            if (this.r0.a()) {
                return Stage.DATA_CACHE;
            }
            return p(Stage.DATA_CACHE);
        } else if (i == 2) {
            return this.y0 ? Stage.FINISHED : Stage.SOURCE;
        } else if (i == 3 || i == 4) {
            return Stage.FINISHED;
        } else {
            if (i == 5) {
                if (this.r0.b()) {
                    return Stage.RESOURCE_CACHE;
                }
                return p(Stage.RESOURCE_CACHE);
            }
            throw new IllegalArgumentException("Unrecognized stage: " + stage);
        }
    }

    public final vn2 r(DataSource dataSource) {
        vn2 vn2Var = this.s0;
        if (Build.VERSION.SDK_INT < 26) {
            return vn2Var;
        }
        boolean z = dataSource == DataSource.RESOURCE_DISK_CACHE || this.a.x();
        mn2<Boolean> mn2Var = com.bumptech.glide.load.resource.bitmap.e.i;
        Boolean bool = (Boolean) vn2Var.c(mn2Var);
        if (bool == null || (bool.booleanValue() && !z)) {
            vn2 vn2Var2 = new vn2();
            vn2Var2.d(this.s0);
            vn2Var2.e(mn2Var, Boolean.valueOf(z));
            return vn2Var2;
        }
        return vn2Var;
    }

    @Override // java.lang.Runnable
    public void run() {
        mg1.c("DecodeJob#run(reason=%s, model=%s)", this.w0, this.z0);
        com.bumptech.glide.load.data.d<?> dVar = this.F0;
        try {
            try {
                if (this.I0) {
                    A();
                    if (dVar != null) {
                        dVar.b();
                    }
                    mg1.e();
                    return;
                }
                J();
                if (dVar != null) {
                    dVar.b();
                }
                mg1.e();
            } catch (CallbackException e2) {
                throw e2;
            }
        }
    }

    public final int s() {
        return this.n0.ordinal();
    }

    public DecodeJob<R> t(com.bumptech.glide.c cVar, Object obj, nv0 nv0Var, fx1 fx1Var, int i, int i2, Class<?> cls, Class<R> cls2, Priority priority, bp0 bp0Var, Map<Class<?>, za4<?>> map, boolean z, boolean z2, boolean z3, vn2 vn2Var, b<R> bVar, int i3) {
        this.a.v(cVar, obj, fx1Var, i, i2, bp0Var, cls, cls2, priority, vn2Var, map, z, z2, this.h0);
        this.l0 = cVar;
        this.m0 = fx1Var;
        this.n0 = priority;
        this.o0 = nv0Var;
        this.p0 = i;
        this.q0 = i2;
        this.r0 = bp0Var;
        this.y0 = z3;
        this.s0 = vn2Var;
        this.t0 = bVar;
        this.u0 = i3;
        this.w0 = RunReason.INITIALIZE;
        this.z0 = obj;
        return this;
    }

    public final void u(String str, long j) {
        v(str, j, null);
    }

    public final void v(String str, long j, String str2) {
        String str3;
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(" in ");
        sb.append(t12.a(j));
        sb.append(", load key: ");
        sb.append(this.o0);
        if (str2 != null) {
            str3 = ", " + str2;
        } else {
            str3 = "";
        }
        sb.append(str3);
        sb.append(", thread: ");
        sb.append(Thread.currentThread().getName());
    }

    public final void x(s73<R> s73Var, DataSource dataSource, boolean z) {
        K();
        this.t0.b(s73Var, dataSource, z);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public final void y(s73<R> s73Var, DataSource dataSource, boolean z) {
        mg1.a("DecodeJob.notifyEncodeAndRelease");
        try {
            if (s73Var instanceof oq1) {
                ((oq1) s73Var).c();
            }
            o12 o12Var = 0;
            if (this.j0.c()) {
                s73Var = o12.e(s73Var);
                o12Var = s73Var;
            }
            x(s73Var, dataSource, z);
            this.v0 = Stage.ENCODE;
            if (this.j0.c()) {
                this.j0.b(this.h0, this.s0);
            }
            if (o12Var != 0) {
                o12Var.h();
            }
            B();
        } finally {
            mg1.e();
        }
    }
}
