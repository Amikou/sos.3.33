package com.bumptech.glide.load.data;

import java.io.IOException;
import java.io.OutputStream;

/* compiled from: BufferedOutputStream.java */
/* loaded from: classes.dex */
public final class c extends OutputStream {
    public final OutputStream a;
    public byte[] f0;
    public sh g0;
    public int h0;

    public c(OutputStream outputStream, sh shVar) {
        this(outputStream, shVar, 65536);
    }

    public final void a() throws IOException {
        int i = this.h0;
        if (i > 0) {
            this.a.write(this.f0, 0, i);
            this.h0 = 0;
        }
    }

    public final void b() throws IOException {
        if (this.h0 == this.f0.length) {
            a();
        }
    }

    public final void c() {
        byte[] bArr = this.f0;
        if (bArr != null) {
            this.g0.c(bArr);
            this.f0 = null;
        }
    }

    @Override // java.io.OutputStream, java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        try {
            flush();
            this.a.close();
            c();
        } catch (Throwable th) {
            this.a.close();
            throw th;
        }
    }

    @Override // java.io.OutputStream, java.io.Flushable
    public void flush() throws IOException {
        a();
        this.a.flush();
    }

    @Override // java.io.OutputStream
    public void write(int i) throws IOException {
        byte[] bArr = this.f0;
        int i2 = this.h0;
        this.h0 = i2 + 1;
        bArr[i2] = (byte) i;
        b();
    }

    public c(OutputStream outputStream, sh shVar, int i) {
        this.a = outputStream;
        this.g0 = shVar;
        this.f0 = (byte[]) shVar.e(i, byte[].class);
    }

    @Override // java.io.OutputStream
    public void write(byte[] bArr) throws IOException {
        write(bArr, 0, bArr.length);
    }

    @Override // java.io.OutputStream
    public void write(byte[] bArr, int i, int i2) throws IOException {
        int i3 = 0;
        do {
            int i4 = i2 - i3;
            int i5 = i + i3;
            int i6 = this.h0;
            if (i6 == 0 && i4 >= this.f0.length) {
                this.a.write(bArr, i5, i4);
                return;
            }
            int min = Math.min(i4, this.f0.length - i6);
            System.arraycopy(bArr, i5, this.f0, this.h0, min);
            this.h0 += min;
            i3 += min;
            b();
        } while (i3 < i2);
    }
}
