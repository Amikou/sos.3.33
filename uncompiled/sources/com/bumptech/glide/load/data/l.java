package com.bumptech.glide.load.data;

import android.content.ContentResolver;
import android.net.Uri;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.data.d;
import java.io.FileNotFoundException;
import java.io.IOException;

/* compiled from: LocalUriFetcher.java */
/* loaded from: classes.dex */
public abstract class l<T> implements d<T> {
    public final Uri a;
    public final ContentResolver f0;
    public T g0;

    public l(ContentResolver contentResolver, Uri uri) {
        this.f0 = contentResolver;
        this.a = uri;
    }

    @Override // com.bumptech.glide.load.data.d
    public void b() {
        T t = this.g0;
        if (t != null) {
            try {
                c(t);
            } catch (IOException unused) {
            }
        }
    }

    public abstract void c(T t) throws IOException;

    @Override // com.bumptech.glide.load.data.d
    public void cancel() {
    }

    @Override // com.bumptech.glide.load.data.d
    public DataSource d() {
        return DataSource.LOCAL;
    }

    @Override // com.bumptech.glide.load.data.d
    public final void e(Priority priority, d.a<? super T> aVar) {
        try {
            T f = f(this.a, this.f0);
            this.g0 = f;
            aVar.f(f);
        } catch (FileNotFoundException e) {
            aVar.c(e);
        }
    }

    public abstract T f(Uri uri, ContentResolver contentResolver) throws FileNotFoundException;
}
