package com.bumptech.glide.load.data;

import android.content.res.AssetManager;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.data.d;
import java.io.IOException;

/* compiled from: AssetPathFetcher.java */
/* loaded from: classes.dex */
public abstract class b<T> implements d<T> {
    public final String a;
    public final AssetManager f0;
    public T g0;

    public b(AssetManager assetManager, String str) {
        this.f0 = assetManager;
        this.a = str;
    }

    @Override // com.bumptech.glide.load.data.d
    public void b() {
        T t = this.g0;
        if (t == null) {
            return;
        }
        try {
            c(t);
        } catch (IOException unused) {
        }
    }

    public abstract void c(T t) throws IOException;

    @Override // com.bumptech.glide.load.data.d
    public void cancel() {
    }

    @Override // com.bumptech.glide.load.data.d
    public DataSource d() {
        return DataSource.LOCAL;
    }

    @Override // com.bumptech.glide.load.data.d
    public void e(Priority priority, d.a<? super T> aVar) {
        try {
            T f = f(this.f0, this.a);
            this.g0 = f;
            aVar.f(f);
        } catch (IOException e) {
            aVar.c(e);
        }
    }

    public abstract T f(AssetManager assetManager, String str) throws IOException;
}
