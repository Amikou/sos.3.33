package com.bumptech.glide.load.data;

import android.text.TextUtils;
import android.util.Log;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.HttpException;
import com.bumptech.glide.load.data.d;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Map;

/* compiled from: HttpUrlFetcher.java */
/* loaded from: classes.dex */
public class j implements d<InputStream> {
    public static final b k0 = new a();
    public final ng1 a;
    public final int f0;
    public final b g0;
    public HttpURLConnection h0;
    public InputStream i0;
    public volatile boolean j0;

    /* compiled from: HttpUrlFetcher.java */
    /* loaded from: classes.dex */
    public static class a implements b {
        @Override // com.bumptech.glide.load.data.j.b
        public HttpURLConnection a(URL url) throws IOException {
            return (HttpURLConnection) url.openConnection();
        }
    }

    /* compiled from: HttpUrlFetcher.java */
    /* loaded from: classes.dex */
    public interface b {
        HttpURLConnection a(URL url) throws IOException;
    }

    public j(ng1 ng1Var, int i) {
        this(ng1Var, i, k0);
    }

    public static int f(HttpURLConnection httpURLConnection) {
        try {
            return httpURLConnection.getResponseCode();
        } catch (IOException unused) {
            return -1;
        }
    }

    public static boolean h(int i) {
        return i / 100 == 2;
    }

    public static boolean i(int i) {
        return i / 100 == 3;
    }

    @Override // com.bumptech.glide.load.data.d
    public Class<InputStream> a() {
        return InputStream.class;
    }

    @Override // com.bumptech.glide.load.data.d
    public void b() {
        InputStream inputStream = this.i0;
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException unused) {
            }
        }
        HttpURLConnection httpURLConnection = this.h0;
        if (httpURLConnection != null) {
            httpURLConnection.disconnect();
        }
        this.h0 = null;
    }

    public final HttpURLConnection c(URL url, Map<String, String> map) throws HttpException {
        try {
            HttpURLConnection a2 = this.g0.a(url);
            for (Map.Entry<String, String> entry : map.entrySet()) {
                a2.addRequestProperty(entry.getKey(), entry.getValue());
            }
            a2.setConnectTimeout(this.f0);
            a2.setReadTimeout(this.f0);
            a2.setUseCaches(false);
            a2.setDoInput(true);
            a2.setInstanceFollowRedirects(false);
            return a2;
        } catch (IOException e) {
            throw new HttpException("URL.openConnection threw", 0, e);
        }
    }

    @Override // com.bumptech.glide.load.data.d
    public void cancel() {
        this.j0 = true;
    }

    @Override // com.bumptech.glide.load.data.d
    public DataSource d() {
        return DataSource.REMOTE;
    }

    @Override // com.bumptech.glide.load.data.d
    public void e(Priority priority, d.a<? super InputStream> aVar) {
        StringBuilder sb;
        long b2 = t12.b();
        try {
            try {
                aVar.f(j(this.a.i(), 0, null, this.a.e()));
            } catch (IOException e) {
                Log.isLoggable("HttpUrlFetcher", 3);
                aVar.c(e);
                if (!Log.isLoggable("HttpUrlFetcher", 2)) {
                    return;
                }
                sb = new StringBuilder();
            }
            if (Log.isLoggable("HttpUrlFetcher", 2)) {
                sb = new StringBuilder();
                sb.append("Finished http url fetcher fetch in ");
                sb.append(t12.a(b2));
            }
        } catch (Throwable th) {
            if (Log.isLoggable("HttpUrlFetcher", 2)) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Finished http url fetcher fetch in ");
                sb2.append(t12.a(b2));
            }
            throw th;
        }
    }

    public final InputStream g(HttpURLConnection httpURLConnection) throws HttpException {
        try {
            if (TextUtils.isEmpty(httpURLConnection.getContentEncoding())) {
                this.i0 = g70.b(httpURLConnection.getInputStream(), httpURLConnection.getContentLength());
            } else {
                if (Log.isLoggable("HttpUrlFetcher", 3)) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Got non empty content encoding: ");
                    sb.append(httpURLConnection.getContentEncoding());
                }
                this.i0 = httpURLConnection.getInputStream();
            }
            return this.i0;
        } catch (IOException e) {
            throw new HttpException("Failed to obtain InputStream", f(httpURLConnection), e);
        }
    }

    public final InputStream j(URL url, int i, URL url2, Map<String, String> map) throws HttpException {
        if (i < 5) {
            if (url2 != null) {
                try {
                    if (url.toURI().equals(url2.toURI())) {
                        throw new HttpException("In re-direct loop", -1);
                    }
                } catch (URISyntaxException unused) {
                }
            }
            HttpURLConnection c = c(url, map);
            this.h0 = c;
            try {
                c.connect();
                this.i0 = this.h0.getInputStream();
                if (this.j0) {
                    return null;
                }
                int f = f(this.h0);
                if (h(f)) {
                    return g(this.h0);
                }
                if (!i(f)) {
                    if (f == -1) {
                        throw new HttpException(f);
                    }
                    try {
                        throw new HttpException(this.h0.getResponseMessage(), f);
                    } catch (IOException e) {
                        throw new HttpException("Failed to get a response message", f, e);
                    }
                }
                String headerField = this.h0.getHeaderField("Location");
                if (!TextUtils.isEmpty(headerField)) {
                    try {
                        URL url3 = new URL(url, headerField);
                        b();
                        return j(url3, i + 1, url, map);
                    } catch (MalformedURLException e2) {
                        throw new HttpException("Bad redirect url: " + headerField, f, e2);
                    }
                }
                throw new HttpException("Received empty or null redirect url", f);
            } catch (IOException e3) {
                throw new HttpException("Failed to connect or obtain data", f(this.h0), e3);
            }
        }
        throw new HttpException("Too many (> 5) redirects!", -1);
    }

    public j(ng1 ng1Var, int i, b bVar) {
        this.a = ng1Var;
        this.f0 = i;
        this.g0 = bVar;
    }
}
