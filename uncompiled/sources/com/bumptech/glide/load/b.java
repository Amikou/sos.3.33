package com.bumptech.glide.load;

import java.io.IOException;

/* compiled from: ResourceDecoder.java */
/* loaded from: classes.dex */
public interface b<T, Z> {
    boolean a(T t, vn2 vn2Var) throws IOException;

    s73<Z> b(T t, int i, int i2, vn2 vn2Var) throws IOException;
}
