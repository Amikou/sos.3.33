package com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import java.io.IOException;

/* compiled from: ParcelFileDescriptorBitmapDecoder.java */
/* loaded from: classes.dex */
public final class i implements com.bumptech.glide.load.b<ParcelFileDescriptor, Bitmap> {
    public final e a;

    public i(e eVar) {
        this.a = eVar;
    }

    @Override // com.bumptech.glide.load.b
    /* renamed from: c */
    public s73<Bitmap> b(ParcelFileDescriptor parcelFileDescriptor, int i, int i2, vn2 vn2Var) throws IOException {
        return this.a.d(parcelFileDescriptor, i, i2, vn2Var);
    }

    @Override // com.bumptech.glide.load.b
    /* renamed from: d */
    public boolean a(ParcelFileDescriptor parcelFileDescriptor, vn2 vn2Var) {
        return e(parcelFileDescriptor) && this.a.o(parcelFileDescriptor);
    }

    public final boolean e(ParcelFileDescriptor parcelFileDescriptor) {
        String str = Build.MANUFACTURER;
        return !("HUAWEI".equalsIgnoreCase(str) || "HONOR".equalsIgnoreCase(str)) || parcelFileDescriptor.getStatSize() <= 536870912;
    }
}
