package com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;

/* compiled from: UnitBitmapDecoder.java */
/* loaded from: classes.dex */
public final class m implements com.bumptech.glide.load.b<Bitmap, Bitmap> {

    /* compiled from: UnitBitmapDecoder.java */
    /* loaded from: classes.dex */
    public static final class a implements s73<Bitmap> {
        public final Bitmap a;

        public a(Bitmap bitmap) {
            this.a = bitmap;
        }

        @Override // defpackage.s73
        public int a() {
            return mg4.h(this.a);
        }

        @Override // defpackage.s73
        public void b() {
        }

        @Override // defpackage.s73
        /* renamed from: c */
        public Bitmap get() {
            return this.a;
        }

        @Override // defpackage.s73
        public Class<Bitmap> d() {
            return Bitmap.class;
        }
    }

    @Override // com.bumptech.glide.load.b
    /* renamed from: c */
    public s73<Bitmap> b(Bitmap bitmap, int i, int i2, vn2 vn2Var) {
        return new a(bitmap);
    }

    @Override // com.bumptech.glide.load.b
    /* renamed from: d */
    public boolean a(Bitmap bitmap, vn2 vn2Var) {
        return true;
    }
}
