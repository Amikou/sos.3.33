package com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;
import android.graphics.ImageDecoder;
import java.io.IOException;
import java.nio.ByteBuffer;

/* compiled from: ByteBufferBitmapImageDecoderResourceDecoder.java */
/* loaded from: classes.dex */
public final class d implements com.bumptech.glide.load.b<ByteBuffer, Bitmap> {
    public final b a = new b();

    @Override // com.bumptech.glide.load.b
    /* renamed from: c */
    public s73<Bitmap> b(ByteBuffer byteBuffer, int i, int i2, vn2 vn2Var) throws IOException {
        return this.a.b(ImageDecoder.createSource(byteBuffer), i, i2, vn2Var);
    }

    @Override // com.bumptech.glide.load.b
    /* renamed from: d */
    public boolean a(ByteBuffer byteBuffer, vn2 vn2Var) throws IOException {
        return true;
    }
}
