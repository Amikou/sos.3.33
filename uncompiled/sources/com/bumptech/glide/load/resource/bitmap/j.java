package com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;

/* compiled from: ResourceBitmapDecoder.java */
/* loaded from: classes.dex */
public class j implements com.bumptech.glide.load.b<Uri, Bitmap> {
    public final x73 a;
    public final jq b;

    public j(x73 x73Var, jq jqVar) {
        this.a = x73Var;
        this.b = jqVar;
    }

    @Override // com.bumptech.glide.load.b
    /* renamed from: c */
    public s73<Bitmap> b(Uri uri, int i, int i2, vn2 vn2Var) {
        s73<Drawable> b = this.a.b(uri, i, i2, vn2Var);
        if (b == null) {
            return null;
        }
        return zq0.a(this.b, b.get(), i, i2);
    }

    @Override // com.bumptech.glide.load.b
    /* renamed from: d */
    public boolean a(Uri uri, vn2 vn2Var) {
        return "android.resource".equals(uri.getScheme());
    }
}
