package com.bumptech.glide.load.resource.bitmap;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.util.DisplayMetrics;
import android.util.Log;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.ImageHeaderParser;
import com.bumptech.glide.load.PreferredColorSpace;
import com.bumptech.glide.load.data.ParcelFileDescriptorRewinder;
import com.bumptech.glide.load.resource.bitmap.DownsampleStrategy;
import com.bumptech.glide.load.resource.bitmap.g;
import com.github.mikephil.charting.utils.Utils;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Queue;
import java.util.Set;

/* compiled from: Downsampler.java */
/* loaded from: classes.dex */
public final class e {
    public static final mn2<DecodeFormat> f = mn2.f("com.bumptech.glide.load.resource.bitmap.Downsampler.DecodeFormat", DecodeFormat.DEFAULT);
    public static final mn2<PreferredColorSpace> g = mn2.e("com.bumptech.glide.load.resource.bitmap.Downsampler.PreferredColorSpace");
    public static final mn2<Boolean> h;
    public static final mn2<Boolean> i;
    public static final Set<String> j;
    public static final b k;
    public static final Set<ImageHeaderParser.ImageType> l;
    public static final Queue<BitmapFactory.Options> m;
    public final jq a;
    public final DisplayMetrics b;
    public final sh c;
    public final List<ImageHeaderParser> d;
    public final vj1 e = vj1.b();

    /* compiled from: Downsampler.java */
    /* loaded from: classes.dex */
    public class a implements b {
        @Override // com.bumptech.glide.load.resource.bitmap.e.b
        public void a() {
        }

        @Override // com.bumptech.glide.load.resource.bitmap.e.b
        public void b(jq jqVar, Bitmap bitmap) {
        }
    }

    /* compiled from: Downsampler.java */
    /* loaded from: classes.dex */
    public interface b {
        void a();

        void b(jq jqVar, Bitmap bitmap) throws IOException;
    }

    static {
        mn2<DownsampleStrategy> mn2Var = DownsampleStrategy.f;
        Boolean bool = Boolean.FALSE;
        h = mn2.f("com.bumptech.glide.load.resource.bitmap.Downsampler.FixBitmapSize", bool);
        i = mn2.f("com.bumptech.glide.load.resource.bitmap.Downsampler.AllowHardwareDecode", bool);
        j = Collections.unmodifiableSet(new HashSet(Arrays.asList("image/vnd.wap.wbmp", "image/x-ico")));
        k = new a();
        l = Collections.unmodifiableSet(EnumSet.of(ImageHeaderParser.ImageType.JPEG, ImageHeaderParser.ImageType.PNG_A, ImageHeaderParser.ImageType.PNG));
        m = mg4.f(0);
    }

    public e(List<ImageHeaderParser> list, DisplayMetrics displayMetrics, jq jqVar, sh shVar) {
        this.d = list;
        this.b = (DisplayMetrics) wt2.d(displayMetrics);
        this.a = (jq) wt2.d(jqVar);
        this.c = (sh) wt2.d(shVar);
    }

    public static int a(double d) {
        int l2 = l(d);
        int x = x(l2 * d);
        return x((d / (x / l2)) * x);
    }

    public static void c(ImageHeaderParser.ImageType imageType, g gVar, b bVar, jq jqVar, DownsampleStrategy downsampleStrategy, int i2, int i3, int i4, int i5, int i6, BitmapFactory.Options options) throws IOException {
        int i7;
        int i8;
        int min;
        int i9;
        int floor;
        double floor2;
        int i10;
        if (i3 > 0 && i4 > 0) {
            if (r(i2)) {
                i8 = i3;
                i7 = i4;
            } else {
                i7 = i3;
                i8 = i4;
            }
            float b2 = downsampleStrategy.b(i7, i8, i5, i6);
            if (b2 > Utils.FLOAT_EPSILON) {
                DownsampleStrategy.SampleSizeRounding a2 = downsampleStrategy.a(i7, i8, i5, i6);
                if (a2 != null) {
                    float f2 = i7;
                    float f3 = i8;
                    int x = i7 / x(b2 * f2);
                    int x2 = i8 / x(b2 * f3);
                    DownsampleStrategy.SampleSizeRounding sampleSizeRounding = DownsampleStrategy.SampleSizeRounding.MEMORY;
                    if (a2 == sampleSizeRounding) {
                        min = Math.max(x, x2);
                    } else {
                        min = Math.min(x, x2);
                    }
                    int i11 = Build.VERSION.SDK_INT;
                    if (i11 > 23 || !j.contains(options.outMimeType)) {
                        int max = Math.max(1, Integer.highestOneBit(min));
                        if (a2 == sampleSizeRounding && max < 1.0f / b2) {
                            max <<= 1;
                        }
                        i9 = max;
                    } else {
                        i9 = 1;
                    }
                    options.inSampleSize = i9;
                    if (imageType == ImageHeaderParser.ImageType.JPEG) {
                        float min2 = Math.min(i9, 8);
                        floor = (int) Math.ceil(f2 / min2);
                        i10 = (int) Math.ceil(f3 / min2);
                        int i12 = i9 / 8;
                        if (i12 > 0) {
                            floor /= i12;
                            i10 /= i12;
                        }
                    } else {
                        if (imageType != ImageHeaderParser.ImageType.PNG && imageType != ImageHeaderParser.ImageType.PNG_A) {
                            if (imageType.isWebp()) {
                                if (i11 >= 24) {
                                    float f4 = i9;
                                    floor = Math.round(f2 / f4);
                                    i10 = Math.round(f3 / f4);
                                } else {
                                    float f5 = i9;
                                    floor = (int) Math.floor(f2 / f5);
                                    floor2 = Math.floor(f3 / f5);
                                }
                            } else if (i7 % i9 == 0 && i8 % i9 == 0) {
                                floor = i7 / i9;
                                i10 = i8 / i9;
                            } else {
                                int[] m2 = m(gVar, options, bVar, jqVar);
                                floor = m2[0];
                                i10 = m2[1];
                            }
                        } else {
                            float f6 = i9;
                            floor = (int) Math.floor(f2 / f6);
                            floor2 = Math.floor(f3 / f6);
                        }
                        i10 = (int) floor2;
                    }
                    double b3 = downsampleStrategy.b(floor, i10, i5, i6);
                    if (i11 >= 19) {
                        options.inTargetDensity = a(b3);
                        options.inDensity = l(b3);
                    }
                    if (s(options)) {
                        options.inScaled = true;
                    } else {
                        options.inTargetDensity = 0;
                        options.inDensity = 0;
                    }
                    if (Log.isLoggable("Downsampler", 2)) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("Calculate scaling, source: [");
                        sb.append(i3);
                        sb.append("x");
                        sb.append(i4);
                        sb.append("], degreesToRotate: ");
                        sb.append(i2);
                        sb.append(", target: [");
                        sb.append(i5);
                        sb.append("x");
                        sb.append(i6);
                        sb.append("], power of two scaled: [");
                        sb.append(floor);
                        sb.append("x");
                        sb.append(i10);
                        sb.append("], exact scale factor: ");
                        sb.append(b2);
                        sb.append(", power of 2 sample size: ");
                        sb.append(i9);
                        sb.append(", adjusted scale factor: ");
                        sb.append(b3);
                        sb.append(", target density: ");
                        sb.append(options.inTargetDensity);
                        sb.append(", density: ");
                        sb.append(options.inDensity);
                        return;
                    }
                    return;
                }
                throw new IllegalArgumentException("Cannot round with null rounding");
            }
            throw new IllegalArgumentException("Cannot scale with factor: " + b2 + " from: " + downsampleStrategy + ", source: [" + i3 + "x" + i4 + "], target: [" + i5 + "x" + i6 + "]");
        } else if (Log.isLoggable("Downsampler", 3)) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Unable to determine dimensions for: ");
            sb2.append(imageType);
            sb2.append(" with target [");
            sb2.append(i5);
            sb2.append("x");
            sb2.append(i6);
            sb2.append("]");
        }
    }

    public static Bitmap i(g gVar, BitmapFactory.Options options, b bVar, jq jqVar) throws IOException {
        if (!options.inJustDecodeBounds) {
            bVar.a();
            gVar.c();
        }
        int i2 = options.outWidth;
        int i3 = options.outHeight;
        String str = options.outMimeType;
        l.i().lock();
        try {
            try {
                Bitmap b2 = gVar.b(options);
                l.i().unlock();
                return b2;
            } catch (IllegalArgumentException e) {
                IOException u = u(e, i2, i3, str, options);
                Log.isLoggable("Downsampler", 3);
                Bitmap bitmap = options.inBitmap;
                if (bitmap != null) {
                    try {
                        jqVar.c(bitmap);
                        options.inBitmap = null;
                        Bitmap i4 = i(gVar, options, bVar, jqVar);
                        l.i().unlock();
                        return i4;
                    } catch (IOException unused) {
                        throw u;
                    }
                }
                throw u;
            }
        } catch (Throwable th) {
            l.i().unlock();
            throw th;
        }
    }

    @TargetApi(19)
    public static String j(Bitmap bitmap) {
        String str;
        if (bitmap == null) {
            return null;
        }
        if (Build.VERSION.SDK_INT >= 19) {
            str = " (" + bitmap.getAllocationByteCount() + ")";
        } else {
            str = "";
        }
        return "[" + bitmap.getWidth() + "x" + bitmap.getHeight() + "] " + bitmap.getConfig() + str;
    }

    public static synchronized BitmapFactory.Options k() {
        BitmapFactory.Options poll;
        synchronized (e.class) {
            Queue<BitmapFactory.Options> queue = m;
            synchronized (queue) {
                poll = queue.poll();
            }
            if (poll == null) {
                poll = new BitmapFactory.Options();
                w(poll);
            }
        }
        return poll;
    }

    public static int l(double d) {
        if (d > 1.0d) {
            d = 1.0d / d;
        }
        return (int) Math.round(d * 2.147483647E9d);
    }

    public static int[] m(g gVar, BitmapFactory.Options options, b bVar, jq jqVar) throws IOException {
        options.inJustDecodeBounds = true;
        i(gVar, options, bVar, jqVar);
        options.inJustDecodeBounds = false;
        return new int[]{options.outWidth, options.outHeight};
    }

    public static String n(BitmapFactory.Options options) {
        return j(options.inBitmap);
    }

    public static boolean r(int i2) {
        return i2 == 90 || i2 == 270;
    }

    public static boolean s(BitmapFactory.Options options) {
        int i2;
        int i3 = options.inTargetDensity;
        return i3 > 0 && (i2 = options.inDensity) > 0 && i3 != i2;
    }

    public static void t(int i2, int i3, String str, BitmapFactory.Options options, Bitmap bitmap, int i4, int i5, long j2) {
        StringBuilder sb = new StringBuilder();
        sb.append("Decoded ");
        sb.append(j(bitmap));
        sb.append(" from [");
        sb.append(i2);
        sb.append("x");
        sb.append(i3);
        sb.append("] ");
        sb.append(str);
        sb.append(" with inBitmap ");
        sb.append(n(options));
        sb.append(" for [");
        sb.append(i4);
        sb.append("x");
        sb.append(i5);
        sb.append("], sample size: ");
        sb.append(options.inSampleSize);
        sb.append(", density: ");
        sb.append(options.inDensity);
        sb.append(", target density: ");
        sb.append(options.inTargetDensity);
        sb.append(", thread: ");
        sb.append(Thread.currentThread().getName());
        sb.append(", duration: ");
        sb.append(t12.a(j2));
    }

    public static IOException u(IllegalArgumentException illegalArgumentException, int i2, int i3, String str, BitmapFactory.Options options) {
        return new IOException("Exception decoding bitmap, outWidth: " + i2 + ", outHeight: " + i3 + ", outMimeType: " + str + ", inBitmap: " + n(options), illegalArgumentException);
    }

    public static void v(BitmapFactory.Options options) {
        w(options);
        Queue<BitmapFactory.Options> queue = m;
        synchronized (queue) {
            queue.offer(options);
        }
    }

    public static void w(BitmapFactory.Options options) {
        options.inTempStorage = null;
        options.inDither = false;
        options.inScaled = false;
        options.inSampleSize = 1;
        options.inPreferredConfig = null;
        options.inJustDecodeBounds = false;
        options.inDensity = 0;
        options.inTargetDensity = 0;
        if (Build.VERSION.SDK_INT >= 26) {
            options.inPreferredColorSpace = null;
            options.outColorSpace = null;
            options.outConfig = null;
        }
        options.outWidth = 0;
        options.outHeight = 0;
        options.outMimeType = null;
        options.inBitmap = null;
        options.inMutable = true;
    }

    public static int x(double d) {
        return (int) (d + 0.5d);
    }

    @TargetApi(26)
    public static void y(BitmapFactory.Options options, jq jqVar, int i2, int i3) {
        Bitmap.Config config;
        if (Build.VERSION.SDK_INT < 26) {
            config = null;
        } else if (options.inPreferredConfig == Bitmap.Config.HARDWARE) {
            return;
        } else {
            config = options.outConfig;
        }
        if (config == null) {
            config = options.inPreferredConfig;
        }
        options.inBitmap = jqVar.e(i2, i3, config);
    }

    public final void b(g gVar, DecodeFormat decodeFormat, boolean z, boolean z2, BitmapFactory.Options options, int i2, int i3) {
        if (this.e.i(i2, i3, options, z, z2)) {
            return;
        }
        if (decodeFormat != DecodeFormat.PREFER_ARGB_8888 && Build.VERSION.SDK_INT != 16) {
            boolean z3 = false;
            try {
                z3 = gVar.d().hasAlpha();
            } catch (IOException unused) {
                if (Log.isLoggable("Downsampler", 3)) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Cannot determine whether the image has alpha or not from header, format ");
                    sb.append(decodeFormat);
                }
            }
            Bitmap.Config config = z3 ? Bitmap.Config.ARGB_8888 : Bitmap.Config.RGB_565;
            options.inPreferredConfig = config;
            if (config == Bitmap.Config.RGB_565) {
                options.inDither = true;
                return;
            }
            return;
        }
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
    }

    public s73<Bitmap> d(ParcelFileDescriptor parcelFileDescriptor, int i2, int i3, vn2 vn2Var) throws IOException {
        return e(new g.c(parcelFileDescriptor, this.d, this.c), i2, i3, vn2Var, k);
    }

    public final s73<Bitmap> e(g gVar, int i2, int i3, vn2 vn2Var, b bVar) throws IOException {
        byte[] bArr = (byte[]) this.c.e(65536, byte[].class);
        BitmapFactory.Options k2 = k();
        k2.inTempStorage = bArr;
        DecodeFormat decodeFormat = (DecodeFormat) vn2Var.c(f);
        PreferredColorSpace preferredColorSpace = (PreferredColorSpace) vn2Var.c(g);
        DownsampleStrategy downsampleStrategy = (DownsampleStrategy) vn2Var.c(DownsampleStrategy.f);
        boolean booleanValue = ((Boolean) vn2Var.c(h)).booleanValue();
        mn2<Boolean> mn2Var = i;
        try {
            return oq.f(h(gVar, k2, downsampleStrategy, decodeFormat, preferredColorSpace, vn2Var.c(mn2Var) != null && ((Boolean) vn2Var.c(mn2Var)).booleanValue(), i2, i3, booleanValue, bVar), this.a);
        } finally {
            v(k2);
            this.c.c(bArr);
        }
    }

    public s73<Bitmap> f(InputStream inputStream, int i2, int i3, vn2 vn2Var, b bVar) throws IOException {
        return e(new g.b(inputStream, this.d, this.c), i2, i3, vn2Var, bVar);
    }

    public s73<Bitmap> g(ByteBuffer byteBuffer, int i2, int i3, vn2 vn2Var) throws IOException {
        return e(new g.a(byteBuffer, this.d, this.c), i2, i3, vn2Var, k);
    }

    /* JADX WARN: Removed duplicated region for block: B:51:0x015c  */
    /* JADX WARN: Removed duplicated region for block: B:69:0x019e  */
    /* JADX WARN: Removed duplicated region for block: B:72:0x01ae  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final android.graphics.Bitmap h(com.bumptech.glide.load.resource.bitmap.g r28, android.graphics.BitmapFactory.Options r29, com.bumptech.glide.load.resource.bitmap.DownsampleStrategy r30, com.bumptech.glide.load.DecodeFormat r31, com.bumptech.glide.load.PreferredColorSpace r32, boolean r33, int r34, int r35, boolean r36, com.bumptech.glide.load.resource.bitmap.e.b r37) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 455
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.load.resource.bitmap.e.h(com.bumptech.glide.load.resource.bitmap.g, android.graphics.BitmapFactory$Options, com.bumptech.glide.load.resource.bitmap.DownsampleStrategy, com.bumptech.glide.load.DecodeFormat, com.bumptech.glide.load.PreferredColorSpace, boolean, int, int, boolean, com.bumptech.glide.load.resource.bitmap.e$b):android.graphics.Bitmap");
    }

    public boolean o(ParcelFileDescriptor parcelFileDescriptor) {
        return ParcelFileDescriptorRewinder.c();
    }

    public boolean p(InputStream inputStream) {
        return true;
    }

    public boolean q(ByteBuffer byteBuffer) {
        return true;
    }

    public final boolean z(ImageHeaderParser.ImageType imageType) {
        if (Build.VERSION.SDK_INT >= 19) {
            return true;
        }
        return l.contains(imageType);
    }
}
