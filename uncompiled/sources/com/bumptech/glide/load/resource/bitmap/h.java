package com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;
import android.graphics.ImageDecoder;
import java.io.IOException;
import java.io.InputStream;

/* compiled from: InputStreamBitmapImageDecoderResourceDecoder.java */
/* loaded from: classes.dex */
public final class h implements com.bumptech.glide.load.b<InputStream, Bitmap> {
    public final b a = new b();

    @Override // com.bumptech.glide.load.b
    /* renamed from: c */
    public s73<Bitmap> b(InputStream inputStream, int i, int i2, vn2 vn2Var) throws IOException {
        return this.a.b(ImageDecoder.createSource(ts.b(inputStream)), i, i2, vn2Var);
    }

    @Override // com.bumptech.glide.load.b
    /* renamed from: d */
    public boolean a(InputStream inputStream, vn2 vn2Var) throws IOException {
        return true;
    }
}
