package com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;
import android.graphics.ImageDecoder;
import android.util.Log;
import java.io.IOException;

/* compiled from: BitmapImageDecoderResourceDecoder.java */
/* loaded from: classes.dex */
public final class b implements com.bumptech.glide.load.b<ImageDecoder.Source, Bitmap> {
    public final jq a = new kq();

    @Override // com.bumptech.glide.load.b
    /* renamed from: c */
    public s73<Bitmap> b(ImageDecoder.Source source, int i, int i2, vn2 vn2Var) throws IOException {
        Bitmap decodeBitmap = ImageDecoder.decodeBitmap(source, new hk0(i, i2, vn2Var));
        if (Log.isLoggable("BitmapImageDecoder", 2)) {
            StringBuilder sb = new StringBuilder();
            sb.append("Decoded [");
            sb.append(decodeBitmap.getWidth());
            sb.append("x");
            sb.append(decodeBitmap.getHeight());
            sb.append("] for [");
            sb.append(i);
            sb.append("x");
            sb.append(i2);
            sb.append("]");
        }
        return new oq(decodeBitmap, this.a);
    }

    @Override // com.bumptech.glide.load.b
    /* renamed from: d */
    public boolean a(ImageDecoder.Source source, vn2 vn2Var) throws IOException {
        return true;
    }
}
