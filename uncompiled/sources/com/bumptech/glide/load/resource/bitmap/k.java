package com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;
import com.bumptech.glide.load.resource.bitmap.e;
import java.io.IOException;
import java.io.InputStream;

/* compiled from: StreamBitmapDecoder.java */
/* loaded from: classes.dex */
public class k implements com.bumptech.glide.load.b<InputStream, Bitmap> {
    public final e a;
    public final sh b;

    /* compiled from: StreamBitmapDecoder.java */
    /* loaded from: classes.dex */
    public static class a implements e.b {
        public final RecyclableBufferedInputStream a;
        public final ly0 b;

        public a(RecyclableBufferedInputStream recyclableBufferedInputStream, ly0 ly0Var) {
            this.a = recyclableBufferedInputStream;
            this.b = ly0Var;
        }

        @Override // com.bumptech.glide.load.resource.bitmap.e.b
        public void a() {
            this.a.b();
        }

        @Override // com.bumptech.glide.load.resource.bitmap.e.b
        public void b(jq jqVar, Bitmap bitmap) throws IOException {
            IOException a = this.b.a();
            if (a != null) {
                if (bitmap != null) {
                    jqVar.c(bitmap);
                }
                throw a;
            }
        }
    }

    public k(e eVar, sh shVar) {
        this.a = eVar;
        this.b = shVar;
    }

    @Override // com.bumptech.glide.load.b
    /* renamed from: c */
    public s73<Bitmap> b(InputStream inputStream, int i, int i2, vn2 vn2Var) throws IOException {
        boolean z;
        RecyclableBufferedInputStream recyclableBufferedInputStream;
        if (inputStream instanceof RecyclableBufferedInputStream) {
            recyclableBufferedInputStream = (RecyclableBufferedInputStream) inputStream;
            z = false;
        } else {
            z = true;
            recyclableBufferedInputStream = new RecyclableBufferedInputStream(inputStream, this.b);
        }
        ly0 b = ly0.b(recyclableBufferedInputStream);
        try {
            return this.a.f(new c42(b), i, i2, vn2Var, new a(recyclableBufferedInputStream, b));
        } finally {
            b.c();
            if (z) {
                recyclableBufferedInputStream.c();
            }
        }
    }

    @Override // com.bumptech.glide.load.b
    /* renamed from: d */
    public boolean a(InputStream inputStream, vn2 vn2Var) {
        return this.a.p(inputStream);
    }
}
