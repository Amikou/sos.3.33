package com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.ParcelFileDescriptor;
import com.bumptech.glide.load.ImageHeaderParser;
import com.bumptech.glide.load.data.ParcelFileDescriptorRewinder;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;

/* compiled from: ImageReader.java */
/* loaded from: classes.dex */
public interface g {

    /* compiled from: ImageReader.java */
    /* loaded from: classes.dex */
    public static final class a implements g {
        public final ByteBuffer a;
        public final List<ImageHeaderParser> b;
        public final sh c;

        public a(ByteBuffer byteBuffer, List<ImageHeaderParser> list, sh shVar) {
            this.a = byteBuffer;
            this.b = list;
            this.c = shVar;
        }

        @Override // com.bumptech.glide.load.resource.bitmap.g
        public int a() throws IOException {
            return com.bumptech.glide.load.a.c(this.b, ts.d(this.a), this.c);
        }

        @Override // com.bumptech.glide.load.resource.bitmap.g
        public Bitmap b(BitmapFactory.Options options) {
            return BitmapFactory.decodeStream(e(), null, options);
        }

        @Override // com.bumptech.glide.load.resource.bitmap.g
        public void c() {
        }

        @Override // com.bumptech.glide.load.resource.bitmap.g
        public ImageHeaderParser.ImageType d() throws IOException {
            return com.bumptech.glide.load.a.g(this.b, ts.d(this.a));
        }

        public final InputStream e() {
            return ts.g(ts.d(this.a));
        }
    }

    /* compiled from: ImageReader.java */
    /* loaded from: classes.dex */
    public static final class b implements g {
        public final com.bumptech.glide.load.data.k a;
        public final sh b;
        public final List<ImageHeaderParser> c;

        public b(InputStream inputStream, List<ImageHeaderParser> list, sh shVar) {
            this.b = (sh) wt2.d(shVar);
            this.c = (List) wt2.d(list);
            this.a = new com.bumptech.glide.load.data.k(inputStream, shVar);
        }

        @Override // com.bumptech.glide.load.resource.bitmap.g
        public int a() throws IOException {
            return com.bumptech.glide.load.a.b(this.c, this.a.a(), this.b);
        }

        @Override // com.bumptech.glide.load.resource.bitmap.g
        public Bitmap b(BitmapFactory.Options options) throws IOException {
            return BitmapFactory.decodeStream(this.a.a(), null, options);
        }

        @Override // com.bumptech.glide.load.resource.bitmap.g
        public void c() {
            this.a.c();
        }

        @Override // com.bumptech.glide.load.resource.bitmap.g
        public ImageHeaderParser.ImageType d() throws IOException {
            return com.bumptech.glide.load.a.f(this.c, this.a.a(), this.b);
        }
    }

    /* compiled from: ImageReader.java */
    /* loaded from: classes.dex */
    public static final class c implements g {
        public final sh a;
        public final List<ImageHeaderParser> b;
        public final ParcelFileDescriptorRewinder c;

        public c(ParcelFileDescriptor parcelFileDescriptor, List<ImageHeaderParser> list, sh shVar) {
            this.a = (sh) wt2.d(shVar);
            this.b = (List) wt2.d(list);
            this.c = new ParcelFileDescriptorRewinder(parcelFileDescriptor);
        }

        @Override // com.bumptech.glide.load.resource.bitmap.g
        public int a() throws IOException {
            return com.bumptech.glide.load.a.a(this.b, this.c, this.a);
        }

        @Override // com.bumptech.glide.load.resource.bitmap.g
        public Bitmap b(BitmapFactory.Options options) throws IOException {
            return BitmapFactory.decodeFileDescriptor(this.c.a().getFileDescriptor(), null, options);
        }

        @Override // com.bumptech.glide.load.resource.bitmap.g
        public void c() {
        }

        @Override // com.bumptech.glide.load.resource.bitmap.g
        public ImageHeaderParser.ImageType d() throws IOException {
            return com.bumptech.glide.load.a.e(this.b, this.c, this.a);
        }
    }

    int a() throws IOException;

    Bitmap b(BitmapFactory.Options options) throws IOException;

    void c();

    ImageHeaderParser.ImageType d() throws IOException;
}
