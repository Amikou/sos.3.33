package com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;
import java.io.IOException;
import java.nio.ByteBuffer;

/* compiled from: ByteBufferBitmapDecoder.java */
/* loaded from: classes.dex */
public class c implements com.bumptech.glide.load.b<ByteBuffer, Bitmap> {
    public final e a;

    public c(e eVar) {
        this.a = eVar;
    }

    @Override // com.bumptech.glide.load.b
    /* renamed from: c */
    public s73<Bitmap> b(ByteBuffer byteBuffer, int i, int i2, vn2 vn2Var) throws IOException {
        return this.a.g(byteBuffer, i, i2, vn2Var);
    }

    @Override // com.bumptech.glide.load.b
    /* renamed from: d */
    public boolean a(ByteBuffer byteBuffer, vn2 vn2Var) {
        return this.a.q(byteBuffer);
    }
}
