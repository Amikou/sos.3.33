package com.bumptech.glide.load.resource.bitmap;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

/* loaded from: classes.dex */
public class RecyclableBufferedInputStream extends FilterInputStream {
    public volatile byte[] a;
    public int f0;
    public int g0;
    public int h0;
    public int i0;
    public final sh j0;

    /* loaded from: classes.dex */
    public static class InvalidMarkException extends IOException {
        private static final long serialVersionUID = -4338378848813561757L;

        public InvalidMarkException(String str) {
            super(str);
        }
    }

    public RecyclableBufferedInputStream(InputStream inputStream, sh shVar) {
        this(inputStream, shVar, 65536);
    }

    public static IOException d() throws IOException {
        throw new IOException("BufferedInputStream is closed");
    }

    public final int a(InputStream inputStream, byte[] bArr) throws IOException {
        int i = this.h0;
        if (i != -1) {
            int i2 = this.i0 - i;
            int i3 = this.g0;
            if (i2 < i3) {
                if (i == 0 && i3 > bArr.length && this.f0 == bArr.length) {
                    int length = bArr.length * 2;
                    if (length <= i3) {
                        i3 = length;
                    }
                    byte[] bArr2 = (byte[]) this.j0.e(i3, byte[].class);
                    System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
                    this.a = bArr2;
                    this.j0.c(bArr);
                    bArr = bArr2;
                } else if (i > 0) {
                    System.arraycopy(bArr, i, bArr, 0, bArr.length - i);
                }
                int i4 = this.i0 - this.h0;
                this.i0 = i4;
                this.h0 = 0;
                this.f0 = 0;
                int read = inputStream.read(bArr, i4, bArr.length - i4);
                int i5 = this.i0;
                if (read > 0) {
                    i5 += read;
                }
                this.f0 = i5;
                return read;
            }
        }
        int read2 = inputStream.read(bArr);
        if (read2 > 0) {
            this.h0 = -1;
            this.i0 = 0;
            this.f0 = read2;
        }
        return read2;
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public synchronized int available() throws IOException {
        InputStream inputStream;
        inputStream = ((FilterInputStream) this).in;
        if (this.a != null && inputStream != null) {
        } else {
            throw d();
        }
        return (this.f0 - this.i0) + inputStream.available();
    }

    public synchronized void b() {
        this.g0 = this.a.length;
    }

    public synchronized void c() {
        if (this.a != null) {
            this.j0.c(this.a);
            this.a = null;
        }
    }

    @Override // java.io.FilterInputStream, java.io.InputStream, java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        if (this.a != null) {
            this.j0.c(this.a);
            this.a = null;
        }
        InputStream inputStream = ((FilterInputStream) this).in;
        ((FilterInputStream) this).in = null;
        if (inputStream != null) {
            inputStream.close();
        }
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public synchronized void mark(int i) {
        this.g0 = Math.max(this.g0, i);
        this.h0 = this.i0;
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public boolean markSupported() {
        return true;
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public synchronized int read() throws IOException {
        byte[] bArr = this.a;
        InputStream inputStream = ((FilterInputStream) this).in;
        if (bArr != null && inputStream != null) {
            if (this.i0 < this.f0 || a(inputStream, bArr) != -1) {
                if (bArr != this.a && (bArr = this.a) == null) {
                    throw d();
                }
                int i = this.f0;
                int i2 = this.i0;
                if (i - i2 > 0) {
                    this.i0 = i2 + 1;
                    return bArr[i2] & 255;
                }
                return -1;
            }
            return -1;
        }
        throw d();
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public synchronized void reset() throws IOException {
        if (this.a != null) {
            int i = this.h0;
            if (-1 != i) {
                this.i0 = i;
            } else {
                throw new InvalidMarkException("Mark has been invalidated, pos: " + this.i0 + " markLimit: " + this.g0);
            }
        } else {
            throw new IOException("Stream is closed");
        }
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public synchronized long skip(long j) throws IOException {
        if (j < 1) {
            return 0L;
        }
        byte[] bArr = this.a;
        if (bArr != null) {
            InputStream inputStream = ((FilterInputStream) this).in;
            if (inputStream != null) {
                int i = this.f0;
                int i2 = this.i0;
                if (i - i2 >= j) {
                    this.i0 = (int) (i2 + j);
                    return j;
                }
                long j2 = i - i2;
                this.i0 = i;
                if (this.h0 != -1 && j <= this.g0) {
                    if (a(inputStream, bArr) == -1) {
                        return j2;
                    }
                    int i3 = this.f0;
                    int i4 = this.i0;
                    if (i3 - i4 >= j - j2) {
                        this.i0 = (int) ((i4 + j) - j2);
                        return j;
                    }
                    long j3 = (j2 + i3) - i4;
                    this.i0 = i3;
                    return j3;
                }
                long skip = inputStream.skip(j - j2);
                if (skip > 0) {
                    this.h0 = -1;
                }
                return j2 + skip;
            }
            throw d();
        }
        throw d();
    }

    public RecyclableBufferedInputStream(InputStream inputStream, sh shVar, int i) {
        super(inputStream);
        this.h0 = -1;
        this.j0 = shVar;
        this.a = (byte[]) shVar.e(i, byte[].class);
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public synchronized int read(byte[] bArr, int i, int i2) throws IOException {
        int i3;
        int i4;
        byte[] bArr2 = this.a;
        if (bArr2 == null) {
            throw d();
        }
        if (i2 == 0) {
            return 0;
        }
        InputStream inputStream = ((FilterInputStream) this).in;
        if (inputStream != null) {
            int i5 = this.i0;
            int i6 = this.f0;
            if (i5 < i6) {
                int i7 = i6 - i5 >= i2 ? i2 : i6 - i5;
                System.arraycopy(bArr2, i5, bArr, i, i7);
                this.i0 += i7;
                if (i7 == i2 || inputStream.available() == 0) {
                    return i7;
                }
                i += i7;
                i3 = i2 - i7;
            } else {
                i3 = i2;
            }
            while (true) {
                if (this.h0 == -1 && i3 >= bArr2.length) {
                    i4 = inputStream.read(bArr, i, i3);
                    if (i4 == -1) {
                        return i3 != i2 ? i2 - i3 : -1;
                    }
                } else if (a(inputStream, bArr2) == -1) {
                    return i3 != i2 ? i2 - i3 : -1;
                } else {
                    if (bArr2 != this.a && (bArr2 = this.a) == null) {
                        throw d();
                    }
                    int i8 = this.f0;
                    int i9 = this.i0;
                    i4 = i8 - i9 >= i3 ? i3 : i8 - i9;
                    System.arraycopy(bArr2, i9, bArr, i, i4);
                    this.i0 += i4;
                }
                i3 -= i4;
                if (i3 == 0) {
                    return i2;
                }
                if (inputStream.available() == 0) {
                    return i2 - i3;
                }
                i += i4;
            }
        } else {
            throw d();
        }
    }
}
