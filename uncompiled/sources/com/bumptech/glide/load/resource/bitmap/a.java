package com.bumptech.glide.load.resource.bitmap;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import java.io.IOException;

/* compiled from: BitmapDrawableDecoder.java */
/* loaded from: classes.dex */
public class a<DataType> implements com.bumptech.glide.load.b<DataType, BitmapDrawable> {
    public final com.bumptech.glide.load.b<DataType, Bitmap> a;
    public final Resources b;

    public a(Resources resources, com.bumptech.glide.load.b<DataType, Bitmap> bVar) {
        this.b = (Resources) wt2.d(resources);
        this.a = (com.bumptech.glide.load.b) wt2.d(bVar);
    }

    @Override // com.bumptech.glide.load.b
    public boolean a(DataType datatype, vn2 vn2Var) throws IOException {
        return this.a.a(datatype, vn2Var);
    }

    @Override // com.bumptech.glide.load.b
    public s73<BitmapDrawable> b(DataType datatype, int i, int i2, vn2 vn2Var) throws IOException {
        return uy1.f(this.b, this.a.b(datatype, i, i2, vn2Var));
    }
}
