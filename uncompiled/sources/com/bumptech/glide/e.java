package com.bumptech.glide;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.widget.ImageView;
import com.bumptech.glide.request.RequestCoordinator;
import com.bumptech.glide.request.SingleRequest;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;

/* compiled from: RequestBuilder.java */
/* loaded from: classes.dex */
public class e<TranscodeType> extends com.bumptech.glide.request.a<e<TranscodeType>> {
    public final Context E0;
    public final k73 F0;
    public final Class<TranscodeType> G0;
    public final c H0;
    public f<?, ? super TranscodeType> I0;
    public Object J0;
    public List<j73<TranscodeType>> K0;
    public e<TranscodeType> L0;
    public e<TranscodeType> M0;
    public Float N0;
    public boolean O0 = true;
    public boolean P0;
    public boolean Q0;

    /* compiled from: RequestBuilder.java */
    /* loaded from: classes.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;
        public static final /* synthetic */ int[] b;

        static {
            int[] iArr = new int[Priority.values().length];
            b = iArr;
            try {
                iArr[Priority.LOW.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                b[Priority.NORMAL.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                b[Priority.HIGH.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                b[Priority.IMMEDIATE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            int[] iArr2 = new int[ImageView.ScaleType.values().length];
            a = iArr2;
            try {
                iArr2[ImageView.ScaleType.CENTER_CROP.ordinal()] = 1;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                a[ImageView.ScaleType.CENTER_INSIDE.ordinal()] = 2;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                a[ImageView.ScaleType.FIT_CENTER.ordinal()] = 3;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                a[ImageView.ScaleType.FIT_START.ordinal()] = 4;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                a[ImageView.ScaleType.FIT_END.ordinal()] = 5;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                a[ImageView.ScaleType.FIT_XY.ordinal()] = 6;
            } catch (NoSuchFieldError unused10) {
            }
            try {
                a[ImageView.ScaleType.CENTER.ordinal()] = 7;
            } catch (NoSuchFieldError unused11) {
            }
            try {
                a[ImageView.ScaleType.MATRIX.ordinal()] = 8;
            } catch (NoSuchFieldError unused12) {
            }
        }
    }

    static {
        new n73().j(bp0.b).f0(Priority.LOW).o0(true);
    }

    @SuppressLint({"CheckResult"})
    public e(com.bumptech.glide.a aVar, k73 k73Var, Class<TranscodeType> cls, Context context) {
        this.F0 = k73Var;
        this.G0 = cls;
        this.E0 = context;
        this.I0 = k73Var.s(cls);
        this.H0 = aVar.i();
        C0(k73Var.q());
        a(k73Var.r());
    }

    @Override // com.bumptech.glide.request.a
    /* renamed from: A0 */
    public e<TranscodeType> g() {
        e<TranscodeType> eVar = (e) super.clone();
        eVar.I0 = (f<?, ? super TranscodeType>) eVar.I0.clone();
        if (eVar.K0 != null) {
            eVar.K0 = new ArrayList(eVar.K0);
        }
        e<TranscodeType> eVar2 = eVar.L0;
        if (eVar2 != null) {
            eVar.L0 = eVar2.g();
        }
        e<TranscodeType> eVar3 = eVar.M0;
        if (eVar3 != null) {
            eVar.M0 = eVar3.g();
        }
        return eVar;
    }

    public final Priority B0(Priority priority) {
        int i = a.b[priority.ordinal()];
        if (i != 1) {
            if (i != 2) {
                if (i != 3 && i != 4) {
                    throw new IllegalArgumentException("unknown priority: " + C());
                }
                return Priority.IMMEDIATE;
            }
            return Priority.HIGH;
        }
        return Priority.NORMAL;
    }

    @SuppressLint({"CheckResult"})
    public final void C0(List<j73<Object>> list) {
        for (j73<Object> j73Var : list) {
            u0(j73Var);
        }
    }

    public <Y extends i34<TranscodeType>> Y D0(Y y) {
        return (Y) G0(y, null, yy0.b());
    }

    public final <Y extends i34<TranscodeType>> Y E0(Y y, j73<TranscodeType> j73Var, com.bumptech.glide.request.a<?> aVar, Executor executor) {
        wt2.d(y);
        if (this.P0) {
            d73 w0 = w0(y, j73Var, aVar, executor);
            d73 l = y.l();
            if (w0.e(l) && !K0(aVar, l)) {
                if (!((d73) wt2.d(l)).isRunning()) {
                    l.k();
                }
                return y;
            }
            this.F0.o(y);
            y.c(w0);
            this.F0.F(y, w0);
            return y;
        }
        throw new IllegalArgumentException("You must call #load() before calling #into()");
    }

    public <Y extends i34<TranscodeType>> Y G0(Y y, j73<TranscodeType> j73Var, Executor executor) {
        return (Y) E0(y, j73Var, this, executor);
    }

    public ek4<ImageView, TranscodeType> I0(ImageView imageView) {
        e<TranscodeType> eVar;
        mg4.b();
        wt2.d(imageView);
        if (!V() && T() && imageView.getScaleType() != null) {
            switch (a.a[imageView.getScaleType().ordinal()]) {
                case 1:
                    eVar = clone().Y();
                    break;
                case 2:
                    eVar = clone().Z();
                    break;
                case 3:
                case 4:
                case 5:
                    eVar = clone().a0();
                    break;
                case 6:
                    eVar = clone().Z();
                    break;
            }
            return (ek4) E0(this.H0.a(imageView, this.G0), null, eVar, yy0.b());
        }
        eVar = this;
        return (ek4) E0(this.H0.a(imageView, this.G0), null, eVar, yy0.b());
    }

    public final boolean K0(com.bumptech.glide.request.a<?> aVar, d73 d73Var) {
        return !aVar.N() && d73Var.l();
    }

    public e<TranscodeType> L0(j73<TranscodeType> j73Var) {
        if (L()) {
            return g().L0(j73Var);
        }
        this.K0 = null;
        return u0(j73Var);
    }

    public e<TranscodeType> M0(Bitmap bitmap) {
        return T0(bitmap).a(n73.x0(bp0.a));
    }

    public e<TranscodeType> N0(Uri uri) {
        return T0(uri);
    }

    public e<TranscodeType> O0(File file) {
        return T0(file);
    }

    public e<TranscodeType> P0(Integer num) {
        return T0(num).a(n73.z0(ad.c(this.E0)));
    }

    public e<TranscodeType> Q0(Object obj) {
        return T0(obj);
    }

    public e<TranscodeType> R0(String str) {
        return T0(str);
    }

    public e<TranscodeType> S0(byte[] bArr) {
        e<TranscodeType> T0 = T0(bArr);
        if (!T0.M()) {
            T0 = T0.a(n73.x0(bp0.a));
        }
        return !T0.S() ? T0.a(n73.A0(true)) : T0;
    }

    public final e<TranscodeType> T0(Object obj) {
        if (L()) {
            return g().T0(obj);
        }
        this.J0 = obj;
        this.P0 = true;
        return k0();
    }

    public final d73 U0(Object obj, i34<TranscodeType> i34Var, j73<TranscodeType> j73Var, com.bumptech.glide.request.a<?> aVar, RequestCoordinator requestCoordinator, f<?, ? super TranscodeType> fVar, Priority priority, int i, int i2, Executor executor) {
        Context context = this.E0;
        c cVar = this.H0;
        return SingleRequest.z(context, cVar, obj, this.J0, this.G0, aVar, i, i2, priority, i34Var, j73Var, this.K0, requestCoordinator, cVar.f(), fVar.b(), executor);
    }

    public ce1<TranscodeType> V0() {
        return W0(Integer.MIN_VALUE, Integer.MIN_VALUE);
    }

    public ce1<TranscodeType> W0(int i, int i2) {
        com.bumptech.glide.request.c cVar = new com.bumptech.glide.request.c(i, i2);
        return (ce1) G0(cVar, cVar, yy0.a());
    }

    public e<TranscodeType> u0(j73<TranscodeType> j73Var) {
        if (L()) {
            return g().u0(j73Var);
        }
        if (j73Var != null) {
            if (this.K0 == null) {
                this.K0 = new ArrayList();
            }
            this.K0.add(j73Var);
        }
        return k0();
    }

    @Override // com.bumptech.glide.request.a
    /* renamed from: v0 */
    public e<TranscodeType> a(com.bumptech.glide.request.a<?> aVar) {
        wt2.d(aVar);
        return (e) super.a(aVar);
    }

    public final d73 w0(i34<TranscodeType> i34Var, j73<TranscodeType> j73Var, com.bumptech.glide.request.a<?> aVar, Executor executor) {
        return x0(new Object(), i34Var, j73Var, null, this.I0, aVar.C(), aVar.y(), aVar.x(), aVar, executor);
    }

    public final d73 x0(Object obj, i34<TranscodeType> i34Var, j73<TranscodeType> j73Var, RequestCoordinator requestCoordinator, f<?, ? super TranscodeType> fVar, Priority priority, int i, int i2, com.bumptech.glide.request.a<?> aVar, Executor executor) {
        com.bumptech.glide.request.b bVar;
        com.bumptech.glide.request.b bVar2;
        if (this.M0 != null) {
            bVar2 = new com.bumptech.glide.request.b(obj, requestCoordinator);
            bVar = bVar2;
        } else {
            bVar = null;
            bVar2 = requestCoordinator;
        }
        d73 z0 = z0(obj, i34Var, j73Var, bVar2, fVar, priority, i, i2, aVar, executor);
        if (bVar == null) {
            return z0;
        }
        int y = this.M0.y();
        int x = this.M0.x();
        if (mg4.t(i, i2) && !this.M0.W()) {
            y = aVar.y();
            x = aVar.x();
        }
        e<TranscodeType> eVar = this.M0;
        com.bumptech.glide.request.b bVar3 = bVar;
        bVar3.q(z0, eVar.x0(obj, i34Var, j73Var, bVar3, eVar.I0, eVar.C(), y, x, this.M0, executor));
        return bVar3;
    }

    /* JADX WARN: Type inference failed for: r0v5, types: [com.bumptech.glide.request.a] */
    public final d73 z0(Object obj, i34<TranscodeType> i34Var, j73<TranscodeType> j73Var, RequestCoordinator requestCoordinator, f<?, ? super TranscodeType> fVar, Priority priority, int i, int i2, com.bumptech.glide.request.a<?> aVar, Executor executor) {
        Priority B0;
        e<TranscodeType> eVar = this.L0;
        if (eVar != null) {
            if (!this.Q0) {
                f<?, ? super TranscodeType> fVar2 = eVar.O0 ? fVar : eVar.I0;
                if (eVar.O()) {
                    B0 = this.L0.C();
                } else {
                    B0 = B0(priority);
                }
                Priority priority2 = B0;
                int y = this.L0.y();
                int x = this.L0.x();
                if (mg4.t(i, i2) && !this.L0.W()) {
                    y = aVar.y();
                    x = aVar.x();
                }
                com.bumptech.glide.request.d dVar = new com.bumptech.glide.request.d(obj, requestCoordinator);
                d73 U0 = U0(obj, i34Var, j73Var, aVar, dVar, fVar, priority, i, i2, executor);
                this.Q0 = true;
                e<TranscodeType> eVar2 = this.L0;
                d73 x0 = eVar2.x0(obj, i34Var, j73Var, dVar, fVar2, priority2, y, x, eVar2, executor);
                this.Q0 = false;
                dVar.p(U0, x0);
                return dVar;
            }
            throw new IllegalStateException("You cannot use a request as both the main request and a thumbnail, consider using clone() on the request(s) passed to thumbnail()");
        } else if (this.N0 != null) {
            com.bumptech.glide.request.d dVar2 = new com.bumptech.glide.request.d(obj, requestCoordinator);
            dVar2.p(U0(obj, i34Var, j73Var, aVar, dVar2, fVar, priority, i, i2, executor), U0(obj, i34Var, j73Var, aVar.clone().n0(this.N0.floatValue()), dVar2, fVar, B0(priority), i, i2, executor));
            return dVar2;
        } else {
            return U0(obj, i34Var, j73Var, aVar, requestCoordinator, fVar, priority, i, i2, executor);
        }
    }
}
