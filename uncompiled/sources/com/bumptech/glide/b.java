package com.bumptech.glide;

import android.content.Context;
import com.bumptech.glide.a;
import com.bumptech.glide.d;
import defpackage.l73;
import defpackage.p72;
import defpackage.yo0;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/* compiled from: GlideBuilder.java */
/* loaded from: classes.dex */
public final class b {
    public com.bumptech.glide.load.engine.f c;
    public jq d;
    public sh e;
    public k72 f;
    public ig1 g;
    public ig1 h;
    public yo0.a i;
    public p72 j;
    public y50 k;
    public l73.b n;
    public ig1 o;
    public boolean p;
    public List<j73<Object>> q;
    public final Map<Class<?>, f<?, ?>> a = new rh();
    public final d.a b = new d.a();
    public int l = 4;
    public a.InterfaceC0074a m = new a(this);

    /* compiled from: GlideBuilder.java */
    /* loaded from: classes.dex */
    public class a implements a.InterfaceC0074a {
        public a(b bVar) {
        }

        @Override // com.bumptech.glide.a.InterfaceC0074a
        public n73 build() {
            return new n73();
        }
    }

    /* compiled from: GlideBuilder.java */
    /* renamed from: com.bumptech.glide.b$b  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static final class C0075b {
    }

    /* compiled from: GlideBuilder.java */
    /* loaded from: classes.dex */
    public static final class c {
    }

    /* compiled from: GlideBuilder.java */
    /* loaded from: classes.dex */
    public static final class d {
    }

    /* compiled from: GlideBuilder.java */
    /* loaded from: classes.dex */
    public static final class e {
    }

    public com.bumptech.glide.a a(Context context) {
        if (this.g == null) {
            this.g = ig1.g();
        }
        if (this.h == null) {
            this.h = ig1.e();
        }
        if (this.o == null) {
            this.o = ig1.c();
        }
        if (this.j == null) {
            this.j = new p72.a(context).a();
        }
        if (this.k == null) {
            this.k = new pi0();
        }
        if (this.d == null) {
            int b = this.j.b();
            if (b > 0) {
                this.d = new o22(b);
            } else {
                this.d = new kq();
            }
        }
        if (this.e == null) {
            this.e = new n22(this.j.a());
        }
        if (this.f == null) {
            this.f = new w22(this.j.d());
        }
        if (this.i == null) {
            this.i = new cs1(context);
        }
        if (this.c == null) {
            this.c = new com.bumptech.glide.load.engine.f(this.f, this.i, this.h, this.g, ig1.h(), this.o, this.p);
        }
        List<j73<Object>> list = this.q;
        if (list == null) {
            this.q = Collections.emptyList();
        } else {
            this.q = Collections.unmodifiableList(list);
        }
        com.bumptech.glide.d b2 = this.b.b();
        return new com.bumptech.glide.a(context, this.c, this.f, this.d, this.e, new l73(this.n, b2), this.k, this.l, this.m, this.a, this.q, b2);
    }

    public void b(l73.b bVar) {
        this.n = bVar;
    }
}
