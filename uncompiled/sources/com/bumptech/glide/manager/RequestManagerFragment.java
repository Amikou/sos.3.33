package com.bumptech.glide.manager;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.os.Build;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Deprecated
/* loaded from: classes.dex */
public class RequestManagerFragment extends Fragment {
    public final d7 a;
    public final m73 f0;
    public final Set<RequestManagerFragment> g0;
    public k73 h0;
    public RequestManagerFragment i0;
    public Fragment j0;

    /* loaded from: classes.dex */
    public class a implements m73 {
        public a() {
        }

        @Override // defpackage.m73
        public Set<k73> a() {
            Set<RequestManagerFragment> b = RequestManagerFragment.this.b();
            HashSet hashSet = new HashSet(b.size());
            for (RequestManagerFragment requestManagerFragment : b) {
                if (requestManagerFragment.e() != null) {
                    hashSet.add(requestManagerFragment.e());
                }
            }
            return hashSet;
        }

        public String toString() {
            return super.toString() + "{fragment=" + RequestManagerFragment.this + "}";
        }
    }

    public RequestManagerFragment() {
        this(new d7());
    }

    public final void a(RequestManagerFragment requestManagerFragment) {
        this.g0.add(requestManagerFragment);
    }

    @TargetApi(17)
    public Set<RequestManagerFragment> b() {
        if (equals(this.i0)) {
            return Collections.unmodifiableSet(this.g0);
        }
        if (this.i0 != null && Build.VERSION.SDK_INT >= 17) {
            HashSet hashSet = new HashSet();
            for (RequestManagerFragment requestManagerFragment : this.i0.b()) {
                if (g(requestManagerFragment.getParentFragment())) {
                    hashSet.add(requestManagerFragment);
                }
            }
            return Collections.unmodifiableSet(hashSet);
        }
        return Collections.emptySet();
    }

    public d7 c() {
        return this.a;
    }

    @TargetApi(17)
    public final Fragment d() {
        Fragment parentFragment = Build.VERSION.SDK_INT >= 17 ? getParentFragment() : null;
        return parentFragment != null ? parentFragment : this.j0;
    }

    public k73 e() {
        return this.h0;
    }

    public m73 f() {
        return this.f0;
    }

    @TargetApi(17)
    public final boolean g(Fragment fragment) {
        Fragment parentFragment = getParentFragment();
        while (true) {
            Fragment parentFragment2 = fragment.getParentFragment();
            if (parentFragment2 == null) {
                return false;
            }
            if (parentFragment2.equals(parentFragment)) {
                return true;
            }
            fragment = fragment.getParentFragment();
        }
    }

    public final void h(Activity activity) {
        l();
        RequestManagerFragment q = com.bumptech.glide.a.c(activity).k().q(activity);
        this.i0 = q;
        if (equals(q)) {
            return;
        }
        this.i0.a(this);
    }

    public final void i(RequestManagerFragment requestManagerFragment) {
        this.g0.remove(requestManagerFragment);
    }

    public void j(Fragment fragment) {
        this.j0 = fragment;
        if (fragment == null || fragment.getActivity() == null) {
            return;
        }
        h(fragment.getActivity());
    }

    public void k(k73 k73Var) {
        this.h0 = k73Var;
    }

    public final void l() {
        RequestManagerFragment requestManagerFragment = this.i0;
        if (requestManagerFragment != null) {
            requestManagerFragment.i(this);
            this.i0 = null;
        }
    }

    @Override // android.app.Fragment
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            h(activity);
        } catch (IllegalStateException unused) {
        }
    }

    @Override // android.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        this.a.c();
        l();
    }

    @Override // android.app.Fragment
    public void onDetach() {
        super.onDetach();
        l();
    }

    @Override // android.app.Fragment
    public void onStart() {
        super.onStart();
        this.a.d();
    }

    @Override // android.app.Fragment
    public void onStop() {
        super.onStop();
        this.a.e();
    }

    @Override // android.app.Fragment
    public String toString() {
        return super.toString() + "{parent=" + d() + "}";
    }

    @SuppressLint({"ValidFragment"})
    public RequestManagerFragment(d7 d7Var) {
        this.f0 = new a();
        this.g0 = new HashSet();
        this.a = d7Var;
    }
}
