package com.bumptech.glide;

import android.content.Context;
import android.content.ContextWrapper;
import android.widget.ImageView;
import com.bumptech.glide.a;
import java.util.List;
import java.util.Map;

/* compiled from: GlideContext.java */
/* loaded from: classes.dex */
public class c extends ContextWrapper {
    public static final f<?, ?> k = new cf1();
    public final sh a;
    public final Registry b;
    public final fp1 c;
    public final a.InterfaceC0074a d;
    public final List<j73<Object>> e;
    public final Map<Class<?>, f<?, ?>> f;
    public final com.bumptech.glide.load.engine.f g;
    public final d h;
    public final int i;
    public n73 j;

    public c(Context context, sh shVar, Registry registry, fp1 fp1Var, a.InterfaceC0074a interfaceC0074a, Map<Class<?>, f<?, ?>> map, List<j73<Object>> list, com.bumptech.glide.load.engine.f fVar, d dVar, int i) {
        super(context.getApplicationContext());
        this.a = shVar;
        this.b = registry;
        this.c = fp1Var;
        this.d = interfaceC0074a;
        this.e = list;
        this.f = map;
        this.g = fVar;
        this.h = dVar;
        this.i = i;
    }

    public <X> ek4<ImageView, X> a(ImageView imageView, Class<X> cls) {
        return this.c.a(imageView, cls);
    }

    public sh b() {
        return this.a;
    }

    public List<j73<Object>> c() {
        return this.e;
    }

    public synchronized n73 d() {
        if (this.j == null) {
            this.j = this.d.build().X();
        }
        return this.j;
    }

    public <T> f<?, T> e(Class<T> cls) {
        f<?, T> fVar = (f<?, T>) this.f.get(cls);
        if (fVar == null) {
            for (Map.Entry<Class<?>, f<?, ?>> entry : this.f.entrySet()) {
                if (entry.getKey().isAssignableFrom(cls)) {
                    fVar = (f<?, T>) entry.getValue();
                }
            }
        }
        return fVar == null ? (f<?, T>) k : fVar;
    }

    public com.bumptech.glide.load.engine.f f() {
        return this.g;
    }

    public d g() {
        return this.h;
    }

    public int h() {
        return this.i;
    }

    public Registry i() {
        return this.b;
    }
}
