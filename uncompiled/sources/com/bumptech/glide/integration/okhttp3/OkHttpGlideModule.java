package com.bumptech.glide.integration.okhttp3;

import android.content.Context;
import com.bumptech.glide.Registry;
import com.bumptech.glide.integration.okhttp3.b;
import java.io.InputStream;

@Deprecated
/* loaded from: classes.dex */
public class OkHttpGlideModule implements jg1 {
    @Override // defpackage.eh
    public void a(Context context, com.bumptech.glide.b bVar) {
    }

    @Override // defpackage.h63
    public void b(Context context, com.bumptech.glide.a aVar, Registry registry) {
        registry.r(ng1.class, InputStream.class, new b.a());
    }
}
