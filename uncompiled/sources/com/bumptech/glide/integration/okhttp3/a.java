package com.bumptech.glide.integration.okhttp3;

import android.content.Context;
import com.bumptech.glide.Registry;
import com.bumptech.glide.integration.okhttp3.b;
import java.io.InputStream;

/* compiled from: OkHttpLibraryGlideModule.java */
/* loaded from: classes.dex */
public final class a extends gz1 {
    @Override // defpackage.gz1, defpackage.h63
    public void b(Context context, com.bumptech.glide.a aVar, Registry registry) {
        registry.r(ng1.class, InputStream.class, new b.a());
    }
}
