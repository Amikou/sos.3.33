package com.bumptech.glide.integration.okhttp3;

import defpackage.j92;
import java.io.InputStream;
import okhttp3.Call;
import okhttp3.OkHttpClient;

/* compiled from: OkHttpUrlLoader.java */
/* loaded from: classes.dex */
public class b implements j92<ng1, InputStream> {
    public final Call.Factory a;

    /* compiled from: OkHttpUrlLoader.java */
    /* loaded from: classes.dex */
    public static class a implements k92<ng1, InputStream> {
        public static volatile Call.Factory b;
        public final Call.Factory a;

        public a() {
            this(b());
        }

        public static Call.Factory b() {
            if (b == null) {
                synchronized (a.class) {
                    if (b == null) {
                        b = new OkHttpClient();
                    }
                }
            }
            return b;
        }

        @Override // defpackage.k92
        public void a() {
        }

        @Override // defpackage.k92
        public j92<ng1, InputStream> c(qa2 qa2Var) {
            return new b(this.a);
        }

        public a(Call.Factory factory) {
            this.a = factory;
        }
    }

    public b(Call.Factory factory) {
        this.a = factory;
    }

    @Override // defpackage.j92
    /* renamed from: c */
    public j92.a<InputStream> b(ng1 ng1Var, int i, int i2, vn2 vn2Var) {
        return new j92.a<>(ng1Var, new bm2(this.a, ng1Var));
    }

    @Override // defpackage.j92
    /* renamed from: d */
    public boolean a(ng1 ng1Var) {
        return true;
    }
}
