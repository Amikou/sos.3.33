package com.bumptech.glide.request;

import android.graphics.drawable.Drawable;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/* compiled from: RequestFutureTarget.java */
/* loaded from: classes.dex */
public class c<R> implements ce1<R>, j73<R> {
    public static final a o0 = new a();
    public final int a;
    public final int f0;
    public final boolean g0;
    public final a h0;
    public R i0;
    public d73 j0;
    public boolean k0;
    public boolean l0;
    public boolean m0;
    public GlideException n0;

    /* compiled from: RequestFutureTarget.java */
    /* loaded from: classes.dex */
    public static class a {
        public void a(Object obj) {
            obj.notifyAll();
        }

        public void b(Object obj, long j) throws InterruptedException {
            obj.wait(j);
        }
    }

    public c(int i, int i2) {
        this(i, i2, true, o0);
    }

    @Override // defpackage.i34
    public void a(dq3 dq3Var) {
        dq3Var.f(this.a, this.f0);
    }

    @Override // defpackage.pz1
    public void b() {
    }

    @Override // defpackage.i34
    public synchronized void c(d73 d73Var) {
        this.j0 = d73Var;
    }

    @Override // java.util.concurrent.Future
    public boolean cancel(boolean z) {
        synchronized (this) {
            if (isDone()) {
                return false;
            }
            this.k0 = true;
            this.h0.a(this);
            d73 d73Var = null;
            if (z) {
                d73 d73Var2 = this.j0;
                this.j0 = null;
                d73Var = d73Var2;
            }
            if (d73Var != null) {
                d73Var.clear();
            }
            return true;
        }
    }

    @Override // defpackage.pz1
    public void e() {
    }

    @Override // defpackage.i34
    public void f(dq3 dq3Var) {
    }

    @Override // defpackage.i34
    public synchronized void g(Drawable drawable) {
    }

    @Override // java.util.concurrent.Future
    public R get() throws InterruptedException, ExecutionException {
        try {
            return o(null);
        } catch (TimeoutException e) {
            throw new AssertionError(e);
        }
    }

    @Override // defpackage.pz1
    public void h() {
    }

    @Override // defpackage.j73
    public synchronized boolean i(GlideException glideException, Object obj, i34<R> i34Var, boolean z) {
        this.m0 = true;
        this.n0 = glideException;
        this.h0.a(this);
        return false;
    }

    @Override // java.util.concurrent.Future
    public synchronized boolean isCancelled() {
        return this.k0;
    }

    @Override // java.util.concurrent.Future
    public synchronized boolean isDone() {
        boolean z;
        if (!this.k0 && !this.l0) {
            z = this.m0;
        }
        return z;
    }

    @Override // defpackage.i34
    public synchronized void j(R r, hb4<? super R> hb4Var) {
    }

    @Override // defpackage.i34
    public void k(Drawable drawable) {
    }

    @Override // defpackage.i34
    public synchronized d73 l() {
        return this.j0;
    }

    @Override // defpackage.i34
    public void m(Drawable drawable) {
    }

    @Override // defpackage.j73
    public synchronized boolean n(R r, Object obj, i34<R> i34Var, DataSource dataSource, boolean z) {
        this.l0 = true;
        this.i0 = r;
        this.h0.a(this);
        return false;
    }

    public final synchronized R o(Long l) throws ExecutionException, InterruptedException, TimeoutException {
        if (this.g0 && !isDone()) {
            mg4.a();
        }
        if (!this.k0) {
            if (!this.m0) {
                if (this.l0) {
                    return this.i0;
                }
                if (l == null) {
                    this.h0.b(this, 0L);
                } else if (l.longValue() > 0) {
                    long currentTimeMillis = System.currentTimeMillis();
                    long longValue = l.longValue() + currentTimeMillis;
                    while (!isDone() && currentTimeMillis < longValue) {
                        this.h0.b(this, longValue - currentTimeMillis);
                        currentTimeMillis = System.currentTimeMillis();
                    }
                }
                if (!Thread.interrupted()) {
                    if (!this.m0) {
                        if (!this.k0) {
                            if (this.l0) {
                                return this.i0;
                            }
                            throw new TimeoutException();
                        }
                        throw new CancellationException();
                    }
                    throw new ExecutionException(this.n0);
                }
                throw new InterruptedException();
            }
            throw new ExecutionException(this.n0);
        }
        throw new CancellationException();
    }

    public String toString() {
        d73 d73Var;
        String str;
        String str2 = super.toString() + "[status=";
        synchronized (this) {
            d73Var = null;
            if (this.k0) {
                str = "CANCELLED";
            } else if (this.m0) {
                str = "FAILURE";
            } else if (this.l0) {
                str = "SUCCESS";
            } else {
                str = "PENDING";
                d73Var = this.j0;
            }
        }
        if (d73Var != null) {
            return str2 + str + ", request=[" + d73Var + "]]";
        }
        return str2 + str + "]";
    }

    public c(int i, int i2, boolean z, a aVar) {
        this.a = i;
        this.f0 = i2;
        this.g0 = z;
        this.h0 = aVar;
    }

    @Override // java.util.concurrent.Future
    public R get(long j, TimeUnit timeUnit) throws InterruptedException, ExecutionException, TimeoutException {
        return o(Long.valueOf(timeUnit.toMillis(j)));
    }
}
