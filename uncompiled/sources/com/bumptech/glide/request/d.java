package com.bumptech.glide.request;

import com.bumptech.glide.request.RequestCoordinator;

/* compiled from: ThumbnailRequestCoordinator.java */
/* loaded from: classes.dex */
public class d implements RequestCoordinator, d73 {
    public final RequestCoordinator a;
    public final Object b;
    public volatile d73 c;
    public volatile d73 d;
    public RequestCoordinator.RequestState e;
    public RequestCoordinator.RequestState f;
    public boolean g;

    public d(Object obj, RequestCoordinator requestCoordinator) {
        RequestCoordinator.RequestState requestState = RequestCoordinator.RequestState.CLEARED;
        this.e = requestState;
        this.f = requestState;
        this.b = obj;
        this.a = requestCoordinator;
    }

    @Override // com.bumptech.glide.request.RequestCoordinator
    public void a(d73 d73Var) {
        synchronized (this.b) {
            if (!d73Var.equals(this.c)) {
                this.f = RequestCoordinator.RequestState.FAILED;
                return;
            }
            this.e = RequestCoordinator.RequestState.FAILED;
            RequestCoordinator requestCoordinator = this.a;
            if (requestCoordinator != null) {
                requestCoordinator.a(this);
            }
        }
    }

    @Override // com.bumptech.glide.request.RequestCoordinator
    public void b(d73 d73Var) {
        synchronized (this.b) {
            if (d73Var.equals(this.d)) {
                this.f = RequestCoordinator.RequestState.SUCCESS;
                return;
            }
            this.e = RequestCoordinator.RequestState.SUCCESS;
            RequestCoordinator requestCoordinator = this.a;
            if (requestCoordinator != null) {
                requestCoordinator.b(this);
            }
            if (!this.f.isComplete()) {
                this.d.clear();
            }
        }
    }

    @Override // defpackage.d73
    public void c() {
        synchronized (this.b) {
            if (!this.f.isComplete()) {
                this.f = RequestCoordinator.RequestState.PAUSED;
                this.d.c();
            }
            if (!this.e.isComplete()) {
                this.e = RequestCoordinator.RequestState.PAUSED;
                this.c.c();
            }
        }
    }

    @Override // defpackage.d73
    public void clear() {
        synchronized (this.b) {
            this.g = false;
            RequestCoordinator.RequestState requestState = RequestCoordinator.RequestState.CLEARED;
            this.e = requestState;
            this.f = requestState;
            this.d.clear();
            this.c.clear();
        }
    }

    @Override // com.bumptech.glide.request.RequestCoordinator, defpackage.d73
    public boolean d() {
        boolean z;
        synchronized (this.b) {
            z = this.d.d() || this.c.d();
        }
        return z;
    }

    @Override // defpackage.d73
    public boolean e(d73 d73Var) {
        if (d73Var instanceof d) {
            d dVar = (d) d73Var;
            if (this.c == null) {
                if (dVar.c != null) {
                    return false;
                }
            } else if (!this.c.e(dVar.c)) {
                return false;
            }
            if (this.d == null) {
                if (dVar.d != null) {
                    return false;
                }
            } else if (!this.d.e(dVar.d)) {
                return false;
            }
            return true;
        }
        return false;
    }

    @Override // com.bumptech.glide.request.RequestCoordinator
    public RequestCoordinator f() {
        RequestCoordinator f;
        synchronized (this.b) {
            RequestCoordinator requestCoordinator = this.a;
            f = requestCoordinator != null ? requestCoordinator.f() : this;
        }
        return f;
    }

    @Override // com.bumptech.glide.request.RequestCoordinator
    public boolean g(d73 d73Var) {
        boolean z;
        synchronized (this.b) {
            z = m() && d73Var.equals(this.c) && this.e != RequestCoordinator.RequestState.PAUSED;
        }
        return z;
    }

    @Override // com.bumptech.glide.request.RequestCoordinator
    public boolean h(d73 d73Var) {
        boolean z;
        synchronized (this.b) {
            z = n() && d73Var.equals(this.c) && !d();
        }
        return z;
    }

    @Override // com.bumptech.glide.request.RequestCoordinator
    public boolean i(d73 d73Var) {
        boolean z;
        synchronized (this.b) {
            z = o() && (d73Var.equals(this.c) || this.e != RequestCoordinator.RequestState.SUCCESS);
        }
        return z;
    }

    @Override // defpackage.d73
    public boolean isRunning() {
        boolean z;
        synchronized (this.b) {
            z = this.e == RequestCoordinator.RequestState.RUNNING;
        }
        return z;
    }

    @Override // defpackage.d73
    public boolean j() {
        boolean z;
        synchronized (this.b) {
            z = this.e == RequestCoordinator.RequestState.CLEARED;
        }
        return z;
    }

    @Override // defpackage.d73
    public void k() {
        synchronized (this.b) {
            this.g = true;
            if (this.e != RequestCoordinator.RequestState.SUCCESS) {
                RequestCoordinator.RequestState requestState = this.f;
                RequestCoordinator.RequestState requestState2 = RequestCoordinator.RequestState.RUNNING;
                if (requestState != requestState2) {
                    this.f = requestState2;
                    this.d.k();
                }
            }
            if (this.g) {
                RequestCoordinator.RequestState requestState3 = this.e;
                RequestCoordinator.RequestState requestState4 = RequestCoordinator.RequestState.RUNNING;
                if (requestState3 != requestState4) {
                    this.e = requestState4;
                    this.c.k();
                }
            }
            this.g = false;
        }
    }

    @Override // defpackage.d73
    public boolean l() {
        boolean z;
        synchronized (this.b) {
            z = this.e == RequestCoordinator.RequestState.SUCCESS;
        }
        return z;
    }

    public final boolean m() {
        RequestCoordinator requestCoordinator = this.a;
        return requestCoordinator == null || requestCoordinator.g(this);
    }

    public final boolean n() {
        RequestCoordinator requestCoordinator = this.a;
        return requestCoordinator == null || requestCoordinator.h(this);
    }

    public final boolean o() {
        RequestCoordinator requestCoordinator = this.a;
        return requestCoordinator == null || requestCoordinator.i(this);
    }

    public void p(d73 d73Var, d73 d73Var2) {
        this.c = d73Var;
        this.d = d73Var2;
    }
}
