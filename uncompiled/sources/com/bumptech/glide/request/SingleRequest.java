package com.bumptech.glide.request;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import com.bumptech.glide.Priority;
import com.bumptech.glide.b;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.engine.f;
import java.util.List;
import java.util.concurrent.Executor;

/* loaded from: classes.dex */
public final class SingleRequest<R> implements d73, dq3, u73 {
    public static final boolean E = Log.isLoggable("GlideRequest", 2);
    public int A;
    public int B;
    public boolean C;
    public RuntimeException D;
    public int a;
    public final String b;
    public final et3 c;
    public final Object d;
    public final j73<R> e;
    public final RequestCoordinator f;
    public final Context g;
    public final com.bumptech.glide.c h;
    public final Object i;
    public final Class<R> j;
    public final a<?> k;
    public final int l;
    public final int m;
    public final Priority n;
    public final i34<R> o;
    public final List<j73<R>> p;
    public final ib4<? super R> q;
    public final Executor r;
    public s73<R> s;
    public f.d t;
    public long u;
    public volatile f v;
    public Status w;
    public Drawable x;
    public Drawable y;
    public Drawable z;

    /* loaded from: classes.dex */
    public enum Status {
        PENDING,
        RUNNING,
        WAITING_FOR_SIZE,
        COMPLETE,
        FAILED,
        CLEARED
    }

    public SingleRequest(Context context, com.bumptech.glide.c cVar, Object obj, Object obj2, Class<R> cls, a<?> aVar, int i, int i2, Priority priority, i34<R> i34Var, j73<R> j73Var, List<j73<R>> list, RequestCoordinator requestCoordinator, f fVar, ib4<? super R> ib4Var, Executor executor) {
        this.b = E ? String.valueOf(super.hashCode()) : null;
        this.c = et3.a();
        this.d = obj;
        this.g = context;
        this.h = cVar;
        this.i = obj2;
        this.j = cls;
        this.k = aVar;
        this.l = i;
        this.m = i2;
        this.n = priority;
        this.o = i34Var;
        this.e = j73Var;
        this.p = list;
        this.f = requestCoordinator;
        this.v = fVar;
        this.q = ib4Var;
        this.r = executor;
        this.w = Status.PENDING;
        if (this.D == null && cVar.g().a(b.d.class)) {
            this.D = new RuntimeException("Glide request origin trace");
        }
    }

    public static int w(int i, float f) {
        return i == Integer.MIN_VALUE ? i : Math.round(f * i);
    }

    public static <R> SingleRequest<R> z(Context context, com.bumptech.glide.c cVar, Object obj, Object obj2, Class<R> cls, a<?> aVar, int i, int i2, Priority priority, i34<R> i34Var, j73<R> j73Var, List<j73<R>> list, RequestCoordinator requestCoordinator, f fVar, ib4<? super R> ib4Var, Executor executor) {
        return new SingleRequest<>(context, cVar, obj, obj2, cls, aVar, i, i2, priority, i34Var, j73Var, list, requestCoordinator, fVar, ib4Var, executor);
    }

    public final void A(GlideException glideException, int i) {
        boolean z;
        this.c.c();
        synchronized (this.d) {
            glideException.setOrigin(this.D);
            int h = this.h.h();
            if (h <= i) {
                StringBuilder sb = new StringBuilder();
                sb.append("Load failed for ");
                sb.append(this.i);
                sb.append(" with size [");
                sb.append(this.A);
                sb.append("x");
                sb.append(this.B);
                sb.append("]");
                if (h <= 4) {
                    glideException.logRootCauses("Glide");
                }
            }
            this.t = null;
            this.w = Status.FAILED;
            boolean z2 = true;
            this.C = true;
            List<j73<R>> list = this.p;
            if (list != null) {
                z = false;
                for (j73<R> j73Var : list) {
                    z |= j73Var.i(glideException, this.i, this.o, t());
                }
            } else {
                z = false;
            }
            j73<R> j73Var2 = this.e;
            if (j73Var2 == null || !j73Var2.i(glideException, this.i, this.o, t())) {
                z2 = false;
            }
            if (!(z | z2)) {
                C();
            }
            this.C = false;
            x();
            mg1.f("GlideRequest", this.a);
        }
    }

    public final void B(s73<R> s73Var, R r, DataSource dataSource, boolean z) {
        boolean z2;
        boolean t = t();
        this.w = Status.COMPLETE;
        this.s = s73Var;
        if (this.h.h() <= 3) {
            StringBuilder sb = new StringBuilder();
            sb.append("Finished loading ");
            sb.append(r.getClass().getSimpleName());
            sb.append(" from ");
            sb.append(dataSource);
            sb.append(" for ");
            sb.append(this.i);
            sb.append(" with size [");
            sb.append(this.A);
            sb.append("x");
            sb.append(this.B);
            sb.append("] in ");
            sb.append(t12.a(this.u));
            sb.append(" ms");
        }
        boolean z3 = true;
        this.C = true;
        try {
            List<j73<R>> list = this.p;
            if (list != null) {
                z2 = false;
                for (j73<R> j73Var : list) {
                    z2 |= j73Var.n(r, this.i, this.o, dataSource, t);
                }
            } else {
                z2 = false;
            }
            j73<R> j73Var2 = this.e;
            if (j73Var2 == null || !j73Var2.n(r, this.i, this.o, dataSource, t)) {
                z3 = false;
            }
            if (!(z3 | z2)) {
                this.o.j(r, this.q.a(dataSource, t));
            }
            this.C = false;
            y();
            mg1.f("GlideRequest", this.a);
        } catch (Throwable th) {
            this.C = false;
            throw th;
        }
    }

    public final void C() {
        if (m()) {
            Drawable r = this.i == null ? r() : null;
            if (r == null) {
                r = q();
            }
            if (r == null) {
                r = s();
            }
            this.o.g(r);
        }
    }

    @Override // defpackage.u73
    public void a(GlideException glideException) {
        A(glideException, 5);
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // defpackage.u73
    public void b(s73<?> s73Var, DataSource dataSource, boolean z) {
        this.c.c();
        s73<?> s73Var2 = null;
        try {
            synchronized (this.d) {
                try {
                    this.t = null;
                    if (s73Var == null) {
                        a(new GlideException("Expected to receive a Resource<R> with an object of " + this.j + " inside, but instead got null."));
                        return;
                    }
                    Object obj = s73Var.get();
                    try {
                        if (obj != null && this.j.isAssignableFrom(obj.getClass())) {
                            if (!n()) {
                                this.s = null;
                                this.w = Status.COMPLETE;
                                mg1.f("GlideRequest", this.a);
                                this.v.k(s73Var);
                                return;
                            }
                            B(s73Var, obj, dataSource, z);
                            return;
                        }
                        this.s = null;
                        StringBuilder sb = new StringBuilder();
                        sb.append("Expected to receive an object of ");
                        sb.append(this.j);
                        sb.append(" but instead got ");
                        sb.append(obj != null ? obj.getClass() : "");
                        sb.append("{");
                        sb.append(obj);
                        sb.append("} inside Resource{");
                        sb.append(s73Var);
                        sb.append("}.");
                        sb.append(obj != null ? "" : " To indicate failure return a null Resource object, rather than a Resource object containing null data.");
                        a(new GlideException(sb.toString()));
                        this.v.k(s73Var);
                    } catch (Throwable th) {
                        s73Var2 = s73Var;
                        th = th;
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                }
            }
        } catch (Throwable th3) {
            if (s73Var2 != null) {
                this.v.k(s73Var2);
            }
            throw th3;
        }
    }

    @Override // defpackage.d73
    public void c() {
        synchronized (this.d) {
            if (isRunning()) {
                clear();
            }
        }
    }

    @Override // defpackage.d73
    public void clear() {
        synchronized (this.d) {
            h();
            this.c.c();
            Status status = this.w;
            Status status2 = Status.CLEARED;
            if (status == status2) {
                return;
            }
            o();
            s73<R> s73Var = this.s;
            if (s73Var != null) {
                this.s = null;
            } else {
                s73Var = null;
            }
            if (i()) {
                this.o.m(s());
            }
            mg1.f("GlideRequest", this.a);
            this.w = status2;
            if (s73Var != null) {
                this.v.k(s73Var);
            }
        }
    }

    @Override // defpackage.d73
    public boolean d() {
        boolean z;
        synchronized (this.d) {
            z = this.w == Status.COMPLETE;
        }
        return z;
    }

    @Override // defpackage.d73
    public boolean e(d73 d73Var) {
        int i;
        int i2;
        Object obj;
        Class<R> cls;
        a<?> aVar;
        Priority priority;
        int size;
        int i3;
        int i4;
        Object obj2;
        Class<R> cls2;
        a<?> aVar2;
        Priority priority2;
        int size2;
        if (d73Var instanceof SingleRequest) {
            synchronized (this.d) {
                i = this.l;
                i2 = this.m;
                obj = this.i;
                cls = this.j;
                aVar = this.k;
                priority = this.n;
                List<j73<R>> list = this.p;
                size = list != null ? list.size() : 0;
            }
            SingleRequest singleRequest = (SingleRequest) d73Var;
            synchronized (singleRequest.d) {
                i3 = singleRequest.l;
                i4 = singleRequest.m;
                obj2 = singleRequest.i;
                cls2 = singleRequest.j;
                aVar2 = singleRequest.k;
                priority2 = singleRequest.n;
                List<j73<R>> list2 = singleRequest.p;
                size2 = list2 != null ? list2.size() : 0;
            }
            return i == i3 && i2 == i4 && mg4.c(obj, obj2) && cls.equals(cls2) && aVar.equals(aVar2) && priority == priority2 && size == size2;
        }
        return false;
    }

    @Override // defpackage.dq3
    public void f(int i, int i2) {
        Object obj;
        this.c.c();
        Object obj2 = this.d;
        synchronized (obj2) {
            try {
                try {
                    boolean z = E;
                    if (z) {
                        v("Got onSizeReady in " + t12.a(this.u));
                    }
                    if (this.w == Status.WAITING_FOR_SIZE) {
                        Status status = Status.RUNNING;
                        this.w = status;
                        float G = this.k.G();
                        this.A = w(i, G);
                        this.B = w(i2, G);
                        if (z) {
                            v("finished setup for calling load in " + t12.a(this.u));
                        }
                        obj = obj2;
                        try {
                            this.t = this.v.f(this.h, this.i, this.k.E(), this.A, this.B, this.k.D(), this.j, this.n, this.k.o(), this.k.I(), this.k.U(), this.k.P(), this.k.v(), this.k.N(), this.k.K(), this.k.J(), this.k.u(), this, this.r);
                            if (this.w != status) {
                                this.t = null;
                            }
                            if (z) {
                                v("finished onSizeReady in " + t12.a(this.u));
                            }
                        } catch (Throwable th) {
                            th = th;
                            throw th;
                        }
                    }
                } catch (Throwable th2) {
                    th = th2;
                    obj = obj2;
                }
            } catch (Throwable th3) {
                th = th3;
            }
        }
    }

    @Override // defpackage.u73
    public Object g() {
        this.c.c();
        return this.d;
    }

    public final void h() {
        if (this.C) {
            throw new IllegalStateException("You can't start or clear loads in RequestListener or Target callbacks. If you're trying to start a fallback request when a load fails, use RequestBuilder#error(RequestBuilder). Otherwise consider posting your into() or clear() calls to the main thread using a Handler instead.");
        }
    }

    public final boolean i() {
        RequestCoordinator requestCoordinator = this.f;
        return requestCoordinator == null || requestCoordinator.g(this);
    }

    @Override // defpackage.d73
    public boolean isRunning() {
        boolean z;
        synchronized (this.d) {
            Status status = this.w;
            z = status == Status.RUNNING || status == Status.WAITING_FOR_SIZE;
        }
        return z;
    }

    @Override // defpackage.d73
    public boolean j() {
        boolean z;
        synchronized (this.d) {
            z = this.w == Status.CLEARED;
        }
        return z;
    }

    @Override // defpackage.d73
    public void k() {
        synchronized (this.d) {
            h();
            this.c.c();
            this.u = t12.b();
            Object obj = this.i;
            if (obj == null) {
                if (mg4.t(this.l, this.m)) {
                    this.A = this.l;
                    this.B = this.m;
                }
                A(new GlideException("Received null model"), r() == null ? 5 : 3);
                return;
            }
            Status status = this.w;
            Status status2 = Status.RUNNING;
            if (status != status2) {
                if (status == Status.COMPLETE) {
                    b(this.s, DataSource.MEMORY_CACHE, false);
                    return;
                }
                p(obj);
                this.a = mg1.b("GlideRequest");
                Status status3 = Status.WAITING_FOR_SIZE;
                this.w = status3;
                if (mg4.t(this.l, this.m)) {
                    f(this.l, this.m);
                } else {
                    this.o.a(this);
                }
                Status status4 = this.w;
                if ((status4 == status2 || status4 == status3) && m()) {
                    this.o.k(s());
                }
                if (E) {
                    v("finished run method in " + t12.a(this.u));
                }
                return;
            }
            throw new IllegalArgumentException("Cannot restart a running request");
        }
    }

    @Override // defpackage.d73
    public boolean l() {
        boolean z;
        synchronized (this.d) {
            z = this.w == Status.COMPLETE;
        }
        return z;
    }

    public final boolean m() {
        RequestCoordinator requestCoordinator = this.f;
        return requestCoordinator == null || requestCoordinator.h(this);
    }

    public final boolean n() {
        RequestCoordinator requestCoordinator = this.f;
        return requestCoordinator == null || requestCoordinator.i(this);
    }

    public final void o() {
        h();
        this.c.c();
        this.o.f(this);
        f.d dVar = this.t;
        if (dVar != null) {
            dVar.a();
            this.t = null;
        }
    }

    public final void p(Object obj) {
        List<j73<R>> list = this.p;
        if (list == null) {
            return;
        }
        for (j73<R> j73Var : list) {
            if (j73Var instanceof a11) {
                ((a11) j73Var).a(obj);
            }
        }
    }

    public final Drawable q() {
        if (this.x == null) {
            Drawable r = this.k.r();
            this.x = r;
            if (r == null && this.k.p() > 0) {
                this.x = u(this.k.p());
            }
        }
        return this.x;
    }

    public final Drawable r() {
        if (this.z == null) {
            Drawable s = this.k.s();
            this.z = s;
            if (s == null && this.k.t() > 0) {
                this.z = u(this.k.t());
            }
        }
        return this.z;
    }

    public final Drawable s() {
        if (this.y == null) {
            Drawable A = this.k.A();
            this.y = A;
            if (A == null && this.k.B() > 0) {
                this.y = u(this.k.B());
            }
        }
        return this.y;
    }

    public final boolean t() {
        RequestCoordinator requestCoordinator = this.f;
        return requestCoordinator == null || !requestCoordinator.f().d();
    }

    public String toString() {
        Object obj;
        Class<R> cls;
        synchronized (this.d) {
            obj = this.i;
            cls = this.j;
        }
        return super.toString() + "[model=" + obj + ", transcodeClass=" + cls + "]";
    }

    public final Drawable u(int i) {
        return tq0.a(this.h, i, this.k.H() != null ? this.k.H() : this.g.getTheme());
    }

    public final void v(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(" this: ");
        sb.append(this.b);
    }

    public final void x() {
        RequestCoordinator requestCoordinator = this.f;
        if (requestCoordinator != null) {
            requestCoordinator.a(this);
        }
    }

    public final void y() {
        RequestCoordinator requestCoordinator = this.f;
        if (requestCoordinator != null) {
            requestCoordinator.b(this);
        }
    }
}
