package com.bumptech.glide.request;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.resource.bitmap.DownsampleStrategy;
import com.bumptech.glide.request.a;
import com.github.mikephil.charting.utils.Utils;
import java.util.Map;
import okhttp3.internal.http2.Http2;

/* compiled from: BaseRequestOptions.java */
/* loaded from: classes.dex */
public abstract class a<T extends a<T>> implements Cloneable {
    public boolean A0;
    public boolean B0;
    public boolean D0;
    public int a;
    public Drawable i0;
    public int j0;
    public Drawable k0;
    public int l0;
    public boolean q0;
    public Drawable s0;
    public int t0;
    public boolean x0;
    public Resources.Theme y0;
    public boolean z0;
    public float f0 = 1.0f;
    public bp0 g0 = bp0.c;
    public Priority h0 = Priority.NORMAL;
    public boolean m0 = true;
    public int n0 = -1;
    public int o0 = -1;
    public fx1 p0 = uu0.c();
    public boolean r0 = true;
    public vn2 u0 = new vn2();
    public Map<Class<?>, za4<?>> v0 = new yt();
    public Class<?> w0 = Object.class;
    public boolean C0 = true;

    public static boolean R(int i, int i2) {
        return (i & i2) != 0;
    }

    public final Drawable A() {
        return this.k0;
    }

    public final int B() {
        return this.l0;
    }

    public final Priority C() {
        return this.h0;
    }

    public final Class<?> D() {
        return this.w0;
    }

    public final fx1 E() {
        return this.p0;
    }

    public final float G() {
        return this.f0;
    }

    public final Resources.Theme H() {
        return this.y0;
    }

    public final Map<Class<?>, za4<?>> I() {
        return this.v0;
    }

    public final boolean J() {
        return this.D0;
    }

    public final boolean K() {
        return this.A0;
    }

    public final boolean L() {
        return this.z0;
    }

    public final boolean M() {
        return Q(4);
    }

    public final boolean N() {
        return this.m0;
    }

    public final boolean O() {
        return Q(8);
    }

    public boolean P() {
        return this.C0;
    }

    public final boolean Q(int i) {
        return R(this.a, i);
    }

    public final boolean S() {
        return Q(256);
    }

    public final boolean T() {
        return this.r0;
    }

    public final boolean U() {
        return this.q0;
    }

    public final boolean V() {
        return Q(2048);
    }

    public final boolean W() {
        return mg4.t(this.o0, this.n0);
    }

    public T X() {
        this.x0 = true;
        return j0();
    }

    public T Y() {
        return c0(DownsampleStrategy.c, new vw());
    }

    public T Z() {
        return b0(DownsampleStrategy.b, new ww());
    }

    public T a(a<?> aVar) {
        if (this.z0) {
            return (T) clone().a(aVar);
        }
        if (R(aVar.a, 2)) {
            this.f0 = aVar.f0;
        }
        if (R(aVar.a, 262144)) {
            this.A0 = aVar.A0;
        }
        if (R(aVar.a, 1048576)) {
            this.D0 = aVar.D0;
        }
        if (R(aVar.a, 4)) {
            this.g0 = aVar.g0;
        }
        if (R(aVar.a, 8)) {
            this.h0 = aVar.h0;
        }
        if (R(aVar.a, 16)) {
            this.i0 = aVar.i0;
            this.j0 = 0;
            this.a &= -33;
        }
        if (R(aVar.a, 32)) {
            this.j0 = aVar.j0;
            this.i0 = null;
            this.a &= -17;
        }
        if (R(aVar.a, 64)) {
            this.k0 = aVar.k0;
            this.l0 = 0;
            this.a &= -129;
        }
        if (R(aVar.a, 128)) {
            this.l0 = aVar.l0;
            this.k0 = null;
            this.a &= -65;
        }
        if (R(aVar.a, 256)) {
            this.m0 = aVar.m0;
        }
        if (R(aVar.a, RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN)) {
            this.o0 = aVar.o0;
            this.n0 = aVar.n0;
        }
        if (R(aVar.a, RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE)) {
            this.p0 = aVar.p0;
        }
        if (R(aVar.a, 4096)) {
            this.w0 = aVar.w0;
        }
        if (R(aVar.a, 8192)) {
            this.s0 = aVar.s0;
            this.t0 = 0;
            this.a &= -16385;
        }
        if (R(aVar.a, Http2.INITIAL_MAX_FRAME_SIZE)) {
            this.t0 = aVar.t0;
            this.s0 = null;
            this.a &= -8193;
        }
        if (R(aVar.a, 32768)) {
            this.y0 = aVar.y0;
        }
        if (R(aVar.a, 65536)) {
            this.r0 = aVar.r0;
        }
        if (R(aVar.a, 131072)) {
            this.q0 = aVar.q0;
        }
        if (R(aVar.a, 2048)) {
            this.v0.putAll(aVar.v0);
            this.C0 = aVar.C0;
        }
        if (R(aVar.a, 524288)) {
            this.B0 = aVar.B0;
        }
        if (!this.r0) {
            this.v0.clear();
            int i = this.a & (-2049);
            this.a = i;
            this.q0 = false;
            this.a = i & (-131073);
            this.C0 = true;
        }
        this.a |= aVar.a;
        this.u0.d(aVar.u0);
        return k0();
    }

    public T a0() {
        return b0(DownsampleStrategy.a, new c61());
    }

    public T b() {
        if (this.x0 && !this.z0) {
            throw new IllegalStateException("You cannot auto lock an already locked options object, try clone() first");
        }
        this.z0 = true;
        return X();
    }

    public final T b0(DownsampleStrategy downsampleStrategy, za4<Bitmap> za4Var) {
        return h0(downsampleStrategy, za4Var, false);
    }

    public final T c0(DownsampleStrategy downsampleStrategy, za4<Bitmap> za4Var) {
        if (this.z0) {
            return (T) clone().c0(downsampleStrategy, za4Var);
        }
        k(downsampleStrategy);
        return q0(za4Var, false);
    }

    public T d() {
        return r0(DownsampleStrategy.c, new vw());
    }

    public T d0(int i, int i2) {
        if (this.z0) {
            return (T) clone().d0(i, i2);
        }
        this.o0 = i;
        this.n0 = i2;
        this.a |= RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN;
        return k0();
    }

    public T e() {
        return g0(DownsampleStrategy.b, new ww());
    }

    public T e0(int i) {
        if (this.z0) {
            return (T) clone().e0(i);
        }
        this.l0 = i;
        int i2 = this.a | 128;
        this.a = i2;
        this.k0 = null;
        this.a = i2 & (-65);
        return k0();
    }

    public boolean equals(Object obj) {
        if (obj instanceof a) {
            a aVar = (a) obj;
            return Float.compare(aVar.f0, this.f0) == 0 && this.j0 == aVar.j0 && mg4.d(this.i0, aVar.i0) && this.l0 == aVar.l0 && mg4.d(this.k0, aVar.k0) && this.t0 == aVar.t0 && mg4.d(this.s0, aVar.s0) && this.m0 == aVar.m0 && this.n0 == aVar.n0 && this.o0 == aVar.o0 && this.q0 == aVar.q0 && this.r0 == aVar.r0 && this.A0 == aVar.A0 && this.B0 == aVar.B0 && this.g0.equals(aVar.g0) && this.h0 == aVar.h0 && this.u0.equals(aVar.u0) && this.v0.equals(aVar.v0) && this.w0.equals(aVar.w0) && mg4.d(this.p0, aVar.p0) && mg4.d(this.y0, aVar.y0);
        }
        return false;
    }

    public T f() {
        return r0(DownsampleStrategy.b, new uy());
    }

    public T f0(Priority priority) {
        if (this.z0) {
            return (T) clone().f0(priority);
        }
        this.h0 = (Priority) wt2.d(priority);
        this.a |= 8;
        return k0();
    }

    @Override // 
    /* renamed from: g */
    public T clone() {
        try {
            T t = (T) super.clone();
            vn2 vn2Var = new vn2();
            t.u0 = vn2Var;
            vn2Var.d(this.u0);
            yt ytVar = new yt();
            t.v0 = ytVar;
            ytVar.putAll(this.v0);
            t.x0 = false;
            t.z0 = false;
            return t;
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    public final T g0(DownsampleStrategy downsampleStrategy, za4<Bitmap> za4Var) {
        return h0(downsampleStrategy, za4Var, true);
    }

    public T h(Class<?> cls) {
        if (this.z0) {
            return (T) clone().h(cls);
        }
        this.w0 = (Class) wt2.d(cls);
        this.a |= 4096;
        return k0();
    }

    public final T h0(DownsampleStrategy downsampleStrategy, za4<Bitmap> za4Var, boolean z) {
        T c0;
        if (z) {
            c0 = r0(downsampleStrategy, za4Var);
        } else {
            c0 = c0(downsampleStrategy, za4Var);
        }
        c0.C0 = true;
        return c0;
    }

    public int hashCode() {
        return mg4.o(this.y0, mg4.o(this.p0, mg4.o(this.w0, mg4.o(this.v0, mg4.o(this.u0, mg4.o(this.h0, mg4.o(this.g0, mg4.p(this.B0, mg4.p(this.A0, mg4.p(this.r0, mg4.p(this.q0, mg4.n(this.o0, mg4.n(this.n0, mg4.p(this.m0, mg4.o(this.s0, mg4.n(this.t0, mg4.o(this.k0, mg4.n(this.l0, mg4.o(this.i0, mg4.n(this.j0, mg4.l(this.f0)))))))))))))))))))));
    }

    public T j(bp0 bp0Var) {
        if (this.z0) {
            return (T) clone().j(bp0Var);
        }
        this.g0 = (bp0) wt2.d(bp0Var);
        this.a |= 4;
        return k0();
    }

    public final T j0() {
        return this;
    }

    public T k(DownsampleStrategy downsampleStrategy) {
        return l0(DownsampleStrategy.f, wt2.d(downsampleStrategy));
    }

    public final T k0() {
        if (!this.x0) {
            return j0();
        }
        throw new IllegalStateException("You cannot modify locked T, consider clone()");
    }

    public T l(int i) {
        if (this.z0) {
            return (T) clone().l(i);
        }
        this.j0 = i;
        int i2 = this.a | 32;
        this.a = i2;
        this.i0 = null;
        this.a = i2 & (-17);
        return k0();
    }

    public <Y> T l0(mn2<Y> mn2Var, Y y) {
        if (this.z0) {
            return (T) clone().l0(mn2Var, y);
        }
        wt2.d(mn2Var);
        wt2.d(y);
        this.u0.e(mn2Var, y);
        return k0();
    }

    public T m0(fx1 fx1Var) {
        if (this.z0) {
            return (T) clone().m0(fx1Var);
        }
        this.p0 = (fx1) wt2.d(fx1Var);
        this.a |= RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE;
        return k0();
    }

    public T n0(float f) {
        if (this.z0) {
            return (T) clone().n0(f);
        }
        if (f >= Utils.FLOAT_EPSILON && f <= 1.0f) {
            this.f0 = f;
            this.a |= 2;
            return k0();
        }
        throw new IllegalArgumentException("sizeMultiplier must be between 0 and 1");
    }

    public final bp0 o() {
        return this.g0;
    }

    public T o0(boolean z) {
        if (this.z0) {
            return (T) clone().o0(true);
        }
        this.m0 = !z;
        this.a |= 256;
        return k0();
    }

    public final int p() {
        return this.j0;
    }

    public T p0(za4<Bitmap> za4Var) {
        return q0(za4Var, true);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public T q0(za4<Bitmap> za4Var, boolean z) {
        if (this.z0) {
            return (T) clone().q0(za4Var, z);
        }
        ar0 ar0Var = new ar0(za4Var, z);
        s0(Bitmap.class, za4Var, z);
        s0(Drawable.class, ar0Var, z);
        s0(BitmapDrawable.class, ar0Var.c(), z);
        s0(uf1.class, new yf1(za4Var), z);
        return k0();
    }

    public final Drawable r() {
        return this.i0;
    }

    public final T r0(DownsampleStrategy downsampleStrategy, za4<Bitmap> za4Var) {
        if (this.z0) {
            return (T) clone().r0(downsampleStrategy, za4Var);
        }
        k(downsampleStrategy);
        return p0(za4Var);
    }

    public final Drawable s() {
        return this.s0;
    }

    public <Y> T s0(Class<Y> cls, za4<Y> za4Var, boolean z) {
        if (this.z0) {
            return (T) clone().s0(cls, za4Var, z);
        }
        wt2.d(cls);
        wt2.d(za4Var);
        this.v0.put(cls, za4Var);
        int i = this.a | 2048;
        this.a = i;
        this.r0 = true;
        int i2 = i | 65536;
        this.a = i2;
        this.C0 = false;
        if (z) {
            this.a = i2 | 131072;
            this.q0 = true;
        }
        return k0();
    }

    public final int t() {
        return this.t0;
    }

    public T t0(boolean z) {
        if (this.z0) {
            return (T) clone().t0(z);
        }
        this.D0 = z;
        this.a |= 1048576;
        return k0();
    }

    public final boolean u() {
        return this.B0;
    }

    public final vn2 v() {
        return this.u0;
    }

    public final int x() {
        return this.n0;
    }

    public final int y() {
        return this.o0;
    }
}
