package com.bumptech.glide.request;

/* loaded from: classes.dex */
public interface RequestCoordinator {

    /* loaded from: classes.dex */
    public enum RequestState {
        RUNNING(false),
        PAUSED(false),
        CLEARED(false),
        SUCCESS(true),
        FAILED(true);
        
        private final boolean isComplete;

        RequestState(boolean z) {
            this.isComplete = z;
        }

        public boolean isComplete() {
            return this.isComplete;
        }
    }

    void a(d73 d73Var);

    void b(d73 d73Var);

    boolean d();

    RequestCoordinator f();

    boolean g(d73 d73Var);

    boolean h(d73 d73Var);

    boolean i(d73 d73Var);
}
