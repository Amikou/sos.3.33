package com.bumptech.glide.request;

import com.bumptech.glide.request.RequestCoordinator;

/* compiled from: ErrorRequestCoordinator.java */
/* loaded from: classes.dex */
public final class b implements RequestCoordinator, d73 {
    public final Object a;
    public final RequestCoordinator b;
    public volatile d73 c;
    public volatile d73 d;
    public RequestCoordinator.RequestState e;
    public RequestCoordinator.RequestState f;

    public b(Object obj, RequestCoordinator requestCoordinator) {
        RequestCoordinator.RequestState requestState = RequestCoordinator.RequestState.CLEARED;
        this.e = requestState;
        this.f = requestState;
        this.a = obj;
        this.b = requestCoordinator;
    }

    @Override // com.bumptech.glide.request.RequestCoordinator
    public void a(d73 d73Var) {
        synchronized (this.a) {
            if (!d73Var.equals(this.d)) {
                this.e = RequestCoordinator.RequestState.FAILED;
                RequestCoordinator.RequestState requestState = this.f;
                RequestCoordinator.RequestState requestState2 = RequestCoordinator.RequestState.RUNNING;
                if (requestState != requestState2) {
                    this.f = requestState2;
                    this.d.k();
                }
                return;
            }
            this.f = RequestCoordinator.RequestState.FAILED;
            RequestCoordinator requestCoordinator = this.b;
            if (requestCoordinator != null) {
                requestCoordinator.a(this);
            }
        }
    }

    @Override // com.bumptech.glide.request.RequestCoordinator
    public void b(d73 d73Var) {
        synchronized (this.a) {
            if (d73Var.equals(this.c)) {
                this.e = RequestCoordinator.RequestState.SUCCESS;
            } else if (d73Var.equals(this.d)) {
                this.f = RequestCoordinator.RequestState.SUCCESS;
            }
            RequestCoordinator requestCoordinator = this.b;
            if (requestCoordinator != null) {
                requestCoordinator.b(this);
            }
        }
    }

    @Override // defpackage.d73
    public void c() {
        synchronized (this.a) {
            RequestCoordinator.RequestState requestState = this.e;
            RequestCoordinator.RequestState requestState2 = RequestCoordinator.RequestState.RUNNING;
            if (requestState == requestState2) {
                this.e = RequestCoordinator.RequestState.PAUSED;
                this.c.c();
            }
            if (this.f == requestState2) {
                this.f = RequestCoordinator.RequestState.PAUSED;
                this.d.c();
            }
        }
    }

    @Override // defpackage.d73
    public void clear() {
        synchronized (this.a) {
            RequestCoordinator.RequestState requestState = RequestCoordinator.RequestState.CLEARED;
            this.e = requestState;
            this.c.clear();
            if (this.f != requestState) {
                this.f = requestState;
                this.d.clear();
            }
        }
    }

    @Override // com.bumptech.glide.request.RequestCoordinator, defpackage.d73
    public boolean d() {
        boolean z;
        synchronized (this.a) {
            z = this.c.d() || this.d.d();
        }
        return z;
    }

    @Override // defpackage.d73
    public boolean e(d73 d73Var) {
        if (d73Var instanceof b) {
            b bVar = (b) d73Var;
            return this.c.e(bVar.c) && this.d.e(bVar.d);
        }
        return false;
    }

    @Override // com.bumptech.glide.request.RequestCoordinator
    public RequestCoordinator f() {
        RequestCoordinator f;
        synchronized (this.a) {
            RequestCoordinator requestCoordinator = this.b;
            f = requestCoordinator != null ? requestCoordinator.f() : this;
        }
        return f;
    }

    @Override // com.bumptech.glide.request.RequestCoordinator
    public boolean g(d73 d73Var) {
        boolean z;
        synchronized (this.a) {
            z = n() && m(d73Var);
        }
        return z;
    }

    @Override // com.bumptech.glide.request.RequestCoordinator
    public boolean h(d73 d73Var) {
        boolean z;
        synchronized (this.a) {
            z = o() && m(d73Var);
        }
        return z;
    }

    @Override // com.bumptech.glide.request.RequestCoordinator
    public boolean i(d73 d73Var) {
        boolean z;
        synchronized (this.a) {
            z = p() && m(d73Var);
        }
        return z;
    }

    @Override // defpackage.d73
    public boolean isRunning() {
        boolean z;
        synchronized (this.a) {
            RequestCoordinator.RequestState requestState = this.e;
            RequestCoordinator.RequestState requestState2 = RequestCoordinator.RequestState.RUNNING;
            z = requestState == requestState2 || this.f == requestState2;
        }
        return z;
    }

    @Override // defpackage.d73
    public boolean j() {
        boolean z;
        synchronized (this.a) {
            RequestCoordinator.RequestState requestState = this.e;
            RequestCoordinator.RequestState requestState2 = RequestCoordinator.RequestState.CLEARED;
            z = requestState == requestState2 && this.f == requestState2;
        }
        return z;
    }

    @Override // defpackage.d73
    public void k() {
        synchronized (this.a) {
            RequestCoordinator.RequestState requestState = this.e;
            RequestCoordinator.RequestState requestState2 = RequestCoordinator.RequestState.RUNNING;
            if (requestState != requestState2) {
                this.e = requestState2;
                this.c.k();
            }
        }
    }

    @Override // defpackage.d73
    public boolean l() {
        boolean z;
        synchronized (this.a) {
            RequestCoordinator.RequestState requestState = this.e;
            RequestCoordinator.RequestState requestState2 = RequestCoordinator.RequestState.SUCCESS;
            z = requestState == requestState2 || this.f == requestState2;
        }
        return z;
    }

    public final boolean m(d73 d73Var) {
        return d73Var.equals(this.c) || (this.e == RequestCoordinator.RequestState.FAILED && d73Var.equals(this.d));
    }

    public final boolean n() {
        RequestCoordinator requestCoordinator = this.b;
        return requestCoordinator == null || requestCoordinator.g(this);
    }

    public final boolean o() {
        RequestCoordinator requestCoordinator = this.b;
        return requestCoordinator == null || requestCoordinator.h(this);
    }

    public final boolean p() {
        RequestCoordinator requestCoordinator = this.b;
        return requestCoordinator == null || requestCoordinator.i(this);
    }

    public void q(d73 d73Var, d73 d73Var2) {
        this.c = d73Var;
        this.d = d73Var2;
    }
}
