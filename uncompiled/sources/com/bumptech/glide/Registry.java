package com.bumptech.glide;

import com.bumptech.glide.load.ImageHeaderParser;
import com.bumptech.glide.load.data.e;
import com.bumptech.glide.load.engine.i;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/* loaded from: classes.dex */
public class Registry {
    public final l92 a;
    public final gv0 b;
    public final w73 c;
    public final z73 d;
    public final com.bumptech.glide.load.data.f e;
    public final y84 f;
    public final zn1 g;
    public final m92 h = new m92();
    public final v02 i = new v02();
    public final et2<List<Throwable>> j;

    /* loaded from: classes.dex */
    public static class MissingComponentException extends RuntimeException {
        public MissingComponentException(String str) {
            super(str);
        }
    }

    /* loaded from: classes.dex */
    public static final class NoImageHeaderParserException extends MissingComponentException {
        public NoImageHeaderParserException() {
            super("Failed to find image header parser.");
        }
    }

    /* loaded from: classes.dex */
    public static class NoModelLoaderAvailableException extends MissingComponentException {
        public NoModelLoaderAvailableException(Object obj) {
            super("Failed to find any ModelLoaders registered for model class: " + obj.getClass());
        }

        public <M> NoModelLoaderAvailableException(M m, List<j92<M, ?>> list) {
            super("Found ModelLoaders for model class: " + list + ", but none that handle this specific model instance: " + m);
        }

        public NoModelLoaderAvailableException(Class<?> cls, Class<?> cls2) {
            super("Failed to find any ModelLoaders for model: " + cls + " and data: " + cls2);
        }
    }

    /* loaded from: classes.dex */
    public static class NoResultEncoderAvailableException extends MissingComponentException {
        public NoResultEncoderAvailableException(Class<?> cls) {
            super("Failed to find result encoder for resource class: " + cls + ", you may need to consider registering a new Encoder for the requested type or DiskCacheStrategy.DATA/DiskCacheStrategy.NONE if caching your transformed resource is unnecessary.");
        }
    }

    /* loaded from: classes.dex */
    public static class NoSourceEncoderAvailableException extends MissingComponentException {
        public NoSourceEncoderAvailableException(Class<?> cls) {
            super("Failed to find source encoder for data class: " + cls);
        }
    }

    public Registry() {
        et2<List<Throwable>> e = a21.e();
        this.j = e;
        this.a = new l92(e);
        this.b = new gv0();
        this.c = new w73();
        this.d = new z73();
        this.e = new com.bumptech.glide.load.data.f();
        this.f = new y84();
        this.g = new zn1();
        s(Arrays.asList("Animation", "Bitmap", "BitmapDrawable"));
    }

    public <Data> Registry a(Class<Data> cls, ev0<Data> ev0Var) {
        this.b.a(cls, ev0Var);
        return this;
    }

    public <TResource> Registry b(Class<TResource> cls, y73<TResource> y73Var) {
        this.d.a(cls, y73Var);
        return this;
    }

    public <Model, Data> Registry c(Class<Model> cls, Class<Data> cls2, k92<Model, Data> k92Var) {
        this.a.a(cls, cls2, k92Var);
        return this;
    }

    public <Data, TResource> Registry d(Class<Data> cls, Class<TResource> cls2, com.bumptech.glide.load.b<Data, TResource> bVar) {
        e("legacy_append", cls, cls2, bVar);
        return this;
    }

    public <Data, TResource> Registry e(String str, Class<Data> cls, Class<TResource> cls2, com.bumptech.glide.load.b<Data, TResource> bVar) {
        this.c.a(str, bVar, cls, cls2);
        return this;
    }

    public final <Data, TResource, Transcode> List<com.bumptech.glide.load.engine.e<Data, TResource, Transcode>> f(Class<Data> cls, Class<TResource> cls2, Class<Transcode> cls3) {
        ArrayList arrayList = new ArrayList();
        for (Class cls4 : this.c.d(cls, cls2)) {
            for (Class cls5 : this.f.b(cls4, cls3)) {
                arrayList.add(new com.bumptech.glide.load.engine.e(cls, cls4, cls5, this.c.b(cls, cls4), this.f.a(cls4, cls5), this.j));
            }
        }
        return arrayList;
    }

    public List<ImageHeaderParser> g() {
        List<ImageHeaderParser> b = this.g.b();
        if (b.isEmpty()) {
            throw new NoImageHeaderParserException();
        }
        return b;
    }

    public <Data, TResource, Transcode> i<Data, TResource, Transcode> h(Class<Data> cls, Class<TResource> cls2, Class<Transcode> cls3) {
        i<Data, TResource, Transcode> a = this.i.a(cls, cls2, cls3);
        if (this.i.c(a)) {
            return null;
        }
        if (a == null) {
            List<com.bumptech.glide.load.engine.e<Data, TResource, Transcode>> f = f(cls, cls2, cls3);
            a = f.isEmpty() ? null : new i<>(cls, cls2, cls3, f, this.j);
            this.i.d(cls, cls2, cls3, a);
        }
        return a;
    }

    public <Model> List<j92<Model, ?>> i(Model model) {
        return this.a.d(model);
    }

    public <Model, TResource, Transcode> List<Class<?>> j(Class<Model> cls, Class<TResource> cls2, Class<Transcode> cls3) {
        List<Class<?>> a = this.h.a(cls, cls2, cls3);
        if (a == null) {
            a = new ArrayList<>();
            for (Class<?> cls4 : this.a.c(cls)) {
                for (Class<?> cls5 : this.c.d(cls4, cls2)) {
                    if (!this.f.b(cls5, cls3).isEmpty() && !a.contains(cls5)) {
                        a.add(cls5);
                    }
                }
            }
            this.h.b(cls, cls2, cls3, Collections.unmodifiableList(a));
        }
        return a;
    }

    public <X> y73<X> k(s73<X> s73Var) throws NoResultEncoderAvailableException {
        y73<X> b = this.d.b(s73Var.d());
        if (b != null) {
            return b;
        }
        throw new NoResultEncoderAvailableException(s73Var.d());
    }

    public <X> com.bumptech.glide.load.data.e<X> l(X x) {
        return this.e.a(x);
    }

    public <X> ev0<X> m(X x) throws NoSourceEncoderAvailableException {
        ev0<X> b = this.b.b(x.getClass());
        if (b != null) {
            return b;
        }
        throw new NoSourceEncoderAvailableException(x.getClass());
    }

    public boolean n(s73<?> s73Var) {
        return this.d.b(s73Var.d()) != null;
    }

    public Registry o(ImageHeaderParser imageHeaderParser) {
        this.g.a(imageHeaderParser);
        return this;
    }

    public Registry p(e.a<?> aVar) {
        this.e.b(aVar);
        return this;
    }

    public <TResource, Transcode> Registry q(Class<TResource> cls, Class<Transcode> cls2, e83<TResource, Transcode> e83Var) {
        this.f.c(cls, cls2, e83Var);
        return this;
    }

    public <Model, Data> Registry r(Class<Model> cls, Class<Data> cls2, k92<? extends Model, ? extends Data> k92Var) {
        this.a.f(cls, cls2, k92Var);
        return this;
    }

    public final Registry s(List<String> list) {
        ArrayList arrayList = new ArrayList(list.size());
        arrayList.add("legacy_prepend_all");
        for (String str : list) {
            arrayList.add(str);
        }
        arrayList.add("legacy_append");
        this.c.e(arrayList);
        return this;
    }
}
