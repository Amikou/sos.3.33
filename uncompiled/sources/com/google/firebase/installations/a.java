package com.google.firebase.installations;

import com.google.firebase.installations.d;
import java.util.Objects;

/* compiled from: AutoValue_InstallationTokenResult.java */
/* loaded from: classes2.dex */
public final class a extends d {
    public final String a;
    public final long b;
    public final long c;

    /* compiled from: AutoValue_InstallationTokenResult.java */
    /* loaded from: classes2.dex */
    public static final class b extends d.a {
        public String a;
        public Long b;
        public Long c;

        @Override // com.google.firebase.installations.d.a
        public d a() {
            String str = "";
            if (this.a == null) {
                str = " token";
            }
            if (this.b == null) {
                str = str + " tokenExpirationTimestamp";
            }
            if (this.c == null) {
                str = str + " tokenCreationTimestamp";
            }
            if (str.isEmpty()) {
                return new a(this.a, this.b.longValue(), this.c.longValue());
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @Override // com.google.firebase.installations.d.a
        public d.a b(String str) {
            Objects.requireNonNull(str, "Null token");
            this.a = str;
            return this;
        }

        @Override // com.google.firebase.installations.d.a
        public d.a c(long j) {
            this.c = Long.valueOf(j);
            return this;
        }

        @Override // com.google.firebase.installations.d.a
        public d.a d(long j) {
            this.b = Long.valueOf(j);
            return this;
        }
    }

    @Override // com.google.firebase.installations.d
    public String b() {
        return this.a;
    }

    @Override // com.google.firebase.installations.d
    public long c() {
        return this.c;
    }

    @Override // com.google.firebase.installations.d
    public long d() {
        return this.b;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof d) {
            d dVar = (d) obj;
            return this.a.equals(dVar.b()) && this.b == dVar.d() && this.c == dVar.c();
        }
        return false;
    }

    public int hashCode() {
        long j = this.b;
        long j2 = this.c;
        return ((((this.a.hashCode() ^ 1000003) * 1000003) ^ ((int) (j ^ (j >>> 32)))) * 1000003) ^ ((int) (j2 ^ (j2 >>> 32)));
    }

    public String toString() {
        return "InstallationTokenResult{token=" + this.a + ", tokenExpirationTimestamp=" + this.b + ", tokenCreationTimestamp=" + this.c + "}";
    }

    public a(String str, long j, long j2) {
        this.a = str;
        this.b = j;
        this.c = j2;
    }
}
