package com.google.firebase.installations.local;

import com.google.firebase.installations.local.PersistedInstallation;
import com.google.firebase.installations.local.b;
import java.util.Objects;

/* compiled from: AutoValue_PersistedInstallationEntry.java */
/* loaded from: classes2.dex */
public final class a extends com.google.firebase.installations.local.b {
    public final String a;
    public final PersistedInstallation.RegistrationStatus b;
    public final String c;
    public final String d;
    public final long e;
    public final long f;
    public final String g;

    /* compiled from: AutoValue_PersistedInstallationEntry.java */
    /* loaded from: classes2.dex */
    public static final class b extends b.a {
        public String a;
        public PersistedInstallation.RegistrationStatus b;
        public String c;
        public String d;
        public Long e;
        public Long f;
        public String g;

        @Override // com.google.firebase.installations.local.b.a
        public com.google.firebase.installations.local.b a() {
            String str = "";
            if (this.b == null) {
                str = " registrationStatus";
            }
            if (this.e == null) {
                str = str + " expiresInSecs";
            }
            if (this.f == null) {
                str = str + " tokenCreationEpochInSecs";
            }
            if (str.isEmpty()) {
                return new a(this.a, this.b, this.c, this.d, this.e.longValue(), this.f.longValue(), this.g);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @Override // com.google.firebase.installations.local.b.a
        public b.a b(String str) {
            this.c = str;
            return this;
        }

        @Override // com.google.firebase.installations.local.b.a
        public b.a c(long j) {
            this.e = Long.valueOf(j);
            return this;
        }

        @Override // com.google.firebase.installations.local.b.a
        public b.a d(String str) {
            this.a = str;
            return this;
        }

        @Override // com.google.firebase.installations.local.b.a
        public b.a e(String str) {
            this.g = str;
            return this;
        }

        @Override // com.google.firebase.installations.local.b.a
        public b.a f(String str) {
            this.d = str;
            return this;
        }

        @Override // com.google.firebase.installations.local.b.a
        public b.a g(PersistedInstallation.RegistrationStatus registrationStatus) {
            Objects.requireNonNull(registrationStatus, "Null registrationStatus");
            this.b = registrationStatus;
            return this;
        }

        @Override // com.google.firebase.installations.local.b.a
        public b.a h(long j) {
            this.f = Long.valueOf(j);
            return this;
        }

        public b() {
        }

        public b(com.google.firebase.installations.local.b bVar) {
            this.a = bVar.d();
            this.b = bVar.g();
            this.c = bVar.b();
            this.d = bVar.f();
            this.e = Long.valueOf(bVar.c());
            this.f = Long.valueOf(bVar.h());
            this.g = bVar.e();
        }
    }

    @Override // com.google.firebase.installations.local.b
    public String b() {
        return this.c;
    }

    @Override // com.google.firebase.installations.local.b
    public long c() {
        return this.e;
    }

    @Override // com.google.firebase.installations.local.b
    public String d() {
        return this.a;
    }

    @Override // com.google.firebase.installations.local.b
    public String e() {
        return this.g;
    }

    public boolean equals(Object obj) {
        String str;
        String str2;
        if (obj == this) {
            return true;
        }
        if (obj instanceof com.google.firebase.installations.local.b) {
            com.google.firebase.installations.local.b bVar = (com.google.firebase.installations.local.b) obj;
            String str3 = this.a;
            if (str3 != null ? str3.equals(bVar.d()) : bVar.d() == null) {
                if (this.b.equals(bVar.g()) && ((str = this.c) != null ? str.equals(bVar.b()) : bVar.b() == null) && ((str2 = this.d) != null ? str2.equals(bVar.f()) : bVar.f() == null) && this.e == bVar.c() && this.f == bVar.h()) {
                    String str4 = this.g;
                    if (str4 == null) {
                        if (bVar.e() == null) {
                            return true;
                        }
                    } else if (str4.equals(bVar.e())) {
                        return true;
                    }
                }
            }
            return false;
        }
        return false;
    }

    @Override // com.google.firebase.installations.local.b
    public String f() {
        return this.d;
    }

    @Override // com.google.firebase.installations.local.b
    public PersistedInstallation.RegistrationStatus g() {
        return this.b;
    }

    @Override // com.google.firebase.installations.local.b
    public long h() {
        return this.f;
    }

    public int hashCode() {
        String str = this.a;
        int hashCode = ((((str == null ? 0 : str.hashCode()) ^ 1000003) * 1000003) ^ this.b.hashCode()) * 1000003;
        String str2 = this.c;
        int hashCode2 = (hashCode ^ (str2 == null ? 0 : str2.hashCode())) * 1000003;
        String str3 = this.d;
        int hashCode3 = str3 == null ? 0 : str3.hashCode();
        long j = this.e;
        long j2 = this.f;
        int i = (((((hashCode2 ^ hashCode3) * 1000003) ^ ((int) (j ^ (j >>> 32)))) * 1000003) ^ ((int) (j2 ^ (j2 >>> 32)))) * 1000003;
        String str4 = this.g;
        return i ^ (str4 != null ? str4.hashCode() : 0);
    }

    @Override // com.google.firebase.installations.local.b
    public b.a n() {
        return new b(this);
    }

    public String toString() {
        return "PersistedInstallationEntry{firebaseInstallationId=" + this.a + ", registrationStatus=" + this.b + ", authToken=" + this.c + ", refreshToken=" + this.d + ", expiresInSecs=" + this.e + ", tokenCreationEpochInSecs=" + this.f + ", fisError=" + this.g + "}";
    }

    public a(String str, PersistedInstallation.RegistrationStatus registrationStatus, String str2, String str3, long j, long j2, String str4) {
        this.a = str;
        this.b = registrationStatus;
        this.c = str2;
        this.d = str3;
        this.e = j;
        this.f = j2;
        this.g = str4;
    }
}
