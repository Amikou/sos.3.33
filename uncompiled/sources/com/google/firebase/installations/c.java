package com.google.firebase.installations;

/* compiled from: GetAuthTokenListener.java */
/* loaded from: classes2.dex */
public class c implements bt3 {
    public final tg4 a;
    public final n34<d> b;

    public c(tg4 tg4Var, n34<d> n34Var) {
        this.a = tg4Var;
        this.b = n34Var;
    }

    @Override // defpackage.bt3
    public boolean a(Exception exc) {
        this.b.d(exc);
        return true;
    }

    @Override // defpackage.bt3
    public boolean b(com.google.firebase.installations.local.b bVar) {
        if (!bVar.k() || this.a.f(bVar)) {
            return false;
        }
        this.b.c(d.a().b(bVar.b()).d(bVar.c()).c(bVar.h()).a());
        return true;
    }
}
