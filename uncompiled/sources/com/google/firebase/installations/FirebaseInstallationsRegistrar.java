package com.google.firebase.installations;

import androidx.annotation.Keep;
import com.google.firebase.heartbeatinfo.HeartBeatInfo;
import java.util.Arrays;
import java.util.List;

@Keep
/* loaded from: classes2.dex */
public class FirebaseInstallationsRegistrar implements g40 {
    /* JADX INFO: Access modifiers changed from: private */
    public static /* synthetic */ k51 lambda$getComponents$0(b40 b40Var) {
        return new b((c51) b40Var.a(c51.class), b40Var.b(wf4.class), b40Var.b(HeartBeatInfo.class));
    }

    @Override // defpackage.g40
    public List<a40<?>> getComponents() {
        return Arrays.asList(a40.c(k51.class).b(hm0.j(c51.class)).b(hm0.i(HeartBeatInfo.class)).b(hm0.i(wf4.class)).f(l51.a).d(), jz1.b("fire-installations", "17.0.0"));
    }
}
