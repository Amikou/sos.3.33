package com.google.firebase.installations;

import android.text.TextUtils;
import com.google.firebase.heartbeatinfo.HeartBeatInfo;
import com.google.firebase.installations.FirebaseInstallationsException;
import com.google.firebase.installations.b;
import com.google.firebase.installations.local.PersistedInstallation;
import com.google.firebase.installations.remote.InstallationResponse;
import com.google.firebase.installations.remote.TokenResult;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: FirebaseInstallations.java */
/* loaded from: classes2.dex */
public class b implements k51 {
    public static final Object m = new Object();
    public static final ThreadFactory n = new a();
    public final c51 a;
    public final com.google.firebase.installations.remote.c b;
    public final PersistedInstallation c;
    public final tg4 d;
    public final pn1 e;
    public final q33 f;
    public final Object g;
    public final ExecutorService h;
    public final ExecutorService i;
    public String j;
    public Set<f31> k;
    public final List<bt3> l;

    /* compiled from: FirebaseInstallations.java */
    /* loaded from: classes2.dex */
    public class a implements ThreadFactory {
        public final AtomicInteger a = new AtomicInteger(1);

        @Override // java.util.concurrent.ThreadFactory
        public Thread newThread(Runnable runnable) {
            return new Thread(runnable, String.format("firebase-installations-executor-%d", Integer.valueOf(this.a.getAndIncrement())));
        }
    }

    /* compiled from: FirebaseInstallations.java */
    /* renamed from: com.google.firebase.installations.b$b  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public static /* synthetic */ class C0144b {
        public static final /* synthetic */ int[] a;
        public static final /* synthetic */ int[] b;

        static {
            int[] iArr = new int[TokenResult.ResponseCode.values().length];
            b = iArr;
            try {
                iArr[TokenResult.ResponseCode.OK.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                b[TokenResult.ResponseCode.BAD_CONFIG.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                b[TokenResult.ResponseCode.AUTH_ERROR.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            int[] iArr2 = new int[InstallationResponse.ResponseCode.values().length];
            a = iArr2;
            try {
                iArr2[InstallationResponse.ResponseCode.OK.ordinal()] = 1;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                a[InstallationResponse.ResponseCode.BAD_CONFIG.ordinal()] = 2;
            } catch (NoSuchFieldError unused5) {
            }
        }
    }

    public b(c51 c51Var, fw2<wf4> fw2Var, fw2<HeartBeatInfo> fw2Var2) {
        this(new ThreadPoolExecutor(0, 1, 30L, TimeUnit.SECONDS, new LinkedBlockingQueue(), n), c51Var, new com.google.firebase.installations.remote.c(c51Var.h(), fw2Var, fw2Var2), new PersistedInstallation(c51Var), tg4.c(), new pn1(c51Var), new q33());
    }

    public static b n() {
        return o(c51.i());
    }

    public static b o(c51 c51Var) {
        zt2.b(c51Var != null, "Null is not a valid value of FirebaseApp.");
        return (b) c51Var.g(k51.class);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void u() {
        v(false);
    }

    public final void A(com.google.firebase.installations.local.b bVar) {
        synchronized (this.g) {
            Iterator<bt3> it = this.l.iterator();
            while (it.hasNext()) {
                if (it.next().b(bVar)) {
                    it.remove();
                }
            }
        }
    }

    public final synchronized void B(String str) {
        this.j = str;
    }

    public final synchronized void C(com.google.firebase.installations.local.b bVar, com.google.firebase.installations.local.b bVar2) {
        if (this.k.size() != 0 && !bVar.d().equals(bVar2.d())) {
            for (f31 f31Var : this.k) {
                f31Var.a(bVar2.d());
            }
        }
    }

    @Override // defpackage.k51
    public com.google.android.gms.tasks.c<d> a(final boolean z) {
        w();
        com.google.android.gms.tasks.c<d> e = e();
        this.h.execute(new Runnable() { // from class: j51
            @Override // java.lang.Runnable
            public final void run() {
                b.this.v(z);
            }
        });
        return e;
    }

    public final com.google.android.gms.tasks.c<d> e() {
        n34 n34Var = new n34();
        g(new c(this.d, n34Var));
        return n34Var.a();
    }

    public final com.google.android.gms.tasks.c<String> f() {
        n34 n34Var = new n34();
        g(new jf1(n34Var));
        return n34Var.a();
    }

    public final void g(bt3 bt3Var) {
        synchronized (this.g) {
            this.l.add(bt3Var);
        }
    }

    @Override // defpackage.k51
    public com.google.android.gms.tasks.c<String> getId() {
        w();
        String m2 = m();
        if (m2 != null) {
            return com.google.android.gms.tasks.d.e(m2);
        }
        com.google.android.gms.tasks.c<String> f = f();
        this.h.execute(new Runnable() { // from class: h51
            @Override // java.lang.Runnable
            public final void run() {
                b.this.u();
            }
        });
        return f;
    }

    /* JADX WARN: Removed duplicated region for block: B:17:0x0032  */
    /* JADX WARN: Removed duplicated region for block: B:20:0x003f  */
    /* JADX WARN: Removed duplicated region for block: B:21:0x004a  */
    /* renamed from: h */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void t(boolean r3) {
        /*
            r2 = this;
            com.google.firebase.installations.local.b r0 = r2.p()
            boolean r1 = r0.i()     // Catch: com.google.firebase.installations.FirebaseInstallationsException -> L5f
            if (r1 != 0) goto L22
            boolean r1 = r0.l()     // Catch: com.google.firebase.installations.FirebaseInstallationsException -> L5f
            if (r1 == 0) goto L11
            goto L22
        L11:
            if (r3 != 0) goto L1d
            tg4 r3 = r2.d     // Catch: com.google.firebase.installations.FirebaseInstallationsException -> L5f
            boolean r3 = r3.f(r0)     // Catch: com.google.firebase.installations.FirebaseInstallationsException -> L5f
            if (r3 == 0) goto L1c
            goto L1d
        L1c:
            return
        L1d:
            com.google.firebase.installations.local.b r3 = r2.j(r0)     // Catch: com.google.firebase.installations.FirebaseInstallationsException -> L5f
            goto L26
        L22:
            com.google.firebase.installations.local.b r3 = r2.y(r0)     // Catch: com.google.firebase.installations.FirebaseInstallationsException -> L5f
        L26:
            r2.s(r3)
            r2.C(r0, r3)
            boolean r0 = r3.k()
            if (r0 == 0) goto L39
            java.lang.String r0 = r3.d()
            r2.B(r0)
        L39:
            boolean r0 = r3.i()
            if (r0 == 0) goto L4a
            com.google.firebase.installations.FirebaseInstallationsException r3 = new com.google.firebase.installations.FirebaseInstallationsException
            com.google.firebase.installations.FirebaseInstallationsException$Status r0 = com.google.firebase.installations.FirebaseInstallationsException.Status.BAD_CONFIG
            r3.<init>(r0)
            r2.z(r3)
            goto L5e
        L4a:
            boolean r0 = r3.j()
            if (r0 == 0) goto L5b
            java.io.IOException r3 = new java.io.IOException
            java.lang.String r0 = "Installation ID could not be validated with the Firebase servers (maybe it was deleted). Firebase Installations will need to create a new Installation ID and auth token. Please retry your last request."
            r3.<init>(r0)
            r2.z(r3)
            goto L5e
        L5b:
            r2.A(r3)
        L5e:
            return
        L5f:
            r3 = move-exception
            r2.z(r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.firebase.installations.b.t(boolean):void");
    }

    /* renamed from: i */
    public final void v(final boolean z) {
        com.google.firebase.installations.local.b q = q();
        if (z) {
            q = q.p();
        }
        A(q);
        this.i.execute(new Runnable() { // from class: i51
            @Override // java.lang.Runnable
            public final void run() {
                b.this.t(z);
            }
        });
    }

    public final com.google.firebase.installations.local.b j(com.google.firebase.installations.local.b bVar) throws FirebaseInstallationsException {
        TokenResult e = this.b.e(k(), bVar.d(), r(), bVar.f());
        int i = C0144b.b[e.b().ordinal()];
        if (i != 1) {
            if (i != 2) {
                if (i == 3) {
                    B(null);
                    return bVar.r();
                }
                throw new FirebaseInstallationsException("Firebase Installations Service is unavailable. Please try again later.", FirebaseInstallationsException.Status.UNAVAILABLE);
            }
            return bVar.q("BAD CONFIG");
        }
        return bVar.o(e.c(), e.d(), this.d.b());
    }

    public String k() {
        return this.a.k().b();
    }

    public String l() {
        return this.a.k().c();
    }

    public final synchronized String m() {
        return this.j;
    }

    public final com.google.firebase.installations.local.b p() {
        com.google.firebase.installations.local.b c;
        synchronized (m) {
            na0 a2 = na0.a(this.a.h(), "generatefid.lock");
            c = this.c.c();
            if (a2 != null) {
                a2.b();
            }
        }
        return c;
    }

    public final com.google.firebase.installations.local.b q() {
        com.google.firebase.installations.local.b c;
        synchronized (m) {
            na0 a2 = na0.a(this.a.h(), "generatefid.lock");
            c = this.c.c();
            if (c.j()) {
                c = this.c.a(c.t(x(c)));
            }
            if (a2 != null) {
                a2.b();
            }
        }
        return c;
    }

    public String r() {
        return this.a.k().e();
    }

    public final void s(com.google.firebase.installations.local.b bVar) {
        synchronized (m) {
            na0 a2 = na0.a(this.a.h(), "generatefid.lock");
            this.c.a(bVar);
            if (a2 != null) {
                a2.b();
            }
        }
    }

    public final void w() {
        zt2.g(l(), "Please set your Application ID. A valid Firebase App ID is required to communicate with Firebase server APIs: It identifies your application with Firebase.Please refer to https://firebase.google.com/support/privacy/init-options.");
        zt2.g(r(), "Please set your Project ID. A valid Firebase Project ID is required to communicate with Firebase server APIs: It identifies your application with Firebase.Please refer to https://firebase.google.com/support/privacy/init-options.");
        zt2.g(k(), "Please set a valid API key. A Firebase API key is required to communicate with Firebase server APIs: It authenticates your project with Google.Please refer to https://firebase.google.com/support/privacy/init-options.");
        zt2.b(tg4.h(l()), "Please set your Application ID. A valid Firebase App ID is required to communicate with Firebase server APIs: It identifies your application with Firebase.Please refer to https://firebase.google.com/support/privacy/init-options.");
        zt2.b(tg4.g(k()), "Please set a valid API key. A Firebase API key is required to communicate with Firebase server APIs: It authenticates your project with Google.Please refer to https://firebase.google.com/support/privacy/init-options.");
    }

    public final String x(com.google.firebase.installations.local.b bVar) {
        if ((!this.a.j().equals("CHIME_ANDROID_SDK") && !this.a.r()) || !bVar.m()) {
            return this.f.a();
        }
        String f = this.e.f();
        return TextUtils.isEmpty(f) ? this.f.a() : f;
    }

    public final com.google.firebase.installations.local.b y(com.google.firebase.installations.local.b bVar) throws FirebaseInstallationsException {
        InstallationResponse d = this.b.d(k(), bVar.d(), r(), l(), (bVar.d() == null || bVar.d().length() != 11) ? null : this.e.i());
        int i = C0144b.a[d.e().ordinal()];
        if (i != 1) {
            if (i == 2) {
                return bVar.q("BAD CONFIG");
            }
            throw new FirebaseInstallationsException("Firebase Installations Service is unavailable. Please try again later.", FirebaseInstallationsException.Status.UNAVAILABLE);
        }
        return bVar.s(d.c(), d.d(), this.d.b(), d.b().c(), d.b().d());
    }

    public final void z(Exception exc) {
        synchronized (this.g) {
            Iterator<bt3> it = this.l.iterator();
            while (it.hasNext()) {
                if (it.next().a(exc)) {
                    it.remove();
                }
            }
        }
    }

    public b(ExecutorService executorService, c51 c51Var, com.google.firebase.installations.remote.c cVar, PersistedInstallation persistedInstallation, tg4 tg4Var, pn1 pn1Var, q33 q33Var) {
        this.g = new Object();
        this.k = new HashSet();
        this.l = new ArrayList();
        this.a = c51Var;
        this.b = cVar;
        this.c = persistedInstallation;
        this.d = tg4Var;
        this.e = pn1Var;
        this.f = q33Var;
        this.h = executorService;
        this.i = new ThreadPoolExecutor(0, 1, 30L, TimeUnit.SECONDS, new LinkedBlockingQueue(), n);
    }
}
