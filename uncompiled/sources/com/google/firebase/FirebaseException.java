package com.google.firebase;

import androidx.annotation.RecentlyNonNull;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* loaded from: classes2.dex */
public class FirebaseException extends Exception {
    @Deprecated
    public FirebaseException() {
    }

    public FirebaseException(@RecentlyNonNull String str) {
        super(zt2.g(str, "Detail message must not be empty"));
    }

    public FirebaseException(@RecentlyNonNull String str, @RecentlyNonNull Throwable th) {
        super(zt2.g(str, "Detail message must not be empty"), th);
    }
}
