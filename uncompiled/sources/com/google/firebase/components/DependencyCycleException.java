package com.google.firebase.components;

import java.util.Arrays;
import java.util.List;

/* loaded from: classes2.dex */
public class DependencyCycleException extends DependencyException {
    private final List<a40<?>> componentsInCycle;

    public DependencyCycleException(List<a40<?>> list) {
        super("Dependency cycle detected: " + Arrays.toString(list.toArray()));
        this.componentsInCycle = list;
    }

    public List<a40<?>> getComponentsInCycle() {
        return this.componentsInCycle;
    }
}
