package com.google.firebase.datatransport;

import android.content.Context;
import androidx.annotation.Keep;
import java.util.Collections;
import java.util.List;

@Keep
/* loaded from: classes2.dex */
public class TransportRegistrar implements g40 {
    /* JADX INFO: Access modifiers changed from: private */
    public static /* synthetic */ pb4 lambda$getComponents$0(b40 b40Var) {
        vb4.f((Context) b40Var.a(Context.class));
        return vb4.c().g(ht.g);
    }

    @Override // defpackage.g40
    public List<a40<?>> getComponents() {
        return Collections.singletonList(a40.c(pb4.class).b(hm0.j(Context.class)).f(ub4.a).d());
    }
}
