package com.google.firebase;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.Build;
import com.google.firebase.heartbeatinfo.a;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes2.dex */
public class FirebaseCommonRegistrar implements g40 {
    public static /* synthetic */ String e(Context context) {
        ApplicationInfo applicationInfo = context.getApplicationInfo();
        return applicationInfo != null ? String.valueOf(applicationInfo.targetSdkVersion) : "";
    }

    public static /* synthetic */ String f(Context context) {
        ApplicationInfo applicationInfo = context.getApplicationInfo();
        return (applicationInfo == null || Build.VERSION.SDK_INT < 24) ? "" : String.valueOf(applicationInfo.minSdkVersion);
    }

    public static /* synthetic */ String g(Context context) {
        int i = Build.VERSION.SDK_INT;
        return (i < 16 || !context.getPackageManager().hasSystemFeature("android.hardware.type.television")) ? (i < 20 || !context.getPackageManager().hasSystemFeature("android.hardware.type.watch")) ? (i < 23 || !context.getPackageManager().hasSystemFeature("android.hardware.type.automotive")) ? (i < 26 || !context.getPackageManager().hasSystemFeature("android.hardware.type.embedded")) ? "" : "embedded" : "auto" : "watch" : "tv";
    }

    public static /* synthetic */ String h(Context context) {
        String installerPackageName = context.getPackageManager().getInstallerPackageName(context.getPackageName());
        return installerPackageName != null ? i(installerPackageName) : "";
    }

    public static String i(String str) {
        return str.replace(' ', '_').replace('/', '_');
    }

    @Override // defpackage.g40
    public List<a40<?>> getComponents() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(ol0.c());
        arrayList.add(a.e());
        arrayList.add(jz1.b("fire-android", String.valueOf(Build.VERSION.SDK_INT)));
        arrayList.add(jz1.b("fire-core", "20.0.0"));
        arrayList.add(jz1.b("device-name", i(Build.PRODUCT)));
        arrayList.add(jz1.b("device-model", i(Build.DEVICE)));
        arrayList.add(jz1.b("device-brand", i(Build.BRAND)));
        arrayList.add(jz1.c("android-target-sdk", e51.a));
        arrayList.add(jz1.c("android-min-sdk", f51.a));
        arrayList.add(jz1.c("android-platform", g51.a));
        arrayList.add(jz1.c("android-installer", d51.a));
        String a = wx1.a();
        if (a != null) {
            arrayList.add(jz1.b("kotlin", a));
        }
        return arrayList;
    }
}
