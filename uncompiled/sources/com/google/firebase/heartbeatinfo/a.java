package com.google.firebase.heartbeatinfo;

import android.content.Context;
import com.google.firebase.heartbeatinfo.HeartBeatInfo;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* compiled from: DefaultHeartBeatInfo.java */
/* loaded from: classes2.dex */
public class a implements HeartBeatInfo {
    public static final ThreadFactory c = rj0.a;
    public fw2<jk1> a;
    public final Executor b;

    public a(final Context context, Set<ik1> set) {
        this(new ty1(new fw2() { // from class: qj0
            @Override // defpackage.fw2
            public final Object get() {
                jk1 a;
                a = jk1.a(context);
                return a;
            }
        }), set, new ThreadPoolExecutor(0, 1, 30L, TimeUnit.SECONDS, new LinkedBlockingQueue(), c));
    }

    public static a40<HeartBeatInfo> e() {
        return a40.c(HeartBeatInfo.class).b(hm0.j(Context.class)).b(hm0.k(ik1.class)).f(pj0.a).d();
    }

    public static /* synthetic */ HeartBeatInfo f(b40 b40Var) {
        return new a((Context) b40Var.a(Context.class), b40Var.d(ik1.class));
    }

    public static /* synthetic */ Thread h(Runnable runnable) {
        return new Thread(runnable, "heartbeat-information-executor");
    }

    @Override // com.google.firebase.heartbeatinfo.HeartBeatInfo
    public HeartBeatInfo.HeartBeat a(String str) {
        long currentTimeMillis = System.currentTimeMillis();
        boolean d = this.a.get().d(str, currentTimeMillis);
        boolean c2 = this.a.get().c(currentTimeMillis);
        if (d && c2) {
            return HeartBeatInfo.HeartBeat.COMBINED;
        }
        if (c2) {
            return HeartBeatInfo.HeartBeat.GLOBAL;
        }
        if (d) {
            return HeartBeatInfo.HeartBeat.SDK;
        }
        return HeartBeatInfo.HeartBeat.NONE;
    }

    public a(fw2<jk1> fw2Var, Set<ik1> set, Executor executor) {
        this.a = fw2Var;
        this.b = executor;
    }
}
