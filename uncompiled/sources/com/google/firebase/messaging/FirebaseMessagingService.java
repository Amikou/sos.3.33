package com.google.firebase.messaging;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.ExecutorService;

/* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
/* loaded from: classes2.dex */
public class FirebaseMessagingService extends EnhancedIntentService {
    public static final Queue<String> j0 = new ArrayDeque(10);

    @Override // com.google.firebase.messaging.EnhancedIntentService
    public Intent c(Intent intent) {
        return im3.b().c();
    }

    @Override // com.google.firebase.messaging.EnhancedIntentService
    public void d(Intent intent) {
        String action = intent.getAction();
        if (!"com.google.android.c2dm.intent.RECEIVE".equals(action) && !"com.google.firebase.messaging.RECEIVE_DIRECT_BOOT".equals(action)) {
            if ("com.google.firebase.messaging.NEW_TOKEN".equals(action)) {
                q(intent.getStringExtra("token"));
                return;
            }
            String valueOf = String.valueOf(intent.getAction());
            if (valueOf.length() != 0) {
                "Unknown intent action: ".concat(valueOf);
                return;
            }
            return;
        }
        m(intent);
    }

    public final boolean j(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        Queue<String> queue = j0;
        if (queue.contains(str)) {
            if (Log.isLoggable("FirebaseMessaging", 3)) {
                String valueOf = String.valueOf(str);
                if (valueOf.length() != 0) {
                    "Received duplicate message: ".concat(valueOf);
                    return true;
                }
                return true;
            }
            return true;
        }
        if (queue.size() >= 10) {
            queue.remove();
        }
        queue.add(str);
        return false;
    }

    public final void k(Intent intent) {
        Bundle extras = intent.getExtras();
        if (extras == null) {
            extras = new Bundle();
        }
        extras.remove("androidx.content.wakelockid");
        if (f.t(extras)) {
            f fVar = new f(extras);
            ExecutorService d = q21.d();
            try {
                if (new wp0(this, fVar, d).a()) {
                    return;
                }
                d.shutdown();
                if (e.A(intent)) {
                    e.t(intent);
                }
            } finally {
                d.shutdown();
            }
        }
        o(new RemoteMessage(extras));
    }

    public final String l(Intent intent) {
        String stringExtra = intent.getStringExtra("google.message_id");
        return stringExtra == null ? intent.getStringExtra("message_id") : stringExtra;
    }

    public final void m(Intent intent) {
        if (j(intent.getStringExtra("google.message_id"))) {
            return;
        }
        s(intent);
    }

    public void n() {
    }

    public void o(RemoteMessage remoteMessage) {
    }

    public void p(String str) {
    }

    public void q(String str) {
    }

    public void r(String str, Exception exc) {
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    public final void s(Intent intent) {
        char c;
        String stringExtra = intent.getStringExtra("message_type");
        if (stringExtra == null) {
            stringExtra = "gcm";
        }
        switch (stringExtra.hashCode()) {
            case -2062414158:
                if (stringExtra.equals("deleted_messages")) {
                    c = 1;
                    break;
                }
                c = 65535;
                break;
            case 102161:
                if (stringExtra.equals("gcm")) {
                    c = 0;
                    break;
                }
                c = 65535;
                break;
            case 814694033:
                if (stringExtra.equals("send_error")) {
                    c = 3;
                    break;
                }
                c = 65535;
                break;
            case 814800675:
                if (stringExtra.equals("send_event")) {
                    c = 2;
                    break;
                }
                c = 65535;
                break;
            default:
                c = 65535;
                break;
        }
        if (c == 0) {
            e.v(intent);
            k(intent);
        } else if (c == 1) {
            n();
        } else if (c == 2) {
            p(intent.getStringExtra("google.message_id"));
        } else if (c != 3) {
            if (stringExtra.length() != 0) {
                "Received message with unknown type: ".concat(stringExtra);
            }
        } else {
            r(l(intent), new SendException(intent.getStringExtra("error")));
        }
    }
}
