package com.google.firebase.messaging;

import android.util.Log;
import com.google.android.gms.tasks.c;
import com.google.firebase.messaging.g;
import java.util.Map;
import java.util.concurrent.Executor;

/* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
/* loaded from: classes2.dex */
public class g {
    public final Executor a;
    public final Map<String, com.google.android.gms.tasks.c<String>> b = new rh();

    /* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
    /* loaded from: classes2.dex */
    public interface a {
        com.google.android.gms.tasks.c<String> start();
    }

    public g(Executor executor) {
        this.a = executor;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public synchronized com.google.android.gms.tasks.c<String> a(final String str, a aVar) {
        com.google.android.gms.tasks.c<String> cVar = this.b.get(str);
        if (cVar != null) {
            if (Log.isLoggable("FirebaseMessaging", 3)) {
                String valueOf = String.valueOf(str);
                if (valueOf.length() != 0) {
                    "Joining ongoing request for: ".concat(valueOf);
                }
            }
            return cVar;
        }
        if (Log.isLoggable("FirebaseMessaging", 3)) {
            String valueOf2 = String.valueOf(str);
            if (valueOf2.length() != 0) {
                "Making new request for: ".concat(valueOf2);
            }
        }
        com.google.android.gms.tasks.c j = aVar.start().j(this.a, new com.google.android.gms.tasks.a(this, str) { // from class: e73
            public final g a;
            public final String b;

            {
                this.a = this;
                this.b = str;
            }

            @Override // com.google.android.gms.tasks.a
            public Object a(c cVar2) {
                this.a.b(this.b, cVar2);
                return cVar2;
            }
        });
        this.b.put(str, j);
        return j;
    }

    public final /* synthetic */ com.google.android.gms.tasks.c b(String str, com.google.android.gms.tasks.c cVar) throws Exception {
        synchronized (this) {
            this.b.remove(str);
        }
        return cVar;
    }
}
