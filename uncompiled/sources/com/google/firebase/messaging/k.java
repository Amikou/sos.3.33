package com.google.firebase.messaging;

import android.content.Intent;
import android.os.Binder;
import android.os.Process;
import com.google.android.gms.tasks.c;
import com.google.firebase.messaging.l;

/* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
/* loaded from: classes2.dex */
public class k extends Binder {
    public final a a;

    /* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
    /* loaded from: classes2.dex */
    public interface a {
        com.google.android.gms.tasks.c<Void> a(Intent intent);
    }

    public k(a aVar) {
        this.a = aVar;
    }

    public void b(final l.a aVar) {
        if (Binder.getCallingUid() == Process.myUid()) {
            this.a.a(aVar.a).c(rp4.a, new im2(aVar) { // from class: sp4
                public final l.a a;

                {
                    this.a = aVar;
                }

                @Override // defpackage.im2
                public void a(c cVar) {
                    this.a.b();
                }
            });
            return;
        }
        throw new SecurityException("Binding only allowed within app");
    }
}
