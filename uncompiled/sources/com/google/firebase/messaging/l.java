package com.google.firebase.messaging;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;
import com.google.android.gms.tasks.c;
import com.google.firebase.messaging.l;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
/* loaded from: classes2.dex */
public class l implements ServiceConnection {
    public final Context a;
    public final Intent f0;
    public final ScheduledExecutorService g0;
    public final Queue<a> h0;
    public k i0;
    public boolean j0;

    /* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
    /* loaded from: classes2.dex */
    public static class a {
        public final Intent a;
        public final n34<Void> b = new n34<>();

        public a(Intent intent) {
            this.a = intent;
        }

        public void a(ScheduledExecutorService scheduledExecutorService) {
            final ScheduledFuture<?> schedule = scheduledExecutorService.schedule(new Runnable(this) { // from class: tp4
                public final l.a a;

                {
                    this.a = this;
                }

                @Override // java.lang.Runnable
                public void run() {
                    this.a.d();
                }
            }, 9000L, TimeUnit.MILLISECONDS);
            c().c(scheduledExecutorService, new im2(schedule) { // from class: up4
                public final ScheduledFuture a;

                {
                    this.a = schedule;
                }

                @Override // defpackage.im2
                public void a(c cVar) {
                    this.a.cancel(false);
                }
            });
        }

        public void b() {
            this.b.e(null);
        }

        public com.google.android.gms.tasks.c<Void> c() {
            return this.b.a();
        }

        public final /* synthetic */ void d() {
            String action = this.a.getAction();
            StringBuilder sb = new StringBuilder(String.valueOf(action).length() + 61);
            sb.append("Service took too long to process intent: ");
            sb.append(action);
            sb.append(" App may get closed.");
            b();
        }
    }

    public l(Context context, String str) {
        this(context, "com.google.firebase.MESSAGING_EVENT", new ScheduledThreadPoolExecutor(0, new yc2("Firebase-FirebaseInstanceIdServiceConnection")));
    }

    public final void a() {
        while (!this.h0.isEmpty()) {
            this.h0.poll().b();
        }
    }

    public final synchronized void b() {
        Log.isLoggable("FirebaseMessaging", 3);
        while (!this.h0.isEmpty()) {
            Log.isLoggable("FirebaseMessaging", 3);
            k kVar = this.i0;
            if (kVar != null && kVar.isBinderAlive()) {
                Log.isLoggable("FirebaseMessaging", 3);
                this.i0.b(this.h0.poll());
            } else {
                d();
                return;
            }
        }
    }

    public synchronized com.google.android.gms.tasks.c<Void> c(Intent intent) {
        a aVar;
        Log.isLoggable("FirebaseMessaging", 3);
        aVar = new a(intent);
        aVar.a(this.g0);
        this.h0.add(aVar);
        b();
        return aVar.c();
    }

    public final void d() {
        if (Log.isLoggable("FirebaseMessaging", 3)) {
            boolean z = this.j0;
            StringBuilder sb = new StringBuilder(39);
            sb.append("binder is dead. start connection? ");
            sb.append(!z);
        }
        if (this.j0) {
            return;
        }
        this.j0 = true;
        try {
            if (v50.b().a(this.a, this.f0, this, 65)) {
                return;
            }
        } catch (SecurityException unused) {
        }
        this.j0 = false;
        a();
    }

    @Override // android.content.ServiceConnection
    public synchronized void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (Log.isLoggable("FirebaseMessaging", 3)) {
            String valueOf = String.valueOf(componentName);
            StringBuilder sb = new StringBuilder(valueOf.length() + 20);
            sb.append("onServiceConnected: ");
            sb.append(valueOf);
        }
        this.j0 = false;
        if (!(iBinder instanceof k)) {
            String valueOf2 = String.valueOf(iBinder);
            StringBuilder sb2 = new StringBuilder(valueOf2.length() + 28);
            sb2.append("Invalid service connection: ");
            sb2.append(valueOf2);
            a();
            return;
        }
        this.i0 = (k) iBinder;
        b();
    }

    @Override // android.content.ServiceConnection
    public void onServiceDisconnected(ComponentName componentName) {
        if (Log.isLoggable("FirebaseMessaging", 3)) {
            String valueOf = String.valueOf(componentName);
            StringBuilder sb = new StringBuilder(valueOf.length() + 23);
            sb.append("onServiceDisconnected: ");
            sb.append(valueOf);
        }
        b();
    }

    public l(Context context, String str, ScheduledExecutorService scheduledExecutorService) {
        this.h0 = new ArrayDeque();
        this.j0 = false;
        Context applicationContext = context.getApplicationContext();
        this.a = applicationContext;
        this.f0 = new Intent("com.google.firebase.MESSAGING_EVENT").setPackage(applicationContext.getPackageName());
        this.g0 = scheduledExecutorService;
    }
}
