package com.google.firebase.messaging;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.util.Base64;
import com.google.android.gms.tasks.c;
import com.google.firebase.messaging.b;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;

/* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
/* loaded from: classes2.dex */
public class b {
    public static final Object c = new Object();
    public static l d;
    public final Context a;
    public final Executor b = j21.a;

    public b(Context context) {
        this.a = context;
    }

    public static com.google.android.gms.tasks.c<Integer> a(Context context, Intent intent) {
        return b(context, "com.google.firebase.MESSAGING_EVENT").c(intent).i(m21.a, n21.a);
    }

    public static l b(Context context, String str) {
        l lVar;
        synchronized (c) {
            if (d == null) {
                d = new l(context, "com.google.firebase.MESSAGING_EVENT");
            }
            lVar = d;
        }
        return lVar;
    }

    public static final /* synthetic */ Integer c(com.google.android.gms.tasks.c cVar) throws Exception {
        return -1;
    }

    public static final /* synthetic */ Integer e(com.google.android.gms.tasks.c cVar) throws Exception {
        return 403;
    }

    public static final /* synthetic */ com.google.android.gms.tasks.c f(Context context, Intent intent, com.google.android.gms.tasks.c cVar) throws Exception {
        return (jr2.h() && ((Integer) cVar.l()).intValue() == 402) ? a(context, intent).i(o21.a, p21.a) : cVar;
    }

    public com.google.android.gms.tasks.c<Integer> g(Intent intent) {
        String stringExtra = intent.getStringExtra("gcm.rawData64");
        if (stringExtra != null) {
            intent.putExtra("rawData", Base64.decode(stringExtra, 0));
            intent.removeExtra("gcm.rawData64");
        }
        return h(this.a, intent);
    }

    @SuppressLint({"InlinedApi"})
    public com.google.android.gms.tasks.c<Integer> h(final Context context, final Intent intent) {
        boolean z = false;
        if (jr2.h() && context.getApplicationInfo().targetSdkVersion >= 26) {
            z = true;
        }
        int flags = intent.getFlags() & 268435456;
        if (z && flags == 0) {
            return a(context, intent);
        }
        return com.google.android.gms.tasks.d.c(this.b, new Callable(context, intent) { // from class: k21
            public final Context a;
            public final Intent b;

            {
                this.a = context;
                this.b = intent;
            }

            @Override // java.util.concurrent.Callable
            public Object call() {
                Integer valueOf;
                valueOf = Integer.valueOf(im3.b().g(this.a, this.b));
                return valueOf;
            }
        }).j(this.b, new com.google.android.gms.tasks.a(context, intent) { // from class: l21
            public final Context a;
            public final Intent b;

            {
                this.a = context;
                this.b = intent;
            }

            @Override // com.google.android.gms.tasks.a
            public Object a(c cVar) {
                return b.f(this.a, this.b, cVar);
            }
        });
    }
}
