package com.google.firebase.messaging;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.c;
import com.google.firebase.messaging.j;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
/* loaded from: classes2.dex */
public class j {
    public static final long j = TimeUnit.HOURS.toSeconds(8);
    public final Context a;
    public final i82 b;
    public final c c;
    public final FirebaseMessaging d;
    public final k51 e;
    public final ScheduledExecutorService g;
    public final p74 i;
    public final Map<String, ArrayDeque<n34<Void>>> f = new rh();
    public boolean h = false;

    public j(FirebaseMessaging firebaseMessaging, k51 k51Var, i82 i82Var, p74 p74Var, c cVar, Context context, ScheduledExecutorService scheduledExecutorService) {
        this.d = firebaseMessaging;
        this.e = k51Var;
        this.b = i82Var;
        this.i = p74Var;
        this.c = cVar;
        this.a = context;
        this.g = scheduledExecutorService;
    }

    public static <T> T a(com.google.android.gms.tasks.c<T> cVar) throws IOException {
        try {
            return (T) com.google.android.gms.tasks.d.b(cVar, 30L, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e = e;
            throw new IOException("SERVICE_NOT_AVAILABLE", e);
        } catch (ExecutionException e2) {
            Throwable cause = e2.getCause();
            if (!(cause instanceof IOException)) {
                if (cause instanceof RuntimeException) {
                    throw ((RuntimeException) cause);
                }
                throw new IOException(e2);
            }
            throw ((IOException) cause);
        } catch (TimeoutException e3) {
            e = e3;
            throw new IOException("SERVICE_NOT_AVAILABLE", e);
        }
    }

    public static com.google.android.gms.tasks.c<j> d(final FirebaseMessaging firebaseMessaging, final k51 k51Var, final i82 i82Var, final c cVar, final Context context, final ScheduledExecutorService scheduledExecutorService) {
        return com.google.android.gms.tasks.d.c(scheduledExecutorService, new Callable(context, scheduledExecutorService, firebaseMessaging, k51Var, i82Var, cVar) { // from class: q74
            public final Context a;
            public final ScheduledExecutorService b;
            public final FirebaseMessaging c;
            public final k51 d;
            public final i82 e;
            public final c f;

            {
                this.a = context;
                this.b = scheduledExecutorService;
                this.c = firebaseMessaging;
                this.d = k51Var;
                this.e = i82Var;
                this.f = cVar;
            }

            @Override // java.util.concurrent.Callable
            public Object call() {
                return j.h(this.a, this.b, this.c, this.d, this.e, this.f);
            }
        });
    }

    public static boolean f() {
        return Log.isLoggable("FirebaseMessaging", 3) || (Build.VERSION.SDK_INT == 23 && Log.isLoggable("FirebaseMessaging", 3));
    }

    public static final /* synthetic */ j h(Context context, ScheduledExecutorService scheduledExecutorService, FirebaseMessaging firebaseMessaging, k51 k51Var, i82 i82Var, c cVar) throws Exception {
        return new j(firebaseMessaging, k51Var, i82Var, p74.a(context, scheduledExecutorService), cVar, context, scheduledExecutorService);
    }

    public final void b(String str) throws IOException {
        a(this.c.k((String) a(this.e.getId()), this.d.c(), str));
    }

    public final void c(String str) throws IOException {
        a(this.c.l((String) a(this.e.getId()), this.d.c(), str));
    }

    public boolean e() {
        return this.i.b() != null;
    }

    public synchronized boolean g() {
        return this.h;
    }

    public final void i(o74 o74Var) {
        synchronized (this.f) {
            String e = o74Var.e();
            if (this.f.containsKey(e)) {
                ArrayDeque<n34<Void>> arrayDeque = this.f.get(e);
                n34<Void> poll = arrayDeque.poll();
                if (poll != null) {
                    poll.c(null);
                }
                if (arrayDeque.isEmpty()) {
                    this.f.remove(e);
                }
            }
        }
    }

    public boolean j(o74 o74Var) throws IOException {
        char c;
        try {
            String b = o74Var.b();
            int hashCode = b.hashCode();
            if (hashCode != 83) {
                if (hashCode == 85 && b.equals("U")) {
                    c = 1;
                }
                c = 65535;
            } else {
                if (b.equals("S")) {
                    c = 0;
                }
                c = 65535;
            }
            if (c == 0) {
                b(o74Var.c());
                if (f()) {
                    String c2 = o74Var.c();
                    StringBuilder sb = new StringBuilder(String.valueOf(c2).length() + 31);
                    sb.append("Subscribe to topic: ");
                    sb.append(c2);
                    sb.append(" succeeded.");
                }
            } else if (c != 1) {
                if (f()) {
                    String valueOf = String.valueOf(o74Var);
                    StringBuilder sb2 = new StringBuilder(valueOf.length() + 24);
                    sb2.append("Unknown topic operation");
                    sb2.append(valueOf);
                    sb2.append(".");
                }
            } else {
                c(o74Var.c());
                if (f()) {
                    String c3 = o74Var.c();
                    StringBuilder sb3 = new StringBuilder(String.valueOf(c3).length() + 35);
                    sb3.append("Unsubscribe from topic: ");
                    sb3.append(c3);
                    sb3.append(" succeeded.");
                }
            }
            return true;
        } catch (IOException e) {
            if (!"SERVICE_NOT_AVAILABLE".equals(e.getMessage()) && !"INTERNAL_SERVER_ERROR".equals(e.getMessage())) {
                if (e.getMessage() == null) {
                    return false;
                }
                throw e;
            }
            String message = e.getMessage();
            StringBuilder sb4 = new StringBuilder(String.valueOf(message).length() + 53);
            sb4.append("Topic operation failed: ");
            sb4.append(message);
            sb4.append(". Will retry Topic operation.");
            return false;
        }
    }

    public void k(Runnable runnable, long j2) {
        this.g.schedule(runnable, j2, TimeUnit.SECONDS);
    }

    public synchronized void l(boolean z) {
        this.h = z;
    }

    public final void m() {
        if (g()) {
            return;
        }
        p(0L);
    }

    public void n() {
        if (e()) {
            m();
        }
    }

    public boolean o() throws IOException {
        while (true) {
            synchronized (this) {
                o74 b = this.i.b();
                if (b == null) {
                    f();
                    return true;
                } else if (!j(b)) {
                    return false;
                } else {
                    this.i.d(b);
                    i(b);
                }
            }
        }
    }

    public void p(long j2) {
        k(new r74(this, this.a, this.b, Math.min(Math.max(30L, j2 + j2), j)), j2);
        l(true);
    }
}
