package com.google.firebase.messaging;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import androidx.annotation.Keep;
import com.google.android.gms.tasks.c;
import com.google.firebase.heartbeatinfo.HeartBeatInfo;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.g;
import com.google.firebase.messaging.h;
import com.google.firebase.messaging.j;
import defpackage.m51;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
/* loaded from: classes2.dex */
public class FirebaseMessaging {
    public static final long n = TimeUnit.HOURS.toSeconds(8);
    @SuppressLint({"StaticFieldLeak"})
    public static h o;
    @SuppressLint({"FirebaseUnknownNullness"})
    public static pb4 p;
    public static ScheduledExecutorService q;
    public final c51 a;
    public final m51 b;
    public final k51 c;
    public final Context d;
    public final c e;
    public final g f;
    public final a g;
    public final Executor h;
    public final Executor i;
    public final com.google.android.gms.tasks.c<j> j;
    public final i82 k;
    public boolean l;
    public final Application.ActivityLifecycleCallbacks m;

    /* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
    /* loaded from: classes2.dex */
    public class a {
        public final mv3 a;
        public boolean b;
        public vx0<de0> c;
        public Boolean d;

        public a(mv3 mv3Var) {
            this.a = mv3Var;
        }

        public synchronized void a() {
            if (this.b) {
                return;
            }
            Boolean d = d();
            this.d = d;
            if (d == null) {
                vx0<de0> vx0Var = new vx0(this) { // from class: w51
                    public final FirebaseMessaging.a a;

                    {
                        this.a = this;
                    }

                    @Override // defpackage.vx0
                    public void a(qx0 qx0Var) {
                        this.a.c(qx0Var);
                    }
                };
                this.c = vx0Var;
                this.a.b(de0.class, vx0Var);
            }
            this.b = true;
        }

        public synchronized boolean b() {
            boolean q;
            a();
            Boolean bool = this.d;
            if (bool == null) {
                q = FirebaseMessaging.this.a.q();
            } else {
                q = bool.booleanValue();
            }
            return q;
        }

        public final /* synthetic */ void c(qx0 qx0Var) {
            if (b()) {
                FirebaseMessaging.this.y();
            }
        }

        public final Boolean d() {
            ApplicationInfo applicationInfo;
            Bundle bundle;
            Context h = FirebaseMessaging.this.a.h();
            SharedPreferences sharedPreferences = h.getSharedPreferences("com.google.firebase.messaging", 0);
            if (sharedPreferences.contains("auto_init")) {
                return Boolean.valueOf(sharedPreferences.getBoolean("auto_init", false));
            }
            try {
                PackageManager packageManager = h.getPackageManager();
                if (packageManager == null || (applicationInfo = packageManager.getApplicationInfo(h.getPackageName(), 128)) == null || (bundle = applicationInfo.metaData) == null || !bundle.containsKey("firebase_messaging_auto_init_enabled")) {
                    return null;
                }
                return Boolean.valueOf(applicationInfo.metaData.getBoolean("firebase_messaging_auto_init_enabled"));
            } catch (PackageManager.NameNotFoundException unused) {
                return null;
            }
        }
    }

    public FirebaseMessaging(c51 c51Var, m51 m51Var, fw2<wf4> fw2Var, fw2<HeartBeatInfo> fw2Var2, k51 k51Var, pb4 pb4Var, mv3 mv3Var) {
        this(c51Var, m51Var, fw2Var, fw2Var2, k51Var, pb4Var, mv3Var, new i82(c51Var.h()));
    }

    public static synchronized FirebaseMessaging g() {
        FirebaseMessaging firebaseMessaging;
        synchronized (FirebaseMessaging.class) {
            firebaseMessaging = getInstance(c51.i());
        }
        return firebaseMessaging;
    }

    @Keep
    public static synchronized FirebaseMessaging getInstance(c51 c51Var) {
        FirebaseMessaging firebaseMessaging;
        synchronized (FirebaseMessaging.class) {
            firebaseMessaging = (FirebaseMessaging) c51Var.g(FirebaseMessaging.class);
            zt2.k(firebaseMessaging, "Firebase Messaging component is not present");
        }
        return firebaseMessaging;
    }

    public static pb4 k() {
        return p;
    }

    public boolean A(h.a aVar) {
        return aVar == null || aVar.b(this.k.a());
    }

    public String c() throws IOException {
        m51 m51Var = this.b;
        if (m51Var != null) {
            try {
                return (String) com.google.android.gms.tasks.d.a(m51Var.b());
            } catch (InterruptedException | ExecutionException e) {
                throw new IOException(e);
            }
        }
        h.a j = j();
        if (!A(j)) {
            return j.a;
        }
        final String c = i82.c(this.a);
        try {
            String str = (String) com.google.android.gms.tasks.d.a(this.c.getId().j(q21.d(), new com.google.android.gms.tasks.a(this, c) { // from class: u51
                public final FirebaseMessaging a;
                public final String b;

                {
                    this.a = this;
                    this.b = c;
                }

                @Override // com.google.android.gms.tasks.a
                public Object a(c cVar) {
                    return this.a.p(this.b, cVar);
                }
            }));
            o.g(h(), c, str, this.k.a());
            if (j == null || !str.equals(j.a)) {
                l(str);
            }
            return str;
        } catch (InterruptedException | ExecutionException e2) {
            throw new IOException(e2);
        }
    }

    public com.google.android.gms.tasks.c<Void> d() {
        if (this.b != null) {
            final n34 n34Var = new n34();
            this.h.execute(new Runnable(this, n34Var) { // from class: s51
                public final FirebaseMessaging a;
                public final n34 f0;

                {
                    this.a = this;
                    this.f0 = n34Var;
                }

                @Override // java.lang.Runnable
                public void run() {
                    this.a.q(this.f0);
                }
            });
            return n34Var.a();
        } else if (j() == null) {
            return com.google.android.gms.tasks.d.e(null);
        } else {
            final ExecutorService d = q21.d();
            return this.c.getId().j(d, new com.google.android.gms.tasks.a(this, d) { // from class: t51
                public final FirebaseMessaging a;
                public final ExecutorService b;

                {
                    this.a = this;
                    this.b = d;
                }

                @Override // com.google.android.gms.tasks.a
                public Object a(c cVar) {
                    return this.a.s(this.b, cVar);
                }
            });
        }
    }

    public void e(Runnable runnable, long j) {
        synchronized (FirebaseMessaging.class) {
            if (q == null) {
                q = new ScheduledThreadPoolExecutor(1, new yc2("TAG"));
            }
            q.schedule(runnable, j, TimeUnit.SECONDS);
        }
    }

    public Context f() {
        return this.d;
    }

    public final String h() {
        return "[DEFAULT]".equals(this.a.j()) ? "" : this.a.l();
    }

    public com.google.android.gms.tasks.c<String> i() {
        m51 m51Var = this.b;
        if (m51Var != null) {
            return m51Var.b();
        }
        final n34 n34Var = new n34();
        this.h.execute(new Runnable(this, n34Var) { // from class: r51
            public final FirebaseMessaging a;
            public final n34 f0;

            {
                this.a = this;
                this.f0 = n34Var;
            }

            @Override // java.lang.Runnable
            public void run() {
                this.a.t(this.f0);
            }
        });
        return n34Var.a();
    }

    public h.a j() {
        return o.e(h(), i82.c(this.a));
    }

    public final void l(String str) {
        if ("[DEFAULT]".equals(this.a.j())) {
            if (Log.isLoggable("FirebaseMessaging", 3)) {
                String valueOf = String.valueOf(this.a.j());
                if (valueOf.length() != 0) {
                    "Invoking onNewToken for app: ".concat(valueOf);
                }
            }
            Intent intent = new Intent("com.google.firebase.messaging.NEW_TOKEN");
            intent.putExtra("token", str);
            new b(this.d).g(intent);
        }
    }

    public boolean m() {
        return this.g.b();
    }

    public boolean n() {
        return this.k.g();
    }

    public final /* synthetic */ com.google.android.gms.tasks.c o(com.google.android.gms.tasks.c cVar) {
        return this.e.e((String) cVar.l());
    }

    public final /* synthetic */ com.google.android.gms.tasks.c p(String str, final com.google.android.gms.tasks.c cVar) throws Exception {
        return this.f.a(str, new g.a(this, cVar) { // from class: v51
            public final FirebaseMessaging a;
            public final c b;

            {
                this.a = this;
                this.b = cVar;
            }

            @Override // com.google.firebase.messaging.g.a
            public c start() {
                return this.a.o(this.b);
            }
        });
    }

    public final /* synthetic */ void q(n34 n34Var) {
        try {
            this.b.a(i82.c(this.a), "FCM");
            n34Var.c(null);
        } catch (Exception e) {
            n34Var.b(e);
        }
    }

    public final /* synthetic */ Void r(com.google.android.gms.tasks.c cVar) throws Exception {
        o.d(h(), i82.c(this.a));
        return null;
    }

    public final /* synthetic */ com.google.android.gms.tasks.c s(ExecutorService executorService, com.google.android.gms.tasks.c cVar) throws Exception {
        return this.e.b((String) cVar.l()).i(executorService, new com.google.android.gms.tasks.a(this) { // from class: o51
            public final FirebaseMessaging a;

            {
                this.a = this;
            }

            @Override // com.google.android.gms.tasks.a
            public Object a(c cVar2) {
                this.a.r(cVar2);
                return null;
            }
        });
    }

    public final /* synthetic */ void t(n34 n34Var) {
        try {
            n34Var.c(c());
        } catch (Exception e) {
            n34Var.b(e);
        }
    }

    public final /* synthetic */ void u() {
        if (m()) {
            y();
        }
    }

    public final /* synthetic */ void v(j jVar) {
        if (m()) {
            jVar.n();
        }
    }

    public synchronized void w(boolean z) {
        this.l = z;
    }

    public final synchronized void x() {
        if (this.l) {
            return;
        }
        z(0L);
    }

    public final void y() {
        m51 m51Var = this.b;
        if (m51Var != null) {
            m51Var.d();
        } else if (A(j())) {
            x();
        }
    }

    public synchronized void z(long j) {
        e(new i(this, Math.min(Math.max(30L, j + j), n)), j);
        this.l = true;
    }

    public FirebaseMessaging(c51 c51Var, m51 m51Var, fw2<wf4> fw2Var, fw2<HeartBeatInfo> fw2Var2, k51 k51Var, pb4 pb4Var, mv3 mv3Var, i82 i82Var) {
        this(c51Var, m51Var, k51Var, pb4Var, mv3Var, i82Var, new c(c51Var, i82Var, fw2Var, fw2Var2, k51Var), q21.e(), q21.b());
    }

    public FirebaseMessaging(c51 c51Var, m51 m51Var, k51 k51Var, pb4 pb4Var, mv3 mv3Var, i82 i82Var, c cVar, Executor executor, Executor executor2) {
        this.l = false;
        p = pb4Var;
        this.a = c51Var;
        this.b = m51Var;
        this.c = k51Var;
        this.g = new a(mv3Var);
        Context h = c51Var.h();
        this.d = h;
        r21 r21Var = new r21();
        this.m = r21Var;
        this.k = i82Var;
        this.i = executor;
        this.e = cVar;
        this.f = new g(executor);
        this.h = executor2;
        Context h2 = c51Var.h();
        if (h2 instanceof Application) {
            ((Application) h2).registerActivityLifecycleCallbacks(r21Var);
        } else {
            String valueOf = String.valueOf(h2);
            StringBuilder sb = new StringBuilder(valueOf.length() + 125);
            sb.append("Context ");
            sb.append(valueOf);
            sb.append(" was not an application, can't register for lifecycle callbacks. Some notification events may be dropped as a result.");
        }
        if (m51Var != null) {
            m51Var.c(new m51.a(this) { // from class: n51
            });
        }
        synchronized (FirebaseMessaging.class) {
            if (o == null) {
                o = new h(h);
            }
        }
        executor2.execute(new Runnable(this) { // from class: p51
            public final FirebaseMessaging a;

            {
                this.a = this;
            }

            @Override // java.lang.Runnable
            public void run() {
                this.a.u();
            }
        });
        com.google.android.gms.tasks.c<j> d = j.d(this, k51Var, i82Var, cVar, h, q21.f());
        this.j = d;
        d.g(q21.g(), new um2(this) { // from class: q51
            public final FirebaseMessaging a;

            {
                this.a = this;
            }

            @Override // defpackage.um2
            public void a(Object obj) {
                this.a.v((j) obj);
            }
        });
    }
}
