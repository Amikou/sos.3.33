package com.google.firebase.messaging;

import androidx.annotation.Keep;
import com.google.firebase.heartbeatinfo.HeartBeatInfo;
import java.util.Arrays;
import java.util.List;

/* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
@Keep
/* loaded from: classes2.dex */
public class FirebaseMessagingRegistrar implements g40 {
    public static final /* synthetic */ FirebaseMessaging lambda$getComponents$0$FirebaseMessagingRegistrar(b40 b40Var) {
        return new FirebaseMessaging((c51) b40Var.a(c51.class), (m51) b40Var.a(m51.class), b40Var.b(wf4.class), b40Var.b(HeartBeatInfo.class), (k51) b40Var.a(k51.class), (pb4) b40Var.a(pb4.class), (mv3) b40Var.a(mv3.class));
    }

    @Override // defpackage.g40
    @Keep
    public List<a40<?>> getComponents() {
        return Arrays.asList(a40.c(FirebaseMessaging.class).b(hm0.j(c51.class)).b(hm0.h(m51.class)).b(hm0.i(wf4.class)).b(hm0.i(HeartBeatInfo.class)).b(hm0.h(pb4.class)).b(hm0.j(k51.class)).b(hm0.j(mv3.class)).f(x51.a).c().d(), jz1.b("fire-fcm", "22.0.0"));
    }
}
