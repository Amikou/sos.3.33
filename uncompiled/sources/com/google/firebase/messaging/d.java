package com.google.firebase.messaging;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.util.Log;
import com.google.firebase.messaging.d;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;

/* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
/* loaded from: classes2.dex */
public class d implements Closeable {
    public final URL a;
    public com.google.android.gms.tasks.c<Bitmap> f0;
    public volatile InputStream g0;

    public d(URL url) {
        this.a = url;
    }

    public static d c(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return new d(new URL(str));
        } catch (MalformedURLException unused) {
            String valueOf = String.valueOf(str);
            if (valueOf.length() != 0) {
                "Not downloading image, bad URL: ".concat(valueOf);
            }
            return null;
        }
    }

    public Bitmap a() throws IOException {
        String valueOf = String.valueOf(this.a);
        StringBuilder sb = new StringBuilder(valueOf.length() + 22);
        sb.append("Starting download of: ");
        sb.append(valueOf);
        byte[] b = b();
        Bitmap decodeByteArray = BitmapFactory.decodeByteArray(b, 0, b.length);
        if (decodeByteArray != null) {
            if (Log.isLoggable("FirebaseMessaging", 3)) {
                String valueOf2 = String.valueOf(this.a);
                StringBuilder sb2 = new StringBuilder(valueOf2.length() + 31);
                sb2.append("Successfully downloaded image: ");
                sb2.append(valueOf2);
            }
            return decodeByteArray;
        }
        String valueOf3 = String.valueOf(this.a);
        StringBuilder sb3 = new StringBuilder(valueOf3.length() + 24);
        sb3.append("Failed to decode image: ");
        sb3.append(valueOf3);
        throw new IOException(sb3.toString());
    }

    public final byte[] b() throws IOException {
        URLConnection openConnection = this.a.openConnection();
        if (openConnection.getContentLength() <= 1048576) {
            InputStream inputStream = openConnection.getInputStream();
            try {
                this.g0 = inputStream;
                byte[] a = com.google.android.gms.internal.firebase_messaging.h.a(com.google.android.gms.internal.firebase_messaging.h.b(inputStream, 1048577L));
                if (inputStream != null) {
                    inputStream.close();
                }
                if (Log.isLoggable("FirebaseMessaging", 2)) {
                    String valueOf = String.valueOf(this.a);
                    StringBuilder sb = new StringBuilder(valueOf.length() + 34);
                    sb.append("Downloaded ");
                    sb.append(a.length);
                    sb.append(" bytes from ");
                    sb.append(valueOf);
                }
                if (a.length <= 1048576) {
                    return a;
                }
                throw new IOException("Image exceeds max size of 1048576");
            } catch (Throwable th) {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (Throwable th2) {
                        t46.a(th, th2);
                    }
                }
                throw th;
            }
        }
        throw new IOException("Content-Length exceeds max size of 1048576");
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        try {
            xy5.a(this.g0);
        } catch (NullPointerException unused) {
        }
    }

    public com.google.android.gms.tasks.c<Bitmap> d() {
        return (com.google.android.gms.tasks.c) zt2.j(this.f0);
    }

    public void e(Executor executor) {
        this.f0 = com.google.android.gms.tasks.d.c(executor, new Callable(this) { // from class: vn1
            public final d a;

            {
                this.a = this;
            }

            @Override // java.util.concurrent.Callable
            public Object call() {
                return this.a.a();
            }
        });
    }
}
