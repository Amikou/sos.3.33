package com.google.firebase.messaging;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import com.google.android.gms.tasks.c;
import com.google.firebase.messaging.EnhancedIntentService;
import com.google.firebase.messaging.k;
import java.util.concurrent.ExecutorService;

/* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
@SuppressLint({"UnwrappedWakefulBroadcastReceiver"})
/* loaded from: classes2.dex */
public abstract class EnhancedIntentService extends Service {
    public Binder f0;
    public int h0;
    public final ExecutorService a = q21.c();
    public final Object g0 = new Object();
    public int i0 = 0;

    /* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
    /* loaded from: classes2.dex */
    public class a implements k.a {
        public a() {
        }

        @Override // com.google.firebase.messaging.k.a
        public com.google.android.gms.tasks.c<Void> a(Intent intent) {
            return EnhancedIntentService.this.h(intent);
        }
    }

    public final void b(Intent intent) {
        if (intent != null) {
            ol4.b(intent);
        }
        synchronized (this.g0) {
            int i = this.i0 - 1;
            this.i0 = i;
            if (i == 0) {
                i(this.h0);
            }
        }
    }

    public Intent c(Intent intent) {
        return intent;
    }

    public abstract void d(Intent intent);

    public boolean e(Intent intent) {
        return false;
    }

    public final /* synthetic */ void f(Intent intent, com.google.android.gms.tasks.c cVar) {
        b(intent);
    }

    public final /* synthetic */ void g(Intent intent, n34 n34Var) {
        try {
            d(intent);
        } finally {
            n34Var.c(null);
        }
    }

    public final com.google.android.gms.tasks.c<Void> h(final Intent intent) {
        if (e(intent)) {
            return com.google.android.gms.tasks.d.e(null);
        }
        final n34 n34Var = new n34();
        this.a.execute(new Runnable(this, intent, n34Var) { // from class: qv0
            public final EnhancedIntentService a;
            public final Intent f0;
            public final n34 g0;

            {
                this.a = this;
                this.f0 = intent;
                this.g0 = n34Var;
            }

            @Override // java.lang.Runnable
            public void run() {
                this.a.g(this.f0, this.g0);
            }
        });
        return n34Var.a();
    }

    public boolean i(int i) {
        return stopSelfResult(i);
    }

    @Override // android.app.Service
    public final synchronized IBinder onBind(Intent intent) {
        Log.isLoggable("EnhancedIntentService", 3);
        if (this.f0 == null) {
            this.f0 = new k(new a());
        }
        return this.f0;
    }

    @Override // android.app.Service
    public void onDestroy() {
        this.a.shutdown();
        super.onDestroy();
    }

    @Override // android.app.Service
    public final int onStartCommand(final Intent intent, int i, int i2) {
        synchronized (this.g0) {
            this.h0 = i2;
            this.i0++;
        }
        Intent c = c(intent);
        if (c == null) {
            b(intent);
            return 2;
        }
        com.google.android.gms.tasks.c<Void> h = h(c);
        if (h.o()) {
            b(intent);
            return 2;
        }
        h.c(rv0.a, new im2(this, intent) { // from class: sv0
            public final EnhancedIntentService a;
            public final Intent b;

            {
                this.a = this;
                this.b = intent;
            }

            @Override // defpackage.im2
            public void a(c cVar) {
                this.a.f(this.b, cVar);
            }
        });
        return 3;
    }
}
