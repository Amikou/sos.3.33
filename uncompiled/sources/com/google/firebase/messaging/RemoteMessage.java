package com.google.firebase.messaging;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.firebase.messaging.a;
import java.util.Map;

/* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
/* loaded from: classes2.dex */
public final class RemoteMessage extends AbstractSafeParcelable {
    public static final Parcelable.Creator<RemoteMessage> CREATOR = new q63();
    public Bundle a;
    public Map<String, String> f0;
    public b g0;

    /* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
    /* loaded from: classes2.dex */
    public static class b {
        public final String a;
        public final String b;

        public b(f fVar) {
            this.a = fVar.p("gcm.n.title");
            fVar.h("gcm.n.title");
            b(fVar, "gcm.n.title");
            this.b = fVar.p("gcm.n.body");
            fVar.h("gcm.n.body");
            b(fVar, "gcm.n.body");
            fVar.p("gcm.n.icon");
            fVar.o();
            fVar.p("gcm.n.tag");
            fVar.p("gcm.n.color");
            fVar.p("gcm.n.click_action");
            fVar.p("gcm.n.android_channel_id");
            fVar.f();
            fVar.p("gcm.n.image");
            fVar.p("gcm.n.ticker");
            fVar.b("gcm.n.notification_priority");
            fVar.b("gcm.n.visibility");
            fVar.b("gcm.n.notification_count");
            fVar.a("gcm.n.sticky");
            fVar.a("gcm.n.local_only");
            fVar.a("gcm.n.default_sound");
            fVar.a("gcm.n.default_vibrate_timings");
            fVar.a("gcm.n.default_light_settings");
            fVar.j("gcm.n.event_time");
            fVar.e();
            fVar.q();
        }

        public static String[] b(f fVar, String str) {
            Object[] g = fVar.g(str);
            if (g == null) {
                return null;
            }
            String[] strArr = new String[g.length];
            for (int i = 0; i < g.length; i++) {
                strArr[i] = String.valueOf(g[i]);
            }
            return strArr;
        }

        public String a() {
            return this.b;
        }

        public String c() {
            return this.a;
        }
    }

    public RemoteMessage(Bundle bundle) {
        this.a = bundle;
    }

    public Map<String, String> I1() {
        if (this.f0 == null) {
            this.f0 = a.C0148a.a(this.a);
        }
        return this.f0;
    }

    public b J1() {
        if (this.g0 == null && f.t(this.a)) {
            this.g0 = new b(new f(this.a));
        }
        return this.g0;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        q63.c(this, parcel, i);
    }
}
