package com.google.firebase.messaging;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import com.google.firebase.messaging.reporting.MessagingClientEvent;
import defpackage.g82;
import java.util.concurrent.ExecutionException;

/* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
/* loaded from: classes2.dex */
public class e {
    public static boolean A(Intent intent) {
        if (intent == null || r(intent)) {
            return false;
        }
        return B(intent.getExtras());
    }

    public static boolean B(Bundle bundle) {
        if (bundle == null) {
            return false;
        }
        return "1".equals(bundle.getString("google.c.a.e"));
    }

    public static boolean a() {
        Context h;
        SharedPreferences sharedPreferences;
        ApplicationInfo applicationInfo;
        Bundle bundle;
        try {
            c51.i();
            h = c51.i().h();
            sharedPreferences = h.getSharedPreferences("com.google.firebase.messaging", 0);
        } catch (PackageManager.NameNotFoundException | IllegalStateException unused) {
        }
        if (sharedPreferences.contains("export_to_big_query")) {
            return sharedPreferences.getBoolean("export_to_big_query", false);
        }
        PackageManager packageManager = h.getPackageManager();
        if (packageManager != null && (applicationInfo = packageManager.getApplicationInfo(h.getPackageName(), 128)) != null && (bundle = applicationInfo.metaData) != null && bundle.containsKey("delivery_metrics_exported_to_big_query_enabled")) {
            return applicationInfo.metaData.getBoolean("delivery_metrics_exported_to_big_query_enabled", false);
        }
        return false;
    }

    public static MessagingClientEvent b(MessagingClientEvent.Event event, Intent intent) {
        if (intent == null) {
            return null;
        }
        Bundle extras = intent.getExtras();
        if (extras == null) {
            extras = Bundle.EMPTY;
        }
        MessagingClientEvent.a p = MessagingClientEvent.p();
        p.m(p(extras));
        p.e(event);
        p.f(f(extras));
        p.i(m());
        p.k(MessagingClientEvent.SDKPlatform.ANDROID);
        p.h(k(extras));
        String h = h(extras);
        if (h != null) {
            p.g(h);
        }
        String o = o(extras);
        if (o != null) {
            p.l(o);
        }
        String c = c(extras);
        if (c != null) {
            p.c(c);
        }
        String i = i(extras);
        if (i != null) {
            p.b(i);
        }
        String e = e(extras);
        if (e != null) {
            p.d(e);
        }
        long n = n(extras);
        if (n > 0) {
            p.j(n);
        }
        return p.a();
    }

    public static String c(Bundle bundle) {
        return bundle.getString("collapse_key");
    }

    public static String d(Bundle bundle) {
        return bundle.getString("google.c.a.c_id");
    }

    public static String e(Bundle bundle) {
        return bundle.getString("google.c.a.c_l");
    }

    public static String f(Bundle bundle) {
        String string = bundle.getString("google.to");
        if (TextUtils.isEmpty(string)) {
            try {
                return (String) com.google.android.gms.tasks.d.a(com.google.firebase.installations.b.o(c51.i()).getId());
            } catch (InterruptedException | ExecutionException e) {
                throw new RuntimeException(e);
            }
        }
        return string;
    }

    public static String g(Bundle bundle) {
        return bundle.getString("google.c.a.m_c");
    }

    public static String h(Bundle bundle) {
        String string = bundle.getString("google.message_id");
        return string == null ? bundle.getString("message_id") : string;
    }

    public static String i(Bundle bundle) {
        return bundle.getString("google.c.a.m_l");
    }

    public static String j(Bundle bundle) {
        return bundle.getString("google.c.a.ts");
    }

    public static MessagingClientEvent.MessageType k(Bundle bundle) {
        if (bundle != null && f.t(bundle)) {
            return MessagingClientEvent.MessageType.DISPLAY_NOTIFICATION;
        }
        return MessagingClientEvent.MessageType.DATA_MESSAGE;
    }

    public static String l(Bundle bundle) {
        return true != f.t(bundle) ? "data" : "display";
    }

    public static String m() {
        return c51.i().h().getPackageName();
    }

    public static long n(Bundle bundle) {
        if (bundle.containsKey("google.c.sender.id")) {
            try {
                return Long.parseLong(bundle.getString("google.c.sender.id"));
            } catch (NumberFormatException unused) {
            }
        }
        c51 i = c51.i();
        String d = i.k().d();
        if (d != null) {
            try {
                return Long.parseLong(d);
            } catch (NumberFormatException unused2) {
            }
        }
        String c = i.k().c();
        try {
            if (!c.startsWith("1:")) {
                return Long.parseLong(c);
            }
            String[] split = c.split(":");
            if (split.length < 2) {
                return 0L;
            }
            String str = split[1];
            if (str.isEmpty()) {
                return 0L;
            }
            return Long.parseLong(str);
        } catch (NumberFormatException unused3) {
            return 0L;
        }
    }

    public static String o(Bundle bundle) {
        String string = bundle.getString("from");
        if (string == null || !string.startsWith("/topics/")) {
            return null;
        }
        return string;
    }

    public static int p(Bundle bundle) {
        Object obj = bundle.get("google.ttl");
        if (obj instanceof Integer) {
            return ((Integer) obj).intValue();
        }
        if (obj instanceof String) {
            try {
                return Integer.parseInt((String) obj);
            } catch (NumberFormatException unused) {
                String valueOf = String.valueOf(obj);
                StringBuilder sb = new StringBuilder(valueOf.length() + 13);
                sb.append("Invalid TTL: ");
                sb.append(valueOf);
                return 0;
            }
        }
        return 0;
    }

    public static String q(Bundle bundle) {
        if (bundle.containsKey("google.c.a.udt")) {
            return bundle.getString("google.c.a.udt");
        }
        return null;
    }

    public static boolean r(Intent intent) {
        return "com.google.firebase.messaging.RECEIVE_DIRECT_BOOT".equals(intent.getAction());
    }

    public static void s(Intent intent) {
        x("_nd", intent.getExtras());
    }

    public static void t(Intent intent) {
        x("_nf", intent.getExtras());
    }

    public static void u(Bundle bundle) {
        y(bundle);
        x("_no", bundle);
    }

    public static void v(Intent intent) {
        if (A(intent)) {
            x("_nr", intent.getExtras());
        }
        if (z(intent)) {
            w(MessagingClientEvent.Event.MESSAGE_DELIVERED, intent, FirebaseMessaging.k());
        }
    }

    public static void w(MessagingClientEvent.Event event, Intent intent, pb4 pb4Var) {
        MessagingClientEvent b;
        if (pb4Var == null || (b = b(event, intent)) == null) {
            return;
        }
        try {
            mb4 a = pb4Var.a("FCM_CLIENT_EVENT_LOGGING", g82.class, hv0.b("proto"), f82.a);
            g82.a b2 = g82.b();
            b2.b(b);
            a.a(com.google.android.datatransport.a.d(b2.a()));
        } catch (RuntimeException unused) {
        }
    }

    public static void x(String str, Bundle bundle) {
        if (bundle == null) {
            bundle = new Bundle();
        }
        Bundle bundle2 = new Bundle();
        String d = d(bundle);
        if (d != null) {
            bundle2.putString("_nmid", d);
        }
        String e = e(bundle);
        if (e != null) {
            bundle2.putString("_nmn", e);
        }
        String i = i(bundle);
        if (!TextUtils.isEmpty(i)) {
            bundle2.putString("label", i);
        }
        String g = g(bundle);
        if (!TextUtils.isEmpty(g)) {
            bundle2.putString("message_channel", g);
        }
        String o = o(bundle);
        if (o != null) {
            bundle2.putString("_nt", o);
        }
        String j = j(bundle);
        if (j != null) {
            try {
                bundle2.putInt("_nmt", Integer.parseInt(j));
            } catch (NumberFormatException unused) {
            }
        }
        String q = q(bundle);
        if (q != null) {
            try {
                bundle2.putInt("_ndt", Integer.parseInt(q));
            } catch (NumberFormatException unused2) {
            }
        }
        String l = l(bundle);
        if ("_nr".equals(str) || "_nf".equals(str)) {
            bundle2.putString("_nmc", l);
        }
        if (Log.isLoggable("FirebaseMessaging", 3)) {
            String valueOf = String.valueOf(bundle2);
            StringBuilder sb = new StringBuilder(str.length() + 37 + valueOf.length());
            sb.append("Logging to scion event=");
            sb.append(str);
            sb.append(" scionPayload=");
            sb.append(valueOf);
        }
        kb kbVar = (kb) c51.i().g(kb.class);
        if (kbVar != null) {
            kbVar.b("fcm", str, bundle2);
        }
    }

    public static void y(Bundle bundle) {
        kb kbVar;
        if (bundle == null || !"1".equals(bundle.getString("google.c.a.tc")) || (kbVar = (kb) c51.i().g(kb.class)) == null) {
            return;
        }
        String string = bundle.getString("google.c.a.c_id");
        kbVar.c("fcm", "_ln", string);
        Bundle bundle2 = new Bundle();
        bundle2.putString("source", "Firebase");
        bundle2.putString("medium", "notification");
        bundle2.putString("campaign", string);
        kbVar.b("fcm", "_cmp", bundle2);
    }

    public static boolean z(Intent intent) {
        if (intent == null || r(intent)) {
            return false;
        }
        return a();
    }
}
