package com.google.firebase.messaging.reporting;

import com.google.android.gms.internal.firebase_messaging.i;

/* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
/* loaded from: classes2.dex */
public final class MessagingClientEvent {
    public final long a;
    public final String b;
    public final String c;
    public final MessageType d;
    public final SDKPlatform e;
    public final String f;
    public final String g;
    public final int h;
    public final int i;
    public final String j;
    public final long k;
    public final Event l;
    public final String m;
    public final long n;
    public final String o;

    /* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
    /* loaded from: classes2.dex */
    public enum Event implements q56 {
        UNKNOWN_EVENT(0),
        MESSAGE_DELIVERED(1),
        MESSAGE_OPEN(2);
        
        private final int number_;

        Event(int i) {
            this.number_ = i;
        }

        @Override // defpackage.q56
        public int getNumber() {
            return this.number_;
        }
    }

    /* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
    /* loaded from: classes2.dex */
    public enum MessageType implements q56 {
        UNKNOWN(0),
        DATA_MESSAGE(1),
        TOPIC(2),
        DISPLAY_NOTIFICATION(3);
        
        private final int number_;

        MessageType(int i) {
            this.number_ = i;
        }

        @Override // defpackage.q56
        public int getNumber() {
            return this.number_;
        }
    }

    /* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
    /* loaded from: classes2.dex */
    public enum SDKPlatform implements q56 {
        UNKNOWN_OS(0),
        ANDROID(1),
        IOS(2),
        WEB(3);
        
        private final int number_;

        SDKPlatform(int i) {
            this.number_ = i;
        }

        @Override // defpackage.q56
        public int getNumber() {
            return this.number_;
        }
    }

    /* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
    /* loaded from: classes2.dex */
    public static final class a {
        public long a = 0;
        public String b = "";
        public String c = "";
        public MessageType d = MessageType.UNKNOWN;
        public SDKPlatform e = SDKPlatform.UNKNOWN_OS;
        public String f = "";
        public String g = "";
        public int h = 0;
        public int i = 0;
        public String j = "";
        public long k = 0;
        public Event l = Event.UNKNOWN_EVENT;
        public String m = "";
        public long n = 0;
        public String o = "";

        public MessagingClientEvent a() {
            return new MessagingClientEvent(this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m, this.n, this.o);
        }

        public a b(String str) {
            this.m = str;
            return this;
        }

        public a c(String str) {
            this.g = str;
            return this;
        }

        public a d(String str) {
            this.o = str;
            return this;
        }

        public a e(Event event) {
            this.l = event;
            return this;
        }

        public a f(String str) {
            this.c = str;
            return this;
        }

        public a g(String str) {
            this.b = str;
            return this;
        }

        public a h(MessageType messageType) {
            this.d = messageType;
            return this;
        }

        public a i(String str) {
            this.f = str;
            return this;
        }

        public a j(long j) {
            this.a = j;
            return this;
        }

        public a k(SDKPlatform sDKPlatform) {
            this.e = sDKPlatform;
            return this;
        }

        public a l(String str) {
            this.j = str;
            return this;
        }

        public a m(int i) {
            this.i = i;
            return this;
        }
    }

    static {
        new a().a();
    }

    public MessagingClientEvent(long j, String str, String str2, MessageType messageType, SDKPlatform sDKPlatform, String str3, String str4, int i, int i2, String str5, long j2, Event event, String str6, long j3, String str7) {
        this.a = j;
        this.b = str;
        this.c = str2;
        this.d = messageType;
        this.e = sDKPlatform;
        this.f = str3;
        this.g = str4;
        this.h = i;
        this.i = i2;
        this.j = str5;
        this.k = j2;
        this.l = event;
        this.m = str6;
        this.n = j3;
        this.o = str7;
    }

    public static a p() {
        return new a();
    }

    @i(zza = 13)
    public String a() {
        return this.m;
    }

    @i(zza = 11)
    public long b() {
        return this.k;
    }

    @i(zza = 14)
    public long c() {
        return this.n;
    }

    @i(zza = 7)
    public String d() {
        return this.g;
    }

    @i(zza = 15)
    public String e() {
        return this.o;
    }

    @i(zza = 12)
    public Event f() {
        return this.l;
    }

    @i(zza = 3)
    public String g() {
        return this.c;
    }

    @i(zza = 2)
    public String h() {
        return this.b;
    }

    @i(zza = 4)
    public MessageType i() {
        return this.d;
    }

    @i(zza = 6)
    public String j() {
        return this.f;
    }

    @i(zza = 8)
    public int k() {
        return this.h;
    }

    @i(zza = 1)
    public long l() {
        return this.a;
    }

    @i(zza = 5)
    public SDKPlatform m() {
        return this.e;
    }

    @i(zza = 10)
    public String n() {
        return this.j;
    }

    @i(zza = 9)
    public int o() {
        return this.i;
    }
}
