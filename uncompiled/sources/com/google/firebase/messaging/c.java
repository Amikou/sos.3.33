package com.google.firebase.messaging;

import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import com.google.firebase.heartbeatinfo.HeartBeatInfo;
import com.google.firebase.messaging.c;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.ExecutionException;
import org.slf4j.Marker;

/* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
/* loaded from: classes2.dex */
public class c {
    public final c51 a;
    public final i82 b;
    public final com.google.android.gms.cloudmessaging.b c;
    public final fw2<wf4> d;
    public final fw2<HeartBeatInfo> e;
    public final k51 f;

    public c(c51 c51Var, i82 i82Var, com.google.android.gms.cloudmessaging.b bVar, fw2<wf4> fw2Var, fw2<HeartBeatInfo> fw2Var2, k51 k51Var) {
        this.a = c51Var;
        this.b = i82Var;
        this.c = bVar;
        this.d = fw2Var;
        this.e = fw2Var2;
        this.f = k51Var;
    }

    public c(c51 c51Var, i82 i82Var, fw2<wf4> fw2Var, fw2<HeartBeatInfo> fw2Var2, k51 k51Var) {
        this(c51Var, i82Var, new com.google.android.gms.cloudmessaging.b(c51Var.h()), fw2Var, fw2Var2, k51Var);
    }

    public static String a(byte[] bArr) {
        return Base64.encodeToString(bArr, 11);
    }

    public static boolean g(String str) {
        return "SERVICE_NOT_AVAILABLE".equals(str) || "INTERNAL_SERVER_ERROR".equals(str) || "InternalServerError".equals(str);
    }

    public com.google.android.gms.tasks.c<?> b(String str) {
        Bundle bundle = new Bundle();
        bundle.putString("delete", "1");
        return c(j(str, i82.c(this.a), Marker.ANY_MARKER, bundle));
    }

    public final com.google.android.gms.tasks.c<String> c(com.google.android.gms.tasks.c<Bundle> cVar) {
        return cVar.i(sg1.a, new com.google.android.gms.tasks.a(this) { // from class: tg1
            public final c a;

            {
                this.a = this;
            }

            @Override // com.google.android.gms.tasks.a
            public Object a(com.google.android.gms.tasks.c cVar2) {
                return this.a.h(cVar2);
            }
        });
    }

    public final String d() {
        try {
            return a(MessageDigest.getInstance("SHA-1").digest(this.a.j().getBytes()));
        } catch (NoSuchAlgorithmException unused) {
            return "[HASH-ERROR]";
        }
    }

    public com.google.android.gms.tasks.c<String> e(String str) {
        return c(j(str, i82.c(this.a), Marker.ANY_MARKER, new Bundle()));
    }

    public final String f(Bundle bundle) throws IOException {
        if (bundle != null) {
            String string = bundle.getString("registration_id");
            if (string != null) {
                return string;
            }
            String string2 = bundle.getString("unregistered");
            if (string2 != null) {
                return string2;
            }
            String string3 = bundle.getString("error");
            if ("RST".equals(string3)) {
                throw new IOException("INSTANCE_ID_RESET");
            }
            if (string3 != null) {
                throw new IOException(string3);
            }
            String valueOf = String.valueOf(bundle);
            StringBuilder sb = new StringBuilder(valueOf.length() + 21);
            sb.append("Unexpected response: ");
            sb.append(valueOf);
            new Throwable();
            throw new IOException("SERVICE_NOT_AVAILABLE");
        }
        throw new IOException("SERVICE_NOT_AVAILABLE");
    }

    public final /* synthetic */ String h(com.google.android.gms.tasks.c cVar) throws Exception {
        return f((Bundle) cVar.m(IOException.class));
    }

    public final Bundle i(String str, String str2, String str3, Bundle bundle) {
        HeartBeatInfo.HeartBeat a;
        bundle.putString("scope", str3);
        bundle.putString("sender", str2);
        bundle.putString("subtype", str2);
        bundle.putString("appid", str);
        bundle.putString("gmp_app_id", this.a.k().c());
        bundle.putString("gmsv", Integer.toString(this.b.d()));
        bundle.putString("osv", Integer.toString(Build.VERSION.SDK_INT));
        bundle.putString("app_ver", this.b.a());
        bundle.putString("app_ver_name", this.b.b());
        bundle.putString("firebase-app-name-hash", d());
        try {
            String b = ((com.google.firebase.installations.d) com.google.android.gms.tasks.d.a(this.f.a(false))).b();
            if (!TextUtils.isEmpty(b)) {
                bundle.putString("Goog-Firebase-Installations-Auth", b);
            }
        } catch (InterruptedException | ExecutionException unused) {
        }
        bundle.putString("cliv", "fcm-22.0.0");
        HeartBeatInfo heartBeatInfo = this.e.get();
        wf4 wf4Var = this.d.get();
        if (heartBeatInfo != null && wf4Var != null && (a = heartBeatInfo.a("fire-iid")) != HeartBeatInfo.HeartBeat.NONE) {
            bundle.putString("Firebase-Client-Log-Type", Integer.toString(a.getCode()));
            bundle.putString("Firebase-Client", wf4Var.a());
        }
        return bundle;
    }

    public final com.google.android.gms.tasks.c<Bundle> j(String str, String str2, String str3, Bundle bundle) {
        i(str, str2, str3, bundle);
        return this.c.a(bundle);
    }

    public com.google.android.gms.tasks.c<?> k(String str, String str2, String str3) {
        Bundle bundle = new Bundle();
        String valueOf = String.valueOf(str3);
        bundle.putString("gcm.topic", valueOf.length() != 0 ? "/topics/".concat(valueOf) : new String("/topics/"));
        String valueOf2 = String.valueOf(str3);
        return c(j(str, str2, valueOf2.length() != 0 ? "/topics/".concat(valueOf2) : new String("/topics/"), bundle));
    }

    public com.google.android.gms.tasks.c<?> l(String str, String str2, String str3) {
        Bundle bundle = new Bundle();
        String valueOf = String.valueOf(str3);
        bundle.putString("gcm.topic", valueOf.length() != 0 ? "/topics/".concat(valueOf) : new String("/topics/"));
        bundle.putString("delete", "1");
        String valueOf2 = String.valueOf(str3);
        return c(j(str, str2, valueOf2.length() != 0 ? "/topics/".concat(valueOf2) : new String("/topics/"), bundle));
    }
}
