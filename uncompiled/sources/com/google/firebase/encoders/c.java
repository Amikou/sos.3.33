package com.google.firebase.encoders;

import java.io.IOException;

/* compiled from: ObjectEncoderContext.java */
/* loaded from: classes2.dex */
public interface c {
    c a(h31 h31Var, Object obj) throws IOException;

    c d(h31 h31Var, boolean z) throws IOException;

    c e(h31 h31Var, int i) throws IOException;

    c f(h31 h31Var, long j) throws IOException;
}
