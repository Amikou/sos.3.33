package com.google.firebase.crashlytics;

import android.content.Context;
import android.content.pm.PackageManager;
import com.google.android.gms.tasks.c;
import com.google.firebase.crashlytics.internal.common.CommonUtils;
import com.google.firebase.crashlytics.internal.common.d;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;

/* compiled from: FirebaseCrashlytics.java */
/* loaded from: classes2.dex */
public class a {

    /* compiled from: FirebaseCrashlytics.java */
    /* renamed from: com.google.firebase.crashlytics.a$a  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public class C0138a implements com.google.android.gms.tasks.a<Void, Object> {
        @Override // com.google.android.gms.tasks.a
        public Object a(c<Void> cVar) throws Exception {
            if (cVar.p()) {
                return null;
            }
            w12.f().e("Error fetching settings.", cVar.k());
            return null;
        }
    }

    /* compiled from: FirebaseCrashlytics.java */
    /* loaded from: classes2.dex */
    public class b implements Callable<Void> {
        public final /* synthetic */ boolean a;
        public final /* synthetic */ d b;
        public final /* synthetic */ com.google.firebase.crashlytics.internal.settings.b c;

        public b(boolean z, d dVar, com.google.firebase.crashlytics.internal.settings.b bVar) {
            this.a = z;
            this.b = dVar;
            this.c = bVar;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public Void call() throws Exception {
            if (this.a) {
                this.b.g(this.c);
                return null;
            }
            return null;
        }
    }

    public a(d dVar) {
    }

    public static a a(c51 c51Var, k51 k51Var, fw2<o90> fw2Var, rl0<kb> rl0Var) {
        Context h = c51Var.h();
        String packageName = h.getPackageName();
        w12 f = w12.f();
        f.g("Initializing Firebase Crashlytics " + d.i() + " for " + packageName);
        be0 be0Var = new be0(c51Var);
        ln1 ln1Var = new ln1(h, packageName, k51Var, be0Var);
        hw2 hw2Var = new hw2(fw2Var);
        pb pbVar = new pb(rl0Var);
        d dVar = new d(c51Var, ln1Var, hw2Var, be0Var, pbVar.e(), pbVar.d(), xy0.c("Crashlytics Exception Handler"));
        String c = c51Var.k().c();
        String n = CommonUtils.n(h);
        w12 f2 = w12.f();
        f2.b("Mapping file ID is: " + n);
        try {
            com.google.firebase.crashlytics.internal.common.a a = com.google.firebase.crashlytics.internal.common.a.a(h, ln1Var, c, n, new f83(h));
            w12 f3 = w12.f();
            f3.i("Installer package name is: " + a.c);
            ExecutorService c2 = xy0.c("com.google.firebase.crashlytics.startup");
            com.google.firebase.crashlytics.internal.settings.b l = com.google.firebase.crashlytics.internal.settings.b.l(h, c, ln1Var, new jl1(), a.e, a.f, be0Var);
            l.p(c2).i(c2, new C0138a());
            com.google.android.gms.tasks.d.c(c2, new b(dVar.n(a, l), dVar, l));
            return new a(dVar);
        } catch (PackageManager.NameNotFoundException e) {
            w12.f().e("Error retrieving app package info.", e);
            return null;
        }
    }
}
