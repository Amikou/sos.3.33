package com.google.firebase.crashlytics;

import com.google.firebase.crashlytics.CrashlyticsRegistrar;
import com.google.firebase.crashlytics.a;
import java.util.Arrays;
import java.util.List;

/* loaded from: classes2.dex */
public class CrashlyticsRegistrar implements g40 {
    public final a b(b40 b40Var) {
        return a.a((c51) b40Var.a(c51.class), (k51) b40Var.a(k51.class), b40Var.b(o90.class), b40Var.e(kb.class));
    }

    @Override // defpackage.g40
    public List<a40<?>> getComponents() {
        return Arrays.asList(a40.c(a.class).b(hm0.j(c51.class)).b(hm0.j(k51.class)).b(hm0.i(o90.class)).b(hm0.a(kb.class)).f(new e40() { // from class: q90
            @Override // defpackage.e40
            public final Object a(b40 b40Var) {
                a b;
                b = CrashlyticsRegistrar.this.b(b40Var);
                return b;
            }
        }).e().d(), jz1.b("fire-cls", "18.2.0"));
    }
}
