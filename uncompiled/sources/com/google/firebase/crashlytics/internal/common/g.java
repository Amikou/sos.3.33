package com.google.firebase.crashlytics.internal.common;

import java.io.File;
import java.io.FileInputStream;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: MetaDataStore.java */
/* loaded from: classes2.dex */
public class g {
    public final File a;

    static {
        Charset.forName("UTF-8");
    }

    public g(File file) {
        this.a = file;
    }

    public static Map<String, String> d(String str) throws JSONException {
        JSONObject jSONObject = new JSONObject(str);
        HashMap hashMap = new HashMap();
        Iterator<String> keys = jSONObject.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            hashMap.put(next, g(jSONObject, next));
        }
        return hashMap;
    }

    public static String g(JSONObject jSONObject, String str) {
        if (jSONObject.isNull(str)) {
            return null;
        }
        return jSONObject.optString(str, null);
    }

    public File a(String str) {
        File file = this.a;
        return new File(file, str + "internal-keys.meta");
    }

    public File b(String str) {
        File file = this.a;
        return new File(file, str + "keys.meta");
    }

    public File c(String str) {
        File file = this.a;
        return new File(file, str + "user.meta");
    }

    public Map<String, String> e(String str) {
        return f(str, false);
    }

    public Map<String, String> f(String str, boolean z) {
        File a = z ? a(str) : b(str);
        if (!a.exists()) {
            return Collections.emptyMap();
        }
        FileInputStream fileInputStream = null;
        try {
            try {
                FileInputStream fileInputStream2 = new FileInputStream(a);
                try {
                    Map<String, String> d = d(CommonUtils.B(fileInputStream2));
                    CommonUtils.e(fileInputStream2, "Failed to close user metadata file.");
                    return d;
                } catch (Exception e) {
                    e = e;
                    fileInputStream = fileInputStream2;
                    w12.f().e("Error deserializing user metadata.", e);
                    CommonUtils.e(fileInputStream, "Failed to close user metadata file.");
                    return Collections.emptyMap();
                } catch (Throwable th) {
                    th = th;
                    fileInputStream = fileInputStream2;
                    CommonUtils.e(fileInputStream, "Failed to close user metadata file.");
                    throw th;
                }
            } catch (Exception e2) {
                e = e2;
            }
        } catch (Throwable th2) {
            th = th2;
        }
    }
}
