package com.google.firebase.crashlytics.internal.common;

import android.content.Context;
import android.text.TextUtils;
import androidx.recyclerview.widget.RecyclerView;
import com.google.firebase.crashlytics.internal.common.d;
import defpackage.r12;
import java.io.File;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/* compiled from: CrashlyticsCore.java */
/* loaded from: classes2.dex */
public class d {
    public final Context a;
    public final be0 b;
    public final long c = System.currentTimeMillis();
    public n90 d;
    public n90 e;
    public com.google.firebase.crashlytics.internal.common.c f;
    public final ln1 g;
    public final lr h;
    public final qb i;
    public final ExecutorService j;
    public final com.google.firebase.crashlytics.internal.common.b k;
    public final o90 l;

    /* compiled from: CrashlyticsCore.java */
    /* loaded from: classes2.dex */
    public class a implements Callable<com.google.android.gms.tasks.c<Void>> {
        public final /* synthetic */ en3 a;

        public a(en3 en3Var) {
            this.a = en3Var;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public com.google.android.gms.tasks.c<Void> call() throws Exception {
            return d.this.f(this.a);
        }
    }

    /* compiled from: CrashlyticsCore.java */
    /* loaded from: classes2.dex */
    public class b implements Runnable {
        public final /* synthetic */ en3 a;

        public b(en3 en3Var) {
            this.a = en3Var;
        }

        @Override // java.lang.Runnable
        public void run() {
            d.this.f(this.a);
        }
    }

    /* compiled from: CrashlyticsCore.java */
    /* loaded from: classes2.dex */
    public class c implements Callable<Boolean> {
        public c() {
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public Boolean call() throws Exception {
            try {
                boolean d = d.this.d.d();
                if (!d) {
                    w12.f().k("Initialization marker file was not properly removed.");
                }
                return Boolean.valueOf(d);
            } catch (Exception e) {
                w12.f().e("Problem encountered deleting Crashlytics initialization marker.", e);
                return Boolean.FALSE;
            }
        }
    }

    /* compiled from: CrashlyticsCore.java */
    /* renamed from: com.google.firebase.crashlytics.internal.common.d$d  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public class CallableC0142d implements Callable<Boolean> {
        public CallableC0142d() {
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public Boolean call() throws Exception {
            return Boolean.valueOf(d.this.f.o());
        }
    }

    /* compiled from: CrashlyticsCore.java */
    /* loaded from: classes2.dex */
    public static final class e implements r12.b {
        public final s31 a;

        public e(s31 s31Var) {
            this.a = s31Var;
        }

        @Override // defpackage.r12.b
        public File a() {
            File file = new File(this.a.b(), "log-files");
            if (!file.exists()) {
                file.mkdirs();
            }
            return file;
        }
    }

    public d(c51 c51Var, ln1 ln1Var, o90 o90Var, be0 be0Var, lr lrVar, qb qbVar, ExecutorService executorService) {
        this.b = be0Var;
        this.a = c51Var.h();
        this.g = ln1Var;
        this.l = o90Var;
        this.h = lrVar;
        this.i = qbVar;
        this.j = executorService;
        this.k = new com.google.firebase.crashlytics.internal.common.b(executorService);
    }

    public static String i() {
        return "18.2.0";
    }

    public static boolean j(String str, boolean z) {
        if (z) {
            return !TextUtils.isEmpty(str);
        }
        w12.f().i("Configured not to require a build ID.");
        return true;
    }

    public final void d() {
        try {
            Boolean.TRUE.equals((Boolean) j.b(this.k.g(new CallableC0142d())));
        } catch (Exception unused) {
        }
    }

    public boolean e() {
        return this.d.c();
    }

    public final com.google.android.gms.tasks.c<Void> f(en3 en3Var) {
        m();
        try {
            this.h.a(new kr() { // from class: m90
                @Override // defpackage.kr
                public final void a(String str) {
                    d.this.k(str);
                }
            });
            if (!en3Var.b().a().a) {
                w12.f().b("Collection of crash reports disabled in Crashlytics settings.");
                return com.google.android.gms.tasks.d.d(new RuntimeException("Collection of crash reports disabled in Crashlytics settings."));
            }
            if (!this.f.w(en3Var)) {
                w12.f().k("Previous sessions could not be finalized.");
            }
            return this.f.O(en3Var.a());
        } catch (Exception e2) {
            w12.f().e("Crashlytics encountered a problem during asynchronous initialization.", e2);
            return com.google.android.gms.tasks.d.d(e2);
        } finally {
            l();
        }
    }

    public com.google.android.gms.tasks.c<Void> g(en3 en3Var) {
        return j.c(this.j, new a(en3Var));
    }

    public final void h(en3 en3Var) {
        Future<?> submit = this.j.submit(new b(en3Var));
        w12.f().b("Crashlytics detected incomplete initialization on previous app launch. Will initialize synchronously.");
        try {
            submit.get(4L, TimeUnit.SECONDS);
        } catch (InterruptedException e2) {
            w12.f().e("Crashlytics was interrupted during initialization.", e2);
        } catch (ExecutionException e3) {
            w12.f().e("Crashlytics encountered a problem during initialization.", e3);
        } catch (TimeoutException e4) {
            w12.f().e("Crashlytics timed out during initialization.", e4);
        }
    }

    public void k(String str) {
        this.f.V(System.currentTimeMillis() - this.c, str);
    }

    public void l() {
        this.k.g(new c());
    }

    public void m() {
        this.k.b();
        this.d.a();
        w12.f().i("Initialization marker file was created.");
    }

    public boolean n(com.google.firebase.crashlytics.internal.common.a aVar, en3 en3Var) {
        if (j(aVar.b, CommonUtils.k(this.a, "com.crashlytics.RequireBuildId", true))) {
            try {
                t31 t31Var = new t31(this.a);
                this.e = new n90("crash_marker", t31Var);
                this.d = new n90("initialization_marker", t31Var);
                zf4 zf4Var = new zf4();
                e eVar = new e(t31Var);
                r12 r12Var = new r12(this.a, eVar);
                this.f = new com.google.firebase.crashlytics.internal.common.c(this.a, this.k, this.g, this.b, t31Var, this.e, aVar, zf4Var, r12Var, eVar, i.g(this.a, this.g, t31Var, aVar, r12Var, zf4Var, new t82(RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE, new s63(10)), en3Var), this.l, this.i);
                boolean e2 = e();
                d();
                this.f.t(Thread.getDefaultUncaughtExceptionHandler(), en3Var);
                if (e2 && CommonUtils.c(this.a)) {
                    w12.f().b("Crashlytics did not finish previous background initialization. Initializing synchronously.");
                    h(en3Var);
                    return false;
                }
                w12.f().b("Successfully configured exception handler.");
                return true;
            } catch (Exception e3) {
                w12.f().e("Crashlytics was not started due to an exception during initialization", e3);
                this.f = null;
                return false;
            }
        }
        throw new IllegalStateException("The Crashlytics build ID is missing. This occurs when Crashlytics tooling is absent from your app's build configuration. Please review Crashlytics onboarding instructions and ensure you have a valid Crashlytics account.");
    }
}
