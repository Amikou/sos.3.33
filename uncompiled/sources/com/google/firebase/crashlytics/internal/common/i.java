package com.google.firebase.crashlytics.internal.common;

import android.app.ApplicationExitInfo;
import android.content.Context;
import com.google.android.gms.tasks.c;
import com.google.firebase.crashlytics.internal.common.i;
import defpackage.r90;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;

/* compiled from: SessionReportingCoordinator.java */
/* loaded from: classes2.dex */
public class i {
    public final s90 a;
    public final ga0 b;
    public final ne0 c;
    public final r12 d;
    public final zf4 e;

    public i(s90 s90Var, ga0 ga0Var, ne0 ne0Var, r12 r12Var, zf4 zf4Var) {
        this.a = s90Var;
        this.b = ga0Var;
        this.c = ne0Var;
        this.d = r12Var;
        this.e = zf4Var;
    }

    public static r90.a e(ApplicationExitInfo applicationExitInfo) {
        String str;
        try {
            str = f(applicationExitInfo.getTraceInputStream());
        } catch (IOException | NullPointerException e) {
            w12 f = w12.f();
            f.k("Could not get input trace in application exit info: " + applicationExitInfo.toString() + " Error: " + e);
            str = null;
        }
        return r90.a.a().b(applicationExitInfo.getImportance()).d(applicationExitInfo.getProcessName()).f(applicationExitInfo.getReason()).h(applicationExitInfo.getTimestamp()).c(applicationExitInfo.getPid()).e(applicationExitInfo.getPss()).g(applicationExitInfo.getRss()).i(str).a();
    }

    public static String f(InputStream inputStream) throws IOException, NullPointerException {
        StringBuilder sb = new StringBuilder();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, Charset.forName(StandardCharsets.UTF_8.name())));
        while (true) {
            try {
                int read = bufferedReader.read();
                if (read != -1) {
                    sb.append((char) read);
                } else {
                    String sb2 = sb.toString();
                    bufferedReader.close();
                    return sb2;
                }
            } catch (Throwable th) {
                try {
                    bufferedReader.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
    }

    public static i g(Context context, ln1 ln1Var, s31 s31Var, a aVar, r12 r12Var, zf4 zf4Var, is3 is3Var, en3 en3Var) {
        return new i(new s90(context, ln1Var, aVar, is3Var), new ga0(new File(s31Var.a()), en3Var), ne0.c(context), r12Var, zf4Var);
    }

    public static List<r90.c> j(Map<String, String> map) {
        ArrayList arrayList = new ArrayList();
        arrayList.ensureCapacity(map.size());
        for (Map.Entry<String, String> entry : map.entrySet()) {
            arrayList.add(r90.c.a().b(entry.getKey()).c(entry.getValue()).a());
        }
        Collections.sort(arrayList, km3.a);
        return arrayList;
    }

    public static /* synthetic */ int l(r90.c cVar, r90.c cVar2) {
        return cVar.b().compareTo(cVar2.b());
    }

    public final r90.e.d c(r90.e.d dVar) {
        return d(dVar, this.d, this.e);
    }

    public final r90.e.d d(r90.e.d dVar, r12 r12Var, zf4 zf4Var) {
        r90.e.d.b g = dVar.g();
        String c = r12Var.c();
        if (c != null) {
            g.d(r90.e.d.AbstractC0276d.a().b(c).a());
        } else {
            w12.f().i("No log data to include with this event.");
        }
        List<r90.c> j = j(zf4Var.a());
        List<r90.c> j2 = j(zf4Var.b());
        if (!j.isEmpty()) {
            g.b(dVar.b().g().c(ip1.e(j)).e(ip1.e(j2)).a());
        }
        return g.a();
    }

    public void h(String str, List<td2> list) {
        ArrayList arrayList = new ArrayList();
        for (td2 td2Var : list) {
            r90.d.b e = td2Var.e();
            if (e != null) {
                arrayList.add(e);
            }
        }
        this.b.o(str, r90.d.a().b(ip1.e(arrayList)).a());
    }

    public void i(long j, String str) {
        this.b.n(str, j);
    }

    public boolean k() {
        return this.b.x();
    }

    public List<String> m() {
        return this.b.E();
    }

    public void n(String str, long j) {
        this.b.J(this.a.d(str, j));
    }

    public final boolean o(com.google.android.gms.tasks.c<e> cVar) {
        if (cVar.p()) {
            e l = cVar.l();
            w12 f = w12.f();
            f.b("Crashlytics report successfully enqueued to DataTransport: " + l.c());
            this.b.m(l.c());
            return true;
        }
        w12.f().l("Crashlytics report could not be enqueued to DataTransport", cVar.k());
        return false;
    }

    public void p(String str, ApplicationExitInfo applicationExitInfo, r12 r12Var, zf4 zf4Var) {
        if (applicationExitInfo.getTimestamp() >= this.b.w(str) && applicationExitInfo.getReason() == 6) {
            r90.e.d b = this.a.b(e(applicationExitInfo));
            w12 f = w12.f();
            f.b("Persisting anr for session " + str);
            this.b.I(d(b, r12Var, zf4Var), str, true);
        }
    }

    public final void q(Throwable th, Thread thread, String str, String str2, long j, boolean z) {
        this.b.I(c(this.a.c(th, thread, str2, j, 4, 8, z)), str, str2.equals("crash"));
    }

    public void r(Throwable th, Thread thread, String str, long j) {
        w12 f = w12.f();
        f.i("Persisting fatal event for session " + str);
        q(th, thread, str, "crash", j, true);
    }

    public void s() {
        this.b.l();
    }

    public com.google.android.gms.tasks.c<Void> t(Executor executor) {
        List<e> F = this.b.F();
        ArrayList arrayList = new ArrayList();
        for (e eVar : F) {
            arrayList.add(this.c.g(eVar).i(executor, new com.google.android.gms.tasks.a() { // from class: jm3
                @Override // com.google.android.gms.tasks.a
                public final Object a(c cVar) {
                    boolean o;
                    o = i.this.o(cVar);
                    return Boolean.valueOf(o);
                }
            }));
        }
        return com.google.android.gms.tasks.d.f(arrayList);
    }
}
