package com.google.firebase.crashlytics.internal.common;

import java.lang.Thread;
import java.util.concurrent.atomic.AtomicBoolean;

/* compiled from: CrashlyticsUncaughtExceptionHandler.java */
/* loaded from: classes2.dex */
public class f implements Thread.UncaughtExceptionHandler {
    public final a a;
    public final en3 b;
    public final Thread.UncaughtExceptionHandler c;
    public final AtomicBoolean d = new AtomicBoolean(false);

    /* compiled from: CrashlyticsUncaughtExceptionHandler.java */
    /* loaded from: classes2.dex */
    public interface a {
        void a(en3 en3Var, Thread thread, Throwable th);
    }

    public f(a aVar, en3 en3Var, Thread.UncaughtExceptionHandler uncaughtExceptionHandler) {
        this.a = aVar;
        this.b = en3Var;
        this.c = uncaughtExceptionHandler;
    }

    public boolean a() {
        return this.d.get();
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v1, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r1v2, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r1v3, types: [java.lang.Thread$UncaughtExceptionHandler] */
    /* JADX WARN: Type inference failed for: r1v4, types: [java.lang.Thread$UncaughtExceptionHandler] */
    /* JADX WARN: Type inference failed for: r3v1, types: [w12] */
    /* JADX WARN: Type inference failed for: r6v0, types: [java.lang.Thread] */
    /* JADX WARN: Type inference failed for: r6v1, types: [java.lang.Thread] */
    /* JADX WARN: Type inference failed for: r6v3, types: [java.util.concurrent.atomic.AtomicBoolean] */
    @Override // java.lang.Thread.UncaughtExceptionHandler
    public void uncaughtException(Thread thread, Throwable th) {
        this.d.set(true);
        ?? r1 = "Completed exception processing. Invoking default exception handler.";
        try {
            try {
                if (thread == 0) {
                    w12.f().d("Could not handle uncaught exception; null thread");
                } else if (th == null) {
                    w12.f().d("Could not handle uncaught exception; null throwable");
                } else {
                    this.a.a(this.b, thread, th);
                }
            } catch (Exception e) {
                w12.f().e("An error occurred in the uncaught exception handler", e);
            }
            w12.f().b("Completed exception processing. Invoking default exception handler.");
            r1 = this.c;
            r1.uncaughtException(thread, th);
            thread = this.d;
            thread.set(false);
        } catch (Throwable th2) {
            w12.f().b(r1);
            this.c.uncaughtException(thread, th);
            this.d.set(false);
            throw th2;
        }
    }
}
