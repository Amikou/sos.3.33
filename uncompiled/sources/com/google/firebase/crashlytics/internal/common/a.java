package com.google.firebase.crashlytics.internal.common;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

/* compiled from: AppData.java */
/* loaded from: classes2.dex */
public class a {
    public final String a;
    public final String b;
    public final String c;
    public final String d;
    public final String e;
    public final String f;
    public final ze4 g;

    public a(String str, String str2, String str3, String str4, String str5, String str6, ze4 ze4Var) {
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.e = str5;
        this.f = str6;
        this.g = ze4Var;
    }

    public static a a(Context context, ln1 ln1Var, String str, String str2, ze4 ze4Var) throws PackageManager.NameNotFoundException {
        String packageName = context.getPackageName();
        String g = ln1Var.g();
        PackageInfo packageInfo = context.getPackageManager().getPackageInfo(packageName, 0);
        String num = Integer.toString(packageInfo.versionCode);
        String str3 = packageInfo.versionName;
        if (str3 == null) {
            str3 = "0.0";
        }
        return new a(str, str2, g, packageName, num, str3, ze4Var);
    }
}
