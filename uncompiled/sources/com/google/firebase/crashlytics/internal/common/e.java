package com.google.firebase.crashlytics.internal.common;

import com.google.auto.value.AutoValue;

/* compiled from: CrashlyticsReportWithSessionId.java */
@AutoValue
/* loaded from: classes2.dex */
public abstract class e {
    public static e a(r90 r90Var, String str) {
        return new mk(r90Var, str);
    }

    public abstract r90 b();

    public abstract String c();
}
