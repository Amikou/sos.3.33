package com.google.firebase.crashlytics.internal.common;

import java.util.concurrent.Callable;
import java.util.concurrent.Executor;

/* compiled from: CrashlyticsBackgroundWorker.java */
/* loaded from: classes2.dex */
public class b {
    public final Executor a;
    public com.google.android.gms.tasks.c<Void> b = com.google.android.gms.tasks.d.e(null);
    public final Object c = new Object();
    public final ThreadLocal<Boolean> d = new ThreadLocal<>();

    /* compiled from: CrashlyticsBackgroundWorker.java */
    /* loaded from: classes2.dex */
    public class a implements Runnable {
        public a() {
        }

        @Override // java.lang.Runnable
        public void run() {
            b.this.d.set(Boolean.TRUE);
        }
    }

    /* compiled from: CrashlyticsBackgroundWorker.java */
    /* renamed from: com.google.firebase.crashlytics.internal.common.b$b  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public class C0139b implements com.google.android.gms.tasks.a<Void, T> {
        public final /* synthetic */ Callable a;

        public C0139b(b bVar, Callable callable) {
            this.a = callable;
        }

        /* JADX WARN: Type inference failed for: r1v2, types: [T, java.lang.Object] */
        @Override // com.google.android.gms.tasks.a
        public T a(com.google.android.gms.tasks.c<Void> cVar) throws Exception {
            return this.a.call();
        }
    }

    /* compiled from: CrashlyticsBackgroundWorker.java */
    /* loaded from: classes2.dex */
    public class c implements com.google.android.gms.tasks.a<T, Void> {
        public c(b bVar) {
        }

        @Override // com.google.android.gms.tasks.a
        /* renamed from: b */
        public Void a(com.google.android.gms.tasks.c<T> cVar) throws Exception {
            return null;
        }
    }

    public b(Executor executor) {
        this.a = executor;
        executor.execute(new a());
    }

    public void b() {
        if (!e()) {
            throw new IllegalStateException("Not running on background worker thread as intended.");
        }
    }

    public Executor c() {
        return this.a;
    }

    public final <T> com.google.android.gms.tasks.c<Void> d(com.google.android.gms.tasks.c<T> cVar) {
        return cVar.i(this.a, new c(this));
    }

    public final boolean e() {
        return Boolean.TRUE.equals(this.d.get());
    }

    public final <T> com.google.android.gms.tasks.a<Void, T> f(Callable<T> callable) {
        return new C0139b(this, callable);
    }

    public <T> com.google.android.gms.tasks.c<T> g(Callable<T> callable) {
        com.google.android.gms.tasks.c<T> i;
        synchronized (this.c) {
            i = this.b.i(this.a, f(callable));
            this.b = d(i);
        }
        return i;
    }

    public <T> com.google.android.gms.tasks.c<T> h(Callable<com.google.android.gms.tasks.c<T>> callable) {
        com.google.android.gms.tasks.c<T> j;
        synchronized (this.c) {
            j = this.b.j(this.a, f(callable));
            this.b = d(j);
        }
        return j;
    }
}
