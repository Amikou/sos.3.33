package com.google.firebase.crashlytics.internal.common;

import android.os.Looper;
import com.google.android.gms.tasks.c;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/* compiled from: Utils.java */
/* loaded from: classes2.dex */
public final class j {
    public static final ExecutorService a = xy0.c("awaitEvenIfOnMainThread task continuation executor");

    /* compiled from: Utils.java */
    /* loaded from: classes2.dex */
    public class a implements com.google.android.gms.tasks.a<T, Void> {
        public final /* synthetic */ n34 a;

        public a(n34 n34Var) {
            this.a = n34Var;
        }

        @Override // com.google.android.gms.tasks.a
        /* renamed from: b */
        public Void a(com.google.android.gms.tasks.c<T> cVar) throws Exception {
            if (cVar.p()) {
                this.a.e(cVar.l());
                return null;
            }
            this.a.d(cVar.k());
            return null;
        }
    }

    /* compiled from: Utils.java */
    /* loaded from: classes2.dex */
    public class b implements Runnable {
        public final /* synthetic */ Callable a;
        public final /* synthetic */ n34 f0;

        /* compiled from: Utils.java */
        /* loaded from: classes2.dex */
        public class a implements com.google.android.gms.tasks.a<T, Void> {
            public a() {
            }

            @Override // com.google.android.gms.tasks.a
            /* renamed from: b */
            public Void a(com.google.android.gms.tasks.c<T> cVar) throws Exception {
                if (cVar.p()) {
                    b.this.f0.c(cVar.l());
                    return null;
                }
                b.this.f0.b(cVar.k());
                return null;
            }
        }

        public b(Callable callable, n34 n34Var) {
            this.a = callable;
            this.f0 = n34Var;
        }

        @Override // java.lang.Runnable
        public void run() {
            try {
                ((com.google.android.gms.tasks.c) this.a.call()).h(new a());
            } catch (Exception e) {
                this.f0.b(e);
            }
        }
    }

    public static <T> T b(com.google.android.gms.tasks.c<T> cVar) throws InterruptedException, TimeoutException {
        final CountDownLatch countDownLatch = new CountDownLatch(1);
        cVar.i(a, new com.google.android.gms.tasks.a() { // from class: qg4
            @Override // com.google.android.gms.tasks.a
            public final Object a(c cVar2) {
                Object countDown;
                countDown = countDownLatch.countDown();
                return countDown;
            }
        });
        if (Looper.getMainLooper() == Looper.myLooper()) {
            countDownLatch.await(4L, TimeUnit.SECONDS);
        } else {
            countDownLatch.await();
        }
        if (cVar.p()) {
            return cVar.l();
        }
        if (!cVar.n()) {
            if (cVar.o()) {
                throw new IllegalStateException(cVar.k());
            }
            throw new TimeoutException();
        }
        throw new CancellationException("Task is already canceled");
    }

    public static <T> com.google.android.gms.tasks.c<T> c(Executor executor, Callable<com.google.android.gms.tasks.c<T>> callable) {
        n34 n34Var = new n34();
        executor.execute(new b(callable, n34Var));
        return n34Var.a();
    }

    public static <T> com.google.android.gms.tasks.c<T> e(com.google.android.gms.tasks.c<T> cVar, com.google.android.gms.tasks.c<T> cVar2) {
        n34 n34Var = new n34();
        a aVar = new a(n34Var);
        cVar.h(aVar);
        cVar2.h(aVar);
        return n34Var.a();
    }
}
