package com.google.firebase.crashlytics.internal.common;

import android.app.ActivityManager;
import android.app.ApplicationExitInfo;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import com.google.firebase.crashlytics.internal.common.f;
import defpackage.r12;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.lang.Thread;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicBoolean;

/* compiled from: CrashlyticsController.java */
/* loaded from: classes2.dex */
public class c {
    public static final FilenameFilter r = l90.a;
    public final Context a;
    public final be0 b;
    public final n90 c;
    public final com.google.firebase.crashlytics.internal.common.b d;
    public final ln1 e;
    public final s31 f;
    public final com.google.firebase.crashlytics.internal.common.a g;
    public final r12.b h;
    public final r12 i;
    public final o90 j;
    public final String k;
    public final qb l;
    public final i m;
    public com.google.firebase.crashlytics.internal.common.f n;
    public final n34<Boolean> o = new n34<>();
    public final n34<Boolean> p = new n34<>();
    public final n34<Void> q = new n34<>();

    /* compiled from: CrashlyticsController.java */
    /* loaded from: classes2.dex */
    public class a implements Callable<Void> {
        public final /* synthetic */ long a;

        public a(long j) {
            this.a = j;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public Void call() throws Exception {
            Bundle bundle = new Bundle();
            bundle.putInt("fatal", 1);
            bundle.putLong("timestamp", this.a);
            c.this.l.a("_ae", bundle);
            return null;
        }
    }

    /* compiled from: CrashlyticsController.java */
    /* loaded from: classes2.dex */
    public class b implements f.a {
        public b() {
        }

        @Override // com.google.firebase.crashlytics.internal.common.f.a
        public void a(en3 en3Var, Thread thread, Throwable th) {
            c.this.F(en3Var, thread, th);
        }
    }

    /* compiled from: CrashlyticsController.java */
    /* renamed from: com.google.firebase.crashlytics.internal.common.c$c  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public class CallableC0140c implements Callable<com.google.android.gms.tasks.c<Void>> {
        public final /* synthetic */ long a;
        public final /* synthetic */ Throwable b;
        public final /* synthetic */ Thread c;
        public final /* synthetic */ en3 d;

        /* compiled from: CrashlyticsController.java */
        /* renamed from: com.google.firebase.crashlytics.internal.common.c$c$a */
        /* loaded from: classes2.dex */
        public class a implements com.google.android.gms.tasks.b<vf, Void> {
            public final /* synthetic */ Executor a;

            public a(Executor executor) {
                this.a = executor;
            }

            @Override // com.google.android.gms.tasks.b
            /* renamed from: b */
            public com.google.android.gms.tasks.c<Void> a(vf vfVar) throws Exception {
                if (vfVar == null) {
                    w12.f().k("Received null app settings, cannot send reports at crash time.");
                    return com.google.android.gms.tasks.d.e(null);
                }
                return com.google.android.gms.tasks.d.g(c.this.M(), c.this.m.t(this.a));
            }
        }

        public CallableC0140c(long j, Throwable th, Thread thread, en3 en3Var) {
            this.a = j;
            this.b = th;
            this.c = thread;
            this.d = en3Var;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public com.google.android.gms.tasks.c<Void> call() throws Exception {
            long E = c.E(this.a);
            String z = c.this.z();
            if (z != null) {
                c.this.c.a();
                c.this.m.r(this.b, this.c, z, E);
                c.this.s(this.a);
                c.this.p(this.d);
                c.this.r();
                if (c.this.b.d()) {
                    Executor c = c.this.d.c();
                    return this.d.a().r(c, new a(c));
                }
                return com.google.android.gms.tasks.d.e(null);
            }
            w12.f().d("Tried to write a fatal exception while no session was open.");
            return com.google.android.gms.tasks.d.e(null);
        }
    }

    /* compiled from: CrashlyticsController.java */
    /* loaded from: classes2.dex */
    public class d implements com.google.android.gms.tasks.b<Void, Boolean> {
        public d(c cVar) {
        }

        @Override // com.google.android.gms.tasks.b
        /* renamed from: b */
        public com.google.android.gms.tasks.c<Boolean> a(Void r1) throws Exception {
            return com.google.android.gms.tasks.d.e(Boolean.TRUE);
        }
    }

    /* compiled from: CrashlyticsController.java */
    /* loaded from: classes2.dex */
    public class e implements com.google.android.gms.tasks.b<Boolean, Void> {
        public final /* synthetic */ com.google.android.gms.tasks.c a;

        /* compiled from: CrashlyticsController.java */
        /* loaded from: classes2.dex */
        public class a implements Callable<com.google.android.gms.tasks.c<Void>> {
            public final /* synthetic */ Boolean a;

            /* compiled from: CrashlyticsController.java */
            /* renamed from: com.google.firebase.crashlytics.internal.common.c$e$a$a  reason: collision with other inner class name */
            /* loaded from: classes2.dex */
            public class C0141a implements com.google.android.gms.tasks.b<vf, Void> {
                public final /* synthetic */ Executor a;

                public C0141a(Executor executor) {
                    this.a = executor;
                }

                @Override // com.google.android.gms.tasks.b
                /* renamed from: b */
                public com.google.android.gms.tasks.c<Void> a(vf vfVar) throws Exception {
                    if (vfVar != null) {
                        c.this.M();
                        c.this.m.t(this.a);
                        c.this.q.e(null);
                        return com.google.android.gms.tasks.d.e(null);
                    }
                    w12.f().k("Received null app settings at app startup. Cannot send cached reports");
                    return com.google.android.gms.tasks.d.e(null);
                }
            }

            public a(Boolean bool) {
                this.a = bool;
            }

            @Override // java.util.concurrent.Callable
            /* renamed from: a */
            public com.google.android.gms.tasks.c<Void> call() throws Exception {
                if (!this.a.booleanValue()) {
                    w12.f().i("Deleting cached crash reports...");
                    c.n(c.this.I());
                    c.this.m.s();
                    c.this.q.e(null);
                    return com.google.android.gms.tasks.d.e(null);
                }
                w12.f().b("Sending cached crash reports...");
                c.this.b.c(this.a.booleanValue());
                Executor c = c.this.d.c();
                return e.this.a.r(c, new C0141a(c));
            }
        }

        public e(com.google.android.gms.tasks.c cVar) {
            this.a = cVar;
        }

        @Override // com.google.android.gms.tasks.b
        /* renamed from: b */
        public com.google.android.gms.tasks.c<Void> a(Boolean bool) throws Exception {
            return c.this.d.h(new a(bool));
        }
    }

    /* compiled from: CrashlyticsController.java */
    /* loaded from: classes2.dex */
    public class f implements Callable<Void> {
        public final /* synthetic */ long a;
        public final /* synthetic */ String b;

        public f(long j, String str) {
            this.a = j;
            this.b = str;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public Void call() throws Exception {
            if (c.this.G()) {
                return null;
            }
            c.this.i.g(this.a, this.b);
            return null;
        }
    }

    /* compiled from: CrashlyticsController.java */
    /* loaded from: classes2.dex */
    public class g implements Callable<Void> {
        public g() {
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public Void call() throws Exception {
            c.this.r();
            return null;
        }
    }

    public c(Context context, com.google.firebase.crashlytics.internal.common.b bVar, ln1 ln1Var, be0 be0Var, s31 s31Var, n90 n90Var, com.google.firebase.crashlytics.internal.common.a aVar, zf4 zf4Var, r12 r12Var, r12.b bVar2, i iVar, o90 o90Var, qb qbVar) {
        new AtomicBoolean(false);
        this.a = context;
        this.d = bVar;
        this.e = ln1Var;
        this.b = be0Var;
        this.f = s31Var;
        this.c = n90Var;
        this.g = aVar;
        this.i = r12Var;
        this.h = bVar2;
        this.j = o90Var;
        this.k = aVar.g.a();
        this.l = qbVar;
        this.m = iVar;
    }

    public static long A() {
        return E(System.currentTimeMillis());
    }

    public static List<td2> C(ud2 ud2Var, String str, File file, byte[] bArr) {
        com.google.firebase.crashlytics.internal.common.g gVar = new com.google.firebase.crashlytics.internal.common.g(file);
        File c = gVar.c(str);
        File b2 = gVar.b(str);
        ArrayList arrayList = new ArrayList();
        arrayList.add(new bt("logs_file", "logs", bArr));
        arrayList.add(new j31("crash_meta_file", "metadata", ud2Var.f()));
        arrayList.add(new j31("session_meta_file", "session", ud2Var.e()));
        arrayList.add(new j31("app_meta_file", "app", ud2Var.a()));
        arrayList.add(new j31("device_meta_file", "device", ud2Var.c()));
        arrayList.add(new j31("os_meta_file", "os", ud2Var.b()));
        arrayList.add(new j31("minidump_file", "minidump", ud2Var.d()));
        arrayList.add(new j31("user_meta_file", "user", c));
        arrayList.add(new j31("keys_file", "keys", b2));
        return arrayList;
    }

    public static long E(long j) {
        return j / 1000;
    }

    public static File[] J(File file, FilenameFilter filenameFilter) {
        return u(file.listFiles(filenameFilter));
    }

    public static void n(File[] fileArr) {
        if (fileArr == null) {
            return;
        }
        for (File file : fileArr) {
            file.delete();
        }
    }

    public static File[] u(File[] fileArr) {
        return fileArr == null ? new File[0] : fileArr;
    }

    public static boolean x() {
        try {
            Class.forName("com.google.firebase.crash.FirebaseCrash");
            return true;
        } catch (ClassNotFoundException unused) {
            return false;
        }
    }

    public File B() {
        return this.f.b();
    }

    public File D() {
        return new File(B(), "native-sessions");
    }

    public synchronized void F(en3 en3Var, Thread thread, Throwable th) {
        w12 f2 = w12.f();
        f2.b("Handling uncaught exception \"" + th + "\" from thread " + thread.getName());
        try {
            j.b(this.d.h(new CallableC0140c(System.currentTimeMillis(), th, thread, en3Var)));
        } catch (Exception e2) {
            w12.f().e("Error handling uncaught exception", e2);
        }
    }

    public boolean G() {
        com.google.firebase.crashlytics.internal.common.f fVar = this.n;
        return fVar != null && fVar.a();
    }

    public File[] I() {
        return K(r);
    }

    public final File[] K(FilenameFilter filenameFilter) {
        return J(B(), filenameFilter);
    }

    public final com.google.android.gms.tasks.c<Void> L(long j) {
        if (x()) {
            w12.f().k("Skipping logging Crashlytics event to Firebase, FirebaseCrash exists");
            return com.google.android.gms.tasks.d.e(null);
        }
        w12.f().b("Logging app exception event to Firebase Analytics");
        return com.google.android.gms.tasks.d.c(new ScheduledThreadPoolExecutor(1), new a(j));
    }

    public final com.google.android.gms.tasks.c<Void> M() {
        File[] I;
        ArrayList arrayList = new ArrayList();
        for (File file : I()) {
            try {
                arrayList.add(L(Long.parseLong(file.getName().substring(3))));
            } catch (NumberFormatException unused) {
                w12.f().k("Could not parse app exception timestamp from file " + file.getName());
            }
            file.delete();
        }
        return com.google.android.gms.tasks.d.f(arrayList);
    }

    public void N() {
        this.d.g(new g());
    }

    public com.google.android.gms.tasks.c<Void> O(com.google.android.gms.tasks.c<vf> cVar) {
        if (!this.m.k()) {
            w12.f().i("No crash reports are available to be sent.");
            this.o.e(Boolean.FALSE);
            return com.google.android.gms.tasks.d.e(null);
        }
        w12.f().i("Crash reports are available to be sent.");
        return P().q(new e(cVar));
    }

    public final com.google.android.gms.tasks.c<Boolean> P() {
        if (this.b.d()) {
            w12.f().b("Automatic data collection is enabled. Allowing upload.");
            this.o.e(Boolean.FALSE);
            return com.google.android.gms.tasks.d.e(Boolean.TRUE);
        }
        w12.f().b("Automatic data collection is disabled.");
        w12.f().i("Notifying that unsent reports are available.");
        this.o.e(Boolean.TRUE);
        com.google.android.gms.tasks.c<TContinuationResult> q = this.b.g().q(new d(this));
        w12.f().b("Waiting for send/deleteUnsentReports to be called.");
        return j.e(q, this.p.a());
    }

    public final void Q(String str) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 30) {
            List<ApplicationExitInfo> historicalProcessExitReasons = ((ActivityManager) this.a.getSystemService("activity")).getHistoricalProcessExitReasons(null, 0, 1);
            if (historicalProcessExitReasons.size() != 0) {
                r12 r12Var = new r12(this.a, this.h, str);
                zf4 zf4Var = new zf4();
                zf4Var.c(new com.google.firebase.crashlytics.internal.common.g(B()).e(str));
                this.m.p(str, historicalProcessExitReasons.get(0), r12Var, zf4Var);
                return;
            }
            return;
        }
        w12 f2 = w12.f();
        f2.i("ANR feature enabled, but device is API " + i);
    }

    public final void R(String str, long j) {
        this.j.d(str, String.format(Locale.US, "Crashlytics Android SDK/%s", com.google.firebase.crashlytics.internal.common.d.i()), j);
    }

    public final void S(String str) {
        String f2 = this.e.f();
        com.google.firebase.crashlytics.internal.common.a aVar = this.g;
        this.j.f(str, f2, aVar.e, aVar.f, this.e.a(), DeliveryMechanism.determineFrom(this.g.c).getId(), this.k);
    }

    public final void T(String str) {
        Context y = y();
        StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
        int l = CommonUtils.l();
        String str2 = Build.MODEL;
        int availableProcessors = Runtime.getRuntime().availableProcessors();
        long s = CommonUtils.s();
        long blockSize = statFs.getBlockSize();
        this.j.c(str, l, str2, availableProcessors, s, blockSize * statFs.getBlockCount(), CommonUtils.x(y), CommonUtils.m(y), Build.MANUFACTURER, Build.PRODUCT);
    }

    public final void U(String str) {
        this.j.g(str, Build.VERSION.RELEASE, Build.VERSION.CODENAME, CommonUtils.y(y()));
    }

    public void V(long j, String str) {
        this.d.g(new f(j, str));
    }

    public boolean o() {
        if (!this.c.c()) {
            String z = z();
            return z != null && this.j.e(z);
        }
        w12.f().i("Found previous crash marker.");
        this.c.d();
        return true;
    }

    public void p(en3 en3Var) {
        q(false, en3Var);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public final void q(boolean z, en3 en3Var) {
        List<String> m = this.m.m();
        if (m.size() <= z) {
            w12.f().i("No open sessions to be closed.");
            return;
        }
        String str = m.get(z ? 1 : 0);
        if (en3Var.b().a().b) {
            Q(str);
        }
        if (this.j.e(str)) {
            v(str);
            if (!this.j.a(str)) {
                w12 f2 = w12.f();
                f2.k("Could not finalize native session: " + str);
            }
        }
        this.m.i(A(), z != 0 ? m.get(0) : null);
    }

    public final void r() {
        long A = A();
        String itVar = new it(this.e).toString();
        w12 f2 = w12.f();
        f2.b("Opening a new session with ID " + itVar);
        this.j.h(itVar);
        R(itVar, A);
        S(itVar);
        U(itVar);
        T(itVar);
        this.i.e(itVar);
        this.m.n(itVar, A);
    }

    public final void s(long j) {
        try {
            File B = B();
            new File(B, ".ae" + j).createNewFile();
        } catch (IOException e2) {
            w12.f().l("Could not create app exception marker file.", e2);
        }
    }

    public void t(Thread.UncaughtExceptionHandler uncaughtExceptionHandler, en3 en3Var) {
        N();
        com.google.firebase.crashlytics.internal.common.f fVar = new com.google.firebase.crashlytics.internal.common.f(new b(), en3Var, uncaughtExceptionHandler);
        this.n = fVar;
        Thread.setDefaultUncaughtExceptionHandler(fVar);
    }

    public final void v(String str) {
        w12 f2 = w12.f();
        f2.i("Finalizing native report for session " + str);
        ud2 b2 = this.j.b(str);
        File d2 = b2.d();
        if (d2 != null && d2.exists()) {
            long lastModified = d2.lastModified();
            r12 r12Var = new r12(this.a, this.h, str);
            File file = new File(D(), str);
            if (!file.mkdirs()) {
                w12.f().k("Couldn't create directory to store native session files, aborting.");
                return;
            }
            s(lastModified);
            List<td2> C = C(b2, str, B(), r12Var.b());
            h.b(file, C);
            this.m.h(str, C);
            r12Var.a();
            return;
        }
        w12 f3 = w12.f();
        f3.k("No minidump data found for session " + str);
    }

    public boolean w(en3 en3Var) {
        this.d.b();
        if (G()) {
            w12.f().k("Skipping session finalization because a crash has already occurred.");
            return false;
        }
        w12.f().i("Finalizing previously open sessions.");
        try {
            q(true, en3Var);
            w12.f().i("Closed all previously open sessions.");
            return true;
        } catch (Exception e2) {
            w12.f().e("Unable to finalize previously open sessions.", e2);
            return false;
        }
    }

    public final Context y() {
        return this.a;
    }

    public final String z() {
        List<String> m = this.m.m();
        if (m.isEmpty()) {
            return null;
        }
        return m.get(0);
    }
}
