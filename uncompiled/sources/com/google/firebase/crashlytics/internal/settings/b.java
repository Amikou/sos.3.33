package com.google.firebase.crashlytics.internal.settings;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import com.google.firebase.crashlytics.internal.common.CommonUtils;
import com.google.firebase.crashlytics.internal.common.DeliveryMechanism;
import java.util.Locale;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: SettingsController.java */
/* loaded from: classes2.dex */
public class b implements en3 {
    public final Context a;
    public final ln3 b;
    public final c c;
    public final tb0 d;
    public final zt e;
    public final mn3 f;
    public final be0 g;
    public final AtomicReference<cn3> h;
    public final AtomicReference<n34<vf>> i;

    /* compiled from: SettingsController.java */
    /* loaded from: classes2.dex */
    public class a implements com.google.android.gms.tasks.b<Void, Void> {
        public a() {
        }

        @Override // com.google.android.gms.tasks.b
        /* renamed from: b */
        public com.google.android.gms.tasks.c<Void> a(Void r5) throws Exception {
            JSONObject a = b.this.f.a(b.this.b, true);
            if (a != null) {
                dn3 b = b.this.c.b(a);
                b.this.e.c(b.d(), a);
                b.this.q(a, "Loaded settings: ");
                b bVar = b.this;
                bVar.r(bVar.b.f);
                b.this.h.set(b);
                ((n34) b.this.i.get()).e(b.c());
                n34 n34Var = new n34();
                n34Var.e(b.c());
                b.this.i.set(n34Var);
            }
            return com.google.android.gms.tasks.d.e(null);
        }
    }

    public b(Context context, ln3 ln3Var, tb0 tb0Var, c cVar, zt ztVar, mn3 mn3Var, be0 be0Var) {
        AtomicReference<cn3> atomicReference = new AtomicReference<>();
        this.h = atomicReference;
        this.i = new AtomicReference<>(new n34());
        this.a = context;
        this.b = ln3Var;
        this.d = tb0Var;
        this.c = cVar;
        this.e = ztVar;
        this.f = mn3Var;
        this.g = be0Var;
        atomicReference.set(com.google.firebase.crashlytics.internal.settings.a.e(tb0Var));
    }

    public static b l(Context context, String str, ln1 ln1Var, jl1 jl1Var, String str2, String str3, be0 be0Var) {
        String g = ln1Var.g();
        r24 r24Var = new r24();
        return new b(context, new ln3(str, ln1Var.h(), ln1Var.i(), ln1Var.j(), ln1Var, CommonUtils.h(CommonUtils.n(context), str, str3, str2), str3, str2, DeliveryMechanism.determineFrom(g).getId()), r24Var, new c(r24Var), new zt(context), new vk0(String.format(Locale.US, "https://firebase-settings.crashlytics.com/spi/v2/platforms/android/gmp/%s/settings", str), jl1Var), be0Var);
    }

    @Override // defpackage.en3
    public com.google.android.gms.tasks.c<vf> a() {
        return this.i.get().a();
    }

    @Override // defpackage.en3
    public cn3 b() {
        return this.h.get();
    }

    public boolean k() {
        return !n().equals(this.b.f);
    }

    public final dn3 m(SettingsCacheBehavior settingsCacheBehavior) {
        dn3 dn3Var = null;
        try {
            if (!SettingsCacheBehavior.SKIP_CACHE_LOOKUP.equals(settingsCacheBehavior)) {
                JSONObject b = this.e.b();
                if (b != null) {
                    dn3 b2 = this.c.b(b);
                    if (b2 != null) {
                        q(b, "Loaded cached settings: ");
                        long a2 = this.d.a();
                        if (!SettingsCacheBehavior.IGNORE_CACHE_EXPIRATION.equals(settingsCacheBehavior) && b2.e(a2)) {
                            w12.f().i("Cached settings have expired.");
                        }
                        try {
                            w12.f().i("Returning cached settings.");
                            dn3Var = b2;
                        } catch (Exception e) {
                            e = e;
                            dn3Var = b2;
                            w12.f().e("Failed to get cached settings", e);
                            return dn3Var;
                        }
                    } else {
                        w12.f().e("Failed to parse cached settings data.", null);
                    }
                } else {
                    w12.f().b("No cached settings data found.");
                }
            }
        } catch (Exception e2) {
            e = e2;
        }
        return dn3Var;
    }

    public final String n() {
        return CommonUtils.r(this.a).getString("existing_instance_identifier", "");
    }

    public com.google.android.gms.tasks.c<Void> o(SettingsCacheBehavior settingsCacheBehavior, Executor executor) {
        dn3 m;
        if (!k() && (m = m(settingsCacheBehavior)) != null) {
            this.h.set(m);
            this.i.get().e(m.c());
            return com.google.android.gms.tasks.d.e(null);
        }
        dn3 m2 = m(SettingsCacheBehavior.IGNORE_CACHE_EXPIRATION);
        if (m2 != null) {
            this.h.set(m2);
            this.i.get().e(m2.c());
        }
        return this.g.h().r(executor, new a());
    }

    public com.google.android.gms.tasks.c<Void> p(Executor executor) {
        return o(SettingsCacheBehavior.USE_CACHE, executor);
    }

    public final void q(JSONObject jSONObject, String str) throws JSONException {
        w12 f = w12.f();
        f.b(str + jSONObject.toString());
    }

    @SuppressLint({"CommitPrefEdits"})
    public final boolean r(String str) {
        SharedPreferences.Editor edit = CommonUtils.r(this.a).edit();
        edit.putString("existing_instance_identifier", str);
        edit.apply();
        return true;
    }
}
