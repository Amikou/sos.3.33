package com.google.firebase.crashlytics.internal.settings;

import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: DefaultSettingsJsonTransform.java */
/* loaded from: classes2.dex */
public class a implements d {
    public static vf b(JSONObject jSONObject) throws JSONException {
        return new vf(jSONObject.getString("status"), jSONObject.getString("url"), jSONObject.getString("reports_url"), jSONObject.getString("ndk_reports_url"), jSONObject.optBoolean("update_required", false));
    }

    public static s21 c(JSONObject jSONObject) {
        return new s21(jSONObject.optBoolean("collect_reports", true), jSONObject.optBoolean("collect_anrs", false));
    }

    public static lm3 d(JSONObject jSONObject) {
        return new lm3(jSONObject.optInt("max_custom_exception_events", 8), 4);
    }

    public static cn3 e(tb0 tb0Var) {
        JSONObject jSONObject = new JSONObject();
        return new dn3(f(tb0Var, 3600L, jSONObject), null, d(jSONObject), c(jSONObject), 0, 3600);
    }

    public static long f(tb0 tb0Var, long j, JSONObject jSONObject) {
        if (jSONObject.has("expires_at")) {
            return jSONObject.optLong("expires_at");
        }
        return tb0Var.a() + (j * 1000);
    }

    @Override // com.google.firebase.crashlytics.internal.settings.d
    public dn3 a(tb0 tb0Var, JSONObject jSONObject) throws JSONException {
        int optInt = jSONObject.optInt("settings_version", 0);
        int optInt2 = jSONObject.optInt("cache_duration", 3600);
        return new dn3(f(tb0Var, optInt2, jSONObject), b(jSONObject.getJSONObject("app")), d(jSONObject.getJSONObject("session")), c(jSONObject.getJSONObject("features")), optInt, optInt2);
    }
}
