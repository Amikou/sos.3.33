package com.google.firebase.crashlytics.internal.settings;

import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: SettingsJsonParser.java */
/* loaded from: classes2.dex */
public class c {
    public final tb0 a;

    public c(tb0 tb0Var) {
        this.a = tb0Var;
    }

    public static d a(int i) {
        if (i != 3) {
            return new a();
        }
        return new e();
    }

    public dn3 b(JSONObject jSONObject) throws JSONException {
        return a(jSONObject.getInt("settings_version")).a(this.a, jSONObject);
    }
}
