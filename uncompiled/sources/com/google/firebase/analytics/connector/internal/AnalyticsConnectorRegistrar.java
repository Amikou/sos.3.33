package com.google.firebase.analytics.connector.internal;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.annotation.Keep;
import java.util.Arrays;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement-api@@19.0.0 */
@Keep
/* loaded from: classes2.dex */
public class AnalyticsConnectorRegistrar implements g40 {
    @Override // defpackage.g40
    @Keep
    @SuppressLint({"MissingPermission"})
    public List<a40<?>> getComponents() {
        return Arrays.asList(a40.c(kb.class).b(hm0.j(c51.class)).b(hm0.j(Context.class)).b(hm0.j(mv3.class)).f(l75.a).e().d(), jz1.b("fire-analytics", "19.0.0"));
    }
}
