package com.google.firebase.analytics;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Keep;
import com.google.android.gms.tasks.d;
import com.google.firebase.installations.b;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/* compiled from: com.google.android.gms:play-services-measurement-api@@19.0.0 */
/* loaded from: classes2.dex */
public final class FirebaseAnalytics {
    public static volatile FirebaseAnalytics b;
    public final wf5 a;

    public FirebaseAnalytics(wf5 wf5Var) {
        zt2.j(wf5Var);
        this.a = wf5Var;
    }

    @Keep
    public static FirebaseAnalytics getInstance(Context context) {
        if (b == null) {
            synchronized (FirebaseAnalytics.class) {
                if (b == null) {
                    b = new FirebaseAnalytics(wf5.r(context, null, null, null, null));
                }
            }
        }
        return b;
    }

    @Keep
    public static fp5 getScionFrontendApiImplementation(Context context, Bundle bundle) {
        wf5 r = wf5.r(context, null, null, null, bundle);
        if (r == null) {
            return null;
        }
        return new ga5(r);
    }

    public void a(String str, Bundle bundle) {
        this.a.v(str, bundle);
    }

    @Keep
    public String getFirebaseInstanceId() {
        try {
            return (String) d.b(b.n().getId(), 30000L, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        } catch (ExecutionException e2) {
            throw new IllegalStateException(e2.getCause());
        } catch (TimeoutException unused) {
            throw new IllegalThreadStateException("Firebase Installations getId Task has timed out.");
        }
    }

    @Keep
    @Deprecated
    public void setCurrentScreen(Activity activity, String str, String str2) {
        this.a.B(activity, str, str2);
    }
}
