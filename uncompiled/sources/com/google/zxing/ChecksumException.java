package com.google.zxing;

/* loaded from: classes2.dex */
public final class ChecksumException extends ReaderException {
    private static final ChecksumException instance = new ChecksumException();

    private ChecksumException() {
    }

    public static ChecksumException getChecksumInstance() {
        if (ReaderException.isStackTrace) {
            return new ChecksumException();
        }
        return instance;
    }

    private ChecksumException(Throwable th) {
        super(th);
    }

    public static ChecksumException getChecksumInstance(Throwable th) {
        if (ReaderException.isStackTrace) {
            return new ChecksumException(th);
        }
        return instance;
    }
}
