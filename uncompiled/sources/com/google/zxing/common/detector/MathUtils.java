package com.google.zxing.common.detector;

import com.github.mikephil.charting.utils.Utils;

/* loaded from: classes2.dex */
public final class MathUtils {
    private MathUtils() {
    }

    public static float distance(float f, float f2, float f3, float f4) {
        float f5 = f - f3;
        float f6 = f2 - f4;
        return (float) Math.sqrt((f5 * f5) + (f6 * f6));
    }

    public static int round(float f) {
        return (int) (f + (f < Utils.FLOAT_EPSILON ? -0.5f : 0.5f));
    }

    public static float distance(int i, int i2, int i3, int i4) {
        int i5 = i - i3;
        int i6 = i2 - i4;
        return (float) Math.sqrt((i5 * i5) + (i6 * i6));
    }
}
