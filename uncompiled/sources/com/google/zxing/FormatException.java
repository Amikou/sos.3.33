package com.google.zxing;

/* loaded from: classes2.dex */
public final class FormatException extends ReaderException {
    private static final FormatException instance = new FormatException();

    private FormatException() {
    }

    public static FormatException getFormatInstance() {
        if (ReaderException.isStackTrace) {
            return new FormatException();
        }
        return instance;
    }

    private FormatException(Throwable th) {
        super(th);
    }

    public static FormatException getFormatInstance(Throwable th) {
        if (ReaderException.isStackTrace) {
            return new FormatException(th);
        }
        return instance;
    }
}
