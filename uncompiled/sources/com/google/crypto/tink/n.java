package com.google.crypto.tink;

import java.security.GeneralSecurityException;

/* compiled from: Mac.java */
/* loaded from: classes2.dex */
public interface n {
    void a(byte[] bArr, byte[] bArr2) throws GeneralSecurityException;

    byte[] b(byte[] bArr) throws GeneralSecurityException;
}
