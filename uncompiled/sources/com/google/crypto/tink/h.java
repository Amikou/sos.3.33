package com.google.crypto.tink;

import com.google.crypto.tink.proto.t;
import com.google.crypto.tink.proto.y;
import com.google.crypto.tink.proto.z;
import com.google.crypto.tink.shaded.protobuf.ByteString;
import com.google.crypto.tink.shaded.protobuf.InvalidProtocolBufferException;
import java.io.IOException;
import java.security.GeneralSecurityException;

/* compiled from: KeysetHandle.java */
/* loaded from: classes2.dex */
public final class h {
    public final y a;

    public h(y yVar) {
        this.a = yVar;
    }

    public static void a(t tVar) throws GeneralSecurityException {
        if (tVar == null || tVar.G().size() == 0) {
            throw new GeneralSecurityException("empty keyset");
        }
    }

    public static void b(y yVar) throws GeneralSecurityException {
        if (yVar == null || yVar.J() <= 0) {
            throw new GeneralSecurityException("empty keyset");
        }
    }

    public static y c(t tVar, a aVar) throws GeneralSecurityException {
        try {
            y N = y.N(aVar.b(tVar.G().toByteArray(), new byte[0]), com.google.crypto.tink.shaded.protobuf.n.b());
            b(N);
            return N;
        } catch (InvalidProtocolBufferException unused) {
            throw new GeneralSecurityException("invalid keyset, corrupted key material");
        }
    }

    public static t d(y yVar, a aVar) throws GeneralSecurityException {
        byte[] a = aVar.a(yVar.toByteArray(), new byte[0]);
        try {
            if (y.N(aVar.b(a, new byte[0]), com.google.crypto.tink.shaded.protobuf.n.b()).equals(yVar)) {
                return t.H().s(ByteString.copyFrom(a)).t(r.b(yVar)).build();
            }
            throw new GeneralSecurityException("cannot encrypt keyset");
        } catch (InvalidProtocolBufferException unused) {
            throw new GeneralSecurityException("invalid keyset, corrupted key material");
        }
    }

    public static final h e(y yVar) throws GeneralSecurityException {
        b(yVar);
        return new h(yVar);
    }

    public static final h j(j jVar, a aVar) throws GeneralSecurityException, IOException {
        t a = jVar.a();
        a(a);
        return new h(c(a, aVar));
    }

    public y f() {
        return this.a;
    }

    public z g() {
        return r.b(this.a);
    }

    public <P> P h(Class<P> cls) throws GeneralSecurityException {
        Class<?> e = q.e(cls);
        if (e != null) {
            return (P) i(cls, e);
        }
        throw new GeneralSecurityException("No wrapper found for " + cls.getName());
    }

    public final <B, P> P i(Class<P> cls, Class<B> cls2) throws GeneralSecurityException {
        return (P) q.t(q.l(this, cls2), cls);
    }

    public void k(k kVar, a aVar) throws GeneralSecurityException, IOException {
        kVar.b(d(this.a, aVar));
    }

    public String toString() {
        return g().toString();
    }
}
