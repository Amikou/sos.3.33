package com.google.crypto.tink;

import com.google.crypto.tink.proto.KeyData;
import com.google.crypto.tink.proto.KeyStatusType;
import com.google.crypto.tink.proto.OutputPrefixType;
import com.google.crypto.tink.proto.x;
import com.google.crypto.tink.proto.y;
import java.security.GeneralSecurityException;
import java.security.SecureRandom;

/* compiled from: KeysetManager.java */
/* loaded from: classes2.dex */
public final class i {
    public final y.b a;

    public i(y.b bVar) {
        this.a = bVar;
    }

    public static int g() {
        SecureRandom secureRandom = new SecureRandom();
        byte[] bArr = new byte[4];
        int i = 0;
        while (i == 0) {
            secureRandom.nextBytes(bArr);
            i = ((bArr[0] & Byte.MAX_VALUE) << 24) | ((bArr[1] & 255) << 16) | ((bArr[2] & 255) << 8) | (bArr[3] & 255);
        }
        return i;
    }

    public static i i() {
        return new i(y.M());
    }

    public static i j(h hVar) {
        return new i(hVar.f().toBuilder());
    }

    public synchronized i a(KeyTemplate keyTemplate) throws GeneralSecurityException {
        b(keyTemplate.b(), false);
        return this;
    }

    @Deprecated
    public synchronized int b(x xVar, boolean z) throws GeneralSecurityException {
        y.c e;
        e = e(xVar);
        this.a.s(e);
        if (z) {
            this.a.x(e.J());
        }
        return e.J();
    }

    public synchronized h c() throws GeneralSecurityException {
        return h.e(this.a.build());
    }

    public final synchronized boolean d(int i) {
        for (y.c cVar : this.a.v()) {
            if (cVar.J() == i) {
                return true;
            }
        }
        return false;
    }

    public final synchronized y.c e(x xVar) throws GeneralSecurityException {
        KeyData p;
        int f;
        OutputPrefixType I;
        p = q.p(xVar);
        f = f();
        I = xVar.I();
        if (I == OutputPrefixType.UNKNOWN_PREFIX) {
            I = OutputPrefixType.TINK;
        }
        return y.c.N().s(p).t(f).v(KeyStatusType.ENABLED).u(I).build();
    }

    public final synchronized int f() {
        int g;
        g = g();
        while (d(g)) {
            g = g();
        }
        return g;
    }

    public synchronized i h(int i) throws GeneralSecurityException {
        for (int i2 = 0; i2 < this.a.u(); i2++) {
            y.c t = this.a.t(i2);
            if (t.J() == i) {
                if (t.L().equals(KeyStatusType.ENABLED)) {
                    this.a.x(i);
                } else {
                    throw new GeneralSecurityException("cannot set key as primary because it's not enabled: " + i);
                }
            }
        }
        throw new GeneralSecurityException("key not found: " + i);
        return this;
    }
}
