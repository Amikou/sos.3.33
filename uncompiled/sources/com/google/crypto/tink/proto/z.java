package com.google.crypto.tink.proto;

import com.google.crypto.tink.shaded.protobuf.GeneratedMessageLite;
import com.google.crypto.tink.shaded.protobuf.j0;
import com.google.crypto.tink.shaded.protobuf.v;

/* compiled from: KeysetInfo.java */
/* loaded from: classes2.dex */
public final class z extends GeneratedMessageLite<z, b> implements e82 {
    private static final z DEFAULT_INSTANCE;
    public static final int KEY_INFO_FIELD_NUMBER = 2;
    private static volatile j0<z> PARSER = null;
    public static final int PRIMARY_KEY_ID_FIELD_NUMBER = 1;
    private v.i<c> keyInfo_ = GeneratedMessageLite.m();
    private int primaryKeyId_;

    /* compiled from: KeysetInfo.java */
    /* loaded from: classes2.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            a = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                a[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                a[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                a[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                a[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    /* compiled from: KeysetInfo.java */
    /* loaded from: classes2.dex */
    public static final class b extends GeneratedMessageLite.a<z, b> implements e82 {
        public /* synthetic */ b(a aVar) {
            this();
        }

        public b s(c cVar) {
            j();
            ((z) this.f0).G(cVar);
            return this;
        }

        public b t(int i) {
            j();
            ((z) this.f0).K(i);
            return this;
        }

        public b() {
            super(z.DEFAULT_INSTANCE);
        }
    }

    /* compiled from: KeysetInfo.java */
    /* loaded from: classes2.dex */
    public static final class c extends GeneratedMessageLite<c, a> implements e82 {
        private static final c DEFAULT_INSTANCE;
        public static final int KEY_ID_FIELD_NUMBER = 3;
        public static final int OUTPUT_PREFIX_TYPE_FIELD_NUMBER = 4;
        private static volatile j0<c> PARSER = null;
        public static final int STATUS_FIELD_NUMBER = 2;
        public static final int TYPE_URL_FIELD_NUMBER = 1;
        private int keyId_;
        private int outputPrefixType_;
        private int status_;
        private String typeUrl_ = "";

        /* compiled from: KeysetInfo.java */
        /* loaded from: classes2.dex */
        public static final class a extends GeneratedMessageLite.a<c, a> implements e82 {
            public /* synthetic */ a(a aVar) {
                this();
            }

            public a s(int i) {
                j();
                ((c) this.f0).K(i);
                return this;
            }

            public a t(OutputPrefixType outputPrefixType) {
                j();
                ((c) this.f0).L(outputPrefixType);
                return this;
            }

            public a u(KeyStatusType keyStatusType) {
                j();
                ((c) this.f0).M(keyStatusType);
                return this;
            }

            public a v(String str) {
                j();
                ((c) this.f0).N(str);
                return this;
            }

            public a() {
                super(c.DEFAULT_INSTANCE);
            }
        }

        static {
            c cVar = new c();
            DEFAULT_INSTANCE = cVar;
            GeneratedMessageLite.A(c.class, cVar);
        }

        public static a J() {
            return DEFAULT_INSTANCE.i();
        }

        public int I() {
            return this.keyId_;
        }

        public final void K(int i) {
            this.keyId_ = i;
        }

        public final void L(OutputPrefixType outputPrefixType) {
            this.outputPrefixType_ = outputPrefixType.getNumber();
        }

        public final void M(KeyStatusType keyStatusType) {
            this.status_ = keyStatusType.getNumber();
        }

        public final void N(String str) {
            str.getClass();
            this.typeUrl_ = str;
        }

        @Override // com.google.crypto.tink.shaded.protobuf.GeneratedMessageLite
        public final Object l(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
            switch (a.a[methodToInvoke.ordinal()]) {
                case 1:
                    return new c();
                case 2:
                    return new a(null);
                case 3:
                    return GeneratedMessageLite.u(DEFAULT_INSTANCE, "\u0000\u0004\u0000\u0000\u0001\u0004\u0004\u0000\u0000\u0000\u0001Ȉ\u0002\f\u0003\u000b\u0004\f", new Object[]{"typeUrl_", "status_", "keyId_", "outputPrefixType_"});
                case 4:
                    return DEFAULT_INSTANCE;
                case 5:
                    j0<c> j0Var = PARSER;
                    if (j0Var == null) {
                        synchronized (c.class) {
                            j0Var = PARSER;
                            if (j0Var == null) {
                                j0Var = new GeneratedMessageLite.b<>(DEFAULT_INSTANCE);
                                PARSER = j0Var;
                            }
                        }
                    }
                    return j0Var;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }
    }

    static {
        z zVar = new z();
        DEFAULT_INSTANCE = zVar;
        GeneratedMessageLite.A(z.class, zVar);
    }

    public static b J() {
        return DEFAULT_INSTANCE.i();
    }

    public final void G(c cVar) {
        cVar.getClass();
        H();
        this.keyInfo_.add(cVar);
    }

    public final void H() {
        if (this.keyInfo_.A()) {
            return;
        }
        this.keyInfo_ = GeneratedMessageLite.s(this.keyInfo_);
    }

    public c I(int i) {
        return this.keyInfo_.get(i);
    }

    public final void K(int i) {
        this.primaryKeyId_ = i;
    }

    @Override // com.google.crypto.tink.shaded.protobuf.GeneratedMessageLite
    public final Object l(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (a.a[methodToInvoke.ordinal()]) {
            case 1:
                return new z();
            case 2:
                return new b(null);
            case 3:
                return GeneratedMessageLite.u(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0001\u0000\u0001\u000b\u0002\u001b", new Object[]{"primaryKeyId_", "keyInfo_", c.class});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                j0<z> j0Var = PARSER;
                if (j0Var == null) {
                    synchronized (z.class) {
                        j0Var = PARSER;
                        if (j0Var == null) {
                            j0Var = new GeneratedMessageLite.b<>(DEFAULT_INSTANCE);
                            PARSER = j0Var;
                        }
                    }
                }
                return j0Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
