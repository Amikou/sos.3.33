package com.google.crypto.tink.proto;

import com.google.crypto.tink.shaded.protobuf.GeneratedMessageLite;
import com.google.crypto.tink.shaded.protobuf.InvalidProtocolBufferException;
import com.google.crypto.tink.shaded.protobuf.j0;
import com.google.crypto.tink.shaded.protobuf.v;
import java.util.Collections;
import java.util.List;

/* compiled from: Keyset.java */
/* loaded from: classes2.dex */
public final class y extends GeneratedMessageLite<y, b> implements e82 {
    private static final y DEFAULT_INSTANCE;
    public static final int KEY_FIELD_NUMBER = 2;
    private static volatile j0<y> PARSER = null;
    public static final int PRIMARY_KEY_ID_FIELD_NUMBER = 1;
    private v.i<c> key_ = GeneratedMessageLite.m();
    private int primaryKeyId_;

    /* compiled from: Keyset.java */
    /* loaded from: classes2.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            a = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                a[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                a[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                a[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                a[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    /* compiled from: Keyset.java */
    /* loaded from: classes2.dex */
    public static final class b extends GeneratedMessageLite.a<y, b> implements e82 {
        public /* synthetic */ b(a aVar) {
            this();
        }

        public b s(c cVar) {
            j();
            ((y) this.f0).G(cVar);
            return this;
        }

        public c t(int i) {
            return ((y) this.f0).I(i);
        }

        public int u() {
            return ((y) this.f0).J();
        }

        public List<c> v() {
            return Collections.unmodifiableList(((y) this.f0).K());
        }

        public b x(int i) {
            j();
            ((y) this.f0).O(i);
            return this;
        }

        public b() {
            super(y.DEFAULT_INSTANCE);
        }
    }

    /* compiled from: Keyset.java */
    /* loaded from: classes2.dex */
    public static final class c extends GeneratedMessageLite<c, a> implements e82 {
        private static final c DEFAULT_INSTANCE;
        public static final int KEY_DATA_FIELD_NUMBER = 1;
        public static final int KEY_ID_FIELD_NUMBER = 3;
        public static final int OUTPUT_PREFIX_TYPE_FIELD_NUMBER = 4;
        private static volatile j0<c> PARSER = null;
        public static final int STATUS_FIELD_NUMBER = 2;
        private KeyData keyData_;
        private int keyId_;
        private int outputPrefixType_;
        private int status_;

        /* compiled from: Keyset.java */
        /* loaded from: classes2.dex */
        public static final class a extends GeneratedMessageLite.a<c, a> implements e82 {
            public /* synthetic */ a(a aVar) {
                this();
            }

            public a s(KeyData keyData) {
                j();
                ((c) this.f0).O(keyData);
                return this;
            }

            public a t(int i) {
                j();
                ((c) this.f0).P(i);
                return this;
            }

            public a u(OutputPrefixType outputPrefixType) {
                j();
                ((c) this.f0).Q(outputPrefixType);
                return this;
            }

            public a v(KeyStatusType keyStatusType) {
                j();
                ((c) this.f0).R(keyStatusType);
                return this;
            }

            public a() {
                super(c.DEFAULT_INSTANCE);
            }
        }

        static {
            c cVar = new c();
            DEFAULT_INSTANCE = cVar;
            GeneratedMessageLite.A(c.class, cVar);
        }

        public static a N() {
            return DEFAULT_INSTANCE.i();
        }

        public KeyData I() {
            KeyData keyData = this.keyData_;
            return keyData == null ? KeyData.H() : keyData;
        }

        public int J() {
            return this.keyId_;
        }

        public OutputPrefixType K() {
            OutputPrefixType forNumber = OutputPrefixType.forNumber(this.outputPrefixType_);
            return forNumber == null ? OutputPrefixType.UNRECOGNIZED : forNumber;
        }

        public KeyStatusType L() {
            KeyStatusType forNumber = KeyStatusType.forNumber(this.status_);
            return forNumber == null ? KeyStatusType.UNRECOGNIZED : forNumber;
        }

        public boolean M() {
            return this.keyData_ != null;
        }

        public final void O(KeyData keyData) {
            keyData.getClass();
            this.keyData_ = keyData;
        }

        public final void P(int i) {
            this.keyId_ = i;
        }

        public final void Q(OutputPrefixType outputPrefixType) {
            this.outputPrefixType_ = outputPrefixType.getNumber();
        }

        public final void R(KeyStatusType keyStatusType) {
            this.status_ = keyStatusType.getNumber();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.GeneratedMessageLite
        public final Object l(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
            switch (a.a[methodToInvoke.ordinal()]) {
                case 1:
                    return new c();
                case 2:
                    return new a(null);
                case 3:
                    return GeneratedMessageLite.u(DEFAULT_INSTANCE, "\u0000\u0004\u0000\u0000\u0001\u0004\u0004\u0000\u0000\u0000\u0001\t\u0002\f\u0003\u000b\u0004\f", new Object[]{"keyData_", "status_", "keyId_", "outputPrefixType_"});
                case 4:
                    return DEFAULT_INSTANCE;
                case 5:
                    j0<c> j0Var = PARSER;
                    if (j0Var == null) {
                        synchronized (c.class) {
                            j0Var = PARSER;
                            if (j0Var == null) {
                                j0Var = new GeneratedMessageLite.b<>(DEFAULT_INSTANCE);
                                PARSER = j0Var;
                            }
                        }
                    }
                    return j0Var;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }
    }

    static {
        y yVar = new y();
        DEFAULT_INSTANCE = yVar;
        GeneratedMessageLite.A(y.class, yVar);
    }

    public static b M() {
        return DEFAULT_INSTANCE.i();
    }

    public static y N(byte[] bArr, com.google.crypto.tink.shaded.protobuf.n nVar) throws InvalidProtocolBufferException {
        return (y) GeneratedMessageLite.w(DEFAULT_INSTANCE, bArr, nVar);
    }

    public final void G(c cVar) {
        cVar.getClass();
        H();
        this.key_.add(cVar);
    }

    public final void H() {
        if (this.key_.A()) {
            return;
        }
        this.key_ = GeneratedMessageLite.s(this.key_);
    }

    public c I(int i) {
        return this.key_.get(i);
    }

    public int J() {
        return this.key_.size();
    }

    public List<c> K() {
        return this.key_;
    }

    public int L() {
        return this.primaryKeyId_;
    }

    public final void O(int i) {
        this.primaryKeyId_ = i;
    }

    @Override // com.google.crypto.tink.shaded.protobuf.GeneratedMessageLite
    public final Object l(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (a.a[methodToInvoke.ordinal()]) {
            case 1:
                return new y();
            case 2:
                return new b(null);
            case 3:
                return GeneratedMessageLite.u(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0001\u0000\u0001\u000b\u0002\u001b", new Object[]{"primaryKeyId_", "key_", c.class});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                j0<y> j0Var = PARSER;
                if (j0Var == null) {
                    synchronized (y.class) {
                        j0Var = PARSER;
                        if (j0Var == null) {
                            j0Var = new GeneratedMessageLite.b<>(DEFAULT_INSTANCE);
                            PARSER = j0Var;
                        }
                    }
                }
                return j0Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
