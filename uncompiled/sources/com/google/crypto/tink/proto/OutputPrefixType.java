package com.google.crypto.tink.proto;

import com.google.crypto.tink.shaded.protobuf.v;

/* loaded from: classes2.dex */
public enum OutputPrefixType implements v.c {
    UNKNOWN_PREFIX(0),
    TINK(1),
    LEGACY(2),
    RAW(3),
    CRUNCHY(4),
    UNRECOGNIZED(-1);
    
    public static final int CRUNCHY_VALUE = 4;
    public static final int LEGACY_VALUE = 2;
    public static final int RAW_VALUE = 3;
    public static final int TINK_VALUE = 1;
    public static final int UNKNOWN_PREFIX_VALUE = 0;
    public static final v.d<OutputPrefixType> a = new v.d<OutputPrefixType>() { // from class: com.google.crypto.tink.proto.OutputPrefixType.a
        @Override // com.google.crypto.tink.shaded.protobuf.v.d
        /* renamed from: a */
        public OutputPrefixType findValueByNumber(int i) {
            return OutputPrefixType.forNumber(i);
        }
    };
    private final int value;

    /* loaded from: classes2.dex */
    public static final class b implements v.e {
        public static final v.e a = new b();

        @Override // com.google.crypto.tink.shaded.protobuf.v.e
        public boolean a(int i) {
            return OutputPrefixType.forNumber(i) != null;
        }
    }

    OutputPrefixType(int i) {
        this.value = i;
    }

    public static OutputPrefixType forNumber(int i) {
        if (i != 0) {
            if (i != 1) {
                if (i != 2) {
                    if (i != 3) {
                        if (i != 4) {
                            return null;
                        }
                        return CRUNCHY;
                    }
                    return RAW;
                }
                return LEGACY;
            }
            return TINK;
        }
        return UNKNOWN_PREFIX;
    }

    public static v.d<OutputPrefixType> internalGetValueMap() {
        return a;
    }

    public static v.e internalGetVerifier() {
        return b.a;
    }

    @Override // com.google.crypto.tink.shaded.protobuf.v.c
    public final int getNumber() {
        if (this != UNRECOGNIZED) {
            return this.value;
        }
        throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
    }

    @Deprecated
    public static OutputPrefixType valueOf(int i) {
        return forNumber(i);
    }
}
