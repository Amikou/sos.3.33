package com.google.crypto.tink.proto;

import com.google.crypto.tink.shaded.protobuf.v;

/* loaded from: classes2.dex */
public enum KeyStatusType implements v.c {
    UNKNOWN_STATUS(0),
    ENABLED(1),
    DISABLED(2),
    DESTROYED(3),
    UNRECOGNIZED(-1);
    
    public static final int DESTROYED_VALUE = 3;
    public static final int DISABLED_VALUE = 2;
    public static final int ENABLED_VALUE = 1;
    public static final int UNKNOWN_STATUS_VALUE = 0;
    public static final v.d<KeyStatusType> a = new v.d<KeyStatusType>() { // from class: com.google.crypto.tink.proto.KeyStatusType.a
        @Override // com.google.crypto.tink.shaded.protobuf.v.d
        /* renamed from: a */
        public KeyStatusType findValueByNumber(int i) {
            return KeyStatusType.forNumber(i);
        }
    };
    private final int value;

    /* loaded from: classes2.dex */
    public static final class b implements v.e {
        public static final v.e a = new b();

        @Override // com.google.crypto.tink.shaded.protobuf.v.e
        public boolean a(int i) {
            return KeyStatusType.forNumber(i) != null;
        }
    }

    KeyStatusType(int i) {
        this.value = i;
    }

    public static KeyStatusType forNumber(int i) {
        if (i != 0) {
            if (i != 1) {
                if (i != 2) {
                    if (i != 3) {
                        return null;
                    }
                    return DESTROYED;
                }
                return DISABLED;
            }
            return ENABLED;
        }
        return UNKNOWN_STATUS;
    }

    public static v.d<KeyStatusType> internalGetValueMap() {
        return a;
    }

    public static v.e internalGetVerifier() {
        return b.a;
    }

    @Override // com.google.crypto.tink.shaded.protobuf.v.c
    public final int getNumber() {
        if (this != UNRECOGNIZED) {
            return this.value;
        }
        throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
    }

    @Deprecated
    public static KeyStatusType valueOf(int i) {
        return forNumber(i);
    }
}
