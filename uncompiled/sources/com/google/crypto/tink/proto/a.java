package com.google.crypto.tink.proto;

import com.google.crypto.tink.shaded.protobuf.ByteString;
import com.google.crypto.tink.shaded.protobuf.GeneratedMessageLite;
import com.google.crypto.tink.shaded.protobuf.InvalidProtocolBufferException;
import com.google.crypto.tink.shaded.protobuf.j0;

/* compiled from: AesCmacKey.java */
/* loaded from: classes2.dex */
public final class a extends GeneratedMessageLite<a, b> implements e82 {
    private static final a DEFAULT_INSTANCE;
    public static final int KEY_VALUE_FIELD_NUMBER = 2;
    public static final int PARAMS_FIELD_NUMBER = 3;
    private static volatile j0<a> PARSER = null;
    public static final int VERSION_FIELD_NUMBER = 1;
    private ByteString keyValue_ = ByteString.EMPTY;
    private c params_;
    private int version_;

    /* compiled from: AesCmacKey.java */
    /* renamed from: com.google.crypto.tink.proto.a$a  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public static /* synthetic */ class C0135a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            a = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                a[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                a[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                a[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                a[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    /* compiled from: AesCmacKey.java */
    /* loaded from: classes2.dex */
    public static final class b extends GeneratedMessageLite.a<a, b> implements e82 {
        public /* synthetic */ b(C0135a c0135a) {
            this();
        }

        public b s(ByteString byteString) {
            j();
            ((a) this.f0).M(byteString);
            return this;
        }

        public b t(c cVar) {
            j();
            ((a) this.f0).N(cVar);
            return this;
        }

        public b u(int i) {
            j();
            ((a) this.f0).O(i);
            return this;
        }

        public b() {
            super(a.DEFAULT_INSTANCE);
        }
    }

    static {
        a aVar = new a();
        DEFAULT_INSTANCE = aVar;
        GeneratedMessageLite.A(a.class, aVar);
    }

    public static b K() {
        return DEFAULT_INSTANCE.i();
    }

    public static a L(ByteString byteString, com.google.crypto.tink.shaded.protobuf.n nVar) throws InvalidProtocolBufferException {
        return (a) GeneratedMessageLite.v(DEFAULT_INSTANCE, byteString, nVar);
    }

    public ByteString H() {
        return this.keyValue_;
    }

    public c I() {
        c cVar = this.params_;
        return cVar == null ? c.D() : cVar;
    }

    public int J() {
        return this.version_;
    }

    public final void M(ByteString byteString) {
        byteString.getClass();
        this.keyValue_ = byteString;
    }

    public final void N(c cVar) {
        cVar.getClass();
        this.params_ = cVar;
    }

    public final void O(int i) {
        this.version_ = i;
    }

    @Override // com.google.crypto.tink.shaded.protobuf.GeneratedMessageLite
    public final Object l(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (C0135a.a[methodToInvoke.ordinal()]) {
            case 1:
                return new a();
            case 2:
                return new b(null);
            case 3:
                return GeneratedMessageLite.u(DEFAULT_INSTANCE, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0000\u0000\u0000\u0001\u000b\u0002\n\u0003\t", new Object[]{"version_", "keyValue_", "params_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                j0<a> j0Var = PARSER;
                if (j0Var == null) {
                    synchronized (a.class) {
                        j0Var = PARSER;
                        if (j0Var == null) {
                            j0Var = new GeneratedMessageLite.b<>(DEFAULT_INSTANCE);
                            PARSER = j0Var;
                        }
                    }
                }
                return j0Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
