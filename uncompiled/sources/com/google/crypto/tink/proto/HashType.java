package com.google.crypto.tink.proto;

import com.google.crypto.tink.shaded.protobuf.v;

/* loaded from: classes2.dex */
public enum HashType implements v.c {
    UNKNOWN_HASH(0),
    SHA1(1),
    SHA384(2),
    SHA256(3),
    SHA512(4),
    UNRECOGNIZED(-1);
    
    public static final int SHA1_VALUE = 1;
    public static final int SHA256_VALUE = 3;
    public static final int SHA384_VALUE = 2;
    public static final int SHA512_VALUE = 4;
    public static final int UNKNOWN_HASH_VALUE = 0;
    public static final v.d<HashType> a = new v.d<HashType>() { // from class: com.google.crypto.tink.proto.HashType.a
        @Override // com.google.crypto.tink.shaded.protobuf.v.d
        /* renamed from: a */
        public HashType findValueByNumber(int i) {
            return HashType.forNumber(i);
        }
    };
    private final int value;

    /* loaded from: classes2.dex */
    public static final class b implements v.e {
        public static final v.e a = new b();

        @Override // com.google.crypto.tink.shaded.protobuf.v.e
        public boolean a(int i) {
            return HashType.forNumber(i) != null;
        }
    }

    HashType(int i) {
        this.value = i;
    }

    public static HashType forNumber(int i) {
        if (i != 0) {
            if (i != 1) {
                if (i != 2) {
                    if (i != 3) {
                        if (i != 4) {
                            return null;
                        }
                        return SHA512;
                    }
                    return SHA256;
                }
                return SHA384;
            }
            return SHA1;
        }
        return UNKNOWN_HASH;
    }

    public static v.d<HashType> internalGetValueMap() {
        return a;
    }

    public static v.e internalGetVerifier() {
        return b.a;
    }

    @Override // com.google.crypto.tink.shaded.protobuf.v.c
    public final int getNumber() {
        if (this != UNRECOGNIZED) {
            return this.value;
        }
        throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
    }

    @Deprecated
    public static HashType valueOf(int i) {
        return forNumber(i);
    }
}
