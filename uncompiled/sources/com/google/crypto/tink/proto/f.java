package com.google.crypto.tink.proto;

import com.google.crypto.tink.shaded.protobuf.ByteString;
import com.google.crypto.tink.shaded.protobuf.GeneratedMessageLite;
import com.google.crypto.tink.shaded.protobuf.InvalidProtocolBufferException;
import com.google.crypto.tink.shaded.protobuf.j0;

/* compiled from: AesCtrKey.java */
/* loaded from: classes2.dex */
public final class f extends GeneratedMessageLite<f, b> implements e82 {
    private static final f DEFAULT_INSTANCE;
    public static final int KEY_VALUE_FIELD_NUMBER = 3;
    public static final int PARAMS_FIELD_NUMBER = 2;
    private static volatile j0<f> PARSER = null;
    public static final int VERSION_FIELD_NUMBER = 1;
    private ByteString keyValue_ = ByteString.EMPTY;
    private h params_;
    private int version_;

    /* compiled from: AesCtrKey.java */
    /* loaded from: classes2.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            a = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                a[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                a[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                a[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                a[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    /* compiled from: AesCtrKey.java */
    /* loaded from: classes2.dex */
    public static final class b extends GeneratedMessageLite.a<f, b> implements e82 {
        public /* synthetic */ b(a aVar) {
            this();
        }

        public b s(ByteString byteString) {
            j();
            ((f) this.f0).N(byteString);
            return this;
        }

        public b t(h hVar) {
            j();
            ((f) this.f0).O(hVar);
            return this;
        }

        public b u(int i) {
            j();
            ((f) this.f0).P(i);
            return this;
        }

        public b() {
            super(f.DEFAULT_INSTANCE);
        }
    }

    static {
        f fVar = new f();
        DEFAULT_INSTANCE = fVar;
        GeneratedMessageLite.A(f.class, fVar);
    }

    public static f H() {
        return DEFAULT_INSTANCE;
    }

    public static b L() {
        return DEFAULT_INSTANCE.i();
    }

    public static f M(ByteString byteString, com.google.crypto.tink.shaded.protobuf.n nVar) throws InvalidProtocolBufferException {
        return (f) GeneratedMessageLite.v(DEFAULT_INSTANCE, byteString, nVar);
    }

    public ByteString I() {
        return this.keyValue_;
    }

    public h J() {
        h hVar = this.params_;
        return hVar == null ? h.D() : hVar;
    }

    public int K() {
        return this.version_;
    }

    public final void N(ByteString byteString) {
        byteString.getClass();
        this.keyValue_ = byteString;
    }

    public final void O(h hVar) {
        hVar.getClass();
        this.params_ = hVar;
    }

    public final void P(int i) {
        this.version_ = i;
    }

    @Override // com.google.crypto.tink.shaded.protobuf.GeneratedMessageLite
    public final Object l(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (a.a[methodToInvoke.ordinal()]) {
            case 1:
                return new f();
            case 2:
                return new b(null);
            case 3:
                return GeneratedMessageLite.u(DEFAULT_INSTANCE, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0000\u0000\u0000\u0001\u000b\u0002\t\u0003\n", new Object[]{"version_", "params_", "keyValue_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                j0<f> j0Var = PARSER;
                if (j0Var == null) {
                    synchronized (f.class) {
                        j0Var = PARSER;
                        if (j0Var == null) {
                            j0Var = new GeneratedMessageLite.b<>(DEFAULT_INSTANCE);
                            PARSER = j0Var;
                        }
                    }
                }
                return j0Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
