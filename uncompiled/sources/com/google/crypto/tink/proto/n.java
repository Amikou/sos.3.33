package com.google.crypto.tink.proto;

import com.google.crypto.tink.shaded.protobuf.ByteString;
import com.google.crypto.tink.shaded.protobuf.GeneratedMessageLite;
import com.google.crypto.tink.shaded.protobuf.InvalidProtocolBufferException;
import com.google.crypto.tink.shaded.protobuf.j0;

/* compiled from: AesGcmSivKey.java */
/* loaded from: classes2.dex */
public final class n extends GeneratedMessageLite<n, b> implements e82 {
    private static final n DEFAULT_INSTANCE;
    public static final int KEY_VALUE_FIELD_NUMBER = 3;
    private static volatile j0<n> PARSER = null;
    public static final int VERSION_FIELD_NUMBER = 1;
    private ByteString keyValue_ = ByteString.EMPTY;
    private int version_;

    /* compiled from: AesGcmSivKey.java */
    /* loaded from: classes2.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            a = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                a[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                a[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                a[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                a[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    /* compiled from: AesGcmSivKey.java */
    /* loaded from: classes2.dex */
    public static final class b extends GeneratedMessageLite.a<n, b> implements e82 {
        public /* synthetic */ b(a aVar) {
            this();
        }

        public b s(ByteString byteString) {
            j();
            ((n) this.f0).K(byteString);
            return this;
        }

        public b t(int i) {
            j();
            ((n) this.f0).L(i);
            return this;
        }

        public b() {
            super(n.DEFAULT_INSTANCE);
        }
    }

    static {
        n nVar = new n();
        DEFAULT_INSTANCE = nVar;
        GeneratedMessageLite.A(n.class, nVar);
    }

    public static b I() {
        return DEFAULT_INSTANCE.i();
    }

    public static n J(ByteString byteString, com.google.crypto.tink.shaded.protobuf.n nVar) throws InvalidProtocolBufferException {
        return (n) GeneratedMessageLite.v(DEFAULT_INSTANCE, byteString, nVar);
    }

    public ByteString G() {
        return this.keyValue_;
    }

    public int H() {
        return this.version_;
    }

    public final void K(ByteString byteString) {
        byteString.getClass();
        this.keyValue_ = byteString;
    }

    public final void L(int i) {
        this.version_ = i;
    }

    @Override // com.google.crypto.tink.shaded.protobuf.GeneratedMessageLite
    public final Object l(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (a.a[methodToInvoke.ordinal()]) {
            case 1:
                return new n();
            case 2:
                return new b(null);
            case 3:
                return GeneratedMessageLite.u(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0003\u0002\u0000\u0000\u0000\u0001\u000b\u0003\n", new Object[]{"version_", "keyValue_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                j0<n> j0Var = PARSER;
                if (j0Var == null) {
                    synchronized (n.class) {
                        j0Var = PARSER;
                        if (j0Var == null) {
                            j0Var = new GeneratedMessageLite.b<>(DEFAULT_INSTANCE);
                            PARSER = j0Var;
                        }
                    }
                }
                return j0Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
