package com.google.crypto.tink;

import com.google.crypto.tink.proto.OutputPrefixType;
import com.google.crypto.tink.proto.y;
import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;

/* compiled from: CryptoFormat.java */
/* loaded from: classes2.dex */
public final class c {
    public static final byte[] a = new byte[0];

    /* compiled from: CryptoFormat.java */
    /* loaded from: classes2.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[OutputPrefixType.values().length];
            a = iArr;
            try {
                iArr[OutputPrefixType.LEGACY.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[OutputPrefixType.CRUNCHY.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[OutputPrefixType.TINK.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                a[OutputPrefixType.RAW.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
        }
    }

    public static byte[] a(y.c cVar) throws GeneralSecurityException {
        int i = a.a[cVar.K().ordinal()];
        if (i == 1 || i == 2) {
            return ByteBuffer.allocate(5).put((byte) 0).putInt(cVar.J()).array();
        }
        if (i != 3) {
            if (i == 4) {
                return a;
            }
            throw new GeneralSecurityException("unknown output prefix type");
        }
        return ByteBuffer.allocate(5).put((byte) 1).putInt(cVar.J()).array();
    }
}
