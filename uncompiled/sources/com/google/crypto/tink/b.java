package com.google.crypto.tink;

import java.io.IOException;
import java.security.GeneralSecurityException;

/* compiled from: CleartextKeysetHandle.java */
/* loaded from: classes2.dex */
public final class b {
    public static h a(j jVar) throws GeneralSecurityException, IOException {
        return h.e(jVar.read());
    }

    public static void b(h hVar, k kVar) throws IOException {
        kVar.a(hVar.f());
    }
}
