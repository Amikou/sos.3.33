package com.google.crypto.tink.shaded.protobuf;

import com.google.crypto.tink.shaded.protobuf.v;
import java.util.AbstractList;
import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

/* compiled from: FloatArrayList.java */
/* loaded from: classes2.dex */
public final class s extends c<Float> implements v.f, RandomAccess, vu2 {
    public static final s h0;
    public float[] f0;
    public int g0;

    static {
        s sVar = new s(new float[0], 0);
        h0 = sVar;
        sVar.l();
    }

    public s() {
        this(new float[10], 0);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.c, java.util.AbstractCollection, java.util.Collection, java.util.List
    public boolean addAll(Collection<? extends Float> collection) {
        e();
        v.a(collection);
        if (!(collection instanceof s)) {
            return super.addAll(collection);
        }
        s sVar = (s) collection;
        int i = sVar.g0;
        if (i == 0) {
            return false;
        }
        int i2 = this.g0;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            float[] fArr = this.f0;
            if (i3 > fArr.length) {
                this.f0 = Arrays.copyOf(fArr, i3);
            }
            System.arraycopy(sVar.f0, 0, this.f0, this.g0, sVar.g0);
            this.g0 = i3;
            ((AbstractList) this).modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    @Override // com.google.crypto.tink.shaded.protobuf.c, java.util.AbstractList, java.util.Collection, java.util.List
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof s)) {
            return super.equals(obj);
        }
        s sVar = (s) obj;
        if (this.g0 != sVar.g0) {
            return false;
        }
        float[] fArr = sVar.f0;
        for (int i = 0; i < this.g0; i++) {
            if (Float.floatToIntBits(this.f0[i]) != Float.floatToIntBits(fArr[i])) {
                return false;
            }
        }
        return true;
    }

    @Override // com.google.crypto.tink.shaded.protobuf.c, java.util.AbstractList, java.util.Collection, java.util.List
    public int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.g0; i2++) {
            i = (i * 31) + Float.floatToIntBits(this.f0[i2]);
        }
        return i;
    }

    @Override // java.util.AbstractList, java.util.List
    /* renamed from: i */
    public void add(int i, Float f) {
        n(i, f.floatValue());
    }

    @Override // com.google.crypto.tink.shaded.protobuf.c, java.util.AbstractList, java.util.AbstractCollection, java.util.Collection, java.util.List
    /* renamed from: k */
    public boolean add(Float f) {
        m(f.floatValue());
        return true;
    }

    public void m(float f) {
        e();
        int i = this.g0;
        float[] fArr = this.f0;
        if (i == fArr.length) {
            float[] fArr2 = new float[((i * 3) / 2) + 1];
            System.arraycopy(fArr, 0, fArr2, 0, i);
            this.f0 = fArr2;
        }
        float[] fArr3 = this.f0;
        int i2 = this.g0;
        this.g0 = i2 + 1;
        fArr3[i2] = f;
    }

    public final void n(int i, float f) {
        int i2;
        e();
        if (i >= 0 && i <= (i2 = this.g0)) {
            float[] fArr = this.f0;
            if (i2 < fArr.length) {
                System.arraycopy(fArr, i, fArr, i + 1, i2 - i);
            } else {
                float[] fArr2 = new float[((i2 * 3) / 2) + 1];
                System.arraycopy(fArr, 0, fArr2, 0, i);
                System.arraycopy(this.f0, i, fArr2, i + 1, this.g0 - i);
                this.f0 = fArr2;
            }
            this.f0[i] = f;
            this.g0++;
            ((AbstractList) this).modCount++;
            return;
        }
        throw new IndexOutOfBoundsException(s(i));
    }

    public final void o(int i) {
        if (i < 0 || i >= this.g0) {
            throw new IndexOutOfBoundsException(s(i));
        }
    }

    @Override // java.util.AbstractList, java.util.List
    /* renamed from: p */
    public Float get(int i) {
        return Float.valueOf(q(i));
    }

    public float q(int i) {
        o(i);
        return this.f0[i];
    }

    @Override // java.util.AbstractList
    public void removeRange(int i, int i2) {
        e();
        if (i2 >= i) {
            float[] fArr = this.f0;
            System.arraycopy(fArr, i2, fArr, i, this.g0 - i2);
            this.g0 -= i2 - i;
            ((AbstractList) this).modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    public final String s(int i) {
        return "Index:" + i + ", Size:" + this.g0;
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public int size() {
        return this.g0;
    }

    @Override // com.google.crypto.tink.shaded.protobuf.v.i
    /* renamed from: t */
    public v.f a(int i) {
        if (i >= this.g0) {
            return new s(Arrays.copyOf(this.f0, i), this.g0);
        }
        throw new IllegalArgumentException();
    }

    @Override // java.util.AbstractList, java.util.List
    /* renamed from: w */
    public Float remove(int i) {
        int i2;
        e();
        o(i);
        float[] fArr = this.f0;
        float f = fArr[i];
        if (i < this.g0 - 1) {
            System.arraycopy(fArr, i + 1, fArr, i, (i2 - i) - 1);
        }
        this.g0--;
        ((AbstractList) this).modCount++;
        return Float.valueOf(f);
    }

    @Override // java.util.AbstractList, java.util.List
    /* renamed from: y */
    public Float set(int i, Float f) {
        return Float.valueOf(z(i, f.floatValue()));
    }

    public float z(int i, float f) {
        e();
        o(i);
        float[] fArr = this.f0;
        float f2 = fArr[i];
        fArr[i] = f;
        return f2;
    }

    public s(float[] fArr, int i) {
        this.f0 = fArr;
        this.g0 = i;
    }

    @Override // com.google.crypto.tink.shaded.protobuf.c, java.util.AbstractCollection, java.util.Collection, java.util.List
    public boolean remove(Object obj) {
        e();
        for (int i = 0; i < this.g0; i++) {
            if (obj.equals(Float.valueOf(this.f0[i]))) {
                float[] fArr = this.f0;
                System.arraycopy(fArr, i + 1, fArr, i, (this.g0 - i) - 1);
                this.g0--;
                ((AbstractList) this).modCount++;
                return true;
            }
        }
        return false;
    }
}
