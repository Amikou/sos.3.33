package com.google.crypto.tink.shaded.protobuf;

import java.io.IOException;

/* compiled from: UnknownFieldSetLiteSchema.java */
/* loaded from: classes2.dex */
public class s0 extends q0<r0, r0> {
    @Override // com.google.crypto.tink.shaded.protobuf.q0
    /* renamed from: A */
    public r0 g(Object obj) {
        return ((GeneratedMessageLite) obj).unknownFields;
    }

    @Override // com.google.crypto.tink.shaded.protobuf.q0
    /* renamed from: B */
    public int h(r0 r0Var) {
        return r0Var.f();
    }

    @Override // com.google.crypto.tink.shaded.protobuf.q0
    /* renamed from: C */
    public int i(r0 r0Var) {
        return r0Var.g();
    }

    @Override // com.google.crypto.tink.shaded.protobuf.q0
    /* renamed from: D */
    public r0 k(r0 r0Var, r0 r0Var2) {
        return r0Var2.equals(r0.e()) ? r0Var : r0.k(r0Var, r0Var2);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.q0
    /* renamed from: E */
    public r0 n() {
        return r0.l();
    }

    @Override // com.google.crypto.tink.shaded.protobuf.q0
    /* renamed from: F */
    public void o(Object obj, r0 r0Var) {
        p(obj, r0Var);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.q0
    /* renamed from: G */
    public void p(Object obj, r0 r0Var) {
        ((GeneratedMessageLite) obj).unknownFields = r0Var;
    }

    @Override // com.google.crypto.tink.shaded.protobuf.q0
    /* renamed from: H */
    public r0 r(r0 r0Var) {
        r0Var.j();
        return r0Var;
    }

    @Override // com.google.crypto.tink.shaded.protobuf.q0
    /* renamed from: I */
    public void s(r0 r0Var, Writer writer) throws IOException {
        r0Var.o(writer);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.q0
    /* renamed from: J */
    public void t(r0 r0Var, Writer writer) throws IOException {
        r0Var.q(writer);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.q0
    public void j(Object obj) {
        g(obj).j();
    }

    @Override // com.google.crypto.tink.shaded.protobuf.q0
    public boolean q(m0 m0Var) {
        return false;
    }

    @Override // com.google.crypto.tink.shaded.protobuf.q0
    /* renamed from: u */
    public void a(r0 r0Var, int i, int i2) {
        r0Var.n(WireFormat.c(i, 5), Integer.valueOf(i2));
    }

    @Override // com.google.crypto.tink.shaded.protobuf.q0
    /* renamed from: v */
    public void b(r0 r0Var, int i, long j) {
        r0Var.n(WireFormat.c(i, 1), Long.valueOf(j));
    }

    @Override // com.google.crypto.tink.shaded.protobuf.q0
    /* renamed from: w */
    public void c(r0 r0Var, int i, r0 r0Var2) {
        r0Var.n(WireFormat.c(i, 3), r0Var2);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.q0
    /* renamed from: x */
    public void d(r0 r0Var, int i, ByteString byteString) {
        r0Var.n(WireFormat.c(i, 2), byteString);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.q0
    /* renamed from: y */
    public void e(r0 r0Var, int i, long j) {
        r0Var.n(WireFormat.c(i, 0), Long.valueOf(j));
    }

    @Override // com.google.crypto.tink.shaded.protobuf.q0
    /* renamed from: z */
    public r0 f(Object obj) {
        r0 g = g(obj);
        if (g == r0.e()) {
            r0 l = r0.l();
            p(obj, l);
            return l;
        }
        return g;
    }
}
