package com.google.crypto.tink.shaded.protobuf;

import com.google.crypto.tink.shaded.protobuf.v;
import java.util.AbstractList;
import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

/* compiled from: LongArrayList.java */
/* loaded from: classes2.dex */
public final class a0 extends c<Long> implements v.h, RandomAccess, vu2 {
    public static final a0 h0;
    public long[] f0;
    public int g0;

    static {
        a0 a0Var = new a0(new long[0], 0);
        h0 = a0Var;
        a0Var.l();
    }

    public a0() {
        this(new long[10], 0);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.c, java.util.AbstractCollection, java.util.Collection, java.util.List
    public boolean addAll(Collection<? extends Long> collection) {
        e();
        v.a(collection);
        if (!(collection instanceof a0)) {
            return super.addAll(collection);
        }
        a0 a0Var = (a0) collection;
        int i = a0Var.g0;
        if (i == 0) {
            return false;
        }
        int i2 = this.g0;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            long[] jArr = this.f0;
            if (i3 > jArr.length) {
                this.f0 = Arrays.copyOf(jArr, i3);
            }
            System.arraycopy(a0Var.f0, 0, this.f0, this.g0, a0Var.g0);
            this.g0 = i3;
            ((AbstractList) this).modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    @Override // com.google.crypto.tink.shaded.protobuf.c, java.util.AbstractList, java.util.Collection, java.util.List
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof a0)) {
            return super.equals(obj);
        }
        a0 a0Var = (a0) obj;
        if (this.g0 != a0Var.g0) {
            return false;
        }
        long[] jArr = a0Var.f0;
        for (int i = 0; i < this.g0; i++) {
            if (this.f0[i] != jArr[i]) {
                return false;
            }
        }
        return true;
    }

    @Override // com.google.crypto.tink.shaded.protobuf.c, java.util.AbstractList, java.util.Collection, java.util.List
    public int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.g0; i2++) {
            i = (i * 31) + v.f(this.f0[i2]);
        }
        return i;
    }

    @Override // java.util.AbstractList, java.util.List
    /* renamed from: i */
    public void add(int i, Long l) {
        m(i, l.longValue());
    }

    @Override // com.google.crypto.tink.shaded.protobuf.c, java.util.AbstractList, java.util.AbstractCollection, java.util.Collection, java.util.List
    /* renamed from: k */
    public boolean add(Long l) {
        n(l.longValue());
        return true;
    }

    public final void m(int i, long j) {
        int i2;
        e();
        if (i >= 0 && i <= (i2 = this.g0)) {
            long[] jArr = this.f0;
            if (i2 < jArr.length) {
                System.arraycopy(jArr, i, jArr, i + 1, i2 - i);
            } else {
                long[] jArr2 = new long[((i2 * 3) / 2) + 1];
                System.arraycopy(jArr, 0, jArr2, 0, i);
                System.arraycopy(this.f0, i, jArr2, i + 1, this.g0 - i);
                this.f0 = jArr2;
            }
            this.f0[i] = j;
            this.g0++;
            ((AbstractList) this).modCount++;
            return;
        }
        throw new IndexOutOfBoundsException(s(i));
    }

    public void n(long j) {
        e();
        int i = this.g0;
        long[] jArr = this.f0;
        if (i == jArr.length) {
            long[] jArr2 = new long[((i * 3) / 2) + 1];
            System.arraycopy(jArr, 0, jArr2, 0, i);
            this.f0 = jArr2;
        }
        long[] jArr3 = this.f0;
        int i2 = this.g0;
        this.g0 = i2 + 1;
        jArr3[i2] = j;
    }

    public final void o(int i) {
        if (i < 0 || i >= this.g0) {
            throw new IndexOutOfBoundsException(s(i));
        }
    }

    @Override // java.util.AbstractList, java.util.List
    /* renamed from: p */
    public Long get(int i) {
        return Long.valueOf(q(i));
    }

    public long q(int i) {
        o(i);
        return this.f0[i];
    }

    @Override // java.util.AbstractList
    public void removeRange(int i, int i2) {
        e();
        if (i2 >= i) {
            long[] jArr = this.f0;
            System.arraycopy(jArr, i2, jArr, i, this.g0 - i2);
            this.g0 -= i2 - i;
            ((AbstractList) this).modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    public final String s(int i) {
        return "Index:" + i + ", Size:" + this.g0;
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public int size() {
        return this.g0;
    }

    @Override // com.google.crypto.tink.shaded.protobuf.v.i
    /* renamed from: t */
    public v.h a(int i) {
        if (i >= this.g0) {
            return new a0(Arrays.copyOf(this.f0, i), this.g0);
        }
        throw new IllegalArgumentException();
    }

    @Override // java.util.AbstractList, java.util.List
    /* renamed from: w */
    public Long remove(int i) {
        int i2;
        e();
        o(i);
        long[] jArr = this.f0;
        long j = jArr[i];
        if (i < this.g0 - 1) {
            System.arraycopy(jArr, i + 1, jArr, i, (i2 - i) - 1);
        }
        this.g0--;
        ((AbstractList) this).modCount++;
        return Long.valueOf(j);
    }

    @Override // java.util.AbstractList, java.util.List
    /* renamed from: y */
    public Long set(int i, Long l) {
        return Long.valueOf(z(i, l.longValue()));
    }

    public long z(int i, long j) {
        e();
        o(i);
        long[] jArr = this.f0;
        long j2 = jArr[i];
        jArr[i] = j;
        return j2;
    }

    public a0(long[] jArr, int i) {
        this.f0 = jArr;
        this.g0 = i;
    }

    @Override // com.google.crypto.tink.shaded.protobuf.c, java.util.AbstractCollection, java.util.Collection, java.util.List
    public boolean remove(Object obj) {
        e();
        for (int i = 0; i < this.g0; i++) {
            if (obj.equals(Long.valueOf(this.f0[i]))) {
                long[] jArr = this.f0;
                System.arraycopy(jArr, i + 1, jArr, i, (this.g0 - i) - 1);
                this.g0--;
                ((AbstractList) this).modCount++;
                return true;
            }
        }
        return false;
    }
}
