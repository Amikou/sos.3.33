package com.google.crypto.tink.shaded.protobuf;

import java.io.IOException;

/* compiled from: MapEntryLite.java */
/* loaded from: classes2.dex */
public class b0<K, V> {
    public final a<K, V> a;

    /* compiled from: MapEntryLite.java */
    /* loaded from: classes2.dex */
    public static class a<K, V> {
    }

    public static <K, V> int b(a<K, V> aVar, K k, V v) {
        throw null;
    }

    public static <K, V> void d(CodedOutputStream codedOutputStream, a<K, V> aVar, K k, V v) throws IOException {
        throw null;
    }

    public int a(int i, K k, V v) {
        return CodedOutputStream.V(i) + CodedOutputStream.D(b(this.a, k, v));
    }

    public a<K, V> c() {
        return this.a;
    }
}
