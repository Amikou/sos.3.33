package com.google.crypto.tink.shaded.protobuf;

import com.google.crypto.tink.shaded.protobuf.b0;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/* compiled from: CodedInputStreamReader.java */
/* loaded from: classes2.dex */
public final class j implements m0 {
    public final i a;
    public int b;
    public int c;
    public int d = 0;

    public j(i iVar) {
        i iVar2 = (i) v.b(iVar, "input");
        this.a = iVar2;
        iVar2.d = this;
    }

    public static j Q(i iVar) {
        j jVar = iVar.d;
        return jVar != null ? jVar : new j(iVar);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.m0
    public void A(List<Float> list) throws IOException {
        int C;
        int C2;
        if (list instanceof s) {
            s sVar = (s) list;
            int b = WireFormat.b(this.b);
            if (b == 2) {
                int D = this.a.D();
                W(D);
                int d = this.a.d() + D;
                do {
                    sVar.m(this.a.t());
                } while (this.a.d() < d);
                return;
            } else if (b == 5) {
                do {
                    sVar.m(this.a.t());
                    if (this.a.e()) {
                        return;
                    }
                    C2 = this.a.C();
                } while (C2 == this.b);
                this.d = C2;
                return;
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }
        int b2 = WireFormat.b(this.b);
        if (b2 == 2) {
            int D2 = this.a.D();
            W(D2);
            int d2 = this.a.d() + D2;
            do {
                list.add(Float.valueOf(this.a.t()));
            } while (this.a.d() < d2);
        } else if (b2 == 5) {
            do {
                list.add(Float.valueOf(this.a.t()));
                if (this.a.e()) {
                    return;
                }
                C = this.a.C();
            } while (C == this.b);
            this.d = C;
        } else {
            throw InvalidProtocolBufferException.invalidWireType();
        }
    }

    @Override // com.google.crypto.tink.shaded.protobuf.m0
    public int B() throws IOException {
        V(0);
        return this.a.u();
    }

    @Override // com.google.crypto.tink.shaded.protobuf.m0
    public boolean C() throws IOException {
        int i;
        if (this.a.e() || (i = this.b) == this.c) {
            return false;
        }
        return this.a.F(i);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.m0
    public int D() throws IOException {
        V(5);
        return this.a.w();
    }

    @Override // com.google.crypto.tink.shaded.protobuf.m0
    public void E(List<ByteString> list) throws IOException {
        int C;
        if (WireFormat.b(this.b) == 2) {
            do {
                list.add(z());
                if (this.a.e()) {
                    return;
                }
                C = this.a.C();
            } while (C == this.b);
            this.d = C;
            return;
        }
        throw InvalidProtocolBufferException.invalidWireType();
    }

    @Override // com.google.crypto.tink.shaded.protobuf.m0
    public void F(List<Double> list) throws IOException {
        int C;
        int C2;
        if (list instanceof l) {
            l lVar = (l) list;
            int b = WireFormat.b(this.b);
            if (b == 1) {
                do {
                    lVar.m(this.a.p());
                    if (this.a.e()) {
                        return;
                    }
                    C2 = this.a.C();
                } while (C2 == this.b);
                this.d = C2;
                return;
            } else if (b == 2) {
                int D = this.a.D();
                X(D);
                int d = this.a.d() + D;
                do {
                    lVar.m(this.a.p());
                } while (this.a.d() < d);
                return;
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }
        int b2 = WireFormat.b(this.b);
        if (b2 == 1) {
            do {
                list.add(Double.valueOf(this.a.p()));
                if (this.a.e()) {
                    return;
                }
                C = this.a.C();
            } while (C == this.b);
            this.d = C;
        } else if (b2 == 2) {
            int D2 = this.a.D();
            X(D2);
            int d2 = this.a.d() + D2;
            do {
                list.add(Double.valueOf(this.a.p()));
            } while (this.a.d() < d2);
        } else {
            throw InvalidProtocolBufferException.invalidWireType();
        }
    }

    @Override // com.google.crypto.tink.shaded.protobuf.m0
    public long G() throws IOException {
        V(0);
        return this.a.v();
    }

    @Override // com.google.crypto.tink.shaded.protobuf.m0
    public String H() throws IOException {
        V(2);
        return this.a.B();
    }

    @Override // com.google.crypto.tink.shaded.protobuf.m0
    public void I(List<Long> list) throws IOException {
        int C;
        int C2;
        if (list instanceof a0) {
            a0 a0Var = (a0) list;
            int b = WireFormat.b(this.b);
            if (b == 1) {
                do {
                    a0Var.n(this.a.s());
                    if (this.a.e()) {
                        return;
                    }
                    C2 = this.a.C();
                } while (C2 == this.b);
                this.d = C2;
                return;
            } else if (b == 2) {
                int D = this.a.D();
                X(D);
                int d = this.a.d() + D;
                do {
                    a0Var.n(this.a.s());
                } while (this.a.d() < d);
                return;
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }
        int b2 = WireFormat.b(this.b);
        if (b2 == 1) {
            do {
                list.add(Long.valueOf(this.a.s()));
                if (this.a.e()) {
                    return;
                }
                C = this.a.C();
            } while (C == this.b);
            this.d = C;
        } else if (b2 == 2) {
            int D2 = this.a.D();
            X(D2);
            int d2 = this.a.d() + D2;
            do {
                list.add(Long.valueOf(this.a.s()));
            } while (this.a.d() < d2);
        } else {
            throw InvalidProtocolBufferException.invalidWireType();
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.google.crypto.tink.shaded.protobuf.m0
    public <T> void J(List<T> list, n0<T> n0Var, n nVar) throws IOException {
        int C;
        if (WireFormat.b(this.b) == 2) {
            int i = this.b;
            do {
                list.add(S(n0Var, nVar));
                if (this.a.e() || this.d != 0) {
                    return;
                }
                C = this.a.C();
            } while (C == i);
            this.d = C;
            return;
        }
        throw InvalidProtocolBufferException.invalidWireType();
    }

    @Override // com.google.crypto.tink.shaded.protobuf.m0
    public <T> T K(n0<T> n0Var, n nVar) throws IOException {
        V(3);
        return (T) R(n0Var, nVar);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.m0
    public <T> T L(n0<T> n0Var, n nVar) throws IOException {
        V(2);
        return (T) S(n0Var, nVar);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.m0
    public <T> T M(Class<T> cls, n nVar) throws IOException {
        V(3);
        return (T) R(k0.a().d(cls), nVar);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.m0
    public <T> T N(Class<T> cls, n nVar) throws IOException {
        V(2);
        return (T) S(k0.a().d(cls), nVar);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.m0
    public <K, V> void O(Map<K, V> map, b0.a<K, V> aVar, n nVar) throws IOException {
        V(2);
        this.a.m(this.a.D());
        throw null;
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.google.crypto.tink.shaded.protobuf.m0
    public <T> void P(List<T> list, n0<T> n0Var, n nVar) throws IOException {
        int C;
        if (WireFormat.b(this.b) == 3) {
            int i = this.b;
            do {
                list.add(R(n0Var, nVar));
                if (this.a.e() || this.d != 0) {
                    return;
                }
                C = this.a.C();
            } while (C == i);
            this.d = C;
            return;
        }
        throw InvalidProtocolBufferException.invalidWireType();
    }

    public final <T> T R(n0<T> n0Var, n nVar) throws IOException {
        int i = this.c;
        this.c = WireFormat.c(WireFormat.a(this.b), 4);
        try {
            T c = n0Var.c();
            n0Var.j(c, this, nVar);
            n0Var.e(c);
            if (this.b == this.c) {
                return c;
            }
            throw InvalidProtocolBufferException.parseFailure();
        } finally {
            this.c = i;
        }
    }

    public final <T> T S(n0<T> n0Var, n nVar) throws IOException {
        i iVar;
        int D = this.a.D();
        i iVar2 = this.a;
        if (iVar2.a < iVar2.b) {
            int m = iVar2.m(D);
            T c = n0Var.c();
            this.a.a++;
            n0Var.j(c, this, nVar);
            n0Var.e(c);
            this.a.a(0);
            iVar.a--;
            this.a.l(m);
            return c;
        }
        throw InvalidProtocolBufferException.recursionLimitExceeded();
    }

    public void T(List<String> list, boolean z) throws IOException {
        int C;
        int C2;
        if (WireFormat.b(this.b) == 2) {
            if ((list instanceof dz1) && !z) {
                dz1 dz1Var = (dz1) list;
                do {
                    dz1Var.i1(z());
                    if (this.a.e()) {
                        return;
                    }
                    C2 = this.a.C();
                } while (C2 == this.b);
                this.d = C2;
                return;
            }
            do {
                list.add(z ? H() : v());
                if (this.a.e()) {
                    return;
                }
                C = this.a.C();
            } while (C == this.b);
            this.d = C;
            return;
        }
        throw InvalidProtocolBufferException.invalidWireType();
    }

    public final void U(int i) throws IOException {
        if (this.a.d() != i) {
            throw InvalidProtocolBufferException.truncatedMessage();
        }
    }

    public final void V(int i) throws IOException {
        if (WireFormat.b(this.b) != i) {
            throw InvalidProtocolBufferException.invalidWireType();
        }
    }

    public final void W(int i) throws IOException {
        if ((i & 3) != 0) {
            throw InvalidProtocolBufferException.parseFailure();
        }
    }

    public final void X(int i) throws IOException {
        if ((i & 7) != 0) {
            throw InvalidProtocolBufferException.parseFailure();
        }
    }

    @Override // com.google.crypto.tink.shaded.protobuf.m0
    public int a() {
        return this.b;
    }

    @Override // com.google.crypto.tink.shaded.protobuf.m0
    public void b(List<Integer> list) throws IOException {
        int C;
        int C2;
        if (list instanceof u) {
            u uVar = (u) list;
            int b = WireFormat.b(this.b);
            if (b == 0) {
                do {
                    uVar.X(this.a.y());
                    if (this.a.e()) {
                        return;
                    }
                    C2 = this.a.C();
                } while (C2 == this.b);
                this.d = C2;
                return;
            } else if (b == 2) {
                int d = this.a.d() + this.a.D();
                do {
                    uVar.X(this.a.y());
                } while (this.a.d() < d);
                U(d);
                return;
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }
        int b2 = WireFormat.b(this.b);
        if (b2 == 0) {
            do {
                list.add(Integer.valueOf(this.a.y()));
                if (this.a.e()) {
                    return;
                }
                C = this.a.C();
            } while (C == this.b);
            this.d = C;
        } else if (b2 == 2) {
            int d2 = this.a.d() + this.a.D();
            do {
                list.add(Integer.valueOf(this.a.y()));
            } while (this.a.d() < d2);
            U(d2);
        } else {
            throw InvalidProtocolBufferException.invalidWireType();
        }
    }

    @Override // com.google.crypto.tink.shaded.protobuf.m0
    public long c() throws IOException {
        V(0);
        return this.a.E();
    }

    @Override // com.google.crypto.tink.shaded.protobuf.m0
    public long d() throws IOException {
        V(1);
        return this.a.s();
    }

    @Override // com.google.crypto.tink.shaded.protobuf.m0
    public void e(List<Integer> list) throws IOException {
        int C;
        int C2;
        if (list instanceof u) {
            u uVar = (u) list;
            int b = WireFormat.b(this.b);
            if (b == 2) {
                int D = this.a.D();
                W(D);
                int d = this.a.d() + D;
                do {
                    uVar.X(this.a.w());
                } while (this.a.d() < d);
                return;
            } else if (b == 5) {
                do {
                    uVar.X(this.a.w());
                    if (this.a.e()) {
                        return;
                    }
                    C2 = this.a.C();
                } while (C2 == this.b);
                this.d = C2;
                return;
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }
        int b2 = WireFormat.b(this.b);
        if (b2 == 2) {
            int D2 = this.a.D();
            W(D2);
            int d2 = this.a.d() + D2;
            do {
                list.add(Integer.valueOf(this.a.w()));
            } while (this.a.d() < d2);
        } else if (b2 == 5) {
            do {
                list.add(Integer.valueOf(this.a.w()));
                if (this.a.e()) {
                    return;
                }
                C = this.a.C();
            } while (C == this.b);
            this.d = C;
        } else {
            throw InvalidProtocolBufferException.invalidWireType();
        }
    }

    @Override // com.google.crypto.tink.shaded.protobuf.m0
    public void f(List<Long> list) throws IOException {
        int C;
        int C2;
        if (list instanceof a0) {
            a0 a0Var = (a0) list;
            int b = WireFormat.b(this.b);
            if (b == 0) {
                do {
                    a0Var.n(this.a.z());
                    if (this.a.e()) {
                        return;
                    }
                    C2 = this.a.C();
                } while (C2 == this.b);
                this.d = C2;
                return;
            } else if (b == 2) {
                int d = this.a.d() + this.a.D();
                do {
                    a0Var.n(this.a.z());
                } while (this.a.d() < d);
                U(d);
                return;
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }
        int b2 = WireFormat.b(this.b);
        if (b2 == 0) {
            do {
                list.add(Long.valueOf(this.a.z()));
                if (this.a.e()) {
                    return;
                }
                C = this.a.C();
            } while (C == this.b);
            this.d = C;
        } else if (b2 == 2) {
            int d2 = this.a.d() + this.a.D();
            do {
                list.add(Long.valueOf(this.a.z()));
            } while (this.a.d() < d2);
            U(d2);
        } else {
            throw InvalidProtocolBufferException.invalidWireType();
        }
    }

    @Override // com.google.crypto.tink.shaded.protobuf.m0
    public void g(List<Integer> list) throws IOException {
        int C;
        int C2;
        if (list instanceof u) {
            u uVar = (u) list;
            int b = WireFormat.b(this.b);
            if (b == 0) {
                do {
                    uVar.X(this.a.D());
                    if (this.a.e()) {
                        return;
                    }
                    C2 = this.a.C();
                } while (C2 == this.b);
                this.d = C2;
                return;
            } else if (b == 2) {
                int d = this.a.d() + this.a.D();
                do {
                    uVar.X(this.a.D());
                } while (this.a.d() < d);
                U(d);
                return;
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }
        int b2 = WireFormat.b(this.b);
        if (b2 == 0) {
            do {
                list.add(Integer.valueOf(this.a.D()));
                if (this.a.e()) {
                    return;
                }
                C = this.a.C();
            } while (C == this.b);
            this.d = C;
        } else if (b2 == 2) {
            int d2 = this.a.d() + this.a.D();
            do {
                list.add(Integer.valueOf(this.a.D()));
            } while (this.a.d() < d2);
            U(d2);
        } else {
            throw InvalidProtocolBufferException.invalidWireType();
        }
    }

    @Override // com.google.crypto.tink.shaded.protobuf.m0
    public int h() throws IOException {
        V(5);
        return this.a.r();
    }

    @Override // com.google.crypto.tink.shaded.protobuf.m0
    public boolean i() throws IOException {
        V(0);
        return this.a.n();
    }

    @Override // com.google.crypto.tink.shaded.protobuf.m0
    public long j() throws IOException {
        V(1);
        return this.a.x();
    }

    @Override // com.google.crypto.tink.shaded.protobuf.m0
    public void k(List<Long> list) throws IOException {
        int C;
        int C2;
        if (list instanceof a0) {
            a0 a0Var = (a0) list;
            int b = WireFormat.b(this.b);
            if (b == 0) {
                do {
                    a0Var.n(this.a.E());
                    if (this.a.e()) {
                        return;
                    }
                    C2 = this.a.C();
                } while (C2 == this.b);
                this.d = C2;
                return;
            } else if (b == 2) {
                int d = this.a.d() + this.a.D();
                do {
                    a0Var.n(this.a.E());
                } while (this.a.d() < d);
                U(d);
                return;
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }
        int b2 = WireFormat.b(this.b);
        if (b2 == 0) {
            do {
                list.add(Long.valueOf(this.a.E()));
                if (this.a.e()) {
                    return;
                }
                C = this.a.C();
            } while (C == this.b);
            this.d = C;
        } else if (b2 == 2) {
            int d2 = this.a.d() + this.a.D();
            do {
                list.add(Long.valueOf(this.a.E()));
            } while (this.a.d() < d2);
            U(d2);
        } else {
            throw InvalidProtocolBufferException.invalidWireType();
        }
    }

    @Override // com.google.crypto.tink.shaded.protobuf.m0
    public int l() throws IOException {
        V(0);
        return this.a.D();
    }

    @Override // com.google.crypto.tink.shaded.protobuf.m0
    public void m(List<Long> list) throws IOException {
        int C;
        int C2;
        if (list instanceof a0) {
            a0 a0Var = (a0) list;
            int b = WireFormat.b(this.b);
            if (b == 0) {
                do {
                    a0Var.n(this.a.v());
                    if (this.a.e()) {
                        return;
                    }
                    C2 = this.a.C();
                } while (C2 == this.b);
                this.d = C2;
                return;
            } else if (b == 2) {
                int d = this.a.d() + this.a.D();
                do {
                    a0Var.n(this.a.v());
                } while (this.a.d() < d);
                U(d);
                return;
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }
        int b2 = WireFormat.b(this.b);
        if (b2 == 0) {
            do {
                list.add(Long.valueOf(this.a.v()));
                if (this.a.e()) {
                    return;
                }
                C = this.a.C();
            } while (C == this.b);
            this.d = C;
        } else if (b2 == 2) {
            int d2 = this.a.d() + this.a.D();
            do {
                list.add(Long.valueOf(this.a.v()));
            } while (this.a.d() < d2);
            U(d2);
        } else {
            throw InvalidProtocolBufferException.invalidWireType();
        }
    }

    @Override // com.google.crypto.tink.shaded.protobuf.m0
    public void n(List<Long> list) throws IOException {
        int C;
        int C2;
        if (list instanceof a0) {
            a0 a0Var = (a0) list;
            int b = WireFormat.b(this.b);
            if (b == 1) {
                do {
                    a0Var.n(this.a.x());
                    if (this.a.e()) {
                        return;
                    }
                    C2 = this.a.C();
                } while (C2 == this.b);
                this.d = C2;
                return;
            } else if (b == 2) {
                int D = this.a.D();
                X(D);
                int d = this.a.d() + D;
                do {
                    a0Var.n(this.a.x());
                } while (this.a.d() < d);
                return;
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }
        int b2 = WireFormat.b(this.b);
        if (b2 == 1) {
            do {
                list.add(Long.valueOf(this.a.x()));
                if (this.a.e()) {
                    return;
                }
                C = this.a.C();
            } while (C == this.b);
            this.d = C;
        } else if (b2 == 2) {
            int D2 = this.a.D();
            X(D2);
            int d2 = this.a.d() + D2;
            do {
                list.add(Long.valueOf(this.a.x()));
            } while (this.a.d() < d2);
        } else {
            throw InvalidProtocolBufferException.invalidWireType();
        }
    }

    @Override // com.google.crypto.tink.shaded.protobuf.m0
    public void o(List<Integer> list) throws IOException {
        int C;
        int C2;
        if (list instanceof u) {
            u uVar = (u) list;
            int b = WireFormat.b(this.b);
            if (b == 0) {
                do {
                    uVar.X(this.a.u());
                    if (this.a.e()) {
                        return;
                    }
                    C2 = this.a.C();
                } while (C2 == this.b);
                this.d = C2;
                return;
            } else if (b == 2) {
                int d = this.a.d() + this.a.D();
                do {
                    uVar.X(this.a.u());
                } while (this.a.d() < d);
                U(d);
                return;
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }
        int b2 = WireFormat.b(this.b);
        if (b2 == 0) {
            do {
                list.add(Integer.valueOf(this.a.u()));
                if (this.a.e()) {
                    return;
                }
                C = this.a.C();
            } while (C == this.b);
            this.d = C;
        } else if (b2 == 2) {
            int d2 = this.a.d() + this.a.D();
            do {
                list.add(Integer.valueOf(this.a.u()));
            } while (this.a.d() < d2);
            U(d2);
        } else {
            throw InvalidProtocolBufferException.invalidWireType();
        }
    }

    @Override // com.google.crypto.tink.shaded.protobuf.m0
    public void p(List<Integer> list) throws IOException {
        int C;
        int C2;
        if (list instanceof u) {
            u uVar = (u) list;
            int b = WireFormat.b(this.b);
            if (b == 0) {
                do {
                    uVar.X(this.a.q());
                    if (this.a.e()) {
                        return;
                    }
                    C2 = this.a.C();
                } while (C2 == this.b);
                this.d = C2;
                return;
            } else if (b == 2) {
                int d = this.a.d() + this.a.D();
                do {
                    uVar.X(this.a.q());
                } while (this.a.d() < d);
                U(d);
                return;
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }
        int b2 = WireFormat.b(this.b);
        if (b2 == 0) {
            do {
                list.add(Integer.valueOf(this.a.q()));
                if (this.a.e()) {
                    return;
                }
                C = this.a.C();
            } while (C == this.b);
            this.d = C;
        } else if (b2 == 2) {
            int d2 = this.a.d() + this.a.D();
            do {
                list.add(Integer.valueOf(this.a.q()));
            } while (this.a.d() < d2);
            U(d2);
        } else {
            throw InvalidProtocolBufferException.invalidWireType();
        }
    }

    @Override // com.google.crypto.tink.shaded.protobuf.m0
    public int q() throws IOException {
        V(0);
        return this.a.q();
    }

    @Override // com.google.crypto.tink.shaded.protobuf.m0
    public void r(List<Integer> list) throws IOException {
        int C;
        int C2;
        if (list instanceof u) {
            u uVar = (u) list;
            int b = WireFormat.b(this.b);
            if (b == 2) {
                int D = this.a.D();
                W(D);
                int d = this.a.d() + D;
                do {
                    uVar.X(this.a.r());
                } while (this.a.d() < d);
                return;
            } else if (b == 5) {
                do {
                    uVar.X(this.a.r());
                    if (this.a.e()) {
                        return;
                    }
                    C2 = this.a.C();
                } while (C2 == this.b);
                this.d = C2;
                return;
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }
        int b2 = WireFormat.b(this.b);
        if (b2 == 2) {
            int D2 = this.a.D();
            W(D2);
            int d2 = this.a.d() + D2;
            do {
                list.add(Integer.valueOf(this.a.r()));
            } while (this.a.d() < d2);
        } else if (b2 == 5) {
            do {
                list.add(Integer.valueOf(this.a.r()));
                if (this.a.e()) {
                    return;
                }
                C = this.a.C();
            } while (C == this.b);
            this.d = C;
        } else {
            throw InvalidProtocolBufferException.invalidWireType();
        }
    }

    @Override // com.google.crypto.tink.shaded.protobuf.m0
    public double readDouble() throws IOException {
        V(1);
        return this.a.p();
    }

    @Override // com.google.crypto.tink.shaded.protobuf.m0
    public float readFloat() throws IOException {
        V(5);
        return this.a.t();
    }

    @Override // com.google.crypto.tink.shaded.protobuf.m0
    public int s() throws IOException {
        V(0);
        return this.a.y();
    }

    @Override // com.google.crypto.tink.shaded.protobuf.m0
    public long t() throws IOException {
        V(0);
        return this.a.z();
    }

    @Override // com.google.crypto.tink.shaded.protobuf.m0
    public void u(List<Boolean> list) throws IOException {
        int C;
        int C2;
        if (list instanceof f) {
            f fVar = (f) list;
            int b = WireFormat.b(this.b);
            if (b == 0) {
                do {
                    fVar.n(this.a.n());
                    if (this.a.e()) {
                        return;
                    }
                    C2 = this.a.C();
                } while (C2 == this.b);
                this.d = C2;
                return;
            } else if (b == 2) {
                int d = this.a.d() + this.a.D();
                do {
                    fVar.n(this.a.n());
                } while (this.a.d() < d);
                U(d);
                return;
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }
        int b2 = WireFormat.b(this.b);
        if (b2 == 0) {
            do {
                list.add(Boolean.valueOf(this.a.n()));
                if (this.a.e()) {
                    return;
                }
                C = this.a.C();
            } while (C == this.b);
            this.d = C;
        } else if (b2 == 2) {
            int d2 = this.a.d() + this.a.D();
            do {
                list.add(Boolean.valueOf(this.a.n()));
            } while (this.a.d() < d2);
            U(d2);
        } else {
            throw InvalidProtocolBufferException.invalidWireType();
        }
    }

    @Override // com.google.crypto.tink.shaded.protobuf.m0
    public String v() throws IOException {
        V(2);
        return this.a.A();
    }

    @Override // com.google.crypto.tink.shaded.protobuf.m0
    public int w() throws IOException {
        int i = this.d;
        if (i != 0) {
            this.b = i;
            this.d = 0;
        } else {
            this.b = this.a.C();
        }
        int i2 = this.b;
        if (i2 == 0 || i2 == this.c) {
            return Integer.MAX_VALUE;
        }
        return WireFormat.a(i2);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.m0
    public void x(List<String> list) throws IOException {
        T(list, false);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.m0
    public void y(List<String> list) throws IOException {
        T(list, true);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.m0
    public ByteString z() throws IOException {
        V(2);
        return this.a.o();
    }
}
