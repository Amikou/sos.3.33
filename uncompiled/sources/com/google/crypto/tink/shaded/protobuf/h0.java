package com.google.crypto.tink.shaded.protobuf;

import com.google.crypto.tink.shaded.protobuf.WireFormat;
import com.google.crypto.tink.shaded.protobuf.r;
import com.google.crypto.tink.shaded.protobuf.w;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

/* compiled from: MessageSetSchema.java */
/* loaded from: classes2.dex */
public final class h0<T> implements n0<T> {
    public final e0 a;
    public final q0<?, ?> b;
    public final boolean c;
    public final o<?> d;

    public h0(q0<?, ?> q0Var, o<?> oVar, e0 e0Var) {
        this.b = q0Var;
        this.c = oVar.e(e0Var);
        this.d = oVar;
        this.a = e0Var;
    }

    public static <T> h0<T> m(q0<?, ?> q0Var, o<?> oVar, e0 e0Var) {
        return new h0<>(q0Var, oVar, e0Var);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.n0
    public void a(T t, T t2) {
        o0.G(this.b, t, t2);
        if (this.c) {
            o0.E(this.d, t, t2);
        }
    }

    @Override // com.google.crypto.tink.shaded.protobuf.n0
    public boolean b(T t, T t2) {
        if (this.b.g(t).equals(this.b.g(t2))) {
            if (this.c) {
                return this.d.c(t).equals(this.d.c(t2));
            }
            return true;
        }
        return false;
    }

    @Override // com.google.crypto.tink.shaded.protobuf.n0
    public T c() {
        return (T) this.a.newBuilderForType().buildPartial();
    }

    @Override // com.google.crypto.tink.shaded.protobuf.n0
    public int d(T t) {
        int hashCode = this.b.g(t).hashCode();
        return this.c ? (hashCode * 53) + this.d.c(t).hashCode() : hashCode;
    }

    @Override // com.google.crypto.tink.shaded.protobuf.n0
    public void e(T t) {
        this.b.j(t);
        this.d.f(t);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.n0
    public final boolean f(T t) {
        return this.d.c(t).o();
    }

    @Override // com.google.crypto.tink.shaded.protobuf.n0
    public int g(T t) {
        int k = k(this.b, t) + 0;
        return this.c ? k + this.d.c(t).j() : k;
    }

    /* JADX WARN: Removed duplicated region for block: B:33:0x00c6  */
    /* JADX WARN: Removed duplicated region for block: B:57:0x00cb A[EDGE_INSN: B:57:0x00cb->B:34:0x00cb ?: BREAK  , SYNTHETIC] */
    @Override // com.google.crypto.tink.shaded.protobuf.n0
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void h(T r11, byte[] r12, int r13, int r14, com.google.crypto.tink.shaded.protobuf.d.b r15) throws java.io.IOException {
        /*
            r10 = this;
            r0 = r11
            com.google.crypto.tink.shaded.protobuf.GeneratedMessageLite r0 = (com.google.crypto.tink.shaded.protobuf.GeneratedMessageLite) r0
            com.google.crypto.tink.shaded.protobuf.r0 r1 = r0.unknownFields
            com.google.crypto.tink.shaded.protobuf.r0 r2 = com.google.crypto.tink.shaded.protobuf.r0.e()
            if (r1 != r2) goto L11
            com.google.crypto.tink.shaded.protobuf.r0 r1 = com.google.crypto.tink.shaded.protobuf.r0.l()
            r0.unknownFields = r1
        L11:
            com.google.crypto.tink.shaded.protobuf.GeneratedMessageLite$c r11 = (com.google.crypto.tink.shaded.protobuf.GeneratedMessageLite.c) r11
            com.google.crypto.tink.shaded.protobuf.r r11 = r11.C()
            r0 = 0
            r2 = r0
        L19:
            if (r13 >= r14) goto Ld7
            int r4 = com.google.crypto.tink.shaded.protobuf.d.I(r12, r13, r15)
            int r13 = r15.a
            int r3 = com.google.crypto.tink.shaded.protobuf.WireFormat.a
            r5 = 2
            if (r13 == r3) goto L6b
            int r3 = com.google.crypto.tink.shaded.protobuf.WireFormat.b(r13)
            if (r3 != r5) goto L66
            com.google.crypto.tink.shaded.protobuf.o<?> r2 = r10.d
            com.google.crypto.tink.shaded.protobuf.n r3 = r15.d
            com.google.crypto.tink.shaded.protobuf.e0 r5 = r10.a
            int r6 = com.google.crypto.tink.shaded.protobuf.WireFormat.a(r13)
            java.lang.Object r2 = r2.b(r3, r5, r6)
            r8 = r2
            com.google.crypto.tink.shaded.protobuf.GeneratedMessageLite$e r8 = (com.google.crypto.tink.shaded.protobuf.GeneratedMessageLite.e) r8
            if (r8 == 0) goto L5b
            com.google.crypto.tink.shaded.protobuf.k0 r13 = com.google.crypto.tink.shaded.protobuf.k0.a()
            com.google.crypto.tink.shaded.protobuf.e0 r2 = r8.b()
            java.lang.Class r2 = r2.getClass()
            com.google.crypto.tink.shaded.protobuf.n0 r13 = r13.d(r2)
            int r13 = com.google.crypto.tink.shaded.protobuf.d.p(r13, r12, r4, r14, r15)
            com.google.crypto.tink.shaded.protobuf.GeneratedMessageLite$d r2 = r8.b
            java.lang.Object r3 = r15.c
            r11.w(r2, r3)
            goto L64
        L5b:
            r2 = r13
            r3 = r12
            r5 = r14
            r6 = r1
            r7 = r15
            int r13 = com.google.crypto.tink.shaded.protobuf.d.G(r2, r3, r4, r5, r6, r7)
        L64:
            r2 = r8
            goto L19
        L66:
            int r13 = com.google.crypto.tink.shaded.protobuf.d.N(r13, r12, r4, r14, r15)
            goto L19
        L6b:
            r13 = 0
            r3 = r0
        L6d:
            if (r4 >= r14) goto Lcb
            int r4 = com.google.crypto.tink.shaded.protobuf.d.I(r12, r4, r15)
            int r6 = r15.a
            int r7 = com.google.crypto.tink.shaded.protobuf.WireFormat.a(r6)
            int r8 = com.google.crypto.tink.shaded.protobuf.WireFormat.b(r6)
            if (r7 == r5) goto Lac
            r9 = 3
            if (r7 == r9) goto L83
            goto Lc1
        L83:
            if (r2 == 0) goto La1
            com.google.crypto.tink.shaded.protobuf.k0 r6 = com.google.crypto.tink.shaded.protobuf.k0.a()
            com.google.crypto.tink.shaded.protobuf.e0 r7 = r2.b()
            java.lang.Class r7 = r7.getClass()
            com.google.crypto.tink.shaded.protobuf.n0 r6 = r6.d(r7)
            int r4 = com.google.crypto.tink.shaded.protobuf.d.p(r6, r12, r4, r14, r15)
            com.google.crypto.tink.shaded.protobuf.GeneratedMessageLite$d r6 = r2.b
            java.lang.Object r7 = r15.c
            r11.w(r6, r7)
            goto L6d
        La1:
            if (r8 != r5) goto Lc1
            int r4 = com.google.crypto.tink.shaded.protobuf.d.b(r12, r4, r15)
            java.lang.Object r3 = r15.c
            com.google.crypto.tink.shaded.protobuf.ByteString r3 = (com.google.crypto.tink.shaded.protobuf.ByteString) r3
            goto L6d
        Lac:
            if (r8 != 0) goto Lc1
            int r4 = com.google.crypto.tink.shaded.protobuf.d.I(r12, r4, r15)
            int r13 = r15.a
            com.google.crypto.tink.shaded.protobuf.o<?> r2 = r10.d
            com.google.crypto.tink.shaded.protobuf.n r6 = r15.d
            com.google.crypto.tink.shaded.protobuf.e0 r7 = r10.a
            java.lang.Object r2 = r2.b(r6, r7, r13)
            com.google.crypto.tink.shaded.protobuf.GeneratedMessageLite$e r2 = (com.google.crypto.tink.shaded.protobuf.GeneratedMessageLite.e) r2
            goto L6d
        Lc1:
            int r7 = com.google.crypto.tink.shaded.protobuf.WireFormat.b
            if (r6 != r7) goto Lc6
            goto Lcb
        Lc6:
            int r4 = com.google.crypto.tink.shaded.protobuf.d.N(r6, r12, r4, r14, r15)
            goto L6d
        Lcb:
            if (r3 == 0) goto Ld4
            int r13 = com.google.crypto.tink.shaded.protobuf.WireFormat.c(r13, r5)
            r1.n(r13, r3)
        Ld4:
            r13 = r4
            goto L19
        Ld7:
            if (r13 != r14) goto Lda
            return
        Lda:
            com.google.crypto.tink.shaded.protobuf.InvalidProtocolBufferException r11 = com.google.crypto.tink.shaded.protobuf.InvalidProtocolBufferException.parseFailure()
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.crypto.tink.shaded.protobuf.h0.h(java.lang.Object, byte[], int, int, com.google.crypto.tink.shaded.protobuf.d$b):void");
    }

    @Override // com.google.crypto.tink.shaded.protobuf.n0
    public void i(T t, Writer writer) throws IOException {
        Iterator<Map.Entry<?, Object>> r = this.d.c(t).r();
        while (r.hasNext()) {
            Map.Entry<?, Object> next = r.next();
            r.b bVar = (r.b) next.getKey();
            if (bVar.z() == WireFormat.JavaType.MESSAGE && !bVar.i() && !bVar.isPacked()) {
                if (next instanceof w.b) {
                    writer.c(bVar.getNumber(), ((w.b) next).a().e());
                } else {
                    writer.c(bVar.getNumber(), next.getValue());
                }
            } else {
                throw new IllegalStateException("Found invalid MessageSet item.");
            }
        }
        o(this.b, t, writer);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.n0
    public void j(T t, m0 m0Var, n nVar) throws IOException {
        l(this.b, this.d, t, m0Var, nVar);
    }

    public final <UT, UB> int k(q0<UT, UB> q0Var, T t) {
        return q0Var.i(q0Var.g(t));
    }

    public final <UT, UB, ET extends r.b<ET>> void l(q0<UT, UB> q0Var, o<ET> oVar, T t, m0 m0Var, n nVar) throws IOException {
        UB f = q0Var.f(t);
        r<ET> d = oVar.d(t);
        do {
            try {
                if (m0Var.w() == Integer.MAX_VALUE) {
                    return;
                }
            } finally {
                q0Var.o(t, f);
            }
        } while (n(m0Var, nVar, oVar, d, q0Var, f));
    }

    public final <UT, UB, ET extends r.b<ET>> boolean n(m0 m0Var, n nVar, o<ET> oVar, r<ET> rVar, q0<UT, UB> q0Var, UB ub) throws IOException {
        int a = m0Var.a();
        if (a != WireFormat.a) {
            if (WireFormat.b(a) == 2) {
                Object b = oVar.b(nVar, this.a, WireFormat.a(a));
                if (b != null) {
                    oVar.h(m0Var, b, nVar, rVar);
                    return true;
                }
                return q0Var.m(ub, m0Var);
            }
            return m0Var.C();
        }
        int i = 0;
        Object obj = null;
        ByteString byteString = null;
        while (m0Var.w() != Integer.MAX_VALUE) {
            int a2 = m0Var.a();
            if (a2 == WireFormat.c) {
                i = m0Var.l();
                obj = oVar.b(nVar, this.a, i);
            } else if (a2 == WireFormat.d) {
                if (obj != null) {
                    oVar.h(m0Var, obj, nVar, rVar);
                } else {
                    byteString = m0Var.z();
                }
            } else if (!m0Var.C()) {
                break;
            }
        }
        if (m0Var.a() == WireFormat.b) {
            if (byteString != null) {
                if (obj != null) {
                    oVar.i(byteString, obj, nVar, rVar);
                } else {
                    q0Var.d(ub, i, byteString);
                }
            }
            return true;
        }
        throw InvalidProtocolBufferException.invalidEndTag();
    }

    public final <UT, UB> void o(q0<UT, UB> q0Var, T t, Writer writer) throws IOException {
        q0Var.s(q0Var.g(t), writer);
    }
}
