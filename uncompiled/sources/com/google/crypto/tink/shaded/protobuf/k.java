package com.google.crypto.tink.shaded.protobuf;

import com.google.crypto.tink.shaded.protobuf.WireFormat;
import com.google.crypto.tink.shaded.protobuf.Writer;
import com.google.crypto.tink.shaded.protobuf.b0;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/* compiled from: CodedOutputStreamWriter.java */
/* loaded from: classes2.dex */
public final class k implements Writer {
    public final CodedOutputStream a;

    /* compiled from: CodedOutputStreamWriter.java */
    /* loaded from: classes2.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[WireFormat.FieldType.values().length];
            a = iArr;
            try {
                iArr[WireFormat.FieldType.BOOL.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[WireFormat.FieldType.FIXED32.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[WireFormat.FieldType.INT32.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                a[WireFormat.FieldType.SFIXED32.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                a[WireFormat.FieldType.SINT32.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                a[WireFormat.FieldType.UINT32.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                a[WireFormat.FieldType.FIXED64.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                a[WireFormat.FieldType.INT64.ordinal()] = 8;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                a[WireFormat.FieldType.SFIXED64.ordinal()] = 9;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                a[WireFormat.FieldType.SINT64.ordinal()] = 10;
            } catch (NoSuchFieldError unused10) {
            }
            try {
                a[WireFormat.FieldType.UINT64.ordinal()] = 11;
            } catch (NoSuchFieldError unused11) {
            }
            try {
                a[WireFormat.FieldType.STRING.ordinal()] = 12;
            } catch (NoSuchFieldError unused12) {
            }
        }
    }

    public k(CodedOutputStream codedOutputStream) {
        CodedOutputStream codedOutputStream2 = (CodedOutputStream) v.b(codedOutputStream, "output");
        this.a = codedOutputStream2;
        codedOutputStream2.a = this;
    }

    public static k P(CodedOutputStream codedOutputStream) {
        k kVar = codedOutputStream.a;
        return kVar != null ? kVar : new k(codedOutputStream);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.Writer
    public void A(int i, float f) throws IOException {
        this.a.t0(i, f);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.Writer
    public void B(int i) throws IOException {
        this.a.N0(i, 4);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.Writer
    public void C(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.N0(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += CodedOutputStream.Q(list.get(i4).intValue());
            }
            this.a.P0(i3);
            while (i2 < list.size()) {
                this.a.J0(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.I0(i, list.get(i2).intValue());
            i2++;
        }
    }

    @Override // com.google.crypto.tink.shaded.protobuf.Writer
    public void D(int i, int i2) throws IOException {
        this.a.n0(i, i2);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.Writer
    public void E(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.N0(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += CodedOutputStream.z(list.get(i4).longValue());
            }
            this.a.P0(i3);
            while (i2 < list.size()) {
                this.a.A0(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.z0(i, list.get(i2).longValue());
            i2++;
        }
    }

    @Override // com.google.crypto.tink.shaded.protobuf.Writer
    public void F(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.N0(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += CodedOutputStream.m(list.get(i4).intValue());
            }
            this.a.P0(i3);
            while (i2 < list.size()) {
                this.a.o0(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.n0(i, list.get(i2).intValue());
            i2++;
        }
    }

    @Override // com.google.crypto.tink.shaded.protobuf.Writer
    public void G(int i, List<Double> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.N0(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += CodedOutputStream.k(list.get(i4).doubleValue());
            }
            this.a.P0(i3);
            while (i2 < list.size()) {
                this.a.m0(list.get(i2).doubleValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.l0(i, list.get(i2).doubleValue());
            i2++;
        }
    }

    @Override // com.google.crypto.tink.shaded.protobuf.Writer
    public void H(int i, int i2) throws IOException {
        this.a.I0(i, i2);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.Writer
    public void I(int i, List<ByteString> list) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            this.a.k0(i, list.get(i2));
        }
    }

    @Override // com.google.crypto.tink.shaded.protobuf.Writer
    public void J(int i, Object obj, n0 n0Var) throws IOException {
        this.a.B0(i, (e0) obj, n0Var);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.Writer
    public <K, V> void K(int i, b0.a<K, V> aVar, Map<K, V> map) throws IOException {
        if (this.a.d0()) {
            Q(i, aVar, map);
            return;
        }
        for (Map.Entry<K, V> entry : map.entrySet()) {
            this.a.N0(i, 2);
            this.a.P0(b0.b(aVar, entry.getKey(), entry.getValue()));
            b0.d(this.a, aVar, entry.getKey(), entry.getValue());
        }
    }

    @Override // com.google.crypto.tink.shaded.protobuf.Writer
    public void L(int i, Object obj, n0 n0Var) throws IOException {
        this.a.v0(i, (e0) obj, n0Var);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.Writer
    public void M(int i, List<?> list, n0 n0Var) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            L(i, list.get(i2), n0Var);
        }
    }

    @Override // com.google.crypto.tink.shaded.protobuf.Writer
    public void N(int i, List<?> list, n0 n0Var) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            J(i, list.get(i2), n0Var);
        }
    }

    @Override // com.google.crypto.tink.shaded.protobuf.Writer
    public void O(int i, ByteString byteString) throws IOException {
        this.a.k0(i, byteString);
    }

    public final <K, V> void Q(int i, b0.a<K, V> aVar, Map<K, V> map) throws IOException {
        int[] iArr = a.a;
        throw null;
    }

    public final void R(int i, Object obj) throws IOException {
        if (obj instanceof String) {
            this.a.M0(i, (String) obj);
        } else {
            this.a.k0(i, (ByteString) obj);
        }
    }

    @Override // com.google.crypto.tink.shaded.protobuf.Writer
    public void a(int i, List<Float> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.N0(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += CodedOutputStream.s(list.get(i4).floatValue());
            }
            this.a.P0(i3);
            while (i2 < list.size()) {
                this.a.u0(list.get(i2).floatValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.t0(i, list.get(i2).floatValue());
            i2++;
        }
    }

    @Override // com.google.crypto.tink.shaded.protobuf.Writer
    public void b(int i, int i2) throws IOException {
        this.a.O0(i, i2);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.Writer
    public final void c(int i, Object obj) throws IOException {
        if (obj instanceof ByteString) {
            this.a.D0(i, (ByteString) obj);
        } else {
            this.a.C0(i, (e0) obj);
        }
    }

    @Override // com.google.crypto.tink.shaded.protobuf.Writer
    public void d(int i, int i2) throws IOException {
        this.a.p0(i, i2);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.Writer
    public void e(int i, double d) throws IOException {
        this.a.l0(i, d);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.Writer
    public void f(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.N0(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += CodedOutputStream.O(list.get(i4).longValue());
            }
            this.a.P0(i3);
            while (i2 < list.size()) {
                this.a.H0(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.G0(i, list.get(i2).longValue());
            i2++;
        }
    }

    @Override // com.google.crypto.tink.shaded.protobuf.Writer
    public void g(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.N0(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += CodedOutputStream.Z(list.get(i4).longValue());
            }
            this.a.P0(i3);
            while (i2 < list.size()) {
                this.a.R0(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.Q0(i, list.get(i2).longValue());
            i2++;
        }
    }

    @Override // com.google.crypto.tink.shaded.protobuf.Writer
    public void h(int i, long j) throws IOException {
        this.a.r0(i, j);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.Writer
    public Writer.FieldOrder i() {
        return Writer.FieldOrder.ASCENDING;
    }

    @Override // com.google.crypto.tink.shaded.protobuf.Writer
    public void j(int i, List<String> list) throws IOException {
        int i2 = 0;
        if (list instanceof dz1) {
            dz1 dz1Var = (dz1) list;
            while (i2 < list.size()) {
                R(i, dz1Var.j(i2));
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.M0(i, list.get(i2));
            i2++;
        }
    }

    @Override // com.google.crypto.tink.shaded.protobuf.Writer
    public void k(int i, String str) throws IOException {
        this.a.M0(i, str);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.Writer
    public void l(int i, long j) throws IOException {
        this.a.Q0(i, j);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.Writer
    public void m(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.N0(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += CodedOutputStream.x(list.get(i4).intValue());
            }
            this.a.P0(i3);
            while (i2 < list.size()) {
                this.a.y0(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.x0(i, list.get(i2).intValue());
            i2++;
        }
    }

    @Override // com.google.crypto.tink.shaded.protobuf.Writer
    public void n(int i, long j) throws IOException {
        this.a.z0(i, j);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.Writer
    public void o(int i, boolean z) throws IOException {
        this.a.i0(i, z);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.Writer
    public void p(int i, int i2) throws IOException {
        this.a.E0(i, i2);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.Writer
    public void q(int i) throws IOException {
        this.a.N0(i, 3);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.Writer
    public void r(int i, int i2) throws IOException {
        this.a.x0(i, i2);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.Writer
    public void s(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.N0(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += CodedOutputStream.q(list.get(i4).longValue());
            }
            this.a.P0(i3);
            while (i2 < list.size()) {
                this.a.s0(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.r0(i, list.get(i2).longValue());
            i2++;
        }
    }

    @Override // com.google.crypto.tink.shaded.protobuf.Writer
    public void t(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.N0(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += CodedOutputStream.M(list.get(i4).intValue());
            }
            this.a.P0(i3);
            while (i2 < list.size()) {
                this.a.F0(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.E0(i, list.get(i2).intValue());
            i2++;
        }
    }

    @Override // com.google.crypto.tink.shaded.protobuf.Writer
    public void u(int i, long j) throws IOException {
        this.a.G0(i, j);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.Writer
    public void v(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.N0(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += CodedOutputStream.o(list.get(i4).intValue());
            }
            this.a.P0(i3);
            while (i2 < list.size()) {
                this.a.q0(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.p0(i, list.get(i2).intValue());
            i2++;
        }
    }

    @Override // com.google.crypto.tink.shaded.protobuf.Writer
    public void w(int i, List<Boolean> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.N0(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += CodedOutputStream.f(list.get(i4).booleanValue());
            }
            this.a.P0(i3);
            while (i2 < list.size()) {
                this.a.j0(list.get(i2).booleanValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.i0(i, list.get(i2).booleanValue());
            i2++;
        }
    }

    @Override // com.google.crypto.tink.shaded.protobuf.Writer
    public void x(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.N0(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += CodedOutputStream.X(list.get(i4).intValue());
            }
            this.a.P0(i3);
            while (i2 < list.size()) {
                this.a.P0(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.O0(i, list.get(i2).intValue());
            i2++;
        }
    }

    @Override // com.google.crypto.tink.shaded.protobuf.Writer
    public void y(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.N0(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += CodedOutputStream.S(list.get(i4).longValue());
            }
            this.a.P0(i3);
            while (i2 < list.size()) {
                this.a.L0(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.K0(i, list.get(i2).longValue());
            i2++;
        }
    }

    @Override // com.google.crypto.tink.shaded.protobuf.Writer
    public void z(int i, long j) throws IOException {
        this.a.K0(i, j);
    }
}
