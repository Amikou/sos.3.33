package com.google.crypto.tink.shaded.protobuf;

import com.github.mikephil.charting.utils.Utils;
import com.google.crypto.tink.shaded.protobuf.ByteString;
import com.google.crypto.tink.shaded.protobuf.Writer;
import com.google.crypto.tink.shaded.protobuf.b0;
import com.google.crypto.tink.shaded.protobuf.d;
import com.google.crypto.tink.shaded.protobuf.v;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import sun.misc.Unsafe;

/* compiled from: MessageSchema.java */
/* loaded from: classes2.dex */
public final class g0<T> implements n0<T> {
    public static final int[] r = new int[0];
    public static final Unsafe s = t0.F();
    public final int[] a;
    public final Object[] b;
    public final int c;
    public final int d;
    public final e0 e;
    public final boolean f;
    public final boolean g;
    public final boolean h;
    public final boolean i;
    public final int[] j;
    public final int k;
    public final int l;
    public final jf2 m;
    public final z n;
    public final q0<?, ?> o;
    public final o<?> p;
    public final c0 q;

    public g0(int[] iArr, Object[] objArr, int i, int i2, e0 e0Var, boolean z, boolean z2, int[] iArr2, int i3, int i4, jf2 jf2Var, z zVar, q0<?, ?> q0Var, o<?> oVar, c0 c0Var) {
        this.a = iArr;
        this.b = objArr;
        this.c = i;
        this.d = i2;
        this.g = e0Var instanceof GeneratedMessageLite;
        this.h = z;
        this.f = oVar != null && oVar.e(e0Var);
        this.i = z2;
        this.j = iArr2;
        this.k = i3;
        this.l = i4;
        this.m = jf2Var;
        this.n = zVar;
        this.o = q0Var;
        this.p = oVar;
        this.e = e0Var;
        this.q = c0Var;
    }

    public static boolean A(int i) {
        return (i & 536870912) != 0;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static boolean D(Object obj, int i, n0 n0Var) {
        return n0Var.f(t0.E(obj, U(i)));
    }

    public static boolean I(int i) {
        return (i & 268435456) != 0;
    }

    public static List<?> J(Object obj, long j) {
        return (List) t0.E(obj, j);
    }

    public static <T> long K(T t, long j) {
        return t0.C(t, j);
    }

    public static <T> g0<T> Q(Class<T> cls, a82 a82Var, jf2 jf2Var, z zVar, q0<?, ?> q0Var, o<?> oVar, c0 c0Var) {
        if (a82Var instanceof x33) {
            return S((x33) a82Var, jf2Var, zVar, q0Var, oVar, c0Var);
        }
        return R((gv3) a82Var, jf2Var, zVar, q0Var, oVar, c0Var);
    }

    public static <T> g0<T> R(gv3 gv3Var, jf2 jf2Var, z zVar, q0<?, ?> q0Var, o<?> oVar, c0 c0Var) {
        boolean z = gv3Var.c() == ProtoSyntax.PROTO3;
        q[] e = gv3Var.e();
        if (e.length == 0) {
            int length = e.length;
            int[] iArr = new int[length * 3];
            Object[] objArr = new Object[length * 2];
            if (e.length <= 0) {
                int[] d = gv3Var.d();
                if (d == null) {
                    d = r;
                }
                if (e.length <= 0) {
                    int[] iArr2 = r;
                    int[] iArr3 = r;
                    int[] iArr4 = new int[d.length + iArr2.length + iArr3.length];
                    System.arraycopy(d, 0, iArr4, 0, d.length);
                    System.arraycopy(iArr2, 0, iArr4, d.length, iArr2.length);
                    System.arraycopy(iArr3, 0, iArr4, d.length + iArr2.length, iArr3.length);
                    return new g0<>(iArr, objArr, 0, 0, gv3Var.b(), z, true, iArr4, d.length, d.length + iArr2.length, jf2Var, zVar, q0Var, oVar, c0Var);
                }
                q qVar = e[0];
                throw null;
            }
            q qVar2 = e[0];
            throw null;
        }
        q qVar3 = e[0];
        throw null;
    }

    /* JADX WARN: Removed duplicated region for block: B:124:0x0277  */
    /* JADX WARN: Removed duplicated region for block: B:125:0x027a  */
    /* JADX WARN: Removed duplicated region for block: B:128:0x0292  */
    /* JADX WARN: Removed duplicated region for block: B:129:0x0295  */
    /* JADX WARN: Removed duplicated region for block: B:163:0x033c  */
    /* JADX WARN: Removed duplicated region for block: B:178:0x0391  */
    /* JADX WARN: Removed duplicated region for block: B:182:0x039e  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static <T> com.google.crypto.tink.shaded.protobuf.g0<T> S(defpackage.x33 r36, defpackage.jf2 r37, com.google.crypto.tink.shaded.protobuf.z r38, com.google.crypto.tink.shaded.protobuf.q0<?, ?> r39, com.google.crypto.tink.shaded.protobuf.o<?> r40, com.google.crypto.tink.shaded.protobuf.c0 r41) {
        /*
            Method dump skipped, instructions count: 1041
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.crypto.tink.shaded.protobuf.g0.S(x33, jf2, com.google.crypto.tink.shaded.protobuf.z, com.google.crypto.tink.shaded.protobuf.q0, com.google.crypto.tink.shaded.protobuf.o, com.google.crypto.tink.shaded.protobuf.c0):com.google.crypto.tink.shaded.protobuf.g0");
    }

    public static long U(int i) {
        return i & 1048575;
    }

    public static <T> boolean V(T t, long j) {
        return ((Boolean) t0.E(t, j)).booleanValue();
    }

    public static <T> double W(T t, long j) {
        return ((Double) t0.E(t, j)).doubleValue();
    }

    public static <T> float X(T t, long j) {
        return ((Float) t0.E(t, j)).floatValue();
    }

    public static <T> int Y(T t, long j) {
        return ((Integer) t0.E(t, j)).intValue();
    }

    public static <T> long Z(T t, long j) {
        return ((Long) t0.E(t, j)).longValue();
    }

    public static <T> boolean l(T t, long j) {
        return t0.r(t, j);
    }

    public static Field m0(Class<?> cls, String str) {
        Field[] declaredFields;
        try {
            return cls.getDeclaredField(str);
        } catch (NoSuchFieldException unused) {
            for (Field field : cls.getDeclaredFields()) {
                if (str.equals(field.getName())) {
                    return field;
                }
            }
            throw new RuntimeException("Field " + str + " for " + cls.getName() + " not found. Known fields are " + Arrays.toString(declaredFields));
        }
    }

    public static <T> double n(T t, long j) {
        return t0.y(t, j);
    }

    public static int q0(int i) {
        return (i & 267386880) >>> 20;
    }

    public static <T> float r(T t, long j) {
        return t0.z(t, j);
    }

    public static r0 v(Object obj) {
        GeneratedMessageLite generatedMessageLite = (GeneratedMessageLite) obj;
        r0 r0Var = generatedMessageLite.unknownFields;
        if (r0Var == r0.e()) {
            r0 l = r0.l();
            generatedMessageLite.unknownFields = l;
            return l;
        }
        return r0Var;
    }

    public static <T> int z(T t, long j) {
        return t0.A(t, j);
    }

    public final boolean B(T t, int i) {
        if (this.h) {
            int r0 = r0(i);
            long U = U(r0);
            switch (q0(r0)) {
                case 0:
                    return t0.y(t, U) != Utils.DOUBLE_EPSILON;
                case 1:
                    return t0.z(t, U) != Utils.FLOAT_EPSILON;
                case 2:
                    return t0.C(t, U) != 0;
                case 3:
                    return t0.C(t, U) != 0;
                case 4:
                    return t0.A(t, U) != 0;
                case 5:
                    return t0.C(t, U) != 0;
                case 6:
                    return t0.A(t, U) != 0;
                case 7:
                    return t0.r(t, U);
                case 8:
                    Object E = t0.E(t, U);
                    if (E instanceof String) {
                        return !((String) E).isEmpty();
                    }
                    if (E instanceof ByteString) {
                        return !ByteString.EMPTY.equals(E);
                    }
                    throw new IllegalArgumentException();
                case 9:
                    return t0.E(t, U) != null;
                case 10:
                    return !ByteString.EMPTY.equals(t0.E(t, U));
                case 11:
                    return t0.A(t, U) != 0;
                case 12:
                    return t0.A(t, U) != 0;
                case 13:
                    return t0.A(t, U) != 0;
                case 14:
                    return t0.C(t, U) != 0;
                case 15:
                    return t0.A(t, U) != 0;
                case 16:
                    return t0.C(t, U) != 0;
                case 17:
                    return t0.E(t, U) != null;
                default:
                    throw new IllegalArgumentException();
            }
        }
        int h0 = h0(i);
        return (t0.A(t, (long) (h0 & 1048575)) & (1 << (h0 >>> 20))) != 0;
    }

    public final boolean C(T t, int i, int i2, int i3) {
        if (this.h) {
            return B(t, i);
        }
        return (i2 & i3) != 0;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public final <N> boolean E(Object obj, int i, int i2) {
        List list = (List) t0.E(obj, U(i));
        if (list.isEmpty()) {
            return true;
        }
        n0 u = u(i2);
        for (int i3 = 0; i3 < list.size(); i3++) {
            if (!u.f(list.get(i3))) {
                return false;
            }
        }
        return true;
    }

    public final boolean F(T t, int i, int i2) {
        if (this.q.h(t0.E(t, U(i))).isEmpty()) {
            return true;
        }
        this.q.c(t(i2));
        throw null;
    }

    public final boolean G(T t, T t2, int i) {
        long h0 = h0(i) & 1048575;
        return t0.A(t, h0) == t0.A(t2, h0);
    }

    public final boolean H(T t, int i, int i2) {
        return t0.A(t, (long) (h0(i2) & 1048575)) == i;
    }

    /* JADX WARN: Code restructure failed: missing block: B:32:0x0077, code lost:
        r0 = r16.k;
     */
    /* JADX WARN: Code restructure failed: missing block: B:34:0x007b, code lost:
        if (r0 >= r16.l) goto L328;
     */
    /* JADX WARN: Code restructure failed: missing block: B:35:0x007d, code lost:
        r13 = p(r19, r16.j[r0], r13, r17);
        r0 = r0 + 1;
     */
    /* JADX WARN: Code restructure failed: missing block: B:363:?, code lost:
        return;
     */
    /* JADX WARN: Code restructure failed: missing block: B:36:0x0088, code lost:
        if (r13 == null) goto L332;
     */
    /* JADX WARN: Code restructure failed: missing block: B:37:0x008a, code lost:
        r17.o(r19, r13);
     */
    /* JADX WARN: Code restructure failed: missing block: B:38:0x008d, code lost:
        return;
     */
    /* JADX WARN: Multi-variable type inference failed */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final <UT, UB, ET extends com.google.crypto.tink.shaded.protobuf.r.b<ET>> void L(com.google.crypto.tink.shaded.protobuf.q0<UT, UB> r17, com.google.crypto.tink.shaded.protobuf.o<ET> r18, T r19, com.google.crypto.tink.shaded.protobuf.m0 r20, com.google.crypto.tink.shaded.protobuf.n r21) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 1720
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.crypto.tink.shaded.protobuf.g0.L(com.google.crypto.tink.shaded.protobuf.q0, com.google.crypto.tink.shaded.protobuf.o, java.lang.Object, com.google.crypto.tink.shaded.protobuf.m0, com.google.crypto.tink.shaded.protobuf.n):void");
    }

    public final <K, V> void M(Object obj, int i, Object obj2, n nVar, m0 m0Var) throws IOException {
        long U = U(r0(i));
        Object E = t0.E(obj, U);
        if (E == null) {
            E = this.q.e(obj2);
            t0.T(obj, U, E);
        } else if (this.q.g(E)) {
            Object e = this.q.e(obj2);
            this.q.a(e, E);
            t0.T(obj, U, e);
            E = e;
        }
        m0Var.O(this.q.d(E), this.q.c(obj2), nVar);
    }

    public final void N(T t, T t2, int i) {
        long U = U(r0(i));
        if (B(t2, i)) {
            Object E = t0.E(t, U);
            Object E2 = t0.E(t2, U);
            if (E != null && E2 != null) {
                t0.T(t, U, v.h(E, E2));
                n0(t, i);
            } else if (E2 != null) {
                t0.T(t, U, E2);
                n0(t, i);
            }
        }
    }

    public final void O(T t, T t2, int i) {
        int r0 = r0(i);
        int T = T(i);
        long U = U(r0);
        if (H(t2, T, i)) {
            Object E = t0.E(t, U);
            Object E2 = t0.E(t2, U);
            if (E != null && E2 != null) {
                t0.T(t, U, v.h(E, E2));
                o0(t, T, i);
            } else if (E2 != null) {
                t0.T(t, U, E2);
                o0(t, T, i);
            }
        }
    }

    public final void P(T t, T t2, int i) {
        int r0 = r0(i);
        long U = U(r0);
        int T = T(i);
        switch (q0(r0)) {
            case 0:
                if (B(t2, i)) {
                    t0.P(t, U, t0.y(t2, U));
                    n0(t, i);
                    return;
                }
                return;
            case 1:
                if (B(t2, i)) {
                    t0.Q(t, U, t0.z(t2, U));
                    n0(t, i);
                    return;
                }
                return;
            case 2:
                if (B(t2, i)) {
                    t0.S(t, U, t0.C(t2, U));
                    n0(t, i);
                    return;
                }
                return;
            case 3:
                if (B(t2, i)) {
                    t0.S(t, U, t0.C(t2, U));
                    n0(t, i);
                    return;
                }
                return;
            case 4:
                if (B(t2, i)) {
                    t0.R(t, U, t0.A(t2, U));
                    n0(t, i);
                    return;
                }
                return;
            case 5:
                if (B(t2, i)) {
                    t0.S(t, U, t0.C(t2, U));
                    n0(t, i);
                    return;
                }
                return;
            case 6:
                if (B(t2, i)) {
                    t0.R(t, U, t0.A(t2, U));
                    n0(t, i);
                    return;
                }
                return;
            case 7:
                if (B(t2, i)) {
                    t0.J(t, U, t0.r(t2, U));
                    n0(t, i);
                    return;
                }
                return;
            case 8:
                if (B(t2, i)) {
                    t0.T(t, U, t0.E(t2, U));
                    n0(t, i);
                    return;
                }
                return;
            case 9:
                N(t, t2, i);
                return;
            case 10:
                if (B(t2, i)) {
                    t0.T(t, U, t0.E(t2, U));
                    n0(t, i);
                    return;
                }
                return;
            case 11:
                if (B(t2, i)) {
                    t0.R(t, U, t0.A(t2, U));
                    n0(t, i);
                    return;
                }
                return;
            case 12:
                if (B(t2, i)) {
                    t0.R(t, U, t0.A(t2, U));
                    n0(t, i);
                    return;
                }
                return;
            case 13:
                if (B(t2, i)) {
                    t0.R(t, U, t0.A(t2, U));
                    n0(t, i);
                    return;
                }
                return;
            case 14:
                if (B(t2, i)) {
                    t0.S(t, U, t0.C(t2, U));
                    n0(t, i);
                    return;
                }
                return;
            case 15:
                if (B(t2, i)) {
                    t0.R(t, U, t0.A(t2, U));
                    n0(t, i);
                    return;
                }
                return;
            case 16:
                if (B(t2, i)) {
                    t0.S(t, U, t0.C(t2, U));
                    n0(t, i);
                    return;
                }
                return;
            case 17:
                N(t, t2, i);
                return;
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
            case 35:
            case 36:
            case 37:
            case 38:
            case 39:
            case 40:
            case 41:
            case 42:
            case 43:
            case 44:
            case 45:
            case 46:
            case 47:
            case 48:
            case 49:
                this.n.d(t, t2, U);
                return;
            case 50:
                o0.F(this.q, t, t2, U);
                return;
            case 51:
            case 52:
            case 53:
            case 54:
            case 55:
            case 56:
            case 57:
            case 58:
            case 59:
                if (H(t2, T, i)) {
                    t0.T(t, U, t0.E(t2, U));
                    o0(t, T, i);
                    return;
                }
                return;
            case 60:
                O(t, t2, i);
                return;
            case 61:
            case 62:
            case 63:
            case 64:
            case 65:
            case 66:
            case 67:
                if (H(t2, T, i)) {
                    t0.T(t, U, t0.E(t2, U));
                    o0(t, T, i);
                    return;
                }
                return;
            case 68:
                O(t, t2, i);
                return;
            default:
                return;
        }
    }

    public final int T(int i) {
        return this.a[i];
    }

    @Override // com.google.crypto.tink.shaded.protobuf.n0
    public void a(T t, T t2) {
        Objects.requireNonNull(t2);
        for (int i = 0; i < this.a.length; i += 3) {
            P(t, t2, i);
        }
        o0.G(this.o, t, t2);
        if (this.f) {
            o0.E(this.p, t, t2);
        }
    }

    public final <K, V> int a0(T t, byte[] bArr, int i, int i2, int i3, long j, d.b bVar) throws IOException {
        Unsafe unsafe = s;
        Object t2 = t(i3);
        Object object = unsafe.getObject(t, j);
        if (this.q.g(object)) {
            Object e = this.q.e(t2);
            this.q.a(e, object);
            unsafe.putObject(t, j, e);
            object = e;
        }
        return m(bArr, i, i2, this.q.c(t2), this.q.d(object), bVar);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.n0
    public boolean b(T t, T t2) {
        int length = this.a.length;
        for (int i = 0; i < length; i += 3) {
            if (!o(t, t2, i)) {
                return false;
            }
        }
        if (this.o.g(t).equals(this.o.g(t2))) {
            if (this.f) {
                return this.p.c(t).equals(this.p.c(t2));
            }
            return true;
        }
        return false;
    }

    public final int b0(T t, byte[] bArr, int i, int i2, int i3, int i4, int i5, int i6, int i7, long j, int i8, d.b bVar) throws IOException {
        Unsafe unsafe = s;
        long j2 = this.a[i8 + 2] & 1048575;
        switch (i7) {
            case 51:
                if (i5 == 1) {
                    unsafe.putObject(t, j, Double.valueOf(d.d(bArr, i)));
                    int i9 = i + 8;
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                break;
            case 52:
                if (i5 == 5) {
                    unsafe.putObject(t, j, Float.valueOf(d.l(bArr, i)));
                    int i10 = i + 4;
                    unsafe.putInt(t, j2, i4);
                    return i10;
                }
                break;
            case 53:
            case 54:
                if (i5 == 0) {
                    int L = d.L(bArr, i, bVar);
                    unsafe.putObject(t, j, Long.valueOf(bVar.b));
                    unsafe.putInt(t, j2, i4);
                    return L;
                }
                break;
            case 55:
            case 62:
                if (i5 == 0) {
                    int I = d.I(bArr, i, bVar);
                    unsafe.putObject(t, j, Integer.valueOf(bVar.a));
                    unsafe.putInt(t, j2, i4);
                    return I;
                }
                break;
            case 56:
            case 65:
                if (i5 == 1) {
                    unsafe.putObject(t, j, Long.valueOf(d.j(bArr, i)));
                    int i11 = i + 8;
                    unsafe.putInt(t, j2, i4);
                    return i11;
                }
                break;
            case 57:
            case 64:
                if (i5 == 5) {
                    unsafe.putObject(t, j, Integer.valueOf(d.h(bArr, i)));
                    int i12 = i + 4;
                    unsafe.putInt(t, j2, i4);
                    return i12;
                }
                break;
            case 58:
                if (i5 == 0) {
                    int L2 = d.L(bArr, i, bVar);
                    unsafe.putObject(t, j, Boolean.valueOf(bVar.b != 0));
                    unsafe.putInt(t, j2, i4);
                    return L2;
                }
                break;
            case 59:
                if (i5 == 2) {
                    int I2 = d.I(bArr, i, bVar);
                    int i13 = bVar.a;
                    if (i13 == 0) {
                        unsafe.putObject(t, j, "");
                    } else if ((i6 & 536870912) != 0 && !Utf8.t(bArr, I2, I2 + i13)) {
                        throw InvalidProtocolBufferException.invalidUtf8();
                    } else {
                        unsafe.putObject(t, j, new String(bArr, I2, i13, v.a));
                        I2 += i13;
                    }
                    unsafe.putInt(t, j2, i4);
                    return I2;
                }
                break;
            case 60:
                if (i5 == 2) {
                    int p = d.p(u(i8), bArr, i, i2, bVar);
                    Object object = unsafe.getInt(t, j2) == i4 ? unsafe.getObject(t, j) : null;
                    if (object == null) {
                        unsafe.putObject(t, j, bVar.c);
                    } else {
                        unsafe.putObject(t, j, v.h(object, bVar.c));
                    }
                    unsafe.putInt(t, j2, i4);
                    return p;
                }
                break;
            case 61:
                if (i5 == 2) {
                    int b = d.b(bArr, i, bVar);
                    unsafe.putObject(t, j, bVar.c);
                    unsafe.putInt(t, j2, i4);
                    return b;
                }
                break;
            case 63:
                if (i5 == 0) {
                    int I3 = d.I(bArr, i, bVar);
                    int i14 = bVar.a;
                    v.e s2 = s(i8);
                    if (s2 != null && !s2.a(i14)) {
                        v(t).n(i3, Long.valueOf(i14));
                    } else {
                        unsafe.putObject(t, j, Integer.valueOf(i14));
                        unsafe.putInt(t, j2, i4);
                    }
                    return I3;
                }
                break;
            case 66:
                if (i5 == 0) {
                    int I4 = d.I(bArr, i, bVar);
                    unsafe.putObject(t, j, Integer.valueOf(i.b(bVar.a)));
                    unsafe.putInt(t, j2, i4);
                    return I4;
                }
                break;
            case 67:
                if (i5 == 0) {
                    int L3 = d.L(bArr, i, bVar);
                    unsafe.putObject(t, j, Long.valueOf(i.c(bVar.b)));
                    unsafe.putInt(t, j2, i4);
                    return L3;
                }
                break;
            case 68:
                if (i5 == 3) {
                    int n = d.n(u(i8), bArr, i, i2, (i3 & (-8)) | 4, bVar);
                    Object object2 = unsafe.getInt(t, j2) == i4 ? unsafe.getObject(t, j) : null;
                    if (object2 == null) {
                        unsafe.putObject(t, j, bVar.c);
                    } else {
                        unsafe.putObject(t, j, v.h(object2, bVar.c));
                    }
                    unsafe.putInt(t, j2, i4);
                    return n;
                }
                break;
        }
        return i;
    }

    @Override // com.google.crypto.tink.shaded.protobuf.n0
    public T c() {
        return (T) this.m.a(this.e);
    }

    /* JADX WARN: Code restructure failed: missing block: B:119:0x0359, code lost:
        if (r0 != r11) goto L202;
     */
    /* JADX WARN: Code restructure failed: missing block: B:120:0x035b, code lost:
        r15 = r30;
        r14 = r31;
        r12 = r32;
        r13 = r34;
        r11 = r35;
        r9 = r36;
        r1 = r17;
        r3 = r18;
        r7 = r19;
        r2 = r20;
        r6 = r22;
     */
    /* JADX WARN: Code restructure failed: missing block: B:127:0x03a2, code lost:
        if (r0 != r15) goto L202;
     */
    /* JADX WARN: Code restructure failed: missing block: B:131:0x03c5, code lost:
        if (r0 != r15) goto L202;
     */
    /* JADX WARN: Code restructure failed: missing block: B:133:0x03c8, code lost:
        r2 = r0;
        r8 = r18;
        r0 = r35;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public int c0(T r31, byte[] r32, int r33, int r34, int r35, com.google.crypto.tink.shaded.protobuf.d.b r36) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 1172
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.crypto.tink.shaded.protobuf.g0.c0(java.lang.Object, byte[], int, int, int, com.google.crypto.tink.shaded.protobuf.d$b):int");
    }

    @Override // com.google.crypto.tink.shaded.protobuf.n0
    public int d(T t) {
        int i;
        int f;
        int length = this.a.length;
        int i2 = 0;
        for (int i3 = 0; i3 < length; i3 += 3) {
            int r0 = r0(i3);
            int T = T(i3);
            long U = U(r0);
            int i4 = 37;
            switch (q0(r0)) {
                case 0:
                    i = i2 * 53;
                    f = v.f(Double.doubleToLongBits(t0.y(t, U)));
                    i2 = i + f;
                    break;
                case 1:
                    i = i2 * 53;
                    f = Float.floatToIntBits(t0.z(t, U));
                    i2 = i + f;
                    break;
                case 2:
                    i = i2 * 53;
                    f = v.f(t0.C(t, U));
                    i2 = i + f;
                    break;
                case 3:
                    i = i2 * 53;
                    f = v.f(t0.C(t, U));
                    i2 = i + f;
                    break;
                case 4:
                    i = i2 * 53;
                    f = t0.A(t, U);
                    i2 = i + f;
                    break;
                case 5:
                    i = i2 * 53;
                    f = v.f(t0.C(t, U));
                    i2 = i + f;
                    break;
                case 6:
                    i = i2 * 53;
                    f = t0.A(t, U);
                    i2 = i + f;
                    break;
                case 7:
                    i = i2 * 53;
                    f = v.c(t0.r(t, U));
                    i2 = i + f;
                    break;
                case 8:
                    i = i2 * 53;
                    f = ((String) t0.E(t, U)).hashCode();
                    i2 = i + f;
                    break;
                case 9:
                    Object E = t0.E(t, U);
                    if (E != null) {
                        i4 = E.hashCode();
                    }
                    i2 = (i2 * 53) + i4;
                    break;
                case 10:
                    i = i2 * 53;
                    f = t0.E(t, U).hashCode();
                    i2 = i + f;
                    break;
                case 11:
                    i = i2 * 53;
                    f = t0.A(t, U);
                    i2 = i + f;
                    break;
                case 12:
                    i = i2 * 53;
                    f = t0.A(t, U);
                    i2 = i + f;
                    break;
                case 13:
                    i = i2 * 53;
                    f = t0.A(t, U);
                    i2 = i + f;
                    break;
                case 14:
                    i = i2 * 53;
                    f = v.f(t0.C(t, U));
                    i2 = i + f;
                    break;
                case 15:
                    i = i2 * 53;
                    f = t0.A(t, U);
                    i2 = i + f;
                    break;
                case 16:
                    i = i2 * 53;
                    f = v.f(t0.C(t, U));
                    i2 = i + f;
                    break;
                case 17:
                    Object E2 = t0.E(t, U);
                    if (E2 != null) {
                        i4 = E2.hashCode();
                    }
                    i2 = (i2 * 53) + i4;
                    break;
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                case 48:
                case 49:
                    i = i2 * 53;
                    f = t0.E(t, U).hashCode();
                    i2 = i + f;
                    break;
                case 50:
                    i = i2 * 53;
                    f = t0.E(t, U).hashCode();
                    i2 = i + f;
                    break;
                case 51:
                    if (H(t, T, i3)) {
                        i = i2 * 53;
                        f = v.f(Double.doubleToLongBits(W(t, U)));
                        i2 = i + f;
                        break;
                    } else {
                        break;
                    }
                case 52:
                    if (H(t, T, i3)) {
                        i = i2 * 53;
                        f = Float.floatToIntBits(X(t, U));
                        i2 = i + f;
                        break;
                    } else {
                        break;
                    }
                case 53:
                    if (H(t, T, i3)) {
                        i = i2 * 53;
                        f = v.f(Z(t, U));
                        i2 = i + f;
                        break;
                    } else {
                        break;
                    }
                case 54:
                    if (H(t, T, i3)) {
                        i = i2 * 53;
                        f = v.f(Z(t, U));
                        i2 = i + f;
                        break;
                    } else {
                        break;
                    }
                case 55:
                    if (H(t, T, i3)) {
                        i = i2 * 53;
                        f = Y(t, U);
                        i2 = i + f;
                        break;
                    } else {
                        break;
                    }
                case 56:
                    if (H(t, T, i3)) {
                        i = i2 * 53;
                        f = v.f(Z(t, U));
                        i2 = i + f;
                        break;
                    } else {
                        break;
                    }
                case 57:
                    if (H(t, T, i3)) {
                        i = i2 * 53;
                        f = Y(t, U);
                        i2 = i + f;
                        break;
                    } else {
                        break;
                    }
                case 58:
                    if (H(t, T, i3)) {
                        i = i2 * 53;
                        f = v.c(V(t, U));
                        i2 = i + f;
                        break;
                    } else {
                        break;
                    }
                case 59:
                    if (H(t, T, i3)) {
                        i = i2 * 53;
                        f = ((String) t0.E(t, U)).hashCode();
                        i2 = i + f;
                        break;
                    } else {
                        break;
                    }
                case 60:
                    if (H(t, T, i3)) {
                        i = i2 * 53;
                        f = t0.E(t, U).hashCode();
                        i2 = i + f;
                        break;
                    } else {
                        break;
                    }
                case 61:
                    if (H(t, T, i3)) {
                        i = i2 * 53;
                        f = t0.E(t, U).hashCode();
                        i2 = i + f;
                        break;
                    } else {
                        break;
                    }
                case 62:
                    if (H(t, T, i3)) {
                        i = i2 * 53;
                        f = Y(t, U);
                        i2 = i + f;
                        break;
                    } else {
                        break;
                    }
                case 63:
                    if (H(t, T, i3)) {
                        i = i2 * 53;
                        f = Y(t, U);
                        i2 = i + f;
                        break;
                    } else {
                        break;
                    }
                case 64:
                    if (H(t, T, i3)) {
                        i = i2 * 53;
                        f = Y(t, U);
                        i2 = i + f;
                        break;
                    } else {
                        break;
                    }
                case 65:
                    if (H(t, T, i3)) {
                        i = i2 * 53;
                        f = v.f(Z(t, U));
                        i2 = i + f;
                        break;
                    } else {
                        break;
                    }
                case 66:
                    if (H(t, T, i3)) {
                        i = i2 * 53;
                        f = Y(t, U);
                        i2 = i + f;
                        break;
                    } else {
                        break;
                    }
                case 67:
                    if (H(t, T, i3)) {
                        i = i2 * 53;
                        f = v.f(Z(t, U));
                        i2 = i + f;
                        break;
                    } else {
                        break;
                    }
                case 68:
                    if (H(t, T, i3)) {
                        i = i2 * 53;
                        f = t0.E(t, U).hashCode();
                        i2 = i + f;
                        break;
                    } else {
                        break;
                    }
            }
        }
        int hashCode = (i2 * 53) + this.o.g(t).hashCode();
        return this.f ? (hashCode * 53) + this.p.c(t).hashCode() : hashCode;
    }

    /* JADX WARN: Code restructure failed: missing block: B:102:0x022b, code lost:
        if (r0 != r15) goto L28;
     */
    /* JADX WARN: Code restructure failed: missing block: B:104:0x022e, code lost:
        r2 = r0;
     */
    /* JADX WARN: Code restructure failed: missing block: B:91:0x01de, code lost:
        if (r0 != r15) goto L28;
     */
    /* JADX WARN: Code restructure failed: missing block: B:98:0x020c, code lost:
        if (r0 != r15) goto L28;
     */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r3v12, types: [int] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final int d0(T r28, byte[] r29, int r30, int r31, com.google.crypto.tink.shaded.protobuf.d.b r32) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 642
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.crypto.tink.shaded.protobuf.g0.d0(java.lang.Object, byte[], int, int, com.google.crypto.tink.shaded.protobuf.d$b):int");
    }

    @Override // com.google.crypto.tink.shaded.protobuf.n0
    public void e(T t) {
        int i;
        int i2 = this.k;
        while (true) {
            i = this.l;
            if (i2 >= i) {
                break;
            }
            long U = U(r0(this.j[i2]));
            Object E = t0.E(t, U);
            if (E != null) {
                t0.T(t, U, this.q.b(E));
            }
            i2++;
        }
        int length = this.j.length;
        while (i < length) {
            this.n.c(t, this.j[i]);
            i++;
        }
        this.o.j(t);
        if (this.f) {
            this.p.f(t);
        }
    }

    public final int e0(T t, byte[] bArr, int i, int i2, int i3, int i4, int i5, int i6, long j, int i7, long j2, d.b bVar) throws IOException {
        int J;
        Unsafe unsafe = s;
        v.i iVar = (v.i) unsafe.getObject(t, j2);
        if (!iVar.A()) {
            int size = iVar.size();
            iVar = iVar.a(size == 0 ? 10 : size * 2);
            unsafe.putObject(t, j2, iVar);
        }
        switch (i7) {
            case 18:
            case 35:
                if (i5 == 2) {
                    return d.s(bArr, i, iVar, bVar);
                }
                if (i5 == 1) {
                    return d.e(i3, bArr, i, i2, iVar, bVar);
                }
                break;
            case 19:
            case 36:
                if (i5 == 2) {
                    return d.v(bArr, i, iVar, bVar);
                }
                if (i5 == 5) {
                    return d.m(i3, bArr, i, i2, iVar, bVar);
                }
                break;
            case 20:
            case 21:
            case 37:
            case 38:
                if (i5 == 2) {
                    return d.z(bArr, i, iVar, bVar);
                }
                if (i5 == 0) {
                    return d.M(i3, bArr, i, i2, iVar, bVar);
                }
                break;
            case 22:
            case 29:
            case 39:
            case 43:
                if (i5 == 2) {
                    return d.y(bArr, i, iVar, bVar);
                }
                if (i5 == 0) {
                    return d.J(i3, bArr, i, i2, iVar, bVar);
                }
                break;
            case 23:
            case 32:
            case 40:
            case 46:
                if (i5 == 2) {
                    return d.u(bArr, i, iVar, bVar);
                }
                if (i5 == 1) {
                    return d.k(i3, bArr, i, i2, iVar, bVar);
                }
                break;
            case 24:
            case 31:
            case 41:
            case 45:
                if (i5 == 2) {
                    return d.t(bArr, i, iVar, bVar);
                }
                if (i5 == 5) {
                    return d.i(i3, bArr, i, i2, iVar, bVar);
                }
                break;
            case 25:
            case 42:
                if (i5 == 2) {
                    return d.r(bArr, i, iVar, bVar);
                }
                if (i5 == 0) {
                    return d.a(i3, bArr, i, i2, iVar, bVar);
                }
                break;
            case 26:
                if (i5 == 2) {
                    if ((j & 536870912) == 0) {
                        return d.D(i3, bArr, i, i2, iVar, bVar);
                    }
                    return d.E(i3, bArr, i, i2, iVar, bVar);
                }
                break;
            case 27:
                if (i5 == 2) {
                    return d.q(u(i6), i3, bArr, i, i2, iVar, bVar);
                }
                break;
            case 28:
                if (i5 == 2) {
                    return d.c(i3, bArr, i, i2, iVar, bVar);
                }
                break;
            case 30:
            case 44:
                if (i5 == 2) {
                    J = d.y(bArr, i, iVar, bVar);
                } else if (i5 == 0) {
                    J = d.J(i3, bArr, i, i2, iVar, bVar);
                }
                GeneratedMessageLite generatedMessageLite = (GeneratedMessageLite) t;
                r0 r0Var = generatedMessageLite.unknownFields;
                if (r0Var == r0.e()) {
                    r0Var = null;
                }
                r0 r0Var2 = (r0) o0.A(i4, iVar, s(i6), r0Var, this.o);
                if (r0Var2 != null) {
                    generatedMessageLite.unknownFields = r0Var2;
                }
                return J;
            case 33:
            case 47:
                if (i5 == 2) {
                    return d.w(bArr, i, iVar, bVar);
                }
                if (i5 == 0) {
                    return d.A(i3, bArr, i, i2, iVar, bVar);
                }
                break;
            case 34:
            case 48:
                if (i5 == 2) {
                    return d.x(bArr, i, iVar, bVar);
                }
                if (i5 == 0) {
                    return d.B(i3, bArr, i, i2, iVar, bVar);
                }
                break;
            case 49:
                if (i5 == 3) {
                    return d.o(u(i6), i3, bArr, i, i2, iVar, bVar);
                }
                break;
        }
        return i;
    }

    @Override // com.google.crypto.tink.shaded.protobuf.n0
    public final boolean f(T t) {
        int i;
        int i2 = -1;
        int i3 = 0;
        for (int i4 = 0; i4 < this.k; i4++) {
            int i5 = this.j[i4];
            int T = T(i5);
            int r0 = r0(i5);
            if (this.h) {
                i = 0;
            } else {
                int i6 = this.a[i5 + 2];
                int i7 = 1048575 & i6;
                i = 1 << (i6 >>> 20);
                if (i7 != i2) {
                    i3 = s.getInt(t, i7);
                    i2 = i7;
                }
            }
            if (I(r0) && !C(t, i5, i3, i)) {
                return false;
            }
            int q0 = q0(r0);
            if (q0 != 9 && q0 != 17) {
                if (q0 != 27) {
                    if (q0 == 60 || q0 == 68) {
                        if (H(t, T, i5) && !D(t, r0, u(i5))) {
                            return false;
                        }
                    } else if (q0 != 49) {
                        if (q0 == 50 && !F(t, r0, i5)) {
                            return false;
                        }
                    }
                }
                if (!E(t, r0, i5)) {
                    return false;
                }
            } else if (C(t, i5, i3, i) && !D(t, r0, u(i5))) {
                return false;
            }
        }
        return !this.f || this.p.c(t).o();
    }

    public final int f0(int i) {
        if (i < this.c || i > this.d) {
            return -1;
        }
        return p0(i, 0);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.n0
    public int g(T t) {
        return this.h ? x(t) : w(t);
    }

    public final int g0(int i, int i2) {
        if (i < this.c || i > this.d) {
            return -1;
        }
        return p0(i, i2);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.n0
    public void h(T t, byte[] bArr, int i, int i2, d.b bVar) throws IOException {
        if (this.h) {
            d0(t, bArr, i, i2, bVar);
        } else {
            c0(t, bArr, i, i2, 0, bVar);
        }
    }

    public final int h0(int i) {
        return this.a[i + 2];
    }

    @Override // com.google.crypto.tink.shaded.protobuf.n0
    public void i(T t, Writer writer) throws IOException {
        if (writer.i() == Writer.FieldOrder.DESCENDING) {
            u0(t, writer);
        } else if (this.h) {
            t0(t, writer);
        } else {
            s0(t, writer);
        }
    }

    public final <E> void i0(Object obj, long j, m0 m0Var, n0<E> n0Var, n nVar) throws IOException {
        m0Var.P(this.n.e(obj, j), n0Var, nVar);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.n0
    public void j(T t, m0 m0Var, n nVar) throws IOException {
        Objects.requireNonNull(nVar);
        L(this.o, this.p, t, m0Var, nVar);
    }

    public final <E> void j0(Object obj, int i, m0 m0Var, n0<E> n0Var, n nVar) throws IOException {
        m0Var.J(this.n.e(obj, U(i)), n0Var, nVar);
    }

    public final boolean k(T t, T t2, int i) {
        return B(t, i) == B(t2, i);
    }

    public final void k0(Object obj, int i, m0 m0Var) throws IOException {
        if (A(i)) {
            t0.T(obj, U(i), m0Var.H());
        } else if (this.g) {
            t0.T(obj, U(i), m0Var.v());
        } else {
            t0.T(obj, U(i), m0Var.z());
        }
    }

    public final void l0(Object obj, int i, m0 m0Var) throws IOException {
        if (A(i)) {
            m0Var.y(this.n.e(obj, U(i)));
        } else {
            m0Var.x(this.n.e(obj, U(i)));
        }
    }

    public final <K, V> int m(byte[] bArr, int i, int i2, b0.a<K, V> aVar, Map<K, V> map, d.b bVar) throws IOException {
        int I = d.I(bArr, i, bVar);
        int i3 = bVar.a;
        if (i3 >= 0 && i3 <= i2 - I) {
            throw null;
        }
        throw InvalidProtocolBufferException.truncatedMessage();
    }

    public final void n0(T t, int i) {
        if (this.h) {
            return;
        }
        int h0 = h0(i);
        long j = h0 & 1048575;
        t0.R(t, j, t0.A(t, j) | (1 << (h0 >>> 20)));
    }

    public final boolean o(T t, T t2, int i) {
        int r0 = r0(i);
        long U = U(r0);
        switch (q0(r0)) {
            case 0:
                return k(t, t2, i) && Double.doubleToLongBits(t0.y(t, U)) == Double.doubleToLongBits(t0.y(t2, U));
            case 1:
                return k(t, t2, i) && Float.floatToIntBits(t0.z(t, U)) == Float.floatToIntBits(t0.z(t2, U));
            case 2:
                return k(t, t2, i) && t0.C(t, U) == t0.C(t2, U);
            case 3:
                return k(t, t2, i) && t0.C(t, U) == t0.C(t2, U);
            case 4:
                return k(t, t2, i) && t0.A(t, U) == t0.A(t2, U);
            case 5:
                return k(t, t2, i) && t0.C(t, U) == t0.C(t2, U);
            case 6:
                return k(t, t2, i) && t0.A(t, U) == t0.A(t2, U);
            case 7:
                return k(t, t2, i) && t0.r(t, U) == t0.r(t2, U);
            case 8:
                return k(t, t2, i) && o0.K(t0.E(t, U), t0.E(t2, U));
            case 9:
                return k(t, t2, i) && o0.K(t0.E(t, U), t0.E(t2, U));
            case 10:
                return k(t, t2, i) && o0.K(t0.E(t, U), t0.E(t2, U));
            case 11:
                return k(t, t2, i) && t0.A(t, U) == t0.A(t2, U);
            case 12:
                return k(t, t2, i) && t0.A(t, U) == t0.A(t2, U);
            case 13:
                return k(t, t2, i) && t0.A(t, U) == t0.A(t2, U);
            case 14:
                return k(t, t2, i) && t0.C(t, U) == t0.C(t2, U);
            case 15:
                return k(t, t2, i) && t0.A(t, U) == t0.A(t2, U);
            case 16:
                return k(t, t2, i) && t0.C(t, U) == t0.C(t2, U);
            case 17:
                return k(t, t2, i) && o0.K(t0.E(t, U), t0.E(t2, U));
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
            case 35:
            case 36:
            case 37:
            case 38:
            case 39:
            case 40:
            case 41:
            case 42:
            case 43:
            case 44:
            case 45:
            case 46:
            case 47:
            case 48:
            case 49:
                return o0.K(t0.E(t, U), t0.E(t2, U));
            case 50:
                return o0.K(t0.E(t, U), t0.E(t2, U));
            case 51:
            case 52:
            case 53:
            case 54:
            case 55:
            case 56:
            case 57:
            case 58:
            case 59:
            case 60:
            case 61:
            case 62:
            case 63:
            case 64:
            case 65:
            case 66:
            case 67:
            case 68:
                return G(t, t2, i) && o0.K(t0.E(t, U), t0.E(t2, U));
            default:
                return true;
        }
    }

    public final void o0(T t, int i, int i2) {
        t0.R(t, h0(i2) & 1048575, i);
    }

    public final <UT, UB> UB p(Object obj, int i, UB ub, q0<UT, UB> q0Var) {
        v.e s2;
        int T = T(i);
        Object E = t0.E(obj, U(r0(i)));
        return (E == null || (s2 = s(i)) == null) ? ub : (UB) q(i, T, this.q.d(E), s2, ub, q0Var);
    }

    public final int p0(int i, int i2) {
        int length = (this.a.length / 3) - 1;
        while (i2 <= length) {
            int i3 = (length + i2) >>> 1;
            int i4 = i3 * 3;
            int T = T(i4);
            if (i == T) {
                return i4;
            }
            if (i < T) {
                length = i3 - 1;
            } else {
                i2 = i3 + 1;
            }
        }
        return -1;
    }

    public final <K, V, UT, UB> UB q(int i, int i2, Map<K, V> map, v.e eVar, UB ub, q0<UT, UB> q0Var) {
        b0.a<?, ?> c = this.q.c(t(i));
        Iterator<Map.Entry<K, V>> it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<K, V> next = it.next();
            if (!eVar.a(((Integer) next.getValue()).intValue())) {
                if (ub == null) {
                    ub = q0Var.n();
                }
                ByteString.g newCodedBuilder = ByteString.newCodedBuilder(b0.b(c, next.getKey(), next.getValue()));
                try {
                    b0.d(newCodedBuilder.b(), c, next.getKey(), next.getValue());
                    q0Var.d(ub, i2, newCodedBuilder.a());
                    it.remove();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        return ub;
    }

    public final int r0(int i) {
        return this.a[i + 1];
    }

    public final v.e s(int i) {
        return (v.e) this.b[((i / 3) * 2) + 1];
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x002d  */
    /* JADX WARN: Removed duplicated region for block: B:173:0x049e  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void s0(T r18, com.google.crypto.tink.shaded.protobuf.Writer r19) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 1352
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.crypto.tink.shaded.protobuf.g0.s0(java.lang.Object, com.google.crypto.tink.shaded.protobuf.Writer):void");
    }

    public final Object t(int i) {
        return this.b[(i / 3) * 2];
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0025  */
    /* JADX WARN: Removed duplicated region for block: B:164:0x0588  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void t0(T r13, com.google.crypto.tink.shaded.protobuf.Writer r14) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 1584
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.crypto.tink.shaded.protobuf.g0.t0(java.lang.Object, com.google.crypto.tink.shaded.protobuf.Writer):void");
    }

    public final n0 u(int i) {
        int i2 = (i / 3) * 2;
        n0 n0Var = (n0) this.b[i2];
        if (n0Var != null) {
            return n0Var;
        }
        n0<T> d = k0.a().d((Class) this.b[i2 + 1]);
        this.b[i2] = d;
        return d;
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x002a  */
    /* JADX WARN: Removed duplicated region for block: B:164:0x058e  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void u0(T r11, com.google.crypto.tink.shaded.protobuf.Writer r12) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 1586
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.crypto.tink.shaded.protobuf.g0.u0(java.lang.Object, com.google.crypto.tink.shaded.protobuf.Writer):void");
    }

    public final <K, V> void v0(Writer writer, int i, Object obj, int i2) throws IOException {
        if (obj != null) {
            writer.K(i, this.q.c(t(i2)), this.q.h(obj));
        }
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    public final int w(T t) {
        int i;
        int i2;
        int j;
        int e;
        int L;
        boolean z;
        int f;
        int i3;
        int V;
        int X;
        Unsafe unsafe = s;
        int i4 = -1;
        int i5 = 0;
        int i6 = 0;
        int i7 = 0;
        while (i5 < this.a.length) {
            int r0 = r0(i5);
            int T = T(i5);
            int q0 = q0(r0);
            if (q0 <= 17) {
                i = this.a[i5 + 2];
                int i8 = 1048575 & i;
                int i9 = 1 << (i >>> 20);
                if (i8 != i4) {
                    i7 = unsafe.getInt(t, i8);
                    i4 = i8;
                }
                i2 = i9;
            } else {
                i = (!this.i || q0 < FieldType.DOUBLE_LIST_PACKED.id() || q0 > FieldType.SINT64_LIST_PACKED.id()) ? 0 : this.a[i5 + 2] & 1048575;
                i2 = 0;
            }
            long U = U(r0);
            int i10 = i4;
            switch (q0) {
                case 0:
                    if ((i7 & i2) == 0) {
                        break;
                    } else {
                        j = CodedOutputStream.j(T, Utils.DOUBLE_EPSILON);
                        i6 += j;
                        break;
                    }
                case 1:
                    if ((i7 & i2) == 0) {
                        break;
                    } else {
                        j = CodedOutputStream.r(T, Utils.FLOAT_EPSILON);
                        i6 += j;
                        break;
                    }
                case 2:
                    if ((i7 & i2) == 0) {
                        break;
                    } else {
                        j = CodedOutputStream.y(T, unsafe.getLong(t, U));
                        i6 += j;
                        break;
                    }
                case 3:
                    if ((i7 & i2) == 0) {
                        break;
                    } else {
                        j = CodedOutputStream.Y(T, unsafe.getLong(t, U));
                        i6 += j;
                        break;
                    }
                case 4:
                    if ((i7 & i2) == 0) {
                        break;
                    } else {
                        j = CodedOutputStream.w(T, unsafe.getInt(t, U));
                        i6 += j;
                        break;
                    }
                case 5:
                    if ((i7 & i2) == 0) {
                        break;
                    } else {
                        j = CodedOutputStream.p(T, 0L);
                        i6 += j;
                        break;
                    }
                case 6:
                    if ((i7 & i2) != 0) {
                        j = CodedOutputStream.n(T, 0);
                        i6 += j;
                        break;
                    }
                    break;
                case 7:
                    if ((i7 & i2) != 0) {
                        e = CodedOutputStream.e(T, true);
                        i6 += e;
                    }
                    break;
                case 8:
                    if ((i7 & i2) != 0) {
                        Object object = unsafe.getObject(t, U);
                        if (object instanceof ByteString) {
                            e = CodedOutputStream.h(T, (ByteString) object);
                        } else {
                            e = CodedOutputStream.T(T, (String) object);
                        }
                        i6 += e;
                    }
                    break;
                case 9:
                    if ((i7 & i2) != 0) {
                        e = o0.o(T, unsafe.getObject(t, U), u(i5));
                        i6 += e;
                    }
                    break;
                case 10:
                    if ((i7 & i2) != 0) {
                        e = CodedOutputStream.h(T, (ByteString) unsafe.getObject(t, U));
                        i6 += e;
                    }
                    break;
                case 11:
                    if ((i7 & i2) != 0) {
                        e = CodedOutputStream.W(T, unsafe.getInt(t, U));
                        i6 += e;
                    }
                    break;
                case 12:
                    if ((i7 & i2) != 0) {
                        e = CodedOutputStream.l(T, unsafe.getInt(t, U));
                        i6 += e;
                    }
                    break;
                case 13:
                    if ((i7 & i2) != 0) {
                        L = CodedOutputStream.L(T, 0);
                        i6 += L;
                    }
                    break;
                case 14:
                    if ((i7 & i2) != 0) {
                        e = CodedOutputStream.N(T, 0L);
                        i6 += e;
                    }
                    break;
                case 15:
                    if ((i7 & i2) != 0) {
                        e = CodedOutputStream.P(T, unsafe.getInt(t, U));
                        i6 += e;
                    }
                    break;
                case 16:
                    if ((i7 & i2) != 0) {
                        e = CodedOutputStream.R(T, unsafe.getLong(t, U));
                        i6 += e;
                    }
                    break;
                case 17:
                    if ((i7 & i2) != 0) {
                        e = CodedOutputStream.t(T, (e0) unsafe.getObject(t, U), u(i5));
                        i6 += e;
                    }
                    break;
                case 18:
                    e = o0.h(T, (List) unsafe.getObject(t, U), false);
                    i6 += e;
                    break;
                case 19:
                    z = false;
                    f = o0.f(T, (List) unsafe.getObject(t, U), false);
                    i6 += f;
                    break;
                case 20:
                    z = false;
                    f = o0.m(T, (List) unsafe.getObject(t, U), false);
                    i6 += f;
                    break;
                case 21:
                    z = false;
                    f = o0.x(T, (List) unsafe.getObject(t, U), false);
                    i6 += f;
                    break;
                case 22:
                    z = false;
                    f = o0.k(T, (List) unsafe.getObject(t, U), false);
                    i6 += f;
                    break;
                case 23:
                    z = false;
                    f = o0.h(T, (List) unsafe.getObject(t, U), false);
                    i6 += f;
                    break;
                case 24:
                    z = false;
                    f = o0.f(T, (List) unsafe.getObject(t, U), false);
                    i6 += f;
                    break;
                case 25:
                    z = false;
                    f = o0.a(T, (List) unsafe.getObject(t, U), false);
                    i6 += f;
                    break;
                case 26:
                    e = o0.u(T, (List) unsafe.getObject(t, U));
                    i6 += e;
                    break;
                case 27:
                    e = o0.p(T, (List) unsafe.getObject(t, U), u(i5));
                    i6 += e;
                    break;
                case 28:
                    e = o0.c(T, (List) unsafe.getObject(t, U));
                    i6 += e;
                    break;
                case 29:
                    e = o0.v(T, (List) unsafe.getObject(t, U), false);
                    i6 += e;
                    break;
                case 30:
                    z = false;
                    f = o0.d(T, (List) unsafe.getObject(t, U), false);
                    i6 += f;
                    break;
                case 31:
                    z = false;
                    f = o0.f(T, (List) unsafe.getObject(t, U), false);
                    i6 += f;
                    break;
                case 32:
                    z = false;
                    f = o0.h(T, (List) unsafe.getObject(t, U), false);
                    i6 += f;
                    break;
                case 33:
                    z = false;
                    f = o0.q(T, (List) unsafe.getObject(t, U), false);
                    i6 += f;
                    break;
                case 34:
                    z = false;
                    f = o0.s(T, (List) unsafe.getObject(t, U), false);
                    i6 += f;
                    break;
                case 35:
                    i3 = o0.i((List) unsafe.getObject(t, U));
                    if (i3 > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i, i3);
                        }
                        V = CodedOutputStream.V(T);
                        X = CodedOutputStream.X(i3);
                        L = V + X + i3;
                        i6 += L;
                    }
                    break;
                case 36:
                    i3 = o0.g((List) unsafe.getObject(t, U));
                    if (i3 > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i, i3);
                        }
                        V = CodedOutputStream.V(T);
                        X = CodedOutputStream.X(i3);
                        L = V + X + i3;
                        i6 += L;
                    }
                    break;
                case 37:
                    i3 = o0.n((List) unsafe.getObject(t, U));
                    if (i3 > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i, i3);
                        }
                        V = CodedOutputStream.V(T);
                        X = CodedOutputStream.X(i3);
                        L = V + X + i3;
                        i6 += L;
                    }
                    break;
                case 38:
                    i3 = o0.y((List) unsafe.getObject(t, U));
                    if (i3 > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i, i3);
                        }
                        V = CodedOutputStream.V(T);
                        X = CodedOutputStream.X(i3);
                        L = V + X + i3;
                        i6 += L;
                    }
                    break;
                case 39:
                    i3 = o0.l((List) unsafe.getObject(t, U));
                    if (i3 > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i, i3);
                        }
                        V = CodedOutputStream.V(T);
                        X = CodedOutputStream.X(i3);
                        L = V + X + i3;
                        i6 += L;
                    }
                    break;
                case 40:
                    i3 = o0.i((List) unsafe.getObject(t, U));
                    if (i3 > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i, i3);
                        }
                        V = CodedOutputStream.V(T);
                        X = CodedOutputStream.X(i3);
                        L = V + X + i3;
                        i6 += L;
                    }
                    break;
                case 41:
                    i3 = o0.g((List) unsafe.getObject(t, U));
                    if (i3 > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i, i3);
                        }
                        V = CodedOutputStream.V(T);
                        X = CodedOutputStream.X(i3);
                        L = V + X + i3;
                        i6 += L;
                    }
                    break;
                case 42:
                    i3 = o0.b((List) unsafe.getObject(t, U));
                    if (i3 > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i, i3);
                        }
                        V = CodedOutputStream.V(T);
                        X = CodedOutputStream.X(i3);
                        L = V + X + i3;
                        i6 += L;
                    }
                    break;
                case 43:
                    i3 = o0.w((List) unsafe.getObject(t, U));
                    if (i3 > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i, i3);
                        }
                        V = CodedOutputStream.V(T);
                        X = CodedOutputStream.X(i3);
                        L = V + X + i3;
                        i6 += L;
                    }
                    break;
                case 44:
                    i3 = o0.e((List) unsafe.getObject(t, U));
                    if (i3 > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i, i3);
                        }
                        V = CodedOutputStream.V(T);
                        X = CodedOutputStream.X(i3);
                        L = V + X + i3;
                        i6 += L;
                    }
                    break;
                case 45:
                    i3 = o0.g((List) unsafe.getObject(t, U));
                    if (i3 > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i, i3);
                        }
                        V = CodedOutputStream.V(T);
                        X = CodedOutputStream.X(i3);
                        L = V + X + i3;
                        i6 += L;
                    }
                    break;
                case 46:
                    i3 = o0.i((List) unsafe.getObject(t, U));
                    if (i3 > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i, i3);
                        }
                        V = CodedOutputStream.V(T);
                        X = CodedOutputStream.X(i3);
                        L = V + X + i3;
                        i6 += L;
                    }
                    break;
                case 47:
                    i3 = o0.r((List) unsafe.getObject(t, U));
                    if (i3 > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i, i3);
                        }
                        V = CodedOutputStream.V(T);
                        X = CodedOutputStream.X(i3);
                        L = V + X + i3;
                        i6 += L;
                    }
                    break;
                case 48:
                    i3 = o0.t((List) unsafe.getObject(t, U));
                    if (i3 > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i, i3);
                        }
                        V = CodedOutputStream.V(T);
                        X = CodedOutputStream.X(i3);
                        L = V + X + i3;
                        i6 += L;
                    }
                    break;
                case 49:
                    e = o0.j(T, (List) unsafe.getObject(t, U), u(i5));
                    i6 += e;
                    break;
                case 50:
                    e = this.q.f(T, unsafe.getObject(t, U), t(i5));
                    i6 += e;
                    break;
                case 51:
                    if (H(t, T, i5)) {
                        e = CodedOutputStream.j(T, Utils.DOUBLE_EPSILON);
                        i6 += e;
                    }
                    break;
                case 52:
                    if (H(t, T, i5)) {
                        e = CodedOutputStream.r(T, Utils.FLOAT_EPSILON);
                        i6 += e;
                    }
                    break;
                case 53:
                    if (H(t, T, i5)) {
                        e = CodedOutputStream.y(T, Z(t, U));
                        i6 += e;
                    }
                    break;
                case 54:
                    if (H(t, T, i5)) {
                        e = CodedOutputStream.Y(T, Z(t, U));
                        i6 += e;
                    }
                    break;
                case 55:
                    if (H(t, T, i5)) {
                        e = CodedOutputStream.w(T, Y(t, U));
                        i6 += e;
                    }
                    break;
                case 56:
                    if (H(t, T, i5)) {
                        e = CodedOutputStream.p(T, 0L);
                        i6 += e;
                    }
                    break;
                case 57:
                    if (H(t, T, i5)) {
                        L = CodedOutputStream.n(T, 0);
                        i6 += L;
                    }
                    break;
                case 58:
                    if (H(t, T, i5)) {
                        e = CodedOutputStream.e(T, true);
                        i6 += e;
                    }
                    break;
                case 59:
                    if (H(t, T, i5)) {
                        Object object2 = unsafe.getObject(t, U);
                        if (object2 instanceof ByteString) {
                            e = CodedOutputStream.h(T, (ByteString) object2);
                        } else {
                            e = CodedOutputStream.T(T, (String) object2);
                        }
                        i6 += e;
                    }
                    break;
                case 60:
                    if (H(t, T, i5)) {
                        e = o0.o(T, unsafe.getObject(t, U), u(i5));
                        i6 += e;
                    }
                    break;
                case 61:
                    if (H(t, T, i5)) {
                        e = CodedOutputStream.h(T, (ByteString) unsafe.getObject(t, U));
                        i6 += e;
                    }
                    break;
                case 62:
                    if (H(t, T, i5)) {
                        e = CodedOutputStream.W(T, Y(t, U));
                        i6 += e;
                    }
                    break;
                case 63:
                    if (H(t, T, i5)) {
                        e = CodedOutputStream.l(T, Y(t, U));
                        i6 += e;
                    }
                    break;
                case 64:
                    if (H(t, T, i5)) {
                        L = CodedOutputStream.L(T, 0);
                        i6 += L;
                    }
                    break;
                case 65:
                    if (H(t, T, i5)) {
                        e = CodedOutputStream.N(T, 0L);
                        i6 += e;
                    }
                    break;
                case 66:
                    if (H(t, T, i5)) {
                        e = CodedOutputStream.P(T, Y(t, U));
                        i6 += e;
                    }
                    break;
                case 67:
                    if (H(t, T, i5)) {
                        e = CodedOutputStream.R(T, Z(t, U));
                        i6 += e;
                    }
                    break;
                case 68:
                    if (H(t, T, i5)) {
                        e = CodedOutputStream.t(T, (e0) unsafe.getObject(t, U), u(i5));
                        i6 += e;
                    }
                    break;
            }
            i5 += 3;
            i4 = i10;
        }
        int y = i6 + y(this.o, t);
        return this.f ? y + this.p.c(t).l() : y;
    }

    public final void w0(int i, Object obj, Writer writer) throws IOException {
        if (obj instanceof String) {
            writer.k(i, (String) obj);
        } else {
            writer.O(i, (ByteString) obj);
        }
    }

    public final int x(T t) {
        int j;
        int i;
        int V;
        int X;
        Unsafe unsafe = s;
        int i2 = 0;
        for (int i3 = 0; i3 < this.a.length; i3 += 3) {
            int r0 = r0(i3);
            int q0 = q0(r0);
            int T = T(i3);
            long U = U(r0);
            int i4 = (q0 < FieldType.DOUBLE_LIST_PACKED.id() || q0 > FieldType.SINT64_LIST_PACKED.id()) ? 0 : this.a[i3 + 2] & 1048575;
            switch (q0) {
                case 0:
                    if (B(t, i3)) {
                        j = CodedOutputStream.j(T, Utils.DOUBLE_EPSILON);
                        break;
                    } else {
                        continue;
                    }
                case 1:
                    if (B(t, i3)) {
                        j = CodedOutputStream.r(T, Utils.FLOAT_EPSILON);
                        break;
                    } else {
                        continue;
                    }
                case 2:
                    if (B(t, i3)) {
                        j = CodedOutputStream.y(T, t0.C(t, U));
                        break;
                    } else {
                        continue;
                    }
                case 3:
                    if (B(t, i3)) {
                        j = CodedOutputStream.Y(T, t0.C(t, U));
                        break;
                    } else {
                        continue;
                    }
                case 4:
                    if (B(t, i3)) {
                        j = CodedOutputStream.w(T, t0.A(t, U));
                        break;
                    } else {
                        continue;
                    }
                case 5:
                    if (B(t, i3)) {
                        j = CodedOutputStream.p(T, 0L);
                        break;
                    } else {
                        continue;
                    }
                case 6:
                    if (B(t, i3)) {
                        j = CodedOutputStream.n(T, 0);
                        break;
                    } else {
                        continue;
                    }
                case 7:
                    if (B(t, i3)) {
                        j = CodedOutputStream.e(T, true);
                        break;
                    } else {
                        continue;
                    }
                case 8:
                    if (B(t, i3)) {
                        Object E = t0.E(t, U);
                        if (E instanceof ByteString) {
                            j = CodedOutputStream.h(T, (ByteString) E);
                            break;
                        } else {
                            j = CodedOutputStream.T(T, (String) E);
                            break;
                        }
                    } else {
                        continue;
                    }
                case 9:
                    if (B(t, i3)) {
                        j = o0.o(T, t0.E(t, U), u(i3));
                        break;
                    } else {
                        continue;
                    }
                case 10:
                    if (B(t, i3)) {
                        j = CodedOutputStream.h(T, (ByteString) t0.E(t, U));
                        break;
                    } else {
                        continue;
                    }
                case 11:
                    if (B(t, i3)) {
                        j = CodedOutputStream.W(T, t0.A(t, U));
                        break;
                    } else {
                        continue;
                    }
                case 12:
                    if (B(t, i3)) {
                        j = CodedOutputStream.l(T, t0.A(t, U));
                        break;
                    } else {
                        continue;
                    }
                case 13:
                    if (B(t, i3)) {
                        j = CodedOutputStream.L(T, 0);
                        break;
                    } else {
                        continue;
                    }
                case 14:
                    if (B(t, i3)) {
                        j = CodedOutputStream.N(T, 0L);
                        break;
                    } else {
                        continue;
                    }
                case 15:
                    if (B(t, i3)) {
                        j = CodedOutputStream.P(T, t0.A(t, U));
                        break;
                    } else {
                        continue;
                    }
                case 16:
                    if (B(t, i3)) {
                        j = CodedOutputStream.R(T, t0.C(t, U));
                        break;
                    } else {
                        continue;
                    }
                case 17:
                    if (B(t, i3)) {
                        j = CodedOutputStream.t(T, (e0) t0.E(t, U), u(i3));
                        break;
                    } else {
                        continue;
                    }
                case 18:
                    j = o0.h(T, J(t, U), false);
                    break;
                case 19:
                    j = o0.f(T, J(t, U), false);
                    break;
                case 20:
                    j = o0.m(T, J(t, U), false);
                    break;
                case 21:
                    j = o0.x(T, J(t, U), false);
                    break;
                case 22:
                    j = o0.k(T, J(t, U), false);
                    break;
                case 23:
                    j = o0.h(T, J(t, U), false);
                    break;
                case 24:
                    j = o0.f(T, J(t, U), false);
                    break;
                case 25:
                    j = o0.a(T, J(t, U), false);
                    break;
                case 26:
                    j = o0.u(T, J(t, U));
                    break;
                case 27:
                    j = o0.p(T, J(t, U), u(i3));
                    break;
                case 28:
                    j = o0.c(T, J(t, U));
                    break;
                case 29:
                    j = o0.v(T, J(t, U), false);
                    break;
                case 30:
                    j = o0.d(T, J(t, U), false);
                    break;
                case 31:
                    j = o0.f(T, J(t, U), false);
                    break;
                case 32:
                    j = o0.h(T, J(t, U), false);
                    break;
                case 33:
                    j = o0.q(T, J(t, U), false);
                    break;
                case 34:
                    j = o0.s(T, J(t, U), false);
                    break;
                case 35:
                    i = o0.i((List) unsafe.getObject(t, U));
                    if (i > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i4, i);
                        }
                        V = CodedOutputStream.V(T);
                        X = CodedOutputStream.X(i);
                        j = V + X + i;
                        break;
                    } else {
                        continue;
                    }
                case 36:
                    i = o0.g((List) unsafe.getObject(t, U));
                    if (i > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i4, i);
                        }
                        V = CodedOutputStream.V(T);
                        X = CodedOutputStream.X(i);
                        j = V + X + i;
                        break;
                    } else {
                        continue;
                    }
                case 37:
                    i = o0.n((List) unsafe.getObject(t, U));
                    if (i > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i4, i);
                        }
                        V = CodedOutputStream.V(T);
                        X = CodedOutputStream.X(i);
                        j = V + X + i;
                        break;
                    } else {
                        continue;
                    }
                case 38:
                    i = o0.y((List) unsafe.getObject(t, U));
                    if (i > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i4, i);
                        }
                        V = CodedOutputStream.V(T);
                        X = CodedOutputStream.X(i);
                        j = V + X + i;
                        break;
                    } else {
                        continue;
                    }
                case 39:
                    i = o0.l((List) unsafe.getObject(t, U));
                    if (i > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i4, i);
                        }
                        V = CodedOutputStream.V(T);
                        X = CodedOutputStream.X(i);
                        j = V + X + i;
                        break;
                    } else {
                        continue;
                    }
                case 40:
                    i = o0.i((List) unsafe.getObject(t, U));
                    if (i > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i4, i);
                        }
                        V = CodedOutputStream.V(T);
                        X = CodedOutputStream.X(i);
                        j = V + X + i;
                        break;
                    } else {
                        continue;
                    }
                case 41:
                    i = o0.g((List) unsafe.getObject(t, U));
                    if (i > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i4, i);
                        }
                        V = CodedOutputStream.V(T);
                        X = CodedOutputStream.X(i);
                        j = V + X + i;
                        break;
                    } else {
                        continue;
                    }
                case 42:
                    i = o0.b((List) unsafe.getObject(t, U));
                    if (i > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i4, i);
                        }
                        V = CodedOutputStream.V(T);
                        X = CodedOutputStream.X(i);
                        j = V + X + i;
                        break;
                    } else {
                        continue;
                    }
                case 43:
                    i = o0.w((List) unsafe.getObject(t, U));
                    if (i > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i4, i);
                        }
                        V = CodedOutputStream.V(T);
                        X = CodedOutputStream.X(i);
                        j = V + X + i;
                        break;
                    } else {
                        continue;
                    }
                case 44:
                    i = o0.e((List) unsafe.getObject(t, U));
                    if (i > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i4, i);
                        }
                        V = CodedOutputStream.V(T);
                        X = CodedOutputStream.X(i);
                        j = V + X + i;
                        break;
                    } else {
                        continue;
                    }
                case 45:
                    i = o0.g((List) unsafe.getObject(t, U));
                    if (i > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i4, i);
                        }
                        V = CodedOutputStream.V(T);
                        X = CodedOutputStream.X(i);
                        j = V + X + i;
                        break;
                    } else {
                        continue;
                    }
                case 46:
                    i = o0.i((List) unsafe.getObject(t, U));
                    if (i > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i4, i);
                        }
                        V = CodedOutputStream.V(T);
                        X = CodedOutputStream.X(i);
                        j = V + X + i;
                        break;
                    } else {
                        continue;
                    }
                case 47:
                    i = o0.r((List) unsafe.getObject(t, U));
                    if (i > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i4, i);
                        }
                        V = CodedOutputStream.V(T);
                        X = CodedOutputStream.X(i);
                        j = V + X + i;
                        break;
                    } else {
                        continue;
                    }
                case 48:
                    i = o0.t((List) unsafe.getObject(t, U));
                    if (i > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i4, i);
                        }
                        V = CodedOutputStream.V(T);
                        X = CodedOutputStream.X(i);
                        j = V + X + i;
                        break;
                    } else {
                        continue;
                    }
                case 49:
                    j = o0.j(T, J(t, U), u(i3));
                    break;
                case 50:
                    j = this.q.f(T, t0.E(t, U), t(i3));
                    break;
                case 51:
                    if (H(t, T, i3)) {
                        j = CodedOutputStream.j(T, Utils.DOUBLE_EPSILON);
                        break;
                    } else {
                        continue;
                    }
                case 52:
                    if (H(t, T, i3)) {
                        j = CodedOutputStream.r(T, Utils.FLOAT_EPSILON);
                        break;
                    } else {
                        continue;
                    }
                case 53:
                    if (H(t, T, i3)) {
                        j = CodedOutputStream.y(T, Z(t, U));
                        break;
                    } else {
                        continue;
                    }
                case 54:
                    if (H(t, T, i3)) {
                        j = CodedOutputStream.Y(T, Z(t, U));
                        break;
                    } else {
                        continue;
                    }
                case 55:
                    if (H(t, T, i3)) {
                        j = CodedOutputStream.w(T, Y(t, U));
                        break;
                    } else {
                        continue;
                    }
                case 56:
                    if (H(t, T, i3)) {
                        j = CodedOutputStream.p(T, 0L);
                        break;
                    } else {
                        continue;
                    }
                case 57:
                    if (H(t, T, i3)) {
                        j = CodedOutputStream.n(T, 0);
                        break;
                    } else {
                        continue;
                    }
                case 58:
                    if (H(t, T, i3)) {
                        j = CodedOutputStream.e(T, true);
                        break;
                    } else {
                        continue;
                    }
                case 59:
                    if (H(t, T, i3)) {
                        Object E2 = t0.E(t, U);
                        if (E2 instanceof ByteString) {
                            j = CodedOutputStream.h(T, (ByteString) E2);
                            break;
                        } else {
                            j = CodedOutputStream.T(T, (String) E2);
                            break;
                        }
                    } else {
                        continue;
                    }
                case 60:
                    if (H(t, T, i3)) {
                        j = o0.o(T, t0.E(t, U), u(i3));
                        break;
                    } else {
                        continue;
                    }
                case 61:
                    if (H(t, T, i3)) {
                        j = CodedOutputStream.h(T, (ByteString) t0.E(t, U));
                        break;
                    } else {
                        continue;
                    }
                case 62:
                    if (H(t, T, i3)) {
                        j = CodedOutputStream.W(T, Y(t, U));
                        break;
                    } else {
                        continue;
                    }
                case 63:
                    if (H(t, T, i3)) {
                        j = CodedOutputStream.l(T, Y(t, U));
                        break;
                    } else {
                        continue;
                    }
                case 64:
                    if (H(t, T, i3)) {
                        j = CodedOutputStream.L(T, 0);
                        break;
                    } else {
                        continue;
                    }
                case 65:
                    if (H(t, T, i3)) {
                        j = CodedOutputStream.N(T, 0L);
                        break;
                    } else {
                        continue;
                    }
                case 66:
                    if (H(t, T, i3)) {
                        j = CodedOutputStream.P(T, Y(t, U));
                        break;
                    } else {
                        continue;
                    }
                case 67:
                    if (H(t, T, i3)) {
                        j = CodedOutputStream.R(T, Z(t, U));
                        break;
                    } else {
                        continue;
                    }
                case 68:
                    if (H(t, T, i3)) {
                        j = CodedOutputStream.t(T, (e0) t0.E(t, U), u(i3));
                        break;
                    } else {
                        continue;
                    }
                default:
            }
            i2 += j;
        }
        return i2 + y(this.o, t);
    }

    public final <UT, UB> void x0(q0<UT, UB> q0Var, T t, Writer writer) throws IOException {
        q0Var.t(q0Var.g(t), writer);
    }

    public final <UT, UB> int y(q0<UT, UB> q0Var, T t) {
        return q0Var.h(q0Var.g(t));
    }
}
