package com.google.crypto.tink.shaded.protobuf;

/* compiled from: GeneratedMessageInfoFactory.java */
/* loaded from: classes2.dex */
public class t implements c82 {
    public static final t a = new t();

    public static t c() {
        return a;
    }

    @Override // defpackage.c82
    public a82 a(Class<?> cls) {
        if (GeneratedMessageLite.class.isAssignableFrom(cls)) {
            try {
                return (a82) GeneratedMessageLite.n(cls.asSubclass(GeneratedMessageLite.class)).g();
            } catch (Exception e) {
                throw new RuntimeException("Unable to get message info for " + cls.getName(), e);
            }
        }
        throw new IllegalArgumentException("Unsupported message type: " + cls.getName());
    }

    @Override // defpackage.c82
    public boolean b(Class<?> cls) {
        return GeneratedMessageLite.class.isAssignableFrom(cls);
    }
}
