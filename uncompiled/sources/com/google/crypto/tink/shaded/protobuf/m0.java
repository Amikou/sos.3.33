package com.google.crypto.tink.shaded.protobuf;

import com.google.crypto.tink.shaded.protobuf.b0;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/* compiled from: Reader.java */
/* loaded from: classes2.dex */
public interface m0 {
    void A(List<Float> list) throws IOException;

    int B() throws IOException;

    boolean C() throws IOException;

    int D() throws IOException;

    void E(List<ByteString> list) throws IOException;

    void F(List<Double> list) throws IOException;

    long G() throws IOException;

    String H() throws IOException;

    void I(List<Long> list) throws IOException;

    <T> void J(List<T> list, n0<T> n0Var, n nVar) throws IOException;

    @Deprecated
    <T> T K(n0<T> n0Var, n nVar) throws IOException;

    <T> T L(n0<T> n0Var, n nVar) throws IOException;

    @Deprecated
    <T> T M(Class<T> cls, n nVar) throws IOException;

    <T> T N(Class<T> cls, n nVar) throws IOException;

    <K, V> void O(Map<K, V> map, b0.a<K, V> aVar, n nVar) throws IOException;

    @Deprecated
    <T> void P(List<T> list, n0<T> n0Var, n nVar) throws IOException;

    int a();

    void b(List<Integer> list) throws IOException;

    long c() throws IOException;

    long d() throws IOException;

    void e(List<Integer> list) throws IOException;

    void f(List<Long> list) throws IOException;

    void g(List<Integer> list) throws IOException;

    int h() throws IOException;

    boolean i() throws IOException;

    long j() throws IOException;

    void k(List<Long> list) throws IOException;

    int l() throws IOException;

    void m(List<Long> list) throws IOException;

    void n(List<Long> list) throws IOException;

    void o(List<Integer> list) throws IOException;

    void p(List<Integer> list) throws IOException;

    int q() throws IOException;

    void r(List<Integer> list) throws IOException;

    double readDouble() throws IOException;

    float readFloat() throws IOException;

    int s() throws IOException;

    long t() throws IOException;

    void u(List<Boolean> list) throws IOException;

    String v() throws IOException;

    int w() throws IOException;

    void x(List<String> list) throws IOException;

    void y(List<String> list) throws IOException;

    ByteString z() throws IOException;
}
