package com.google.crypto.tink.shaded.protobuf;

import com.google.crypto.tink.shaded.protobuf.b0;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Map;

/* compiled from: BinaryReader.java */
/* loaded from: classes2.dex */
public abstract class e implements m0 {

    /* compiled from: BinaryReader.java */
    /* loaded from: classes2.dex */
    public static final class b extends e {
        public final boolean a;
        public final byte[] b;
        public int c;
        public int d;
        public int e;
        public int f;

        public b(ByteBuffer byteBuffer, boolean z) {
            super();
            this.a = z;
            this.b = byteBuffer.array();
            this.c = byteBuffer.arrayOffset() + byteBuffer.position();
            this.d = byteBuffer.arrayOffset() + byteBuffer.limit();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.m0
        public void A(List<Float> list) throws IOException {
            int i;
            int i2;
            if (list instanceof s) {
                s sVar = (s) list;
                int b = WireFormat.b(this.e);
                if (b == 2) {
                    int b0 = b0();
                    l0(b0);
                    int i3 = this.c + b0;
                    while (this.c < i3) {
                        sVar.m(Float.intBitsToFloat(V()));
                    }
                    return;
                } else if (b == 5) {
                    do {
                        sVar.m(readFloat());
                        if (R()) {
                            return;
                        }
                        i2 = this.c;
                    } while (b0() == this.e);
                    this.c = i2;
                    return;
                } else {
                    throw InvalidProtocolBufferException.invalidWireType();
                }
            }
            int b2 = WireFormat.b(this.e);
            if (b2 == 2) {
                int b02 = b0();
                l0(b02);
                int i4 = this.c + b02;
                while (this.c < i4) {
                    list.add(Float.valueOf(Float.intBitsToFloat(V())));
                }
            } else if (b2 == 5) {
                do {
                    list.add(Float.valueOf(readFloat()));
                    if (R()) {
                        return;
                    }
                    i = this.c;
                } while (b0() == this.e);
                this.c = i;
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }

        @Override // com.google.crypto.tink.shaded.protobuf.m0
        public int B() throws IOException {
            g0(0);
            return b0();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.m0
        public boolean C() throws IOException {
            int i;
            if (R() || (i = this.e) == this.f) {
                return false;
            }
            int b = WireFormat.b(i);
            if (b == 0) {
                j0();
                return true;
            } else if (b == 1) {
                h0(8);
                return true;
            } else if (b == 2) {
                h0(b0());
                return true;
            } else if (b == 3) {
                i0();
                return true;
            } else if (b == 5) {
                h0(4);
                return true;
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }

        @Override // com.google.crypto.tink.shaded.protobuf.m0
        public int D() throws IOException {
            g0(5);
            return U();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.m0
        public void E(List<ByteString> list) throws IOException {
            int i;
            if (WireFormat.b(this.e) == 2) {
                do {
                    list.add(z());
                    if (R()) {
                        return;
                    }
                    i = this.c;
                } while (b0() == this.e);
                this.c = i;
                return;
            }
            throw InvalidProtocolBufferException.invalidWireType();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.m0
        public void F(List<Double> list) throws IOException {
            int i;
            int i2;
            if (list instanceof l) {
                l lVar = (l) list;
                int b = WireFormat.b(this.e);
                if (b == 1) {
                    do {
                        lVar.m(readDouble());
                        if (R()) {
                            return;
                        }
                        i2 = this.c;
                    } while (b0() == this.e);
                    this.c = i2;
                    return;
                } else if (b == 2) {
                    int b0 = b0();
                    m0(b0);
                    int i3 = this.c + b0;
                    while (this.c < i3) {
                        lVar.m(Double.longBitsToDouble(X()));
                    }
                    return;
                } else {
                    throw InvalidProtocolBufferException.invalidWireType();
                }
            }
            int b2 = WireFormat.b(this.e);
            if (b2 == 1) {
                do {
                    list.add(Double.valueOf(readDouble()));
                    if (R()) {
                        return;
                    }
                    i = this.c;
                } while (b0() == this.e);
                this.c = i;
            } else if (b2 == 2) {
                int b02 = b0();
                m0(b02);
                int i4 = this.c + b02;
                while (this.c < i4) {
                    list.add(Double.valueOf(Double.longBitsToDouble(X())));
                }
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }

        @Override // com.google.crypto.tink.shaded.protobuf.m0
        public long G() throws IOException {
            g0(0);
            return c0();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.m0
        public String H() throws IOException {
            return Z(true);
        }

        @Override // com.google.crypto.tink.shaded.protobuf.m0
        public void I(List<Long> list) throws IOException {
            int i;
            int i2;
            if (list instanceof a0) {
                a0 a0Var = (a0) list;
                int b = WireFormat.b(this.e);
                if (b == 1) {
                    do {
                        a0Var.n(d());
                        if (R()) {
                            return;
                        }
                        i2 = this.c;
                    } while (b0() == this.e);
                    this.c = i2;
                    return;
                } else if (b == 2) {
                    int b0 = b0();
                    m0(b0);
                    int i3 = this.c + b0;
                    while (this.c < i3) {
                        a0Var.n(X());
                    }
                    return;
                } else {
                    throw InvalidProtocolBufferException.invalidWireType();
                }
            }
            int b2 = WireFormat.b(this.e);
            if (b2 == 1) {
                do {
                    list.add(Long.valueOf(d()));
                    if (R()) {
                        return;
                    }
                    i = this.c;
                } while (b0() == this.e);
                this.c = i;
            } else if (b2 == 2) {
                int b02 = b0();
                m0(b02);
                int i4 = this.c + b02;
                while (this.c < i4) {
                    list.add(Long.valueOf(X()));
                }
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.google.crypto.tink.shaded.protobuf.m0
        public <T> void J(List<T> list, n0<T> n0Var, n nVar) throws IOException {
            int i;
            if (WireFormat.b(this.e) == 2) {
                int i2 = this.e;
                do {
                    list.add(Y(n0Var, nVar));
                    if (R()) {
                        return;
                    }
                    i = this.c;
                } while (b0() == i2);
                this.c = i;
                return;
            }
            throw InvalidProtocolBufferException.invalidWireType();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.m0
        public <T> T K(n0<T> n0Var, n nVar) throws IOException {
            g0(3);
            return (T) T(n0Var, nVar);
        }

        @Override // com.google.crypto.tink.shaded.protobuf.m0
        public <T> T L(n0<T> n0Var, n nVar) throws IOException {
            g0(2);
            return (T) Y(n0Var, nVar);
        }

        @Override // com.google.crypto.tink.shaded.protobuf.m0
        public <T> T M(Class<T> cls, n nVar) throws IOException {
            g0(3);
            return (T) T(k0.a().d(cls), nVar);
        }

        @Override // com.google.crypto.tink.shaded.protobuf.m0
        public <T> T N(Class<T> cls, n nVar) throws IOException {
            g0(2);
            return (T) Y(k0.a().d(cls), nVar);
        }

        @Override // com.google.crypto.tink.shaded.protobuf.m0
        public <K, V> void O(Map<K, V> map, b0.a<K, V> aVar, n nVar) throws IOException {
            g0(2);
            int b0 = b0();
            e0(b0);
            int i = this.d;
            this.d = this.c + b0;
            try {
                throw null;
            } catch (Throwable th) {
                this.d = i;
                throw th;
            }
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.google.crypto.tink.shaded.protobuf.m0
        public <T> void P(List<T> list, n0<T> n0Var, n nVar) throws IOException {
            int i;
            if (WireFormat.b(this.e) == 3) {
                int i2 = this.e;
                do {
                    list.add(T(n0Var, nVar));
                    if (R()) {
                        return;
                    }
                    i = this.c;
                } while (b0() == i2);
                this.c = i;
                return;
            }
            throw InvalidProtocolBufferException.invalidWireType();
        }

        public final boolean R() {
            return this.c == this.d;
        }

        public final byte S() throws IOException {
            int i = this.c;
            if (i != this.d) {
                byte[] bArr = this.b;
                this.c = i + 1;
                return bArr[i];
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }

        public final <T> T T(n0<T> n0Var, n nVar) throws IOException {
            int i = this.f;
            this.f = WireFormat.c(WireFormat.a(this.e), 4);
            try {
                T c = n0Var.c();
                n0Var.j(c, this, nVar);
                n0Var.e(c);
                if (this.e == this.f) {
                    return c;
                }
                throw InvalidProtocolBufferException.parseFailure();
            } finally {
                this.f = i;
            }
        }

        public final int U() throws IOException {
            e0(4);
            return V();
        }

        public final int V() {
            int i = this.c;
            byte[] bArr = this.b;
            this.c = i + 4;
            return ((bArr[i + 3] & 255) << 24) | (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << 16);
        }

        public final long W() throws IOException {
            e0(8);
            return X();
        }

        public final long X() {
            int i = this.c;
            byte[] bArr = this.b;
            this.c = i + 8;
            return ((bArr[i + 7] & 255) << 56) | (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << 16) | ((bArr[i + 3] & 255) << 24) | ((bArr[i + 4] & 255) << 32) | ((bArr[i + 5] & 255) << 40) | ((bArr[i + 6] & 255) << 48);
        }

        public final <T> T Y(n0<T> n0Var, n nVar) throws IOException {
            int b0 = b0();
            e0(b0);
            int i = this.d;
            int i2 = this.c + b0;
            this.d = i2;
            try {
                T c = n0Var.c();
                n0Var.j(c, this, nVar);
                n0Var.e(c);
                if (this.c == i2) {
                    return c;
                }
                throw InvalidProtocolBufferException.parseFailure();
            } finally {
                this.d = i;
            }
        }

        public String Z(boolean z) throws IOException {
            g0(2);
            int b0 = b0();
            if (b0 == 0) {
                return "";
            }
            e0(b0);
            if (z) {
                byte[] bArr = this.b;
                int i = this.c;
                if (!Utf8.t(bArr, i, i + b0)) {
                    throw InvalidProtocolBufferException.invalidUtf8();
                }
            }
            String str = new String(this.b, this.c, b0, v.a);
            this.c += b0;
            return str;
        }

        @Override // com.google.crypto.tink.shaded.protobuf.m0
        public int a() {
            return this.e;
        }

        public void a0(List<String> list, boolean z) throws IOException {
            int i;
            int i2;
            if (WireFormat.b(this.e) == 2) {
                if ((list instanceof dz1) && !z) {
                    dz1 dz1Var = (dz1) list;
                    do {
                        dz1Var.i1(z());
                        if (R()) {
                            return;
                        }
                        i2 = this.c;
                    } while (b0() == this.e);
                    this.c = i2;
                    return;
                }
                do {
                    list.add(Z(z));
                    if (R()) {
                        return;
                    }
                    i = this.c;
                } while (b0() == this.e);
                this.c = i;
                return;
            }
            throw InvalidProtocolBufferException.invalidWireType();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.m0
        public void b(List<Integer> list) throws IOException {
            int i;
            int i2;
            if (list instanceof u) {
                u uVar = (u) list;
                int b = WireFormat.b(this.e);
                if (b != 0) {
                    if (b == 2) {
                        int b0 = this.c + b0();
                        while (this.c < b0) {
                            uVar.X(i.b(b0()));
                        }
                        return;
                    }
                    throw InvalidProtocolBufferException.invalidWireType();
                }
                do {
                    uVar.X(s());
                    if (R()) {
                        return;
                    }
                    i2 = this.c;
                } while (b0() == this.e);
                this.c = i2;
                return;
            }
            int b2 = WireFormat.b(this.e);
            if (b2 != 0) {
                if (b2 == 2) {
                    int b02 = this.c + b0();
                    while (this.c < b02) {
                        list.add(Integer.valueOf(i.b(b0())));
                    }
                    return;
                }
                throw InvalidProtocolBufferException.invalidWireType();
            }
            do {
                list.add(Integer.valueOf(s()));
                if (R()) {
                    return;
                }
                i = this.c;
            } while (b0() == this.e);
            this.c = i;
        }

        public final int b0() throws IOException {
            int i;
            int i2 = this.c;
            int i3 = this.d;
            if (i3 != i2) {
                byte[] bArr = this.b;
                int i4 = i2 + 1;
                byte b = bArr[i2];
                if (b >= 0) {
                    this.c = i4;
                    return b;
                } else if (i3 - i4 < 9) {
                    return (int) d0();
                } else {
                    int i5 = i4 + 1;
                    int i6 = b ^ (bArr[i4] << 7);
                    if (i6 < 0) {
                        i = i6 ^ (-128);
                    } else {
                        int i7 = i5 + 1;
                        int i8 = i6 ^ (bArr[i5] << 14);
                        if (i8 >= 0) {
                            i = i8 ^ 16256;
                        } else {
                            i5 = i7 + 1;
                            int i9 = i8 ^ (bArr[i7] << 21);
                            if (i9 < 0) {
                                i = i9 ^ (-2080896);
                            } else {
                                i7 = i5 + 1;
                                byte b2 = bArr[i5];
                                i = (i9 ^ (b2 << 28)) ^ 266354560;
                                if (b2 < 0) {
                                    i5 = i7 + 1;
                                    if (bArr[i7] < 0) {
                                        i7 = i5 + 1;
                                        if (bArr[i5] < 0) {
                                            i5 = i7 + 1;
                                            if (bArr[i7] < 0) {
                                                i7 = i5 + 1;
                                                if (bArr[i5] < 0) {
                                                    i5 = i7 + 1;
                                                    if (bArr[i7] < 0) {
                                                        throw InvalidProtocolBufferException.malformedVarint();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        i5 = i7;
                    }
                    this.c = i5;
                    return i;
                }
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.m0
        public long c() throws IOException {
            g0(0);
            return c0();
        }

        public long c0() throws IOException {
            long j;
            long j2;
            long j3;
            int i;
            int i2 = this.c;
            int i3 = this.d;
            if (i3 != i2) {
                byte[] bArr = this.b;
                int i4 = i2 + 1;
                byte b = bArr[i2];
                if (b >= 0) {
                    this.c = i4;
                    return b;
                } else if (i3 - i4 < 9) {
                    return d0();
                } else {
                    int i5 = i4 + 1;
                    int i6 = b ^ (bArr[i4] << 7);
                    if (i6 >= 0) {
                        int i7 = i5 + 1;
                        int i8 = i6 ^ (bArr[i5] << 14);
                        if (i8 >= 0) {
                            i5 = i7;
                            j = i8 ^ 16256;
                        } else {
                            i5 = i7 + 1;
                            int i9 = i8 ^ (bArr[i7] << 21);
                            if (i9 < 0) {
                                i = i9 ^ (-2080896);
                            } else {
                                long j4 = i9;
                                int i10 = i5 + 1;
                                long j5 = j4 ^ (bArr[i5] << 28);
                                if (j5 >= 0) {
                                    j3 = 266354560;
                                } else {
                                    i5 = i10 + 1;
                                    long j6 = j5 ^ (bArr[i10] << 35);
                                    if (j6 < 0) {
                                        j2 = -34093383808L;
                                    } else {
                                        i10 = i5 + 1;
                                        j5 = j6 ^ (bArr[i5] << 42);
                                        if (j5 >= 0) {
                                            j3 = 4363953127296L;
                                        } else {
                                            i5 = i10 + 1;
                                            j6 = j5 ^ (bArr[i10] << 49);
                                            if (j6 < 0) {
                                                j2 = -558586000294016L;
                                            } else {
                                                int i11 = i5 + 1;
                                                long j7 = (j6 ^ (bArr[i5] << 56)) ^ 71499008037633920L;
                                                if (j7 < 0) {
                                                    i5 = i11 + 1;
                                                    if (bArr[i11] < 0) {
                                                        throw InvalidProtocolBufferException.malformedVarint();
                                                    }
                                                } else {
                                                    i5 = i11;
                                                }
                                                j = j7;
                                            }
                                        }
                                    }
                                    j = j6 ^ j2;
                                }
                                j = j5 ^ j3;
                                i5 = i10;
                            }
                        }
                        this.c = i5;
                        return j;
                    }
                    i = i6 ^ (-128);
                    j = i;
                    this.c = i5;
                    return j;
                }
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.m0
        public long d() throws IOException {
            g0(1);
            return W();
        }

        public final long d0() throws IOException {
            long j = 0;
            for (int i = 0; i < 64; i += 7) {
                byte S = S();
                j |= (S & Byte.MAX_VALUE) << i;
                if ((S & 128) == 0) {
                    return j;
                }
            }
            throw InvalidProtocolBufferException.malformedVarint();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.m0
        public void e(List<Integer> list) throws IOException {
            int i;
            int i2;
            if (list instanceof u) {
                u uVar = (u) list;
                int b = WireFormat.b(this.e);
                if (b == 2) {
                    int b0 = b0();
                    l0(b0);
                    int i3 = this.c + b0;
                    while (this.c < i3) {
                        uVar.X(V());
                    }
                    return;
                } else if (b == 5) {
                    do {
                        uVar.X(D());
                        if (R()) {
                            return;
                        }
                        i2 = this.c;
                    } while (b0() == this.e);
                    this.c = i2;
                    return;
                } else {
                    throw InvalidProtocolBufferException.invalidWireType();
                }
            }
            int b2 = WireFormat.b(this.e);
            if (b2 == 2) {
                int b02 = b0();
                l0(b02);
                int i4 = this.c + b02;
                while (this.c < i4) {
                    list.add(Integer.valueOf(V()));
                }
            } else if (b2 == 5) {
                do {
                    list.add(Integer.valueOf(D()));
                    if (R()) {
                        return;
                    }
                    i = this.c;
                } while (b0() == this.e);
                this.c = i;
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }

        public final void e0(int i) throws IOException {
            if (i < 0 || i > this.d - this.c) {
                throw InvalidProtocolBufferException.truncatedMessage();
            }
        }

        @Override // com.google.crypto.tink.shaded.protobuf.m0
        public void f(List<Long> list) throws IOException {
            int i;
            int i2;
            if (list instanceof a0) {
                a0 a0Var = (a0) list;
                int b = WireFormat.b(this.e);
                if (b != 0) {
                    if (b == 2) {
                        int b0 = this.c + b0();
                        while (this.c < b0) {
                            a0Var.n(i.c(c0()));
                        }
                        return;
                    }
                    throw InvalidProtocolBufferException.invalidWireType();
                }
                do {
                    a0Var.n(t());
                    if (R()) {
                        return;
                    }
                    i2 = this.c;
                } while (b0() == this.e);
                this.c = i2;
                return;
            }
            int b2 = WireFormat.b(this.e);
            if (b2 != 0) {
                if (b2 == 2) {
                    int b02 = this.c + b0();
                    while (this.c < b02) {
                        list.add(Long.valueOf(i.c(c0())));
                    }
                    return;
                }
                throw InvalidProtocolBufferException.invalidWireType();
            }
            do {
                list.add(Long.valueOf(t()));
                if (R()) {
                    return;
                }
                i = this.c;
            } while (b0() == this.e);
            this.c = i;
        }

        public final void f0(int i) throws IOException {
            if (this.c != i) {
                throw InvalidProtocolBufferException.truncatedMessage();
            }
        }

        @Override // com.google.crypto.tink.shaded.protobuf.m0
        public void g(List<Integer> list) throws IOException {
            int i;
            int i2;
            if (list instanceof u) {
                u uVar = (u) list;
                int b = WireFormat.b(this.e);
                if (b != 0) {
                    if (b == 2) {
                        int b0 = this.c + b0();
                        while (this.c < b0) {
                            uVar.X(b0());
                        }
                        return;
                    }
                    throw InvalidProtocolBufferException.invalidWireType();
                }
                do {
                    uVar.X(l());
                    if (R()) {
                        return;
                    }
                    i2 = this.c;
                } while (b0() == this.e);
                this.c = i2;
                return;
            }
            int b2 = WireFormat.b(this.e);
            if (b2 != 0) {
                if (b2 == 2) {
                    int b02 = this.c + b0();
                    while (this.c < b02) {
                        list.add(Integer.valueOf(b0()));
                    }
                    return;
                }
                throw InvalidProtocolBufferException.invalidWireType();
            }
            do {
                list.add(Integer.valueOf(l()));
                if (R()) {
                    return;
                }
                i = this.c;
            } while (b0() == this.e);
            this.c = i;
        }

        public final void g0(int i) throws IOException {
            if (WireFormat.b(this.e) != i) {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }

        @Override // com.google.crypto.tink.shaded.protobuf.m0
        public int h() throws IOException {
            g0(5);
            return U();
        }

        public final void h0(int i) throws IOException {
            e0(i);
            this.c += i;
        }

        @Override // com.google.crypto.tink.shaded.protobuf.m0
        public boolean i() throws IOException {
            g0(0);
            return b0() != 0;
        }

        public final void i0() throws IOException {
            int i = this.f;
            this.f = WireFormat.c(WireFormat.a(this.e), 4);
            while (w() != Integer.MAX_VALUE && C()) {
            }
            if (this.e == this.f) {
                this.f = i;
                return;
            }
            throw InvalidProtocolBufferException.parseFailure();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.m0
        public long j() throws IOException {
            g0(1);
            return W();
        }

        public final void j0() throws IOException {
            int i = this.d;
            int i2 = this.c;
            if (i - i2 >= 10) {
                byte[] bArr = this.b;
                int i3 = 0;
                while (i3 < 10) {
                    int i4 = i2 + 1;
                    if (bArr[i2] >= 0) {
                        this.c = i4;
                        return;
                    } else {
                        i3++;
                        i2 = i4;
                    }
                }
            }
            k0();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.m0
        public void k(List<Long> list) throws IOException {
            int i;
            int i2;
            if (list instanceof a0) {
                a0 a0Var = (a0) list;
                int b = WireFormat.b(this.e);
                if (b == 0) {
                    do {
                        a0Var.n(c());
                        if (R()) {
                            return;
                        }
                        i2 = this.c;
                    } while (b0() == this.e);
                    this.c = i2;
                    return;
                } else if (b == 2) {
                    int b0 = this.c + b0();
                    while (this.c < b0) {
                        a0Var.n(c0());
                    }
                    f0(b0);
                    return;
                } else {
                    throw InvalidProtocolBufferException.invalidWireType();
                }
            }
            int b2 = WireFormat.b(this.e);
            if (b2 == 0) {
                do {
                    list.add(Long.valueOf(c()));
                    if (R()) {
                        return;
                    }
                    i = this.c;
                } while (b0() == this.e);
                this.c = i;
            } else if (b2 == 2) {
                int b02 = this.c + b0();
                while (this.c < b02) {
                    list.add(Long.valueOf(c0()));
                }
                f0(b02);
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }

        public final void k0() throws IOException {
            for (int i = 0; i < 10; i++) {
                if (S() >= 0) {
                    return;
                }
            }
            throw InvalidProtocolBufferException.malformedVarint();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.m0
        public int l() throws IOException {
            g0(0);
            return b0();
        }

        public final void l0(int i) throws IOException {
            e0(i);
            if ((i & 3) != 0) {
                throw InvalidProtocolBufferException.parseFailure();
            }
        }

        @Override // com.google.crypto.tink.shaded.protobuf.m0
        public void m(List<Long> list) throws IOException {
            int i;
            int i2;
            if (list instanceof a0) {
                a0 a0Var = (a0) list;
                int b = WireFormat.b(this.e);
                if (b == 0) {
                    do {
                        a0Var.n(G());
                        if (R()) {
                            return;
                        }
                        i2 = this.c;
                    } while (b0() == this.e);
                    this.c = i2;
                    return;
                } else if (b == 2) {
                    int b0 = this.c + b0();
                    while (this.c < b0) {
                        a0Var.n(c0());
                    }
                    f0(b0);
                    return;
                } else {
                    throw InvalidProtocolBufferException.invalidWireType();
                }
            }
            int b2 = WireFormat.b(this.e);
            if (b2 == 0) {
                do {
                    list.add(Long.valueOf(G()));
                    if (R()) {
                        return;
                    }
                    i = this.c;
                } while (b0() == this.e);
                this.c = i;
            } else if (b2 == 2) {
                int b02 = this.c + b0();
                while (this.c < b02) {
                    list.add(Long.valueOf(c0()));
                }
                f0(b02);
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }

        public final void m0(int i) throws IOException {
            e0(i);
            if ((i & 7) != 0) {
                throw InvalidProtocolBufferException.parseFailure();
            }
        }

        @Override // com.google.crypto.tink.shaded.protobuf.m0
        public void n(List<Long> list) throws IOException {
            int i;
            int i2;
            if (list instanceof a0) {
                a0 a0Var = (a0) list;
                int b = WireFormat.b(this.e);
                if (b == 1) {
                    do {
                        a0Var.n(j());
                        if (R()) {
                            return;
                        }
                        i2 = this.c;
                    } while (b0() == this.e);
                    this.c = i2;
                    return;
                } else if (b == 2) {
                    int b0 = b0();
                    m0(b0);
                    int i3 = this.c + b0;
                    while (this.c < i3) {
                        a0Var.n(X());
                    }
                    return;
                } else {
                    throw InvalidProtocolBufferException.invalidWireType();
                }
            }
            int b2 = WireFormat.b(this.e);
            if (b2 == 1) {
                do {
                    list.add(Long.valueOf(j()));
                    if (R()) {
                        return;
                    }
                    i = this.c;
                } while (b0() == this.e);
                this.c = i;
            } else if (b2 == 2) {
                int b02 = b0();
                m0(b02);
                int i4 = this.c + b02;
                while (this.c < i4) {
                    list.add(Long.valueOf(X()));
                }
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }

        @Override // com.google.crypto.tink.shaded.protobuf.m0
        public void o(List<Integer> list) throws IOException {
            int i;
            int i2;
            if (list instanceof u) {
                u uVar = (u) list;
                int b = WireFormat.b(this.e);
                if (b == 0) {
                    do {
                        uVar.X(B());
                        if (R()) {
                            return;
                        }
                        i2 = this.c;
                    } while (b0() == this.e);
                    this.c = i2;
                    return;
                } else if (b == 2) {
                    int b0 = this.c + b0();
                    while (this.c < b0) {
                        uVar.X(b0());
                    }
                    f0(b0);
                    return;
                } else {
                    throw InvalidProtocolBufferException.invalidWireType();
                }
            }
            int b2 = WireFormat.b(this.e);
            if (b2 == 0) {
                do {
                    list.add(Integer.valueOf(B()));
                    if (R()) {
                        return;
                    }
                    i = this.c;
                } while (b0() == this.e);
                this.c = i;
            } else if (b2 == 2) {
                int b02 = this.c + b0();
                while (this.c < b02) {
                    list.add(Integer.valueOf(b0()));
                }
                f0(b02);
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }

        @Override // com.google.crypto.tink.shaded.protobuf.m0
        public void p(List<Integer> list) throws IOException {
            int i;
            int i2;
            if (list instanceof u) {
                u uVar = (u) list;
                int b = WireFormat.b(this.e);
                if (b != 0) {
                    if (b == 2) {
                        int b0 = this.c + b0();
                        while (this.c < b0) {
                            uVar.X(b0());
                        }
                        return;
                    }
                    throw InvalidProtocolBufferException.invalidWireType();
                }
                do {
                    uVar.X(q());
                    if (R()) {
                        return;
                    }
                    i2 = this.c;
                } while (b0() == this.e);
                this.c = i2;
                return;
            }
            int b2 = WireFormat.b(this.e);
            if (b2 != 0) {
                if (b2 == 2) {
                    int b02 = this.c + b0();
                    while (this.c < b02) {
                        list.add(Integer.valueOf(b0()));
                    }
                    return;
                }
                throw InvalidProtocolBufferException.invalidWireType();
            }
            do {
                list.add(Integer.valueOf(q()));
                if (R()) {
                    return;
                }
                i = this.c;
            } while (b0() == this.e);
            this.c = i;
        }

        @Override // com.google.crypto.tink.shaded.protobuf.m0
        public int q() throws IOException {
            g0(0);
            return b0();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.m0
        public void r(List<Integer> list) throws IOException {
            int i;
            int i2;
            if (list instanceof u) {
                u uVar = (u) list;
                int b = WireFormat.b(this.e);
                if (b == 2) {
                    int b0 = b0();
                    l0(b0);
                    int i3 = this.c + b0;
                    while (this.c < i3) {
                        uVar.X(V());
                    }
                    return;
                } else if (b == 5) {
                    do {
                        uVar.X(h());
                        if (R()) {
                            return;
                        }
                        i2 = this.c;
                    } while (b0() == this.e);
                    this.c = i2;
                    return;
                } else {
                    throw InvalidProtocolBufferException.invalidWireType();
                }
            }
            int b2 = WireFormat.b(this.e);
            if (b2 == 2) {
                int b02 = b0();
                l0(b02);
                int i4 = this.c + b02;
                while (this.c < i4) {
                    list.add(Integer.valueOf(V()));
                }
            } else if (b2 == 5) {
                do {
                    list.add(Integer.valueOf(h()));
                    if (R()) {
                        return;
                    }
                    i = this.c;
                } while (b0() == this.e);
                this.c = i;
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }

        @Override // com.google.crypto.tink.shaded.protobuf.m0
        public double readDouble() throws IOException {
            g0(1);
            return Double.longBitsToDouble(W());
        }

        @Override // com.google.crypto.tink.shaded.protobuf.m0
        public float readFloat() throws IOException {
            g0(5);
            return Float.intBitsToFloat(U());
        }

        @Override // com.google.crypto.tink.shaded.protobuf.m0
        public int s() throws IOException {
            g0(0);
            return i.b(b0());
        }

        @Override // com.google.crypto.tink.shaded.protobuf.m0
        public long t() throws IOException {
            g0(0);
            return i.c(c0());
        }

        @Override // com.google.crypto.tink.shaded.protobuf.m0
        public void u(List<Boolean> list) throws IOException {
            int i;
            int i2;
            if (list instanceof f) {
                f fVar = (f) list;
                int b = WireFormat.b(this.e);
                if (b != 0) {
                    if (b == 2) {
                        int b0 = this.c + b0();
                        while (this.c < b0) {
                            fVar.n(b0() != 0);
                        }
                        f0(b0);
                        return;
                    }
                    throw InvalidProtocolBufferException.invalidWireType();
                }
                do {
                    fVar.n(i());
                    if (R()) {
                        return;
                    }
                    i2 = this.c;
                } while (b0() == this.e);
                this.c = i2;
                return;
            }
            int b2 = WireFormat.b(this.e);
            if (b2 != 0) {
                if (b2 == 2) {
                    int b02 = this.c + b0();
                    while (this.c < b02) {
                        list.add(Boolean.valueOf(b0() != 0));
                    }
                    f0(b02);
                    return;
                }
                throw InvalidProtocolBufferException.invalidWireType();
            }
            do {
                list.add(Boolean.valueOf(i()));
                if (R()) {
                    return;
                }
                i = this.c;
            } while (b0() == this.e);
            this.c = i;
        }

        @Override // com.google.crypto.tink.shaded.protobuf.m0
        public String v() throws IOException {
            return Z(false);
        }

        @Override // com.google.crypto.tink.shaded.protobuf.m0
        public int w() throws IOException {
            if (R()) {
                return Integer.MAX_VALUE;
            }
            int b0 = b0();
            this.e = b0;
            if (b0 == this.f) {
                return Integer.MAX_VALUE;
            }
            return WireFormat.a(b0);
        }

        @Override // com.google.crypto.tink.shaded.protobuf.m0
        public void x(List<String> list) throws IOException {
            a0(list, false);
        }

        @Override // com.google.crypto.tink.shaded.protobuf.m0
        public void y(List<String> list) throws IOException {
            a0(list, true);
        }

        @Override // com.google.crypto.tink.shaded.protobuf.m0
        public ByteString z() throws IOException {
            ByteString copyFrom;
            g0(2);
            int b0 = b0();
            if (b0 == 0) {
                return ByteString.EMPTY;
            }
            e0(b0);
            if (this.a) {
                copyFrom = ByteString.wrap(this.b, this.c, b0);
            } else {
                copyFrom = ByteString.copyFrom(this.b, this.c, b0);
            }
            this.c += b0;
            return copyFrom;
        }
    }

    public static e Q(ByteBuffer byteBuffer, boolean z) {
        if (byteBuffer.hasArray()) {
            return new b(byteBuffer, z);
        }
        throw new IllegalArgumentException("Direct buffers not yet supported");
    }

    public e() {
    }
}
