package com.google.crypto.tink.shaded.protobuf;

/* compiled from: Parser.java */
/* loaded from: classes2.dex */
public interface j0<MessageType> {
    MessageType a(ByteString byteString, n nVar) throws InvalidProtocolBufferException;

    MessageType b(i iVar, n nVar) throws InvalidProtocolBufferException;
}
