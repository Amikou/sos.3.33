package com.google.crypto.tink.shaded.protobuf;

import com.google.crypto.tink.shaded.protobuf.WireFormat;
import com.google.crypto.tink.shaded.protobuf.e0;
import com.google.crypto.tink.shaded.protobuf.r.b;
import com.google.crypto.tink.shaded.protobuf.v;
import com.google.crypto.tink.shaded.protobuf.w;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: FieldSet.java */
/* loaded from: classes2.dex */
public final class r<T extends b<T>> {
    public static final r d = new r(true);
    public final p0<T, Object> a;
    public boolean b;
    public boolean c;

    /* compiled from: FieldSet.java */
    /* loaded from: classes2.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;
        public static final /* synthetic */ int[] b;

        static {
            int[] iArr = new int[WireFormat.FieldType.values().length];
            b = iArr;
            try {
                iArr[WireFormat.FieldType.DOUBLE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                b[WireFormat.FieldType.FLOAT.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                b[WireFormat.FieldType.INT64.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                b[WireFormat.FieldType.UINT64.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                b[WireFormat.FieldType.INT32.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                b[WireFormat.FieldType.FIXED64.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                b[WireFormat.FieldType.FIXED32.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                b[WireFormat.FieldType.BOOL.ordinal()] = 8;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                b[WireFormat.FieldType.GROUP.ordinal()] = 9;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                b[WireFormat.FieldType.MESSAGE.ordinal()] = 10;
            } catch (NoSuchFieldError unused10) {
            }
            try {
                b[WireFormat.FieldType.STRING.ordinal()] = 11;
            } catch (NoSuchFieldError unused11) {
            }
            try {
                b[WireFormat.FieldType.BYTES.ordinal()] = 12;
            } catch (NoSuchFieldError unused12) {
            }
            try {
                b[WireFormat.FieldType.UINT32.ordinal()] = 13;
            } catch (NoSuchFieldError unused13) {
            }
            try {
                b[WireFormat.FieldType.SFIXED32.ordinal()] = 14;
            } catch (NoSuchFieldError unused14) {
            }
            try {
                b[WireFormat.FieldType.SFIXED64.ordinal()] = 15;
            } catch (NoSuchFieldError unused15) {
            }
            try {
                b[WireFormat.FieldType.SINT32.ordinal()] = 16;
            } catch (NoSuchFieldError unused16) {
            }
            try {
                b[WireFormat.FieldType.SINT64.ordinal()] = 17;
            } catch (NoSuchFieldError unused17) {
            }
            try {
                b[WireFormat.FieldType.ENUM.ordinal()] = 18;
            } catch (NoSuchFieldError unused18) {
            }
            int[] iArr2 = new int[WireFormat.JavaType.values().length];
            a = iArr2;
            try {
                iArr2[WireFormat.JavaType.INT.ordinal()] = 1;
            } catch (NoSuchFieldError unused19) {
            }
            try {
                a[WireFormat.JavaType.LONG.ordinal()] = 2;
            } catch (NoSuchFieldError unused20) {
            }
            try {
                a[WireFormat.JavaType.FLOAT.ordinal()] = 3;
            } catch (NoSuchFieldError unused21) {
            }
            try {
                a[WireFormat.JavaType.DOUBLE.ordinal()] = 4;
            } catch (NoSuchFieldError unused22) {
            }
            try {
                a[WireFormat.JavaType.BOOLEAN.ordinal()] = 5;
            } catch (NoSuchFieldError unused23) {
            }
            try {
                a[WireFormat.JavaType.STRING.ordinal()] = 6;
            } catch (NoSuchFieldError unused24) {
            }
            try {
                a[WireFormat.JavaType.BYTE_STRING.ordinal()] = 7;
            } catch (NoSuchFieldError unused25) {
            }
            try {
                a[WireFormat.JavaType.ENUM.ordinal()] = 8;
            } catch (NoSuchFieldError unused26) {
            }
            try {
                a[WireFormat.JavaType.MESSAGE.ordinal()] = 9;
            } catch (NoSuchFieldError unused27) {
            }
        }
    }

    /* compiled from: FieldSet.java */
    /* loaded from: classes2.dex */
    public interface b<T extends b<T>> extends Comparable<T> {
        int getNumber();

        boolean i();

        boolean isPacked();

        WireFormat.FieldType n();

        e0.a y0(e0.a aVar, e0 e0Var);

        WireFormat.JavaType z();
    }

    public r() {
        this.a = p0.q(16);
    }

    public static Object c(Object obj) {
        if (obj instanceof byte[]) {
            byte[] bArr = (byte[]) obj;
            byte[] bArr2 = new byte[bArr.length];
            System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
            return bArr2;
        }
        return obj;
    }

    public static int d(WireFormat.FieldType fieldType, int i, Object obj) {
        int V = CodedOutputStream.V(i);
        if (fieldType == WireFormat.FieldType.GROUP) {
            V *= 2;
        }
        return V + e(fieldType, obj);
    }

    public static int e(WireFormat.FieldType fieldType, Object obj) {
        switch (a.b[fieldType.ordinal()]) {
            case 1:
                return CodedOutputStream.k(((Double) obj).doubleValue());
            case 2:
                return CodedOutputStream.s(((Float) obj).floatValue());
            case 3:
                return CodedOutputStream.z(((Long) obj).longValue());
            case 4:
                return CodedOutputStream.Z(((Long) obj).longValue());
            case 5:
                return CodedOutputStream.x(((Integer) obj).intValue());
            case 6:
                return CodedOutputStream.q(((Long) obj).longValue());
            case 7:
                return CodedOutputStream.o(((Integer) obj).intValue());
            case 8:
                return CodedOutputStream.f(((Boolean) obj).booleanValue());
            case 9:
                return CodedOutputStream.u((e0) obj);
            case 10:
                if (obj instanceof w) {
                    return CodedOutputStream.C((w) obj);
                }
                return CodedOutputStream.H((e0) obj);
            case 11:
                if (obj instanceof ByteString) {
                    return CodedOutputStream.i((ByteString) obj);
                }
                return CodedOutputStream.U((String) obj);
            case 12:
                if (obj instanceof ByteString) {
                    return CodedOutputStream.i((ByteString) obj);
                }
                return CodedOutputStream.g((byte[]) obj);
            case 13:
                return CodedOutputStream.X(((Integer) obj).intValue());
            case 14:
                return CodedOutputStream.M(((Integer) obj).intValue());
            case 15:
                return CodedOutputStream.O(((Long) obj).longValue());
            case 16:
                return CodedOutputStream.Q(((Integer) obj).intValue());
            case 17:
                return CodedOutputStream.S(((Long) obj).longValue());
            case 18:
                if (obj instanceof v.c) {
                    return CodedOutputStream.m(((v.c) obj).getNumber());
                }
                return CodedOutputStream.m(((Integer) obj).intValue());
            default:
                throw new RuntimeException("There is no way to get here, but the compiler thinks otherwise.");
        }
    }

    public static int f(b<?> bVar, Object obj) {
        WireFormat.FieldType n = bVar.n();
        int number = bVar.getNumber();
        if (bVar.i()) {
            int i = 0;
            if (bVar.isPacked()) {
                for (Object obj2 : (List) obj) {
                    i += e(n, obj2);
                }
                return CodedOutputStream.V(number) + i + CodedOutputStream.K(i);
            }
            for (Object obj3 : (List) obj) {
                i += d(n, number, obj3);
            }
            return i;
        }
        return d(n, number, obj);
    }

    public static <T extends b<T>> r<T> h() {
        return d;
    }

    public static <T extends b<T>> boolean p(Map.Entry<T, Object> entry) {
        T key = entry.getKey();
        if (key.z() == WireFormat.JavaType.MESSAGE) {
            if (key.i()) {
                for (e0 e0Var : (List) entry.getValue()) {
                    if (!e0Var.isInitialized()) {
                        return false;
                    }
                }
            } else {
                Object value = entry.getValue();
                if (value instanceof e0) {
                    if (!((e0) value).isInitialized()) {
                        return false;
                    }
                } else if (value instanceof w) {
                    return true;
                } else {
                    throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
                }
            }
        }
        return true;
    }

    public static boolean q(WireFormat.FieldType fieldType, Object obj) {
        v.a(obj);
        switch (a.a[fieldType.getJavaType().ordinal()]) {
            case 1:
                return obj instanceof Integer;
            case 2:
                return obj instanceof Long;
            case 3:
                return obj instanceof Float;
            case 4:
                return obj instanceof Double;
            case 5:
                return obj instanceof Boolean;
            case 6:
                return obj instanceof String;
            case 7:
                return (obj instanceof ByteString) || (obj instanceof byte[]);
            case 8:
                return (obj instanceof Integer) || (obj instanceof v.c);
            case 9:
                return (obj instanceof e0) || (obj instanceof w);
            default:
                return false;
        }
    }

    public static <T extends b<T>> r<T> v() {
        return new r<>();
    }

    public void a(T t, Object obj) {
        List list;
        if (t.i()) {
            x(t.n(), obj);
            Object i = i(t);
            if (i == null) {
                list = new ArrayList();
                this.a.put(t, list);
            } else {
                list = (List) i;
            }
            list.add(obj);
            return;
        }
        throw new IllegalArgumentException("addRepeatedField() can only be called on repeated fields.");
    }

    /* renamed from: b */
    public r<T> clone() {
        r<T> v = v();
        for (int i = 0; i < this.a.k(); i++) {
            Map.Entry<T, Object> j = this.a.j(i);
            v.w(j.getKey(), j.getValue());
        }
        for (Map.Entry<T, Object> entry : this.a.m()) {
            v.w(entry.getKey(), entry.getValue());
        }
        v.c = this.c;
        return v;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof r) {
            return this.a.equals(((r) obj).a);
        }
        return false;
    }

    public Iterator<Map.Entry<T, Object>> g() {
        if (this.c) {
            return new w.c(this.a.h().iterator());
        }
        return this.a.h().iterator();
    }

    public int hashCode() {
        return this.a.hashCode();
    }

    public Object i(T t) {
        Object obj = this.a.get(t);
        return obj instanceof w ? ((w) obj).f() : obj;
    }

    public int j() {
        int i = 0;
        for (int i2 = 0; i2 < this.a.k(); i2++) {
            i += k(this.a.j(i2));
        }
        for (Map.Entry<T, Object> entry : this.a.m()) {
            i += k(entry);
        }
        return i;
    }

    public final int k(Map.Entry<T, Object> entry) {
        T key = entry.getKey();
        Object value = entry.getValue();
        if (key.z() == WireFormat.JavaType.MESSAGE && !key.i() && !key.isPacked()) {
            if (value instanceof w) {
                return CodedOutputStream.A(entry.getKey().getNumber(), (w) value);
            }
            return CodedOutputStream.E(entry.getKey().getNumber(), (e0) value);
        }
        return f(key, value);
    }

    public int l() {
        int i = 0;
        for (int i2 = 0; i2 < this.a.k(); i2++) {
            Map.Entry<T, Object> j = this.a.j(i2);
            i += f(j.getKey(), j.getValue());
        }
        for (Map.Entry<T, Object> entry : this.a.m()) {
            i += f(entry.getKey(), entry.getValue());
        }
        return i;
    }

    public boolean m() {
        return this.a.isEmpty();
    }

    public boolean n() {
        return this.b;
    }

    public boolean o() {
        for (int i = 0; i < this.a.k(); i++) {
            if (!p(this.a.j(i))) {
                return false;
            }
        }
        for (Map.Entry<T, Object> entry : this.a.m()) {
            if (!p(entry)) {
                return false;
            }
        }
        return true;
    }

    public Iterator<Map.Entry<T, Object>> r() {
        if (this.c) {
            return new w.c(this.a.entrySet().iterator());
        }
        return this.a.entrySet().iterator();
    }

    public void s() {
        if (this.b) {
            return;
        }
        this.a.p();
        this.b = true;
    }

    public void t(r<T> rVar) {
        for (int i = 0; i < rVar.a.k(); i++) {
            u(rVar.a.j(i));
        }
        for (Map.Entry<T, Object> entry : rVar.a.m()) {
            u(entry);
        }
    }

    public final void u(Map.Entry<T, Object> entry) {
        T key = entry.getKey();
        Object value = entry.getValue();
        if (value instanceof w) {
            value = ((w) value).f();
        }
        if (key.i()) {
            Object i = i(key);
            if (i == null) {
                i = new ArrayList();
            }
            for (Object obj : (List) value) {
                ((List) i).add(c(obj));
            }
            this.a.put(key, i);
        } else if (key.z() == WireFormat.JavaType.MESSAGE) {
            Object i2 = i(key);
            if (i2 == null) {
                this.a.put(key, c(value));
                return;
            }
            this.a.put(key, key.y0(((e0) i2).toBuilder(), (e0) value).build());
        } else {
            this.a.put(key, c(value));
        }
    }

    public void w(T t, Object obj) {
        if (t.i()) {
            if (obj instanceof List) {
                ArrayList<Object> arrayList = new ArrayList();
                arrayList.addAll((List) obj);
                for (Object obj2 : arrayList) {
                    x(t.n(), obj2);
                }
                obj = arrayList;
            } else {
                throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
            }
        } else {
            x(t.n(), obj);
        }
        if (obj instanceof w) {
            this.c = true;
        }
        this.a.put(t, obj);
    }

    public final void x(WireFormat.FieldType fieldType, Object obj) {
        if (!q(fieldType, obj)) {
            throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
        }
    }

    public r(boolean z) {
        this(p0.q(0));
        s();
    }

    public r(p0<T, Object> p0Var) {
        this.a = p0Var;
        s();
    }
}
