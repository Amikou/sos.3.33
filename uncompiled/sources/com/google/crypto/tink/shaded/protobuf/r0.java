package com.google.crypto.tink.shaded.protobuf;

import com.google.crypto.tink.shaded.protobuf.Writer;
import java.io.IOException;
import java.util.Arrays;

/* compiled from: UnknownFieldSetLite.java */
/* loaded from: classes2.dex */
public final class r0 {
    public static final r0 f = new r0(0, new int[0], new Object[0], false);
    public int a;
    public int[] b;
    public Object[] c;
    public int d;
    public boolean e;

    public r0() {
        this(0, new int[8], new Object[8], true);
    }

    public static boolean c(int[] iArr, int[] iArr2, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            if (iArr[i2] != iArr2[i2]) {
                return false;
            }
        }
        return true;
    }

    public static boolean d(Object[] objArr, Object[] objArr2, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            if (!objArr[i2].equals(objArr2[i2])) {
                return false;
            }
        }
        return true;
    }

    public static r0 e() {
        return f;
    }

    public static int h(int[] iArr, int i) {
        int i2 = 17;
        for (int i3 = 0; i3 < i; i3++) {
            i2 = (i2 * 31) + iArr[i3];
        }
        return i2;
    }

    public static int i(Object[] objArr, int i) {
        int i2 = 17;
        for (int i3 = 0; i3 < i; i3++) {
            i2 = (i2 * 31) + objArr[i3].hashCode();
        }
        return i2;
    }

    public static r0 k(r0 r0Var, r0 r0Var2) {
        int i = r0Var.a + r0Var2.a;
        int[] copyOf = Arrays.copyOf(r0Var.b, i);
        System.arraycopy(r0Var2.b, 0, copyOf, r0Var.a, r0Var2.a);
        Object[] copyOf2 = Arrays.copyOf(r0Var.c, i);
        System.arraycopy(r0Var2.c, 0, copyOf2, r0Var.a, r0Var2.a);
        return new r0(i, copyOf, copyOf2, true);
    }

    public static r0 l() {
        return new r0();
    }

    public static void p(int i, Object obj, Writer writer) throws IOException {
        int a = WireFormat.a(i);
        int b = WireFormat.b(i);
        if (b == 0) {
            writer.n(a, ((Long) obj).longValue());
        } else if (b == 1) {
            writer.h(a, ((Long) obj).longValue());
        } else if (b == 2) {
            writer.O(a, (ByteString) obj);
        } else if (b != 3) {
            if (b == 5) {
                writer.d(a, ((Integer) obj).intValue());
                return;
            }
            throw new RuntimeException(InvalidProtocolBufferException.invalidWireType());
        } else if (writer.i() == Writer.FieldOrder.ASCENDING) {
            writer.q(a);
            ((r0) obj).q(writer);
            writer.B(a);
        } else {
            writer.B(a);
            ((r0) obj).q(writer);
            writer.q(a);
        }
    }

    public void a() {
        if (!this.e) {
            throw new UnsupportedOperationException();
        }
    }

    public final void b() {
        int i = this.a;
        int[] iArr = this.b;
        if (i == iArr.length) {
            int i2 = i + (i < 4 ? 8 : i >> 1);
            this.b = Arrays.copyOf(iArr, i2);
            this.c = Arrays.copyOf(this.c, i2);
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && (obj instanceof r0)) {
            r0 r0Var = (r0) obj;
            int i = this.a;
            return i == r0Var.a && c(this.b, r0Var.b, i) && d(this.c, r0Var.c, this.a);
        }
        return false;
    }

    public int f() {
        int Y;
        int i = this.d;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < this.a; i3++) {
            int i4 = this.b[i3];
            int a = WireFormat.a(i4);
            int b = WireFormat.b(i4);
            if (b == 0) {
                Y = CodedOutputStream.Y(a, ((Long) this.c[i3]).longValue());
            } else if (b == 1) {
                Y = CodedOutputStream.p(a, ((Long) this.c[i3]).longValue());
            } else if (b == 2) {
                Y = CodedOutputStream.h(a, (ByteString) this.c[i3]);
            } else if (b == 3) {
                Y = (CodedOutputStream.V(a) * 2) + ((r0) this.c[i3]).f();
            } else if (b == 5) {
                Y = CodedOutputStream.n(a, ((Integer) this.c[i3]).intValue());
            } else {
                throw new IllegalStateException(InvalidProtocolBufferException.invalidWireType());
            }
            i2 += Y;
        }
        this.d = i2;
        return i2;
    }

    public int g() {
        int i = this.d;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < this.a; i3++) {
            i2 += CodedOutputStream.J(WireFormat.a(this.b[i3]), (ByteString) this.c[i3]);
        }
        this.d = i2;
        return i2;
    }

    public int hashCode() {
        int i = this.a;
        return ((((527 + i) * 31) + h(this.b, i)) * 31) + i(this.c, this.a);
    }

    public void j() {
        this.e = false;
    }

    public final void m(StringBuilder sb, int i) {
        for (int i2 = 0; i2 < this.a; i2++) {
            f0.c(sb, i, String.valueOf(WireFormat.a(this.b[i2])), this.c[i2]);
        }
    }

    public void n(int i, Object obj) {
        a();
        b();
        int[] iArr = this.b;
        int i2 = this.a;
        iArr[i2] = i;
        this.c[i2] = obj;
        this.a = i2 + 1;
    }

    public void o(Writer writer) throws IOException {
        if (writer.i() == Writer.FieldOrder.DESCENDING) {
            for (int i = this.a - 1; i >= 0; i--) {
                writer.c(WireFormat.a(this.b[i]), this.c[i]);
            }
            return;
        }
        for (int i2 = 0; i2 < this.a; i2++) {
            writer.c(WireFormat.a(this.b[i2]), this.c[i2]);
        }
    }

    public void q(Writer writer) throws IOException {
        if (this.a == 0) {
            return;
        }
        if (writer.i() == Writer.FieldOrder.ASCENDING) {
            for (int i = 0; i < this.a; i++) {
                p(this.b[i], this.c[i], writer);
            }
            return;
        }
        for (int i2 = this.a - 1; i2 >= 0; i2--) {
            p(this.b[i2], this.c[i2], writer);
        }
    }

    public r0(int i, int[] iArr, Object[] objArr, boolean z) {
        this.d = -1;
        this.a = i;
        this.b = iArr;
        this.c = objArr;
        this.e = z;
    }
}
