package com.google.crypto.tink.shaded.protobuf;

import com.google.crypto.tink.shaded.protobuf.GeneratedMessageLite;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* compiled from: ExtensionRegistryLite.java */
/* loaded from: classes2.dex */
public class n {
    public static boolean b = true;
    public static volatile n c;
    public static final n d = new n(true);
    public final Map<a, GeneratedMessageLite.e<?, ?>> a;

    /* compiled from: ExtensionRegistryLite.java */
    /* loaded from: classes2.dex */
    public static final class a {
        public final Object a;
        public final int b;

        public a(Object obj, int i) {
            this.a = obj;
            this.b = i;
        }

        public boolean equals(Object obj) {
            if (obj instanceof a) {
                a aVar = (a) obj;
                return this.a == aVar.a && this.b == aVar.b;
            }
            return false;
        }

        public int hashCode() {
            return (System.identityHashCode(this.a) * 65535) + this.b;
        }
    }

    public n() {
        this.a = new HashMap();
    }

    public static n b() {
        n nVar = c;
        if (nVar == null) {
            synchronized (n.class) {
                nVar = c;
                if (nVar == null) {
                    if (b) {
                        nVar = j11.a();
                    } else {
                        nVar = d;
                    }
                    c = nVar;
                }
            }
        }
        return nVar;
    }

    public <ContainingType extends e0> GeneratedMessageLite.e<ContainingType, ?> a(ContainingType containingtype, int i) {
        return (GeneratedMessageLite.e<ContainingType, ?>) this.a.get(new a(containingtype, i));
    }

    public n(boolean z) {
        this.a = Collections.emptyMap();
    }
}
