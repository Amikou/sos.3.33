package com.google.crypto.tink.shaded.protobuf;

import java.io.IOException;

/* compiled from: MessageLite.java */
/* loaded from: classes2.dex */
public interface e0 extends e82 {

    /* compiled from: MessageLite.java */
    /* loaded from: classes2.dex */
    public interface a extends e82, Cloneable {
        a F(e0 e0Var);

        e0 build();

        e0 buildPartial();
    }

    void a(CodedOutputStream codedOutputStream) throws IOException;

    j0<? extends e0> getParserForType();

    int getSerializedSize();

    a newBuilderForType();

    a toBuilder();

    byte[] toByteArray();

    ByteString toByteString();
}
