package com.google.crypto.tink.shaded.protobuf;

import java.util.AbstractList;
import java.util.Arrays;
import java.util.RandomAccess;

/* compiled from: ProtobufArrayList.java */
/* loaded from: classes2.dex */
public final class l0<E> extends c<E> implements RandomAccess {
    public static final l0<Object> h0;
    public E[] f0;
    public int g0;

    static {
        l0<Object> l0Var = new l0<>(new Object[0], 0);
        h0 = l0Var;
        l0Var.l();
    }

    public l0(E[] eArr, int i) {
        this.f0 = eArr;
        this.g0 = i;
    }

    public static <E> E[] i(int i) {
        return (E[]) new Object[i];
    }

    public static <E> l0<E> k() {
        return (l0<E>) h0;
    }

    @Override // com.google.crypto.tink.shaded.protobuf.c, java.util.AbstractList, java.util.AbstractCollection, java.util.Collection, java.util.List
    public boolean add(E e) {
        e();
        int i = this.g0;
        E[] eArr = this.f0;
        if (i == eArr.length) {
            this.f0 = (E[]) Arrays.copyOf(eArr, ((i * 3) / 2) + 1);
        }
        E[] eArr2 = this.f0;
        int i2 = this.g0;
        this.g0 = i2 + 1;
        eArr2[i2] = e;
        ((AbstractList) this).modCount++;
        return true;
    }

    @Override // java.util.AbstractList, java.util.List
    public E get(int i) {
        m(i);
        return this.f0[i];
    }

    public final void m(int i) {
        if (i < 0 || i >= this.g0) {
            throw new IndexOutOfBoundsException(n(i));
        }
    }

    public final String n(int i) {
        return "Index:" + i + ", Size:" + this.g0;
    }

    @Override // com.google.crypto.tink.shaded.protobuf.v.i
    /* renamed from: o */
    public l0<E> a(int i) {
        if (i >= this.g0) {
            return new l0<>(Arrays.copyOf(this.f0, i), this.g0);
        }
        throw new IllegalArgumentException();
    }

    @Override // java.util.AbstractList, java.util.List
    public E remove(int i) {
        int i2;
        e();
        m(i);
        E[] eArr = this.f0;
        E e = eArr[i];
        if (i < this.g0 - 1) {
            System.arraycopy(eArr, i + 1, eArr, i, (i2 - i) - 1);
        }
        this.g0--;
        ((AbstractList) this).modCount++;
        return e;
    }

    @Override // java.util.AbstractList, java.util.List
    public E set(int i, E e) {
        e();
        m(i);
        E[] eArr = this.f0;
        E e2 = eArr[i];
        eArr[i] = e;
        ((AbstractList) this).modCount++;
        return e2;
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public int size() {
        return this.g0;
    }

    @Override // java.util.AbstractList, java.util.List
    public void add(int i, E e) {
        int i2;
        e();
        if (i >= 0 && i <= (i2 = this.g0)) {
            E[] eArr = this.f0;
            if (i2 < eArr.length) {
                System.arraycopy(eArr, i, eArr, i + 1, i2 - i);
            } else {
                E[] eArr2 = (E[]) i(((i2 * 3) / 2) + 1);
                System.arraycopy(this.f0, 0, eArr2, 0, i);
                System.arraycopy(this.f0, i, eArr2, i + 1, this.g0 - i);
                this.f0 = eArr2;
            }
            this.f0[i] = e;
            this.g0++;
            ((AbstractList) this).modCount++;
            return;
        }
        throw new IndexOutOfBoundsException(n(i));
    }
}
