package com.google.crypto.tink.shaded.protobuf;

import com.google.crypto.tink.shaded.protobuf.e0;

/* compiled from: AbstractParser.java */
/* loaded from: classes2.dex */
public abstract class b<MessageType extends e0> implements j0<MessageType> {
    static {
        n.b();
    }

    public final MessageType c(MessageType messagetype) throws InvalidProtocolBufferException {
        if (messagetype == null || messagetype.isInitialized()) {
            return messagetype;
        }
        throw d(messagetype).asInvalidProtocolBufferException().setUnfinishedMessage(messagetype);
    }

    public final UninitializedMessageException d(MessageType messagetype) {
        if (messagetype instanceof a) {
            return ((a) messagetype).e();
        }
        return new UninitializedMessageException(messagetype);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.j0
    /* renamed from: e */
    public MessageType a(ByteString byteString, n nVar) throws InvalidProtocolBufferException {
        return c(f(byteString, nVar));
    }

    public MessageType f(ByteString byteString, n nVar) throws InvalidProtocolBufferException {
        try {
            i newCodedInput = byteString.newCodedInput();
            MessageType messagetype = (MessageType) b(newCodedInput, nVar);
            try {
                newCodedInput.a(0);
                return messagetype;
            } catch (InvalidProtocolBufferException e) {
                throw e.setUnfinishedMessage(messagetype);
            }
        } catch (InvalidProtocolBufferException e2) {
            throw e2;
        }
    }
}
