package com.google.crypto.tink.shaded.protobuf;

import com.google.crypto.tink.shaded.protobuf.v;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* compiled from: ListFieldSchema.java */
/* loaded from: classes2.dex */
public abstract class z {
    public static final z a = new b();
    public static final z b = new c();

    /* compiled from: ListFieldSchema.java */
    /* loaded from: classes2.dex */
    public static final class b extends z {
        public static final Class<?> c = Collections.unmodifiableList(Collections.emptyList()).getClass();

        public b() {
            super();
        }

        public static <E> List<E> f(Object obj, long j) {
            return (List) t0.E(obj, j);
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static <L> List<L> g(Object obj, long j, int i) {
            y yVar;
            List<L> arrayList;
            List<L> f = f(obj, j);
            if (f.isEmpty()) {
                if (f instanceof dz1) {
                    arrayList = new y(i);
                } else if ((f instanceof vu2) && (f instanceof v.i)) {
                    arrayList = ((v.i) f).a(i);
                } else {
                    arrayList = new ArrayList<>(i);
                }
                t0.T(obj, j, arrayList);
                return arrayList;
            }
            if (c.isAssignableFrom(f.getClass())) {
                ArrayList arrayList2 = new ArrayList(f.size() + i);
                arrayList2.addAll(f);
                t0.T(obj, j, arrayList2);
                yVar = arrayList2;
            } else if (f instanceof cf4) {
                y yVar2 = new y(f.size() + i);
                yVar2.addAll((cf4) f);
                t0.T(obj, j, yVar2);
                yVar = yVar2;
            } else if ((f instanceof vu2) && (f instanceof v.i)) {
                v.i iVar = (v.i) f;
                if (iVar.A()) {
                    return f;
                }
                v.i a = iVar.a(f.size() + i);
                t0.T(obj, j, a);
                return a;
            } else {
                return f;
            }
            return yVar;
        }

        @Override // com.google.crypto.tink.shaded.protobuf.z
        public void c(Object obj, long j) {
            Object unmodifiableList;
            List list = (List) t0.E(obj, j);
            if (list instanceof dz1) {
                unmodifiableList = ((dz1) list).x();
            } else if (c.isAssignableFrom(list.getClass())) {
                return;
            } else {
                if ((list instanceof vu2) && (list instanceof v.i)) {
                    v.i iVar = (v.i) list;
                    if (iVar.A()) {
                        iVar.l();
                        return;
                    }
                    return;
                }
                unmodifiableList = Collections.unmodifiableList(list);
            }
            t0.T(obj, j, unmodifiableList);
        }

        @Override // com.google.crypto.tink.shaded.protobuf.z
        public <E> void d(Object obj, Object obj2, long j) {
            List f = f(obj2, j);
            List g = g(obj, j, f.size());
            int size = g.size();
            int size2 = f.size();
            if (size > 0 && size2 > 0) {
                g.addAll(f);
            }
            if (size > 0) {
                f = g;
            }
            t0.T(obj, j, f);
        }

        @Override // com.google.crypto.tink.shaded.protobuf.z
        public <L> List<L> e(Object obj, long j) {
            return g(obj, j, 10);
        }
    }

    /* compiled from: ListFieldSchema.java */
    /* loaded from: classes2.dex */
    public static final class c extends z {
        public c() {
            super();
        }

        public static <E> v.i<E> f(Object obj, long j) {
            return (v.i) t0.E(obj, j);
        }

        @Override // com.google.crypto.tink.shaded.protobuf.z
        public void c(Object obj, long j) {
            f(obj, j).l();
        }

        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r0v2, types: [java.util.List] */
        @Override // com.google.crypto.tink.shaded.protobuf.z
        public <E> void d(Object obj, Object obj2, long j) {
            v.i<E> f = f(obj, j);
            v.i<E> f2 = f(obj2, j);
            int size = f.size();
            int size2 = f2.size();
            v.i<E> iVar = f;
            iVar = f;
            if (size > 0 && size2 > 0) {
                boolean A = f.A();
                v.i<E> iVar2 = f;
                if (!A) {
                    iVar2 = f.a(size2 + size);
                }
                iVar2.addAll(f2);
                iVar = iVar2;
            }
            if (size > 0) {
                f2 = iVar;
            }
            t0.T(obj, j, f2);
        }

        @Override // com.google.crypto.tink.shaded.protobuf.z
        public <L> List<L> e(Object obj, long j) {
            v.i f = f(obj, j);
            if (f.A()) {
                return f;
            }
            int size = f.size();
            v.i a = f.a(size == 0 ? 10 : size * 2);
            t0.T(obj, j, a);
            return a;
        }
    }

    public static z a() {
        return a;
    }

    public static z b() {
        return b;
    }

    public abstract void c(Object obj, long j);

    public abstract <L> void d(Object obj, Object obj2, long j);

    public abstract <L> List<L> e(Object obj, long j);

    public z() {
    }
}
