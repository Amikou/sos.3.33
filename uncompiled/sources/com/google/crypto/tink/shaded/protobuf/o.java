package com.google.crypto.tink.shaded.protobuf;

import com.google.crypto.tink.shaded.protobuf.r;
import com.google.crypto.tink.shaded.protobuf.r.b;
import java.io.IOException;
import java.util.Map;

/* compiled from: ExtensionSchema.java */
/* loaded from: classes2.dex */
public abstract class o<T extends r.b<T>> {
    public abstract int a(Map.Entry<?, ?> entry);

    public abstract Object b(n nVar, e0 e0Var, int i);

    public abstract r<T> c(Object obj);

    public abstract r<T> d(Object obj);

    public abstract boolean e(e0 e0Var);

    public abstract void f(Object obj);

    public abstract <UT, UB> UB g(m0 m0Var, Object obj, n nVar, r<T> rVar, UB ub, q0<UT, UB> q0Var) throws IOException;

    public abstract void h(m0 m0Var, Object obj, n nVar, r<T> rVar) throws IOException;

    public abstract void i(ByteString byteString, Object obj, n nVar, r<T> rVar) throws IOException;

    public abstract void j(Writer writer, Map.Entry<?, ?> entry) throws IOException;
}
