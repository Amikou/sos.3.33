package com.google.crypto.tink.shaded.protobuf;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/* compiled from: Protobuf.java */
/* loaded from: classes2.dex */
public final class k0 {
    public static final k0 c = new k0();
    public final ConcurrentMap<Class<?>, n0<?>> b = new ConcurrentHashMap();
    public final nd3 a = new r32();

    public static k0 a() {
        return c;
    }

    public <T> void b(T t, m0 m0Var, n nVar) throws IOException {
        e(t).j(t, m0Var, nVar);
    }

    public n0<?> c(Class<?> cls, n0<?> n0Var) {
        v.b(cls, "messageType");
        v.b(n0Var, "schema");
        return this.b.putIfAbsent(cls, n0Var);
    }

    public <T> n0<T> d(Class<T> cls) {
        v.b(cls, "messageType");
        n0<T> n0Var = (n0<T>) this.b.get(cls);
        if (n0Var == null) {
            n0<T> a = this.a.a(cls);
            n0<T> n0Var2 = (n0<T>) c(cls, a);
            return n0Var2 != null ? n0Var2 : a;
        }
        return n0Var;
    }

    public <T> n0<T> e(T t) {
        return d(t.getClass());
    }
}
