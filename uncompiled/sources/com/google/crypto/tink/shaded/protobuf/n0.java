package com.google.crypto.tink.shaded.protobuf;

import com.google.crypto.tink.shaded.protobuf.d;
import java.io.IOException;

/* compiled from: Schema.java */
/* loaded from: classes2.dex */
public interface n0<T> {
    void a(T t, T t2);

    boolean b(T t, T t2);

    T c();

    int d(T t);

    void e(T t);

    boolean f(T t);

    int g(T t);

    void h(T t, byte[] bArr, int i, int i2, d.b bVar) throws IOException;

    void i(T t, Writer writer) throws IOException;

    void j(T t, m0 m0Var, n nVar) throws IOException;
}
