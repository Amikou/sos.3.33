package com.google.crypto.tink.shaded.protobuf;

import com.google.crypto.tink.shaded.protobuf.v;
import java.util.AbstractList;
import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

/* compiled from: DoubleArrayList.java */
/* loaded from: classes2.dex */
public final class l extends c<Double> implements v.b, RandomAccess, vu2 {
    public static final l h0;
    public double[] f0;
    public int g0;

    static {
        l lVar = new l(new double[0], 0);
        h0 = lVar;
        lVar.l();
    }

    public l() {
        this(new double[10], 0);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.c, java.util.AbstractCollection, java.util.Collection, java.util.List
    public boolean addAll(Collection<? extends Double> collection) {
        e();
        v.a(collection);
        if (!(collection instanceof l)) {
            return super.addAll(collection);
        }
        l lVar = (l) collection;
        int i = lVar.g0;
        if (i == 0) {
            return false;
        }
        int i2 = this.g0;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            double[] dArr = this.f0;
            if (i3 > dArr.length) {
                this.f0 = Arrays.copyOf(dArr, i3);
            }
            System.arraycopy(lVar.f0, 0, this.f0, this.g0, lVar.g0);
            this.g0 = i3;
            ((AbstractList) this).modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    @Override // com.google.crypto.tink.shaded.protobuf.c, java.util.AbstractList, java.util.Collection, java.util.List
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof l)) {
            return super.equals(obj);
        }
        l lVar = (l) obj;
        if (this.g0 != lVar.g0) {
            return false;
        }
        double[] dArr = lVar.f0;
        for (int i = 0; i < this.g0; i++) {
            if (Double.doubleToLongBits(this.f0[i]) != Double.doubleToLongBits(dArr[i])) {
                return false;
            }
        }
        return true;
    }

    @Override // com.google.crypto.tink.shaded.protobuf.c, java.util.AbstractList, java.util.Collection, java.util.List
    public int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.g0; i2++) {
            i = (i * 31) + v.f(Double.doubleToLongBits(this.f0[i2]));
        }
        return i;
    }

    @Override // java.util.AbstractList, java.util.List
    /* renamed from: i */
    public void add(int i, Double d) {
        n(i, d.doubleValue());
    }

    @Override // com.google.crypto.tink.shaded.protobuf.c, java.util.AbstractList, java.util.AbstractCollection, java.util.Collection, java.util.List
    /* renamed from: k */
    public boolean add(Double d) {
        m(d.doubleValue());
        return true;
    }

    public void m(double d) {
        e();
        int i = this.g0;
        double[] dArr = this.f0;
        if (i == dArr.length) {
            double[] dArr2 = new double[((i * 3) / 2) + 1];
            System.arraycopy(dArr, 0, dArr2, 0, i);
            this.f0 = dArr2;
        }
        double[] dArr3 = this.f0;
        int i2 = this.g0;
        this.g0 = i2 + 1;
        dArr3[i2] = d;
    }

    public final void n(int i, double d) {
        int i2;
        e();
        if (i >= 0 && i <= (i2 = this.g0)) {
            double[] dArr = this.f0;
            if (i2 < dArr.length) {
                System.arraycopy(dArr, i, dArr, i + 1, i2 - i);
            } else {
                double[] dArr2 = new double[((i2 * 3) / 2) + 1];
                System.arraycopy(dArr, 0, dArr2, 0, i);
                System.arraycopy(this.f0, i, dArr2, i + 1, this.g0 - i);
                this.f0 = dArr2;
            }
            this.f0[i] = d;
            this.g0++;
            ((AbstractList) this).modCount++;
            return;
        }
        throw new IndexOutOfBoundsException(s(i));
    }

    public final void o(int i) {
        if (i < 0 || i >= this.g0) {
            throw new IndexOutOfBoundsException(s(i));
        }
    }

    @Override // java.util.AbstractList, java.util.List
    /* renamed from: p */
    public Double get(int i) {
        return Double.valueOf(q(i));
    }

    public double q(int i) {
        o(i);
        return this.f0[i];
    }

    @Override // java.util.AbstractList
    public void removeRange(int i, int i2) {
        e();
        if (i2 >= i) {
            double[] dArr = this.f0;
            System.arraycopy(dArr, i2, dArr, i, this.g0 - i2);
            this.g0 -= i2 - i;
            ((AbstractList) this).modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    public final String s(int i) {
        return "Index:" + i + ", Size:" + this.g0;
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public int size() {
        return this.g0;
    }

    @Override // com.google.crypto.tink.shaded.protobuf.v.i
    /* renamed from: t */
    public v.b a(int i) {
        if (i >= this.g0) {
            return new l(Arrays.copyOf(this.f0, i), this.g0);
        }
        throw new IllegalArgumentException();
    }

    @Override // java.util.AbstractList, java.util.List
    /* renamed from: w */
    public Double remove(int i) {
        int i2;
        e();
        o(i);
        double[] dArr = this.f0;
        double d = dArr[i];
        if (i < this.g0 - 1) {
            System.arraycopy(dArr, i + 1, dArr, i, (i2 - i) - 1);
        }
        this.g0--;
        ((AbstractList) this).modCount++;
        return Double.valueOf(d);
    }

    @Override // java.util.AbstractList, java.util.List
    /* renamed from: y */
    public Double set(int i, Double d) {
        return Double.valueOf(z(i, d.doubleValue()));
    }

    public double z(int i, double d) {
        e();
        o(i);
        double[] dArr = this.f0;
        double d2 = dArr[i];
        dArr[i] = d;
        return d2;
    }

    public l(double[] dArr, int i) {
        this.f0 = dArr;
        this.g0 = i;
    }

    @Override // com.google.crypto.tink.shaded.protobuf.c, java.util.AbstractCollection, java.util.Collection, java.util.List
    public boolean remove(Object obj) {
        e();
        for (int i = 0; i < this.g0; i++) {
            if (obj.equals(Double.valueOf(this.f0[i]))) {
                double[] dArr = this.f0;
                System.arraycopy(dArr, i + 1, dArr, i, (this.g0 - i) - 1);
                this.g0--;
                ((AbstractList) this).modCount++;
                return true;
            }
        }
        return false;
    }
}
