package com.google.crypto.tink.shaded.protobuf;

import com.google.zxing.qrcode.encoder.Encoder;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Objects;
import java.util.RandomAccess;

/* compiled from: Internal.java */
/* loaded from: classes2.dex */
public final class v {
    public static final Charset a = Charset.forName("UTF-8");
    public static final byte[] b;

    /* compiled from: Internal.java */
    /* loaded from: classes2.dex */
    public interface a extends i<Boolean> {
    }

    /* compiled from: Internal.java */
    /* loaded from: classes2.dex */
    public interface b extends i<Double> {
    }

    /* compiled from: Internal.java */
    /* loaded from: classes2.dex */
    public interface c {
        int getNumber();
    }

    /* compiled from: Internal.java */
    /* loaded from: classes2.dex */
    public interface d<T extends c> {
        T findValueByNumber(int i);
    }

    /* compiled from: Internal.java */
    /* loaded from: classes2.dex */
    public interface e {
        boolean a(int i);
    }

    /* compiled from: Internal.java */
    /* loaded from: classes2.dex */
    public interface f extends i<Float> {
    }

    /* compiled from: Internal.java */
    /* loaded from: classes2.dex */
    public interface g extends i<Integer> {
    }

    /* compiled from: Internal.java */
    /* loaded from: classes2.dex */
    public interface h extends i<Long> {
    }

    /* compiled from: Internal.java */
    /* loaded from: classes2.dex */
    public interface i<E> extends List<E>, RandomAccess {
        boolean A();

        i<E> a(int i);

        void l();
    }

    static {
        Charset.forName(Encoder.DEFAULT_BYTE_MODE_ENCODING);
        byte[] bArr = new byte[0];
        b = bArr;
        ByteBuffer.wrap(bArr);
        com.google.crypto.tink.shaded.protobuf.i.i(bArr);
    }

    public static <T> T a(T t) {
        Objects.requireNonNull(t);
        return t;
    }

    public static <T> T b(T t, String str) {
        Objects.requireNonNull(t, str);
        return t;
    }

    public static int c(boolean z) {
        return z ? 1231 : 1237;
    }

    public static int d(byte[] bArr) {
        return e(bArr, 0, bArr.length);
    }

    public static int e(byte[] bArr, int i2, int i3) {
        int i4 = i(i3, bArr, i2, i3);
        if (i4 == 0) {
            return 1;
        }
        return i4;
    }

    public static int f(long j) {
        return (int) (j ^ (j >>> 32));
    }

    public static boolean g(byte[] bArr) {
        return Utf8.s(bArr);
    }

    public static Object h(Object obj, Object obj2) {
        return ((e0) obj).toBuilder().F((e0) obj2).buildPartial();
    }

    public static int i(int i2, byte[] bArr, int i3, int i4) {
        for (int i5 = i3; i5 < i3 + i4; i5++) {
            i2 = (i2 * 31) + bArr[i5];
        }
        return i2;
    }

    public static String j(byte[] bArr) {
        return new String(bArr, a);
    }
}
