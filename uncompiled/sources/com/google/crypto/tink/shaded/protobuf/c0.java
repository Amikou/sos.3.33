package com.google.crypto.tink.shaded.protobuf;

import com.google.crypto.tink.shaded.protobuf.b0;
import java.util.Map;

/* compiled from: MapFieldSchema.java */
/* loaded from: classes2.dex */
public interface c0 {
    Object a(Object obj, Object obj2);

    Object b(Object obj);

    b0.a<?, ?> c(Object obj);

    Map<?, ?> d(Object obj);

    Object e(Object obj);

    int f(int i, Object obj, Object obj2);

    boolean g(Object obj);

    Map<?, ?> h(Object obj);
}
