package com.google.crypto.tink.shaded.protobuf;

import com.google.crypto.tink.shaded.protobuf.ByteString;
import com.google.crypto.tink.shaded.protobuf.a;
import com.google.crypto.tink.shaded.protobuf.a.AbstractC0137a;
import com.google.crypto.tink.shaded.protobuf.e0;
import java.io.IOException;

/* compiled from: AbstractMessageLite.java */
/* loaded from: classes2.dex */
public abstract class a<MessageType extends a<MessageType, BuilderType>, BuilderType extends AbstractC0137a<MessageType, BuilderType>> implements e0 {
    public int memoizedHashCode = 0;

    /* compiled from: AbstractMessageLite.java */
    /* renamed from: com.google.crypto.tink.shaded.protobuf.a$a  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public static abstract class AbstractC0137a<MessageType extends a<MessageType, BuilderType>, BuilderType extends AbstractC0137a<MessageType, BuilderType>> implements e0.a {
        public static UninitializedMessageException e(e0 e0Var) {
            return new UninitializedMessageException(e0Var);
        }

        public abstract BuilderType b(MessageType messagetype);

        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.google.crypto.tink.shaded.protobuf.e0.a
        /* renamed from: d */
        public BuilderType F(e0 e0Var) {
            if (getDefaultInstanceForType().getClass().isInstance(e0Var)) {
                return (BuilderType) b((a) e0Var);
            }
            throw new IllegalArgumentException("mergeFrom(MessageLite) can only merge messages of the same type.");
        }
    }

    int b() {
        throw new UnsupportedOperationException();
    }

    public int c(n0 n0Var) {
        int b = b();
        if (b == -1) {
            int g = n0Var.g(this);
            f(g);
            return g;
        }
        return b;
    }

    public final String d(String str) {
        return "Serializing " + getClass().getName() + " to a " + str + " threw an IOException (should never happen).";
    }

    public UninitializedMessageException e() {
        return new UninitializedMessageException(this);
    }

    void f(int i) {
        throw new UnsupportedOperationException();
    }

    @Override // com.google.crypto.tink.shaded.protobuf.e0
    public byte[] toByteArray() {
        try {
            byte[] bArr = new byte[getSerializedSize()];
            CodedOutputStream e0 = CodedOutputStream.e0(bArr);
            a(e0);
            e0.d();
            return bArr;
        } catch (IOException e) {
            throw new RuntimeException(d("byte array"), e);
        }
    }

    @Override // com.google.crypto.tink.shaded.protobuf.e0
    public ByteString toByteString() {
        try {
            ByteString.g newCodedBuilder = ByteString.newCodedBuilder(getSerializedSize());
            a(newCodedBuilder.b());
            return newCodedBuilder.a();
        } catch (IOException e) {
            throw new RuntimeException(d("ByteString"), e);
        }
    }
}
