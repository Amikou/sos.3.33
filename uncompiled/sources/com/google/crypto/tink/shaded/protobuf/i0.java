package com.google.crypto.tink.shaded.protobuf;

import com.google.crypto.tink.shaded.protobuf.GeneratedMessageLite;

/* compiled from: NewInstanceSchemaLite.java */
/* loaded from: classes2.dex */
public final class i0 implements jf2 {
    @Override // defpackage.jf2
    public Object a(Object obj) {
        return ((GeneratedMessageLite) obj).j(GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE);
    }
}
