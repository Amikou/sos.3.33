package com.google.crypto.tink.shaded.protobuf;

import com.google.crypto.tink.shaded.protobuf.GeneratedMessageLite;
import com.google.crypto.tink.shaded.protobuf.WireFormat;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/* compiled from: ExtensionSchemaLite.java */
/* loaded from: classes2.dex */
public final class p extends o<GeneratedMessageLite.d> {

    /* compiled from: ExtensionSchemaLite.java */
    /* loaded from: classes2.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[WireFormat.FieldType.values().length];
            a = iArr;
            try {
                iArr[WireFormat.FieldType.DOUBLE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[WireFormat.FieldType.FLOAT.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[WireFormat.FieldType.INT64.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                a[WireFormat.FieldType.UINT64.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                a[WireFormat.FieldType.INT32.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                a[WireFormat.FieldType.FIXED64.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                a[WireFormat.FieldType.FIXED32.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                a[WireFormat.FieldType.BOOL.ordinal()] = 8;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                a[WireFormat.FieldType.UINT32.ordinal()] = 9;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                a[WireFormat.FieldType.SFIXED32.ordinal()] = 10;
            } catch (NoSuchFieldError unused10) {
            }
            try {
                a[WireFormat.FieldType.SFIXED64.ordinal()] = 11;
            } catch (NoSuchFieldError unused11) {
            }
            try {
                a[WireFormat.FieldType.SINT32.ordinal()] = 12;
            } catch (NoSuchFieldError unused12) {
            }
            try {
                a[WireFormat.FieldType.SINT64.ordinal()] = 13;
            } catch (NoSuchFieldError unused13) {
            }
            try {
                a[WireFormat.FieldType.ENUM.ordinal()] = 14;
            } catch (NoSuchFieldError unused14) {
            }
            try {
                a[WireFormat.FieldType.BYTES.ordinal()] = 15;
            } catch (NoSuchFieldError unused15) {
            }
            try {
                a[WireFormat.FieldType.STRING.ordinal()] = 16;
            } catch (NoSuchFieldError unused16) {
            }
            try {
                a[WireFormat.FieldType.GROUP.ordinal()] = 17;
            } catch (NoSuchFieldError unused17) {
            }
            try {
                a[WireFormat.FieldType.MESSAGE.ordinal()] = 18;
            } catch (NoSuchFieldError unused18) {
            }
        }
    }

    @Override // com.google.crypto.tink.shaded.protobuf.o
    public int a(Map.Entry<?, ?> entry) {
        return ((GeneratedMessageLite.d) entry.getKey()).getNumber();
    }

    @Override // com.google.crypto.tink.shaded.protobuf.o
    public Object b(n nVar, e0 e0Var, int i) {
        return nVar.a(e0Var, i);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.o
    public r<GeneratedMessageLite.d> c(Object obj) {
        return ((GeneratedMessageLite.c) obj).extensions;
    }

    @Override // com.google.crypto.tink.shaded.protobuf.o
    public r<GeneratedMessageLite.d> d(Object obj) {
        return ((GeneratedMessageLite.c) obj).C();
    }

    @Override // com.google.crypto.tink.shaded.protobuf.o
    public boolean e(e0 e0Var) {
        return e0Var instanceof GeneratedMessageLite.c;
    }

    @Override // com.google.crypto.tink.shaded.protobuf.o
    public void f(Object obj) {
        c(obj).s();
    }

    @Override // com.google.crypto.tink.shaded.protobuf.o
    public <UT, UB> UB g(m0 m0Var, Object obj, n nVar, r<GeneratedMessageLite.d> rVar, UB ub, q0<UT, UB> q0Var) throws IOException {
        Object i;
        ArrayList arrayList;
        GeneratedMessageLite.e eVar = (GeneratedMessageLite.e) obj;
        int c = eVar.c();
        if (eVar.b.i() && eVar.b.isPacked()) {
            switch (a.a[eVar.a().ordinal()]) {
                case 1:
                    arrayList = new ArrayList();
                    m0Var.F(arrayList);
                    break;
                case 2:
                    arrayList = new ArrayList();
                    m0Var.A(arrayList);
                    break;
                case 3:
                    arrayList = new ArrayList();
                    m0Var.m(arrayList);
                    break;
                case 4:
                    arrayList = new ArrayList();
                    m0Var.k(arrayList);
                    break;
                case 5:
                    arrayList = new ArrayList();
                    m0Var.o(arrayList);
                    break;
                case 6:
                    arrayList = new ArrayList();
                    m0Var.I(arrayList);
                    break;
                case 7:
                    arrayList = new ArrayList();
                    m0Var.r(arrayList);
                    break;
                case 8:
                    arrayList = new ArrayList();
                    m0Var.u(arrayList);
                    break;
                case 9:
                    arrayList = new ArrayList();
                    m0Var.g(arrayList);
                    break;
                case 10:
                    arrayList = new ArrayList();
                    m0Var.e(arrayList);
                    break;
                case 11:
                    arrayList = new ArrayList();
                    m0Var.n(arrayList);
                    break;
                case 12:
                    arrayList = new ArrayList();
                    m0Var.b(arrayList);
                    break;
                case 13:
                    arrayList = new ArrayList();
                    m0Var.f(arrayList);
                    break;
                case 14:
                    arrayList = new ArrayList();
                    m0Var.p(arrayList);
                    ub = (UB) o0.z(c, arrayList, eVar.b.d(), ub, q0Var);
                    break;
                default:
                    throw new IllegalStateException("Type cannot be packed: " + eVar.b.n());
            }
            rVar.w(eVar.b, arrayList);
        } else {
            Object obj2 = null;
            if (eVar.a() == WireFormat.FieldType.ENUM) {
                int B = m0Var.B();
                if (eVar.b.d().findValueByNumber(B) == null) {
                    return (UB) o0.L(c, B, ub, q0Var);
                }
                obj2 = Integer.valueOf(B);
            } else {
                switch (a.a[eVar.a().ordinal()]) {
                    case 1:
                        obj2 = Double.valueOf(m0Var.readDouble());
                        break;
                    case 2:
                        obj2 = Float.valueOf(m0Var.readFloat());
                        break;
                    case 3:
                        obj2 = Long.valueOf(m0Var.G());
                        break;
                    case 4:
                        obj2 = Long.valueOf(m0Var.c());
                        break;
                    case 5:
                        obj2 = Integer.valueOf(m0Var.B());
                        break;
                    case 6:
                        obj2 = Long.valueOf(m0Var.d());
                        break;
                    case 7:
                        obj2 = Integer.valueOf(m0Var.h());
                        break;
                    case 8:
                        obj2 = Boolean.valueOf(m0Var.i());
                        break;
                    case 9:
                        obj2 = Integer.valueOf(m0Var.l());
                        break;
                    case 10:
                        obj2 = Integer.valueOf(m0Var.D());
                        break;
                    case 11:
                        obj2 = Long.valueOf(m0Var.j());
                        break;
                    case 12:
                        obj2 = Integer.valueOf(m0Var.s());
                        break;
                    case 13:
                        obj2 = Long.valueOf(m0Var.t());
                        break;
                    case 14:
                        throw new IllegalStateException("Shouldn't reach here.");
                    case 15:
                        obj2 = m0Var.z();
                        break;
                    case 16:
                        obj2 = m0Var.v();
                        break;
                    case 17:
                        obj2 = m0Var.M(eVar.b().getClass(), nVar);
                        break;
                    case 18:
                        obj2 = m0Var.N(eVar.b().getClass(), nVar);
                        break;
                }
            }
            if (eVar.d()) {
                rVar.a(eVar.b, obj2);
            } else {
                int i2 = a.a[eVar.a().ordinal()];
                if ((i2 == 17 || i2 == 18) && (i = rVar.i(eVar.b)) != null) {
                    obj2 = v.h(i, obj2);
                }
                rVar.w(eVar.b, obj2);
            }
        }
        return ub;
    }

    @Override // com.google.crypto.tink.shaded.protobuf.o
    public void h(m0 m0Var, Object obj, n nVar, r<GeneratedMessageLite.d> rVar) throws IOException {
        GeneratedMessageLite.e eVar = (GeneratedMessageLite.e) obj;
        rVar.w(eVar.b, m0Var.N(eVar.b().getClass(), nVar));
    }

    @Override // com.google.crypto.tink.shaded.protobuf.o
    public void i(ByteString byteString, Object obj, n nVar, r<GeneratedMessageLite.d> rVar) throws IOException {
        GeneratedMessageLite.e eVar = (GeneratedMessageLite.e) obj;
        e0 buildPartial = eVar.b().newBuilderForType().buildPartial();
        e Q = e.Q(ByteBuffer.wrap(byteString.toByteArray()), true);
        k0.a().b(buildPartial, Q, nVar);
        rVar.w(eVar.b, buildPartial);
        if (Q.w() != Integer.MAX_VALUE) {
            throw InvalidProtocolBufferException.invalidEndTag();
        }
    }

    @Override // com.google.crypto.tink.shaded.protobuf.o
    public void j(Writer writer, Map.Entry<?, ?> entry) throws IOException {
        GeneratedMessageLite.d dVar = (GeneratedMessageLite.d) entry.getKey();
        if (dVar.i()) {
            switch (a.a[dVar.n().ordinal()]) {
                case 1:
                    o0.P(dVar.getNumber(), (List) entry.getValue(), writer, dVar.isPacked());
                    return;
                case 2:
                    o0.T(dVar.getNumber(), (List) entry.getValue(), writer, dVar.isPacked());
                    return;
                case 3:
                    o0.W(dVar.getNumber(), (List) entry.getValue(), writer, dVar.isPacked());
                    return;
                case 4:
                    o0.e0(dVar.getNumber(), (List) entry.getValue(), writer, dVar.isPacked());
                    return;
                case 5:
                    o0.V(dVar.getNumber(), (List) entry.getValue(), writer, dVar.isPacked());
                    return;
                case 6:
                    o0.S(dVar.getNumber(), (List) entry.getValue(), writer, dVar.isPacked());
                    return;
                case 7:
                    o0.R(dVar.getNumber(), (List) entry.getValue(), writer, dVar.isPacked());
                    return;
                case 8:
                    o0.N(dVar.getNumber(), (List) entry.getValue(), writer, dVar.isPacked());
                    return;
                case 9:
                    o0.d0(dVar.getNumber(), (List) entry.getValue(), writer, dVar.isPacked());
                    return;
                case 10:
                    o0.Y(dVar.getNumber(), (List) entry.getValue(), writer, dVar.isPacked());
                    return;
                case 11:
                    o0.Z(dVar.getNumber(), (List) entry.getValue(), writer, dVar.isPacked());
                    return;
                case 12:
                    o0.a0(dVar.getNumber(), (List) entry.getValue(), writer, dVar.isPacked());
                    return;
                case 13:
                    o0.b0(dVar.getNumber(), (List) entry.getValue(), writer, dVar.isPacked());
                    return;
                case 14:
                    o0.V(dVar.getNumber(), (List) entry.getValue(), writer, dVar.isPacked());
                    return;
                case 15:
                    o0.O(dVar.getNumber(), (List) entry.getValue(), writer);
                    return;
                case 16:
                    o0.c0(dVar.getNumber(), (List) entry.getValue(), writer);
                    return;
                case 17:
                    List list = (List) entry.getValue();
                    if (list == null || list.isEmpty()) {
                        return;
                    }
                    o0.U(dVar.getNumber(), (List) entry.getValue(), writer, k0.a().d(list.get(0).getClass()));
                    return;
                case 18:
                    List list2 = (List) entry.getValue();
                    if (list2 == null || list2.isEmpty()) {
                        return;
                    }
                    o0.X(dVar.getNumber(), (List) entry.getValue(), writer, k0.a().d(list2.get(0).getClass()));
                    return;
                default:
                    return;
            }
        }
        switch (a.a[dVar.n().ordinal()]) {
            case 1:
                writer.e(dVar.getNumber(), ((Double) entry.getValue()).doubleValue());
                return;
            case 2:
                writer.A(dVar.getNumber(), ((Float) entry.getValue()).floatValue());
                return;
            case 3:
                writer.n(dVar.getNumber(), ((Long) entry.getValue()).longValue());
                return;
            case 4:
                writer.l(dVar.getNumber(), ((Long) entry.getValue()).longValue());
                return;
            case 5:
                writer.r(dVar.getNumber(), ((Integer) entry.getValue()).intValue());
                return;
            case 6:
                writer.h(dVar.getNumber(), ((Long) entry.getValue()).longValue());
                return;
            case 7:
                writer.d(dVar.getNumber(), ((Integer) entry.getValue()).intValue());
                return;
            case 8:
                writer.o(dVar.getNumber(), ((Boolean) entry.getValue()).booleanValue());
                return;
            case 9:
                writer.b(dVar.getNumber(), ((Integer) entry.getValue()).intValue());
                return;
            case 10:
                writer.p(dVar.getNumber(), ((Integer) entry.getValue()).intValue());
                return;
            case 11:
                writer.u(dVar.getNumber(), ((Long) entry.getValue()).longValue());
                return;
            case 12:
                writer.H(dVar.getNumber(), ((Integer) entry.getValue()).intValue());
                return;
            case 13:
                writer.z(dVar.getNumber(), ((Long) entry.getValue()).longValue());
                return;
            case 14:
                writer.r(dVar.getNumber(), ((Integer) entry.getValue()).intValue());
                return;
            case 15:
                writer.O(dVar.getNumber(), (ByteString) entry.getValue());
                return;
            case 16:
                writer.k(dVar.getNumber(), (String) entry.getValue());
                return;
            case 17:
                writer.L(dVar.getNumber(), entry.getValue(), k0.a().d(entry.getValue().getClass()));
                return;
            case 18:
                writer.J(dVar.getNumber(), entry.getValue(), k0.a().d(entry.getValue().getClass()));
                return;
            default:
                return;
        }
    }
}
