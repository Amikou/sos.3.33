package com.google.crypto.tink.shaded.protobuf;

import com.google.crypto.tink.shaded.protobuf.r;
import java.lang.Comparable;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

/* compiled from: SmallSortedMap.java */
/* loaded from: classes2.dex */
public class p0<K extends Comparable<K>, V> extends AbstractMap<K, V> {
    public final int a;
    public List<p0<K, V>.e> f0;
    public Map<K, V> g0;
    public boolean h0;
    public volatile p0<K, V>.g i0;
    public Map<K, V> j0;
    public volatile p0<K, V>.c k0;

    /* compiled from: SmallSortedMap.java */
    /* loaded from: classes2.dex */
    public class a extends p0<FieldDescriptorType, Object> {
        public a(int i) {
            super(i, null);
        }

        @Override // com.google.crypto.tink.shaded.protobuf.p0
        public void p() {
            if (!o()) {
                for (int i = 0; i < k(); i++) {
                    Map.Entry<FieldDescriptorType, Object> j = j(i);
                    if (((r.b) j.getKey()).i()) {
                        j.setValue(Collections.unmodifiableList((List) j.getValue()));
                    }
                }
                Iterator it = m().iterator();
                while (it.hasNext()) {
                    Map.Entry entry = (Map.Entry) it.next();
                    if (((r.b) entry.getKey()).i()) {
                        entry.setValue(Collections.unmodifiableList((List) entry.getValue()));
                    }
                }
            }
            super.p();
        }
    }

    /* compiled from: SmallSortedMap.java */
    /* loaded from: classes2.dex */
    public class c extends p0<K, V>.g {
        public c() {
            super(p0.this, null);
        }

        @Override // com.google.crypto.tink.shaded.protobuf.p0.g, java.util.AbstractCollection, java.util.Collection, java.lang.Iterable, java.util.Set
        public Iterator<Map.Entry<K, V>> iterator() {
            return new b(p0.this, null);
        }

        public /* synthetic */ c(p0 p0Var, a aVar) {
            this();
        }
    }

    /* compiled from: SmallSortedMap.java */
    /* loaded from: classes2.dex */
    public static class d {
        public static final Iterator<Object> a = new a();
        public static final Iterable<Object> b = new b();

        /* compiled from: SmallSortedMap.java */
        /* loaded from: classes2.dex */
        public class a implements Iterator<Object> {
            @Override // java.util.Iterator
            public boolean hasNext() {
                return false;
            }

            @Override // java.util.Iterator
            public Object next() {
                throw new NoSuchElementException();
            }

            @Override // java.util.Iterator
            public void remove() {
                throw new UnsupportedOperationException();
            }
        }

        /* compiled from: SmallSortedMap.java */
        /* loaded from: classes2.dex */
        public class b implements Iterable<Object> {
            @Override // java.lang.Iterable
            public Iterator<Object> iterator() {
                return d.a;
            }
        }

        public static <T> Iterable<T> b() {
            return (Iterable<T>) b;
        }
    }

    /* compiled from: SmallSortedMap.java */
    /* loaded from: classes2.dex */
    public class e implements Map.Entry<K, V>, Comparable<p0<K, V>.e> {
        public final K a;
        public V f0;

        public e(p0 p0Var, Map.Entry<K, V> entry) {
            this(entry.getKey(), entry.getValue());
        }

        @Override // java.lang.Comparable
        /* renamed from: a */
        public int compareTo(p0<K, V>.e eVar) {
            return getKey().compareTo(eVar.getKey());
        }

        public final boolean d(Object obj, Object obj2) {
            if (obj == null) {
                return obj2 == null;
            }
            return obj.equals(obj2);
        }

        @Override // java.util.Map.Entry
        /* renamed from: e */
        public K getKey() {
            return this.a;
        }

        @Override // java.util.Map.Entry
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (obj instanceof Map.Entry) {
                Map.Entry entry = (Map.Entry) obj;
                return d(this.a, entry.getKey()) && d(this.f0, entry.getValue());
            }
            return false;
        }

        @Override // java.util.Map.Entry
        public V getValue() {
            return this.f0;
        }

        @Override // java.util.Map.Entry
        public int hashCode() {
            K k = this.a;
            int hashCode = k == null ? 0 : k.hashCode();
            V v = this.f0;
            return hashCode ^ (v != null ? v.hashCode() : 0);
        }

        @Override // java.util.Map.Entry
        public V setValue(V v) {
            p0.this.g();
            V v2 = this.f0;
            this.f0 = v;
            return v2;
        }

        public String toString() {
            return this.a + "=" + this.f0;
        }

        public e(K k, V v) {
            this.a = k;
            this.f0 = v;
        }
    }

    /* compiled from: SmallSortedMap.java */
    /* loaded from: classes2.dex */
    public class g extends AbstractSet<Map.Entry<K, V>> {
        public g() {
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public void clear() {
            p0.this.clear();
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean contains(Object obj) {
            Map.Entry entry = (Map.Entry) obj;
            Object obj2 = p0.this.get(entry.getKey());
            Object value = entry.getValue();
            return obj2 == value || (obj2 != null && obj2.equals(value));
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        /* renamed from: e */
        public boolean add(Map.Entry<K, V> entry) {
            if (contains(entry)) {
                return false;
            }
            p0.this.put(entry.getKey(), entry.getValue());
            return true;
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable, java.util.Set
        public Iterator<Map.Entry<K, V>> iterator() {
            return new f(p0.this, null);
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean remove(Object obj) {
            Map.Entry entry = (Map.Entry) obj;
            if (contains(entry)) {
                p0.this.remove(entry.getKey());
                return true;
            }
            return false;
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public int size() {
            return p0.this.size();
        }

        public /* synthetic */ g(p0 p0Var, a aVar) {
            this();
        }
    }

    public /* synthetic */ p0(int i, a aVar) {
        this(i);
    }

    public static <FieldDescriptorType extends r.b<FieldDescriptorType>> p0<FieldDescriptorType, Object> q(int i) {
        return new a(i);
    }

    @Override // java.util.AbstractMap, java.util.Map
    public void clear() {
        g();
        if (!this.f0.isEmpty()) {
            this.f0.clear();
        }
        if (this.g0.isEmpty()) {
            return;
        }
        this.g0.clear();
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.AbstractMap, java.util.Map
    public boolean containsKey(Object obj) {
        Comparable comparable = (Comparable) obj;
        return f(comparable) >= 0 || this.g0.containsKey(comparable);
    }

    @Override // java.util.AbstractMap, java.util.Map
    public Set<Map.Entry<K, V>> entrySet() {
        if (this.i0 == null) {
            this.i0 = new g(this, null);
        }
        return this.i0;
    }

    @Override // java.util.AbstractMap, java.util.Map
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof p0)) {
            return super.equals(obj);
        }
        p0 p0Var = (p0) obj;
        int size = size();
        if (size != p0Var.size()) {
            return false;
        }
        int k = k();
        if (k != p0Var.k()) {
            return entrySet().equals(p0Var.entrySet());
        }
        for (int i = 0; i < k; i++) {
            if (!j(i).equals(p0Var.j(i))) {
                return false;
            }
        }
        if (k != size) {
            return this.g0.equals(p0Var.g0);
        }
        return true;
    }

    public final int f(K k) {
        int size = this.f0.size() - 1;
        if (size >= 0) {
            int compareTo = k.compareTo(this.f0.get(size).getKey());
            if (compareTo > 0) {
                return -(size + 2);
            }
            if (compareTo == 0) {
                return size;
            }
        }
        int i = 0;
        while (i <= size) {
            int i2 = (i + size) / 2;
            int compareTo2 = k.compareTo(this.f0.get(i2).getKey());
            if (compareTo2 < 0) {
                size = i2 - 1;
            } else if (compareTo2 <= 0) {
                return i2;
            } else {
                i = i2 + 1;
            }
        }
        return -(i + 1);
    }

    public final void g() {
        if (this.h0) {
            throw new UnsupportedOperationException();
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.AbstractMap, java.util.Map
    public V get(Object obj) {
        Comparable comparable = (Comparable) obj;
        int f2 = f(comparable);
        if (f2 >= 0) {
            return this.f0.get(f2).getValue();
        }
        return this.g0.get(comparable);
    }

    public Set<Map.Entry<K, V>> h() {
        if (this.k0 == null) {
            this.k0 = new c(this, null);
        }
        return this.k0;
    }

    @Override // java.util.AbstractMap, java.util.Map
    public int hashCode() {
        int k = k();
        int i = 0;
        for (int i2 = 0; i2 < k; i2++) {
            i += this.f0.get(i2).hashCode();
        }
        return l() > 0 ? i + this.g0.hashCode() : i;
    }

    public final void i() {
        g();
        if (!this.f0.isEmpty() || (this.f0 instanceof ArrayList)) {
            return;
        }
        this.f0 = new ArrayList(this.a);
    }

    public Map.Entry<K, V> j(int i) {
        return this.f0.get(i);
    }

    public int k() {
        return this.f0.size();
    }

    public int l() {
        return this.g0.size();
    }

    public Iterable<Map.Entry<K, V>> m() {
        if (this.g0.isEmpty()) {
            return d.b();
        }
        return this.g0.entrySet();
    }

    public final SortedMap<K, V> n() {
        g();
        if (this.g0.isEmpty() && !(this.g0 instanceof TreeMap)) {
            TreeMap treeMap = new TreeMap();
            this.g0 = treeMap;
            this.j0 = treeMap.descendingMap();
        }
        return (SortedMap) this.g0;
    }

    public boolean o() {
        return this.h0;
    }

    public void p() {
        Map<K, V> unmodifiableMap;
        Map<K, V> unmodifiableMap2;
        if (this.h0) {
            return;
        }
        if (this.g0.isEmpty()) {
            unmodifiableMap = Collections.emptyMap();
        } else {
            unmodifiableMap = Collections.unmodifiableMap(this.g0);
        }
        this.g0 = unmodifiableMap;
        if (this.j0.isEmpty()) {
            unmodifiableMap2 = Collections.emptyMap();
        } else {
            unmodifiableMap2 = Collections.unmodifiableMap(this.j0);
        }
        this.j0 = unmodifiableMap2;
        this.h0 = true;
    }

    @Override // java.util.AbstractMap, java.util.Map
    /* renamed from: r */
    public V put(K k, V v) {
        g();
        int f2 = f(k);
        if (f2 >= 0) {
            return this.f0.get(f2).setValue(v);
        }
        i();
        int i = -(f2 + 1);
        if (i >= this.a) {
            return n().put(k, v);
        }
        int size = this.f0.size();
        int i2 = this.a;
        if (size == i2) {
            p0<K, V>.e remove = this.f0.remove(i2 - 1);
            n().put((K) remove.getKey(), remove.getValue());
        }
        this.f0.add(i, new e(k, v));
        return null;
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.AbstractMap, java.util.Map
    public V remove(Object obj) {
        g();
        Comparable comparable = (Comparable) obj;
        int f2 = f(comparable);
        if (f2 >= 0) {
            return (V) s(f2);
        }
        if (this.g0.isEmpty()) {
            return null;
        }
        return this.g0.remove(comparable);
    }

    public final V s(int i) {
        g();
        V value = this.f0.remove(i).getValue();
        if (!this.g0.isEmpty()) {
            Iterator<Map.Entry<K, V>> it = n().entrySet().iterator();
            this.f0.add(new e(this, it.next()));
            it.remove();
        }
        return value;
    }

    @Override // java.util.AbstractMap, java.util.Map
    public int size() {
        return this.f0.size() + this.g0.size();
    }

    /* compiled from: SmallSortedMap.java */
    /* loaded from: classes2.dex */
    public class b implements Iterator<Map.Entry<K, V>> {
        public int a;
        public Iterator<Map.Entry<K, V>> f0;

        public b() {
            this.a = p0.this.f0.size();
        }

        public final Iterator<Map.Entry<K, V>> a() {
            if (this.f0 == null) {
                this.f0 = p0.this.j0.entrySet().iterator();
            }
            return this.f0;
        }

        @Override // java.util.Iterator
        /* renamed from: b */
        public Map.Entry<K, V> next() {
            if (!a().hasNext()) {
                List list = p0.this.f0;
                int i = this.a - 1;
                this.a = i;
                return (Map.Entry) list.get(i);
            }
            return a().next();
        }

        @Override // java.util.Iterator
        public boolean hasNext() {
            int i = this.a;
            return (i > 0 && i <= p0.this.f0.size()) || a().hasNext();
        }

        @Override // java.util.Iterator
        public void remove() {
            throw new UnsupportedOperationException();
        }

        public /* synthetic */ b(p0 p0Var, a aVar) {
            this();
        }
    }

    /* compiled from: SmallSortedMap.java */
    /* loaded from: classes2.dex */
    public class f implements Iterator<Map.Entry<K, V>> {
        public int a;
        public boolean f0;
        public Iterator<Map.Entry<K, V>> g0;

        public f() {
            this.a = -1;
        }

        public final Iterator<Map.Entry<K, V>> a() {
            if (this.g0 == null) {
                this.g0 = p0.this.g0.entrySet().iterator();
            }
            return this.g0;
        }

        @Override // java.util.Iterator
        /* renamed from: b */
        public Map.Entry<K, V> next() {
            this.f0 = true;
            int i = this.a + 1;
            this.a = i;
            if (i < p0.this.f0.size()) {
                return (Map.Entry) p0.this.f0.get(this.a);
            }
            return a().next();
        }

        @Override // java.util.Iterator
        public boolean hasNext() {
            if (this.a + 1 >= p0.this.f0.size()) {
                return !p0.this.g0.isEmpty() && a().hasNext();
            }
            return true;
        }

        @Override // java.util.Iterator
        public void remove() {
            if (this.f0) {
                this.f0 = false;
                p0.this.g();
                if (this.a < p0.this.f0.size()) {
                    p0 p0Var = p0.this;
                    int i = this.a;
                    this.a = i - 1;
                    p0Var.s(i);
                    return;
                }
                a().remove();
                return;
            }
            throw new IllegalStateException("remove() was called before next()");
        }

        public /* synthetic */ f(p0 p0Var, a aVar) {
            this();
        }
    }

    public p0(int i) {
        this.a = i;
        this.f0 = Collections.emptyList();
        this.g0 = Collections.emptyMap();
        this.j0 = Collections.emptyMap();
    }
}
