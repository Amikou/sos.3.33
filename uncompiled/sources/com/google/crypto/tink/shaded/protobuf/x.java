package com.google.crypto.tink.shaded.protobuf;

/* compiled from: LazyFieldLite.java */
/* loaded from: classes2.dex */
public class x {
    public ByteString a;
    public n b;
    public volatile e0 c;
    public volatile ByteString d;

    static {
        n.b();
    }

    public void a(e0 e0Var) {
        if (this.c != null) {
            return;
        }
        synchronized (this) {
            if (this.c != null) {
                return;
            }
            try {
                if (this.a != null) {
                    this.c = e0Var.getParserForType().a(this.a, this.b);
                    this.d = this.a;
                } else {
                    this.c = e0Var;
                    this.d = ByteString.EMPTY;
                }
            } catch (InvalidProtocolBufferException unused) {
                this.c = e0Var;
                this.d = ByteString.EMPTY;
            }
        }
    }

    public int b() {
        if (this.d != null) {
            return this.d.size();
        }
        ByteString byteString = this.a;
        if (byteString != null) {
            return byteString.size();
        }
        if (this.c != null) {
            return this.c.getSerializedSize();
        }
        return 0;
    }

    public e0 c(e0 e0Var) {
        a(e0Var);
        return this.c;
    }

    public e0 d(e0 e0Var) {
        e0 e0Var2 = this.c;
        this.a = null;
        this.d = null;
        this.c = e0Var;
        return e0Var2;
    }

    public ByteString e() {
        if (this.d != null) {
            return this.d;
        }
        ByteString byteString = this.a;
        if (byteString != null) {
            return byteString;
        }
        synchronized (this) {
            if (this.d != null) {
                return this.d;
            }
            if (this.c == null) {
                this.d = ByteString.EMPTY;
            } else {
                this.d = this.c.toByteString();
            }
            return this.d;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof x) {
            x xVar = (x) obj;
            e0 e0Var = this.c;
            e0 e0Var2 = xVar.c;
            if (e0Var == null && e0Var2 == null) {
                return e().equals(xVar.e());
            }
            if (e0Var == null || e0Var2 == null) {
                if (e0Var != null) {
                    return e0Var.equals(xVar.c(e0Var.getDefaultInstanceForType()));
                }
                return c(e0Var2.getDefaultInstanceForType()).equals(e0Var2);
            }
            return e0Var.equals(e0Var2);
        }
        return false;
    }

    public int hashCode() {
        return 1;
    }
}
