package com.google.crypto.tink.shaded.protobuf;

import com.google.crypto.tink.shaded.protobuf.GeneratedMessageLite;
import com.google.crypto.tink.shaded.protobuf.GeneratedMessageLite.a;
import com.google.crypto.tink.shaded.protobuf.WireFormat;
import com.google.crypto.tink.shaded.protobuf.a;
import com.google.crypto.tink.shaded.protobuf.d;
import com.google.crypto.tink.shaded.protobuf.e0;
import com.google.crypto.tink.shaded.protobuf.r;
import com.google.crypto.tink.shaded.protobuf.v;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* loaded from: classes2.dex */
public abstract class GeneratedMessageLite<MessageType extends GeneratedMessageLite<MessageType, BuilderType>, BuilderType extends a<MessageType, BuilderType>> extends com.google.crypto.tink.shaded.protobuf.a<MessageType, BuilderType> {
    private static Map<Object, GeneratedMessageLite<?, ?>> defaultInstanceMap = new ConcurrentHashMap();
    public r0 unknownFields = r0.e();
    public int memoizedSerializedSize = -1;

    /* loaded from: classes2.dex */
    public enum MethodToInvoke {
        GET_MEMOIZED_IS_INITIALIZED,
        SET_MEMOIZED_IS_INITIALIZED,
        BUILD_MESSAGE_INFO,
        NEW_MUTABLE_INSTANCE,
        NEW_BUILDER,
        GET_DEFAULT_INSTANCE,
        GET_PARSER
    }

    /* loaded from: classes2.dex */
    public static abstract class a<MessageType extends GeneratedMessageLite<MessageType, BuilderType>, BuilderType extends a<MessageType, BuilderType>> extends a.AbstractC0137a<MessageType, BuilderType> {
        public final MessageType a;
        public MessageType f0;
        public boolean g0 = false;

        public a(MessageType messagetype) {
            this.a = messagetype;
            this.f0 = (MessageType) messagetype.j(MethodToInvoke.NEW_MUTABLE_INSTANCE);
        }

        @Override // com.google.crypto.tink.shaded.protobuf.e0.a
        /* renamed from: f */
        public final MessageType build() {
            MessageType buildPartial = buildPartial();
            if (buildPartial.isInitialized()) {
                return buildPartial;
            }
            throw a.AbstractC0137a.e(buildPartial);
        }

        @Override // com.google.crypto.tink.shaded.protobuf.e0.a
        /* renamed from: g */
        public MessageType buildPartial() {
            if (this.g0) {
                return this.f0;
            }
            this.f0.r();
            this.g0 = true;
            return this.f0;
        }

        /* renamed from: h */
        public BuilderType clone() {
            BuilderType buildertype = (BuilderType) getDefaultInstanceForType().newBuilderForType();
            buildertype.p(buildPartial());
            return buildertype;
        }

        public final void j() {
            if (this.g0) {
                k();
                this.g0 = false;
            }
        }

        public void k() {
            MessageType messagetype = (MessageType) this.f0.j(MethodToInvoke.NEW_MUTABLE_INSTANCE);
            r(messagetype, this.f0);
            this.f0 = messagetype;
        }

        @Override // defpackage.e82
        /* renamed from: l */
        public MessageType getDefaultInstanceForType() {
            return this.a;
        }

        @Override // com.google.crypto.tink.shaded.protobuf.a.AbstractC0137a
        /* renamed from: o */
        public BuilderType b(MessageType messagetype) {
            return p(messagetype);
        }

        public BuilderType p(MessageType messagetype) {
            j();
            r(this.f0, messagetype);
            return this;
        }

        public final void r(MessageType messagetype, MessageType messagetype2) {
            k0.a().e(messagetype).a(messagetype, messagetype2);
        }
    }

    /* loaded from: classes2.dex */
    public static class b<T extends GeneratedMessageLite<T, ?>> extends com.google.crypto.tink.shaded.protobuf.b<T> {
        public final T a;

        public b(T t) {
            this.a = t;
        }

        @Override // com.google.crypto.tink.shaded.protobuf.j0
        /* renamed from: g */
        public T b(i iVar, n nVar) throws InvalidProtocolBufferException {
            return (T) GeneratedMessageLite.y(this.a, iVar, nVar);
        }
    }

    /* loaded from: classes2.dex */
    public static abstract class c<MessageType extends c<MessageType, BuilderType>, BuilderType> extends GeneratedMessageLite<MessageType, BuilderType> implements e82 {
        public r<d> extensions = r.h();

        public r<d> C() {
            if (this.extensions.n()) {
                this.extensions = this.extensions.clone();
            }
            return this.extensions;
        }
    }

    /* loaded from: classes2.dex */
    public static final class d implements r.b<d> {
        public final v.d<?> a;
        public final int f0;
        public final WireFormat.FieldType g0;
        public final boolean h0;
        public final boolean i0;

        @Override // java.lang.Comparable
        /* renamed from: a */
        public int compareTo(d dVar) {
            return this.f0 - dVar.f0;
        }

        public v.d<?> d() {
            return this.a;
        }

        @Override // com.google.crypto.tink.shaded.protobuf.r.b
        public int getNumber() {
            return this.f0;
        }

        @Override // com.google.crypto.tink.shaded.protobuf.r.b
        public boolean i() {
            return this.h0;
        }

        @Override // com.google.crypto.tink.shaded.protobuf.r.b
        public boolean isPacked() {
            return this.i0;
        }

        @Override // com.google.crypto.tink.shaded.protobuf.r.b
        public WireFormat.FieldType n() {
            return this.g0;
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.google.crypto.tink.shaded.protobuf.r.b
        public e0.a y0(e0.a aVar, e0 e0Var) {
            return ((a) aVar).p((GeneratedMessageLite) e0Var);
        }

        @Override // com.google.crypto.tink.shaded.protobuf.r.b
        public WireFormat.JavaType z() {
            return this.g0.getJavaType();
        }
    }

    /* loaded from: classes2.dex */
    public static class e<ContainingType extends e0, Type> extends m<ContainingType, Type> {
        public final e0 a;
        public final d b;

        public WireFormat.FieldType a() {
            return this.b.n();
        }

        public e0 b() {
            return this.a;
        }

        public int c() {
            return this.b.getNumber();
        }

        public boolean d() {
            return this.b.h0;
        }
    }

    public static <T extends GeneratedMessageLite<?, ?>> void A(Class<T> cls, T t) {
        defaultInstanceMap.put(cls, t);
    }

    public static <T extends GeneratedMessageLite<T, ?>> T h(T t) throws InvalidProtocolBufferException {
        if (t == null || t.isInitialized()) {
            return t;
        }
        throw t.e().asInvalidProtocolBufferException().setUnfinishedMessage(t);
    }

    public static <E> v.i<E> m() {
        return l0.k();
    }

    public static <T extends GeneratedMessageLite<?, ?>> T n(Class<T> cls) {
        GeneratedMessageLite<?, ?> generatedMessageLite = defaultInstanceMap.get(cls);
        if (generatedMessageLite == null) {
            try {
                Class.forName(cls.getName(), true, cls.getClassLoader());
                generatedMessageLite = defaultInstanceMap.get(cls);
            } catch (ClassNotFoundException e2) {
                throw new IllegalStateException("Class initialization cannot fail.", e2);
            }
        }
        if (generatedMessageLite == null) {
            generatedMessageLite = (T) ((GeneratedMessageLite) t0.j(cls)).getDefaultInstanceForType();
            if (generatedMessageLite != null) {
                defaultInstanceMap.put(cls, generatedMessageLite);
            } else {
                throw new IllegalStateException();
            }
        }
        return (T) generatedMessageLite;
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    public static Object p(Method method, Object obj, Object... objArr) {
        try {
            return method.invoke(obj, objArr);
        } catch (IllegalAccessException e2) {
            throw new RuntimeException("Couldn't use Java reflection to implement protocol message reflection.", e2);
        } catch (InvocationTargetException e3) {
            Throwable cause = e3.getCause();
            if (!(cause instanceof RuntimeException)) {
                if (cause instanceof Error) {
                    throw ((Error) cause);
                }
                throw new RuntimeException("Unexpected exception thrown by generated accessor method.", cause);
            }
            throw ((RuntimeException) cause);
        }
    }

    public static final <T extends GeneratedMessageLite<T, ?>> boolean q(T t, boolean z) {
        byte byteValue = ((Byte) t.j(MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED)).byteValue();
        if (byteValue == 1) {
            return true;
        }
        if (byteValue == 0) {
            return false;
        }
        boolean f = k0.a().e(t).f(t);
        if (z) {
            t.k(MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED, f ? t : null);
        }
        return f;
    }

    public static <E> v.i<E> s(v.i<E> iVar) {
        int size = iVar.size();
        return iVar.a(size == 0 ? 10 : size * 2);
    }

    public static Object u(e0 e0Var, String str, Object[] objArr) {
        return new x33(e0Var, str, objArr);
    }

    public static <T extends GeneratedMessageLite<T, ?>> T v(T t, ByteString byteString, n nVar) throws InvalidProtocolBufferException {
        return (T) h(x(t, byteString, nVar));
    }

    public static <T extends GeneratedMessageLite<T, ?>> T w(T t, byte[] bArr, n nVar) throws InvalidProtocolBufferException {
        return (T) h(z(t, bArr, 0, bArr.length, nVar));
    }

    public static <T extends GeneratedMessageLite<T, ?>> T x(T t, ByteString byteString, n nVar) throws InvalidProtocolBufferException {
        try {
            i newCodedInput = byteString.newCodedInput();
            T t2 = (T) y(t, newCodedInput, nVar);
            try {
                newCodedInput.a(0);
                return t2;
            } catch (InvalidProtocolBufferException e2) {
                throw e2.setUnfinishedMessage(t2);
            }
        } catch (InvalidProtocolBufferException e3) {
            throw e3;
        }
    }

    public static <T extends GeneratedMessageLite<T, ?>> T y(T t, i iVar, n nVar) throws InvalidProtocolBufferException {
        T t2 = (T) t.j(MethodToInvoke.NEW_MUTABLE_INSTANCE);
        try {
            n0 e2 = k0.a().e(t2);
            e2.j(t2, j.Q(iVar), nVar);
            e2.e(t2);
            return t2;
        } catch (IOException e3) {
            if (e3.getCause() instanceof InvalidProtocolBufferException) {
                throw ((InvalidProtocolBufferException) e3.getCause());
            }
            throw new InvalidProtocolBufferException(e3.getMessage()).setUnfinishedMessage(t2);
        } catch (RuntimeException e4) {
            if (e4.getCause() instanceof InvalidProtocolBufferException) {
                throw ((InvalidProtocolBufferException) e4.getCause());
            }
            throw e4;
        }
    }

    public static <T extends GeneratedMessageLite<T, ?>> T z(T t, byte[] bArr, int i, int i2, n nVar) throws InvalidProtocolBufferException {
        T t2 = (T) t.j(MethodToInvoke.NEW_MUTABLE_INSTANCE);
        try {
            n0 e2 = k0.a().e(t2);
            e2.h(t2, bArr, i, i + i2, new d.b(nVar));
            e2.e(t2);
            if (t2.memoizedHashCode == 0) {
                return t2;
            }
            throw new RuntimeException();
        } catch (IOException e3) {
            if (e3.getCause() instanceof InvalidProtocolBufferException) {
                throw ((InvalidProtocolBufferException) e3.getCause());
            }
            throw new InvalidProtocolBufferException(e3.getMessage()).setUnfinishedMessage(t2);
        } catch (IndexOutOfBoundsException unused) {
            throw InvalidProtocolBufferException.truncatedMessage().setUnfinishedMessage(t2);
        }
    }

    @Override // com.google.crypto.tink.shaded.protobuf.e0
    /* renamed from: B */
    public final BuilderType toBuilder() {
        BuilderType buildertype = (BuilderType) j(MethodToInvoke.NEW_BUILDER);
        buildertype.p(this);
        return buildertype;
    }

    @Override // com.google.crypto.tink.shaded.protobuf.e0
    public void a(CodedOutputStream codedOutputStream) throws IOException {
        k0.a().e(this).i(this, k.P(codedOutputStream));
    }

    @Override // com.google.crypto.tink.shaded.protobuf.a
    int b() {
        return this.memoizedSerializedSize;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (getDefaultInstanceForType().getClass().isInstance(obj)) {
            return k0.a().e(this).b(this, (GeneratedMessageLite) obj);
        }
        return false;
    }

    @Override // com.google.crypto.tink.shaded.protobuf.a
    void f(int i) {
        this.memoizedSerializedSize = i;
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    public Object g() throws Exception {
        return j(MethodToInvoke.BUILD_MESSAGE_INFO);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.e0
    public final j0<MessageType> getParserForType() {
        return (j0) j(MethodToInvoke.GET_PARSER);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.e0
    public int getSerializedSize() {
        if (this.memoizedSerializedSize == -1) {
            this.memoizedSerializedSize = k0.a().e(this).g(this);
        }
        return this.memoizedSerializedSize;
    }

    public int hashCode() {
        int i = this.memoizedHashCode;
        if (i != 0) {
            return i;
        }
        int d2 = k0.a().e(this).d(this);
        this.memoizedHashCode = d2;
        return d2;
    }

    public final <MessageType extends GeneratedMessageLite<MessageType, BuilderType>, BuilderType extends a<MessageType, BuilderType>> BuilderType i() {
        return (BuilderType) j(MethodToInvoke.NEW_BUILDER);
    }

    @Override // defpackage.e82
    public final boolean isInitialized() {
        return q(this, true);
    }

    public Object j(MethodToInvoke methodToInvoke) {
        return l(methodToInvoke, null, null);
    }

    public Object k(MethodToInvoke methodToInvoke, Object obj) {
        return l(methodToInvoke, obj, null);
    }

    public abstract Object l(MethodToInvoke methodToInvoke, Object obj, Object obj2);

    @Override // defpackage.e82
    /* renamed from: o */
    public final MessageType getDefaultInstanceForType() {
        return (MessageType) j(MethodToInvoke.GET_DEFAULT_INSTANCE);
    }

    public void r() {
        k0.a().e(this).e(this);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.e0
    /* renamed from: t */
    public final BuilderType newBuilderForType() {
        return (BuilderType) j(MethodToInvoke.NEW_BUILDER);
    }

    public String toString() {
        return f0.e(this, super.toString());
    }
}
