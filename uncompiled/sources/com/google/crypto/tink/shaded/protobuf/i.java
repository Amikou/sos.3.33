package com.google.crypto.tink.shaded.protobuf;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* compiled from: CodedInputStream.java */
/* loaded from: classes2.dex */
public abstract class i {
    public int a;
    public int b;
    public int c;
    public j d;

    /* compiled from: CodedInputStream.java */
    /* loaded from: classes2.dex */
    public static final class b extends i {
        public final byte[] e;
        public final boolean f;
        public int g;
        public int h;
        public int i;
        public int j;
        public int k;
        public boolean l;
        public int m;

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public String A() throws IOException {
            int K = K();
            if (K > 0) {
                int i = this.g;
                int i2 = this.i;
                if (K <= i - i2) {
                    String str = new String(this.e, i2, K, v.a);
                    this.i += K;
                    return str;
                }
            }
            if (K == 0) {
                return "";
            }
            if (K < 0) {
                throw InvalidProtocolBufferException.negativeSize();
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public String B() throws IOException {
            int K = K();
            if (K > 0) {
                int i = this.g;
                int i2 = this.i;
                if (K <= i - i2) {
                    String h = Utf8.h(this.e, i2, K);
                    this.i += K;
                    return h;
                }
            }
            if (K == 0) {
                return "";
            }
            if (K <= 0) {
                throw InvalidProtocolBufferException.negativeSize();
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public int C() throws IOException {
            if (e()) {
                this.k = 0;
                return 0;
            }
            int K = K();
            this.k = K;
            if (WireFormat.a(K) != 0) {
                return this.k;
            }
            throw InvalidProtocolBufferException.invalidTag();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public int D() throws IOException {
            return K();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public long E() throws IOException {
            return L();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public boolean F(int i) throws IOException {
            int b = WireFormat.b(i);
            if (b == 0) {
                Q();
                return true;
            } else if (b == 1) {
                P(8);
                return true;
            } else if (b == 2) {
                P(K());
                return true;
            } else if (b == 3) {
                O();
                a(WireFormat.c(WireFormat.a(i), 4));
                return true;
            } else if (b != 4) {
                if (b == 5) {
                    P(4);
                    return true;
                }
                throw InvalidProtocolBufferException.invalidWireType();
            } else {
                return false;
            }
        }

        public byte G() throws IOException {
            int i = this.i;
            if (i != this.g) {
                byte[] bArr = this.e;
                this.i = i + 1;
                return bArr[i];
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }

        public byte[] H(int i) throws IOException {
            if (i > 0) {
                int i2 = this.g;
                int i3 = this.i;
                if (i <= i2 - i3) {
                    int i4 = i + i3;
                    this.i = i4;
                    return Arrays.copyOfRange(this.e, i3, i4);
                }
            }
            if (i <= 0) {
                if (i == 0) {
                    return v.b;
                }
                throw InvalidProtocolBufferException.negativeSize();
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }

        public int I() throws IOException {
            int i = this.i;
            if (this.g - i >= 4) {
                byte[] bArr = this.e;
                this.i = i + 4;
                return ((bArr[i + 3] & 255) << 24) | (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << 16);
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }

        public long J() throws IOException {
            int i = this.i;
            if (this.g - i >= 8) {
                byte[] bArr = this.e;
                this.i = i + 8;
                return ((bArr[i + 7] & 255) << 56) | (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << 16) | ((bArr[i + 3] & 255) << 24) | ((bArr[i + 4] & 255) << 32) | ((bArr[i + 5] & 255) << 40) | ((bArr[i + 6] & 255) << 48);
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }

        /* JADX WARN: Code restructure failed: missing block: B:33:0x0068, code lost:
            if (r2[r3] < 0) goto L34;
         */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public int K() throws java.io.IOException {
            /*
                r5 = this;
                int r0 = r5.i
                int r1 = r5.g
                if (r1 != r0) goto L7
                goto L6a
            L7:
                byte[] r2 = r5.e
                int r3 = r0 + 1
                r0 = r2[r0]
                if (r0 < 0) goto L12
                r5.i = r3
                return r0
            L12:
                int r1 = r1 - r3
                r4 = 9
                if (r1 >= r4) goto L18
                goto L6a
            L18:
                int r1 = r3 + 1
                r3 = r2[r3]
                int r3 = r3 << 7
                r0 = r0 ^ r3
                if (r0 >= 0) goto L24
                r0 = r0 ^ (-128(0xffffffffffffff80, float:NaN))
                goto L70
            L24:
                int r3 = r1 + 1
                r1 = r2[r1]
                int r1 = r1 << 14
                r0 = r0 ^ r1
                if (r0 < 0) goto L31
                r0 = r0 ^ 16256(0x3f80, float:2.278E-41)
            L2f:
                r1 = r3
                goto L70
            L31:
                int r1 = r3 + 1
                r3 = r2[r3]
                int r3 = r3 << 21
                r0 = r0 ^ r3
                if (r0 >= 0) goto L3f
                r2 = -2080896(0xffffffffffe03f80, float:NaN)
                r0 = r0 ^ r2
                goto L70
            L3f:
                int r3 = r1 + 1
                r1 = r2[r1]
                int r4 = r1 << 28
                r0 = r0 ^ r4
                r4 = 266354560(0xfe03f80, float:2.2112565E-29)
                r0 = r0 ^ r4
                if (r1 >= 0) goto L2f
                int r1 = r3 + 1
                r3 = r2[r3]
                if (r3 >= 0) goto L70
                int r3 = r1 + 1
                r1 = r2[r1]
                if (r1 >= 0) goto L2f
                int r1 = r3 + 1
                r3 = r2[r3]
                if (r3 >= 0) goto L70
                int r3 = r1 + 1
                r1 = r2[r1]
                if (r1 >= 0) goto L2f
                int r1 = r3 + 1
                r2 = r2[r3]
                if (r2 >= 0) goto L70
            L6a:
                long r0 = r5.M()
                int r0 = (int) r0
                return r0
            L70:
                r5.i = r1
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.crypto.tink.shaded.protobuf.i.b.K():int");
        }

        /* JADX WARN: Code restructure failed: missing block: B:39:0x00b4, code lost:
            if (r2[r0] < 0) goto L42;
         */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public long L() throws java.io.IOException {
            /*
                Method dump skipped, instructions count: 192
                To view this dump change 'Code comments level' option to 'DEBUG'
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.crypto.tink.shaded.protobuf.i.b.L():long");
        }

        public long M() throws IOException {
            long j = 0;
            for (int i = 0; i < 64; i += 7) {
                byte G = G();
                j |= (G & Byte.MAX_VALUE) << i;
                if ((G & 128) == 0) {
                    return j;
                }
            }
            throw InvalidProtocolBufferException.malformedVarint();
        }

        public final void N() {
            int i = this.g + this.h;
            this.g = i;
            int i2 = i - this.j;
            int i3 = this.m;
            if (i2 > i3) {
                int i4 = i2 - i3;
                this.h = i4;
                this.g = i - i4;
                return;
            }
            this.h = 0;
        }

        public void O() throws IOException {
            int C;
            do {
                C = C();
                if (C == 0) {
                    return;
                }
            } while (F(C));
        }

        public void P(int i) throws IOException {
            if (i >= 0) {
                int i2 = this.g;
                int i3 = this.i;
                if (i <= i2 - i3) {
                    this.i = i3 + i;
                    return;
                }
            }
            if (i < 0) {
                throw InvalidProtocolBufferException.negativeSize();
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }

        public final void Q() throws IOException {
            if (this.g - this.i >= 10) {
                R();
            } else {
                S();
            }
        }

        public final void R() throws IOException {
            for (int i = 0; i < 10; i++) {
                byte[] bArr = this.e;
                int i2 = this.i;
                this.i = i2 + 1;
                if (bArr[i2] >= 0) {
                    return;
                }
            }
            throw InvalidProtocolBufferException.malformedVarint();
        }

        public final void S() throws IOException {
            for (int i = 0; i < 10; i++) {
                if (G() >= 0) {
                    return;
                }
            }
            throw InvalidProtocolBufferException.malformedVarint();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public void a(int i) throws InvalidProtocolBufferException {
            if (this.k != i) {
                throw InvalidProtocolBufferException.invalidEndTag();
            }
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public int d() {
            return this.i - this.j;
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public boolean e() throws IOException {
            return this.i == this.g;
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public void l(int i) {
            this.m = i;
            N();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public int m(int i) throws InvalidProtocolBufferException {
            if (i >= 0) {
                int d = i + d();
                int i2 = this.m;
                if (d <= i2) {
                    this.m = d;
                    N();
                    return i2;
                }
                throw InvalidProtocolBufferException.truncatedMessage();
            }
            throw InvalidProtocolBufferException.negativeSize();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public boolean n() throws IOException {
            return L() != 0;
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public ByteString o() throws IOException {
            ByteString copyFrom;
            int K = K();
            if (K > 0) {
                int i = this.g;
                int i2 = this.i;
                if (K <= i - i2) {
                    if (this.f && this.l) {
                        copyFrom = ByteString.wrap(this.e, i2, K);
                    } else {
                        copyFrom = ByteString.copyFrom(this.e, i2, K);
                    }
                    this.i += K;
                    return copyFrom;
                }
            }
            if (K == 0) {
                return ByteString.EMPTY;
            }
            return ByteString.wrap(H(K));
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public double p() throws IOException {
            return Double.longBitsToDouble(J());
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public int q() throws IOException {
            return K();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public int r() throws IOException {
            return I();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public long s() throws IOException {
            return J();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public float t() throws IOException {
            return Float.intBitsToFloat(I());
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public int u() throws IOException {
            return K();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public long v() throws IOException {
            return L();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public int w() throws IOException {
            return I();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public long x() throws IOException {
            return J();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public int y() throws IOException {
            return i.b(K());
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public long z() throws IOException {
            return i.c(L());
        }

        public b(byte[] bArr, int i, int i2, boolean z) {
            super();
            this.m = Integer.MAX_VALUE;
            this.e = bArr;
            this.g = i2 + i;
            this.i = i;
            this.j = i;
            this.f = z;
        }
    }

    /* compiled from: CodedInputStream.java */
    /* loaded from: classes2.dex */
    public static final class c extends i {
        public final InputStream e;
        public final byte[] f;
        public int g;
        public int h;
        public int i;
        public int j;
        public int k;
        public int l;
        public a m;

        /* compiled from: CodedInputStream.java */
        /* loaded from: classes2.dex */
        public interface a {
            void a();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public String A() throws IOException {
            int N = N();
            if (N > 0) {
                int i = this.g;
                int i2 = this.i;
                if (N <= i - i2) {
                    String str = new String(this.f, i2, N, v.a);
                    this.i += N;
                    return str;
                }
            }
            if (N == 0) {
                return "";
            }
            if (N <= this.g) {
                R(N);
                String str2 = new String(this.f, this.i, N, v.a);
                this.i += N;
                return str2;
            }
            return new String(I(N, false), v.a);
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public String B() throws IOException {
            byte[] I;
            int N = N();
            int i = this.i;
            int i2 = this.g;
            if (N <= i2 - i && N > 0) {
                I = this.f;
                this.i = i + N;
            } else if (N == 0) {
                return "";
            } else {
                if (N <= i2) {
                    R(N);
                    I = this.f;
                    this.i = N + 0;
                } else {
                    I = I(N, false);
                }
                i = 0;
            }
            return Utf8.h(I, i, N);
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public int C() throws IOException {
            if (e()) {
                this.j = 0;
                return 0;
            }
            int N = N();
            this.j = N;
            if (WireFormat.a(N) != 0) {
                return this.j;
            }
            throw InvalidProtocolBufferException.invalidTag();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public int D() throws IOException {
            return N();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public long E() throws IOException {
            return O();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public boolean F(int i) throws IOException {
            int b = WireFormat.b(i);
            if (b == 0) {
                V();
                return true;
            } else if (b == 1) {
                T(8);
                return true;
            } else if (b == 2) {
                T(N());
                return true;
            } else if (b == 3) {
                S();
                a(WireFormat.c(WireFormat.a(i), 4));
                return true;
            } else if (b != 4) {
                if (b == 5) {
                    T(4);
                    return true;
                }
                throw InvalidProtocolBufferException.invalidWireType();
            } else {
                return false;
            }
        }

        public final ByteString G(int i) throws IOException {
            byte[] J = J(i);
            if (J != null) {
                return ByteString.copyFrom(J);
            }
            int i2 = this.i;
            int i3 = this.g;
            int i4 = i3 - i2;
            this.k += i3;
            this.i = 0;
            this.g = 0;
            List<byte[]> K = K(i - i4);
            byte[] bArr = new byte[i];
            System.arraycopy(this.f, i2, bArr, 0, i4);
            for (byte[] bArr2 : K) {
                System.arraycopy(bArr2, 0, bArr, i4, bArr2.length);
                i4 += bArr2.length;
            }
            return ByteString.wrap(bArr);
        }

        public byte H() throws IOException {
            if (this.i == this.g) {
                R(1);
            }
            byte[] bArr = this.f;
            int i = this.i;
            this.i = i + 1;
            return bArr[i];
        }

        public final byte[] I(int i, boolean z) throws IOException {
            byte[] J = J(i);
            if (J != null) {
                return z ? (byte[]) J.clone() : J;
            }
            int i2 = this.i;
            int i3 = this.g;
            int i4 = i3 - i2;
            this.k += i3;
            this.i = 0;
            this.g = 0;
            List<byte[]> K = K(i - i4);
            byte[] bArr = new byte[i];
            System.arraycopy(this.f, i2, bArr, 0, i4);
            for (byte[] bArr2 : K) {
                System.arraycopy(bArr2, 0, bArr, i4, bArr2.length);
                i4 += bArr2.length;
            }
            return bArr;
        }

        public final byte[] J(int i) throws IOException {
            if (i == 0) {
                return v.b;
            }
            if (i >= 0) {
                int i2 = this.k;
                int i3 = this.i;
                int i4 = i2 + i3 + i;
                if (i4 - this.c <= 0) {
                    int i5 = this.l;
                    if (i4 <= i5) {
                        int i6 = this.g - i3;
                        int i7 = i - i6;
                        if (i7 < 4096 || i7 <= this.e.available()) {
                            byte[] bArr = new byte[i];
                            System.arraycopy(this.f, this.i, bArr, 0, i6);
                            this.k += this.g;
                            this.i = 0;
                            this.g = 0;
                            while (i6 < i) {
                                int read = this.e.read(bArr, i6, i - i6);
                                if (read != -1) {
                                    this.k += read;
                                    i6 += read;
                                } else {
                                    throw InvalidProtocolBufferException.truncatedMessage();
                                }
                            }
                            return bArr;
                        }
                        return null;
                    }
                    T((i5 - i2) - i3);
                    throw InvalidProtocolBufferException.truncatedMessage();
                }
                throw InvalidProtocolBufferException.sizeLimitExceeded();
            }
            throw InvalidProtocolBufferException.negativeSize();
        }

        public final List<byte[]> K(int i) throws IOException {
            ArrayList arrayList = new ArrayList();
            while (i > 0) {
                int min = Math.min(i, 4096);
                byte[] bArr = new byte[min];
                int i2 = 0;
                while (i2 < min) {
                    int read = this.e.read(bArr, i2, min - i2);
                    if (read != -1) {
                        this.k += read;
                        i2 += read;
                    } else {
                        throw InvalidProtocolBufferException.truncatedMessage();
                    }
                }
                i -= min;
                arrayList.add(bArr);
            }
            return arrayList;
        }

        public int L() throws IOException {
            int i = this.i;
            if (this.g - i < 4) {
                R(4);
                i = this.i;
            }
            byte[] bArr = this.f;
            this.i = i + 4;
            return ((bArr[i + 3] & 255) << 24) | (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << 16);
        }

        public long M() throws IOException {
            int i = this.i;
            if (this.g - i < 8) {
                R(8);
                i = this.i;
            }
            byte[] bArr = this.f;
            this.i = i + 8;
            return ((bArr[i + 7] & 255) << 56) | (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << 16) | ((bArr[i + 3] & 255) << 24) | ((bArr[i + 4] & 255) << 32) | ((bArr[i + 5] & 255) << 40) | ((bArr[i + 6] & 255) << 48);
        }

        /* JADX WARN: Code restructure failed: missing block: B:33:0x0068, code lost:
            if (r2[r3] < 0) goto L34;
         */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public int N() throws java.io.IOException {
            /*
                r5 = this;
                int r0 = r5.i
                int r1 = r5.g
                if (r1 != r0) goto L7
                goto L6a
            L7:
                byte[] r2 = r5.f
                int r3 = r0 + 1
                r0 = r2[r0]
                if (r0 < 0) goto L12
                r5.i = r3
                return r0
            L12:
                int r1 = r1 - r3
                r4 = 9
                if (r1 >= r4) goto L18
                goto L6a
            L18:
                int r1 = r3 + 1
                r3 = r2[r3]
                int r3 = r3 << 7
                r0 = r0 ^ r3
                if (r0 >= 0) goto L24
                r0 = r0 ^ (-128(0xffffffffffffff80, float:NaN))
                goto L70
            L24:
                int r3 = r1 + 1
                r1 = r2[r1]
                int r1 = r1 << 14
                r0 = r0 ^ r1
                if (r0 < 0) goto L31
                r0 = r0 ^ 16256(0x3f80, float:2.278E-41)
            L2f:
                r1 = r3
                goto L70
            L31:
                int r1 = r3 + 1
                r3 = r2[r3]
                int r3 = r3 << 21
                r0 = r0 ^ r3
                if (r0 >= 0) goto L3f
                r2 = -2080896(0xffffffffffe03f80, float:NaN)
                r0 = r0 ^ r2
                goto L70
            L3f:
                int r3 = r1 + 1
                r1 = r2[r1]
                int r4 = r1 << 28
                r0 = r0 ^ r4
                r4 = 266354560(0xfe03f80, float:2.2112565E-29)
                r0 = r0 ^ r4
                if (r1 >= 0) goto L2f
                int r1 = r3 + 1
                r3 = r2[r3]
                if (r3 >= 0) goto L70
                int r3 = r1 + 1
                r1 = r2[r1]
                if (r1 >= 0) goto L2f
                int r1 = r3 + 1
                r3 = r2[r3]
                if (r3 >= 0) goto L70
                int r3 = r1 + 1
                r1 = r2[r1]
                if (r1 >= 0) goto L2f
                int r1 = r3 + 1
                r2 = r2[r3]
                if (r2 >= 0) goto L70
            L6a:
                long r0 = r5.P()
                int r0 = (int) r0
                return r0
            L70:
                r5.i = r1
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.crypto.tink.shaded.protobuf.i.c.N():int");
        }

        /* JADX WARN: Code restructure failed: missing block: B:39:0x00b4, code lost:
            if (r2[r0] < 0) goto L42;
         */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public long O() throws java.io.IOException {
            /*
                Method dump skipped, instructions count: 192
                To view this dump change 'Code comments level' option to 'DEBUG'
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.crypto.tink.shaded.protobuf.i.c.O():long");
        }

        public long P() throws IOException {
            long j = 0;
            for (int i = 0; i < 64; i += 7) {
                byte H = H();
                j |= (H & Byte.MAX_VALUE) << i;
                if ((H & 128) == 0) {
                    return j;
                }
            }
            throw InvalidProtocolBufferException.malformedVarint();
        }

        public final void Q() {
            int i = this.g + this.h;
            this.g = i;
            int i2 = this.k + i;
            int i3 = this.l;
            if (i2 > i3) {
                int i4 = i2 - i3;
                this.h = i4;
                this.g = i - i4;
                return;
            }
            this.h = 0;
        }

        public final void R(int i) throws IOException {
            if (Y(i)) {
                return;
            }
            if (i > (this.c - this.k) - this.i) {
                throw InvalidProtocolBufferException.sizeLimitExceeded();
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }

        public void S() throws IOException {
            int C;
            do {
                C = C();
                if (C == 0) {
                    return;
                }
            } while (F(C));
        }

        public void T(int i) throws IOException {
            int i2 = this.g;
            int i3 = this.i;
            if (i <= i2 - i3 && i >= 0) {
                this.i = i3 + i;
            } else {
                U(i);
            }
        }

        public final void U(int i) throws IOException {
            if (i >= 0) {
                int i2 = this.k;
                int i3 = this.i;
                int i4 = i2 + i3 + i;
                int i5 = this.l;
                if (i4 <= i5) {
                    int i6 = 0;
                    if (this.m == null) {
                        this.k = i2 + i3;
                        this.g = 0;
                        this.i = 0;
                        i6 = this.g - i3;
                        while (i6 < i) {
                            try {
                                long j = i - i6;
                                long skip = this.e.skip(j);
                                int i7 = (skip > 0L ? 1 : (skip == 0L ? 0 : -1));
                                if (i7 < 0 || skip > j) {
                                    throw new IllegalStateException(this.e.getClass() + "#skip returned invalid result: " + skip + "\nThe InputStream implementation is buggy.");
                                } else if (i7 == 0) {
                                    break;
                                } else {
                                    i6 += (int) skip;
                                }
                            } finally {
                                this.k += i6;
                                Q();
                            }
                        }
                    }
                    if (i6 >= i) {
                        return;
                    }
                    int i8 = this.g;
                    int i9 = i8 - this.i;
                    this.i = i8;
                    R(1);
                    while (true) {
                        int i10 = i - i9;
                        int i11 = this.g;
                        if (i10 > i11) {
                            i9 += i11;
                            this.i = i11;
                            R(1);
                        } else {
                            this.i = i10;
                            return;
                        }
                    }
                } else {
                    T((i5 - i2) - i3);
                    throw InvalidProtocolBufferException.truncatedMessage();
                }
            } else {
                throw InvalidProtocolBufferException.negativeSize();
            }
        }

        public final void V() throws IOException {
            if (this.g - this.i >= 10) {
                W();
            } else {
                X();
            }
        }

        public final void W() throws IOException {
            for (int i = 0; i < 10; i++) {
                byte[] bArr = this.f;
                int i2 = this.i;
                this.i = i2 + 1;
                if (bArr[i2] >= 0) {
                    return;
                }
            }
            throw InvalidProtocolBufferException.malformedVarint();
        }

        public final void X() throws IOException {
            for (int i = 0; i < 10; i++) {
                if (H() >= 0) {
                    return;
                }
            }
            throw InvalidProtocolBufferException.malformedVarint();
        }

        public final boolean Y(int i) throws IOException {
            int i2 = this.i;
            if (i2 + i > this.g) {
                int i3 = this.c;
                int i4 = this.k;
                if (i <= (i3 - i4) - i2 && i4 + i2 + i <= this.l) {
                    a aVar = this.m;
                    if (aVar != null) {
                        aVar.a();
                    }
                    int i5 = this.i;
                    if (i5 > 0) {
                        int i6 = this.g;
                        if (i6 > i5) {
                            byte[] bArr = this.f;
                            System.arraycopy(bArr, i5, bArr, 0, i6 - i5);
                        }
                        this.k += i5;
                        this.g -= i5;
                        this.i = 0;
                    }
                    InputStream inputStream = this.e;
                    byte[] bArr2 = this.f;
                    int i7 = this.g;
                    int read = inputStream.read(bArr2, i7, Math.min(bArr2.length - i7, (this.c - this.k) - i7));
                    if (read == 0 || read < -1 || read > this.f.length) {
                        throw new IllegalStateException(this.e.getClass() + "#read(byte[]) returned invalid result: " + read + "\nThe InputStream implementation is buggy.");
                    } else if (read > 0) {
                        this.g += read;
                        Q();
                        if (this.g >= i) {
                            return true;
                        }
                        return Y(i);
                    } else {
                        return false;
                    }
                }
                return false;
            }
            throw new IllegalStateException("refillBuffer() called when " + i + " bytes were already available in buffer");
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public void a(int i) throws InvalidProtocolBufferException {
            if (this.j != i) {
                throw InvalidProtocolBufferException.invalidEndTag();
            }
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public int d() {
            return this.k + this.i;
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public boolean e() throws IOException {
            return this.i == this.g && !Y(1);
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public void l(int i) {
            this.l = i;
            Q();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public int m(int i) throws InvalidProtocolBufferException {
            if (i >= 0) {
                int i2 = i + this.k + this.i;
                int i3 = this.l;
                if (i2 <= i3) {
                    this.l = i2;
                    Q();
                    return i3;
                }
                throw InvalidProtocolBufferException.truncatedMessage();
            }
            throw InvalidProtocolBufferException.negativeSize();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public boolean n() throws IOException {
            return O() != 0;
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public ByteString o() throws IOException {
            int N = N();
            int i = this.g;
            int i2 = this.i;
            if (N > i - i2 || N <= 0) {
                if (N == 0) {
                    return ByteString.EMPTY;
                }
                return G(N);
            }
            ByteString copyFrom = ByteString.copyFrom(this.f, i2, N);
            this.i += N;
            return copyFrom;
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public double p() throws IOException {
            return Double.longBitsToDouble(M());
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public int q() throws IOException {
            return N();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public int r() throws IOException {
            return L();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public long s() throws IOException {
            return M();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public float t() throws IOException {
            return Float.intBitsToFloat(L());
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public int u() throws IOException {
            return N();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public long v() throws IOException {
            return O();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public int w() throws IOException {
            return L();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public long x() throws IOException {
            return M();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public int y() throws IOException {
            return i.b(N());
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public long z() throws IOException {
            return i.c(O());
        }

        public c(InputStream inputStream, int i) {
            super();
            this.l = Integer.MAX_VALUE;
            this.m = null;
            v.b(inputStream, "input");
            this.e = inputStream;
            this.f = new byte[i];
            this.g = 0;
            this.i = 0;
            this.k = 0;
        }
    }

    /* compiled from: CodedInputStream.java */
    /* loaded from: classes2.dex */
    public static final class d extends i {
        public final ByteBuffer e;
        public final boolean f;
        public final long g;
        public long h;
        public long i;
        public long j;
        public int k;
        public int l;
        public boolean m;
        public int n;

        public static boolean H() {
            return t0.H();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public String A() throws IOException {
            int L = L();
            if (L <= 0 || L > P()) {
                if (L == 0) {
                    return "";
                }
                if (L < 0) {
                    throw InvalidProtocolBufferException.negativeSize();
                }
                throw InvalidProtocolBufferException.truncatedMessage();
            }
            byte[] bArr = new byte[L];
            long j = L;
            t0.n(this.i, bArr, 0L, j);
            String str = new String(bArr, v.a);
            this.i += j;
            return str;
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public String B() throws IOException {
            int L = L();
            if (L > 0 && L <= P()) {
                String g = Utf8.g(this.e, G(this.i), L);
                this.i += L;
                return g;
            } else if (L == 0) {
                return "";
            } else {
                if (L <= 0) {
                    throw InvalidProtocolBufferException.negativeSize();
                }
                throw InvalidProtocolBufferException.truncatedMessage();
            }
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public int C() throws IOException {
            if (e()) {
                this.l = 0;
                return 0;
            }
            int L = L();
            this.l = L;
            if (WireFormat.a(L) != 0) {
                return this.l;
            }
            throw InvalidProtocolBufferException.invalidTag();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public int D() throws IOException {
            return L();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public long E() throws IOException {
            return M();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public boolean F(int i) throws IOException {
            int b = WireFormat.b(i);
            if (b == 0) {
                S();
                return true;
            } else if (b == 1) {
                R(8);
                return true;
            } else if (b == 2) {
                R(L());
                return true;
            } else if (b == 3) {
                Q();
                a(WireFormat.c(WireFormat.a(i), 4));
                return true;
            } else if (b != 4) {
                if (b == 5) {
                    R(4);
                    return true;
                }
                throw InvalidProtocolBufferException.invalidWireType();
            } else {
                return false;
            }
        }

        public final int G(long j) {
            return (int) (j - this.g);
        }

        public byte I() throws IOException {
            long j = this.i;
            if (j != this.h) {
                this.i = 1 + j;
                return t0.u(j);
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }

        public int J() throws IOException {
            long j = this.i;
            if (this.h - j >= 4) {
                this.i = 4 + j;
                return ((t0.u(j + 3) & 255) << 24) | (t0.u(j) & 255) | ((t0.u(1 + j) & 255) << 8) | ((t0.u(2 + j) & 255) << 16);
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }

        public long K() throws IOException {
            long j = this.i;
            if (this.h - j >= 8) {
                this.i = 8 + j;
                return ((t0.u(j + 7) & 255) << 56) | (t0.u(j) & 255) | ((t0.u(1 + j) & 255) << 8) | ((t0.u(2 + j) & 255) << 16) | ((t0.u(3 + j) & 255) << 24) | ((t0.u(4 + j) & 255) << 32) | ((t0.u(5 + j) & 255) << 40) | ((t0.u(6 + j) & 255) << 48);
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }

        /* JADX WARN: Code restructure failed: missing block: B:33:0x0083, code lost:
            if (com.google.crypto.tink.shaded.protobuf.t0.u(r4) < 0) goto L34;
         */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public int L() throws java.io.IOException {
            /*
                r10 = this;
                long r0 = r10.i
                long r2 = r10.h
                int r2 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
                if (r2 != 0) goto La
                goto L85
            La:
                r2 = 1
                long r4 = r0 + r2
                byte r0 = com.google.crypto.tink.shaded.protobuf.t0.u(r0)
                if (r0 < 0) goto L17
                r10.i = r4
                return r0
            L17:
                long r6 = r10.h
                long r6 = r6 - r4
                r8 = 9
                int r1 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
                if (r1 >= 0) goto L21
                goto L85
            L21:
                long r6 = r4 + r2
                byte r1 = com.google.crypto.tink.shaded.protobuf.t0.u(r4)
                int r1 = r1 << 7
                r0 = r0 ^ r1
                if (r0 >= 0) goto L2f
                r0 = r0 ^ (-128(0xffffffffffffff80, float:NaN))
                goto L8b
            L2f:
                long r4 = r6 + r2
                byte r1 = com.google.crypto.tink.shaded.protobuf.t0.u(r6)
                int r1 = r1 << 14
                r0 = r0 ^ r1
                if (r0 < 0) goto L3e
                r0 = r0 ^ 16256(0x3f80, float:2.278E-41)
            L3c:
                r6 = r4
                goto L8b
            L3e:
                long r6 = r4 + r2
                byte r1 = com.google.crypto.tink.shaded.protobuf.t0.u(r4)
                int r1 = r1 << 21
                r0 = r0 ^ r1
                if (r0 >= 0) goto L4e
                r1 = -2080896(0xffffffffffe03f80, float:NaN)
                r0 = r0 ^ r1
                goto L8b
            L4e:
                long r4 = r6 + r2
                byte r1 = com.google.crypto.tink.shaded.protobuf.t0.u(r6)
                int r6 = r1 << 28
                r0 = r0 ^ r6
                r6 = 266354560(0xfe03f80, float:2.2112565E-29)
                r0 = r0 ^ r6
                if (r1 >= 0) goto L3c
                long r6 = r4 + r2
                byte r1 = com.google.crypto.tink.shaded.protobuf.t0.u(r4)
                if (r1 >= 0) goto L8b
                long r4 = r6 + r2
                byte r1 = com.google.crypto.tink.shaded.protobuf.t0.u(r6)
                if (r1 >= 0) goto L3c
                long r6 = r4 + r2
                byte r1 = com.google.crypto.tink.shaded.protobuf.t0.u(r4)
                if (r1 >= 0) goto L8b
                long r4 = r6 + r2
                byte r1 = com.google.crypto.tink.shaded.protobuf.t0.u(r6)
                if (r1 >= 0) goto L3c
                long r6 = r4 + r2
                byte r1 = com.google.crypto.tink.shaded.protobuf.t0.u(r4)
                if (r1 >= 0) goto L8b
            L85:
                long r0 = r10.N()
                int r0 = (int) r0
                return r0
            L8b:
                r10.i = r6
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.crypto.tink.shaded.protobuf.i.d.L():int");
        }

        public long M() throws IOException {
            long u;
            long j;
            long j2;
            int i;
            long j3 = this.i;
            if (this.h != j3) {
                long j4 = j3 + 1;
                byte u2 = t0.u(j3);
                if (u2 >= 0) {
                    this.i = j4;
                    return u2;
                } else if (this.h - j4 >= 9) {
                    long j5 = j4 + 1;
                    int u3 = u2 ^ (t0.u(j4) << 7);
                    if (u3 >= 0) {
                        long j6 = j5 + 1;
                        int u4 = u3 ^ (t0.u(j5) << 14);
                        if (u4 >= 0) {
                            u = u4 ^ 16256;
                        } else {
                            j5 = j6 + 1;
                            int u5 = u4 ^ (t0.u(j6) << 21);
                            if (u5 < 0) {
                                i = u5 ^ (-2080896);
                            } else {
                                j6 = j5 + 1;
                                long u6 = u5 ^ (t0.u(j5) << 28);
                                if (u6 < 0) {
                                    long j7 = j6 + 1;
                                    long u7 = u6 ^ (t0.u(j6) << 35);
                                    if (u7 < 0) {
                                        j = -34093383808L;
                                    } else {
                                        j6 = j7 + 1;
                                        u6 = u7 ^ (t0.u(j7) << 42);
                                        if (u6 >= 0) {
                                            j2 = 4363953127296L;
                                        } else {
                                            j7 = j6 + 1;
                                            u7 = u6 ^ (t0.u(j6) << 49);
                                            if (u7 < 0) {
                                                j = -558586000294016L;
                                            } else {
                                                j6 = j7 + 1;
                                                u = (u7 ^ (t0.u(j7) << 56)) ^ 71499008037633920L;
                                                if (u < 0) {
                                                    long j8 = 1 + j6;
                                                    if (t0.u(j6) >= 0) {
                                                        j5 = j8;
                                                        this.i = j5;
                                                        return u;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    u = u7 ^ j;
                                    j5 = j7;
                                    this.i = j5;
                                    return u;
                                }
                                j2 = 266354560;
                                u = u6 ^ j2;
                            }
                        }
                        j5 = j6;
                        this.i = j5;
                        return u;
                    }
                    i = u3 ^ (-128);
                    u = i;
                    this.i = j5;
                    return u;
                }
            }
            return N();
        }

        public long N() throws IOException {
            long j = 0;
            for (int i = 0; i < 64; i += 7) {
                byte I = I();
                j |= (I & Byte.MAX_VALUE) << i;
                if ((I & 128) == 0) {
                    return j;
                }
            }
            throw InvalidProtocolBufferException.malformedVarint();
        }

        public final void O() {
            long j = this.h + this.k;
            this.h = j;
            int i = (int) (j - this.j);
            int i2 = this.n;
            if (i > i2) {
                int i3 = i - i2;
                this.k = i3;
                this.h = j - i3;
                return;
            }
            this.k = 0;
        }

        public final int P() {
            return (int) (this.h - this.i);
        }

        public void Q() throws IOException {
            int C;
            do {
                C = C();
                if (C == 0) {
                    return;
                }
            } while (F(C));
        }

        public void R(int i) throws IOException {
            if (i >= 0 && i <= P()) {
                this.i += i;
            } else if (i < 0) {
                throw InvalidProtocolBufferException.negativeSize();
            } else {
                throw InvalidProtocolBufferException.truncatedMessage();
            }
        }

        public final void S() throws IOException {
            if (P() >= 10) {
                T();
            } else {
                U();
            }
        }

        public final void T() throws IOException {
            for (int i = 0; i < 10; i++) {
                long j = this.i;
                this.i = 1 + j;
                if (t0.u(j) >= 0) {
                    return;
                }
            }
            throw InvalidProtocolBufferException.malformedVarint();
        }

        public final void U() throws IOException {
            for (int i = 0; i < 10; i++) {
                if (I() >= 0) {
                    return;
                }
            }
            throw InvalidProtocolBufferException.malformedVarint();
        }

        public final ByteBuffer V(long j, long j2) throws IOException {
            int position = this.e.position();
            int limit = this.e.limit();
            try {
                try {
                    this.e.position(G(j));
                    this.e.limit(G(j2));
                    return this.e.slice();
                } catch (IllegalArgumentException unused) {
                    throw InvalidProtocolBufferException.truncatedMessage();
                }
            } finally {
                this.e.position(position);
                this.e.limit(limit);
            }
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public void a(int i) throws InvalidProtocolBufferException {
            if (this.l != i) {
                throw InvalidProtocolBufferException.invalidEndTag();
            }
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public int d() {
            return (int) (this.i - this.j);
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public boolean e() throws IOException {
            return this.i == this.h;
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public void l(int i) {
            this.n = i;
            O();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public int m(int i) throws InvalidProtocolBufferException {
            if (i >= 0) {
                int d = i + d();
                int i2 = this.n;
                if (d <= i2) {
                    this.n = d;
                    O();
                    return i2;
                }
                throw InvalidProtocolBufferException.truncatedMessage();
            }
            throw InvalidProtocolBufferException.negativeSize();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public boolean n() throws IOException {
            return M() != 0;
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public ByteString o() throws IOException {
            int L = L();
            if (L <= 0 || L > P()) {
                if (L == 0) {
                    return ByteString.EMPTY;
                }
                if (L < 0) {
                    throw InvalidProtocolBufferException.negativeSize();
                }
                throw InvalidProtocolBufferException.truncatedMessage();
            } else if (this.f && this.m) {
                long j = this.i;
                long j2 = L;
                ByteBuffer V = V(j, j + j2);
                this.i += j2;
                return ByteString.wrap(V);
            } else {
                byte[] bArr = new byte[L];
                long j3 = L;
                t0.n(this.i, bArr, 0L, j3);
                this.i += j3;
                return ByteString.wrap(bArr);
            }
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public double p() throws IOException {
            return Double.longBitsToDouble(K());
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public int q() throws IOException {
            return L();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public int r() throws IOException {
            return J();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public long s() throws IOException {
            return K();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public float t() throws IOException {
            return Float.intBitsToFloat(J());
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public int u() throws IOException {
            return L();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public long v() throws IOException {
            return M();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public int w() throws IOException {
            return J();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public long x() throws IOException {
            return K();
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public int y() throws IOException {
            return i.b(L());
        }

        @Override // com.google.crypto.tink.shaded.protobuf.i
        public long z() throws IOException {
            return i.c(M());
        }

        public d(ByteBuffer byteBuffer, boolean z) {
            super();
            this.n = Integer.MAX_VALUE;
            this.e = byteBuffer;
            long i = t0.i(byteBuffer);
            this.g = i;
            this.h = byteBuffer.limit() + i;
            long position = i + byteBuffer.position();
            this.i = position;
            this.j = position;
            this.f = z;
        }
    }

    public static int b(int i) {
        return (-(i & 1)) ^ (i >>> 1);
    }

    public static long c(long j) {
        return (-(j & 1)) ^ (j >>> 1);
    }

    public static i f(InputStream inputStream) {
        return g(inputStream, 4096);
    }

    public static i g(InputStream inputStream, int i) {
        if (i > 0) {
            if (inputStream == null) {
                return i(v.b);
            }
            return new c(inputStream, i);
        }
        throw new IllegalArgumentException("bufferSize must be > 0");
    }

    public static i h(ByteBuffer byteBuffer, boolean z) {
        if (byteBuffer.hasArray()) {
            return k(byteBuffer.array(), byteBuffer.arrayOffset() + byteBuffer.position(), byteBuffer.remaining(), z);
        }
        if (byteBuffer.isDirect() && d.H()) {
            return new d(byteBuffer, z);
        }
        int remaining = byteBuffer.remaining();
        byte[] bArr = new byte[remaining];
        byteBuffer.duplicate().get(bArr);
        return k(bArr, 0, remaining, true);
    }

    public static i i(byte[] bArr) {
        return j(bArr, 0, bArr.length);
    }

    public static i j(byte[] bArr, int i, int i2) {
        return k(bArr, i, i2, false);
    }

    public static i k(byte[] bArr, int i, int i2, boolean z) {
        b bVar = new b(bArr, i, i2, z);
        try {
            bVar.m(i2);
            return bVar;
        } catch (InvalidProtocolBufferException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public abstract String A() throws IOException;

    public abstract String B() throws IOException;

    public abstract int C() throws IOException;

    public abstract int D() throws IOException;

    public abstract long E() throws IOException;

    public abstract boolean F(int i) throws IOException;

    public abstract void a(int i) throws InvalidProtocolBufferException;

    public abstract int d();

    public abstract boolean e() throws IOException;

    public abstract void l(int i);

    public abstract int m(int i) throws InvalidProtocolBufferException;

    public abstract boolean n() throws IOException;

    public abstract ByteString o() throws IOException;

    public abstract double p() throws IOException;

    public abstract int q() throws IOException;

    public abstract int r() throws IOException;

    public abstract long s() throws IOException;

    public abstract float t() throws IOException;

    public abstract int u() throws IOException;

    public abstract long v() throws IOException;

    public abstract int w() throws IOException;

    public abstract long x() throws IOException;

    public abstract int y() throws IOException;

    public abstract long z() throws IOException;

    public i() {
        this.b = 100;
        this.c = Integer.MAX_VALUE;
    }
}
