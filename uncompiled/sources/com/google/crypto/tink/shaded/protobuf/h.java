package com.google.crypto.tink.shaded.protobuf;

import java.io.IOException;
import java.nio.ByteBuffer;

/* compiled from: ByteOutput.java */
/* loaded from: classes2.dex */
public abstract class h {
    public abstract void a(ByteBuffer byteBuffer) throws IOException;

    public abstract void b(byte[] bArr, int i, int i2) throws IOException;
}
