package com.google.crypto.tink.shaded.protobuf;

import com.google.crypto.tink.shaded.protobuf.b0;
import java.util.Map;

/* compiled from: MapFieldSchemaLite.java */
/* loaded from: classes2.dex */
public class d0 implements c0 {
    public static <K, V> int i(int i, Object obj, Object obj2) {
        MapFieldLite mapFieldLite = (MapFieldLite) obj;
        b0 b0Var = (b0) obj2;
        int i2 = 0;
        if (mapFieldLite.isEmpty()) {
            return 0;
        }
        for (Map.Entry<K, V> entry : mapFieldLite.entrySet()) {
            i2 += b0Var.a(i, entry.getKey(), entry.getValue());
        }
        return i2;
    }

    public static <K, V> MapFieldLite<K, V> j(Object obj, Object obj2) {
        MapFieldLite<K, V> mapFieldLite = (MapFieldLite) obj;
        MapFieldLite<K, V> mapFieldLite2 = (MapFieldLite) obj2;
        if (!mapFieldLite2.isEmpty()) {
            if (!mapFieldLite.isMutable()) {
                mapFieldLite = mapFieldLite.mutableCopy();
            }
            mapFieldLite.mergeFrom(mapFieldLite2);
        }
        return mapFieldLite;
    }

    @Override // com.google.crypto.tink.shaded.protobuf.c0
    public Object a(Object obj, Object obj2) {
        return j(obj, obj2);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.c0
    public Object b(Object obj) {
        ((MapFieldLite) obj).makeImmutable();
        return obj;
    }

    @Override // com.google.crypto.tink.shaded.protobuf.c0
    public b0.a<?, ?> c(Object obj) {
        return ((b0) obj).c();
    }

    @Override // com.google.crypto.tink.shaded.protobuf.c0
    public Map<?, ?> d(Object obj) {
        return (MapFieldLite) obj;
    }

    @Override // com.google.crypto.tink.shaded.protobuf.c0
    public Object e(Object obj) {
        return MapFieldLite.emptyMapField().mutableCopy();
    }

    @Override // com.google.crypto.tink.shaded.protobuf.c0
    public int f(int i, Object obj, Object obj2) {
        return i(i, obj, obj2);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.c0
    public boolean g(Object obj) {
        return !((MapFieldLite) obj).isMutable();
    }

    @Override // com.google.crypto.tink.shaded.protobuf.c0
    public Map<?, ?> h(Object obj) {
        return (MapFieldLite) obj;
    }
}
