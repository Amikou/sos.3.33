package com.google.crypto.tink.shaded.protobuf;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.RandomAccess;

/* compiled from: LazyStringArrayList.java */
/* loaded from: classes2.dex */
public class y extends c<String> implements dz1, RandomAccess {
    public static final y g0;
    public final List<Object> f0;

    static {
        y yVar = new y();
        g0 = yVar;
        yVar.l();
    }

    public y() {
        this(10);
    }

    public static String k(Object obj) {
        if (obj instanceof String) {
            return (String) obj;
        }
        if (obj instanceof ByteString) {
            return ((ByteString) obj).toStringUtf8();
        }
        return v.j((byte[]) obj);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.c, java.util.AbstractCollection, java.util.Collection, java.util.List
    public boolean addAll(Collection<? extends String> collection) {
        return addAll(size(), collection);
    }

    @Override // com.google.crypto.tink.shaded.protobuf.c, java.util.AbstractList, java.util.AbstractCollection, java.util.Collection, java.util.List
    public void clear() {
        e();
        this.f0.clear();
        ((AbstractList) this).modCount++;
    }

    @Override // java.util.AbstractList, java.util.List
    /* renamed from: i */
    public void add(int i, String str) {
        e();
        this.f0.add(i, str);
        ((AbstractList) this).modCount++;
    }

    @Override // defpackage.dz1
    public void i1(ByteString byteString) {
        e();
        this.f0.add(byteString);
        ((AbstractList) this).modCount++;
    }

    @Override // defpackage.dz1
    public Object j(int i) {
        return this.f0.get(i);
    }

    @Override // java.util.AbstractList, java.util.List
    /* renamed from: m */
    public String get(int i) {
        Object obj = this.f0.get(i);
        if (obj instanceof String) {
            return (String) obj;
        }
        if (obj instanceof ByteString) {
            ByteString byteString = (ByteString) obj;
            String stringUtf8 = byteString.toStringUtf8();
            if (byteString.isValidUtf8()) {
                this.f0.set(i, stringUtf8);
            }
            return stringUtf8;
        }
        byte[] bArr = (byte[]) obj;
        String j = v.j(bArr);
        if (v.g(bArr)) {
            this.f0.set(i, j);
        }
        return j;
    }

    @Override // com.google.crypto.tink.shaded.protobuf.v.i
    /* renamed from: n */
    public y a(int i) {
        if (i >= size()) {
            ArrayList arrayList = new ArrayList(i);
            arrayList.addAll(this.f0);
            return new y(arrayList);
        }
        throw new IllegalArgumentException();
    }

    @Override // java.util.AbstractList, java.util.List
    /* renamed from: o */
    public String remove(int i) {
        e();
        Object remove = this.f0.remove(i);
        ((AbstractList) this).modCount++;
        return k(remove);
    }

    @Override // java.util.AbstractList, java.util.List
    /* renamed from: p */
    public String set(int i, String str) {
        e();
        return k(this.f0.set(i, str));
    }

    @Override // defpackage.dz1
    public List<?> r() {
        return Collections.unmodifiableList(this.f0);
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public int size() {
        return this.f0.size();
    }

    @Override // defpackage.dz1
    public dz1 x() {
        return A() ? new cf4(this) : this;
    }

    public y(int i) {
        this(new ArrayList(i));
    }

    @Override // com.google.crypto.tink.shaded.protobuf.c, java.util.AbstractList, java.util.List
    public boolean addAll(int i, Collection<? extends String> collection) {
        e();
        if (collection instanceof dz1) {
            collection = ((dz1) collection).r();
        }
        boolean addAll = this.f0.addAll(i, collection);
        ((AbstractList) this).modCount++;
        return addAll;
    }

    public y(ArrayList<Object> arrayList) {
        this.f0 = arrayList;
    }
}
