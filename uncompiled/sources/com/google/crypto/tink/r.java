package com.google.crypto.tink;

import com.google.crypto.tink.proto.KeyData;
import com.google.crypto.tink.proto.KeyStatusType;
import com.google.crypto.tink.proto.OutputPrefixType;
import com.google.crypto.tink.proto.y;
import com.google.crypto.tink.proto.z;
import java.nio.charset.Charset;
import java.security.GeneralSecurityException;

/* compiled from: Util.java */
/* loaded from: classes2.dex */
public class r {
    static {
        Charset.forName("UTF-8");
    }

    public static z.c a(y.c cVar) {
        return z.c.J().v(cVar.I().J()).u(cVar.L()).t(cVar.K()).s(cVar.J()).build();
    }

    public static z b(y yVar) {
        z.b t = z.J().t(yVar.L());
        for (y.c cVar : yVar.K()) {
            t.s(a(cVar));
        }
        return t.build();
    }

    public static void c(y.c cVar) throws GeneralSecurityException {
        if (cVar.M()) {
            if (cVar.K() != OutputPrefixType.UNKNOWN_PREFIX) {
                if (cVar.L() == KeyStatusType.UNKNOWN_STATUS) {
                    throw new GeneralSecurityException(String.format("key %d has unknown status", Integer.valueOf(cVar.J())));
                }
                return;
            }
            throw new GeneralSecurityException(String.format("key %d has unknown prefix", Integer.valueOf(cVar.J())));
        }
        throw new GeneralSecurityException(String.format("key %d has no key data", Integer.valueOf(cVar.J())));
    }

    public static void d(y yVar) throws GeneralSecurityException {
        int L = yVar.L();
        boolean z = true;
        int i = 0;
        boolean z2 = false;
        for (y.c cVar : yVar.K()) {
            if (cVar.L() == KeyStatusType.ENABLED) {
                c(cVar);
                if (cVar.J() == L) {
                    if (z2) {
                        throw new GeneralSecurityException("keyset contains multiple primary keys");
                    }
                    z2 = true;
                }
                if (cVar.I().I() != KeyData.KeyMaterialType.ASYMMETRIC_PUBLIC) {
                    z = false;
                }
                i++;
            }
        }
        if (i == 0) {
            throw new GeneralSecurityException("keyset must contain at least one ENABLED key");
        }
        if (!z2 && !z) {
            throw new GeneralSecurityException("keyset doesn't contain a valid primary key");
        }
    }
}
