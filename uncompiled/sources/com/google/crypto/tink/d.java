package com.google.crypto.tink;

import java.security.GeneralSecurityException;

/* compiled from: DeterministicAead.java */
/* loaded from: classes2.dex */
public interface d {
    byte[] a(byte[] bArr, byte[] bArr2) throws GeneralSecurityException;

    byte[] b(byte[] bArr, byte[] bArr2) throws GeneralSecurityException;
}
