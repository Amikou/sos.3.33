package com.google.crypto.tink;

import com.google.crypto.tink.g;
import com.google.crypto.tink.proto.KeyData;
import com.google.crypto.tink.shaded.protobuf.ByteString;
import com.google.crypto.tink.shaded.protobuf.InvalidProtocolBufferException;
import com.google.crypto.tink.shaded.protobuf.e0;
import java.security.GeneralSecurityException;

/* compiled from: KeyManagerImpl.java */
/* loaded from: classes2.dex */
public class f<PrimitiveT, KeyProtoT extends e0> implements e<PrimitiveT> {
    public final g<KeyProtoT> a;
    public final Class<PrimitiveT> b;

    /* compiled from: KeyManagerImpl.java */
    /* loaded from: classes2.dex */
    public static class a<KeyFormatProtoT extends e0, KeyProtoT extends e0> {
        public final g.a<KeyFormatProtoT, KeyProtoT> a;

        public a(g.a<KeyFormatProtoT, KeyProtoT> aVar) {
            this.a = aVar;
        }

        public KeyProtoT a(ByteString byteString) throws GeneralSecurityException, InvalidProtocolBufferException {
            return b(this.a.c(byteString));
        }

        public final KeyProtoT b(KeyFormatProtoT keyformatprotot) throws GeneralSecurityException {
            this.a.d(keyformatprotot);
            return this.a.a(keyformatprotot);
        }
    }

    public f(g<KeyProtoT> gVar, Class<PrimitiveT> cls) {
        if (!gVar.h().contains(cls) && !Void.class.equals(cls)) {
            throw new IllegalArgumentException(String.format("Given internalKeyMananger %s does not support primitive class %s", gVar.toString(), cls.getName()));
        }
        this.a = gVar;
        this.b = cls;
    }

    @Override // com.google.crypto.tink.e
    public final boolean a(String str) {
        return str.equals(e());
    }

    @Override // com.google.crypto.tink.e
    public final e0 b(ByteString byteString) throws GeneralSecurityException {
        try {
            return f().a(byteString);
        } catch (InvalidProtocolBufferException e) {
            throw new GeneralSecurityException("Failures parsing proto of type " + this.a.e().b().getName(), e);
        }
    }

    @Override // com.google.crypto.tink.e
    public final KeyData c(ByteString byteString) throws GeneralSecurityException {
        try {
            return KeyData.L().t(e()).u(f().a(byteString).toByteString()).s(this.a.f()).build();
        } catch (InvalidProtocolBufferException e) {
            throw new GeneralSecurityException("Unexpected proto", e);
        }
    }

    @Override // com.google.crypto.tink.e
    public final PrimitiveT d(ByteString byteString) throws GeneralSecurityException {
        try {
            return g(this.a.g(byteString));
        } catch (InvalidProtocolBufferException e) {
            throw new GeneralSecurityException("Failures parsing proto of type " + this.a.b().getName(), e);
        }
    }

    public final String e() {
        return this.a.c();
    }

    public final a<?, KeyProtoT> f() {
        return new a<>(this.a.e());
    }

    public final PrimitiveT g(KeyProtoT keyprotot) throws GeneralSecurityException {
        if (!Void.class.equals(this.b)) {
            this.a.i(keyprotot);
            return (PrimitiveT) this.a.d(keyprotot, (Class<PrimitiveT>) this.b);
        }
        throw new GeneralSecurityException("Cannot create a primitive for Void");
    }
}
