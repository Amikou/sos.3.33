package com.google.crypto.tink;

import com.google.crypto.tink.o;
import com.google.crypto.tink.proto.KeyData;
import com.google.crypto.tink.proto.KeyStatusType;
import com.google.crypto.tink.proto.x;
import com.google.crypto.tink.proto.y;
import com.google.crypto.tink.shaded.protobuf.ByteString;
import com.google.crypto.tink.shaded.protobuf.e0;
import java.security.GeneralSecurityException;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.logging.Logger;

/* compiled from: Registry.java */
/* loaded from: classes2.dex */
public final class q {
    public static final Logger a = Logger.getLogger(q.class.getName());
    public static final ConcurrentMap<String, d> b = new ConcurrentHashMap();
    public static final ConcurrentMap<String, c> c = new ConcurrentHashMap();
    public static final ConcurrentMap<String, Boolean> d = new ConcurrentHashMap();
    public static final ConcurrentMap<Class<?>, p<?, ?>> e;

    /* compiled from: Registry.java */
    /* loaded from: classes2.dex */
    public class a implements d {
        public final /* synthetic */ g a;

        public a(g gVar) {
            this.a = gVar;
        }

        @Override // com.google.crypto.tink.q.d
        public <Q> e<Q> a(Class<Q> cls) throws GeneralSecurityException {
            try {
                return new f(this.a, cls);
            } catch (IllegalArgumentException e) {
                throw new GeneralSecurityException("Primitive type not supported", e);
            }
        }

        @Override // com.google.crypto.tink.q.d
        public e<?> b() {
            g gVar = this.a;
            return new f(gVar, gVar.a());
        }

        @Override // com.google.crypto.tink.q.d
        public Class<?> c() {
            return this.a.getClass();
        }

        @Override // com.google.crypto.tink.q.d
        public Set<Class<?>> d() {
            return this.a.h();
        }
    }

    /* compiled from: Registry.java */
    /* loaded from: classes2.dex */
    public class b implements c {
        public b(g gVar) {
        }
    }

    /* compiled from: Registry.java */
    /* loaded from: classes2.dex */
    public interface c {
    }

    /* compiled from: Registry.java */
    /* loaded from: classes2.dex */
    public interface d {
        <P> e<P> a(Class<P> cls) throws GeneralSecurityException;

        e<?> b();

        Class<?> c();

        Set<Class<?>> d();
    }

    static {
        new ConcurrentHashMap();
        e = new ConcurrentHashMap();
    }

    public static <T> T a(T t) {
        Objects.requireNonNull(t);
        return t;
    }

    public static <KeyProtoT extends e0> d b(g<KeyProtoT> gVar) {
        return new a(gVar);
    }

    public static <KeyProtoT extends e0> c c(g<KeyProtoT> gVar) {
        return new b(gVar);
    }

    public static synchronized void d(String str, Class<?> cls, boolean z) throws GeneralSecurityException {
        synchronized (q.class) {
            ConcurrentMap<String, d> concurrentMap = b;
            if (concurrentMap.containsKey(str)) {
                d dVar = concurrentMap.get(str);
                if (dVar.c().equals(cls)) {
                    if (z && !d.get(str).booleanValue()) {
                        throw new GeneralSecurityException("New keys are already disallowed for key type " + str);
                    }
                    return;
                }
                Logger logger = a;
                logger.warning("Attempted overwrite of a registered key manager for key type " + str);
                throw new GeneralSecurityException(String.format("typeUrl (%s) is already registered with %s, cannot be re-registered with %s", str, dVar.c().getName(), cls.getName()));
            }
        }
    }

    public static Class<?> e(Class<?> cls) {
        p<?, ?> pVar = e.get(cls);
        if (pVar == null) {
            return null;
        }
        return pVar.a();
    }

    public static synchronized d f(String str) throws GeneralSecurityException {
        d dVar;
        synchronized (q.class) {
            ConcurrentMap<String, d> concurrentMap = b;
            if (concurrentMap.containsKey(str)) {
                dVar = concurrentMap.get(str);
            } else {
                throw new GeneralSecurityException("No key manager found for key type " + str);
            }
        }
        return dVar;
    }

    public static <P> e<P> g(String str, Class<P> cls) throws GeneralSecurityException {
        d f = f(str);
        if (cls == null) {
            return (e<P>) f.b();
        }
        if (f.d().contains(cls)) {
            return f.a(cls);
        }
        throw new GeneralSecurityException("Primitive type " + cls.getName() + " not supported by key manager of type " + f.c() + ", supported primitives: " + s(f.d()));
    }

    public static <P> P h(String str, ByteString byteString, Class<P> cls) throws GeneralSecurityException {
        return (P) j(str, byteString, (Class) a(cls));
    }

    public static <P> P i(String str, byte[] bArr, Class<P> cls) throws GeneralSecurityException {
        return (P) h(str, ByteString.copyFrom(bArr), cls);
    }

    public static <P> P j(String str, ByteString byteString, Class<P> cls) throws GeneralSecurityException {
        return (P) g(str, cls).d(byteString);
    }

    public static <P> o<P> k(h hVar, e<P> eVar, Class<P> cls) throws GeneralSecurityException {
        return m(hVar, eVar, (Class) a(cls));
    }

    public static <P> o<P> l(h hVar, Class<P> cls) throws GeneralSecurityException {
        return k(hVar, null, cls);
    }

    public static <P> o<P> m(h hVar, e<P> eVar, Class<P> cls) throws GeneralSecurityException {
        P p;
        r.d(hVar.f());
        o<P> f = o.f(cls);
        for (y.c cVar : hVar.f().K()) {
            if (cVar.L() == KeyStatusType.ENABLED) {
                if (eVar != null && eVar.a(cVar.I().J())) {
                    p = eVar.d(cVar.I().K());
                } else {
                    p = (P) j(cVar.I().J(), cVar.I().K(), cls);
                }
                o.b<P> a2 = f.a(p, cVar);
                if (cVar.J() == hVar.f().L()) {
                    f.g(a2);
                }
            }
        }
        return f;
    }

    public static e<?> n(String str) throws GeneralSecurityException {
        return f(str).b();
    }

    public static synchronized e0 o(x xVar) throws GeneralSecurityException {
        e0 b2;
        synchronized (q.class) {
            e<?> n = n(xVar.J());
            if (d.get(xVar.J()).booleanValue()) {
                b2 = n.b(xVar.K());
            } else {
                throw new GeneralSecurityException("newKey-operation not permitted for key type " + xVar.J());
            }
        }
        return b2;
    }

    public static synchronized KeyData p(x xVar) throws GeneralSecurityException {
        KeyData c2;
        synchronized (q.class) {
            e<?> n = n(xVar.J());
            if (d.get(xVar.J()).booleanValue()) {
                c2 = n.c(xVar.K());
            } else {
                throw new GeneralSecurityException("newKey-operation not permitted for key type " + xVar.J());
            }
        }
        return c2;
    }

    public static synchronized <KeyProtoT extends e0> void q(g<KeyProtoT> gVar, boolean z) throws GeneralSecurityException {
        synchronized (q.class) {
            if (gVar != null) {
                String c2 = gVar.c();
                d(c2, gVar.getClass(), z);
                ConcurrentMap<String, d> concurrentMap = b;
                if (!concurrentMap.containsKey(c2)) {
                    concurrentMap.put(c2, b(gVar));
                    c.put(c2, c(gVar));
                }
                d.put(c2, Boolean.valueOf(z));
            } else {
                throw new IllegalArgumentException("key manager must be non-null.");
            }
        }
    }

    public static synchronized <B, P> void r(p<B, P> pVar) throws GeneralSecurityException {
        synchronized (q.class) {
            if (pVar != null) {
                Class<P> b2 = pVar.b();
                ConcurrentMap<Class<?>, p<?, ?>> concurrentMap = e;
                if (concurrentMap.containsKey(b2)) {
                    p<?, ?> pVar2 = concurrentMap.get(b2);
                    if (!pVar.getClass().equals(pVar2.getClass())) {
                        Logger logger = a;
                        logger.warning("Attempted overwrite of a registered SetWrapper for type " + b2);
                        throw new GeneralSecurityException(String.format("SetWrapper for primitive (%s) is already registered to be %s, cannot be re-registered with %s", b2.getName(), pVar2.getClass().getName(), pVar.getClass().getName()));
                    }
                }
                concurrentMap.put(b2, pVar);
            } else {
                throw new IllegalArgumentException("wrapper must be non-null");
            }
        }
    }

    public static String s(Set<Class<?>> set) {
        StringBuilder sb = new StringBuilder();
        boolean z = true;
        for (Class<?> cls : set) {
            if (!z) {
                sb.append(", ");
            }
            sb.append(cls.getCanonicalName());
            z = false;
        }
        return sb.toString();
    }

    public static <B, P> P t(o<B> oVar, Class<P> cls) throws GeneralSecurityException {
        p<?, ?> pVar = e.get(cls);
        if (pVar != null) {
            if (pVar.a().equals(oVar.d())) {
                return (P) pVar.c(oVar);
            }
            throw new GeneralSecurityException("Wrong input primitive class, expected " + pVar.a() + ", got " + oVar.d());
        }
        throw new GeneralSecurityException("No wrapper found for " + oVar.d().getName());
    }
}
