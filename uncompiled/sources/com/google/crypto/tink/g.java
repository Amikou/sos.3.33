package com.google.crypto.tink;

import com.google.crypto.tink.proto.KeyData;
import com.google.crypto.tink.shaded.protobuf.ByteString;
import com.google.crypto.tink.shaded.protobuf.InvalidProtocolBufferException;
import com.google.crypto.tink.shaded.protobuf.e0;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/* compiled from: KeyTypeManager.java */
/* loaded from: classes2.dex */
public abstract class g<KeyProtoT extends e0> {
    public final Class<KeyProtoT> a;
    public final Map<Class<?>, b<?, KeyProtoT>> b;
    public final Class<?> c;

    /* compiled from: KeyTypeManager.java */
    /* loaded from: classes2.dex */
    public static abstract class a<KeyFormatProtoT extends e0, KeyT> {
        public final Class<KeyFormatProtoT> a;

        public a(Class<KeyFormatProtoT> cls) {
            this.a = cls;
        }

        public abstract KeyT a(KeyFormatProtoT keyformatprotot) throws GeneralSecurityException;

        public final Class<KeyFormatProtoT> b() {
            return this.a;
        }

        public abstract KeyFormatProtoT c(ByteString byteString) throws InvalidProtocolBufferException;

        public abstract void d(KeyFormatProtoT keyformatprotot) throws GeneralSecurityException;
    }

    /* compiled from: KeyTypeManager.java */
    /* loaded from: classes2.dex */
    public static abstract class b<PrimitiveT, KeyT> {
        public final Class<PrimitiveT> a;

        public b(Class<PrimitiveT> cls) {
            this.a = cls;
        }

        public abstract PrimitiveT a(KeyT keyt) throws GeneralSecurityException;

        public final Class<PrimitiveT> b() {
            return this.a;
        }
    }

    public g(Class<KeyProtoT> cls, b<?, KeyProtoT>... bVarArr) {
        this.a = cls;
        HashMap hashMap = new HashMap();
        for (b<?, KeyProtoT> bVar : bVarArr) {
            if (!hashMap.containsKey(bVar.b())) {
                hashMap.put(bVar.b(), bVar);
            } else {
                throw new IllegalArgumentException("KeyTypeManager constructed with duplicate factories for primitive " + bVar.b().getCanonicalName());
            }
        }
        if (bVarArr.length > 0) {
            this.c = bVarArr[0].b();
        } else {
            this.c = Void.class;
        }
        this.b = Collections.unmodifiableMap(hashMap);
    }

    public final Class<?> a() {
        return this.c;
    }

    public final Class<KeyProtoT> b() {
        return this.a;
    }

    public abstract String c();

    public final <P> P d(KeyProtoT keyprotot, Class<P> cls) throws GeneralSecurityException {
        b<?, KeyProtoT> bVar = this.b.get(cls);
        if (bVar != null) {
            return (P) bVar.a(keyprotot);
        }
        throw new IllegalArgumentException("Requested primitive class " + cls.getCanonicalName() + " not supported.");
    }

    public abstract a<?, KeyProtoT> e();

    public abstract KeyData.KeyMaterialType f();

    public abstract KeyProtoT g(ByteString byteString) throws InvalidProtocolBufferException;

    public final Set<Class<?>> h() {
        return this.b.keySet();
    }

    public abstract void i(KeyProtoT keyprotot) throws GeneralSecurityException;
}
