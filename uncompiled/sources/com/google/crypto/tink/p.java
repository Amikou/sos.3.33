package com.google.crypto.tink;

import java.security.GeneralSecurityException;

/* compiled from: PrimitiveWrapper.java */
/* loaded from: classes2.dex */
public interface p<B, P> {
    Class<B> a();

    Class<P> b();

    P c(o<B> oVar) throws GeneralSecurityException;
}
