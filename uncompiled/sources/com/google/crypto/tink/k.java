package com.google.crypto.tink;

import com.google.crypto.tink.proto.t;
import com.google.crypto.tink.proto.y;
import java.io.IOException;

/* compiled from: KeysetWriter.java */
/* loaded from: classes2.dex */
public interface k {
    void a(y yVar) throws IOException;

    void b(t tVar) throws IOException;
}
