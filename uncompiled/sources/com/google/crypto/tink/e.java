package com.google.crypto.tink;

import com.google.crypto.tink.proto.KeyData;
import com.google.crypto.tink.shaded.protobuf.ByteString;
import com.google.crypto.tink.shaded.protobuf.e0;
import java.security.GeneralSecurityException;

/* compiled from: KeyManager.java */
/* loaded from: classes2.dex */
public interface e<P> {
    boolean a(String str);

    e0 b(ByteString byteString) throws GeneralSecurityException;

    KeyData c(ByteString byteString) throws GeneralSecurityException;

    P d(ByteString byteString) throws GeneralSecurityException;
}
