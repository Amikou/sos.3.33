package com.google.crypto.tink;

import com.google.crypto.tink.proto.KeyStatusType;
import com.google.crypto.tink.proto.OutputPrefixType;
import com.google.crypto.tink.proto.y;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/* compiled from: PrimitiveSet.java */
/* loaded from: classes2.dex */
public final class o<P> {
    public final ConcurrentMap<c, List<b<P>>> a = new ConcurrentHashMap();
    public b<P> b;
    public final Class<P> c;

    /* compiled from: PrimitiveSet.java */
    /* loaded from: classes2.dex */
    public static final class b<P> {
        public final P a;
        public final byte[] b;
        public final KeyStatusType c;
        public final OutputPrefixType d;

        public b(P p, byte[] bArr, KeyStatusType keyStatusType, OutputPrefixType outputPrefixType, int i) {
            this.a = p;
            this.b = Arrays.copyOf(bArr, bArr.length);
            this.c = keyStatusType;
            this.d = outputPrefixType;
        }

        public final byte[] a() {
            byte[] bArr = this.b;
            if (bArr == null) {
                return null;
            }
            return Arrays.copyOf(bArr, bArr.length);
        }

        public OutputPrefixType b() {
            return this.d;
        }

        public P c() {
            return this.a;
        }

        public KeyStatusType d() {
            return this.c;
        }
    }

    /* compiled from: PrimitiveSet.java */
    /* loaded from: classes2.dex */
    public static class c implements Comparable<c> {
        public final byte[] a;

        @Override // java.lang.Comparable
        /* renamed from: a */
        public int compareTo(c cVar) {
            int i;
            int i2;
            byte[] bArr = this.a;
            int length = bArr.length;
            byte[] bArr2 = cVar.a;
            if (length != bArr2.length) {
                i = bArr.length;
                i2 = bArr2.length;
            } else {
                int i3 = 0;
                while (true) {
                    byte[] bArr3 = this.a;
                    if (i3 >= bArr3.length) {
                        return 0;
                    }
                    byte b = bArr3[i3];
                    byte[] bArr4 = cVar.a;
                    if (b != bArr4[i3]) {
                        i = bArr3[i3];
                        i2 = bArr4[i3];
                        break;
                    }
                    i3++;
                }
            }
            return i - i2;
        }

        public boolean equals(Object obj) {
            if (obj instanceof c) {
                return Arrays.equals(this.a, ((c) obj).a);
            }
            return false;
        }

        public int hashCode() {
            return Arrays.hashCode(this.a);
        }

        public String toString() {
            return ok1.b(this.a);
        }

        public c(byte[] bArr) {
            this.a = Arrays.copyOf(bArr, bArr.length);
        }
    }

    public o(Class<P> cls) {
        this.c = cls;
    }

    public static <P> o<P> f(Class<P> cls) {
        return new o<>(cls);
    }

    public b<P> a(P p, y.c cVar) throws GeneralSecurityException {
        if (cVar.L() == KeyStatusType.ENABLED) {
            b<P> bVar = new b<>(p, com.google.crypto.tink.c.a(cVar), cVar.L(), cVar.K(), cVar.J());
            ArrayList arrayList = new ArrayList();
            arrayList.add(bVar);
            c cVar2 = new c(bVar.a());
            List<b<P>> put = this.a.put(cVar2, Collections.unmodifiableList(arrayList));
            if (put != null) {
                ArrayList arrayList2 = new ArrayList();
                arrayList2.addAll(put);
                arrayList2.add(bVar);
                this.a.put(cVar2, Collections.unmodifiableList(arrayList2));
            }
            return bVar;
        }
        throw new GeneralSecurityException("only ENABLED key is allowed");
    }

    public b<P> b() {
        return this.b;
    }

    public List<b<P>> c(byte[] bArr) {
        List<b<P>> list = this.a.get(new c(bArr));
        return list != null ? list : Collections.emptyList();
    }

    public Class<P> d() {
        return this.c;
    }

    public List<b<P>> e() {
        return c(com.google.crypto.tink.c.a);
    }

    public void g(b<P> bVar) {
        if (bVar != null) {
            if (bVar.d() == KeyStatusType.ENABLED) {
                if (!c(bVar.a()).isEmpty()) {
                    this.b = bVar;
                    return;
                }
                throw new IllegalArgumentException("the primary entry cannot be set to an entry which is not held by this primitive set");
            }
            throw new IllegalArgumentException("the primary entry has to be ENABLED");
        }
        throw new IllegalArgumentException("the primary entry must be non-null");
    }
}
