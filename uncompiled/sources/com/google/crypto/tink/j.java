package com.google.crypto.tink;

import com.google.crypto.tink.proto.t;
import com.google.crypto.tink.proto.y;
import java.io.IOException;

/* compiled from: KeysetReader.java */
/* loaded from: classes2.dex */
public interface j {
    t a() throws IOException;

    y read() throws IOException;
}
