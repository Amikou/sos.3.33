package com.google.crypto.tink;

import java.security.GeneralSecurityException;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

/* compiled from: KmsClients.java */
/* loaded from: classes2.dex */
public final class m {
    public static final CopyOnWriteArrayList<l> a = new CopyOnWriteArrayList<>();

    public static l a(String str) throws GeneralSecurityException {
        Iterator<l> it = a.iterator();
        while (it.hasNext()) {
            l next = it.next();
            if (next.a(str)) {
                return next;
            }
        }
        throw new GeneralSecurityException("No KMS client does support: " + str);
    }
}
