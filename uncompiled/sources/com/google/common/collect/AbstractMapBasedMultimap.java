package com.google.common.collect;

import com.google.common.collect.Maps;
import com.google.common.collect.Multimaps;
import com.google.common.collect.a;
import java.io.Serializable;
import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.NavigableMap;
import java.util.NavigableSet;
import java.util.Objects;
import java.util.RandomAccess;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;

/* loaded from: classes2.dex */
public abstract class AbstractMapBasedMultimap<K, V> extends com.google.common.collect.a<K, V> implements Serializable {
    private static final long serialVersionUID = 2447537837011683357L;
    public transient Map<K, Collection<V>> j0;
    public transient int k0;

    /* loaded from: classes2.dex */
    public class a extends AbstractMapBasedMultimap<K, V>.d<V> {
        public a(AbstractMapBasedMultimap abstractMapBasedMultimap) {
            super();
        }

        @Override // com.google.common.collect.AbstractMapBasedMultimap.d
        public V a(K k, V v) {
            return v;
        }
    }

    /* loaded from: classes2.dex */
    public class b extends AbstractMapBasedMultimap<K, V>.d<Map.Entry<K, V>> {
        public b(AbstractMapBasedMultimap abstractMapBasedMultimap) {
            super();
        }

        @Override // com.google.common.collect.AbstractMapBasedMultimap.d
        /* renamed from: b */
        public Map.Entry<K, V> a(K k, V v) {
            return Maps.e(k, v);
        }
    }

    /* loaded from: classes2.dex */
    public class c extends Maps.f<K, Collection<V>> {
        public final transient Map<K, Collection<V>> g0;

        /* loaded from: classes2.dex */
        public class a extends Maps.c<K, Collection<V>> {
            public a() {
            }

            @Override // com.google.common.collect.Maps.c, java.util.AbstractCollection, java.util.Collection, java.util.Set
            public boolean contains(Object obj) {
                return com.google.common.collect.c.c(c.this.g0.entrySet(), obj);
            }

            @Override // com.google.common.collect.Maps.c
            public Map<K, Collection<V>> e() {
                return c.this;
            }

            @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable, java.util.Set
            public Iterator<Map.Entry<K, Collection<V>>> iterator() {
                return new b();
            }

            @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
            public boolean remove(Object obj) {
                if (contains(obj)) {
                    Map.Entry entry = (Map.Entry) obj;
                    Objects.requireNonNull(entry);
                    AbstractMapBasedMultimap.this.c(entry.getKey());
                    return true;
                }
                return false;
            }
        }

        /* loaded from: classes2.dex */
        public class b implements Iterator<Map.Entry<K, Collection<V>>> {
            public final Iterator<Map.Entry<K, Collection<V>>> a;
            public Collection<V> f0;

            public b() {
                this.a = c.this.g0.entrySet().iterator();
            }

            @Override // java.util.Iterator
            /* renamed from: a */
            public Map.Entry<K, Collection<V>> next() {
                Map.Entry<K, Collection<V>> next = this.a.next();
                this.f0 = next.getValue();
                return c.this.e(next);
            }

            @Override // java.util.Iterator
            public boolean hasNext() {
                return this.a.hasNext();
            }

            @Override // java.util.Iterator
            public void remove() {
                au2.q(this.f0 != null, "no calls to next() since the last call to remove()");
                this.a.remove();
                AbstractMapBasedMultimap.access$220(AbstractMapBasedMultimap.this, this.f0.size());
                this.f0.clear();
                this.f0 = null;
            }
        }

        public c(Map<K, Collection<V>> map) {
            this.g0 = map;
        }

        @Override // com.google.common.collect.Maps.f
        public Set<Map.Entry<K, Collection<V>>> a() {
            return new a();
        }

        @Override // java.util.AbstractMap, java.util.Map
        /* renamed from: c */
        public Collection<V> get(Object obj) {
            Collection<V> collection = (Collection) Maps.l(this.g0, obj);
            if (collection == null) {
                return null;
            }
            return AbstractMapBasedMultimap.this.wrapCollection(obj, collection);
        }

        @Override // java.util.AbstractMap, java.util.Map
        public void clear() {
            if (this.g0 == AbstractMapBasedMultimap.this.j0) {
                AbstractMapBasedMultimap.this.clear();
            } else {
                Iterators.c(new b());
            }
        }

        @Override // java.util.AbstractMap, java.util.Map
        public boolean containsKey(Object obj) {
            return Maps.k(this.g0, obj);
        }

        @Override // java.util.AbstractMap, java.util.Map
        /* renamed from: d */
        public Collection<V> remove(Object obj) {
            Collection<V> remove = this.g0.remove(obj);
            if (remove == null) {
                return null;
            }
            Collection<V> createCollection = AbstractMapBasedMultimap.this.createCollection();
            createCollection.addAll(remove);
            AbstractMapBasedMultimap.access$220(AbstractMapBasedMultimap.this, remove.size());
            remove.clear();
            return createCollection;
        }

        public Map.Entry<K, Collection<V>> e(Map.Entry<K, Collection<V>> entry) {
            K key = entry.getKey();
            return Maps.e(key, AbstractMapBasedMultimap.this.wrapCollection(key, entry.getValue()));
        }

        @Override // java.util.AbstractMap, java.util.Map
        public boolean equals(Object obj) {
            return this == obj || this.g0.equals(obj);
        }

        @Override // java.util.AbstractMap, java.util.Map
        public int hashCode() {
            return this.g0.hashCode();
        }

        @Override // java.util.AbstractMap, java.util.Map
        public Set<K> keySet() {
            return AbstractMapBasedMultimap.this.keySet();
        }

        @Override // java.util.AbstractMap, java.util.Map
        public int size() {
            return this.g0.size();
        }

        @Override // java.util.AbstractMap
        public String toString() {
            return this.g0.toString();
        }
    }

    /* loaded from: classes2.dex */
    public abstract class d<T> implements Iterator<T> {
        public final Iterator<Map.Entry<K, Collection<V>>> a;
        public K f0 = null;
        public Collection<V> g0 = null;
        public Iterator<V> h0 = Iterators.h();

        public d() {
            this.a = (Iterator<Map.Entry<K, V>>) AbstractMapBasedMultimap.this.j0.entrySet().iterator();
        }

        public abstract T a(K k, V v);

        @Override // java.util.Iterator
        public boolean hasNext() {
            return this.a.hasNext() || this.h0.hasNext();
        }

        @Override // java.util.Iterator
        public T next() {
            if (!this.h0.hasNext()) {
                Map.Entry<K, Collection<V>> next = this.a.next();
                this.f0 = next.getKey();
                Collection<V> value = next.getValue();
                this.g0 = value;
                this.h0 = value.iterator();
            }
            return a(yi2.a(this.f0), this.h0.next());
        }

        @Override // java.util.Iterator
        public void remove() {
            this.h0.remove();
            Collection<V> collection = this.g0;
            Objects.requireNonNull(collection);
            if (collection.isEmpty()) {
                this.a.remove();
            }
            AbstractMapBasedMultimap.access$210(AbstractMapBasedMultimap.this);
        }
    }

    /* loaded from: classes2.dex */
    public class e extends Maps.d<K, Collection<V>> {

        /* loaded from: classes2.dex */
        public class a implements Iterator<K> {
            public Map.Entry<K, Collection<V>> a;
            public final /* synthetic */ Iterator f0;

            public a(Iterator it) {
                this.f0 = it;
            }

            @Override // java.util.Iterator
            public boolean hasNext() {
                return this.f0.hasNext();
            }

            @Override // java.util.Iterator
            public K next() {
                Map.Entry<K, Collection<V>> entry = (Map.Entry) this.f0.next();
                this.a = entry;
                return entry.getKey();
            }

            @Override // java.util.Iterator
            public void remove() {
                au2.q(this.a != null, "no calls to next() since the last call to remove()");
                Collection<V> value = this.a.getValue();
                this.f0.remove();
                AbstractMapBasedMultimap.access$220(AbstractMapBasedMultimap.this, value.size());
                value.clear();
                this.a = null;
            }
        }

        public e(Map<K, Collection<V>> map) {
            super(map);
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public void clear() {
            Iterators.c(iterator());
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean containsAll(Collection<?> collection) {
            return e().keySet().containsAll(collection);
        }

        @Override // java.util.AbstractSet, java.util.Collection, java.util.Set
        public boolean equals(Object obj) {
            return this == obj || e().keySet().equals(obj);
        }

        @Override // java.util.AbstractSet, java.util.Collection, java.util.Set
        public int hashCode() {
            return e().keySet().hashCode();
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable, java.util.Set
        public Iterator<K> iterator() {
            return new a(e().entrySet().iterator());
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean remove(Object obj) {
            int i;
            Collection<V> remove = e().remove(obj);
            if (remove != null) {
                i = remove.size();
                remove.clear();
                AbstractMapBasedMultimap.access$220(AbstractMapBasedMultimap.this, i);
            } else {
                i = 0;
            }
            return i > 0;
        }
    }

    /* loaded from: classes2.dex */
    public class f extends AbstractMapBasedMultimap<K, V>.i implements NavigableMap<K, Collection<V>> {
        public f(NavigableMap<K, Collection<V>> navigableMap) {
            super(navigableMap);
        }

        @Override // java.util.NavigableMap
        public Map.Entry<K, Collection<V>> ceilingEntry(K k) {
            Map.Entry<K, Collection<V>> ceilingEntry = h().ceilingEntry(k);
            if (ceilingEntry == null) {
                return null;
            }
            return e(ceilingEntry);
        }

        @Override // java.util.NavigableMap
        public K ceilingKey(K k) {
            return h().ceilingKey(k);
        }

        @Override // java.util.NavigableMap
        public NavigableSet<K> descendingKeySet() {
            return descendingMap().navigableKeySet();
        }

        @Override // java.util.NavigableMap
        public NavigableMap<K, Collection<V>> descendingMap() {
            return new f(h().descendingMap());
        }

        @Override // java.util.NavigableMap
        public Map.Entry<K, Collection<V>> firstEntry() {
            Map.Entry<K, Collection<V>> firstEntry = h().firstEntry();
            if (firstEntry == null) {
                return null;
            }
            return e(firstEntry);
        }

        @Override // java.util.NavigableMap
        public Map.Entry<K, Collection<V>> floorEntry(K k) {
            Map.Entry<K, Collection<V>> floorEntry = h().floorEntry(k);
            if (floorEntry == null) {
                return null;
            }
            return e(floorEntry);
        }

        @Override // java.util.NavigableMap
        public K floorKey(K k) {
            return h().floorKey(k);
        }

        @Override // java.util.NavigableMap
        public Map.Entry<K, Collection<V>> higherEntry(K k) {
            Map.Entry<K, Collection<V>> higherEntry = h().higherEntry(k);
            if (higherEntry == null) {
                return null;
            }
            return e(higherEntry);
        }

        @Override // java.util.NavigableMap
        public K higherKey(K k) {
            return h().higherKey(k);
        }

        @Override // com.google.common.collect.AbstractMapBasedMultimap.i
        /* renamed from: i */
        public NavigableSet<K> f() {
            return new g(h());
        }

        @Override // com.google.common.collect.AbstractMapBasedMultimap.i, java.util.SortedMap, java.util.NavigableMap
        /* renamed from: j */
        public NavigableMap<K, Collection<V>> headMap(K k) {
            return headMap(k, false);
        }

        @Override // com.google.common.collect.AbstractMapBasedMultimap.i, com.google.common.collect.AbstractMapBasedMultimap.c, java.util.AbstractMap, java.util.Map
        /* renamed from: k */
        public NavigableSet<K> keySet() {
            return (NavigableSet) super.keySet();
        }

        public Map.Entry<K, Collection<V>> l(Iterator<Map.Entry<K, Collection<V>>> it) {
            if (it.hasNext()) {
                Map.Entry<K, Collection<V>> next = it.next();
                Collection<V> createCollection = AbstractMapBasedMultimap.this.createCollection();
                createCollection.addAll(next.getValue());
                it.remove();
                return Maps.e(next.getKey(), AbstractMapBasedMultimap.this.unmodifiableCollectionSubclass(createCollection));
            }
            return null;
        }

        @Override // java.util.NavigableMap
        public Map.Entry<K, Collection<V>> lastEntry() {
            Map.Entry<K, Collection<V>> lastEntry = h().lastEntry();
            if (lastEntry == null) {
                return null;
            }
            return e(lastEntry);
        }

        @Override // java.util.NavigableMap
        public Map.Entry<K, Collection<V>> lowerEntry(K k) {
            Map.Entry<K, Collection<V>> lowerEntry = h().lowerEntry(k);
            if (lowerEntry == null) {
                return null;
            }
            return e(lowerEntry);
        }

        @Override // java.util.NavigableMap
        public K lowerKey(K k) {
            return h().lowerKey(k);
        }

        @Override // com.google.common.collect.AbstractMapBasedMultimap.i
        /* renamed from: m */
        public NavigableMap<K, Collection<V>> h() {
            return (NavigableMap) super.h();
        }

        @Override // com.google.common.collect.AbstractMapBasedMultimap.i, java.util.SortedMap, java.util.NavigableMap
        /* renamed from: n */
        public NavigableMap<K, Collection<V>> subMap(K k, K k2) {
            return subMap(k, true, k2, false);
        }

        @Override // java.util.NavigableMap
        public NavigableSet<K> navigableKeySet() {
            return keySet();
        }

        @Override // com.google.common.collect.AbstractMapBasedMultimap.i, java.util.SortedMap, java.util.NavigableMap
        /* renamed from: o */
        public NavigableMap<K, Collection<V>> tailMap(K k) {
            return tailMap(k, true);
        }

        @Override // java.util.NavigableMap
        public Map.Entry<K, Collection<V>> pollFirstEntry() {
            return l(entrySet().iterator());
        }

        @Override // java.util.NavigableMap
        public Map.Entry<K, Collection<V>> pollLastEntry() {
            return l(descendingMap().entrySet().iterator());
        }

        @Override // java.util.NavigableMap
        public NavigableMap<K, Collection<V>> headMap(K k, boolean z) {
            return new f(h().headMap(k, z));
        }

        @Override // java.util.NavigableMap
        public NavigableMap<K, Collection<V>> subMap(K k, boolean z, K k2, boolean z2) {
            return new f(h().subMap(k, z, k2, z2));
        }

        @Override // java.util.NavigableMap
        public NavigableMap<K, Collection<V>> tailMap(K k, boolean z) {
            return new f(h().tailMap(k, z));
        }
    }

    /* loaded from: classes2.dex */
    public class g extends AbstractMapBasedMultimap<K, V>.j implements NavigableSet<K> {
        public g(NavigableMap<K, Collection<V>> navigableMap) {
            super(navigableMap);
        }

        @Override // java.util.NavigableSet
        public K ceiling(K k) {
            return i().ceilingKey(k);
        }

        @Override // java.util.NavigableSet
        public Iterator<K> descendingIterator() {
            return descendingSet().iterator();
        }

        @Override // java.util.NavigableSet
        public NavigableSet<K> descendingSet() {
            return new g(i().descendingMap());
        }

        @Override // java.util.NavigableSet
        public K floor(K k) {
            return i().floorKey(k);
        }

        @Override // java.util.NavigableSet
        public K higher(K k) {
            return i().higherKey(k);
        }

        @Override // com.google.common.collect.AbstractMapBasedMultimap.j, java.util.SortedSet, java.util.NavigableSet
        /* renamed from: k */
        public NavigableSet<K> headSet(K k) {
            return headSet(k, false);
        }

        @Override // java.util.NavigableSet
        public K lower(K k) {
            return i().lowerKey(k);
        }

        @Override // com.google.common.collect.AbstractMapBasedMultimap.j
        /* renamed from: m */
        public NavigableMap<K, Collection<V>> i() {
            return (NavigableMap) super.i();
        }

        @Override // com.google.common.collect.AbstractMapBasedMultimap.j, java.util.SortedSet, java.util.NavigableSet
        /* renamed from: n */
        public NavigableSet<K> subSet(K k, K k2) {
            return subSet(k, true, k2, false);
        }

        @Override // com.google.common.collect.AbstractMapBasedMultimap.j, java.util.SortedSet, java.util.NavigableSet
        /* renamed from: o */
        public NavigableSet<K> tailSet(K k) {
            return tailSet(k, true);
        }

        @Override // java.util.NavigableSet
        public K pollFirst() {
            return (K) Iterators.n(iterator());
        }

        @Override // java.util.NavigableSet
        public K pollLast() {
            return (K) Iterators.n(descendingIterator());
        }

        @Override // java.util.NavigableSet
        public NavigableSet<K> headSet(K k, boolean z) {
            return new g(i().headMap(k, z));
        }

        @Override // java.util.NavigableSet
        public NavigableSet<K> subSet(K k, boolean z, K k2, boolean z2) {
            return new g(i().subMap(k, z, k2, z2));
        }

        @Override // java.util.NavigableSet
        public NavigableSet<K> tailSet(K k, boolean z) {
            return new g(i().tailMap(k, z));
        }
    }

    /* loaded from: classes2.dex */
    public class h extends AbstractMapBasedMultimap<K, V>.l implements RandomAccess {
        public h(AbstractMapBasedMultimap abstractMapBasedMultimap, K k, List<V> list, AbstractMapBasedMultimap<K, V>.k kVar) {
            super(k, list, kVar);
        }
    }

    /* loaded from: classes2.dex */
    public class i extends AbstractMapBasedMultimap<K, V>.c implements SortedMap<K, Collection<V>> {
        public SortedSet<K> i0;

        public i(SortedMap<K, Collection<V>> sortedMap) {
            super(sortedMap);
        }

        @Override // java.util.SortedMap
        public Comparator<? super K> comparator() {
            return h().comparator();
        }

        public SortedSet<K> f() {
            return new j(h());
        }

        @Override // java.util.SortedMap
        public K firstKey() {
            return h().firstKey();
        }

        @Override // com.google.common.collect.AbstractMapBasedMultimap.c, java.util.AbstractMap, java.util.Map
        /* renamed from: g */
        public SortedSet<K> keySet() {
            SortedSet<K> sortedSet = this.i0;
            if (sortedSet == null) {
                SortedSet<K> f = f();
                this.i0 = f;
                return f;
            }
            return sortedSet;
        }

        public SortedMap<K, Collection<V>> h() {
            return (SortedMap) this.g0;
        }

        public SortedMap<K, Collection<V>> headMap(K k) {
            return new i(h().headMap(k));
        }

        @Override // java.util.SortedMap
        public K lastKey() {
            return h().lastKey();
        }

        public SortedMap<K, Collection<V>> subMap(K k, K k2) {
            return new i(h().subMap(k, k2));
        }

        public SortedMap<K, Collection<V>> tailMap(K k) {
            return new i(h().tailMap(k));
        }
    }

    /* loaded from: classes2.dex */
    public class j extends AbstractMapBasedMultimap<K, V>.e implements SortedSet<K> {
        public j(SortedMap<K, Collection<V>> sortedMap) {
            super(sortedMap);
        }

        @Override // java.util.SortedSet
        public Comparator<? super K> comparator() {
            return i().comparator();
        }

        @Override // java.util.SortedSet
        public K first() {
            return i().firstKey();
        }

        public SortedSet<K> headSet(K k) {
            return new j(i().headMap(k));
        }

        public SortedMap<K, Collection<V>> i() {
            return (SortedMap) super.e();
        }

        @Override // java.util.SortedSet
        public K last() {
            return i().lastKey();
        }

        public SortedSet<K> subSet(K k, K k2) {
            return new j(i().subMap(k, k2));
        }

        public SortedSet<K> tailSet(K k) {
            return new j(i().tailMap(k));
        }
    }

    public AbstractMapBasedMultimap(Map<K, Collection<V>> map) {
        au2.d(map.isEmpty());
        this.j0 = map;
    }

    public static /* synthetic */ int access$208(AbstractMapBasedMultimap abstractMapBasedMultimap) {
        int i2 = abstractMapBasedMultimap.k0;
        abstractMapBasedMultimap.k0 = i2 + 1;
        return i2;
    }

    public static /* synthetic */ int access$210(AbstractMapBasedMultimap abstractMapBasedMultimap) {
        int i2 = abstractMapBasedMultimap.k0;
        abstractMapBasedMultimap.k0 = i2 - 1;
        return i2;
    }

    public static /* synthetic */ int access$212(AbstractMapBasedMultimap abstractMapBasedMultimap, int i2) {
        int i3 = abstractMapBasedMultimap.k0 + i2;
        abstractMapBasedMultimap.k0 = i3;
        return i3;
    }

    public static /* synthetic */ int access$220(AbstractMapBasedMultimap abstractMapBasedMultimap, int i2) {
        int i3 = abstractMapBasedMultimap.k0 - i2;
        abstractMapBasedMultimap.k0 = i3;
        return i3;
    }

    public static <E> Iterator<E> b(Collection<E> collection) {
        if (collection instanceof List) {
            return ((List) collection).listIterator();
        }
        return collection.iterator();
    }

    public final Collection<V> a(K k2) {
        Collection<V> collection = this.j0.get(k2);
        if (collection == null) {
            Collection<V> createCollection = createCollection(k2);
            this.j0.put(k2, createCollection);
            return createCollection;
        }
        return collection;
    }

    public Map<K, Collection<V>> backingMap() {
        return this.j0;
    }

    public final void c(Object obj) {
        Collection collection = (Collection) Maps.m(this.j0, obj);
        if (collection != null) {
            int size = collection.size();
            collection.clear();
            this.k0 -= size;
        }
    }

    @Override // defpackage.wa2
    public void clear() {
        for (Collection<V> collection : this.j0.values()) {
            collection.clear();
        }
        this.j0.clear();
        this.k0 = 0;
    }

    @Override // defpackage.wa2
    public boolean containsKey(Object obj) {
        return this.j0.containsKey(obj);
    }

    @Override // com.google.common.collect.a
    public Map<K, Collection<V>> createAsMap() {
        return new c(this.j0);
    }

    public abstract Collection<V> createCollection();

    public Collection<V> createCollection(K k2) {
        return createCollection();
    }

    @Override // com.google.common.collect.a
    public Collection<Map.Entry<K, V>> createEntries() {
        if (this instanceof mm3) {
            return new a.b(this);
        }
        return new a.C0132a();
    }

    @Override // com.google.common.collect.a
    public Set<K> createKeySet() {
        return new e(this.j0);
    }

    @Override // com.google.common.collect.a
    public com.google.common.collect.d<K> createKeys() {
        return new Multimaps.b(this);
    }

    public final Map<K, Collection<V>> createMaybeNavigableAsMap() {
        Map<K, Collection<V>> map = this.j0;
        if (map instanceof NavigableMap) {
            return new f((NavigableMap) this.j0);
        }
        if (map instanceof SortedMap) {
            return new i((SortedMap) this.j0);
        }
        return new c(this.j0);
    }

    public final Set<K> createMaybeNavigableKeySet() {
        Map<K, Collection<V>> map = this.j0;
        if (map instanceof NavigableMap) {
            return new g((NavigableMap) this.j0);
        }
        if (map instanceof SortedMap) {
            return new j((SortedMap) this.j0);
        }
        return new e(this.j0);
    }

    public Collection<V> createUnmodifiableEmptyCollection() {
        return (Collection<V>) unmodifiableCollectionSubclass(createCollection());
    }

    @Override // com.google.common.collect.a
    public Collection<V> createValues() {
        return new a.c();
    }

    @Override // com.google.common.collect.a, defpackage.wa2
    public Collection<Map.Entry<K, V>> entries() {
        return super.entries();
    }

    @Override // com.google.common.collect.a
    public Iterator<Map.Entry<K, V>> entryIterator() {
        return new b(this);
    }

    @Override // defpackage.wa2
    public Collection<V> get(K k2) {
        Collection<V> collection = this.j0.get(k2);
        if (collection == null) {
            collection = createCollection(k2);
        }
        return wrapCollection(k2, collection);
    }

    @Override // com.google.common.collect.a, defpackage.wa2
    public boolean put(K k2, V v) {
        Collection<V> collection = this.j0.get(k2);
        if (collection == null) {
            Collection<V> createCollection = createCollection(k2);
            if (createCollection.add(v)) {
                this.k0++;
                this.j0.put(k2, createCollection);
                return true;
            }
            throw new AssertionError("New Collection violated the Collection spec");
        } else if (collection.add(v)) {
            this.k0++;
            return true;
        } else {
            return false;
        }
    }

    @Override // defpackage.wa2
    public Collection<V> removeAll(Object obj) {
        Collection<V> remove = this.j0.remove(obj);
        if (remove == null) {
            return createUnmodifiableEmptyCollection();
        }
        Collection createCollection = createCollection();
        createCollection.addAll(remove);
        this.k0 -= remove.size();
        remove.clear();
        return (Collection<V>) unmodifiableCollectionSubclass(createCollection);
    }

    @Override // com.google.common.collect.a
    public Collection<V> replaceValues(K k2, Iterable<? extends V> iterable) {
        Iterator<? extends V> it = iterable.iterator();
        if (!it.hasNext()) {
            return removeAll(k2);
        }
        Collection<V> a2 = a(k2);
        Collection createCollection = createCollection();
        createCollection.addAll(a2);
        this.k0 -= a2.size();
        a2.clear();
        while (it.hasNext()) {
            if (a2.add(it.next())) {
                this.k0++;
            }
        }
        return (Collection<V>) unmodifiableCollectionSubclass(createCollection);
    }

    public final void setMap(Map<K, Collection<V>> map) {
        this.j0 = map;
        this.k0 = 0;
        for (Collection<V> collection : map.values()) {
            au2.d(!collection.isEmpty());
            this.k0 += collection.size();
        }
    }

    @Override // defpackage.wa2
    public int size() {
        return this.k0;
    }

    public <E> Collection<E> unmodifiableCollectionSubclass(Collection<E> collection) {
        return Collections.unmodifiableCollection(collection);
    }

    @Override // com.google.common.collect.a
    public Iterator<V> valueIterator() {
        return new a(this);
    }

    @Override // com.google.common.collect.a, defpackage.wa2
    public Collection<V> values() {
        return super.values();
    }

    public Collection<V> wrapCollection(K k2, Collection<V> collection) {
        return new k(k2, collection, null);
    }

    public final List<V> wrapList(K k2, List<V> list, AbstractMapBasedMultimap<K, V>.k kVar) {
        if (list instanceof RandomAccess) {
            return new h(this, k2, list, kVar);
        }
        return new l(k2, list, kVar);
    }

    /* loaded from: classes2.dex */
    public class k extends AbstractCollection<V> {
        public final K a;
        public Collection<V> f0;
        public final AbstractMapBasedMultimap<K, V>.k g0;
        public final Collection<V> h0;

        public k(K k, Collection<V> collection, AbstractMapBasedMultimap<K, V>.k kVar) {
            this.a = k;
            this.f0 = collection;
            this.g0 = kVar;
            this.h0 = kVar == null ? null : kVar.k();
        }

        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean add(V v) {
            n();
            boolean isEmpty = this.f0.isEmpty();
            boolean add = this.f0.add(v);
            if (add) {
                AbstractMapBasedMultimap.access$208(AbstractMapBasedMultimap.this);
                if (isEmpty) {
                    e();
                }
            }
            return add;
        }

        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean addAll(Collection<? extends V> collection) {
            if (collection.isEmpty()) {
                return false;
            }
            int size = size();
            boolean addAll = this.f0.addAll(collection);
            if (addAll) {
                AbstractMapBasedMultimap.access$212(AbstractMapBasedMultimap.this, this.f0.size() - size);
                if (size == 0) {
                    e();
                }
            }
            return addAll;
        }

        @Override // java.util.AbstractCollection, java.util.Collection
        public void clear() {
            int size = size();
            if (size == 0) {
                return;
            }
            this.f0.clear();
            AbstractMapBasedMultimap.access$220(AbstractMapBasedMultimap.this, size);
            o();
        }

        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean contains(Object obj) {
            n();
            return this.f0.contains(obj);
        }

        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean containsAll(Collection<?> collection) {
            n();
            return this.f0.containsAll(collection);
        }

        public void e() {
            AbstractMapBasedMultimap<K, V>.k kVar = this.g0;
            if (kVar == null) {
                AbstractMapBasedMultimap.this.j0.put(this.a, this.f0);
            } else {
                kVar.e();
            }
        }

        @Override // java.util.Collection
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            n();
            return this.f0.equals(obj);
        }

        @Override // java.util.Collection
        public int hashCode() {
            n();
            return this.f0.hashCode();
        }

        public AbstractMapBasedMultimap<K, V>.k i() {
            return this.g0;
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable
        public Iterator<V> iterator() {
            n();
            return new a();
        }

        public Collection<V> k() {
            return this.f0;
        }

        K m() {
            return this.a;
        }

        public void n() {
            Collection<V> collection;
            AbstractMapBasedMultimap<K, V>.k kVar = this.g0;
            if (kVar != null) {
                kVar.n();
                if (this.g0.k() != this.h0) {
                    throw new ConcurrentModificationException();
                }
            } else if (!this.f0.isEmpty() || (collection = (Collection) AbstractMapBasedMultimap.this.j0.get(this.a)) == null) {
            } else {
                this.f0 = collection;
            }
        }

        public void o() {
            AbstractMapBasedMultimap<K, V>.k kVar = this.g0;
            if (kVar != null) {
                kVar.o();
            } else if (this.f0.isEmpty()) {
                AbstractMapBasedMultimap.this.j0.remove(this.a);
            }
        }

        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean remove(Object obj) {
            n();
            boolean remove = this.f0.remove(obj);
            if (remove) {
                AbstractMapBasedMultimap.access$210(AbstractMapBasedMultimap.this);
                o();
            }
            return remove;
        }

        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean removeAll(Collection<?> collection) {
            if (collection.isEmpty()) {
                return false;
            }
            int size = size();
            boolean removeAll = this.f0.removeAll(collection);
            if (removeAll) {
                AbstractMapBasedMultimap.access$212(AbstractMapBasedMultimap.this, this.f0.size() - size);
                o();
            }
            return removeAll;
        }

        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean retainAll(Collection<?> collection) {
            au2.k(collection);
            int size = size();
            boolean retainAll = this.f0.retainAll(collection);
            if (retainAll) {
                AbstractMapBasedMultimap.access$212(AbstractMapBasedMultimap.this, this.f0.size() - size);
                o();
            }
            return retainAll;
        }

        @Override // java.util.AbstractCollection, java.util.Collection
        public int size() {
            n();
            return this.f0.size();
        }

        @Override // java.util.AbstractCollection
        public String toString() {
            n();
            return this.f0.toString();
        }

        /* loaded from: classes2.dex */
        public class a implements Iterator<V> {
            public final Iterator<V> a;
            public final Collection<V> f0;

            public a() {
                Collection<V> collection = k.this.f0;
                this.f0 = collection;
                this.a = AbstractMapBasedMultimap.b(collection);
            }

            public Iterator<V> a() {
                b();
                return this.a;
            }

            public void b() {
                k.this.n();
                if (k.this.f0 != this.f0) {
                    throw new ConcurrentModificationException();
                }
            }

            @Override // java.util.Iterator
            public boolean hasNext() {
                b();
                return this.a.hasNext();
            }

            @Override // java.util.Iterator
            public V next() {
                b();
                return this.a.next();
            }

            @Override // java.util.Iterator
            public void remove() {
                this.a.remove();
                AbstractMapBasedMultimap.access$210(AbstractMapBasedMultimap.this);
                k.this.o();
            }

            public a(Iterator<V> it) {
                this.f0 = k.this.f0;
                this.a = it;
            }
        }
    }

    /* loaded from: classes2.dex */
    public class l extends AbstractMapBasedMultimap<K, V>.k implements List<V> {

        /* loaded from: classes2.dex */
        public class a extends AbstractMapBasedMultimap<K, V>.k.a implements ListIterator<V> {
            public a() {
                super();
            }

            @Override // java.util.ListIterator
            public void add(V v) {
                boolean isEmpty = l.this.isEmpty();
                c().add(v);
                AbstractMapBasedMultimap.access$208(AbstractMapBasedMultimap.this);
                if (isEmpty) {
                    l.this.e();
                }
            }

            public final ListIterator<V> c() {
                return (ListIterator) a();
            }

            @Override // java.util.ListIterator
            public boolean hasPrevious() {
                return c().hasPrevious();
            }

            @Override // java.util.ListIterator
            public int nextIndex() {
                return c().nextIndex();
            }

            @Override // java.util.ListIterator
            public V previous() {
                return c().previous();
            }

            @Override // java.util.ListIterator
            public int previousIndex() {
                return c().previousIndex();
            }

            @Override // java.util.ListIterator
            public void set(V v) {
                c().set(v);
            }

            public a(int i) {
                super(l.this.p().listIterator(i));
            }
        }

        public l(K k, List<V> list, AbstractMapBasedMultimap<K, V>.k kVar) {
            super(k, list, kVar);
        }

        @Override // java.util.List
        public void add(int i, V v) {
            n();
            boolean isEmpty = k().isEmpty();
            p().add(i, v);
            AbstractMapBasedMultimap.access$208(AbstractMapBasedMultimap.this);
            if (isEmpty) {
                e();
            }
        }

        @Override // java.util.List
        public boolean addAll(int i, Collection<? extends V> collection) {
            if (collection.isEmpty()) {
                return false;
            }
            int size = size();
            boolean addAll = p().addAll(i, collection);
            if (addAll) {
                AbstractMapBasedMultimap.access$212(AbstractMapBasedMultimap.this, k().size() - size);
                if (size == 0) {
                    e();
                }
            }
            return addAll;
        }

        @Override // java.util.List
        public V get(int i) {
            n();
            return p().get(i);
        }

        @Override // java.util.List
        public int indexOf(Object obj) {
            n();
            return p().indexOf(obj);
        }

        @Override // java.util.List
        public int lastIndexOf(Object obj) {
            n();
            return p().lastIndexOf(obj);
        }

        @Override // java.util.List
        public ListIterator<V> listIterator() {
            n();
            return new a();
        }

        public List<V> p() {
            return (List) k();
        }

        @Override // java.util.List
        public V remove(int i) {
            n();
            V remove = p().remove(i);
            AbstractMapBasedMultimap.access$210(AbstractMapBasedMultimap.this);
            o();
            return remove;
        }

        @Override // java.util.List
        public V set(int i, V v) {
            n();
            return p().set(i, v);
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // java.util.List
        public List<V> subList(int i, int i2) {
            n();
            return AbstractMapBasedMultimap.this.wrapList(m(), p().subList(i, i2), i() == null ? this : i());
        }

        @Override // java.util.List
        public ListIterator<V> listIterator(int i) {
            n();
            return new a(i);
        }
    }
}
