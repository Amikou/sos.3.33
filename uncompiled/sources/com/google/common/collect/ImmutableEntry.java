package com.google.common.collect;

import java.io.Serializable;

/* loaded from: classes2.dex */
public class ImmutableEntry<K, V> extends d5<K, V> implements Serializable {
    private static final long serialVersionUID = 0;
    public final K key;
    public final V value;

    public ImmutableEntry(K k, V v) {
        this.key = k;
        this.value = v;
    }

    @Override // defpackage.d5, java.util.Map.Entry
    public final K getKey() {
        return this.key;
    }

    @Override // defpackage.d5, java.util.Map.Entry
    public final V getValue() {
        return this.value;
    }

    @Override // defpackage.d5, java.util.Map.Entry
    public final V setValue(V v) {
        throw new UnsupportedOperationException();
    }
}
