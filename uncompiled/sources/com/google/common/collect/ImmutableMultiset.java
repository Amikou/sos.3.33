package com.google.common.collect;

import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.d;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;

/* loaded from: classes2.dex */
public abstract class ImmutableMultiset<E> extends ImmutableMultisetGwtSerializationDependencies<E> implements d<E> {
    public transient ImmutableList<E> f0;
    public transient ImmutableSet<d.a<E>> g0;

    /* loaded from: classes2.dex */
    public final class EntrySet extends IndexedImmutableSet<d.a<E>> {
        private static final long serialVersionUID = 0;

        public EntrySet() {
        }

        @Override // com.google.common.collect.ImmutableCollection, java.util.AbstractCollection, java.util.Collection
        public boolean contains(Object obj) {
            if (obj instanceof d.a) {
                d.a aVar = (d.a) obj;
                return aVar.getCount() > 0 && ImmutableMultiset.this.count(aVar.getElement()) == aVar.getCount();
            }
            return false;
        }

        @Override // com.google.common.collect.ImmutableSet, java.util.Collection, java.util.Set
        public int hashCode() {
            return ImmutableMultiset.this.hashCode();
        }

        @Override // com.google.common.collect.ImmutableCollection
        public boolean isPartialView() {
            return ImmutableMultiset.this.isPartialView();
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public int size() {
            return ImmutableMultiset.this.elementSet().size();
        }

        @Override // com.google.common.collect.ImmutableSet, com.google.common.collect.ImmutableCollection
        public Object writeReplace() {
            return new EntrySetSerializedForm(ImmutableMultiset.this);
        }

        public /* synthetic */ EntrySet(ImmutableMultiset immutableMultiset, a aVar) {
            this();
        }

        @Override // com.google.common.collect.IndexedImmutableSet
        public d.a<E> get(int i) {
            return ImmutableMultiset.this.getEntry(i);
        }
    }

    /* loaded from: classes2.dex */
    public static class EntrySetSerializedForm<E> implements Serializable {
        public final ImmutableMultiset<E> multiset;

        public EntrySetSerializedForm(ImmutableMultiset<E> immutableMultiset) {
            this.multiset = immutableMultiset;
        }

        public Object readResolve() {
            return this.multiset.entrySet();
        }
    }

    /* loaded from: classes2.dex */
    public class a extends af4<E> {
        public int a;
        public E f0;
        public final /* synthetic */ Iterator g0;

        public a(ImmutableMultiset immutableMultiset, Iterator it) {
            this.g0 = it;
        }

        @Override // java.util.Iterator
        public boolean hasNext() {
            return this.a > 0 || this.g0.hasNext();
        }

        @Override // java.util.Iterator
        public E next() {
            if (this.a <= 0) {
                d.a aVar = (d.a) this.g0.next();
                this.f0 = (E) aVar.getElement();
                this.a = aVar.getCount();
            }
            this.a--;
            E e = this.f0;
            Objects.requireNonNull(e);
            return e;
        }
    }

    /* loaded from: classes2.dex */
    public static class b<E> extends ImmutableCollection.b<E> {
        public e<E> a;
        public boolean b;
        public boolean c;

        public b() {
            this(4);
        }

        public static <T> e<T> l(Iterable<T> iterable) {
            if (iterable instanceof RegularImmutableMultiset) {
                return (e<E>) ((RegularImmutableMultiset) iterable).contents;
            }
            if (iterable instanceof AbstractMapBasedMultiset) {
                return (e<E>) ((AbstractMapBasedMultiset) iterable).backingMap;
            }
            return null;
        }

        @Override // com.google.common.collect.ImmutableCollection.b
        /* renamed from: f */
        public b<E> a(E e) {
            return j(e, 1);
        }

        public b<E> g(E... eArr) {
            super.b(eArr);
            return this;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public b<E> h(Iterable<? extends E> iterable) {
            Objects.requireNonNull(this.a);
            if (iterable instanceof d) {
                d d = Multisets.d(iterable);
                e l = l(d);
                if (l != null) {
                    e<E> eVar = this.a;
                    eVar.d(Math.max(eVar.C(), l.C()));
                    for (int e = l.e(); e >= 0; e = l.s(e)) {
                        j(l.i(e), l.k(e));
                    }
                } else {
                    Set<d.a<E>> entrySet = d.entrySet();
                    e<E> eVar2 = this.a;
                    eVar2.d(Math.max(eVar2.C(), entrySet.size()));
                    for (d.a<E> aVar : d.entrySet()) {
                        j(aVar.getElement(), aVar.getCount());
                    }
                }
            } else {
                super.c(iterable);
            }
            return this;
        }

        public b<E> i(Iterator<? extends E> it) {
            super.d(it);
            return this;
        }

        public b<E> j(E e, int i) {
            Objects.requireNonNull(this.a);
            if (i == 0) {
                return this;
            }
            if (this.b) {
                this.a = new e<>((e<? extends E>) this.a);
                this.c = false;
            }
            this.b = false;
            au2.k(e);
            e<E> eVar = this.a;
            eVar.u(e, i + eVar.f(e));
            return this;
        }

        public ImmutableMultiset<E> k() {
            Objects.requireNonNull(this.a);
            if (this.a.C() == 0) {
                return ImmutableMultiset.of();
            }
            if (this.c) {
                this.a = new e<>((e<? extends E>) this.a);
                this.c = false;
            }
            this.b = true;
            return new RegularImmutableMultiset(this.a);
        }

        public b(int i) {
            this.b = false;
            this.c = false;
            this.a = e.c(i);
        }
    }

    public static <E> b<E> builder() {
        return new b<>();
    }

    public static <E> ImmutableMultiset<E> copyFromEntries(Collection<? extends d.a<? extends E>> collection) {
        b bVar = new b(collection.size());
        for (d.a<? extends E> aVar : collection) {
            bVar.j(aVar.getElement(), aVar.getCount());
        }
        return bVar.k();
    }

    public static <E> ImmutableMultiset<E> copyOf(E[] eArr) {
        return e(eArr);
    }

    public static <E> ImmutableMultiset<E> e(E... eArr) {
        return new b().g(eArr).k();
    }

    private ImmutableSet<d.a<E>> i() {
        return isEmpty() ? ImmutableSet.of() : new EntrySet(this, null);
    }

    public static <E> ImmutableMultiset<E> of() {
        return RegularImmutableMultiset.EMPTY;
    }

    @Override // com.google.common.collect.d
    @Deprecated
    public final int add(E e, int i) {
        throw new UnsupportedOperationException();
    }

    @Override // com.google.common.collect.ImmutableCollection
    public ImmutableList<E> asList() {
        ImmutableList<E> immutableList = this.f0;
        if (immutableList == null) {
            ImmutableList<E> asList = super.asList();
            this.f0 = asList;
            return asList;
        }
        return immutableList;
    }

    @Override // com.google.common.collect.ImmutableCollection, java.util.AbstractCollection, java.util.Collection
    public boolean contains(Object obj) {
        return count(obj) > 0;
    }

    @Override // com.google.common.collect.ImmutableCollection
    public int copyIntoArray(Object[] objArr, int i) {
        af4<d.a<E>> it = entrySet().iterator();
        while (it.hasNext()) {
            d.a<E> next = it.next();
            Arrays.fill(objArr, i, next.getCount() + i, next.getElement());
            i += next.getCount();
        }
        return i;
    }

    public abstract /* synthetic */ int count(Object obj);

    public abstract ImmutableSet<E> elementSet();

    @Override // java.util.Collection
    public boolean equals(Object obj) {
        return Multisets.e(this, obj);
    }

    public abstract d.a<E> getEntry(int i);

    @Override // java.util.Collection
    public int hashCode() {
        return h.d(entrySet());
    }

    @Override // com.google.common.collect.d
    @Deprecated
    public final int remove(Object obj, int i) {
        throw new UnsupportedOperationException();
    }

    @Override // com.google.common.collect.d
    @Deprecated
    public final int setCount(E e, int i) {
        throw new UnsupportedOperationException();
    }

    @Override // java.util.AbstractCollection
    public String toString() {
        return entrySet().toString();
    }

    @Override // com.google.common.collect.ImmutableCollection
    abstract Object writeReplace();

    public static <E> ImmutableMultiset<E> copyOf(Iterable<? extends E> iterable) {
        if (iterable instanceof ImmutableMultiset) {
            ImmutableMultiset<E> immutableMultiset = (ImmutableMultiset) iterable;
            if (!immutableMultiset.isPartialView()) {
                return immutableMultiset;
            }
        }
        b bVar = new b(Multisets.g(iterable));
        bVar.h(iterable);
        return bVar.k();
    }

    public static <E> ImmutableMultiset<E> of(E e) {
        return e(e);
    }

    @Override // com.google.common.collect.d
    public ImmutableSet<d.a<E>> entrySet() {
        ImmutableSet<d.a<E>> immutableSet = this.g0;
        if (immutableSet == null) {
            ImmutableSet<d.a<E>> i = i();
            this.g0 = i;
            return i;
        }
        return immutableSet;
    }

    @Override // com.google.common.collect.ImmutableCollection, java.util.AbstractCollection, java.util.Collection, java.lang.Iterable
    public af4<E> iterator() {
        return new a(this, entrySet().iterator());
    }

    @Override // com.google.common.collect.d
    @Deprecated
    public final boolean setCount(E e, int i, int i2) {
        throw new UnsupportedOperationException();
    }

    public static <E> ImmutableMultiset<E> of(E e, E e2) {
        return e(e, e2);
    }

    public static <E> ImmutableMultiset<E> of(E e, E e2, E e3) {
        return e(e, e2, e3);
    }

    public static <E> ImmutableMultiset<E> of(E e, E e2, E e3, E e4) {
        return e(e, e2, e3, e4);
    }

    public static <E> ImmutableMultiset<E> of(E e, E e2, E e3, E e4, E e5) {
        return e(e, e2, e3, e4, e5);
    }

    public static <E> ImmutableMultiset<E> of(E e, E e2, E e3, E e4, E e5, E e6, E... eArr) {
        return new b().a(e).a(e2).a(e3).a(e4).a(e5).a(e6).g(eArr).k();
    }

    public static <E> ImmutableMultiset<E> copyOf(Iterator<? extends E> it) {
        return new b().i(it).k();
    }
}
