package com.google.common.collect;

import com.google.common.collect.d;
import com.google.common.primitives.Ints;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

/* loaded from: classes2.dex */
public abstract class AbstractMapBasedMultiset<E> extends com.google.common.collect.b<E> implements Serializable {
    private static final long serialVersionUID = 0;
    public transient e<E> backingMap;
    public transient long size;

    /* loaded from: classes2.dex */
    public class a extends AbstractMapBasedMultiset<E>.c<E> {
        public a() {
            super();
        }

        @Override // com.google.common.collect.AbstractMapBasedMultiset.c
        public E b(int i) {
            return AbstractMapBasedMultiset.this.backingMap.i(i);
        }
    }

    /* loaded from: classes2.dex */
    public class b extends AbstractMapBasedMultiset<E>.c<d.a<E>> {
        public b() {
            super();
        }

        @Override // com.google.common.collect.AbstractMapBasedMultiset.c
        /* renamed from: c */
        public d.a<E> b(int i) {
            return AbstractMapBasedMultiset.this.backingMap.g(i);
        }
    }

    /* loaded from: classes2.dex */
    public abstract class c<T> implements Iterator<T> {
        public int a;
        public int f0 = -1;
        public int g0;

        public c() {
            this.a = AbstractMapBasedMultiset.this.backingMap.e();
            this.g0 = AbstractMapBasedMultiset.this.backingMap.d;
        }

        public final void a() {
            if (AbstractMapBasedMultiset.this.backingMap.d != this.g0) {
                throw new ConcurrentModificationException();
            }
        }

        public abstract T b(int i);

        @Override // java.util.Iterator
        public boolean hasNext() {
            a();
            return this.a >= 0;
        }

        @Override // java.util.Iterator
        public T next() {
            if (hasNext()) {
                T b = b(this.a);
                int i = this.a;
                this.f0 = i;
                this.a = AbstractMapBasedMultiset.this.backingMap.s(i);
                return b;
            }
            throw new NoSuchElementException();
        }

        @Override // java.util.Iterator
        public void remove() {
            AbstractMapBasedMultiset abstractMapBasedMultiset;
            a();
            o00.d(this.f0 != -1);
            AbstractMapBasedMultiset.this.size -= abstractMapBasedMultiset.backingMap.x(this.f0);
            this.a = AbstractMapBasedMultiset.this.backingMap.t(this.a, this.f0);
            this.f0 = -1;
            this.g0 = AbstractMapBasedMultiset.this.backingMap.d;
        }
    }

    public AbstractMapBasedMultiset(int i) {
        this.backingMap = newBackingMap(i);
    }

    private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        int c2 = f.c(objectInputStream);
        this.backingMap = newBackingMap(3);
        f.b(this, objectInputStream, c2);
    }

    private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.defaultWriteObject();
        f.e(this, objectOutputStream);
    }

    @Override // com.google.common.collect.b, com.google.common.collect.d
    public final int add(E e, int i) {
        if (i == 0) {
            return count(e);
        }
        au2.f(i > 0, "occurrences cannot be negative: %s", i);
        int m = this.backingMap.m(e);
        if (m == -1) {
            this.backingMap.u(e, i);
            this.size += i;
            return 0;
        }
        int k = this.backingMap.k(m);
        long j = i;
        long j2 = k + j;
        au2.h(j2 <= 2147483647L, "too many occurrences: %s", j2);
        this.backingMap.B(m, (int) j2);
        this.size += j;
        return k;
    }

    public void addTo(d<? super E> dVar) {
        au2.k(dVar);
        int e = this.backingMap.e();
        while (e >= 0) {
            dVar.add((E) this.backingMap.i(e), this.backingMap.k(e));
            e = this.backingMap.s(e);
        }
    }

    @Override // com.google.common.collect.b, java.util.AbstractCollection, java.util.Collection
    public final void clear() {
        this.backingMap.a();
        this.size = 0L;
    }

    @Override // com.google.common.collect.d
    public final int count(Object obj) {
        return this.backingMap.f(obj);
    }

    @Override // com.google.common.collect.b
    public final int distinctElements() {
        return this.backingMap.C();
    }

    @Override // com.google.common.collect.b
    public final Iterator<E> elementIterator() {
        return new a();
    }

    @Override // com.google.common.collect.b
    public final Iterator<d.a<E>> entryIterator() {
        return new b();
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable
    public final Iterator<E> iterator() {
        return Multisets.h(this);
    }

    public abstract e<E> newBackingMap(int i);

    @Override // com.google.common.collect.b, com.google.common.collect.d
    public final int remove(Object obj, int i) {
        if (i == 0) {
            return count(obj);
        }
        au2.f(i > 0, "occurrences cannot be negative: %s", i);
        int m = this.backingMap.m(obj);
        if (m == -1) {
            return 0;
        }
        int k = this.backingMap.k(m);
        if (k > i) {
            this.backingMap.B(m, k - i);
        } else {
            this.backingMap.x(m);
            i = k;
        }
        this.size -= i;
        return k;
    }

    @Override // com.google.common.collect.b, com.google.common.collect.d
    public final int setCount(E e, int i) {
        o00.b(i, "count");
        e<E> eVar = this.backingMap;
        int v = i == 0 ? eVar.v(e) : eVar.u(e, i);
        this.size += i - v;
        return v;
    }

    @Override // java.util.AbstractCollection, java.util.Collection, com.google.common.collect.d
    public final int size() {
        return Ints.j(this.size);
    }

    @Override // com.google.common.collect.b, com.google.common.collect.d
    public final boolean setCount(E e, int i, int i2) {
        o00.b(i, "oldCount");
        o00.b(i2, "newCount");
        int m = this.backingMap.m(e);
        if (m == -1) {
            if (i != 0) {
                return false;
            }
            if (i2 > 0) {
                this.backingMap.u(e, i2);
                this.size += i2;
            }
            return true;
        } else if (this.backingMap.k(m) != i) {
            return false;
        } else {
            if (i2 == 0) {
                this.backingMap.x(m);
                this.size -= i;
            } else {
                this.backingMap.B(m, i2);
                this.size += i2 - i;
            }
            return true;
        }
    }
}
