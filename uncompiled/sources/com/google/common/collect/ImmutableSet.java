package com.google.common.collect;

import com.google.common.collect.ImmutableCollection;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;
import java.util.SortedSet;

/* loaded from: classes2.dex */
public abstract class ImmutableSet<E> extends ImmutableCollection<E> implements Set<E> {
    public static final int MAX_TABLE_SIZE = 1073741824;
    public transient ImmutableList<E> f0;

    /* loaded from: classes2.dex */
    public static class SerializedForm implements Serializable {
        private static final long serialVersionUID = 0;
        public final Object[] elements;

        public SerializedForm(Object[] objArr) {
            this.elements = objArr;
        }

        public Object readResolve() {
            return ImmutableSet.copyOf(this.elements);
        }
    }

    /* loaded from: classes2.dex */
    public static class a<E> extends ImmutableCollection.a<E> {
        public Object[] d;
        public int e;

        public a() {
            super(4);
        }

        @Override // com.google.common.collect.ImmutableCollection.b
        /* renamed from: i */
        public a<E> a(E e) {
            au2.k(e);
            if (this.d != null && ImmutableSet.chooseTableSize(this.b) <= this.d.length) {
                l(e);
                return this;
            }
            this.d = null;
            super.f(e);
            return this;
        }

        public a<E> j(E... eArr) {
            if (this.d != null) {
                for (E e : eArr) {
                    a(e);
                }
            } else {
                super.b(eArr);
            }
            return this;
        }

        public a<E> k(Iterator<? extends E> it) {
            au2.k(it);
            while (it.hasNext()) {
                a(it.next());
            }
            return this;
        }

        public final void l(E e) {
            Objects.requireNonNull(this.d);
            int length = this.d.length - 1;
            int hashCode = e.hashCode();
            int b = dk1.b(hashCode);
            while (true) {
                int i = b & length;
                Object[] objArr = this.d;
                Object obj = objArr[i];
                if (obj == null) {
                    objArr[i] = e;
                    this.e += hashCode;
                    super.f(e);
                    return;
                } else if (obj.equals(e)) {
                    return;
                } else {
                    b = i + 1;
                }
            }
        }

        public ImmutableSet<E> m() {
            ImmutableSet<E> e;
            int i = this.b;
            if (i != 0) {
                if (i != 1) {
                    if (this.d == null || ImmutableSet.chooseTableSize(i) != this.d.length) {
                        e = ImmutableSet.e(this.b, this.a);
                        this.b = e.size();
                    } else {
                        Object[] copyOf = ImmutableSet.i(this.b, this.a.length) ? Arrays.copyOf(this.a, this.b) : this.a;
                        int i2 = this.e;
                        Object[] objArr = this.d;
                        e = new RegularImmutableSet<>(copyOf, i2, objArr, objArr.length - 1, this.b);
                    }
                    this.c = true;
                    this.d = null;
                    return e;
                }
                Object obj = this.a[0];
                Objects.requireNonNull(obj);
                return ImmutableSet.of(obj);
            }
            return ImmutableSet.of();
        }

        public a(int i) {
            super(i);
            this.d = new Object[ImmutableSet.chooseTableSize(i)];
        }
    }

    public static <E> a<E> builder() {
        return new a<>();
    }

    public static <E> a<E> builderWithExpectedSize(int i) {
        o00.b(i, "expectedSize");
        return new a<>(i);
    }

    public static int chooseTableSize(int i) {
        int max = Math.max(i, 2);
        if (max < 751619276) {
            int highestOneBit = Integer.highestOneBit(max - 1) << 1;
            while (highestOneBit * 0.7d < max) {
                highestOneBit <<= 1;
            }
            return highestOneBit;
        }
        au2.e(max < 1073741824, "collection too large");
        return 1073741824;
    }

    public static <E> ImmutableSet<E> copyOf(Collection<? extends E> collection) {
        if ((collection instanceof ImmutableSet) && !(collection instanceof SortedSet)) {
            ImmutableSet<E> immutableSet = (ImmutableSet) collection;
            if (!immutableSet.isPartialView()) {
                return immutableSet;
            }
        }
        Object[] array = collection.toArray();
        return e(array.length, array);
    }

    public static <E> ImmutableSet<E> e(int i, Object... objArr) {
        if (i != 0) {
            if (i != 1) {
                int chooseTableSize = chooseTableSize(i);
                Object[] objArr2 = new Object[chooseTableSize];
                int i2 = chooseTableSize - 1;
                int i3 = 0;
                int i4 = 0;
                for (int i5 = 0; i5 < i; i5++) {
                    Object a2 = fl2.a(objArr[i5], i5);
                    int hashCode = a2.hashCode();
                    int b = dk1.b(hashCode);
                    while (true) {
                        int i6 = b & i2;
                        Object obj = objArr2[i6];
                        if (obj == null) {
                            objArr[i4] = a2;
                            objArr2[i6] = a2;
                            i3 += hashCode;
                            i4++;
                            break;
                        } else if (obj.equals(a2)) {
                            break;
                        } else {
                            b++;
                        }
                    }
                }
                Arrays.fill(objArr, i4, i, (Object) null);
                if (i4 == 1) {
                    Object obj2 = objArr[0];
                    Objects.requireNonNull(obj2);
                    return new SingletonImmutableSet(obj2);
                } else if (chooseTableSize(i4) < chooseTableSize / 2) {
                    return e(i4, objArr);
                } else {
                    if (i(i4, objArr.length)) {
                        objArr = Arrays.copyOf(objArr, i4);
                    }
                    return new RegularImmutableSet(objArr, i3, objArr2, i2, i4);
                }
            }
            Object obj3 = objArr[0];
            Objects.requireNonNull(obj3);
            return of(obj3);
        }
        return of();
    }

    public static boolean i(int i, int i2) {
        return i < (i2 >> 1) + (i2 >> 2);
    }

    public static <E> ImmutableSet<E> of() {
        return RegularImmutableSet.EMPTY;
    }

    @Override // com.google.common.collect.ImmutableCollection
    public ImmutableList<E> asList() {
        ImmutableList<E> immutableList = this.f0;
        if (immutableList == null) {
            ImmutableList<E> createAsList = createAsList();
            this.f0 = createAsList;
            return createAsList;
        }
        return immutableList;
    }

    public ImmutableList<E> createAsList() {
        return ImmutableList.asImmutableList(toArray());
    }

    @Override // java.util.Collection, java.util.Set
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if ((obj instanceof ImmutableSet) && isHashCodeFast() && ((ImmutableSet) obj).isHashCodeFast() && hashCode() != obj.hashCode()) {
            return false;
        }
        return h.a(this, obj);
    }

    @Override // java.util.Collection, java.util.Set
    public int hashCode() {
        return h.d(this);
    }

    public boolean isHashCodeFast() {
        return false;
    }

    @Override // com.google.common.collect.ImmutableCollection, java.util.AbstractCollection, java.util.Collection, java.lang.Iterable
    public abstract af4<E> iterator();

    @Override // com.google.common.collect.ImmutableCollection
    Object writeReplace() {
        return new SerializedForm(toArray());
    }

    public static <E> ImmutableSet<E> of(E e) {
        return new SingletonImmutableSet(e);
    }

    public static <E> ImmutableSet<E> of(E e, E e2) {
        return e(2, e, e2);
    }

    public static <E> ImmutableSet<E> of(E e, E e2, E e3) {
        return e(3, e, e2, e3);
    }

    public static <E> ImmutableSet<E> of(E e, E e2, E e3, E e4) {
        return e(4, e, e2, e3, e4);
    }

    public static <E> ImmutableSet<E> copyOf(Iterable<? extends E> iterable) {
        if (iterable instanceof Collection) {
            return copyOf((Collection) iterable);
        }
        return copyOf(iterable.iterator());
    }

    public static <E> ImmutableSet<E> of(E e, E e2, E e3, E e4, E e5) {
        return e(5, e, e2, e3, e4, e5);
    }

    public static <E> ImmutableSet<E> of(E e, E e2, E e3, E e4, E e5, E e6, E... eArr) {
        au2.e(eArr.length <= 2147483641, "the total number of elements must fit in an int");
        int length = eArr.length + 6;
        Object[] objArr = new Object[length];
        objArr[0] = e;
        objArr[1] = e2;
        objArr[2] = e3;
        objArr[3] = e4;
        objArr[4] = e5;
        objArr[5] = e6;
        System.arraycopy(eArr, 0, objArr, 6, eArr.length);
        return e(length, objArr);
    }

    public static <E> ImmutableSet<E> copyOf(Iterator<? extends E> it) {
        if (!it.hasNext()) {
            return of();
        }
        E next = it.next();
        if (!it.hasNext()) {
            return of((Object) next);
        }
        return new a().a(next).k(it).m();
    }

    public static <E> ImmutableSet<E> copyOf(E[] eArr) {
        int length = eArr.length;
        if (length != 0) {
            if (length != 1) {
                return e(eArr.length, (Object[]) eArr.clone());
            }
            return of((Object) eArr[0]);
        }
        return of();
    }
}
