package com.google.common.collect;

import com.google.common.primitives.Ints;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.AbstractCollection;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Arrays;
import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Set;

/* loaded from: classes2.dex */
public class CompactHashMap<K, V> extends AbstractMap<K, V> implements Serializable {
    public static final double HASH_FLOODING_FPP = 0.001d;
    public static final Object k0 = new Object();
    public transient Object a;
    public transient int[] entries;
    public transient int f0;
    public transient int g0;
    public transient Set<K> h0;
    public transient Set<Map.Entry<K, V>> i0;
    public transient Collection<V> j0;
    public transient Object[] keys;
    public transient Object[] values;

    /* loaded from: classes2.dex */
    public class a extends CompactHashMap<K, V>.e<K> {
        public a() {
            super(CompactHashMap.this, null);
        }

        @Override // com.google.common.collect.CompactHashMap.e
        public K b(int i) {
            return (K) CompactHashMap.this.d(i);
        }
    }

    /* loaded from: classes2.dex */
    public class b extends CompactHashMap<K, V>.e<Map.Entry<K, V>> {
        public b() {
            super(CompactHashMap.this, null);
        }

        @Override // com.google.common.collect.CompactHashMap.e
        /* renamed from: d */
        public Map.Entry<K, V> b(int i) {
            return new g(i);
        }
    }

    /* loaded from: classes2.dex */
    public class c extends CompactHashMap<K, V>.e<V> {
        public c() {
            super(CompactHashMap.this, null);
        }

        @Override // com.google.common.collect.CompactHashMap.e
        public V b(int i) {
            return (V) CompactHashMap.this.p(i);
        }
    }

    /* loaded from: classes2.dex */
    public class d extends AbstractSet<Map.Entry<K, V>> {
        public d() {
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public void clear() {
            CompactHashMap.this.clear();
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean contains(Object obj) {
            Map<K, V> delegateOrNull = CompactHashMap.this.delegateOrNull();
            if (delegateOrNull != null) {
                return delegateOrNull.entrySet().contains(obj);
            }
            if (obj instanceof Map.Entry) {
                Map.Entry entry = (Map.Entry) obj;
                int c = CompactHashMap.this.c(entry.getKey());
                return c != -1 && ql2.a(CompactHashMap.this.p(c), entry.getValue());
            }
            return false;
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable, java.util.Set
        public Iterator<Map.Entry<K, V>> iterator() {
            return CompactHashMap.this.entrySetIterator();
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean remove(Object obj) {
            Map<K, V> delegateOrNull = CompactHashMap.this.delegateOrNull();
            if (delegateOrNull != null) {
                return delegateOrNull.entrySet().remove(obj);
            }
            if (obj instanceof Map.Entry) {
                Map.Entry entry = (Map.Entry) obj;
                if (CompactHashMap.this.needsAllocArrays()) {
                    return false;
                }
                int b = CompactHashMap.this.b();
                int f = j30.f(entry.getKey(), entry.getValue(), b, CompactHashMap.this.h(), CompactHashMap.this.f(), CompactHashMap.this.g(), CompactHashMap.this.i());
                if (f == -1) {
                    return false;
                }
                CompactHashMap.this.moveLastEntry(f, b);
                CompactHashMap.access$1210(CompactHashMap.this);
                CompactHashMap.this.incrementModCount();
                return true;
            }
            return false;
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public int size() {
            return CompactHashMap.this.size();
        }
    }

    /* loaded from: classes2.dex */
    public class f extends AbstractSet<K> {
        public f() {
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public void clear() {
            CompactHashMap.this.clear();
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean contains(Object obj) {
            return CompactHashMap.this.containsKey(obj);
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable, java.util.Set
        public Iterator<K> iterator() {
            return CompactHashMap.this.keySetIterator();
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean remove(Object obj) {
            Map<K, V> delegateOrNull = CompactHashMap.this.delegateOrNull();
            if (delegateOrNull != null) {
                return delegateOrNull.keySet().remove(obj);
            }
            return CompactHashMap.this.e(obj) != CompactHashMap.k0;
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public int size() {
            return CompactHashMap.this.size();
        }
    }

    /* loaded from: classes2.dex */
    public final class g extends d5<K, V> {
        public final K a;
        public int f0;

        public g(int i) {
            this.a = (K) CompactHashMap.this.d(i);
            this.f0 = i;
        }

        public final void a() {
            int i = this.f0;
            if (i == -1 || i >= CompactHashMap.this.size() || !ql2.a(this.a, CompactHashMap.this.d(this.f0))) {
                this.f0 = CompactHashMap.this.c(this.a);
            }
        }

        @Override // defpackage.d5, java.util.Map.Entry
        public K getKey() {
            return this.a;
        }

        @Override // defpackage.d5, java.util.Map.Entry
        public V getValue() {
            Map<K, V> delegateOrNull = CompactHashMap.this.delegateOrNull();
            if (delegateOrNull != null) {
                return (V) yi2.a(delegateOrNull.get(this.a));
            }
            a();
            int i = this.f0;
            return i == -1 ? (V) yi2.b() : (V) CompactHashMap.this.p(i);
        }

        @Override // defpackage.d5, java.util.Map.Entry
        public V setValue(V v) {
            Map<K, V> delegateOrNull = CompactHashMap.this.delegateOrNull();
            if (delegateOrNull != null) {
                return (V) yi2.a(delegateOrNull.put(this.a, v));
            }
            a();
            int i = this.f0;
            if (i != -1) {
                V v2 = (V) CompactHashMap.this.p(i);
                CompactHashMap.this.o(this.f0, v);
                return v2;
            }
            CompactHashMap.this.put(this.a, v);
            return (V) yi2.b();
        }
    }

    /* loaded from: classes2.dex */
    public class h extends AbstractCollection<V> {
        public h() {
        }

        @Override // java.util.AbstractCollection, java.util.Collection
        public void clear() {
            CompactHashMap.this.clear();
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable
        public Iterator<V> iterator() {
            return CompactHashMap.this.valuesIterator();
        }

        @Override // java.util.AbstractCollection, java.util.Collection
        public int size() {
            return CompactHashMap.this.size();
        }
    }

    public CompactHashMap() {
        init(3);
    }

    public static /* synthetic */ int access$1210(CompactHashMap compactHashMap) {
        int i = compactHashMap.g0;
        compactHashMap.g0 = i - 1;
        return i;
    }

    public static <K, V> CompactHashMap<K, V> create() {
        return new CompactHashMap<>();
    }

    public static <K, V> CompactHashMap<K, V> createWithExpectedSize(int i) {
        return new CompactHashMap<>(i);
    }

    /* JADX WARN: Multi-variable type inference failed */
    private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        int readInt = objectInputStream.readInt();
        if (readInt >= 0) {
            init(readInt);
            for (int i = 0; i < readInt; i++) {
                put(objectInputStream.readObject(), objectInputStream.readObject());
            }
            return;
        }
        StringBuilder sb = new StringBuilder(25);
        sb.append("Invalid size: ");
        sb.append(readInt);
        throw new InvalidObjectException(sb.toString());
    }

    private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeInt(size());
        Iterator<Map.Entry<K, V>> entrySetIterator = entrySetIterator();
        while (entrySetIterator.hasNext()) {
            Map.Entry<K, V> next = entrySetIterator.next();
            objectOutputStream.writeObject(next.getKey());
            objectOutputStream.writeObject(next.getValue());
        }
    }

    public final int a(int i) {
        return f()[i];
    }

    public void accessEntry(int i) {
    }

    public int adjustAfterRemove(int i, int i2) {
        return i - 1;
    }

    public int allocArrays() {
        au2.q(needsAllocArrays(), "Arrays already allocated");
        int i = this.f0;
        int j = j30.j(i);
        this.a = j30.a(j);
        m(j - 1);
        this.entries = new int[i];
        this.keys = new Object[i];
        this.values = new Object[i];
        return i;
    }

    public final int b() {
        return (1 << (this.f0 & 31)) - 1;
    }

    public final int c(Object obj) {
        if (needsAllocArrays()) {
            return -1;
        }
        int c2 = dk1.c(obj);
        int b2 = b();
        int h2 = j30.h(h(), c2 & b2);
        if (h2 == 0) {
            return -1;
        }
        int b3 = j30.b(c2, b2);
        do {
            int i = h2 - 1;
            int a2 = a(i);
            if (j30.b(a2, b2) == b3 && ql2.a(obj, d(i))) {
                return i;
            }
            h2 = j30.c(a2, b2);
        } while (h2 != 0);
        return -1;
    }

    @Override // java.util.AbstractMap, java.util.Map
    public void clear() {
        if (needsAllocArrays()) {
            return;
        }
        incrementModCount();
        Map<K, V> delegateOrNull = delegateOrNull();
        if (delegateOrNull != null) {
            this.f0 = Ints.f(size(), 3, 1073741823);
            delegateOrNull.clear();
            this.a = null;
            this.g0 = 0;
            return;
        }
        Arrays.fill(g(), 0, this.g0, (Object) null);
        Arrays.fill(i(), 0, this.g0, (Object) null);
        j30.g(h());
        Arrays.fill(f(), 0, this.g0, 0);
        this.g0 = 0;
    }

    @Override // java.util.AbstractMap, java.util.Map
    public boolean containsKey(Object obj) {
        Map<K, V> delegateOrNull = delegateOrNull();
        if (delegateOrNull != null) {
            return delegateOrNull.containsKey(obj);
        }
        return c(obj) != -1;
    }

    @Override // java.util.AbstractMap, java.util.Map
    public boolean containsValue(Object obj) {
        Map<K, V> delegateOrNull = delegateOrNull();
        if (delegateOrNull != null) {
            return delegateOrNull.containsValue(obj);
        }
        for (int i = 0; i < this.g0; i++) {
            if (ql2.a(obj, p(i))) {
                return true;
            }
        }
        return false;
    }

    public Map<K, V> convertToHashFloodingResistantImplementation() {
        Map<K, V> createHashFloodingResistantDelegate = createHashFloodingResistantDelegate(b() + 1);
        int firstEntryIndex = firstEntryIndex();
        while (firstEntryIndex >= 0) {
            createHashFloodingResistantDelegate.put(d(firstEntryIndex), p(firstEntryIndex));
            firstEntryIndex = getSuccessor(firstEntryIndex);
        }
        this.a = createHashFloodingResistantDelegate;
        this.entries = null;
        this.keys = null;
        this.values = null;
        incrementModCount();
        return createHashFloodingResistantDelegate;
    }

    public Set<Map.Entry<K, V>> createEntrySet() {
        return new d();
    }

    public Map<K, V> createHashFloodingResistantDelegate(int i) {
        return new LinkedHashMap(i, 1.0f);
    }

    public Set<K> createKeySet() {
        return new f();
    }

    public Collection<V> createValues() {
        return new h();
    }

    public final K d(int i) {
        return (K) g()[i];
    }

    public Map<K, V> delegateOrNull() {
        Object obj = this.a;
        if (obj instanceof Map) {
            return (Map) obj;
        }
        return null;
    }

    public final Object e(Object obj) {
        if (needsAllocArrays()) {
            return k0;
        }
        int b2 = b();
        int f2 = j30.f(obj, null, b2, h(), f(), g(), null);
        if (f2 == -1) {
            return k0;
        }
        V p = p(f2);
        moveLastEntry(f2, b2);
        this.g0--;
        incrementModCount();
        return p;
    }

    @Override // java.util.AbstractMap, java.util.Map
    public Set<Map.Entry<K, V>> entrySet() {
        Set<Map.Entry<K, V>> set = this.i0;
        if (set == null) {
            Set<Map.Entry<K, V>> createEntrySet = createEntrySet();
            this.i0 = createEntrySet;
            return createEntrySet;
        }
        return set;
    }

    public Iterator<Map.Entry<K, V>> entrySetIterator() {
        Map<K, V> delegateOrNull = delegateOrNull();
        if (delegateOrNull != null) {
            return delegateOrNull.entrySet().iterator();
        }
        return new b();
    }

    public final int[] f() {
        int[] iArr = this.entries;
        Objects.requireNonNull(iArr);
        return iArr;
    }

    public int firstEntryIndex() {
        return isEmpty() ? -1 : 0;
    }

    public final Object[] g() {
        Object[] objArr = this.keys;
        Objects.requireNonNull(objArr);
        return objArr;
    }

    @Override // java.util.AbstractMap, java.util.Map
    public V get(Object obj) {
        Map<K, V> delegateOrNull = delegateOrNull();
        if (delegateOrNull != null) {
            return delegateOrNull.get(obj);
        }
        int c2 = c(obj);
        if (c2 == -1) {
            return null;
        }
        accessEntry(c2);
        return p(c2);
    }

    public int getSuccessor(int i) {
        int i2 = i + 1;
        if (i2 < this.g0) {
            return i2;
        }
        return -1;
    }

    public final Object h() {
        Object obj = this.a;
        Objects.requireNonNull(obj);
        return obj;
    }

    public final Object[] i() {
        Object[] objArr = this.values;
        Objects.requireNonNull(objArr);
        return objArr;
    }

    public void incrementModCount() {
        this.f0 += 32;
    }

    public void init(int i) {
        au2.e(i >= 0, "Expected size must be >= 0");
        this.f0 = Ints.f(i, 1, 1073741823);
    }

    public void insertEntry(int i, K k, V v, int i2, int i3) {
        l(i, j30.d(i2, 0, i3));
        n(i, k);
        o(i, v);
    }

    @Override // java.util.AbstractMap, java.util.Map
    public boolean isEmpty() {
        return size() == 0;
    }

    public final void j(int i) {
        int min;
        int length = f().length;
        if (i <= length || (min = Math.min(1073741823, (Math.max(1, length >>> 1) + length) | 1)) == length) {
            return;
        }
        resizeEntries(min);
    }

    public final int k(int i, int i2, int i3, int i4) {
        Object a2 = j30.a(i2);
        int i5 = i2 - 1;
        if (i4 != 0) {
            j30.i(a2, i3 & i5, i4 + 1);
        }
        Object h2 = h();
        int[] f2 = f();
        for (int i6 = 0; i6 <= i; i6++) {
            int h3 = j30.h(h2, i6);
            while (h3 != 0) {
                int i7 = h3 - 1;
                int i8 = f2[i7];
                int b2 = j30.b(i8, i) | i6;
                int i9 = b2 & i5;
                int h4 = j30.h(a2, i9);
                j30.i(a2, i9, h3);
                f2[i7] = j30.d(b2, h4, i5);
                h3 = j30.c(i8, i);
            }
        }
        this.a = a2;
        m(i5);
        return i5;
    }

    @Override // java.util.AbstractMap, java.util.Map
    public Set<K> keySet() {
        Set<K> set = this.h0;
        if (set == null) {
            Set<K> createKeySet = createKeySet();
            this.h0 = createKeySet;
            return createKeySet;
        }
        return set;
    }

    public Iterator<K> keySetIterator() {
        Map<K, V> delegateOrNull = delegateOrNull();
        if (delegateOrNull != null) {
            return delegateOrNull.keySet().iterator();
        }
        return new a();
    }

    public final void l(int i, int i2) {
        f()[i] = i2;
    }

    public final void m(int i) {
        this.f0 = j30.d(this.f0, 32 - Integer.numberOfLeadingZeros(i), 31);
    }

    public void moveLastEntry(int i, int i2) {
        Object h2 = h();
        int[] f2 = f();
        Object[] g2 = g();
        Object[] i3 = i();
        int size = size() - 1;
        if (i < size) {
            Object obj = g2[size];
            g2[i] = obj;
            i3[i] = i3[size];
            g2[size] = null;
            i3[size] = null;
            f2[i] = f2[size];
            f2[size] = 0;
            int c2 = dk1.c(obj) & i2;
            int h3 = j30.h(h2, c2);
            int i4 = size + 1;
            if (h3 == i4) {
                j30.i(h2, c2, i + 1);
                return;
            }
            while (true) {
                int i5 = h3 - 1;
                int i6 = f2[i5];
                int c3 = j30.c(i6, i2);
                if (c3 == i4) {
                    f2[i5] = j30.d(i6, i + 1, i2);
                    return;
                }
                h3 = c3;
            }
        } else {
            g2[i] = null;
            i3[i] = null;
            f2[i] = 0;
        }
    }

    public final void n(int i, K k) {
        g()[i] = k;
    }

    public boolean needsAllocArrays() {
        return this.a == null;
    }

    public final void o(int i, V v) {
        i()[i] = v;
    }

    public final V p(int i) {
        return (V) i()[i];
    }

    @Override // java.util.AbstractMap, java.util.Map
    public V put(K k, V v) {
        int k2;
        int i;
        if (needsAllocArrays()) {
            allocArrays();
        }
        Map<K, V> delegateOrNull = delegateOrNull();
        if (delegateOrNull != null) {
            return delegateOrNull.put(k, v);
        }
        int[] f2 = f();
        Object[] g2 = g();
        Object[] i2 = i();
        int i3 = this.g0;
        int i4 = i3 + 1;
        int c2 = dk1.c(k);
        int b2 = b();
        int i5 = c2 & b2;
        int h2 = j30.h(h(), i5);
        if (h2 != 0) {
            int b3 = j30.b(c2, b2);
            int i6 = 0;
            while (true) {
                int i7 = h2 - 1;
                int i8 = f2[i7];
                if (j30.b(i8, b2) == b3 && ql2.a(k, g2[i7])) {
                    V v2 = (V) i2[i7];
                    i2[i7] = v;
                    accessEntry(i7);
                    return v2;
                }
                int c3 = j30.c(i8, b2);
                i6++;
                if (c3 != 0) {
                    h2 = c3;
                } else if (i6 >= 9) {
                    return convertToHashFloodingResistantImplementation().put(k, v);
                } else {
                    if (i4 > b2) {
                        k2 = k(b2, j30.e(b2), c2, i3);
                    } else {
                        f2[i7] = j30.d(i8, i4, b2);
                    }
                }
            }
        } else if (i4 > b2) {
            k2 = k(b2, j30.e(b2), c2, i3);
            i = k2;
        } else {
            j30.i(h(), i5, i4);
            i = b2;
        }
        j(i4);
        insertEntry(i3, k, v, c2, i);
        this.g0 = i4;
        incrementModCount();
        return null;
    }

    @Override // java.util.AbstractMap, java.util.Map
    public V remove(Object obj) {
        Map<K, V> delegateOrNull = delegateOrNull();
        if (delegateOrNull != null) {
            return delegateOrNull.remove(obj);
        }
        V v = (V) e(obj);
        if (v == k0) {
            return null;
        }
        return v;
    }

    public void resizeEntries(int i) {
        this.entries = Arrays.copyOf(f(), i);
        this.keys = Arrays.copyOf(g(), i);
        this.values = Arrays.copyOf(i(), i);
    }

    @Override // java.util.AbstractMap, java.util.Map
    public int size() {
        Map<K, V> delegateOrNull = delegateOrNull();
        return delegateOrNull != null ? delegateOrNull.size() : this.g0;
    }

    public void trimToSize() {
        if (needsAllocArrays()) {
            return;
        }
        Map<K, V> delegateOrNull = delegateOrNull();
        if (delegateOrNull != null) {
            Map<K, V> createHashFloodingResistantDelegate = createHashFloodingResistantDelegate(size());
            createHashFloodingResistantDelegate.putAll(delegateOrNull);
            this.a = createHashFloodingResistantDelegate;
            return;
        }
        int i = this.g0;
        if (i < f().length) {
            resizeEntries(i);
        }
        int j = j30.j(i);
        int b2 = b();
        if (j < b2) {
            k(b2, j, 0, 0);
        }
    }

    @Override // java.util.AbstractMap, java.util.Map
    public Collection<V> values() {
        Collection<V> collection = this.j0;
        if (collection == null) {
            Collection<V> createValues = createValues();
            this.j0 = createValues;
            return createValues;
        }
        return collection;
    }

    public Iterator<V> valuesIterator() {
        Map<K, V> delegateOrNull = delegateOrNull();
        if (delegateOrNull != null) {
            return delegateOrNull.values().iterator();
        }
        return new c();
    }

    public CompactHashMap(int i) {
        init(i);
    }

    /* loaded from: classes2.dex */
    public abstract class e<T> implements Iterator<T> {
        public int a;
        public int f0;
        public int g0;

        public e() {
            this.a = CompactHashMap.this.f0;
            this.f0 = CompactHashMap.this.firstEntryIndex();
            this.g0 = -1;
        }

        public final void a() {
            if (CompactHashMap.this.f0 != this.a) {
                throw new ConcurrentModificationException();
            }
        }

        public abstract T b(int i);

        public void c() {
            this.a += 32;
        }

        @Override // java.util.Iterator
        public boolean hasNext() {
            return this.f0 >= 0;
        }

        @Override // java.util.Iterator
        public T next() {
            a();
            if (hasNext()) {
                int i = this.f0;
                this.g0 = i;
                T b = b(i);
                this.f0 = CompactHashMap.this.getSuccessor(this.f0);
                return b;
            }
            throw new NoSuchElementException();
        }

        @Override // java.util.Iterator
        public void remove() {
            a();
            o00.d(this.g0 >= 0);
            c();
            CompactHashMap compactHashMap = CompactHashMap.this;
            compactHashMap.remove(compactHashMap.d(this.g0));
            this.f0 = CompactHashMap.this.adjustAfterRemove(this.f0, this.g0);
            this.g0 = -1;
        }

        public /* synthetic */ e(CompactHashMap compactHashMap, a aVar) {
            this();
        }
    }
}
