package com.google.common.collect;

import com.google.common.base.Equivalence;
import com.google.common.collect.MapMakerInternalMap;
import defpackage.s92;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/* loaded from: classes2.dex */
public final class MapMaker {
    public boolean a;
    public int b = -1;
    public int c = -1;
    public MapMakerInternalMap.Strength d;
    public MapMakerInternalMap.Strength e;
    public Equivalence<Object> f;

    /* loaded from: classes2.dex */
    public enum Dummy {
        VALUE
    }

    public MapMaker a(int i) {
        int i2 = this.c;
        au2.r(i2 == -1, "concurrency level was already set to %s", i2);
        au2.d(i > 0);
        this.c = i;
        return this;
    }

    public int b() {
        int i = this.c;
        if (i == -1) {
            return 4;
        }
        return i;
    }

    public int c() {
        int i = this.b;
        if (i == -1) {
            return 16;
        }
        return i;
    }

    public Equivalence<Object> d() {
        return (Equivalence) s92.a(this.f, e().defaultEquivalence());
    }

    public MapMakerInternalMap.Strength e() {
        return (MapMakerInternalMap.Strength) s92.a(this.d, MapMakerInternalMap.Strength.STRONG);
    }

    public MapMakerInternalMap.Strength f() {
        return (MapMakerInternalMap.Strength) s92.a(this.e, MapMakerInternalMap.Strength.STRONG);
    }

    public MapMaker g(int i) {
        int i2 = this.b;
        au2.r(i2 == -1, "initial capacity was already set to %s", i2);
        au2.d(i >= 0);
        this.b = i;
        return this;
    }

    public MapMaker h(Equivalence<Object> equivalence) {
        Equivalence<Object> equivalence2 = this.f;
        au2.s(equivalence2 == null, "key equivalence was already set to %s", equivalence2);
        this.f = (Equivalence) au2.k(equivalence);
        this.a = true;
        return this;
    }

    public <K, V> ConcurrentMap<K, V> i() {
        if (!this.a) {
            return new ConcurrentHashMap(c(), 0.75f, b());
        }
        return MapMakerInternalMap.create(this);
    }

    public MapMaker j(MapMakerInternalMap.Strength strength) {
        MapMakerInternalMap.Strength strength2 = this.d;
        au2.s(strength2 == null, "Key strength was already set to %s", strength2);
        this.d = (MapMakerInternalMap.Strength) au2.k(strength);
        if (strength != MapMakerInternalMap.Strength.STRONG) {
            this.a = true;
        }
        return this;
    }

    public MapMaker k(MapMakerInternalMap.Strength strength) {
        MapMakerInternalMap.Strength strength2 = this.e;
        au2.s(strength2 == null, "Value strength was already set to %s", strength2);
        this.e = (MapMakerInternalMap.Strength) au2.k(strength);
        if (strength != MapMakerInternalMap.Strength.STRONG) {
            this.a = true;
        }
        return this;
    }

    public MapMaker l() {
        return j(MapMakerInternalMap.Strength.WEAK);
    }

    public String toString() {
        s92.b b = s92.b(this);
        int i = this.b;
        if (i != -1) {
            b.a("initialCapacity", i);
        }
        int i2 = this.c;
        if (i2 != -1) {
            b.a("concurrencyLevel", i2);
        }
        MapMakerInternalMap.Strength strength = this.d;
        if (strength != null) {
            b.b("keyStrength", ei.e(strength.toString()));
        }
        MapMakerInternalMap.Strength strength2 = this.e;
        if (strength2 != null) {
            b.b("valueStrength", ei.e(strength2.toString()));
        }
        if (this.f != null) {
            b.h("keyEquivalence");
        }
        return b.toString();
    }
}
