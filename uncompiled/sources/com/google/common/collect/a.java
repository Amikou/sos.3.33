package com.google.common.collect;

import com.google.common.collect.Multimaps;
import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/* compiled from: AbstractMultimap.java */
/* loaded from: classes2.dex */
public abstract class a<K, V> implements wa2<K, V> {
    public transient Collection<Map.Entry<K, V>> a;
    public transient Set<K> f0;
    public transient d<K> g0;
    public transient Collection<V> h0;
    public transient Map<K, Collection<V>> i0;

    /* compiled from: AbstractMultimap.java */
    /* renamed from: com.google.common.collect.a$a  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public class C0132a extends Multimaps.a<K, V> {
        public C0132a() {
        }

        @Override // com.google.common.collect.Multimaps.a
        public wa2<K, V> e() {
            return a.this;
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable
        public Iterator<Map.Entry<K, V>> iterator() {
            return a.this.entryIterator();
        }
    }

    /* compiled from: AbstractMultimap.java */
    /* loaded from: classes2.dex */
    public class b extends a<K, V>.C0132a implements Set<Map.Entry<K, V>> {
        public b(a aVar) {
            super();
        }

        @Override // java.util.Collection, java.util.Set
        public boolean equals(Object obj) {
            return h.a(this, obj);
        }

        @Override // java.util.Collection, java.util.Set
        public int hashCode() {
            return h.d(this);
        }
    }

    /* compiled from: AbstractMultimap.java */
    /* loaded from: classes2.dex */
    public class c extends AbstractCollection<V> {
        public c() {
        }

        @Override // java.util.AbstractCollection, java.util.Collection
        public void clear() {
            a.this.clear();
        }

        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean contains(Object obj) {
            return a.this.containsValue(obj);
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable
        public Iterator<V> iterator() {
            return a.this.valueIterator();
        }

        @Override // java.util.AbstractCollection, java.util.Collection
        public int size() {
            return a.this.size();
        }
    }

    @Override // defpackage.wa2
    public Map<K, Collection<V>> asMap() {
        Map<K, Collection<V>> map = this.i0;
        if (map == null) {
            Map<K, Collection<V>> createAsMap = createAsMap();
            this.i0 = createAsMap;
            return createAsMap;
        }
        return map;
    }

    @Override // defpackage.wa2
    public boolean containsEntry(Object obj, Object obj2) {
        Collection<V> collection = asMap().get(obj);
        return collection != null && collection.contains(obj2);
    }

    public boolean containsValue(Object obj) {
        for (Collection<V> collection : asMap().values()) {
            if (collection.contains(obj)) {
                return true;
            }
        }
        return false;
    }

    public abstract Map<K, Collection<V>> createAsMap();

    public abstract Collection<Map.Entry<K, V>> createEntries();

    public abstract Set<K> createKeySet();

    public abstract d<K> createKeys();

    public abstract Collection<V> createValues();

    @Override // defpackage.wa2
    public Collection<Map.Entry<K, V>> entries() {
        Collection<Map.Entry<K, V>> collection = this.a;
        if (collection == null) {
            Collection<Map.Entry<K, V>> createEntries = createEntries();
            this.a = createEntries;
            return createEntries;
        }
        return collection;
    }

    public abstract Iterator<Map.Entry<K, V>> entryIterator();

    public boolean equals(Object obj) {
        return Multimaps.a(this, obj);
    }

    public int hashCode() {
        return asMap().hashCode();
    }

    @Override // defpackage.wa2
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override // defpackage.wa2
    public Set<K> keySet() {
        Set<K> set = this.f0;
        if (set == null) {
            Set<K> createKeySet = createKeySet();
            this.f0 = createKeySet;
            return createKeySet;
        }
        return set;
    }

    public d<K> keys() {
        d<K> dVar = this.g0;
        if (dVar == null) {
            d<K> createKeys = createKeys();
            this.g0 = createKeys;
            return createKeys;
        }
        return dVar;
    }

    @Override // defpackage.wa2
    public boolean put(K k, V v) {
        return get(k).add(v);
    }

    public boolean putAll(K k, Iterable<? extends V> iterable) {
        au2.k(iterable);
        if (iterable instanceof Collection) {
            Collection<? extends V> collection = (Collection) iterable;
            return !collection.isEmpty() && get(k).addAll(collection);
        }
        Iterator<? extends V> it = iterable.iterator();
        return it.hasNext() && Iterators.a(get(k), it);
    }

    @Override // defpackage.wa2
    public boolean remove(Object obj, Object obj2) {
        Collection<V> collection = asMap().get(obj);
        return collection != null && collection.remove(obj2);
    }

    public Collection<V> replaceValues(K k, Iterable<? extends V> iterable) {
        au2.k(iterable);
        Collection<V> removeAll = removeAll(k);
        putAll(k, iterable);
        return removeAll;
    }

    public String toString() {
        return asMap().toString();
    }

    public Iterator<V> valueIterator() {
        return Maps.p(entries().iterator());
    }

    @Override // defpackage.wa2
    public Collection<V> values() {
        Collection<V> collection = this.h0;
        if (collection == null) {
            Collection<V> createValues = createValues();
            this.h0 = createValues;
            return createValues;
        }
        return collection;
    }

    public boolean putAll(wa2<? extends K, ? extends V> wa2Var) {
        boolean z = false;
        for (Map.Entry<? extends K, ? extends V> entry : wa2Var.entries()) {
            z |= put(entry.getKey(), entry.getValue());
        }
        return z;
    }
}
