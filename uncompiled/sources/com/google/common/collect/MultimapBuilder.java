package com.google.common.collect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/* loaded from: classes2.dex */
public abstract class MultimapBuilder<K0, V0> {

    /* loaded from: classes2.dex */
    public static final class ArrayListSupplier<V> implements dw3<List<V>>, Serializable {
        private final int expectedValuesPerKey;

        public ArrayListSupplier(int i) {
            this.expectedValuesPerKey = o00.b(i, "expectedValuesPerKey");
        }

        @Override // defpackage.dw3
        public List<V> get() {
            return new ArrayList(this.expectedValuesPerKey);
        }
    }

    /* loaded from: classes2.dex */
    public class a extends d<Object> {
        public final /* synthetic */ int a;

        public a(int i) {
            this.a = i;
        }

        @Override // com.google.common.collect.MultimapBuilder.d
        public <K, V> Map<K, Collection<V>> c() {
            return ar2.c(this.a);
        }
    }

    /* loaded from: classes2.dex */
    public class b extends d<K0> {
        public final /* synthetic */ Comparator a;

        public b(Comparator comparator) {
            this.a = comparator;
        }

        @Override // com.google.common.collect.MultimapBuilder.d
        public <K extends K0, V> Map<K, Collection<V>> c() {
            return new TreeMap(this.a);
        }
    }

    /* loaded from: classes2.dex */
    public static abstract class c<K0, V0> extends MultimapBuilder<K0, V0> {
        public c() {
            super(null);
        }

        public abstract <K extends K0, V extends V0> h02<K, V> e();
    }

    /* loaded from: classes2.dex */
    public static abstract class d<K0> {

        /* loaded from: classes2.dex */
        public class a extends c<K0, Object> {
            public final /* synthetic */ int a;

            public a(int i) {
                this.a = i;
            }

            @Override // com.google.common.collect.MultimapBuilder.c
            public <K extends K0, V> h02<K, V> e() {
                return Multimaps.b(d.this.c(), new ArrayListSupplier(this.a));
            }
        }

        public c<K0, Object> a() {
            return b(2);
        }

        public c<K0, Object> b(int i) {
            o00.b(i, "expectedValuesPerKey");
            return new a(i);
        }

        public abstract <K extends K0, V> Map<K, Collection<V>> c();
    }

    public /* synthetic */ MultimapBuilder(a aVar) {
        this();
    }

    public static d<Object> a() {
        return b(8);
    }

    public static d<Object> b(int i) {
        o00.b(i, "expectedKeys");
        return new a(i);
    }

    public static d<Comparable> c() {
        return d(Ordering.natural());
    }

    public static <K0> d<K0> d(Comparator<K0> comparator) {
        au2.k(comparator);
        return new b(comparator);
    }

    public MultimapBuilder() {
    }
}
