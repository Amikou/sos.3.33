package com.google.common.collect;

import com.google.common.primitives.Ints;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.AbstractSet;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Set;

/* loaded from: classes2.dex */
public class CompactHashSet<E> extends AbstractSet<E> implements Serializable {
    public static final double HASH_FLOODING_FPP = 0.001d;
    public transient Object a;
    public transient Object[] elements;
    public transient int[] f0;
    public transient int g0;
    public transient int h0;

    /* loaded from: classes2.dex */
    public class a implements Iterator<E> {
        public int a;
        public int f0;
        public int g0 = -1;

        public a() {
            this.a = CompactHashSet.this.g0;
            this.f0 = CompactHashSet.this.firstEntryIndex();
        }

        public final void a() {
            if (CompactHashSet.this.g0 != this.a) {
                throw new ConcurrentModificationException();
            }
        }

        public void b() {
            this.a += 32;
        }

        @Override // java.util.Iterator
        public boolean hasNext() {
            return this.f0 >= 0;
        }

        @Override // java.util.Iterator
        public E next() {
            a();
            if (hasNext()) {
                int i = this.f0;
                this.g0 = i;
                E e = (E) CompactHashSet.this.i(i);
                this.f0 = CompactHashSet.this.getSuccessor(this.f0);
                return e;
            }
            throw new NoSuchElementException();
        }

        @Override // java.util.Iterator
        public void remove() {
            a();
            o00.d(this.g0 >= 0);
            b();
            CompactHashSet compactHashSet = CompactHashSet.this;
            compactHashSet.remove(compactHashSet.i(this.g0));
            this.f0 = CompactHashSet.this.adjustAfterRemove(this.f0, this.g0);
            this.g0 = -1;
        }
    }

    public CompactHashSet() {
        init(3);
    }

    public static <E> CompactHashSet<E> create() {
        return new CompactHashSet<>();
    }

    public static <E> CompactHashSet<E> createWithExpectedSize(int i) {
        return new CompactHashSet<>(i);
    }

    /* JADX WARN: Multi-variable type inference failed */
    private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        int readInt = objectInputStream.readInt();
        if (readInt >= 0) {
            init(readInt);
            for (int i = 0; i < readInt; i++) {
                add(objectInputStream.readObject());
            }
            return;
        }
        StringBuilder sb = new StringBuilder(25);
        sb.append("Invalid size: ");
        sb.append(readInt);
        throw new InvalidObjectException(sb.toString());
    }

    private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeInt(size());
        Iterator<E> it = iterator();
        while (it.hasNext()) {
            objectOutputStream.writeObject(it.next());
        }
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public boolean add(E e) {
        if (needsAllocArrays()) {
            allocArrays();
        }
        Set<E> delegateOrNull = delegateOrNull();
        if (delegateOrNull != null) {
            return delegateOrNull.add(e);
        }
        int[] o = o();
        Object[] n = n();
        int i = this.h0;
        int i2 = i + 1;
        int c = dk1.c(e);
        int m = m();
        int i3 = c & m;
        int h = j30.h(p(), i3);
        if (h != 0) {
            int b = j30.b(c, m);
            int i4 = 0;
            while (true) {
                int i5 = h - 1;
                int i6 = o[i5];
                if (j30.b(i6, m) == b && ql2.a(e, n[i5])) {
                    return false;
                }
                int c2 = j30.c(i6, m);
                i4++;
                if (c2 != 0) {
                    h = c2;
                } else if (i4 >= 9) {
                    return convertToHashFloodingResistantImplementation().add(e);
                } else {
                    if (i2 > m) {
                        m = s(m, j30.e(m), c, i);
                    } else {
                        o[i5] = j30.d(i6, i2, m);
                    }
                }
            }
        } else if (i2 > m) {
            m = s(m, j30.e(m), c, i);
        } else {
            j30.i(p(), i3, i2);
        }
        q(i2);
        insertEntry(i, e, c, m);
        this.h0 = i2;
        incrementModCount();
        return true;
    }

    public int adjustAfterRemove(int i, int i2) {
        return i - 1;
    }

    public int allocArrays() {
        au2.q(needsAllocArrays(), "Arrays already allocated");
        int i = this.g0;
        int j = j30.j(i);
        this.a = j30.a(j);
        y(j - 1);
        this.f0 = new int[i];
        this.elements = new Object[i];
        return i;
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public void clear() {
        if (needsAllocArrays()) {
            return;
        }
        incrementModCount();
        Set<E> delegateOrNull = delegateOrNull();
        if (delegateOrNull != null) {
            this.g0 = Ints.f(size(), 3, 1073741823);
            delegateOrNull.clear();
            this.a = null;
            this.h0 = 0;
            return;
        }
        Arrays.fill(n(), 0, this.h0, (Object) null);
        j30.g(p());
        Arrays.fill(o(), 0, this.h0, 0);
        this.h0 = 0;
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public boolean contains(Object obj) {
        if (needsAllocArrays()) {
            return false;
        }
        Set<E> delegateOrNull = delegateOrNull();
        if (delegateOrNull != null) {
            return delegateOrNull.contains(obj);
        }
        int c = dk1.c(obj);
        int m = m();
        int h = j30.h(p(), c & m);
        if (h == 0) {
            return false;
        }
        int b = j30.b(c, m);
        do {
            int i = h - 1;
            int k = k(i);
            if (j30.b(k, m) == b && ql2.a(obj, i(i))) {
                return true;
            }
            h = j30.c(k, m);
        } while (h != 0);
        return false;
    }

    public Set<E> convertToHashFloodingResistantImplementation() {
        Set<E> e = e(m() + 1);
        int firstEntryIndex = firstEntryIndex();
        while (firstEntryIndex >= 0) {
            e.add(i(firstEntryIndex));
            firstEntryIndex = getSuccessor(firstEntryIndex);
        }
        this.a = e;
        this.f0 = null;
        this.elements = null;
        incrementModCount();
        return e;
    }

    public Set<E> delegateOrNull() {
        Object obj = this.a;
        if (obj instanceof Set) {
            return (Set) obj;
        }
        return null;
    }

    public final Set<E> e(int i) {
        return new LinkedHashSet(i, 1.0f);
    }

    public int firstEntryIndex() {
        return isEmpty() ? -1 : 0;
    }

    public int getSuccessor(int i) {
        int i2 = i + 1;
        if (i2 < this.h0) {
            return i2;
        }
        return -1;
    }

    public final E i(int i) {
        return (E) n()[i];
    }

    public void incrementModCount() {
        this.g0 += 32;
    }

    public void init(int i) {
        au2.e(i >= 0, "Expected size must be >= 0");
        this.g0 = Ints.f(i, 1, 1073741823);
    }

    public void insertEntry(int i, E e, int i2, int i3) {
        w(i, j30.d(i2, 0, i3));
        t(i, e);
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public boolean isEmpty() {
        return size() == 0;
    }

    public boolean isUsingHashFloodingResistance() {
        return delegateOrNull() != null;
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable, java.util.Set
    public Iterator<E> iterator() {
        Set<E> delegateOrNull = delegateOrNull();
        if (delegateOrNull != null) {
            return delegateOrNull.iterator();
        }
        return new a();
    }

    public final int k(int i) {
        return o()[i];
    }

    public final int m() {
        return (1 << (this.g0 & 31)) - 1;
    }

    public void moveLastEntry(int i, int i2) {
        Object p = p();
        int[] o = o();
        Object[] n = n();
        int size = size() - 1;
        if (i < size) {
            Object obj = n[size];
            n[i] = obj;
            n[size] = null;
            o[i] = o[size];
            o[size] = 0;
            int c = dk1.c(obj) & i2;
            int h = j30.h(p, c);
            int i3 = size + 1;
            if (h == i3) {
                j30.i(p, c, i + 1);
                return;
            }
            while (true) {
                int i4 = h - 1;
                int i5 = o[i4];
                int c2 = j30.c(i5, i2);
                if (c2 == i3) {
                    o[i4] = j30.d(i5, i + 1, i2);
                    return;
                }
                h = c2;
            }
        } else {
            n[i] = null;
            o[i] = 0;
        }
    }

    public final Object[] n() {
        Object[] objArr = this.elements;
        Objects.requireNonNull(objArr);
        return objArr;
    }

    public boolean needsAllocArrays() {
        return this.a == null;
    }

    public final int[] o() {
        int[] iArr = this.f0;
        Objects.requireNonNull(iArr);
        return iArr;
    }

    public final Object p() {
        Object obj = this.a;
        Objects.requireNonNull(obj);
        return obj;
    }

    public final void q(int i) {
        int min;
        int length = o().length;
        if (i <= length || (min = Math.min(1073741823, (Math.max(1, length >>> 1) + length) | 1)) == length) {
            return;
        }
        resizeEntries(min);
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public boolean remove(Object obj) {
        if (needsAllocArrays()) {
            return false;
        }
        Set<E> delegateOrNull = delegateOrNull();
        if (delegateOrNull != null) {
            return delegateOrNull.remove(obj);
        }
        int m = m();
        int f = j30.f(obj, null, m, p(), o(), n(), null);
        if (f == -1) {
            return false;
        }
        moveLastEntry(f, m);
        this.h0--;
        incrementModCount();
        return true;
    }

    public void resizeEntries(int i) {
        this.f0 = Arrays.copyOf(o(), i);
        this.elements = Arrays.copyOf(n(), i);
    }

    public final int s(int i, int i2, int i3, int i4) {
        Object a2 = j30.a(i2);
        int i5 = i2 - 1;
        if (i4 != 0) {
            j30.i(a2, i3 & i5, i4 + 1);
        }
        Object p = p();
        int[] o = o();
        for (int i6 = 0; i6 <= i; i6++) {
            int h = j30.h(p, i6);
            while (h != 0) {
                int i7 = h - 1;
                int i8 = o[i7];
                int b = j30.b(i8, i) | i6;
                int i9 = b & i5;
                int h2 = j30.h(a2, i9);
                j30.i(a2, i9, h);
                o[i7] = j30.d(b, h2, i5);
                h = j30.c(i8, i);
            }
        }
        this.a = a2;
        y(i5);
        return i5;
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public int size() {
        Set<E> delegateOrNull = delegateOrNull();
        return delegateOrNull != null ? delegateOrNull.size() : this.h0;
    }

    public final void t(int i, E e) {
        n()[i] = e;
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public Object[] toArray() {
        if (needsAllocArrays()) {
            return new Object[0];
        }
        Set<E> delegateOrNull = delegateOrNull();
        return delegateOrNull != null ? delegateOrNull.toArray() : Arrays.copyOf(n(), this.h0);
    }

    public void trimToSize() {
        if (needsAllocArrays()) {
            return;
        }
        Set<E> delegateOrNull = delegateOrNull();
        if (delegateOrNull != null) {
            Set<E> e = e(size());
            e.addAll(delegateOrNull);
            this.a = e;
            return;
        }
        int i = this.h0;
        if (i < o().length) {
            resizeEntries(i);
        }
        int j = j30.j(i);
        int m = m();
        if (j < m) {
            s(m, j, 0, 0);
        }
    }

    public final void w(int i, int i2) {
        o()[i] = i2;
    }

    public final void y(int i) {
        this.g0 = j30.d(this.g0, 32 - Integer.numberOfLeadingZeros(i), 31);
    }

    public static <E> CompactHashSet<E> create(Collection<? extends E> collection) {
        CompactHashSet<E> createWithExpectedSize = createWithExpectedSize(collection.size());
        createWithExpectedSize.addAll(collection);
        return createWithExpectedSize;
    }

    public CompactHashSet(int i) {
        init(i);
    }

    public static <E> CompactHashSet<E> create(E... eArr) {
        CompactHashSet<E> createWithExpectedSize = createWithExpectedSize(eArr.length);
        Collections.addAll(createWithExpectedSize, eArr);
        return createWithExpectedSize;
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public <T> T[] toArray(T[] tArr) {
        if (needsAllocArrays()) {
            if (tArr.length > 0) {
                tArr[0] = null;
            }
            return tArr;
        }
        Set<E> delegateOrNull = delegateOrNull();
        if (delegateOrNull != null) {
            return (T[]) delegateOrNull.toArray(tArr);
        }
        return (T[]) fl2.e(n(), 0, this.h0, tArr);
    }
}
