package com.google.common.collect;

import com.google.common.collect.d;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Map;

/* compiled from: Serialization.java */
/* loaded from: classes2.dex */
public final class f {

    /* compiled from: Serialization.java */
    /* loaded from: classes2.dex */
    public static final class b<T> {
        public final Field a;

        public void a(T t, int i) {
            try {
                this.a.set(t, Integer.valueOf(i));
            } catch (IllegalAccessException e) {
                throw new AssertionError(e);
            }
        }

        public void b(T t, Object obj) {
            try {
                this.a.set(t, obj);
            } catch (IllegalAccessException e) {
                throw new AssertionError(e);
            }
        }

        public b(Field field) {
            this.a = field;
            field.setAccessible(true);
        }
    }

    public static <T> b<T> a(Class<T> cls, String str) {
        try {
            return new b<>(cls.getDeclaredField(str));
        } catch (NoSuchFieldException e) {
            throw new AssertionError(e);
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static <E> void b(d<E> dVar, ObjectInputStream objectInputStream, int i) throws IOException, ClassNotFoundException {
        for (int i2 = 0; i2 < i; i2++) {
            dVar.add(objectInputStream.readObject(), objectInputStream.readInt());
        }
    }

    public static int c(ObjectInputStream objectInputStream) throws IOException {
        return objectInputStream.readInt();
    }

    public static <K, V> void d(wa2<K, V> wa2Var, ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.writeInt(wa2Var.asMap().size());
        for (Map.Entry<K, Collection<V>> entry : wa2Var.asMap().entrySet()) {
            objectOutputStream.writeObject(entry.getKey());
            objectOutputStream.writeInt(entry.getValue().size());
            for (V v : entry.getValue()) {
                objectOutputStream.writeObject(v);
            }
        }
    }

    public static <E> void e(d<E> dVar, ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.writeInt(dVar.entrySet().size());
        for (d.a<E> aVar : dVar.entrySet()) {
            objectOutputStream.writeObject(aVar.getElement());
            objectOutputStream.writeInt(aVar.getCount());
        }
    }
}
