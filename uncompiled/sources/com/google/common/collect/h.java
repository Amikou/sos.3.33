package com.google.common.collect;

import com.google.common.base.Predicates;
import com.google.common.collect.c;
import java.util.AbstractSet;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.SortedSet;

/* compiled from: Sets.java */
/* loaded from: classes2.dex */
public final class h {

    /* compiled from: Sets.java */
    /* loaded from: classes2.dex */
    public class a extends e<E> {
        public final /* synthetic */ Set a;
        public final /* synthetic */ Set f0;

        /* compiled from: Sets.java */
        /* renamed from: com.google.common.collect.h$a$a  reason: collision with other inner class name */
        /* loaded from: classes2.dex */
        public class C0134a extends AbstractIterator<E> {
            public final Iterator<E> g0;

            public C0134a() {
                this.g0 = a.this.a.iterator();
            }

            /* JADX WARN: Multi-variable type inference failed */
            /* JADX WARN: Type inference failed for: r0v4, types: [E, java.lang.Object] */
            @Override // com.google.common.collect.AbstractIterator
            public E a() {
                while (this.g0.hasNext()) {
                    ?? next = this.g0.next();
                    if (a.this.f0.contains(next)) {
                        return next;
                    }
                }
                return b();
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(Set set, Set set2) {
            super(null);
            this.a = set;
            this.f0 = set2;
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean contains(Object obj) {
            return this.a.contains(obj) && this.f0.contains(obj);
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean containsAll(Collection<?> collection) {
            return this.a.containsAll(collection) && this.f0.containsAll(collection);
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable, java.util.Set
        /* renamed from: e */
        public af4<E> iterator() {
            return new C0134a();
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean isEmpty() {
            return Collections.disjoint(this.f0, this.a);
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public int size() {
            int i = 0;
            for (Object obj : this.a) {
                if (this.f0.contains(obj)) {
                    i++;
                }
            }
            return i;
        }
    }

    /* compiled from: Sets.java */
    /* loaded from: classes2.dex */
    public static class b<E> extends c.a<E> implements Set<E> {
        public b(Set<E> set, gu2<? super E> gu2Var) {
            super(set, gu2Var);
        }

        @Override // java.util.Collection, java.util.Set
        public boolean equals(Object obj) {
            return h.a(this, obj);
        }

        @Override // java.util.Collection, java.util.Set
        public int hashCode() {
            return h.d(this);
        }
    }

    /* compiled from: Sets.java */
    /* loaded from: classes2.dex */
    public static class c<E> extends b<E> implements SortedSet<E> {
        public c(SortedSet<E> sortedSet, gu2<? super E> gu2Var) {
            super(sortedSet, gu2Var);
        }

        @Override // java.util.SortedSet
        public Comparator<? super E> comparator() {
            return ((SortedSet) this.a).comparator();
        }

        @Override // java.util.SortedSet
        public E first() {
            return (E) Iterators.j(this.a.iterator(), this.f0);
        }

        @Override // java.util.SortedSet
        public SortedSet<E> headSet(E e) {
            return new c(((SortedSet) this.a).headSet(e), this.f0);
        }

        /* JADX WARN: Type inference failed for: r1v0, types: [E, java.lang.Object] */
        @Override // java.util.SortedSet
        public E last() {
            SortedSet sortedSet = (SortedSet) this.a;
            while (true) {
                ?? r1 = (Object) sortedSet.last();
                if (this.f0.apply(r1)) {
                    return r1;
                }
                sortedSet = sortedSet.headSet(r1);
            }
        }

        @Override // java.util.SortedSet
        public SortedSet<E> subSet(E e, E e2) {
            return new c(((SortedSet) this.a).subSet(e, e2), this.f0);
        }

        @Override // java.util.SortedSet
        public SortedSet<E> tailSet(E e) {
            return new c(((SortedSet) this.a).tailSet(e), this.f0);
        }
    }

    /* compiled from: Sets.java */
    /* loaded from: classes2.dex */
    public static abstract class d<E> extends AbstractSet<E> {
        @Override // java.util.AbstractSet, java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean removeAll(Collection<?> collection) {
            return h.i(this, collection);
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean retainAll(Collection<?> collection) {
            return super.retainAll((Collection) au2.k(collection));
        }
    }

    /* compiled from: Sets.java */
    /* loaded from: classes2.dex */
    public static abstract class e<E> extends AbstractSet<E> {
        public /* synthetic */ e(g gVar) {
            this();
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        @Deprecated
        public final boolean add(E e) {
            throw new UnsupportedOperationException();
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        @Deprecated
        public final boolean addAll(Collection<? extends E> collection) {
            throw new UnsupportedOperationException();
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        @Deprecated
        public final void clear() {
            throw new UnsupportedOperationException();
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        @Deprecated
        public final boolean remove(Object obj) {
            throw new UnsupportedOperationException();
        }

        @Override // java.util.AbstractSet, java.util.AbstractCollection, java.util.Collection, java.util.Set
        @Deprecated
        public final boolean removeAll(Collection<?> collection) {
            throw new UnsupportedOperationException();
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        @Deprecated
        public final boolean retainAll(Collection<?> collection) {
            throw new UnsupportedOperationException();
        }

        public e() {
        }
    }

    public static boolean a(Set<?> set, Object obj) {
        if (set == obj) {
            return true;
        }
        if (obj instanceof Set) {
            Set set2 = (Set) obj;
            try {
                if (set.size() == set2.size()) {
                    if (set.containsAll(set2)) {
                        return true;
                    }
                }
                return false;
            } catch (ClassCastException | NullPointerException unused) {
            }
        }
        return false;
    }

    public static <E> Set<E> b(Set<E> set, gu2<? super E> gu2Var) {
        if (set instanceof SortedSet) {
            return c((SortedSet) set, gu2Var);
        }
        if (set instanceof b) {
            b bVar = (b) set;
            return new b((Set) bVar.a, Predicates.b(bVar.f0, gu2Var));
        }
        return new b((Set) au2.k(set), (gu2) au2.k(gu2Var));
    }

    public static <E> SortedSet<E> c(SortedSet<E> sortedSet, gu2<? super E> gu2Var) {
        if (sortedSet instanceof b) {
            b bVar = (b) sortedSet;
            return new c((SortedSet) bVar.a, Predicates.b(bVar.f0, gu2Var));
        }
        return new c((SortedSet) au2.k(sortedSet), (gu2) au2.k(gu2Var));
    }

    public static int d(Set<?> set) {
        Iterator<?> it = set.iterator();
        int i = 0;
        while (it.hasNext()) {
            Object next = it.next();
            i = ~(~(i + (next != null ? next.hashCode() : 0)));
        }
        return i;
    }

    public static <E> e<E> e(Set<E> set, Set<?> set2) {
        au2.l(set, "set1");
        au2.l(set2, "set2");
        return new a(set, set2);
    }

    public static <E> HashSet<E> f() {
        return new HashSet<>();
    }

    public static <E> HashSet<E> g(int i) {
        return new HashSet<>(Maps.a(i));
    }

    public static <E> Set<E> h() {
        return Collections.newSetFromMap(Maps.i());
    }

    public static boolean i(Set<?> set, Collection<?> collection) {
        au2.k(collection);
        if (collection instanceof com.google.common.collect.d) {
            collection = ((com.google.common.collect.d) collection).elementSet();
        }
        if ((collection instanceof Set) && collection.size() > set.size()) {
            return Iterators.o(set.iterator(), collection);
        }
        return j(set, collection.iterator());
    }

    public static boolean j(Set<?> set, Iterator<?> it) {
        boolean z = false;
        while (it.hasNext()) {
            z |= set.remove(it.next());
        }
        return z;
    }
}
