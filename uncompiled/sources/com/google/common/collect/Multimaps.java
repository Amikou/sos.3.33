package com.google.common.collect;

import com.google.common.collect.Multisets;
import com.google.common.collect.d;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* loaded from: classes2.dex */
public final class Multimaps {

    /* loaded from: classes2.dex */
    public static class CustomListMultimap<K, V> extends AbstractListMultimap<K, V> {
        private static final long serialVersionUID = 0;
        public transient dw3<? extends List<V>> factory;

        public CustomListMultimap(Map<K, Collection<V>> map, dw3<? extends List<V>> dw3Var) {
            super(map);
            this.factory = (dw3) au2.k(dw3Var);
        }

        private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
            objectInputStream.defaultReadObject();
            this.factory = (dw3) objectInputStream.readObject();
            setMap((Map) objectInputStream.readObject());
        }

        private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
            objectOutputStream.defaultWriteObject();
            objectOutputStream.writeObject(this.factory);
            objectOutputStream.writeObject(backingMap());
        }

        @Override // com.google.common.collect.AbstractMapBasedMultimap, com.google.common.collect.a
        public Map<K, Collection<V>> createAsMap() {
            return createMaybeNavigableAsMap();
        }

        @Override // com.google.common.collect.AbstractMapBasedMultimap, com.google.common.collect.a
        public Set<K> createKeySet() {
            return createMaybeNavigableKeySet();
        }

        @Override // com.google.common.collect.AbstractListMultimap, com.google.common.collect.AbstractMapBasedMultimap
        public List<V> createCollection() {
            return this.factory.get();
        }
    }

    /* loaded from: classes2.dex */
    public static abstract class a<K, V> extends AbstractCollection<Map.Entry<K, V>> {
        @Override // java.util.AbstractCollection, java.util.Collection
        public void clear() {
            e().clear();
        }

        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean contains(Object obj) {
            if (obj instanceof Map.Entry) {
                Map.Entry entry = (Map.Entry) obj;
                return e().containsEntry(entry.getKey(), entry.getValue());
            }
            return false;
        }

        public abstract wa2<K, V> e();

        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean remove(Object obj) {
            if (obj instanceof Map.Entry) {
                Map.Entry entry = (Map.Entry) obj;
                return e().remove(entry.getKey(), entry.getValue());
            }
            return false;
        }

        @Override // java.util.AbstractCollection, java.util.Collection
        public int size() {
            return e().size();
        }
    }

    /* loaded from: classes2.dex */
    public static class b<K, V> extends com.google.common.collect.b<K> {
        public final wa2<K, V> g0;

        /* loaded from: classes2.dex */
        public class a extends db4<Map.Entry<K, Collection<V>>, d.a<K>> {

            /* renamed from: com.google.common.collect.Multimaps$b$a$a  reason: collision with other inner class name */
            /* loaded from: classes2.dex */
            public class C0131a extends Multisets.a<K> {
                public final /* synthetic */ Map.Entry a;

                public C0131a(a aVar, Map.Entry entry) {
                    this.a = entry;
                }

                @Override // com.google.common.collect.d.a
                public int getCount() {
                    return ((Collection) this.a.getValue()).size();
                }

                @Override // com.google.common.collect.d.a
                public K getElement() {
                    return (K) this.a.getKey();
                }
            }

            public a(b bVar, Iterator it) {
                super(it);
            }

            @Override // defpackage.db4
            /* renamed from: b */
            public d.a<K> a(Map.Entry<K, Collection<V>> entry) {
                return new C0131a(this, entry);
            }
        }

        public b(wa2<K, V> wa2Var) {
            this.g0 = wa2Var;
        }

        @Override // com.google.common.collect.b, java.util.AbstractCollection, java.util.Collection
        public void clear() {
            this.g0.clear();
        }

        @Override // com.google.common.collect.b, java.util.AbstractCollection, java.util.Collection, com.google.common.collect.d
        public boolean contains(Object obj) {
            return this.g0.containsKey(obj);
        }

        @Override // com.google.common.collect.d
        public int count(Object obj) {
            Collection collection = (Collection) Maps.l(this.g0.asMap(), obj);
            if (collection == null) {
                return 0;
            }
            return collection.size();
        }

        @Override // com.google.common.collect.b
        public int distinctElements() {
            return this.g0.asMap().size();
        }

        @Override // com.google.common.collect.b
        public Iterator<K> elementIterator() {
            throw new AssertionError("should never be called");
        }

        @Override // com.google.common.collect.b, com.google.common.collect.d
        public Set<K> elementSet() {
            return this.g0.keySet();
        }

        @Override // com.google.common.collect.b
        public Iterator<d.a<K>> entryIterator() {
            return new a(this, this.g0.asMap().entrySet().iterator());
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable
        public Iterator<K> iterator() {
            return Maps.h(this.g0.entries().iterator());
        }

        @Override // com.google.common.collect.b, com.google.common.collect.d
        public int remove(Object obj, int i) {
            o00.b(i, "occurrences");
            if (i == 0) {
                return count(obj);
            }
            Collection collection = (Collection) Maps.l(this.g0.asMap(), obj);
            if (collection == null) {
                return 0;
            }
            int size = collection.size();
            if (i >= size) {
                collection.clear();
            } else {
                Iterator it = collection.iterator();
                for (int i2 = 0; i2 < i; i2++) {
                    it.next();
                    it.remove();
                }
            }
            return size;
        }

        @Override // java.util.AbstractCollection, java.util.Collection, com.google.common.collect.d
        public int size() {
            return this.g0.size();
        }
    }

    public static boolean a(wa2<?, ?> wa2Var, Object obj) {
        if (obj == wa2Var) {
            return true;
        }
        if (obj instanceof wa2) {
            return wa2Var.asMap().equals(((wa2) obj).asMap());
        }
        return false;
    }

    public static <K, V> h02<K, V> b(Map<K, Collection<V>> map, dw3<? extends List<V>> dw3Var) {
        return new CustomListMultimap(map, dw3Var);
    }
}
