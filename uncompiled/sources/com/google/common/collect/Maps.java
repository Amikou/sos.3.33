package com.google.common.collect;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.h;
import java.util.AbstractCollection;
import java.util.AbstractMap;
import java.util.Collection;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/* loaded from: classes2.dex */
public final class Maps {

    /* loaded from: classes2.dex */
    public enum EntryFunction implements jd1<Map.Entry<?, ?>, Object> {
        KEY { // from class: com.google.common.collect.Maps.EntryFunction.1
            @Override // com.google.common.collect.Maps.EntryFunction, defpackage.jd1
            public Object apply(Map.Entry<?, ?> entry) {
                return entry.getKey();
            }
        },
        VALUE { // from class: com.google.common.collect.Maps.EntryFunction.2
            @Override // com.google.common.collect.Maps.EntryFunction, defpackage.jd1
            public Object apply(Map.Entry<?, ?> entry) {
                return entry.getValue();
            }
        };

        @Override // defpackage.jd1
        public abstract /* synthetic */ T apply(F f);

        /* synthetic */ EntryFunction(a aVar) {
            this();
        }
    }

    /* loaded from: classes2.dex */
    public class a extends db4<Map.Entry<K, V>, K> {
        public a(Iterator it) {
            super(it);
        }

        /* JADX WARN: Type inference failed for: r1v1, types: [java.lang.Object, K] */
        @Override // defpackage.db4
        /* renamed from: b */
        public K a(Map.Entry<K, V> entry) {
            return entry.getKey();
        }
    }

    /* loaded from: classes2.dex */
    public class b extends db4<Map.Entry<K, V>, V> {
        public b(Iterator it) {
            super(it);
        }

        /* JADX WARN: Type inference failed for: r1v1, types: [V, java.lang.Object] */
        @Override // defpackage.db4
        /* renamed from: b */
        public V a(Map.Entry<K, V> entry) {
            return entry.getValue();
        }
    }

    /* loaded from: classes2.dex */
    public static abstract class c<K, V> extends h.d<Map.Entry<K, V>> {
        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public void clear() {
            e().clear();
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public abstract boolean contains(Object obj);

        public abstract Map<K, V> e();

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean isEmpty() {
            return e().isEmpty();
        }

        @Override // com.google.common.collect.h.d, java.util.AbstractSet, java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean removeAll(Collection<?> collection) {
            try {
                return super.removeAll((Collection) au2.k(collection));
            } catch (UnsupportedOperationException unused) {
                return h.j(this, collection.iterator());
            }
        }

        @Override // com.google.common.collect.h.d, java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean retainAll(Collection<?> collection) {
            try {
                return super.retainAll((Collection) au2.k(collection));
            } catch (UnsupportedOperationException unused) {
                HashSet g = h.g(collection.size());
                for (Object obj : collection) {
                    if (contains(obj) && (obj instanceof Map.Entry)) {
                        g.add(((Map.Entry) obj).getKey());
                    }
                }
                return e().keySet().retainAll(g);
            }
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public int size() {
            return e().size();
        }
    }

    /* loaded from: classes2.dex */
    public static class d<K, V> extends h.d<K> {
        public final Map<K, V> a;

        public d(Map<K, V> map) {
            this.a = (Map) au2.k(map);
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean contains(Object obj) {
            return e().containsKey(obj);
        }

        public Map<K, V> e() {
            return this.a;
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean isEmpty() {
            return e().isEmpty();
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public int size() {
            return e().size();
        }
    }

    /* loaded from: classes2.dex */
    public static class e<K, V> extends AbstractCollection<V> {
        public final Map<K, V> a;

        public e(Map<K, V> map) {
            this.a = (Map) au2.k(map);
        }

        @Override // java.util.AbstractCollection, java.util.Collection
        public void clear() {
            e().clear();
        }

        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean contains(Object obj) {
            return e().containsValue(obj);
        }

        public final Map<K, V> e() {
            return this.a;
        }

        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean isEmpty() {
            return e().isEmpty();
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable
        public Iterator<V> iterator() {
            return Maps.p(e().entrySet().iterator());
        }

        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean remove(Object obj) {
            try {
                return super.remove(obj);
            } catch (UnsupportedOperationException unused) {
                for (Map.Entry<K, V> entry : e().entrySet()) {
                    if (ql2.a(obj, entry.getValue())) {
                        e().remove(entry.getKey());
                        return true;
                    }
                }
                return false;
            }
        }

        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean removeAll(Collection<?> collection) {
            try {
                return super.removeAll((Collection) au2.k(collection));
            } catch (UnsupportedOperationException unused) {
                HashSet f = h.f();
                for (Map.Entry<K, V> entry : e().entrySet()) {
                    if (collection.contains(entry.getValue())) {
                        f.add(entry.getKey());
                    }
                }
                return e().keySet().removeAll(f);
            }
        }

        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean retainAll(Collection<?> collection) {
            try {
                return super.retainAll((Collection) au2.k(collection));
            } catch (UnsupportedOperationException unused) {
                HashSet f = h.f();
                for (Map.Entry<K, V> entry : e().entrySet()) {
                    if (collection.contains(entry.getValue())) {
                        f.add(entry.getKey());
                    }
                }
                return e().keySet().retainAll(f);
            }
        }

        @Override // java.util.AbstractCollection, java.util.Collection
        public int size() {
            return e().size();
        }
    }

    /* loaded from: classes2.dex */
    public static abstract class f<K, V> extends AbstractMap<K, V> {
        public transient Set<Map.Entry<K, V>> a;
        public transient Collection<V> f0;

        public abstract Set<Map.Entry<K, V>> a();

        public Collection<V> b() {
            return new e(this);
        }

        @Override // java.util.AbstractMap, java.util.Map
        public Set<Map.Entry<K, V>> entrySet() {
            Set<Map.Entry<K, V>> set = this.a;
            if (set == null) {
                Set<Map.Entry<K, V>> a = a();
                this.a = a;
                return a;
            }
            return set;
        }

        @Override // java.util.AbstractMap, java.util.Map
        public Collection<V> values() {
            Collection<V> collection = this.f0;
            if (collection == null) {
                Collection<V> b = b();
                this.f0 = b;
                return b;
            }
            return collection;
        }
    }

    public static int a(int i) {
        if (i < 3) {
            o00.b(i, "expectedSize");
            return i + 1;
        } else if (i < 1073741824) {
            return (int) ((i / 0.75f) + 1.0f);
        } else {
            return Integer.MAX_VALUE;
        }
    }

    public static boolean b(Map<?, ?> map, Object obj) {
        return Iterators.d(h(map.entrySet().iterator()), obj);
    }

    public static boolean c(Map<?, ?> map, Object obj) {
        return Iterators.d(p(map.entrySet().iterator()), obj);
    }

    public static boolean d(Map<?, ?> map, Object obj) {
        if (map == obj) {
            return true;
        }
        if (obj instanceof Map) {
            return map.entrySet().equals(((Map) obj).entrySet());
        }
        return false;
    }

    public static <K, V> Map.Entry<K, V> e(K k, V v) {
        return new ImmutableEntry(k, v);
    }

    public static <E> ImmutableMap<E, Integer> f(Collection<E> collection) {
        ImmutableMap.b bVar = new ImmutableMap.b(collection.size());
        int i = 0;
        for (E e2 : collection) {
            bVar.d(e2, Integer.valueOf(i));
            i++;
        }
        return bVar.a();
    }

    public static <K> jd1<Map.Entry<K, ?>, K> g() {
        return EntryFunction.KEY;
    }

    public static <K, V> Iterator<K> h(Iterator<Map.Entry<K, V>> it) {
        return new a(it);
    }

    public static <K, V> IdentityHashMap<K, V> i() {
        return new IdentityHashMap<>();
    }

    public static <K, V> void j(Map<K, V> map, Map<? extends K, ? extends V> map2) {
        for (Map.Entry<? extends K, ? extends V> entry : map2.entrySet()) {
            map.put(entry.getKey(), entry.getValue());
        }
    }

    public static boolean k(Map<?, ?> map, Object obj) {
        au2.k(map);
        try {
            return map.containsKey(obj);
        } catch (ClassCastException | NullPointerException unused) {
            return false;
        }
    }

    public static <V> V l(Map<?, V> map, Object obj) {
        au2.k(map);
        try {
            return map.get(obj);
        } catch (ClassCastException | NullPointerException unused) {
            return null;
        }
    }

    public static <V> V m(Map<?, V> map, Object obj) {
        au2.k(map);
        try {
            return map.remove(obj);
        } catch (ClassCastException | NullPointerException unused) {
            return null;
        }
    }

    public static String n(Map<?, ?> map) {
        StringBuilder b2 = com.google.common.collect.c.b(map.size());
        b2.append('{');
        boolean z = true;
        for (Map.Entry<?, ?> entry : map.entrySet()) {
            if (!z) {
                b2.append(", ");
            }
            z = false;
            b2.append(entry.getKey());
            b2.append('=');
            b2.append(entry.getValue());
        }
        b2.append('}');
        return b2.toString();
    }

    public static <V> jd1<Map.Entry<?, V>, V> o() {
        return EntryFunction.VALUE;
    }

    public static <K, V> Iterator<V> p(Iterator<Map.Entry<K, V>> it) {
        return new b(it);
    }
}
