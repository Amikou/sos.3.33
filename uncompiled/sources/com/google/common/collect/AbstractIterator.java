package com.google.common.collect;

import java.util.NoSuchElementException;

/* loaded from: classes2.dex */
public abstract class AbstractIterator<T> extends af4<T> {
    public State a = State.NOT_READY;
    public T f0;

    /* loaded from: classes2.dex */
    public enum State {
        READY,
        NOT_READY,
        DONE,
        FAILED
    }

    /* loaded from: classes2.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[State.values().length];
            a = iArr;
            try {
                iArr[State.DONE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[State.READY.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
        }
    }

    public abstract T a();

    public final T b() {
        this.a = State.DONE;
        return null;
    }

    public final boolean c() {
        this.a = State.FAILED;
        this.f0 = a();
        if (this.a != State.DONE) {
            this.a = State.READY;
            return true;
        }
        return false;
    }

    @Override // java.util.Iterator
    public final boolean hasNext() {
        au2.p(this.a != State.FAILED);
        int i = a.a[this.a.ordinal()];
        if (i != 1) {
            if (i != 2) {
                return c();
            }
            return true;
        }
        return false;
    }

    @Override // java.util.Iterator
    public final T next() {
        if (hasNext()) {
            this.a = State.NOT_READY;
            T t = (T) yi2.a(this.f0);
            this.f0 = null;
            return t;
        }
        throw new NoSuchElementException();
    }
}
