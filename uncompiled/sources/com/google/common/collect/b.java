package com.google.common.collect;

import com.google.common.collect.Multisets;
import com.google.common.collect.d;
import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

/* compiled from: AbstractMultiset.java */
/* loaded from: classes2.dex */
public abstract class b<E> extends AbstractCollection<E> implements d<E> {
    public transient Set<E> a;
    public transient Set<d.a<E>> f0;

    /* compiled from: AbstractMultiset.java */
    /* loaded from: classes2.dex */
    public class a extends Multisets.b<E> {
        public a() {
        }

        @Override // com.google.common.collect.Multisets.b
        public d<E> e() {
            return b.this;
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable, java.util.Set
        public Iterator<E> iterator() {
            return b.this.elementIterator();
        }
    }

    /* compiled from: AbstractMultiset.java */
    /* renamed from: com.google.common.collect.b$b  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public class C0133b extends Multisets.c<E> {
        public C0133b() {
        }

        @Override // com.google.common.collect.Multisets.c
        public d<E> e() {
            return b.this;
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable, java.util.Set
        public Iterator<d.a<E>> iterator() {
            return b.this.entryIterator();
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public int size() {
            return b.this.distinctElements();
        }
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public final boolean add(E e) {
        add(e, 1);
        return true;
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public final boolean addAll(Collection<? extends E> collection) {
        return Multisets.c(this, collection);
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public abstract void clear();

    @Override // java.util.AbstractCollection, java.util.Collection, com.google.common.collect.d
    public boolean contains(Object obj) {
        return count(obj) > 0;
    }

    public Set<E> createElementSet() {
        return new a();
    }

    public Set<d.a<E>> createEntrySet() {
        return new C0133b();
    }

    public abstract int distinctElements();

    public abstract Iterator<E> elementIterator();

    public Set<E> elementSet() {
        Set<E> set = this.a;
        if (set == null) {
            Set<E> createElementSet = createElementSet();
            this.a = createElementSet;
            return createElementSet;
        }
        return set;
    }

    public abstract Iterator<d.a<E>> entryIterator();

    @Override // com.google.common.collect.d
    public Set<d.a<E>> entrySet() {
        Set<d.a<E>> set = this.f0;
        if (set == null) {
            Set<d.a<E>> createEntrySet = createEntrySet();
            this.f0 = createEntrySet;
            return createEntrySet;
        }
        return set;
    }

    @Override // java.util.Collection
    public final boolean equals(Object obj) {
        return Multisets.e(this, obj);
    }

    @Override // java.util.Collection
    public final int hashCode() {
        return entrySet().hashCode();
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public boolean isEmpty() {
        return entrySet().isEmpty();
    }

    @Override // java.util.AbstractCollection, java.util.Collection, com.google.common.collect.d
    public final boolean remove(Object obj) {
        return remove(obj, 1) > 0;
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public final boolean removeAll(Collection<?> collection) {
        return Multisets.i(this, collection);
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public final boolean retainAll(Collection<?> collection) {
        return Multisets.j(this, collection);
    }

    public int setCount(E e, int i) {
        return Multisets.k(this, e, i);
    }

    @Override // java.util.AbstractCollection
    public final String toString() {
        return entrySet().toString();
    }

    public int add(E e, int i) {
        throw new UnsupportedOperationException();
    }

    public int remove(Object obj, int i) {
        throw new UnsupportedOperationException();
    }

    public boolean setCount(E e, int i, int i2) {
        return Multisets.l(this, e, i, i2);
    }
}
