package com.google.common.collect;

import java.util.Collection;
import java.util.Set;

/* compiled from: Multiset.java */
/* loaded from: classes2.dex */
public interface d<E> extends Collection<E> {

    /* compiled from: Multiset.java */
    /* loaded from: classes2.dex */
    public interface a<E> {
        int getCount();

        E getElement();
    }

    int add(E e, int i);

    @Override // java.util.Collection, com.google.common.collect.d
    boolean contains(Object obj);

    @Override // java.util.Collection
    boolean containsAll(Collection<?> collection);

    int count(Object obj);

    Set<E> elementSet();

    Set<a<E>> entrySet();

    int remove(Object obj, int i);

    @Override // java.util.Collection, com.google.common.collect.d
    boolean remove(Object obj);

    int setCount(E e, int i);

    boolean setCount(E e, int i, int i2);

    @Override // java.util.Collection, com.google.common.collect.d
    int size();
}
