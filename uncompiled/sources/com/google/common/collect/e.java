package com.google.common.collect;

import com.github.mikephil.charting.utils.Utils;
import com.google.common.collect.Multisets;
import com.google.common.collect.d;
import java.util.Arrays;

/* compiled from: ObjectCountHashMap.java */
/* loaded from: classes2.dex */
public class e<K> {
    public transient Object[] a;
    public transient int[] b;
    public transient int c;
    public transient int d;
    public transient int[] e;
    public transient long[] f;
    public transient float g;
    public transient int h;

    /* compiled from: ObjectCountHashMap.java */
    /* loaded from: classes2.dex */
    public class a extends Multisets.a<K> {
        public final K a;
        public int f0;

        public a(int i) {
            this.a = (K) e.this.a[i];
            this.f0 = i;
        }

        public void a() {
            int i = this.f0;
            if (i == -1 || i >= e.this.C() || !ql2.a(this.a, e.this.a[this.f0])) {
                this.f0 = e.this.m(this.a);
            }
        }

        @Override // com.google.common.collect.d.a
        public int getCount() {
            a();
            int i = this.f0;
            if (i == -1) {
                return 0;
            }
            return e.this.b[i];
        }

        @Override // com.google.common.collect.d.a
        public K getElement() {
            return this.a;
        }
    }

    public e() {
        n(3, 1.0f);
    }

    public static long D(long j, int i) {
        return (j & (-4294967296L)) | (i & 4294967295L);
    }

    public static <K> e<K> b() {
        return new e<>();
    }

    public static <K> e<K> c(int i) {
        return new e<>(i);
    }

    public static int h(long j) {
        return (int) (j >>> 32);
    }

    public static int j(long j) {
        return (int) j;
    }

    public static long[] q(int i) {
        long[] jArr = new long[i];
        Arrays.fill(jArr, -1L);
        return jArr;
    }

    public static int[] r(int i) {
        int[] iArr = new int[i];
        Arrays.fill(iArr, -1);
        return iArr;
    }

    public final void A(int i) {
        if (this.e.length >= 1073741824) {
            this.h = Integer.MAX_VALUE;
            return;
        }
        int i2 = ((int) (i * this.g)) + 1;
        int[] r = r(i);
        long[] jArr = this.f;
        int length = r.length - 1;
        for (int i3 = 0; i3 < this.c; i3++) {
            int h = h(jArr[i3]);
            int i4 = h & length;
            int i5 = r[i4];
            r[i4] = i3;
            jArr[i3] = (h << 32) | (i5 & 4294967295L);
        }
        this.h = i2;
        this.e = r;
    }

    public void B(int i, int i2) {
        au2.i(i, this.c);
        this.b[i] = i2;
    }

    public int C() {
        return this.c;
    }

    public void a() {
        this.d++;
        Arrays.fill(this.a, 0, this.c, (Object) null);
        Arrays.fill(this.b, 0, this.c, 0);
        Arrays.fill(this.e, -1);
        Arrays.fill(this.f, -1L);
        this.c = 0;
    }

    public void d(int i) {
        if (i > this.f.length) {
            y(i);
        }
        if (i >= this.h) {
            A(Math.max(2, Integer.highestOneBit(i - 1) << 1));
        }
    }

    public int e() {
        return this.c == 0 ? -1 : 0;
    }

    public int f(Object obj) {
        int m = m(obj);
        if (m == -1) {
            return 0;
        }
        return this.b[m];
    }

    public d.a<K> g(int i) {
        au2.i(i, this.c);
        return new a(i);
    }

    public K i(int i) {
        au2.i(i, this.c);
        return (K) this.a[i];
    }

    public int k(int i) {
        au2.i(i, this.c);
        return this.b[i];
    }

    public final int l() {
        return this.e.length - 1;
    }

    public int m(Object obj) {
        int c = dk1.c(obj);
        int i = this.e[l() & c];
        while (i != -1) {
            long j = this.f[i];
            if (h(j) == c && ql2.a(obj, this.a[i])) {
                return i;
            }
            i = j(j);
        }
        return -1;
    }

    public void n(int i, float f) {
        au2.e(i >= 0, "Initial capacity must be non-negative");
        au2.e(f > Utils.FLOAT_EPSILON, "Illegal load factor");
        int a2 = dk1.a(i, f);
        this.e = r(a2);
        this.g = f;
        this.a = new Object[i];
        this.b = new int[i];
        this.f = q(i);
        this.h = Math.max(1, (int) (a2 * f));
    }

    public void o(int i, K k, int i2, int i3) {
        this.f[i] = (i3 << 32) | 4294967295L;
        this.a[i] = k;
        this.b[i] = i2;
    }

    public void p(int i) {
        int C = C() - 1;
        if (i < C) {
            Object[] objArr = this.a;
            objArr[i] = objArr[C];
            int[] iArr = this.b;
            iArr[i] = iArr[C];
            objArr[C] = null;
            iArr[C] = 0;
            long[] jArr = this.f;
            long j = jArr[C];
            jArr[i] = j;
            jArr[C] = -1;
            int h = h(j) & l();
            int[] iArr2 = this.e;
            int i2 = iArr2[h];
            if (i2 == C) {
                iArr2[h] = i;
                return;
            }
            while (true) {
                long j2 = this.f[i2];
                int j3 = j(j2);
                if (j3 == C) {
                    this.f[i2] = D(j2, i);
                    return;
                }
                i2 = j3;
            }
        } else {
            this.a[i] = null;
            this.b[i] = 0;
            this.f[i] = -1;
        }
    }

    public int s(int i) {
        int i2 = i + 1;
        if (i2 < this.c) {
            return i2;
        }
        return -1;
    }

    public int t(int i, int i2) {
        return i - 1;
    }

    public int u(K k, int i) {
        o00.c(i, "count");
        long[] jArr = this.f;
        Object[] objArr = this.a;
        int[] iArr = this.b;
        int c = dk1.c(k);
        int l = l() & c;
        int i2 = this.c;
        int[] iArr2 = this.e;
        int i3 = iArr2[l];
        if (i3 == -1) {
            iArr2[l] = i2;
        } else {
            while (true) {
                long j = jArr[i3];
                if (h(j) == c && ql2.a(k, objArr[i3])) {
                    int i4 = iArr[i3];
                    iArr[i3] = i;
                    return i4;
                }
                int j2 = j(j);
                if (j2 == -1) {
                    jArr[i3] = D(j, i2);
                    break;
                }
                i3 = j2;
            }
        }
        if (i2 != Integer.MAX_VALUE) {
            int i5 = i2 + 1;
            z(i5);
            o(i2, k, i, c);
            this.c = i5;
            if (i2 >= this.h) {
                A(this.e.length * 2);
            }
            this.d++;
            return 0;
        }
        throw new IllegalStateException("Cannot contain more than Integer.MAX_VALUE elements!");
    }

    public int v(Object obj) {
        return w(obj, dk1.c(obj));
    }

    public final int w(Object obj, int i) {
        int l = l() & i;
        int i2 = this.e[l];
        if (i2 == -1) {
            return 0;
        }
        int i3 = -1;
        while (true) {
            if (h(this.f[i2]) == i && ql2.a(obj, this.a[i2])) {
                int i4 = this.b[i2];
                if (i3 == -1) {
                    this.e[l] = j(this.f[i2]);
                } else {
                    long[] jArr = this.f;
                    jArr[i3] = D(jArr[i3], j(jArr[i2]));
                }
                p(i2);
                this.c--;
                this.d++;
                return i4;
            }
            int j = j(this.f[i2]);
            if (j == -1) {
                return 0;
            }
            i3 = i2;
            i2 = j;
        }
    }

    public int x(int i) {
        return w(this.a[i], h(this.f[i]));
    }

    public void y(int i) {
        this.a = Arrays.copyOf(this.a, i);
        this.b = Arrays.copyOf(this.b, i);
        long[] jArr = this.f;
        int length = jArr.length;
        long[] copyOf = Arrays.copyOf(jArr, i);
        if (i > length) {
            Arrays.fill(copyOf, length, i, -1L);
        }
        this.f = copyOf;
    }

    public final void z(int i) {
        int length = this.f.length;
        if (i > length) {
            int max = Math.max(1, length >>> 1) + length;
            if (max < 0) {
                max = Integer.MAX_VALUE;
            }
            if (max != length) {
                y(max);
            }
        }
    }

    public e(e<? extends K> eVar) {
        n(eVar.C(), 1.0f);
        int e = eVar.e();
        while (e != -1) {
            u(eVar.i(e), eVar.k(e));
            e = eVar.s(e);
        }
    }

    public e(int i) {
        this(i, 1.0f);
    }

    public e(int i, float f) {
        n(i, f);
    }
}
