package com.google.common.collect;

import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;

/* loaded from: classes2.dex */
public final class Iterators {

    /* loaded from: classes2.dex */
    public enum EmptyModifiableIterator implements Iterator<Object> {
        INSTANCE;

        @Override // java.util.Iterator
        public boolean hasNext() {
            return false;
        }

        @Override // java.util.Iterator
        public Object next() {
            throw new NoSuchElementException();
        }

        @Override // java.util.Iterator
        public void remove() {
            o00.d(false);
        }
    }

    /* loaded from: classes2.dex */
    public class a extends AbstractIterator<T> {
        public final /* synthetic */ Iterator g0;
        public final /* synthetic */ gu2 h0;

        public a(Iterator it, gu2 gu2Var) {
            this.g0 = it;
            this.h0 = gu2Var;
        }

        /* JADX WARN: Type inference failed for: r0v4, types: [T, java.lang.Object] */
        @Override // com.google.common.collect.AbstractIterator
        public T a() {
            while (this.g0.hasNext()) {
                ?? next = this.g0.next();
                if (this.h0.apply(next)) {
                    return next;
                }
            }
            return b();
        }
    }

    /* loaded from: classes2.dex */
    public class b extends af4<T> {
        public boolean a;
        public final /* synthetic */ Object f0;

        public b(Object obj) {
            this.f0 = obj;
        }

        @Override // java.util.Iterator
        public boolean hasNext() {
            return !this.a;
        }

        /* JADX WARN: Type inference failed for: r0v3, types: [T, java.lang.Object] */
        @Override // java.util.Iterator
        public T next() {
            if (!this.a) {
                this.a = true;
                return this.f0;
            }
            throw new NoSuchElementException();
        }
    }

    /* loaded from: classes2.dex */
    public static final class c<T> extends z4<T> {
        public static final df4<Object> i0 = new c(new Object[0], 0, 0, 0);
        public final T[] g0;
        public final int h0;

        public c(T[] tArr, int i, int i2, int i3) {
            super(i2, i3);
            this.g0 = tArr;
            this.h0 = i;
        }

        @Override // defpackage.z4
        public T a(int i) {
            return this.g0[this.h0 + i];
        }
    }

    public static <T> boolean a(Collection<T> collection, Iterator<? extends T> it) {
        au2.k(collection);
        au2.k(it);
        boolean z = false;
        while (it.hasNext()) {
            z |= collection.add(it.next());
        }
        return z;
    }

    public static <T> boolean b(Iterator<T> it, gu2<? super T> gu2Var) {
        return m(it, gu2Var) != -1;
    }

    public static void c(Iterator<?> it) {
        au2.k(it);
        while (it.hasNext()) {
            it.next();
            it.remove();
        }
    }

    public static boolean d(Iterator<?> it, Object obj) {
        if (obj == null) {
            while (it.hasNext()) {
                if (it.next() == null) {
                    return true;
                }
            }
            return false;
        }
        while (it.hasNext()) {
            if (obj.equals(it.next())) {
                return true;
            }
        }
        return false;
    }

    /* JADX WARN: Removed duplicated region for block: B:4:0x0006  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static boolean e(java.util.Iterator<?> r3, java.util.Iterator<?> r4) {
        /*
        L0:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L1d
            boolean r0 = r4.hasNext()
            r1 = 0
            if (r0 != 0) goto Le
            return r1
        Le:
            java.lang.Object r0 = r3.next()
            java.lang.Object r2 = r4.next()
            boolean r0 = defpackage.ql2.a(r0, r2)
            if (r0 != 0) goto L0
            return r1
        L1d:
            boolean r3 = r4.hasNext()
            r3 = r3 ^ 1
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.common.collect.Iterators.e(java.util.Iterator, java.util.Iterator):boolean");
    }

    public static <T> af4<T> f() {
        return g();
    }

    public static <T> df4<T> g() {
        return (df4<T>) c.i0;
    }

    public static <T> Iterator<T> h() {
        return EmptyModifiableIterator.INSTANCE;
    }

    public static <T> af4<T> i(Iterator<T> it, gu2<? super T> gu2Var) {
        au2.k(it);
        au2.k(gu2Var);
        return new a(it, gu2Var);
    }

    public static <T> T j(Iterator<T> it, gu2<? super T> gu2Var) {
        au2.k(it);
        au2.k(gu2Var);
        while (it.hasNext()) {
            T next = it.next();
            if (gu2Var.apply(next)) {
                return next;
            }
        }
        throw new NoSuchElementException();
    }

    public static <T> T k(Iterator<T> it) {
        T next;
        do {
            next = it.next();
        } while (it.hasNext());
        return next;
    }

    public static <T> T l(Iterator<? extends T> it, T t) {
        return it.hasNext() ? it.next() : t;
    }

    public static <T> int m(Iterator<T> it, gu2<? super T> gu2Var) {
        au2.l(gu2Var, "predicate");
        int i = 0;
        while (it.hasNext()) {
            if (gu2Var.apply(it.next())) {
                return i;
            }
            i++;
        }
        return -1;
    }

    public static <T> T n(Iterator<T> it) {
        if (it.hasNext()) {
            T next = it.next();
            it.remove();
            return next;
        }
        return null;
    }

    public static boolean o(Iterator<?> it, Collection<?> collection) {
        au2.k(collection);
        boolean z = false;
        while (it.hasNext()) {
            if (collection.contains(it.next())) {
                it.remove();
                z = true;
            }
        }
        return z;
    }

    public static <T> boolean p(Iterator<T> it, gu2<? super T> gu2Var) {
        au2.k(gu2Var);
        boolean z = false;
        while (it.hasNext()) {
            if (gu2Var.apply(it.next())) {
                it.remove();
                z = true;
            }
        }
        return z;
    }

    public static <T> af4<T> q(T t) {
        return new b(t);
    }
}
