package com.google.common.collect;

import com.google.common.collect.d;
import com.google.common.collect.h;
import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;

/* loaded from: classes2.dex */
public final class Multisets {

    /* loaded from: classes2.dex */
    public static class ImmutableEntry<E> extends a<E> implements Serializable {
        private static final long serialVersionUID = 0;
        private final int count;
        private final E element;

        public ImmutableEntry(E e, int i) {
            this.element = e;
            this.count = i;
            o00.b(i, "count");
        }

        @Override // com.google.common.collect.d.a
        public final int getCount() {
            return this.count;
        }

        @Override // com.google.common.collect.d.a
        public final E getElement() {
            return this.element;
        }

        public ImmutableEntry<E> nextInBucket() {
            return null;
        }
    }

    /* loaded from: classes2.dex */
    public static abstract class a<E> implements d.a<E> {
        public boolean equals(Object obj) {
            if (obj instanceof d.a) {
                d.a aVar = (d.a) obj;
                return getCount() == aVar.getCount() && ql2.a(getElement(), aVar.getElement());
            }
            return false;
        }

        public int hashCode() {
            E element = getElement();
            return (element == null ? 0 : element.hashCode()) ^ getCount();
        }

        public String toString() {
            String valueOf = String.valueOf(getElement());
            int count = getCount();
            if (count == 1) {
                return valueOf;
            }
            StringBuilder sb = new StringBuilder(valueOf.length() + 14);
            sb.append(valueOf);
            sb.append(" x ");
            sb.append(count);
            return sb.toString();
        }
    }

    /* loaded from: classes2.dex */
    public static abstract class b<E> extends h.d<E> {
        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public void clear() {
            e().clear();
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean contains(Object obj) {
            return e().contains(obj);
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean containsAll(Collection<?> collection) {
            return e().containsAll(collection);
        }

        public abstract com.google.common.collect.d<E> e();

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean isEmpty() {
            return e().isEmpty();
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean remove(Object obj) {
            return e().remove(obj, Integer.MAX_VALUE) > 0;
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public int size() {
            return e().entrySet().size();
        }
    }

    /* loaded from: classes2.dex */
    public static abstract class c<E> extends h.d<d.a<E>> {
        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public void clear() {
            e().clear();
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean contains(Object obj) {
            if (obj instanceof d.a) {
                d.a aVar = (d.a) obj;
                return aVar.getCount() > 0 && e().count(aVar.getElement()) == aVar.getCount();
            }
            return false;
        }

        public abstract com.google.common.collect.d<E> e();

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean remove(Object obj) {
            if (obj instanceof d.a) {
                d.a aVar = (d.a) obj;
                E e = (E) aVar.getElement();
                int count = aVar.getCount();
                if (count != 0) {
                    return e().setCount(e, count, 0);
                }
            }
            return false;
        }
    }

    /* loaded from: classes2.dex */
    public static final class d<E> implements Iterator<E> {
        public final com.google.common.collect.d<E> a;
        public final Iterator<d.a<E>> f0;
        public d.a<E> g0;
        public int h0;
        public int i0;
        public boolean j0;

        public d(com.google.common.collect.d<E> dVar, Iterator<d.a<E>> it) {
            this.a = dVar;
            this.f0 = it;
        }

        @Override // java.util.Iterator
        public boolean hasNext() {
            return this.h0 > 0 || this.f0.hasNext();
        }

        @Override // java.util.Iterator
        public E next() {
            if (hasNext()) {
                if (this.h0 == 0) {
                    d.a<E> next = this.f0.next();
                    this.g0 = next;
                    int count = next.getCount();
                    this.h0 = count;
                    this.i0 = count;
                }
                this.h0--;
                this.j0 = true;
                d.a<E> aVar = this.g0;
                Objects.requireNonNull(aVar);
                return aVar.getElement();
            }
            throw new NoSuchElementException();
        }

        @Override // java.util.Iterator
        public void remove() {
            o00.d(this.j0);
            if (this.i0 == 1) {
                this.f0.remove();
            } else {
                com.google.common.collect.d<E> dVar = this.a;
                d.a<E> aVar = this.g0;
                Objects.requireNonNull(aVar);
                dVar.remove(aVar.getElement());
            }
            this.i0--;
            this.j0 = false;
        }
    }

    public static <E> boolean a(com.google.common.collect.d<E> dVar, AbstractMapBasedMultiset<? extends E> abstractMapBasedMultiset) {
        if (abstractMapBasedMultiset.isEmpty()) {
            return false;
        }
        abstractMapBasedMultiset.addTo(dVar);
        return true;
    }

    public static <E> boolean b(com.google.common.collect.d<E> dVar, com.google.common.collect.d<? extends E> dVar2) {
        if (dVar2 instanceof AbstractMapBasedMultiset) {
            return a(dVar, (AbstractMapBasedMultiset) dVar2);
        }
        if (dVar2.isEmpty()) {
            return false;
        }
        for (d.a<? extends E> aVar : dVar2.entrySet()) {
            dVar.add(aVar.getElement(), aVar.getCount());
        }
        return true;
    }

    public static <E> boolean c(com.google.common.collect.d<E> dVar, Collection<? extends E> collection) {
        au2.k(dVar);
        au2.k(collection);
        if (collection instanceof com.google.common.collect.d) {
            return b(dVar, d(collection));
        }
        if (collection.isEmpty()) {
            return false;
        }
        return Iterators.a(dVar, collection.iterator());
    }

    public static <T> com.google.common.collect.d<T> d(Iterable<T> iterable) {
        return (com.google.common.collect.d) iterable;
    }

    public static boolean e(com.google.common.collect.d<?> dVar, Object obj) {
        if (obj == dVar) {
            return true;
        }
        if (obj instanceof com.google.common.collect.d) {
            com.google.common.collect.d dVar2 = (com.google.common.collect.d) obj;
            if (dVar.size() == dVar2.size() && dVar.entrySet().size() == dVar2.entrySet().size()) {
                for (d.a aVar : dVar2.entrySet()) {
                    if (dVar.count(aVar.getElement()) != aVar.getCount()) {
                        return false;
                    }
                }
                return true;
            }
        }
        return false;
    }

    public static <E> d.a<E> f(E e, int i) {
        return new ImmutableEntry(e, i);
    }

    public static int g(Iterable<?> iterable) {
        if (iterable instanceof com.google.common.collect.d) {
            return ((com.google.common.collect.d) iterable).elementSet().size();
        }
        return 11;
    }

    public static <E> Iterator<E> h(com.google.common.collect.d<E> dVar) {
        return new d(dVar, dVar.entrySet().iterator());
    }

    public static boolean i(com.google.common.collect.d<?> dVar, Collection<?> collection) {
        if (collection instanceof com.google.common.collect.d) {
            collection = ((com.google.common.collect.d) collection).elementSet();
        }
        return dVar.elementSet().removeAll(collection);
    }

    public static boolean j(com.google.common.collect.d<?> dVar, Collection<?> collection) {
        au2.k(collection);
        if (collection instanceof com.google.common.collect.d) {
            collection = ((com.google.common.collect.d) collection).elementSet();
        }
        return dVar.elementSet().retainAll(collection);
    }

    public static <E> int k(com.google.common.collect.d<E> dVar, E e, int i) {
        o00.b(i, "count");
        int count = dVar.count(e);
        int i2 = i - count;
        if (i2 > 0) {
            dVar.add(e, i2);
        } else if (i2 < 0) {
            dVar.remove(e, -i2);
        }
        return count;
    }

    public static <E> boolean l(com.google.common.collect.d<E> dVar, E e, int i, int i2) {
        o00.b(i, "oldCount");
        o00.b(i2, "newCount");
        if (dVar.count(e) == i) {
            dVar.setCount(e, i2);
            return true;
        }
        return false;
    }
}
