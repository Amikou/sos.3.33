package com.google.common.collect;

import com.google.common.collect.ImmutableMultiset;
import com.google.common.collect.d;
import com.google.common.primitives.Ints;
import java.io.Serializable;

/* loaded from: classes2.dex */
public class RegularImmutableMultiset<E> extends ImmutableMultiset<E> {
    public static final RegularImmutableMultiset<Object> EMPTY = new RegularImmutableMultiset<>(e.b());
    public final transient e<E> contents;
    public final transient int h0;
    public transient ImmutableSet<E> i0;

    /* loaded from: classes2.dex */
    public final class ElementSet extends IndexedImmutableSet<E> {
        public ElementSet() {
        }

        @Override // com.google.common.collect.ImmutableCollection, java.util.AbstractCollection, java.util.Collection
        public boolean contains(Object obj) {
            return RegularImmutableMultiset.this.contains(obj);
        }

        @Override // com.google.common.collect.IndexedImmutableSet
        public E get(int i) {
            return RegularImmutableMultiset.this.contents.i(i);
        }

        @Override // com.google.common.collect.ImmutableCollection
        public boolean isPartialView() {
            return true;
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public int size() {
            return RegularImmutableMultiset.this.contents.C();
        }
    }

    /* loaded from: classes2.dex */
    public static class SerializedForm implements Serializable {
        private static final long serialVersionUID = 0;
        public final int[] counts;
        public final Object[] elements;

        public SerializedForm(d<? extends Object> dVar) {
            int size = dVar.entrySet().size();
            this.elements = new Object[size];
            this.counts = new int[size];
            int i = 0;
            for (d.a<? extends Object> aVar : dVar.entrySet()) {
                this.elements[i] = aVar.getElement();
                this.counts[i] = aVar.getCount();
                i++;
            }
        }

        /* JADX WARN: Multi-variable type inference failed */
        public Object readResolve() {
            ImmutableMultiset.b bVar = new ImmutableMultiset.b(this.elements.length);
            int i = 0;
            while (true) {
                Object[] objArr = this.elements;
                if (i < objArr.length) {
                    bVar.j(objArr[i], this.counts[i]);
                    i++;
                } else {
                    return bVar.k();
                }
            }
        }
    }

    public RegularImmutableMultiset(e<E> eVar) {
        this.contents = eVar;
        long j = 0;
        for (int i = 0; i < eVar.C(); i++) {
            j += eVar.k(i);
        }
        this.h0 = Ints.j(j);
    }

    @Override // com.google.common.collect.ImmutableMultiset, com.google.common.collect.d
    public int count(Object obj) {
        return this.contents.f(obj);
    }

    @Override // com.google.common.collect.ImmutableMultiset
    public d.a<E> getEntry(int i) {
        return this.contents.g(i);
    }

    @Override // com.google.common.collect.ImmutableCollection
    public boolean isPartialView() {
        return false;
    }

    @Override // java.util.AbstractCollection, java.util.Collection, com.google.common.collect.d
    public int size() {
        return this.h0;
    }

    @Override // com.google.common.collect.ImmutableMultiset, com.google.common.collect.ImmutableCollection
    public Object writeReplace() {
        return new SerializedForm(this);
    }

    @Override // com.google.common.collect.ImmutableMultiset, com.google.common.collect.d
    public ImmutableSet<E> elementSet() {
        ImmutableSet<E> immutableSet = this.i0;
        if (immutableSet == null) {
            ElementSet elementSet = new ElementSet();
            this.i0 = elementSet;
            return elementSet;
        }
        return immutableSet;
    }
}
