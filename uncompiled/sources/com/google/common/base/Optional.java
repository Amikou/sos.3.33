package com.google.common.base;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Set;

/* loaded from: classes2.dex */
public abstract class Optional<T> implements Serializable {
    private static final long serialVersionUID = 0;

    /* loaded from: classes2.dex */
    public class a implements Iterable<T> {
        public final /* synthetic */ Iterable a;

        /* renamed from: com.google.common.base.Optional$a$a  reason: collision with other inner class name */
        /* loaded from: classes2.dex */
        public class C0129a extends AbstractIterator<T> {
            public final Iterator<? extends Optional<? extends T>> g0;

            public C0129a() {
                this.g0 = (Iterator) au2.k(a.this.a.iterator());
            }

            @Override // com.google.common.base.AbstractIterator
            public T a() {
                while (this.g0.hasNext()) {
                    Optional<? extends T> next = this.g0.next();
                    if (next.isPresent()) {
                        return next.get();
                    }
                }
                return b();
            }
        }

        public a(Iterable iterable) {
            this.a = iterable;
        }

        @Override // java.lang.Iterable
        public Iterator<T> iterator() {
            return new C0129a();
        }
    }

    public static <T> Optional<T> absent() {
        return Absent.withType();
    }

    public static <T> Optional<T> fromNullable(T t) {
        return t == null ? absent() : new Present(t);
    }

    public static <T> Optional<T> of(T t) {
        return new Present(au2.k(t));
    }

    public static <T> Iterable<T> presentInstances(Iterable<? extends Optional<? extends T>> iterable) {
        au2.k(iterable);
        return new a(iterable);
    }

    public abstract Set<T> asSet();

    public abstract boolean equals(Object obj);

    public abstract T get();

    public abstract int hashCode();

    public abstract boolean isPresent();

    public abstract Optional<T> or(Optional<? extends T> optional);

    public abstract T or(dw3<? extends T> dw3Var);

    public abstract T or(T t);

    public abstract T orNull();

    public abstract String toString();

    public abstract <V> Optional<V> transform(jd1<? super T, V> jd1Var);
}
