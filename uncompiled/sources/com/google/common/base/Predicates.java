package com.google.common.base;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/* loaded from: classes2.dex */
public final class Predicates {

    /* loaded from: classes2.dex */
    public static class AndPredicate<T> implements gu2<T>, Serializable {
        private static final long serialVersionUID = 0;
        private final List<? extends gu2<? super T>> components;

        @Override // defpackage.gu2
        public boolean apply(T t) {
            for (int i = 0; i < this.components.size(); i++) {
                if (!this.components.get(i).apply(t)) {
                    return false;
                }
            }
            return true;
        }

        public boolean equals(Object obj) {
            if (obj instanceof AndPredicate) {
                return this.components.equals(((AndPredicate) obj).components);
            }
            return false;
        }

        public int hashCode() {
            return this.components.hashCode() + 306654252;
        }

        public String toString() {
            return Predicates.d("and", this.components);
        }

        public AndPredicate(List<? extends gu2<? super T>> list) {
            this.components = list;
        }
    }

    public static <T> gu2<T> b(gu2<? super T> gu2Var, gu2<? super T> gu2Var2) {
        return new AndPredicate(c((gu2) au2.k(gu2Var), (gu2) au2.k(gu2Var2)));
    }

    public static <T> List<gu2<? super T>> c(gu2<? super T> gu2Var, gu2<? super T> gu2Var2) {
        return Arrays.asList(gu2Var, gu2Var2);
    }

    public static String d(String str, Iterable<?> iterable) {
        StringBuilder sb = new StringBuilder("Predicates.");
        sb.append(str);
        sb.append('(');
        boolean z = true;
        for (Object obj : iterable) {
            if (!z) {
                sb.append(',');
            }
            sb.append(obj);
            z = false;
        }
        sb.append(')');
        return sb.toString();
    }
}
