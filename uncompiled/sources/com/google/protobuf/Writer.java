package com.google.protobuf;

import com.google.protobuf.h0;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/* loaded from: classes2.dex */
public interface Writer {

    /* loaded from: classes2.dex */
    public enum FieldOrder {
        ASCENDING,
        DESCENDING
    }

    void A(int i, float f) throws IOException;

    @Deprecated
    void B(int i) throws IOException;

    void C(int i, List<Integer> list, boolean z) throws IOException;

    void D(int i, int i2) throws IOException;

    void E(int i, List<Long> list, boolean z) throws IOException;

    void F(int i, List<Integer> list, boolean z) throws IOException;

    void G(int i, List<Double> list, boolean z) throws IOException;

    void H(int i, int i2) throws IOException;

    void I(int i, List<ByteString> list) throws IOException;

    void J(int i, List<?> list, y0 y0Var) throws IOException;

    @Deprecated
    void K(int i, List<?> list, y0 y0Var) throws IOException;

    void L(int i, Object obj, y0 y0Var) throws IOException;

    @Deprecated
    void M(int i, List<?> list) throws IOException;

    void N(int i, List<?> list) throws IOException;

    void O(int i, Object obj) throws IOException;

    @Deprecated
    void P(int i, Object obj, y0 y0Var) throws IOException;

    void Q(int i, ByteString byteString) throws IOException;

    @Deprecated
    void R(int i, Object obj) throws IOException;

    <K, V> void S(int i, h0.b<K, V> bVar, Map<K, V> map) throws IOException;

    void a(int i, List<Float> list, boolean z) throws IOException;

    void b(int i, int i2) throws IOException;

    void c(int i, Object obj) throws IOException;

    void d(int i, int i2) throws IOException;

    void e(int i, double d) throws IOException;

    void f(int i, List<Long> list, boolean z) throws IOException;

    void g(int i, List<Long> list, boolean z) throws IOException;

    void h(int i, long j) throws IOException;

    FieldOrder i();

    void j(int i, List<String> list) throws IOException;

    void k(int i, String str) throws IOException;

    void l(int i, long j) throws IOException;

    void m(int i, List<Integer> list, boolean z) throws IOException;

    void n(int i, long j) throws IOException;

    void o(int i, boolean z) throws IOException;

    void p(int i, int i2) throws IOException;

    @Deprecated
    void q(int i) throws IOException;

    void r(int i, int i2) throws IOException;

    void s(int i, List<Long> list, boolean z) throws IOException;

    void t(int i, List<Integer> list, boolean z) throws IOException;

    void u(int i, long j) throws IOException;

    void v(int i, List<Integer> list, boolean z) throws IOException;

    void w(int i, List<Boolean> list, boolean z) throws IOException;

    void x(int i, List<Integer> list, boolean z) throws IOException;

    void y(int i, List<Long> list, boolean z) throws IOException;

    void z(int i, long j) throws IOException;
}
