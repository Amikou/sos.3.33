package com.google.protobuf;

import com.google.protobuf.m0;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* compiled from: CodedInputStream.java */
/* loaded from: classes2.dex */
public abstract class j {
    public int a;
    public int b;
    public int c;
    public k d;
    public boolean e;

    /* compiled from: CodedInputStream.java */
    /* loaded from: classes2.dex */
    public static final class b extends j {
        public final byte[] f;
        public final boolean g;
        public int h;
        public int i;
        public int j;
        public int k;
        public int l;
        public boolean m;
        public int n;

        @Override // com.google.protobuf.j
        public void A(m0.a aVar, r rVar) throws IOException {
            int B = B();
            if (this.a < this.b) {
                int o = o(B);
                this.a++;
                aVar.mergeFrom(this, rVar);
                a(0);
                this.a--;
                n(o);
                return;
            }
            throw InvalidProtocolBufferException.recursionLimitExceeded();
        }

        /* JADX WARN: Code restructure failed: missing block: B:33:0x0068, code lost:
            if (r2[r3] < 0) goto L34;
         */
        @Override // com.google.protobuf.j
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public int B() throws java.io.IOException {
            /*
                r5 = this;
                int r0 = r5.j
                int r1 = r5.h
                if (r1 != r0) goto L7
                goto L6a
            L7:
                byte[] r2 = r5.f
                int r3 = r0 + 1
                r0 = r2[r0]
                if (r0 < 0) goto L12
                r5.j = r3
                return r0
            L12:
                int r1 = r1 - r3
                r4 = 9
                if (r1 >= r4) goto L18
                goto L6a
            L18:
                int r1 = r3 + 1
                r3 = r2[r3]
                int r3 = r3 << 7
                r0 = r0 ^ r3
                if (r0 >= 0) goto L24
                r0 = r0 ^ (-128(0xffffffffffffff80, float:NaN))
                goto L70
            L24:
                int r3 = r1 + 1
                r1 = r2[r1]
                int r1 = r1 << 14
                r0 = r0 ^ r1
                if (r0 < 0) goto L31
                r0 = r0 ^ 16256(0x3f80, float:2.278E-41)
            L2f:
                r1 = r3
                goto L70
            L31:
                int r1 = r3 + 1
                r3 = r2[r3]
                int r3 = r3 << 21
                r0 = r0 ^ r3
                if (r0 >= 0) goto L3f
                r2 = -2080896(0xffffffffffe03f80, float:NaN)
                r0 = r0 ^ r2
                goto L70
            L3f:
                int r3 = r1 + 1
                r1 = r2[r1]
                int r4 = r1 << 28
                r0 = r0 ^ r4
                r4 = 266354560(0xfe03f80, float:2.2112565E-29)
                r0 = r0 ^ r4
                if (r1 >= 0) goto L2f
                int r1 = r3 + 1
                r3 = r2[r3]
                if (r3 >= 0) goto L70
                int r3 = r1 + 1
                r1 = r2[r1]
                if (r1 >= 0) goto L2f
                int r1 = r3 + 1
                r3 = r2[r3]
                if (r3 >= 0) goto L70
                int r3 = r1 + 1
                r1 = r2[r1]
                if (r1 >= 0) goto L2f
                int r1 = r3 + 1
                r2 = r2[r3]
                if (r2 >= 0) goto L70
            L6a:
                long r0 = r5.T()
                int r0 = (int) r0
                return r0
            L70:
                r5.j = r1
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.j.b.B():int");
        }

        @Override // com.google.protobuf.j
        public int D() throws IOException {
            return Q();
        }

        @Override // com.google.protobuf.j
        public long E() throws IOException {
            return R();
        }

        @Override // com.google.protobuf.j
        public int F() throws IOException {
            return j.b(B());
        }

        @Override // com.google.protobuf.j
        public long G() throws IOException {
            return j.c(S());
        }

        @Override // com.google.protobuf.j
        public String H() throws IOException {
            int B = B();
            if (B > 0) {
                int i = this.h;
                int i2 = this.j;
                if (B <= i - i2) {
                    String str = new String(this.f, i2, B, a0.a);
                    this.j += B;
                    return str;
                }
            }
            if (B == 0) {
                return "";
            }
            if (B < 0) {
                throw InvalidProtocolBufferException.negativeSize();
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }

        @Override // com.google.protobuf.j
        public String I() throws IOException {
            int B = B();
            if (B > 0) {
                int i = this.h;
                int i2 = this.j;
                if (B <= i - i2) {
                    String h = Utf8.h(this.f, i2, B);
                    this.j += B;
                    return h;
                }
            }
            if (B == 0) {
                return "";
            }
            if (B <= 0) {
                throw InvalidProtocolBufferException.negativeSize();
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }

        @Override // com.google.protobuf.j
        public int J() throws IOException {
            if (f()) {
                this.l = 0;
                return 0;
            }
            int B = B();
            this.l = B;
            if (WireFormat.a(B) != 0) {
                return this.l;
            }
            throw InvalidProtocolBufferException.invalidTag();
        }

        @Override // com.google.protobuf.j
        public int K() throws IOException {
            return B();
        }

        @Override // com.google.protobuf.j
        public long L() throws IOException {
            return S();
        }

        @Override // com.google.protobuf.j
        public boolean N(int i) throws IOException {
            int b = WireFormat.b(i);
            if (b == 0) {
                X();
                return true;
            } else if (b == 1) {
                W(8);
                return true;
            } else if (b == 2) {
                W(B());
                return true;
            } else if (b == 3) {
                V();
                a(WireFormat.c(WireFormat.a(i), 4));
                return true;
            } else if (b != 4) {
                if (b == 5) {
                    W(4);
                    return true;
                }
                throw InvalidProtocolBufferException.invalidWireType();
            } else {
                return false;
            }
        }

        public byte O() throws IOException {
            int i = this.j;
            if (i != this.h) {
                byte[] bArr = this.f;
                this.j = i + 1;
                return bArr[i];
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }

        public byte[] P(int i) throws IOException {
            if (i > 0) {
                int i2 = this.h;
                int i3 = this.j;
                if (i <= i2 - i3) {
                    int i4 = i + i3;
                    this.j = i4;
                    return Arrays.copyOfRange(this.f, i3, i4);
                }
            }
            if (i <= 0) {
                if (i == 0) {
                    return a0.c;
                }
                throw InvalidProtocolBufferException.negativeSize();
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }

        public int Q() throws IOException {
            int i = this.j;
            if (this.h - i >= 4) {
                byte[] bArr = this.f;
                this.j = i + 4;
                return ((bArr[i + 3] & 255) << 24) | (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << 16);
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }

        public long R() throws IOException {
            int i = this.j;
            if (this.h - i >= 8) {
                byte[] bArr = this.f;
                this.j = i + 8;
                return ((bArr[i + 7] & 255) << 56) | (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << 16) | ((bArr[i + 3] & 255) << 24) | ((bArr[i + 4] & 255) << 32) | ((bArr[i + 5] & 255) << 40) | ((bArr[i + 6] & 255) << 48);
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }

        /* JADX WARN: Code restructure failed: missing block: B:39:0x00b4, code lost:
            if (r2[r0] < 0) goto L42;
         */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public long S() throws java.io.IOException {
            /*
                Method dump skipped, instructions count: 192
                To view this dump change 'Code comments level' option to 'DEBUG'
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.j.b.S():long");
        }

        public long T() throws IOException {
            long j = 0;
            for (int i = 0; i < 64; i += 7) {
                byte O = O();
                j |= (O & Byte.MAX_VALUE) << i;
                if ((O & 128) == 0) {
                    return j;
                }
            }
            throw InvalidProtocolBufferException.malformedVarint();
        }

        public final void U() {
            int i = this.h + this.i;
            this.h = i;
            int i2 = i - this.k;
            int i3 = this.n;
            if (i2 > i3) {
                int i4 = i2 - i3;
                this.i = i4;
                this.h = i - i4;
                return;
            }
            this.i = 0;
        }

        public void V() throws IOException {
            int J;
            do {
                J = J();
                if (J == 0) {
                    return;
                }
            } while (N(J));
        }

        public void W(int i) throws IOException {
            if (i >= 0) {
                int i2 = this.h;
                int i3 = this.j;
                if (i <= i2 - i3) {
                    this.j = i3 + i;
                    return;
                }
            }
            if (i < 0) {
                throw InvalidProtocolBufferException.negativeSize();
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }

        public final void X() throws IOException {
            if (this.h - this.j >= 10) {
                Y();
            } else {
                Z();
            }
        }

        public final void Y() throws IOException {
            for (int i = 0; i < 10; i++) {
                byte[] bArr = this.f;
                int i2 = this.j;
                this.j = i2 + 1;
                if (bArr[i2] >= 0) {
                    return;
                }
            }
            throw InvalidProtocolBufferException.malformedVarint();
        }

        public final void Z() throws IOException {
            for (int i = 0; i < 10; i++) {
                if (O() >= 0) {
                    return;
                }
            }
            throw InvalidProtocolBufferException.malformedVarint();
        }

        @Override // com.google.protobuf.j
        public void a(int i) throws InvalidProtocolBufferException {
            if (this.l != i) {
                throw InvalidProtocolBufferException.invalidEndTag();
            }
        }

        @Override // com.google.protobuf.j
        public int d() {
            int i = this.n;
            if (i == Integer.MAX_VALUE) {
                return -1;
            }
            return i - e();
        }

        @Override // com.google.protobuf.j
        public int e() {
            return this.j - this.k;
        }

        @Override // com.google.protobuf.j
        public boolean f() throws IOException {
            return this.j == this.h;
        }

        @Override // com.google.protobuf.j
        public void n(int i) {
            this.n = i;
            U();
        }

        @Override // com.google.protobuf.j
        public int o(int i) throws InvalidProtocolBufferException {
            if (i >= 0) {
                int e = i + e();
                int i2 = this.n;
                if (e <= i2) {
                    this.n = e;
                    U();
                    return i2;
                }
                throw InvalidProtocolBufferException.truncatedMessage();
            }
            throw InvalidProtocolBufferException.negativeSize();
        }

        @Override // com.google.protobuf.j
        public boolean p() throws IOException {
            return S() != 0;
        }

        @Override // com.google.protobuf.j
        public ByteString q() throws IOException {
            ByteString copyFrom;
            int B = B();
            if (B > 0) {
                int i = this.h;
                int i2 = this.j;
                if (B <= i - i2) {
                    if (this.g && this.m) {
                        copyFrom = ByteString.wrap(this.f, i2, B);
                    } else {
                        copyFrom = ByteString.copyFrom(this.f, i2, B);
                    }
                    this.j += B;
                    return copyFrom;
                }
            }
            if (B == 0) {
                return ByteString.EMPTY;
            }
            return ByteString.wrap(P(B));
        }

        @Override // com.google.protobuf.j
        public double r() throws IOException {
            return Double.longBitsToDouble(R());
        }

        @Override // com.google.protobuf.j
        public int s() throws IOException {
            return B();
        }

        @Override // com.google.protobuf.j
        public int t() throws IOException {
            return Q();
        }

        @Override // com.google.protobuf.j
        public long u() throws IOException {
            return R();
        }

        @Override // com.google.protobuf.j
        public float v() throws IOException {
            return Float.intBitsToFloat(Q());
        }

        @Override // com.google.protobuf.j
        public void w(int i, m0.a aVar, r rVar) throws IOException {
            int i2 = this.a;
            if (i2 < this.b) {
                this.a = i2 + 1;
                aVar.mergeFrom(this, rVar);
                a(WireFormat.c(i, 4));
                this.a--;
                return;
            }
            throw InvalidProtocolBufferException.recursionLimitExceeded();
        }

        @Override // com.google.protobuf.j
        public int x() throws IOException {
            return B();
        }

        @Override // com.google.protobuf.j
        public long y() throws IOException {
            return S();
        }

        @Override // com.google.protobuf.j
        public <T extends m0> T z(t0<T> t0Var, r rVar) throws IOException {
            int B = B();
            if (this.a < this.b) {
                int o = o(B);
                this.a++;
                T parsePartialFrom = t0Var.parsePartialFrom(this, rVar);
                a(0);
                this.a--;
                n(o);
                return parsePartialFrom;
            }
            throw InvalidProtocolBufferException.recursionLimitExceeded();
        }

        public b(byte[] bArr, int i, int i2, boolean z) {
            super();
            this.n = Integer.MAX_VALUE;
            this.f = bArr;
            this.h = i2 + i;
            this.j = i;
            this.k = i;
            this.g = z;
        }
    }

    /* compiled from: CodedInputStream.java */
    /* loaded from: classes2.dex */
    public static final class c extends j {
        public final InputStream f;
        public final byte[] g;
        public int h;
        public int i;
        public int j;
        public int k;
        public int l;
        public int m;
        public a n;

        /* compiled from: CodedInputStream.java */
        /* loaded from: classes2.dex */
        public interface a {
            void a();
        }

        @Override // com.google.protobuf.j
        public void A(m0.a aVar, r rVar) throws IOException {
            int B = B();
            if (this.a < this.b) {
                int o = o(B);
                this.a++;
                aVar.mergeFrom(this, rVar);
                a(0);
                this.a--;
                n(o);
                return;
            }
            throw InvalidProtocolBufferException.recursionLimitExceeded();
        }

        /* JADX WARN: Code restructure failed: missing block: B:33:0x0068, code lost:
            if (r2[r3] < 0) goto L34;
         */
        @Override // com.google.protobuf.j
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public int B() throws java.io.IOException {
            /*
                r5 = this;
                int r0 = r5.j
                int r1 = r5.h
                if (r1 != r0) goto L7
                goto L6a
            L7:
                byte[] r2 = r5.g
                int r3 = r0 + 1
                r0 = r2[r0]
                if (r0 < 0) goto L12
                r5.j = r3
                return r0
            L12:
                int r1 = r1 - r3
                r4 = 9
                if (r1 >= r4) goto L18
                goto L6a
            L18:
                int r1 = r3 + 1
                r3 = r2[r3]
                int r3 = r3 << 7
                r0 = r0 ^ r3
                if (r0 >= 0) goto L24
                r0 = r0 ^ (-128(0xffffffffffffff80, float:NaN))
                goto L70
            L24:
                int r3 = r1 + 1
                r1 = r2[r1]
                int r1 = r1 << 14
                r0 = r0 ^ r1
                if (r0 < 0) goto L31
                r0 = r0 ^ 16256(0x3f80, float:2.278E-41)
            L2f:
                r1 = r3
                goto L70
            L31:
                int r1 = r3 + 1
                r3 = r2[r3]
                int r3 = r3 << 21
                r0 = r0 ^ r3
                if (r0 >= 0) goto L3f
                r2 = -2080896(0xffffffffffe03f80, float:NaN)
                r0 = r0 ^ r2
                goto L70
            L3f:
                int r3 = r1 + 1
                r1 = r2[r1]
                int r4 = r1 << 28
                r0 = r0 ^ r4
                r4 = 266354560(0xfe03f80, float:2.2112565E-29)
                r0 = r0 ^ r4
                if (r1 >= 0) goto L2f
                int r1 = r3 + 1
                r3 = r2[r3]
                if (r3 >= 0) goto L70
                int r3 = r1 + 1
                r1 = r2[r1]
                if (r1 >= 0) goto L2f
                int r1 = r3 + 1
                r3 = r2[r3]
                if (r3 >= 0) goto L70
                int r3 = r1 + 1
                r1 = r2[r1]
                if (r1 >= 0) goto L2f
                int r1 = r3 + 1
                r2 = r2[r3]
                if (r2 >= 0) goto L70
            L6a:
                long r0 = r5.W()
                int r0 = (int) r0
                return r0
            L70:
                r5.j = r1
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.j.c.B():int");
        }

        @Override // com.google.protobuf.j
        public int D() throws IOException {
            return T();
        }

        @Override // com.google.protobuf.j
        public long E() throws IOException {
            return U();
        }

        @Override // com.google.protobuf.j
        public int F() throws IOException {
            return j.b(B());
        }

        @Override // com.google.protobuf.j
        public long G() throws IOException {
            return j.c(V());
        }

        @Override // com.google.protobuf.j
        public String H() throws IOException {
            int B = B();
            if (B > 0) {
                int i = this.h;
                int i2 = this.j;
                if (B <= i - i2) {
                    String str = new String(this.g, i2, B, a0.a);
                    this.j += B;
                    return str;
                }
            }
            if (B == 0) {
                return "";
            }
            if (B <= this.h) {
                Y(B);
                String str2 = new String(this.g, this.j, B, a0.a);
                this.j += B;
                return str2;
            }
            return new String(Q(B, false), a0.a);
        }

        @Override // com.google.protobuf.j
        public String I() throws IOException {
            byte[] Q;
            int B = B();
            int i = this.j;
            int i2 = this.h;
            if (B <= i2 - i && B > 0) {
                Q = this.g;
                this.j = i + B;
            } else if (B == 0) {
                return "";
            } else {
                if (B <= i2) {
                    Y(B);
                    Q = this.g;
                    this.j = B + 0;
                } else {
                    Q = Q(B, false);
                }
                i = 0;
            }
            return Utf8.h(Q, i, B);
        }

        @Override // com.google.protobuf.j
        public int J() throws IOException {
            if (f()) {
                this.k = 0;
                return 0;
            }
            int B = B();
            this.k = B;
            if (WireFormat.a(B) != 0) {
                return this.k;
            }
            throw InvalidProtocolBufferException.invalidTag();
        }

        @Override // com.google.protobuf.j
        public int K() throws IOException {
            return B();
        }

        @Override // com.google.protobuf.j
        public long L() throws IOException {
            return V();
        }

        @Override // com.google.protobuf.j
        public boolean N(int i) throws IOException {
            int b = WireFormat.b(i);
            if (b == 0) {
                c0();
                return true;
            } else if (b == 1) {
                a0(8);
                return true;
            } else if (b == 2) {
                a0(B());
                return true;
            } else if (b == 3) {
                Z();
                a(WireFormat.c(WireFormat.a(i), 4));
                return true;
            } else if (b != 4) {
                if (b == 5) {
                    a0(4);
                    return true;
                }
                throw InvalidProtocolBufferException.invalidWireType();
            } else {
                return false;
            }
        }

        public final ByteString O(int i) throws IOException {
            byte[] R = R(i);
            if (R != null) {
                return ByteString.copyFrom(R);
            }
            int i2 = this.j;
            int i3 = this.h;
            int i4 = i3 - i2;
            this.l += i3;
            this.j = 0;
            this.h = 0;
            List<byte[]> S = S(i - i4);
            byte[] bArr = new byte[i];
            System.arraycopy(this.g, i2, bArr, 0, i4);
            for (byte[] bArr2 : S) {
                System.arraycopy(bArr2, 0, bArr, i4, bArr2.length);
                i4 += bArr2.length;
            }
            return ByteString.wrap(bArr);
        }

        public byte P() throws IOException {
            if (this.j == this.h) {
                Y(1);
            }
            byte[] bArr = this.g;
            int i = this.j;
            this.j = i + 1;
            return bArr[i];
        }

        public final byte[] Q(int i, boolean z) throws IOException {
            byte[] R = R(i);
            if (R != null) {
                return z ? (byte[]) R.clone() : R;
            }
            int i2 = this.j;
            int i3 = this.h;
            int i4 = i3 - i2;
            this.l += i3;
            this.j = 0;
            this.h = 0;
            List<byte[]> S = S(i - i4);
            byte[] bArr = new byte[i];
            System.arraycopy(this.g, i2, bArr, 0, i4);
            for (byte[] bArr2 : S) {
                System.arraycopy(bArr2, 0, bArr, i4, bArr2.length);
                i4 += bArr2.length;
            }
            return bArr;
        }

        public final byte[] R(int i) throws IOException {
            if (i == 0) {
                return a0.c;
            }
            if (i >= 0) {
                int i2 = this.l;
                int i3 = this.j;
                int i4 = i2 + i3 + i;
                if (i4 - this.c <= 0) {
                    int i5 = this.m;
                    if (i4 <= i5) {
                        int i6 = this.h - i3;
                        int i7 = i - i6;
                        if (i7 < 4096 || i7 <= this.f.available()) {
                            byte[] bArr = new byte[i];
                            System.arraycopy(this.g, this.j, bArr, 0, i6);
                            this.l += this.h;
                            this.j = 0;
                            this.h = 0;
                            while (i6 < i) {
                                int read = this.f.read(bArr, i6, i - i6);
                                if (read != -1) {
                                    this.l += read;
                                    i6 += read;
                                } else {
                                    throw InvalidProtocolBufferException.truncatedMessage();
                                }
                            }
                            return bArr;
                        }
                        return null;
                    }
                    a0((i5 - i2) - i3);
                    throw InvalidProtocolBufferException.truncatedMessage();
                }
                throw InvalidProtocolBufferException.sizeLimitExceeded();
            }
            throw InvalidProtocolBufferException.negativeSize();
        }

        public final List<byte[]> S(int i) throws IOException {
            ArrayList arrayList = new ArrayList();
            while (i > 0) {
                int min = Math.min(i, 4096);
                byte[] bArr = new byte[min];
                int i2 = 0;
                while (i2 < min) {
                    int read = this.f.read(bArr, i2, min - i2);
                    if (read != -1) {
                        this.l += read;
                        i2 += read;
                    } else {
                        throw InvalidProtocolBufferException.truncatedMessage();
                    }
                }
                i -= min;
                arrayList.add(bArr);
            }
            return arrayList;
        }

        public int T() throws IOException {
            int i = this.j;
            if (this.h - i < 4) {
                Y(4);
                i = this.j;
            }
            byte[] bArr = this.g;
            this.j = i + 4;
            return ((bArr[i + 3] & 255) << 24) | (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << 16);
        }

        public long U() throws IOException {
            int i = this.j;
            if (this.h - i < 8) {
                Y(8);
                i = this.j;
            }
            byte[] bArr = this.g;
            this.j = i + 8;
            return ((bArr[i + 7] & 255) << 56) | (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << 16) | ((bArr[i + 3] & 255) << 24) | ((bArr[i + 4] & 255) << 32) | ((bArr[i + 5] & 255) << 40) | ((bArr[i + 6] & 255) << 48);
        }

        /* JADX WARN: Code restructure failed: missing block: B:39:0x00b4, code lost:
            if (r2[r0] < 0) goto L42;
         */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public long V() throws java.io.IOException {
            /*
                Method dump skipped, instructions count: 192
                To view this dump change 'Code comments level' option to 'DEBUG'
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.j.c.V():long");
        }

        public long W() throws IOException {
            long j = 0;
            for (int i = 0; i < 64; i += 7) {
                byte P = P();
                j |= (P & Byte.MAX_VALUE) << i;
                if ((P & 128) == 0) {
                    return j;
                }
            }
            throw InvalidProtocolBufferException.malformedVarint();
        }

        public final void X() {
            int i = this.h + this.i;
            this.h = i;
            int i2 = this.l + i;
            int i3 = this.m;
            if (i2 > i3) {
                int i4 = i2 - i3;
                this.i = i4;
                this.h = i - i4;
                return;
            }
            this.i = 0;
        }

        public final void Y(int i) throws IOException {
            if (f0(i)) {
                return;
            }
            if (i > (this.c - this.l) - this.j) {
                throw InvalidProtocolBufferException.sizeLimitExceeded();
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }

        public void Z() throws IOException {
            int J;
            do {
                J = J();
                if (J == 0) {
                    return;
                }
            } while (N(J));
        }

        @Override // com.google.protobuf.j
        public void a(int i) throws InvalidProtocolBufferException {
            if (this.k != i) {
                throw InvalidProtocolBufferException.invalidEndTag();
            }
        }

        public void a0(int i) throws IOException {
            int i2 = this.h;
            int i3 = this.j;
            if (i <= i2 - i3 && i >= 0) {
                this.j = i3 + i;
            } else {
                b0(i);
            }
        }

        public final void b0(int i) throws IOException {
            if (i >= 0) {
                int i2 = this.l;
                int i3 = this.j;
                int i4 = i2 + i3 + i;
                int i5 = this.m;
                if (i4 <= i5) {
                    int i6 = 0;
                    if (this.n == null) {
                        this.l = i2 + i3;
                        this.h = 0;
                        this.j = 0;
                        i6 = this.h - i3;
                        while (i6 < i) {
                            try {
                                long j = i - i6;
                                long skip = this.f.skip(j);
                                int i7 = (skip > 0L ? 1 : (skip == 0L ? 0 : -1));
                                if (i7 < 0 || skip > j) {
                                    throw new IllegalStateException(this.f.getClass() + "#skip returned invalid result: " + skip + "\nThe InputStream implementation is buggy.");
                                } else if (i7 == 0) {
                                    break;
                                } else {
                                    i6 += (int) skip;
                                }
                            } finally {
                                this.l += i6;
                                X();
                            }
                        }
                    }
                    if (i6 >= i) {
                        return;
                    }
                    int i8 = this.h;
                    int i9 = i8 - this.j;
                    this.j = i8;
                    Y(1);
                    while (true) {
                        int i10 = i - i9;
                        int i11 = this.h;
                        if (i10 > i11) {
                            i9 += i11;
                            this.j = i11;
                            Y(1);
                        } else {
                            this.j = i10;
                            return;
                        }
                    }
                } else {
                    a0((i5 - i2) - i3);
                    throw InvalidProtocolBufferException.truncatedMessage();
                }
            } else {
                throw InvalidProtocolBufferException.negativeSize();
            }
        }

        public final void c0() throws IOException {
            if (this.h - this.j >= 10) {
                d0();
            } else {
                e0();
            }
        }

        @Override // com.google.protobuf.j
        public int d() {
            int i = this.m;
            if (i == Integer.MAX_VALUE) {
                return -1;
            }
            return i - (this.l + this.j);
        }

        public final void d0() throws IOException {
            for (int i = 0; i < 10; i++) {
                byte[] bArr = this.g;
                int i2 = this.j;
                this.j = i2 + 1;
                if (bArr[i2] >= 0) {
                    return;
                }
            }
            throw InvalidProtocolBufferException.malformedVarint();
        }

        @Override // com.google.protobuf.j
        public int e() {
            return this.l + this.j;
        }

        public final void e0() throws IOException {
            for (int i = 0; i < 10; i++) {
                if (P() >= 0) {
                    return;
                }
            }
            throw InvalidProtocolBufferException.malformedVarint();
        }

        @Override // com.google.protobuf.j
        public boolean f() throws IOException {
            return this.j == this.h && !f0(1);
        }

        public final boolean f0(int i) throws IOException {
            int i2 = this.j;
            if (i2 + i > this.h) {
                int i3 = this.c;
                int i4 = this.l;
                if (i <= (i3 - i4) - i2 && i4 + i2 + i <= this.m) {
                    a aVar = this.n;
                    if (aVar != null) {
                        aVar.a();
                    }
                    int i5 = this.j;
                    if (i5 > 0) {
                        int i6 = this.h;
                        if (i6 > i5) {
                            byte[] bArr = this.g;
                            System.arraycopy(bArr, i5, bArr, 0, i6 - i5);
                        }
                        this.l += i5;
                        this.h -= i5;
                        this.j = 0;
                    }
                    InputStream inputStream = this.f;
                    byte[] bArr2 = this.g;
                    int i7 = this.h;
                    int read = inputStream.read(bArr2, i7, Math.min(bArr2.length - i7, (this.c - this.l) - i7));
                    if (read == 0 || read < -1 || read > this.g.length) {
                        throw new IllegalStateException(this.f.getClass() + "#read(byte[]) returned invalid result: " + read + "\nThe InputStream implementation is buggy.");
                    } else if (read > 0) {
                        this.h += read;
                        X();
                        if (this.h >= i) {
                            return true;
                        }
                        return f0(i);
                    } else {
                        return false;
                    }
                }
                return false;
            }
            throw new IllegalStateException("refillBuffer() called when " + i + " bytes were already available in buffer");
        }

        @Override // com.google.protobuf.j
        public void n(int i) {
            this.m = i;
            X();
        }

        @Override // com.google.protobuf.j
        public int o(int i) throws InvalidProtocolBufferException {
            if (i >= 0) {
                int i2 = i + this.l + this.j;
                int i3 = this.m;
                if (i2 <= i3) {
                    this.m = i2;
                    X();
                    return i3;
                }
                throw InvalidProtocolBufferException.truncatedMessage();
            }
            throw InvalidProtocolBufferException.negativeSize();
        }

        @Override // com.google.protobuf.j
        public boolean p() throws IOException {
            return V() != 0;
        }

        @Override // com.google.protobuf.j
        public ByteString q() throws IOException {
            int B = B();
            int i = this.h;
            int i2 = this.j;
            if (B > i - i2 || B <= 0) {
                if (B == 0) {
                    return ByteString.EMPTY;
                }
                return O(B);
            }
            ByteString copyFrom = ByteString.copyFrom(this.g, i2, B);
            this.j += B;
            return copyFrom;
        }

        @Override // com.google.protobuf.j
        public double r() throws IOException {
            return Double.longBitsToDouble(U());
        }

        @Override // com.google.protobuf.j
        public int s() throws IOException {
            return B();
        }

        @Override // com.google.protobuf.j
        public int t() throws IOException {
            return T();
        }

        @Override // com.google.protobuf.j
        public long u() throws IOException {
            return U();
        }

        @Override // com.google.protobuf.j
        public float v() throws IOException {
            return Float.intBitsToFloat(T());
        }

        @Override // com.google.protobuf.j
        public void w(int i, m0.a aVar, r rVar) throws IOException {
            int i2 = this.a;
            if (i2 < this.b) {
                this.a = i2 + 1;
                aVar.mergeFrom(this, rVar);
                a(WireFormat.c(i, 4));
                this.a--;
                return;
            }
            throw InvalidProtocolBufferException.recursionLimitExceeded();
        }

        @Override // com.google.protobuf.j
        public int x() throws IOException {
            return B();
        }

        @Override // com.google.protobuf.j
        public long y() throws IOException {
            return V();
        }

        @Override // com.google.protobuf.j
        public <T extends m0> T z(t0<T> t0Var, r rVar) throws IOException {
            int B = B();
            if (this.a < this.b) {
                int o = o(B);
                this.a++;
                T parsePartialFrom = t0Var.parsePartialFrom(this, rVar);
                a(0);
                this.a--;
                n(o);
                return parsePartialFrom;
            }
            throw InvalidProtocolBufferException.recursionLimitExceeded();
        }

        public c(InputStream inputStream, int i) {
            super();
            this.m = Integer.MAX_VALUE;
            this.n = null;
            a0.b(inputStream, "input");
            this.f = inputStream;
            this.g = new byte[i];
            this.h = 0;
            this.j = 0;
            this.l = 0;
        }
    }

    /* compiled from: CodedInputStream.java */
    /* loaded from: classes2.dex */
    public static final class d extends j {
        public final ByteBuffer f;
        public final boolean g;
        public final long h;
        public long i;
        public long j;
        public long k;
        public int l;
        public int m;
        public boolean n;
        public int o;

        public static boolean P() {
            return k1.I();
        }

        @Override // com.google.protobuf.j
        public void A(m0.a aVar, r rVar) throws IOException {
            int B = B();
            if (this.a < this.b) {
                int o = o(B);
                this.a++;
                aVar.mergeFrom(this, rVar);
                a(0);
                this.a--;
                n(o);
                return;
            }
            throw InvalidProtocolBufferException.recursionLimitExceeded();
        }

        /* JADX WARN: Code restructure failed: missing block: B:33:0x0083, code lost:
            if (com.google.protobuf.k1.u(r4) < 0) goto L34;
         */
        @Override // com.google.protobuf.j
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public int B() throws java.io.IOException {
            /*
                r10 = this;
                long r0 = r10.j
                long r2 = r10.i
                int r2 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
                if (r2 != 0) goto La
                goto L85
            La:
                r2 = 1
                long r4 = r0 + r2
                byte r0 = com.google.protobuf.k1.u(r0)
                if (r0 < 0) goto L17
                r10.j = r4
                return r0
            L17:
                long r6 = r10.i
                long r6 = r6 - r4
                r8 = 9
                int r1 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
                if (r1 >= 0) goto L21
                goto L85
            L21:
                long r6 = r4 + r2
                byte r1 = com.google.protobuf.k1.u(r4)
                int r1 = r1 << 7
                r0 = r0 ^ r1
                if (r0 >= 0) goto L2f
                r0 = r0 ^ (-128(0xffffffffffffff80, float:NaN))
                goto L8b
            L2f:
                long r4 = r6 + r2
                byte r1 = com.google.protobuf.k1.u(r6)
                int r1 = r1 << 14
                r0 = r0 ^ r1
                if (r0 < 0) goto L3e
                r0 = r0 ^ 16256(0x3f80, float:2.278E-41)
            L3c:
                r6 = r4
                goto L8b
            L3e:
                long r6 = r4 + r2
                byte r1 = com.google.protobuf.k1.u(r4)
                int r1 = r1 << 21
                r0 = r0 ^ r1
                if (r0 >= 0) goto L4e
                r1 = -2080896(0xffffffffffe03f80, float:NaN)
                r0 = r0 ^ r1
                goto L8b
            L4e:
                long r4 = r6 + r2
                byte r1 = com.google.protobuf.k1.u(r6)
                int r6 = r1 << 28
                r0 = r0 ^ r6
                r6 = 266354560(0xfe03f80, float:2.2112565E-29)
                r0 = r0 ^ r6
                if (r1 >= 0) goto L3c
                long r6 = r4 + r2
                byte r1 = com.google.protobuf.k1.u(r4)
                if (r1 >= 0) goto L8b
                long r4 = r6 + r2
                byte r1 = com.google.protobuf.k1.u(r6)
                if (r1 >= 0) goto L3c
                long r6 = r4 + r2
                byte r1 = com.google.protobuf.k1.u(r4)
                if (r1 >= 0) goto L8b
                long r4 = r6 + r2
                byte r1 = com.google.protobuf.k1.u(r6)
                if (r1 >= 0) goto L3c
                long r6 = r4 + r2
                byte r1 = com.google.protobuf.k1.u(r4)
                if (r1 >= 0) goto L8b
            L85:
                long r0 = r10.U()
                int r0 = (int) r0
                return r0
            L8b:
                r10.j = r6
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.j.d.B():int");
        }

        @Override // com.google.protobuf.j
        public int D() throws IOException {
            return R();
        }

        @Override // com.google.protobuf.j
        public long E() throws IOException {
            return S();
        }

        @Override // com.google.protobuf.j
        public int F() throws IOException {
            return j.b(B());
        }

        @Override // com.google.protobuf.j
        public long G() throws IOException {
            return j.c(T());
        }

        @Override // com.google.protobuf.j
        public String H() throws IOException {
            int B = B();
            if (B <= 0 || B > W()) {
                if (B == 0) {
                    return "";
                }
                if (B < 0) {
                    throw InvalidProtocolBufferException.negativeSize();
                }
                throw InvalidProtocolBufferException.truncatedMessage();
            }
            byte[] bArr = new byte[B];
            long j = B;
            k1.n(this.j, bArr, 0L, j);
            String str = new String(bArr, a0.a);
            this.j += j;
            return str;
        }

        @Override // com.google.protobuf.j
        public String I() throws IOException {
            int B = B();
            if (B > 0 && B <= W()) {
                String g = Utf8.g(this.f, O(this.j), B);
                this.j += B;
                return g;
            } else if (B == 0) {
                return "";
            } else {
                if (B <= 0) {
                    throw InvalidProtocolBufferException.negativeSize();
                }
                throw InvalidProtocolBufferException.truncatedMessage();
            }
        }

        @Override // com.google.protobuf.j
        public int J() throws IOException {
            if (f()) {
                this.m = 0;
                return 0;
            }
            int B = B();
            this.m = B;
            if (WireFormat.a(B) != 0) {
                return this.m;
            }
            throw InvalidProtocolBufferException.invalidTag();
        }

        @Override // com.google.protobuf.j
        public int K() throws IOException {
            return B();
        }

        @Override // com.google.protobuf.j
        public long L() throws IOException {
            return T();
        }

        @Override // com.google.protobuf.j
        public boolean N(int i) throws IOException {
            int b = WireFormat.b(i);
            if (b == 0) {
                Z();
                return true;
            } else if (b == 1) {
                Y(8);
                return true;
            } else if (b == 2) {
                Y(B());
                return true;
            } else if (b == 3) {
                X();
                a(WireFormat.c(WireFormat.a(i), 4));
                return true;
            } else if (b != 4) {
                if (b == 5) {
                    Y(4);
                    return true;
                }
                throw InvalidProtocolBufferException.invalidWireType();
            } else {
                return false;
            }
        }

        public final int O(long j) {
            return (int) (j - this.h);
        }

        public byte Q() throws IOException {
            long j = this.j;
            if (j != this.i) {
                this.j = 1 + j;
                return k1.u(j);
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }

        public int R() throws IOException {
            long j = this.j;
            if (this.i - j >= 4) {
                this.j = 4 + j;
                return ((k1.u(j + 3) & 255) << 24) | (k1.u(j) & 255) | ((k1.u(1 + j) & 255) << 8) | ((k1.u(2 + j) & 255) << 16);
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }

        public long S() throws IOException {
            long j = this.j;
            if (this.i - j >= 8) {
                this.j = 8 + j;
                return ((k1.u(j + 7) & 255) << 56) | (k1.u(j) & 255) | ((k1.u(1 + j) & 255) << 8) | ((k1.u(2 + j) & 255) << 16) | ((k1.u(3 + j) & 255) << 24) | ((k1.u(4 + j) & 255) << 32) | ((k1.u(5 + j) & 255) << 40) | ((k1.u(6 + j) & 255) << 48);
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }

        public long T() throws IOException {
            long u;
            long j;
            long j2;
            int i;
            long j3 = this.j;
            if (this.i != j3) {
                long j4 = j3 + 1;
                byte u2 = k1.u(j3);
                if (u2 >= 0) {
                    this.j = j4;
                    return u2;
                } else if (this.i - j4 >= 9) {
                    long j5 = j4 + 1;
                    int u3 = u2 ^ (k1.u(j4) << 7);
                    if (u3 >= 0) {
                        long j6 = j5 + 1;
                        int u4 = u3 ^ (k1.u(j5) << 14);
                        if (u4 >= 0) {
                            u = u4 ^ 16256;
                        } else {
                            j5 = j6 + 1;
                            int u5 = u4 ^ (k1.u(j6) << 21);
                            if (u5 < 0) {
                                i = u5 ^ (-2080896);
                            } else {
                                j6 = j5 + 1;
                                long u6 = u5 ^ (k1.u(j5) << 28);
                                if (u6 < 0) {
                                    long j7 = j6 + 1;
                                    long u7 = u6 ^ (k1.u(j6) << 35);
                                    if (u7 < 0) {
                                        j = -34093383808L;
                                    } else {
                                        j6 = j7 + 1;
                                        u6 = u7 ^ (k1.u(j7) << 42);
                                        if (u6 >= 0) {
                                            j2 = 4363953127296L;
                                        } else {
                                            j7 = j6 + 1;
                                            u7 = u6 ^ (k1.u(j6) << 49);
                                            if (u7 < 0) {
                                                j = -558586000294016L;
                                            } else {
                                                j6 = j7 + 1;
                                                u = (u7 ^ (k1.u(j7) << 56)) ^ 71499008037633920L;
                                                if (u < 0) {
                                                    long j8 = 1 + j6;
                                                    if (k1.u(j6) >= 0) {
                                                        j5 = j8;
                                                        this.j = j5;
                                                        return u;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    u = u7 ^ j;
                                    j5 = j7;
                                    this.j = j5;
                                    return u;
                                }
                                j2 = 266354560;
                                u = u6 ^ j2;
                            }
                        }
                        j5 = j6;
                        this.j = j5;
                        return u;
                    }
                    i = u3 ^ (-128);
                    u = i;
                    this.j = j5;
                    return u;
                }
            }
            return U();
        }

        public long U() throws IOException {
            long j = 0;
            for (int i = 0; i < 64; i += 7) {
                byte Q = Q();
                j |= (Q & Byte.MAX_VALUE) << i;
                if ((Q & 128) == 0) {
                    return j;
                }
            }
            throw InvalidProtocolBufferException.malformedVarint();
        }

        public final void V() {
            long j = this.i + this.l;
            this.i = j;
            int i = (int) (j - this.k);
            int i2 = this.o;
            if (i > i2) {
                int i3 = i - i2;
                this.l = i3;
                this.i = j - i3;
                return;
            }
            this.l = 0;
        }

        public final int W() {
            return (int) (this.i - this.j);
        }

        public void X() throws IOException {
            int J;
            do {
                J = J();
                if (J == 0) {
                    return;
                }
            } while (N(J));
        }

        public void Y(int i) throws IOException {
            if (i >= 0 && i <= W()) {
                this.j += i;
            } else if (i < 0) {
                throw InvalidProtocolBufferException.negativeSize();
            } else {
                throw InvalidProtocolBufferException.truncatedMessage();
            }
        }

        public final void Z() throws IOException {
            if (W() >= 10) {
                a0();
            } else {
                b0();
            }
        }

        @Override // com.google.protobuf.j
        public void a(int i) throws InvalidProtocolBufferException {
            if (this.m != i) {
                throw InvalidProtocolBufferException.invalidEndTag();
            }
        }

        public final void a0() throws IOException {
            for (int i = 0; i < 10; i++) {
                long j = this.j;
                this.j = 1 + j;
                if (k1.u(j) >= 0) {
                    return;
                }
            }
            throw InvalidProtocolBufferException.malformedVarint();
        }

        public final void b0() throws IOException {
            for (int i = 0; i < 10; i++) {
                if (Q() >= 0) {
                    return;
                }
            }
            throw InvalidProtocolBufferException.malformedVarint();
        }

        public final ByteBuffer c0(long j, long j2) throws IOException {
            int position = this.f.position();
            int limit = this.f.limit();
            try {
                try {
                    this.f.position(O(j));
                    this.f.limit(O(j2));
                    return this.f.slice();
                } catch (IllegalArgumentException unused) {
                    throw InvalidProtocolBufferException.truncatedMessage();
                }
            } finally {
                this.f.position(position);
                this.f.limit(limit);
            }
        }

        @Override // com.google.protobuf.j
        public int d() {
            int i = this.o;
            if (i == Integer.MAX_VALUE) {
                return -1;
            }
            return i - e();
        }

        @Override // com.google.protobuf.j
        public int e() {
            return (int) (this.j - this.k);
        }

        @Override // com.google.protobuf.j
        public boolean f() throws IOException {
            return this.j == this.i;
        }

        @Override // com.google.protobuf.j
        public void n(int i) {
            this.o = i;
            V();
        }

        @Override // com.google.protobuf.j
        public int o(int i) throws InvalidProtocolBufferException {
            if (i >= 0) {
                int e = i + e();
                int i2 = this.o;
                if (e <= i2) {
                    this.o = e;
                    V();
                    return i2;
                }
                throw InvalidProtocolBufferException.truncatedMessage();
            }
            throw InvalidProtocolBufferException.negativeSize();
        }

        @Override // com.google.protobuf.j
        public boolean p() throws IOException {
            return T() != 0;
        }

        @Override // com.google.protobuf.j
        public ByteString q() throws IOException {
            int B = B();
            if (B <= 0 || B > W()) {
                if (B == 0) {
                    return ByteString.EMPTY;
                }
                if (B < 0) {
                    throw InvalidProtocolBufferException.negativeSize();
                }
                throw InvalidProtocolBufferException.truncatedMessage();
            } else if (this.g && this.n) {
                long j = this.j;
                long j2 = B;
                ByteBuffer c0 = c0(j, j + j2);
                this.j += j2;
                return ByteString.wrap(c0);
            } else {
                byte[] bArr = new byte[B];
                long j3 = B;
                k1.n(this.j, bArr, 0L, j3);
                this.j += j3;
                return ByteString.wrap(bArr);
            }
        }

        @Override // com.google.protobuf.j
        public double r() throws IOException {
            return Double.longBitsToDouble(S());
        }

        @Override // com.google.protobuf.j
        public int s() throws IOException {
            return B();
        }

        @Override // com.google.protobuf.j
        public int t() throws IOException {
            return R();
        }

        @Override // com.google.protobuf.j
        public long u() throws IOException {
            return S();
        }

        @Override // com.google.protobuf.j
        public float v() throws IOException {
            return Float.intBitsToFloat(R());
        }

        @Override // com.google.protobuf.j
        public void w(int i, m0.a aVar, r rVar) throws IOException {
            int i2 = this.a;
            if (i2 < this.b) {
                this.a = i2 + 1;
                aVar.mergeFrom(this, rVar);
                a(WireFormat.c(i, 4));
                this.a--;
                return;
            }
            throw InvalidProtocolBufferException.recursionLimitExceeded();
        }

        @Override // com.google.protobuf.j
        public int x() throws IOException {
            return B();
        }

        @Override // com.google.protobuf.j
        public long y() throws IOException {
            return T();
        }

        @Override // com.google.protobuf.j
        public <T extends m0> T z(t0<T> t0Var, r rVar) throws IOException {
            int B = B();
            if (this.a < this.b) {
                int o = o(B);
                this.a++;
                T parsePartialFrom = t0Var.parsePartialFrom(this, rVar);
                a(0);
                this.a--;
                n(o);
                return parsePartialFrom;
            }
            throw InvalidProtocolBufferException.recursionLimitExceeded();
        }

        public d(ByteBuffer byteBuffer, boolean z) {
            super();
            this.o = Integer.MAX_VALUE;
            this.f = byteBuffer;
            long i = k1.i(byteBuffer);
            this.h = i;
            this.i = byteBuffer.limit() + i;
            long position = i + byteBuffer.position();
            this.j = position;
            this.k = position;
            this.g = z;
        }
    }

    public static int C(int i, InputStream inputStream) throws IOException {
        if ((i & 128) == 0) {
            return i;
        }
        int i2 = i & 127;
        int i3 = 7;
        while (i3 < 32) {
            int read = inputStream.read();
            if (read == -1) {
                throw InvalidProtocolBufferException.truncatedMessage();
            }
            i2 |= (read & 127) << i3;
            if ((read & 128) == 0) {
                return i2;
            }
            i3 += 7;
        }
        while (i3 < 64) {
            int read2 = inputStream.read();
            if (read2 == -1) {
                throw InvalidProtocolBufferException.truncatedMessage();
            }
            if ((read2 & 128) == 0) {
                return i2;
            }
            i3 += 7;
        }
        throw InvalidProtocolBufferException.malformedVarint();
    }

    public static int b(int i) {
        return (-(i & 1)) ^ (i >>> 1);
    }

    public static long c(long j) {
        return (-(j & 1)) ^ (j >>> 1);
    }

    public static j g(InputStream inputStream) {
        return h(inputStream, 4096);
    }

    public static j h(InputStream inputStream, int i) {
        if (i > 0) {
            if (inputStream == null) {
                return k(a0.c);
            }
            return new c(inputStream, i);
        }
        throw new IllegalArgumentException("bufferSize must be > 0");
    }

    public static j i(ByteBuffer byteBuffer) {
        return j(byteBuffer, false);
    }

    public static j j(ByteBuffer byteBuffer, boolean z) {
        if (byteBuffer.hasArray()) {
            return m(byteBuffer.array(), byteBuffer.arrayOffset() + byteBuffer.position(), byteBuffer.remaining(), z);
        }
        if (byteBuffer.isDirect() && d.P()) {
            return new d(byteBuffer, z);
        }
        int remaining = byteBuffer.remaining();
        byte[] bArr = new byte[remaining];
        byteBuffer.duplicate().get(bArr);
        return m(bArr, 0, remaining, true);
    }

    public static j k(byte[] bArr) {
        return l(bArr, 0, bArr.length);
    }

    public static j l(byte[] bArr, int i, int i2) {
        return m(bArr, i, i2, false);
    }

    public static j m(byte[] bArr, int i, int i2, boolean z) {
        b bVar = new b(bArr, i, i2, z);
        try {
            bVar.o(i2);
            return bVar;
        } catch (InvalidProtocolBufferException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public abstract void A(m0.a aVar, r rVar) throws IOException;

    public abstract int B() throws IOException;

    public abstract int D() throws IOException;

    public abstract long E() throws IOException;

    public abstract int F() throws IOException;

    public abstract long G() throws IOException;

    public abstract String H() throws IOException;

    public abstract String I() throws IOException;

    public abstract int J() throws IOException;

    public abstract int K() throws IOException;

    public abstract long L() throws IOException;

    public final boolean M() {
        return this.e;
    }

    public abstract boolean N(int i) throws IOException;

    public abstract void a(int i) throws InvalidProtocolBufferException;

    public abstract int d();

    public abstract int e();

    public abstract boolean f() throws IOException;

    public abstract void n(int i);

    public abstract int o(int i) throws InvalidProtocolBufferException;

    public abstract boolean p() throws IOException;

    public abstract ByteString q() throws IOException;

    public abstract double r() throws IOException;

    public abstract int s() throws IOException;

    public abstract int t() throws IOException;

    public abstract long u() throws IOException;

    public abstract float v() throws IOException;

    public abstract void w(int i, m0.a aVar, r rVar) throws IOException;

    public abstract int x() throws IOException;

    public abstract long y() throws IOException;

    public abstract <T extends m0> T z(t0<T> t0Var, r rVar) throws IOException;

    public j() {
        this.b = 100;
        this.c = Integer.MAX_VALUE;
        this.e = false;
    }
}
