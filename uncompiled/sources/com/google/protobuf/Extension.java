package com.google.protobuf;

import com.google.protobuf.Descriptors;
import com.google.protobuf.m0;

/* loaded from: classes2.dex */
public abstract class Extension<ContainingType extends m0, Type> extends p<ContainingType, Type> {

    /* loaded from: classes2.dex */
    public enum ExtensionType {
        IMMUTABLE,
        MUTABLE,
        PROTO1
    }

    @Override // com.google.protobuf.p
    public final boolean a() {
        return false;
    }

    public abstract Object b(Object obj);

    public abstract Descriptors.FieldDescriptor c();

    public abstract l0 d();

    public abstract Object e(Object obj);
}
