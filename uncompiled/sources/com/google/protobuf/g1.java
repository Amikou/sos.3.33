package com.google.protobuf;

import com.google.protobuf.ByteString;
import com.google.protobuf.Writer;
import com.google.protobuf.m0;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.TreeMap;

/* compiled from: UnknownFieldSet.java */
/* loaded from: classes2.dex */
public final class g1 implements m0 {
    public static final g1 g0 = new g1(Collections.emptyMap(), Collections.emptyMap());
    public static final d h0 = new d();
    public final Map<Integer, c> a;
    public final Map<Integer, c> f0;

    /* compiled from: UnknownFieldSet.java */
    /* loaded from: classes2.dex */
    public static final class b implements m0.a {
        public Map<Integer, c> a;
        public int f0;
        public c.a g0;

        public static b g() {
            b bVar = new b();
            bVar.y();
            return bVar;
        }

        public b b(int i, c cVar) {
            if (i != 0) {
                if (this.g0 != null && this.f0 == i) {
                    this.g0 = null;
                    this.f0 = 0;
                }
                if (this.a.isEmpty()) {
                    this.a = new TreeMap();
                }
                this.a.put(Integer.valueOf(i), cVar);
                return this;
            }
            throw new IllegalArgumentException("Zero is not a valid field number.");
        }

        @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
        /* renamed from: d */
        public g1 build() {
            g1 g1Var;
            j(0);
            if (this.a.isEmpty()) {
                g1Var = g1.c();
            } else {
                g1Var = new g1(Collections.unmodifiableMap(this.a), Collections.unmodifiableMap(((TreeMap) this.a).descendingMap()));
            }
            this.a = null;
            return g1Var;
        }

        @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
        /* renamed from: e */
        public g1 buildPartial() {
            return build();
        }

        /* renamed from: f */
        public b clone() {
            j(0);
            return g1.g().u(new g1(this.a, Collections.unmodifiableMap(((TreeMap) this.a).descendingMap())));
        }

        @Override // defpackage.d82, com.google.protobuf.o0
        /* renamed from: h */
        public g1 getDefaultInstanceForType() {
            return g1.c();
        }

        @Override // defpackage.d82
        public boolean isInitialized() {
            return true;
        }

        public final c.a j(int i) {
            c.a aVar = this.g0;
            if (aVar != null) {
                int i2 = this.f0;
                if (i == i2) {
                    return aVar;
                }
                b(i2, aVar.g());
            }
            if (i == 0) {
                return null;
            }
            c cVar = this.a.get(Integer.valueOf(i));
            this.f0 = i;
            c.a t = c.t();
            this.g0 = t;
            if (cVar != null) {
                t.i(cVar);
            }
            return this.g0;
        }

        public boolean k(int i) {
            if (i != 0) {
                return i == this.f0 || this.a.containsKey(Integer.valueOf(i));
            }
            throw new IllegalArgumentException("Zero is not a valid field number.");
        }

        public b l(int i, c cVar) {
            if (i != 0) {
                if (k(i)) {
                    j(i).i(cVar);
                } else {
                    b(i, cVar);
                }
                return this;
            }
            throw new IllegalArgumentException("Zero is not a valid field number.");
        }

        public boolean o(int i, j jVar) throws IOException {
            int a = WireFormat.a(i);
            int b = WireFormat.b(i);
            if (b == 0) {
                j(a).f(jVar.y());
                return true;
            } else if (b == 1) {
                j(a).c(jVar.u());
                return true;
            } else if (b == 2) {
                j(a).e(jVar.q());
                return true;
            } else if (b == 3) {
                b g = g1.g();
                jVar.w(a, g, q.f());
                j(a).d(g.build());
                return true;
            } else if (b != 4) {
                if (b == 5) {
                    j(a).b(jVar.t());
                    return true;
                }
                throw InvalidProtocolBufferException.invalidWireType();
            } else {
                return false;
            }
        }

        public b p(ByteString byteString) throws InvalidProtocolBufferException {
            try {
                j newCodedInput = byteString.newCodedInput();
                r(newCodedInput);
                newCodedInput.a(0);
                return this;
            } catch (InvalidProtocolBufferException e) {
                throw e;
            } catch (IOException e2) {
                throw new RuntimeException("Reading from a ByteString threw an IOException (should never happen).", e2);
            }
        }

        public b r(j jVar) throws IOException {
            int J;
            do {
                J = jVar.J();
                if (J == 0) {
                    break;
                }
            } while (o(J, jVar));
            return this;
        }

        @Override // com.google.protobuf.m0.a
        /* renamed from: s */
        public b mergeFrom(j jVar, r rVar) throws IOException {
            return r(jVar);
        }

        @Override // com.google.protobuf.m0.a
        /* renamed from: t */
        public b mergeFrom(m0 m0Var) {
            if (m0Var instanceof g1) {
                return u((g1) m0Var);
            }
            throw new IllegalArgumentException("mergeFrom(MessageLite) can only merge messages of the same type.");
        }

        public b u(g1 g1Var) {
            if (g1Var != g1.c()) {
                for (Map.Entry entry : g1Var.a.entrySet()) {
                    l(((Integer) entry.getKey()).intValue(), (c) entry.getValue());
                }
            }
            return this;
        }

        @Override // com.google.protobuf.m0.a
        /* renamed from: v */
        public b mergeFrom(byte[] bArr) throws InvalidProtocolBufferException {
            try {
                j k = j.k(bArr);
                r(k);
                k.a(0);
                return this;
            } catch (InvalidProtocolBufferException e) {
                throw e;
            } catch (IOException e2) {
                throw new RuntimeException("Reading from a byte array threw an IOException (should never happen).", e2);
            }
        }

        public b x(int i, int i2) {
            if (i != 0) {
                j(i).f(i2);
                return this;
            }
            throw new IllegalArgumentException("Zero is not a valid field number.");
        }

        public final void y() {
            this.a = Collections.emptyMap();
            this.f0 = 0;
            this.g0 = null;
        }
    }

    /* compiled from: UnknownFieldSet.java */
    /* loaded from: classes2.dex */
    public static final class c {
        public List<Long> a;
        public List<Integer> b;
        public List<Long> c;
        public List<ByteString> d;
        public List<g1> e;

        /* compiled from: UnknownFieldSet.java */
        /* loaded from: classes2.dex */
        public static final class a {
            public c a;

            public static a h() {
                a aVar = new a();
                aVar.a = new c();
                return aVar;
            }

            public a b(int i) {
                if (this.a.b == null) {
                    this.a.b = new ArrayList();
                }
                this.a.b.add(Integer.valueOf(i));
                return this;
            }

            public a c(long j) {
                if (this.a.c == null) {
                    this.a.c = new ArrayList();
                }
                this.a.c.add(Long.valueOf(j));
                return this;
            }

            public a d(g1 g1Var) {
                if (this.a.e == null) {
                    this.a.e = new ArrayList();
                }
                this.a.e.add(g1Var);
                return this;
            }

            public a e(ByteString byteString) {
                if (this.a.d == null) {
                    this.a.d = new ArrayList();
                }
                this.a.d.add(byteString);
                return this;
            }

            public a f(long j) {
                if (this.a.a == null) {
                    this.a.a = new ArrayList();
                }
                this.a.a.add(Long.valueOf(j));
                return this;
            }

            public c g() {
                if (this.a.a == null) {
                    this.a.a = Collections.emptyList();
                } else {
                    c cVar = this.a;
                    cVar.a = Collections.unmodifiableList(cVar.a);
                }
                if (this.a.b == null) {
                    this.a.b = Collections.emptyList();
                } else {
                    c cVar2 = this.a;
                    cVar2.b = Collections.unmodifiableList(cVar2.b);
                }
                if (this.a.c == null) {
                    this.a.c = Collections.emptyList();
                } else {
                    c cVar3 = this.a;
                    cVar3.c = Collections.unmodifiableList(cVar3.c);
                }
                if (this.a.d == null) {
                    this.a.d = Collections.emptyList();
                } else {
                    c cVar4 = this.a;
                    cVar4.d = Collections.unmodifiableList(cVar4.d);
                }
                if (this.a.e == null) {
                    this.a.e = Collections.emptyList();
                } else {
                    c cVar5 = this.a;
                    cVar5.e = Collections.unmodifiableList(cVar5.e);
                }
                c cVar6 = this.a;
                this.a = null;
                return cVar6;
            }

            public a i(c cVar) {
                if (!cVar.a.isEmpty()) {
                    if (this.a.a == null) {
                        this.a.a = new ArrayList();
                    }
                    this.a.a.addAll(cVar.a);
                }
                if (!cVar.b.isEmpty()) {
                    if (this.a.b == null) {
                        this.a.b = new ArrayList();
                    }
                    this.a.b.addAll(cVar.b);
                }
                if (!cVar.c.isEmpty()) {
                    if (this.a.c == null) {
                        this.a.c = new ArrayList();
                    }
                    this.a.c.addAll(cVar.c);
                }
                if (!cVar.d.isEmpty()) {
                    if (this.a.d == null) {
                        this.a.d = new ArrayList();
                    }
                    this.a.d.addAll(cVar.d);
                }
                if (!cVar.e.isEmpty()) {
                    if (this.a.e == null) {
                        this.a.e = new ArrayList();
                    }
                    this.a.e.addAll(cVar.e);
                }
                return this;
            }
        }

        static {
            t().g();
        }

        public static a t() {
            return a.h();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof c) {
                return Arrays.equals(o(), ((c) obj).o());
            }
            return false;
        }

        public int hashCode() {
            return Arrays.hashCode(o());
        }

        public List<Integer> l() {
            return this.b;
        }

        public List<Long> m() {
            return this.c;
        }

        public List<g1> n() {
            return this.e;
        }

        public final Object[] o() {
            return new Object[]{this.a, this.b, this.c, this.d, this.e};
        }

        public List<ByteString> p() {
            return this.d;
        }

        public int q(int i) {
            int i2 = 0;
            for (Long l : this.a) {
                i2 += CodedOutputStream.a0(i, l.longValue());
            }
            for (Integer num : this.b) {
                i2 += CodedOutputStream.n(i, num.intValue());
            }
            for (Long l2 : this.c) {
                i2 += CodedOutputStream.p(i, l2.longValue());
            }
            for (ByteString byteString : this.d) {
                i2 += CodedOutputStream.h(i, byteString);
            }
            for (g1 g1Var : this.e) {
                i2 += CodedOutputStream.t(i, g1Var);
            }
            return i2;
        }

        public int r(int i) {
            int i2 = 0;
            for (ByteString byteString : this.d) {
                i2 += CodedOutputStream.L(i, byteString);
            }
            return i2;
        }

        public List<Long> s() {
            return this.a;
        }

        public void u(int i, CodedOutputStream codedOutputStream) throws IOException {
            for (ByteString byteString : this.d) {
                codedOutputStream.O0(i, byteString);
            }
        }

        public final void v(int i, Writer writer) throws IOException {
            if (writer.i() == Writer.FieldOrder.DESCENDING) {
                List<ByteString> list = this.d;
                ListIterator<ByteString> listIterator = list.listIterator(list.size());
                while (listIterator.hasPrevious()) {
                    writer.c(i, listIterator.previous());
                }
                return;
            }
            for (ByteString byteString : this.d) {
                writer.c(i, byteString);
            }
        }

        public void w(int i, CodedOutputStream codedOutputStream) throws IOException {
            for (Long l : this.a) {
                codedOutputStream.d1(i, l.longValue());
            }
            for (Integer num : this.b) {
                codedOutputStream.w0(i, num.intValue());
            }
            for (Long l2 : this.c) {
                codedOutputStream.y0(i, l2.longValue());
            }
            for (ByteString byteString : this.d) {
                codedOutputStream.q0(i, byteString);
            }
            for (g1 g1Var : this.e) {
                codedOutputStream.C0(i, g1Var);
            }
        }

        public void x(int i, Writer writer) throws IOException {
            writer.E(i, this.a, false);
            writer.v(i, this.b, false);
            writer.s(i, this.c, false);
            writer.I(i, this.d);
            if (writer.i() == Writer.FieldOrder.ASCENDING) {
                for (int i2 = 0; i2 < this.e.size(); i2++) {
                    writer.q(i);
                    this.e.get(i2).n(writer);
                    writer.B(i);
                }
                return;
            }
            for (int size = this.e.size() - 1; size >= 0; size--) {
                writer.B(i);
                this.e.get(size).n(writer);
                writer.q(i);
            }
        }

        public c() {
        }
    }

    /* compiled from: UnknownFieldSet.java */
    /* loaded from: classes2.dex */
    public static final class d extends com.google.protobuf.c<g1> {
        @Override // com.google.protobuf.t0
        /* renamed from: a */
        public g1 parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
            b g = g1.g();
            try {
                g.r(jVar);
                return g.buildPartial();
            } catch (InvalidProtocolBufferException e) {
                throw e.setUnfinishedMessage(g.buildPartial());
            } catch (IOException e2) {
                throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(g.buildPartial());
            }
        }
    }

    public g1() {
        this.a = null;
        this.f0 = null;
    }

    public static g1 c() {
        return g0;
    }

    public static b g() {
        return b.g();
    }

    public static b h(g1 g1Var) {
        return g().u(g1Var);
    }

    public static g1 j(ByteString byteString) throws InvalidProtocolBufferException {
        return g().p(byteString).build();
    }

    public Map<Integer, c> b() {
        return this.a;
    }

    @Override // defpackage.d82, com.google.protobuf.o0
    /* renamed from: d */
    public g1 getDefaultInstanceForType() {
        return g0;
    }

    @Override // com.google.protobuf.m0
    /* renamed from: e */
    public final d getParserForType() {
        return h0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return (obj instanceof g1) && this.a.equals(((g1) obj).a);
    }

    public int f() {
        int i = 0;
        for (Map.Entry<Integer, c> entry : this.a.entrySet()) {
            i += entry.getValue().r(entry.getKey().intValue());
        }
        return i;
    }

    @Override // com.google.protobuf.m0
    public int getSerializedSize() {
        int i = 0;
        for (Map.Entry<Integer, c> entry : this.a.entrySet()) {
            i += entry.getValue().q(entry.getKey().intValue());
        }
        return i;
    }

    public int hashCode() {
        return this.a.hashCode();
    }

    @Override // com.google.protobuf.m0, com.google.protobuf.l0
    /* renamed from: i */
    public b newBuilderForType() {
        return g();
    }

    @Override // defpackage.d82
    public boolean isInitialized() {
        return true;
    }

    @Override // com.google.protobuf.m0, com.google.protobuf.l0
    /* renamed from: k */
    public b toBuilder() {
        return g().u(this);
    }

    public void l(CodedOutputStream codedOutputStream) throws IOException {
        for (Map.Entry<Integer, c> entry : this.a.entrySet()) {
            entry.getValue().u(entry.getKey().intValue(), codedOutputStream);
        }
    }

    public void m(Writer writer) throws IOException {
        if (writer.i() == Writer.FieldOrder.DESCENDING) {
            for (Map.Entry<Integer, c> entry : this.f0.entrySet()) {
                entry.getValue().v(entry.getKey().intValue(), writer);
            }
            return;
        }
        for (Map.Entry<Integer, c> entry2 : this.a.entrySet()) {
            entry2.getValue().v(entry2.getKey().intValue(), writer);
        }
    }

    public void n(Writer writer) throws IOException {
        if (writer.i() == Writer.FieldOrder.DESCENDING) {
            for (Map.Entry<Integer, c> entry : this.f0.entrySet()) {
                entry.getValue().x(entry.getKey().intValue(), writer);
            }
            return;
        }
        for (Map.Entry<Integer, c> entry2 : this.a.entrySet()) {
            entry2.getValue().x(entry2.getKey().intValue(), writer);
        }
    }

    @Override // com.google.protobuf.m0
    public byte[] toByteArray() {
        try {
            byte[] bArr = new byte[getSerializedSize()];
            CodedOutputStream i0 = CodedOutputStream.i0(bArr);
            writeTo(i0);
            i0.d();
            return bArr;
        } catch (IOException e) {
            throw new RuntimeException("Serializing to a byte array threw an IOException (should never happen).", e);
        }
    }

    @Override // com.google.protobuf.m0
    public ByteString toByteString() {
        try {
            ByteString.g newCodedBuilder = ByteString.newCodedBuilder(getSerializedSize());
            writeTo(newCodedBuilder.b());
            return newCodedBuilder.a();
        } catch (IOException e) {
            throw new RuntimeException("Serializing to a ByteString threw an IOException (should never happen).", e);
        }
    }

    public String toString() {
        return TextFormat.o().k(this);
    }

    @Override // com.google.protobuf.m0
    public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
        for (Map.Entry<Integer, c> entry : this.a.entrySet()) {
            entry.getValue().w(entry.getKey().intValue(), codedOutputStream);
        }
    }

    public g1(Map<Integer, c> map, Map<Integer, c> map2) {
        this.a = map;
        this.f0 = map2;
    }
}
