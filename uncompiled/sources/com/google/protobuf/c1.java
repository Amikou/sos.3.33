package com.google.protobuf;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* compiled from: StructuralMessageInfo.java */
/* loaded from: classes2.dex */
public final class c1 implements z72 {
    public final ProtoSyntax a;
    public final boolean b;
    public final int[] c;
    public final v[] d;
    public final m0 e;

    /* compiled from: StructuralMessageInfo.java */
    /* loaded from: classes2.dex */
    public static final class a {
        public final List<v> a;
        public ProtoSyntax b;
        public boolean c;
        public boolean d;
        public int[] e = null;
        public Object f;

        public a(int i) {
            this.a = new ArrayList(i);
        }

        public c1 a() {
            if (!this.c) {
                if (this.b != null) {
                    this.c = true;
                    Collections.sort(this.a);
                    return new c1(this.b, this.d, this.e, (v[]) this.a.toArray(new v[0]), this.f);
                }
                throw new IllegalStateException("Must specify a proto syntax");
            }
            throw new IllegalStateException("Builder can only build once");
        }

        public void b(int[] iArr) {
            this.e = iArr;
        }

        public void c(Object obj) {
            this.f = obj;
        }

        public void d(v vVar) {
            if (!this.c) {
                this.a.add(vVar);
                return;
            }
            throw new IllegalStateException("Builder can only build once");
        }

        public void e(boolean z) {
            this.d = z;
        }

        public void f(ProtoSyntax protoSyntax) {
            this.b = (ProtoSyntax) a0.b(protoSyntax, "syntax");
        }
    }

    public c1(ProtoSyntax protoSyntax, boolean z, int[] iArr, v[] vVarArr, Object obj) {
        this.a = protoSyntax;
        this.b = z;
        this.c = iArr;
        this.d = vVarArr;
        this.e = (m0) a0.b(obj, "defaultInstance");
    }

    public static a f(int i) {
        return new a(i);
    }

    @Override // defpackage.z72
    public boolean a() {
        return this.b;
    }

    @Override // defpackage.z72
    public m0 b() {
        return this.e;
    }

    @Override // defpackage.z72
    public ProtoSyntax c() {
        return this.a;
    }

    public int[] d() {
        return this.c;
    }

    public v[] e() {
        return this.d;
    }
}
