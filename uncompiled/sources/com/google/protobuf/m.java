package com.google.protobuf;

import com.google.protobuf.Descriptors;
import com.google.protobuf.a0;
import com.google.protobuf.c1;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: DescriptorMessageInfoFactory.java */
/* loaded from: classes2.dex */
public final class m implements b82 {
    public static final Set<String> a;
    public static d b;

    /* compiled from: DescriptorMessageInfoFactory.java */
    /* loaded from: classes2.dex */
    public static class a implements a0.e {
        public final /* synthetic */ Descriptors.FieldDescriptor a;

        public a(Descriptors.FieldDescriptor fieldDescriptor) {
            this.a = fieldDescriptor;
        }

        @Override // com.google.protobuf.a0.e
        public boolean a(int i) {
            return this.a.s().findValueByNumber(i) != null;
        }
    }

    /* compiled from: DescriptorMessageInfoFactory.java */
    /* loaded from: classes2.dex */
    public static class b implements a0.e {
        public final /* synthetic */ Descriptors.FieldDescriptor a;

        public b(Descriptors.FieldDescriptor fieldDescriptor) {
            this.a = fieldDescriptor;
        }

        @Override // com.google.protobuf.a0.e
        public boolean a(int i) {
            return this.a.s().findValueByNumber(i) != null;
        }
    }

    /* compiled from: DescriptorMessageInfoFactory.java */
    /* loaded from: classes2.dex */
    public static /* synthetic */ class c {
        public static final /* synthetic */ int[] a;
        public static final /* synthetic */ int[] b;
        public static final /* synthetic */ int[] c;

        static {
            int[] iArr = new int[Descriptors.FieldDescriptor.Type.values().length];
            c = iArr;
            try {
                iArr[Descriptors.FieldDescriptor.Type.BOOL.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                c[Descriptors.FieldDescriptor.Type.BYTES.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                c[Descriptors.FieldDescriptor.Type.DOUBLE.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                c[Descriptors.FieldDescriptor.Type.ENUM.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                c[Descriptors.FieldDescriptor.Type.FIXED32.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                c[Descriptors.FieldDescriptor.Type.FIXED64.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                c[Descriptors.FieldDescriptor.Type.FLOAT.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                c[Descriptors.FieldDescriptor.Type.GROUP.ordinal()] = 8;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                c[Descriptors.FieldDescriptor.Type.INT32.ordinal()] = 9;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                c[Descriptors.FieldDescriptor.Type.INT64.ordinal()] = 10;
            } catch (NoSuchFieldError unused10) {
            }
            try {
                c[Descriptors.FieldDescriptor.Type.MESSAGE.ordinal()] = 11;
            } catch (NoSuchFieldError unused11) {
            }
            try {
                c[Descriptors.FieldDescriptor.Type.SFIXED32.ordinal()] = 12;
            } catch (NoSuchFieldError unused12) {
            }
            try {
                c[Descriptors.FieldDescriptor.Type.SFIXED64.ordinal()] = 13;
            } catch (NoSuchFieldError unused13) {
            }
            try {
                c[Descriptors.FieldDescriptor.Type.SINT32.ordinal()] = 14;
            } catch (NoSuchFieldError unused14) {
            }
            try {
                c[Descriptors.FieldDescriptor.Type.SINT64.ordinal()] = 15;
            } catch (NoSuchFieldError unused15) {
            }
            try {
                c[Descriptors.FieldDescriptor.Type.STRING.ordinal()] = 16;
            } catch (NoSuchFieldError unused16) {
            }
            try {
                c[Descriptors.FieldDescriptor.Type.UINT32.ordinal()] = 17;
            } catch (NoSuchFieldError unused17) {
            }
            try {
                c[Descriptors.FieldDescriptor.Type.UINT64.ordinal()] = 18;
            } catch (NoSuchFieldError unused18) {
            }
            int[] iArr2 = new int[JavaType.values().length];
            b = iArr2;
            try {
                iArr2[JavaType.BOOLEAN.ordinal()] = 1;
            } catch (NoSuchFieldError unused19) {
            }
            try {
                b[JavaType.BYTE_STRING.ordinal()] = 2;
            } catch (NoSuchFieldError unused20) {
            }
            try {
                b[JavaType.DOUBLE.ordinal()] = 3;
            } catch (NoSuchFieldError unused21) {
            }
            try {
                b[JavaType.FLOAT.ordinal()] = 4;
            } catch (NoSuchFieldError unused22) {
            }
            try {
                b[JavaType.ENUM.ordinal()] = 5;
            } catch (NoSuchFieldError unused23) {
            }
            try {
                b[JavaType.INT.ordinal()] = 6;
            } catch (NoSuchFieldError unused24) {
            }
            try {
                b[JavaType.LONG.ordinal()] = 7;
            } catch (NoSuchFieldError unused25) {
            }
            try {
                b[JavaType.STRING.ordinal()] = 8;
            } catch (NoSuchFieldError unused26) {
            }
            try {
                b[JavaType.MESSAGE.ordinal()] = 9;
            } catch (NoSuchFieldError unused27) {
            }
            int[] iArr3 = new int[Descriptors.FileDescriptor.Syntax.values().length];
            a = iArr3;
            try {
                iArr3[Descriptors.FileDescriptor.Syntax.PROTO2.ordinal()] = 1;
            } catch (NoSuchFieldError unused28) {
            }
            try {
                a[Descriptors.FileDescriptor.Syntax.PROTO3.ordinal()] = 2;
            } catch (NoSuchFieldError unused29) {
            }
        }
    }

    static {
        new m();
        a = new HashSet(Arrays.asList("cached_size", "serialized_size", "class"));
        b = new d();
    }

    public static Field e(Class<?> cls, int i) {
        return m(cls, "bitField" + i + "_");
    }

    public static v f(Class<?> cls, Descriptors.FieldDescriptor fieldDescriptor, e eVar, boolean z, a0.e eVar2) {
        en2 a2 = eVar.a(cls, fieldDescriptor.o());
        FieldType q = q(fieldDescriptor);
        return v.h(fieldDescriptor.getNumber(), q, a2, r(cls, fieldDescriptor, q), z, eVar2);
    }

    public static Field g(Class<?> cls, Descriptors.FieldDescriptor fieldDescriptor) {
        return m(cls, n(fieldDescriptor));
    }

    public static z72 h(Class<?> cls, Descriptors.b bVar) {
        int i = c.a[bVar.a().t().ordinal()];
        if (i != 1) {
            if (i == 2) {
                return j(cls, bVar);
            }
            throw new IllegalArgumentException("Unsupported syntax: " + bVar.a().t());
        }
        return i(cls, bVar);
    }

    public static c1 i(Class<?> cls, Descriptors.b bVar) {
        List<Descriptors.FieldDescriptor> p = bVar.p();
        c1.a f = c1.f(p.size());
        f.c(o(cls));
        f.f(ProtoSyntax.PROTO2);
        f.e(bVar.t().getMessageSetWireFormat());
        a0.e eVar = null;
        e eVar2 = new e(null);
        Field field = null;
        int i = 0;
        int i2 = 0;
        int i3 = 1;
        while (i < p.size()) {
            Descriptors.FieldDescriptor fieldDescriptor = p.get(i);
            boolean javaStringCheckUtf8 = fieldDescriptor.a().p().getJavaStringCheckUtf8();
            Descriptors.FieldDescriptor.JavaType v = fieldDescriptor.v();
            Descriptors.FieldDescriptor.JavaType javaType = Descriptors.FieldDescriptor.JavaType.ENUM;
            a0.e aVar = v == javaType ? new a(fieldDescriptor) : eVar;
            if (fieldDescriptor.o() != null) {
                f.d(f(cls, fieldDescriptor, eVar2, javaStringCheckUtf8, aVar));
            } else {
                Field l = l(cls, fieldDescriptor);
                int number = fieldDescriptor.getNumber();
                FieldType q = q(fieldDescriptor);
                if (fieldDescriptor.D()) {
                    Descriptors.FieldDescriptor k = fieldDescriptor.x().k(2);
                    if (k.v() == javaType) {
                        aVar = new b(k);
                    }
                    f.d(v.g(l, number, z0.C(cls, fieldDescriptor.e()), aVar));
                } else if (!fieldDescriptor.i()) {
                    if (field == null) {
                        field = e(cls, i2);
                    }
                    if (fieldDescriptor.H()) {
                        f.d(v.o(l, number, q, field, i3, javaStringCheckUtf8, aVar));
                    } else {
                        f.d(v.l(l, number, q, field, i3, javaStringCheckUtf8, aVar));
                    }
                } else if (aVar != null) {
                    if (fieldDescriptor.isPacked()) {
                        f.d(v.k(l, number, q, aVar, g(cls, fieldDescriptor)));
                    } else {
                        f.d(v.f(l, number, q, aVar));
                    }
                } else if (fieldDescriptor.v() == Descriptors.FieldDescriptor.JavaType.MESSAGE) {
                    f.d(v.p(l, number, q, t(cls, fieldDescriptor)));
                } else if (fieldDescriptor.isPacked()) {
                    f.d(v.j(l, number, q, g(cls, fieldDescriptor)));
                } else {
                    f.d(v.e(l, number, q, javaStringCheckUtf8));
                }
                i++;
                eVar = null;
            }
            i3 <<= 1;
            if (i3 == 0) {
                i2++;
                i3 = 1;
                field = null;
            }
            i++;
            eVar = null;
        }
        ArrayList arrayList = new ArrayList();
        for (int i4 = 0; i4 < p.size(); i4++) {
            Descriptors.FieldDescriptor fieldDescriptor2 = p.get(i4);
            if (fieldDescriptor2.H() || (fieldDescriptor2.v() == Descriptors.FieldDescriptor.JavaType.MESSAGE && v(fieldDescriptor2.x()))) {
                arrayList.add(Integer.valueOf(fieldDescriptor2.getNumber()));
            }
        }
        int[] iArr = new int[arrayList.size()];
        for (int i5 = 0; i5 < arrayList.size(); i5++) {
            iArr[i5] = ((Integer) arrayList.get(i5)).intValue();
        }
        f.b(iArr);
        return f.a();
    }

    public static c1 j(Class<?> cls, Descriptors.b bVar) {
        List<Descriptors.FieldDescriptor> p = bVar.p();
        c1.a f = c1.f(p.size());
        f.c(o(cls));
        f.f(ProtoSyntax.PROTO3);
        e eVar = new e(null);
        for (int i = 0; i < p.size(); i++) {
            Descriptors.FieldDescriptor fieldDescriptor = p.get(i);
            if (fieldDescriptor.o() != null) {
                f.d(f(cls, fieldDescriptor, eVar, true, null));
            } else if (fieldDescriptor.D()) {
                f.d(v.g(l(cls, fieldDescriptor), fieldDescriptor.getNumber(), z0.C(cls, fieldDescriptor.e()), null));
            } else if (fieldDescriptor.i() && fieldDescriptor.v() == Descriptors.FieldDescriptor.JavaType.MESSAGE) {
                f.d(v.p(l(cls, fieldDescriptor), fieldDescriptor.getNumber(), q(fieldDescriptor), t(cls, fieldDescriptor)));
            } else if (fieldDescriptor.isPacked()) {
                f.d(v.j(l(cls, fieldDescriptor), fieldDescriptor.getNumber(), q(fieldDescriptor), g(cls, fieldDescriptor)));
            } else {
                f.d(v.e(l(cls, fieldDescriptor), fieldDescriptor.getNumber(), q(fieldDescriptor), true));
            }
        }
        return f.a();
    }

    public static Descriptors.b k(Class<?> cls) {
        return o(cls).getDescriptorForType();
    }

    public static Field l(Class<?> cls, Descriptors.FieldDescriptor fieldDescriptor) {
        return m(cls, p(fieldDescriptor));
    }

    public static Field m(Class<?> cls, String str) {
        try {
            return cls.getDeclaredField(str);
        } catch (Exception unused) {
            throw new IllegalArgumentException("Unable to find field " + str + " in message class " + cls.getName());
        }
    }

    public static String n(Descriptors.FieldDescriptor fieldDescriptor) {
        return w(fieldDescriptor.e()) + "MemoizedSerializedSize";
    }

    public static l0 o(Class<?> cls) {
        try {
            return (l0) cls.getDeclaredMethod("getDefaultInstance", new Class[0]).invoke(null, new Object[0]);
        } catch (Exception e2) {
            throw new IllegalArgumentException("Unable to get default instance for message class " + cls.getName(), e2);
        }
    }

    public static String p(Descriptors.FieldDescriptor fieldDescriptor) {
        String e2;
        if (fieldDescriptor.A() == Descriptors.FieldDescriptor.Type.GROUP) {
            e2 = fieldDescriptor.x().e();
        } else {
            e2 = fieldDescriptor.e();
        }
        String str = a.contains(e2) ? "__" : "_";
        return w(e2) + str;
    }

    public static FieldType q(Descriptors.FieldDescriptor fieldDescriptor) {
        switch (c.c[fieldDescriptor.A().ordinal()]) {
            case 1:
                if (fieldDescriptor.i()) {
                    return fieldDescriptor.isPacked() ? FieldType.BOOL_LIST_PACKED : FieldType.BOOL_LIST;
                }
                return FieldType.BOOL;
            case 2:
                return fieldDescriptor.i() ? FieldType.BYTES_LIST : FieldType.BYTES;
            case 3:
                if (fieldDescriptor.i()) {
                    return fieldDescriptor.isPacked() ? FieldType.DOUBLE_LIST_PACKED : FieldType.DOUBLE_LIST;
                }
                return FieldType.DOUBLE;
            case 4:
                if (fieldDescriptor.i()) {
                    return fieldDescriptor.isPacked() ? FieldType.ENUM_LIST_PACKED : FieldType.ENUM_LIST;
                }
                return FieldType.ENUM;
            case 5:
                if (fieldDescriptor.i()) {
                    return fieldDescriptor.isPacked() ? FieldType.FIXED32_LIST_PACKED : FieldType.FIXED32_LIST;
                }
                return FieldType.FIXED32;
            case 6:
                if (fieldDescriptor.i()) {
                    return fieldDescriptor.isPacked() ? FieldType.FIXED64_LIST_PACKED : FieldType.FIXED64_LIST;
                }
                return FieldType.FIXED64;
            case 7:
                if (fieldDescriptor.i()) {
                    return fieldDescriptor.isPacked() ? FieldType.FLOAT_LIST_PACKED : FieldType.FLOAT_LIST;
                }
                return FieldType.FLOAT;
            case 8:
                return fieldDescriptor.i() ? FieldType.GROUP_LIST : FieldType.GROUP;
            case 9:
                if (fieldDescriptor.i()) {
                    return fieldDescriptor.isPacked() ? FieldType.INT32_LIST_PACKED : FieldType.INT32_LIST;
                }
                return FieldType.INT32;
            case 10:
                if (fieldDescriptor.i()) {
                    return fieldDescriptor.isPacked() ? FieldType.INT64_LIST_PACKED : FieldType.INT64_LIST;
                }
                return FieldType.INT64;
            case 11:
                if (fieldDescriptor.D()) {
                    return FieldType.MAP;
                }
                return fieldDescriptor.i() ? FieldType.MESSAGE_LIST : FieldType.MESSAGE;
            case 12:
                if (fieldDescriptor.i()) {
                    return fieldDescriptor.isPacked() ? FieldType.SFIXED32_LIST_PACKED : FieldType.SFIXED32_LIST;
                }
                return FieldType.SFIXED32;
            case 13:
                if (fieldDescriptor.i()) {
                    return fieldDescriptor.isPacked() ? FieldType.SFIXED64_LIST_PACKED : FieldType.SFIXED64_LIST;
                }
                return FieldType.SFIXED64;
            case 14:
                if (fieldDescriptor.i()) {
                    return fieldDescriptor.isPacked() ? FieldType.SINT32_LIST_PACKED : FieldType.SINT32_LIST;
                }
                return FieldType.SINT32;
            case 15:
                if (fieldDescriptor.i()) {
                    return fieldDescriptor.isPacked() ? FieldType.SINT64_LIST_PACKED : FieldType.SINT64_LIST;
                }
                return FieldType.SINT64;
            case 16:
                return fieldDescriptor.i() ? FieldType.STRING_LIST : FieldType.STRING;
            case 17:
                if (fieldDescriptor.i()) {
                    return fieldDescriptor.isPacked() ? FieldType.UINT32_LIST_PACKED : FieldType.UINT32_LIST;
                }
                return FieldType.UINT32;
            case 18:
                if (fieldDescriptor.i()) {
                    return fieldDescriptor.isPacked() ? FieldType.UINT64_LIST_PACKED : FieldType.UINT64_LIST;
                }
                return FieldType.UINT64;
            default:
                throw new IllegalArgumentException("Unsupported field type: " + fieldDescriptor.A());
        }
    }

    public static Class<?> r(Class<?> cls, Descriptors.FieldDescriptor fieldDescriptor, FieldType fieldType) {
        switch (c.b[fieldType.getJavaType().ordinal()]) {
            case 1:
                return Boolean.class;
            case 2:
                return ByteString.class;
            case 3:
                return Double.class;
            case 4:
                return Float.class;
            case 5:
            case 6:
                return Integer.class;
            case 7:
                return Long.class;
            case 8:
                return String.class;
            case 9:
                return s(cls, fieldDescriptor);
            default:
                throw new IllegalArgumentException("Invalid type for oneof: " + fieldType);
        }
    }

    public static Class<?> s(Class<?> cls, Descriptors.FieldDescriptor fieldDescriptor) {
        try {
            return cls.getDeclaredMethod(u(fieldDescriptor.A() == Descriptors.FieldDescriptor.Type.GROUP ? fieldDescriptor.x().e() : fieldDescriptor.e()), new Class[0]).getReturnType();
        } catch (Exception e2) {
            throw new RuntimeException(e2);
        }
    }

    public static Class<?> t(Class<?> cls, Descriptors.FieldDescriptor fieldDescriptor) {
        try {
            return cls.getDeclaredMethod(u(fieldDescriptor.A() == Descriptors.FieldDescriptor.Type.GROUP ? fieldDescriptor.x().e() : fieldDescriptor.e()), Integer.TYPE).getReturnType();
        } catch (Exception e2) {
            throw new RuntimeException(e2);
        }
    }

    public static String u(String str) {
        String w = w(str);
        return "get" + Character.toUpperCase(w.charAt(0)) + w.substring(1, w.length());
    }

    public static boolean v(Descriptors.b bVar) {
        return b.c(bVar);
    }

    public static String w(String str) {
        StringBuilder sb = new StringBuilder(str.length() + 1);
        boolean z = false;
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (charAt != '_') {
                if (Character.isDigit(charAt)) {
                    sb.append(charAt);
                } else {
                    if (z) {
                        sb.append(Character.toUpperCase(charAt));
                        z = false;
                    } else if (i == 0) {
                        sb.append(Character.toLowerCase(charAt));
                    } else {
                        sb.append(charAt);
                    }
                }
            }
            z = true;
        }
        return sb.toString();
    }

    @Override // defpackage.b82
    public z72 a(Class<?> cls) {
        if (GeneratedMessageV3.class.isAssignableFrom(cls)) {
            return h(cls, k(cls));
        }
        throw new IllegalArgumentException("Unsupported message type: " + cls.getName());
    }

    @Override // defpackage.b82
    public boolean b(Class<?> cls) {
        return GeneratedMessageV3.class.isAssignableFrom(cls);
    }

    /* compiled from: DescriptorMessageInfoFactory.java */
    /* loaded from: classes2.dex */
    public static class d {
        public final Map<Descriptors.b, Boolean> a = new ConcurrentHashMap();
        public int b = 0;
        public final Stack<a> c = new Stack<>();
        public final Map<Descriptors.b, a> d = new HashMap();

        /* compiled from: DescriptorMessageInfoFactory.java */
        /* loaded from: classes2.dex */
        public static class a {
            public final Descriptors.b a;
            public final int b;
            public int c;
            public b d = null;

            public a(Descriptors.b bVar, int i) {
                this.a = bVar;
                this.b = i;
                this.c = i;
            }
        }

        public final void a(b bVar) {
            boolean z;
            b bVar2;
            Iterator<Descriptors.b> it = bVar.a.iterator();
            loop0: while (true) {
                z = true;
                if (!it.hasNext()) {
                    z = false;
                    break;
                }
                Descriptors.b next = it.next();
                if (next.u()) {
                    break;
                }
                for (Descriptors.FieldDescriptor fieldDescriptor : next.p()) {
                    if (fieldDescriptor.H() || (fieldDescriptor.v() == Descriptors.FieldDescriptor.JavaType.MESSAGE && (bVar2 = this.d.get(fieldDescriptor.x()).d) != bVar && bVar2.b)) {
                        break loop0;
                    }
                }
            }
            bVar.b = z;
            for (Descriptors.b bVar3 : bVar.a) {
                this.a.put(bVar3, Boolean.valueOf(bVar.b));
            }
        }

        public final a b(Descriptors.b bVar) {
            a pop;
            int i = this.b;
            this.b = i + 1;
            a aVar = new a(bVar, i);
            this.c.push(aVar);
            this.d.put(bVar, aVar);
            for (Descriptors.FieldDescriptor fieldDescriptor : bVar.p()) {
                if (fieldDescriptor.v() == Descriptors.FieldDescriptor.JavaType.MESSAGE) {
                    a aVar2 = this.d.get(fieldDescriptor.x());
                    if (aVar2 == null) {
                        aVar.c = Math.min(aVar.c, b(fieldDescriptor.x()).c);
                    } else if (aVar2.d == null) {
                        aVar.c = Math.min(aVar.c, aVar2.c);
                    }
                }
            }
            if (aVar.b == aVar.c) {
                b bVar2 = new b(null);
                do {
                    pop = this.c.pop();
                    pop.d = bVar2;
                    bVar2.a.add(pop.a);
                } while (pop != aVar);
                a(bVar2);
            }
            return aVar;
        }

        public boolean c(Descriptors.b bVar) {
            Boolean bool = this.a.get(bVar);
            if (bool != null) {
                return bool.booleanValue();
            }
            synchronized (this) {
                Boolean bool2 = this.a.get(bVar);
                if (bool2 != null) {
                    return bool2.booleanValue();
                }
                return b(bVar).d.b;
            }
        }

        /* compiled from: DescriptorMessageInfoFactory.java */
        /* loaded from: classes2.dex */
        public static class b {
            public final List<Descriptors.b> a;
            public boolean b;

            public b() {
                this.a = new ArrayList();
                this.b = false;
            }

            public /* synthetic */ b(a aVar) {
                this();
            }
        }
    }

    /* compiled from: DescriptorMessageInfoFactory.java */
    /* loaded from: classes2.dex */
    public static final class e {
        public en2[] a;

        public e() {
            this.a = new en2[2];
        }

        public static en2 b(Class<?> cls, Descriptors.g gVar) {
            String w = m.w(gVar.e());
            return new en2(gVar.r(), m.m(cls, w + "Case_"), m.m(cls, w + "_"));
        }

        public en2 a(Class<?> cls, Descriptors.g gVar) {
            int r = gVar.r();
            en2[] en2VarArr = this.a;
            if (r >= en2VarArr.length) {
                this.a = (en2[]) Arrays.copyOf(en2VarArr, r * 2);
            }
            en2 en2Var = this.a[r];
            if (en2Var == null) {
                en2 b = b(cls, gVar);
                this.a[r] = b;
                return b;
            }
            return en2Var;
        }

        public /* synthetic */ e(a aVar) {
            this();
        }
    }
}
