package com.google.protobuf;

import com.google.protobuf.a0;
import java.util.AbstractList;
import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

/* compiled from: DoubleArrayList.java */
/* loaded from: classes2.dex */
public final class n extends d<Double> implements a0.b, RandomAccess, uu2 {
    public static final n h0;
    public double[] f0;
    public int g0;

    static {
        n nVar = new n(new double[0], 0);
        h0 = nVar;
        nVar.l();
    }

    public n() {
        this(new double[10], 0);
    }

    public static n o() {
        return h0;
    }

    @Override // com.google.protobuf.d, java.util.AbstractCollection, java.util.Collection, java.util.List
    public boolean addAll(Collection<? extends Double> collection) {
        e();
        a0.a(collection);
        if (!(collection instanceof n)) {
            return super.addAll(collection);
        }
        n nVar = (n) collection;
        int i = nVar.g0;
        if (i == 0) {
            return false;
        }
        int i2 = this.g0;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            double[] dArr = this.f0;
            if (i3 > dArr.length) {
                this.f0 = Arrays.copyOf(dArr, i3);
            }
            System.arraycopy(nVar.f0, 0, this.f0, this.g0, nVar.g0);
            this.g0 = i3;
            ((AbstractList) this).modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public boolean contains(Object obj) {
        return indexOf(obj) != -1;
    }

    @Override // com.google.protobuf.d, java.util.AbstractList, java.util.Collection, java.util.List
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof n)) {
            return super.equals(obj);
        }
        n nVar = (n) obj;
        if (this.g0 != nVar.g0) {
            return false;
        }
        double[] dArr = nVar.f0;
        for (int i = 0; i < this.g0; i++) {
            if (Double.doubleToLongBits(this.f0[i]) != Double.doubleToLongBits(dArr[i])) {
                return false;
            }
        }
        return true;
    }

    @Override // com.google.protobuf.d, java.util.AbstractList, java.util.Collection, java.util.List
    public int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.g0; i2++) {
            i = (i * 31) + a0.h(Double.doubleToLongBits(this.f0[i2]));
        }
        return i;
    }

    @Override // java.util.AbstractList, java.util.List
    /* renamed from: i */
    public void add(int i, Double d) {
        n(i, d.doubleValue());
    }

    @Override // java.util.AbstractList, java.util.List
    public int indexOf(Object obj) {
        if (obj instanceof Double) {
            double doubleValue = ((Double) obj).doubleValue();
            int size = size();
            for (int i = 0; i < size; i++) {
                if (this.f0[i] == doubleValue) {
                    return i;
                }
            }
            return -1;
        }
        return -1;
    }

    @Override // com.google.protobuf.d, java.util.AbstractList, java.util.AbstractCollection, java.util.Collection, java.util.List
    /* renamed from: k */
    public boolean add(Double d) {
        m(d.doubleValue());
        return true;
    }

    public void m(double d) {
        e();
        int i = this.g0;
        double[] dArr = this.f0;
        if (i == dArr.length) {
            double[] dArr2 = new double[((i * 3) / 2) + 1];
            System.arraycopy(dArr, 0, dArr2, 0, i);
            this.f0 = dArr2;
        }
        double[] dArr3 = this.f0;
        int i2 = this.g0;
        this.g0 = i2 + 1;
        dArr3[i2] = d;
    }

    public final void n(int i, double d) {
        int i2;
        e();
        if (i >= 0 && i <= (i2 = this.g0)) {
            double[] dArr = this.f0;
            if (i2 < dArr.length) {
                System.arraycopy(dArr, i, dArr, i + 1, i2 - i);
            } else {
                double[] dArr2 = new double[((i2 * 3) / 2) + 1];
                System.arraycopy(dArr, 0, dArr2, 0, i);
                System.arraycopy(this.f0, i, dArr2, i + 1, this.g0 - i);
                this.f0 = dArr2;
            }
            this.f0[i] = d;
            this.g0++;
            ((AbstractList) this).modCount++;
            return;
        }
        throw new IndexOutOfBoundsException(t(i));
    }

    public final void p(int i) {
        if (i < 0 || i >= this.g0) {
            throw new IndexOutOfBoundsException(t(i));
        }
    }

    @Override // java.util.AbstractList, java.util.List
    /* renamed from: q */
    public Double get(int i) {
        return Double.valueOf(s(i));
    }

    @Override // java.util.AbstractList
    public void removeRange(int i, int i2) {
        e();
        if (i2 >= i) {
            double[] dArr = this.f0;
            System.arraycopy(dArr, i2, dArr, i, this.g0 - i2);
            this.g0 -= i2 - i;
            ((AbstractList) this).modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    public double s(int i) {
        p(i);
        return this.f0[i];
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public int size() {
        return this.g0;
    }

    public final String t(int i) {
        return "Index:" + i + ", Size:" + this.g0;
    }

    @Override // java.util.AbstractList, java.util.List
    /* renamed from: w */
    public Double remove(int i) {
        int i2;
        e();
        p(i);
        double[] dArr = this.f0;
        double d = dArr[i];
        if (i < this.g0 - 1) {
            System.arraycopy(dArr, i + 1, dArr, i, (i2 - i) - 1);
        }
        this.g0--;
        ((AbstractList) this).modCount++;
        return Double.valueOf(d);
    }

    @Override // java.util.AbstractList, java.util.List
    /* renamed from: y */
    public Double set(int i, Double d) {
        return Double.valueOf(z(i, d.doubleValue()));
    }

    public double z(int i, double d) {
        e();
        p(i);
        double[] dArr = this.f0;
        double d2 = dArr[i];
        dArr[i] = d;
        return d2;
    }

    public n(double[] dArr, int i) {
        this.f0 = dArr;
        this.g0 = i;
    }

    @Override // com.google.protobuf.a0.i
    /* renamed from: a */
    public a0.i<Double> a2(int i) {
        if (i >= this.g0) {
            return new n(Arrays.copyOf(this.f0, i), this.g0);
        }
        throw new IllegalArgumentException();
    }

    @Override // com.google.protobuf.d, java.util.AbstractCollection, java.util.Collection, java.util.List
    public boolean remove(Object obj) {
        e();
        for (int i = 0; i < this.g0; i++) {
            if (obj.equals(Double.valueOf(this.f0[i]))) {
                double[] dArr = this.f0;
                System.arraycopy(dArr, i + 1, dArr, i, (this.g0 - i) - 1);
                this.g0--;
                ((AbstractList) this).modCount++;
                return true;
            }
        }
        return false;
    }
}
