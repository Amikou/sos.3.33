package com.google.protobuf;

import com.google.protobuf.WireFormat;
import com.google.protobuf.a0;
import com.google.protobuf.b0;
import com.google.protobuf.m0;
import com.google.protobuf.w.c;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: FieldSet.java */
/* loaded from: classes2.dex */
public final class w<T extends c<T>> {
    public static final w d = new w(true);
    public final b1<T, Object> a;
    public boolean b;
    public boolean c;

    /* compiled from: FieldSet.java */
    /* loaded from: classes2.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;
        public static final /* synthetic */ int[] b;

        static {
            int[] iArr = new int[WireFormat.FieldType.values().length];
            b = iArr;
            try {
                iArr[WireFormat.FieldType.DOUBLE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                b[WireFormat.FieldType.FLOAT.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                b[WireFormat.FieldType.INT64.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                b[WireFormat.FieldType.UINT64.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                b[WireFormat.FieldType.INT32.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                b[WireFormat.FieldType.FIXED64.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                b[WireFormat.FieldType.FIXED32.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                b[WireFormat.FieldType.BOOL.ordinal()] = 8;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                b[WireFormat.FieldType.GROUP.ordinal()] = 9;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                b[WireFormat.FieldType.MESSAGE.ordinal()] = 10;
            } catch (NoSuchFieldError unused10) {
            }
            try {
                b[WireFormat.FieldType.STRING.ordinal()] = 11;
            } catch (NoSuchFieldError unused11) {
            }
            try {
                b[WireFormat.FieldType.BYTES.ordinal()] = 12;
            } catch (NoSuchFieldError unused12) {
            }
            try {
                b[WireFormat.FieldType.UINT32.ordinal()] = 13;
            } catch (NoSuchFieldError unused13) {
            }
            try {
                b[WireFormat.FieldType.SFIXED32.ordinal()] = 14;
            } catch (NoSuchFieldError unused14) {
            }
            try {
                b[WireFormat.FieldType.SFIXED64.ordinal()] = 15;
            } catch (NoSuchFieldError unused15) {
            }
            try {
                b[WireFormat.FieldType.SINT32.ordinal()] = 16;
            } catch (NoSuchFieldError unused16) {
            }
            try {
                b[WireFormat.FieldType.SINT64.ordinal()] = 17;
            } catch (NoSuchFieldError unused17) {
            }
            try {
                b[WireFormat.FieldType.ENUM.ordinal()] = 18;
            } catch (NoSuchFieldError unused18) {
            }
            int[] iArr2 = new int[WireFormat.JavaType.values().length];
            a = iArr2;
            try {
                iArr2[WireFormat.JavaType.INT.ordinal()] = 1;
            } catch (NoSuchFieldError unused19) {
            }
            try {
                a[WireFormat.JavaType.LONG.ordinal()] = 2;
            } catch (NoSuchFieldError unused20) {
            }
            try {
                a[WireFormat.JavaType.FLOAT.ordinal()] = 3;
            } catch (NoSuchFieldError unused21) {
            }
            try {
                a[WireFormat.JavaType.DOUBLE.ordinal()] = 4;
            } catch (NoSuchFieldError unused22) {
            }
            try {
                a[WireFormat.JavaType.BOOLEAN.ordinal()] = 5;
            } catch (NoSuchFieldError unused23) {
            }
            try {
                a[WireFormat.JavaType.STRING.ordinal()] = 6;
            } catch (NoSuchFieldError unused24) {
            }
            try {
                a[WireFormat.JavaType.BYTE_STRING.ordinal()] = 7;
            } catch (NoSuchFieldError unused25) {
            }
            try {
                a[WireFormat.JavaType.ENUM.ordinal()] = 8;
            } catch (NoSuchFieldError unused26) {
            }
            try {
                a[WireFormat.JavaType.MESSAGE.ordinal()] = 9;
            } catch (NoSuchFieldError unused27) {
            }
        }
    }

    /* compiled from: FieldSet.java */
    /* loaded from: classes2.dex */
    public static final class b<T extends c<T>> {
        public b1<T, Object> a;
        public boolean b;
        public boolean c;
        public boolean d;

        public /* synthetic */ b(a aVar) {
            this();
        }

        public static Object o(Object obj) {
            return obj instanceof m0.a ? ((m0.a) obj).build() : obj;
        }

        public static <T extends c<T>> Object p(T t, Object obj) {
            if (obj != null && t.z() == WireFormat.JavaType.MESSAGE) {
                if (t.i()) {
                    if (obj instanceof List) {
                        List list = (List) obj;
                        for (int i = 0; i < list.size(); i++) {
                            Object obj2 = list.get(i);
                            Object o = o(obj2);
                            if (o != obj2) {
                                if (list == obj) {
                                    list = new ArrayList(list);
                                }
                                list.set(i, o);
                            }
                        }
                        return list;
                    }
                    throw new IllegalStateException("Repeated field should contains a List but actually contains type: " + obj.getClass());
                }
                return o(obj);
            }
            return obj;
        }

        public static <T extends c<T>> void q(b1<T, Object> b1Var) {
            for (int i = 0; i < b1Var.k(); i++) {
                r(b1Var.j(i));
            }
            for (Map.Entry<T, Object> entry : b1Var.m()) {
                r(entry);
            }
        }

        public static <T extends c<T>> void r(Map.Entry<T, Object> entry) {
            entry.setValue(p(entry.getKey(), entry.getValue()));
        }

        public static void u(WireFormat.FieldType fieldType, Object obj) {
            if (w.F(fieldType, obj)) {
                return;
            }
            if (fieldType.getJavaType() != WireFormat.JavaType.MESSAGE || !(obj instanceof m0.a)) {
                throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
            }
        }

        public void a(T t, Object obj) {
            List list;
            d();
            if (t.i()) {
                this.d = this.d || (obj instanceof m0.a);
                u(t.n(), obj);
                Object f = f(t);
                if (f == null) {
                    list = new ArrayList();
                    this.a.put(t, list);
                } else {
                    list = (List) f;
                }
                list.add(obj);
                return;
            }
            throw new IllegalArgumentException("addRepeatedField() can only be called on repeated fields.");
        }

        public w<T> b() {
            if (this.a.isEmpty()) {
                return w.r();
            }
            this.c = false;
            b1<T, Object> b1Var = this.a;
            if (this.d) {
                b1Var = w.k(b1Var, false);
                q(b1Var);
            }
            w<T> wVar = new w<>(b1Var, null);
            wVar.c = this.b;
            return wVar;
        }

        public void c(T t) {
            d();
            this.a.remove(t);
            if (this.a.isEmpty()) {
                this.b = false;
            }
        }

        public final void d() {
            if (this.c) {
                return;
            }
            this.a = w.k(this.a, true);
            this.c = true;
        }

        public Map<T, Object> e() {
            if (!this.b) {
                return this.a.o() ? this.a : Collections.unmodifiableMap(this.a);
            }
            b1 k = w.k(this.a, false);
            if (this.a.o()) {
                k.p();
            } else {
                q(k);
            }
            return k;
        }

        public Object f(T t) {
            return p(t, g(t));
        }

        public Object g(T t) {
            Object obj = this.a.get(t);
            return obj instanceof b0 ? ((b0) obj).g() : obj;
        }

        public Object h(T t, int i) {
            if (this.d) {
                d();
            }
            return o(i(t, i));
        }

        public Object i(T t, int i) {
            if (t.i()) {
                Object g = g(t);
                if (g != null) {
                    return ((List) g).get(i);
                }
                throw new IndexOutOfBoundsException();
            }
            throw new IllegalArgumentException("getRepeatedField() can only be called on repeated fields.");
        }

        public int j(T t) {
            if (t.i()) {
                Object f = f(t);
                if (f == null) {
                    return 0;
                }
                return ((List) f).size();
            }
            throw new IllegalArgumentException("getRepeatedField() can only be called on repeated fields.");
        }

        public boolean k(T t) {
            if (t.i()) {
                throw new IllegalArgumentException("hasField() can only be called on non-repeated fields.");
            }
            return this.a.get(t) != null;
        }

        public boolean l() {
            for (int i = 0; i < this.a.k(); i++) {
                if (!w.E(this.a.j(i))) {
                    return false;
                }
            }
            for (Map.Entry<T, Object> entry : this.a.m()) {
                if (!w.E(entry)) {
                    return false;
                }
            }
            return true;
        }

        public void m(w<T> wVar) {
            d();
            for (int i = 0; i < wVar.a.k(); i++) {
                n(wVar.a.j(i));
            }
            for (Map.Entry<T, Object> entry : wVar.a.m()) {
                n(entry);
            }
        }

        public final void n(Map.Entry<T, Object> entry) {
            T key = entry.getKey();
            Object value = entry.getValue();
            if (value instanceof b0) {
                value = ((b0) value).g();
            }
            if (key.i()) {
                Object f = f(key);
                if (f == null) {
                    f = new ArrayList();
                }
                for (Object obj : (List) value) {
                    ((List) f).add(w.m(obj));
                }
                this.a.put(key, f);
            } else if (key.z() != WireFormat.JavaType.MESSAGE) {
                this.a.put(key, w.m(value));
            } else {
                Object f2 = f(key);
                if (f2 == null) {
                    this.a.put(key, w.m(value));
                } else if (f2 instanceof m0.a) {
                    key.i0((m0.a) f2, (m0) value);
                } else {
                    this.a.put(key, key.i0(((m0) f2).toBuilder(), (m0) value).build());
                }
            }
        }

        public void s(T t, Object obj) {
            d();
            boolean z = false;
            if (t.i()) {
                if (obj instanceof List) {
                    ArrayList arrayList = new ArrayList();
                    arrayList.addAll((List) obj);
                    for (Object obj2 : arrayList) {
                        u(t.n(), obj2);
                        this.d = this.d || (obj2 instanceof m0.a);
                    }
                    obj = arrayList;
                } else {
                    throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
                }
            } else {
                u(t.n(), obj);
            }
            if (obj instanceof b0) {
                this.b = true;
            }
            this.d = (this.d || (obj instanceof m0.a)) ? true : true;
            this.a.put(t, obj);
        }

        public void t(T t, int i, Object obj) {
            d();
            if (t.i()) {
                this.d = this.d || (obj instanceof m0.a);
                Object f = f(t);
                if (f != null) {
                    u(t.n(), obj);
                    ((List) f).set(i, obj);
                    return;
                }
                throw new IndexOutOfBoundsException();
            }
            throw new IllegalArgumentException("getRepeatedField() can only be called on repeated fields.");
        }

        public b() {
            this(b1.q(16));
        }

        public b(b1<T, Object> b1Var) {
            this.a = b1Var;
            this.c = true;
        }
    }

    /* compiled from: FieldSet.java */
    /* loaded from: classes2.dex */
    public interface c<T extends c<T>> extends Comparable<T> {
        int getNumber();

        boolean i();

        m0.a i0(m0.a aVar, m0 m0Var);

        boolean isPacked();

        WireFormat.FieldType n();

        WireFormat.JavaType z();
    }

    public /* synthetic */ w(b1 b1Var, a aVar) {
        this(b1Var);
    }

    public static <T extends c<T>> boolean E(Map.Entry<T, Object> entry) {
        T key = entry.getKey();
        if (key.z() == WireFormat.JavaType.MESSAGE) {
            if (key.i()) {
                for (m0 m0Var : (List) entry.getValue()) {
                    if (!m0Var.isInitialized()) {
                        return false;
                    }
                }
            } else {
                Object value = entry.getValue();
                if (value instanceof m0) {
                    if (!((m0) value).isInitialized()) {
                        return false;
                    }
                } else if (value instanceof b0) {
                    return true;
                } else {
                    throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
                }
            }
        }
        return true;
    }

    public static boolean F(WireFormat.FieldType fieldType, Object obj) {
        a0.a(obj);
        switch (a.a[fieldType.getJavaType().ordinal()]) {
            case 1:
                return obj instanceof Integer;
            case 2:
                return obj instanceof Long;
            case 3:
                return obj instanceof Float;
            case 4:
                return obj instanceof Double;
            case 5:
                return obj instanceof Boolean;
            case 6:
                return obj instanceof String;
            case 7:
                return (obj instanceof ByteString) || (obj instanceof byte[]);
            case 8:
                return (obj instanceof Integer) || (obj instanceof a0.c);
            case 9:
                return (obj instanceof m0) || (obj instanceof b0);
            default:
                return false;
        }
    }

    public static <T extends c<T>> b<T> K() {
        return new b<>((a) null);
    }

    public static <T extends c<T>> w<T> L() {
        return new w<>();
    }

    public static Object M(j jVar, WireFormat.FieldType fieldType, boolean z) throws IOException {
        if (z) {
            return WireFormat.d(jVar, fieldType, WireFormat.Utf8Validation.STRICT);
        }
        return WireFormat.d(jVar, fieldType, WireFormat.Utf8Validation.LOOSE);
    }

    public static void P(CodedOutputStream codedOutputStream, WireFormat.FieldType fieldType, int i, Object obj) throws IOException {
        if (fieldType == WireFormat.FieldType.GROUP) {
            codedOutputStream.C0(i, (m0) obj);
            return;
        }
        codedOutputStream.a1(i, z(fieldType, false));
        Q(codedOutputStream, fieldType, obj);
    }

    public static void Q(CodedOutputStream codedOutputStream, WireFormat.FieldType fieldType, Object obj) throws IOException {
        switch (a.b[fieldType.ordinal()]) {
            case 1:
                codedOutputStream.t0(((Double) obj).doubleValue());
                return;
            case 2:
                codedOutputStream.B0(((Float) obj).floatValue());
                return;
            case 3:
                codedOutputStream.J0(((Long) obj).longValue());
                return;
            case 4:
                codedOutputStream.e1(((Long) obj).longValue());
                return;
            case 5:
                codedOutputStream.H0(((Integer) obj).intValue());
                return;
            case 6:
                codedOutputStream.z0(((Long) obj).longValue());
                return;
            case 7:
                codedOutputStream.x0(((Integer) obj).intValue());
                return;
            case 8:
                codedOutputStream.n0(((Boolean) obj).booleanValue());
                return;
            case 9:
                codedOutputStream.E0((m0) obj);
                return;
            case 10:
                codedOutputStream.M0((m0) obj);
                return;
            case 11:
                if (obj instanceof ByteString) {
                    codedOutputStream.r0((ByteString) obj);
                    return;
                } else {
                    codedOutputStream.Z0((String) obj);
                    return;
                }
            case 12:
                if (obj instanceof ByteString) {
                    codedOutputStream.r0((ByteString) obj);
                    return;
                } else {
                    codedOutputStream.o0((byte[]) obj);
                    return;
                }
            case 13:
                codedOutputStream.c1(((Integer) obj).intValue());
                return;
            case 14:
                codedOutputStream.R0(((Integer) obj).intValue());
                return;
            case 15:
                codedOutputStream.T0(((Long) obj).longValue());
                return;
            case 16:
                codedOutputStream.V0(((Integer) obj).intValue());
                return;
            case 17:
                codedOutputStream.X0(((Long) obj).longValue());
                return;
            case 18:
                if (obj instanceof a0.c) {
                    codedOutputStream.v0(((a0.c) obj).getNumber());
                    return;
                } else {
                    codedOutputStream.v0(((Integer) obj).intValue());
                    return;
                }
            default:
                return;
        }
    }

    public static void R(c<?> cVar, Object obj, CodedOutputStream codedOutputStream) throws IOException {
        WireFormat.FieldType n = cVar.n();
        int number = cVar.getNumber();
        if (cVar.i()) {
            List<Object> list = (List) obj;
            if (cVar.isPacked()) {
                codedOutputStream.a1(number, 2);
                int i = 0;
                for (Object obj2 : list) {
                    i += o(n, obj2);
                }
                codedOutputStream.P0(i);
                for (Object obj3 : list) {
                    Q(codedOutputStream, n, obj3);
                }
                return;
            }
            for (Object obj4 : list) {
                P(codedOutputStream, n, number, obj4);
            }
        } else if (obj instanceof b0) {
            P(codedOutputStream, n, number, ((b0) obj).g());
        } else {
            P(codedOutputStream, n, number, obj);
        }
    }

    public static <T extends c<T>> b1<T, Object> k(b1<T, Object> b1Var, boolean z) {
        b1<T, Object> q = b1.q(16);
        for (int i = 0; i < b1Var.k(); i++) {
            l(q, b1Var.j(i), z);
        }
        for (Map.Entry<T, Object> entry : b1Var.m()) {
            l(q, entry, z);
        }
        return q;
    }

    public static <T extends c<T>> void l(Map<T, Object> map, Map.Entry<T, Object> entry, boolean z) {
        T key = entry.getKey();
        Object value = entry.getValue();
        if (value instanceof b0) {
            map.put(key, ((b0) value).g());
        } else if (z && (value instanceof List)) {
            map.put(key, new ArrayList((List) value));
        } else {
            map.put(key, value);
        }
    }

    public static Object m(Object obj) {
        if (obj instanceof byte[]) {
            byte[] bArr = (byte[]) obj;
            byte[] bArr2 = new byte[bArr.length];
            System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
            return bArr2;
        }
        return obj;
    }

    public static int n(WireFormat.FieldType fieldType, int i, Object obj) {
        int X = CodedOutputStream.X(i);
        if (fieldType == WireFormat.FieldType.GROUP) {
            X *= 2;
        }
        return X + o(fieldType, obj);
    }

    public static int o(WireFormat.FieldType fieldType, Object obj) {
        switch (a.b[fieldType.ordinal()]) {
            case 1:
                return CodedOutputStream.k(((Double) obj).doubleValue());
            case 2:
                return CodedOutputStream.s(((Float) obj).floatValue());
            case 3:
                return CodedOutputStream.A(((Long) obj).longValue());
            case 4:
                return CodedOutputStream.b0(((Long) obj).longValue());
            case 5:
                return CodedOutputStream.y(((Integer) obj).intValue());
            case 6:
                return CodedOutputStream.q(((Long) obj).longValue());
            case 7:
                return CodedOutputStream.o(((Integer) obj).intValue());
            case 8:
                return CodedOutputStream.f(((Boolean) obj).booleanValue());
            case 9:
                return CodedOutputStream.v((m0) obj);
            case 10:
                if (obj instanceof b0) {
                    return CodedOutputStream.D((b0) obj);
                }
                return CodedOutputStream.I((m0) obj);
            case 11:
                if (obj instanceof ByteString) {
                    return CodedOutputStream.i((ByteString) obj);
                }
                return CodedOutputStream.W((String) obj);
            case 12:
                if (obj instanceof ByteString) {
                    return CodedOutputStream.i((ByteString) obj);
                }
                return CodedOutputStream.g((byte[]) obj);
            case 13:
                return CodedOutputStream.Z(((Integer) obj).intValue());
            case 14:
                return CodedOutputStream.O(((Integer) obj).intValue());
            case 15:
                return CodedOutputStream.Q(((Long) obj).longValue());
            case 16:
                return CodedOutputStream.S(((Integer) obj).intValue());
            case 17:
                return CodedOutputStream.U(((Long) obj).longValue());
            case 18:
                if (obj instanceof a0.c) {
                    return CodedOutputStream.m(((a0.c) obj).getNumber());
                }
                return CodedOutputStream.m(((Integer) obj).intValue());
            default:
                throw new RuntimeException("There is no way to get here, but the compiler thinks otherwise.");
        }
    }

    public static int p(c<?> cVar, Object obj) {
        WireFormat.FieldType n = cVar.n();
        int number = cVar.getNumber();
        if (cVar.i()) {
            int i = 0;
            if (cVar.isPacked()) {
                for (Object obj2 : (List) obj) {
                    i += o(n, obj2);
                }
                return CodedOutputStream.X(number) + i + CodedOutputStream.M(i);
            }
            for (Object obj3 : (List) obj) {
                i += n(n, number, obj3);
            }
            return i;
        }
        return n(n, number, obj);
    }

    public static <T extends c<T>> w<T> r() {
        return d;
    }

    public static int z(WireFormat.FieldType fieldType, boolean z) {
        if (z) {
            return 2;
        }
        return fieldType.getWireType();
    }

    public boolean A(T t) {
        if (t.i()) {
            throw new IllegalArgumentException("hasField() can only be called on non-repeated fields.");
        }
        return this.a.get(t) != null;
    }

    public boolean B() {
        return this.a.isEmpty();
    }

    public boolean C() {
        return this.b;
    }

    public boolean D() {
        for (int i = 0; i < this.a.k(); i++) {
            if (!E(this.a.j(i))) {
                return false;
            }
        }
        for (Map.Entry<T, Object> entry : this.a.m()) {
            if (!E(entry)) {
                return false;
            }
        }
        return true;
    }

    public Iterator<Map.Entry<T, Object>> G() {
        if (this.c) {
            return new b0.c(this.a.entrySet().iterator());
        }
        return this.a.entrySet().iterator();
    }

    public void H() {
        if (this.b) {
            return;
        }
        this.a.p();
        this.b = true;
    }

    public void I(w<T> wVar) {
        for (int i = 0; i < wVar.a.k(); i++) {
            J(wVar.a.j(i));
        }
        for (Map.Entry<T, Object> entry : wVar.a.m()) {
            J(entry);
        }
    }

    public final void J(Map.Entry<T, Object> entry) {
        T key = entry.getKey();
        Object value = entry.getValue();
        if (value instanceof b0) {
            value = ((b0) value).g();
        }
        if (key.i()) {
            Object t = t(key);
            if (t == null) {
                t = new ArrayList();
            }
            for (Object obj : (List) value) {
                ((List) t).add(m(obj));
            }
            this.a.put(key, t);
        } else if (key.z() == WireFormat.JavaType.MESSAGE) {
            Object t2 = t(key);
            if (t2 == null) {
                this.a.put(key, m(value));
                return;
            }
            this.a.put(key, key.i0(((m0) t2).toBuilder(), (m0) value).build());
        } else {
            this.a.put(key, m(value));
        }
    }

    public void N(T t, Object obj) {
        if (t.i()) {
            if (obj instanceof List) {
                ArrayList<Object> arrayList = new ArrayList();
                arrayList.addAll((List) obj);
                for (Object obj2 : arrayList) {
                    O(t.n(), obj2);
                }
                obj = arrayList;
            } else {
                throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
            }
        } else {
            O(t.n(), obj);
        }
        if (obj instanceof b0) {
            this.c = true;
        }
        this.a.put(t, obj);
    }

    public final void O(WireFormat.FieldType fieldType, Object obj) {
        if (!F(fieldType, obj)) {
            throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
        }
    }

    public void S(CodedOutputStream codedOutputStream) throws IOException {
        for (int i = 0; i < this.a.k(); i++) {
            T(this.a.j(i), codedOutputStream);
        }
        for (Map.Entry<T, Object> entry : this.a.m()) {
            T(entry, codedOutputStream);
        }
    }

    public final void T(Map.Entry<T, Object> entry, CodedOutputStream codedOutputStream) throws IOException {
        T key = entry.getKey();
        if (key.z() == WireFormat.JavaType.MESSAGE && !key.i() && !key.isPacked()) {
            Object value = entry.getValue();
            if (value instanceof b0) {
                value = ((b0) value).g();
            }
            codedOutputStream.N0(entry.getKey().getNumber(), (m0) value);
            return;
        }
        R(key, entry.getValue(), codedOutputStream);
    }

    public void U(CodedOutputStream codedOutputStream) throws IOException {
        for (int i = 0; i < this.a.k(); i++) {
            Map.Entry<T, Object> j = this.a.j(i);
            R(j.getKey(), j.getValue(), codedOutputStream);
        }
        for (Map.Entry<T, Object> entry : this.a.m()) {
            R(entry.getKey(), entry.getValue(), codedOutputStream);
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof w) {
            return this.a.equals(((w) obj).a);
        }
        return false;
    }

    public void g(T t, Object obj) {
        List list;
        if (t.i()) {
            O(t.n(), obj);
            Object t2 = t(t);
            if (t2 == null) {
                list = new ArrayList();
                this.a.put(t, list);
            } else {
                list = (List) t2;
            }
            list.add(obj);
            return;
        }
        throw new IllegalArgumentException("addRepeatedField() can only be called on repeated fields.");
    }

    public void h() {
        this.a.clear();
        this.c = false;
    }

    public int hashCode() {
        return this.a.hashCode();
    }

    public void i(T t) {
        this.a.remove(t);
        if (this.a.isEmpty()) {
            this.c = false;
        }
    }

    /* renamed from: j */
    public w<T> clone() {
        w<T> L = L();
        for (int i = 0; i < this.a.k(); i++) {
            Map.Entry<T, Object> j = this.a.j(i);
            L.N(j.getKey(), j.getValue());
        }
        for (Map.Entry<T, Object> entry : this.a.m()) {
            L.N(entry.getKey(), entry.getValue());
        }
        L.c = this.c;
        return L;
    }

    public Iterator<Map.Entry<T, Object>> q() {
        if (this.c) {
            return new b0.c(this.a.h().iterator());
        }
        return this.a.h().iterator();
    }

    public Map<T, Object> s() {
        if (!this.c) {
            return this.a.o() ? this.a : Collections.unmodifiableMap(this.a);
        }
        b1 k = k(this.a, false);
        if (this.a.o()) {
            k.p();
        }
        return k;
    }

    public Object t(T t) {
        Object obj = this.a.get(t);
        return obj instanceof b0 ? ((b0) obj).g() : obj;
    }

    public int u() {
        int i = 0;
        for (int i2 = 0; i2 < this.a.k(); i2++) {
            i += v(this.a.j(i2));
        }
        for (Map.Entry<T, Object> entry : this.a.m()) {
            i += v(entry);
        }
        return i;
    }

    public final int v(Map.Entry<T, Object> entry) {
        T key = entry.getKey();
        Object value = entry.getValue();
        if (key.z() == WireFormat.JavaType.MESSAGE && !key.i() && !key.isPacked()) {
            if (value instanceof b0) {
                return CodedOutputStream.B(entry.getKey().getNumber(), (b0) value);
            }
            return CodedOutputStream.F(entry.getKey().getNumber(), (m0) value);
        }
        return p(key, value);
    }

    public Object w(T t, int i) {
        if (t.i()) {
            Object t2 = t(t);
            if (t2 != null) {
                return ((List) t2).get(i);
            }
            throw new IndexOutOfBoundsException();
        }
        throw new IllegalArgumentException("getRepeatedField() can only be called on repeated fields.");
    }

    public int x(T t) {
        if (t.i()) {
            Object t2 = t(t);
            if (t2 == null) {
                return 0;
            }
            return ((List) t2).size();
        }
        throw new IllegalArgumentException("getRepeatedField() can only be called on repeated fields.");
    }

    public int y() {
        int i = 0;
        for (int i2 = 0; i2 < this.a.k(); i2++) {
            Map.Entry<T, Object> j = this.a.j(i2);
            i += p(j.getKey(), j.getValue());
        }
        for (Map.Entry<T, Object> entry : this.a.m()) {
            i += p(entry.getKey(), entry.getValue());
        }
        return i;
    }

    public w() {
        this.a = b1.q(16);
    }

    public w(boolean z) {
        this(b1.q(0));
        H();
    }

    public w(b1<T, Object> b1Var) {
        this.a = b1Var;
        H();
    }
}
