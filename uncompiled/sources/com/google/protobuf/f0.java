package com.google.protobuf;

import com.google.protobuf.a0;
import java.util.AbstractList;
import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

/* compiled from: LongArrayList.java */
/* loaded from: classes2.dex */
public final class f0 extends d<Long> implements a0.h, RandomAccess, uu2 {
    public static final f0 h0;
    public long[] f0;
    public int g0;

    static {
        f0 f0Var = new f0(new long[0], 0);
        h0 = f0Var;
        f0Var.l();
    }

    public f0() {
        this(new long[10], 0);
    }

    public static f0 o() {
        return h0;
    }

    @Override // com.google.protobuf.d, java.util.AbstractCollection, java.util.Collection, java.util.List
    public boolean addAll(Collection<? extends Long> collection) {
        e();
        a0.a(collection);
        if (!(collection instanceof f0)) {
            return super.addAll(collection);
        }
        f0 f0Var = (f0) collection;
        int i = f0Var.g0;
        if (i == 0) {
            return false;
        }
        int i2 = this.g0;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            long[] jArr = this.f0;
            if (i3 > jArr.length) {
                this.f0 = Arrays.copyOf(jArr, i3);
            }
            System.arraycopy(f0Var.f0, 0, this.f0, this.g0, f0Var.g0);
            this.g0 = i3;
            ((AbstractList) this).modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public boolean contains(Object obj) {
        return indexOf(obj) != -1;
    }

    @Override // com.google.protobuf.d, java.util.AbstractList, java.util.Collection, java.util.List
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof f0)) {
            return super.equals(obj);
        }
        f0 f0Var = (f0) obj;
        if (this.g0 != f0Var.g0) {
            return false;
        }
        long[] jArr = f0Var.f0;
        for (int i = 0; i < this.g0; i++) {
            if (this.f0[i] != jArr[i]) {
                return false;
            }
        }
        return true;
    }

    @Override // com.google.protobuf.d, java.util.AbstractList, java.util.Collection, java.util.List
    public int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.g0; i2++) {
            i = (i * 31) + a0.h(this.f0[i2]);
        }
        return i;
    }

    @Override // java.util.AbstractList, java.util.List
    /* renamed from: i */
    public void add(int i, Long l) {
        m(i, l.longValue());
    }

    @Override // java.util.AbstractList, java.util.List
    public int indexOf(Object obj) {
        if (obj instanceof Long) {
            long longValue = ((Long) obj).longValue();
            int size = size();
            for (int i = 0; i < size; i++) {
                if (this.f0[i] == longValue) {
                    return i;
                }
            }
            return -1;
        }
        return -1;
    }

    @Override // com.google.protobuf.d, java.util.AbstractList, java.util.AbstractCollection, java.util.Collection, java.util.List
    /* renamed from: k */
    public boolean add(Long l) {
        n(l.longValue());
        return true;
    }

    public final void m(int i, long j) {
        int i2;
        e();
        if (i >= 0 && i <= (i2 = this.g0)) {
            long[] jArr = this.f0;
            if (i2 < jArr.length) {
                System.arraycopy(jArr, i, jArr, i + 1, i2 - i);
            } else {
                long[] jArr2 = new long[((i2 * 3) / 2) + 1];
                System.arraycopy(jArr, 0, jArr2, 0, i);
                System.arraycopy(this.f0, i, jArr2, i + 1, this.g0 - i);
                this.f0 = jArr2;
            }
            this.f0[i] = j;
            this.g0++;
            ((AbstractList) this).modCount++;
            return;
        }
        throw new IndexOutOfBoundsException(t(i));
    }

    public void n(long j) {
        e();
        int i = this.g0;
        long[] jArr = this.f0;
        if (i == jArr.length) {
            long[] jArr2 = new long[((i * 3) / 2) + 1];
            System.arraycopy(jArr, 0, jArr2, 0, i);
            this.f0 = jArr2;
        }
        long[] jArr3 = this.f0;
        int i2 = this.g0;
        this.g0 = i2 + 1;
        jArr3[i2] = j;
    }

    public final void p(int i) {
        if (i < 0 || i >= this.g0) {
            throw new IndexOutOfBoundsException(t(i));
        }
    }

    @Override // java.util.AbstractList, java.util.List
    /* renamed from: q */
    public Long get(int i) {
        return Long.valueOf(s(i));
    }

    @Override // java.util.AbstractList
    public void removeRange(int i, int i2) {
        e();
        if (i2 >= i) {
            long[] jArr = this.f0;
            System.arraycopy(jArr, i2, jArr, i, this.g0 - i2);
            this.g0 -= i2 - i;
            ((AbstractList) this).modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    public long s(int i) {
        p(i);
        return this.f0[i];
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public int size() {
        return this.g0;
    }

    public final String t(int i) {
        return "Index:" + i + ", Size:" + this.g0;
    }

    @Override // java.util.AbstractList, java.util.List
    /* renamed from: w */
    public Long remove(int i) {
        int i2;
        e();
        p(i);
        long[] jArr = this.f0;
        long j = jArr[i];
        if (i < this.g0 - 1) {
            System.arraycopy(jArr, i + 1, jArr, i, (i2 - i) - 1);
        }
        this.g0--;
        ((AbstractList) this).modCount++;
        return Long.valueOf(j);
    }

    @Override // java.util.AbstractList, java.util.List
    /* renamed from: y */
    public Long set(int i, Long l) {
        return Long.valueOf(z(i, l.longValue()));
    }

    public long z(int i, long j) {
        e();
        p(i);
        long[] jArr = this.f0;
        long j2 = jArr[i];
        jArr[i] = j;
        return j2;
    }

    public f0(long[] jArr, int i) {
        this.f0 = jArr;
        this.g0 = i;
    }

    @Override // com.google.protobuf.a0.i
    /* renamed from: a */
    public a0.i<Long> a2(int i) {
        if (i >= this.g0) {
            return new f0(Arrays.copyOf(this.f0, i), this.g0);
        }
        throw new IllegalArgumentException();
    }

    @Override // com.google.protobuf.d, java.util.AbstractCollection, java.util.Collection, java.util.List
    public boolean remove(Object obj) {
        e();
        for (int i = 0; i < this.g0; i++) {
            if (obj.equals(Long.valueOf(this.f0[i]))) {
                long[] jArr = this.f0;
                System.arraycopy(jArr, i + 1, jArr, i, (this.g0 - i) - 1);
                this.g0--;
                ((AbstractList) this).modCount++;
                return true;
            }
        }
        return false;
    }
}
