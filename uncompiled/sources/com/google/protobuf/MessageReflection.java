package com.google.protobuf;

import com.google.protobuf.Descriptors;
import com.google.protobuf.WireFormat;
import com.google.protobuf.g1;
import com.google.protobuf.l0;
import com.google.protobuf.q;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/* loaded from: classes2.dex */
public class MessageReflection {

    /* loaded from: classes2.dex */
    public interface MergeTarget {

        /* loaded from: classes2.dex */
        public enum ContainerType {
            MESSAGE,
            EXTENSION_SET
        }

        Object a(j jVar, r rVar, Descriptors.FieldDescriptor fieldDescriptor, l0 l0Var) throws IOException;

        MergeTarget addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj);

        Object b(j jVar, r rVar, Descriptors.FieldDescriptor fieldDescriptor, l0 l0Var) throws IOException;

        q.b c(q qVar, Descriptors.b bVar, int i);

        ContainerType d();

        WireFormat.Utf8Validation e(Descriptors.FieldDescriptor fieldDescriptor);

        Object f(ByteString byteString, r rVar, Descriptors.FieldDescriptor fieldDescriptor, l0 l0Var) throws IOException;

        boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        MergeTarget setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj);
    }

    /* loaded from: classes2.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[Descriptors.FieldDescriptor.Type.values().length];
            a = iArr;
            try {
                iArr[Descriptors.FieldDescriptor.Type.GROUP.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[Descriptors.FieldDescriptor.Type.MESSAGE.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[Descriptors.FieldDescriptor.Type.ENUM.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    /* loaded from: classes2.dex */
    public static class b implements MergeTarget {
        public final l0.a a;

        public b(l0.a aVar) {
            this.a = aVar;
        }

        @Override // com.google.protobuf.MessageReflection.MergeTarget
        public Object a(j jVar, r rVar, Descriptors.FieldDescriptor fieldDescriptor, l0 l0Var) throws IOException {
            l0.a newBuilderForField;
            l0 l0Var2;
            if (l0Var != null) {
                newBuilderForField = l0Var.newBuilderForType();
            } else {
                newBuilderForField = this.a.newBuilderForField(fieldDescriptor);
            }
            if (!fieldDescriptor.i() && (l0Var2 = (l0) g(fieldDescriptor)) != null) {
                newBuilderForField.mergeFrom(l0Var2);
            }
            jVar.A(newBuilderForField, rVar);
            return newBuilderForField.buildPartial();
        }

        @Override // com.google.protobuf.MessageReflection.MergeTarget
        public MergeTarget addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
            this.a.addRepeatedField(fieldDescriptor, obj);
            return this;
        }

        @Override // com.google.protobuf.MessageReflection.MergeTarget
        public Object b(j jVar, r rVar, Descriptors.FieldDescriptor fieldDescriptor, l0 l0Var) throws IOException {
            l0.a newBuilderForField;
            l0 l0Var2;
            if (l0Var != null) {
                newBuilderForField = l0Var.newBuilderForType();
            } else {
                newBuilderForField = this.a.newBuilderForField(fieldDescriptor);
            }
            if (!fieldDescriptor.i() && (l0Var2 = (l0) g(fieldDescriptor)) != null) {
                newBuilderForField.mergeFrom(l0Var2);
            }
            jVar.w(fieldDescriptor.getNumber(), newBuilderForField, rVar);
            return newBuilderForField.buildPartial();
        }

        @Override // com.google.protobuf.MessageReflection.MergeTarget
        public q.b c(q qVar, Descriptors.b bVar, int i) {
            return qVar.e(bVar, i);
        }

        @Override // com.google.protobuf.MessageReflection.MergeTarget
        public MergeTarget.ContainerType d() {
            return MergeTarget.ContainerType.MESSAGE;
        }

        @Override // com.google.protobuf.MessageReflection.MergeTarget
        public WireFormat.Utf8Validation e(Descriptors.FieldDescriptor fieldDescriptor) {
            if (fieldDescriptor.I()) {
                return WireFormat.Utf8Validation.STRICT;
            }
            fieldDescriptor.i();
            return WireFormat.Utf8Validation.LOOSE;
        }

        @Override // com.google.protobuf.MessageReflection.MergeTarget
        public Object f(ByteString byteString, r rVar, Descriptors.FieldDescriptor fieldDescriptor, l0 l0Var) throws IOException {
            l0.a newBuilderForField;
            l0 l0Var2;
            if (l0Var != null) {
                newBuilderForField = l0Var.newBuilderForType();
            } else {
                newBuilderForField = this.a.newBuilderForField(fieldDescriptor);
            }
            if (!fieldDescriptor.i() && (l0Var2 = (l0) g(fieldDescriptor)) != null) {
                newBuilderForField.mergeFrom(l0Var2);
            }
            newBuilderForField.mergeFrom(byteString, rVar);
            return newBuilderForField.buildPartial();
        }

        public Object g(Descriptors.FieldDescriptor fieldDescriptor) {
            return this.a.getField(fieldDescriptor);
        }

        @Override // com.google.protobuf.MessageReflection.MergeTarget
        public boolean hasField(Descriptors.FieldDescriptor fieldDescriptor) {
            return this.a.hasField(fieldDescriptor);
        }

        @Override // com.google.protobuf.MessageReflection.MergeTarget
        public MergeTarget setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
            this.a.setField(fieldDescriptor, obj);
            return this;
        }
    }

    /* loaded from: classes2.dex */
    public static class c implements MergeTarget {
        public final w<Descriptors.FieldDescriptor> a;

        public c(w<Descriptors.FieldDescriptor> wVar) {
            this.a = wVar;
        }

        @Override // com.google.protobuf.MessageReflection.MergeTarget
        public Object a(j jVar, r rVar, Descriptors.FieldDescriptor fieldDescriptor, l0 l0Var) throws IOException {
            l0 l0Var2;
            l0.a newBuilderForType = l0Var.newBuilderForType();
            if (!fieldDescriptor.i() && (l0Var2 = (l0) g(fieldDescriptor)) != null) {
                newBuilderForType.mergeFrom(l0Var2);
            }
            jVar.A(newBuilderForType, rVar);
            return newBuilderForType.buildPartial();
        }

        @Override // com.google.protobuf.MessageReflection.MergeTarget
        public MergeTarget addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
            this.a.g(fieldDescriptor, obj);
            return this;
        }

        @Override // com.google.protobuf.MessageReflection.MergeTarget
        public Object b(j jVar, r rVar, Descriptors.FieldDescriptor fieldDescriptor, l0 l0Var) throws IOException {
            l0 l0Var2;
            l0.a newBuilderForType = l0Var.newBuilderForType();
            if (!fieldDescriptor.i() && (l0Var2 = (l0) g(fieldDescriptor)) != null) {
                newBuilderForType.mergeFrom(l0Var2);
            }
            jVar.w(fieldDescriptor.getNumber(), newBuilderForType, rVar);
            return newBuilderForType.buildPartial();
        }

        @Override // com.google.protobuf.MessageReflection.MergeTarget
        public q.b c(q qVar, Descriptors.b bVar, int i) {
            return qVar.e(bVar, i);
        }

        @Override // com.google.protobuf.MessageReflection.MergeTarget
        public MergeTarget.ContainerType d() {
            return MergeTarget.ContainerType.EXTENSION_SET;
        }

        @Override // com.google.protobuf.MessageReflection.MergeTarget
        public WireFormat.Utf8Validation e(Descriptors.FieldDescriptor fieldDescriptor) {
            if (fieldDescriptor.I()) {
                return WireFormat.Utf8Validation.STRICT;
            }
            return WireFormat.Utf8Validation.LOOSE;
        }

        @Override // com.google.protobuf.MessageReflection.MergeTarget
        public Object f(ByteString byteString, r rVar, Descriptors.FieldDescriptor fieldDescriptor, l0 l0Var) throws IOException {
            l0 l0Var2;
            l0.a newBuilderForType = l0Var.newBuilderForType();
            if (!fieldDescriptor.i() && (l0Var2 = (l0) g(fieldDescriptor)) != null) {
                newBuilderForType.mergeFrom(l0Var2);
            }
            newBuilderForType.mergeFrom(byteString, rVar);
            return newBuilderForType.buildPartial();
        }

        public Object g(Descriptors.FieldDescriptor fieldDescriptor) {
            return this.a.t(fieldDescriptor);
        }

        @Override // com.google.protobuf.MessageReflection.MergeTarget
        public boolean hasField(Descriptors.FieldDescriptor fieldDescriptor) {
            return this.a.A(fieldDescriptor);
        }

        @Override // com.google.protobuf.MessageReflection.MergeTarget
        public MergeTarget setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
            this.a.N(fieldDescriptor, obj);
            return this;
        }
    }

    public static String a(List<String> list) {
        StringBuilder sb = new StringBuilder();
        for (String str : list) {
            if (sb.length() > 0) {
                sb.append(", ");
            }
            sb.append(str);
        }
        return sb.toString();
    }

    public static void b(j jVar, q.b bVar, r rVar, MergeTarget mergeTarget) throws IOException {
        Descriptors.FieldDescriptor fieldDescriptor = bVar.a;
        mergeTarget.setField(fieldDescriptor, mergeTarget.a(jVar, rVar, fieldDescriptor, bVar.b));
    }

    public static List<String> c(o0 o0Var) {
        ArrayList arrayList = new ArrayList();
        d(o0Var, "", arrayList);
        return arrayList;
    }

    public static void d(o0 o0Var, String str, List<String> list) {
        for (Descriptors.FieldDescriptor fieldDescriptor : o0Var.getDescriptorForType().p()) {
            if (fieldDescriptor.H() && !o0Var.hasField(fieldDescriptor)) {
                list.add(str + fieldDescriptor.e());
            }
        }
        for (Map.Entry<Descriptors.FieldDescriptor, Object> entry : o0Var.getAllFields().entrySet()) {
            Descriptors.FieldDescriptor key = entry.getKey();
            Object value = entry.getValue();
            if (key.v() == Descriptors.FieldDescriptor.JavaType.MESSAGE) {
                if (key.i()) {
                    int i = 0;
                    for (o0 o0Var2 : (List) value) {
                        d(o0Var2, j(str, key, i), list);
                        i++;
                    }
                } else if (o0Var.hasField(key)) {
                    d((o0) value, j(str, key, -1), list);
                }
            }
        }
    }

    public static int e(l0 l0Var, Map<Descriptors.FieldDescriptor, Object> map) {
        int serializedSize;
        int p;
        boolean messageSetWireFormat = l0Var.getDescriptorForType().t().getMessageSetWireFormat();
        int i = 0;
        for (Map.Entry<Descriptors.FieldDescriptor, Object> entry : map.entrySet()) {
            Descriptors.FieldDescriptor key = entry.getKey();
            Object value = entry.getValue();
            if (messageSetWireFormat && key.C() && key.A() == Descriptors.FieldDescriptor.Type.MESSAGE && !key.i()) {
                p = CodedOutputStream.F(key.getNumber(), (l0) value);
            } else {
                p = w.p(key, value);
            }
            i += p;
        }
        g1 unknownFields = l0Var.getUnknownFields();
        if (messageSetWireFormat) {
            serializedSize = unknownFields.f();
        } else {
            serializedSize = unknownFields.getSerializedSize();
        }
        return i + serializedSize;
    }

    public static boolean f(o0 o0Var) {
        for (Descriptors.FieldDescriptor fieldDescriptor : o0Var.getDescriptorForType().p()) {
            if (fieldDescriptor.H() && !o0Var.hasField(fieldDescriptor)) {
                return false;
            }
        }
        for (Map.Entry<Descriptors.FieldDescriptor, Object> entry : o0Var.getAllFields().entrySet()) {
            Descriptors.FieldDescriptor key = entry.getKey();
            if (key.v() == Descriptors.FieldDescriptor.JavaType.MESSAGE) {
                if (key.i()) {
                    for (l0 l0Var : (List) entry.getValue()) {
                        if (!l0Var.isInitialized()) {
                            return false;
                        }
                    }
                    continue;
                } else if (!((l0) entry.getValue()).isInitialized()) {
                    return false;
                }
            }
        }
        return true;
    }

    /* JADX WARN: Removed duplicated region for block: B:39:0x0093  */
    /* JADX WARN: Removed duplicated region for block: B:44:0x009f  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static boolean g(com.google.protobuf.j r7, com.google.protobuf.g1.b r8, com.google.protobuf.r r9, com.google.protobuf.Descriptors.b r10, com.google.protobuf.MessageReflection.MergeTarget r11, int r12) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 352
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.MessageReflection.g(com.google.protobuf.j, com.google.protobuf.g1$b, com.google.protobuf.r, com.google.protobuf.Descriptors$b, com.google.protobuf.MessageReflection$MergeTarget, int):boolean");
    }

    public static void h(ByteString byteString, q.b bVar, r rVar, MergeTarget mergeTarget) throws IOException {
        Descriptors.FieldDescriptor fieldDescriptor = bVar.a;
        if (!mergeTarget.hasField(fieldDescriptor) && !r.c()) {
            mergeTarget.setField(fieldDescriptor, new b0(bVar.b, rVar, byteString));
        } else {
            mergeTarget.setField(fieldDescriptor, mergeTarget.f(byteString, rVar, fieldDescriptor, bVar.b));
        }
    }

    public static void i(j jVar, g1.b bVar, r rVar, Descriptors.b bVar2, MergeTarget mergeTarget) throws IOException {
        int i = 0;
        ByteString byteString = null;
        q.b bVar3 = null;
        while (true) {
            int J = jVar.J();
            if (J == 0) {
                break;
            } else if (J == WireFormat.c) {
                i = jVar.K();
                if (i != 0 && (rVar instanceof q)) {
                    bVar3 = mergeTarget.c((q) rVar, bVar2, i);
                }
            } else if (J == WireFormat.d) {
                if (i != 0 && bVar3 != null && r.c()) {
                    b(jVar, bVar3, rVar, mergeTarget);
                    byteString = null;
                } else {
                    byteString = jVar.q();
                }
            } else if (!jVar.N(J)) {
                break;
            }
        }
        jVar.a(WireFormat.b);
        if (byteString == null || i == 0) {
            return;
        }
        if (bVar3 != null) {
            h(byteString, bVar3, rVar, mergeTarget);
        } else if (bVar != null) {
            bVar.l(i, g1.c.t().e(byteString).g());
        }
    }

    public static String j(String str, Descriptors.FieldDescriptor fieldDescriptor, int i) {
        StringBuilder sb = new StringBuilder(str);
        if (fieldDescriptor.C()) {
            sb.append('(');
            sb.append(fieldDescriptor.d());
            sb.append(')');
        } else {
            sb.append(fieldDescriptor.e());
        }
        if (i != -1) {
            sb.append('[');
            sb.append(i);
            sb.append(']');
        }
        sb.append('.');
        return sb.toString();
    }

    public static void k(l0 l0Var, Map<Descriptors.FieldDescriptor, Object> map, CodedOutputStream codedOutputStream, boolean z) throws IOException {
        boolean messageSetWireFormat = l0Var.getDescriptorForType().t().getMessageSetWireFormat();
        if (z) {
            TreeMap treeMap = new TreeMap(map);
            for (Descriptors.FieldDescriptor fieldDescriptor : l0Var.getDescriptorForType().p()) {
                if (fieldDescriptor.H() && !treeMap.containsKey(fieldDescriptor)) {
                    treeMap.put(fieldDescriptor, l0Var.getField(fieldDescriptor));
                }
            }
            map = treeMap;
        }
        for (Map.Entry<Descriptors.FieldDescriptor, Object> entry : map.entrySet()) {
            Descriptors.FieldDescriptor key = entry.getKey();
            Object value = entry.getValue();
            if (messageSetWireFormat && key.C() && key.A() == Descriptors.FieldDescriptor.Type.MESSAGE && !key.i()) {
                codedOutputStream.N0(key.getNumber(), (l0) value);
            } else {
                w.R(key, value, codedOutputStream);
            }
        }
        g1 unknownFields = l0Var.getUnknownFields();
        if (messageSetWireFormat) {
            unknownFields.l(codedOutputStream);
        } else {
            unknownFields.writeTo(codedOutputStream);
        }
    }
}
