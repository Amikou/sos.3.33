package com.google.protobuf;

import com.google.protobuf.GeneratedMessageLite;

/* compiled from: NewInstanceSchemaLite.java */
/* loaded from: classes2.dex */
public final class s0 implements if2 {
    @Override // defpackage.if2
    public Object a(Object obj) {
        return ((GeneratedMessageLite) obj).b(GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE);
    }
}
