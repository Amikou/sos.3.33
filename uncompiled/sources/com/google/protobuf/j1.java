package com.google.protobuf;

import com.google.protobuf.g1;
import java.io.IOException;

/* compiled from: UnknownFieldSetSchema.java */
/* loaded from: classes2.dex */
public class j1 extends f1<g1, g1.b> {
    @Override // com.google.protobuf.f1
    /* renamed from: A */
    public g1 g(Object obj) {
        return ((GeneratedMessageV3) obj).unknownFields;
    }

    @Override // com.google.protobuf.f1
    /* renamed from: B */
    public int h(g1 g1Var) {
        return g1Var.getSerializedSize();
    }

    @Override // com.google.protobuf.f1
    /* renamed from: C */
    public int i(g1 g1Var) {
        return g1Var.f();
    }

    @Override // com.google.protobuf.f1
    /* renamed from: D */
    public g1 k(g1 g1Var, g1 g1Var2) {
        return g1Var.toBuilder().u(g1Var2).build();
    }

    @Override // com.google.protobuf.f1
    /* renamed from: E */
    public g1.b n() {
        return g1.g();
    }

    @Override // com.google.protobuf.f1
    /* renamed from: F */
    public void o(Object obj, g1.b bVar) {
        ((GeneratedMessageV3) obj).unknownFields = bVar.build();
    }

    @Override // com.google.protobuf.f1
    /* renamed from: G */
    public void p(Object obj, g1 g1Var) {
        ((GeneratedMessageV3) obj).unknownFields = g1Var;
    }

    @Override // com.google.protobuf.f1
    /* renamed from: H */
    public g1 r(g1.b bVar) {
        return bVar.build();
    }

    @Override // com.google.protobuf.f1
    /* renamed from: I */
    public void s(g1 g1Var, Writer writer) throws IOException {
        g1Var.m(writer);
    }

    @Override // com.google.protobuf.f1
    /* renamed from: J */
    public void t(g1 g1Var, Writer writer) throws IOException {
        g1Var.n(writer);
    }

    @Override // com.google.protobuf.f1
    public void j(Object obj) {
    }

    @Override // com.google.protobuf.f1
    public boolean q(w0 w0Var) {
        return w0Var.Q();
    }

    @Override // com.google.protobuf.f1
    /* renamed from: u */
    public void a(g1.b bVar, int i, int i2) {
        bVar.l(i, g1.c.t().b(i2).g());
    }

    @Override // com.google.protobuf.f1
    /* renamed from: v */
    public void b(g1.b bVar, int i, long j) {
        bVar.l(i, g1.c.t().c(j).g());
    }

    @Override // com.google.protobuf.f1
    /* renamed from: w */
    public void c(g1.b bVar, int i, g1 g1Var) {
        bVar.l(i, g1.c.t().d(g1Var).g());
    }

    @Override // com.google.protobuf.f1
    /* renamed from: x */
    public void d(g1.b bVar, int i, ByteString byteString) {
        bVar.l(i, g1.c.t().e(byteString).g());
    }

    @Override // com.google.protobuf.f1
    /* renamed from: y */
    public void e(g1.b bVar, int i, long j) {
        bVar.l(i, g1.c.t().f(j).g());
    }

    @Override // com.google.protobuf.f1
    /* renamed from: z */
    public g1.b f(Object obj) {
        return ((GeneratedMessageV3) obj).unknownFields.toBuilder();
    }
}
