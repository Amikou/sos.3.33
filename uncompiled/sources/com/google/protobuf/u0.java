package com.google.protobuf;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/* compiled from: Protobuf.java */
/* loaded from: classes2.dex */
public final class u0 {
    public static final u0 c = new u0();
    public final ConcurrentMap<Class<?>, y0<?>> b = new ConcurrentHashMap();
    public final md3 a = new q32();

    public static u0 a() {
        return c;
    }

    public <T> void b(T t, w0 w0Var, r rVar) throws IOException {
        e(t).i(t, w0Var, rVar);
    }

    public y0<?> c(Class<?> cls, y0<?> y0Var) {
        a0.b(cls, "messageType");
        a0.b(y0Var, "schema");
        return this.b.putIfAbsent(cls, y0Var);
    }

    public <T> y0<T> d(Class<T> cls) {
        a0.b(cls, "messageType");
        y0<T> y0Var = (y0<T>) this.b.get(cls);
        if (y0Var == null) {
            y0<T> a = this.a.a(cls);
            y0<T> y0Var2 = (y0<T>) c(cls, a);
            return y0Var2 != null ? y0Var2 : a;
        }
        return y0Var;
    }

    public <T> y0<T> e(T t) {
        return d(t.getClass());
    }
}
