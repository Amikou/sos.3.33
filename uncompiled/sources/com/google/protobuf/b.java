package com.google.protobuf;

import com.google.protobuf.ByteString;
import com.google.protobuf.b;
import com.google.protobuf.b.a;
import com.google.protobuf.m0;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/* compiled from: AbstractMessageLite.java */
/* loaded from: classes2.dex */
public abstract class b<MessageType extends b<MessageType, BuilderType>, BuilderType extends a<MessageType, BuilderType>> implements m0 {
    public int memoizedHashCode = 0;

    /* compiled from: AbstractMessageLite.java */
    /* loaded from: classes2.dex */
    public static abstract class a<MessageType extends b<MessageType, BuilderType>, BuilderType extends a<MessageType, BuilderType>> implements m0.a {
        @Deprecated
        public static <T> void addAll(Iterable<T> iterable, Collection<? super T> collection) {
            addAll((Iterable) iterable, (List) collection);
        }

        private static <T> void addAllCheckingNulls(Iterable<T> iterable, List<? super T> list) {
            if ((list instanceof ArrayList) && (iterable instanceof Collection)) {
                ((ArrayList) list).ensureCapacity(list.size() + ((Collection) iterable).size());
            }
            int size = list.size();
            for (T t : iterable) {
                if (t == null) {
                    String str = "Element at index " + (list.size() - size) + " is null.";
                    for (int size2 = list.size() - 1; size2 >= size; size2--) {
                        list.remove(size2);
                    }
                    throw new NullPointerException(str);
                }
                list.add(t);
            }
        }

        private String getReadingExceptionMessage(String str) {
            return "Reading " + getClass().getName() + " from a " + str + " threw an IOException (should never happen).";
        }

        public static UninitializedMessageException newUninitializedMessageException(m0 m0Var) {
            return new UninitializedMessageException(m0Var);
        }

        /* renamed from: clone */
        public abstract BuilderType m25clone();

        public abstract BuilderType internalMergeFrom(MessageType messagetype);

        public boolean mergeDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            int read = inputStream.read();
            if (read == -1) {
                return false;
            }
            m30mergeFrom((InputStream) new C0152a(inputStream, j.C(read, inputStream)), rVar);
            return true;
        }

        @Override // com.google.protobuf.m0.a
        public abstract BuilderType mergeFrom(j jVar, r rVar) throws IOException;

        public static <T> void addAll(Iterable<T> iterable, List<? super T> list) {
            a0.a(iterable);
            if (iterable instanceof cz1) {
                List<?> r = ((cz1) iterable).r();
                cz1 cz1Var = (cz1) list;
                int size = list.size();
                for (Object obj : r) {
                    if (obj == null) {
                        String str = "Element at index " + (cz1Var.size() - size) + " is null.";
                        for (int size2 = cz1Var.size() - 1; size2 >= size; size2--) {
                            cz1Var.remove(size2);
                        }
                        throw new NullPointerException(str);
                    } else if (obj instanceof ByteString) {
                        cz1Var.W((ByteString) obj);
                    } else {
                        cz1Var.add((String) obj);
                    }
                }
            } else if (iterable instanceof uu2) {
                list.addAll((Collection) iterable);
            } else {
                addAllCheckingNulls(iterable, list);
            }
        }

        /* compiled from: AbstractMessageLite.java */
        /* renamed from: com.google.protobuf.b$a$a  reason: collision with other inner class name */
        /* loaded from: classes2.dex */
        public static final class C0152a extends FilterInputStream {
            public int a;

            public C0152a(InputStream inputStream, int i) {
                super(inputStream);
                this.a = i;
            }

            @Override // java.io.FilterInputStream, java.io.InputStream
            public int available() throws IOException {
                return Math.min(super.available(), this.a);
            }

            @Override // java.io.FilterInputStream, java.io.InputStream
            public int read() throws IOException {
                if (this.a <= 0) {
                    return -1;
                }
                int read = super.read();
                if (read >= 0) {
                    this.a--;
                }
                return read;
            }

            @Override // java.io.FilterInputStream, java.io.InputStream
            public long skip(long j) throws IOException {
                long skip = super.skip(Math.min(j, this.a));
                if (skip >= 0) {
                    this.a = (int) (this.a - skip);
                }
                return skip;
            }

            @Override // java.io.FilterInputStream, java.io.InputStream
            public int read(byte[] bArr, int i, int i2) throws IOException {
                int i3 = this.a;
                if (i3 <= 0) {
                    return -1;
                }
                int read = super.read(bArr, i, Math.min(i2, i3));
                if (read >= 0) {
                    this.a -= read;
                }
                return read;
            }
        }

        public boolean mergeDelimitedFrom(InputStream inputStream) throws IOException {
            return mergeDelimitedFrom(inputStream, r.b());
        }

        /* renamed from: mergeFrom */
        public BuilderType m28mergeFrom(j jVar) throws IOException {
            return mergeFrom(jVar, r.b());
        }

        /* renamed from: mergeFrom */
        public BuilderType m26mergeFrom(ByteString byteString) throws InvalidProtocolBufferException {
            try {
                j newCodedInput = byteString.newCodedInput();
                m28mergeFrom(newCodedInput);
                newCodedInput.a(0);
                return this;
            } catch (InvalidProtocolBufferException e) {
                throw e;
            } catch (IOException e2) {
                throw new RuntimeException(getReadingExceptionMessage("ByteString"), e2);
            }
        }

        /* renamed from: mergeFrom */
        public BuilderType m27mergeFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            try {
                j newCodedInput = byteString.newCodedInput();
                mergeFrom(newCodedInput, rVar);
                newCodedInput.a(0);
                return this;
            } catch (InvalidProtocolBufferException e) {
                throw e;
            } catch (IOException e2) {
                throw new RuntimeException(getReadingExceptionMessage("ByteString"), e2);
            }
        }

        @Override // com.google.protobuf.m0.a
        public BuilderType mergeFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return m31mergeFrom(bArr, 0, bArr.length);
        }

        /* renamed from: mergeFrom */
        public BuilderType m31mergeFrom(byte[] bArr, int i, int i2) throws InvalidProtocolBufferException {
            try {
                j l = j.l(bArr, i, i2);
                m28mergeFrom(l);
                l.a(0);
                return this;
            } catch (InvalidProtocolBufferException e) {
                throw e;
            } catch (IOException e2) {
                throw new RuntimeException(getReadingExceptionMessage("byte array"), e2);
            }
        }

        /* renamed from: mergeFrom */
        public BuilderType m33mergeFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return m32mergeFrom(bArr, 0, bArr.length, rVar);
        }

        /* renamed from: mergeFrom */
        public BuilderType m32mergeFrom(byte[] bArr, int i, int i2, r rVar) throws InvalidProtocolBufferException {
            try {
                j l = j.l(bArr, i, i2);
                mergeFrom(l, rVar);
                l.a(0);
                return this;
            } catch (InvalidProtocolBufferException e) {
                throw e;
            } catch (IOException e2) {
                throw new RuntimeException(getReadingExceptionMessage("byte array"), e2);
            }
        }

        /* renamed from: mergeFrom */
        public BuilderType m29mergeFrom(InputStream inputStream) throws IOException {
            j g = j.g(inputStream);
            m28mergeFrom(g);
            g.a(0);
            return this;
        }

        /* renamed from: mergeFrom */
        public BuilderType m30mergeFrom(InputStream inputStream, r rVar) throws IOException {
            j g = j.g(inputStream);
            mergeFrom(g, rVar);
            g.a(0);
            return this;
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.google.protobuf.m0.a
        public BuilderType mergeFrom(m0 m0Var) {
            if (getDefaultInstanceForType().getClass().isInstance(m0Var)) {
                return (BuilderType) internalMergeFrom((b) m0Var);
            }
            throw new IllegalArgumentException("mergeFrom(MessageLite) can only merge messages of the same type.");
        }
    }

    @Deprecated
    public static <T> void addAll(Iterable<T> iterable, Collection<? super T> collection) {
        a.addAll((Iterable) iterable, (List) collection);
    }

    public static void checkByteStringIsUtf8(ByteString byteString) throws IllegalArgumentException {
        if (!byteString.isValidUtf8()) {
            throw new IllegalArgumentException("Byte string is not UTF-8.");
        }
    }

    private String getSerializingExceptionMessage(String str) {
        return "Serializing " + getClass().getName() + " to a " + str + " threw an IOException (should never happen).";
    }

    int getMemoizedSerializedSize() {
        throw new UnsupportedOperationException();
    }

    public int getSerializedSize(y0 y0Var) {
        int memoizedSerializedSize = getMemoizedSerializedSize();
        if (memoizedSerializedSize == -1) {
            int g = y0Var.g(this);
            setMemoizedSerializedSize(g);
            return g;
        }
        return memoizedSerializedSize;
    }

    public UninitializedMessageException newUninitializedMessageException() {
        return new UninitializedMessageException(this);
    }

    void setMemoizedSerializedSize(int i) {
        throw new UnsupportedOperationException();
    }

    @Override // com.google.protobuf.m0
    public byte[] toByteArray() {
        try {
            byte[] bArr = new byte[getSerializedSize()];
            CodedOutputStream i0 = CodedOutputStream.i0(bArr);
            writeTo(i0);
            i0.d();
            return bArr;
        } catch (IOException e) {
            throw new RuntimeException(getSerializingExceptionMessage("byte array"), e);
        }
    }

    @Override // com.google.protobuf.m0
    public ByteString toByteString() {
        try {
            ByteString.g newCodedBuilder = ByteString.newCodedBuilder(getSerializedSize());
            writeTo(newCodedBuilder.b());
            return newCodedBuilder.a();
        } catch (IOException e) {
            throw new RuntimeException(getSerializingExceptionMessage("ByteString"), e);
        }
    }

    public void writeDelimitedTo(OutputStream outputStream) throws IOException {
        int serializedSize = getSerializedSize();
        CodedOutputStream h0 = CodedOutputStream.h0(outputStream, CodedOutputStream.K(CodedOutputStream.M(serializedSize) + serializedSize));
        h0.P0(serializedSize);
        writeTo(h0);
        h0.e0();
    }

    public void writeTo(OutputStream outputStream) throws IOException {
        CodedOutputStream h0 = CodedOutputStream.h0(outputStream, CodedOutputStream.K(getSerializedSize()));
        writeTo(h0);
        h0.e0();
    }

    public static <T> void addAll(Iterable<T> iterable, List<? super T> list) {
        a.addAll((Iterable) iterable, (List) list);
    }
}
