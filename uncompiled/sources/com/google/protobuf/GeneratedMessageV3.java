package com.google.protobuf;

import com.google.protobuf.Descriptors;
import com.google.protobuf.GeneratedMessage;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.MessageReflection;
import com.google.protobuf.WireFormat;
import com.google.protobuf.a;
import com.google.protobuf.a0;
import com.google.protobuf.b0;
import com.google.protobuf.g1;
import com.google.protobuf.l0;
import com.google.protobuf.m0;
import com.google.protobuf.o;
import com.google.protobuf.w;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/* loaded from: classes2.dex */
public abstract class GeneratedMessageV3 extends com.google.protobuf.a implements Serializable {
    public static boolean alwaysUseFieldBuilders = false;
    private static final long serialVersionUID = 1;
    public g1 unknownFields;

    /* loaded from: classes2.dex */
    public class a implements c {
        public final /* synthetic */ a.b a;

        public a(GeneratedMessageV3 generatedMessageV3, a.b bVar) {
            this.a = bVar;
        }

        @Override // com.google.protobuf.a.b
        public void a() {
            this.a.a();
        }
    }

    /* loaded from: classes2.dex */
    public static abstract class b<BuilderType extends b<BuilderType>> extends a.AbstractC0151a<BuilderType> {
        private c builderParent;
        private boolean isClean;
        private b<BuilderType>.a meAsParent;
        private g1 unknownFields;

        /* loaded from: classes2.dex */
        public class a implements c {
            public a() {
            }

            @Override // com.google.protobuf.a.b
            public void a() {
                b.this.onChanged();
            }

            public /* synthetic */ a(b bVar, a aVar) {
                this();
            }
        }

        public b() {
            this(null);
        }

        /* JADX INFO: Access modifiers changed from: private */
        public Map<Descriptors.FieldDescriptor, Object> getAllFieldsMutable() {
            TreeMap treeMap = new TreeMap();
            List<Descriptors.FieldDescriptor> p = internalGetFieldAccessorTable().a.p();
            int i = 0;
            while (i < p.size()) {
                Descriptors.FieldDescriptor fieldDescriptor = p.get(i);
                Descriptors.g o = fieldDescriptor.o();
                if (o != null) {
                    i += o.o() - 1;
                    if (hasOneof(o)) {
                        fieldDescriptor = getOneofFieldDescriptor(o);
                        treeMap.put(fieldDescriptor, getField(fieldDescriptor));
                        i++;
                    } else {
                        i++;
                    }
                } else {
                    if (fieldDescriptor.i()) {
                        List list = (List) getField(fieldDescriptor);
                        if (!list.isEmpty()) {
                            treeMap.put(fieldDescriptor, list);
                        }
                    } else {
                        if (!hasField(fieldDescriptor)) {
                        }
                        treeMap.put(fieldDescriptor, getField(fieldDescriptor));
                    }
                    i++;
                }
            }
            return treeMap;
        }

        private BuilderType setUnknownFieldsInternal(g1 g1Var) {
            this.unknownFields = g1Var;
            onChanged();
            return this;
        }

        @Override // com.google.protobuf.a.AbstractC0151a
        void dispose() {
            this.builderParent = null;
        }

        @Override // com.google.protobuf.o0
        public Map<Descriptors.FieldDescriptor, Object> getAllFields() {
            return Collections.unmodifiableMap(getAllFieldsMutable());
        }

        public Descriptors.b getDescriptorForType() {
            return internalGetFieldAccessorTable().a;
        }

        @Override // com.google.protobuf.o0
        public Object getField(Descriptors.FieldDescriptor fieldDescriptor) {
            Object c = internalGetFieldAccessorTable().e(fieldDescriptor).c(this);
            return fieldDescriptor.i() ? Collections.unmodifiableList((List) c) : c;
        }

        @Override // com.google.protobuf.a.AbstractC0151a
        public l0.a getFieldBuilder(Descriptors.FieldDescriptor fieldDescriptor) {
            return internalGetFieldAccessorTable().e(fieldDescriptor).p(this);
        }

        @Override // com.google.protobuf.a.AbstractC0151a
        public Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar) {
            return internalGetFieldAccessorTable().f(gVar).b(this);
        }

        public c getParentForChildren() {
            if (this.meAsParent == null) {
                this.meAsParent = new a(this, null);
            }
            return this.meAsParent;
        }

        public Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i) {
            return internalGetFieldAccessorTable().e(fieldDescriptor).j(this, i);
        }

        @Override // com.google.protobuf.a.AbstractC0151a
        public l0.a getRepeatedFieldBuilder(Descriptors.FieldDescriptor fieldDescriptor, int i) {
            return internalGetFieldAccessorTable().e(fieldDescriptor).n(this, i);
        }

        public int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor) {
            return internalGetFieldAccessorTable().e(fieldDescriptor).e(this);
        }

        @Override // com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.o0
        public boolean hasField(Descriptors.FieldDescriptor fieldDescriptor) {
            return internalGetFieldAccessorTable().e(fieldDescriptor).l(this);
        }

        @Override // com.google.protobuf.a.AbstractC0151a
        public boolean hasOneof(Descriptors.g gVar) {
            return internalGetFieldAccessorTable().f(gVar).d(this);
        }

        public abstract e internalGetFieldAccessorTable();

        public MapField internalGetMapField(int i) {
            throw new RuntimeException("No map fields found in " + getClass().getName());
        }

        public MapField internalGetMutableMapField(int i) {
            throw new RuntimeException("No map fields found in " + getClass().getName());
        }

        public boolean isClean() {
            return this.isClean;
        }

        @Override // defpackage.d82
        public boolean isInitialized() {
            for (Descriptors.FieldDescriptor fieldDescriptor : getDescriptorForType().p()) {
                if (fieldDescriptor.H() && !hasField(fieldDescriptor)) {
                    return false;
                }
                if (fieldDescriptor.v() == Descriptors.FieldDescriptor.JavaType.MESSAGE) {
                    if (fieldDescriptor.i()) {
                        for (l0 l0Var : (List) getField(fieldDescriptor)) {
                            if (!l0Var.isInitialized()) {
                                return false;
                            }
                        }
                        continue;
                    } else if (hasField(fieldDescriptor) && !((l0) getField(fieldDescriptor)).isInitialized()) {
                        return false;
                    }
                }
            }
            return true;
        }

        @Override // com.google.protobuf.a.AbstractC0151a
        public void markClean() {
            this.isClean = true;
        }

        @Override // com.google.protobuf.l0.a
        public l0.a newBuilderForField(Descriptors.FieldDescriptor fieldDescriptor) {
            return internalGetFieldAccessorTable().e(fieldDescriptor).m();
        }

        public void onBuilt() {
            if (this.builderParent != null) {
                markClean();
            }
        }

        public final void onChanged() {
            c cVar;
            if (!this.isClean || (cVar = this.builderParent) == null) {
                return;
            }
            cVar.a();
            this.isClean = false;
        }

        public BuilderType setUnknownFieldsProto3(g1 g1Var) {
            return setUnknownFieldsInternal(g1Var);
        }

        public b(c cVar) {
            this.unknownFields = g1.c();
            this.builderParent = cVar;
        }

        public BuilderType addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
            internalGetFieldAccessorTable().e(fieldDescriptor).d(this, obj);
            return this;
        }

        public BuilderType clearField(Descriptors.FieldDescriptor fieldDescriptor) {
            internalGetFieldAccessorTable().e(fieldDescriptor).a(this);
            return this;
        }

        public BuilderType setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
            internalGetFieldAccessorTable().e(fieldDescriptor).h(this, obj);
            return this;
        }

        /* renamed from: setRepeatedField */
        public BuilderType m19setRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
            internalGetFieldAccessorTable().e(fieldDescriptor).i(this, i, obj);
            return this;
        }

        public BuilderType setUnknownFields(g1 g1Var) {
            return setUnknownFieldsInternal(g1Var);
        }

        @Override // com.google.protobuf.a.AbstractC0151a
        public BuilderType clearOneof(Descriptors.g gVar) {
            internalGetFieldAccessorTable().f(gVar).a(this);
            return this;
        }

        @Override // com.google.protobuf.a.AbstractC0151a
        public BuilderType mergeUnknownFields(g1 g1Var) {
            return setUnknownFields(g1.h(this.unknownFields).u(g1Var).build());
        }

        @Override // com.google.protobuf.a.AbstractC0151a
        public BuilderType clear() {
            this.unknownFields = g1.c();
            onChanged();
            return this;
        }

        @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
        public BuilderType clone() {
            BuilderType buildertype = (BuilderType) getDefaultInstanceForType().newBuilderForType();
            buildertype.mergeFrom(buildPartial());
            return buildertype;
        }
    }

    /* loaded from: classes2.dex */
    public interface c extends a.b {
    }

    /* loaded from: classes2.dex */
    public static abstract class d<MessageType extends ExtendableMessage, BuilderType extends d<MessageType, BuilderType>> extends b<BuilderType> implements o0 {
        public w.b<Descriptors.FieldDescriptor> a;

        public d() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public w<Descriptors.FieldDescriptor> d() {
            w.b<Descriptors.FieldDescriptor> bVar = this.a;
            if (bVar == null) {
                return w.r();
            }
            return bVar.b();
        }

        private void o(Descriptors.FieldDescriptor fieldDescriptor) {
            if (fieldDescriptor.p() != getDescriptorForType()) {
                throw new IllegalArgumentException("FieldDescriptor does not match message type.");
            }
        }

        @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
        /* renamed from: b */
        public BuilderType addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
            if (fieldDescriptor.C()) {
                o(fieldDescriptor);
                g();
                this.a.a(fieldDescriptor, obj);
                onChanged();
                return this;
            }
            return (BuilderType) super.addRepeatedField(fieldDescriptor, obj);
        }

        @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
        /* renamed from: e */
        public BuilderType clear() {
            this.a = null;
            return (BuilderType) super.clear();
        }

        @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
        /* renamed from: f */
        public BuilderType clearField(Descriptors.FieldDescriptor fieldDescriptor) {
            if (fieldDescriptor.C()) {
                o(fieldDescriptor);
                g();
                this.a.c(fieldDescriptor);
                onChanged();
                return this;
            }
            return (BuilderType) super.clearField(fieldDescriptor);
        }

        public final void g() {
            if (this.a == null) {
                this.a = w.K();
            }
        }

        @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.o0
        public Map<Descriptors.FieldDescriptor, Object> getAllFields() {
            Map allFieldsMutable = getAllFieldsMutable();
            w.b<Descriptors.FieldDescriptor> bVar = this.a;
            if (bVar != null) {
                allFieldsMutable.putAll(bVar.e());
            }
            return Collections.unmodifiableMap(allFieldsMutable);
        }

        @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.o0
        public Object getField(Descriptors.FieldDescriptor fieldDescriptor) {
            if (fieldDescriptor.C()) {
                o(fieldDescriptor);
                w.b<Descriptors.FieldDescriptor> bVar = this.a;
                Object f = bVar == null ? null : bVar.f(fieldDescriptor);
                if (f == null) {
                    if (fieldDescriptor.v() == Descriptors.FieldDescriptor.JavaType.MESSAGE) {
                        return o.e(fieldDescriptor.x());
                    }
                    return fieldDescriptor.r();
                }
                return f;
            }
            return super.getField(fieldDescriptor);
        }

        @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
        public l0.a getFieldBuilder(Descriptors.FieldDescriptor fieldDescriptor) {
            if (fieldDescriptor.C()) {
                o(fieldDescriptor);
                if (fieldDescriptor.v() == Descriptors.FieldDescriptor.JavaType.MESSAGE) {
                    g();
                    Object g = this.a.g(fieldDescriptor);
                    if (g == null) {
                        o.b h = o.h(fieldDescriptor.x());
                        this.a.s(fieldDescriptor, h);
                        onChanged();
                        return h;
                    } else if (g instanceof l0.a) {
                        return (l0.a) g;
                    } else {
                        if (g instanceof l0) {
                            l0.a builder = ((l0) g).toBuilder();
                            this.a.s(fieldDescriptor, builder);
                            onChanged();
                            return builder;
                        }
                        throw new UnsupportedOperationException("getRepeatedFieldBuilder() called on a non-Message type.");
                    }
                }
                throw new UnsupportedOperationException("getFieldBuilder() called on a non-Message type.");
            }
            return super.getFieldBuilder(fieldDescriptor);
        }

        @Override // com.google.protobuf.GeneratedMessageV3.b
        public Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i) {
            if (fieldDescriptor.C()) {
                o(fieldDescriptor);
                w.b<Descriptors.FieldDescriptor> bVar = this.a;
                if (bVar != null) {
                    return bVar.h(fieldDescriptor, i);
                }
                throw new IndexOutOfBoundsException();
            }
            return super.getRepeatedField(fieldDescriptor, i);
        }

        @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
        public l0.a getRepeatedFieldBuilder(Descriptors.FieldDescriptor fieldDescriptor, int i) {
            if (fieldDescriptor.C()) {
                o(fieldDescriptor);
                g();
                if (fieldDescriptor.v() == Descriptors.FieldDescriptor.JavaType.MESSAGE) {
                    Object i2 = this.a.i(fieldDescriptor, i);
                    if (i2 instanceof l0.a) {
                        return (l0.a) i2;
                    }
                    if (i2 instanceof l0) {
                        l0.a builder = ((l0) i2).toBuilder();
                        this.a.t(fieldDescriptor, i, builder);
                        onChanged();
                        return builder;
                    }
                    throw new UnsupportedOperationException("getRepeatedFieldBuilder() called on a non-Message type.");
                }
                throw new UnsupportedOperationException("getRepeatedFieldBuilder() called on a non-Message type.");
            }
            return super.getRepeatedFieldBuilder(fieldDescriptor, i);
        }

        @Override // com.google.protobuf.GeneratedMessageV3.b
        public int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor) {
            if (fieldDescriptor.C()) {
                o(fieldDescriptor);
                w.b<Descriptors.FieldDescriptor> bVar = this.a;
                if (bVar == null) {
                    return 0;
                }
                return bVar.j(fieldDescriptor);
            }
            return super.getRepeatedFieldCount(fieldDescriptor);
        }

        public boolean h() {
            w.b<Descriptors.FieldDescriptor> bVar = this.a;
            if (bVar == null) {
                return true;
            }
            return bVar.l();
        }

        @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.o0
        public boolean hasField(Descriptors.FieldDescriptor fieldDescriptor) {
            if (fieldDescriptor.C()) {
                o(fieldDescriptor);
                w.b<Descriptors.FieldDescriptor> bVar = this.a;
                if (bVar == null) {
                    return false;
                }
                return bVar.k(fieldDescriptor);
            }
            return super.hasField(fieldDescriptor);
        }

        @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
        public boolean isInitialized() {
            return super.isInitialized() && h();
        }

        public final void j(ExtendableMessage extendableMessage) {
            if (extendableMessage.extensions != null) {
                g();
                this.a.m(extendableMessage.extensions);
                onChanged();
            }
        }

        @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
        /* renamed from: k */
        public BuilderType setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
            if (fieldDescriptor.C()) {
                o(fieldDescriptor);
                g();
                this.a.s(fieldDescriptor, obj);
                onChanged();
                return this;
            }
            return (BuilderType) super.setField(fieldDescriptor, obj);
        }

        @Override // com.google.protobuf.GeneratedMessageV3.b
        /* renamed from: l */
        public BuilderType setRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
            if (fieldDescriptor.C()) {
                o(fieldDescriptor);
                g();
                this.a.t(fieldDescriptor, i, obj);
                onChanged();
                return this;
            }
            return (BuilderType) super.m19setRepeatedField(fieldDescriptor, i, obj);
        }

        @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
        public l0.a newBuilderForField(Descriptors.FieldDescriptor fieldDescriptor) {
            if (fieldDescriptor.C()) {
                return o.h(fieldDescriptor.x());
            }
            return super.newBuilderForField(fieldDescriptor);
        }

        public d(c cVar) {
            super(cVar);
        }
    }

    /* loaded from: classes2.dex */
    public static final class e {
        public final Descriptors.b a;
        public final a[] b;
        public String[] c;
        public final c[] d;
        public volatile boolean e = false;

        /* loaded from: classes2.dex */
        public interface a {
            void a(b bVar);

            Object b(GeneratedMessageV3 generatedMessageV3);

            Object c(b bVar);

            void d(b bVar, Object obj);

            int e(b bVar);

            int f(GeneratedMessageV3 generatedMessageV3);

            boolean g(GeneratedMessageV3 generatedMessageV3);

            void h(b bVar, Object obj);

            void i(b bVar, int i, Object obj);

            Object j(b bVar, int i);

            Object k(GeneratedMessageV3 generatedMessageV3, int i);

            boolean l(b bVar);

            l0.a m();

            l0.a n(b bVar, int i);

            Object o(GeneratedMessageV3 generatedMessageV3);

            l0.a p(b bVar);
        }

        /* loaded from: classes2.dex */
        public static class b implements a {
            public final Descriptors.FieldDescriptor a;
            public final l0 b;

            public b(Descriptors.FieldDescriptor fieldDescriptor, String str, Class<? extends GeneratedMessageV3> cls, Class<? extends b> cls2) {
                this.a = fieldDescriptor;
                this.b = s((GeneratedMessageV3) GeneratedMessageV3.invokeOrDie(GeneratedMessageV3.getMethodOrDie(cls, "getDefaultInstance", new Class[0]), null, new Object[0])).k();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public void a(b bVar) {
                t(bVar).l().clear();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public Object b(GeneratedMessageV3 generatedMessageV3) {
                ArrayList arrayList = new ArrayList();
                for (int i = 0; i < f(generatedMessageV3); i++) {
                    arrayList.add(k(generatedMessageV3, i));
                }
                return Collections.unmodifiableList(arrayList);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public Object c(b bVar) {
                ArrayList arrayList = new ArrayList();
                for (int i = 0; i < e(bVar); i++) {
                    arrayList.add(j(bVar, i));
                }
                return Collections.unmodifiableList(arrayList);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public void d(b bVar, Object obj) {
                t(bVar).l().add(q((l0) obj));
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public int e(b bVar) {
                return r(bVar).i().size();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public int f(GeneratedMessageV3 generatedMessageV3) {
                return s(generatedMessageV3).i().size();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public boolean g(GeneratedMessageV3 generatedMessageV3) {
                throw new UnsupportedOperationException("hasField() is not supported for repeated fields.");
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public void h(b bVar, Object obj) {
                a(bVar);
                for (Object obj2 : (List) obj) {
                    d(bVar, obj2);
                }
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public void i(b bVar, int i, Object obj) {
                t(bVar).l().set(i, q((l0) obj));
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public Object j(b bVar, int i) {
                return r(bVar).i().get(i);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public Object k(GeneratedMessageV3 generatedMessageV3, int i) {
                return s(generatedMessageV3).i().get(i);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public boolean l(b bVar) {
                throw new UnsupportedOperationException("hasField() is not supported for repeated fields.");
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public l0.a m() {
                return this.b.newBuilderForType();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public l0.a n(b bVar, int i) {
                throw new UnsupportedOperationException("Nested builder not supported for map fields.");
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public Object o(GeneratedMessageV3 generatedMessageV3) {
                return b(generatedMessageV3);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public l0.a p(b bVar) {
                throw new UnsupportedOperationException("Nested builder not supported for map fields.");
            }

            public final l0 q(l0 l0Var) {
                if (l0Var == null) {
                    return null;
                }
                return this.b.getClass().isInstance(l0Var) ? l0Var : this.b.toBuilder().mergeFrom(l0Var).build();
            }

            public final MapField<?, ?> r(b bVar) {
                return bVar.internalGetMapField(this.a.getNumber());
            }

            public final MapField<?, ?> s(GeneratedMessageV3 generatedMessageV3) {
                return generatedMessageV3.internalGetMapField(this.a.getNumber());
            }

            public final MapField<?, ?> t(b bVar) {
                return bVar.internalGetMutableMapField(this.a.getNumber());
            }
        }

        /* loaded from: classes2.dex */
        public static class c {
            public final Descriptors.b a;
            public final Method b;
            public final Method c;
            public final Method d;
            public final Descriptors.FieldDescriptor e;

            public c(Descriptors.b bVar, int i, String str, Class<? extends GeneratedMessageV3> cls, Class<? extends b> cls2) {
                this.a = bVar;
                Descriptors.g gVar = bVar.s().get(i);
                if (gVar.s()) {
                    this.b = null;
                    this.c = null;
                    this.e = gVar.p().get(0);
                } else {
                    this.b = GeneratedMessageV3.getMethodOrDie(cls, "get" + str + "Case", new Class[0]);
                    this.c = GeneratedMessageV3.getMethodOrDie(cls2, "get" + str + "Case", new Class[0]);
                    this.e = null;
                }
                this.d = GeneratedMessageV3.getMethodOrDie(cls2, "clear" + str, new Class[0]);
            }

            public void a(b bVar) {
                GeneratedMessageV3.invokeOrDie(this.d, bVar, new Object[0]);
            }

            public Descriptors.FieldDescriptor b(b bVar) {
                Descriptors.FieldDescriptor fieldDescriptor = this.e;
                if (fieldDescriptor == null) {
                    int number = ((a0.c) GeneratedMessageV3.invokeOrDie(this.c, bVar, new Object[0])).getNumber();
                    if (number > 0) {
                        return this.a.k(number);
                    }
                    return null;
                } else if (bVar.hasField(fieldDescriptor)) {
                    return this.e;
                } else {
                    return null;
                }
            }

            public Descriptors.FieldDescriptor c(GeneratedMessageV3 generatedMessageV3) {
                Descriptors.FieldDescriptor fieldDescriptor = this.e;
                if (fieldDescriptor == null) {
                    int number = ((a0.c) GeneratedMessageV3.invokeOrDie(this.b, generatedMessageV3, new Object[0])).getNumber();
                    if (number > 0) {
                        return this.a.k(number);
                    }
                    return null;
                } else if (generatedMessageV3.hasField(fieldDescriptor)) {
                    return this.e;
                } else {
                    return null;
                }
            }

            public boolean d(b bVar) {
                Descriptors.FieldDescriptor fieldDescriptor = this.e;
                if (fieldDescriptor != null) {
                    return bVar.hasField(fieldDescriptor);
                }
                return ((a0.c) GeneratedMessageV3.invokeOrDie(this.c, bVar, new Object[0])).getNumber() != 0;
            }

            public boolean e(GeneratedMessageV3 generatedMessageV3) {
                Descriptors.FieldDescriptor fieldDescriptor = this.e;
                if (fieldDescriptor != null) {
                    return generatedMessageV3.hasField(fieldDescriptor);
                }
                return ((a0.c) GeneratedMessageV3.invokeOrDie(this.b, generatedMessageV3, new Object[0])).getNumber() != 0;
            }
        }

        /* loaded from: classes2.dex */
        public static final class d extends C0149e {
            public Descriptors.c c;
            public final Method d;
            public final Method e;
            public boolean f;
            public Method g;
            public Method h;
            public Method i;
            public Method j;

            public d(Descriptors.FieldDescriptor fieldDescriptor, String str, Class<? extends GeneratedMessageV3> cls, Class<? extends b> cls2) {
                super(fieldDescriptor, str, cls, cls2);
                this.c = fieldDescriptor.s();
                this.d = GeneratedMessageV3.getMethodOrDie(this.a, "valueOf", Descriptors.d.class);
                this.e = GeneratedMessageV3.getMethodOrDie(this.a, "getValueDescriptor", new Class[0]);
                boolean x = fieldDescriptor.a().x();
                this.f = x;
                if (x) {
                    Class cls3 = Integer.TYPE;
                    this.g = GeneratedMessageV3.getMethodOrDie(cls, "get" + str + "Value", cls3);
                    this.h = GeneratedMessageV3.getMethodOrDie(cls2, "get" + str + "Value", cls3);
                    this.i = GeneratedMessageV3.getMethodOrDie(cls2, "set" + str + "Value", cls3, cls3);
                    this.j = GeneratedMessageV3.getMethodOrDie(cls2, "add" + str + "Value", cls3);
                }
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.C0149e, com.google.protobuf.GeneratedMessageV3.e.a
            public Object b(GeneratedMessageV3 generatedMessageV3) {
                ArrayList arrayList = new ArrayList();
                int f = f(generatedMessageV3);
                for (int i = 0; i < f; i++) {
                    arrayList.add(k(generatedMessageV3, i));
                }
                return Collections.unmodifiableList(arrayList);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.C0149e, com.google.protobuf.GeneratedMessageV3.e.a
            public Object c(b bVar) {
                ArrayList arrayList = new ArrayList();
                int e = e(bVar);
                for (int i = 0; i < e; i++) {
                    arrayList.add(j(bVar, i));
                }
                return Collections.unmodifiableList(arrayList);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.C0149e, com.google.protobuf.GeneratedMessageV3.e.a
            public void d(b bVar, Object obj) {
                if (this.f) {
                    GeneratedMessageV3.invokeOrDie(this.j, bVar, Integer.valueOf(((Descriptors.d) obj).getNumber()));
                } else {
                    super.d(bVar, GeneratedMessageV3.invokeOrDie(this.d, null, obj));
                }
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.C0149e, com.google.protobuf.GeneratedMessageV3.e.a
            public void i(b bVar, int i, Object obj) {
                if (this.f) {
                    GeneratedMessageV3.invokeOrDie(this.i, bVar, Integer.valueOf(i), Integer.valueOf(((Descriptors.d) obj).getNumber()));
                } else {
                    super.i(bVar, i, GeneratedMessageV3.invokeOrDie(this.d, null, obj));
                }
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.C0149e, com.google.protobuf.GeneratedMessageV3.e.a
            public Object j(b bVar, int i) {
                if (!this.f) {
                    return GeneratedMessageV3.invokeOrDie(this.e, super.j(bVar, i), new Object[0]);
                }
                return this.c.j(((Integer) GeneratedMessageV3.invokeOrDie(this.h, bVar, Integer.valueOf(i))).intValue());
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.C0149e, com.google.protobuf.GeneratedMessageV3.e.a
            public Object k(GeneratedMessageV3 generatedMessageV3, int i) {
                if (!this.f) {
                    return GeneratedMessageV3.invokeOrDie(this.e, super.k(generatedMessageV3, i), new Object[0]);
                }
                return this.c.j(((Integer) GeneratedMessageV3.invokeOrDie(this.g, generatedMessageV3, Integer.valueOf(i))).intValue());
            }
        }

        /* renamed from: com.google.protobuf.GeneratedMessageV3$e$e  reason: collision with other inner class name */
        /* loaded from: classes2.dex */
        public static class C0149e implements a {
            public final Class a;
            public final a b;

            /* renamed from: com.google.protobuf.GeneratedMessageV3$e$e$a */
            /* loaded from: classes2.dex */
            public interface a {
                void a(b<?> bVar);

                Object b(GeneratedMessageV3 generatedMessageV3);

                Object c(b<?> bVar);

                void d(b<?> bVar, Object obj);

                int e(b<?> bVar);

                int f(GeneratedMessageV3 generatedMessageV3);

                void i(b<?> bVar, int i, Object obj);

                Object j(b<?> bVar, int i);

                Object k(GeneratedMessageV3 generatedMessageV3, int i);
            }

            /* renamed from: com.google.protobuf.GeneratedMessageV3$e$e$b */
            /* loaded from: classes2.dex */
            public static final class b implements a {
                public final Method a;
                public final Method b;
                public final Method c;
                public final Method d;
                public final Method e;
                public final Method f;
                public final Method g;
                public final Method h;
                public final Method i;

                public b(Descriptors.FieldDescriptor fieldDescriptor, String str, Class<? extends GeneratedMessageV3> cls, Class<? extends b> cls2) {
                    this.a = GeneratedMessageV3.getMethodOrDie(cls, "get" + str + "List", new Class[0]);
                    this.b = GeneratedMessageV3.getMethodOrDie(cls2, "get" + str + "List", new Class[0]);
                    StringBuilder sb = new StringBuilder();
                    sb.append("get");
                    sb.append(str);
                    String sb2 = sb.toString();
                    Class cls3 = Integer.TYPE;
                    Method methodOrDie = GeneratedMessageV3.getMethodOrDie(cls, sb2, cls3);
                    this.c = methodOrDie;
                    this.d = GeneratedMessageV3.getMethodOrDie(cls2, "get" + str, cls3);
                    Class<?> returnType = methodOrDie.getReturnType();
                    this.e = GeneratedMessageV3.getMethodOrDie(cls2, "set" + str, cls3, returnType);
                    this.f = GeneratedMessageV3.getMethodOrDie(cls2, "add" + str, returnType);
                    this.g = GeneratedMessageV3.getMethodOrDie(cls, "get" + str + "Count", new Class[0]);
                    this.h = GeneratedMessageV3.getMethodOrDie(cls2, "get" + str + "Count", new Class[0]);
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("clear");
                    sb3.append(str);
                    this.i = GeneratedMessageV3.getMethodOrDie(cls2, sb3.toString(), new Class[0]);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.e.C0149e.a
                public void a(b<?> bVar) {
                    GeneratedMessageV3.invokeOrDie(this.i, bVar, new Object[0]);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.e.C0149e.a
                public Object b(GeneratedMessageV3 generatedMessageV3) {
                    return GeneratedMessageV3.invokeOrDie(this.a, generatedMessageV3, new Object[0]);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.e.C0149e.a
                public Object c(b<?> bVar) {
                    return GeneratedMessageV3.invokeOrDie(this.b, bVar, new Object[0]);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.e.C0149e.a
                public void d(b<?> bVar, Object obj) {
                    GeneratedMessageV3.invokeOrDie(this.f, bVar, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.e.C0149e.a
                public int e(b<?> bVar) {
                    return ((Integer) GeneratedMessageV3.invokeOrDie(this.h, bVar, new Object[0])).intValue();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.e.C0149e.a
                public int f(GeneratedMessageV3 generatedMessageV3) {
                    return ((Integer) GeneratedMessageV3.invokeOrDie(this.g, generatedMessageV3, new Object[0])).intValue();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.e.C0149e.a
                public void i(b<?> bVar, int i, Object obj) {
                    GeneratedMessageV3.invokeOrDie(this.e, bVar, Integer.valueOf(i), obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.e.C0149e.a
                public Object j(b<?> bVar, int i) {
                    return GeneratedMessageV3.invokeOrDie(this.d, bVar, Integer.valueOf(i));
                }

                @Override // com.google.protobuf.GeneratedMessageV3.e.C0149e.a
                public Object k(GeneratedMessageV3 generatedMessageV3, int i) {
                    return GeneratedMessageV3.invokeOrDie(this.c, generatedMessageV3, Integer.valueOf(i));
                }
            }

            public C0149e(Descriptors.FieldDescriptor fieldDescriptor, String str, Class<? extends GeneratedMessageV3> cls, Class<? extends b> cls2) {
                b bVar = new b(fieldDescriptor, str, cls, cls2);
                this.a = bVar.c.getReturnType();
                this.b = q(bVar);
            }

            public static a q(b bVar) {
                return bVar;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public void a(b bVar) {
                this.b.a(bVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public Object b(GeneratedMessageV3 generatedMessageV3) {
                return this.b.b(generatedMessageV3);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public Object c(b bVar) {
                return this.b.c(bVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public void d(b bVar, Object obj) {
                this.b.d(bVar, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public int e(b bVar) {
                return this.b.e(bVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public int f(GeneratedMessageV3 generatedMessageV3) {
                return this.b.f(generatedMessageV3);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public boolean g(GeneratedMessageV3 generatedMessageV3) {
                throw new UnsupportedOperationException("hasField() called on a repeated field.");
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public void h(b bVar, Object obj) {
                a(bVar);
                for (Object obj2 : (List) obj) {
                    d(bVar, obj2);
                }
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public void i(b bVar, int i, Object obj) {
                this.b.i(bVar, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public Object j(b bVar, int i) {
                return this.b.j(bVar, i);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public Object k(GeneratedMessageV3 generatedMessageV3, int i) {
                return this.b.k(generatedMessageV3, i);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public boolean l(b bVar) {
                throw new UnsupportedOperationException("hasField() called on a repeated field.");
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public l0.a m() {
                throw new UnsupportedOperationException("newBuilderForField() called on a non-Message type.");
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public l0.a n(b bVar, int i) {
                throw new UnsupportedOperationException("getRepeatedFieldBuilder() called on a non-Message type.");
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public Object o(GeneratedMessageV3 generatedMessageV3) {
                return b(generatedMessageV3);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public l0.a p(b bVar) {
                throw new UnsupportedOperationException("getFieldBuilder() called on a non-Message type.");
            }
        }

        /* loaded from: classes2.dex */
        public static final class f extends C0149e {
            public final Method c;
            public final Method d;

            public f(Descriptors.FieldDescriptor fieldDescriptor, String str, Class<? extends GeneratedMessageV3> cls, Class<? extends b> cls2) {
                super(fieldDescriptor, str, cls, cls2);
                this.c = GeneratedMessageV3.getMethodOrDie(this.a, "newBuilder", new Class[0]);
                this.d = GeneratedMessageV3.getMethodOrDie(cls2, "get" + str + "Builder", Integer.TYPE);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.C0149e, com.google.protobuf.GeneratedMessageV3.e.a
            public void d(b bVar, Object obj) {
                super.d(bVar, r(obj));
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.C0149e, com.google.protobuf.GeneratedMessageV3.e.a
            public void i(b bVar, int i, Object obj) {
                super.i(bVar, i, r(obj));
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.C0149e, com.google.protobuf.GeneratedMessageV3.e.a
            public l0.a m() {
                return (l0.a) GeneratedMessageV3.invokeOrDie(this.c, null, new Object[0]);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.C0149e, com.google.protobuf.GeneratedMessageV3.e.a
            public l0.a n(b bVar, int i) {
                return (l0.a) GeneratedMessageV3.invokeOrDie(this.d, bVar, Integer.valueOf(i));
            }

            public final Object r(Object obj) {
                return this.a.isInstance(obj) ? obj : ((l0.a) GeneratedMessageV3.invokeOrDie(this.c, null, new Object[0])).mergeFrom((l0) obj).build();
            }
        }

        /* loaded from: classes2.dex */
        public static final class g extends h {
            public Descriptors.c f;
            public Method g;
            public Method h;
            public boolean i;
            public Method j;
            public Method k;
            public Method l;

            public g(Descriptors.FieldDescriptor fieldDescriptor, String str, Class<? extends GeneratedMessageV3> cls, Class<? extends b> cls2, String str2) {
                super(fieldDescriptor, str, cls, cls2, str2);
                this.f = fieldDescriptor.s();
                this.g = GeneratedMessageV3.getMethodOrDie(this.a, "valueOf", Descriptors.d.class);
                this.h = GeneratedMessageV3.getMethodOrDie(this.a, "getValueDescriptor", new Class[0]);
                boolean x = fieldDescriptor.a().x();
                this.i = x;
                if (x) {
                    this.j = GeneratedMessageV3.getMethodOrDie(cls, "get" + str + "Value", new Class[0]);
                    this.k = GeneratedMessageV3.getMethodOrDie(cls2, "get" + str + "Value", new Class[0]);
                    this.l = GeneratedMessageV3.getMethodOrDie(cls2, "set" + str + "Value", Integer.TYPE);
                }
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.h, com.google.protobuf.GeneratedMessageV3.e.a
            public Object b(GeneratedMessageV3 generatedMessageV3) {
                if (this.i) {
                    return this.f.j(((Integer) GeneratedMessageV3.invokeOrDie(this.j, generatedMessageV3, new Object[0])).intValue());
                }
                return GeneratedMessageV3.invokeOrDie(this.h, super.b(generatedMessageV3), new Object[0]);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.h, com.google.protobuf.GeneratedMessageV3.e.a
            public Object c(b bVar) {
                if (this.i) {
                    return this.f.j(((Integer) GeneratedMessageV3.invokeOrDie(this.k, bVar, new Object[0])).intValue());
                }
                return GeneratedMessageV3.invokeOrDie(this.h, super.c(bVar), new Object[0]);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.h, com.google.protobuf.GeneratedMessageV3.e.a
            public void h(b bVar, Object obj) {
                if (this.i) {
                    GeneratedMessageV3.invokeOrDie(this.l, bVar, Integer.valueOf(((Descriptors.d) obj).getNumber()));
                } else {
                    super.h(bVar, GeneratedMessageV3.invokeOrDie(this.g, null, obj));
                }
            }
        }

        /* loaded from: classes2.dex */
        public static class h implements a {
            public final Class<?> a;
            public final Descriptors.FieldDescriptor b;
            public final boolean c;
            public final boolean d;
            public final a e;

            /* loaded from: classes2.dex */
            public interface a {
                void a(b<?> bVar);

                Object b(GeneratedMessageV3 generatedMessageV3);

                Object c(b<?> bVar);

                int d(GeneratedMessageV3 generatedMessageV3);

                int e(b<?> bVar);

                boolean g(GeneratedMessageV3 generatedMessageV3);

                void h(b<?> bVar, Object obj);

                boolean l(b<?> bVar);
            }

            /* loaded from: classes2.dex */
            public static final class b implements a {
                public final Method a;
                public final Method b;
                public final Method c;
                public final Method d;
                public final Method e;
                public final Method f;
                public final Method g;
                public final Method h;

                public b(Descriptors.FieldDescriptor fieldDescriptor, String str, Class<? extends GeneratedMessageV3> cls, Class<? extends b> cls2, String str2, boolean z, boolean z2) {
                    Method method;
                    Method method2;
                    Method method3;
                    Method methodOrDie = GeneratedMessageV3.getMethodOrDie(cls, "get" + str, new Class[0]);
                    this.a = methodOrDie;
                    this.b = GeneratedMessageV3.getMethodOrDie(cls2, "get" + str, new Class[0]);
                    this.c = GeneratedMessageV3.getMethodOrDie(cls2, "set" + str, methodOrDie.getReturnType());
                    Method method4 = null;
                    if (z2) {
                        method = GeneratedMessageV3.getMethodOrDie(cls, "has" + str, new Class[0]);
                    } else {
                        method = null;
                    }
                    this.d = method;
                    if (z2) {
                        method2 = GeneratedMessageV3.getMethodOrDie(cls2, "has" + str, new Class[0]);
                    } else {
                        method2 = null;
                    }
                    this.e = method2;
                    this.f = GeneratedMessageV3.getMethodOrDie(cls2, "clear" + str, new Class[0]);
                    if (z) {
                        method3 = GeneratedMessageV3.getMethodOrDie(cls, "get" + str2 + "Case", new Class[0]);
                    } else {
                        method3 = null;
                    }
                    this.g = method3;
                    if (z) {
                        method4 = GeneratedMessageV3.getMethodOrDie(cls2, "get" + str2 + "Case", new Class[0]);
                    }
                    this.h = method4;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.e.h.a
                public void a(b<?> bVar) {
                    GeneratedMessageV3.invokeOrDie(this.f, bVar, new Object[0]);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.e.h.a
                public Object b(GeneratedMessageV3 generatedMessageV3) {
                    return GeneratedMessageV3.invokeOrDie(this.a, generatedMessageV3, new Object[0]);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.e.h.a
                public Object c(b<?> bVar) {
                    return GeneratedMessageV3.invokeOrDie(this.b, bVar, new Object[0]);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.e.h.a
                public int d(GeneratedMessageV3 generatedMessageV3) {
                    return ((a0.c) GeneratedMessageV3.invokeOrDie(this.g, generatedMessageV3, new Object[0])).getNumber();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.e.h.a
                public int e(b<?> bVar) {
                    return ((a0.c) GeneratedMessageV3.invokeOrDie(this.h, bVar, new Object[0])).getNumber();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.e.h.a
                public boolean g(GeneratedMessageV3 generatedMessageV3) {
                    return ((Boolean) GeneratedMessageV3.invokeOrDie(this.d, generatedMessageV3, new Object[0])).booleanValue();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.e.h.a
                public void h(b<?> bVar, Object obj) {
                    GeneratedMessageV3.invokeOrDie(this.c, bVar, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.e.h.a
                public boolean l(b<?> bVar) {
                    return ((Boolean) GeneratedMessageV3.invokeOrDie(this.e, bVar, new Object[0])).booleanValue();
                }
            }

            public h(Descriptors.FieldDescriptor fieldDescriptor, String str, Class<? extends GeneratedMessageV3> cls, Class<? extends b> cls2, String str2) {
                boolean z = (fieldDescriptor.o() == null || fieldDescriptor.o().s()) ? false : true;
                this.c = z;
                boolean z2 = fieldDescriptor.a().t() == Descriptors.FileDescriptor.Syntax.PROTO2 || fieldDescriptor.B() || (!z && fieldDescriptor.v() == Descriptors.FieldDescriptor.JavaType.MESSAGE);
                this.d = z2;
                b bVar = new b(fieldDescriptor, str, cls, cls2, str2, z, z2);
                this.b = fieldDescriptor;
                this.a = bVar.a.getReturnType();
                this.e = q(bVar);
            }

            public static a q(b bVar) {
                return bVar;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public void a(b bVar) {
                this.e.a(bVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public Object b(GeneratedMessageV3 generatedMessageV3) {
                return this.e.b(generatedMessageV3);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public Object c(b bVar) {
                return this.e.c(bVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public void d(b bVar, Object obj) {
                throw new UnsupportedOperationException("addRepeatedField() called on a singular field.");
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public int e(b bVar) {
                throw new UnsupportedOperationException("getRepeatedFieldSize() called on a singular field.");
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public int f(GeneratedMessageV3 generatedMessageV3) {
                throw new UnsupportedOperationException("getRepeatedFieldSize() called on a singular field.");
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public boolean g(GeneratedMessageV3 generatedMessageV3) {
                if (!this.d) {
                    if (this.c) {
                        return this.e.d(generatedMessageV3) == this.b.getNumber();
                    }
                    return !b(generatedMessageV3).equals(this.b.r());
                }
                return this.e.g(generatedMessageV3);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public void h(b bVar, Object obj) {
                this.e.h(bVar, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public void i(b bVar, int i, Object obj) {
                throw new UnsupportedOperationException("setRepeatedField() called on a singular field.");
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public Object j(b bVar, int i) {
                throw new UnsupportedOperationException("getRepeatedField() called on a singular field.");
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public Object k(GeneratedMessageV3 generatedMessageV3, int i) {
                throw new UnsupportedOperationException("getRepeatedField() called on a singular field.");
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public boolean l(b bVar) {
                if (!this.d) {
                    if (this.c) {
                        return this.e.e(bVar) == this.b.getNumber();
                    }
                    return !c(bVar).equals(this.b.r());
                }
                return this.e.l(bVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public l0.a m() {
                throw new UnsupportedOperationException("newBuilderForField() called on a non-Message type.");
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public l0.a n(b bVar, int i) {
                throw new UnsupportedOperationException("getRepeatedFieldBuilder() called on a non-Message type.");
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public Object o(GeneratedMessageV3 generatedMessageV3) {
                return b(generatedMessageV3);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.a
            public l0.a p(b bVar) {
                throw new UnsupportedOperationException("getFieldBuilder() called on a non-Message type.");
            }
        }

        /* loaded from: classes2.dex */
        public static final class i extends h {
            public final Method f;
            public final Method g;

            public i(Descriptors.FieldDescriptor fieldDescriptor, String str, Class<? extends GeneratedMessageV3> cls, Class<? extends b> cls2, String str2) {
                super(fieldDescriptor, str, cls, cls2, str2);
                this.f = GeneratedMessageV3.getMethodOrDie(this.a, "newBuilder", new Class[0]);
                this.g = GeneratedMessageV3.getMethodOrDie(cls2, "get" + str + "Builder", new Class[0]);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.h, com.google.protobuf.GeneratedMessageV3.e.a
            public void h(b bVar, Object obj) {
                super.h(bVar, r(obj));
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.h, com.google.protobuf.GeneratedMessageV3.e.a
            public l0.a m() {
                return (l0.a) GeneratedMessageV3.invokeOrDie(this.f, null, new Object[0]);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.h, com.google.protobuf.GeneratedMessageV3.e.a
            public l0.a p(b bVar) {
                return (l0.a) GeneratedMessageV3.invokeOrDie(this.g, bVar, new Object[0]);
            }

            public final Object r(Object obj) {
                return this.a.isInstance(obj) ? obj : ((l0.a) GeneratedMessageV3.invokeOrDie(this.f, null, new Object[0])).mergeFrom((l0) obj).buildPartial();
            }
        }

        /* loaded from: classes2.dex */
        public static final class j extends h {
            public final Method f;
            public final Method g;

            public j(Descriptors.FieldDescriptor fieldDescriptor, String str, Class<? extends GeneratedMessageV3> cls, Class<? extends b> cls2, String str2) {
                super(fieldDescriptor, str, cls, cls2, str2);
                this.f = GeneratedMessageV3.getMethodOrDie(cls, "get" + str + "Bytes", new Class[0]);
                GeneratedMessageV3.getMethodOrDie(cls2, "get" + str + "Bytes", new Class[0]);
                this.g = GeneratedMessageV3.getMethodOrDie(cls2, "set" + str + "Bytes", ByteString.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.h, com.google.protobuf.GeneratedMessageV3.e.a
            public void h(b bVar, Object obj) {
                if (obj instanceof ByteString) {
                    GeneratedMessageV3.invokeOrDie(this.g, bVar, obj);
                } else {
                    super.h(bVar, obj);
                }
            }

            @Override // com.google.protobuf.GeneratedMessageV3.e.h, com.google.protobuf.GeneratedMessageV3.e.a
            public Object o(GeneratedMessageV3 generatedMessageV3) {
                return GeneratedMessageV3.invokeOrDie(this.f, generatedMessageV3, new Object[0]);
            }
        }

        public e(Descriptors.b bVar, String[] strArr) {
            this.a = bVar;
            this.c = strArr;
            this.b = new a[bVar.p().size()];
            this.d = new c[bVar.s().size()];
        }

        public e d(Class<? extends GeneratedMessageV3> cls, Class<? extends b> cls2) {
            if (this.e) {
                return this;
            }
            synchronized (this) {
                if (this.e) {
                    return this;
                }
                int length = this.b.length;
                int i2 = 0;
                while (true) {
                    if (i2 >= length) {
                        break;
                    }
                    Descriptors.FieldDescriptor fieldDescriptor = this.a.p().get(i2);
                    String str = fieldDescriptor.o() != null ? this.c[fieldDescriptor.o().r() + length] : null;
                    if (fieldDescriptor.i()) {
                        if (fieldDescriptor.v() == Descriptors.FieldDescriptor.JavaType.MESSAGE) {
                            if (fieldDescriptor.D()) {
                                this.b[i2] = new b(fieldDescriptor, this.c[i2], cls, cls2);
                            } else {
                                this.b[i2] = new f(fieldDescriptor, this.c[i2], cls, cls2);
                            }
                        } else if (fieldDescriptor.v() == Descriptors.FieldDescriptor.JavaType.ENUM) {
                            this.b[i2] = new d(fieldDescriptor, this.c[i2], cls, cls2);
                        } else {
                            this.b[i2] = new C0149e(fieldDescriptor, this.c[i2], cls, cls2);
                        }
                    } else if (fieldDescriptor.v() == Descriptors.FieldDescriptor.JavaType.MESSAGE) {
                        this.b[i2] = new i(fieldDescriptor, this.c[i2], cls, cls2, str);
                    } else if (fieldDescriptor.v() == Descriptors.FieldDescriptor.JavaType.ENUM) {
                        this.b[i2] = new g(fieldDescriptor, this.c[i2], cls, cls2, str);
                    } else if (fieldDescriptor.v() == Descriptors.FieldDescriptor.JavaType.STRING) {
                        this.b[i2] = new j(fieldDescriptor, this.c[i2], cls, cls2, str);
                    } else {
                        this.b[i2] = new h(fieldDescriptor, this.c[i2], cls, cls2, str);
                    }
                    i2++;
                }
                int length2 = this.d.length;
                for (int i3 = 0; i3 < length2; i3++) {
                    this.d[i3] = new c(this.a, i3, this.c[i3 + length], cls, cls2);
                }
                this.e = true;
                this.c = null;
                return this;
            }
        }

        public final a e(Descriptors.FieldDescriptor fieldDescriptor) {
            if (fieldDescriptor.p() == this.a) {
                if (!fieldDescriptor.C()) {
                    return this.b[fieldDescriptor.u()];
                }
                throw new IllegalArgumentException("This type does not have extensions.");
            }
            throw new IllegalArgumentException("FieldDescriptor does not match message type.");
        }

        public final c f(Descriptors.g gVar) {
            if (gVar.l() == this.a) {
                return this.d[gVar.r()];
            }
            throw new IllegalArgumentException("OneofDescriptor does not match message type.");
        }
    }

    /* loaded from: classes2.dex */
    public static final class f {
        public static final f a = new f();
    }

    public GeneratedMessageV3() {
        this.unknownFields = g1.c();
    }

    public static boolean canUseUnsafe() {
        return k1.H() && k1.I();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public static <MessageType extends ExtendableMessage<MessageType>, T> Extension<MessageType, T> checkNotLite(p<MessageType, T> pVar) {
        if (!pVar.a()) {
            return (Extension) pVar;
        }
        throw new IllegalArgumentException("Expected non-lite extension.");
    }

    public static int computeStringSize(int i, Object obj) {
        if (obj instanceof String) {
            return CodedOutputStream.V(i, (String) obj);
        }
        return CodedOutputStream.h(i, (ByteString) obj);
    }

    public static int computeStringSizeNoTag(Object obj) {
        if (obj instanceof String) {
            return CodedOutputStream.W((String) obj);
        }
        return CodedOutputStream.i((ByteString) obj);
    }

    public static a0.a emptyBooleanList() {
        return g.o();
    }

    public static a0.b emptyDoubleList() {
        return n.o();
    }

    public static a0.f emptyFloatList() {
        return x.o();
    }

    public static a0.g emptyIntList() {
        return z.n();
    }

    public static a0.h emptyLongList() {
        return f0.o();
    }

    public static void enableAlwaysUseFieldBuildersForTesting() {
        setAlwaysUseFieldBuildersForTesting(true);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public Map<Descriptors.FieldDescriptor, Object> getAllFieldsMutable(boolean z) {
        TreeMap treeMap = new TreeMap();
        List<Descriptors.FieldDescriptor> p = internalGetFieldAccessorTable().a.p();
        int i = 0;
        while (i < p.size()) {
            Descriptors.FieldDescriptor fieldDescriptor = p.get(i);
            Descriptors.g o = fieldDescriptor.o();
            if (o != null) {
                i += o.o() - 1;
                if (hasOneof(o)) {
                    fieldDescriptor = getOneofFieldDescriptor(o);
                    if (!z && fieldDescriptor.v() == Descriptors.FieldDescriptor.JavaType.STRING) {
                        treeMap.put(fieldDescriptor, getFieldRaw(fieldDescriptor));
                    } else {
                        treeMap.put(fieldDescriptor, getField(fieldDescriptor));
                    }
                    i++;
                } else {
                    i++;
                }
            } else {
                if (fieldDescriptor.i()) {
                    List list = (List) getField(fieldDescriptor);
                    if (!list.isEmpty()) {
                        treeMap.put(fieldDescriptor, list);
                    }
                } else {
                    if (!hasField(fieldDescriptor)) {
                    }
                    if (!z) {
                    }
                    treeMap.put(fieldDescriptor, getField(fieldDescriptor));
                }
                i++;
            }
        }
        return treeMap;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public static Method getMethodOrDie(Class cls, String str, Class... clsArr) {
        try {
            return cls.getMethod(str, clsArr);
        } catch (NoSuchMethodException e2) {
            throw new RuntimeException("Generated message class \"" + cls.getName() + "\" missing method \"" + str + "\".", e2);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public static Object invokeOrDie(Method method, Object obj, Object... objArr) {
        try {
            return method.invoke(obj, objArr);
        } catch (IllegalAccessException e2) {
            throw new RuntimeException("Couldn't use Java reflection to implement protocol message reflection.", e2);
        } catch (InvocationTargetException e3) {
            Throwable cause = e3.getCause();
            if (!(cause instanceof RuntimeException)) {
                if (cause instanceof Error) {
                    throw ((Error) cause);
                }
                throw new RuntimeException("Unexpected exception thrown by generated accessor method.", cause);
            }
            throw ((RuntimeException) cause);
        }
    }

    private static <V> void maybeSerializeBooleanEntryTo(CodedOutputStream codedOutputStream, Map<Boolean, V> map, g0<Boolean, V> g0Var, int i, boolean z) throws IOException {
        if (map.containsKey(Boolean.valueOf(z))) {
            codedOutputStream.K0(i, g0Var.newBuilderForType().r(Boolean.valueOf(z)).t(map.get(Boolean.valueOf(z))).build());
        }
    }

    /* JADX WARN: Type inference failed for: r1v1, types: [com.google.protobuf.a0$g] */
    public static a0.g mutableCopy(a0.g gVar) {
        int size = gVar.size();
        return gVar.a(size == 0 ? 10 : size * 2);
    }

    public static a0.a newBooleanList() {
        return new g();
    }

    public static a0.b newDoubleList() {
        return new n();
    }

    public static a0.f newFloatList() {
        return new x();
    }

    public static a0.g newIntList() {
        return new z();
    }

    public static a0.h newLongList() {
        return new f0();
    }

    public static <M extends l0> M parseDelimitedWithIOException(t0<M> t0Var, InputStream inputStream) throws IOException {
        try {
            return t0Var.parseDelimitedFrom(inputStream);
        } catch (InvalidProtocolBufferException e2) {
            throw e2.unwrapIOException();
        }
    }

    public static <M extends l0> M parseWithIOException(t0<M> t0Var, InputStream inputStream) throws IOException {
        try {
            return t0Var.parseFrom(inputStream);
        } catch (InvalidProtocolBufferException e2) {
            throw e2.unwrapIOException();
        }
    }

    public static <V> void serializeBooleanMapTo(CodedOutputStream codedOutputStream, MapField<Boolean, V> mapField, g0<Boolean, V> g0Var, int i) throws IOException {
        Map<Boolean, V> j = mapField.j();
        if (!codedOutputStream.g0()) {
            serializeMapTo(codedOutputStream, j, g0Var, i);
            return;
        }
        maybeSerializeBooleanEntryTo(codedOutputStream, j, g0Var, i, false);
        maybeSerializeBooleanEntryTo(codedOutputStream, j, g0Var, i, true);
    }

    public static <V> void serializeIntegerMapTo(CodedOutputStream codedOutputStream, MapField<Integer, V> mapField, g0<Integer, V> g0Var, int i) throws IOException {
        Map<Integer, V> j = mapField.j();
        if (!codedOutputStream.g0()) {
            serializeMapTo(codedOutputStream, j, g0Var, i);
            return;
        }
        int size = j.size();
        int[] iArr = new int[size];
        int i2 = 0;
        for (Integer num : j.keySet()) {
            iArr[i2] = num.intValue();
            i2++;
        }
        Arrays.sort(iArr);
        for (int i3 = 0; i3 < size; i3++) {
            int i4 = iArr[i3];
            codedOutputStream.K0(i, g0Var.newBuilderForType().r(Integer.valueOf(i4)).t(j.get(Integer.valueOf(i4))).build());
        }
    }

    public static <V> void serializeLongMapTo(CodedOutputStream codedOutputStream, MapField<Long, V> mapField, g0<Long, V> g0Var, int i) throws IOException {
        Map<Long, V> j = mapField.j();
        if (!codedOutputStream.g0()) {
            serializeMapTo(codedOutputStream, j, g0Var, i);
            return;
        }
        int size = j.size();
        long[] jArr = new long[size];
        int i2 = 0;
        for (Long l : j.keySet()) {
            jArr[i2] = l.longValue();
            i2++;
        }
        Arrays.sort(jArr);
        for (int i3 = 0; i3 < size; i3++) {
            long j2 = jArr[i3];
            codedOutputStream.K0(i, g0Var.newBuilderForType().r(Long.valueOf(j2)).t(j.get(Long.valueOf(j2))).build());
        }
    }

    private static <K, V> void serializeMapTo(CodedOutputStream codedOutputStream, Map<K, V> map, g0<K, V> g0Var, int i) throws IOException {
        for (Map.Entry<K, V> entry : map.entrySet()) {
            codedOutputStream.K0(i, g0Var.newBuilderForType().r(entry.getKey()).t(entry.getValue()).build());
        }
    }

    public static <V> void serializeStringMapTo(CodedOutputStream codedOutputStream, MapField<String, V> mapField, g0<String, V> g0Var, int i) throws IOException {
        Map<String, V> j = mapField.j();
        if (!codedOutputStream.g0()) {
            serializeMapTo(codedOutputStream, j, g0Var, i);
            return;
        }
        String[] strArr = (String[]) j.keySet().toArray(new String[j.size()]);
        Arrays.sort(strArr);
        for (String str : strArr) {
            codedOutputStream.K0(i, g0Var.newBuilderForType().r(str).t(j.get(str)).build());
        }
    }

    public static void setAlwaysUseFieldBuildersForTesting(boolean z) {
        alwaysUseFieldBuilders = z;
    }

    public static void writeString(CodedOutputStream codedOutputStream, int i, Object obj) throws IOException {
        if (obj instanceof String) {
            codedOutputStream.Y0(i, (String) obj);
        } else {
            codedOutputStream.q0(i, (ByteString) obj);
        }
    }

    public static void writeStringNoTag(CodedOutputStream codedOutputStream, Object obj) throws IOException {
        if (obj instanceof String) {
            codedOutputStream.Z0((String) obj);
        } else {
            codedOutputStream.r0((ByteString) obj);
        }
    }

    @Override // com.google.protobuf.o0
    public Map<Descriptors.FieldDescriptor, Object> getAllFields() {
        return Collections.unmodifiableMap(getAllFieldsMutable(false));
    }

    public Map<Descriptors.FieldDescriptor, Object> getAllFieldsRaw() {
        return Collections.unmodifiableMap(getAllFieldsMutable(true));
    }

    @Override // defpackage.d82, com.google.protobuf.o0
    public abstract /* synthetic */ l0 getDefaultInstanceForType();

    @Override // defpackage.d82, com.google.protobuf.o0
    public abstract /* synthetic */ m0 getDefaultInstanceForType();

    @Override // com.google.protobuf.o0
    public Descriptors.b getDescriptorForType() {
        return internalGetFieldAccessorTable().a;
    }

    @Override // com.google.protobuf.o0
    public Object getField(Descriptors.FieldDescriptor fieldDescriptor) {
        return internalGetFieldAccessorTable().e(fieldDescriptor).b(this);
    }

    public Object getFieldRaw(Descriptors.FieldDescriptor fieldDescriptor) {
        return internalGetFieldAccessorTable().e(fieldDescriptor).o(this);
    }

    @Override // com.google.protobuf.a
    public Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar) {
        return internalGetFieldAccessorTable().f(gVar).c(this);
    }

    @Override // com.google.protobuf.m0
    public t0<? extends GeneratedMessageV3> getParserForType() {
        throw new UnsupportedOperationException("This is supposed to be overridden by subclasses.");
    }

    public Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i) {
        return internalGetFieldAccessorTable().e(fieldDescriptor).k(this, i);
    }

    public int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor) {
        return internalGetFieldAccessorTable().e(fieldDescriptor).f(this);
    }

    @Override // com.google.protobuf.a, com.google.protobuf.m0
    public int getSerializedSize() {
        int i = this.memoizedSize;
        if (i != -1) {
            return i;
        }
        int e2 = MessageReflection.e(this, getAllFieldsRaw());
        this.memoizedSize = e2;
        return e2;
    }

    public g1 getUnknownFields() {
        throw new UnsupportedOperationException("This is supposed to be overridden by subclasses.");
    }

    @Override // com.google.protobuf.o0
    public boolean hasField(Descriptors.FieldDescriptor fieldDescriptor) {
        return internalGetFieldAccessorTable().e(fieldDescriptor).g(this);
    }

    @Override // com.google.protobuf.a
    public boolean hasOneof(Descriptors.g gVar) {
        return internalGetFieldAccessorTable().f(gVar).e(this);
    }

    public abstract e internalGetFieldAccessorTable();

    public MapField internalGetMapField(int i) {
        throw new RuntimeException("No map fields found in " + getClass().getName());
    }

    @Override // com.google.protobuf.a, defpackage.d82
    public boolean isInitialized() {
        for (Descriptors.FieldDescriptor fieldDescriptor : getDescriptorForType().p()) {
            if (fieldDescriptor.H() && !hasField(fieldDescriptor)) {
                return false;
            }
            if (fieldDescriptor.v() == Descriptors.FieldDescriptor.JavaType.MESSAGE) {
                if (fieldDescriptor.i()) {
                    for (l0 l0Var : (List) getField(fieldDescriptor)) {
                        if (!l0Var.isInitialized()) {
                            return false;
                        }
                    }
                    continue;
                } else if (hasField(fieldDescriptor) && !((l0) getField(fieldDescriptor)).isInitialized()) {
                    return false;
                }
            }
        }
        return true;
    }

    public void makeExtensionsImmutable() {
    }

    public void mergeFromAndMakeImmutableInternal(j jVar, r rVar) throws InvalidProtocolBufferException {
        y0 e2 = u0.a().e(this);
        try {
            e2.i(this, k.R(jVar), rVar);
            e2.e(this);
        } catch (InvalidProtocolBufferException e3) {
            throw e3.setUnfinishedMessage(this);
        } catch (IOException e4) {
            throw new InvalidProtocolBufferException(e4).setUnfinishedMessage(this);
        }
    }

    @Override // com.google.protobuf.m0, com.google.protobuf.l0
    public abstract /* synthetic */ l0.a newBuilderForType();

    public abstract l0.a newBuilderForType(c cVar);

    @Override // com.google.protobuf.a
    public l0.a newBuilderForType(a.b bVar) {
        return newBuilderForType((c) new a(this, bVar));
    }

    @Override // com.google.protobuf.m0, com.google.protobuf.l0
    public abstract /* synthetic */ m0.a newBuilderForType();

    public Object newInstance(f fVar) {
        throw new UnsupportedOperationException("This method must be overridden by the subclass.");
    }

    public boolean parseUnknownField(j jVar, g1.b bVar, r rVar, int i) throws IOException {
        if (jVar.M()) {
            return jVar.N(i);
        }
        return bVar.o(i, jVar);
    }

    public boolean parseUnknownFieldProto3(j jVar, g1.b bVar, r rVar, int i) throws IOException {
        return parseUnknownField(jVar, bVar, rVar, i);
    }

    @Override // com.google.protobuf.m0, com.google.protobuf.l0
    public abstract /* synthetic */ l0.a toBuilder();

    @Override // com.google.protobuf.m0, com.google.protobuf.l0
    public abstract /* synthetic */ m0.a toBuilder();

    public Object writeReplace() throws ObjectStreamException {
        return new GeneratedMessageLite.SerializedForm(this);
    }

    @Override // com.google.protobuf.a, com.google.protobuf.m0
    public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
        MessageReflection.k(this, getAllFieldsRaw(), codedOutputStream, false);
    }

    /* loaded from: classes2.dex */
    public static abstract class ExtendableMessage<MessageType extends ExtendableMessage> extends GeneratedMessageV3 implements o0 {
        private static final long serialVersionUID = 1;
        private final w<Descriptors.FieldDescriptor> extensions;

        /* loaded from: classes2.dex */
        public class a {
            public final Iterator<Map.Entry<Descriptors.FieldDescriptor, Object>> a;
            public Map.Entry<Descriptors.FieldDescriptor, Object> b;
            public final boolean c;

            public /* synthetic */ a(ExtendableMessage extendableMessage, boolean z, a aVar) {
                this(z);
            }

            public void a(int i, CodedOutputStream codedOutputStream) throws IOException {
                while (true) {
                    Map.Entry<Descriptors.FieldDescriptor, Object> entry = this.b;
                    if (entry == null || entry.getKey().getNumber() >= i) {
                        return;
                    }
                    Descriptors.FieldDescriptor key = this.b.getKey();
                    if (this.c && key.z() == WireFormat.JavaType.MESSAGE && !key.i()) {
                        if (this.b instanceof b0.b) {
                            codedOutputStream.O0(key.getNumber(), ((b0.b) this.b).a().f());
                        } else {
                            codedOutputStream.N0(key.getNumber(), (l0) this.b.getValue());
                        }
                    } else {
                        w.R(key, this.b.getValue(), codedOutputStream);
                    }
                    if (this.a.hasNext()) {
                        this.b = this.a.next();
                    } else {
                        this.b = null;
                    }
                }
            }

            public a(boolean z) {
                Iterator<Map.Entry<Descriptors.FieldDescriptor, Object>> G = ExtendableMessage.this.extensions.G();
                this.a = G;
                if (G.hasNext()) {
                    this.b = G.next();
                }
                this.c = z;
            }
        }

        public ExtendableMessage() {
            this.extensions = w.L();
        }

        private void a(Descriptors.FieldDescriptor fieldDescriptor) {
            if (fieldDescriptor.p() != getDescriptorForType()) {
                throw new IllegalArgumentException("FieldDescriptor does not match message type.");
            }
        }

        private void b(Extension<MessageType, ?> extension) {
            if (extension.c().p() == getDescriptorForType()) {
                return;
            }
            throw new IllegalArgumentException("Extension is for type \"" + extension.c().p().d() + "\" which does not match message type \"" + getDescriptorForType().d() + "\".");
        }

        public boolean extensionsAreInitialized() {
            return this.extensions.D();
        }

        public int extensionsSerializedSize() {
            return this.extensions.y();
        }

        public int extensionsSerializedSizeAsMessageSet() {
            return this.extensions.u();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public Map<Descriptors.FieldDescriptor, Object> getAllFields() {
            Map allFieldsMutable = getAllFieldsMutable(false);
            allFieldsMutable.putAll(getExtensionFields());
            return Collections.unmodifiableMap(allFieldsMutable);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Map<Descriptors.FieldDescriptor, Object> getAllFieldsRaw() {
            Map allFieldsMutable = getAllFieldsMutable(false);
            allFieldsMutable.putAll(getExtensionFields());
            return Collections.unmodifiableMap(allFieldsMutable);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public abstract /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public abstract /* synthetic */ m0 getDefaultInstanceForType();

        public final <Type> Type getExtension(p<MessageType, Type> pVar) {
            Extension<MessageType, ?> checkNotLite = GeneratedMessageV3.checkNotLite(pVar);
            b(checkNotLite);
            Descriptors.FieldDescriptor c = checkNotLite.c();
            Object t = this.extensions.t(c);
            if (t == null) {
                if (c.i()) {
                    return (Type) Collections.emptyList();
                }
                if (c.v() == Descriptors.FieldDescriptor.JavaType.MESSAGE) {
                    return (Type) checkNotLite.d();
                }
                return (Type) checkNotLite.b(c.r());
            }
            return (Type) checkNotLite.b(t);
        }

        public final <Type> int getExtensionCount(p<MessageType, List<Type>> pVar) {
            Extension<MessageType, ?> checkNotLite = GeneratedMessageV3.checkNotLite(pVar);
            b(checkNotLite);
            return this.extensions.x(checkNotLite.c());
        }

        public Map<Descriptors.FieldDescriptor, Object> getExtensionFields() {
            return this.extensions.s();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public Object getField(Descriptors.FieldDescriptor fieldDescriptor) {
            if (fieldDescriptor.C()) {
                a(fieldDescriptor);
                Object t = this.extensions.t(fieldDescriptor);
                if (t == null) {
                    if (fieldDescriptor.i()) {
                        return Collections.emptyList();
                    }
                    if (fieldDescriptor.v() == Descriptors.FieldDescriptor.JavaType.MESSAGE) {
                        return o.e(fieldDescriptor.x());
                    }
                    return fieldDescriptor.r();
                }
                return t;
            }
            return super.getField(fieldDescriptor);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i) {
            if (fieldDescriptor.C()) {
                a(fieldDescriptor);
                return this.extensions.w(fieldDescriptor, i);
            }
            return super.getRepeatedField(fieldDescriptor, i);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor) {
            if (fieldDescriptor.C()) {
                a(fieldDescriptor);
                return this.extensions.x(fieldDescriptor);
            }
            return super.getRepeatedFieldCount(fieldDescriptor);
        }

        public final <Type> boolean hasExtension(p<MessageType, Type> pVar) {
            Extension<MessageType, ?> checkNotLite = GeneratedMessageV3.checkNotLite(pVar);
            b(checkNotLite);
            return this.extensions.A(checkNotLite.c());
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public boolean hasField(Descriptors.FieldDescriptor fieldDescriptor) {
            if (fieldDescriptor.C()) {
                a(fieldDescriptor);
                return this.extensions.A(fieldDescriptor);
            }
            return super.hasField(fieldDescriptor);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public boolean isInitialized() {
            return super.isInitialized() && extensionsAreInitialized();
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public void makeExtensionsImmutable() {
            this.extensions.H();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public abstract /* synthetic */ l0.a newBuilderForType();

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public abstract /* synthetic */ m0.a newBuilderForType();

        public ExtendableMessage<MessageType>.a newExtensionWriter() {
            return new a(this, false, null);
        }

        public ExtendableMessage<MessageType>.a newMessageSetExtensionWriter() {
            return new a(this, true, null);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public boolean parseUnknownField(j jVar, g1.b bVar, r rVar, int i) throws IOException {
            if (jVar.M()) {
                bVar = null;
            }
            return MessageReflection.g(jVar, bVar, rVar, getDescriptorForType(), new MessageReflection.c(this.extensions), i);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public boolean parseUnknownFieldProto3(j jVar, g1.b bVar, r rVar, int i) throws IOException {
            return parseUnknownField(jVar, bVar, rVar, i);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public abstract /* synthetic */ l0.a toBuilder();

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public abstract /* synthetic */ m0.a toBuilder();

        public ExtendableMessage(d<MessageType, ?> dVar) {
            super(dVar);
            this.extensions = dVar.d();
        }

        public final <Type> boolean hasExtension(Extension<MessageType, Type> extension) {
            return hasExtension((p) extension);
        }

        public final <Type> int getExtensionCount(Extension<MessageType, List<Type>> extension) {
            return getExtensionCount((p) extension);
        }

        public final <Type> boolean hasExtension(GeneratedMessage.k<MessageType, Type> kVar) {
            return hasExtension((p) kVar);
        }

        public final <Type> int getExtensionCount(GeneratedMessage.k<MessageType, List<Type>> kVar) {
            return getExtensionCount((p) kVar);
        }

        public final <Type> Type getExtension(p<MessageType, List<Type>> pVar, int i) {
            Extension<MessageType, ?> checkNotLite = GeneratedMessageV3.checkNotLite(pVar);
            b(checkNotLite);
            return (Type) checkNotLite.e(this.extensions.w(checkNotLite.c(), i));
        }

        public final <Type> Type getExtension(Extension<MessageType, Type> extension) {
            return (Type) getExtension((p<MessageType, Object>) extension);
        }

        public final <Type> Type getExtension(GeneratedMessage.k<MessageType, Type> kVar) {
            return (Type) getExtension((p<MessageType, Object>) kVar);
        }

        public final <Type> Type getExtension(Extension<MessageType, List<Type>> extension, int i) {
            return (Type) getExtension((p<MessageType, List<Object>>) extension, i);
        }

        public final <Type> Type getExtension(GeneratedMessage.k<MessageType, List<Type>> kVar, int i) {
            return (Type) getExtension((p<MessageType, List<Object>>) kVar, i);
        }
    }

    public GeneratedMessageV3(b<?> bVar) {
        this.unknownFields = bVar.getUnknownFields();
    }

    /* JADX WARN: Type inference failed for: r1v1, types: [com.google.protobuf.a0$h] */
    public static a0.h mutableCopy(a0.h hVar) {
        int size = hVar.size();
        return hVar.a(size == 0 ? 10 : size * 2);
    }

    public static <M extends l0> M parseDelimitedWithIOException(t0<M> t0Var, InputStream inputStream, r rVar) throws IOException {
        try {
            return t0Var.parseDelimitedFrom(inputStream, rVar);
        } catch (InvalidProtocolBufferException e2) {
            throw e2.unwrapIOException();
        }
    }

    public static <M extends l0> M parseWithIOException(t0<M> t0Var, InputStream inputStream, r rVar) throws IOException {
        try {
            return t0Var.parseFrom(inputStream, rVar);
        } catch (InvalidProtocolBufferException e2) {
            throw e2.unwrapIOException();
        }
    }

    /* JADX WARN: Type inference failed for: r1v1, types: [com.google.protobuf.a0$f] */
    public static a0.f mutableCopy(a0.f fVar) {
        int size = fVar.size();
        return fVar.a(size == 0 ? 10 : size * 2);
    }

    public static <M extends l0> M parseWithIOException(t0<M> t0Var, j jVar) throws IOException {
        try {
            return t0Var.parseFrom(jVar);
        } catch (InvalidProtocolBufferException e2) {
            throw e2.unwrapIOException();
        }
    }

    /* JADX WARN: Type inference failed for: r1v1, types: [com.google.protobuf.a0$b] */
    public static a0.b mutableCopy(a0.b bVar) {
        int size = bVar.size();
        return bVar.a(size == 0 ? 10 : size * 2);
    }

    public static <M extends l0> M parseWithIOException(t0<M> t0Var, j jVar, r rVar) throws IOException {
        try {
            return t0Var.parseFrom(jVar, rVar);
        } catch (InvalidProtocolBufferException e2) {
            throw e2.unwrapIOException();
        }
    }

    /* JADX WARN: Type inference failed for: r1v1, types: [com.google.protobuf.a0$a] */
    public static a0.a mutableCopy(a0.a aVar) {
        int size = aVar.size();
        return aVar.a(size == 0 ? 10 : size * 2);
    }
}
