package com.google.protobuf;

import com.google.protobuf.Descriptors;
import com.google.protobuf.m0;

/* compiled from: Message.java */
/* loaded from: classes2.dex */
public interface l0 extends m0, o0 {

    /* compiled from: Message.java */
    /* loaded from: classes2.dex */
    public interface a extends m0.a, o0 {
        a addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj);

        @Override // 
        l0 build();

        @Override // 
        l0 buildPartial();

        a clearField(Descriptors.FieldDescriptor fieldDescriptor);

        Descriptors.b getDescriptorForType();

        a mergeFrom(ByteString byteString) throws InvalidProtocolBufferException;

        a mergeFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException;

        a mergeFrom(l0 l0Var);

        a newBuilderForField(Descriptors.FieldDescriptor fieldDescriptor);

        a setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj);

        a setUnknownFields(g1 g1Var);
    }

    @Override // 
    a newBuilderForType();

    @Override // 
    a toBuilder();
}
