package com.google.protobuf;

import com.google.protobuf.Descriptors;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* compiled from: ExtensionRegistry.java */
/* loaded from: classes2.dex */
public class q extends r {
    public static final q g = new q(true);
    public final Map<a, b> f;

    /* compiled from: ExtensionRegistry.java */
    /* loaded from: classes2.dex */
    public static final class a {
        public final Descriptors.b a;
        public final int b;

        public a(Descriptors.b bVar, int i) {
            this.a = bVar;
            this.b = i;
        }

        public boolean equals(Object obj) {
            if (obj instanceof a) {
                a aVar = (a) obj;
                return this.a == aVar.a && this.b == aVar.b;
            }
            return false;
        }

        public int hashCode() {
            return (this.a.hashCode() * 65535) + this.b;
        }
    }

    /* compiled from: ExtensionRegistry.java */
    /* loaded from: classes2.dex */
    public static final class b {
        public final Descriptors.FieldDescriptor a;
        public final l0 b;
    }

    public q() {
        new HashMap();
        new HashMap();
        this.f = new HashMap();
        new HashMap();
    }

    public static q f() {
        return g;
    }

    @Deprecated
    public b d(Descriptors.b bVar, int i) {
        return e(bVar, i);
    }

    public b e(Descriptors.b bVar, int i) {
        return this.f.get(new a(bVar, i));
    }

    public q(boolean z) {
        super(r.e);
        Collections.emptyMap();
        Collections.emptyMap();
        this.f = Collections.emptyMap();
        Collections.emptyMap();
    }
}
