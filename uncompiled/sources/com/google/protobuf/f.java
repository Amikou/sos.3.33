package com.google.protobuf;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.WireFormat;
import com.google.protobuf.h0;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Map;

/* compiled from: BinaryReader.java */
/* loaded from: classes2.dex */
public abstract class f implements w0 {

    /* compiled from: BinaryReader.java */
    /* loaded from: classes2.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[WireFormat.FieldType.values().length];
            a = iArr;
            try {
                iArr[WireFormat.FieldType.BOOL.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[WireFormat.FieldType.BYTES.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[WireFormat.FieldType.DOUBLE.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                a[WireFormat.FieldType.ENUM.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                a[WireFormat.FieldType.FIXED32.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                a[WireFormat.FieldType.FIXED64.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                a[WireFormat.FieldType.FLOAT.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                a[WireFormat.FieldType.INT32.ordinal()] = 8;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                a[WireFormat.FieldType.INT64.ordinal()] = 9;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                a[WireFormat.FieldType.MESSAGE.ordinal()] = 10;
            } catch (NoSuchFieldError unused10) {
            }
            try {
                a[WireFormat.FieldType.SFIXED32.ordinal()] = 11;
            } catch (NoSuchFieldError unused11) {
            }
            try {
                a[WireFormat.FieldType.SFIXED64.ordinal()] = 12;
            } catch (NoSuchFieldError unused12) {
            }
            try {
                a[WireFormat.FieldType.SINT32.ordinal()] = 13;
            } catch (NoSuchFieldError unused13) {
            }
            try {
                a[WireFormat.FieldType.SINT64.ordinal()] = 14;
            } catch (NoSuchFieldError unused14) {
            }
            try {
                a[WireFormat.FieldType.STRING.ordinal()] = 15;
            } catch (NoSuchFieldError unused15) {
            }
            try {
                a[WireFormat.FieldType.UINT32.ordinal()] = 16;
            } catch (NoSuchFieldError unused16) {
            }
            try {
                a[WireFormat.FieldType.UINT64.ordinal()] = 17;
            } catch (NoSuchFieldError unused17) {
            }
        }
    }

    /* compiled from: BinaryReader.java */
    /* loaded from: classes2.dex */
    public static final class b extends f {
        public final boolean a;
        public final byte[] b;
        public int c;
        public int d;
        public int e;
        public int f;

        public b(ByteBuffer byteBuffer, boolean z) {
            super(null);
            this.a = z;
            this.b = byteBuffer.array();
            this.c = byteBuffer.arrayOffset() + byteBuffer.position();
            this.d = byteBuffer.arrayOffset() + byteBuffer.limit();
        }

        @Override // com.google.protobuf.w0
        public void A(List<Float> list) throws IOException {
            int i;
            int i2;
            if (list instanceof x) {
                x xVar = (x) list;
                int b = WireFormat.b(this.e);
                if (b == 2) {
                    int d0 = d0();
                    n0(d0);
                    int i3 = this.c + d0;
                    while (this.c < i3) {
                        xVar.m(Float.intBitsToFloat(X()));
                    }
                    return;
                } else if (b == 5) {
                    do {
                        xVar.m(readFloat());
                        if (S()) {
                            return;
                        }
                        i2 = this.c;
                    } while (d0() == this.e);
                    this.c = i2;
                    return;
                } else {
                    throw InvalidProtocolBufferException.invalidWireType();
                }
            }
            int b2 = WireFormat.b(this.e);
            if (b2 == 2) {
                int d02 = d0();
                n0(d02);
                int i4 = this.c + d02;
                while (this.c < i4) {
                    list.add(Float.valueOf(Float.intBitsToFloat(X())));
                }
            } else if (b2 == 5) {
                do {
                    list.add(Float.valueOf(readFloat()));
                    if (S()) {
                        return;
                    }
                    i = this.c;
                } while (d0() == this.e);
                this.c = i;
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }

        @Override // com.google.protobuf.w0
        public int B() throws IOException {
            i0(0);
            return d0();
        }

        @Override // com.google.protobuf.w0
        public boolean C() throws IOException {
            int i;
            if (S() || (i = this.e) == this.f) {
                return false;
            }
            int b = WireFormat.b(i);
            if (b == 0) {
                l0();
                return true;
            } else if (b == 1) {
                j0(8);
                return true;
            } else if (b == 2) {
                j0(d0());
                return true;
            } else if (b == 3) {
                k0();
                return true;
            } else if (b == 5) {
                j0(4);
                return true;
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }

        @Override // com.google.protobuf.w0
        public int D() throws IOException {
            i0(5);
            return W();
        }

        @Override // com.google.protobuf.w0
        public void E(List<ByteString> list) throws IOException {
            int i;
            if (WireFormat.b(this.e) == 2) {
                do {
                    list.add(z());
                    if (S()) {
                        return;
                    }
                    i = this.c;
                } while (d0() == this.e);
                this.c = i;
                return;
            }
            throw InvalidProtocolBufferException.invalidWireType();
        }

        @Override // com.google.protobuf.w0
        public void F(List<Double> list) throws IOException {
            int i;
            int i2;
            if (list instanceof n) {
                n nVar = (n) list;
                int b = WireFormat.b(this.e);
                if (b == 1) {
                    do {
                        nVar.m(readDouble());
                        if (S()) {
                            return;
                        }
                        i2 = this.c;
                    } while (d0() == this.e);
                    this.c = i2;
                    return;
                } else if (b == 2) {
                    int d0 = d0();
                    o0(d0);
                    int i3 = this.c + d0;
                    while (this.c < i3) {
                        nVar.m(Double.longBitsToDouble(Z()));
                    }
                    return;
                } else {
                    throw InvalidProtocolBufferException.invalidWireType();
                }
            }
            int b2 = WireFormat.b(this.e);
            if (b2 == 1) {
                do {
                    list.add(Double.valueOf(readDouble()));
                    if (S()) {
                        return;
                    }
                    i = this.c;
                } while (d0() == this.e);
                this.c = i;
            } else if (b2 == 2) {
                int d02 = d0();
                o0(d02);
                int i4 = this.c + d02;
                while (this.c < i4) {
                    list.add(Double.valueOf(Double.longBitsToDouble(Z())));
                }
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }

        @Override // com.google.protobuf.w0
        public long G() throws IOException {
            i0(0);
            return e0();
        }

        @Override // com.google.protobuf.w0
        public String H() throws IOException {
            return b0(true);
        }

        @Override // com.google.protobuf.w0
        public void I(List<Long> list) throws IOException {
            int i;
            int i2;
            if (list instanceof f0) {
                f0 f0Var = (f0) list;
                int b = WireFormat.b(this.e);
                if (b == 1) {
                    do {
                        f0Var.n(d());
                        if (S()) {
                            return;
                        }
                        i2 = this.c;
                    } while (d0() == this.e);
                    this.c = i2;
                    return;
                } else if (b == 2) {
                    int d0 = d0();
                    o0(d0);
                    int i3 = this.c + d0;
                    while (this.c < i3) {
                        f0Var.n(Z());
                    }
                    return;
                } else {
                    throw InvalidProtocolBufferException.invalidWireType();
                }
            }
            int b2 = WireFormat.b(this.e);
            if (b2 == 1) {
                do {
                    list.add(Long.valueOf(d()));
                    if (S()) {
                        return;
                    }
                    i = this.c;
                } while (d0() == this.e);
                this.c = i;
            } else if (b2 == 2) {
                int d02 = d0();
                o0(d02);
                int i4 = this.c + d02;
                while (this.c < i4) {
                    list.add(Long.valueOf(Z()));
                }
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.google.protobuf.w0
        public <T> void J(List<T> list, y0<T> y0Var, r rVar) throws IOException {
            int i;
            if (WireFormat.b(this.e) == 3) {
                int i2 = this.e;
                do {
                    list.add(V(y0Var, rVar));
                    if (S()) {
                        return;
                    }
                    i = this.c;
                } while (d0() == i2);
                this.c = i;
                return;
            }
            throw InvalidProtocolBufferException.invalidWireType();
        }

        @Override // com.google.protobuf.w0
        public <T> T K(Class<T> cls, r rVar) throws IOException {
            i0(2);
            return (T) a0(u0.a().d(cls), rVar);
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.google.protobuf.w0
        public <T> void L(List<T> list, y0<T> y0Var, r rVar) throws IOException {
            int i;
            if (WireFormat.b(this.e) == 2) {
                int i2 = this.e;
                do {
                    list.add(a0(y0Var, rVar));
                    if (S()) {
                        return;
                    }
                    i = this.c;
                } while (d0() == i2);
                this.c = i;
                return;
            }
            throw InvalidProtocolBufferException.invalidWireType();
        }

        @Override // com.google.protobuf.w0
        public <T> T M(y0<T> y0Var, r rVar) throws IOException {
            i0(3);
            return (T) V(y0Var, rVar);
        }

        @Override // com.google.protobuf.w0
        public <T> T N(Class<T> cls, r rVar) throws IOException {
            i0(3);
            return (T) V(u0.a().d(cls), rVar);
        }

        @Override // com.google.protobuf.w0
        public <T> T O(y0<T> y0Var, r rVar) throws IOException {
            i0(2);
            return (T) a0(y0Var, rVar);
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.google.protobuf.w0
        public <K, V> void P(Map<K, V> map, h0.b<K, V> bVar, r rVar) throws IOException {
            i0(2);
            int d0 = d0();
            g0(d0);
            int i = this.d;
            this.d = this.c + d0;
            try {
                Object obj = bVar.b;
                Object obj2 = bVar.d;
                while (true) {
                    int w = w();
                    if (w == Integer.MAX_VALUE) {
                        map.put(obj, obj2);
                        return;
                    } else if (w == 1) {
                        obj = U(bVar.a, null, null);
                    } else if (w != 2) {
                        try {
                            if (!C()) {
                                throw new InvalidProtocolBufferException("Unable to parse map entry.");
                                break;
                            }
                        } catch (InvalidProtocolBufferException.InvalidWireTypeException unused) {
                            if (!C()) {
                                throw new InvalidProtocolBufferException("Unable to parse map entry.");
                            }
                        }
                    } else {
                        obj2 = U(bVar.c, bVar.d.getClass(), rVar);
                    }
                }
            } finally {
                this.d = i;
            }
        }

        public final boolean S() {
            return this.c == this.d;
        }

        public final byte T() throws IOException {
            int i = this.c;
            if (i != this.d) {
                byte[] bArr = this.b;
                this.c = i + 1;
                return bArr[i];
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }

        public final Object U(WireFormat.FieldType fieldType, Class<?> cls, r rVar) throws IOException {
            switch (a.a[fieldType.ordinal()]) {
                case 1:
                    return Boolean.valueOf(i());
                case 2:
                    return z();
                case 3:
                    return Double.valueOf(readDouble());
                case 4:
                    return Integer.valueOf(q());
                case 5:
                    return Integer.valueOf(h());
                case 6:
                    return Long.valueOf(d());
                case 7:
                    return Float.valueOf(readFloat());
                case 8:
                    return Integer.valueOf(B());
                case 9:
                    return Long.valueOf(G());
                case 10:
                    return K(cls, rVar);
                case 11:
                    return Integer.valueOf(D());
                case 12:
                    return Long.valueOf(j());
                case 13:
                    return Integer.valueOf(s());
                case 14:
                    return Long.valueOf(t());
                case 15:
                    return H();
                case 16:
                    return Integer.valueOf(l());
                case 17:
                    return Long.valueOf(c());
                default:
                    throw new RuntimeException("unsupported field type.");
            }
        }

        public final <T> T V(y0<T> y0Var, r rVar) throws IOException {
            int i = this.f;
            this.f = WireFormat.c(WireFormat.a(this.e), 4);
            try {
                T c = y0Var.c();
                y0Var.i(c, this, rVar);
                y0Var.e(c);
                if (this.e == this.f) {
                    return c;
                }
                throw InvalidProtocolBufferException.parseFailure();
            } finally {
                this.f = i;
            }
        }

        public final int W() throws IOException {
            g0(4);
            return X();
        }

        public final int X() {
            int i = this.c;
            byte[] bArr = this.b;
            this.c = i + 4;
            return ((bArr[i + 3] & 255) << 24) | (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << 16);
        }

        public final long Y() throws IOException {
            g0(8);
            return Z();
        }

        public final long Z() {
            int i = this.c;
            byte[] bArr = this.b;
            this.c = i + 8;
            return ((bArr[i + 7] & 255) << 56) | (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << 16) | ((bArr[i + 3] & 255) << 24) | ((bArr[i + 4] & 255) << 32) | ((bArr[i + 5] & 255) << 40) | ((bArr[i + 6] & 255) << 48);
        }

        @Override // com.google.protobuf.w0
        public int a() {
            return this.e;
        }

        public final <T> T a0(y0<T> y0Var, r rVar) throws IOException {
            int d0 = d0();
            g0(d0);
            int i = this.d;
            int i2 = this.c + d0;
            this.d = i2;
            try {
                T c = y0Var.c();
                y0Var.i(c, this, rVar);
                y0Var.e(c);
                if (this.c == i2) {
                    return c;
                }
                throw InvalidProtocolBufferException.parseFailure();
            } finally {
                this.d = i;
            }
        }

        @Override // com.google.protobuf.w0
        public void b(List<Integer> list) throws IOException {
            int i;
            int i2;
            if (list instanceof z) {
                z zVar = (z) list;
                int b = WireFormat.b(this.e);
                if (b != 0) {
                    if (b == 2) {
                        int d0 = this.c + d0();
                        while (this.c < d0) {
                            zVar.X(j.b(d0()));
                        }
                        return;
                    }
                    throw InvalidProtocolBufferException.invalidWireType();
                }
                do {
                    zVar.X(s());
                    if (S()) {
                        return;
                    }
                    i2 = this.c;
                } while (d0() == this.e);
                this.c = i2;
                return;
            }
            int b2 = WireFormat.b(this.e);
            if (b2 != 0) {
                if (b2 == 2) {
                    int d02 = this.c + d0();
                    while (this.c < d02) {
                        list.add(Integer.valueOf(j.b(d0())));
                    }
                    return;
                }
                throw InvalidProtocolBufferException.invalidWireType();
            }
            do {
                list.add(Integer.valueOf(s()));
                if (S()) {
                    return;
                }
                i = this.c;
            } while (d0() == this.e);
            this.c = i;
        }

        public String b0(boolean z) throws IOException {
            i0(2);
            int d0 = d0();
            if (d0 == 0) {
                return "";
            }
            g0(d0);
            if (z) {
                byte[] bArr = this.b;
                int i = this.c;
                if (!Utf8.t(bArr, i, i + d0)) {
                    throw InvalidProtocolBufferException.invalidUtf8();
                }
            }
            String str = new String(this.b, this.c, d0, a0.a);
            this.c += d0;
            return str;
        }

        @Override // com.google.protobuf.w0
        public long c() throws IOException {
            i0(0);
            return e0();
        }

        public void c0(List<String> list, boolean z) throws IOException {
            int i;
            int i2;
            if (WireFormat.b(this.e) == 2) {
                if ((list instanceof cz1) && !z) {
                    cz1 cz1Var = (cz1) list;
                    do {
                        cz1Var.W(z());
                        if (S()) {
                            return;
                        }
                        i2 = this.c;
                    } while (d0() == this.e);
                    this.c = i2;
                    return;
                }
                do {
                    list.add(b0(z));
                    if (S()) {
                        return;
                    }
                    i = this.c;
                } while (d0() == this.e);
                this.c = i;
                return;
            }
            throw InvalidProtocolBufferException.invalidWireType();
        }

        @Override // com.google.protobuf.w0
        public long d() throws IOException {
            i0(1);
            return Y();
        }

        public final int d0() throws IOException {
            int i;
            int i2 = this.c;
            int i3 = this.d;
            if (i3 != i2) {
                byte[] bArr = this.b;
                int i4 = i2 + 1;
                byte b = bArr[i2];
                if (b >= 0) {
                    this.c = i4;
                    return b;
                } else if (i3 - i4 < 9) {
                    return (int) f0();
                } else {
                    int i5 = i4 + 1;
                    int i6 = b ^ (bArr[i4] << 7);
                    if (i6 < 0) {
                        i = i6 ^ (-128);
                    } else {
                        int i7 = i5 + 1;
                        int i8 = i6 ^ (bArr[i5] << 14);
                        if (i8 >= 0) {
                            i = i8 ^ 16256;
                        } else {
                            i5 = i7 + 1;
                            int i9 = i8 ^ (bArr[i7] << 21);
                            if (i9 < 0) {
                                i = i9 ^ (-2080896);
                            } else {
                                i7 = i5 + 1;
                                byte b2 = bArr[i5];
                                i = (i9 ^ (b2 << 28)) ^ 266354560;
                                if (b2 < 0) {
                                    i5 = i7 + 1;
                                    if (bArr[i7] < 0) {
                                        i7 = i5 + 1;
                                        if (bArr[i5] < 0) {
                                            i5 = i7 + 1;
                                            if (bArr[i7] < 0) {
                                                i7 = i5 + 1;
                                                if (bArr[i5] < 0) {
                                                    i5 = i7 + 1;
                                                    if (bArr[i7] < 0) {
                                                        throw InvalidProtocolBufferException.malformedVarint();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        i5 = i7;
                    }
                    this.c = i5;
                    return i;
                }
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }

        @Override // com.google.protobuf.w0
        public void e(List<Integer> list) throws IOException {
            int i;
            int i2;
            if (list instanceof z) {
                z zVar = (z) list;
                int b = WireFormat.b(this.e);
                if (b == 2) {
                    int d0 = d0();
                    n0(d0);
                    int i3 = this.c + d0;
                    while (this.c < i3) {
                        zVar.X(X());
                    }
                    return;
                } else if (b == 5) {
                    do {
                        zVar.X(D());
                        if (S()) {
                            return;
                        }
                        i2 = this.c;
                    } while (d0() == this.e);
                    this.c = i2;
                    return;
                } else {
                    throw InvalidProtocolBufferException.invalidWireType();
                }
            }
            int b2 = WireFormat.b(this.e);
            if (b2 == 2) {
                int d02 = d0();
                n0(d02);
                int i4 = this.c + d02;
                while (this.c < i4) {
                    list.add(Integer.valueOf(X()));
                }
            } else if (b2 == 5) {
                do {
                    list.add(Integer.valueOf(D()));
                    if (S()) {
                        return;
                    }
                    i = this.c;
                } while (d0() == this.e);
                this.c = i;
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }

        public long e0() throws IOException {
            long j;
            long j2;
            long j3;
            int i;
            int i2 = this.c;
            int i3 = this.d;
            if (i3 != i2) {
                byte[] bArr = this.b;
                int i4 = i2 + 1;
                byte b = bArr[i2];
                if (b >= 0) {
                    this.c = i4;
                    return b;
                } else if (i3 - i4 < 9) {
                    return f0();
                } else {
                    int i5 = i4 + 1;
                    int i6 = b ^ (bArr[i4] << 7);
                    if (i6 >= 0) {
                        int i7 = i5 + 1;
                        int i8 = i6 ^ (bArr[i5] << 14);
                        if (i8 >= 0) {
                            i5 = i7;
                            j = i8 ^ 16256;
                        } else {
                            i5 = i7 + 1;
                            int i9 = i8 ^ (bArr[i7] << 21);
                            if (i9 < 0) {
                                i = i9 ^ (-2080896);
                            } else {
                                long j4 = i9;
                                int i10 = i5 + 1;
                                long j5 = j4 ^ (bArr[i5] << 28);
                                if (j5 >= 0) {
                                    j3 = 266354560;
                                } else {
                                    i5 = i10 + 1;
                                    long j6 = j5 ^ (bArr[i10] << 35);
                                    if (j6 < 0) {
                                        j2 = -34093383808L;
                                    } else {
                                        i10 = i5 + 1;
                                        j5 = j6 ^ (bArr[i5] << 42);
                                        if (j5 >= 0) {
                                            j3 = 4363953127296L;
                                        } else {
                                            i5 = i10 + 1;
                                            j6 = j5 ^ (bArr[i10] << 49);
                                            if (j6 < 0) {
                                                j2 = -558586000294016L;
                                            } else {
                                                int i11 = i5 + 1;
                                                long j7 = (j6 ^ (bArr[i5] << 56)) ^ 71499008037633920L;
                                                if (j7 < 0) {
                                                    i5 = i11 + 1;
                                                    if (bArr[i11] < 0) {
                                                        throw InvalidProtocolBufferException.malformedVarint();
                                                    }
                                                } else {
                                                    i5 = i11;
                                                }
                                                j = j7;
                                            }
                                        }
                                    }
                                    j = j6 ^ j2;
                                }
                                j = j5 ^ j3;
                                i5 = i10;
                            }
                        }
                        this.c = i5;
                        return j;
                    }
                    i = i6 ^ (-128);
                    j = i;
                    this.c = i5;
                    return j;
                }
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }

        @Override // com.google.protobuf.w0
        public void f(List<Long> list) throws IOException {
            int i;
            int i2;
            if (list instanceof f0) {
                f0 f0Var = (f0) list;
                int b = WireFormat.b(this.e);
                if (b != 0) {
                    if (b == 2) {
                        int d0 = this.c + d0();
                        while (this.c < d0) {
                            f0Var.n(j.c(e0()));
                        }
                        return;
                    }
                    throw InvalidProtocolBufferException.invalidWireType();
                }
                do {
                    f0Var.n(t());
                    if (S()) {
                        return;
                    }
                    i2 = this.c;
                } while (d0() == this.e);
                this.c = i2;
                return;
            }
            int b2 = WireFormat.b(this.e);
            if (b2 != 0) {
                if (b2 == 2) {
                    int d02 = this.c + d0();
                    while (this.c < d02) {
                        list.add(Long.valueOf(j.c(e0())));
                    }
                    return;
                }
                throw InvalidProtocolBufferException.invalidWireType();
            }
            do {
                list.add(Long.valueOf(t()));
                if (S()) {
                    return;
                }
                i = this.c;
            } while (d0() == this.e);
            this.c = i;
        }

        public final long f0() throws IOException {
            long j = 0;
            for (int i = 0; i < 64; i += 7) {
                byte T = T();
                j |= (T & Byte.MAX_VALUE) << i;
                if ((T & 128) == 0) {
                    return j;
                }
            }
            throw InvalidProtocolBufferException.malformedVarint();
        }

        @Override // com.google.protobuf.w0
        public void g(List<Integer> list) throws IOException {
            int i;
            int i2;
            if (list instanceof z) {
                z zVar = (z) list;
                int b = WireFormat.b(this.e);
                if (b != 0) {
                    if (b == 2) {
                        int d0 = this.c + d0();
                        while (this.c < d0) {
                            zVar.X(d0());
                        }
                        return;
                    }
                    throw InvalidProtocolBufferException.invalidWireType();
                }
                do {
                    zVar.X(l());
                    if (S()) {
                        return;
                    }
                    i2 = this.c;
                } while (d0() == this.e);
                this.c = i2;
                return;
            }
            int b2 = WireFormat.b(this.e);
            if (b2 != 0) {
                if (b2 == 2) {
                    int d02 = this.c + d0();
                    while (this.c < d02) {
                        list.add(Integer.valueOf(d0()));
                    }
                    return;
                }
                throw InvalidProtocolBufferException.invalidWireType();
            }
            do {
                list.add(Integer.valueOf(l()));
                if (S()) {
                    return;
                }
                i = this.c;
            } while (d0() == this.e);
            this.c = i;
        }

        public final void g0(int i) throws IOException {
            if (i < 0 || i > this.d - this.c) {
                throw InvalidProtocolBufferException.truncatedMessage();
            }
        }

        @Override // com.google.protobuf.w0
        public int h() throws IOException {
            i0(5);
            return W();
        }

        public final void h0(int i) throws IOException {
            if (this.c != i) {
                throw InvalidProtocolBufferException.truncatedMessage();
            }
        }

        @Override // com.google.protobuf.w0
        public boolean i() throws IOException {
            i0(0);
            return d0() != 0;
        }

        public final void i0(int i) throws IOException {
            if (WireFormat.b(this.e) != i) {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }

        @Override // com.google.protobuf.w0
        public long j() throws IOException {
            i0(1);
            return Y();
        }

        public final void j0(int i) throws IOException {
            g0(i);
            this.c += i;
        }

        @Override // com.google.protobuf.w0
        public void k(List<Long> list) throws IOException {
            int i;
            int i2;
            if (list instanceof f0) {
                f0 f0Var = (f0) list;
                int b = WireFormat.b(this.e);
                if (b == 0) {
                    do {
                        f0Var.n(c());
                        if (S()) {
                            return;
                        }
                        i2 = this.c;
                    } while (d0() == this.e);
                    this.c = i2;
                    return;
                } else if (b == 2) {
                    int d0 = this.c + d0();
                    while (this.c < d0) {
                        f0Var.n(e0());
                    }
                    h0(d0);
                    return;
                } else {
                    throw InvalidProtocolBufferException.invalidWireType();
                }
            }
            int b2 = WireFormat.b(this.e);
            if (b2 == 0) {
                do {
                    list.add(Long.valueOf(c()));
                    if (S()) {
                        return;
                    }
                    i = this.c;
                } while (d0() == this.e);
                this.c = i;
            } else if (b2 == 2) {
                int d02 = this.c + d0();
                while (this.c < d02) {
                    list.add(Long.valueOf(e0()));
                }
                h0(d02);
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }

        public final void k0() throws IOException {
            int i = this.f;
            this.f = WireFormat.c(WireFormat.a(this.e), 4);
            while (w() != Integer.MAX_VALUE && C()) {
            }
            if (this.e == this.f) {
                this.f = i;
                return;
            }
            throw InvalidProtocolBufferException.parseFailure();
        }

        @Override // com.google.protobuf.w0
        public int l() throws IOException {
            i0(0);
            return d0();
        }

        public final void l0() throws IOException {
            int i = this.d;
            int i2 = this.c;
            if (i - i2 >= 10) {
                byte[] bArr = this.b;
                int i3 = 0;
                while (i3 < 10) {
                    int i4 = i2 + 1;
                    if (bArr[i2] >= 0) {
                        this.c = i4;
                        return;
                    } else {
                        i3++;
                        i2 = i4;
                    }
                }
            }
            m0();
        }

        @Override // com.google.protobuf.w0
        public void m(List<Long> list) throws IOException {
            int i;
            int i2;
            if (list instanceof f0) {
                f0 f0Var = (f0) list;
                int b = WireFormat.b(this.e);
                if (b == 0) {
                    do {
                        f0Var.n(G());
                        if (S()) {
                            return;
                        }
                        i2 = this.c;
                    } while (d0() == this.e);
                    this.c = i2;
                    return;
                } else if (b == 2) {
                    int d0 = this.c + d0();
                    while (this.c < d0) {
                        f0Var.n(e0());
                    }
                    h0(d0);
                    return;
                } else {
                    throw InvalidProtocolBufferException.invalidWireType();
                }
            }
            int b2 = WireFormat.b(this.e);
            if (b2 == 0) {
                do {
                    list.add(Long.valueOf(G()));
                    if (S()) {
                        return;
                    }
                    i = this.c;
                } while (d0() == this.e);
                this.c = i;
            } else if (b2 == 2) {
                int d02 = this.c + d0();
                while (this.c < d02) {
                    list.add(Long.valueOf(e0()));
                }
                h0(d02);
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }

        public final void m0() throws IOException {
            for (int i = 0; i < 10; i++) {
                if (T() >= 0) {
                    return;
                }
            }
            throw InvalidProtocolBufferException.malformedVarint();
        }

        @Override // com.google.protobuf.w0
        public void n(List<Long> list) throws IOException {
            int i;
            int i2;
            if (list instanceof f0) {
                f0 f0Var = (f0) list;
                int b = WireFormat.b(this.e);
                if (b == 1) {
                    do {
                        f0Var.n(j());
                        if (S()) {
                            return;
                        }
                        i2 = this.c;
                    } while (d0() == this.e);
                    this.c = i2;
                    return;
                } else if (b == 2) {
                    int d0 = d0();
                    o0(d0);
                    int i3 = this.c + d0;
                    while (this.c < i3) {
                        f0Var.n(Z());
                    }
                    return;
                } else {
                    throw InvalidProtocolBufferException.invalidWireType();
                }
            }
            int b2 = WireFormat.b(this.e);
            if (b2 == 1) {
                do {
                    list.add(Long.valueOf(j()));
                    if (S()) {
                        return;
                    }
                    i = this.c;
                } while (d0() == this.e);
                this.c = i;
            } else if (b2 == 2) {
                int d02 = d0();
                o0(d02);
                int i4 = this.c + d02;
                while (this.c < i4) {
                    list.add(Long.valueOf(Z()));
                }
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }

        public final void n0(int i) throws IOException {
            g0(i);
            if ((i & 3) != 0) {
                throw InvalidProtocolBufferException.parseFailure();
            }
        }

        @Override // com.google.protobuf.w0
        public void o(List<Integer> list) throws IOException {
            int i;
            int i2;
            if (list instanceof z) {
                z zVar = (z) list;
                int b = WireFormat.b(this.e);
                if (b == 0) {
                    do {
                        zVar.X(B());
                        if (S()) {
                            return;
                        }
                        i2 = this.c;
                    } while (d0() == this.e);
                    this.c = i2;
                    return;
                } else if (b == 2) {
                    int d0 = this.c + d0();
                    while (this.c < d0) {
                        zVar.X(d0());
                    }
                    h0(d0);
                    return;
                } else {
                    throw InvalidProtocolBufferException.invalidWireType();
                }
            }
            int b2 = WireFormat.b(this.e);
            if (b2 == 0) {
                do {
                    list.add(Integer.valueOf(B()));
                    if (S()) {
                        return;
                    }
                    i = this.c;
                } while (d0() == this.e);
                this.c = i;
            } else if (b2 == 2) {
                int d02 = this.c + d0();
                while (this.c < d02) {
                    list.add(Integer.valueOf(d0()));
                }
                h0(d02);
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }

        public final void o0(int i) throws IOException {
            g0(i);
            if ((i & 7) != 0) {
                throw InvalidProtocolBufferException.parseFailure();
            }
        }

        @Override // com.google.protobuf.w0
        public void p(List<Integer> list) throws IOException {
            int i;
            int i2;
            if (list instanceof z) {
                z zVar = (z) list;
                int b = WireFormat.b(this.e);
                if (b != 0) {
                    if (b == 2) {
                        int d0 = this.c + d0();
                        while (this.c < d0) {
                            zVar.X(d0());
                        }
                        return;
                    }
                    throw InvalidProtocolBufferException.invalidWireType();
                }
                do {
                    zVar.X(q());
                    if (S()) {
                        return;
                    }
                    i2 = this.c;
                } while (d0() == this.e);
                this.c = i2;
                return;
            }
            int b2 = WireFormat.b(this.e);
            if (b2 != 0) {
                if (b2 == 2) {
                    int d02 = this.c + d0();
                    while (this.c < d02) {
                        list.add(Integer.valueOf(d0()));
                    }
                    return;
                }
                throw InvalidProtocolBufferException.invalidWireType();
            }
            do {
                list.add(Integer.valueOf(q()));
                if (S()) {
                    return;
                }
                i = this.c;
            } while (d0() == this.e);
            this.c = i;
        }

        @Override // com.google.protobuf.w0
        public int q() throws IOException {
            i0(0);
            return d0();
        }

        @Override // com.google.protobuf.w0
        public void r(List<Integer> list) throws IOException {
            int i;
            int i2;
            if (list instanceof z) {
                z zVar = (z) list;
                int b = WireFormat.b(this.e);
                if (b == 2) {
                    int d0 = d0();
                    n0(d0);
                    int i3 = this.c + d0;
                    while (this.c < i3) {
                        zVar.X(X());
                    }
                    return;
                } else if (b == 5) {
                    do {
                        zVar.X(h());
                        if (S()) {
                            return;
                        }
                        i2 = this.c;
                    } while (d0() == this.e);
                    this.c = i2;
                    return;
                } else {
                    throw InvalidProtocolBufferException.invalidWireType();
                }
            }
            int b2 = WireFormat.b(this.e);
            if (b2 == 2) {
                int d02 = d0();
                n0(d02);
                int i4 = this.c + d02;
                while (this.c < i4) {
                    list.add(Integer.valueOf(X()));
                }
            } else if (b2 == 5) {
                do {
                    list.add(Integer.valueOf(h()));
                    if (S()) {
                        return;
                    }
                    i = this.c;
                } while (d0() == this.e);
                this.c = i;
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }

        @Override // com.google.protobuf.w0
        public double readDouble() throws IOException {
            i0(1);
            return Double.longBitsToDouble(Y());
        }

        @Override // com.google.protobuf.w0
        public float readFloat() throws IOException {
            i0(5);
            return Float.intBitsToFloat(W());
        }

        @Override // com.google.protobuf.w0
        public int s() throws IOException {
            i0(0);
            return j.b(d0());
        }

        @Override // com.google.protobuf.w0
        public long t() throws IOException {
            i0(0);
            return j.c(e0());
        }

        @Override // com.google.protobuf.w0
        public void u(List<Boolean> list) throws IOException {
            int i;
            int i2;
            if (list instanceof g) {
                g gVar = (g) list;
                int b = WireFormat.b(this.e);
                if (b != 0) {
                    if (b == 2) {
                        int d0 = this.c + d0();
                        while (this.c < d0) {
                            gVar.n(d0() != 0);
                        }
                        h0(d0);
                        return;
                    }
                    throw InvalidProtocolBufferException.invalidWireType();
                }
                do {
                    gVar.n(i());
                    if (S()) {
                        return;
                    }
                    i2 = this.c;
                } while (d0() == this.e);
                this.c = i2;
                return;
            }
            int b2 = WireFormat.b(this.e);
            if (b2 != 0) {
                if (b2 == 2) {
                    int d02 = this.c + d0();
                    while (this.c < d02) {
                        list.add(Boolean.valueOf(d0() != 0));
                    }
                    h0(d02);
                    return;
                }
                throw InvalidProtocolBufferException.invalidWireType();
            }
            do {
                list.add(Boolean.valueOf(i()));
                if (S()) {
                    return;
                }
                i = this.c;
            } while (d0() == this.e);
            this.c = i;
        }

        @Override // com.google.protobuf.w0
        public String v() throws IOException {
            return b0(false);
        }

        @Override // com.google.protobuf.w0
        public int w() throws IOException {
            if (S()) {
                return Integer.MAX_VALUE;
            }
            int d0 = d0();
            this.e = d0;
            if (d0 == this.f) {
                return Integer.MAX_VALUE;
            }
            return WireFormat.a(d0);
        }

        @Override // com.google.protobuf.w0
        public void x(List<String> list) throws IOException {
            c0(list, false);
        }

        @Override // com.google.protobuf.w0
        public void y(List<String> list) throws IOException {
            c0(list, true);
        }

        @Override // com.google.protobuf.w0
        public ByteString z() throws IOException {
            ByteString copyFrom;
            i0(2);
            int d0 = d0();
            if (d0 == 0) {
                return ByteString.EMPTY;
            }
            g0(d0);
            if (this.a) {
                copyFrom = ByteString.wrap(this.b, this.c, d0);
            } else {
                copyFrom = ByteString.copyFrom(this.b, this.c, d0);
            }
            this.c += d0;
            return copyFrom;
        }
    }

    public /* synthetic */ f(a aVar) {
        this();
    }

    public static f R(ByteBuffer byteBuffer, boolean z) {
        if (byteBuffer.hasArray()) {
            return new b(byteBuffer, z);
        }
        throw new IllegalArgumentException("Direct buffers not yet supported");
    }

    @Override // com.google.protobuf.w0
    public boolean Q() {
        return false;
    }

    public f() {
    }
}
