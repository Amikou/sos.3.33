package com.google.protobuf;

import com.google.protobuf.Descriptors;
import java.util.Map;

/* compiled from: MessageOrBuilder.java */
/* loaded from: classes2.dex */
public interface o0 extends d82 {
    Map<Descriptors.FieldDescriptor, Object> getAllFields();

    @Override // 
    l0 getDefaultInstanceForType();

    Descriptors.b getDescriptorForType();

    Object getField(Descriptors.FieldDescriptor fieldDescriptor);

    g1 getUnknownFields();

    boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);
}
