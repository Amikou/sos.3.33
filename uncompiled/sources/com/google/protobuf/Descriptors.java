package com.google.protobuf;

import com.github.mikephil.charting.utils.Utils;
import com.google.protobuf.DescriptorProtos;
import com.google.protobuf.TextFormat;
import com.google.protobuf.WireFormat;
import com.google.protobuf.a0;
import com.google.protobuf.l0;
import com.google.protobuf.m0;
import com.google.protobuf.w;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.logging.Logger;

/* loaded from: classes2.dex */
public final class Descriptors {
    public static final Logger a = Logger.getLogger(Descriptors.class.getName());

    /* loaded from: classes2.dex */
    public static final class DescriptorPool {
        public boolean b;
        public final Map<String, e> c = new HashMap();
        public final Map<a, FieldDescriptor> d = new HashMap();
        public final Map<a, d> e = new HashMap();
        public final Set<FileDescriptor> a = new HashSet();

        /* loaded from: classes2.dex */
        public enum SearchFilter {
            TYPES_ONLY,
            AGGREGATES_ONLY,
            ALL_SYMBOLS
        }

        /* loaded from: classes2.dex */
        public static final class a {
            public final e a;
            public final int b;

            public a(e eVar, int i) {
                this.a = eVar;
                this.b = i;
            }

            public boolean equals(Object obj) {
                if (obj instanceof a) {
                    a aVar = (a) obj;
                    return this.a == aVar.a && this.b == aVar.b;
                }
                return false;
            }

            public int hashCode() {
                return (this.a.hashCode() * 65535) + this.b;
            }
        }

        /* loaded from: classes2.dex */
        public static final class b extends e {
            public final String a;
            public final String b;
            public final FileDescriptor c;

            public b(String str, String str2, FileDescriptor fileDescriptor) {
                super(null);
                this.c = fileDescriptor;
                this.b = str2;
                this.a = str;
            }

            @Override // com.google.protobuf.Descriptors.e
            public FileDescriptor a() {
                return this.c;
            }

            @Override // com.google.protobuf.Descriptors.e
            public String d() {
                return this.b;
            }

            @Override // com.google.protobuf.Descriptors.e
            public String e() {
                return this.a;
            }

            @Override // com.google.protobuf.Descriptors.e
            public l0 f() {
                return this.c.f();
            }
        }

        public DescriptorPool(FileDescriptor[] fileDescriptorArr, boolean z) {
            this.b = z;
            for (int i = 0; i < fileDescriptorArr.length; i++) {
                this.a.add(fileDescriptorArr[i]);
                i(fileDescriptorArr[i]);
            }
            for (FileDescriptor fileDescriptor : this.a) {
                try {
                    e(fileDescriptor.r(), fileDescriptor);
                } catch (DescriptorValidationException e) {
                    throw new AssertionError(e);
                }
            }
        }

        public static void m(e eVar) throws DescriptorValidationException {
            String e = eVar.e();
            if (e.length() != 0) {
                for (int i = 0; i < e.length(); i++) {
                    char charAt = e.charAt(i);
                    if (('a' > charAt || charAt > 'z') && (('A' > charAt || charAt > 'Z') && charAt != '_' && ('0' > charAt || charAt > '9' || i <= 0))) {
                        throw new DescriptorValidationException(eVar, '\"' + e + "\" is not a valid identifier.", (a) null);
                    }
                }
                return;
            }
            throw new DescriptorValidationException(eVar, "Missing name.", (a) null);
        }

        public void c(d dVar) {
            a aVar = new a(dVar.h(), dVar.getNumber());
            d put = this.e.put(aVar, dVar);
            if (put != null) {
                this.e.put(aVar, put);
            }
        }

        public void d(FieldDescriptor fieldDescriptor) throws DescriptorValidationException {
            a aVar = new a(fieldDescriptor.p(), fieldDescriptor.getNumber());
            FieldDescriptor put = this.d.put(aVar, fieldDescriptor);
            if (put == null) {
                return;
            }
            this.d.put(aVar, put);
            throw new DescriptorValidationException(fieldDescriptor, "Field number " + fieldDescriptor.getNumber() + " has already been used in \"" + fieldDescriptor.p().d() + "\" by field \"" + put.e() + "\".", (a) null);
        }

        public void e(String str, FileDescriptor fileDescriptor) throws DescriptorValidationException {
            String substring;
            int lastIndexOf = str.lastIndexOf(46);
            if (lastIndexOf == -1) {
                substring = str;
            } else {
                e(str.substring(0, lastIndexOf), fileDescriptor);
                substring = str.substring(lastIndexOf + 1);
            }
            e put = this.c.put(str, new b(substring, str, fileDescriptor));
            if (put != null) {
                this.c.put(str, put);
                if (put instanceof b) {
                    return;
                }
                throw new DescriptorValidationException(fileDescriptor, '\"' + substring + "\" is already defined (as something other than a package) in file \"" + put.a().e() + "\".", (a) null);
            }
        }

        public void f(e eVar) throws DescriptorValidationException {
            m(eVar);
            String d = eVar.d();
            e put = this.c.put(d, eVar);
            if (put != null) {
                this.c.put(d, put);
                if (eVar.a() == put.a()) {
                    int lastIndexOf = d.lastIndexOf(46);
                    if (lastIndexOf == -1) {
                        throw new DescriptorValidationException(eVar, '\"' + d + "\" is already defined.", (a) null);
                    }
                    throw new DescriptorValidationException(eVar, '\"' + d.substring(lastIndexOf + 1) + "\" is already defined in \"" + d.substring(0, lastIndexOf) + "\".", (a) null);
                }
                throw new DescriptorValidationException(eVar, '\"' + d + "\" is already defined in file \"" + put.a().e() + "\".", (a) null);
            }
        }

        public e g(String str) {
            return h(str, SearchFilter.ALL_SYMBOLS);
        }

        public e h(String str, SearchFilter searchFilter) {
            e eVar = this.c.get(str);
            if (eVar == null || !(searchFilter == SearchFilter.ALL_SYMBOLS || ((searchFilter == SearchFilter.TYPES_ONLY && k(eVar)) || (searchFilter == SearchFilter.AGGREGATES_ONLY && j(eVar))))) {
                for (FileDescriptor fileDescriptor : this.a) {
                    e eVar2 = fileDescriptor.g.c.get(str);
                    if (eVar2 != null && (searchFilter == SearchFilter.ALL_SYMBOLS || ((searchFilter == SearchFilter.TYPES_ONLY && k(eVar2)) || (searchFilter == SearchFilter.AGGREGATES_ONLY && j(eVar2))))) {
                        return eVar2;
                    }
                }
                return null;
            }
            return eVar;
        }

        public final void i(FileDescriptor fileDescriptor) {
            for (FileDescriptor fileDescriptor2 : fileDescriptor.s()) {
                if (this.a.add(fileDescriptor2)) {
                    i(fileDescriptor2);
                }
            }
        }

        public boolean j(e eVar) {
            return (eVar instanceof b) || (eVar instanceof c) || (eVar instanceof b) || (eVar instanceof h);
        }

        public boolean k(e eVar) {
            return (eVar instanceof b) || (eVar instanceof c);
        }

        public e l(String str, e eVar, SearchFilter searchFilter) throws DescriptorValidationException {
            e h;
            String str2;
            if (str.startsWith(".")) {
                str2 = str.substring(1);
                h = h(str2, searchFilter);
            } else {
                int indexOf = str.indexOf(46);
                String substring = indexOf == -1 ? str : str.substring(0, indexOf);
                StringBuilder sb = new StringBuilder(eVar.d());
                while (true) {
                    int lastIndexOf = sb.lastIndexOf(".");
                    if (lastIndexOf == -1) {
                        h = h(str, searchFilter);
                        str2 = str;
                        break;
                    }
                    int i = lastIndexOf + 1;
                    sb.setLength(i);
                    sb.append(substring);
                    e h2 = h(sb.toString(), SearchFilter.AGGREGATES_ONLY);
                    if (h2 != null) {
                        if (indexOf != -1) {
                            sb.setLength(i);
                            sb.append(str);
                            h = h(sb.toString(), searchFilter);
                        } else {
                            h = h2;
                        }
                        str2 = sb.toString();
                    } else {
                        sb.setLength(lastIndexOf);
                    }
                }
            }
            if (h == null) {
                if (this.b && searchFilter == SearchFilter.TYPES_ONLY) {
                    Logger logger = Descriptors.a;
                    logger.warning("The descriptor for message type \"" + str + "\" can not be found and a placeholder is created for it");
                    b bVar = new b(str2);
                    this.a.add(bVar.a());
                    return bVar;
                }
                throw new DescriptorValidationException(eVar, '\"' + str + "\" is not defined.", (a) null);
            }
            return h;
        }
    }

    /* loaded from: classes2.dex */
    public static class DescriptorValidationException extends Exception {
        private static final long serialVersionUID = 5750205775490483148L;
        private final String description;
        private final String name;
        private final l0 proto;

        public /* synthetic */ DescriptorValidationException(FileDescriptor fileDescriptor, String str, a aVar) {
            this(fileDescriptor, str);
        }

        public String getDescription() {
            return this.description;
        }

        public l0 getProblemProto() {
            return this.proto;
        }

        public String getProblemSymbolName() {
            return this.name;
        }

        public /* synthetic */ DescriptorValidationException(e eVar, String str, a aVar) {
            this(eVar, str);
        }

        public /* synthetic */ DescriptorValidationException(e eVar, String str, Throwable th, a aVar) {
            this(eVar, str, th);
        }

        public DescriptorValidationException(e eVar, String str) {
            super(eVar.d() + ": " + str);
            this.name = eVar.d();
            this.proto = eVar.f();
            this.description = str;
        }

        public DescriptorValidationException(e eVar, String str, Throwable th) {
            this(eVar, str);
            initCause(th);
        }

        public DescriptorValidationException(FileDescriptor fileDescriptor, String str) {
            super(fileDescriptor.e() + ": " + str);
            this.name = fileDescriptor.e();
            this.proto = fileDescriptor.f();
            this.description = str;
        }
    }

    /* loaded from: classes2.dex */
    public static final class FieldDescriptor extends e implements Comparable<FieldDescriptor>, w.c<FieldDescriptor> {
        public static final WireFormat.FieldType[] q0 = WireFormat.FieldType.values();
        public final int a;
        public DescriptorProtos.FieldDescriptorProto f0;
        public final String g0;
        public final FileDescriptor h0;
        public final b i0;
        public final boolean j0;
        public Type k0;
        public b l0;
        public b m0;
        public g n0;
        public c o0;
        public Object p0;

        /* loaded from: classes2.dex */
        public enum JavaType {
            INT(0),
            LONG(0L),
            FLOAT(Float.valueOf((float) Utils.FLOAT_EPSILON)),
            DOUBLE(Double.valueOf((double) Utils.DOUBLE_EPSILON)),
            BOOLEAN(Boolean.FALSE),
            STRING(""),
            BYTE_STRING(ByteString.EMPTY),
            ENUM(null),
            MESSAGE(null);
            
            private final Object defaultDefault;

            JavaType(Object obj) {
                this.defaultDefault = obj;
            }
        }

        /* JADX WARN: Enum visitor error
        jadx.core.utils.exceptions.JadxRuntimeException: Init of enum INT64 uses external variables
        	at jadx.core.dex.visitors.EnumVisitor.createEnumFieldByConstructor(EnumVisitor.java:444)
        	at jadx.core.dex.visitors.EnumVisitor.processEnumFieldByRegister(EnumVisitor.java:391)
        	at jadx.core.dex.visitors.EnumVisitor.extractEnumFieldsFromFilledArray(EnumVisitor.java:320)
        	at jadx.core.dex.visitors.EnumVisitor.extractEnumFieldsFromInsn(EnumVisitor.java:258)
        	at jadx.core.dex.visitors.EnumVisitor.convertToEnum(EnumVisitor.java:151)
        	at jadx.core.dex.visitors.EnumVisitor.visit(EnumVisitor.java:100)
         */
        /* JADX WARN: Failed to restore enum class, 'enum' modifier and super class removed */
        /* loaded from: classes2.dex */
        public static final class Type {
            public static final Type BOOL;
            public static final Type BYTES;
            public static final Type DOUBLE;
            public static final Type ENUM;
            public static final Type FIXED32;
            public static final Type FIXED64;
            public static final Type FLOAT;
            public static final Type GROUP;
            public static final Type INT32;
            public static final Type INT64;
            public static final Type MESSAGE;
            public static final Type SFIXED32;
            public static final Type SFIXED64;
            public static final Type SINT32;
            public static final Type SINT64;
            public static final Type STRING;
            public static final Type UINT32;
            public static final Type UINT64;
            public static final /* synthetic */ Type[] a;
            private JavaType javaType;

            static {
                Type type = new Type("DOUBLE", 0, JavaType.DOUBLE);
                DOUBLE = type;
                Type type2 = new Type("FLOAT", 1, JavaType.FLOAT);
                FLOAT = type2;
                JavaType javaType = JavaType.LONG;
                Type type3 = new Type("INT64", 2, javaType);
                INT64 = type3;
                Type type4 = new Type("UINT64", 3, javaType);
                UINT64 = type4;
                JavaType javaType2 = JavaType.INT;
                Type type5 = new Type("INT32", 4, javaType2);
                INT32 = type5;
                Type type6 = new Type("FIXED64", 5, javaType);
                FIXED64 = type6;
                Type type7 = new Type("FIXED32", 6, javaType2);
                FIXED32 = type7;
                Type type8 = new Type("BOOL", 7, JavaType.BOOLEAN);
                BOOL = type8;
                Type type9 = new Type("STRING", 8, JavaType.STRING);
                STRING = type9;
                JavaType javaType3 = JavaType.MESSAGE;
                Type type10 = new Type("GROUP", 9, javaType3);
                GROUP = type10;
                Type type11 = new Type("MESSAGE", 10, javaType3);
                MESSAGE = type11;
                Type type12 = new Type("BYTES", 11, JavaType.BYTE_STRING);
                BYTES = type12;
                Type type13 = new Type("UINT32", 12, javaType2);
                UINT32 = type13;
                Type type14 = new Type("ENUM", 13, JavaType.ENUM);
                ENUM = type14;
                Type type15 = new Type("SFIXED32", 14, javaType2);
                SFIXED32 = type15;
                Type type16 = new Type("SFIXED64", 15, javaType);
                SFIXED64 = type16;
                Type type17 = new Type("SINT32", 16, javaType2);
                SINT32 = type17;
                Type type18 = new Type("SINT64", 17, javaType);
                SINT64 = type18;
                a = new Type[]{type, type2, type3, type4, type5, type6, type7, type8, type9, type10, type11, type12, type13, type14, type15, type16, type17, type18};
            }

            public Type(String str, int i, JavaType javaType) {
                this.javaType = javaType;
            }

            public static Type valueOf(String str) {
                return (Type) Enum.valueOf(Type.class, str);
            }

            public static Type[] values() {
                return (Type[]) a.clone();
            }

            public JavaType getJavaType() {
                return this.javaType;
            }

            public DescriptorProtos.FieldDescriptorProto.Type toProto() {
                return DescriptorProtos.FieldDescriptorProto.Type.forNumber(ordinal() + 1);
            }

            public static Type valueOf(DescriptorProtos.FieldDescriptorProto.Type type) {
                return values()[type.getNumber() - 1];
            }
        }

        static {
            if (Type.values().length != DescriptorProtos.FieldDescriptorProto.Type.values().length) {
                throw new RuntimeException("descriptor.proto has a new declared type but Descriptors.java wasn't updated.");
            }
        }

        public /* synthetic */ FieldDescriptor(DescriptorProtos.FieldDescriptorProto fieldDescriptorProto, FileDescriptor fileDescriptor, b bVar, int i, boolean z, a aVar) throws DescriptorValidationException {
            this(fieldDescriptorProto, fileDescriptor, bVar, i, z);
        }

        public static String l(String str) {
            int length = str.length();
            StringBuilder sb = new StringBuilder(length);
            boolean z = false;
            for (int i = 0; i < length; i++) {
                char charAt = str.charAt(i);
                if (charAt == '_') {
                    z = true;
                } else if (z) {
                    if ('a' <= charAt && charAt <= 'z') {
                        charAt = (char) ((charAt - 'a') + 65);
                    }
                    sb.append(charAt);
                    z = false;
                } else {
                    sb.append(charAt);
                }
            }
            return sb.toString();
        }

        public Type A() {
            return this.k0;
        }

        public boolean B() {
            return this.j0 || (this.h0.t() == FileDescriptor.Syntax.PROTO2 && E() && o() == null);
        }

        public boolean C() {
            return this.f0.hasExtendee();
        }

        public boolean D() {
            return A() == Type.MESSAGE && i() && x().t().getMapEntry();
        }

        public boolean E() {
            return this.f0.getLabel() == DescriptorProtos.FieldDescriptorProto.Label.LABEL_OPTIONAL;
        }

        public boolean G() {
            return i() && n().isPackable();
        }

        public boolean H() {
            return this.f0.getLabel() == DescriptorProtos.FieldDescriptorProto.Label.LABEL_REQUIRED;
        }

        public boolean I() {
            if (this.k0 != Type.STRING) {
                return false;
            }
            if (p().t().getMapEntry() || a().t() == FileDescriptor.Syntax.PROTO3) {
                return true;
            }
            return a().p().getJavaStringCheckUtf8();
        }

        @Override // com.google.protobuf.Descriptors.e
        /* renamed from: J */
        public DescriptorProtos.FieldDescriptorProto f() {
            return this.f0;
        }

        @Override // com.google.protobuf.Descriptors.e
        public FileDescriptor a() {
            return this.h0;
        }

        @Override // com.google.protobuf.Descriptors.e
        public String d() {
            return this.g0;
        }

        @Override // com.google.protobuf.Descriptors.e
        public String e() {
            return this.f0.getName();
        }

        @Override // com.google.protobuf.w.c
        public int getNumber() {
            return this.f0.getNumber();
        }

        @Override // com.google.protobuf.w.c
        public boolean i() {
            return this.f0.getLabel() == DescriptorProtos.FieldDescriptorProto.Label.LABEL_REPEATED;
        }

        @Override // com.google.protobuf.w.c
        public m0.a i0(m0.a aVar, m0 m0Var) {
            return ((l0.a) aVar).mergeFrom((l0) m0Var);
        }

        @Override // com.google.protobuf.w.c
        public boolean isPacked() {
            if (G()) {
                if (a().t() == FileDescriptor.Syntax.PROTO2) {
                    return y().getPacked();
                }
                return !y().hasPacked() || y().getPacked();
            }
            return false;
        }

        @Override // java.lang.Comparable
        /* renamed from: j */
        public int compareTo(FieldDescriptor fieldDescriptor) {
            if (fieldDescriptor.l0 == this.l0) {
                return getNumber() - fieldDescriptor.getNumber();
            }
            throw new IllegalArgumentException("FieldDescriptors can only be compared to other FieldDescriptors for fields of the same message type.");
        }

        public final void k() throws DescriptorValidationException {
            if (this.f0.hasExtendee()) {
                e l = this.h0.g.l(this.f0.getExtendee(), this, DescriptorPool.SearchFilter.TYPES_ONLY);
                if (l instanceof b) {
                    this.l0 = (b) l;
                    if (!p().v(getNumber())) {
                        throw new DescriptorValidationException(this, '\"' + p().d() + "\" does not declare " + getNumber() + " as an extension number.", (a) null);
                    }
                } else {
                    throw new DescriptorValidationException(this, '\"' + this.f0.getExtendee() + "\" is not a message type.", (a) null);
                }
            }
            if (this.f0.hasTypeName()) {
                e l2 = this.h0.g.l(this.f0.getTypeName(), this, DescriptorPool.SearchFilter.TYPES_ONLY);
                if (!this.f0.hasType()) {
                    if (l2 instanceof b) {
                        this.k0 = Type.MESSAGE;
                    } else if (l2 instanceof c) {
                        this.k0 = Type.ENUM;
                    } else {
                        throw new DescriptorValidationException(this, '\"' + this.f0.getTypeName() + "\" is not a type.", (a) null);
                    }
                }
                if (v() == JavaType.MESSAGE) {
                    if (l2 instanceof b) {
                        this.m0 = (b) l2;
                        if (this.f0.hasDefaultValue()) {
                            throw new DescriptorValidationException(this, "Messages can't have default values.", (a) null);
                        }
                    } else {
                        throw new DescriptorValidationException(this, '\"' + this.f0.getTypeName() + "\" is not a message type.", (a) null);
                    }
                } else if (v() == JavaType.ENUM) {
                    if (l2 instanceof c) {
                        this.o0 = (c) l2;
                    } else {
                        throw new DescriptorValidationException(this, '\"' + this.f0.getTypeName() + "\" is not an enum type.", (a) null);
                    }
                } else {
                    throw new DescriptorValidationException(this, "Field with primitive type has type_name.", (a) null);
                }
            } else if (v() == JavaType.MESSAGE || v() == JavaType.ENUM) {
                throw new DescriptorValidationException(this, "Field with message or enum type missing type_name.", (a) null);
            }
            if (this.f0.getOptions().getPacked() && !G()) {
                throw new DescriptorValidationException(this, "[packed = true] can only be specified for repeated primitive fields.", (a) null);
            }
            if (this.f0.hasDefaultValue()) {
                if (!i()) {
                    try {
                        switch (a.a[A().ordinal()]) {
                            case 1:
                            case 2:
                            case 3:
                                this.p0 = Integer.valueOf(TextFormat.j(this.f0.getDefaultValue()));
                                break;
                            case 4:
                            case 5:
                                this.p0 = Integer.valueOf(TextFormat.m(this.f0.getDefaultValue()));
                                break;
                            case 6:
                            case 7:
                            case 8:
                                this.p0 = Long.valueOf(TextFormat.k(this.f0.getDefaultValue()));
                                break;
                            case 9:
                            case 10:
                                this.p0 = Long.valueOf(TextFormat.n(this.f0.getDefaultValue()));
                                break;
                            case 11:
                                if (this.f0.getDefaultValue().equals("inf")) {
                                    this.p0 = Float.valueOf(Float.POSITIVE_INFINITY);
                                    break;
                                } else if (this.f0.getDefaultValue().equals("-inf")) {
                                    this.p0 = Float.valueOf(Float.NEGATIVE_INFINITY);
                                    break;
                                } else if (this.f0.getDefaultValue().equals("nan")) {
                                    this.p0 = Float.valueOf(Float.NaN);
                                    break;
                                } else {
                                    this.p0 = Float.valueOf(this.f0.getDefaultValue());
                                    break;
                                }
                            case 12:
                                if (this.f0.getDefaultValue().equals("inf")) {
                                    this.p0 = Double.valueOf(Double.POSITIVE_INFINITY);
                                    break;
                                } else if (this.f0.getDefaultValue().equals("-inf")) {
                                    this.p0 = Double.valueOf(Double.NEGATIVE_INFINITY);
                                    break;
                                } else if (this.f0.getDefaultValue().equals("nan")) {
                                    this.p0 = Double.valueOf(Double.NaN);
                                    break;
                                } else {
                                    this.p0 = Double.valueOf(this.f0.getDefaultValue());
                                    break;
                                }
                            case 13:
                                this.p0 = Boolean.valueOf(this.f0.getDefaultValue());
                                break;
                            case 14:
                                this.p0 = this.f0.getDefaultValue();
                                break;
                            case 15:
                                try {
                                    this.p0 = TextFormat.p(this.f0.getDefaultValue());
                                    break;
                                } catch (TextFormat.InvalidEscapeSequenceException e) {
                                    throw new DescriptorValidationException(this, "Couldn't parse default value: " + e.getMessage(), e, null);
                                }
                            case 16:
                                d g = this.o0.g(this.f0.getDefaultValue());
                                this.p0 = g;
                                if (g == null) {
                                    throw new DescriptorValidationException(this, "Unknown enum default value: \"" + this.f0.getDefaultValue() + '\"', (a) null);
                                }
                                break;
                            case 17:
                            case 18:
                                throw new DescriptorValidationException(this, "Message type had default value.", (a) null);
                        }
                    } catch (NumberFormatException e2) {
                        throw new DescriptorValidationException(this, "Could not parse default value: \"" + this.f0.getDefaultValue() + '\"', e2, null);
                    }
                } else {
                    throw new DescriptorValidationException(this, "Repeated fields cannot have default values.", (a) null);
                }
            } else if (i()) {
                this.p0 = Collections.emptyList();
            } else {
                int i = a.b[v().ordinal()];
                if (i == 1) {
                    this.p0 = this.o0.k().get(0);
                } else if (i != 2) {
                    this.p0 = v().defaultDefault;
                } else {
                    this.p0 = null;
                }
            }
            if (!C()) {
                this.h0.g.d(this);
            }
            b bVar = this.l0;
            if (bVar == null || !bVar.t().getMessageSetWireFormat()) {
                return;
            }
            if (C()) {
                if (!E() || A() != Type.MESSAGE) {
                    throw new DescriptorValidationException(this, "Extensions of MessageSets must be optional messages.", (a) null);
                }
                return;
            }
            throw new DescriptorValidationException(this, "MessageSets cannot have fields, only extensions.", (a) null);
        }

        @Override // com.google.protobuf.w.c
        public WireFormat.FieldType n() {
            return q0[this.k0.ordinal()];
        }

        public g o() {
            return this.n0;
        }

        public b p() {
            return this.l0;
        }

        public Object r() {
            if (v() != JavaType.MESSAGE) {
                return this.p0;
            }
            throw new UnsupportedOperationException("FieldDescriptor.getDefaultValue() called on an embedded message field.");
        }

        public c s() {
            if (v() == JavaType.ENUM) {
                return this.o0;
            }
            throw new UnsupportedOperationException(String.format("This field is not of enum type. (%s)", this.g0));
        }

        public b t() {
            if (C()) {
                return this.i0;
            }
            throw new UnsupportedOperationException(String.format("This field is not an extension. (%s)", this.g0));
        }

        public String toString() {
            return d();
        }

        public int u() {
            return this.a;
        }

        public JavaType v() {
            return this.k0.getJavaType();
        }

        public b x() {
            if (v() == JavaType.MESSAGE) {
                return this.m0;
            }
            throw new UnsupportedOperationException(String.format("This field is not of message type. (%s)", this.g0));
        }

        public DescriptorProtos.FieldOptions y() {
            return this.f0.getOptions();
        }

        @Override // com.google.protobuf.w.c
        public WireFormat.JavaType z() {
            return n().getJavaType();
        }

        public FieldDescriptor(DescriptorProtos.FieldDescriptorProto fieldDescriptorProto, FileDescriptor fileDescriptor, b bVar, int i, boolean z) throws DescriptorValidationException {
            super(null);
            this.a = i;
            this.f0 = fieldDescriptorProto;
            this.g0 = Descriptors.c(fileDescriptor, bVar, fieldDescriptorProto.getName());
            this.h0 = fileDescriptor;
            if (fieldDescriptorProto.hasJsonName()) {
                fieldDescriptorProto.getJsonName();
            } else {
                l(fieldDescriptorProto.getName());
            }
            if (fieldDescriptorProto.hasType()) {
                this.k0 = Type.valueOf(fieldDescriptorProto.getType());
            }
            this.j0 = fieldDescriptorProto.getProto3Optional();
            if (getNumber() > 0) {
                if (z) {
                    if (fieldDescriptorProto.hasExtendee()) {
                        this.l0 = null;
                        if (bVar != null) {
                            this.i0 = bVar;
                        } else {
                            this.i0 = null;
                        }
                        if (!fieldDescriptorProto.hasOneofIndex()) {
                            this.n0 = null;
                        } else {
                            throw new DescriptorValidationException(this, "FieldDescriptorProto.oneof_index set for extension field.", (a) null);
                        }
                    } else {
                        throw new DescriptorValidationException(this, "FieldDescriptorProto.extendee not set for extension field.", (a) null);
                    }
                } else if (!fieldDescriptorProto.hasExtendee()) {
                    this.l0 = bVar;
                    if (fieldDescriptorProto.hasOneofIndex()) {
                        if (fieldDescriptorProto.getOneofIndex() >= 0 && fieldDescriptorProto.getOneofIndex() < bVar.f().getOneofDeclCount()) {
                            g gVar = bVar.s().get(fieldDescriptorProto.getOneofIndex());
                            this.n0 = gVar;
                            g.k(gVar);
                        } else {
                            throw new DescriptorValidationException(this, "FieldDescriptorProto.oneof_index is out of range for type " + bVar.e(), (a) null);
                        }
                    } else {
                        this.n0 = null;
                    }
                    this.i0 = null;
                } else {
                    throw new DescriptorValidationException(this, "FieldDescriptorProto.extendee set for non-extension field.", (a) null);
                }
                fileDescriptor.g.f(this);
                return;
            }
            throw new DescriptorValidationException(this, "Field numbers must be positive integers.", (a) null);
        }
    }

    /* loaded from: classes2.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;
        public static final /* synthetic */ int[] b;

        static {
            int[] iArr = new int[FieldDescriptor.JavaType.values().length];
            b = iArr;
            try {
                iArr[FieldDescriptor.JavaType.ENUM.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                b[FieldDescriptor.JavaType.MESSAGE.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            int[] iArr2 = new int[FieldDescriptor.Type.values().length];
            a = iArr2;
            try {
                iArr2[FieldDescriptor.Type.INT32.ordinal()] = 1;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                a[FieldDescriptor.Type.SINT32.ordinal()] = 2;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                a[FieldDescriptor.Type.SFIXED32.ordinal()] = 3;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                a[FieldDescriptor.Type.UINT32.ordinal()] = 4;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                a[FieldDescriptor.Type.FIXED32.ordinal()] = 5;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                a[FieldDescriptor.Type.INT64.ordinal()] = 6;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                a[FieldDescriptor.Type.SINT64.ordinal()] = 7;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                a[FieldDescriptor.Type.SFIXED64.ordinal()] = 8;
            } catch (NoSuchFieldError unused10) {
            }
            try {
                a[FieldDescriptor.Type.UINT64.ordinal()] = 9;
            } catch (NoSuchFieldError unused11) {
            }
            try {
                a[FieldDescriptor.Type.FIXED64.ordinal()] = 10;
            } catch (NoSuchFieldError unused12) {
            }
            try {
                a[FieldDescriptor.Type.FLOAT.ordinal()] = 11;
            } catch (NoSuchFieldError unused13) {
            }
            try {
                a[FieldDescriptor.Type.DOUBLE.ordinal()] = 12;
            } catch (NoSuchFieldError unused14) {
            }
            try {
                a[FieldDescriptor.Type.BOOL.ordinal()] = 13;
            } catch (NoSuchFieldError unused15) {
            }
            try {
                a[FieldDescriptor.Type.STRING.ordinal()] = 14;
            } catch (NoSuchFieldError unused16) {
            }
            try {
                a[FieldDescriptor.Type.BYTES.ordinal()] = 15;
            } catch (NoSuchFieldError unused17) {
            }
            try {
                a[FieldDescriptor.Type.ENUM.ordinal()] = 16;
            } catch (NoSuchFieldError unused18) {
            }
            try {
                a[FieldDescriptor.Type.MESSAGE.ordinal()] = 17;
            } catch (NoSuchFieldError unused19) {
            }
            try {
                a[FieldDescriptor.Type.GROUP.ordinal()] = 18;
            } catch (NoSuchFieldError unused20) {
            }
        }
    }

    /* loaded from: classes2.dex */
    public static final class b extends e {
        public DescriptorProtos.DescriptorProto a;
        public final String b;
        public final FileDescriptor c;
        public final b[] d;
        public final c[] e;
        public final FieldDescriptor[] f;
        public final FieldDescriptor[] g;
        public final g[] h;

        public /* synthetic */ b(DescriptorProtos.DescriptorProto descriptorProto, FileDescriptor fileDescriptor, b bVar, int i, a aVar) throws DescriptorValidationException {
            this(descriptorProto, fileDescriptor, bVar, i);
        }

        @Override // com.google.protobuf.Descriptors.e
        public FileDescriptor a() {
            return this.c;
        }

        @Override // com.google.protobuf.Descriptors.e
        public String d() {
            return this.b;
        }

        @Override // com.google.protobuf.Descriptors.e
        public String e() {
            return this.a.getName();
        }

        public final void h() throws DescriptorValidationException {
            for (b bVar : this.d) {
                bVar.h();
            }
            for (FieldDescriptor fieldDescriptor : this.f) {
                fieldDescriptor.k();
            }
            for (FieldDescriptor fieldDescriptor2 : this.g) {
                fieldDescriptor2.k();
            }
        }

        public FieldDescriptor j(String str) {
            DescriptorPool descriptorPool = this.c.g;
            e g = descriptorPool.g(this.b + '.' + str);
            if (g == null || !(g instanceof FieldDescriptor)) {
                return null;
            }
            return (FieldDescriptor) g;
        }

        public FieldDescriptor k(int i) {
            return (FieldDescriptor) this.c.g.d.get(new DescriptorPool.a(this, i));
        }

        public List<c> l() {
            return Collections.unmodifiableList(Arrays.asList(this.e));
        }

        public List<FieldDescriptor> o() {
            return Collections.unmodifiableList(Arrays.asList(this.g));
        }

        public List<FieldDescriptor> p() {
            return Collections.unmodifiableList(Arrays.asList(this.f));
        }

        public List<b> r() {
            return Collections.unmodifiableList(Arrays.asList(this.d));
        }

        public List<g> s() {
            return Collections.unmodifiableList(Arrays.asList(this.h));
        }

        public DescriptorProtos.MessageOptions t() {
            return this.a.getOptions();
        }

        public boolean u() {
            return this.a.getExtensionRangeList().size() != 0;
        }

        public boolean v(int i) {
            for (DescriptorProtos.DescriptorProto.ExtensionRange extensionRange : this.a.getExtensionRangeList()) {
                if (extensionRange.getStart() <= i && i < extensionRange.getEnd()) {
                    return true;
                }
            }
            return false;
        }

        @Override // com.google.protobuf.Descriptors.e
        /* renamed from: x */
        public DescriptorProtos.DescriptorProto f() {
            return this.a;
        }

        public b(String str) throws DescriptorValidationException {
            super(null);
            String str2;
            String str3;
            int lastIndexOf = str.lastIndexOf(46);
            if (lastIndexOf != -1) {
                str3 = str.substring(lastIndexOf + 1);
                str2 = str.substring(0, lastIndexOf);
            } else {
                str2 = "";
                str3 = str;
            }
            this.a = DescriptorProtos.DescriptorProto.newBuilder().b0(str3).a(DescriptorProtos.DescriptorProto.ExtensionRange.newBuilder().A(1).v(536870912).build()).build();
            this.b = str;
            this.d = new b[0];
            this.e = new c[0];
            this.f = new FieldDescriptor[0];
            this.g = new FieldDescriptor[0];
            this.h = new g[0];
            this.c = new FileDescriptor(str2, this);
        }

        public b(DescriptorProtos.DescriptorProto descriptorProto, FileDescriptor fileDescriptor, b bVar, int i) throws DescriptorValidationException {
            super(null);
            this.a = descriptorProto;
            this.b = Descriptors.c(fileDescriptor, bVar, descriptorProto.getName());
            this.c = fileDescriptor;
            this.h = new g[descriptorProto.getOneofDeclCount()];
            for (int i2 = 0; i2 < descriptorProto.getOneofDeclCount(); i2++) {
                this.h[i2] = new g(descriptorProto.getOneofDecl(i2), fileDescriptor, this, i2, null);
            }
            this.d = new b[descriptorProto.getNestedTypeCount()];
            for (int i3 = 0; i3 < descriptorProto.getNestedTypeCount(); i3++) {
                this.d[i3] = new b(descriptorProto.getNestedType(i3), fileDescriptor, this, i3);
            }
            this.e = new c[descriptorProto.getEnumTypeCount()];
            for (int i4 = 0; i4 < descriptorProto.getEnumTypeCount(); i4++) {
                this.e[i4] = new c(descriptorProto.getEnumType(i4), fileDescriptor, this, i4, null);
            }
            this.f = new FieldDescriptor[descriptorProto.getFieldCount()];
            for (int i5 = 0; i5 < descriptorProto.getFieldCount(); i5++) {
                this.f[i5] = new FieldDescriptor(descriptorProto.getField(i5), fileDescriptor, this, i5, false, null);
            }
            this.g = new FieldDescriptor[descriptorProto.getExtensionCount()];
            for (int i6 = 0; i6 < descriptorProto.getExtensionCount(); i6++) {
                this.g[i6] = new FieldDescriptor(descriptorProto.getExtension(i6), fileDescriptor, this, i6, true, null);
            }
            for (int i7 = 0; i7 < descriptorProto.getOneofDeclCount(); i7++) {
                g[] gVarArr = this.h;
                gVarArr[i7].g = new FieldDescriptor[gVarArr[i7].o()];
                this.h[i7].f = 0;
            }
            for (int i8 = 0; i8 < descriptorProto.getFieldCount(); i8++) {
                g o = this.f[i8].o();
                if (o != null) {
                    o.g[g.k(o)] = this.f[i8];
                }
            }
            int i9 = 0;
            for (g gVar : this.h) {
                if (gVar.s()) {
                    i9++;
                } else if (i9 > 0) {
                    throw new DescriptorValidationException(this, "Synthetic oneofs must come last.", (a) null);
                }
            }
            int length = this.h.length;
            fileDescriptor.g.f(this);
        }
    }

    /* loaded from: classes2.dex */
    public static final class c extends e implements a0.d<d> {
        public DescriptorProtos.EnumDescriptorProto a;
        public final String f0;
        public final FileDescriptor g0;
        public d[] h0;
        public final WeakHashMap<Integer, WeakReference<d>> i0;

        public /* synthetic */ c(DescriptorProtos.EnumDescriptorProto enumDescriptorProto, FileDescriptor fileDescriptor, b bVar, int i, a aVar) throws DescriptorValidationException {
            this(enumDescriptorProto, fileDescriptor, bVar, i);
        }

        @Override // com.google.protobuf.Descriptors.e
        public FileDescriptor a() {
            return this.g0;
        }

        @Override // com.google.protobuf.Descriptors.e
        public String d() {
            return this.f0;
        }

        @Override // com.google.protobuf.Descriptors.e
        public String e() {
            return this.a.getName();
        }

        public d g(String str) {
            DescriptorPool descriptorPool = this.g0.g;
            e g = descriptorPool.g(this.f0 + '.' + str);
            if (g == null || !(g instanceof d)) {
                return null;
            }
            return (d) g;
        }

        @Override // com.google.protobuf.a0.d
        /* renamed from: h */
        public d findValueByNumber(int i) {
            return (d) this.g0.g.e.get(new DescriptorPool.a(this, i));
        }

        public d j(int i) {
            d findValueByNumber = findValueByNumber(i);
            if (findValueByNumber != null) {
                return findValueByNumber;
            }
            synchronized (this) {
                Integer num = new Integer(i);
                WeakReference<d> weakReference = this.i0.get(num);
                if (weakReference != null) {
                    findValueByNumber = weakReference.get();
                }
                if (findValueByNumber == null) {
                    findValueByNumber = new d(this.g0, this, num, (a) null);
                    this.i0.put(num, new WeakReference<>(findValueByNumber));
                }
            }
            return findValueByNumber;
        }

        public List<d> k() {
            return Collections.unmodifiableList(Arrays.asList(this.h0));
        }

        @Override // com.google.protobuf.Descriptors.e
        /* renamed from: l */
        public DescriptorProtos.EnumDescriptorProto f() {
            return this.a;
        }

        public c(DescriptorProtos.EnumDescriptorProto enumDescriptorProto, FileDescriptor fileDescriptor, b bVar, int i) throws DescriptorValidationException {
            super(null);
            this.i0 = new WeakHashMap<>();
            this.a = enumDescriptorProto;
            this.f0 = Descriptors.c(fileDescriptor, bVar, enumDescriptorProto.getName());
            this.g0 = fileDescriptor;
            if (enumDescriptorProto.getValueCount() != 0) {
                this.h0 = new d[enumDescriptorProto.getValueCount()];
                for (int i2 = 0; i2 < enumDescriptorProto.getValueCount(); i2++) {
                    this.h0[i2] = new d(enumDescriptorProto.getValue(i2), fileDescriptor, this, i2, null);
                }
                fileDescriptor.g.f(this);
                return;
            }
            throw new DescriptorValidationException(this, "Enums must contain at least one value.", (a) null);
        }
    }

    /* loaded from: classes2.dex */
    public static final class d extends e implements a0.c {
        public final int a;
        public DescriptorProtos.EnumValueDescriptorProto f0;
        public final String g0;
        public final FileDescriptor h0;
        public final c i0;

        public /* synthetic */ d(DescriptorProtos.EnumValueDescriptorProto enumValueDescriptorProto, FileDescriptor fileDescriptor, c cVar, int i, a aVar) throws DescriptorValidationException {
            this(enumValueDescriptorProto, fileDescriptor, cVar, i);
        }

        @Override // com.google.protobuf.Descriptors.e
        public FileDescriptor a() {
            return this.h0;
        }

        @Override // com.google.protobuf.Descriptors.e
        public String d() {
            return this.g0;
        }

        @Override // com.google.protobuf.Descriptors.e
        public String e() {
            return this.f0.getName();
        }

        public int g() {
            return this.a;
        }

        @Override // com.google.protobuf.a0.c
        public int getNumber() {
            return this.f0.getNumber();
        }

        public c h() {
            return this.i0;
        }

        @Override // com.google.protobuf.Descriptors.e
        /* renamed from: j */
        public DescriptorProtos.EnumValueDescriptorProto f() {
            return this.f0;
        }

        public String toString() {
            return this.f0.getName();
        }

        public /* synthetic */ d(FileDescriptor fileDescriptor, c cVar, Integer num, a aVar) {
            this(fileDescriptor, cVar, num);
        }

        public d(DescriptorProtos.EnumValueDescriptorProto enumValueDescriptorProto, FileDescriptor fileDescriptor, c cVar, int i) throws DescriptorValidationException {
            super(null);
            this.a = i;
            this.f0 = enumValueDescriptorProto;
            this.h0 = fileDescriptor;
            this.i0 = cVar;
            this.g0 = cVar.d() + '.' + enumValueDescriptorProto.getName();
            fileDescriptor.g.f(this);
            fileDescriptor.g.c(this);
        }

        public d(FileDescriptor fileDescriptor, c cVar, Integer num) {
            super(null);
            DescriptorProtos.EnumValueDescriptorProto build = DescriptorProtos.EnumValueDescriptorProto.newBuilder().x("UNKNOWN_ENUM_VALUE_" + cVar.e() + "_" + num).y(num.intValue()).build();
            this.a = -1;
            this.f0 = build;
            this.h0 = fileDescriptor;
            this.i0 = cVar;
            this.g0 = cVar.d() + '.' + build.getName();
        }
    }

    /* loaded from: classes2.dex */
    public static abstract class e {
        public /* synthetic */ e(a aVar) {
            this();
        }

        public abstract FileDescriptor a();

        public abstract String d();

        public abstract String e();

        public abstract l0 f();

        public e() {
        }
    }

    /* loaded from: classes2.dex */
    public static final class f extends e {
        public DescriptorProtos.MethodDescriptorProto a;
        public final String b;
        public final FileDescriptor c;

        public /* synthetic */ f(DescriptorProtos.MethodDescriptorProto methodDescriptorProto, FileDescriptor fileDescriptor, h hVar, int i, a aVar) throws DescriptorValidationException {
            this(methodDescriptorProto, fileDescriptor, hVar, i);
        }

        @Override // com.google.protobuf.Descriptors.e
        public FileDescriptor a() {
            return this.c;
        }

        @Override // com.google.protobuf.Descriptors.e
        public String d() {
            return this.b;
        }

        @Override // com.google.protobuf.Descriptors.e
        public String e() {
            return this.a.getName();
        }

        public final void h() throws DescriptorValidationException {
            DescriptorPool descriptorPool = this.c.g;
            String inputType = this.a.getInputType();
            DescriptorPool.SearchFilter searchFilter = DescriptorPool.SearchFilter.TYPES_ONLY;
            e l = descriptorPool.l(inputType, this, searchFilter);
            if (l instanceof b) {
                b bVar = (b) l;
                e l2 = this.c.g.l(this.a.getOutputType(), this, searchFilter);
                if (l2 instanceof b) {
                    b bVar2 = (b) l2;
                    return;
                }
                throw new DescriptorValidationException(this, '\"' + this.a.getOutputType() + "\" is not a message type.", (a) null);
            }
            throw new DescriptorValidationException(this, '\"' + this.a.getInputType() + "\" is not a message type.", (a) null);
        }

        @Override // com.google.protobuf.Descriptors.e
        /* renamed from: j */
        public DescriptorProtos.MethodDescriptorProto f() {
            return this.a;
        }

        public f(DescriptorProtos.MethodDescriptorProto methodDescriptorProto, FileDescriptor fileDescriptor, h hVar, int i) throws DescriptorValidationException {
            super(null);
            this.a = methodDescriptorProto;
            this.c = fileDescriptor;
            this.b = hVar.d() + '.' + methodDescriptorProto.getName();
            fileDescriptor.g.f(this);
        }
    }

    /* loaded from: classes2.dex */
    public static final class g extends e {
        public final int a;
        public DescriptorProtos.OneofDescriptorProto b;
        public final String c;
        public final FileDescriptor d;
        public b e;
        public int f;
        public FieldDescriptor[] g;

        public /* synthetic */ g(DescriptorProtos.OneofDescriptorProto oneofDescriptorProto, FileDescriptor fileDescriptor, b bVar, int i, a aVar) throws DescriptorValidationException {
            this(oneofDescriptorProto, fileDescriptor, bVar, i);
        }

        public static /* synthetic */ int k(g gVar) {
            int i = gVar.f;
            gVar.f = i + 1;
            return i;
        }

        @Override // com.google.protobuf.Descriptors.e
        public FileDescriptor a() {
            return this.d;
        }

        @Override // com.google.protobuf.Descriptors.e
        public String d() {
            return this.c;
        }

        @Override // com.google.protobuf.Descriptors.e
        public String e() {
            return this.b.getName();
        }

        public b l() {
            return this.e;
        }

        public int o() {
            return this.f;
        }

        public List<FieldDescriptor> p() {
            return Collections.unmodifiableList(Arrays.asList(this.g));
        }

        public int r() {
            return this.a;
        }

        public boolean s() {
            FieldDescriptor[] fieldDescriptorArr = this.g;
            return fieldDescriptorArr.length == 1 && fieldDescriptorArr[0].j0;
        }

        @Override // com.google.protobuf.Descriptors.e
        /* renamed from: t */
        public DescriptorProtos.OneofDescriptorProto f() {
            return this.b;
        }

        public g(DescriptorProtos.OneofDescriptorProto oneofDescriptorProto, FileDescriptor fileDescriptor, b bVar, int i) throws DescriptorValidationException {
            super(null);
            this.b = oneofDescriptorProto;
            this.c = Descriptors.c(fileDescriptor, bVar, oneofDescriptorProto.getName());
            this.d = fileDescriptor;
            this.a = i;
            this.e = bVar;
            this.f = 0;
        }
    }

    /* loaded from: classes2.dex */
    public static final class h extends e {
        public DescriptorProtos.ServiceDescriptorProto a;
        public final String b;
        public final FileDescriptor c;
        public f[] d;

        public /* synthetic */ h(DescriptorProtos.ServiceDescriptorProto serviceDescriptorProto, FileDescriptor fileDescriptor, int i, a aVar) throws DescriptorValidationException {
            this(serviceDescriptorProto, fileDescriptor, i);
        }

        @Override // com.google.protobuf.Descriptors.e
        public FileDescriptor a() {
            return this.c;
        }

        @Override // com.google.protobuf.Descriptors.e
        public String d() {
            return this.b;
        }

        @Override // com.google.protobuf.Descriptors.e
        public String e() {
            return this.a.getName();
        }

        public final void h() throws DescriptorValidationException {
            for (f fVar : this.d) {
                fVar.h();
            }
        }

        @Override // com.google.protobuf.Descriptors.e
        /* renamed from: j */
        public DescriptorProtos.ServiceDescriptorProto f() {
            return this.a;
        }

        public h(DescriptorProtos.ServiceDescriptorProto serviceDescriptorProto, FileDescriptor fileDescriptor, int i) throws DescriptorValidationException {
            super(null);
            this.a = serviceDescriptorProto;
            this.b = Descriptors.c(fileDescriptor, null, serviceDescriptorProto.getName());
            this.c = fileDescriptor;
            this.d = new f[serviceDescriptorProto.getMethodCount()];
            for (int i2 = 0; i2 < serviceDescriptorProto.getMethodCount(); i2++) {
                this.d[i2] = new f(serviceDescriptorProto.getMethod(i2), fileDescriptor, this, i2, null);
            }
            fileDescriptor.g.f(this);
        }
    }

    public static String c(FileDescriptor fileDescriptor, b bVar, String str) {
        if (bVar != null) {
            return bVar.d() + '.' + str;
        }
        String r = fileDescriptor.r();
        if (r.isEmpty()) {
            return str;
        }
        return r + '.' + str;
    }

    /* loaded from: classes2.dex */
    public static final class FileDescriptor extends e {
        public DescriptorProtos.FileDescriptorProto a;
        public final b[] b;
        public final c[] c;
        public final h[] d;
        public final FieldDescriptor[] e;
        public final FileDescriptor[] f;
        public final DescriptorPool g;

        /* loaded from: classes2.dex */
        public enum Syntax {
            UNKNOWN("unknown"),
            PROTO2("proto2"),
            PROTO3("proto3");
            
            private final String name;

            Syntax(String str) {
                this.name = str;
            }
        }

        public FileDescriptor(DescriptorProtos.FileDescriptorProto fileDescriptorProto, FileDescriptor[] fileDescriptorArr, DescriptorPool descriptorPool, boolean z) throws DescriptorValidationException {
            super(null);
            this.g = descriptorPool;
            this.a = fileDescriptorProto;
            fileDescriptorArr.clone();
            HashMap hashMap = new HashMap();
            for (FileDescriptor fileDescriptor : fileDescriptorArr) {
                hashMap.put(fileDescriptor.e(), fileDescriptor);
            }
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < fileDescriptorProto.getPublicDependencyCount(); i++) {
                int publicDependency = fileDescriptorProto.getPublicDependency(i);
                if (publicDependency >= 0 && publicDependency < fileDescriptorProto.getDependencyCount()) {
                    String dependency = fileDescriptorProto.getDependency(publicDependency);
                    FileDescriptor fileDescriptor2 = (FileDescriptor) hashMap.get(dependency);
                    if (fileDescriptor2 != null) {
                        arrayList.add(fileDescriptor2);
                    } else if (!z) {
                        throw new DescriptorValidationException(this, "Invalid public dependency: " + dependency, (a) null);
                    }
                } else {
                    throw new DescriptorValidationException(this, "Invalid public dependency index.", (a) null);
                }
            }
            FileDescriptor[] fileDescriptorArr2 = new FileDescriptor[arrayList.size()];
            this.f = fileDescriptorArr2;
            arrayList.toArray(fileDescriptorArr2);
            descriptorPool.e(r(), this);
            this.b = new b[fileDescriptorProto.getMessageTypeCount()];
            for (int i2 = 0; i2 < fileDescriptorProto.getMessageTypeCount(); i2++) {
                this.b[i2] = new b(fileDescriptorProto.getMessageType(i2), this, null, i2, null);
            }
            this.c = new c[fileDescriptorProto.getEnumTypeCount()];
            for (int i3 = 0; i3 < fileDescriptorProto.getEnumTypeCount(); i3++) {
                this.c[i3] = new c(fileDescriptorProto.getEnumType(i3), this, null, i3, null);
            }
            this.d = new h[fileDescriptorProto.getServiceCount()];
            for (int i4 = 0; i4 < fileDescriptorProto.getServiceCount(); i4++) {
                this.d[i4] = new h(fileDescriptorProto.getService(i4), this, i4, null);
            }
            this.e = new FieldDescriptor[fileDescriptorProto.getExtensionCount()];
            for (int i5 = 0; i5 < fileDescriptorProto.getExtensionCount(); i5++) {
                this.e[i5] = new FieldDescriptor(fileDescriptorProto.getExtension(i5), this, null, i5, true, null);
            }
        }

        public static FileDescriptor h(DescriptorProtos.FileDescriptorProto fileDescriptorProto, FileDescriptor[] fileDescriptorArr, boolean z) throws DescriptorValidationException {
            FileDescriptor fileDescriptor = new FileDescriptor(fileDescriptorProto, fileDescriptorArr, new DescriptorPool(fileDescriptorArr, z), z);
            fileDescriptor.j();
            return fileDescriptor;
        }

        public static FileDescriptor u(String[] strArr, FileDescriptor[] fileDescriptorArr) {
            DescriptorProtos.FileDescriptorProto fileDescriptorProto;
            try {
                try {
                    return h(DescriptorProtos.FileDescriptorProto.parseFrom(v(strArr)), fileDescriptorArr, true);
                } catch (DescriptorValidationException e) {
                    throw new IllegalArgumentException("Invalid embedded descriptor for \"" + fileDescriptorProto.getName() + "\".", e);
                }
            } catch (InvalidProtocolBufferException e2) {
                throw new IllegalArgumentException("Failed to parse protocol buffer descriptor for generated code.", e2);
            }
        }

        public static byte[] v(String[] strArr) {
            if (strArr.length == 1) {
                return strArr[0].getBytes(a0.b);
            }
            StringBuilder sb = new StringBuilder();
            for (String str : strArr) {
                sb.append(str);
            }
            return sb.toString().getBytes(a0.b);
        }

        @Override // com.google.protobuf.Descriptors.e
        public FileDescriptor a() {
            return this;
        }

        @Override // com.google.protobuf.Descriptors.e
        public String d() {
            return this.a.getName();
        }

        @Override // com.google.protobuf.Descriptors.e
        public String e() {
            return this.a.getName();
        }

        public final void j() throws DescriptorValidationException {
            for (b bVar : this.b) {
                bVar.h();
            }
            for (h hVar : this.d) {
                hVar.h();
            }
            for (FieldDescriptor fieldDescriptor : this.e) {
                fieldDescriptor.k();
            }
        }

        public FieldDescriptor k(String str) {
            if (str.indexOf(46) != -1) {
                return null;
            }
            String r = r();
            if (!r.isEmpty()) {
                str = r + '.' + str;
            }
            e g = this.g.g(str);
            if (g != null && (g instanceof FieldDescriptor) && g.a() == this) {
                return (FieldDescriptor) g;
            }
            return null;
        }

        public List<c> l() {
            return Collections.unmodifiableList(Arrays.asList(this.c));
        }

        public List<b> o() {
            return Collections.unmodifiableList(Arrays.asList(this.b));
        }

        public DescriptorProtos.FileOptions p() {
            return this.a.getOptions();
        }

        public String r() {
            return this.a.getPackage();
        }

        public List<FileDescriptor> s() {
            return Collections.unmodifiableList(Arrays.asList(this.f));
        }

        public Syntax t() {
            Syntax syntax = Syntax.PROTO3;
            return syntax.name.equals(this.a.getSyntax()) ? syntax : Syntax.PROTO2;
        }

        public boolean x() {
            return t() == Syntax.PROTO3;
        }

        @Override // com.google.protobuf.Descriptors.e
        /* renamed from: y */
        public DescriptorProtos.FileDescriptorProto f() {
            return this.a;
        }

        public FileDescriptor(String str, b bVar) throws DescriptorValidationException {
            super(null);
            DescriptorPool descriptorPool = new DescriptorPool(new FileDescriptor[0], true);
            this.g = descriptorPool;
            DescriptorProtos.FileDescriptorProto.b newBuilder = DescriptorProtos.FileDescriptorProto.newBuilder();
            this.a = newBuilder.W(bVar.d() + ".placeholder.proto").X(str).a(bVar.f()).build();
            this.f = new FileDescriptor[0];
            this.b = new b[]{bVar};
            this.c = new c[0];
            this.d = new h[0];
            this.e = new FieldDescriptor[0];
            descriptorPool.e(str, this);
            descriptorPool.f(bVar);
        }
    }
}
