package com.google.protobuf;

import com.google.protobuf.a0;
import java.util.AbstractList;
import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

/* compiled from: BooleanArrayList.java */
/* loaded from: classes2.dex */
public final class g extends d<Boolean> implements a0.a, RandomAccess, uu2 {
    public static final g h0;
    public boolean[] f0;
    public int g0;

    static {
        g gVar = new g(new boolean[0], 0);
        h0 = gVar;
        gVar.l();
    }

    public g() {
        this(new boolean[10], 0);
    }

    public static g o() {
        return h0;
    }

    @Override // com.google.protobuf.d, java.util.AbstractCollection, java.util.Collection, java.util.List
    public boolean addAll(Collection<? extends Boolean> collection) {
        e();
        a0.a(collection);
        if (!(collection instanceof g)) {
            return super.addAll(collection);
        }
        g gVar = (g) collection;
        int i = gVar.g0;
        if (i == 0) {
            return false;
        }
        int i2 = this.g0;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            boolean[] zArr = this.f0;
            if (i3 > zArr.length) {
                this.f0 = Arrays.copyOf(zArr, i3);
            }
            System.arraycopy(gVar.f0, 0, this.f0, this.g0, gVar.g0);
            this.g0 = i3;
            ((AbstractList) this).modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public boolean contains(Object obj) {
        return indexOf(obj) != -1;
    }

    @Override // com.google.protobuf.d, java.util.AbstractList, java.util.Collection, java.util.List
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof g)) {
            return super.equals(obj);
        }
        g gVar = (g) obj;
        if (this.g0 != gVar.g0) {
            return false;
        }
        boolean[] zArr = gVar.f0;
        for (int i = 0; i < this.g0; i++) {
            if (this.f0[i] != zArr[i]) {
                return false;
            }
        }
        return true;
    }

    @Override // com.google.protobuf.d, java.util.AbstractList, java.util.Collection, java.util.List
    public int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.g0; i2++) {
            i = (i * 31) + a0.c(this.f0[i2]);
        }
        return i;
    }

    @Override // java.util.AbstractList, java.util.List
    /* renamed from: i */
    public void add(int i, Boolean bool) {
        m(i, bool.booleanValue());
    }

    @Override // java.util.AbstractList, java.util.List
    public int indexOf(Object obj) {
        if (obj instanceof Boolean) {
            boolean booleanValue = ((Boolean) obj).booleanValue();
            int size = size();
            for (int i = 0; i < size; i++) {
                if (this.f0[i] == booleanValue) {
                    return i;
                }
            }
            return -1;
        }
        return -1;
    }

    @Override // com.google.protobuf.d, java.util.AbstractList, java.util.AbstractCollection, java.util.Collection, java.util.List
    /* renamed from: k */
    public boolean add(Boolean bool) {
        n(bool.booleanValue());
        return true;
    }

    public final void m(int i, boolean z) {
        int i2;
        e();
        if (i >= 0 && i <= (i2 = this.g0)) {
            boolean[] zArr = this.f0;
            if (i2 < zArr.length) {
                System.arraycopy(zArr, i, zArr, i + 1, i2 - i);
            } else {
                boolean[] zArr2 = new boolean[((i2 * 3) / 2) + 1];
                System.arraycopy(zArr, 0, zArr2, 0, i);
                System.arraycopy(this.f0, i, zArr2, i + 1, this.g0 - i);
                this.f0 = zArr2;
            }
            this.f0[i] = z;
            this.g0++;
            ((AbstractList) this).modCount++;
            return;
        }
        throw new IndexOutOfBoundsException(t(i));
    }

    public void n(boolean z) {
        e();
        int i = this.g0;
        boolean[] zArr = this.f0;
        if (i == zArr.length) {
            boolean[] zArr2 = new boolean[((i * 3) / 2) + 1];
            System.arraycopy(zArr, 0, zArr2, 0, i);
            this.f0 = zArr2;
        }
        boolean[] zArr3 = this.f0;
        int i2 = this.g0;
        this.g0 = i2 + 1;
        zArr3[i2] = z;
    }

    public final void p(int i) {
        if (i < 0 || i >= this.g0) {
            throw new IndexOutOfBoundsException(t(i));
        }
    }

    @Override // java.util.AbstractList, java.util.List
    /* renamed from: q */
    public Boolean get(int i) {
        return Boolean.valueOf(s(i));
    }

    @Override // java.util.AbstractList
    public void removeRange(int i, int i2) {
        e();
        if (i2 >= i) {
            boolean[] zArr = this.f0;
            System.arraycopy(zArr, i2, zArr, i, this.g0 - i2);
            this.g0 -= i2 - i;
            ((AbstractList) this).modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    public boolean s(int i) {
        p(i);
        return this.f0[i];
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public int size() {
        return this.g0;
    }

    public final String t(int i) {
        return "Index:" + i + ", Size:" + this.g0;
    }

    @Override // java.util.AbstractList, java.util.List
    /* renamed from: w */
    public Boolean remove(int i) {
        int i2;
        e();
        p(i);
        boolean[] zArr = this.f0;
        boolean z = zArr[i];
        if (i < this.g0 - 1) {
            System.arraycopy(zArr, i + 1, zArr, i, (i2 - i) - 1);
        }
        this.g0--;
        ((AbstractList) this).modCount++;
        return Boolean.valueOf(z);
    }

    @Override // java.util.AbstractList, java.util.List
    /* renamed from: y */
    public Boolean set(int i, Boolean bool) {
        return Boolean.valueOf(z(i, bool.booleanValue()));
    }

    public boolean z(int i, boolean z) {
        e();
        p(i);
        boolean[] zArr = this.f0;
        boolean z2 = zArr[i];
        zArr[i] = z;
        return z2;
    }

    public g(boolean[] zArr, int i) {
        this.f0 = zArr;
        this.g0 = i;
    }

    @Override // com.google.protobuf.a0.i
    /* renamed from: a */
    public a0.i<Boolean> a2(int i) {
        if (i >= this.g0) {
            return new g(Arrays.copyOf(this.f0, i), this.g0);
        }
        throw new IllegalArgumentException();
    }

    @Override // com.google.protobuf.d, java.util.AbstractCollection, java.util.Collection, java.util.List
    public boolean remove(Object obj) {
        e();
        for (int i = 0; i < this.g0; i++) {
            if (obj.equals(Boolean.valueOf(this.f0[i]))) {
                boolean[] zArr = this.f0;
                System.arraycopy(zArr, i + 1, zArr, i, (this.g0 - i) - 1);
                this.g0--;
                ((AbstractList) this).modCount++;
                return true;
            }
        }
        return false;
    }
}
