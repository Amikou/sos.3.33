package com.google.protobuf;

import com.google.protobuf.Descriptors;
import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.WireFormat;
import com.google.protobuf.q;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/* compiled from: ExtensionSchemaFull.java */
/* loaded from: classes2.dex */
public final class t extends s<Descriptors.FieldDescriptor> {
    public static final long a = k();
    public static final /* synthetic */ int b = 0;

    /* compiled from: ExtensionSchemaFull.java */
    /* loaded from: classes2.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[WireFormat.FieldType.values().length];
            a = iArr;
            try {
                iArr[WireFormat.FieldType.DOUBLE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[WireFormat.FieldType.FLOAT.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[WireFormat.FieldType.INT64.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                a[WireFormat.FieldType.UINT64.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                a[WireFormat.FieldType.INT32.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                a[WireFormat.FieldType.FIXED64.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                a[WireFormat.FieldType.FIXED32.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                a[WireFormat.FieldType.BOOL.ordinal()] = 8;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                a[WireFormat.FieldType.UINT32.ordinal()] = 9;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                a[WireFormat.FieldType.SFIXED32.ordinal()] = 10;
            } catch (NoSuchFieldError unused10) {
            }
            try {
                a[WireFormat.FieldType.SFIXED64.ordinal()] = 11;
            } catch (NoSuchFieldError unused11) {
            }
            try {
                a[WireFormat.FieldType.SINT32.ordinal()] = 12;
            } catch (NoSuchFieldError unused12) {
            }
            try {
                a[WireFormat.FieldType.SINT64.ordinal()] = 13;
            } catch (NoSuchFieldError unused13) {
            }
            try {
                a[WireFormat.FieldType.ENUM.ordinal()] = 14;
            } catch (NoSuchFieldError unused14) {
            }
            try {
                a[WireFormat.FieldType.BYTES.ordinal()] = 15;
            } catch (NoSuchFieldError unused15) {
            }
            try {
                a[WireFormat.FieldType.STRING.ordinal()] = 16;
            } catch (NoSuchFieldError unused16) {
            }
            try {
                a[WireFormat.FieldType.GROUP.ordinal()] = 17;
            } catch (NoSuchFieldError unused17) {
            }
            try {
                a[WireFormat.FieldType.MESSAGE.ordinal()] = 18;
            } catch (NoSuchFieldError unused18) {
            }
        }
    }

    public static <T> long k() {
        return k1.J(GeneratedMessageV3.ExtendableMessage.class.getDeclaredField("extensions"));
    }

    @Override // com.google.protobuf.s
    public int a(Map.Entry<?, ?> entry) {
        return ((Descriptors.FieldDescriptor) entry.getKey()).getNumber();
    }

    @Override // com.google.protobuf.s
    public Object b(r rVar, m0 m0Var, int i) {
        return ((q) rVar).d(((l0) m0Var).getDescriptorForType(), i);
    }

    @Override // com.google.protobuf.s
    public w<Descriptors.FieldDescriptor> c(Object obj) {
        return (w) k1.E(obj, a);
    }

    @Override // com.google.protobuf.s
    public w<Descriptors.FieldDescriptor> d(Object obj) {
        w<Descriptors.FieldDescriptor> c = c(obj);
        if (c.C()) {
            w<Descriptors.FieldDescriptor> clone = c.clone();
            l(obj, clone);
            return clone;
        }
        return c;
    }

    @Override // com.google.protobuf.s
    public boolean e(m0 m0Var) {
        return m0Var instanceof GeneratedMessageV3.ExtendableMessage;
    }

    @Override // com.google.protobuf.s
    public void f(Object obj) {
        c(obj).H();
    }

    @Override // com.google.protobuf.s
    public <UT, UB> UB g(w0 w0Var, Object obj, r rVar, w<Descriptors.FieldDescriptor> wVar, UB ub, f1<UT, UB> f1Var) throws IOException {
        Object t;
        ArrayList arrayList;
        ArrayList arrayList2;
        q.b bVar = (q.b) obj;
        int number = bVar.a.getNumber();
        if (bVar.a.i() && bVar.a.isPacked()) {
            switch (a.a[bVar.a.n().ordinal()]) {
                case 1:
                    arrayList = new ArrayList();
                    w0Var.F(arrayList);
                    arrayList2 = arrayList;
                    break;
                case 2:
                    arrayList = new ArrayList();
                    w0Var.A(arrayList);
                    arrayList2 = arrayList;
                    break;
                case 3:
                    arrayList = new ArrayList();
                    w0Var.m(arrayList);
                    arrayList2 = arrayList;
                    break;
                case 4:
                    arrayList = new ArrayList();
                    w0Var.k(arrayList);
                    arrayList2 = arrayList;
                    break;
                case 5:
                    arrayList = new ArrayList();
                    w0Var.o(arrayList);
                    arrayList2 = arrayList;
                    break;
                case 6:
                    arrayList = new ArrayList();
                    w0Var.I(arrayList);
                    arrayList2 = arrayList;
                    break;
                case 7:
                    arrayList = new ArrayList();
                    w0Var.r(arrayList);
                    arrayList2 = arrayList;
                    break;
                case 8:
                    arrayList = new ArrayList();
                    w0Var.u(arrayList);
                    arrayList2 = arrayList;
                    break;
                case 9:
                    arrayList = new ArrayList();
                    w0Var.g(arrayList);
                    arrayList2 = arrayList;
                    break;
                case 10:
                    arrayList = new ArrayList();
                    w0Var.e(arrayList);
                    arrayList2 = arrayList;
                    break;
                case 11:
                    arrayList = new ArrayList();
                    w0Var.n(arrayList);
                    arrayList2 = arrayList;
                    break;
                case 12:
                    arrayList = new ArrayList();
                    w0Var.b(arrayList);
                    arrayList2 = arrayList;
                    break;
                case 13:
                    arrayList = new ArrayList();
                    w0Var.f(arrayList);
                    arrayList2 = arrayList;
                    break;
                case 14:
                    ArrayList<Integer> arrayList3 = new ArrayList();
                    w0Var.p(arrayList3);
                    arrayList2 = new ArrayList();
                    for (Integer num : arrayList3) {
                        int intValue = num.intValue();
                        Descriptors.d findValueByNumber = bVar.a.s().findValueByNumber(intValue);
                        if (findValueByNumber != null) {
                            arrayList2.add(findValueByNumber);
                        } else {
                            ub = (UB) z0.M(number, intValue, ub, f1Var);
                        }
                    }
                    break;
                default:
                    throw new IllegalStateException("Type cannot be packed: " + bVar.a.n());
            }
            wVar.N(bVar.a, arrayList2);
        } else {
            Object obj2 = null;
            if (bVar.a.n() == WireFormat.FieldType.ENUM) {
                int B = w0Var.B();
                obj2 = bVar.a.s().findValueByNumber(B);
                if (obj2 == null) {
                    return (UB) z0.M(number, B, ub, f1Var);
                }
            } else {
                switch (a.a[bVar.a.n().ordinal()]) {
                    case 1:
                        obj2 = Double.valueOf(w0Var.readDouble());
                        break;
                    case 2:
                        obj2 = Float.valueOf(w0Var.readFloat());
                        break;
                    case 3:
                        obj2 = Long.valueOf(w0Var.G());
                        break;
                    case 4:
                        obj2 = Long.valueOf(w0Var.c());
                        break;
                    case 5:
                        obj2 = Integer.valueOf(w0Var.B());
                        break;
                    case 6:
                        obj2 = Long.valueOf(w0Var.d());
                        break;
                    case 7:
                        obj2 = Integer.valueOf(w0Var.h());
                        break;
                    case 8:
                        obj2 = Boolean.valueOf(w0Var.i());
                        break;
                    case 9:
                        obj2 = Integer.valueOf(w0Var.l());
                        break;
                    case 10:
                        obj2 = Integer.valueOf(w0Var.D());
                        break;
                    case 11:
                        obj2 = Long.valueOf(w0Var.j());
                        break;
                    case 12:
                        obj2 = Integer.valueOf(w0Var.s());
                        break;
                    case 13:
                        obj2 = Long.valueOf(w0Var.t());
                        break;
                    case 14:
                        throw new IllegalStateException("Shouldn't reach here.");
                    case 15:
                        obj2 = w0Var.z();
                        break;
                    case 16:
                        obj2 = w0Var.v();
                        break;
                    case 17:
                        obj2 = w0Var.N(bVar.b.getClass(), rVar);
                        break;
                    case 18:
                        obj2 = w0Var.K(bVar.b.getClass(), rVar);
                        break;
                }
            }
            if (bVar.a.i()) {
                wVar.g(bVar.a, obj2);
            } else {
                int i = a.a[bVar.a.n().ordinal()];
                if ((i == 17 || i == 18) && (t = wVar.t(bVar.a)) != null) {
                    obj2 = a0.j(t, obj2);
                }
                wVar.N(bVar.a, obj2);
            }
        }
        return ub;
    }

    @Override // com.google.protobuf.s
    public void h(w0 w0Var, Object obj, r rVar, w<Descriptors.FieldDescriptor> wVar) throws IOException {
        q.b bVar = (q.b) obj;
        if (r.c()) {
            wVar.N(bVar.a, w0Var.K(bVar.b.getClass(), rVar));
            return;
        }
        wVar.N(bVar.a, new b0(bVar.b, rVar, w0Var.z()));
    }

    @Override // com.google.protobuf.s
    public void i(ByteString byteString, Object obj, r rVar, w<Descriptors.FieldDescriptor> wVar) throws IOException {
        q.b bVar = (q.b) obj;
        l0 buildPartial = bVar.b.newBuilderForType().buildPartial();
        if (r.c()) {
            f R = f.R(ByteBuffer.wrap(byteString.toByteArray()), true);
            u0.a().b(buildPartial, R, rVar);
            wVar.N(bVar.a, buildPartial);
            if (R.w() != Integer.MAX_VALUE) {
                throw InvalidProtocolBufferException.invalidEndTag();
            }
            return;
        }
        wVar.N(bVar.a, new b0(bVar.b, rVar, byteString));
    }

    @Override // com.google.protobuf.s
    public void j(Writer writer, Map.Entry<?, ?> entry) throws IOException {
        Descriptors.FieldDescriptor fieldDescriptor = (Descriptors.FieldDescriptor) entry.getKey();
        if (fieldDescriptor.i()) {
            switch (a.a[fieldDescriptor.n().ordinal()]) {
                case 1:
                    z0.R(fieldDescriptor.getNumber(), (List) entry.getValue(), writer, fieldDescriptor.isPacked());
                    return;
                case 2:
                    z0.V(fieldDescriptor.getNumber(), (List) entry.getValue(), writer, fieldDescriptor.isPacked());
                    return;
                case 3:
                    z0.Z(fieldDescriptor.getNumber(), (List) entry.getValue(), writer, fieldDescriptor.isPacked());
                    return;
                case 4:
                    z0.i0(fieldDescriptor.getNumber(), (List) entry.getValue(), writer, fieldDescriptor.isPacked());
                    return;
                case 5:
                    z0.Y(fieldDescriptor.getNumber(), (List) entry.getValue(), writer, fieldDescriptor.isPacked());
                    return;
                case 6:
                    z0.U(fieldDescriptor.getNumber(), (List) entry.getValue(), writer, fieldDescriptor.isPacked());
                    return;
                case 7:
                    z0.T(fieldDescriptor.getNumber(), (List) entry.getValue(), writer, fieldDescriptor.isPacked());
                    return;
                case 8:
                    z0.P(fieldDescriptor.getNumber(), (List) entry.getValue(), writer, fieldDescriptor.isPacked());
                    return;
                case 9:
                    z0.h0(fieldDescriptor.getNumber(), (List) entry.getValue(), writer, fieldDescriptor.isPacked());
                    return;
                case 10:
                    z0.c0(fieldDescriptor.getNumber(), (List) entry.getValue(), writer, fieldDescriptor.isPacked());
                    return;
                case 11:
                    z0.d0(fieldDescriptor.getNumber(), (List) entry.getValue(), writer, fieldDescriptor.isPacked());
                    return;
                case 12:
                    z0.e0(fieldDescriptor.getNumber(), (List) entry.getValue(), writer, fieldDescriptor.isPacked());
                    return;
                case 13:
                    z0.f0(fieldDescriptor.getNumber(), (List) entry.getValue(), writer, fieldDescriptor.isPacked());
                    return;
                case 14:
                    ArrayList arrayList = new ArrayList();
                    for (Descriptors.d dVar : (List) entry.getValue()) {
                        arrayList.add(Integer.valueOf(dVar.getNumber()));
                    }
                    z0.Y(fieldDescriptor.getNumber(), arrayList, writer, fieldDescriptor.isPacked());
                    return;
                case 15:
                    z0.Q(fieldDescriptor.getNumber(), (List) entry.getValue(), writer);
                    return;
                case 16:
                    z0.g0(fieldDescriptor.getNumber(), (List) entry.getValue(), writer);
                    return;
                case 17:
                    z0.W(fieldDescriptor.getNumber(), (List) entry.getValue(), writer);
                    return;
                case 18:
                    z0.a0(fieldDescriptor.getNumber(), (List) entry.getValue(), writer);
                    return;
                default:
                    return;
            }
        }
        switch (a.a[fieldDescriptor.n().ordinal()]) {
            case 1:
                writer.e(fieldDescriptor.getNumber(), ((Double) entry.getValue()).doubleValue());
                return;
            case 2:
                writer.A(fieldDescriptor.getNumber(), ((Float) entry.getValue()).floatValue());
                return;
            case 3:
                writer.n(fieldDescriptor.getNumber(), ((Long) entry.getValue()).longValue());
                return;
            case 4:
                writer.l(fieldDescriptor.getNumber(), ((Long) entry.getValue()).longValue());
                return;
            case 5:
                writer.r(fieldDescriptor.getNumber(), ((Integer) entry.getValue()).intValue());
                return;
            case 6:
                writer.h(fieldDescriptor.getNumber(), ((Long) entry.getValue()).longValue());
                return;
            case 7:
                writer.d(fieldDescriptor.getNumber(), ((Integer) entry.getValue()).intValue());
                return;
            case 8:
                writer.o(fieldDescriptor.getNumber(), ((Boolean) entry.getValue()).booleanValue());
                return;
            case 9:
                writer.b(fieldDescriptor.getNumber(), ((Integer) entry.getValue()).intValue());
                return;
            case 10:
                writer.p(fieldDescriptor.getNumber(), ((Integer) entry.getValue()).intValue());
                return;
            case 11:
                writer.u(fieldDescriptor.getNumber(), ((Long) entry.getValue()).longValue());
                return;
            case 12:
                writer.H(fieldDescriptor.getNumber(), ((Integer) entry.getValue()).intValue());
                return;
            case 13:
                writer.z(fieldDescriptor.getNumber(), ((Long) entry.getValue()).longValue());
                return;
            case 14:
                writer.r(fieldDescriptor.getNumber(), ((Descriptors.d) entry.getValue()).getNumber());
                return;
            case 15:
                writer.Q(fieldDescriptor.getNumber(), (ByteString) entry.getValue());
                return;
            case 16:
                writer.k(fieldDescriptor.getNumber(), (String) entry.getValue());
                return;
            case 17:
                writer.R(fieldDescriptor.getNumber(), entry.getValue());
                return;
            case 18:
                writer.O(fieldDescriptor.getNumber(), entry.getValue());
                return;
            default:
                return;
        }
    }

    public void l(Object obj, w<Descriptors.FieldDescriptor> wVar) {
        k1.U(obj, a, wVar);
    }
}
