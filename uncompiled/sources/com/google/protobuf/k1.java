package com.google.protobuf;

import java.lang.reflect.Field;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.AccessController;
import java.security.PrivilegedExceptionAction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.web3j.abi.datatypes.Address;
import sun.misc.Unsafe;

/* compiled from: UnsafeUtil.java */
/* loaded from: classes2.dex */
public final class k1 {
    public static final Logger a = Logger.getLogger(k1.class.getName());
    public static final Unsafe b = G();
    public static final Class<?> c = sc.b();
    public static final boolean d = o(Long.TYPE);
    public static final boolean e = o(Integer.TYPE);
    public static final e f = D();
    public static final boolean g = W();
    public static final boolean h = V();
    public static final long i = k(byte[].class);
    public static final long j;
    public static final boolean k;

    /* compiled from: UnsafeUtil.java */
    /* loaded from: classes2.dex */
    public static class a implements PrivilegedExceptionAction<Unsafe> {
        @Override // java.security.PrivilegedExceptionAction
        /* renamed from: a */
        public Unsafe run() throws Exception {
            Field[] declaredFields;
            for (Field field : Unsafe.class.getDeclaredFields()) {
                field.setAccessible(true);
                Object obj = field.get(null);
                if (Unsafe.class.isInstance(obj)) {
                    return (Unsafe) Unsafe.class.cast(obj);
                }
            }
            return null;
        }
    }

    /* compiled from: UnsafeUtil.java */
    /* loaded from: classes2.dex */
    public static final class b extends e {
        public b(Unsafe unsafe) {
            super(unsafe);
        }

        @Override // com.google.protobuf.k1.e
        public void c(long j, byte[] bArr, long j2, long j3) {
            throw new UnsupportedOperationException();
        }

        @Override // com.google.protobuf.k1.e
        public boolean d(Object obj, long j) {
            return k1.k ? k1.s(obj, j) : k1.t(obj, j);
        }

        @Override // com.google.protobuf.k1.e
        public byte e(long j) {
            throw new UnsupportedOperationException();
        }

        @Override // com.google.protobuf.k1.e
        public byte f(Object obj, long j) {
            return k1.k ? k1.w(obj, j) : k1.x(obj, j);
        }

        @Override // com.google.protobuf.k1.e
        public double g(Object obj, long j) {
            return Double.longBitsToDouble(k(obj, j));
        }

        @Override // com.google.protobuf.k1.e
        public float h(Object obj, long j) {
            return Float.intBitsToFloat(i(obj, j));
        }

        @Override // com.google.protobuf.k1.e
        public long j(long j) {
            throw new UnsupportedOperationException();
        }

        @Override // com.google.protobuf.k1.e
        public Object m(Field field) {
            try {
                return field.get(null);
            } catch (IllegalAccessException unused) {
                return null;
            }
        }

        @Override // com.google.protobuf.k1.e
        public void o(Object obj, long j, boolean z) {
            if (k1.k) {
                k1.L(obj, j, z);
            } else {
                k1.M(obj, j, z);
            }
        }

        @Override // com.google.protobuf.k1.e
        public void p(Object obj, long j, byte b) {
            if (k1.k) {
                k1.O(obj, j, b);
            } else {
                k1.P(obj, j, b);
            }
        }

        @Override // com.google.protobuf.k1.e
        public void q(Object obj, long j, double d) {
            t(obj, j, Double.doubleToLongBits(d));
        }

        @Override // com.google.protobuf.k1.e
        public void r(Object obj, long j, float f) {
            s(obj, j, Float.floatToIntBits(f));
        }
    }

    /* compiled from: UnsafeUtil.java */
    /* loaded from: classes2.dex */
    public static final class c extends e {
        public c(Unsafe unsafe) {
            super(unsafe);
        }

        @Override // com.google.protobuf.k1.e
        public void c(long j, byte[] bArr, long j2, long j3) {
            throw new UnsupportedOperationException();
        }

        @Override // com.google.protobuf.k1.e
        public boolean d(Object obj, long j) {
            return k1.k ? k1.s(obj, j) : k1.t(obj, j);
        }

        @Override // com.google.protobuf.k1.e
        public byte e(long j) {
            throw new UnsupportedOperationException();
        }

        @Override // com.google.protobuf.k1.e
        public byte f(Object obj, long j) {
            return k1.k ? k1.w(obj, j) : k1.x(obj, j);
        }

        @Override // com.google.protobuf.k1.e
        public double g(Object obj, long j) {
            return Double.longBitsToDouble(k(obj, j));
        }

        @Override // com.google.protobuf.k1.e
        public float h(Object obj, long j) {
            return Float.intBitsToFloat(i(obj, j));
        }

        @Override // com.google.protobuf.k1.e
        public long j(long j) {
            throw new UnsupportedOperationException();
        }

        @Override // com.google.protobuf.k1.e
        public Object m(Field field) {
            try {
                return field.get(null);
            } catch (IllegalAccessException unused) {
                return null;
            }
        }

        @Override // com.google.protobuf.k1.e
        public void o(Object obj, long j, boolean z) {
            if (k1.k) {
                k1.L(obj, j, z);
            } else {
                k1.M(obj, j, z);
            }
        }

        @Override // com.google.protobuf.k1.e
        public void p(Object obj, long j, byte b) {
            if (k1.k) {
                k1.O(obj, j, b);
            } else {
                k1.P(obj, j, b);
            }
        }

        @Override // com.google.protobuf.k1.e
        public void q(Object obj, long j, double d) {
            t(obj, j, Double.doubleToLongBits(d));
        }

        @Override // com.google.protobuf.k1.e
        public void r(Object obj, long j, float f) {
            s(obj, j, Float.floatToIntBits(f));
        }
    }

    /* compiled from: UnsafeUtil.java */
    /* loaded from: classes2.dex */
    public static final class d extends e {
        public d(Unsafe unsafe) {
            super(unsafe);
        }

        @Override // com.google.protobuf.k1.e
        public void c(long j, byte[] bArr, long j2, long j3) {
            this.a.copyMemory((Object) null, j, bArr, k1.i + j2, j3);
        }

        @Override // com.google.protobuf.k1.e
        public boolean d(Object obj, long j) {
            return this.a.getBoolean(obj, j);
        }

        @Override // com.google.protobuf.k1.e
        public byte e(long j) {
            return this.a.getByte(j);
        }

        @Override // com.google.protobuf.k1.e
        public byte f(Object obj, long j) {
            return this.a.getByte(obj, j);
        }

        @Override // com.google.protobuf.k1.e
        public double g(Object obj, long j) {
            return this.a.getDouble(obj, j);
        }

        @Override // com.google.protobuf.k1.e
        public float h(Object obj, long j) {
            return this.a.getFloat(obj, j);
        }

        @Override // com.google.protobuf.k1.e
        public long j(long j) {
            return this.a.getLong(j);
        }

        @Override // com.google.protobuf.k1.e
        public Object m(Field field) {
            return l(this.a.staticFieldBase(field), this.a.staticFieldOffset(field));
        }

        @Override // com.google.protobuf.k1.e
        public void o(Object obj, long j, boolean z) {
            this.a.putBoolean(obj, j, z);
        }

        @Override // com.google.protobuf.k1.e
        public void p(Object obj, long j, byte b) {
            this.a.putByte(obj, j, b);
        }

        @Override // com.google.protobuf.k1.e
        public void q(Object obj, long j, double d) {
            this.a.putDouble(obj, j, d);
        }

        @Override // com.google.protobuf.k1.e
        public void r(Object obj, long j, float f) {
            this.a.putFloat(obj, j, f);
        }
    }

    /* compiled from: UnsafeUtil.java */
    /* loaded from: classes2.dex */
    public static abstract class e {
        public Unsafe a;

        public e(Unsafe unsafe) {
            this.a = unsafe;
        }

        public final int a(Class<?> cls) {
            return this.a.arrayBaseOffset(cls);
        }

        public final int b(Class<?> cls) {
            return this.a.arrayIndexScale(cls);
        }

        public abstract void c(long j, byte[] bArr, long j2, long j3);

        public abstract boolean d(Object obj, long j);

        public abstract byte e(long j);

        public abstract byte f(Object obj, long j);

        public abstract double g(Object obj, long j);

        public abstract float h(Object obj, long j);

        public final int i(Object obj, long j) {
            return this.a.getInt(obj, j);
        }

        public abstract long j(long j);

        public final long k(Object obj, long j) {
            return this.a.getLong(obj, j);
        }

        public final Object l(Object obj, long j) {
            return this.a.getObject(obj, j);
        }

        public abstract Object m(Field field);

        public final long n(Field field) {
            return this.a.objectFieldOffset(field);
        }

        public abstract void o(Object obj, long j, boolean z);

        public abstract void p(Object obj, long j, byte b);

        public abstract void q(Object obj, long j, double d);

        public abstract void r(Object obj, long j, float f);

        public final void s(Object obj, long j, int i) {
            this.a.putInt(obj, j, i);
        }

        public final void t(Object obj, long j, long j2) {
            this.a.putLong(obj, j, j2);
        }

        public final void u(Object obj, long j, Object obj2) {
            this.a.putObject(obj, j, obj2);
        }
    }

    static {
        k(boolean[].class);
        l(boolean[].class);
        k(int[].class);
        l(int[].class);
        k(long[].class);
        l(long[].class);
        k(float[].class);
        l(float[].class);
        k(double[].class);
        l(double[].class);
        k(Object[].class);
        l(Object[].class);
        j = q(m());
        k = ByteOrder.nativeOrder() == ByteOrder.BIG_ENDIAN;
    }

    public static int A(Object obj, long j2) {
        return f.i(obj, j2);
    }

    public static long B(long j2) {
        return f.j(j2);
    }

    public static long C(Object obj, long j2) {
        return f.k(obj, j2);
    }

    public static e D() {
        Unsafe unsafe = b;
        if (unsafe == null) {
            return null;
        }
        if (sc.c()) {
            if (d) {
                return new c(unsafe);
            }
            if (e) {
                return new b(unsafe);
            }
            return null;
        }
        return new d(unsafe);
    }

    public static Object E(Object obj, long j2) {
        return f.l(obj, j2);
    }

    public static Object F(Field field) {
        return f.m(field);
    }

    public static Unsafe G() {
        try {
            return (Unsafe) AccessController.doPrivileged(new a());
        } catch (Throwable unused) {
            return null;
        }
    }

    public static boolean H() {
        return h;
    }

    public static boolean I() {
        return g;
    }

    public static long J(Field field) {
        return f.n(field);
    }

    public static void K(Object obj, long j2, boolean z) {
        f.o(obj, j2, z);
    }

    public static void L(Object obj, long j2, boolean z) {
        O(obj, j2, z ? (byte) 1 : (byte) 0);
    }

    public static void M(Object obj, long j2, boolean z) {
        P(obj, j2, z ? (byte) 1 : (byte) 0);
    }

    public static void N(byte[] bArr, long j2, byte b2) {
        f.p(bArr, i + j2, b2);
    }

    public static void O(Object obj, long j2, byte b2) {
        long j3 = (-4) & j2;
        int A = A(obj, j3);
        int i2 = ((~((int) j2)) & 3) << 3;
        S(obj, j3, ((255 & b2) << i2) | (A & (~(255 << i2))));
    }

    public static void P(Object obj, long j2, byte b2) {
        long j3 = (-4) & j2;
        int i2 = (((int) j2) & 3) << 3;
        S(obj, j3, ((255 & b2) << i2) | (A(obj, j3) & (~(255 << i2))));
    }

    public static void Q(Object obj, long j2, double d2) {
        f.q(obj, j2, d2);
    }

    public static void R(Object obj, long j2, float f2) {
        f.r(obj, j2, f2);
    }

    public static void S(Object obj, long j2, int i2) {
        f.s(obj, j2, i2);
    }

    public static void T(Object obj, long j2, long j3) {
        f.t(obj, j2, j3);
    }

    public static void U(Object obj, long j2, Object obj2) {
        f.u(obj, j2, obj2);
    }

    public static boolean V() {
        Unsafe unsafe = b;
        if (unsafe == null) {
            return false;
        }
        try {
            Class<?> cls = unsafe.getClass();
            cls.getMethod("objectFieldOffset", Field.class);
            cls.getMethod("arrayBaseOffset", Class.class);
            cls.getMethod("arrayIndexScale", Class.class);
            Class<?> cls2 = Long.TYPE;
            cls.getMethod("getInt", Object.class, cls2);
            cls.getMethod("putInt", Object.class, cls2, Integer.TYPE);
            cls.getMethod("getLong", Object.class, cls2);
            cls.getMethod("putLong", Object.class, cls2, cls2);
            cls.getMethod("getObject", Object.class, cls2);
            cls.getMethod("putObject", Object.class, cls2, Object.class);
            if (sc.c()) {
                return true;
            }
            cls.getMethod("getByte", Object.class, cls2);
            cls.getMethod("putByte", Object.class, cls2, Byte.TYPE);
            cls.getMethod("getBoolean", Object.class, cls2);
            cls.getMethod("putBoolean", Object.class, cls2, Boolean.TYPE);
            cls.getMethod("getFloat", Object.class, cls2);
            cls.getMethod("putFloat", Object.class, cls2, Float.TYPE);
            cls.getMethod("getDouble", Object.class, cls2);
            cls.getMethod("putDouble", Object.class, cls2, Double.TYPE);
            return true;
        } catch (Throwable th) {
            Logger logger = a;
            Level level = Level.WARNING;
            logger.log(level, "platform method missing - proto runtime falling back to safer methods: " + th);
            return false;
        }
    }

    public static boolean W() {
        Unsafe unsafe = b;
        if (unsafe == null) {
            return false;
        }
        try {
            Class<?> cls = unsafe.getClass();
            cls.getMethod("objectFieldOffset", Field.class);
            Class<?> cls2 = Long.TYPE;
            cls.getMethod("getLong", Object.class, cls2);
            if (m() == null) {
                return false;
            }
            if (sc.c()) {
                return true;
            }
            cls.getMethod("getByte", cls2);
            cls.getMethod("putByte", cls2, Byte.TYPE);
            cls.getMethod("getInt", cls2);
            cls.getMethod("putInt", cls2, Integer.TYPE);
            cls.getMethod("getLong", cls2);
            cls.getMethod("putLong", cls2, cls2);
            cls.getMethod("copyMemory", cls2, cls2, cls2);
            cls.getMethod("copyMemory", Object.class, cls2, Object.class, cls2, cls2);
            return true;
        } catch (Throwable th) {
            Logger logger = a;
            Level level = Level.WARNING;
            logger.log(level, "platform method missing - proto runtime falling back to safer methods: " + th);
            return false;
        }
    }

    public static long i(ByteBuffer byteBuffer) {
        return f.k(byteBuffer, j);
    }

    public static <T> T j(Class<T> cls) {
        try {
            return (T) b.allocateInstance(cls);
        } catch (InstantiationException e2) {
            throw new IllegalStateException(e2);
        }
    }

    public static int k(Class<?> cls) {
        if (h) {
            return f.a(cls);
        }
        return -1;
    }

    public static int l(Class<?> cls) {
        if (h) {
            return f.b(cls);
        }
        return -1;
    }

    public static Field m() {
        Field p;
        if (!sc.c() || (p = p(Buffer.class, "effectiveDirectAddress")) == null) {
            Field p2 = p(Buffer.class, Address.TYPE_NAME);
            if (p2 == null || p2.getType() != Long.TYPE) {
                return null;
            }
            return p2;
        }
        return p;
    }

    public static void n(long j2, byte[] bArr, long j3, long j4) {
        f.c(j2, bArr, j3, j4);
    }

    public static boolean o(Class<?> cls) {
        if (sc.c()) {
            try {
                Class<?> cls2 = c;
                Class<?> cls3 = Boolean.TYPE;
                cls2.getMethod("peekLong", cls, cls3);
                cls2.getMethod("pokeLong", cls, Long.TYPE, cls3);
                Class<?> cls4 = Integer.TYPE;
                cls2.getMethod("pokeInt", cls, cls4, cls3);
                cls2.getMethod("peekInt", cls, cls3);
                cls2.getMethod("pokeByte", cls, Byte.TYPE);
                cls2.getMethod("peekByte", cls);
                cls2.getMethod("pokeByteArray", cls, byte[].class, cls4, cls4);
                cls2.getMethod("peekByteArray", cls, byte[].class, cls4, cls4);
                return true;
            } catch (Throwable unused) {
                return false;
            }
        }
        return false;
    }

    public static Field p(Class<?> cls, String str) {
        try {
            return cls.getDeclaredField(str);
        } catch (Throwable unused) {
            return null;
        }
    }

    public static long q(Field field) {
        e eVar;
        if (field == null || (eVar = f) == null) {
            return -1L;
        }
        return eVar.n(field);
    }

    public static boolean r(Object obj, long j2) {
        return f.d(obj, j2);
    }

    public static boolean s(Object obj, long j2) {
        return w(obj, j2) != 0;
    }

    public static boolean t(Object obj, long j2) {
        return x(obj, j2) != 0;
    }

    public static byte u(long j2) {
        return f.e(j2);
    }

    public static byte v(byte[] bArr, long j2) {
        return f.f(bArr, i + j2);
    }

    public static byte w(Object obj, long j2) {
        return (byte) ((A(obj, (-4) & j2) >>> ((int) (((~j2) & 3) << 3))) & 255);
    }

    public static byte x(Object obj, long j2) {
        return (byte) ((A(obj, (-4) & j2) >>> ((int) ((j2 & 3) << 3))) & 255);
    }

    public static double y(Object obj, long j2) {
        return f.g(obj, j2);
    }

    public static float z(Object obj, long j2) {
        return f.h(obj, j2);
    }
}
