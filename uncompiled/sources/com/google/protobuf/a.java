package com.google.protobuf;

import com.google.protobuf.Descriptors;
import com.google.protobuf.MessageReflection;
import com.google.protobuf.a0;
import com.google.protobuf.b;
import com.google.protobuf.g1;
import com.google.protobuf.l0;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: AbstractMessage.java */
/* loaded from: classes2.dex */
public abstract class a extends com.google.protobuf.b implements l0 {
    public int memoizedSize = -1;

    /* compiled from: AbstractMessage.java */
    /* renamed from: com.google.protobuf.a$a  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public static abstract class AbstractC0151a<BuilderType extends AbstractC0151a<BuilderType>> extends b.a implements l0.a {
        public static UninitializedMessageException newUninitializedMessageException(l0 l0Var) {
            return new UninitializedMessageException(MessageReflection.c(l0Var));
        }

        /* JADX INFO: Access modifiers changed from: package-private */
        public void dispose() {
            throw new IllegalStateException("Should be overridden by subclasses.");
        }

        public List<String> findInitializationErrors() {
            return MessageReflection.c(this);
        }

        public l0.a getFieldBuilder(Descriptors.FieldDescriptor fieldDescriptor) {
            throw new UnsupportedOperationException("getFieldBuilder() called on an unsupported message type.");
        }

        public String getInitializationErrorString() {
            return MessageReflection.a(findInitializationErrors());
        }

        public Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar) {
            throw new UnsupportedOperationException("getOneofFieldDescriptor() is not implemented.");
        }

        public l0.a getRepeatedFieldBuilder(Descriptors.FieldDescriptor fieldDescriptor, int i) {
            throw new UnsupportedOperationException("getRepeatedFieldBuilder() called on an unsupported message type.");
        }

        public boolean hasOneof(Descriptors.g gVar) {
            throw new UnsupportedOperationException("hasOneof() is not implemented.");
        }

        public void markClean() {
            throw new IllegalStateException("Should be overridden by subclasses.");
        }

        @Override // com.google.protobuf.b.a
        public boolean mergeDelimitedFrom(InputStream inputStream) throws IOException {
            return super.mergeDelimitedFrom(inputStream);
        }

        public String toString() {
            return TextFormat.o().j(this);
        }

        /* renamed from: clearOneof */
        public BuilderType m22clearOneof(Descriptors.g gVar) {
            throw new UnsupportedOperationException("clearOneof() is not implemented.");
        }

        @Override // com.google.protobuf.b.a
        public BuilderType internalMergeFrom(com.google.protobuf.b bVar) {
            return mergeFrom((l0) bVar);
        }

        @Override // com.google.protobuf.b.a
        public boolean mergeDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return super.mergeDelimitedFrom(inputStream, rVar);
        }

        /* renamed from: mergeUnknownFields */
        public BuilderType m23mergeUnknownFields(g1 g1Var) {
            setUnknownFields(g1.h(getUnknownFields()).u(g1Var).build());
            return this;
        }

        /* renamed from: clear */
        public BuilderType m21clear() {
            for (Map.Entry<Descriptors.FieldDescriptor, Object> entry : getAllFields().entrySet()) {
                clearField(entry.getKey());
            }
            return this;
        }

        @Override // com.google.protobuf.b.a
        public BuilderType clone() {
            throw new UnsupportedOperationException("clone() should be implemented in subclasses.");
        }

        public BuilderType mergeFrom(l0 l0Var) {
            return mergeFrom(l0Var, l0Var.getAllFields());
        }

        public BuilderType mergeFrom(l0 l0Var, Map<Descriptors.FieldDescriptor, Object> map) {
            if (l0Var.getDescriptorForType() == getDescriptorForType()) {
                for (Map.Entry<Descriptors.FieldDescriptor, Object> entry : map.entrySet()) {
                    Descriptors.FieldDescriptor key = entry.getKey();
                    if (key.i()) {
                        for (Object obj : (List) entry.getValue()) {
                            addRepeatedField(key, obj);
                        }
                    } else if (key.v() == Descriptors.FieldDescriptor.JavaType.MESSAGE) {
                        l0 l0Var2 = (l0) getField(key);
                        if (l0Var2 == l0Var2.getDefaultInstanceForType()) {
                            setField(key, entry.getValue());
                        } else {
                            setField(key, l0Var2.newBuilderForType().mergeFrom(l0Var2).mergeFrom((l0) entry.getValue()).build());
                        }
                    } else {
                        setField(key, entry.getValue());
                    }
                }
                m23mergeUnknownFields(l0Var.getUnknownFields());
                return this;
            }
            throw new IllegalArgumentException("mergeFrom(Message) can only merge messages of the same type.");
        }

        @Override // com.google.protobuf.b.a
        public BuilderType mergeFrom(j jVar) throws IOException {
            return mergeFrom(jVar, (r) q.f());
        }

        @Override // com.google.protobuf.b.a, com.google.protobuf.m0.a
        public BuilderType mergeFrom(j jVar, r rVar) throws IOException {
            int J;
            g1.b h = jVar.M() ? null : g1.h(getUnknownFields());
            do {
                J = jVar.J();
                if (J == 0) {
                    break;
                }
            } while (MessageReflection.g(jVar, h, rVar, getDescriptorForType(), new MessageReflection.b(this), J));
            if (h != null) {
                setUnknownFields(h.build());
            }
            return this;
        }

        @Override // com.google.protobuf.b.a, com.google.protobuf.l0.a
        public BuilderType mergeFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return (BuilderType) super.m26mergeFrom(byteString);
        }

        @Override // com.google.protobuf.b.a, com.google.protobuf.l0.a
        public BuilderType mergeFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return (BuilderType) super.m27mergeFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.b.a, com.google.protobuf.m0.a
        public BuilderType mergeFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return (BuilderType) super.mergeFrom(bArr);
        }

        @Override // com.google.protobuf.b.a
        public BuilderType mergeFrom(byte[] bArr, int i, int i2) throws InvalidProtocolBufferException {
            return (BuilderType) super.m31mergeFrom(bArr, i, i2);
        }

        @Override // com.google.protobuf.b.a
        public BuilderType mergeFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return (BuilderType) super.m33mergeFrom(bArr, rVar);
        }

        @Override // com.google.protobuf.b.a
        public BuilderType mergeFrom(byte[] bArr, int i, int i2, r rVar) throws InvalidProtocolBufferException {
            return (BuilderType) super.m32mergeFrom(bArr, i, i2, rVar);
        }

        @Override // com.google.protobuf.b.a
        public BuilderType mergeFrom(InputStream inputStream) throws IOException {
            return (BuilderType) super.m29mergeFrom(inputStream);
        }

        @Override // com.google.protobuf.b.a
        public BuilderType mergeFrom(InputStream inputStream, r rVar) throws IOException {
            return (BuilderType) super.m30mergeFrom(inputStream, rVar);
        }
    }

    /* compiled from: AbstractMessage.java */
    /* loaded from: classes2.dex */
    public interface b {
        void a();
    }

    private static boolean compareBytes(Object obj, Object obj2) {
        if ((obj instanceof byte[]) && (obj2 instanceof byte[])) {
            return Arrays.equals((byte[]) obj, (byte[]) obj2);
        }
        return toByteString(obj).equals(toByteString(obj2));
    }

    public static boolean compareFields(Map<Descriptors.FieldDescriptor, Object> map, Map<Descriptors.FieldDescriptor, Object> map2) {
        if (map.size() != map2.size()) {
            return false;
        }
        for (Descriptors.FieldDescriptor fieldDescriptor : map.keySet()) {
            if (!map2.containsKey(fieldDescriptor)) {
                return false;
            }
            Object obj = map.get(fieldDescriptor);
            Object obj2 = map2.get(fieldDescriptor);
            if (fieldDescriptor.A() == Descriptors.FieldDescriptor.Type.BYTES) {
                if (fieldDescriptor.i()) {
                    List list = (List) obj;
                    List list2 = (List) obj2;
                    if (list.size() != list2.size()) {
                        return false;
                    }
                    for (int i = 0; i < list.size(); i++) {
                        if (!compareBytes(list.get(i), list2.get(i))) {
                            return false;
                        }
                    }
                    continue;
                } else if (!compareBytes(obj, obj2)) {
                    return false;
                }
            } else if (fieldDescriptor.D()) {
                if (!compareMapField(obj, obj2)) {
                    return false;
                }
            } else if (!obj.equals(obj2)) {
                return false;
            }
        }
        return true;
    }

    private static boolean compareMapField(Object obj, Object obj2) {
        return MapFieldLite.equals(convertMapEntryListToMap((List) obj), convertMapEntryListToMap((List) obj2));
    }

    private static Map convertMapEntryListToMap(List list) {
        if (list.isEmpty()) {
            return Collections.emptyMap();
        }
        HashMap hashMap = new HashMap();
        Iterator it = list.iterator();
        l0 l0Var = (l0) it.next();
        Descriptors.b descriptorForType = l0Var.getDescriptorForType();
        Descriptors.FieldDescriptor j = descriptorForType.j("key");
        Descriptors.FieldDescriptor j2 = descriptorForType.j("value");
        Object field = l0Var.getField(j2);
        if (field instanceof Descriptors.d) {
            field = Integer.valueOf(((Descriptors.d) field).getNumber());
        }
        hashMap.put(l0Var.getField(j), field);
        while (it.hasNext()) {
            l0 l0Var2 = (l0) it.next();
            Object field2 = l0Var2.getField(j2);
            if (field2 instanceof Descriptors.d) {
                field2 = Integer.valueOf(((Descriptors.d) field2).getNumber());
            }
            hashMap.put(l0Var2.getField(j), field2);
        }
        return hashMap;
    }

    @Deprecated
    public static int hashBoolean(boolean z) {
        return z ? 1231 : 1237;
    }

    @Deprecated
    public static int hashEnum(a0.c cVar) {
        return cVar.getNumber();
    }

    @Deprecated
    public static int hashEnumList(List<? extends a0.c> list) {
        int i = 1;
        for (a0.c cVar : list) {
            i = (i * 31) + hashEnum(cVar);
        }
        return i;
    }

    public static int hashFields(int i, Map<Descriptors.FieldDescriptor, Object> map) {
        int i2;
        int f;
        for (Map.Entry<Descriptors.FieldDescriptor, Object> entry : map.entrySet()) {
            Descriptors.FieldDescriptor key = entry.getKey();
            Object value = entry.getValue();
            int number = (i * 37) + key.getNumber();
            if (key.D()) {
                i2 = number * 53;
                f = hashMapField(value);
            } else if (key.A() != Descriptors.FieldDescriptor.Type.ENUM) {
                i2 = number * 53;
                f = value.hashCode();
            } else if (key.i()) {
                i2 = number * 53;
                f = a0.g((List) value);
            } else {
                i2 = number * 53;
                f = a0.f((a0.c) value);
            }
            i = i2 + f;
        }
        return i;
    }

    @Deprecated
    public static int hashLong(long j) {
        return (int) (j ^ (j >>> 32));
    }

    private static int hashMapField(Object obj) {
        return MapFieldLite.calculateHashCodeForMap(convertMapEntryListToMap((List) obj));
    }

    private static ByteString toByteString(Object obj) {
        if (obj instanceof byte[]) {
            return ByteString.copyFrom((byte[]) obj);
        }
        return (ByteString) obj;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof l0) {
            l0 l0Var = (l0) obj;
            if (getDescriptorForType() != l0Var.getDescriptorForType()) {
                return false;
            }
            return compareFields(getAllFields(), l0Var.getAllFields()) && getUnknownFields().equals(l0Var.getUnknownFields());
        }
        return false;
    }

    public List<String> findInitializationErrors() {
        return MessageReflection.c(this);
    }

    public String getInitializationErrorString() {
        return MessageReflection.a(findInitializationErrors());
    }

    @Override // com.google.protobuf.b
    int getMemoizedSerializedSize() {
        return this.memoizedSize;
    }

    public Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar) {
        throw new UnsupportedOperationException("getOneofFieldDescriptor() is not implemented.");
    }

    @Override // com.google.protobuf.m0
    public int getSerializedSize() {
        int i = this.memoizedSize;
        if (i != -1) {
            return i;
        }
        int e = MessageReflection.e(this, getAllFields());
        this.memoizedSize = e;
        return e;
    }

    public boolean hasOneof(Descriptors.g gVar) {
        throw new UnsupportedOperationException("hasOneof() is not implemented.");
    }

    public int hashCode() {
        int i = this.memoizedHashCode;
        if (i == 0) {
            int hashFields = (hashFields(779 + getDescriptorForType().hashCode(), getAllFields()) * 29) + getUnknownFields().hashCode();
            this.memoizedHashCode = hashFields;
            return hashFields;
        }
        return i;
    }

    @Override // defpackage.d82
    public boolean isInitialized() {
        return MessageReflection.f(this);
    }

    public l0.a newBuilderForType(b bVar) {
        throw new UnsupportedOperationException("Nested builder is not supported for this type.");
    }

    @Override // com.google.protobuf.b
    public UninitializedMessageException newUninitializedMessageException() {
        return AbstractC0151a.newUninitializedMessageException((l0) this);
    }

    @Override // com.google.protobuf.b
    void setMemoizedSerializedSize(int i) {
        this.memoizedSize = i;
    }

    public final String toString() {
        return TextFormat.o().j(this);
    }

    @Override // com.google.protobuf.m0
    public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
        MessageReflection.k(this, getAllFields(), codedOutputStream, false);
    }
}
