package com.google.protobuf;

import com.google.protobuf.h0;
import java.util.Map;

/* compiled from: MapFieldSchemaFull.java */
/* loaded from: classes2.dex */
public class j0 implements i0 {
    public static <K, V> int i(int i, Object obj, Object obj2) {
        int i2 = 0;
        if (obj == null) {
            return 0;
        }
        Map<K, V> j = ((MapField) obj).j();
        g0 g0Var = (g0) obj2;
        if (j.isEmpty()) {
            return 0;
        }
        for (Map.Entry<K, V> entry : j.entrySet()) {
            i2 += CodedOutputStream.X(i) + CodedOutputStream.E(h0.b(g0Var.g(), entry.getKey(), entry.getValue()));
        }
        return i2;
    }

    public static <K, V> Object j(Object obj, Object obj2) {
        MapField mapField = (MapField) obj;
        MapField<K, V> mapField2 = (MapField) obj2;
        if (!mapField.n()) {
            mapField.g();
        }
        mapField.p(mapField2);
        return mapField;
    }

    @Override // com.google.protobuf.i0
    public Object a(Object obj, Object obj2) {
        return j(obj, obj2);
    }

    @Override // com.google.protobuf.i0
    public Object b(Object obj) {
        ((MapField) obj).o();
        return obj;
    }

    @Override // com.google.protobuf.i0
    public h0.b<?, ?> c(Object obj) {
        return ((g0) obj).g();
    }

    @Override // com.google.protobuf.i0
    public Map<?, ?> d(Object obj) {
        return ((MapField) obj).m();
    }

    @Override // com.google.protobuf.i0
    public Object e(Object obj) {
        return MapField.q((g0) obj);
    }

    @Override // com.google.protobuf.i0
    public int f(int i, Object obj, Object obj2) {
        return i(i, obj, obj2);
    }

    @Override // com.google.protobuf.i0
    public boolean g(Object obj) {
        return !((MapField) obj).n();
    }

    @Override // com.google.protobuf.i0
    public Map<?, ?> h(Object obj) {
        return ((MapField) obj).j();
    }
}
