package com.google.protobuf;

import java.io.IOException;

/* compiled from: UnknownFieldSchema.java */
/* loaded from: classes2.dex */
public abstract class f1<T, B> {
    public abstract void a(B b, int i, int i2);

    public abstract void b(B b, int i, long j);

    public abstract void c(B b, int i, T t);

    public abstract void d(B b, int i, ByteString byteString);

    public abstract void e(B b, int i, long j);

    public abstract B f(Object obj);

    public abstract T g(Object obj);

    public abstract int h(T t);

    public abstract int i(T t);

    public abstract void j(Object obj);

    public abstract T k(T t, T t2);

    public final void l(B b, w0 w0Var) throws IOException {
        while (w0Var.w() != Integer.MAX_VALUE && m(b, w0Var)) {
        }
    }

    public final boolean m(B b, w0 w0Var) throws IOException {
        int a = w0Var.a();
        int a2 = WireFormat.a(a);
        int b2 = WireFormat.b(a);
        if (b2 == 0) {
            e(b, a2, w0Var.G());
            return true;
        } else if (b2 == 1) {
            b(b, a2, w0Var.d());
            return true;
        } else if (b2 == 2) {
            d(b, a2, w0Var.z());
            return true;
        } else if (b2 != 3) {
            if (b2 != 4) {
                if (b2 == 5) {
                    a(b, a2, w0Var.h());
                    return true;
                }
                throw InvalidProtocolBufferException.invalidWireType();
            }
            return false;
        } else {
            B n = n();
            int c = WireFormat.c(a2, 4);
            l(n, w0Var);
            if (c == w0Var.a()) {
                c(b, a2, r(n));
                return true;
            }
            throw InvalidProtocolBufferException.invalidEndTag();
        }
    }

    public abstract B n();

    public abstract void o(Object obj, B b);

    public abstract void p(Object obj, T t);

    public abstract boolean q(w0 w0Var);

    public abstract T r(B b);

    public abstract void s(T t, Writer writer) throws IOException;

    public abstract void t(T t, Writer writer) throws IOException;
}
