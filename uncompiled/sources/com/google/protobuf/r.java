package com.google.protobuf;

import com.google.protobuf.GeneratedMessageLite;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* compiled from: ExtensionRegistryLite.java */
/* loaded from: classes2.dex */
public class r {
    public static volatile boolean b = false;
    public static boolean c = true;
    public static volatile r d;
    public static final r e = new r(true);
    public final Map<a, GeneratedMessageLite.d<?, ?>> a;

    /* compiled from: ExtensionRegistryLite.java */
    /* loaded from: classes2.dex */
    public static final class a {
        public final Object a;
        public final int b;

        public a(Object obj, int i) {
            this.a = obj;
            this.b = i;
        }

        public boolean equals(Object obj) {
            if (obj instanceof a) {
                a aVar = (a) obj;
                return this.a == aVar.a && this.b == aVar.b;
            }
            return false;
        }

        public int hashCode() {
            return (System.identityHashCode(this.a) * 65535) + this.b;
        }
    }

    public r() {
        this.a = new HashMap();
    }

    public static r b() {
        r rVar = d;
        if (rVar == null) {
            synchronized (r.class) {
                rVar = d;
                if (rVar == null) {
                    rVar = c ? i11.a() : e;
                    d = rVar;
                }
            }
        }
        return rVar;
    }

    public static boolean c() {
        return b;
    }

    public <ContainingType extends m0> GeneratedMessageLite.d<ContainingType, ?> a(ContainingType containingtype, int i) {
        return (GeneratedMessageLite.d<ContainingType, ?>) this.a.get(new a(containingtype, i));
    }

    public r(r rVar) {
        if (rVar == e) {
            this.a = Collections.emptyMap();
        } else {
            this.a = Collections.unmodifiableMap(rVar.a);
        }
    }

    public r(boolean z) {
        this.a = Collections.emptyMap();
    }
}
