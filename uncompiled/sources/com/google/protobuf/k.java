package com.google.protobuf;

import com.google.protobuf.WireFormat;
import java.io.IOException;
import java.util.List;

/* compiled from: CodedInputStreamReader.java */
/* loaded from: classes2.dex */
public final class k implements w0 {
    public final j a;
    public int b;
    public int c;
    public int d = 0;

    /* compiled from: CodedInputStreamReader.java */
    /* loaded from: classes2.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[WireFormat.FieldType.values().length];
            a = iArr;
            try {
                iArr[WireFormat.FieldType.BOOL.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[WireFormat.FieldType.BYTES.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[WireFormat.FieldType.DOUBLE.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                a[WireFormat.FieldType.ENUM.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                a[WireFormat.FieldType.FIXED32.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                a[WireFormat.FieldType.FIXED64.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                a[WireFormat.FieldType.FLOAT.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                a[WireFormat.FieldType.INT32.ordinal()] = 8;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                a[WireFormat.FieldType.INT64.ordinal()] = 9;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                a[WireFormat.FieldType.MESSAGE.ordinal()] = 10;
            } catch (NoSuchFieldError unused10) {
            }
            try {
                a[WireFormat.FieldType.SFIXED32.ordinal()] = 11;
            } catch (NoSuchFieldError unused11) {
            }
            try {
                a[WireFormat.FieldType.SFIXED64.ordinal()] = 12;
            } catch (NoSuchFieldError unused12) {
            }
            try {
                a[WireFormat.FieldType.SINT32.ordinal()] = 13;
            } catch (NoSuchFieldError unused13) {
            }
            try {
                a[WireFormat.FieldType.SINT64.ordinal()] = 14;
            } catch (NoSuchFieldError unused14) {
            }
            try {
                a[WireFormat.FieldType.STRING.ordinal()] = 15;
            } catch (NoSuchFieldError unused15) {
            }
            try {
                a[WireFormat.FieldType.UINT32.ordinal()] = 16;
            } catch (NoSuchFieldError unused16) {
            }
            try {
                a[WireFormat.FieldType.UINT64.ordinal()] = 17;
            } catch (NoSuchFieldError unused17) {
            }
        }
    }

    public k(j jVar) {
        j jVar2 = (j) a0.b(jVar, "input");
        this.a = jVar2;
        jVar2.d = this;
    }

    public static k R(j jVar) {
        k kVar = jVar.d;
        return kVar != null ? kVar : new k(jVar);
    }

    @Override // com.google.protobuf.w0
    public void A(List<Float> list) throws IOException {
        int J;
        int J2;
        if (list instanceof x) {
            x xVar = (x) list;
            int b = WireFormat.b(this.b);
            if (b == 2) {
                int K = this.a.K();
                Y(K);
                int e = this.a.e() + K;
                do {
                    xVar.m(this.a.v());
                } while (this.a.e() < e);
                return;
            } else if (b == 5) {
                do {
                    xVar.m(this.a.v());
                    if (this.a.f()) {
                        return;
                    }
                    J2 = this.a.J();
                } while (J2 == this.b);
                this.d = J2;
                return;
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }
        int b2 = WireFormat.b(this.b);
        if (b2 == 2) {
            int K2 = this.a.K();
            Y(K2);
            int e2 = this.a.e() + K2;
            do {
                list.add(Float.valueOf(this.a.v()));
            } while (this.a.e() < e2);
        } else if (b2 == 5) {
            do {
                list.add(Float.valueOf(this.a.v()));
                if (this.a.f()) {
                    return;
                }
                J = this.a.J();
            } while (J == this.b);
            this.d = J;
        } else {
            throw InvalidProtocolBufferException.invalidWireType();
        }
    }

    @Override // com.google.protobuf.w0
    public int B() throws IOException {
        X(0);
        return this.a.x();
    }

    @Override // com.google.protobuf.w0
    public boolean C() throws IOException {
        int i;
        if (this.a.f() || (i = this.b) == this.c) {
            return false;
        }
        return this.a.N(i);
    }

    @Override // com.google.protobuf.w0
    public int D() throws IOException {
        X(5);
        return this.a.D();
    }

    @Override // com.google.protobuf.w0
    public void E(List<ByteString> list) throws IOException {
        int J;
        if (WireFormat.b(this.b) == 2) {
            do {
                list.add(z());
                if (this.a.f()) {
                    return;
                }
                J = this.a.J();
            } while (J == this.b);
            this.d = J;
            return;
        }
        throw InvalidProtocolBufferException.invalidWireType();
    }

    @Override // com.google.protobuf.w0
    public void F(List<Double> list) throws IOException {
        int J;
        int J2;
        if (list instanceof n) {
            n nVar = (n) list;
            int b = WireFormat.b(this.b);
            if (b == 1) {
                do {
                    nVar.m(this.a.r());
                    if (this.a.f()) {
                        return;
                    }
                    J2 = this.a.J();
                } while (J2 == this.b);
                this.d = J2;
                return;
            } else if (b == 2) {
                int K = this.a.K();
                Z(K);
                int e = this.a.e() + K;
                do {
                    nVar.m(this.a.r());
                } while (this.a.e() < e);
                return;
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }
        int b2 = WireFormat.b(this.b);
        if (b2 == 1) {
            do {
                list.add(Double.valueOf(this.a.r()));
                if (this.a.f()) {
                    return;
                }
                J = this.a.J();
            } while (J == this.b);
            this.d = J;
        } else if (b2 == 2) {
            int K2 = this.a.K();
            Z(K2);
            int e2 = this.a.e() + K2;
            do {
                list.add(Double.valueOf(this.a.r()));
            } while (this.a.e() < e2);
        } else {
            throw InvalidProtocolBufferException.invalidWireType();
        }
    }

    @Override // com.google.protobuf.w0
    public long G() throws IOException {
        X(0);
        return this.a.y();
    }

    @Override // com.google.protobuf.w0
    public String H() throws IOException {
        X(2);
        return this.a.I();
    }

    @Override // com.google.protobuf.w0
    public void I(List<Long> list) throws IOException {
        int J;
        int J2;
        if (list instanceof f0) {
            f0 f0Var = (f0) list;
            int b = WireFormat.b(this.b);
            if (b == 1) {
                do {
                    f0Var.n(this.a.u());
                    if (this.a.f()) {
                        return;
                    }
                    J2 = this.a.J();
                } while (J2 == this.b);
                this.d = J2;
                return;
            } else if (b == 2) {
                int K = this.a.K();
                Z(K);
                int e = this.a.e() + K;
                do {
                    f0Var.n(this.a.u());
                } while (this.a.e() < e);
                return;
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }
        int b2 = WireFormat.b(this.b);
        if (b2 == 1) {
            do {
                list.add(Long.valueOf(this.a.u()));
                if (this.a.f()) {
                    return;
                }
                J = this.a.J();
            } while (J == this.b);
            this.d = J;
        } else if (b2 == 2) {
            int K2 = this.a.K();
            Z(K2);
            int e2 = this.a.e() + K2;
            do {
                list.add(Long.valueOf(this.a.u()));
            } while (this.a.e() < e2);
        } else {
            throw InvalidProtocolBufferException.invalidWireType();
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.google.protobuf.w0
    public <T> void J(List<T> list, y0<T> y0Var, r rVar) throws IOException {
        int J;
        if (WireFormat.b(this.b) == 3) {
            int i = this.b;
            do {
                list.add(T(y0Var, rVar));
                if (this.a.f() || this.d != 0) {
                    return;
                }
                J = this.a.J();
            } while (J == i);
            this.d = J;
            return;
        }
        throw InvalidProtocolBufferException.invalidWireType();
    }

    @Override // com.google.protobuf.w0
    public <T> T K(Class<T> cls, r rVar) throws IOException {
        X(2);
        return (T) U(u0.a().d(cls), rVar);
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.google.protobuf.w0
    public <T> void L(List<T> list, y0<T> y0Var, r rVar) throws IOException {
        int J;
        if (WireFormat.b(this.b) == 2) {
            int i = this.b;
            do {
                list.add(U(y0Var, rVar));
                if (this.a.f() || this.d != 0) {
                    return;
                }
                J = this.a.J();
            } while (J == i);
            this.d = J;
            return;
        }
        throw InvalidProtocolBufferException.invalidWireType();
    }

    @Override // com.google.protobuf.w0
    public <T> T M(y0<T> y0Var, r rVar) throws IOException {
        X(3);
        return (T) T(y0Var, rVar);
    }

    @Override // com.google.protobuf.w0
    public <T> T N(Class<T> cls, r rVar) throws IOException {
        X(3);
        return (T) T(u0.a().d(cls), rVar);
    }

    @Override // com.google.protobuf.w0
    public <T> T O(y0<T> y0Var, r rVar) throws IOException {
        X(2);
        return (T) U(y0Var, rVar);
    }

    /* JADX WARN: Code restructure failed: missing block: B:24:0x005c, code lost:
        r8.put(r2, r3);
     */
    /* JADX WARN: Code restructure failed: missing block: B:26:0x0064, code lost:
        return;
     */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.google.protobuf.w0
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public <K, V> void P(java.util.Map<K, V> r8, com.google.protobuf.h0.b<K, V> r9, com.google.protobuf.r r10) throws java.io.IOException {
        /*
            r7 = this;
            r0 = 2
            r7.X(r0)
            com.google.protobuf.j r1 = r7.a
            int r1 = r1.K()
            com.google.protobuf.j r2 = r7.a
            int r1 = r2.o(r1)
            K r2 = r9.b
            V r3 = r9.d
        L14:
            int r4 = r7.w()     // Catch: java.lang.Throwable -> L65
            r5 = 2147483647(0x7fffffff, float:NaN)
            if (r4 == r5) goto L5c
            com.google.protobuf.j r5 = r7.a     // Catch: java.lang.Throwable -> L65
            boolean r5 = r5.f()     // Catch: java.lang.Throwable -> L65
            if (r5 == 0) goto L26
            goto L5c
        L26:
            r5 = 1
            java.lang.String r6 = "Unable to parse map entry."
            if (r4 == r5) goto L47
            if (r4 == r0) goto L3a
            boolean r4 = r7.C()     // Catch: com.google.protobuf.InvalidProtocolBufferException.InvalidWireTypeException -> L4f java.lang.Throwable -> L65
            if (r4 == 0) goto L34
            goto L14
        L34:
            com.google.protobuf.InvalidProtocolBufferException r4 = new com.google.protobuf.InvalidProtocolBufferException     // Catch: com.google.protobuf.InvalidProtocolBufferException.InvalidWireTypeException -> L4f java.lang.Throwable -> L65
            r4.<init>(r6)     // Catch: com.google.protobuf.InvalidProtocolBufferException.InvalidWireTypeException -> L4f java.lang.Throwable -> L65
            throw r4     // Catch: com.google.protobuf.InvalidProtocolBufferException.InvalidWireTypeException -> L4f java.lang.Throwable -> L65
        L3a:
            com.google.protobuf.WireFormat$FieldType r4 = r9.c     // Catch: com.google.protobuf.InvalidProtocolBufferException.InvalidWireTypeException -> L4f java.lang.Throwable -> L65
            V r5 = r9.d     // Catch: com.google.protobuf.InvalidProtocolBufferException.InvalidWireTypeException -> L4f java.lang.Throwable -> L65
            java.lang.Class r5 = r5.getClass()     // Catch: com.google.protobuf.InvalidProtocolBufferException.InvalidWireTypeException -> L4f java.lang.Throwable -> L65
            java.lang.Object r3 = r7.S(r4, r5, r10)     // Catch: com.google.protobuf.InvalidProtocolBufferException.InvalidWireTypeException -> L4f java.lang.Throwable -> L65
            goto L14
        L47:
            com.google.protobuf.WireFormat$FieldType r4 = r9.a     // Catch: com.google.protobuf.InvalidProtocolBufferException.InvalidWireTypeException -> L4f java.lang.Throwable -> L65
            r5 = 0
            java.lang.Object r2 = r7.S(r4, r5, r5)     // Catch: com.google.protobuf.InvalidProtocolBufferException.InvalidWireTypeException -> L4f java.lang.Throwable -> L65
            goto L14
        L4f:
            boolean r4 = r7.C()     // Catch: java.lang.Throwable -> L65
            if (r4 == 0) goto L56
            goto L14
        L56:
            com.google.protobuf.InvalidProtocolBufferException r8 = new com.google.protobuf.InvalidProtocolBufferException     // Catch: java.lang.Throwable -> L65
            r8.<init>(r6)     // Catch: java.lang.Throwable -> L65
            throw r8     // Catch: java.lang.Throwable -> L65
        L5c:
            r8.put(r2, r3)     // Catch: java.lang.Throwable -> L65
            com.google.protobuf.j r8 = r7.a
            r8.n(r1)
            return
        L65:
            r8 = move-exception
            com.google.protobuf.j r9 = r7.a
            r9.n(r1)
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.k.P(java.util.Map, com.google.protobuf.h0$b, com.google.protobuf.r):void");
    }

    @Override // com.google.protobuf.w0
    public boolean Q() {
        return this.a.M();
    }

    public final Object S(WireFormat.FieldType fieldType, Class<?> cls, r rVar) throws IOException {
        switch (a.a[fieldType.ordinal()]) {
            case 1:
                return Boolean.valueOf(i());
            case 2:
                return z();
            case 3:
                return Double.valueOf(readDouble());
            case 4:
                return Integer.valueOf(q());
            case 5:
                return Integer.valueOf(h());
            case 6:
                return Long.valueOf(d());
            case 7:
                return Float.valueOf(readFloat());
            case 8:
                return Integer.valueOf(B());
            case 9:
                return Long.valueOf(G());
            case 10:
                return K(cls, rVar);
            case 11:
                return Integer.valueOf(D());
            case 12:
                return Long.valueOf(j());
            case 13:
                return Integer.valueOf(s());
            case 14:
                return Long.valueOf(t());
            case 15:
                return H();
            case 16:
                return Integer.valueOf(l());
            case 17:
                return Long.valueOf(c());
            default:
                throw new RuntimeException("unsupported field type.");
        }
    }

    public final <T> T T(y0<T> y0Var, r rVar) throws IOException {
        int i = this.c;
        this.c = WireFormat.c(WireFormat.a(this.b), 4);
        try {
            T c = y0Var.c();
            y0Var.i(c, this, rVar);
            y0Var.e(c);
            if (this.b == this.c) {
                return c;
            }
            throw InvalidProtocolBufferException.parseFailure();
        } finally {
            this.c = i;
        }
    }

    public final <T> T U(y0<T> y0Var, r rVar) throws IOException {
        j jVar;
        int K = this.a.K();
        j jVar2 = this.a;
        if (jVar2.a < jVar2.b) {
            int o = jVar2.o(K);
            T c = y0Var.c();
            this.a.a++;
            y0Var.i(c, this, rVar);
            y0Var.e(c);
            this.a.a(0);
            jVar.a--;
            this.a.n(o);
            return c;
        }
        throw InvalidProtocolBufferException.recursionLimitExceeded();
    }

    public void V(List<String> list, boolean z) throws IOException {
        int J;
        int J2;
        if (WireFormat.b(this.b) == 2) {
            if ((list instanceof cz1) && !z) {
                cz1 cz1Var = (cz1) list;
                do {
                    cz1Var.W(z());
                    if (this.a.f()) {
                        return;
                    }
                    J2 = this.a.J();
                } while (J2 == this.b);
                this.d = J2;
                return;
            }
            do {
                list.add(z ? H() : v());
                if (this.a.f()) {
                    return;
                }
                J = this.a.J();
            } while (J == this.b);
            this.d = J;
            return;
        }
        throw InvalidProtocolBufferException.invalidWireType();
    }

    public final void W(int i) throws IOException {
        if (this.a.e() != i) {
            throw InvalidProtocolBufferException.truncatedMessage();
        }
    }

    public final void X(int i) throws IOException {
        if (WireFormat.b(this.b) != i) {
            throw InvalidProtocolBufferException.invalidWireType();
        }
    }

    public final void Y(int i) throws IOException {
        if ((i & 3) != 0) {
            throw InvalidProtocolBufferException.parseFailure();
        }
    }

    public final void Z(int i) throws IOException {
        if ((i & 7) != 0) {
            throw InvalidProtocolBufferException.parseFailure();
        }
    }

    @Override // com.google.protobuf.w0
    public int a() {
        return this.b;
    }

    @Override // com.google.protobuf.w0
    public void b(List<Integer> list) throws IOException {
        int J;
        int J2;
        if (list instanceof z) {
            z zVar = (z) list;
            int b = WireFormat.b(this.b);
            if (b == 0) {
                do {
                    zVar.X(this.a.F());
                    if (this.a.f()) {
                        return;
                    }
                    J2 = this.a.J();
                } while (J2 == this.b);
                this.d = J2;
                return;
            } else if (b == 2) {
                int e = this.a.e() + this.a.K();
                do {
                    zVar.X(this.a.F());
                } while (this.a.e() < e);
                W(e);
                return;
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }
        int b2 = WireFormat.b(this.b);
        if (b2 == 0) {
            do {
                list.add(Integer.valueOf(this.a.F()));
                if (this.a.f()) {
                    return;
                }
                J = this.a.J();
            } while (J == this.b);
            this.d = J;
        } else if (b2 == 2) {
            int e2 = this.a.e() + this.a.K();
            do {
                list.add(Integer.valueOf(this.a.F()));
            } while (this.a.e() < e2);
            W(e2);
        } else {
            throw InvalidProtocolBufferException.invalidWireType();
        }
    }

    @Override // com.google.protobuf.w0
    public long c() throws IOException {
        X(0);
        return this.a.L();
    }

    @Override // com.google.protobuf.w0
    public long d() throws IOException {
        X(1);
        return this.a.u();
    }

    @Override // com.google.protobuf.w0
    public void e(List<Integer> list) throws IOException {
        int J;
        int J2;
        if (list instanceof z) {
            z zVar = (z) list;
            int b = WireFormat.b(this.b);
            if (b == 2) {
                int K = this.a.K();
                Y(K);
                int e = this.a.e() + K;
                do {
                    zVar.X(this.a.D());
                } while (this.a.e() < e);
                return;
            } else if (b == 5) {
                do {
                    zVar.X(this.a.D());
                    if (this.a.f()) {
                        return;
                    }
                    J2 = this.a.J();
                } while (J2 == this.b);
                this.d = J2;
                return;
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }
        int b2 = WireFormat.b(this.b);
        if (b2 == 2) {
            int K2 = this.a.K();
            Y(K2);
            int e2 = this.a.e() + K2;
            do {
                list.add(Integer.valueOf(this.a.D()));
            } while (this.a.e() < e2);
        } else if (b2 == 5) {
            do {
                list.add(Integer.valueOf(this.a.D()));
                if (this.a.f()) {
                    return;
                }
                J = this.a.J();
            } while (J == this.b);
            this.d = J;
        } else {
            throw InvalidProtocolBufferException.invalidWireType();
        }
    }

    @Override // com.google.protobuf.w0
    public void f(List<Long> list) throws IOException {
        int J;
        int J2;
        if (list instanceof f0) {
            f0 f0Var = (f0) list;
            int b = WireFormat.b(this.b);
            if (b == 0) {
                do {
                    f0Var.n(this.a.G());
                    if (this.a.f()) {
                        return;
                    }
                    J2 = this.a.J();
                } while (J2 == this.b);
                this.d = J2;
                return;
            } else if (b == 2) {
                int e = this.a.e() + this.a.K();
                do {
                    f0Var.n(this.a.G());
                } while (this.a.e() < e);
                W(e);
                return;
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }
        int b2 = WireFormat.b(this.b);
        if (b2 == 0) {
            do {
                list.add(Long.valueOf(this.a.G()));
                if (this.a.f()) {
                    return;
                }
                J = this.a.J();
            } while (J == this.b);
            this.d = J;
        } else if (b2 == 2) {
            int e2 = this.a.e() + this.a.K();
            do {
                list.add(Long.valueOf(this.a.G()));
            } while (this.a.e() < e2);
            W(e2);
        } else {
            throw InvalidProtocolBufferException.invalidWireType();
        }
    }

    @Override // com.google.protobuf.w0
    public void g(List<Integer> list) throws IOException {
        int J;
        int J2;
        if (list instanceof z) {
            z zVar = (z) list;
            int b = WireFormat.b(this.b);
            if (b == 0) {
                do {
                    zVar.X(this.a.K());
                    if (this.a.f()) {
                        return;
                    }
                    J2 = this.a.J();
                } while (J2 == this.b);
                this.d = J2;
                return;
            } else if (b == 2) {
                int e = this.a.e() + this.a.K();
                do {
                    zVar.X(this.a.K());
                } while (this.a.e() < e);
                W(e);
                return;
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }
        int b2 = WireFormat.b(this.b);
        if (b2 == 0) {
            do {
                list.add(Integer.valueOf(this.a.K()));
                if (this.a.f()) {
                    return;
                }
                J = this.a.J();
            } while (J == this.b);
            this.d = J;
        } else if (b2 == 2) {
            int e2 = this.a.e() + this.a.K();
            do {
                list.add(Integer.valueOf(this.a.K()));
            } while (this.a.e() < e2);
            W(e2);
        } else {
            throw InvalidProtocolBufferException.invalidWireType();
        }
    }

    @Override // com.google.protobuf.w0
    public int h() throws IOException {
        X(5);
        return this.a.t();
    }

    @Override // com.google.protobuf.w0
    public boolean i() throws IOException {
        X(0);
        return this.a.p();
    }

    @Override // com.google.protobuf.w0
    public long j() throws IOException {
        X(1);
        return this.a.E();
    }

    @Override // com.google.protobuf.w0
    public void k(List<Long> list) throws IOException {
        int J;
        int J2;
        if (list instanceof f0) {
            f0 f0Var = (f0) list;
            int b = WireFormat.b(this.b);
            if (b == 0) {
                do {
                    f0Var.n(this.a.L());
                    if (this.a.f()) {
                        return;
                    }
                    J2 = this.a.J();
                } while (J2 == this.b);
                this.d = J2;
                return;
            } else if (b == 2) {
                int e = this.a.e() + this.a.K();
                do {
                    f0Var.n(this.a.L());
                } while (this.a.e() < e);
                W(e);
                return;
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }
        int b2 = WireFormat.b(this.b);
        if (b2 == 0) {
            do {
                list.add(Long.valueOf(this.a.L()));
                if (this.a.f()) {
                    return;
                }
                J = this.a.J();
            } while (J == this.b);
            this.d = J;
        } else if (b2 == 2) {
            int e2 = this.a.e() + this.a.K();
            do {
                list.add(Long.valueOf(this.a.L()));
            } while (this.a.e() < e2);
            W(e2);
        } else {
            throw InvalidProtocolBufferException.invalidWireType();
        }
    }

    @Override // com.google.protobuf.w0
    public int l() throws IOException {
        X(0);
        return this.a.K();
    }

    @Override // com.google.protobuf.w0
    public void m(List<Long> list) throws IOException {
        int J;
        int J2;
        if (list instanceof f0) {
            f0 f0Var = (f0) list;
            int b = WireFormat.b(this.b);
            if (b == 0) {
                do {
                    f0Var.n(this.a.y());
                    if (this.a.f()) {
                        return;
                    }
                    J2 = this.a.J();
                } while (J2 == this.b);
                this.d = J2;
                return;
            } else if (b == 2) {
                int e = this.a.e() + this.a.K();
                do {
                    f0Var.n(this.a.y());
                } while (this.a.e() < e);
                W(e);
                return;
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }
        int b2 = WireFormat.b(this.b);
        if (b2 == 0) {
            do {
                list.add(Long.valueOf(this.a.y()));
                if (this.a.f()) {
                    return;
                }
                J = this.a.J();
            } while (J == this.b);
            this.d = J;
        } else if (b2 == 2) {
            int e2 = this.a.e() + this.a.K();
            do {
                list.add(Long.valueOf(this.a.y()));
            } while (this.a.e() < e2);
            W(e2);
        } else {
            throw InvalidProtocolBufferException.invalidWireType();
        }
    }

    @Override // com.google.protobuf.w0
    public void n(List<Long> list) throws IOException {
        int J;
        int J2;
        if (list instanceof f0) {
            f0 f0Var = (f0) list;
            int b = WireFormat.b(this.b);
            if (b == 1) {
                do {
                    f0Var.n(this.a.E());
                    if (this.a.f()) {
                        return;
                    }
                    J2 = this.a.J();
                } while (J2 == this.b);
                this.d = J2;
                return;
            } else if (b == 2) {
                int K = this.a.K();
                Z(K);
                int e = this.a.e() + K;
                do {
                    f0Var.n(this.a.E());
                } while (this.a.e() < e);
                return;
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }
        int b2 = WireFormat.b(this.b);
        if (b2 == 1) {
            do {
                list.add(Long.valueOf(this.a.E()));
                if (this.a.f()) {
                    return;
                }
                J = this.a.J();
            } while (J == this.b);
            this.d = J;
        } else if (b2 == 2) {
            int K2 = this.a.K();
            Z(K2);
            int e2 = this.a.e() + K2;
            do {
                list.add(Long.valueOf(this.a.E()));
            } while (this.a.e() < e2);
        } else {
            throw InvalidProtocolBufferException.invalidWireType();
        }
    }

    @Override // com.google.protobuf.w0
    public void o(List<Integer> list) throws IOException {
        int J;
        int J2;
        if (list instanceof z) {
            z zVar = (z) list;
            int b = WireFormat.b(this.b);
            if (b == 0) {
                do {
                    zVar.X(this.a.x());
                    if (this.a.f()) {
                        return;
                    }
                    J2 = this.a.J();
                } while (J2 == this.b);
                this.d = J2;
                return;
            } else if (b == 2) {
                int e = this.a.e() + this.a.K();
                do {
                    zVar.X(this.a.x());
                } while (this.a.e() < e);
                W(e);
                return;
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }
        int b2 = WireFormat.b(this.b);
        if (b2 == 0) {
            do {
                list.add(Integer.valueOf(this.a.x()));
                if (this.a.f()) {
                    return;
                }
                J = this.a.J();
            } while (J == this.b);
            this.d = J;
        } else if (b2 == 2) {
            int e2 = this.a.e() + this.a.K();
            do {
                list.add(Integer.valueOf(this.a.x()));
            } while (this.a.e() < e2);
            W(e2);
        } else {
            throw InvalidProtocolBufferException.invalidWireType();
        }
    }

    @Override // com.google.protobuf.w0
    public void p(List<Integer> list) throws IOException {
        int J;
        int J2;
        if (list instanceof z) {
            z zVar = (z) list;
            int b = WireFormat.b(this.b);
            if (b == 0) {
                do {
                    zVar.X(this.a.s());
                    if (this.a.f()) {
                        return;
                    }
                    J2 = this.a.J();
                } while (J2 == this.b);
                this.d = J2;
                return;
            } else if (b == 2) {
                int e = this.a.e() + this.a.K();
                do {
                    zVar.X(this.a.s());
                } while (this.a.e() < e);
                W(e);
                return;
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }
        int b2 = WireFormat.b(this.b);
        if (b2 == 0) {
            do {
                list.add(Integer.valueOf(this.a.s()));
                if (this.a.f()) {
                    return;
                }
                J = this.a.J();
            } while (J == this.b);
            this.d = J;
        } else if (b2 == 2) {
            int e2 = this.a.e() + this.a.K();
            do {
                list.add(Integer.valueOf(this.a.s()));
            } while (this.a.e() < e2);
            W(e2);
        } else {
            throw InvalidProtocolBufferException.invalidWireType();
        }
    }

    @Override // com.google.protobuf.w0
    public int q() throws IOException {
        X(0);
        return this.a.s();
    }

    @Override // com.google.protobuf.w0
    public void r(List<Integer> list) throws IOException {
        int J;
        int J2;
        if (list instanceof z) {
            z zVar = (z) list;
            int b = WireFormat.b(this.b);
            if (b == 2) {
                int K = this.a.K();
                Y(K);
                int e = this.a.e() + K;
                do {
                    zVar.X(this.a.t());
                } while (this.a.e() < e);
                return;
            } else if (b == 5) {
                do {
                    zVar.X(this.a.t());
                    if (this.a.f()) {
                        return;
                    }
                    J2 = this.a.J();
                } while (J2 == this.b);
                this.d = J2;
                return;
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }
        int b2 = WireFormat.b(this.b);
        if (b2 == 2) {
            int K2 = this.a.K();
            Y(K2);
            int e2 = this.a.e() + K2;
            do {
                list.add(Integer.valueOf(this.a.t()));
            } while (this.a.e() < e2);
        } else if (b2 == 5) {
            do {
                list.add(Integer.valueOf(this.a.t()));
                if (this.a.f()) {
                    return;
                }
                J = this.a.J();
            } while (J == this.b);
            this.d = J;
        } else {
            throw InvalidProtocolBufferException.invalidWireType();
        }
    }

    @Override // com.google.protobuf.w0
    public double readDouble() throws IOException {
        X(1);
        return this.a.r();
    }

    @Override // com.google.protobuf.w0
    public float readFloat() throws IOException {
        X(5);
        return this.a.v();
    }

    @Override // com.google.protobuf.w0
    public int s() throws IOException {
        X(0);
        return this.a.F();
    }

    @Override // com.google.protobuf.w0
    public long t() throws IOException {
        X(0);
        return this.a.G();
    }

    @Override // com.google.protobuf.w0
    public void u(List<Boolean> list) throws IOException {
        int J;
        int J2;
        if (list instanceof g) {
            g gVar = (g) list;
            int b = WireFormat.b(this.b);
            if (b == 0) {
                do {
                    gVar.n(this.a.p());
                    if (this.a.f()) {
                        return;
                    }
                    J2 = this.a.J();
                } while (J2 == this.b);
                this.d = J2;
                return;
            } else if (b == 2) {
                int e = this.a.e() + this.a.K();
                do {
                    gVar.n(this.a.p());
                } while (this.a.e() < e);
                W(e);
                return;
            } else {
                throw InvalidProtocolBufferException.invalidWireType();
            }
        }
        int b2 = WireFormat.b(this.b);
        if (b2 == 0) {
            do {
                list.add(Boolean.valueOf(this.a.p()));
                if (this.a.f()) {
                    return;
                }
                J = this.a.J();
            } while (J == this.b);
            this.d = J;
        } else if (b2 == 2) {
            int e2 = this.a.e() + this.a.K();
            do {
                list.add(Boolean.valueOf(this.a.p()));
            } while (this.a.e() < e2);
            W(e2);
        } else {
            throw InvalidProtocolBufferException.invalidWireType();
        }
    }

    @Override // com.google.protobuf.w0
    public String v() throws IOException {
        X(2);
        return this.a.H();
    }

    @Override // com.google.protobuf.w0
    public int w() throws IOException {
        int i = this.d;
        if (i != 0) {
            this.b = i;
            this.d = 0;
        } else {
            this.b = this.a.J();
        }
        int i2 = this.b;
        if (i2 == 0 || i2 == this.c) {
            return Integer.MAX_VALUE;
        }
        return WireFormat.a(i2);
    }

    @Override // com.google.protobuf.w0
    public void x(List<String> list) throws IOException {
        V(list, false);
    }

    @Override // com.google.protobuf.w0
    public void y(List<String> list) throws IOException {
        V(list, true);
    }

    @Override // com.google.protobuf.w0
    public ByteString z() throws IOException {
        X(2);
        return this.a.q();
    }
}
