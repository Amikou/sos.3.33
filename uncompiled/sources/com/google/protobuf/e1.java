package com.google.protobuf;

import com.google.protobuf.Descriptors;
import java.util.Collections;
import java.util.Map;
import java.util.logging.Logger;

/* compiled from: TypeRegistry.java */
/* loaded from: classes2.dex */
public class e1 {
    public final Map<String, Descriptors.b> a;

    /* compiled from: TypeRegistry.java */
    /* loaded from: classes2.dex */
    public static class a {
        public static final e1 a = new e1(Collections.emptyMap());
    }

    static {
        Logger.getLogger(e1.class.getName());
    }

    public e1(Map<String, Descriptors.b> map) {
        this.a = map;
    }

    public static e1 c() {
        return a.a;
    }

    public static String d(String str) throws InvalidProtocolBufferException {
        String[] split = str.split("/");
        if (split.length != 1) {
            return split[split.length - 1];
        }
        throw new InvalidProtocolBufferException("Invalid type url found: " + str);
    }

    public Descriptors.b a(String str) {
        return this.a.get(str);
    }

    public final Descriptors.b b(String str) throws InvalidProtocolBufferException {
        return a(d(str));
    }
}
