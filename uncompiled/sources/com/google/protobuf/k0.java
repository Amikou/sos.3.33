package com.google.protobuf;

import com.google.protobuf.h0;
import java.util.Map;

/* compiled from: MapFieldSchemaLite.java */
/* loaded from: classes2.dex */
public class k0 implements i0 {
    public static <K, V> int i(int i, Object obj, Object obj2) {
        MapFieldLite mapFieldLite = (MapFieldLite) obj;
        h0 h0Var = (h0) obj2;
        int i2 = 0;
        if (mapFieldLite.isEmpty()) {
            return 0;
        }
        for (Map.Entry<K, V> entry : mapFieldLite.entrySet()) {
            i2 += h0Var.a(i, entry.getKey(), entry.getValue());
        }
        return i2;
    }

    public static <K, V> MapFieldLite<K, V> j(Object obj, Object obj2) {
        MapFieldLite<K, V> mapFieldLite = (MapFieldLite) obj;
        MapFieldLite<K, V> mapFieldLite2 = (MapFieldLite) obj2;
        if (!mapFieldLite2.isEmpty()) {
            if (!mapFieldLite.isMutable()) {
                mapFieldLite = mapFieldLite.mutableCopy();
            }
            mapFieldLite.mergeFrom(mapFieldLite2);
        }
        return mapFieldLite;
    }

    @Override // com.google.protobuf.i0
    public Object a(Object obj, Object obj2) {
        return j(obj, obj2);
    }

    @Override // com.google.protobuf.i0
    public Object b(Object obj) {
        ((MapFieldLite) obj).makeImmutable();
        return obj;
    }

    @Override // com.google.protobuf.i0
    public h0.b<?, ?> c(Object obj) {
        return ((h0) obj).c();
    }

    @Override // com.google.protobuf.i0
    public Map<?, ?> d(Object obj) {
        return (MapFieldLite) obj;
    }

    @Override // com.google.protobuf.i0
    public Object e(Object obj) {
        return MapFieldLite.emptyMapField().mutableCopy();
    }

    @Override // com.google.protobuf.i0
    public int f(int i, Object obj, Object obj2) {
        return i(i, obj, obj2);
    }

    @Override // com.google.protobuf.i0
    public boolean g(Object obj) {
        return !((MapFieldLite) obj).isMutable();
    }

    @Override // com.google.protobuf.i0
    public Map<?, ?> h(Object obj) {
        return (MapFieldLite) obj;
    }
}
