package com.google.protobuf;

import com.google.protobuf.a0;
import java.util.AbstractList;
import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

/* compiled from: FloatArrayList.java */
/* loaded from: classes2.dex */
public final class x extends d<Float> implements a0.f, RandomAccess, uu2 {
    public static final x h0;
    public float[] f0;
    public int g0;

    static {
        x xVar = new x(new float[0], 0);
        h0 = xVar;
        xVar.l();
    }

    public x() {
        this(new float[10], 0);
    }

    public static x o() {
        return h0;
    }

    @Override // com.google.protobuf.d, java.util.AbstractCollection, java.util.Collection, java.util.List
    public boolean addAll(Collection<? extends Float> collection) {
        e();
        a0.a(collection);
        if (!(collection instanceof x)) {
            return super.addAll(collection);
        }
        x xVar = (x) collection;
        int i = xVar.g0;
        if (i == 0) {
            return false;
        }
        int i2 = this.g0;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            float[] fArr = this.f0;
            if (i3 > fArr.length) {
                this.f0 = Arrays.copyOf(fArr, i3);
            }
            System.arraycopy(xVar.f0, 0, this.f0, this.g0, xVar.g0);
            this.g0 = i3;
            ((AbstractList) this).modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public boolean contains(Object obj) {
        return indexOf(obj) != -1;
    }

    @Override // com.google.protobuf.d, java.util.AbstractList, java.util.Collection, java.util.List
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof x)) {
            return super.equals(obj);
        }
        x xVar = (x) obj;
        if (this.g0 != xVar.g0) {
            return false;
        }
        float[] fArr = xVar.f0;
        for (int i = 0; i < this.g0; i++) {
            if (Float.floatToIntBits(this.f0[i]) != Float.floatToIntBits(fArr[i])) {
                return false;
            }
        }
        return true;
    }

    @Override // com.google.protobuf.d, java.util.AbstractList, java.util.Collection, java.util.List
    public int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.g0; i2++) {
            i = (i * 31) + Float.floatToIntBits(this.f0[i2]);
        }
        return i;
    }

    @Override // java.util.AbstractList, java.util.List
    /* renamed from: i */
    public void add(int i, Float f) {
        n(i, f.floatValue());
    }

    @Override // java.util.AbstractList, java.util.List
    public int indexOf(Object obj) {
        if (obj instanceof Float) {
            float floatValue = ((Float) obj).floatValue();
            int size = size();
            for (int i = 0; i < size; i++) {
                if (this.f0[i] == floatValue) {
                    return i;
                }
            }
            return -1;
        }
        return -1;
    }

    @Override // com.google.protobuf.d, java.util.AbstractList, java.util.AbstractCollection, java.util.Collection, java.util.List
    /* renamed from: k */
    public boolean add(Float f) {
        m(f.floatValue());
        return true;
    }

    public void m(float f) {
        e();
        int i = this.g0;
        float[] fArr = this.f0;
        if (i == fArr.length) {
            float[] fArr2 = new float[((i * 3) / 2) + 1];
            System.arraycopy(fArr, 0, fArr2, 0, i);
            this.f0 = fArr2;
        }
        float[] fArr3 = this.f0;
        int i2 = this.g0;
        this.g0 = i2 + 1;
        fArr3[i2] = f;
    }

    public final void n(int i, float f) {
        int i2;
        e();
        if (i >= 0 && i <= (i2 = this.g0)) {
            float[] fArr = this.f0;
            if (i2 < fArr.length) {
                System.arraycopy(fArr, i, fArr, i + 1, i2 - i);
            } else {
                float[] fArr2 = new float[((i2 * 3) / 2) + 1];
                System.arraycopy(fArr, 0, fArr2, 0, i);
                System.arraycopy(this.f0, i, fArr2, i + 1, this.g0 - i);
                this.f0 = fArr2;
            }
            this.f0[i] = f;
            this.g0++;
            ((AbstractList) this).modCount++;
            return;
        }
        throw new IndexOutOfBoundsException(t(i));
    }

    public final void p(int i) {
        if (i < 0 || i >= this.g0) {
            throw new IndexOutOfBoundsException(t(i));
        }
    }

    @Override // java.util.AbstractList, java.util.List
    /* renamed from: q */
    public Float get(int i) {
        return Float.valueOf(s(i));
    }

    @Override // java.util.AbstractList
    public void removeRange(int i, int i2) {
        e();
        if (i2 >= i) {
            float[] fArr = this.f0;
            System.arraycopy(fArr, i2, fArr, i, this.g0 - i2);
            this.g0 -= i2 - i;
            ((AbstractList) this).modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    public float s(int i) {
        p(i);
        return this.f0[i];
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public int size() {
        return this.g0;
    }

    public final String t(int i) {
        return "Index:" + i + ", Size:" + this.g0;
    }

    @Override // java.util.AbstractList, java.util.List
    /* renamed from: w */
    public Float remove(int i) {
        int i2;
        e();
        p(i);
        float[] fArr = this.f0;
        float f = fArr[i];
        if (i < this.g0 - 1) {
            System.arraycopy(fArr, i + 1, fArr, i, (i2 - i) - 1);
        }
        this.g0--;
        ((AbstractList) this).modCount++;
        return Float.valueOf(f);
    }

    @Override // java.util.AbstractList, java.util.List
    /* renamed from: y */
    public Float set(int i, Float f) {
        return Float.valueOf(z(i, f.floatValue()));
    }

    public float z(int i, float f) {
        e();
        p(i);
        float[] fArr = this.f0;
        float f2 = fArr[i];
        fArr[i] = f;
        return f2;
    }

    public x(float[] fArr, int i) {
        this.f0 = fArr;
        this.g0 = i;
    }

    @Override // com.google.protobuf.a0.i
    /* renamed from: a */
    public a0.i<Float> a2(int i) {
        if (i >= this.g0) {
            return new x(Arrays.copyOf(this.f0, i), this.g0);
        }
        throw new IllegalArgumentException();
    }

    @Override // com.google.protobuf.d, java.util.AbstractCollection, java.util.Collection, java.util.List
    public boolean remove(Object obj) {
        e();
        for (int i = 0; i < this.g0; i++) {
            if (obj.equals(Float.valueOf(this.f0[i]))) {
                float[] fArr = this.f0;
                System.arraycopy(fArr, i + 1, fArr, i, (this.g0 - i) - 1);
                this.g0--;
                ((AbstractList) this).modCount++;
                return true;
            }
        }
        return false;
    }
}
