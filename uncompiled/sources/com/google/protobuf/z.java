package com.google.protobuf;

import com.google.protobuf.a0;
import java.util.AbstractList;
import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

/* compiled from: IntArrayList.java */
/* loaded from: classes2.dex */
public final class z extends d<Integer> implements a0.g, RandomAccess, uu2 {
    public static final z h0;
    public int[] f0;
    public int g0;

    static {
        z zVar = new z(new int[0], 0);
        h0 = zVar;
        zVar.l();
    }

    public z() {
        this(new int[10], 0);
    }

    public static z n() {
        return h0;
    }

    @Override // com.google.protobuf.a0.g
    public void X(int i) {
        e();
        int i2 = this.g0;
        int[] iArr = this.f0;
        if (i2 == iArr.length) {
            int[] iArr2 = new int[((i2 * 3) / 2) + 1];
            System.arraycopy(iArr, 0, iArr2, 0, i2);
            this.f0 = iArr2;
        }
        int[] iArr3 = this.f0;
        int i3 = this.g0;
        this.g0 = i3 + 1;
        iArr3[i3] = i;
    }

    @Override // com.google.protobuf.d, java.util.AbstractCollection, java.util.Collection, java.util.List
    public boolean addAll(Collection<? extends Integer> collection) {
        e();
        a0.a(collection);
        if (!(collection instanceof z)) {
            return super.addAll(collection);
        }
        z zVar = (z) collection;
        int i = zVar.g0;
        if (i == 0) {
            return false;
        }
        int i2 = this.g0;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            int[] iArr = this.f0;
            if (i3 > iArr.length) {
                this.f0 = Arrays.copyOf(iArr, i3);
            }
            System.arraycopy(zVar.f0, 0, this.f0, this.g0, zVar.g0);
            this.g0 = i3;
            ((AbstractList) this).modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public boolean contains(Object obj) {
        return indexOf(obj) != -1;
    }

    @Override // com.google.protobuf.d, java.util.AbstractList, java.util.Collection, java.util.List
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof z)) {
            return super.equals(obj);
        }
        z zVar = (z) obj;
        if (this.g0 != zVar.g0) {
            return false;
        }
        int[] iArr = zVar.f0;
        for (int i = 0; i < this.g0; i++) {
            if (this.f0[i] != iArr[i]) {
                return false;
            }
        }
        return true;
    }

    @Override // com.google.protobuf.a0.g
    public int getInt(int i) {
        o(i);
        return this.f0[i];
    }

    @Override // com.google.protobuf.d, java.util.AbstractList, java.util.Collection, java.util.List
    public int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.g0; i2++) {
            i = (i * 31) + this.f0[i2];
        }
        return i;
    }

    @Override // java.util.AbstractList, java.util.List
    /* renamed from: i */
    public void add(int i, Integer num) {
        m(i, num.intValue());
    }

    @Override // java.util.AbstractList, java.util.List
    public int indexOf(Object obj) {
        if (obj instanceof Integer) {
            int intValue = ((Integer) obj).intValue();
            int size = size();
            for (int i = 0; i < size; i++) {
                if (this.f0[i] == intValue) {
                    return i;
                }
            }
            return -1;
        }
        return -1;
    }

    @Override // com.google.protobuf.d, java.util.AbstractList, java.util.AbstractCollection, java.util.Collection, java.util.List
    /* renamed from: k */
    public boolean add(Integer num) {
        X(num.intValue());
        return true;
    }

    public final void m(int i, int i2) {
        int i3;
        e();
        if (i >= 0 && i <= (i3 = this.g0)) {
            int[] iArr = this.f0;
            if (i3 < iArr.length) {
                System.arraycopy(iArr, i, iArr, i + 1, i3 - i);
            } else {
                int[] iArr2 = new int[((i3 * 3) / 2) + 1];
                System.arraycopy(iArr, 0, iArr2, 0, i);
                System.arraycopy(this.f0, i, iArr2, i + 1, this.g0 - i);
                this.f0 = iArr2;
            }
            this.f0[i] = i2;
            this.g0++;
            ((AbstractList) this).modCount++;
            return;
        }
        throw new IndexOutOfBoundsException(q(i));
    }

    public final void o(int i) {
        if (i < 0 || i >= this.g0) {
            throw new IndexOutOfBoundsException(q(i));
        }
    }

    @Override // java.util.AbstractList, java.util.List
    /* renamed from: p */
    public Integer get(int i) {
        return Integer.valueOf(getInt(i));
    }

    public final String q(int i) {
        return "Index:" + i + ", Size:" + this.g0;
    }

    @Override // java.util.AbstractList
    public void removeRange(int i, int i2) {
        e();
        if (i2 >= i) {
            int[] iArr = this.f0;
            System.arraycopy(iArr, i2, iArr, i, this.g0 - i2);
            this.g0 -= i2 - i;
            ((AbstractList) this).modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    @Override // java.util.AbstractList, java.util.List
    /* renamed from: s */
    public Integer remove(int i) {
        int i2;
        e();
        o(i);
        int[] iArr = this.f0;
        int i3 = iArr[i];
        if (i < this.g0 - 1) {
            System.arraycopy(iArr, i + 1, iArr, i, (i2 - i) - 1);
        }
        this.g0--;
        ((AbstractList) this).modCount++;
        return Integer.valueOf(i3);
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public int size() {
        return this.g0;
    }

    @Override // java.util.AbstractList, java.util.List
    /* renamed from: t */
    public Integer set(int i, Integer num) {
        return Integer.valueOf(w(i, num.intValue()));
    }

    public int w(int i, int i2) {
        e();
        o(i);
        int[] iArr = this.f0;
        int i3 = iArr[i];
        iArr[i] = i2;
        return i3;
    }

    public z(int[] iArr, int i) {
        this.f0 = iArr;
        this.g0 = i;
    }

    @Override // com.google.protobuf.a0.i
    /* renamed from: a */
    public a0.i<Integer> a2(int i) {
        if (i >= this.g0) {
            return new z(Arrays.copyOf(this.f0, i), this.g0);
        }
        throw new IllegalArgumentException();
    }

    @Override // com.google.protobuf.d, java.util.AbstractCollection, java.util.Collection, java.util.List
    public boolean remove(Object obj) {
        e();
        for (int i = 0; i < this.g0; i++) {
            if (obj.equals(Integer.valueOf(this.f0[i]))) {
                int[] iArr = this.f0;
                System.arraycopy(iArr, i + 1, iArr, i, (this.g0 - i) - 1);
                this.g0--;
                ((AbstractList) this).modCount++;
                return true;
            }
        }
        return false;
    }
}
