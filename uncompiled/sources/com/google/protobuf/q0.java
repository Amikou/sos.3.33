package com.google.protobuf;

import com.google.protobuf.WireFormat;
import com.google.protobuf.b0;
import com.google.protobuf.w;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

/* compiled from: MessageSetSchema.java */
/* loaded from: classes2.dex */
public final class q0<T> implements y0<T> {
    public final m0 a;
    public final f1<?, ?> b;
    public final boolean c;
    public final s<?> d;

    public q0(f1<?, ?> f1Var, s<?> sVar, m0 m0Var) {
        this.b = f1Var;
        this.c = sVar.e(m0Var);
        this.d = sVar;
        this.a = m0Var;
    }

    public static <T> q0<T> m(f1<?, ?> f1Var, s<?> sVar, m0 m0Var) {
        return new q0<>(f1Var, sVar, m0Var);
    }

    @Override // com.google.protobuf.y0
    public void a(T t, T t2) {
        z0.H(this.b, t, t2);
        if (this.c) {
            z0.F(this.d, t, t2);
        }
    }

    @Override // com.google.protobuf.y0
    public boolean b(T t, T t2) {
        if (this.b.g(t).equals(this.b.g(t2))) {
            if (this.c) {
                return this.d.c(t).equals(this.d.c(t2));
            }
            return true;
        }
        return false;
    }

    @Override // com.google.protobuf.y0
    public T c() {
        return (T) this.a.newBuilderForType().buildPartial();
    }

    @Override // com.google.protobuf.y0
    public int d(T t) {
        int hashCode = this.b.g(t).hashCode();
        return this.c ? (hashCode * 53) + this.d.c(t).hashCode() : hashCode;
    }

    @Override // com.google.protobuf.y0
    public void e(T t) {
        this.b.j(t);
        this.d.f(t);
    }

    @Override // com.google.protobuf.y0
    public final boolean f(T t) {
        return this.d.c(t).D();
    }

    @Override // com.google.protobuf.y0
    public int g(T t) {
        int k = k(this.b, t) + 0;
        return this.c ? k + this.d.c(t).u() : k;
    }

    @Override // com.google.protobuf.y0
    public void h(T t, Writer writer) throws IOException {
        Iterator<Map.Entry<?, Object>> G = this.d.c(t).G();
        while (G.hasNext()) {
            Map.Entry<?, Object> next = G.next();
            w.c cVar = (w.c) next.getKey();
            if (cVar.z() == WireFormat.JavaType.MESSAGE && !cVar.i() && !cVar.isPacked()) {
                if (next instanceof b0.b) {
                    writer.c(cVar.getNumber(), ((b0.b) next).a().f());
                } else {
                    writer.c(cVar.getNumber(), next.getValue());
                }
            } else {
                throw new IllegalStateException("Found invalid MessageSet item.");
            }
        }
        o(this.b, t, writer);
    }

    @Override // com.google.protobuf.y0
    public void i(T t, w0 w0Var, r rVar) throws IOException {
        l(this.b, this.d, t, w0Var, rVar);
    }

    /* JADX WARN: Removed duplicated region for block: B:33:0x00c6  */
    /* JADX WARN: Removed duplicated region for block: B:57:0x00cb A[EDGE_INSN: B:57:0x00cb->B:34:0x00cb ?: BREAK  , SYNTHETIC] */
    @Override // com.google.protobuf.y0
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void j(T r11, byte[] r12, int r13, int r14, com.google.protobuf.e.b r15) throws java.io.IOException {
        /*
            r10 = this;
            r0 = r11
            com.google.protobuf.GeneratedMessageLite r0 = (com.google.protobuf.GeneratedMessageLite) r0
            com.google.protobuf.h1 r1 = r0.a
            com.google.protobuf.h1 r2 = com.google.protobuf.h1.c()
            if (r1 != r2) goto L11
            com.google.protobuf.h1 r1 = com.google.protobuf.h1.j()
            r0.a = r1
        L11:
            com.google.protobuf.GeneratedMessageLite$b r11 = (com.google.protobuf.GeneratedMessageLite.b) r11
            com.google.protobuf.w r11 = r11.k()
            r0 = 0
            r2 = r0
        L19:
            if (r13 >= r14) goto Ld7
            int r4 = com.google.protobuf.e.I(r12, r13, r15)
            int r13 = r15.a
            int r3 = com.google.protobuf.WireFormat.a
            r5 = 2
            if (r13 == r3) goto L6b
            int r3 = com.google.protobuf.WireFormat.b(r13)
            if (r3 != r5) goto L66
            com.google.protobuf.s<?> r2 = r10.d
            com.google.protobuf.r r3 = r15.d
            com.google.protobuf.m0 r5 = r10.a
            int r6 = com.google.protobuf.WireFormat.a(r13)
            java.lang.Object r2 = r2.b(r3, r5, r6)
            r8 = r2
            com.google.protobuf.GeneratedMessageLite$d r8 = (com.google.protobuf.GeneratedMessageLite.d) r8
            if (r8 == 0) goto L5b
            com.google.protobuf.u0 r13 = com.google.protobuf.u0.a()
            com.google.protobuf.m0 r2 = r8.c()
            java.lang.Class r2 = r2.getClass()
            com.google.protobuf.y0 r13 = r13.d(r2)
            int r13 = com.google.protobuf.e.p(r13, r12, r4, r14, r15)
            com.google.protobuf.GeneratedMessageLite$c r2 = r8.b
            java.lang.Object r3 = r15.c
            r11.N(r2, r3)
            goto L64
        L5b:
            r2 = r13
            r3 = r12
            r5 = r14
            r6 = r1
            r7 = r15
            int r13 = com.google.protobuf.e.G(r2, r3, r4, r5, r6, r7)
        L64:
            r2 = r8
            goto L19
        L66:
            int r13 = com.google.protobuf.e.N(r13, r12, r4, r14, r15)
            goto L19
        L6b:
            r13 = 0
            r3 = r0
        L6d:
            if (r4 >= r14) goto Lcb
            int r4 = com.google.protobuf.e.I(r12, r4, r15)
            int r6 = r15.a
            int r7 = com.google.protobuf.WireFormat.a(r6)
            int r8 = com.google.protobuf.WireFormat.b(r6)
            if (r7 == r5) goto Lac
            r9 = 3
            if (r7 == r9) goto L83
            goto Lc1
        L83:
            if (r2 == 0) goto La1
            com.google.protobuf.u0 r6 = com.google.protobuf.u0.a()
            com.google.protobuf.m0 r7 = r2.c()
            java.lang.Class r7 = r7.getClass()
            com.google.protobuf.y0 r6 = r6.d(r7)
            int r4 = com.google.protobuf.e.p(r6, r12, r4, r14, r15)
            com.google.protobuf.GeneratedMessageLite$c r6 = r2.b
            java.lang.Object r7 = r15.c
            r11.N(r6, r7)
            goto L6d
        La1:
            if (r8 != r5) goto Lc1
            int r4 = com.google.protobuf.e.b(r12, r4, r15)
            java.lang.Object r3 = r15.c
            com.google.protobuf.ByteString r3 = (com.google.protobuf.ByteString) r3
            goto L6d
        Lac:
            if (r8 != 0) goto Lc1
            int r4 = com.google.protobuf.e.I(r12, r4, r15)
            int r13 = r15.a
            com.google.protobuf.s<?> r2 = r10.d
            com.google.protobuf.r r6 = r15.d
            com.google.protobuf.m0 r7 = r10.a
            java.lang.Object r2 = r2.b(r6, r7, r13)
            com.google.protobuf.GeneratedMessageLite$d r2 = (com.google.protobuf.GeneratedMessageLite.d) r2
            goto L6d
        Lc1:
            int r7 = com.google.protobuf.WireFormat.b
            if (r6 != r7) goto Lc6
            goto Lcb
        Lc6:
            int r4 = com.google.protobuf.e.N(r6, r12, r4, r14, r15)
            goto L6d
        Lcb:
            if (r3 == 0) goto Ld4
            int r13 = com.google.protobuf.WireFormat.c(r13, r5)
            r1.m(r13, r3)
        Ld4:
            r13 = r4
            goto L19
        Ld7:
            if (r13 != r14) goto Lda
            return
        Lda:
            com.google.protobuf.InvalidProtocolBufferException r11 = com.google.protobuf.InvalidProtocolBufferException.parseFailure()
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.q0.j(java.lang.Object, byte[], int, int, com.google.protobuf.e$b):void");
    }

    public final <UT, UB> int k(f1<UT, UB> f1Var, T t) {
        return f1Var.i(f1Var.g(t));
    }

    public final <UT, UB, ET extends w.c<ET>> void l(f1<UT, UB> f1Var, s<ET> sVar, T t, w0 w0Var, r rVar) throws IOException {
        UB f = f1Var.f(t);
        w<ET> d = sVar.d(t);
        do {
            try {
                if (w0Var.w() == Integer.MAX_VALUE) {
                    return;
                }
            } finally {
                f1Var.o(t, f);
            }
        } while (n(w0Var, rVar, sVar, d, f1Var, f));
    }

    public final <UT, UB, ET extends w.c<ET>> boolean n(w0 w0Var, r rVar, s<ET> sVar, w<ET> wVar, f1<UT, UB> f1Var, UB ub) throws IOException {
        int a = w0Var.a();
        if (a != WireFormat.a) {
            if (WireFormat.b(a) == 2) {
                Object b = sVar.b(rVar, this.a, WireFormat.a(a));
                if (b != null) {
                    sVar.h(w0Var, b, rVar, wVar);
                    return true;
                }
                return f1Var.m(ub, w0Var);
            }
            return w0Var.C();
        }
        int i = 0;
        Object obj = null;
        ByteString byteString = null;
        while (w0Var.w() != Integer.MAX_VALUE) {
            int a2 = w0Var.a();
            if (a2 == WireFormat.c) {
                i = w0Var.l();
                obj = sVar.b(rVar, this.a, i);
            } else if (a2 == WireFormat.d) {
                if (obj != null) {
                    sVar.h(w0Var, obj, rVar, wVar);
                } else {
                    byteString = w0Var.z();
                }
            } else if (!w0Var.C()) {
                break;
            }
        }
        if (w0Var.a() == WireFormat.b) {
            if (byteString != null) {
                if (obj != null) {
                    sVar.i(byteString, obj, rVar, wVar);
                } else {
                    f1Var.d(ub, i, byteString);
                }
            }
            return true;
        }
        throw InvalidProtocolBufferException.invalidEndTag();
    }

    public final <UT, UB> void o(f1<UT, UB> f1Var, T t, Writer writer) throws IOException {
        f1Var.s(f1Var.g(t), writer);
    }
}
