package com.google.protobuf;

import com.google.protobuf.h0;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/* compiled from: Reader.java */
/* loaded from: classes2.dex */
public interface w0 {
    void A(List<Float> list) throws IOException;

    int B() throws IOException;

    boolean C() throws IOException;

    int D() throws IOException;

    void E(List<ByteString> list) throws IOException;

    void F(List<Double> list) throws IOException;

    long G() throws IOException;

    String H() throws IOException;

    void I(List<Long> list) throws IOException;

    @Deprecated
    <T> void J(List<T> list, y0<T> y0Var, r rVar) throws IOException;

    <T> T K(Class<T> cls, r rVar) throws IOException;

    <T> void L(List<T> list, y0<T> y0Var, r rVar) throws IOException;

    @Deprecated
    <T> T M(y0<T> y0Var, r rVar) throws IOException;

    @Deprecated
    <T> T N(Class<T> cls, r rVar) throws IOException;

    <T> T O(y0<T> y0Var, r rVar) throws IOException;

    <K, V> void P(Map<K, V> map, h0.b<K, V> bVar, r rVar) throws IOException;

    boolean Q();

    int a();

    void b(List<Integer> list) throws IOException;

    long c() throws IOException;

    long d() throws IOException;

    void e(List<Integer> list) throws IOException;

    void f(List<Long> list) throws IOException;

    void g(List<Integer> list) throws IOException;

    int h() throws IOException;

    boolean i() throws IOException;

    long j() throws IOException;

    void k(List<Long> list) throws IOException;

    int l() throws IOException;

    void m(List<Long> list) throws IOException;

    void n(List<Long> list) throws IOException;

    void o(List<Integer> list) throws IOException;

    void p(List<Integer> list) throws IOException;

    int q() throws IOException;

    void r(List<Integer> list) throws IOException;

    double readDouble() throws IOException;

    float readFloat() throws IOException;

    int s() throws IOException;

    long t() throws IOException;

    void u(List<Boolean> list) throws IOException;

    String v() throws IOException;

    int w() throws IOException;

    void x(List<String> list) throws IOException;

    void y(List<String> list) throws IOException;

    ByteString z() throws IOException;
}
