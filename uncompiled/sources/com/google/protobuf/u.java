package com.google.protobuf;

import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.WireFormat;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/* compiled from: ExtensionSchemaLite.java */
/* loaded from: classes2.dex */
public final class u extends s<GeneratedMessageLite.c> {

    /* compiled from: ExtensionSchemaLite.java */
    /* loaded from: classes2.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[WireFormat.FieldType.values().length];
            a = iArr;
            try {
                iArr[WireFormat.FieldType.DOUBLE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[WireFormat.FieldType.FLOAT.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[WireFormat.FieldType.INT64.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                a[WireFormat.FieldType.UINT64.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                a[WireFormat.FieldType.INT32.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                a[WireFormat.FieldType.FIXED64.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                a[WireFormat.FieldType.FIXED32.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                a[WireFormat.FieldType.BOOL.ordinal()] = 8;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                a[WireFormat.FieldType.UINT32.ordinal()] = 9;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                a[WireFormat.FieldType.SFIXED32.ordinal()] = 10;
            } catch (NoSuchFieldError unused10) {
            }
            try {
                a[WireFormat.FieldType.SFIXED64.ordinal()] = 11;
            } catch (NoSuchFieldError unused11) {
            }
            try {
                a[WireFormat.FieldType.SINT32.ordinal()] = 12;
            } catch (NoSuchFieldError unused12) {
            }
            try {
                a[WireFormat.FieldType.SINT64.ordinal()] = 13;
            } catch (NoSuchFieldError unused13) {
            }
            try {
                a[WireFormat.FieldType.ENUM.ordinal()] = 14;
            } catch (NoSuchFieldError unused14) {
            }
            try {
                a[WireFormat.FieldType.BYTES.ordinal()] = 15;
            } catch (NoSuchFieldError unused15) {
            }
            try {
                a[WireFormat.FieldType.STRING.ordinal()] = 16;
            } catch (NoSuchFieldError unused16) {
            }
            try {
                a[WireFormat.FieldType.GROUP.ordinal()] = 17;
            } catch (NoSuchFieldError unused17) {
            }
            try {
                a[WireFormat.FieldType.MESSAGE.ordinal()] = 18;
            } catch (NoSuchFieldError unused18) {
            }
        }
    }

    @Override // com.google.protobuf.s
    public int a(Map.Entry<?, ?> entry) {
        return ((GeneratedMessageLite.c) entry.getKey()).getNumber();
    }

    @Override // com.google.protobuf.s
    public Object b(r rVar, m0 m0Var, int i) {
        return rVar.a(m0Var, i);
    }

    @Override // com.google.protobuf.s
    public w<GeneratedMessageLite.c> c(Object obj) {
        return ((GeneratedMessageLite.b) obj).h0;
    }

    @Override // com.google.protobuf.s
    public w<GeneratedMessageLite.c> d(Object obj) {
        return ((GeneratedMessageLite.b) obj).k();
    }

    @Override // com.google.protobuf.s
    public boolean e(m0 m0Var) {
        return m0Var instanceof GeneratedMessageLite.b;
    }

    @Override // com.google.protobuf.s
    public void f(Object obj) {
        c(obj).H();
    }

    @Override // com.google.protobuf.s
    public <UT, UB> UB g(w0 w0Var, Object obj, r rVar, w<GeneratedMessageLite.c> wVar, UB ub, f1<UT, UB> f1Var) throws IOException {
        Object t;
        ArrayList arrayList;
        GeneratedMessageLite.d dVar = (GeneratedMessageLite.d) obj;
        int d = dVar.d();
        if (dVar.b.i() && dVar.b.isPacked()) {
            switch (a.a[dVar.b().ordinal()]) {
                case 1:
                    arrayList = new ArrayList();
                    w0Var.F(arrayList);
                    break;
                case 2:
                    arrayList = new ArrayList();
                    w0Var.A(arrayList);
                    break;
                case 3:
                    arrayList = new ArrayList();
                    w0Var.m(arrayList);
                    break;
                case 4:
                    arrayList = new ArrayList();
                    w0Var.k(arrayList);
                    break;
                case 5:
                    arrayList = new ArrayList();
                    w0Var.o(arrayList);
                    break;
                case 6:
                    arrayList = new ArrayList();
                    w0Var.I(arrayList);
                    break;
                case 7:
                    arrayList = new ArrayList();
                    w0Var.r(arrayList);
                    break;
                case 8:
                    arrayList = new ArrayList();
                    w0Var.u(arrayList);
                    break;
                case 9:
                    arrayList = new ArrayList();
                    w0Var.g(arrayList);
                    break;
                case 10:
                    arrayList = new ArrayList();
                    w0Var.e(arrayList);
                    break;
                case 11:
                    arrayList = new ArrayList();
                    w0Var.n(arrayList);
                    break;
                case 12:
                    arrayList = new ArrayList();
                    w0Var.b(arrayList);
                    break;
                case 13:
                    arrayList = new ArrayList();
                    w0Var.f(arrayList);
                    break;
                case 14:
                    arrayList = new ArrayList();
                    w0Var.p(arrayList);
                    ub = (UB) z0.z(d, arrayList, dVar.b.d(), ub, f1Var);
                    break;
                default:
                    throw new IllegalStateException("Type cannot be packed: " + dVar.b.n());
            }
            wVar.N(dVar.b, arrayList);
        } else {
            Object obj2 = null;
            if (dVar.b() == WireFormat.FieldType.ENUM) {
                int B = w0Var.B();
                if (dVar.b.d().findValueByNumber(B) == null) {
                    return (UB) z0.M(d, B, ub, f1Var);
                }
                obj2 = Integer.valueOf(B);
            } else {
                switch (a.a[dVar.b().ordinal()]) {
                    case 1:
                        obj2 = Double.valueOf(w0Var.readDouble());
                        break;
                    case 2:
                        obj2 = Float.valueOf(w0Var.readFloat());
                        break;
                    case 3:
                        obj2 = Long.valueOf(w0Var.G());
                        break;
                    case 4:
                        obj2 = Long.valueOf(w0Var.c());
                        break;
                    case 5:
                        obj2 = Integer.valueOf(w0Var.B());
                        break;
                    case 6:
                        obj2 = Long.valueOf(w0Var.d());
                        break;
                    case 7:
                        obj2 = Integer.valueOf(w0Var.h());
                        break;
                    case 8:
                        obj2 = Boolean.valueOf(w0Var.i());
                        break;
                    case 9:
                        obj2 = Integer.valueOf(w0Var.l());
                        break;
                    case 10:
                        obj2 = Integer.valueOf(w0Var.D());
                        break;
                    case 11:
                        obj2 = Long.valueOf(w0Var.j());
                        break;
                    case 12:
                        obj2 = Integer.valueOf(w0Var.s());
                        break;
                    case 13:
                        obj2 = Long.valueOf(w0Var.t());
                        break;
                    case 14:
                        throw new IllegalStateException("Shouldn't reach here.");
                    case 15:
                        obj2 = w0Var.z();
                        break;
                    case 16:
                        obj2 = w0Var.v();
                        break;
                    case 17:
                        obj2 = w0Var.N(dVar.c().getClass(), rVar);
                        break;
                    case 18:
                        obj2 = w0Var.K(dVar.c().getClass(), rVar);
                        break;
                }
            }
            if (dVar.e()) {
                wVar.g(dVar.b, obj2);
            } else {
                int i = a.a[dVar.b().ordinal()];
                if ((i == 17 || i == 18) && (t = wVar.t(dVar.b)) != null) {
                    obj2 = a0.j(t, obj2);
                }
                wVar.N(dVar.b, obj2);
            }
        }
        return ub;
    }

    @Override // com.google.protobuf.s
    public void h(w0 w0Var, Object obj, r rVar, w<GeneratedMessageLite.c> wVar) throws IOException {
        GeneratedMessageLite.d dVar = (GeneratedMessageLite.d) obj;
        wVar.N(dVar.b, w0Var.K(dVar.c().getClass(), rVar));
    }

    @Override // com.google.protobuf.s
    public void i(ByteString byteString, Object obj, r rVar, w<GeneratedMessageLite.c> wVar) throws IOException {
        GeneratedMessageLite.d dVar = (GeneratedMessageLite.d) obj;
        m0 buildPartial = dVar.c().newBuilderForType().buildPartial();
        f R = f.R(ByteBuffer.wrap(byteString.toByteArray()), true);
        u0.a().b(buildPartial, R, rVar);
        wVar.N(dVar.b, buildPartial);
        if (R.w() != Integer.MAX_VALUE) {
            throw InvalidProtocolBufferException.invalidEndTag();
        }
    }

    @Override // com.google.protobuf.s
    public void j(Writer writer, Map.Entry<?, ?> entry) throws IOException {
        GeneratedMessageLite.c cVar = (GeneratedMessageLite.c) entry.getKey();
        if (cVar.i()) {
            switch (a.a[cVar.n().ordinal()]) {
                case 1:
                    z0.R(cVar.getNumber(), (List) entry.getValue(), writer, cVar.isPacked());
                    return;
                case 2:
                    z0.V(cVar.getNumber(), (List) entry.getValue(), writer, cVar.isPacked());
                    return;
                case 3:
                    z0.Z(cVar.getNumber(), (List) entry.getValue(), writer, cVar.isPacked());
                    return;
                case 4:
                    z0.i0(cVar.getNumber(), (List) entry.getValue(), writer, cVar.isPacked());
                    return;
                case 5:
                    z0.Y(cVar.getNumber(), (List) entry.getValue(), writer, cVar.isPacked());
                    return;
                case 6:
                    z0.U(cVar.getNumber(), (List) entry.getValue(), writer, cVar.isPacked());
                    return;
                case 7:
                    z0.T(cVar.getNumber(), (List) entry.getValue(), writer, cVar.isPacked());
                    return;
                case 8:
                    z0.P(cVar.getNumber(), (List) entry.getValue(), writer, cVar.isPacked());
                    return;
                case 9:
                    z0.h0(cVar.getNumber(), (List) entry.getValue(), writer, cVar.isPacked());
                    return;
                case 10:
                    z0.c0(cVar.getNumber(), (List) entry.getValue(), writer, cVar.isPacked());
                    return;
                case 11:
                    z0.d0(cVar.getNumber(), (List) entry.getValue(), writer, cVar.isPacked());
                    return;
                case 12:
                    z0.e0(cVar.getNumber(), (List) entry.getValue(), writer, cVar.isPacked());
                    return;
                case 13:
                    z0.f0(cVar.getNumber(), (List) entry.getValue(), writer, cVar.isPacked());
                    return;
                case 14:
                    z0.Y(cVar.getNumber(), (List) entry.getValue(), writer, cVar.isPacked());
                    return;
                case 15:
                    z0.Q(cVar.getNumber(), (List) entry.getValue(), writer);
                    return;
                case 16:
                    z0.g0(cVar.getNumber(), (List) entry.getValue(), writer);
                    return;
                case 17:
                    List list = (List) entry.getValue();
                    if (list == null || list.isEmpty()) {
                        return;
                    }
                    z0.X(cVar.getNumber(), (List) entry.getValue(), writer, u0.a().d(list.get(0).getClass()));
                    return;
                case 18:
                    List list2 = (List) entry.getValue();
                    if (list2 == null || list2.isEmpty()) {
                        return;
                    }
                    z0.b0(cVar.getNumber(), (List) entry.getValue(), writer, u0.a().d(list2.get(0).getClass()));
                    return;
                default:
                    return;
            }
        }
        switch (a.a[cVar.n().ordinal()]) {
            case 1:
                writer.e(cVar.getNumber(), ((Double) entry.getValue()).doubleValue());
                return;
            case 2:
                writer.A(cVar.getNumber(), ((Float) entry.getValue()).floatValue());
                return;
            case 3:
                writer.n(cVar.getNumber(), ((Long) entry.getValue()).longValue());
                return;
            case 4:
                writer.l(cVar.getNumber(), ((Long) entry.getValue()).longValue());
                return;
            case 5:
                writer.r(cVar.getNumber(), ((Integer) entry.getValue()).intValue());
                return;
            case 6:
                writer.h(cVar.getNumber(), ((Long) entry.getValue()).longValue());
                return;
            case 7:
                writer.d(cVar.getNumber(), ((Integer) entry.getValue()).intValue());
                return;
            case 8:
                writer.o(cVar.getNumber(), ((Boolean) entry.getValue()).booleanValue());
                return;
            case 9:
                writer.b(cVar.getNumber(), ((Integer) entry.getValue()).intValue());
                return;
            case 10:
                writer.p(cVar.getNumber(), ((Integer) entry.getValue()).intValue());
                return;
            case 11:
                writer.u(cVar.getNumber(), ((Long) entry.getValue()).longValue());
                return;
            case 12:
                writer.H(cVar.getNumber(), ((Integer) entry.getValue()).intValue());
                return;
            case 13:
                writer.z(cVar.getNumber(), ((Long) entry.getValue()).longValue());
                return;
            case 14:
                writer.r(cVar.getNumber(), ((Integer) entry.getValue()).intValue());
                return;
            case 15:
                writer.Q(cVar.getNumber(), (ByteString) entry.getValue());
                return;
            case 16:
                writer.k(cVar.getNumber(), (String) entry.getValue());
                return;
            case 17:
                writer.P(cVar.getNumber(), entry.getValue(), u0.a().d(entry.getValue().getClass()));
                return;
            case 18:
                writer.L(cVar.getNumber(), entry.getValue(), u0.a().d(entry.getValue().getClass()));
                return;
            default:
                return;
        }
    }
}
