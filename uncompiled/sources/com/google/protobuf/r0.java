package com.google.protobuf;

import com.google.protobuf.GeneratedMessageV3;

/* compiled from: NewInstanceSchemaFull.java */
/* loaded from: classes2.dex */
public final class r0 implements if2 {
    @Override // defpackage.if2
    public Object a(Object obj) {
        return ((GeneratedMessageV3) obj).newInstance(GeneratedMessageV3.f.a);
    }
}
