package com.google.protobuf;

import com.google.protobuf.WireFormat;
import com.google.protobuf.Writer;
import com.google.protobuf.h0;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/* compiled from: CodedOutputStreamWriter.java */
/* loaded from: classes2.dex */
public final class l implements Writer {
    public final CodedOutputStream a;

    /* compiled from: CodedOutputStreamWriter.java */
    /* loaded from: classes2.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[WireFormat.FieldType.values().length];
            a = iArr;
            try {
                iArr[WireFormat.FieldType.BOOL.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[WireFormat.FieldType.FIXED32.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[WireFormat.FieldType.INT32.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                a[WireFormat.FieldType.SFIXED32.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                a[WireFormat.FieldType.SINT32.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                a[WireFormat.FieldType.UINT32.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                a[WireFormat.FieldType.FIXED64.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                a[WireFormat.FieldType.INT64.ordinal()] = 8;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                a[WireFormat.FieldType.SFIXED64.ordinal()] = 9;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                a[WireFormat.FieldType.SINT64.ordinal()] = 10;
            } catch (NoSuchFieldError unused10) {
            }
            try {
                a[WireFormat.FieldType.UINT64.ordinal()] = 11;
            } catch (NoSuchFieldError unused11) {
            }
            try {
                a[WireFormat.FieldType.STRING.ordinal()] = 12;
            } catch (NoSuchFieldError unused12) {
            }
        }
    }

    public l(CodedOutputStream codedOutputStream) {
        CodedOutputStream codedOutputStream2 = (CodedOutputStream) a0.b(codedOutputStream, "output");
        this.a = codedOutputStream2;
        codedOutputStream2.a = this;
    }

    public static l T(CodedOutputStream codedOutputStream) {
        l lVar = codedOutputStream.a;
        return lVar != null ? lVar : new l(codedOutputStream);
    }

    @Override // com.google.protobuf.Writer
    public void A(int i, float f) throws IOException {
        this.a.A0(i, f);
    }

    @Override // com.google.protobuf.Writer
    public void B(int i) throws IOException {
        this.a.a1(i, 4);
    }

    @Override // com.google.protobuf.Writer
    public void C(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a1(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += CodedOutputStream.S(list.get(i4).intValue());
            }
            this.a.c1(i3);
            while (i2 < list.size()) {
                this.a.V0(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.U0(i, list.get(i2).intValue());
            i2++;
        }
    }

    @Override // com.google.protobuf.Writer
    public void D(int i, int i2) throws IOException {
        this.a.u0(i, i2);
    }

    @Override // com.google.protobuf.Writer
    public void E(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a1(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += CodedOutputStream.A(list.get(i4).longValue());
            }
            this.a.c1(i3);
            while (i2 < list.size()) {
                this.a.J0(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.I0(i, list.get(i2).longValue());
            i2++;
        }
    }

    @Override // com.google.protobuf.Writer
    public void F(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a1(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += CodedOutputStream.m(list.get(i4).intValue());
            }
            this.a.c1(i3);
            while (i2 < list.size()) {
                this.a.v0(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.u0(i, list.get(i2).intValue());
            i2++;
        }
    }

    @Override // com.google.protobuf.Writer
    public void G(int i, List<Double> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a1(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += CodedOutputStream.k(list.get(i4).doubleValue());
            }
            this.a.c1(i3);
            while (i2 < list.size()) {
                this.a.t0(list.get(i2).doubleValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.s0(i, list.get(i2).doubleValue());
            i2++;
        }
    }

    @Override // com.google.protobuf.Writer
    public void H(int i, int i2) throws IOException {
        this.a.U0(i, i2);
    }

    @Override // com.google.protobuf.Writer
    public void I(int i, List<ByteString> list) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            this.a.q0(i, list.get(i2));
        }
    }

    @Override // com.google.protobuf.Writer
    public void J(int i, List<?> list, y0 y0Var) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            L(i, list.get(i2), y0Var);
        }
    }

    @Override // com.google.protobuf.Writer
    public void K(int i, List<?> list, y0 y0Var) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            P(i, list.get(i2), y0Var);
        }
    }

    @Override // com.google.protobuf.Writer
    public void L(int i, Object obj, y0 y0Var) throws IOException {
        this.a.L0(i, (m0) obj, y0Var);
    }

    @Override // com.google.protobuf.Writer
    public void M(int i, List<?> list) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            R(i, list.get(i2));
        }
    }

    @Override // com.google.protobuf.Writer
    public void N(int i, List<?> list) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            O(i, list.get(i2));
        }
    }

    @Override // com.google.protobuf.Writer
    public void O(int i, Object obj) throws IOException {
        this.a.K0(i, (m0) obj);
    }

    @Override // com.google.protobuf.Writer
    public void P(int i, Object obj, y0 y0Var) throws IOException {
        this.a.D0(i, (m0) obj, y0Var);
    }

    @Override // com.google.protobuf.Writer
    public void Q(int i, ByteString byteString) throws IOException {
        this.a.q0(i, byteString);
    }

    @Override // com.google.protobuf.Writer
    public void R(int i, Object obj) throws IOException {
        this.a.C0(i, (m0) obj);
    }

    @Override // com.google.protobuf.Writer
    public <K, V> void S(int i, h0.b<K, V> bVar, Map<K, V> map) throws IOException {
        if (this.a.g0()) {
            X(i, bVar, map);
            return;
        }
        for (Map.Entry<K, V> entry : map.entrySet()) {
            this.a.a1(i, 2);
            this.a.c1(h0.b(bVar, entry.getKey(), entry.getValue()));
            h0.f(this.a, bVar, entry.getKey(), entry.getValue());
        }
    }

    public final <V> void U(int i, boolean z, V v, h0.b<Boolean, V> bVar) throws IOException {
        this.a.a1(i, 2);
        this.a.c1(h0.b(bVar, Boolean.valueOf(z), v));
        h0.f(this.a, bVar, Boolean.valueOf(z), v);
    }

    public final <V> void V(int i, h0.b<Integer, V> bVar, Map<Integer, V> map) throws IOException {
        int size = map.size();
        int[] iArr = new int[size];
        int i2 = 0;
        for (Integer num : map.keySet()) {
            iArr[i2] = num.intValue();
            i2++;
        }
        Arrays.sort(iArr);
        for (int i3 = 0; i3 < size; i3++) {
            int i4 = iArr[i3];
            V v = map.get(Integer.valueOf(i4));
            this.a.a1(i, 2);
            this.a.c1(h0.b(bVar, Integer.valueOf(i4), v));
            h0.f(this.a, bVar, Integer.valueOf(i4), v);
        }
    }

    public final <V> void W(int i, h0.b<Long, V> bVar, Map<Long, V> map) throws IOException {
        int size = map.size();
        long[] jArr = new long[size];
        int i2 = 0;
        for (Long l : map.keySet()) {
            jArr[i2] = l.longValue();
            i2++;
        }
        Arrays.sort(jArr);
        for (int i3 = 0; i3 < size; i3++) {
            long j = jArr[i3];
            V v = map.get(Long.valueOf(j));
            this.a.a1(i, 2);
            this.a.c1(h0.b(bVar, Long.valueOf(j), v));
            h0.f(this.a, bVar, Long.valueOf(j), v);
        }
    }

    public final <K, V> void X(int i, h0.b<K, V> bVar, Map<K, V> map) throws IOException {
        switch (a.a[bVar.a.ordinal()]) {
            case 1:
                V v = map.get(Boolean.FALSE);
                if (v != null) {
                    U(i, false, v, bVar);
                }
                V v2 = map.get(Boolean.TRUE);
                if (v2 != null) {
                    U(i, true, v2, bVar);
                    return;
                }
                return;
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                V(i, bVar, map);
                return;
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
                W(i, bVar, map);
                return;
            case 12:
                Y(i, bVar, map);
                return;
            default:
                throw new IllegalArgumentException("does not support key type: " + bVar.a);
        }
    }

    public final <V> void Y(int i, h0.b<String, V> bVar, Map<String, V> map) throws IOException {
        int size = map.size();
        String[] strArr = new String[size];
        int i2 = 0;
        for (String str : map.keySet()) {
            strArr[i2] = str;
            i2++;
        }
        Arrays.sort(strArr);
        for (int i3 = 0; i3 < size; i3++) {
            String str2 = strArr[i3];
            V v = map.get(str2);
            this.a.a1(i, 2);
            this.a.c1(h0.b(bVar, str2, v));
            h0.f(this.a, bVar, str2, v);
        }
    }

    public final void Z(int i, Object obj) throws IOException {
        if (obj instanceof String) {
            this.a.Y0(i, (String) obj);
        } else {
            this.a.q0(i, (ByteString) obj);
        }
    }

    @Override // com.google.protobuf.Writer
    public void a(int i, List<Float> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a1(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += CodedOutputStream.s(list.get(i4).floatValue());
            }
            this.a.c1(i3);
            while (i2 < list.size()) {
                this.a.B0(list.get(i2).floatValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.A0(i, list.get(i2).floatValue());
            i2++;
        }
    }

    @Override // com.google.protobuf.Writer
    public void b(int i, int i2) throws IOException {
        this.a.b1(i, i2);
    }

    @Override // com.google.protobuf.Writer
    public final void c(int i, Object obj) throws IOException {
        if (obj instanceof ByteString) {
            this.a.O0(i, (ByteString) obj);
        } else {
            this.a.N0(i, (m0) obj);
        }
    }

    @Override // com.google.protobuf.Writer
    public void d(int i, int i2) throws IOException {
        this.a.w0(i, i2);
    }

    @Override // com.google.protobuf.Writer
    public void e(int i, double d) throws IOException {
        this.a.s0(i, d);
    }

    @Override // com.google.protobuf.Writer
    public void f(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a1(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += CodedOutputStream.Q(list.get(i4).longValue());
            }
            this.a.c1(i3);
            while (i2 < list.size()) {
                this.a.T0(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.S0(i, list.get(i2).longValue());
            i2++;
        }
    }

    @Override // com.google.protobuf.Writer
    public void g(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a1(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += CodedOutputStream.b0(list.get(i4).longValue());
            }
            this.a.c1(i3);
            while (i2 < list.size()) {
                this.a.e1(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.d1(i, list.get(i2).longValue());
            i2++;
        }
    }

    @Override // com.google.protobuf.Writer
    public void h(int i, long j) throws IOException {
        this.a.y0(i, j);
    }

    @Override // com.google.protobuf.Writer
    public Writer.FieldOrder i() {
        return Writer.FieldOrder.ASCENDING;
    }

    @Override // com.google.protobuf.Writer
    public void j(int i, List<String> list) throws IOException {
        int i2 = 0;
        if (list instanceof cz1) {
            cz1 cz1Var = (cz1) list;
            while (i2 < list.size()) {
                Z(i, cz1Var.j(i2));
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.Y0(i, list.get(i2));
            i2++;
        }
    }

    @Override // com.google.protobuf.Writer
    public void k(int i, String str) throws IOException {
        this.a.Y0(i, str);
    }

    @Override // com.google.protobuf.Writer
    public void l(int i, long j) throws IOException {
        this.a.d1(i, j);
    }

    @Override // com.google.protobuf.Writer
    public void m(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a1(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += CodedOutputStream.y(list.get(i4).intValue());
            }
            this.a.c1(i3);
            while (i2 < list.size()) {
                this.a.H0(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.G0(i, list.get(i2).intValue());
            i2++;
        }
    }

    @Override // com.google.protobuf.Writer
    public void n(int i, long j) throws IOException {
        this.a.I0(i, j);
    }

    @Override // com.google.protobuf.Writer
    public void o(int i, boolean z) throws IOException {
        this.a.m0(i, z);
    }

    @Override // com.google.protobuf.Writer
    public void p(int i, int i2) throws IOException {
        this.a.Q0(i, i2);
    }

    @Override // com.google.protobuf.Writer
    public void q(int i) throws IOException {
        this.a.a1(i, 3);
    }

    @Override // com.google.protobuf.Writer
    public void r(int i, int i2) throws IOException {
        this.a.G0(i, i2);
    }

    @Override // com.google.protobuf.Writer
    public void s(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a1(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += CodedOutputStream.q(list.get(i4).longValue());
            }
            this.a.c1(i3);
            while (i2 < list.size()) {
                this.a.z0(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.y0(i, list.get(i2).longValue());
            i2++;
        }
    }

    @Override // com.google.protobuf.Writer
    public void t(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a1(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += CodedOutputStream.O(list.get(i4).intValue());
            }
            this.a.c1(i3);
            while (i2 < list.size()) {
                this.a.R0(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.Q0(i, list.get(i2).intValue());
            i2++;
        }
    }

    @Override // com.google.protobuf.Writer
    public void u(int i, long j) throws IOException {
        this.a.S0(i, j);
    }

    @Override // com.google.protobuf.Writer
    public void v(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a1(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += CodedOutputStream.o(list.get(i4).intValue());
            }
            this.a.c1(i3);
            while (i2 < list.size()) {
                this.a.x0(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.w0(i, list.get(i2).intValue());
            i2++;
        }
    }

    @Override // com.google.protobuf.Writer
    public void w(int i, List<Boolean> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a1(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += CodedOutputStream.f(list.get(i4).booleanValue());
            }
            this.a.c1(i3);
            while (i2 < list.size()) {
                this.a.n0(list.get(i2).booleanValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.m0(i, list.get(i2).booleanValue());
            i2++;
        }
    }

    @Override // com.google.protobuf.Writer
    public void x(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a1(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += CodedOutputStream.Z(list.get(i4).intValue());
            }
            this.a.c1(i3);
            while (i2 < list.size()) {
                this.a.c1(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.b1(i, list.get(i2).intValue());
            i2++;
        }
    }

    @Override // com.google.protobuf.Writer
    public void y(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a1(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += CodedOutputStream.U(list.get(i4).longValue());
            }
            this.a.c1(i3);
            while (i2 < list.size()) {
                this.a.X0(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.W0(i, list.get(i2).longValue());
            i2++;
        }
    }

    @Override // com.google.protobuf.Writer
    public void z(int i, long j) throws IOException {
        this.a.W0(i, j);
    }
}
