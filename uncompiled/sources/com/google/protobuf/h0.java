package com.google.protobuf;

import com.google.protobuf.WireFormat;
import com.google.protobuf.m0;
import java.io.IOException;
import java.util.AbstractMap;
import java.util.Map;

/* compiled from: MapEntryLite.java */
/* loaded from: classes2.dex */
public class h0<K, V> {
    public final b<K, V> a;

    /* compiled from: MapEntryLite.java */
    /* loaded from: classes2.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[WireFormat.FieldType.values().length];
            a = iArr;
            try {
                iArr[WireFormat.FieldType.MESSAGE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[WireFormat.FieldType.ENUM.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[WireFormat.FieldType.GROUP.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    /* compiled from: MapEntryLite.java */
    /* loaded from: classes2.dex */
    public static class b<K, V> {
        public final WireFormat.FieldType a;
        public final K b;
        public final WireFormat.FieldType c;
        public final V d;

        public b(WireFormat.FieldType fieldType, K k, WireFormat.FieldType fieldType2, V v) {
            this.a = fieldType;
            this.b = k;
            this.c = fieldType2;
            this.d = v;
        }
    }

    public static <K, V> int b(b<K, V> bVar, K k, V v) {
        return w.n(bVar.a, 1, k) + w.n(bVar.c, 2, v);
    }

    public static <K, V> Map.Entry<K, V> d(j jVar, b<K, V> bVar, r rVar) throws IOException {
        Object obj = bVar.b;
        Object obj2 = bVar.d;
        while (true) {
            int J = jVar.J();
            if (J == 0) {
                break;
            } else if (J == WireFormat.c(1, bVar.a.getWireType())) {
                obj = e(jVar, rVar, bVar.a, obj);
            } else if (J == WireFormat.c(2, bVar.c.getWireType())) {
                obj2 = e(jVar, rVar, bVar.c, obj2);
            } else if (!jVar.N(J)) {
                break;
            }
        }
        return new AbstractMap.SimpleImmutableEntry(obj, obj2);
    }

    public static <T> T e(j jVar, r rVar, WireFormat.FieldType fieldType, T t) throws IOException {
        int i = a.a[fieldType.ordinal()];
        if (i == 1) {
            m0.a builder = ((m0) t).toBuilder();
            jVar.A(builder, rVar);
            return (T) builder.buildPartial();
        } else if (i != 2) {
            if (i != 3) {
                return (T) w.M(jVar, fieldType, true);
            }
            throw new RuntimeException("Groups are not allowed in maps.");
        } else {
            return (T) Integer.valueOf(jVar.s());
        }
    }

    public static <K, V> void f(CodedOutputStream codedOutputStream, b<K, V> bVar, K k, V v) throws IOException {
        w.P(codedOutputStream, bVar.a, 1, k);
        w.P(codedOutputStream, bVar.c, 2, v);
    }

    public int a(int i, K k, V v) {
        return CodedOutputStream.X(i) + CodedOutputStream.E(b(this.a, k, v));
    }

    public b<K, V> c() {
        return this.a;
    }
}
