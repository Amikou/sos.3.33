package com.google.protobuf;

import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import com.google.protobuf.Descriptors;
import com.google.protobuf.g1;
import com.google.protobuf.o;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/* loaded from: classes2.dex */
public final class TextFormat {
    public static final Logger a = Logger.getLogger(TextFormat.class.getName());

    /* loaded from: classes2.dex */
    public static class InvalidEscapeSequenceException extends IOException {
        private static final long serialVersionUID = -8164033650142593304L;

        public InvalidEscapeSequenceException(String str) {
            super(str);
        }
    }

    /* loaded from: classes2.dex */
    public static class Parser {

        /* loaded from: classes2.dex */
        public enum SingularOverwritePolicy {
            ALLOW_SINGULAR_OVERWRITES,
            FORBID_SINGULAR_OVERWRITES
        }

        /* loaded from: classes2.dex */
        public static class a {
            public d1 e;
            public boolean a = false;
            public boolean b = false;
            public boolean c = false;
            public SingularOverwritePolicy d = SingularOverwritePolicy.ALLOW_SINGULAR_OVERWRITES;
            public e1 f = e1.c();

            public Parser a() {
                return new Parser(this.f, this.a, this.b, this.c, this.d, this.e, null);
            }
        }

        public /* synthetic */ Parser(e1 e1Var, boolean z, boolean z2, boolean z3, SingularOverwritePolicy singularOverwritePolicy, d1 d1Var, a aVar) {
            this(e1Var, z, z2, z3, singularOverwritePolicy, d1Var);
        }

        public static a a() {
            return new a();
        }

        public Parser(e1 e1Var, boolean z, boolean z2, boolean z3, SingularOverwritePolicy singularOverwritePolicy, d1 d1Var) {
        }
    }

    /* loaded from: classes2.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;
        public static final /* synthetic */ int[] b;

        static {
            int[] iArr = new int[Descriptors.FieldDescriptor.Type.values().length];
            b = iArr;
            try {
                iArr[Descriptors.FieldDescriptor.Type.INT32.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                b[Descriptors.FieldDescriptor.Type.SINT32.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                b[Descriptors.FieldDescriptor.Type.SFIXED32.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                b[Descriptors.FieldDescriptor.Type.INT64.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                b[Descriptors.FieldDescriptor.Type.SINT64.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                b[Descriptors.FieldDescriptor.Type.SFIXED64.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                b[Descriptors.FieldDescriptor.Type.BOOL.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                b[Descriptors.FieldDescriptor.Type.FLOAT.ordinal()] = 8;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                b[Descriptors.FieldDescriptor.Type.DOUBLE.ordinal()] = 9;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                b[Descriptors.FieldDescriptor.Type.UINT32.ordinal()] = 10;
            } catch (NoSuchFieldError unused10) {
            }
            try {
                b[Descriptors.FieldDescriptor.Type.FIXED32.ordinal()] = 11;
            } catch (NoSuchFieldError unused11) {
            }
            try {
                b[Descriptors.FieldDescriptor.Type.UINT64.ordinal()] = 12;
            } catch (NoSuchFieldError unused12) {
            }
            try {
                b[Descriptors.FieldDescriptor.Type.FIXED64.ordinal()] = 13;
            } catch (NoSuchFieldError unused13) {
            }
            try {
                b[Descriptors.FieldDescriptor.Type.STRING.ordinal()] = 14;
            } catch (NoSuchFieldError unused14) {
            }
            try {
                b[Descriptors.FieldDescriptor.Type.BYTES.ordinal()] = 15;
            } catch (NoSuchFieldError unused15) {
            }
            try {
                b[Descriptors.FieldDescriptor.Type.ENUM.ordinal()] = 16;
            } catch (NoSuchFieldError unused16) {
            }
            try {
                b[Descriptors.FieldDescriptor.Type.MESSAGE.ordinal()] = 17;
            } catch (NoSuchFieldError unused17) {
            }
            try {
                b[Descriptors.FieldDescriptor.Type.GROUP.ordinal()] = 18;
            } catch (NoSuchFieldError unused18) {
            }
            int[] iArr2 = new int[Descriptors.FieldDescriptor.JavaType.values().length];
            a = iArr2;
            try {
                iArr2[Descriptors.FieldDescriptor.JavaType.BOOLEAN.ordinal()] = 1;
            } catch (NoSuchFieldError unused19) {
            }
            try {
                a[Descriptors.FieldDescriptor.JavaType.LONG.ordinal()] = 2;
            } catch (NoSuchFieldError unused20) {
            }
            try {
                a[Descriptors.FieldDescriptor.JavaType.INT.ordinal()] = 3;
            } catch (NoSuchFieldError unused21) {
            }
            try {
                a[Descriptors.FieldDescriptor.JavaType.STRING.ordinal()] = 4;
            } catch (NoSuchFieldError unused22) {
            }
        }
    }

    /* loaded from: classes2.dex */
    public static final class b {
        public static final b c = new b(true, e1.c());
        public final boolean a;
        public final e1 b;

        /* loaded from: classes2.dex */
        public static class a implements Comparable<a> {
            public Object a;
            public g0 f0;
            public final Descriptors.FieldDescriptor.JavaType g0;

            public a(Object obj, Descriptors.FieldDescriptor fieldDescriptor) {
                if (obj instanceof g0) {
                    this.f0 = (g0) obj;
                } else {
                    this.a = obj;
                }
                this.g0 = d(fieldDescriptor);
            }

            public static Descriptors.FieldDescriptor.JavaType d(Descriptors.FieldDescriptor fieldDescriptor) {
                return fieldDescriptor.x().p().get(0).v();
            }

            @Override // java.lang.Comparable
            /* renamed from: a */
            public int compareTo(a aVar) {
                if (f() == null || aVar.f() == null) {
                    TextFormat.a.info("Invalid key for map field.");
                    return -1;
                }
                int i = a.a[this.g0.ordinal()];
                if (i != 1) {
                    if (i != 2) {
                        if (i != 3) {
                            if (i != 4) {
                                return 0;
                            }
                            String str = (String) f();
                            String str2 = (String) aVar.f();
                            if (str == null && str2 == null) {
                                return 0;
                            }
                            if (str != null || str2 == null) {
                                if (str == null || str2 != null) {
                                    return str.compareTo(str2);
                                }
                                return 1;
                            }
                            return -1;
                        }
                        return Integer.compare(((Integer) f()).intValue(), ((Integer) aVar.f()).intValue());
                    }
                    return Long.compare(((Long) f()).longValue(), ((Long) aVar.f()).longValue());
                }
                return Boolean.compare(((Boolean) f()).booleanValue(), ((Boolean) aVar.f()).booleanValue());
            }

            public Object e() {
                g0 g0Var = this.f0;
                return g0Var != null ? g0Var : this.a;
            }

            public Object f() {
                g0 g0Var = this.f0;
                if (g0Var != null) {
                    return g0Var.f();
                }
                return null;
            }
        }

        public b(boolean z, e1 e1Var) {
            this.a = z;
            this.b = e1Var;
        }

        public static void l(int i, int i2, List<?> list, c cVar) throws IOException {
            for (Object obj : list) {
                cVar.d(String.valueOf(i));
                cVar.d(": ");
                m(i2, obj, cVar);
                cVar.a();
            }
        }

        public static void m(int i, Object obj, c cVar) throws IOException {
            int b = WireFormat.b(i);
            if (b == 0) {
                cVar.d(TextFormat.r(((Long) obj).longValue()));
            } else if (b == 1) {
                cVar.d(String.format(null, "0x%016x", (Long) obj));
            } else if (b != 2) {
                if (b == 3) {
                    n((g1) obj, cVar);
                } else if (b == 5) {
                    cVar.d(String.format(null, "0x%08x", (Integer) obj));
                } else {
                    throw new IllegalArgumentException("Bad tag: " + i);
                }
            } else {
                try {
                    g1 j = g1.j((ByteString) obj);
                    cVar.d("{");
                    cVar.a();
                    cVar.b();
                    n(j, cVar);
                    cVar.c();
                    cVar.d("}");
                } catch (InvalidProtocolBufferException unused) {
                    cVar.d("\"");
                    cVar.d(TextFormat.d((ByteString) obj));
                    cVar.d("\"");
                }
            }
        }

        public static void n(g1 g1Var, c cVar) throws IOException {
            for (Map.Entry<Integer, g1.c> entry : g1Var.b().entrySet()) {
                int intValue = entry.getKey().intValue();
                g1.c value = entry.getValue();
                l(intValue, 0, value.s(), cVar);
                l(intValue, 5, value.l(), cVar);
                l(intValue, 1, value.m(), cVar);
                l(intValue, 2, value.p(), cVar);
                for (g1 g1Var2 : value.n()) {
                    cVar.d(entry.getKey().toString());
                    cVar.d(" {");
                    cVar.a();
                    cVar.b();
                    n(g1Var2, cVar);
                    cVar.c();
                    cVar.d("}");
                    cVar.a();
                }
            }
        }

        public final void b(o0 o0Var, c cVar) throws IOException {
            if (o0Var.getDescriptorForType().d().equals("google.protobuf.Any") && e(o0Var, cVar)) {
                return;
            }
            h(o0Var, cVar);
        }

        public void c(o0 o0Var, Appendable appendable) throws IOException {
            b(o0Var, TextFormat.i(appendable));
        }

        public void d(g1 g1Var, Appendable appendable) throws IOException {
            n(g1Var, TextFormat.i(appendable));
        }

        public final boolean e(o0 o0Var, c cVar) throws IOException {
            Descriptors.b descriptorForType = o0Var.getDescriptorForType();
            Descriptors.FieldDescriptor k = descriptorForType.k(1);
            Descriptors.FieldDescriptor k2 = descriptorForType.k(2);
            if (k != null && k.A() == Descriptors.FieldDescriptor.Type.STRING && k2 != null && k2.A() == Descriptors.FieldDescriptor.Type.BYTES) {
                String str = (String) o0Var.getField(k);
                if (str.isEmpty()) {
                    return false;
                }
                Object field = o0Var.getField(k2);
                try {
                    Descriptors.b b = this.b.b(str);
                    if (b == null) {
                        return false;
                    }
                    o.b newBuilderForType = o.e(b).newBuilderForType();
                    newBuilderForType.mergeFrom((ByteString) field);
                    cVar.d("[");
                    cVar.d(str);
                    cVar.d("] {");
                    cVar.a();
                    cVar.b();
                    b(newBuilderForType, cVar);
                    cVar.c();
                    cVar.d("}");
                    cVar.a();
                    return true;
                } catch (InvalidProtocolBufferException unused) {
                }
            }
            return false;
        }

        public final void f(Descriptors.FieldDescriptor fieldDescriptor, Object obj, c cVar) throws IOException {
            if (fieldDescriptor.D()) {
                ArrayList<a> arrayList = new ArrayList();
                for (Object obj2 : (List) obj) {
                    arrayList.add(new a(obj2, fieldDescriptor));
                }
                Collections.sort(arrayList);
                for (a aVar : arrayList) {
                    i(fieldDescriptor, aVar.e(), cVar);
                }
            } else if (fieldDescriptor.i()) {
                for (Object obj3 : (List) obj) {
                    i(fieldDescriptor, obj3, cVar);
                }
            } else {
                i(fieldDescriptor, obj, cVar);
            }
        }

        public final void g(Descriptors.FieldDescriptor fieldDescriptor, Object obj, c cVar) throws IOException {
            String replace;
            switch (a.b[fieldDescriptor.A().ordinal()]) {
                case 1:
                case 2:
                case 3:
                    cVar.d(((Integer) obj).toString());
                    return;
                case 4:
                case 5:
                case 6:
                    cVar.d(((Long) obj).toString());
                    return;
                case 7:
                    cVar.d(((Boolean) obj).toString());
                    return;
                case 8:
                    cVar.d(((Float) obj).toString());
                    return;
                case 9:
                    cVar.d(((Double) obj).toString());
                    return;
                case 10:
                case 11:
                    cVar.d(TextFormat.q(((Integer) obj).intValue()));
                    return;
                case 12:
                case 13:
                    cVar.d(TextFormat.r(((Long) obj).longValue()));
                    return;
                case 14:
                    cVar.d("\"");
                    if (this.a) {
                        replace = l44.e((String) obj);
                    } else {
                        replace = TextFormat.f((String) obj).replace("\n", "\\n");
                    }
                    cVar.d(replace);
                    cVar.d("\"");
                    return;
                case 15:
                    cVar.d("\"");
                    if (obj instanceof ByteString) {
                        cVar.d(TextFormat.d((ByteString) obj));
                    } else {
                        cVar.d(TextFormat.e((byte[]) obj));
                    }
                    cVar.d("\"");
                    return;
                case 16:
                    cVar.d(((Descriptors.d) obj).e());
                    return;
                case 17:
                case 18:
                    b((l0) obj, cVar);
                    return;
                default:
                    return;
            }
        }

        public final void h(o0 o0Var, c cVar) throws IOException {
            for (Map.Entry<Descriptors.FieldDescriptor, Object> entry : o0Var.getAllFields().entrySet()) {
                f(entry.getKey(), entry.getValue(), cVar);
            }
            n(o0Var.getUnknownFields(), cVar);
        }

        public final void i(Descriptors.FieldDescriptor fieldDescriptor, Object obj, c cVar) throws IOException {
            if (fieldDescriptor.C()) {
                cVar.d("[");
                if (fieldDescriptor.p().t().getMessageSetWireFormat() && fieldDescriptor.A() == Descriptors.FieldDescriptor.Type.MESSAGE && fieldDescriptor.E() && fieldDescriptor.t() == fieldDescriptor.x()) {
                    cVar.d(fieldDescriptor.x().d());
                } else {
                    cVar.d(fieldDescriptor.d());
                }
                cVar.d("]");
            } else if (fieldDescriptor.A() == Descriptors.FieldDescriptor.Type.GROUP) {
                cVar.d(fieldDescriptor.x().e());
            } else {
                cVar.d(fieldDescriptor.e());
            }
            Descriptors.FieldDescriptor.JavaType v = fieldDescriptor.v();
            Descriptors.FieldDescriptor.JavaType javaType = Descriptors.FieldDescriptor.JavaType.MESSAGE;
            if (v == javaType) {
                cVar.d(" {");
                cVar.a();
                cVar.b();
            } else {
                cVar.d(": ");
            }
            g(fieldDescriptor, obj, cVar);
            if (fieldDescriptor.v() == javaType) {
                cVar.c();
                cVar.d("}");
            }
            cVar.a();
        }

        public String j(o0 o0Var) {
            try {
                StringBuilder sb = new StringBuilder();
                c(o0Var, sb);
                return sb.toString();
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }

        public String k(g1 g1Var) {
            try {
                StringBuilder sb = new StringBuilder();
                d(g1Var, sb);
                return sb.toString();
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    /* loaded from: classes2.dex */
    public static final class c {
        public final Appendable a;
        public final StringBuilder b;
        public final boolean c;
        public boolean d;

        public /* synthetic */ c(Appendable appendable, boolean z, a aVar) {
            this(appendable, z);
        }

        public void a() throws IOException {
            if (!this.c) {
                this.a.append("\n");
            }
            this.d = true;
        }

        public void b() {
            this.b.append("  ");
        }

        public void c() {
            int length = this.b.length();
            if (length != 0) {
                this.b.setLength(length - 2);
                return;
            }
            throw new IllegalArgumentException(" Outdent() without matching Indent().");
        }

        public void d(CharSequence charSequence) throws IOException {
            if (this.d) {
                this.d = false;
                this.a.append(this.c ? MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR : this.b);
            }
            this.a.append(charSequence);
        }

        public c(Appendable appendable, boolean z) {
            this.b = new StringBuilder();
            this.d = false;
            this.a = appendable;
            this.c = z;
        }
    }

    static {
        Parser.a().a();
    }

    public static int c(byte b2) {
        if (48 > b2 || b2 > 57) {
            return ((97 > b2 || b2 > 122) ? b2 - 65 : b2 - 97) + 10;
        }
        return b2 - 48;
    }

    public static String d(ByteString byteString) {
        return l44.b(byteString);
    }

    public static String e(byte[] bArr) {
        return l44.c(bArr);
    }

    public static String f(String str) {
        return l44.d(str);
    }

    public static boolean g(byte b2) {
        return (48 <= b2 && b2 <= 57) || (97 <= b2 && b2 <= 102) || (65 <= b2 && b2 <= 70);
    }

    public static boolean h(byte b2) {
        return 48 <= b2 && b2 <= 55;
    }

    public static c i(Appendable appendable) {
        return new c(appendable, false, null);
    }

    public static int j(String str) throws NumberFormatException {
        return (int) l(str, true, false);
    }

    public static long k(String str) throws NumberFormatException {
        return l(str, true, true);
    }

    public static long l(String str, boolean z, boolean z2) throws NumberFormatException {
        int i = 0;
        boolean z3 = true;
        if (!str.startsWith("-", 0)) {
            z3 = false;
        } else if (!z) {
            throw new NumberFormatException("Number must be positive: " + str);
        } else {
            i = 1;
        }
        int i2 = 10;
        if (str.startsWith("0x", i)) {
            i += 2;
            i2 = 16;
        } else if (str.startsWith("0", i)) {
            i2 = 8;
        }
        String substring = str.substring(i);
        if (substring.length() < 16) {
            long parseLong = Long.parseLong(substring, i2);
            if (z3) {
                parseLong = -parseLong;
            }
            if (z2) {
                return parseLong;
            }
            if (z) {
                if (parseLong > 2147483647L || parseLong < -2147483648L) {
                    throw new NumberFormatException("Number out of range for 32-bit signed integer: " + str);
                }
                return parseLong;
            } else if (parseLong >= 4294967296L || parseLong < 0) {
                throw new NumberFormatException("Number out of range for 32-bit unsigned integer: " + str);
            } else {
                return parseLong;
            }
        }
        BigInteger bigInteger = new BigInteger(substring, i2);
        if (z3) {
            bigInteger = bigInteger.negate();
        }
        if (z2) {
            if (z) {
                if (bigInteger.bitLength() > 63) {
                    throw new NumberFormatException("Number out of range for 64-bit signed integer: " + str);
                }
            } else if (bigInteger.bitLength() > 64) {
                throw new NumberFormatException("Number out of range for 64-bit unsigned integer: " + str);
            }
        } else if (z) {
            if (bigInteger.bitLength() > 31) {
                throw new NumberFormatException("Number out of range for 32-bit signed integer: " + str);
            }
        } else if (bigInteger.bitLength() > 32) {
            throw new NumberFormatException("Number out of range for 32-bit unsigned integer: " + str);
        }
        return bigInteger.longValue();
    }

    public static int m(String str) throws NumberFormatException {
        return (int) l(str, false, false);
    }

    public static long n(String str) throws NumberFormatException {
        return l(str, false, true);
    }

    public static b o() {
        return b.c;
    }

    public static ByteString p(CharSequence charSequence) throws InvalidEscapeSequenceException {
        int i;
        int i2;
        ByteString copyFromUtf8 = ByteString.copyFromUtf8(charSequence.toString());
        int size = copyFromUtf8.size();
        byte[] bArr = new byte[size];
        int i3 = 0;
        int i4 = 0;
        while (i3 < copyFromUtf8.size()) {
            byte byteAt = copyFromUtf8.byteAt(i3);
            if (byteAt == 92) {
                i3++;
                if (i3 < copyFromUtf8.size()) {
                    byte byteAt2 = copyFromUtf8.byteAt(i3);
                    if (h(byteAt2)) {
                        int c2 = c(byteAt2);
                        int i5 = i3 + 1;
                        if (i5 < copyFromUtf8.size() && h(copyFromUtf8.byteAt(i5))) {
                            c2 = (c2 * 8) + c(copyFromUtf8.byteAt(i5));
                            i3 = i5;
                        }
                        int i6 = i3 + 1;
                        if (i6 < copyFromUtf8.size() && h(copyFromUtf8.byteAt(i6))) {
                            c2 = (c2 * 8) + c(copyFromUtf8.byteAt(i6));
                            i3 = i6;
                        }
                        i = i4 + 1;
                        bArr[i4] = (byte) c2;
                    } else {
                        if (byteAt2 == 34) {
                            i2 = i4 + 1;
                            bArr[i4] = 34;
                        } else if (byteAt2 == 39) {
                            i2 = i4 + 1;
                            bArr[i4] = 39;
                        } else if (byteAt2 == 92) {
                            i2 = i4 + 1;
                            bArr[i4] = 92;
                        } else if (byteAt2 == 102) {
                            i2 = i4 + 1;
                            bArr[i4] = 12;
                        } else if (byteAt2 == 110) {
                            i2 = i4 + 1;
                            bArr[i4] = 10;
                        } else if (byteAt2 == 114) {
                            i2 = i4 + 1;
                            bArr[i4] = 13;
                        } else if (byteAt2 == 116) {
                            i2 = i4 + 1;
                            bArr[i4] = 9;
                        } else if (byteAt2 == 118) {
                            i2 = i4 + 1;
                            bArr[i4] = 11;
                        } else if (byteAt2 == 120) {
                            i3++;
                            if (i3 < copyFromUtf8.size() && g(copyFromUtf8.byteAt(i3))) {
                                int c3 = c(copyFromUtf8.byteAt(i3));
                                int i7 = i3 + 1;
                                if (i7 < copyFromUtf8.size() && g(copyFromUtf8.byteAt(i7))) {
                                    c3 = (c3 * 16) + c(copyFromUtf8.byteAt(i7));
                                    i3 = i7;
                                }
                                i = i4 + 1;
                                bArr[i4] = (byte) c3;
                            } else {
                                throw new InvalidEscapeSequenceException("Invalid escape sequence: '\\x' with no digits");
                            }
                        } else if (byteAt2 == 97) {
                            i2 = i4 + 1;
                            bArr[i4] = 7;
                        } else if (byteAt2 == 98) {
                            i2 = i4 + 1;
                            bArr[i4] = 8;
                        } else {
                            throw new InvalidEscapeSequenceException("Invalid escape sequence: '\\" + ((char) byteAt2) + '\'');
                        }
                        i4 = i2;
                        i3++;
                    }
                } else {
                    throw new InvalidEscapeSequenceException("Invalid escape sequence: '\\' at end of string.");
                }
            } else {
                i = i4 + 1;
                bArr[i4] = byteAt;
            }
            i4 = i;
            i3++;
        }
        if (size == i4) {
            return ByteString.wrap(bArr);
        }
        return ByteString.copyFrom(bArr, 0, i4);
    }

    public static String q(int i) {
        if (i >= 0) {
            return Integer.toString(i);
        }
        return Long.toString(i & 4294967295L);
    }

    public static String r(long j) {
        if (j >= 0) {
            return Long.toString(j);
        }
        return BigInteger.valueOf(j & Long.MAX_VALUE).setBit(63).toString();
    }
}
