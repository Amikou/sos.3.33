package com.google.protobuf;

import com.google.protobuf.a0;
import java.lang.reflect.Field;

/* compiled from: FieldInfo.java */
/* loaded from: classes2.dex */
public final class v implements Comparable<v> {
    public final Field a;
    public final FieldType f0;
    public final Class<?> g0;
    public final int h0;
    public final Field i0;
    public final int j0;
    public final boolean k0;
    public final boolean l0;
    public final en2 m0;
    public final Field n0;
    public final Class<?> o0;
    public final Object p0;
    public final a0.e q0;

    /* compiled from: FieldInfo.java */
    /* loaded from: classes2.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[FieldType.values().length];
            a = iArr;
            try {
                iArr[FieldType.MESSAGE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[FieldType.GROUP.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[FieldType.MESSAGE_LIST.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                a[FieldType.GROUP_LIST.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
        }
    }

    public v(Field field, int i, FieldType fieldType, Class<?> cls, Field field2, int i2, boolean z, boolean z2, en2 en2Var, Class<?> cls2, Object obj, a0.e eVar, Field field3) {
        this.a = field;
        this.f0 = fieldType;
        this.g0 = cls;
        this.h0 = i;
        this.i0 = field2;
        this.j0 = i2;
        this.k0 = z;
        this.l0 = z2;
        this.m0 = en2Var;
        this.o0 = cls2;
        this.p0 = obj;
        this.q0 = eVar;
        this.n0 = field3;
    }

    public static boolean E(int i) {
        return i != 0 && (i & (i + (-1))) == 0;
    }

    public static void a(int i) {
        if (i > 0) {
            return;
        }
        throw new IllegalArgumentException("fieldNumber must be positive: " + i);
    }

    public static v e(Field field, int i, FieldType fieldType, boolean z) {
        a(i);
        a0.b(field, "field");
        a0.b(fieldType, "fieldType");
        if (fieldType != FieldType.MESSAGE_LIST && fieldType != FieldType.GROUP_LIST) {
            return new v(field, i, fieldType, null, null, 0, false, z, null, null, null, null, null);
        }
        throw new IllegalStateException("Shouldn't be called for repeated message fields.");
    }

    public static v f(Field field, int i, FieldType fieldType, a0.e eVar) {
        a(i);
        a0.b(field, "field");
        return new v(field, i, fieldType, null, null, 0, false, false, null, null, null, eVar, null);
    }

    public static v g(Field field, int i, Object obj, a0.e eVar) {
        a0.b(obj, "mapDefaultEntry");
        a(i);
        a0.b(field, "field");
        return new v(field, i, FieldType.MAP, null, null, 0, false, true, null, null, obj, eVar, null);
    }

    public static v h(int i, FieldType fieldType, en2 en2Var, Class<?> cls, boolean z, a0.e eVar) {
        a(i);
        a0.b(fieldType, "fieldType");
        a0.b(en2Var, "oneof");
        a0.b(cls, "oneofStoredType");
        if (fieldType.isScalar()) {
            return new v(null, i, fieldType, null, null, 0, false, z, en2Var, cls, null, eVar, null);
        }
        throw new IllegalArgumentException("Oneof is only supported for scalar fields. Field " + i + " is of type " + fieldType);
    }

    public static v j(Field field, int i, FieldType fieldType, Field field2) {
        a(i);
        a0.b(field, "field");
        a0.b(fieldType, "fieldType");
        if (fieldType != FieldType.MESSAGE_LIST && fieldType != FieldType.GROUP_LIST) {
            return new v(field, i, fieldType, null, null, 0, false, false, null, null, null, null, field2);
        }
        throw new IllegalStateException("Shouldn't be called for repeated message fields.");
    }

    public static v k(Field field, int i, FieldType fieldType, a0.e eVar, Field field2) {
        a(i);
        a0.b(field, "field");
        return new v(field, i, fieldType, null, null, 0, false, false, null, null, null, eVar, field2);
    }

    public static v l(Field field, int i, FieldType fieldType, Field field2, int i2, boolean z, a0.e eVar) {
        a(i);
        a0.b(field, "field");
        a0.b(fieldType, "fieldType");
        a0.b(field2, "presenceField");
        if (field2 != null && !E(i2)) {
            throw new IllegalArgumentException("presenceMask must have exactly one bit set: " + i2);
        }
        return new v(field, i, fieldType, null, field2, i2, false, z, null, null, null, eVar, null);
    }

    public static v o(Field field, int i, FieldType fieldType, Field field2, int i2, boolean z, a0.e eVar) {
        a(i);
        a0.b(field, "field");
        a0.b(fieldType, "fieldType");
        a0.b(field2, "presenceField");
        if (field2 != null && !E(i2)) {
            throw new IllegalArgumentException("presenceMask must have exactly one bit set: " + i2);
        }
        return new v(field, i, fieldType, null, field2, i2, true, z, null, null, null, eVar, null);
    }

    public static v p(Field field, int i, FieldType fieldType, Class<?> cls) {
        a(i);
        a0.b(field, "field");
        a0.b(fieldType, "fieldType");
        a0.b(cls, "messageClass");
        return new v(field, i, fieldType, cls, null, 0, false, false, null, null, null, null, null);
    }

    public Field A() {
        return this.i0;
    }

    public int B() {
        return this.j0;
    }

    public FieldType C() {
        return this.f0;
    }

    public boolean D() {
        return this.l0;
    }

    public boolean G() {
        return this.k0;
    }

    @Override // java.lang.Comparable
    /* renamed from: d */
    public int compareTo(v vVar) {
        return this.h0 - vVar.h0;
    }

    public Field r() {
        return this.n0;
    }

    public a0.e s() {
        return this.q0;
    }

    public Field t() {
        return this.a;
    }

    public int u() {
        return this.h0;
    }

    public Object v() {
        return this.p0;
    }

    public Class<?> x() {
        int i = a.a[this.f0.ordinal()];
        if (i == 1 || i == 2) {
            Field field = this.a;
            return field != null ? field.getType() : this.o0;
        } else if (i == 3 || i == 4) {
            return this.g0;
        } else {
            return null;
        }
    }

    public en2 y() {
        return this.m0;
    }
}
