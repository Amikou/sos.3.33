package com.google.protobuf;

import com.google.protobuf.Descriptors;
import com.google.protobuf.Extension;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.a;
import com.google.protobuf.g1;
import com.google.protobuf.l0;
import com.google.protobuf.m0;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/* loaded from: classes2.dex */
public abstract class GeneratedMessage extends com.google.protobuf.a implements Serializable {
    public static boolean alwaysUseFieldBuilders = false;
    private static final long serialVersionUID = 1;
    public g1 unknownFields;

    /* loaded from: classes2.dex */
    public class a implements g {
        public final /* synthetic */ a.b a;

        public a(GeneratedMessage generatedMessage, a.b bVar) {
            this.a = bVar;
        }

        @Override // com.google.protobuf.a.b
        public void a() {
            this.a.a();
        }
    }

    /* loaded from: classes2.dex */
    public static class b extends h {
        public final /* synthetic */ l0 b;
        public final /* synthetic */ int c;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(l0 l0Var, int i) {
            super(null);
            this.b = l0Var;
            this.c = i;
        }

        @Override // com.google.protobuf.GeneratedMessage.h
        public Descriptors.FieldDescriptor b() {
            return this.b.getDescriptorForType().o().get(this.c);
        }
    }

    /* loaded from: classes2.dex */
    public static class c extends h {
        public final /* synthetic */ l0 b;
        public final /* synthetic */ String c;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public c(l0 l0Var, String str) {
            super(null);
            this.b = l0Var;
            this.c = str;
        }

        @Override // com.google.protobuf.GeneratedMessage.h
        public Descriptors.FieldDescriptor b() {
            return this.b.getDescriptorForType().j(this.c);
        }
    }

    /* loaded from: classes2.dex */
    public static class d extends h {
        public final /* synthetic */ Class b;
        public final /* synthetic */ String c;
        public final /* synthetic */ String d;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public d(Class cls, String str, String str2) {
            super(null);
            this.b = cls;
            this.c = str;
            this.d = str2;
        }

        @Override // com.google.protobuf.GeneratedMessage.h
        public Descriptors.FieldDescriptor b() {
            try {
                return ((Descriptors.FileDescriptor) this.b.getClassLoader().loadClass(this.c).getField("descriptor").get(null)).k(this.d);
            } catch (Exception e) {
                throw new RuntimeException("Cannot load descriptors: " + this.c + " is not a valid descriptor class name", e);
            }
        }
    }

    /* loaded from: classes2.dex */
    public static /* synthetic */ class e {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[Descriptors.FieldDescriptor.JavaType.values().length];
            a = iArr;
            try {
                iArr[Descriptors.FieldDescriptor.JavaType.MESSAGE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[Descriptors.FieldDescriptor.JavaType.ENUM.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
        }
    }

    /* loaded from: classes2.dex */
    public static abstract class f<BuilderType extends f<BuilderType>> extends a.AbstractC0151a<BuilderType> {
    }

    /* loaded from: classes2.dex */
    public interface g extends a.b {
    }

    /* loaded from: classes2.dex */
    public static abstract class h implements i {
        public volatile Descriptors.FieldDescriptor a;

        public h() {
        }

        @Override // com.google.protobuf.GeneratedMessage.i
        public Descriptors.FieldDescriptor a() {
            if (this.a == null) {
                synchronized (this) {
                    if (this.a == null) {
                        this.a = b();
                    }
                }
            }
            return this.a;
        }

        public abstract Descriptors.FieldDescriptor b();

        public /* synthetic */ h(a aVar) {
            this();
        }
    }

    /* loaded from: classes2.dex */
    public interface i {
        Descriptors.FieldDescriptor a();
    }

    /* loaded from: classes2.dex */
    public static final class j {

        /* loaded from: classes2.dex */
        public interface a {
            Object a(GeneratedMessage generatedMessage, int i);

            Object b(GeneratedMessage generatedMessage);

            boolean c(GeneratedMessage generatedMessage);

            Object d(GeneratedMessage generatedMessage);

            int e(GeneratedMessage generatedMessage);
        }

        /* loaded from: classes2.dex */
        public static class b {
        }

        public static /* synthetic */ Descriptors.b a(j jVar) {
            throw null;
        }

        public static /* synthetic */ b b(j jVar, Descriptors.g gVar) {
            throw null;
        }

        public static /* synthetic */ a c(j jVar, Descriptors.FieldDescriptor fieldDescriptor) {
            throw null;
        }
    }

    /* loaded from: classes2.dex */
    public static class k<ContainingType extends l0, Type> extends Extension<ContainingType, Type> {
        public i a;
        public final Class b;
        public final l0 c;
        public final Method d;

        public k(i iVar, Class cls, l0 l0Var, Extension.ExtensionType extensionType) {
            if (l0.class.isAssignableFrom(cls) && !cls.isInstance(l0Var)) {
                throw new IllegalArgumentException("Bad messageDefaultInstance for " + cls.getName());
            }
            this.a = iVar;
            this.b = cls;
            this.c = l0Var;
            if (v0.class.isAssignableFrom(cls)) {
                this.d = GeneratedMessage.getMethodOrDie(cls, "valueOf", Descriptors.d.class);
                GeneratedMessage.getMethodOrDie(cls, "getValueDescriptor", new Class[0]);
                return;
            }
            this.d = null;
        }

        @Override // com.google.protobuf.Extension
        public Object b(Object obj) {
            Descriptors.FieldDescriptor c = c();
            if (c.i()) {
                if (c.v() == Descriptors.FieldDescriptor.JavaType.MESSAGE || c.v() == Descriptors.FieldDescriptor.JavaType.ENUM) {
                    ArrayList arrayList = new ArrayList();
                    for (Object obj2 : (List) obj) {
                        arrayList.add(e(obj2));
                    }
                    return arrayList;
                }
                return obj;
            }
            return e(obj);
        }

        @Override // com.google.protobuf.Extension
        public Descriptors.FieldDescriptor c() {
            i iVar = this.a;
            if (iVar != null) {
                return iVar.a();
            }
            throw new IllegalStateException("getDescriptor() called before internalInit()");
        }

        @Override // com.google.protobuf.Extension
        public l0 d() {
            return this.c;
        }

        @Override // com.google.protobuf.Extension
        public Object e(Object obj) {
            int i = e.a[c().v().ordinal()];
            return i != 1 ? i != 2 ? obj : GeneratedMessage.invokeOrDie(this.d, null, (Descriptors.d) obj) : this.b.isInstance(obj) ? obj : this.c.newBuilderForType().mergeFrom((l0) obj).build();
        }
    }

    public GeneratedMessage() {
        this.unknownFields = g1.c();
    }

    public static <MessageType, T> Extension<MessageType, T> checkNotLite(p<MessageType, T> pVar) {
        if (!pVar.a()) {
            return (Extension) pVar;
        }
        throw new IllegalArgumentException("Expected non-lite extension.");
    }

    public static int computeStringSize(int i2, Object obj) {
        if (obj instanceof String) {
            return CodedOutputStream.V(i2, (String) obj);
        }
        return CodedOutputStream.h(i2, (ByteString) obj);
    }

    public static int computeStringSizeNoTag(Object obj) {
        if (obj instanceof String) {
            return CodedOutputStream.W((String) obj);
        }
        return CodedOutputStream.i((ByteString) obj);
    }

    public static void enableAlwaysUseFieldBuildersForTesting() {
        alwaysUseFieldBuilders = true;
    }

    public static Method getMethodOrDie(Class cls, String str, Class... clsArr) {
        try {
            return cls.getMethod(str, clsArr);
        } catch (NoSuchMethodException e2) {
            throw new RuntimeException("Generated message class \"" + cls.getName() + "\" missing method \"" + str + "\".", e2);
        }
    }

    public static Object invokeOrDie(Method method, Object obj, Object... objArr) {
        try {
            return method.invoke(obj, objArr);
        } catch (IllegalAccessException e2) {
            throw new RuntimeException("Couldn't use Java reflection to implement protocol message reflection.", e2);
        } catch (InvocationTargetException e3) {
            Throwable cause = e3.getCause();
            if (!(cause instanceof RuntimeException)) {
                if (cause instanceof Error) {
                    throw ((Error) cause);
                }
                throw new RuntimeException("Unexpected exception thrown by generated accessor method.", cause);
            }
            throw ((RuntimeException) cause);
        }
    }

    public static <ContainingType extends l0, Type> k<ContainingType, Type> newFileScopedGeneratedExtension(Class cls, l0 l0Var) {
        return new k<>(null, cls, l0Var, Extension.ExtensionType.IMMUTABLE);
    }

    public static <ContainingType extends l0, Type> k<ContainingType, Type> newMessageScopedGeneratedExtension(l0 l0Var, int i2, Class cls, l0 l0Var2) {
        return new k<>(new b(l0Var, i2), cls, l0Var2, Extension.ExtensionType.IMMUTABLE);
    }

    public static <M extends l0> M parseDelimitedWithIOException(t0<M> t0Var, InputStream inputStream) throws IOException {
        try {
            return t0Var.parseDelimitedFrom(inputStream);
        } catch (InvalidProtocolBufferException e2) {
            throw e2.unwrapIOException();
        }
    }

    public static <M extends l0> M parseWithIOException(t0<M> t0Var, InputStream inputStream) throws IOException {
        try {
            return t0Var.parseFrom(inputStream);
        } catch (InvalidProtocolBufferException e2) {
            throw e2.unwrapIOException();
        }
    }

    public static void writeString(CodedOutputStream codedOutputStream, int i2, Object obj) throws IOException {
        if (obj instanceof String) {
            codedOutputStream.Y0(i2, (String) obj);
        } else {
            codedOutputStream.q0(i2, (ByteString) obj);
        }
    }

    public static void writeStringNoTag(CodedOutputStream codedOutputStream, Object obj) throws IOException {
        if (obj instanceof String) {
            codedOutputStream.Z0((String) obj);
        } else {
            codedOutputStream.r0((ByteString) obj);
        }
    }

    @Override // com.google.protobuf.o0
    public Map<Descriptors.FieldDescriptor, Object> getAllFields() {
        return Collections.unmodifiableMap(getAllFieldsMutable(false));
    }

    public final Map<Descriptors.FieldDescriptor, Object> getAllFieldsMutable(boolean z) {
        TreeMap treeMap = new TreeMap();
        List<Descriptors.FieldDescriptor> p = j.a(internalGetFieldAccessorTable()).p();
        int i2 = 0;
        while (i2 < p.size()) {
            Descriptors.FieldDescriptor fieldDescriptor = p.get(i2);
            Descriptors.g o = fieldDescriptor.o();
            if (o != null) {
                i2 += o.o() - 1;
                if (hasOneof(o)) {
                    fieldDescriptor = getOneofFieldDescriptor(o);
                    if (!z && fieldDescriptor.v() == Descriptors.FieldDescriptor.JavaType.STRING) {
                        treeMap.put(fieldDescriptor, getFieldRaw(fieldDescriptor));
                    } else {
                        treeMap.put(fieldDescriptor, getField(fieldDescriptor));
                    }
                    i2++;
                } else {
                    i2++;
                }
            } else {
                if (fieldDescriptor.i()) {
                    List list = (List) getField(fieldDescriptor);
                    if (!list.isEmpty()) {
                        treeMap.put(fieldDescriptor, list);
                    }
                } else {
                    if (!hasField(fieldDescriptor)) {
                    }
                    if (!z) {
                    }
                    treeMap.put(fieldDescriptor, getField(fieldDescriptor));
                }
                i2++;
            }
        }
        return treeMap;
    }

    public Map<Descriptors.FieldDescriptor, Object> getAllFieldsRaw() {
        return Collections.unmodifiableMap(getAllFieldsMutable(true));
    }

    @Override // defpackage.d82, com.google.protobuf.o0
    public abstract /* synthetic */ l0 getDefaultInstanceForType();

    @Override // defpackage.d82, com.google.protobuf.o0
    public abstract /* synthetic */ m0 getDefaultInstanceForType();

    @Override // com.google.protobuf.o0
    public Descriptors.b getDescriptorForType() {
        return j.a(internalGetFieldAccessorTable());
    }

    @Override // com.google.protobuf.o0
    public Object getField(Descriptors.FieldDescriptor fieldDescriptor) {
        return j.c(internalGetFieldAccessorTable(), fieldDescriptor).b(this);
    }

    public Object getFieldRaw(Descriptors.FieldDescriptor fieldDescriptor) {
        return j.c(internalGetFieldAccessorTable(), fieldDescriptor).d(this);
    }

    @Override // com.google.protobuf.a
    public Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar) {
        j.b(internalGetFieldAccessorTable(), gVar);
        throw null;
    }

    @Override // com.google.protobuf.m0
    public t0<? extends GeneratedMessage> getParserForType() {
        throw new UnsupportedOperationException("This is supposed to be overridden by subclasses.");
    }

    public Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i2) {
        return j.c(internalGetFieldAccessorTable(), fieldDescriptor).a(this, i2);
    }

    public int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor) {
        return j.c(internalGetFieldAccessorTable(), fieldDescriptor).e(this);
    }

    @Override // com.google.protobuf.a, com.google.protobuf.m0
    public int getSerializedSize() {
        int i2 = this.memoizedSize;
        if (i2 != -1) {
            return i2;
        }
        int e2 = MessageReflection.e(this, getAllFieldsRaw());
        this.memoizedSize = e2;
        return e2;
    }

    @Override // com.google.protobuf.o0
    public g1 getUnknownFields() {
        throw new UnsupportedOperationException("This is supposed to be overridden by subclasses.");
    }

    @Override // com.google.protobuf.o0
    public boolean hasField(Descriptors.FieldDescriptor fieldDescriptor) {
        return j.c(internalGetFieldAccessorTable(), fieldDescriptor).c(this);
    }

    @Override // com.google.protobuf.a
    public boolean hasOneof(Descriptors.g gVar) {
        j.b(internalGetFieldAccessorTable(), gVar);
        throw null;
    }

    public abstract j internalGetFieldAccessorTable();

    public MapField internalGetMapField(int i2) {
        throw new RuntimeException("No map fields found in " + getClass().getName());
    }

    @Override // com.google.protobuf.a, defpackage.d82
    public boolean isInitialized() {
        for (Descriptors.FieldDescriptor fieldDescriptor : getDescriptorForType().p()) {
            if (fieldDescriptor.H() && !hasField(fieldDescriptor)) {
                return false;
            }
            if (fieldDescriptor.v() == Descriptors.FieldDescriptor.JavaType.MESSAGE) {
                if (fieldDescriptor.i()) {
                    for (l0 l0Var : (List) getField(fieldDescriptor)) {
                        if (!l0Var.isInitialized()) {
                            return false;
                        }
                    }
                    continue;
                } else if (hasField(fieldDescriptor) && !((l0) getField(fieldDescriptor)).isInitialized()) {
                    return false;
                }
            }
        }
        return true;
    }

    public void makeExtensionsImmutable() {
    }

    @Override // com.google.protobuf.m0, com.google.protobuf.l0
    public abstract /* synthetic */ l0.a newBuilderForType();

    public abstract l0.a newBuilderForType(g gVar);

    @Override // com.google.protobuf.a
    public l0.a newBuilderForType(a.b bVar) {
        return newBuilderForType((g) new a(this, bVar));
    }

    @Override // com.google.protobuf.m0, com.google.protobuf.l0
    public abstract /* synthetic */ m0.a newBuilderForType();

    public boolean parseUnknownField(com.google.protobuf.j jVar, g1.b bVar, r rVar, int i2) throws IOException {
        return bVar.o(i2, jVar);
    }

    @Override // com.google.protobuf.m0, com.google.protobuf.l0
    public abstract /* synthetic */ l0.a toBuilder();

    @Override // com.google.protobuf.m0, com.google.protobuf.l0
    public abstract /* synthetic */ m0.a toBuilder();

    public Object writeReplace() throws ObjectStreamException {
        return new GeneratedMessageLite.SerializedForm(this);
    }

    @Override // com.google.protobuf.a, com.google.protobuf.m0
    public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
        MessageReflection.k(this, getAllFieldsRaw(), codedOutputStream, false);
    }

    public static <ContainingType extends l0, Type> k<ContainingType, Type> newFileScopedGeneratedExtension(Class cls, l0 l0Var, String str, String str2) {
        return new k<>(new d(cls, str, str2), cls, l0Var, Extension.ExtensionType.MUTABLE);
    }

    public static <ContainingType extends l0, Type> k<ContainingType, Type> newMessageScopedGeneratedExtension(l0 l0Var, String str, Class cls, l0 l0Var2) {
        return new k<>(new c(l0Var, str), cls, l0Var2, Extension.ExtensionType.MUTABLE);
    }

    public GeneratedMessage(f<?> fVar) {
        throw null;
    }

    public static <M extends l0> M parseDelimitedWithIOException(t0<M> t0Var, InputStream inputStream, r rVar) throws IOException {
        try {
            return t0Var.parseDelimitedFrom(inputStream, rVar);
        } catch (InvalidProtocolBufferException e2) {
            throw e2.unwrapIOException();
        }
    }

    public static <M extends l0> M parseWithIOException(t0<M> t0Var, InputStream inputStream, r rVar) throws IOException {
        try {
            return t0Var.parseFrom(inputStream, rVar);
        } catch (InvalidProtocolBufferException e2) {
            throw e2.unwrapIOException();
        }
    }

    public static <M extends l0> M parseWithIOException(t0<M> t0Var, com.google.protobuf.j jVar) throws IOException {
        try {
            return t0Var.parseFrom(jVar);
        } catch (InvalidProtocolBufferException e2) {
            throw e2.unwrapIOException();
        }
    }

    public static <M extends l0> M parseWithIOException(t0<M> t0Var, com.google.protobuf.j jVar, r rVar) throws IOException {
        try {
            return t0Var.parseFrom(jVar, rVar);
        } catch (InvalidProtocolBufferException e2) {
            throw e2.unwrapIOException();
        }
    }
}
