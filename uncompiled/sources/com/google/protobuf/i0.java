package com.google.protobuf;

import com.google.protobuf.h0;
import java.util.Map;

/* compiled from: MapFieldSchema.java */
/* loaded from: classes2.dex */
public interface i0 {
    Object a(Object obj, Object obj2);

    Object b(Object obj);

    h0.b<?, ?> c(Object obj);

    Map<?, ?> d(Object obj);

    Object e(Object obj);

    int f(int i, Object obj, Object obj2);

    boolean g(Object obj);

    Map<?, ?> h(Object obj);
}
