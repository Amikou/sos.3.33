package com.google.protobuf;

import java.io.IOException;

/* compiled from: UnknownFieldSetLiteSchema.java */
/* loaded from: classes2.dex */
public class i1 extends f1<h1, h1> {
    @Override // com.google.protobuf.f1
    /* renamed from: A */
    public h1 g(Object obj) {
        return ((GeneratedMessageLite) obj).a;
    }

    @Override // com.google.protobuf.f1
    /* renamed from: B */
    public int h(h1 h1Var) {
        return h1Var.d();
    }

    @Override // com.google.protobuf.f1
    /* renamed from: C */
    public int i(h1 h1Var) {
        return h1Var.e();
    }

    @Override // com.google.protobuf.f1
    /* renamed from: D */
    public h1 k(h1 h1Var, h1 h1Var2) {
        return h1Var2.equals(h1.c()) ? h1Var : h1.i(h1Var, h1Var2);
    }

    @Override // com.google.protobuf.f1
    /* renamed from: E */
    public h1 n() {
        return h1.j();
    }

    @Override // com.google.protobuf.f1
    /* renamed from: F */
    public void o(Object obj, h1 h1Var) {
        p(obj, h1Var);
    }

    @Override // com.google.protobuf.f1
    /* renamed from: G */
    public void p(Object obj, h1 h1Var) {
        ((GeneratedMessageLite) obj).a = h1Var;
    }

    @Override // com.google.protobuf.f1
    /* renamed from: H */
    public h1 r(h1 h1Var) {
        h1Var.h();
        return h1Var;
    }

    @Override // com.google.protobuf.f1
    /* renamed from: I */
    public void s(h1 h1Var, Writer writer) throws IOException {
        h1Var.o(writer);
    }

    @Override // com.google.protobuf.f1
    /* renamed from: J */
    public void t(h1 h1Var, Writer writer) throws IOException {
        h1Var.q(writer);
    }

    @Override // com.google.protobuf.f1
    public void j(Object obj) {
        g(obj).h();
    }

    @Override // com.google.protobuf.f1
    public boolean q(w0 w0Var) {
        return false;
    }

    @Override // com.google.protobuf.f1
    /* renamed from: u */
    public void a(h1 h1Var, int i, int i2) {
        h1Var.m(WireFormat.c(i, 5), Integer.valueOf(i2));
    }

    @Override // com.google.protobuf.f1
    /* renamed from: v */
    public void b(h1 h1Var, int i, long j) {
        h1Var.m(WireFormat.c(i, 1), Long.valueOf(j));
    }

    @Override // com.google.protobuf.f1
    /* renamed from: w */
    public void c(h1 h1Var, int i, h1 h1Var2) {
        h1Var.m(WireFormat.c(i, 3), h1Var2);
    }

    @Override // com.google.protobuf.f1
    /* renamed from: x */
    public void d(h1 h1Var, int i, ByteString byteString) {
        h1Var.m(WireFormat.c(i, 2), byteString);
    }

    @Override // com.google.protobuf.f1
    /* renamed from: y */
    public void e(h1 h1Var, int i, long j) {
        h1Var.m(WireFormat.c(i, 0), Long.valueOf(j));
    }

    @Override // com.google.protobuf.f1
    /* renamed from: z */
    public h1 f(Object obj) {
        h1 g = g(obj);
        if (g == h1.c()) {
            h1 j = h1.j();
            p(obj, j);
            return j;
        }
        return g;
    }
}
