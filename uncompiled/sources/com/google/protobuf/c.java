package com.google.protobuf;

import com.google.protobuf.b;
import com.google.protobuf.m0;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* compiled from: AbstractParser.java */
/* loaded from: classes2.dex */
public abstract class c<MessageType extends m0> implements t0<MessageType> {
    private static final r EMPTY_REGISTRY = r.b();

    private MessageType checkMessageInitialized(MessageType messagetype) throws InvalidProtocolBufferException {
        if (messagetype == null || messagetype.isInitialized()) {
            return messagetype;
        }
        throw newUninitializedMessageException(messagetype).asInvalidProtocolBufferException().setUnfinishedMessage(messagetype);
    }

    private UninitializedMessageException newUninitializedMessageException(MessageType messagetype) {
        if (messagetype instanceof b) {
            return ((b) messagetype).newUninitializedMessageException();
        }
        return new UninitializedMessageException(messagetype);
    }

    @Override // com.google.protobuf.t0
    public MessageType parseDelimitedFrom(InputStream inputStream, r rVar) throws InvalidProtocolBufferException {
        return checkMessageInitialized(m37parsePartialDelimitedFrom(inputStream, rVar));
    }

    /* renamed from: parsePartialDelimitedFrom */
    public MessageType m37parsePartialDelimitedFrom(InputStream inputStream, r rVar) throws InvalidProtocolBufferException {
        try {
            int read = inputStream.read();
            if (read == -1) {
                return null;
            }
            return m42parsePartialFrom((InputStream) new b.a.C0152a(inputStream, j.C(read, inputStream)), rVar);
        } catch (IOException e) {
            throw new InvalidProtocolBufferException(e);
        }
    }

    @Override // com.google.protobuf.t0
    public MessageType parseDelimitedFrom(InputStream inputStream) throws InvalidProtocolBufferException {
        return parseDelimitedFrom(inputStream, EMPTY_REGISTRY);
    }

    /* renamed from: parsePartialDelimitedFrom */
    public MessageType m36parsePartialDelimitedFrom(InputStream inputStream) throws InvalidProtocolBufferException {
        return m37parsePartialDelimitedFrom(inputStream, EMPTY_REGISTRY);
    }

    /* renamed from: parsePartialFrom */
    public MessageType m40parsePartialFrom(j jVar) throws InvalidProtocolBufferException {
        return (MessageType) parsePartialFrom(jVar, EMPTY_REGISTRY);
    }

    /* renamed from: parsePartialFrom */
    public MessageType m39parsePartialFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
        try {
            j newCodedInput = byteString.newCodedInput();
            MessageType messagetype = (MessageType) parsePartialFrom(newCodedInput, rVar);
            try {
                newCodedInput.a(0);
                return messagetype;
            } catch (InvalidProtocolBufferException e) {
                throw e.setUnfinishedMessage(messagetype);
            }
        } catch (InvalidProtocolBufferException e2) {
            throw e2;
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.google.protobuf.t0
    public MessageType parseFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
        return (MessageType) checkMessageInitialized((m0) parsePartialFrom(jVar, rVar));
    }

    @Override // com.google.protobuf.t0
    public MessageType parseFrom(j jVar) throws InvalidProtocolBufferException {
        return parseFrom(jVar, EMPTY_REGISTRY);
    }

    @Override // com.google.protobuf.t0
    public MessageType parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
        return checkMessageInitialized(m39parsePartialFrom(byteString, rVar));
    }

    @Override // com.google.protobuf.t0
    public MessageType parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return parseFrom(byteString, EMPTY_REGISTRY);
    }

    /* renamed from: parsePartialFrom */
    public MessageType m38parsePartialFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return m39parsePartialFrom(byteString, EMPTY_REGISTRY);
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.google.protobuf.t0
    public MessageType parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
        try {
            j i = j.i(byteBuffer);
            m0 m0Var = (m0) parsePartialFrom(i, rVar);
            try {
                i.a(0);
                return (MessageType) checkMessageInitialized(m0Var);
            } catch (InvalidProtocolBufferException e) {
                throw e.setUnfinishedMessage(m0Var);
            }
        } catch (InvalidProtocolBufferException e2) {
            throw e2;
        }
    }

    /* renamed from: parsePartialFrom */
    public MessageType m45parsePartialFrom(byte[] bArr, int i, int i2, r rVar) throws InvalidProtocolBufferException {
        try {
            j l = j.l(bArr, i, i2);
            MessageType messagetype = (MessageType) parsePartialFrom(l, rVar);
            try {
                l.a(0);
                return messagetype;
            } catch (InvalidProtocolBufferException e) {
                throw e.setUnfinishedMessage(messagetype);
            }
        } catch (InvalidProtocolBufferException e2) {
            throw e2;
        }
    }

    /* renamed from: parsePartialFrom */
    public MessageType m44parsePartialFrom(byte[] bArr, int i, int i2) throws InvalidProtocolBufferException {
        return m45parsePartialFrom(bArr, i, i2, EMPTY_REGISTRY);
    }

    @Override // com.google.protobuf.t0
    public MessageType parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return parseFrom(byteBuffer, EMPTY_REGISTRY);
    }

    /* renamed from: parsePartialFrom */
    public MessageType m46parsePartialFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
        return m45parsePartialFrom(bArr, 0, bArr.length, rVar);
    }

    /* renamed from: parseFrom */
    public MessageType m35parseFrom(byte[] bArr, int i, int i2, r rVar) throws InvalidProtocolBufferException {
        return checkMessageInitialized(m45parsePartialFrom(bArr, i, i2, rVar));
    }

    /* renamed from: parsePartialFrom */
    public MessageType m43parsePartialFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return m45parsePartialFrom(bArr, 0, bArr.length, EMPTY_REGISTRY);
    }

    /* renamed from: parseFrom */
    public MessageType m34parseFrom(byte[] bArr, int i, int i2) throws InvalidProtocolBufferException {
        return m35parseFrom(bArr, i, i2, EMPTY_REGISTRY);
    }

    /* renamed from: parsePartialFrom */
    public MessageType m42parsePartialFrom(InputStream inputStream, r rVar) throws InvalidProtocolBufferException {
        j g = j.g(inputStream);
        MessageType messagetype = (MessageType) parsePartialFrom(g, rVar);
        try {
            g.a(0);
            return messagetype;
        } catch (InvalidProtocolBufferException e) {
            throw e.setUnfinishedMessage(messagetype);
        }
    }

    @Override // com.google.protobuf.t0
    public MessageType parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
        return m35parseFrom(bArr, 0, bArr.length, rVar);
    }

    @Override // com.google.protobuf.t0
    public MessageType parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return parseFrom(bArr, EMPTY_REGISTRY);
    }

    @Override // com.google.protobuf.t0
    public MessageType parseFrom(InputStream inputStream, r rVar) throws InvalidProtocolBufferException {
        return checkMessageInitialized(m42parsePartialFrom(inputStream, rVar));
    }

    @Override // com.google.protobuf.t0
    public MessageType parseFrom(InputStream inputStream) throws InvalidProtocolBufferException {
        return parseFrom(inputStream, EMPTY_REGISTRY);
    }

    /* renamed from: parsePartialFrom */
    public MessageType m41parsePartialFrom(InputStream inputStream) throws InvalidProtocolBufferException {
        return m42parsePartialFrom(inputStream, EMPTY_REGISTRY);
    }
}
