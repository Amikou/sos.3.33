package com.google.protobuf;

import com.google.protobuf.w;
import com.google.protobuf.w.c;
import java.io.IOException;
import java.util.Map;

/* compiled from: ExtensionSchema.java */
/* loaded from: classes2.dex */
public abstract class s<T extends w.c<T>> {
    public abstract int a(Map.Entry<?, ?> entry);

    public abstract Object b(r rVar, m0 m0Var, int i);

    public abstract w<T> c(Object obj);

    public abstract w<T> d(Object obj);

    public abstract boolean e(m0 m0Var);

    public abstract void f(Object obj);

    public abstract <UT, UB> UB g(w0 w0Var, Object obj, r rVar, w<T> wVar, UB ub, f1<UT, UB> f1Var) throws IOException;

    public abstract void h(w0 w0Var, Object obj, r rVar, w<T> wVar) throws IOException;

    public abstract void i(ByteString byteString, Object obj, r rVar, w<T> wVar) throws IOException;

    public abstract void j(Writer writer, Map.Entry<?, ?> entry) throws IOException;
}
