package com.google.protobuf;

import com.github.mikephil.charting.utils.Utils;
import com.google.protobuf.ByteString;
import com.google.protobuf.WireFormat;
import com.google.protobuf.Writer;
import com.google.protobuf.a0;
import com.google.protobuf.e;
import com.google.protobuf.h0;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import sun.misc.Unsafe;

/* compiled from: MessageSchema.java */
/* loaded from: classes2.dex */
public final class p0<T> implements y0<T> {
    public static final int[] r = new int[0];
    public static final Unsafe s = k1.G();
    public final int[] a;
    public final Object[] b;
    public final int c;
    public final int d;
    public final m0 e;
    public final boolean f;
    public final boolean g;
    public final boolean h;
    public final boolean i;
    public final int[] j;
    public final int k;
    public final int l;
    public final if2 m;
    public final e0 n;
    public final f1<?, ?> o;
    public final s<?> p;
    public final i0 q;

    /* compiled from: MessageSchema.java */
    /* loaded from: classes2.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[WireFormat.FieldType.values().length];
            a = iArr;
            try {
                iArr[WireFormat.FieldType.BOOL.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[WireFormat.FieldType.BYTES.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[WireFormat.FieldType.DOUBLE.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                a[WireFormat.FieldType.FIXED32.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                a[WireFormat.FieldType.SFIXED32.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                a[WireFormat.FieldType.FIXED64.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                a[WireFormat.FieldType.SFIXED64.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                a[WireFormat.FieldType.FLOAT.ordinal()] = 8;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                a[WireFormat.FieldType.ENUM.ordinal()] = 9;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                a[WireFormat.FieldType.INT32.ordinal()] = 10;
            } catch (NoSuchFieldError unused10) {
            }
            try {
                a[WireFormat.FieldType.UINT32.ordinal()] = 11;
            } catch (NoSuchFieldError unused11) {
            }
            try {
                a[WireFormat.FieldType.INT64.ordinal()] = 12;
            } catch (NoSuchFieldError unused12) {
            }
            try {
                a[WireFormat.FieldType.UINT64.ordinal()] = 13;
            } catch (NoSuchFieldError unused13) {
            }
            try {
                a[WireFormat.FieldType.MESSAGE.ordinal()] = 14;
            } catch (NoSuchFieldError unused14) {
            }
            try {
                a[WireFormat.FieldType.SINT32.ordinal()] = 15;
            } catch (NoSuchFieldError unused15) {
            }
            try {
                a[WireFormat.FieldType.SINT64.ordinal()] = 16;
            } catch (NoSuchFieldError unused16) {
            }
            try {
                a[WireFormat.FieldType.STRING.ordinal()] = 17;
            } catch (NoSuchFieldError unused17) {
            }
        }
    }

    public p0(int[] iArr, Object[] objArr, int i, int i2, m0 m0Var, boolean z, boolean z2, int[] iArr2, int i3, int i4, if2 if2Var, e0 e0Var, f1<?, ?> f1Var, s<?> sVar, i0 i0Var) {
        this.a = iArr;
        this.b = objArr;
        this.c = i;
        this.d = i2;
        this.g = m0Var instanceof GeneratedMessageLite;
        this.h = z;
        this.f = sVar != null && sVar.e(m0Var);
        this.i = z2;
        this.j = iArr2;
        this.k = i3;
        this.l = i4;
        this.m = if2Var;
        this.n = e0Var;
        this.o = f1Var;
        this.p = sVar;
        this.e = m0Var;
        this.q = i0Var;
    }

    public static <T> int A(T t, long j) {
        return k1.A(t, j);
    }

    public static boolean B(int i) {
        return (i & 536870912) != 0;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static boolean E(Object obj, int i, y0 y0Var) {
        return y0Var.f(k1.E(obj, V(i)));
    }

    public static boolean J(int i) {
        return (i & 268435456) != 0;
    }

    public static List<?> K(Object obj, long j) {
        return (List) k1.E(obj, j);
    }

    public static <T> long L(T t, long j) {
        return k1.C(t, j);
    }

    public static <T> p0<T> R(Class<T> cls, z72 z72Var, if2 if2Var, e0 e0Var, f1<?, ?> f1Var, s<?> sVar, i0 i0Var) {
        if (z72Var instanceof w33) {
            return T((w33) z72Var, if2Var, e0Var, f1Var, sVar, i0Var);
        }
        return S((c1) z72Var, if2Var, e0Var, f1Var, sVar, i0Var);
    }

    public static <T> p0<T> S(c1 c1Var, if2 if2Var, e0 e0Var, f1<?, ?> f1Var, s<?> sVar, i0 i0Var) {
        int u;
        int u2;
        int i;
        boolean z = c1Var.c() == ProtoSyntax.PROTO3;
        v[] e = c1Var.e();
        if (e.length == 0) {
            u = 0;
            u2 = 0;
        } else {
            u = e[0].u();
            u2 = e[e.length - 1].u();
        }
        int length = e.length;
        int[] iArr = new int[length * 3];
        Object[] objArr = new Object[length * 2];
        int i2 = 0;
        int i3 = 0;
        for (v vVar : e) {
            if (vVar.C() == FieldType.MAP) {
                i2++;
            } else if (vVar.C().id() >= 18 && vVar.C().id() <= 49) {
                i3++;
            }
        }
        int[] iArr2 = i2 > 0 ? new int[i2] : null;
        int[] iArr3 = i3 > 0 ? new int[i3] : null;
        int[] d = c1Var.d();
        if (d == null) {
            d = r;
        }
        int i4 = 0;
        int i5 = 0;
        int i6 = 0;
        int i7 = 0;
        int i8 = 0;
        while (i4 < e.length) {
            v vVar2 = e[i4];
            int u3 = vVar2.u();
            r0(vVar2, iArr, i5, objArr);
            if (i6 < d.length && d[i6] == u3) {
                d[i6] = i5;
                i6++;
            }
            if (vVar2.C() == FieldType.MAP) {
                iArr2[i7] = i5;
                i7++;
            } else if (vVar2.C().id() >= 18 && vVar2.C().id() <= 49) {
                i = i5;
                iArr3[i8] = (int) k1.J(vVar2.t());
                i8++;
                i4++;
                i5 = i + 3;
            }
            i = i5;
            i4++;
            i5 = i + 3;
        }
        if (iArr2 == null) {
            iArr2 = r;
        }
        if (iArr3 == null) {
            iArr3 = r;
        }
        int[] iArr4 = new int[d.length + iArr2.length + iArr3.length];
        System.arraycopy(d, 0, iArr4, 0, d.length);
        System.arraycopy(iArr2, 0, iArr4, d.length, iArr2.length);
        System.arraycopy(iArr3, 0, iArr4, d.length + iArr2.length, iArr3.length);
        return new p0<>(iArr, objArr, u, u2, c1Var.b(), z, true, iArr4, d.length, d.length + iArr2.length, if2Var, e0Var, f1Var, sVar, i0Var);
    }

    /* JADX WARN: Removed duplicated region for block: B:122:0x024e  */
    /* JADX WARN: Removed duplicated region for block: B:123:0x0251  */
    /* JADX WARN: Removed duplicated region for block: B:126:0x0269  */
    /* JADX WARN: Removed duplicated region for block: B:127:0x026c  */
    /* JADX WARN: Removed duplicated region for block: B:161:0x031a  */
    /* JADX WARN: Removed duplicated region for block: B:162:0x031d  */
    /* JADX WARN: Removed duplicated region for block: B:164:0x0320  */
    /* JADX WARN: Removed duplicated region for block: B:182:0x037a  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static <T> com.google.protobuf.p0<T> T(defpackage.w33 r34, defpackage.if2 r35, com.google.protobuf.e0 r36, com.google.protobuf.f1<?, ?> r37, com.google.protobuf.s<?> r38, com.google.protobuf.i0 r39) {
        /*
            Method dump skipped, instructions count: 996
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.p0.T(w33, if2, com.google.protobuf.e0, com.google.protobuf.f1, com.google.protobuf.s, com.google.protobuf.i0):com.google.protobuf.p0");
    }

    public static long V(int i) {
        return i & 1048575;
    }

    public static <T> boolean W(T t, long j) {
        return ((Boolean) k1.E(t, j)).booleanValue();
    }

    public static <T> double X(T t, long j) {
        return ((Double) k1.E(t, j)).doubleValue();
    }

    public static <T> float Y(T t, long j) {
        return ((Float) k1.E(t, j)).floatValue();
    }

    public static <T> int Z(T t, long j) {
        return ((Integer) k1.E(t, j)).intValue();
    }

    public static <T> long a0(T t, long j) {
        return ((Long) k1.E(t, j)).longValue();
    }

    public static <T> boolean l(T t, long j) {
        return k1.r(t, j);
    }

    public static Field n0(Class<?> cls, String str) {
        Field[] declaredFields;
        try {
            return cls.getDeclaredField(str);
        } catch (NoSuchFieldException unused) {
            for (Field field : cls.getDeclaredFields()) {
                if (str.equals(field.getName())) {
                    return field;
                }
            }
            throw new RuntimeException("Field " + str + " for " + cls.getName() + " not found. Known fields are " + Arrays.toString(declaredFields));
        }
    }

    public static <T> double o(T t, long j) {
        return k1.y(t, j);
    }

    /* JADX WARN: Removed duplicated region for block: B:21:0x007a  */
    /* JADX WARN: Removed duplicated region for block: B:22:0x007d  */
    /* JADX WARN: Removed duplicated region for block: B:25:0x0084  */
    /* JADX WARN: Removed duplicated region for block: B:28:0x009e  */
    /* JADX WARN: Removed duplicated region for block: B:34:0x00be  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static void r0(com.google.protobuf.v r8, int[] r9, int r10, java.lang.Object[] r11) {
        /*
            en2 r0 = r8.y()
            r1 = 0
            if (r0 == 0) goto L25
            com.google.protobuf.FieldType r2 = r8.C()
            int r2 = r2.id()
            int r2 = r2 + 51
            java.lang.reflect.Field r3 = r0.b()
            long r3 = com.google.protobuf.k1.J(r3)
            int r3 = (int) r3
            java.lang.reflect.Field r0 = r0.a()
            long r4 = com.google.protobuf.k1.J(r0)
        L22:
            int r0 = (int) r4
            r4 = r1
            goto L6c
        L25:
            com.google.protobuf.FieldType r0 = r8.C()
            java.lang.reflect.Field r2 = r8.t()
            long r2 = com.google.protobuf.k1.J(r2)
            int r3 = (int) r2
            int r2 = r0.id()
            boolean r4 = r0.isList()
            if (r4 != 0) goto L5a
            boolean r0 = r0.isMap()
            if (r0 != 0) goto L5a
            java.lang.reflect.Field r0 = r8.A()
            if (r0 != 0) goto L4c
            r0 = 1048575(0xfffff, float:1.469367E-39)
            goto L51
        L4c:
            long r4 = com.google.protobuf.k1.J(r0)
            int r0 = (int) r4
        L51:
            int r4 = r8.B()
            int r4 = java.lang.Integer.numberOfTrailingZeros(r4)
            goto L6c
        L5a:
            java.lang.reflect.Field r0 = r8.r()
            if (r0 != 0) goto L63
            r0 = r1
            r4 = r0
            goto L6c
        L63:
            java.lang.reflect.Field r0 = r8.r()
            long r4 = com.google.protobuf.k1.J(r0)
            goto L22
        L6c:
            int r5 = r8.u()
            r9[r10] = r5
            int r5 = r10 + 1
            boolean r6 = r8.D()
            if (r6 == 0) goto L7d
            r6 = 536870912(0x20000000, float:1.0842022E-19)
            goto L7e
        L7d:
            r6 = r1
        L7e:
            boolean r7 = r8.G()
            if (r7 == 0) goto L86
            r1 = 268435456(0x10000000, float:2.524355E-29)
        L86:
            r1 = r1 | r6
            int r2 = r2 << 20
            r1 = r1 | r2
            r1 = r1 | r3
            r9[r5] = r1
            int r1 = r10 + 2
            int r2 = r4 << 20
            r0 = r0 | r2
            r9[r1] = r0
            java.lang.Class r9 = r8.x()
            java.lang.Object r0 = r8.v()
            if (r0 == 0) goto Lbe
            int r10 = r10 / 3
            int r10 = r10 * 2
            java.lang.Object r0 = r8.v()
            r11[r10] = r0
            if (r9 == 0) goto Laf
            int r10 = r10 + 1
            r11[r10] = r9
            goto Ldb
        Laf:
            com.google.protobuf.a0$e r9 = r8.s()
            if (r9 == 0) goto Ldb
            int r10 = r10 + 1
            com.google.protobuf.a0$e r8 = r8.s()
            r11[r10] = r8
            goto Ldb
        Lbe:
            if (r9 == 0) goto Lc9
            int r10 = r10 / 3
            int r10 = r10 * 2
            int r10 = r10 + 1
            r11[r10] = r9
            goto Ldb
        Lc9:
            com.google.protobuf.a0$e r9 = r8.s()
            if (r9 == 0) goto Ldb
            int r10 = r10 / 3
            int r10 = r10 * 2
            int r10 = r10 + 1
            com.google.protobuf.a0$e r8 = r8.s()
            r11[r10] = r8
        Ldb:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.p0.r0(com.google.protobuf.v, int[], int, java.lang.Object[]):void");
    }

    public static <T> float s(T t, long j) {
        return k1.z(t, j);
    }

    public static int s0(int i) {
        return (i & 267386880) >>> 20;
    }

    public static h1 w(Object obj) {
        GeneratedMessageLite generatedMessageLite = (GeneratedMessageLite) obj;
        h1 h1Var = generatedMessageLite.a;
        if (h1Var == h1.c()) {
            h1 j = h1.j();
            generatedMessageLite.a = j;
            return j;
        }
        return h1Var;
    }

    public final boolean C(T t, int i) {
        int i0 = i0(i);
        long j = 1048575 & i0;
        if (j != 1048575) {
            return (k1.A(t, j) & (1 << (i0 >>> 20))) != 0;
        }
        int t0 = t0(i);
        long V = V(t0);
        switch (s0(t0)) {
            case 0:
                return k1.y(t, V) != Utils.DOUBLE_EPSILON;
            case 1:
                return k1.z(t, V) != Utils.FLOAT_EPSILON;
            case 2:
                return k1.C(t, V) != 0;
            case 3:
                return k1.C(t, V) != 0;
            case 4:
                return k1.A(t, V) != 0;
            case 5:
                return k1.C(t, V) != 0;
            case 6:
                return k1.A(t, V) != 0;
            case 7:
                return k1.r(t, V);
            case 8:
                Object E = k1.E(t, V);
                if (E instanceof String) {
                    return !((String) E).isEmpty();
                }
                if (E instanceof ByteString) {
                    return !ByteString.EMPTY.equals(E);
                }
                throw new IllegalArgumentException();
            case 9:
                return k1.E(t, V) != null;
            case 10:
                return !ByteString.EMPTY.equals(k1.E(t, V));
            case 11:
                return k1.A(t, V) != 0;
            case 12:
                return k1.A(t, V) != 0;
            case 13:
                return k1.A(t, V) != 0;
            case 14:
                return k1.C(t, V) != 0;
            case 15:
                return k1.A(t, V) != 0;
            case 16:
                return k1.C(t, V) != 0;
            case 17:
                return k1.E(t, V) != null;
            default:
                throw new IllegalArgumentException();
        }
    }

    public final boolean D(T t, int i, int i2, int i3, int i4) {
        if (i2 == 1048575) {
            return C(t, i);
        }
        return (i3 & i4) != 0;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public final <N> boolean F(Object obj, int i, int i2) {
        List list = (List) k1.E(obj, V(i));
        if (list.isEmpty()) {
            return true;
        }
        y0 v = v(i2);
        for (int i3 = 0; i3 < list.size(); i3++) {
            if (!v.f(list.get(i3))) {
                return false;
            }
        }
        return true;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r5v11 */
    /* JADX WARN: Type inference failed for: r5v6 */
    /* JADX WARN: Type inference failed for: r5v8, types: [com.google.protobuf.y0] */
    public final boolean G(T t, int i, int i2) {
        Map<?, ?> h = this.q.h(k1.E(t, V(i)));
        if (h.isEmpty()) {
            return true;
        }
        if (this.q.c(u(i2)).c.getJavaType() != WireFormat.JavaType.MESSAGE) {
            return true;
        }
        y0<T> y0Var = 0;
        for (Object obj : h.values()) {
            if (y0Var == null) {
                y0Var = u0.a().d(obj.getClass());
            }
            boolean f = y0Var.f(obj);
            y0Var = y0Var;
            if (!f) {
                return false;
            }
        }
        return true;
    }

    public final boolean H(T t, T t2, int i) {
        long i0 = i0(i) & 1048575;
        return k1.A(t, i0) == k1.A(t2, i0);
    }

    public final boolean I(T t, int i, int i2) {
        return k1.A(t, (long) (i0(i2) & 1048575)) == i;
    }

    /* JADX WARN: Code restructure failed: missing block: B:32:0x0077, code lost:
        r0 = r16.k;
     */
    /* JADX WARN: Code restructure failed: missing block: B:34:0x007b, code lost:
        if (r0 >= r16.l) goto L328;
     */
    /* JADX WARN: Code restructure failed: missing block: B:35:0x007d, code lost:
        r13 = q(r19, r16.j[r0], r13, r17);
        r0 = r0 + 1;
     */
    /* JADX WARN: Code restructure failed: missing block: B:363:?, code lost:
        return;
     */
    /* JADX WARN: Code restructure failed: missing block: B:36:0x0088, code lost:
        if (r13 == null) goto L332;
     */
    /* JADX WARN: Code restructure failed: missing block: B:37:0x008a, code lost:
        r17.o(r19, r13);
     */
    /* JADX WARN: Code restructure failed: missing block: B:38:0x008d, code lost:
        return;
     */
    /* JADX WARN: Multi-variable type inference failed */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final <UT, UB, ET extends com.google.protobuf.w.c<ET>> void M(com.google.protobuf.f1<UT, UB> r17, com.google.protobuf.s<ET> r18, T r19, com.google.protobuf.w0 r20, com.google.protobuf.r r21) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 1720
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.p0.M(com.google.protobuf.f1, com.google.protobuf.s, java.lang.Object, com.google.protobuf.w0, com.google.protobuf.r):void");
    }

    public final <K, V> void N(Object obj, int i, Object obj2, r rVar, w0 w0Var) throws IOException {
        long V = V(t0(i));
        Object E = k1.E(obj, V);
        if (E == null) {
            E = this.q.e(obj2);
            k1.U(obj, V, E);
        } else if (this.q.g(E)) {
            Object e = this.q.e(obj2);
            this.q.a(e, E);
            k1.U(obj, V, e);
            E = e;
        }
        w0Var.P(this.q.d(E), this.q.c(obj2), rVar);
    }

    public final void O(T t, T t2, int i) {
        long V = V(t0(i));
        if (C(t2, i)) {
            Object E = k1.E(t, V);
            Object E2 = k1.E(t2, V);
            if (E != null && E2 != null) {
                k1.U(t, V, a0.j(E, E2));
                o0(t, i);
            } else if (E2 != null) {
                k1.U(t, V, E2);
                o0(t, i);
            }
        }
    }

    public final void P(T t, T t2, int i) {
        int t0 = t0(i);
        int U = U(i);
        long V = V(t0);
        if (I(t2, U, i)) {
            Object E = k1.E(t, V);
            Object E2 = k1.E(t2, V);
            if (E != null && E2 != null) {
                k1.U(t, V, a0.j(E, E2));
                p0(t, U, i);
            } else if (E2 != null) {
                k1.U(t, V, E2);
                p0(t, U, i);
            }
        }
    }

    public final void Q(T t, T t2, int i) {
        int t0 = t0(i);
        long V = V(t0);
        int U = U(i);
        switch (s0(t0)) {
            case 0:
                if (C(t2, i)) {
                    k1.Q(t, V, k1.y(t2, V));
                    o0(t, i);
                    return;
                }
                return;
            case 1:
                if (C(t2, i)) {
                    k1.R(t, V, k1.z(t2, V));
                    o0(t, i);
                    return;
                }
                return;
            case 2:
                if (C(t2, i)) {
                    k1.T(t, V, k1.C(t2, V));
                    o0(t, i);
                    return;
                }
                return;
            case 3:
                if (C(t2, i)) {
                    k1.T(t, V, k1.C(t2, V));
                    o0(t, i);
                    return;
                }
                return;
            case 4:
                if (C(t2, i)) {
                    k1.S(t, V, k1.A(t2, V));
                    o0(t, i);
                    return;
                }
                return;
            case 5:
                if (C(t2, i)) {
                    k1.T(t, V, k1.C(t2, V));
                    o0(t, i);
                    return;
                }
                return;
            case 6:
                if (C(t2, i)) {
                    k1.S(t, V, k1.A(t2, V));
                    o0(t, i);
                    return;
                }
                return;
            case 7:
                if (C(t2, i)) {
                    k1.K(t, V, k1.r(t2, V));
                    o0(t, i);
                    return;
                }
                return;
            case 8:
                if (C(t2, i)) {
                    k1.U(t, V, k1.E(t2, V));
                    o0(t, i);
                    return;
                }
                return;
            case 9:
                O(t, t2, i);
                return;
            case 10:
                if (C(t2, i)) {
                    k1.U(t, V, k1.E(t2, V));
                    o0(t, i);
                    return;
                }
                return;
            case 11:
                if (C(t2, i)) {
                    k1.S(t, V, k1.A(t2, V));
                    o0(t, i);
                    return;
                }
                return;
            case 12:
                if (C(t2, i)) {
                    k1.S(t, V, k1.A(t2, V));
                    o0(t, i);
                    return;
                }
                return;
            case 13:
                if (C(t2, i)) {
                    k1.S(t, V, k1.A(t2, V));
                    o0(t, i);
                    return;
                }
                return;
            case 14:
                if (C(t2, i)) {
                    k1.T(t, V, k1.C(t2, V));
                    o0(t, i);
                    return;
                }
                return;
            case 15:
                if (C(t2, i)) {
                    k1.S(t, V, k1.A(t2, V));
                    o0(t, i);
                    return;
                }
                return;
            case 16:
                if (C(t2, i)) {
                    k1.T(t, V, k1.C(t2, V));
                    o0(t, i);
                    return;
                }
                return;
            case 17:
                O(t, t2, i);
                return;
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
            case 35:
            case 36:
            case 37:
            case 38:
            case 39:
            case 40:
            case 41:
            case 42:
            case 43:
            case 44:
            case 45:
            case 46:
            case 47:
            case 48:
            case 49:
                this.n.d(t, t2, V);
                return;
            case 50:
                z0.G(this.q, t, t2, V);
                return;
            case 51:
            case 52:
            case 53:
            case 54:
            case 55:
            case 56:
            case 57:
            case 58:
            case 59:
                if (I(t2, U, i)) {
                    k1.U(t, V, k1.E(t2, V));
                    p0(t, U, i);
                    return;
                }
                return;
            case 60:
                P(t, t2, i);
                return;
            case 61:
            case 62:
            case 63:
            case 64:
            case 65:
            case 66:
            case 67:
                if (I(t2, U, i)) {
                    k1.U(t, V, k1.E(t2, V));
                    p0(t, U, i);
                    return;
                }
                return;
            case 68:
                P(t, t2, i);
                return;
            default:
                return;
        }
    }

    public final int U(int i) {
        return this.a[i];
    }

    @Override // com.google.protobuf.y0
    public void a(T t, T t2) {
        Objects.requireNonNull(t2);
        for (int i = 0; i < this.a.length; i += 3) {
            Q(t, t2, i);
        }
        z0.H(this.o, t, t2);
        if (this.f) {
            z0.F(this.p, t, t2);
        }
    }

    @Override // com.google.protobuf.y0
    public boolean b(T t, T t2) {
        int length = this.a.length;
        for (int i = 0; i < length; i += 3) {
            if (!p(t, t2, i)) {
                return false;
            }
        }
        if (this.o.g(t).equals(this.o.g(t2))) {
            if (this.f) {
                return this.p.c(t).equals(this.p.c(t2));
            }
            return true;
        }
        return false;
    }

    public final <K, V> int b0(T t, byte[] bArr, int i, int i2, int i3, long j, e.b bVar) throws IOException {
        Unsafe unsafe = s;
        Object u = u(i3);
        Object object = unsafe.getObject(t, j);
        if (this.q.g(object)) {
            Object e = this.q.e(u);
            this.q.a(e, object);
            unsafe.putObject(t, j, e);
            object = e;
        }
        return m(bArr, i, i2, this.q.c(u), this.q.d(object), bVar);
    }

    @Override // com.google.protobuf.y0
    public T c() {
        return (T) this.m.a(this.e);
    }

    public final int c0(T t, byte[] bArr, int i, int i2, int i3, int i4, int i5, int i6, int i7, long j, int i8, e.b bVar) throws IOException {
        Unsafe unsafe = s;
        long j2 = this.a[i8 + 2] & 1048575;
        switch (i7) {
            case 51:
                if (i5 == 1) {
                    unsafe.putObject(t, j, Double.valueOf(e.d(bArr, i)));
                    int i9 = i + 8;
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                break;
            case 52:
                if (i5 == 5) {
                    unsafe.putObject(t, j, Float.valueOf(e.l(bArr, i)));
                    int i10 = i + 4;
                    unsafe.putInt(t, j2, i4);
                    return i10;
                }
                break;
            case 53:
            case 54:
                if (i5 == 0) {
                    int L = e.L(bArr, i, bVar);
                    unsafe.putObject(t, j, Long.valueOf(bVar.b));
                    unsafe.putInt(t, j2, i4);
                    return L;
                }
                break;
            case 55:
            case 62:
                if (i5 == 0) {
                    int I = e.I(bArr, i, bVar);
                    unsafe.putObject(t, j, Integer.valueOf(bVar.a));
                    unsafe.putInt(t, j2, i4);
                    return I;
                }
                break;
            case 56:
            case 65:
                if (i5 == 1) {
                    unsafe.putObject(t, j, Long.valueOf(e.j(bArr, i)));
                    int i11 = i + 8;
                    unsafe.putInt(t, j2, i4);
                    return i11;
                }
                break;
            case 57:
            case 64:
                if (i5 == 5) {
                    unsafe.putObject(t, j, Integer.valueOf(e.h(bArr, i)));
                    int i12 = i + 4;
                    unsafe.putInt(t, j2, i4);
                    return i12;
                }
                break;
            case 58:
                if (i5 == 0) {
                    int L2 = e.L(bArr, i, bVar);
                    unsafe.putObject(t, j, Boolean.valueOf(bVar.b != 0));
                    unsafe.putInt(t, j2, i4);
                    return L2;
                }
                break;
            case 59:
                if (i5 == 2) {
                    int I2 = e.I(bArr, i, bVar);
                    int i13 = bVar.a;
                    if (i13 == 0) {
                        unsafe.putObject(t, j, "");
                    } else if ((i6 & 536870912) != 0 && !Utf8.t(bArr, I2, I2 + i13)) {
                        throw InvalidProtocolBufferException.invalidUtf8();
                    } else {
                        unsafe.putObject(t, j, new String(bArr, I2, i13, a0.a));
                        I2 += i13;
                    }
                    unsafe.putInt(t, j2, i4);
                    return I2;
                }
                break;
            case 60:
                if (i5 == 2) {
                    int p = e.p(v(i8), bArr, i, i2, bVar);
                    Object object = unsafe.getInt(t, j2) == i4 ? unsafe.getObject(t, j) : null;
                    if (object == null) {
                        unsafe.putObject(t, j, bVar.c);
                    } else {
                        unsafe.putObject(t, j, a0.j(object, bVar.c));
                    }
                    unsafe.putInt(t, j2, i4);
                    return p;
                }
                break;
            case 61:
                if (i5 == 2) {
                    int b = e.b(bArr, i, bVar);
                    unsafe.putObject(t, j, bVar.c);
                    unsafe.putInt(t, j2, i4);
                    return b;
                }
                break;
            case 63:
                if (i5 == 0) {
                    int I3 = e.I(bArr, i, bVar);
                    int i14 = bVar.a;
                    a0.e t2 = t(i8);
                    if (t2 != null && !t2.a(i14)) {
                        w(t).m(i3, Long.valueOf(i14));
                    } else {
                        unsafe.putObject(t, j, Integer.valueOf(i14));
                        unsafe.putInt(t, j2, i4);
                    }
                    return I3;
                }
                break;
            case 66:
                if (i5 == 0) {
                    int I4 = e.I(bArr, i, bVar);
                    unsafe.putObject(t, j, Integer.valueOf(j.b(bVar.a)));
                    unsafe.putInt(t, j2, i4);
                    return I4;
                }
                break;
            case 67:
                if (i5 == 0) {
                    int L3 = e.L(bArr, i, bVar);
                    unsafe.putObject(t, j, Long.valueOf(j.c(bVar.b)));
                    unsafe.putInt(t, j2, i4);
                    return L3;
                }
                break;
            case 68:
                if (i5 == 3) {
                    int n = e.n(v(i8), bArr, i, i2, (i3 & (-8)) | 4, bVar);
                    Object object2 = unsafe.getInt(t, j2) == i4 ? unsafe.getObject(t, j) : null;
                    if (object2 == null) {
                        unsafe.putObject(t, j, bVar.c);
                    } else {
                        unsafe.putObject(t, j, a0.j(object2, bVar.c));
                    }
                    unsafe.putInt(t, j2, i4);
                    return n;
                }
                break;
        }
        return i;
    }

    @Override // com.google.protobuf.y0
    public int d(T t) {
        int i;
        int h;
        int length = this.a.length;
        int i2 = 0;
        for (int i3 = 0; i3 < length; i3 += 3) {
            int t0 = t0(i3);
            int U = U(i3);
            long V = V(t0);
            int i4 = 37;
            switch (s0(t0)) {
                case 0:
                    i = i2 * 53;
                    h = a0.h(Double.doubleToLongBits(k1.y(t, V)));
                    i2 = i + h;
                    break;
                case 1:
                    i = i2 * 53;
                    h = Float.floatToIntBits(k1.z(t, V));
                    i2 = i + h;
                    break;
                case 2:
                    i = i2 * 53;
                    h = a0.h(k1.C(t, V));
                    i2 = i + h;
                    break;
                case 3:
                    i = i2 * 53;
                    h = a0.h(k1.C(t, V));
                    i2 = i + h;
                    break;
                case 4:
                    i = i2 * 53;
                    h = k1.A(t, V);
                    i2 = i + h;
                    break;
                case 5:
                    i = i2 * 53;
                    h = a0.h(k1.C(t, V));
                    i2 = i + h;
                    break;
                case 6:
                    i = i2 * 53;
                    h = k1.A(t, V);
                    i2 = i + h;
                    break;
                case 7:
                    i = i2 * 53;
                    h = a0.c(k1.r(t, V));
                    i2 = i + h;
                    break;
                case 8:
                    i = i2 * 53;
                    h = ((String) k1.E(t, V)).hashCode();
                    i2 = i + h;
                    break;
                case 9:
                    Object E = k1.E(t, V);
                    if (E != null) {
                        i4 = E.hashCode();
                    }
                    i2 = (i2 * 53) + i4;
                    break;
                case 10:
                    i = i2 * 53;
                    h = k1.E(t, V).hashCode();
                    i2 = i + h;
                    break;
                case 11:
                    i = i2 * 53;
                    h = k1.A(t, V);
                    i2 = i + h;
                    break;
                case 12:
                    i = i2 * 53;
                    h = k1.A(t, V);
                    i2 = i + h;
                    break;
                case 13:
                    i = i2 * 53;
                    h = k1.A(t, V);
                    i2 = i + h;
                    break;
                case 14:
                    i = i2 * 53;
                    h = a0.h(k1.C(t, V));
                    i2 = i + h;
                    break;
                case 15:
                    i = i2 * 53;
                    h = k1.A(t, V);
                    i2 = i + h;
                    break;
                case 16:
                    i = i2 * 53;
                    h = a0.h(k1.C(t, V));
                    i2 = i + h;
                    break;
                case 17:
                    Object E2 = k1.E(t, V);
                    if (E2 != null) {
                        i4 = E2.hashCode();
                    }
                    i2 = (i2 * 53) + i4;
                    break;
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                case 48:
                case 49:
                    i = i2 * 53;
                    h = k1.E(t, V).hashCode();
                    i2 = i + h;
                    break;
                case 50:
                    i = i2 * 53;
                    h = k1.E(t, V).hashCode();
                    i2 = i + h;
                    break;
                case 51:
                    if (I(t, U, i3)) {
                        i = i2 * 53;
                        h = a0.h(Double.doubleToLongBits(X(t, V)));
                        i2 = i + h;
                        break;
                    } else {
                        break;
                    }
                case 52:
                    if (I(t, U, i3)) {
                        i = i2 * 53;
                        h = Float.floatToIntBits(Y(t, V));
                        i2 = i + h;
                        break;
                    } else {
                        break;
                    }
                case 53:
                    if (I(t, U, i3)) {
                        i = i2 * 53;
                        h = a0.h(a0(t, V));
                        i2 = i + h;
                        break;
                    } else {
                        break;
                    }
                case 54:
                    if (I(t, U, i3)) {
                        i = i2 * 53;
                        h = a0.h(a0(t, V));
                        i2 = i + h;
                        break;
                    } else {
                        break;
                    }
                case 55:
                    if (I(t, U, i3)) {
                        i = i2 * 53;
                        h = Z(t, V);
                        i2 = i + h;
                        break;
                    } else {
                        break;
                    }
                case 56:
                    if (I(t, U, i3)) {
                        i = i2 * 53;
                        h = a0.h(a0(t, V));
                        i2 = i + h;
                        break;
                    } else {
                        break;
                    }
                case 57:
                    if (I(t, U, i3)) {
                        i = i2 * 53;
                        h = Z(t, V);
                        i2 = i + h;
                        break;
                    } else {
                        break;
                    }
                case 58:
                    if (I(t, U, i3)) {
                        i = i2 * 53;
                        h = a0.c(W(t, V));
                        i2 = i + h;
                        break;
                    } else {
                        break;
                    }
                case 59:
                    if (I(t, U, i3)) {
                        i = i2 * 53;
                        h = ((String) k1.E(t, V)).hashCode();
                        i2 = i + h;
                        break;
                    } else {
                        break;
                    }
                case 60:
                    if (I(t, U, i3)) {
                        i = i2 * 53;
                        h = k1.E(t, V).hashCode();
                        i2 = i + h;
                        break;
                    } else {
                        break;
                    }
                case 61:
                    if (I(t, U, i3)) {
                        i = i2 * 53;
                        h = k1.E(t, V).hashCode();
                        i2 = i + h;
                        break;
                    } else {
                        break;
                    }
                case 62:
                    if (I(t, U, i3)) {
                        i = i2 * 53;
                        h = Z(t, V);
                        i2 = i + h;
                        break;
                    } else {
                        break;
                    }
                case 63:
                    if (I(t, U, i3)) {
                        i = i2 * 53;
                        h = Z(t, V);
                        i2 = i + h;
                        break;
                    } else {
                        break;
                    }
                case 64:
                    if (I(t, U, i3)) {
                        i = i2 * 53;
                        h = Z(t, V);
                        i2 = i + h;
                        break;
                    } else {
                        break;
                    }
                case 65:
                    if (I(t, U, i3)) {
                        i = i2 * 53;
                        h = a0.h(a0(t, V));
                        i2 = i + h;
                        break;
                    } else {
                        break;
                    }
                case 66:
                    if (I(t, U, i3)) {
                        i = i2 * 53;
                        h = Z(t, V);
                        i2 = i + h;
                        break;
                    } else {
                        break;
                    }
                case 67:
                    if (I(t, U, i3)) {
                        i = i2 * 53;
                        h = a0.h(a0(t, V));
                        i2 = i + h;
                        break;
                    } else {
                        break;
                    }
                case 68:
                    if (I(t, U, i3)) {
                        i = i2 * 53;
                        h = k1.E(t, V).hashCode();
                        i2 = i + h;
                        break;
                    } else {
                        break;
                    }
            }
        }
        int hashCode = (i2 * 53) + this.o.g(t).hashCode();
        return this.f ? (hashCode * 53) + this.p.c(t).hashCode() : hashCode;
    }

    public int d0(T t, byte[] bArr, int i, int i2, int i3, e.b bVar) throws IOException {
        Unsafe unsafe;
        int i4;
        p0<T> p0Var;
        int i5;
        T t2;
        byte b;
        int g0;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        int i12;
        int i13;
        T t3;
        T t4;
        int i14;
        T t5;
        int i15;
        int i16;
        p0<T> p0Var2 = this;
        T t6 = t;
        byte[] bArr2 = bArr;
        int i17 = i2;
        int i18 = i3;
        e.b bVar2 = bVar;
        Unsafe unsafe2 = s;
        int i19 = i;
        int i20 = 0;
        int i21 = 0;
        int i22 = 0;
        int i23 = -1;
        int i24 = 1048575;
        while (true) {
            if (i19 < i17) {
                int i25 = i19 + 1;
                byte b2 = bArr2[i19];
                if (b2 < 0) {
                    int H = e.H(b2, bArr2, i25, bVar2);
                    b = bVar2.a;
                    i25 = H;
                } else {
                    b = b2;
                }
                int i26 = b >>> 3;
                int i27 = b & 7;
                if (i26 > i23) {
                    g0 = p0Var2.h0(i26, i20 / 3);
                } else {
                    g0 = p0Var2.g0(i26);
                }
                int i28 = g0;
                if (i28 == -1) {
                    i6 = i26;
                    i7 = i25;
                    i8 = b;
                    i9 = i22;
                    i10 = i24;
                    unsafe = unsafe2;
                    i4 = i18;
                    i11 = 0;
                } else {
                    int i29 = p0Var2.a[i28 + 1];
                    int s0 = s0(i29);
                    long V = V(i29);
                    int i30 = b;
                    if (s0 <= 17) {
                        int i31 = p0Var2.a[i28 + 2];
                        int i32 = 1 << (i31 >>> 20);
                        int i33 = i31 & 1048575;
                        if (i33 != i24) {
                            if (i24 != 1048575) {
                                unsafe2.putInt(t6, i24, i22);
                            }
                            i22 = unsafe2.getInt(t6, i33);
                            i10 = i33;
                        } else {
                            i10 = i24;
                        }
                        int i34 = i22;
                        switch (s0) {
                            case 0:
                                t3 = t6;
                                i12 = i28;
                                i6 = i26;
                                bArr2 = bArr;
                                i13 = i30;
                                if (i27 == 1) {
                                    k1.Q(t3, V, e.d(bArr2, i25));
                                    i19 = i25 + 8;
                                    i22 = i34 | i32;
                                    i17 = i2;
                                    t6 = t3;
                                    i20 = i12;
                                    i21 = i13;
                                    i23 = i6;
                                    i24 = i10;
                                    i18 = i3;
                                    break;
                                } else {
                                    i7 = i25;
                                    i9 = i34;
                                    i11 = i12;
                                    unsafe = unsafe2;
                                    i8 = i13;
                                    i4 = i3;
                                    break;
                                }
                            case 1:
                                t3 = t6;
                                i12 = i28;
                                i6 = i26;
                                bArr2 = bArr;
                                i13 = i30;
                                if (i27 == 5) {
                                    k1.R(t3, V, e.l(bArr2, i25));
                                    i19 = i25 + 4;
                                    i22 = i34 | i32;
                                    i17 = i2;
                                    t6 = t3;
                                    i20 = i12;
                                    i21 = i13;
                                    i23 = i6;
                                    i24 = i10;
                                    i18 = i3;
                                    break;
                                } else {
                                    i7 = i25;
                                    i9 = i34;
                                    i11 = i12;
                                    unsafe = unsafe2;
                                    i8 = i13;
                                    i4 = i3;
                                    break;
                                }
                            case 2:
                            case 3:
                                T t7 = t6;
                                i12 = i28;
                                i6 = i26;
                                bArr2 = bArr;
                                i13 = i30;
                                if (i27 == 0) {
                                    int L = e.L(bArr2, i25, bVar2);
                                    t4 = t7;
                                    unsafe2.putLong(t, V, bVar2.b);
                                    i22 = i34 | i32;
                                    i19 = L;
                                    i20 = i12;
                                    i21 = i13;
                                    t6 = t4;
                                    i23 = i6;
                                    i24 = i10;
                                    i17 = i2;
                                    i18 = i3;
                                    break;
                                } else {
                                    i7 = i25;
                                    i9 = i34;
                                    i11 = i12;
                                    unsafe = unsafe2;
                                    i8 = i13;
                                    i4 = i3;
                                    break;
                                }
                            case 4:
                            case 11:
                                t3 = t6;
                                i12 = i28;
                                i6 = i26;
                                bArr2 = bArr;
                                i13 = i30;
                                if (i27 == 0) {
                                    i19 = e.I(bArr2, i25, bVar2);
                                    unsafe2.putInt(t3, V, bVar2.a);
                                    i22 = i34 | i32;
                                    i17 = i2;
                                    t6 = t3;
                                    i20 = i12;
                                    i21 = i13;
                                    i23 = i6;
                                    i24 = i10;
                                    i18 = i3;
                                    break;
                                } else {
                                    i7 = i25;
                                    i9 = i34;
                                    i11 = i12;
                                    unsafe = unsafe2;
                                    i8 = i13;
                                    i4 = i3;
                                    break;
                                }
                            case 5:
                            case 14:
                                T t8 = t6;
                                i12 = i28;
                                i6 = i26;
                                bArr2 = bArr;
                                i13 = i30;
                                if (i27 == 1) {
                                    t4 = t8;
                                    unsafe2.putLong(t, V, e.j(bArr2, i25));
                                    i19 = i25 + 8;
                                    i22 = i34 | i32;
                                    i20 = i12;
                                    i21 = i13;
                                    t6 = t4;
                                    i23 = i6;
                                    i24 = i10;
                                    i17 = i2;
                                    i18 = i3;
                                    break;
                                } else {
                                    i7 = i25;
                                    i9 = i34;
                                    i11 = i12;
                                    unsafe = unsafe2;
                                    i8 = i13;
                                    i4 = i3;
                                    break;
                                }
                            case 6:
                            case 13:
                                i14 = i2;
                                t5 = t6;
                                i12 = i28;
                                i6 = i26;
                                bArr2 = bArr;
                                i13 = i30;
                                if (i27 == 5) {
                                    unsafe2.putInt(t5, V, e.h(bArr2, i25));
                                    i19 = i25 + 4;
                                    int i35 = i34 | i32;
                                    t6 = t5;
                                    i17 = i14;
                                    i20 = i12;
                                    i21 = i13;
                                    i24 = i10;
                                    i18 = i3;
                                    i22 = i35;
                                    i23 = i6;
                                    break;
                                } else {
                                    i7 = i25;
                                    i9 = i34;
                                    i11 = i12;
                                    unsafe = unsafe2;
                                    i8 = i13;
                                    i4 = i3;
                                    break;
                                }
                            case 7:
                                i14 = i2;
                                t5 = t6;
                                i12 = i28;
                                i6 = i26;
                                bArr2 = bArr;
                                i13 = i30;
                                if (i27 == 0) {
                                    i19 = e.L(bArr2, i25, bVar2);
                                    k1.K(t5, V, bVar2.b != 0);
                                    int i352 = i34 | i32;
                                    t6 = t5;
                                    i17 = i14;
                                    i20 = i12;
                                    i21 = i13;
                                    i24 = i10;
                                    i18 = i3;
                                    i22 = i352;
                                    i23 = i6;
                                    break;
                                } else {
                                    i7 = i25;
                                    i9 = i34;
                                    i11 = i12;
                                    unsafe = unsafe2;
                                    i8 = i13;
                                    i4 = i3;
                                    break;
                                }
                            case 8:
                                i14 = i2;
                                t5 = t6;
                                i12 = i28;
                                i6 = i26;
                                bArr2 = bArr;
                                i13 = i30;
                                if (i27 == 2) {
                                    if ((i29 & 536870912) == 0) {
                                        i19 = e.C(bArr2, i25, bVar2);
                                    } else {
                                        i19 = e.F(bArr2, i25, bVar2);
                                    }
                                    unsafe2.putObject(t5, V, bVar2.c);
                                    int i3522 = i34 | i32;
                                    t6 = t5;
                                    i17 = i14;
                                    i20 = i12;
                                    i21 = i13;
                                    i24 = i10;
                                    i18 = i3;
                                    i22 = i3522;
                                    i23 = i6;
                                    break;
                                } else {
                                    i7 = i25;
                                    i9 = i34;
                                    i11 = i12;
                                    unsafe = unsafe2;
                                    i8 = i13;
                                    i4 = i3;
                                    break;
                                }
                            case 9:
                                t5 = t6;
                                i12 = i28;
                                i6 = i26;
                                i13 = i30;
                                bArr2 = bArr;
                                if (i27 == 2) {
                                    i14 = i2;
                                    i19 = e.p(p0Var2.v(i12), bArr2, i25, i14, bVar2);
                                    if ((i34 & i32) == 0) {
                                        unsafe2.putObject(t5, V, bVar2.c);
                                    } else {
                                        unsafe2.putObject(t5, V, a0.j(unsafe2.getObject(t5, V), bVar2.c));
                                    }
                                    int i35222 = i34 | i32;
                                    t6 = t5;
                                    i17 = i14;
                                    i20 = i12;
                                    i21 = i13;
                                    i24 = i10;
                                    i18 = i3;
                                    i22 = i35222;
                                    i23 = i6;
                                    break;
                                } else {
                                    i7 = i25;
                                    i9 = i34;
                                    i11 = i12;
                                    unsafe = unsafe2;
                                    i8 = i13;
                                    i4 = i3;
                                    break;
                                }
                            case 10:
                                t3 = t6;
                                i12 = i28;
                                i6 = i26;
                                i13 = i30;
                                bArr2 = bArr;
                                if (i27 == 2) {
                                    i19 = e.b(bArr2, i25, bVar2);
                                    unsafe2.putObject(t3, V, bVar2.c);
                                    i22 = i34 | i32;
                                    i17 = i2;
                                    t6 = t3;
                                    i20 = i12;
                                    i21 = i13;
                                    i23 = i6;
                                    i24 = i10;
                                    i18 = i3;
                                    break;
                                } else {
                                    i7 = i25;
                                    i9 = i34;
                                    i11 = i12;
                                    unsafe = unsafe2;
                                    i8 = i13;
                                    i4 = i3;
                                    break;
                                }
                            case 12:
                                t3 = t6;
                                i12 = i28;
                                i6 = i26;
                                i13 = i30;
                                bArr2 = bArr;
                                if (i27 == 0) {
                                    i19 = e.I(bArr2, i25, bVar2);
                                    int i36 = bVar2.a;
                                    a0.e t9 = p0Var2.t(i12);
                                    if (t9 != null && !t9.a(i36)) {
                                        w(t).m(i13, Long.valueOf(i36));
                                        i17 = i2;
                                        t6 = t3;
                                        i22 = i34;
                                        i20 = i12;
                                        i21 = i13;
                                        i23 = i6;
                                        i24 = i10;
                                        i18 = i3;
                                    } else {
                                        unsafe2.putInt(t3, V, i36);
                                        i22 = i34 | i32;
                                        i17 = i2;
                                        t6 = t3;
                                        i20 = i12;
                                        i21 = i13;
                                        i23 = i6;
                                        i24 = i10;
                                        i18 = i3;
                                        break;
                                    }
                                } else {
                                    i7 = i25;
                                    i9 = i34;
                                    i11 = i12;
                                    unsafe = unsafe2;
                                    i8 = i13;
                                    i4 = i3;
                                    break;
                                }
                                break;
                            case 15:
                                i12 = i28;
                                i13 = i30;
                                bArr2 = bArr;
                                i6 = i26;
                                if (i27 == 0) {
                                    i19 = e.I(bArr2, i25, bVar2);
                                    t3 = t;
                                    unsafe2.putInt(t3, V, j.b(bVar2.a));
                                    i22 = i34 | i32;
                                    i17 = i2;
                                    t6 = t3;
                                    i20 = i12;
                                    i21 = i13;
                                    i23 = i6;
                                    i24 = i10;
                                    i18 = i3;
                                    break;
                                } else {
                                    i7 = i25;
                                    i9 = i34;
                                    i11 = i12;
                                    unsafe = unsafe2;
                                    i8 = i13;
                                    i4 = i3;
                                    break;
                                }
                            case 16:
                                i12 = i28;
                                i6 = i26;
                                if (i27 == 0) {
                                    bArr2 = bArr;
                                    int L2 = e.L(bArr2, i25, bVar2);
                                    i13 = i30;
                                    unsafe2.putLong(t, V, j.c(bVar2.b));
                                    i22 = i34 | i32;
                                    t6 = t;
                                    i17 = i2;
                                    i19 = L2;
                                    i20 = i12;
                                    i21 = i13;
                                    i23 = i6;
                                    i24 = i10;
                                    i18 = i3;
                                    break;
                                } else {
                                    i13 = i30;
                                    i7 = i25;
                                    i9 = i34;
                                    i11 = i12;
                                    unsafe = unsafe2;
                                    i8 = i13;
                                    i4 = i3;
                                    break;
                                }
                            case 17:
                                if (i27 == 3) {
                                    i19 = e.n(p0Var2.v(i28), bArr, i25, i2, (i26 << 3) | 4, bVar);
                                    if ((i34 & i32) == 0) {
                                        unsafe2.putObject(t6, V, bVar2.c);
                                    } else {
                                        unsafe2.putObject(t6, V, a0.j(unsafe2.getObject(t6, V), bVar2.c));
                                    }
                                    i22 = i34 | i32;
                                    bArr2 = bArr;
                                    i17 = i2;
                                    i18 = i3;
                                    i21 = i30;
                                    i20 = i28;
                                    i23 = i26;
                                    i24 = i10;
                                    break;
                                } else {
                                    i12 = i28;
                                    i6 = i26;
                                    i13 = i30;
                                    i7 = i25;
                                    i9 = i34;
                                    i11 = i12;
                                    unsafe = unsafe2;
                                    i8 = i13;
                                    i4 = i3;
                                    break;
                                }
                            default:
                                i12 = i28;
                                i13 = i30;
                                i6 = i26;
                                i7 = i25;
                                i9 = i34;
                                i11 = i12;
                                unsafe = unsafe2;
                                i8 = i13;
                                i4 = i3;
                                break;
                        }
                    } else {
                        i6 = i26;
                        T t10 = t6;
                        bArr2 = bArr;
                        if (s0 != 27) {
                            i11 = i28;
                            i9 = i22;
                            i10 = i24;
                            if (s0 <= 49) {
                                int i37 = i25;
                                unsafe = unsafe2;
                                i16 = i30;
                                i19 = f0(t, bArr, i25, i2, i30, i6, i27, i11, i29, s0, V, bVar);
                                if (i19 != i37) {
                                    p0Var2 = this;
                                    t6 = t;
                                    bArr2 = bArr;
                                    i17 = i2;
                                    i18 = i3;
                                    bVar2 = bVar;
                                    i23 = i6;
                                    i21 = i16;
                                    i20 = i11;
                                    i22 = i9;
                                    i24 = i10;
                                    unsafe2 = unsafe;
                                } else {
                                    i4 = i3;
                                    i7 = i19;
                                    i8 = i16;
                                }
                            } else {
                                i15 = i25;
                                unsafe = unsafe2;
                                i16 = i30;
                                if (s0 != 50) {
                                    i19 = c0(t, bArr, i15, i2, i16, i6, i27, i29, s0, V, i11, bVar);
                                    if (i19 != i15) {
                                        p0Var2 = this;
                                        t6 = t;
                                        bArr2 = bArr;
                                        i17 = i2;
                                        i18 = i3;
                                        bVar2 = bVar;
                                        i23 = i6;
                                        i21 = i16;
                                        i20 = i11;
                                        i22 = i9;
                                        i24 = i10;
                                        unsafe2 = unsafe;
                                    } else {
                                        i4 = i3;
                                        i7 = i19;
                                        i8 = i16;
                                    }
                                } else if (i27 == 2) {
                                    i19 = b0(t, bArr, i15, i2, i11, V, bVar);
                                    if (i19 != i15) {
                                        p0Var2 = this;
                                        t6 = t;
                                        bArr2 = bArr;
                                        i17 = i2;
                                        i18 = i3;
                                        bVar2 = bVar;
                                        i23 = i6;
                                        i21 = i16;
                                        i20 = i11;
                                        i22 = i9;
                                        i24 = i10;
                                        unsafe2 = unsafe;
                                    } else {
                                        i4 = i3;
                                        i7 = i19;
                                        i8 = i16;
                                    }
                                }
                            }
                        } else if (i27 == 2) {
                            a0.i iVar = (a0.i) unsafe2.getObject(t10, V);
                            if (!iVar.A()) {
                                int size = iVar.size();
                                iVar = iVar.a(size == 0 ? 10 : size * 2);
                                unsafe2.putObject(t10, V, iVar);
                            }
                            i10 = i24;
                            i19 = e.q(p0Var2.v(i28), i30, bArr, i25, i2, iVar, bVar);
                            t6 = t;
                            i17 = i2;
                            i21 = i30;
                            i23 = i6;
                            i20 = i28;
                            i22 = i22;
                            i24 = i10;
                            i18 = i3;
                        } else {
                            i11 = i28;
                            i9 = i22;
                            i10 = i24;
                            i15 = i25;
                            unsafe = unsafe2;
                            i16 = i30;
                        }
                        i4 = i3;
                        i7 = i15;
                        i8 = i16;
                    }
                }
                if (i8 != i4 || i4 == 0) {
                    if (this.f && bVar.d != r.b()) {
                        i19 = e.g(i8, bArr, i7, i2, t, this.e, this.o, bVar);
                    } else {
                        i19 = e.G(i8, bArr, i7, i2, w(t), bVar);
                    }
                    t6 = t;
                    bArr2 = bArr;
                    i17 = i2;
                    i21 = i8;
                    p0Var2 = this;
                    bVar2 = bVar;
                    i23 = i6;
                    i20 = i11;
                    i22 = i9;
                    i24 = i10;
                    unsafe2 = unsafe;
                    i18 = i4;
                } else {
                    i5 = 1048575;
                    p0Var = this;
                    i19 = i7;
                    i21 = i8;
                    i22 = i9;
                    i24 = i10;
                }
            } else {
                unsafe = unsafe2;
                i4 = i18;
                p0Var = p0Var2;
                i5 = 1048575;
            }
        }
        if (i24 != i5) {
            t2 = t;
            unsafe.putInt(t2, i24, i22);
        } else {
            t2 = t;
        }
        h1 h1Var = null;
        for (int i38 = p0Var.k; i38 < p0Var.l; i38++) {
            h1Var = (h1) p0Var.q(t2, p0Var.j[i38], h1Var, p0Var.o);
        }
        if (h1Var != null) {
            p0Var.o.o(t2, h1Var);
        }
        if (i4 == 0) {
            if (i19 != i2) {
                throw InvalidProtocolBufferException.parseFailure();
            }
        } else if (i19 > i2 || i21 != i4) {
            throw InvalidProtocolBufferException.parseFailure();
        }
        return i19;
    }

    @Override // com.google.protobuf.y0
    public void e(T t) {
        int i;
        int i2 = this.k;
        while (true) {
            i = this.l;
            if (i2 >= i) {
                break;
            }
            long V = V(t0(this.j[i2]));
            Object E = k1.E(t, V);
            if (E != null) {
                k1.U(t, V, this.q.b(E));
            }
            i2++;
        }
        int length = this.j.length;
        while (i < length) {
            this.n.c(t, this.j[i]);
            i++;
        }
        this.o.j(t);
        if (this.f) {
            this.p.f(t);
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:103:0x028e, code lost:
        if (r0 != r15) goto L151;
     */
    /* JADX WARN: Code restructure failed: missing block: B:104:0x0290, code lost:
        r15 = r30;
        r14 = r31;
        r12 = r32;
        r13 = r34;
        r11 = r35;
        r10 = r18;
        r1 = r19;
        r2 = r20;
        r6 = r24;
        r7 = r25;
     */
    /* JADX WARN: Code restructure failed: missing block: B:110:0x02d7, code lost:
        if (r0 != r15) goto L151;
     */
    /* JADX WARN: Code restructure failed: missing block: B:115:0x02fa, code lost:
        if (r0 != r15) goto L151;
     */
    /* JADX WARN: Code restructure failed: missing block: B:117:0x02fd, code lost:
        r2 = r0;
     */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r3v10, types: [int] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final int e0(T r31, byte[] r32, int r33, int r34, com.google.protobuf.e.b r35) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 870
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.p0.e0(java.lang.Object, byte[], int, int, com.google.protobuf.e$b):int");
    }

    @Override // com.google.protobuf.y0
    public final boolean f(T t) {
        int i;
        int i2;
        int i3 = 1048575;
        int i4 = 0;
        int i5 = 0;
        while (i5 < this.k) {
            int i6 = this.j[i5];
            int U = U(i6);
            int t0 = t0(i6);
            int i7 = this.a[i6 + 2];
            int i8 = i7 & 1048575;
            int i9 = 1 << (i7 >>> 20);
            if (i8 != i3) {
                if (i8 != 1048575) {
                    i4 = s.getInt(t, i8);
                }
                i2 = i4;
                i = i8;
            } else {
                i = i3;
                i2 = i4;
            }
            if (J(t0) && !D(t, i6, i, i2, i9)) {
                return false;
            }
            int s0 = s0(t0);
            if (s0 != 9 && s0 != 17) {
                if (s0 != 27) {
                    if (s0 == 60 || s0 == 68) {
                        if (I(t, U, i6) && !E(t, t0, v(i6))) {
                            return false;
                        }
                    } else if (s0 != 49) {
                        if (s0 == 50 && !G(t, t0, i6)) {
                            return false;
                        }
                    }
                }
                if (!F(t, t0, i6)) {
                    return false;
                }
            } else if (D(t, i6, i, i2, i9) && !E(t, t0, v(i6))) {
                return false;
            }
            i5++;
            i3 = i;
            i4 = i2;
        }
        return !this.f || this.p.c(t).D();
    }

    public final int f0(T t, byte[] bArr, int i, int i2, int i3, int i4, int i5, int i6, long j, int i7, long j2, e.b bVar) throws IOException {
        int J;
        Unsafe unsafe = s;
        a0.i iVar = (a0.i) unsafe.getObject(t, j2);
        if (!iVar.A()) {
            int size = iVar.size();
            iVar = iVar.a(size == 0 ? 10 : size * 2);
            unsafe.putObject(t, j2, iVar);
        }
        switch (i7) {
            case 18:
            case 35:
                if (i5 == 2) {
                    return e.s(bArr, i, iVar, bVar);
                }
                if (i5 == 1) {
                    return e.e(i3, bArr, i, i2, iVar, bVar);
                }
                break;
            case 19:
            case 36:
                if (i5 == 2) {
                    return e.v(bArr, i, iVar, bVar);
                }
                if (i5 == 5) {
                    return e.m(i3, bArr, i, i2, iVar, bVar);
                }
                break;
            case 20:
            case 21:
            case 37:
            case 38:
                if (i5 == 2) {
                    return e.z(bArr, i, iVar, bVar);
                }
                if (i5 == 0) {
                    return e.M(i3, bArr, i, i2, iVar, bVar);
                }
                break;
            case 22:
            case 29:
            case 39:
            case 43:
                if (i5 == 2) {
                    return e.y(bArr, i, iVar, bVar);
                }
                if (i5 == 0) {
                    return e.J(i3, bArr, i, i2, iVar, bVar);
                }
                break;
            case 23:
            case 32:
            case 40:
            case 46:
                if (i5 == 2) {
                    return e.u(bArr, i, iVar, bVar);
                }
                if (i5 == 1) {
                    return e.k(i3, bArr, i, i2, iVar, bVar);
                }
                break;
            case 24:
            case 31:
            case 41:
            case 45:
                if (i5 == 2) {
                    return e.t(bArr, i, iVar, bVar);
                }
                if (i5 == 5) {
                    return e.i(i3, bArr, i, i2, iVar, bVar);
                }
                break;
            case 25:
            case 42:
                if (i5 == 2) {
                    return e.r(bArr, i, iVar, bVar);
                }
                if (i5 == 0) {
                    return e.a(i3, bArr, i, i2, iVar, bVar);
                }
                break;
            case 26:
                if (i5 == 2) {
                    if ((j & 536870912) == 0) {
                        return e.D(i3, bArr, i, i2, iVar, bVar);
                    }
                    return e.E(i3, bArr, i, i2, iVar, bVar);
                }
                break;
            case 27:
                if (i5 == 2) {
                    return e.q(v(i6), i3, bArr, i, i2, iVar, bVar);
                }
                break;
            case 28:
                if (i5 == 2) {
                    return e.c(i3, bArr, i, i2, iVar, bVar);
                }
                break;
            case 30:
            case 44:
                if (i5 == 2) {
                    J = e.y(bArr, i, iVar, bVar);
                } else if (i5 == 0) {
                    J = e.J(i3, bArr, i, i2, iVar, bVar);
                }
                GeneratedMessageLite generatedMessageLite = (GeneratedMessageLite) t;
                h1 h1Var = generatedMessageLite.a;
                if (h1Var == h1.c()) {
                    h1Var = null;
                }
                h1 h1Var2 = (h1) z0.A(i4, iVar, t(i6), h1Var, this.o);
                if (h1Var2 != null) {
                    generatedMessageLite.a = h1Var2;
                }
                return J;
            case 33:
            case 47:
                if (i5 == 2) {
                    return e.w(bArr, i, iVar, bVar);
                }
                if (i5 == 0) {
                    return e.A(i3, bArr, i, i2, iVar, bVar);
                }
                break;
            case 34:
            case 48:
                if (i5 == 2) {
                    return e.x(bArr, i, iVar, bVar);
                }
                if (i5 == 0) {
                    return e.B(i3, bArr, i, i2, iVar, bVar);
                }
                break;
            case 49:
                if (i5 == 3) {
                    return e.o(v(i6), i3, bArr, i, i2, iVar, bVar);
                }
                break;
        }
        return i;
    }

    @Override // com.google.protobuf.y0
    public int g(T t) {
        return this.h ? y(t) : x(t);
    }

    public final int g0(int i) {
        if (i < this.c || i > this.d) {
            return -1;
        }
        return q0(i, 0);
    }

    @Override // com.google.protobuf.y0
    public void h(T t, Writer writer) throws IOException {
        if (writer.i() == Writer.FieldOrder.DESCENDING) {
            w0(t, writer);
        } else if (this.h) {
            v0(t, writer);
        } else {
            u0(t, writer);
        }
    }

    public final int h0(int i, int i2) {
        if (i < this.c || i > this.d) {
            return -1;
        }
        return q0(i, i2);
    }

    @Override // com.google.protobuf.y0
    public void i(T t, w0 w0Var, r rVar) throws IOException {
        Objects.requireNonNull(rVar);
        M(this.o, this.p, t, w0Var, rVar);
    }

    public final int i0(int i) {
        return this.a[i + 2];
    }

    @Override // com.google.protobuf.y0
    public void j(T t, byte[] bArr, int i, int i2, e.b bVar) throws IOException {
        if (this.h) {
            e0(t, bArr, i, i2, bVar);
        } else {
            d0(t, bArr, i, i2, 0, bVar);
        }
    }

    public final <E> void j0(Object obj, long j, w0 w0Var, y0<E> y0Var, r rVar) throws IOException {
        w0Var.J(this.n.e(obj, j), y0Var, rVar);
    }

    public final boolean k(T t, T t2, int i) {
        return C(t, i) == C(t2, i);
    }

    public final <E> void k0(Object obj, int i, w0 w0Var, y0<E> y0Var, r rVar) throws IOException {
        w0Var.L(this.n.e(obj, V(i)), y0Var, rVar);
    }

    public final void l0(Object obj, int i, w0 w0Var) throws IOException {
        if (B(i)) {
            k1.U(obj, V(i), w0Var.H());
        } else if (this.g) {
            k1.U(obj, V(i), w0Var.v());
        } else {
            k1.U(obj, V(i), w0Var.z());
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r12v1, types: [java.lang.Object] */
    /* JADX WARN: Type inference failed for: r12v2 */
    /* JADX WARN: Type inference failed for: r12v3, types: [java.lang.Object] */
    /* JADX WARN: Type inference failed for: r13v1, types: [java.lang.Object] */
    /* JADX WARN: Type inference failed for: r13v2, types: [java.lang.Object] */
    /* JADX WARN: Type inference failed for: r13v3 */
    /* JADX WARN: Type inference failed for: r19v0, types: [java.util.Map, java.util.Map<K, V>] */
    /* JADX WARN: Type inference failed for: r1v10, types: [int] */
    public final <K, V> int m(byte[] bArr, int i, int i2, h0.b<K, V> bVar, Map<K, V> map, e.b bVar2) throws IOException {
        int i3;
        int I = e.I(bArr, i, bVar2);
        int i4 = bVar2.a;
        if (i4 >= 0 && i4 <= i2 - I) {
            int i5 = I + i4;
            K k = bVar.b;
            V v = bVar.d;
            while (I < i5) {
                int i6 = I + 1;
                byte b = bArr[I];
                if (b < 0) {
                    i3 = e.H(b, bArr, i6, bVar2);
                    b = bVar2.a;
                } else {
                    i3 = i6;
                }
                int i7 = b >>> 3;
                int i8 = b & 7;
                if (i7 != 1) {
                    if (i7 == 2 && i8 == bVar.c.getWireType()) {
                        I = n(bArr, i3, i2, bVar.c, bVar.d.getClass(), bVar2);
                        v = bVar2.c;
                    }
                    I = e.N(b, bArr, i3, i2, bVar2);
                } else if (i8 == bVar.a.getWireType()) {
                    I = n(bArr, i3, i2, bVar.a, null, bVar2);
                    k = bVar2.c;
                } else {
                    I = e.N(b, bArr, i3, i2, bVar2);
                }
            }
            if (I == i5) {
                map.put(k, v);
                return i5;
            }
            throw InvalidProtocolBufferException.parseFailure();
        }
        throw InvalidProtocolBufferException.truncatedMessage();
    }

    public final void m0(Object obj, int i, w0 w0Var) throws IOException {
        if (B(i)) {
            w0Var.y(this.n.e(obj, V(i)));
        } else {
            w0Var.x(this.n.e(obj, V(i)));
        }
    }

    public final int n(byte[] bArr, int i, int i2, WireFormat.FieldType fieldType, Class<?> cls, e.b bVar) throws IOException {
        switch (a.a[fieldType.ordinal()]) {
            case 1:
                int L = e.L(bArr, i, bVar);
                bVar.c = Boolean.valueOf(bVar.b != 0);
                return L;
            case 2:
                return e.b(bArr, i, bVar);
            case 3:
                bVar.c = Double.valueOf(e.d(bArr, i));
                return i + 8;
            case 4:
            case 5:
                bVar.c = Integer.valueOf(e.h(bArr, i));
                return i + 4;
            case 6:
            case 7:
                bVar.c = Long.valueOf(e.j(bArr, i));
                return i + 8;
            case 8:
                bVar.c = Float.valueOf(e.l(bArr, i));
                return i + 4;
            case 9:
            case 10:
            case 11:
                int I = e.I(bArr, i, bVar);
                bVar.c = Integer.valueOf(bVar.a);
                return I;
            case 12:
            case 13:
                int L2 = e.L(bArr, i, bVar);
                bVar.c = Long.valueOf(bVar.b);
                return L2;
            case 14:
                return e.p(u0.a().d(cls), bArr, i, i2, bVar);
            case 15:
                int I2 = e.I(bArr, i, bVar);
                bVar.c = Integer.valueOf(j.b(bVar.a));
                return I2;
            case 16:
                int L3 = e.L(bArr, i, bVar);
                bVar.c = Long.valueOf(j.c(bVar.b));
                return L3;
            case 17:
                return e.F(bArr, i, bVar);
            default:
                throw new RuntimeException("unsupported field type.");
        }
    }

    public final void o0(T t, int i) {
        int i0 = i0(i);
        long j = 1048575 & i0;
        if (j == 1048575) {
            return;
        }
        k1.S(t, j, (1 << (i0 >>> 20)) | k1.A(t, j));
    }

    public final boolean p(T t, T t2, int i) {
        int t0 = t0(i);
        long V = V(t0);
        switch (s0(t0)) {
            case 0:
                return k(t, t2, i) && Double.doubleToLongBits(k1.y(t, V)) == Double.doubleToLongBits(k1.y(t2, V));
            case 1:
                return k(t, t2, i) && Float.floatToIntBits(k1.z(t, V)) == Float.floatToIntBits(k1.z(t2, V));
            case 2:
                return k(t, t2, i) && k1.C(t, V) == k1.C(t2, V);
            case 3:
                return k(t, t2, i) && k1.C(t, V) == k1.C(t2, V);
            case 4:
                return k(t, t2, i) && k1.A(t, V) == k1.A(t2, V);
            case 5:
                return k(t, t2, i) && k1.C(t, V) == k1.C(t2, V);
            case 6:
                return k(t, t2, i) && k1.A(t, V) == k1.A(t2, V);
            case 7:
                return k(t, t2, i) && k1.r(t, V) == k1.r(t2, V);
            case 8:
                return k(t, t2, i) && z0.L(k1.E(t, V), k1.E(t2, V));
            case 9:
                return k(t, t2, i) && z0.L(k1.E(t, V), k1.E(t2, V));
            case 10:
                return k(t, t2, i) && z0.L(k1.E(t, V), k1.E(t2, V));
            case 11:
                return k(t, t2, i) && k1.A(t, V) == k1.A(t2, V);
            case 12:
                return k(t, t2, i) && k1.A(t, V) == k1.A(t2, V);
            case 13:
                return k(t, t2, i) && k1.A(t, V) == k1.A(t2, V);
            case 14:
                return k(t, t2, i) && k1.C(t, V) == k1.C(t2, V);
            case 15:
                return k(t, t2, i) && k1.A(t, V) == k1.A(t2, V);
            case 16:
                return k(t, t2, i) && k1.C(t, V) == k1.C(t2, V);
            case 17:
                return k(t, t2, i) && z0.L(k1.E(t, V), k1.E(t2, V));
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
            case 35:
            case 36:
            case 37:
            case 38:
            case 39:
            case 40:
            case 41:
            case 42:
            case 43:
            case 44:
            case 45:
            case 46:
            case 47:
            case 48:
            case 49:
                return z0.L(k1.E(t, V), k1.E(t2, V));
            case 50:
                return z0.L(k1.E(t, V), k1.E(t2, V));
            case 51:
            case 52:
            case 53:
            case 54:
            case 55:
            case 56:
            case 57:
            case 58:
            case 59:
            case 60:
            case 61:
            case 62:
            case 63:
            case 64:
            case 65:
            case 66:
            case 67:
            case 68:
                return H(t, t2, i) && z0.L(k1.E(t, V), k1.E(t2, V));
            default:
                return true;
        }
    }

    public final void p0(T t, int i, int i2) {
        k1.S(t, i0(i2) & 1048575, i);
    }

    public final <UT, UB> UB q(Object obj, int i, UB ub, f1<UT, UB> f1Var) {
        a0.e t;
        int U = U(i);
        Object E = k1.E(obj, V(t0(i)));
        return (E == null || (t = t(i)) == null) ? ub : (UB) r(i, U, this.q.d(E), t, ub, f1Var);
    }

    public final int q0(int i, int i2) {
        int length = (this.a.length / 3) - 1;
        while (i2 <= length) {
            int i3 = (length + i2) >>> 1;
            int i4 = i3 * 3;
            int U = U(i4);
            if (i == U) {
                return i4;
            }
            if (i < U) {
                length = i3 - 1;
            } else {
                i2 = i3 + 1;
            }
        }
        return -1;
    }

    public final <K, V, UT, UB> UB r(int i, int i2, Map<K, V> map, a0.e eVar, UB ub, f1<UT, UB> f1Var) {
        h0.b<?, ?> c = this.q.c(u(i));
        Iterator<Map.Entry<K, V>> it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<K, V> next = it.next();
            if (!eVar.a(((Integer) next.getValue()).intValue())) {
                if (ub == null) {
                    ub = f1Var.n();
                }
                ByteString.g newCodedBuilder = ByteString.newCodedBuilder(h0.b(c, next.getKey(), next.getValue()));
                try {
                    h0.f(newCodedBuilder.b(), c, next.getKey(), next.getValue());
                    f1Var.d(ub, i2, newCodedBuilder.a());
                    it.remove();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        return ub;
    }

    public final a0.e t(int i) {
        return (a0.e) this.b[((i / 3) * 2) + 1];
    }

    public final int t0(int i) {
        return this.a[i + 1];
    }

    public final Object u(int i) {
        return this.b[(i / 3) * 2];
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0030  */
    /* JADX WARN: Removed duplicated region for block: B:172:0x0493  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void u0(T r18, com.google.protobuf.Writer r19) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 1340
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.p0.u0(java.lang.Object, com.google.protobuf.Writer):void");
    }

    public final y0 v(int i) {
        int i2 = (i / 3) * 2;
        y0 y0Var = (y0) this.b[i2];
        if (y0Var != null) {
            return y0Var;
        }
        y0<T> d = u0.a().d((Class) this.b[i2 + 1]);
        this.b[i2] = d;
        return d;
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0025  */
    /* JADX WARN: Removed duplicated region for block: B:164:0x0588  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void v0(T r13, com.google.protobuf.Writer r14) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 1584
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.p0.v0(java.lang.Object, com.google.protobuf.Writer):void");
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x002a  */
    /* JADX WARN: Removed duplicated region for block: B:164:0x058e  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void w0(T r11, com.google.protobuf.Writer r12) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 1586
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.p0.w0(java.lang.Object, com.google.protobuf.Writer):void");
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    public final int x(T t) {
        int i;
        int i2;
        int j;
        int e;
        int N;
        boolean z;
        int f;
        int i3;
        int X;
        int Z;
        Unsafe unsafe = s;
        int i4 = 1048575;
        int i5 = 1048575;
        int i6 = 0;
        int i7 = 0;
        int i8 = 0;
        while (i6 < this.a.length) {
            int t0 = t0(i6);
            int U = U(i6);
            int s0 = s0(t0);
            if (s0 <= 17) {
                i = this.a[i6 + 2];
                int i9 = i & i4;
                i2 = 1 << (i >>> 20);
                if (i9 != i5) {
                    i8 = unsafe.getInt(t, i9);
                    i5 = i9;
                }
            } else {
                i = (!this.i || s0 < FieldType.DOUBLE_LIST_PACKED.id() || s0 > FieldType.SINT64_LIST_PACKED.id()) ? 0 : this.a[i6 + 2] & i4;
                i2 = 0;
            }
            long V = V(t0);
            switch (s0) {
                case 0:
                    if ((i8 & i2) == 0) {
                        break;
                    } else {
                        j = CodedOutputStream.j(U, Utils.DOUBLE_EPSILON);
                        i7 += j;
                        break;
                    }
                case 1:
                    if ((i8 & i2) == 0) {
                        break;
                    } else {
                        j = CodedOutputStream.r(U, Utils.FLOAT_EPSILON);
                        i7 += j;
                        break;
                    }
                case 2:
                    if ((i8 & i2) == 0) {
                        break;
                    } else {
                        j = CodedOutputStream.z(U, unsafe.getLong(t, V));
                        i7 += j;
                        break;
                    }
                case 3:
                    if ((i8 & i2) == 0) {
                        break;
                    } else {
                        j = CodedOutputStream.a0(U, unsafe.getLong(t, V));
                        i7 += j;
                        break;
                    }
                case 4:
                    if ((i8 & i2) == 0) {
                        break;
                    } else {
                        j = CodedOutputStream.x(U, unsafe.getInt(t, V));
                        i7 += j;
                        break;
                    }
                case 5:
                    if ((i8 & i2) == 0) {
                        break;
                    } else {
                        j = CodedOutputStream.p(U, 0L);
                        i7 += j;
                        break;
                    }
                case 6:
                    if ((i8 & i2) != 0) {
                        j = CodedOutputStream.n(U, 0);
                        i7 += j;
                        break;
                    }
                    break;
                case 7:
                    if ((i8 & i2) != 0) {
                        e = CodedOutputStream.e(U, true);
                        i7 += e;
                    }
                    break;
                case 8:
                    if ((i8 & i2) != 0) {
                        Object object = unsafe.getObject(t, V);
                        if (object instanceof ByteString) {
                            e = CodedOutputStream.h(U, (ByteString) object);
                        } else {
                            e = CodedOutputStream.V(U, (String) object);
                        }
                        i7 += e;
                    }
                    break;
                case 9:
                    if ((i8 & i2) != 0) {
                        e = z0.o(U, unsafe.getObject(t, V), v(i6));
                        i7 += e;
                    }
                    break;
                case 10:
                    if ((i8 & i2) != 0) {
                        e = CodedOutputStream.h(U, (ByteString) unsafe.getObject(t, V));
                        i7 += e;
                    }
                    break;
                case 11:
                    if ((i8 & i2) != 0) {
                        e = CodedOutputStream.Y(U, unsafe.getInt(t, V));
                        i7 += e;
                    }
                    break;
                case 12:
                    if ((i8 & i2) != 0) {
                        e = CodedOutputStream.l(U, unsafe.getInt(t, V));
                        i7 += e;
                    }
                    break;
                case 13:
                    if ((i8 & i2) != 0) {
                        N = CodedOutputStream.N(U, 0);
                        i7 += N;
                    }
                    break;
                case 14:
                    if ((i8 & i2) != 0) {
                        e = CodedOutputStream.P(U, 0L);
                        i7 += e;
                    }
                    break;
                case 15:
                    if ((i8 & i2) != 0) {
                        e = CodedOutputStream.R(U, unsafe.getInt(t, V));
                        i7 += e;
                    }
                    break;
                case 16:
                    if ((i8 & i2) != 0) {
                        e = CodedOutputStream.T(U, unsafe.getLong(t, V));
                        i7 += e;
                    }
                    break;
                case 17:
                    if ((i8 & i2) != 0) {
                        e = CodedOutputStream.u(U, (m0) unsafe.getObject(t, V), v(i6));
                        i7 += e;
                    }
                    break;
                case 18:
                    e = z0.h(U, (List) unsafe.getObject(t, V), false);
                    i7 += e;
                    break;
                case 19:
                    z = false;
                    f = z0.f(U, (List) unsafe.getObject(t, V), false);
                    i7 += f;
                    break;
                case 20:
                    z = false;
                    f = z0.m(U, (List) unsafe.getObject(t, V), false);
                    i7 += f;
                    break;
                case 21:
                    z = false;
                    f = z0.x(U, (List) unsafe.getObject(t, V), false);
                    i7 += f;
                    break;
                case 22:
                    z = false;
                    f = z0.k(U, (List) unsafe.getObject(t, V), false);
                    i7 += f;
                    break;
                case 23:
                    z = false;
                    f = z0.h(U, (List) unsafe.getObject(t, V), false);
                    i7 += f;
                    break;
                case 24:
                    z = false;
                    f = z0.f(U, (List) unsafe.getObject(t, V), false);
                    i7 += f;
                    break;
                case 25:
                    z = false;
                    f = z0.a(U, (List) unsafe.getObject(t, V), false);
                    i7 += f;
                    break;
                case 26:
                    e = z0.u(U, (List) unsafe.getObject(t, V));
                    i7 += e;
                    break;
                case 27:
                    e = z0.p(U, (List) unsafe.getObject(t, V), v(i6));
                    i7 += e;
                    break;
                case 28:
                    e = z0.c(U, (List) unsafe.getObject(t, V));
                    i7 += e;
                    break;
                case 29:
                    e = z0.v(U, (List) unsafe.getObject(t, V), false);
                    i7 += e;
                    break;
                case 30:
                    z = false;
                    f = z0.d(U, (List) unsafe.getObject(t, V), false);
                    i7 += f;
                    break;
                case 31:
                    z = false;
                    f = z0.f(U, (List) unsafe.getObject(t, V), false);
                    i7 += f;
                    break;
                case 32:
                    z = false;
                    f = z0.h(U, (List) unsafe.getObject(t, V), false);
                    i7 += f;
                    break;
                case 33:
                    z = false;
                    f = z0.q(U, (List) unsafe.getObject(t, V), false);
                    i7 += f;
                    break;
                case 34:
                    z = false;
                    f = z0.s(U, (List) unsafe.getObject(t, V), false);
                    i7 += f;
                    break;
                case 35:
                    i3 = z0.i((List) unsafe.getObject(t, V));
                    if (i3 > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i, i3);
                        }
                        X = CodedOutputStream.X(U);
                        Z = CodedOutputStream.Z(i3);
                        N = X + Z + i3;
                        i7 += N;
                    }
                    break;
                case 36:
                    i3 = z0.g((List) unsafe.getObject(t, V));
                    if (i3 > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i, i3);
                        }
                        X = CodedOutputStream.X(U);
                        Z = CodedOutputStream.Z(i3);
                        N = X + Z + i3;
                        i7 += N;
                    }
                    break;
                case 37:
                    i3 = z0.n((List) unsafe.getObject(t, V));
                    if (i3 > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i, i3);
                        }
                        X = CodedOutputStream.X(U);
                        Z = CodedOutputStream.Z(i3);
                        N = X + Z + i3;
                        i7 += N;
                    }
                    break;
                case 38:
                    i3 = z0.y((List) unsafe.getObject(t, V));
                    if (i3 > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i, i3);
                        }
                        X = CodedOutputStream.X(U);
                        Z = CodedOutputStream.Z(i3);
                        N = X + Z + i3;
                        i7 += N;
                    }
                    break;
                case 39:
                    i3 = z0.l((List) unsafe.getObject(t, V));
                    if (i3 > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i, i3);
                        }
                        X = CodedOutputStream.X(U);
                        Z = CodedOutputStream.Z(i3);
                        N = X + Z + i3;
                        i7 += N;
                    }
                    break;
                case 40:
                    i3 = z0.i((List) unsafe.getObject(t, V));
                    if (i3 > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i, i3);
                        }
                        X = CodedOutputStream.X(U);
                        Z = CodedOutputStream.Z(i3);
                        N = X + Z + i3;
                        i7 += N;
                    }
                    break;
                case 41:
                    i3 = z0.g((List) unsafe.getObject(t, V));
                    if (i3 > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i, i3);
                        }
                        X = CodedOutputStream.X(U);
                        Z = CodedOutputStream.Z(i3);
                        N = X + Z + i3;
                        i7 += N;
                    }
                    break;
                case 42:
                    i3 = z0.b((List) unsafe.getObject(t, V));
                    if (i3 > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i, i3);
                        }
                        X = CodedOutputStream.X(U);
                        Z = CodedOutputStream.Z(i3);
                        N = X + Z + i3;
                        i7 += N;
                    }
                    break;
                case 43:
                    i3 = z0.w((List) unsafe.getObject(t, V));
                    if (i3 > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i, i3);
                        }
                        X = CodedOutputStream.X(U);
                        Z = CodedOutputStream.Z(i3);
                        N = X + Z + i3;
                        i7 += N;
                    }
                    break;
                case 44:
                    i3 = z0.e((List) unsafe.getObject(t, V));
                    if (i3 > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i, i3);
                        }
                        X = CodedOutputStream.X(U);
                        Z = CodedOutputStream.Z(i3);
                        N = X + Z + i3;
                        i7 += N;
                    }
                    break;
                case 45:
                    i3 = z0.g((List) unsafe.getObject(t, V));
                    if (i3 > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i, i3);
                        }
                        X = CodedOutputStream.X(U);
                        Z = CodedOutputStream.Z(i3);
                        N = X + Z + i3;
                        i7 += N;
                    }
                    break;
                case 46:
                    i3 = z0.i((List) unsafe.getObject(t, V));
                    if (i3 > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i, i3);
                        }
                        X = CodedOutputStream.X(U);
                        Z = CodedOutputStream.Z(i3);
                        N = X + Z + i3;
                        i7 += N;
                    }
                    break;
                case 47:
                    i3 = z0.r((List) unsafe.getObject(t, V));
                    if (i3 > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i, i3);
                        }
                        X = CodedOutputStream.X(U);
                        Z = CodedOutputStream.Z(i3);
                        N = X + Z + i3;
                        i7 += N;
                    }
                    break;
                case 48:
                    i3 = z0.t((List) unsafe.getObject(t, V));
                    if (i3 > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i, i3);
                        }
                        X = CodedOutputStream.X(U);
                        Z = CodedOutputStream.Z(i3);
                        N = X + Z + i3;
                        i7 += N;
                    }
                    break;
                case 49:
                    e = z0.j(U, (List) unsafe.getObject(t, V), v(i6));
                    i7 += e;
                    break;
                case 50:
                    e = this.q.f(U, unsafe.getObject(t, V), u(i6));
                    i7 += e;
                    break;
                case 51:
                    if (I(t, U, i6)) {
                        e = CodedOutputStream.j(U, Utils.DOUBLE_EPSILON);
                        i7 += e;
                    }
                    break;
                case 52:
                    if (I(t, U, i6)) {
                        e = CodedOutputStream.r(U, Utils.FLOAT_EPSILON);
                        i7 += e;
                    }
                    break;
                case 53:
                    if (I(t, U, i6)) {
                        e = CodedOutputStream.z(U, a0(t, V));
                        i7 += e;
                    }
                    break;
                case 54:
                    if (I(t, U, i6)) {
                        e = CodedOutputStream.a0(U, a0(t, V));
                        i7 += e;
                    }
                    break;
                case 55:
                    if (I(t, U, i6)) {
                        e = CodedOutputStream.x(U, Z(t, V));
                        i7 += e;
                    }
                    break;
                case 56:
                    if (I(t, U, i6)) {
                        e = CodedOutputStream.p(U, 0L);
                        i7 += e;
                    }
                    break;
                case 57:
                    if (I(t, U, i6)) {
                        N = CodedOutputStream.n(U, 0);
                        i7 += N;
                    }
                    break;
                case 58:
                    if (I(t, U, i6)) {
                        e = CodedOutputStream.e(U, true);
                        i7 += e;
                    }
                    break;
                case 59:
                    if (I(t, U, i6)) {
                        Object object2 = unsafe.getObject(t, V);
                        if (object2 instanceof ByteString) {
                            e = CodedOutputStream.h(U, (ByteString) object2);
                        } else {
                            e = CodedOutputStream.V(U, (String) object2);
                        }
                        i7 += e;
                    }
                    break;
                case 60:
                    if (I(t, U, i6)) {
                        e = z0.o(U, unsafe.getObject(t, V), v(i6));
                        i7 += e;
                    }
                    break;
                case 61:
                    if (I(t, U, i6)) {
                        e = CodedOutputStream.h(U, (ByteString) unsafe.getObject(t, V));
                        i7 += e;
                    }
                    break;
                case 62:
                    if (I(t, U, i6)) {
                        e = CodedOutputStream.Y(U, Z(t, V));
                        i7 += e;
                    }
                    break;
                case 63:
                    if (I(t, U, i6)) {
                        e = CodedOutputStream.l(U, Z(t, V));
                        i7 += e;
                    }
                    break;
                case 64:
                    if (I(t, U, i6)) {
                        N = CodedOutputStream.N(U, 0);
                        i7 += N;
                    }
                    break;
                case 65:
                    if (I(t, U, i6)) {
                        e = CodedOutputStream.P(U, 0L);
                        i7 += e;
                    }
                    break;
                case 66:
                    if (I(t, U, i6)) {
                        e = CodedOutputStream.R(U, Z(t, V));
                        i7 += e;
                    }
                    break;
                case 67:
                    if (I(t, U, i6)) {
                        e = CodedOutputStream.T(U, a0(t, V));
                        i7 += e;
                    }
                    break;
                case 68:
                    if (I(t, U, i6)) {
                        e = CodedOutputStream.u(U, (m0) unsafe.getObject(t, V), v(i6));
                        i7 += e;
                    }
                    break;
            }
            i6 += 3;
            i4 = 1048575;
        }
        int z2 = i7 + z(this.o, t);
        return this.f ? z2 + this.p.c(t).y() : z2;
    }

    public final <K, V> void x0(Writer writer, int i, Object obj, int i2) throws IOException {
        if (obj != null) {
            writer.S(i, this.q.c(u(i2)), this.q.h(obj));
        }
    }

    public final int y(T t) {
        int j;
        int i;
        int X;
        int Z;
        Unsafe unsafe = s;
        int i2 = 0;
        for (int i3 = 0; i3 < this.a.length; i3 += 3) {
            int t0 = t0(i3);
            int s0 = s0(t0);
            int U = U(i3);
            long V = V(t0);
            int i4 = (s0 < FieldType.DOUBLE_LIST_PACKED.id() || s0 > FieldType.SINT64_LIST_PACKED.id()) ? 0 : this.a[i3 + 2] & 1048575;
            switch (s0) {
                case 0:
                    if (C(t, i3)) {
                        j = CodedOutputStream.j(U, Utils.DOUBLE_EPSILON);
                        break;
                    } else {
                        continue;
                    }
                case 1:
                    if (C(t, i3)) {
                        j = CodedOutputStream.r(U, Utils.FLOAT_EPSILON);
                        break;
                    } else {
                        continue;
                    }
                case 2:
                    if (C(t, i3)) {
                        j = CodedOutputStream.z(U, k1.C(t, V));
                        break;
                    } else {
                        continue;
                    }
                case 3:
                    if (C(t, i3)) {
                        j = CodedOutputStream.a0(U, k1.C(t, V));
                        break;
                    } else {
                        continue;
                    }
                case 4:
                    if (C(t, i3)) {
                        j = CodedOutputStream.x(U, k1.A(t, V));
                        break;
                    } else {
                        continue;
                    }
                case 5:
                    if (C(t, i3)) {
                        j = CodedOutputStream.p(U, 0L);
                        break;
                    } else {
                        continue;
                    }
                case 6:
                    if (C(t, i3)) {
                        j = CodedOutputStream.n(U, 0);
                        break;
                    } else {
                        continue;
                    }
                case 7:
                    if (C(t, i3)) {
                        j = CodedOutputStream.e(U, true);
                        break;
                    } else {
                        continue;
                    }
                case 8:
                    if (C(t, i3)) {
                        Object E = k1.E(t, V);
                        if (E instanceof ByteString) {
                            j = CodedOutputStream.h(U, (ByteString) E);
                            break;
                        } else {
                            j = CodedOutputStream.V(U, (String) E);
                            break;
                        }
                    } else {
                        continue;
                    }
                case 9:
                    if (C(t, i3)) {
                        j = z0.o(U, k1.E(t, V), v(i3));
                        break;
                    } else {
                        continue;
                    }
                case 10:
                    if (C(t, i3)) {
                        j = CodedOutputStream.h(U, (ByteString) k1.E(t, V));
                        break;
                    } else {
                        continue;
                    }
                case 11:
                    if (C(t, i3)) {
                        j = CodedOutputStream.Y(U, k1.A(t, V));
                        break;
                    } else {
                        continue;
                    }
                case 12:
                    if (C(t, i3)) {
                        j = CodedOutputStream.l(U, k1.A(t, V));
                        break;
                    } else {
                        continue;
                    }
                case 13:
                    if (C(t, i3)) {
                        j = CodedOutputStream.N(U, 0);
                        break;
                    } else {
                        continue;
                    }
                case 14:
                    if (C(t, i3)) {
                        j = CodedOutputStream.P(U, 0L);
                        break;
                    } else {
                        continue;
                    }
                case 15:
                    if (C(t, i3)) {
                        j = CodedOutputStream.R(U, k1.A(t, V));
                        break;
                    } else {
                        continue;
                    }
                case 16:
                    if (C(t, i3)) {
                        j = CodedOutputStream.T(U, k1.C(t, V));
                        break;
                    } else {
                        continue;
                    }
                case 17:
                    if (C(t, i3)) {
                        j = CodedOutputStream.u(U, (m0) k1.E(t, V), v(i3));
                        break;
                    } else {
                        continue;
                    }
                case 18:
                    j = z0.h(U, K(t, V), false);
                    break;
                case 19:
                    j = z0.f(U, K(t, V), false);
                    break;
                case 20:
                    j = z0.m(U, K(t, V), false);
                    break;
                case 21:
                    j = z0.x(U, K(t, V), false);
                    break;
                case 22:
                    j = z0.k(U, K(t, V), false);
                    break;
                case 23:
                    j = z0.h(U, K(t, V), false);
                    break;
                case 24:
                    j = z0.f(U, K(t, V), false);
                    break;
                case 25:
                    j = z0.a(U, K(t, V), false);
                    break;
                case 26:
                    j = z0.u(U, K(t, V));
                    break;
                case 27:
                    j = z0.p(U, K(t, V), v(i3));
                    break;
                case 28:
                    j = z0.c(U, K(t, V));
                    break;
                case 29:
                    j = z0.v(U, K(t, V), false);
                    break;
                case 30:
                    j = z0.d(U, K(t, V), false);
                    break;
                case 31:
                    j = z0.f(U, K(t, V), false);
                    break;
                case 32:
                    j = z0.h(U, K(t, V), false);
                    break;
                case 33:
                    j = z0.q(U, K(t, V), false);
                    break;
                case 34:
                    j = z0.s(U, K(t, V), false);
                    break;
                case 35:
                    i = z0.i((List) unsafe.getObject(t, V));
                    if (i > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i4, i);
                        }
                        X = CodedOutputStream.X(U);
                        Z = CodedOutputStream.Z(i);
                        j = X + Z + i;
                        break;
                    } else {
                        continue;
                    }
                case 36:
                    i = z0.g((List) unsafe.getObject(t, V));
                    if (i > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i4, i);
                        }
                        X = CodedOutputStream.X(U);
                        Z = CodedOutputStream.Z(i);
                        j = X + Z + i;
                        break;
                    } else {
                        continue;
                    }
                case 37:
                    i = z0.n((List) unsafe.getObject(t, V));
                    if (i > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i4, i);
                        }
                        X = CodedOutputStream.X(U);
                        Z = CodedOutputStream.Z(i);
                        j = X + Z + i;
                        break;
                    } else {
                        continue;
                    }
                case 38:
                    i = z0.y((List) unsafe.getObject(t, V));
                    if (i > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i4, i);
                        }
                        X = CodedOutputStream.X(U);
                        Z = CodedOutputStream.Z(i);
                        j = X + Z + i;
                        break;
                    } else {
                        continue;
                    }
                case 39:
                    i = z0.l((List) unsafe.getObject(t, V));
                    if (i > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i4, i);
                        }
                        X = CodedOutputStream.X(U);
                        Z = CodedOutputStream.Z(i);
                        j = X + Z + i;
                        break;
                    } else {
                        continue;
                    }
                case 40:
                    i = z0.i((List) unsafe.getObject(t, V));
                    if (i > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i4, i);
                        }
                        X = CodedOutputStream.X(U);
                        Z = CodedOutputStream.Z(i);
                        j = X + Z + i;
                        break;
                    } else {
                        continue;
                    }
                case 41:
                    i = z0.g((List) unsafe.getObject(t, V));
                    if (i > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i4, i);
                        }
                        X = CodedOutputStream.X(U);
                        Z = CodedOutputStream.Z(i);
                        j = X + Z + i;
                        break;
                    } else {
                        continue;
                    }
                case 42:
                    i = z0.b((List) unsafe.getObject(t, V));
                    if (i > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i4, i);
                        }
                        X = CodedOutputStream.X(U);
                        Z = CodedOutputStream.Z(i);
                        j = X + Z + i;
                        break;
                    } else {
                        continue;
                    }
                case 43:
                    i = z0.w((List) unsafe.getObject(t, V));
                    if (i > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i4, i);
                        }
                        X = CodedOutputStream.X(U);
                        Z = CodedOutputStream.Z(i);
                        j = X + Z + i;
                        break;
                    } else {
                        continue;
                    }
                case 44:
                    i = z0.e((List) unsafe.getObject(t, V));
                    if (i > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i4, i);
                        }
                        X = CodedOutputStream.X(U);
                        Z = CodedOutputStream.Z(i);
                        j = X + Z + i;
                        break;
                    } else {
                        continue;
                    }
                case 45:
                    i = z0.g((List) unsafe.getObject(t, V));
                    if (i > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i4, i);
                        }
                        X = CodedOutputStream.X(U);
                        Z = CodedOutputStream.Z(i);
                        j = X + Z + i;
                        break;
                    } else {
                        continue;
                    }
                case 46:
                    i = z0.i((List) unsafe.getObject(t, V));
                    if (i > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i4, i);
                        }
                        X = CodedOutputStream.X(U);
                        Z = CodedOutputStream.Z(i);
                        j = X + Z + i;
                        break;
                    } else {
                        continue;
                    }
                case 47:
                    i = z0.r((List) unsafe.getObject(t, V));
                    if (i > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i4, i);
                        }
                        X = CodedOutputStream.X(U);
                        Z = CodedOutputStream.Z(i);
                        j = X + Z + i;
                        break;
                    } else {
                        continue;
                    }
                case 48:
                    i = z0.t((List) unsafe.getObject(t, V));
                    if (i > 0) {
                        if (this.i) {
                            unsafe.putInt(t, i4, i);
                        }
                        X = CodedOutputStream.X(U);
                        Z = CodedOutputStream.Z(i);
                        j = X + Z + i;
                        break;
                    } else {
                        continue;
                    }
                case 49:
                    j = z0.j(U, K(t, V), v(i3));
                    break;
                case 50:
                    j = this.q.f(U, k1.E(t, V), u(i3));
                    break;
                case 51:
                    if (I(t, U, i3)) {
                        j = CodedOutputStream.j(U, Utils.DOUBLE_EPSILON);
                        break;
                    } else {
                        continue;
                    }
                case 52:
                    if (I(t, U, i3)) {
                        j = CodedOutputStream.r(U, Utils.FLOAT_EPSILON);
                        break;
                    } else {
                        continue;
                    }
                case 53:
                    if (I(t, U, i3)) {
                        j = CodedOutputStream.z(U, a0(t, V));
                        break;
                    } else {
                        continue;
                    }
                case 54:
                    if (I(t, U, i3)) {
                        j = CodedOutputStream.a0(U, a0(t, V));
                        break;
                    } else {
                        continue;
                    }
                case 55:
                    if (I(t, U, i3)) {
                        j = CodedOutputStream.x(U, Z(t, V));
                        break;
                    } else {
                        continue;
                    }
                case 56:
                    if (I(t, U, i3)) {
                        j = CodedOutputStream.p(U, 0L);
                        break;
                    } else {
                        continue;
                    }
                case 57:
                    if (I(t, U, i3)) {
                        j = CodedOutputStream.n(U, 0);
                        break;
                    } else {
                        continue;
                    }
                case 58:
                    if (I(t, U, i3)) {
                        j = CodedOutputStream.e(U, true);
                        break;
                    } else {
                        continue;
                    }
                case 59:
                    if (I(t, U, i3)) {
                        Object E2 = k1.E(t, V);
                        if (E2 instanceof ByteString) {
                            j = CodedOutputStream.h(U, (ByteString) E2);
                            break;
                        } else {
                            j = CodedOutputStream.V(U, (String) E2);
                            break;
                        }
                    } else {
                        continue;
                    }
                case 60:
                    if (I(t, U, i3)) {
                        j = z0.o(U, k1.E(t, V), v(i3));
                        break;
                    } else {
                        continue;
                    }
                case 61:
                    if (I(t, U, i3)) {
                        j = CodedOutputStream.h(U, (ByteString) k1.E(t, V));
                        break;
                    } else {
                        continue;
                    }
                case 62:
                    if (I(t, U, i3)) {
                        j = CodedOutputStream.Y(U, Z(t, V));
                        break;
                    } else {
                        continue;
                    }
                case 63:
                    if (I(t, U, i3)) {
                        j = CodedOutputStream.l(U, Z(t, V));
                        break;
                    } else {
                        continue;
                    }
                case 64:
                    if (I(t, U, i3)) {
                        j = CodedOutputStream.N(U, 0);
                        break;
                    } else {
                        continue;
                    }
                case 65:
                    if (I(t, U, i3)) {
                        j = CodedOutputStream.P(U, 0L);
                        break;
                    } else {
                        continue;
                    }
                case 66:
                    if (I(t, U, i3)) {
                        j = CodedOutputStream.R(U, Z(t, V));
                        break;
                    } else {
                        continue;
                    }
                case 67:
                    if (I(t, U, i3)) {
                        j = CodedOutputStream.T(U, a0(t, V));
                        break;
                    } else {
                        continue;
                    }
                case 68:
                    if (I(t, U, i3)) {
                        j = CodedOutputStream.u(U, (m0) k1.E(t, V), v(i3));
                        break;
                    } else {
                        continue;
                    }
                default:
            }
            i2 += j;
        }
        return i2 + z(this.o, t);
    }

    public final void y0(int i, Object obj, Writer writer) throws IOException {
        if (obj instanceof String) {
            writer.k(i, (String) obj);
        } else {
            writer.Q(i, (ByteString) obj);
        }
    }

    public final <UT, UB> int z(f1<UT, UB> f1Var, T t) {
        return f1Var.h(f1Var.g(t));
    }

    public final <UT, UB> void z0(f1<UT, UB> f1Var, T t, Writer writer) throws IOException {
        f1Var.t(f1Var.g(t), writer);
    }
}
