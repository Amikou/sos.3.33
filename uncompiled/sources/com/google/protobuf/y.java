package com.google.protobuf;

/* compiled from: GeneratedMessageInfoFactory.java */
/* loaded from: classes2.dex */
public class y implements b82 {
    public static final y a = new y();

    public static y c() {
        return a;
    }

    @Override // defpackage.b82
    public z72 a(Class<?> cls) {
        if (GeneratedMessageLite.class.isAssignableFrom(cls)) {
            try {
                return (z72) GeneratedMessageLite.e(cls.asSubclass(GeneratedMessageLite.class)).a();
            } catch (Exception e) {
                throw new RuntimeException("Unable to get message info for " + cls.getName(), e);
            }
        }
        throw new IllegalArgumentException("Unsupported message type: " + cls.getName());
    }

    @Override // defpackage.b82
    public boolean b(Class<?> cls) {
        return GeneratedMessageLite.class.isAssignableFrom(cls);
    }
}
