package com.google.protobuf;

import java.io.IOException;

/* compiled from: MessageLite.java */
/* loaded from: classes2.dex */
public interface m0 extends d82 {

    /* compiled from: MessageLite.java */
    /* loaded from: classes2.dex */
    public interface a extends d82, Cloneable {
        m0 build();

        m0 buildPartial();

        a mergeFrom(j jVar, r rVar) throws IOException;

        a mergeFrom(m0 m0Var);

        a mergeFrom(byte[] bArr) throws InvalidProtocolBufferException;
    }

    t0<? extends m0> getParserForType();

    int getSerializedSize();

    a newBuilderForType();

    a toBuilder();

    byte[] toByteArray();

    ByteString toByteString();

    void writeTo(CodedOutputStream codedOutputStream) throws IOException;
}
