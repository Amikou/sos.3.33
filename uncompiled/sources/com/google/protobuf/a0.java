package com.google.protobuf;

import com.google.zxing.qrcode.encoder.Encoder;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Objects;
import java.util.RandomAccess;

/* compiled from: Internal.java */
/* loaded from: classes2.dex */
public final class a0 {
    public static final Charset a = Charset.forName("UTF-8");
    public static final Charset b = Charset.forName(Encoder.DEFAULT_BYTE_MODE_ENCODING);
    public static final byte[] c;

    /* compiled from: Internal.java */
    /* loaded from: classes2.dex */
    public interface a extends i<Boolean> {
        @Override // com.google.protobuf.a0.i
        i<Boolean> a(int i);
    }

    /* compiled from: Internal.java */
    /* loaded from: classes2.dex */
    public interface b extends i<Double> {
        @Override // com.google.protobuf.a0.i
        i<Double> a(int i);
    }

    /* compiled from: Internal.java */
    /* loaded from: classes2.dex */
    public interface c {
        int getNumber();
    }

    /* compiled from: Internal.java */
    /* loaded from: classes2.dex */
    public interface d<T extends c> {
        T findValueByNumber(int i);
    }

    /* compiled from: Internal.java */
    /* loaded from: classes2.dex */
    public interface e {
        boolean a(int i);
    }

    /* compiled from: Internal.java */
    /* loaded from: classes2.dex */
    public interface f extends i<Float> {
        @Override // com.google.protobuf.a0.i
        i<Float> a(int i);
    }

    /* compiled from: Internal.java */
    /* loaded from: classes2.dex */
    public interface g extends i<Integer> {
        void X(int i);

        @Override // com.google.protobuf.a0.i
        i<Integer> a(int i);

        int getInt(int i);
    }

    /* compiled from: Internal.java */
    /* loaded from: classes2.dex */
    public interface h extends i<Long> {
        @Override // com.google.protobuf.a0.i
        i<Long> a(int i);
    }

    /* compiled from: Internal.java */
    /* loaded from: classes2.dex */
    public interface i<E> extends List<E>, RandomAccess {
        boolean A();

        i<E> a(int i);

        void l();
    }

    static {
        byte[] bArr = new byte[0];
        c = bArr;
        ByteBuffer.wrap(bArr);
        j.k(bArr);
    }

    public static <T> T a(T t) {
        Objects.requireNonNull(t);
        return t;
    }

    public static <T> T b(T t, String str) {
        Objects.requireNonNull(t, str);
        return t;
    }

    public static int c(boolean z) {
        return z ? 1231 : 1237;
    }

    public static int d(byte[] bArr) {
        return e(bArr, 0, bArr.length);
    }

    public static int e(byte[] bArr, int i2, int i3) {
        int k = k(i3, bArr, i2, i3);
        if (k == 0) {
            return 1;
        }
        return k;
    }

    public static int f(c cVar) {
        return cVar.getNumber();
    }

    public static int g(List<? extends c> list) {
        int i2 = 1;
        for (c cVar : list) {
            i2 = (i2 * 31) + f(cVar);
        }
        return i2;
    }

    public static int h(long j) {
        return (int) (j ^ (j >>> 32));
    }

    public static boolean i(byte[] bArr) {
        return Utf8.s(bArr);
    }

    public static Object j(Object obj, Object obj2) {
        return ((m0) obj).toBuilder().mergeFrom((m0) obj2).buildPartial();
    }

    public static int k(int i2, byte[] bArr, int i3, int i4) {
        for (int i5 = i3; i5 < i3 + i4; i5++) {
            i2 = (i2 * 31) + bArr[i5];
        }
        return i2;
    }

    public static String l(byte[] bArr) {
        return new String(bArr, a);
    }
}
