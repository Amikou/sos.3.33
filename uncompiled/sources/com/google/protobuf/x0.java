package com.google.protobuf;

import com.google.protobuf.a;
import com.google.protobuf.a.AbstractC0151a;
import com.google.protobuf.o0;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/* compiled from: RepeatedFieldBuilderV3.java */
/* loaded from: classes2.dex */
public class x0<MType extends com.google.protobuf.a, BType extends a.AbstractC0151a, IType extends o0> implements a.b {
    public a.b a;
    public List<MType> b;
    public boolean c;
    public List<a1<MType, BType, IType>> d;
    public boolean e;
    public b<MType, BType, IType> f;
    public a<MType, BType, IType> g;
    public c<MType, BType, IType> h;

    /* compiled from: RepeatedFieldBuilderV3.java */
    /* loaded from: classes2.dex */
    public static class a<MType extends com.google.protobuf.a, BType extends a.AbstractC0151a, IType extends o0> extends AbstractList<BType> {
        public x0<MType, BType, IType> a;

        public a(x0<MType, BType, IType> x0Var) {
            this.a = x0Var;
        }

        @Override // java.util.AbstractList, java.util.List
        /* renamed from: e */
        public BType get(int i) {
            return this.a.l(i);
        }

        public void i() {
            ((AbstractList) this).modCount++;
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
        public int size() {
            return this.a.n();
        }
    }

    /* compiled from: RepeatedFieldBuilderV3.java */
    /* loaded from: classes2.dex */
    public static class b<MType extends com.google.protobuf.a, BType extends a.AbstractC0151a, IType extends o0> extends AbstractList<MType> {
        public x0<MType, BType, IType> a;

        public b(x0<MType, BType, IType> x0Var) {
            this.a = x0Var;
        }

        @Override // java.util.AbstractList, java.util.List
        /* renamed from: e */
        public MType get(int i) {
            return this.a.o(i);
        }

        public void i() {
            ((AbstractList) this).modCount++;
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
        public int size() {
            return this.a.n();
        }
    }

    /* compiled from: RepeatedFieldBuilderV3.java */
    /* loaded from: classes2.dex */
    public static class c<MType extends com.google.protobuf.a, BType extends a.AbstractC0151a, IType extends o0> extends AbstractList<IType> {
        public x0<MType, BType, IType> a;

        public c(x0<MType, BType, IType> x0Var) {
            this.a = x0Var;
        }

        @Override // java.util.AbstractList, java.util.List
        /* renamed from: e */
        public IType get(int i) {
            return this.a.r(i);
        }

        public void i() {
            ((AbstractList) this).modCount++;
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
        public int size() {
            return this.a.n();
        }
    }

    public x0(List<MType> list, boolean z, a.b bVar, boolean z2) {
        this.b = list;
        this.c = z;
        this.a = bVar;
        this.e = z2;
    }

    @Override // com.google.protobuf.a.b
    public void a() {
        v();
    }

    public x0<MType, BType, IType> b(Iterable<? extends MType> iterable) {
        for (MType mtype : iterable) {
            a0.a(mtype);
        }
        int i = -1;
        if (iterable instanceof Collection) {
            Collection collection = (Collection) iterable;
            if (collection.isEmpty()) {
                return this;
            }
            i = collection.size();
        }
        k();
        if (i >= 0) {
            List<MType> list = this.b;
            if (list instanceof ArrayList) {
                ((ArrayList) list).ensureCapacity(list.size() + i);
            }
        }
        for (MType mtype2 : iterable) {
            f(mtype2);
        }
        v();
        t();
        return this;
    }

    public BType c(int i, MType mtype) {
        k();
        j();
        a1<MType, BType, IType> a1Var = new a1<>(mtype, this, this.e);
        this.b.add(i, null);
        this.d.add(i, a1Var);
        v();
        t();
        return a1Var.e();
    }

    public BType d(MType mtype) {
        k();
        j();
        a1<MType, BType, IType> a1Var = new a1<>(mtype, this, this.e);
        this.b.add(null);
        this.d.add(a1Var);
        v();
        t();
        return a1Var.e();
    }

    public x0<MType, BType, IType> e(int i, MType mtype) {
        a0.a(mtype);
        k();
        this.b.add(i, mtype);
        List<a1<MType, BType, IType>> list = this.d;
        if (list != null) {
            list.add(i, null);
        }
        v();
        t();
        return this;
    }

    public x0<MType, BType, IType> f(MType mtype) {
        a0.a(mtype);
        k();
        this.b.add(mtype);
        List<a1<MType, BType, IType>> list = this.d;
        if (list != null) {
            list.add(null);
        }
        v();
        t();
        return this;
    }

    public List<MType> g() {
        boolean z;
        this.e = true;
        boolean z2 = this.c;
        if (!z2 && this.d == null) {
            return this.b;
        }
        if (!z2) {
            int i = 0;
            while (true) {
                if (i >= this.b.size()) {
                    z = true;
                    break;
                }
                MType mtype = this.b.get(i);
                a1<MType, BType, IType> a1Var = this.d.get(i);
                if (a1Var != null && a1Var.b() != mtype) {
                    z = false;
                    break;
                }
                i++;
            }
            if (z) {
                return this.b;
            }
        }
        k();
        for (int i2 = 0; i2 < this.b.size(); i2++) {
            this.b.set(i2, p(i2, true));
        }
        List<MType> unmodifiableList = Collections.unmodifiableList(this.b);
        this.b = unmodifiableList;
        this.c = false;
        return unmodifiableList;
    }

    public void h() {
        this.b = Collections.emptyList();
        this.c = false;
        List<a1<MType, BType, IType>> list = this.d;
        if (list != null) {
            for (a1<MType, BType, IType> a1Var : list) {
                if (a1Var != null) {
                    a1Var.d();
                }
            }
            this.d = null;
        }
        v();
        t();
    }

    public void i() {
        this.a = null;
    }

    public final void j() {
        if (this.d == null) {
            this.d = new ArrayList(this.b.size());
            for (int i = 0; i < this.b.size(); i++) {
                this.d.add(null);
            }
        }
    }

    public final void k() {
        if (this.c) {
            return;
        }
        this.b = new ArrayList(this.b);
        this.c = true;
    }

    public BType l(int i) {
        j();
        a1<MType, BType, IType> a1Var = this.d.get(i);
        if (a1Var == null) {
            a1<MType, BType, IType> a1Var2 = new a1<>(this.b.get(i), this, this.e);
            this.d.set(i, a1Var2);
            a1Var = a1Var2;
        }
        return a1Var.e();
    }

    public List<BType> m() {
        if (this.g == null) {
            this.g = new a<>(this);
        }
        return this.g;
    }

    public int n() {
        return this.b.size();
    }

    public MType o(int i) {
        return p(i, false);
    }

    public final MType p(int i, boolean z) {
        List<a1<MType, BType, IType>> list = this.d;
        if (list == null) {
            return this.b.get(i);
        }
        a1<MType, BType, IType> a1Var = list.get(i);
        if (a1Var == null) {
            return this.b.get(i);
        }
        return z ? a1Var.b() : a1Var.f();
    }

    public List<MType> q() {
        if (this.f == null) {
            this.f = new b<>(this);
        }
        return this.f;
    }

    /* JADX WARN: Type inference failed for: r2v3, types: [com.google.protobuf.o0, IType extends com.google.protobuf.o0] */
    /* JADX WARN: Type inference failed for: r2v5, types: [com.google.protobuf.o0, IType extends com.google.protobuf.o0] */
    public IType r(int i) {
        List<a1<MType, BType, IType>> list = this.d;
        if (list == null) {
            return this.b.get(i);
        }
        a1<MType, BType, IType> a1Var = list.get(i);
        if (a1Var == null) {
            return this.b.get(i);
        }
        return a1Var.g();
    }

    public List<IType> s() {
        if (this.h == null) {
            this.h = new c<>(this);
        }
        return this.h;
    }

    public final void t() {
        b<MType, BType, IType> bVar = this.f;
        if (bVar != null) {
            bVar.i();
        }
        a<MType, BType, IType> aVar = this.g;
        if (aVar != null) {
            aVar.i();
        }
        c<MType, BType, IType> cVar = this.h;
        if (cVar != null) {
            cVar.i();
        }
    }

    public boolean u() {
        return this.b.isEmpty();
    }

    public final void v() {
        a.b bVar;
        if (!this.e || (bVar = this.a) == null) {
            return;
        }
        bVar.a();
        this.e = false;
    }

    public void w(int i) {
        a1<MType, BType, IType> remove;
        k();
        this.b.remove(i);
        List<a1<MType, BType, IType>> list = this.d;
        if (list != null && (remove = list.remove(i)) != null) {
            remove.d();
        }
        v();
        t();
    }

    public x0<MType, BType, IType> x(int i, MType mtype) {
        a1<MType, BType, IType> a1Var;
        a0.a(mtype);
        k();
        this.b.set(i, mtype);
        List<a1<MType, BType, IType>> list = this.d;
        if (list != null && (a1Var = list.set(i, null)) != null) {
            a1Var.d();
        }
        v();
        t();
        return this;
    }
}
