package com.google.protobuf;

import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.WireFormat;
import com.google.protobuf.a0;
import java.io.IOException;
import java.util.Objects;

/* compiled from: ArrayDecoders.java */
/* loaded from: classes2.dex */
public final class e {

    /* compiled from: ArrayDecoders.java */
    /* loaded from: classes2.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[WireFormat.FieldType.values().length];
            a = iArr;
            try {
                iArr[WireFormat.FieldType.DOUBLE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[WireFormat.FieldType.FLOAT.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[WireFormat.FieldType.INT64.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                a[WireFormat.FieldType.UINT64.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                a[WireFormat.FieldType.INT32.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                a[WireFormat.FieldType.UINT32.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                a[WireFormat.FieldType.FIXED64.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                a[WireFormat.FieldType.SFIXED64.ordinal()] = 8;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                a[WireFormat.FieldType.FIXED32.ordinal()] = 9;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                a[WireFormat.FieldType.SFIXED32.ordinal()] = 10;
            } catch (NoSuchFieldError unused10) {
            }
            try {
                a[WireFormat.FieldType.BOOL.ordinal()] = 11;
            } catch (NoSuchFieldError unused11) {
            }
            try {
                a[WireFormat.FieldType.SINT32.ordinal()] = 12;
            } catch (NoSuchFieldError unused12) {
            }
            try {
                a[WireFormat.FieldType.SINT64.ordinal()] = 13;
            } catch (NoSuchFieldError unused13) {
            }
            try {
                a[WireFormat.FieldType.ENUM.ordinal()] = 14;
            } catch (NoSuchFieldError unused14) {
            }
            try {
                a[WireFormat.FieldType.BYTES.ordinal()] = 15;
            } catch (NoSuchFieldError unused15) {
            }
            try {
                a[WireFormat.FieldType.STRING.ordinal()] = 16;
            } catch (NoSuchFieldError unused16) {
            }
            try {
                a[WireFormat.FieldType.GROUP.ordinal()] = 17;
            } catch (NoSuchFieldError unused17) {
            }
            try {
                a[WireFormat.FieldType.MESSAGE.ordinal()] = 18;
            } catch (NoSuchFieldError unused18) {
            }
        }
    }

    /* compiled from: ArrayDecoders.java */
    /* loaded from: classes2.dex */
    public static final class b {
        public int a;
        public long b;
        public Object c;
        public final r d;

        public b(r rVar) {
            Objects.requireNonNull(rVar);
            this.d = rVar;
        }
    }

    public static int A(int i, byte[] bArr, int i2, int i3, a0.i<?> iVar, b bVar) {
        z zVar = (z) iVar;
        int I = I(bArr, i2, bVar);
        zVar.X(j.b(bVar.a));
        while (I < i3) {
            int I2 = I(bArr, I, bVar);
            if (i != bVar.a) {
                break;
            }
            I = I(bArr, I2, bVar);
            zVar.X(j.b(bVar.a));
        }
        return I;
    }

    public static int B(int i, byte[] bArr, int i2, int i3, a0.i<?> iVar, b bVar) {
        f0 f0Var = (f0) iVar;
        int L = L(bArr, i2, bVar);
        f0Var.n(j.c(bVar.b));
        while (L < i3) {
            int I = I(bArr, L, bVar);
            if (i != bVar.a) {
                break;
            }
            L = L(bArr, I, bVar);
            f0Var.n(j.c(bVar.b));
        }
        return L;
    }

    public static int C(byte[] bArr, int i, b bVar) throws InvalidProtocolBufferException {
        int I = I(bArr, i, bVar);
        int i2 = bVar.a;
        if (i2 >= 0) {
            if (i2 == 0) {
                bVar.c = "";
                return I;
            }
            bVar.c = new String(bArr, I, i2, a0.a);
            return I + i2;
        }
        throw InvalidProtocolBufferException.negativeSize();
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x001d  */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:8:0x001a -> B:9:0x001b). Please submit an issue!!! */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static int D(int r4, byte[] r5, int r6, int r7, com.google.protobuf.a0.i<?> r8, com.google.protobuf.e.b r9) throws com.google.protobuf.InvalidProtocolBufferException {
        /*
            int r6 = I(r5, r6, r9)
            int r0 = r9.a
            if (r0 < 0) goto L45
            java.lang.String r1 = ""
            if (r0 != 0) goto L10
            r8.add(r1)
            goto L1b
        L10:
            java.lang.String r2 = new java.lang.String
            java.nio.charset.Charset r3 = com.google.protobuf.a0.a
            r2.<init>(r5, r6, r0, r3)
            r8.add(r2)
        L1a:
            int r6 = r6 + r0
        L1b:
            if (r6 >= r7) goto L44
            int r0 = I(r5, r6, r9)
            int r2 = r9.a
            if (r4 == r2) goto L26
            goto L44
        L26:
            int r6 = I(r5, r0, r9)
            int r0 = r9.a
            if (r0 < 0) goto L3f
            if (r0 != 0) goto L34
            r8.add(r1)
            goto L1b
        L34:
            java.lang.String r2 = new java.lang.String
            java.nio.charset.Charset r3 = com.google.protobuf.a0.a
            r2.<init>(r5, r6, r0, r3)
            r8.add(r2)
            goto L1a
        L3f:
            com.google.protobuf.InvalidProtocolBufferException r4 = com.google.protobuf.InvalidProtocolBufferException.negativeSize()
            throw r4
        L44:
            return r6
        L45:
            com.google.protobuf.InvalidProtocolBufferException r4 = com.google.protobuf.InvalidProtocolBufferException.negativeSize()
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.e.D(int, byte[], int, int, com.google.protobuf.a0$i, com.google.protobuf.e$b):int");
    }

    /* JADX WARN: Removed duplicated region for block: B:12:0x0025  */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:10:0x0022 -> B:11:0x0023). Please submit an issue!!! */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static int E(int r5, byte[] r6, int r7, int r8, com.google.protobuf.a0.i<?> r9, com.google.protobuf.e.b r10) throws com.google.protobuf.InvalidProtocolBufferException {
        /*
            int r7 = I(r6, r7, r10)
            int r0 = r10.a
            if (r0 < 0) goto L5f
            java.lang.String r1 = ""
            if (r0 != 0) goto L10
            r9.add(r1)
            goto L23
        L10:
            int r2 = r7 + r0
            boolean r3 = com.google.protobuf.Utf8.t(r6, r7, r2)
            if (r3 == 0) goto L5a
            java.lang.String r3 = new java.lang.String
            java.nio.charset.Charset r4 = com.google.protobuf.a0.a
            r3.<init>(r6, r7, r0, r4)
            r9.add(r3)
        L22:
            r7 = r2
        L23:
            if (r7 >= r8) goto L59
            int r0 = I(r6, r7, r10)
            int r2 = r10.a
            if (r5 == r2) goto L2e
            goto L59
        L2e:
            int r7 = I(r6, r0, r10)
            int r0 = r10.a
            if (r0 < 0) goto L54
            if (r0 != 0) goto L3c
            r9.add(r1)
            goto L23
        L3c:
            int r2 = r7 + r0
            boolean r3 = com.google.protobuf.Utf8.t(r6, r7, r2)
            if (r3 == 0) goto L4f
            java.lang.String r3 = new java.lang.String
            java.nio.charset.Charset r4 = com.google.protobuf.a0.a
            r3.<init>(r6, r7, r0, r4)
            r9.add(r3)
            goto L22
        L4f:
            com.google.protobuf.InvalidProtocolBufferException r5 = com.google.protobuf.InvalidProtocolBufferException.invalidUtf8()
            throw r5
        L54:
            com.google.protobuf.InvalidProtocolBufferException r5 = com.google.protobuf.InvalidProtocolBufferException.negativeSize()
            throw r5
        L59:
            return r7
        L5a:
            com.google.protobuf.InvalidProtocolBufferException r5 = com.google.protobuf.InvalidProtocolBufferException.invalidUtf8()
            throw r5
        L5f:
            com.google.protobuf.InvalidProtocolBufferException r5 = com.google.protobuf.InvalidProtocolBufferException.negativeSize()
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.e.E(int, byte[], int, int, com.google.protobuf.a0$i, com.google.protobuf.e$b):int");
    }

    public static int F(byte[] bArr, int i, b bVar) throws InvalidProtocolBufferException {
        int I = I(bArr, i, bVar);
        int i2 = bVar.a;
        if (i2 >= 0) {
            if (i2 == 0) {
                bVar.c = "";
                return I;
            }
            bVar.c = Utf8.h(bArr, I, i2);
            return I + i2;
        }
        throw InvalidProtocolBufferException.negativeSize();
    }

    public static int G(int i, byte[] bArr, int i2, int i3, h1 h1Var, b bVar) throws InvalidProtocolBufferException {
        if (WireFormat.a(i) != 0) {
            int b2 = WireFormat.b(i);
            if (b2 == 0) {
                int L = L(bArr, i2, bVar);
                h1Var.m(i, Long.valueOf(bVar.b));
                return L;
            } else if (b2 == 1) {
                h1Var.m(i, Long.valueOf(j(bArr, i2)));
                return i2 + 8;
            } else if (b2 == 2) {
                int I = I(bArr, i2, bVar);
                int i4 = bVar.a;
                if (i4 >= 0) {
                    if (i4 <= bArr.length - I) {
                        if (i4 == 0) {
                            h1Var.m(i, ByteString.EMPTY);
                        } else {
                            h1Var.m(i, ByteString.copyFrom(bArr, I, i4));
                        }
                        return I + i4;
                    }
                    throw InvalidProtocolBufferException.truncatedMessage();
                }
                throw InvalidProtocolBufferException.negativeSize();
            } else if (b2 != 3) {
                if (b2 == 5) {
                    h1Var.m(i, Integer.valueOf(h(bArr, i2)));
                    return i2 + 4;
                }
                throw InvalidProtocolBufferException.invalidTag();
            } else {
                h1 j = h1.j();
                int i5 = (i & (-8)) | 4;
                int i6 = 0;
                while (true) {
                    if (i2 >= i3) {
                        break;
                    }
                    int I2 = I(bArr, i2, bVar);
                    int i7 = bVar.a;
                    if (i7 == i5) {
                        i6 = i7;
                        i2 = I2;
                        break;
                    }
                    i6 = i7;
                    i2 = G(i7, bArr, I2, i3, j, bVar);
                }
                if (i2 <= i3 && i6 == i5) {
                    h1Var.m(i, j);
                    return i2;
                }
                throw InvalidProtocolBufferException.parseFailure();
            }
        }
        throw InvalidProtocolBufferException.invalidTag();
    }

    public static int H(int i, byte[] bArr, int i2, b bVar) {
        int i3 = i & 127;
        int i4 = i2 + 1;
        byte b2 = bArr[i2];
        if (b2 >= 0) {
            bVar.a = i3 | (b2 << 7);
            return i4;
        }
        int i5 = i3 | ((b2 & Byte.MAX_VALUE) << 7);
        int i6 = i4 + 1;
        byte b3 = bArr[i4];
        if (b3 >= 0) {
            bVar.a = i5 | (b3 << 14);
            return i6;
        }
        int i7 = i5 | ((b3 & Byte.MAX_VALUE) << 14);
        int i8 = i6 + 1;
        byte b4 = bArr[i6];
        if (b4 >= 0) {
            bVar.a = i7 | (b4 << 21);
            return i8;
        }
        int i9 = i7 | ((b4 & Byte.MAX_VALUE) << 21);
        int i10 = i8 + 1;
        byte b5 = bArr[i8];
        if (b5 >= 0) {
            bVar.a = i9 | (b5 << 28);
            return i10;
        }
        int i11 = i9 | ((b5 & Byte.MAX_VALUE) << 28);
        while (true) {
            int i12 = i10 + 1;
            if (bArr[i10] >= 0) {
                bVar.a = i11;
                return i12;
            }
            i10 = i12;
        }
    }

    public static int I(byte[] bArr, int i, b bVar) {
        int i2 = i + 1;
        byte b2 = bArr[i];
        if (b2 >= 0) {
            bVar.a = b2;
            return i2;
        }
        return H(b2, bArr, i2, bVar);
    }

    public static int J(int i, byte[] bArr, int i2, int i3, a0.i<?> iVar, b bVar) {
        z zVar = (z) iVar;
        int I = I(bArr, i2, bVar);
        zVar.X(bVar.a);
        while (I < i3) {
            int I2 = I(bArr, I, bVar);
            if (i != bVar.a) {
                break;
            }
            I = I(bArr, I2, bVar);
            zVar.X(bVar.a);
        }
        return I;
    }

    public static int K(long j, byte[] bArr, int i, b bVar) {
        int i2 = i + 1;
        byte b2 = bArr[i];
        long j2 = (j & 127) | ((b2 & Byte.MAX_VALUE) << 7);
        int i3 = 7;
        while (b2 < 0) {
            int i4 = i2 + 1;
            byte b3 = bArr[i2];
            i3 += 7;
            j2 |= (b3 & Byte.MAX_VALUE) << i3;
            i2 = i4;
            b2 = b3;
        }
        bVar.b = j2;
        return i2;
    }

    public static int L(byte[] bArr, int i, b bVar) {
        int i2 = i + 1;
        long j = bArr[i];
        if (j >= 0) {
            bVar.b = j;
            return i2;
        }
        return K(j, bArr, i2, bVar);
    }

    public static int M(int i, byte[] bArr, int i2, int i3, a0.i<?> iVar, b bVar) {
        f0 f0Var = (f0) iVar;
        int L = L(bArr, i2, bVar);
        f0Var.n(bVar.b);
        while (L < i3) {
            int I = I(bArr, L, bVar);
            if (i != bVar.a) {
                break;
            }
            L = L(bArr, I, bVar);
            f0Var.n(bVar.b);
        }
        return L;
    }

    public static int N(int i, byte[] bArr, int i2, int i3, b bVar) throws InvalidProtocolBufferException {
        if (WireFormat.a(i) != 0) {
            int b2 = WireFormat.b(i);
            if (b2 != 0) {
                if (b2 != 1) {
                    if (b2 != 2) {
                        if (b2 != 3) {
                            if (b2 == 5) {
                                return i2 + 4;
                            }
                            throw InvalidProtocolBufferException.invalidTag();
                        }
                        int i4 = (i & (-8)) | 4;
                        int i5 = 0;
                        while (i2 < i3) {
                            i2 = I(bArr, i2, bVar);
                            i5 = bVar.a;
                            if (i5 == i4) {
                                break;
                            }
                            i2 = N(i5, bArr, i2, i3, bVar);
                        }
                        if (i2 > i3 || i5 != i4) {
                            throw InvalidProtocolBufferException.parseFailure();
                        }
                        return i2;
                    }
                    return I(bArr, i2, bVar) + bVar.a;
                }
                return i2 + 8;
            }
            return L(bArr, i2, bVar);
        }
        throw InvalidProtocolBufferException.invalidTag();
    }

    public static int a(int i, byte[] bArr, int i2, int i3, a0.i<?> iVar, b bVar) {
        g gVar = (g) iVar;
        int L = L(bArr, i2, bVar);
        gVar.n(bVar.b != 0);
        while (L < i3) {
            int I = I(bArr, L, bVar);
            if (i != bVar.a) {
                break;
            }
            L = L(bArr, I, bVar);
            gVar.n(bVar.b != 0);
        }
        return L;
    }

    public static int b(byte[] bArr, int i, b bVar) throws InvalidProtocolBufferException {
        int I = I(bArr, i, bVar);
        int i2 = bVar.a;
        if (i2 >= 0) {
            if (i2 <= bArr.length - I) {
                if (i2 == 0) {
                    bVar.c = ByteString.EMPTY;
                    return I;
                }
                bVar.c = ByteString.copyFrom(bArr, I, i2);
                return I + i2;
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }
        throw InvalidProtocolBufferException.negativeSize();
    }

    /* JADX WARN: Removed duplicated region for block: B:11:0x001e  */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:9:0x001b -> B:10:0x001c). Please submit an issue!!! */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static int c(int r2, byte[] r3, int r4, int r5, com.google.protobuf.a0.i<?> r6, com.google.protobuf.e.b r7) throws com.google.protobuf.InvalidProtocolBufferException {
        /*
            int r4 = I(r3, r4, r7)
            int r0 = r7.a
            if (r0 < 0) goto L53
            int r1 = r3.length
            int r1 = r1 - r4
            if (r0 > r1) goto L4e
            if (r0 != 0) goto L14
            com.google.protobuf.ByteString r0 = com.google.protobuf.ByteString.EMPTY
            r6.add(r0)
            goto L1c
        L14:
            com.google.protobuf.ByteString r1 = com.google.protobuf.ByteString.copyFrom(r3, r4, r0)
            r6.add(r1)
        L1b:
            int r4 = r4 + r0
        L1c:
            if (r4 >= r5) goto L4d
            int r0 = I(r3, r4, r7)
            int r1 = r7.a
            if (r2 == r1) goto L27
            goto L4d
        L27:
            int r4 = I(r3, r0, r7)
            int r0 = r7.a
            if (r0 < 0) goto L48
            int r1 = r3.length
            int r1 = r1 - r4
            if (r0 > r1) goto L43
            if (r0 != 0) goto L3b
            com.google.protobuf.ByteString r0 = com.google.protobuf.ByteString.EMPTY
            r6.add(r0)
            goto L1c
        L3b:
            com.google.protobuf.ByteString r1 = com.google.protobuf.ByteString.copyFrom(r3, r4, r0)
            r6.add(r1)
            goto L1b
        L43:
            com.google.protobuf.InvalidProtocolBufferException r2 = com.google.protobuf.InvalidProtocolBufferException.truncatedMessage()
            throw r2
        L48:
            com.google.protobuf.InvalidProtocolBufferException r2 = com.google.protobuf.InvalidProtocolBufferException.negativeSize()
            throw r2
        L4d:
            return r4
        L4e:
            com.google.protobuf.InvalidProtocolBufferException r2 = com.google.protobuf.InvalidProtocolBufferException.truncatedMessage()
            throw r2
        L53:
            com.google.protobuf.InvalidProtocolBufferException r2 = com.google.protobuf.InvalidProtocolBufferException.negativeSize()
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.e.c(int, byte[], int, int, com.google.protobuf.a0$i, com.google.protobuf.e$b):int");
    }

    public static double d(byte[] bArr, int i) {
        return Double.longBitsToDouble(j(bArr, i));
    }

    public static int e(int i, byte[] bArr, int i2, int i3, a0.i<?> iVar, b bVar) {
        n nVar = (n) iVar;
        nVar.m(d(bArr, i2));
        int i4 = i2 + 8;
        while (i4 < i3) {
            int I = I(bArr, i4, bVar);
            if (i != bVar.a) {
                break;
            }
            nVar.m(d(bArr, I));
            i4 = I + 8;
        }
        return i4;
    }

    public static int f(int i, byte[] bArr, int i2, int i3, GeneratedMessageLite.b<?, ?> bVar, GeneratedMessageLite.d<?, ?> dVar, f1<h1, h1> f1Var, b bVar2) throws IOException {
        Object t;
        w<GeneratedMessageLite.c> wVar = bVar.h0;
        int i4 = i >>> 3;
        if (dVar.b.i() && dVar.b.isPacked()) {
            switch (a.a[dVar.b().ordinal()]) {
                case 1:
                    n nVar = new n();
                    int s = s(bArr, i2, nVar, bVar2);
                    wVar.N(dVar.b, nVar);
                    return s;
                case 2:
                    x xVar = new x();
                    int v = v(bArr, i2, xVar, bVar2);
                    wVar.N(dVar.b, xVar);
                    return v;
                case 3:
                case 4:
                    f0 f0Var = new f0();
                    int z = z(bArr, i2, f0Var, bVar2);
                    wVar.N(dVar.b, f0Var);
                    return z;
                case 5:
                case 6:
                    z zVar = new z();
                    int y = y(bArr, i2, zVar, bVar2);
                    wVar.N(dVar.b, zVar);
                    return y;
                case 7:
                case 8:
                    f0 f0Var2 = new f0();
                    int u = u(bArr, i2, f0Var2, bVar2);
                    wVar.N(dVar.b, f0Var2);
                    return u;
                case 9:
                case 10:
                    z zVar2 = new z();
                    int t2 = t(bArr, i2, zVar2, bVar2);
                    wVar.N(dVar.b, zVar2);
                    return t2;
                case 11:
                    g gVar = new g();
                    int r = r(bArr, i2, gVar, bVar2);
                    wVar.N(dVar.b, gVar);
                    return r;
                case 12:
                    z zVar3 = new z();
                    int w = w(bArr, i2, zVar3, bVar2);
                    wVar.N(dVar.b, zVar3);
                    return w;
                case 13:
                    f0 f0Var3 = new f0();
                    int x = x(bArr, i2, f0Var3, bVar2);
                    wVar.N(dVar.b, f0Var3);
                    return x;
                case 14:
                    z zVar4 = new z();
                    int y2 = y(bArr, i2, zVar4, bVar2);
                    h1 h1Var = bVar.a;
                    h1 h1Var2 = (h1) z0.z(i4, zVar4, dVar.b.d(), h1Var != h1.c() ? h1Var : null, f1Var);
                    if (h1Var2 != null) {
                        bVar.a = h1Var2;
                    }
                    wVar.N(dVar.b, zVar4);
                    return y2;
                default:
                    throw new IllegalStateException("Type cannot be packed: " + dVar.b.n());
            }
        }
        if (dVar.b() == WireFormat.FieldType.ENUM) {
            i2 = I(bArr, i2, bVar2);
            if (dVar.b.d().findValueByNumber(bVar2.a) == null) {
                h1 h1Var3 = bVar.a;
                if (h1Var3 == h1.c()) {
                    h1Var3 = h1.j();
                    bVar.a = h1Var3;
                }
                z0.M(i4, bVar2.a, h1Var3, f1Var);
                return i2;
            }
            r2 = Integer.valueOf(bVar2.a);
        } else {
            switch (a.a[dVar.b().ordinal()]) {
                case 1:
                    r2 = Double.valueOf(d(bArr, i2));
                    i2 += 8;
                    break;
                case 2:
                    r2 = Float.valueOf(l(bArr, i2));
                    i2 += 4;
                    break;
                case 3:
                case 4:
                    i2 = L(bArr, i2, bVar2);
                    r2 = Long.valueOf(bVar2.b);
                    break;
                case 5:
                case 6:
                    i2 = I(bArr, i2, bVar2);
                    r2 = Integer.valueOf(bVar2.a);
                    break;
                case 7:
                case 8:
                    r2 = Long.valueOf(j(bArr, i2));
                    i2 += 8;
                    break;
                case 9:
                case 10:
                    r2 = Integer.valueOf(h(bArr, i2));
                    i2 += 4;
                    break;
                case 11:
                    i2 = L(bArr, i2, bVar2);
                    r2 = Boolean.valueOf(bVar2.b != 0);
                    break;
                case 12:
                    i2 = I(bArr, i2, bVar2);
                    r2 = Integer.valueOf(j.b(bVar2.a));
                    break;
                case 13:
                    i2 = L(bArr, i2, bVar2);
                    r2 = Long.valueOf(j.c(bVar2.b));
                    break;
                case 14:
                    throw new IllegalStateException("Shouldn't reach here.");
                case 15:
                    i2 = b(bArr, i2, bVar2);
                    r2 = bVar2.c;
                    break;
                case 16:
                    i2 = C(bArr, i2, bVar2);
                    r2 = bVar2.c;
                    break;
                case 17:
                    i2 = n(u0.a().d(dVar.c().getClass()), bArr, i2, i3, (i4 << 3) | 4, bVar2);
                    r2 = bVar2.c;
                    break;
                case 18:
                    i2 = p(u0.a().d(dVar.c().getClass()), bArr, i2, i3, bVar2);
                    r2 = bVar2.c;
                    break;
            }
        }
        if (dVar.e()) {
            wVar.g(dVar.b, r2);
        } else {
            int i5 = a.a[dVar.b().ordinal()];
            if ((i5 == 17 || i5 == 18) && (t = wVar.t(dVar.b)) != null) {
                r2 = a0.j(t, r2);
            }
            wVar.N(dVar.b, r2);
        }
        return i2;
    }

    public static int g(int i, byte[] bArr, int i2, int i3, Object obj, m0 m0Var, f1<h1, h1> f1Var, b bVar) throws IOException {
        GeneratedMessageLite.d a2 = bVar.d.a(m0Var, i >>> 3);
        if (a2 == null) {
            return G(i, bArr, i2, i3, p0.w(obj), bVar);
        }
        GeneratedMessageLite.b bVar2 = (GeneratedMessageLite.b) obj;
        bVar2.k();
        return f(i, bArr, i2, i3, bVar2, a2, f1Var, bVar);
    }

    public static int h(byte[] bArr, int i) {
        return ((bArr[i + 3] & 255) << 24) | (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << 16);
    }

    public static int i(int i, byte[] bArr, int i2, int i3, a0.i<?> iVar, b bVar) {
        z zVar = (z) iVar;
        zVar.X(h(bArr, i2));
        int i4 = i2 + 4;
        while (i4 < i3) {
            int I = I(bArr, i4, bVar);
            if (i != bVar.a) {
                break;
            }
            zVar.X(h(bArr, I));
            i4 = I + 4;
        }
        return i4;
    }

    public static long j(byte[] bArr, int i) {
        return ((bArr[i + 7] & 255) << 56) | (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << 16) | ((bArr[i + 3] & 255) << 24) | ((bArr[i + 4] & 255) << 32) | ((bArr[i + 5] & 255) << 40) | ((bArr[i + 6] & 255) << 48);
    }

    public static int k(int i, byte[] bArr, int i2, int i3, a0.i<?> iVar, b bVar) {
        f0 f0Var = (f0) iVar;
        f0Var.n(j(bArr, i2));
        int i4 = i2 + 8;
        while (i4 < i3) {
            int I = I(bArr, i4, bVar);
            if (i != bVar.a) {
                break;
            }
            f0Var.n(j(bArr, I));
            i4 = I + 8;
        }
        return i4;
    }

    public static float l(byte[] bArr, int i) {
        return Float.intBitsToFloat(h(bArr, i));
    }

    public static int m(int i, byte[] bArr, int i2, int i3, a0.i<?> iVar, b bVar) {
        x xVar = (x) iVar;
        xVar.m(l(bArr, i2));
        int i4 = i2 + 4;
        while (i4 < i3) {
            int I = I(bArr, i4, bVar);
            if (i != bVar.a) {
                break;
            }
            xVar.m(l(bArr, I));
            i4 = I + 4;
        }
        return i4;
    }

    public static int n(y0 y0Var, byte[] bArr, int i, int i2, int i3, b bVar) throws IOException {
        p0 p0Var = (p0) y0Var;
        Object c = p0Var.c();
        int d0 = p0Var.d0(c, bArr, i, i2, i3, bVar);
        p0Var.e(c);
        bVar.c = c;
        return d0;
    }

    public static int o(y0 y0Var, int i, byte[] bArr, int i2, int i3, a0.i<?> iVar, b bVar) throws IOException {
        int i4 = (i & (-8)) | 4;
        int n = n(y0Var, bArr, i2, i3, i4, bVar);
        iVar.add(bVar.c);
        while (n < i3) {
            int I = I(bArr, n, bVar);
            if (i != bVar.a) {
                break;
            }
            n = n(y0Var, bArr, I, i3, i4, bVar);
            iVar.add(bVar.c);
        }
        return n;
    }

    public static int p(y0 y0Var, byte[] bArr, int i, int i2, b bVar) throws IOException {
        int i3 = i + 1;
        int i4 = bArr[i];
        if (i4 < 0) {
            i3 = H(i4, bArr, i3, bVar);
            i4 = bVar.a;
        }
        int i5 = i3;
        if (i4 >= 0 && i4 <= i2 - i5) {
            Object c = y0Var.c();
            int i6 = i4 + i5;
            y0Var.j(c, bArr, i5, i6, bVar);
            y0Var.e(c);
            bVar.c = c;
            return i6;
        }
        throw InvalidProtocolBufferException.truncatedMessage();
    }

    public static int q(y0<?> y0Var, int i, byte[] bArr, int i2, int i3, a0.i<?> iVar, b bVar) throws IOException {
        int p = p(y0Var, bArr, i2, i3, bVar);
        iVar.add(bVar.c);
        while (p < i3) {
            int I = I(bArr, p, bVar);
            if (i != bVar.a) {
                break;
            }
            p = p(y0Var, bArr, I, i3, bVar);
            iVar.add(bVar.c);
        }
        return p;
    }

    public static int r(byte[] bArr, int i, a0.i<?> iVar, b bVar) throws IOException {
        g gVar = (g) iVar;
        int I = I(bArr, i, bVar);
        int i2 = bVar.a + I;
        while (I < i2) {
            I = L(bArr, I, bVar);
            gVar.n(bVar.b != 0);
        }
        if (I == i2) {
            return I;
        }
        throw InvalidProtocolBufferException.truncatedMessage();
    }

    public static int s(byte[] bArr, int i, a0.i<?> iVar, b bVar) throws IOException {
        n nVar = (n) iVar;
        int I = I(bArr, i, bVar);
        int i2 = bVar.a + I;
        while (I < i2) {
            nVar.m(d(bArr, I));
            I += 8;
        }
        if (I == i2) {
            return I;
        }
        throw InvalidProtocolBufferException.truncatedMessage();
    }

    public static int t(byte[] bArr, int i, a0.i<?> iVar, b bVar) throws IOException {
        z zVar = (z) iVar;
        int I = I(bArr, i, bVar);
        int i2 = bVar.a + I;
        while (I < i2) {
            zVar.X(h(bArr, I));
            I += 4;
        }
        if (I == i2) {
            return I;
        }
        throw InvalidProtocolBufferException.truncatedMessage();
    }

    public static int u(byte[] bArr, int i, a0.i<?> iVar, b bVar) throws IOException {
        f0 f0Var = (f0) iVar;
        int I = I(bArr, i, bVar);
        int i2 = bVar.a + I;
        while (I < i2) {
            f0Var.n(j(bArr, I));
            I += 8;
        }
        if (I == i2) {
            return I;
        }
        throw InvalidProtocolBufferException.truncatedMessage();
    }

    public static int v(byte[] bArr, int i, a0.i<?> iVar, b bVar) throws IOException {
        x xVar = (x) iVar;
        int I = I(bArr, i, bVar);
        int i2 = bVar.a + I;
        while (I < i2) {
            xVar.m(l(bArr, I));
            I += 4;
        }
        if (I == i2) {
            return I;
        }
        throw InvalidProtocolBufferException.truncatedMessage();
    }

    public static int w(byte[] bArr, int i, a0.i<?> iVar, b bVar) throws IOException {
        z zVar = (z) iVar;
        int I = I(bArr, i, bVar);
        int i2 = bVar.a + I;
        while (I < i2) {
            I = I(bArr, I, bVar);
            zVar.X(j.b(bVar.a));
        }
        if (I == i2) {
            return I;
        }
        throw InvalidProtocolBufferException.truncatedMessage();
    }

    public static int x(byte[] bArr, int i, a0.i<?> iVar, b bVar) throws IOException {
        f0 f0Var = (f0) iVar;
        int I = I(bArr, i, bVar);
        int i2 = bVar.a + I;
        while (I < i2) {
            I = L(bArr, I, bVar);
            f0Var.n(j.c(bVar.b));
        }
        if (I == i2) {
            return I;
        }
        throw InvalidProtocolBufferException.truncatedMessage();
    }

    public static int y(byte[] bArr, int i, a0.i<?> iVar, b bVar) throws IOException {
        z zVar = (z) iVar;
        int I = I(bArr, i, bVar);
        int i2 = bVar.a + I;
        while (I < i2) {
            I = I(bArr, I, bVar);
            zVar.X(bVar.a);
        }
        if (I == i2) {
            return I;
        }
        throw InvalidProtocolBufferException.truncatedMessage();
    }

    public static int z(byte[] bArr, int i, a0.i<?> iVar, b bVar) throws IOException {
        f0 f0Var = (f0) iVar;
        int I = I(bArr, i, bVar);
        int i2 = bVar.a + I;
        while (I < i2) {
            I = L(bArr, I, bVar);
            f0Var.n(bVar.b);
        }
        if (I == i2) {
            return I;
        }
        throw InvalidProtocolBufferException.truncatedMessage();
    }
}
