package com.google.protobuf;

import com.google.protobuf.a;
import com.google.protobuf.a.AbstractC0151a;
import com.google.protobuf.o0;

/* compiled from: SingleFieldBuilderV3.java */
/* loaded from: classes2.dex */
public class a1<MType extends a, BType extends a.AbstractC0151a, IType extends o0> implements a.b {
    public a.b a;
    public BType b;
    public MType c;
    public boolean d;

    public a1(MType mtype, a.b bVar, boolean z) {
        this.c = (MType) a0.a(mtype);
        this.a = bVar;
        this.d = z;
    }

    @Override // com.google.protobuf.a.b
    public void a() {
        i();
    }

    public MType b() {
        this.d = true;
        return f();
    }

    public a1<MType, BType, IType> c() {
        l0 defaultInstanceForType;
        MType mtype = this.c;
        if (mtype != null) {
            defaultInstanceForType = mtype.getDefaultInstanceForType();
        } else {
            defaultInstanceForType = this.b.getDefaultInstanceForType();
        }
        this.c = (MType) defaultInstanceForType;
        BType btype = this.b;
        if (btype != null) {
            btype.dispose();
            this.b = null;
        }
        i();
        return this;
    }

    public void d() {
        this.a = null;
    }

    public BType e() {
        if (this.b == null) {
            BType btype = (BType) this.c.newBuilderForType(this);
            this.b = btype;
            btype.mergeFrom(this.c);
            this.b.markClean();
        }
        return this.b;
    }

    public MType f() {
        if (this.c == null) {
            this.c = (MType) this.b.buildPartial();
        }
        return this.c;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [BType extends com.google.protobuf.a$a, IType extends com.google.protobuf.o0] */
    /* JADX WARN: Type inference failed for: r0v1, types: [MType extends com.google.protobuf.a, IType extends com.google.protobuf.o0] */
    public IType g() {
        BType btype = this.b;
        return btype != 0 ? btype : this.c;
    }

    public a1<MType, BType, IType> h(MType mtype) {
        if (this.b == null) {
            l0 l0Var = this.c;
            if (l0Var == l0Var.getDefaultInstanceForType()) {
                this.c = mtype;
                i();
                return this;
            }
        }
        e().mergeFrom(mtype);
        i();
        return this;
    }

    public final void i() {
        a.b bVar;
        if (this.b != null) {
            this.c = null;
        }
        if (!this.d || (bVar = this.a) == null) {
            return;
        }
        bVar.a();
        this.d = false;
    }

    public a1<MType, BType, IType> j(MType mtype) {
        this.c = (MType) a0.a(mtype);
        BType btype = this.b;
        if (btype != null) {
            btype.dispose();
            this.b = null;
        }
        i();
        return this;
    }
}
