package com.google.protobuf;

import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.GeneratedMessageLite.a;
import com.google.protobuf.WireFormat;
import com.google.protobuf.a0;
import com.google.protobuf.b;
import com.google.protobuf.e;
import com.google.protobuf.m0;
import com.google.protobuf.w;
import java.io.IOException;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* loaded from: classes2.dex */
public abstract class GeneratedMessageLite<MessageType extends GeneratedMessageLite<MessageType, BuilderType>, BuilderType extends a<MessageType, BuilderType>> extends com.google.protobuf.b<MessageType, BuilderType> {
    public static Map<Object, GeneratedMessageLite<?, ?>> g0 = new ConcurrentHashMap();
    public h1 a = h1.c();
    public int f0 = -1;

    /* loaded from: classes2.dex */
    public enum MethodToInvoke {
        GET_MEMOIZED_IS_INITIALIZED,
        SET_MEMOIZED_IS_INITIALIZED,
        BUILD_MESSAGE_INFO,
        NEW_MUTABLE_INSTANCE,
        NEW_BUILDER,
        GET_DEFAULT_INSTANCE,
        GET_PARSER
    }

    /* loaded from: classes2.dex */
    public static final class SerializedForm implements Serializable {
        private static final long serialVersionUID = 0;
        private final byte[] asBytes;
        private final Class<?> messageClass;
        private final String messageClassName;

        public SerializedForm(m0 m0Var) {
            Class<?> cls = m0Var.getClass();
            this.messageClass = cls;
            this.messageClassName = cls.getName();
            this.asBytes = m0Var.toByteArray();
        }

        public static SerializedForm of(m0 m0Var) {
            return new SerializedForm(m0Var);
        }

        @Deprecated
        public final Object a() throws ObjectStreamException {
            try {
                Field declaredField = b().getDeclaredField("defaultInstance");
                declaredField.setAccessible(true);
                return ((m0) declaredField.get(null)).newBuilderForType().mergeFrom(this.asBytes).buildPartial();
            } catch (InvalidProtocolBufferException e) {
                throw new RuntimeException("Unable to understand proto buffer", e);
            } catch (ClassNotFoundException e2) {
                throw new RuntimeException("Unable to find proto buffer class: " + this.messageClassName, e2);
            } catch (IllegalAccessException e3) {
                throw new RuntimeException("Unable to call parsePartialFrom", e3);
            } catch (NoSuchFieldException e4) {
                throw new RuntimeException("Unable to find defaultInstance in " + this.messageClassName, e4);
            } catch (SecurityException e5) {
                throw new RuntimeException("Unable to call defaultInstance in " + this.messageClassName, e5);
            }
        }

        public final Class<?> b() throws ClassNotFoundException {
            Class<?> cls = this.messageClass;
            return cls != null ? cls : Class.forName(this.messageClassName);
        }

        public Object readResolve() throws ObjectStreamException {
            try {
                Field declaredField = b().getDeclaredField("DEFAULT_INSTANCE");
                declaredField.setAccessible(true);
                return ((m0) declaredField.get(null)).newBuilderForType().mergeFrom(this.asBytes).buildPartial();
            } catch (InvalidProtocolBufferException e) {
                throw new RuntimeException("Unable to understand proto buffer", e);
            } catch (ClassNotFoundException e2) {
                throw new RuntimeException("Unable to find proto buffer class: " + this.messageClassName, e2);
            } catch (IllegalAccessException e3) {
                throw new RuntimeException("Unable to call parsePartialFrom", e3);
            } catch (NoSuchFieldException unused) {
                return a();
            } catch (SecurityException e4) {
                throw new RuntimeException("Unable to call DEFAULT_INSTANCE in " + this.messageClassName, e4);
            }
        }
    }

    /* loaded from: classes2.dex */
    public static abstract class a<MessageType extends GeneratedMessageLite<MessageType, BuilderType>, BuilderType extends a<MessageType, BuilderType>> extends b.a<MessageType, BuilderType> {
        public final MessageType a;
        public MessageType f0;
        public boolean g0;

        @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
        /* renamed from: a */
        public final MessageType build() {
            MessageType buildPartial = buildPartial();
            if (buildPartial.isInitialized()) {
                return buildPartial;
            }
            throw b.a.newUninitializedMessageException(buildPartial);
        }

        @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
        /* renamed from: b */
        public MessageType buildPartial() {
            if (this.g0) {
                return this.f0;
            }
            this.f0.h();
            this.g0 = true;
            return this.f0;
        }

        @Override // com.google.protobuf.b.a
        /* renamed from: d */
        public BuilderType clone() {
            BuilderType buildertype = (BuilderType) getDefaultInstanceForType().newBuilderForType();
            buildertype.k(buildPartial());
            return buildertype;
        }

        public final void e() {
            if (this.g0) {
                f();
                this.g0 = false;
            }
        }

        public void f() {
            MessageType messagetype = (MessageType) this.f0.b(MethodToInvoke.NEW_MUTABLE_INSTANCE);
            p(messagetype, this.f0);
            this.f0 = messagetype;
        }

        @Override // defpackage.d82, com.google.protobuf.o0
        /* renamed from: g */
        public MessageType getDefaultInstanceForType() {
            return this.a;
        }

        @Override // com.google.protobuf.b.a
        /* renamed from: h */
        public BuilderType internalMergeFrom(MessageType messagetype) {
            return k(messagetype);
        }

        @Override // defpackage.d82
        public final boolean isInitialized() {
            return GeneratedMessageLite.g(this.f0, false);
        }

        @Override // com.google.protobuf.b.a, com.google.protobuf.m0.a
        /* renamed from: j */
        public BuilderType mergeFrom(j jVar, r rVar) throws IOException {
            e();
            try {
                u0.a().e(this.f0).i(this.f0, k.R(jVar), rVar);
                return this;
            } catch (RuntimeException e) {
                if (e.getCause() instanceof IOException) {
                    throw ((IOException) e.getCause());
                }
                throw e;
            }
        }

        public BuilderType k(MessageType messagetype) {
            e();
            p(this.f0, messagetype);
            return this;
        }

        @Override // com.google.protobuf.b.a
        /* renamed from: l */
        public BuilderType mergeFrom(byte[] bArr, int i, int i2) throws InvalidProtocolBufferException {
            return mergeFrom(bArr, i, i2, r.b());
        }

        @Override // com.google.protobuf.b.a
        /* renamed from: o */
        public BuilderType mergeFrom(byte[] bArr, int i, int i2, r rVar) throws InvalidProtocolBufferException {
            e();
            try {
                u0.a().e(this.f0).j(this.f0, bArr, i, i + i2, new e.b(rVar));
                return this;
            } catch (InvalidProtocolBufferException e) {
                throw e;
            } catch (IOException e2) {
                throw new RuntimeException("Reading from byte array should not throw IOException.", e2);
            } catch (IndexOutOfBoundsException unused) {
                throw InvalidProtocolBufferException.truncatedMessage();
            }
        }

        public final void p(MessageType messagetype, MessageType messagetype2) {
            u0.a().e(messagetype).a(messagetype, messagetype2);
        }
    }

    /* loaded from: classes2.dex */
    public static abstract class b<MessageType extends b<MessageType, BuilderType>, BuilderType> extends GeneratedMessageLite<MessageType, BuilderType> implements d82 {
        public w<c> h0 = w.r();

        @Override // com.google.protobuf.GeneratedMessageLite, defpackage.d82, com.google.protobuf.o0
        public /* bridge */ /* synthetic */ m0 getDefaultInstanceForType() {
            return super.getDefaultInstanceForType();
        }

        public w<c> k() {
            if (this.h0.C()) {
                this.h0 = this.h0.clone();
            }
            return this.h0;
        }

        @Override // com.google.protobuf.GeneratedMessageLite, com.google.protobuf.m0, com.google.protobuf.l0
        public /* bridge */ /* synthetic */ m0.a newBuilderForType() {
            return super.newBuilderForType();
        }

        @Override // com.google.protobuf.GeneratedMessageLite, com.google.protobuf.m0, com.google.protobuf.l0
        public /* bridge */ /* synthetic */ m0.a toBuilder() {
            return super.toBuilder();
        }
    }

    /* loaded from: classes2.dex */
    public static final class c implements w.c<c> {
        public final a0.d<?> a;
        public final int f0;
        public final WireFormat.FieldType g0;
        public final boolean h0;
        public final boolean i0;

        @Override // java.lang.Comparable
        /* renamed from: a */
        public int compareTo(c cVar) {
            return this.f0 - cVar.f0;
        }

        public a0.d<?> d() {
            return this.a;
        }

        @Override // com.google.protobuf.w.c
        public int getNumber() {
            return this.f0;
        }

        @Override // com.google.protobuf.w.c
        public boolean i() {
            return this.h0;
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.google.protobuf.w.c
        public m0.a i0(m0.a aVar, m0 m0Var) {
            return ((a) aVar).k((GeneratedMessageLite) m0Var);
        }

        @Override // com.google.protobuf.w.c
        public boolean isPacked() {
            return this.i0;
        }

        @Override // com.google.protobuf.w.c
        public WireFormat.FieldType n() {
            return this.g0;
        }

        @Override // com.google.protobuf.w.c
        public WireFormat.JavaType z() {
            return this.g0.getJavaType();
        }
    }

    /* loaded from: classes2.dex */
    public static class d<ContainingType extends m0, Type> extends p<ContainingType, Type> {
        public final m0 a;
        public final c b;

        public WireFormat.FieldType b() {
            return this.b.n();
        }

        public m0 c() {
            return this.a;
        }

        public int d() {
            return this.b.getNumber();
        }

        public boolean e() {
            return this.b.h0;
        }
    }

    public static <T extends GeneratedMessageLite<?, ?>> T e(Class<T> cls) {
        GeneratedMessageLite<?, ?> generatedMessageLite = g0.get(cls);
        if (generatedMessageLite == null) {
            try {
                Class.forName(cls.getName(), true, cls.getClassLoader());
                generatedMessageLite = g0.get(cls);
            } catch (ClassNotFoundException e) {
                throw new IllegalStateException("Class initialization cannot fail.", e);
            }
        }
        if (generatedMessageLite == null) {
            generatedMessageLite = (T) ((GeneratedMessageLite) k1.j(cls)).getDefaultInstanceForType();
            if (generatedMessageLite != null) {
                g0.put(cls, generatedMessageLite);
            } else {
                throw new IllegalStateException();
            }
        }
        return (T) generatedMessageLite;
    }

    public static final <T extends GeneratedMessageLite<T, ?>> boolean g(T t, boolean z) {
        byte byteValue = ((Byte) t.b(MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED)).byteValue();
        if (byteValue == 1) {
            return true;
        }
        if (byteValue == 0) {
            return false;
        }
        boolean f = u0.a().e(t).f(t);
        if (z) {
            t.c(MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED, f ? t : null);
        }
        return f;
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    public static Object invokeOrDie(Method method, Object obj, Object... objArr) {
        try {
            return method.invoke(obj, objArr);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Couldn't use Java reflection to implement protocol message reflection.", e);
        } catch (InvocationTargetException e2) {
            Throwable cause = e2.getCause();
            if (!(cause instanceof RuntimeException)) {
                if (cause instanceof Error) {
                    throw ((Error) cause);
                }
                throw new RuntimeException("Unexpected exception thrown by generated accessor method.", cause);
            }
            throw ((RuntimeException) cause);
        }
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    public Object a() throws Exception {
        return b(MethodToInvoke.BUILD_MESSAGE_INFO);
    }

    public Object b(MethodToInvoke methodToInvoke) {
        return d(methodToInvoke, null, null);
    }

    public Object c(MethodToInvoke methodToInvoke, Object obj) {
        return d(methodToInvoke, obj, null);
    }

    public abstract Object d(MethodToInvoke methodToInvoke, Object obj, Object obj2);

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass()) {
            return u0.a().e(this).b(this, (GeneratedMessageLite) obj);
        }
        return false;
    }

    @Override // defpackage.d82, com.google.protobuf.o0
    /* renamed from: f */
    public final MessageType getDefaultInstanceForType() {
        return (MessageType) b(MethodToInvoke.GET_DEFAULT_INSTANCE);
    }

    @Override // com.google.protobuf.b
    int getMemoizedSerializedSize() {
        return this.f0;
    }

    @Override // com.google.protobuf.m0
    public final t0<MessageType> getParserForType() {
        return (t0) b(MethodToInvoke.GET_PARSER);
    }

    @Override // com.google.protobuf.m0
    public int getSerializedSize() {
        if (this.f0 == -1) {
            this.f0 = u0.a().e(this).g(this);
        }
        return this.f0;
    }

    public void h() {
        u0.a().e(this).e(this);
    }

    public int hashCode() {
        int i = this.memoizedHashCode;
        if (i != 0) {
            return i;
        }
        int d2 = u0.a().e(this).d(this);
        this.memoizedHashCode = d2;
        return d2;
    }

    @Override // com.google.protobuf.m0, com.google.protobuf.l0
    /* renamed from: i */
    public final BuilderType newBuilderForType() {
        return (BuilderType) b(MethodToInvoke.NEW_BUILDER);
    }

    @Override // defpackage.d82
    public final boolean isInitialized() {
        return g(this, true);
    }

    @Override // com.google.protobuf.m0, com.google.protobuf.l0
    /* renamed from: j */
    public final BuilderType toBuilder() {
        BuilderType buildertype = (BuilderType) b(MethodToInvoke.NEW_BUILDER);
        buildertype.k(this);
        return buildertype;
    }

    @Override // com.google.protobuf.b
    void setMemoizedSerializedSize(int i) {
        this.f0 = i;
    }

    public String toString() {
        return n0.e(this, super.toString());
    }

    @Override // com.google.protobuf.m0
    public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
        u0.a().e(this).h(this, l.T(codedOutputStream));
    }
}
