package com.google.protobuf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* loaded from: classes2.dex */
public class MapField<K, V> implements fb2 {
    public volatile boolean a;
    public volatile StorageMode b;
    public c<K, V> c;
    public List<l0> d;
    public final a<K, V> e;

    /* loaded from: classes2.dex */
    public enum StorageMode {
        MAP,
        LIST,
        BOTH
    }

    /* loaded from: classes2.dex */
    public interface a<K, V> {
        l0 a(K k, V v);

        l0 b();

        void c(l0 l0Var, Map<K, V> map);
    }

    /* loaded from: classes2.dex */
    public static class b<K, V> implements a<K, V> {
        public final g0<K, V> a;

        public b(g0<K, V> g0Var) {
            this.a = g0Var;
        }

        @Override // com.google.protobuf.MapField.a
        public l0 a(K k, V v) {
            return this.a.newBuilderForType().r(k).t(v).buildPartial();
        }

        @Override // com.google.protobuf.MapField.a
        public l0 b() {
            return this.a;
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.google.protobuf.MapField.a
        public void c(l0 l0Var, Map<K, V> map) {
            g0 g0Var = (g0) l0Var;
            map.put(g0Var.f(), g0Var.h());
        }
    }

    /* loaded from: classes2.dex */
    public static class c<K, V> implements Map<K, V> {
        public final fb2 a;
        public final Map<K, V> f0;

        /* loaded from: classes2.dex */
        public static class a<E> implements Collection<E> {
            public final fb2 a;
            public final Collection<E> f0;

            public a(fb2 fb2Var, Collection<E> collection) {
                this.a = fb2Var;
                this.f0 = collection;
            }

            @Override // java.util.Collection
            public boolean add(E e) {
                throw new UnsupportedOperationException();
            }

            @Override // java.util.Collection
            public boolean addAll(Collection<? extends E> collection) {
                throw new UnsupportedOperationException();
            }

            @Override // java.util.Collection
            public void clear() {
                this.a.a();
                this.f0.clear();
            }

            @Override // java.util.Collection
            public boolean contains(Object obj) {
                return this.f0.contains(obj);
            }

            @Override // java.util.Collection
            public boolean containsAll(Collection<?> collection) {
                return this.f0.containsAll(collection);
            }

            @Override // java.util.Collection
            public boolean equals(Object obj) {
                return this.f0.equals(obj);
            }

            @Override // java.util.Collection
            public int hashCode() {
                return this.f0.hashCode();
            }

            @Override // java.util.Collection
            public boolean isEmpty() {
                return this.f0.isEmpty();
            }

            @Override // java.util.Collection, java.lang.Iterable
            public Iterator<E> iterator() {
                return new b(this.a, this.f0.iterator());
            }

            @Override // java.util.Collection
            public boolean remove(Object obj) {
                this.a.a();
                return this.f0.remove(obj);
            }

            @Override // java.util.Collection
            public boolean removeAll(Collection<?> collection) {
                this.a.a();
                return this.f0.removeAll(collection);
            }

            @Override // java.util.Collection
            public boolean retainAll(Collection<?> collection) {
                this.a.a();
                return this.f0.retainAll(collection);
            }

            @Override // java.util.Collection
            public int size() {
                return this.f0.size();
            }

            @Override // java.util.Collection
            public Object[] toArray() {
                return this.f0.toArray();
            }

            public String toString() {
                return this.f0.toString();
            }

            @Override // java.util.Collection
            public <T> T[] toArray(T[] tArr) {
                return (T[]) this.f0.toArray(tArr);
            }
        }

        /* loaded from: classes2.dex */
        public static class b<E> implements Iterator<E> {
            public final fb2 a;
            public final Iterator<E> f0;

            public b(fb2 fb2Var, Iterator<E> it) {
                this.a = fb2Var;
                this.f0 = it;
            }

            public boolean equals(Object obj) {
                return this.f0.equals(obj);
            }

            @Override // java.util.Iterator
            public boolean hasNext() {
                return this.f0.hasNext();
            }

            public int hashCode() {
                return this.f0.hashCode();
            }

            @Override // java.util.Iterator
            public E next() {
                return this.f0.next();
            }

            @Override // java.util.Iterator
            public void remove() {
                this.a.a();
                this.f0.remove();
            }

            public String toString() {
                return this.f0.toString();
            }
        }

        /* renamed from: com.google.protobuf.MapField$c$c  reason: collision with other inner class name */
        /* loaded from: classes2.dex */
        public static class C0150c<E> implements Set<E> {
            public final fb2 a;
            public final Set<E> f0;

            public C0150c(fb2 fb2Var, Set<E> set) {
                this.a = fb2Var;
                this.f0 = set;
            }

            @Override // java.util.Set, java.util.Collection
            public boolean add(E e) {
                this.a.a();
                return this.f0.add(e);
            }

            @Override // java.util.Set, java.util.Collection
            public boolean addAll(Collection<? extends E> collection) {
                this.a.a();
                return this.f0.addAll(collection);
            }

            @Override // java.util.Set, java.util.Collection
            public void clear() {
                this.a.a();
                this.f0.clear();
            }

            @Override // java.util.Set, java.util.Collection
            public boolean contains(Object obj) {
                return this.f0.contains(obj);
            }

            @Override // java.util.Set, java.util.Collection
            public boolean containsAll(Collection<?> collection) {
                return this.f0.containsAll(collection);
            }

            @Override // java.util.Set, java.util.Collection
            public boolean equals(Object obj) {
                return this.f0.equals(obj);
            }

            @Override // java.util.Set, java.util.Collection
            public int hashCode() {
                return this.f0.hashCode();
            }

            @Override // java.util.Set, java.util.Collection
            public boolean isEmpty() {
                return this.f0.isEmpty();
            }

            @Override // java.util.Set, java.util.Collection, java.lang.Iterable
            public Iterator<E> iterator() {
                return new b(this.a, this.f0.iterator());
            }

            @Override // java.util.Set, java.util.Collection
            public boolean remove(Object obj) {
                this.a.a();
                return this.f0.remove(obj);
            }

            @Override // java.util.Set, java.util.Collection
            public boolean removeAll(Collection<?> collection) {
                this.a.a();
                return this.f0.removeAll(collection);
            }

            @Override // java.util.Set, java.util.Collection
            public boolean retainAll(Collection<?> collection) {
                this.a.a();
                return this.f0.retainAll(collection);
            }

            @Override // java.util.Set, java.util.Collection
            public int size() {
                return this.f0.size();
            }

            @Override // java.util.Set, java.util.Collection
            public Object[] toArray() {
                return this.f0.toArray();
            }

            public String toString() {
                return this.f0.toString();
            }

            @Override // java.util.Set, java.util.Collection
            public <T> T[] toArray(T[] tArr) {
                return (T[]) this.f0.toArray(tArr);
            }
        }

        public c(fb2 fb2Var, Map<K, V> map) {
            this.a = fb2Var;
            this.f0 = map;
        }

        @Override // java.util.Map
        public void clear() {
            this.a.a();
            this.f0.clear();
        }

        @Override // java.util.Map
        public boolean containsKey(Object obj) {
            return this.f0.containsKey(obj);
        }

        @Override // java.util.Map
        public boolean containsValue(Object obj) {
            return this.f0.containsValue(obj);
        }

        @Override // java.util.Map
        public Set<Map.Entry<K, V>> entrySet() {
            return new C0150c(this.a, this.f0.entrySet());
        }

        @Override // java.util.Map
        public boolean equals(Object obj) {
            return this.f0.equals(obj);
        }

        @Override // java.util.Map
        public V get(Object obj) {
            return this.f0.get(obj);
        }

        @Override // java.util.Map
        public int hashCode() {
            return this.f0.hashCode();
        }

        @Override // java.util.Map
        public boolean isEmpty() {
            return this.f0.isEmpty();
        }

        @Override // java.util.Map
        public Set<K> keySet() {
            return new C0150c(this.a, this.f0.keySet());
        }

        @Override // java.util.Map
        public V put(K k, V v) {
            this.a.a();
            a0.a(k);
            a0.a(v);
            return this.f0.put(k, v);
        }

        @Override // java.util.Map
        public void putAll(Map<? extends K, ? extends V> map) {
            this.a.a();
            for (K k : map.keySet()) {
                a0.a(k);
                a0.a(map.get(k));
            }
            this.f0.putAll(map);
        }

        @Override // java.util.Map
        public V remove(Object obj) {
            this.a.a();
            return this.f0.remove(obj);
        }

        @Override // java.util.Map
        public int size() {
            return this.f0.size();
        }

        public String toString() {
            return this.f0.toString();
        }

        @Override // java.util.Map
        public Collection<V> values() {
            return new a(this.a, this.f0.values());
        }
    }

    public MapField(a<K, V> aVar, StorageMode storageMode, Map<K, V> map) {
        this.e = aVar;
        this.a = true;
        this.b = storageMode;
        this.c = new c<>(this, map);
        this.d = null;
    }

    public static <K, V> MapField<K, V> h(g0<K, V> g0Var) {
        return new MapField<>(g0Var, StorageMode.MAP, Collections.emptyMap());
    }

    public static <K, V> MapField<K, V> q(g0<K, V> g0Var) {
        return new MapField<>(g0Var, StorageMode.MAP, new LinkedHashMap());
    }

    @Override // defpackage.fb2
    public void a() {
        if (!n()) {
            throw new UnsupportedOperationException();
        }
    }

    public void b() {
        this.c = new c<>(this, new LinkedHashMap());
        this.b = StorageMode.MAP;
    }

    public final l0 c(K k, V v) {
        return this.e.a(k, v);
    }

    public final c<K, V> d(List<l0> list) {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (l0 l0Var : list) {
            f(l0Var, linkedHashMap);
        }
        return new c<>(this, linkedHashMap);
    }

    public final List<l0> e(c<K, V> cVar) {
        ArrayList arrayList = new ArrayList();
        for (Map.Entry<K, V> entry : cVar.entrySet()) {
            arrayList.add(c(entry.getKey(), entry.getValue()));
        }
        return arrayList;
    }

    public boolean equals(Object obj) {
        if (obj instanceof MapField) {
            return MapFieldLite.equals(j(), ((MapField) obj).j());
        }
        return false;
    }

    public final void f(l0 l0Var, Map<K, V> map) {
        this.e.c(l0Var, map);
    }

    public MapField<K, V> g() {
        return new MapField<>(this.e, StorageMode.MAP, MapFieldLite.copy(j()));
    }

    public int hashCode() {
        return MapFieldLite.calculateHashCodeForMap(j());
    }

    public List<l0> i() {
        StorageMode storageMode = this.b;
        StorageMode storageMode2 = StorageMode.MAP;
        if (storageMode == storageMode2) {
            synchronized (this) {
                if (this.b == storageMode2) {
                    this.d = e(this.c);
                    this.b = StorageMode.BOTH;
                }
            }
        }
        return Collections.unmodifiableList(this.d);
    }

    public Map<K, V> j() {
        StorageMode storageMode = this.b;
        StorageMode storageMode2 = StorageMode.LIST;
        if (storageMode == storageMode2) {
            synchronized (this) {
                if (this.b == storageMode2) {
                    this.c = d(this.d);
                    this.b = StorageMode.BOTH;
                }
            }
        }
        return Collections.unmodifiableMap(this.c);
    }

    public l0 k() {
        return this.e.b();
    }

    public List<l0> l() {
        StorageMode storageMode = this.b;
        StorageMode storageMode2 = StorageMode.LIST;
        if (storageMode != storageMode2) {
            if (this.b == StorageMode.MAP) {
                this.d = e(this.c);
            }
            this.c = null;
            this.b = storageMode2;
        }
        return this.d;
    }

    public Map<K, V> m() {
        StorageMode storageMode = this.b;
        StorageMode storageMode2 = StorageMode.MAP;
        if (storageMode != storageMode2) {
            if (this.b == StorageMode.LIST) {
                this.c = d(this.d);
            }
            this.d = null;
            this.b = storageMode2;
        }
        return this.c;
    }

    public boolean n() {
        return this.a;
    }

    public void o() {
        this.a = false;
    }

    public void p(MapField<K, V> mapField) {
        m().putAll(MapFieldLite.copy(mapField.j()));
    }

    public MapField(g0<K, V> g0Var, StorageMode storageMode, Map<K, V> map) {
        this(new b(g0Var), storageMode, map);
    }
}
