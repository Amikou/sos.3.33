package com.google.protobuf;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.RandomAccess;

/* compiled from: LazyStringArrayList.java */
/* loaded from: classes2.dex */
public class d0 extends d<String> implements cz1, RandomAccess {
    public static final d0 g0;
    public static final cz1 h0;
    public final List<Object> f0;

    static {
        d0 d0Var = new d0();
        g0 = d0Var;
        d0Var.l();
        h0 = d0Var;
    }

    public d0() {
        this(10);
    }

    public static ByteString k(Object obj) {
        if (obj instanceof ByteString) {
            return (ByteString) obj;
        }
        if (obj instanceof String) {
            return ByteString.copyFromUtf8((String) obj);
        }
        return ByteString.copyFrom((byte[]) obj);
    }

    public static String m(Object obj) {
        if (obj instanceof String) {
            return (String) obj;
        }
        if (obj instanceof ByteString) {
            return ((ByteString) obj).toStringUtf8();
        }
        return a0.l((byte[]) obj);
    }

    @Override // defpackage.cz1
    public void W(ByteString byteString) {
        e();
        this.f0.add(byteString);
        ((AbstractList) this).modCount++;
    }

    @Override // com.google.protobuf.d, java.util.AbstractCollection, java.util.Collection, java.util.List
    public boolean addAll(Collection<? extends String> collection) {
        return addAll(size(), collection);
    }

    @Override // com.google.protobuf.d, java.util.AbstractList, java.util.AbstractCollection, java.util.Collection, java.util.List
    public void clear() {
        e();
        this.f0.clear();
        ((AbstractList) this).modCount++;
    }

    @Override // defpackage.cz1
    public ByteString e1(int i) {
        Object obj = this.f0.get(i);
        ByteString k = k(obj);
        if (k != obj) {
            this.f0.set(i, k);
        }
        return k;
    }

    @Override // java.util.AbstractList, java.util.List
    /* renamed from: i */
    public void add(int i, String str) {
        e();
        this.f0.add(i, str);
        ((AbstractList) this).modCount++;
    }

    @Override // defpackage.cz1
    public Object j(int i) {
        return this.f0.get(i);
    }

    @Override // java.util.AbstractList, java.util.List
    /* renamed from: n */
    public String get(int i) {
        Object obj = this.f0.get(i);
        if (obj instanceof String) {
            return (String) obj;
        }
        if (obj instanceof ByteString) {
            ByteString byteString = (ByteString) obj;
            String stringUtf8 = byteString.toStringUtf8();
            if (byteString.isValidUtf8()) {
                this.f0.set(i, stringUtf8);
            }
            return stringUtf8;
        }
        byte[] bArr = (byte[]) obj;
        String l = a0.l(bArr);
        if (a0.i(bArr)) {
            this.f0.set(i, l);
        }
        return l;
    }

    @Override // com.google.protobuf.a0.i
    /* renamed from: o */
    public d0 a(int i) {
        if (i >= size()) {
            ArrayList arrayList = new ArrayList(i);
            arrayList.addAll(this.f0);
            return new d0(arrayList);
        }
        throw new IllegalArgumentException();
    }

    @Override // java.util.AbstractList, java.util.List
    /* renamed from: p */
    public String remove(int i) {
        e();
        Object remove = this.f0.remove(i);
        ((AbstractList) this).modCount++;
        return m(remove);
    }

    @Override // java.util.AbstractList, java.util.List
    /* renamed from: q */
    public String set(int i, String str) {
        e();
        return m(this.f0.set(i, str));
    }

    @Override // defpackage.cz1
    public List<?> r() {
        return Collections.unmodifiableList(this.f0);
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public int size() {
        return this.f0.size();
    }

    @Override // defpackage.cz1
    public cz1 x() {
        return A() ? new bf4(this) : this;
    }

    public d0(int i) {
        this(new ArrayList(i));
    }

    @Override // com.google.protobuf.d, java.util.AbstractList, java.util.List
    public boolean addAll(int i, Collection<? extends String> collection) {
        e();
        if (collection instanceof cz1) {
            collection = ((cz1) collection).r();
        }
        boolean addAll = this.f0.addAll(i, collection);
        ((AbstractList) this).modCount++;
        return addAll;
    }

    public d0(cz1 cz1Var) {
        this.f0 = new ArrayList(cz1Var.size());
        addAll(cz1Var);
    }

    public d0(ArrayList<Object> arrayList) {
        this.f0 = arrayList;
    }
}
