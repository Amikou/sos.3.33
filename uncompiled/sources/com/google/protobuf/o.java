package com.google.protobuf;

import com.google.protobuf.Descriptors;
import com.google.protobuf.a;
import com.google.protobuf.l0;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/* compiled from: DynamicMessage.java */
/* loaded from: classes2.dex */
public final class o extends com.google.protobuf.a {
    public final Descriptors.b a;
    public final w<Descriptors.FieldDescriptor> f0;
    public final Descriptors.FieldDescriptor[] g0;
    public final g1 h0;
    public int i0 = -1;

    /* compiled from: DynamicMessage.java */
    /* loaded from: classes2.dex */
    public class a extends c<o> {
        public a() {
        }

        @Override // com.google.protobuf.t0
        /* renamed from: a */
        public o parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
            b h = o.h(o.this.a);
            try {
                h.mergeFrom(jVar, rVar);
                return h.buildPartial();
            } catch (InvalidProtocolBufferException e) {
                throw e.setUnfinishedMessage(h.buildPartial());
            } catch (IOException e2) {
                throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(h.buildPartial());
            }
        }
    }

    /* compiled from: DynamicMessage.java */
    /* loaded from: classes2.dex */
    public static final class b extends a.AbstractC0151a<b> {
        public final Descriptors.b a;
        public w<Descriptors.FieldDescriptor> f0;
        public final Descriptors.FieldDescriptor[] g0;
        public g1 h0;

        public /* synthetic */ b(Descriptors.b bVar, a aVar) {
            this(bVar);
        }

        @Override // com.google.protobuf.l0.a
        /* renamed from: a */
        public b addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
            x(fieldDescriptor);
            k();
            this.f0.g(fieldDescriptor, obj);
            return this;
        }

        @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
        /* renamed from: b */
        public o build() {
            if (isInitialized()) {
                return buildPartial();
            }
            Descriptors.b bVar = this.a;
            w<Descriptors.FieldDescriptor> wVar = this.f0;
            Descriptors.FieldDescriptor[] fieldDescriptorArr = this.g0;
            throw a.AbstractC0151a.newUninitializedMessageException((l0) new o(bVar, wVar, (Descriptors.FieldDescriptor[]) Arrays.copyOf(fieldDescriptorArr, fieldDescriptorArr.length), this.h0));
        }

        @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
        /* renamed from: d */
        public o buildPartial() {
            this.f0.H();
            Descriptors.b bVar = this.a;
            w<Descriptors.FieldDescriptor> wVar = this.f0;
            Descriptors.FieldDescriptor[] fieldDescriptorArr = this.g0;
            return new o(bVar, wVar, (Descriptors.FieldDescriptor[]) Arrays.copyOf(fieldDescriptorArr, fieldDescriptorArr.length), this.h0);
        }

        @Override // com.google.protobuf.a.AbstractC0151a
        /* renamed from: e */
        public b clear() {
            if (this.f0.C()) {
                this.f0 = w.L();
            } else {
                this.f0.h();
            }
            if (this.a.t().getMapEntry()) {
                t();
            }
            this.h0 = g1.c();
            return this;
        }

        @Override // com.google.protobuf.l0.a
        /* renamed from: f */
        public b clearField(Descriptors.FieldDescriptor fieldDescriptor) {
            x(fieldDescriptor);
            k();
            Descriptors.g o = fieldDescriptor.o();
            if (o != null) {
                int r = o.r();
                Descriptors.FieldDescriptor[] fieldDescriptorArr = this.g0;
                if (fieldDescriptorArr[r] == fieldDescriptor) {
                    fieldDescriptorArr[r] = null;
                }
            }
            this.f0.i(fieldDescriptor);
            return this;
        }

        @Override // com.google.protobuf.a.AbstractC0151a
        /* renamed from: g */
        public b clearOneof2(Descriptors.g gVar) {
            y(gVar);
            Descriptors.FieldDescriptor fieldDescriptor = this.g0[gVar.r()];
            if (fieldDescriptor != null) {
                clearField(fieldDescriptor);
            }
            return this;
        }

        @Override // com.google.protobuf.o0
        public Map<Descriptors.FieldDescriptor, Object> getAllFields() {
            return this.f0.s();
        }

        @Override // com.google.protobuf.l0.a, com.google.protobuf.o0
        public Descriptors.b getDescriptorForType() {
            return this.a;
        }

        @Override // com.google.protobuf.o0
        public Object getField(Descriptors.FieldDescriptor fieldDescriptor) {
            x(fieldDescriptor);
            Object t = this.f0.t(fieldDescriptor);
            if (t == null) {
                if (fieldDescriptor.i()) {
                    return Collections.emptyList();
                }
                if (fieldDescriptor.v() == Descriptors.FieldDescriptor.JavaType.MESSAGE) {
                    return o.e(fieldDescriptor.x());
                }
                return fieldDescriptor.r();
            }
            return t;
        }

        @Override // com.google.protobuf.a.AbstractC0151a
        public l0.a getFieldBuilder(Descriptors.FieldDescriptor fieldDescriptor) {
            throw new UnsupportedOperationException("getFieldBuilder() called on a dynamic message type.");
        }

        @Override // com.google.protobuf.a.AbstractC0151a
        public Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar) {
            y(gVar);
            return this.g0[gVar.r()];
        }

        @Override // com.google.protobuf.a.AbstractC0151a
        public l0.a getRepeatedFieldBuilder(Descriptors.FieldDescriptor fieldDescriptor, int i) {
            throw new UnsupportedOperationException("getRepeatedFieldBuilder() called on a dynamic message type.");
        }

        @Override // com.google.protobuf.o0
        public g1 getUnknownFields() {
            return this.h0;
        }

        @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
        /* renamed from: h */
        public b clone() {
            b bVar = new b(this.a);
            bVar.f0.I(this.f0);
            bVar.mergeUnknownFields2(this.h0);
            Descriptors.FieldDescriptor[] fieldDescriptorArr = this.g0;
            System.arraycopy(fieldDescriptorArr, 0, bVar.g0, 0, fieldDescriptorArr.length);
            return bVar;
        }

        @Override // com.google.protobuf.o0
        public boolean hasField(Descriptors.FieldDescriptor fieldDescriptor) {
            x(fieldDescriptor);
            return this.f0.A(fieldDescriptor);
        }

        @Override // com.google.protobuf.a.AbstractC0151a
        public boolean hasOneof(Descriptors.g gVar) {
            y(gVar);
            return this.g0[gVar.r()] != null;
        }

        @Override // defpackage.d82
        public boolean isInitialized() {
            return o.g(this.a, this.f0);
        }

        public final void j(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
            if (fieldDescriptor.i()) {
                for (Object obj2 : (List) obj) {
                    l(fieldDescriptor, obj2);
                }
                return;
            }
            l(fieldDescriptor, obj);
        }

        public final void k() {
            if (this.f0.C()) {
                this.f0 = this.f0.clone();
            }
        }

        public final void l(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
            a0.a(obj);
            if (!(obj instanceof Descriptors.d)) {
                throw new IllegalArgumentException("DynamicMessage should use EnumValueDescriptor to set Enum Value.");
            }
        }

        @Override // defpackage.d82, com.google.protobuf.o0
        /* renamed from: o */
        public o getDefaultInstanceForType() {
            return o.e(this.a);
        }

        @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
        /* renamed from: p */
        public b mergeFrom(l0 l0Var) {
            if (l0Var instanceof o) {
                o oVar = (o) l0Var;
                if (oVar.a == this.a) {
                    k();
                    this.f0.I(oVar.f0);
                    mergeUnknownFields2(oVar.h0);
                    int i = 0;
                    while (true) {
                        Descriptors.FieldDescriptor[] fieldDescriptorArr = this.g0;
                        if (i >= fieldDescriptorArr.length) {
                            return this;
                        }
                        if (fieldDescriptorArr[i] == null) {
                            fieldDescriptorArr[i] = oVar.g0[i];
                        } else if (oVar.g0[i] != null && this.g0[i] != oVar.g0[i]) {
                            this.f0.i(this.g0[i]);
                            this.g0[i] = oVar.g0[i];
                        }
                        i++;
                    }
                } else {
                    throw new IllegalArgumentException("mergeFrom(Message) can only merge messages of the same type.");
                }
            } else {
                return (b) super.mergeFrom(l0Var);
            }
        }

        @Override // com.google.protobuf.a.AbstractC0151a
        /* renamed from: r */
        public b mergeUnknownFields2(g1 g1Var) {
            this.h0 = g1.h(this.h0).u(g1Var).build();
            return this;
        }

        @Override // com.google.protobuf.l0.a
        /* renamed from: s */
        public b newBuilderForField(Descriptors.FieldDescriptor fieldDescriptor) {
            x(fieldDescriptor);
            if (fieldDescriptor.v() == Descriptors.FieldDescriptor.JavaType.MESSAGE) {
                return new b(fieldDescriptor.x());
            }
            throw new IllegalArgumentException("newBuilderForField is only valid for fields with message type.");
        }

        public final void t() {
            for (Descriptors.FieldDescriptor fieldDescriptor : this.a.p()) {
                if (fieldDescriptor.v() == Descriptors.FieldDescriptor.JavaType.MESSAGE) {
                    this.f0.N(fieldDescriptor, o.e(fieldDescriptor.x()));
                } else {
                    this.f0.N(fieldDescriptor, fieldDescriptor.r());
                }
            }
        }

        @Override // com.google.protobuf.l0.a
        /* renamed from: u */
        public b setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
            x(fieldDescriptor);
            k();
            if (fieldDescriptor.A() == Descriptors.FieldDescriptor.Type.ENUM) {
                j(fieldDescriptor, obj);
            }
            Descriptors.g o = fieldDescriptor.o();
            if (o != null) {
                int r = o.r();
                Descriptors.FieldDescriptor fieldDescriptor2 = this.g0[r];
                if (fieldDescriptor2 != null && fieldDescriptor2 != fieldDescriptor) {
                    this.f0.i(fieldDescriptor2);
                }
                this.g0[r] = fieldDescriptor;
            } else if (fieldDescriptor.a().t() == Descriptors.FileDescriptor.Syntax.PROTO3 && !fieldDescriptor.i() && fieldDescriptor.v() != Descriptors.FieldDescriptor.JavaType.MESSAGE && obj.equals(fieldDescriptor.r())) {
                this.f0.i(fieldDescriptor);
                return this;
            }
            this.f0.N(fieldDescriptor, obj);
            return this;
        }

        @Override // com.google.protobuf.l0.a
        /* renamed from: v */
        public b setUnknownFields(g1 g1Var) {
            this.h0 = g1Var;
            return this;
        }

        public final void x(Descriptors.FieldDescriptor fieldDescriptor) {
            if (fieldDescriptor.p() != this.a) {
                throw new IllegalArgumentException("FieldDescriptor does not match message type.");
            }
        }

        public final void y(Descriptors.g gVar) {
            if (gVar.l() != this.a) {
                throw new IllegalArgumentException("OneofDescriptor does not match message type.");
            }
        }

        public b(Descriptors.b bVar) {
            this.a = bVar;
            this.f0 = w.L();
            this.h0 = g1.c();
            this.g0 = new Descriptors.FieldDescriptor[bVar.f().getOneofDeclCount()];
            if (bVar.t().getMapEntry()) {
                t();
            }
        }
    }

    public o(Descriptors.b bVar, w<Descriptors.FieldDescriptor> wVar, Descriptors.FieldDescriptor[] fieldDescriptorArr, g1 g1Var) {
        this.a = bVar;
        this.f0 = wVar;
        this.g0 = fieldDescriptorArr;
        this.h0 = g1Var;
    }

    public static o e(Descriptors.b bVar) {
        return new o(bVar, w.r(), new Descriptors.FieldDescriptor[bVar.f().getOneofDeclCount()], g1.c());
    }

    public static boolean g(Descriptors.b bVar, w<Descriptors.FieldDescriptor> wVar) {
        for (Descriptors.FieldDescriptor fieldDescriptor : bVar.p()) {
            if (fieldDescriptor.H() && !wVar.A(fieldDescriptor)) {
                return false;
            }
        }
        return wVar.D();
    }

    public static b h(Descriptors.b bVar) {
        return new b(bVar, null);
    }

    @Override // defpackage.d82, com.google.protobuf.o0
    /* renamed from: f */
    public o getDefaultInstanceForType() {
        return e(this.a);
    }

    @Override // com.google.protobuf.o0
    public Map<Descriptors.FieldDescriptor, Object> getAllFields() {
        return this.f0.s();
    }

    @Override // com.google.protobuf.o0
    public Descriptors.b getDescriptorForType() {
        return this.a;
    }

    @Override // com.google.protobuf.o0
    public Object getField(Descriptors.FieldDescriptor fieldDescriptor) {
        k(fieldDescriptor);
        Object t = this.f0.t(fieldDescriptor);
        if (t == null) {
            if (fieldDescriptor.i()) {
                return Collections.emptyList();
            }
            if (fieldDescriptor.v() == Descriptors.FieldDescriptor.JavaType.MESSAGE) {
                return e(fieldDescriptor.x());
            }
            return fieldDescriptor.r();
        }
        return t;
    }

    @Override // com.google.protobuf.a
    public Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar) {
        l(gVar);
        return this.g0[gVar.r()];
    }

    @Override // com.google.protobuf.m0
    public t0<o> getParserForType() {
        return new a();
    }

    @Override // com.google.protobuf.a, com.google.protobuf.m0
    public int getSerializedSize() {
        int y;
        int serializedSize;
        int i = this.i0;
        if (i != -1) {
            return i;
        }
        if (this.a.t().getMessageSetWireFormat()) {
            y = this.f0.u();
            serializedSize = this.h0.f();
        } else {
            y = this.f0.y();
            serializedSize = this.h0.getSerializedSize();
        }
        int i2 = y + serializedSize;
        this.i0 = i2;
        return i2;
    }

    @Override // com.google.protobuf.o0
    public g1 getUnknownFields() {
        return this.h0;
    }

    @Override // com.google.protobuf.o0
    public boolean hasField(Descriptors.FieldDescriptor fieldDescriptor) {
        k(fieldDescriptor);
        return this.f0.A(fieldDescriptor);
    }

    @Override // com.google.protobuf.a
    public boolean hasOneof(Descriptors.g gVar) {
        l(gVar);
        return this.g0[gVar.r()] != null;
    }

    @Override // com.google.protobuf.m0, com.google.protobuf.l0
    /* renamed from: i */
    public b newBuilderForType() {
        return new b(this.a, null);
    }

    @Override // com.google.protobuf.a, defpackage.d82
    public boolean isInitialized() {
        return g(this.a, this.f0);
    }

    @Override // com.google.protobuf.m0, com.google.protobuf.l0
    /* renamed from: j */
    public b toBuilder() {
        return newBuilderForType().mergeFrom(this);
    }

    public final void k(Descriptors.FieldDescriptor fieldDescriptor) {
        if (fieldDescriptor.p() != this.a) {
            throw new IllegalArgumentException("FieldDescriptor does not match message type.");
        }
    }

    public final void l(Descriptors.g gVar) {
        if (gVar.l() != this.a) {
            throw new IllegalArgumentException("OneofDescriptor does not match message type.");
        }
    }

    @Override // com.google.protobuf.a, com.google.protobuf.m0
    public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
        if (this.a.t().getMessageSetWireFormat()) {
            this.f0.S(codedOutputStream);
            this.h0.l(codedOutputStream);
            return;
        }
        this.f0.U(codedOutputStream);
        this.h0.writeTo(codedOutputStream);
    }
}
