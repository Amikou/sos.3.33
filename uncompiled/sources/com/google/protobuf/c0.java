package com.google.protobuf;

import java.util.Objects;

/* compiled from: LazyFieldLite.java */
/* loaded from: classes2.dex */
public class c0 {
    public ByteString a;
    public r b;
    public volatile m0 c;
    public volatile ByteString d;

    static {
        r.b();
    }

    public c0(r rVar, ByteString byteString) {
        a(rVar, byteString);
        this.b = rVar;
        this.a = byteString;
    }

    public static void a(r rVar, ByteString byteString) {
        Objects.requireNonNull(rVar, "found null ExtensionRegistry");
        Objects.requireNonNull(byteString, "found null ByteString");
    }

    public void b(m0 m0Var) {
        if (this.c != null) {
            return;
        }
        synchronized (this) {
            if (this.c != null) {
                return;
            }
            try {
                if (this.a != null) {
                    this.c = m0Var.getParserForType().parseFrom(this.a, this.b);
                    this.d = this.a;
                } else {
                    this.c = m0Var;
                    this.d = ByteString.EMPTY;
                }
            } catch (InvalidProtocolBufferException unused) {
                this.c = m0Var;
                this.d = ByteString.EMPTY;
            }
        }
    }

    public int c() {
        if (this.d != null) {
            return this.d.size();
        }
        ByteString byteString = this.a;
        if (byteString != null) {
            return byteString.size();
        }
        if (this.c != null) {
            return this.c.getSerializedSize();
        }
        return 0;
    }

    public m0 d(m0 m0Var) {
        b(m0Var);
        return this.c;
    }

    public m0 e(m0 m0Var) {
        m0 m0Var2 = this.c;
        this.a = null;
        this.d = null;
        this.c = m0Var;
        return m0Var2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof c0) {
            c0 c0Var = (c0) obj;
            m0 m0Var = this.c;
            m0 m0Var2 = c0Var.c;
            if (m0Var == null && m0Var2 == null) {
                return f().equals(c0Var.f());
            }
            if (m0Var == null || m0Var2 == null) {
                if (m0Var != null) {
                    return m0Var.equals(c0Var.d(m0Var.getDefaultInstanceForType()));
                }
                return d(m0Var2.getDefaultInstanceForType()).equals(m0Var2);
            }
            return m0Var.equals(m0Var2);
        }
        return false;
    }

    public ByteString f() {
        if (this.d != null) {
            return this.d;
        }
        ByteString byteString = this.a;
        if (byteString != null) {
            return byteString;
        }
        synchronized (this) {
            if (this.d != null) {
                return this.d;
            }
            if (this.c == null) {
                this.d = ByteString.EMPTY;
            } else {
                this.d = this.c.toByteString();
            }
            return this.d;
        }
    }

    public int hashCode() {
        return 1;
    }

    public c0() {
    }
}
