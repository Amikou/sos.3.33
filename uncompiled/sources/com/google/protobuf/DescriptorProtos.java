package com.google.protobuf;

import androidx.recyclerview.widget.RecyclerView;
import com.github.mikephil.charting.utils.Utils;
import com.google.protobuf.Descriptors;
import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.a;
import com.google.protobuf.a0;
import com.google.protobuf.g1;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import okhttp3.internal.http2.Http2;

/* loaded from: classes2.dex */
public final class DescriptorProtos {
    public static final GeneratedMessageV3.e A;
    public static final Descriptors.b B;
    public static final GeneratedMessageV3.e C;
    public static final Descriptors.b D;
    public static final GeneratedMessageV3.e E;
    public static final Descriptors.b F;
    public static final GeneratedMessageV3.e G;
    public static final Descriptors.b H;
    public static final GeneratedMessageV3.e I;
    public static final Descriptors.b J;
    public static final GeneratedMessageV3.e K;
    public static final Descriptors.b L;
    public static final GeneratedMessageV3.e M;
    public static final Descriptors.b N;
    public static final GeneratedMessageV3.e O;
    public static final Descriptors.b P;
    public static final GeneratedMessageV3.e Q;
    public static final Descriptors.b R;
    public static final GeneratedMessageV3.e S;
    public static final Descriptors.b T;
    public static final GeneratedMessageV3.e U;
    public static final Descriptors.b V;
    public static final GeneratedMessageV3.e W;
    public static final Descriptors.b X;
    public static final Descriptors.b Y;
    public static Descriptors.FileDescriptor Z = Descriptors.FileDescriptor.u(new String[]{"\n google/protobuf/descriptor.proto\u0012\u000fgoogle.protobuf\"G\n\u0011FileDescriptorSet\u00122\n\u0004file\u0018\u0001 \u0003(\u000b2$.google.protobuf.FileDescriptorProto\"Û\u0003\n\u0013FileDescriptorProto\u0012\f\n\u0004name\u0018\u0001 \u0001(\t\u0012\u000f\n\u0007package\u0018\u0002 \u0001(\t\u0012\u0012\n\ndependency\u0018\u0003 \u0003(\t\u0012\u0019\n\u0011public_dependency\u0018\n \u0003(\u0005\u0012\u0017\n\u000fweak_dependency\u0018\u000b \u0003(\u0005\u00126\n\fmessage_type\u0018\u0004 \u0003(\u000b2 .google.protobuf.DescriptorProto\u00127\n\tenum_type\u0018\u0005 \u0003(\u000b2$.google.protobuf.EnumDescriptorProto\u00128\n\u0007service\u0018\u0006 \u0003(\u000b2'.google.protobuf.ServiceDescriptorProto\u00128\n\textension\u0018\u0007 \u0003(\u000b2%.google.protobuf.FieldDescriptorProto\u0012-\n\u0007options\u0018\b \u0001(\u000b2\u001c.google.protobuf.FileOptions\u00129\n\u0010source_code_info\u0018\t \u0001(\u000b2\u001f.google.protobuf.SourceCodeInfo\u0012\u000e\n\u0006syntax\u0018\f \u0001(\t\"©\u0005\n\u000fDescriptorProto\u0012\f\n\u0004name\u0018\u0001 \u0001(\t\u00124\n\u0005field\u0018\u0002 \u0003(\u000b2%.google.protobuf.FieldDescriptorProto\u00128\n\textension\u0018\u0006 \u0003(\u000b2%.google.protobuf.FieldDescriptorProto\u00125\n\u000bnested_type\u0018\u0003 \u0003(\u000b2 .google.protobuf.DescriptorProto\u00127\n\tenum_type\u0018\u0004 \u0003(\u000b2$.google.protobuf.EnumDescriptorProto\u0012H\n\u000fextension_range\u0018\u0005 \u0003(\u000b2/.google.protobuf.DescriptorProto.ExtensionRange\u00129\n\noneof_decl\u0018\b \u0003(\u000b2%.google.protobuf.OneofDescriptorProto\u00120\n\u0007options\u0018\u0007 \u0001(\u000b2\u001f.google.protobuf.MessageOptions\u0012F\n\u000ereserved_range\u0018\t \u0003(\u000b2..google.protobuf.DescriptorProto.ReservedRange\u0012\u0015\n\rreserved_name\u0018\n \u0003(\t\u001ae\n\u000eExtensionRange\u0012\r\n\u0005start\u0018\u0001 \u0001(\u0005\u0012\u000b\n\u0003end\u0018\u0002 \u0001(\u0005\u00127\n\u0007options\u0018\u0003 \u0001(\u000b2&.google.protobuf.ExtensionRangeOptions\u001a+\n\rReservedRange\u0012\r\n\u0005start\u0018\u0001 \u0001(\u0005\u0012\u000b\n\u0003end\u0018\u0002 \u0001(\u0005\"g\n\u0015ExtensionRangeOptions\u0012C\n\u0014uninterpreted_option\u0018ç\u0007 \u0003(\u000b2$.google.protobuf.UninterpretedOption*\t\bè\u0007\u0010\u0080\u0080\u0080\u0080\u0002\"Õ\u0005\n\u0014FieldDescriptorProto\u0012\f\n\u0004name\u0018\u0001 \u0001(\t\u0012\u000e\n\u0006number\u0018\u0003 \u0001(\u0005\u0012:\n\u0005label\u0018\u0004 \u0001(\u000e2+.google.protobuf.FieldDescriptorProto.Label\u00128\n\u0004type\u0018\u0005 \u0001(\u000e2*.google.protobuf.FieldDescriptorProto.Type\u0012\u0011\n\ttype_name\u0018\u0006 \u0001(\t\u0012\u0010\n\bextendee\u0018\u0002 \u0001(\t\u0012\u0015\n\rdefault_value\u0018\u0007 \u0001(\t\u0012\u0013\n\u000boneof_index\u0018\t \u0001(\u0005\u0012\u0011\n\tjson_name\u0018\n \u0001(\t\u0012.\n\u0007options\u0018\b \u0001(\u000b2\u001d.google.protobuf.FieldOptions\u0012\u0017\n\u000fproto3_optional\u0018\u0011 \u0001(\b\"¶\u0002\n\u0004Type\u0012\u000f\n\u000bTYPE_DOUBLE\u0010\u0001\u0012\u000e\n\nTYPE_FLOAT\u0010\u0002\u0012\u000e\n\nTYPE_INT64\u0010\u0003\u0012\u000f\n\u000bTYPE_UINT64\u0010\u0004\u0012\u000e\n\nTYPE_INT32\u0010\u0005\u0012\u0010\n\fTYPE_FIXED64\u0010\u0006\u0012\u0010\n\fTYPE_FIXED32\u0010\u0007\u0012\r\n\tTYPE_BOOL\u0010\b\u0012\u000f\n\u000bTYPE_STRING\u0010\t\u0012\u000e\n\nTYPE_GROUP\u0010\n\u0012\u0010\n\fTYPE_MESSAGE\u0010\u000b\u0012\u000e\n\nTYPE_BYTES\u0010\f\u0012\u000f\n\u000bTYPE_UINT32\u0010\r\u0012\r\n\tTYPE_ENUM\u0010\u000e\u0012\u0011\n\rTYPE_SFIXED32\u0010\u000f\u0012\u0011\n\rTYPE_SFIXED64\u0010\u0010\u0012\u000f\n\u000bTYPE_SINT32\u0010\u0011\u0012\u000f\n\u000bTYPE_SINT64\u0010\u0012\"C\n\u0005Label\u0012\u0012\n\u000eLABEL_OPTIONAL\u0010\u0001\u0012\u0012\n\u000eLABEL_REQUIRED\u0010\u0002\u0012\u0012\n\u000eLABEL_REPEATED\u0010\u0003\"T\n\u0014OneofDescriptorProto\u0012\f\n\u0004name\u0018\u0001 \u0001(\t\u0012.\n\u0007options\u0018\u0002 \u0001(\u000b2\u001d.google.protobuf.OneofOptions\"¤\u0002\n\u0013EnumDescriptorProto\u0012\f\n\u0004name\u0018\u0001 \u0001(\t\u00128\n\u0005value\u0018\u0002 \u0003(\u000b2).google.protobuf.EnumValueDescriptorProto\u0012-\n\u0007options\u0018\u0003 \u0001(\u000b2\u001c.google.protobuf.EnumOptions\u0012N\n\u000ereserved_range\u0018\u0004 \u0003(\u000b26.google.protobuf.EnumDescriptorProto.EnumReservedRange\u0012\u0015\n\rreserved_name\u0018\u0005 \u0003(\t\u001a/\n\u0011EnumReservedRange\u0012\r\n\u0005start\u0018\u0001 \u0001(\u0005\u0012\u000b\n\u0003end\u0018\u0002 \u0001(\u0005\"l\n\u0018EnumValueDescriptorProto\u0012\f\n\u0004name\u0018\u0001 \u0001(\t\u0012\u000e\n\u0006number\u0018\u0002 \u0001(\u0005\u00122\n\u0007options\u0018\u0003 \u0001(\u000b2!.google.protobuf.EnumValueOptions\"\u0090\u0001\n\u0016ServiceDescriptorProto\u0012\f\n\u0004name\u0018\u0001 \u0001(\t\u00126\n\u0006method\u0018\u0002 \u0003(\u000b2&.google.protobuf.MethodDescriptorProto\u00120\n\u0007options\u0018\u0003 \u0001(\u000b2\u001f.google.protobuf.ServiceOptions\"Á\u0001\n\u0015MethodDescriptorProto\u0012\f\n\u0004name\u0018\u0001 \u0001(\t\u0012\u0012\n\ninput_type\u0018\u0002 \u0001(\t\u0012\u0013\n\u000boutput_type\u0018\u0003 \u0001(\t\u0012/\n\u0007options\u0018\u0004 \u0001(\u000b2\u001e.google.protobuf.MethodOptions\u0012\u001f\n\u0010client_streaming\u0018\u0005 \u0001(\b:\u0005false\u0012\u001f\n\u0010server_streaming\u0018\u0006 \u0001(\b:\u0005false\"¥\u0006\n\u000bFileOptions\u0012\u0014\n\fjava_package\u0018\u0001 \u0001(\t\u0012\u001c\n\u0014java_outer_classname\u0018\b \u0001(\t\u0012\"\n\u0013java_multiple_files\u0018\n \u0001(\b:\u0005false\u0012)\n\u001djava_generate_equals_and_hash\u0018\u0014 \u0001(\bB\u0002\u0018\u0001\u0012%\n\u0016java_string_check_utf8\u0018\u001b \u0001(\b:\u0005false\u0012F\n\foptimize_for\u0018\t \u0001(\u000e2).google.protobuf.FileOptions.OptimizeMode:\u0005SPEED\u0012\u0012\n\ngo_package\u0018\u000b \u0001(\t\u0012\"\n\u0013cc_generic_services\u0018\u0010 \u0001(\b:\u0005false\u0012$\n\u0015java_generic_services\u0018\u0011 \u0001(\b:\u0005false\u0012\"\n\u0013py_generic_services\u0018\u0012 \u0001(\b:\u0005false\u0012#\n\u0014php_generic_services\u0018* \u0001(\b:\u0005false\u0012\u0019\n\ndeprecated\u0018\u0017 \u0001(\b:\u0005false\u0012\u001e\n\u0010cc_enable_arenas\u0018\u001f \u0001(\b:\u0004true\u0012\u0019\n\u0011objc_class_prefix\u0018$ \u0001(\t\u0012\u0018\n\u0010csharp_namespace\u0018% \u0001(\t\u0012\u0014\n\fswift_prefix\u0018' \u0001(\t\u0012\u0018\n\u0010php_class_prefix\u0018( \u0001(\t\u0012\u0015\n\rphp_namespace\u0018) \u0001(\t\u0012\u001e\n\u0016php_metadata_namespace\u0018, \u0001(\t\u0012\u0014\n\fruby_package\u0018- \u0001(\t\u0012C\n\u0014uninterpreted_option\u0018ç\u0007 \u0003(\u000b2$.google.protobuf.UninterpretedOption\":\n\fOptimizeMode\u0012\t\n\u0005SPEED\u0010\u0001\u0012\r\n\tCODE_SIZE\u0010\u0002\u0012\u0010\n\fLITE_RUNTIME\u0010\u0003*\t\bè\u0007\u0010\u0080\u0080\u0080\u0080\u0002J\u0004\b&\u0010'\"ò\u0001\n\u000eMessageOptions\u0012&\n\u0017message_set_wire_format\u0018\u0001 \u0001(\b:\u0005false\u0012.\n\u001fno_standard_descriptor_accessor\u0018\u0002 \u0001(\b:\u0005false\u0012\u0019\n\ndeprecated\u0018\u0003 \u0001(\b:\u0005false\u0012\u0011\n\tmap_entry\u0018\u0007 \u0001(\b\u0012C\n\u0014uninterpreted_option\u0018ç\u0007 \u0003(\u000b2$.google.protobuf.UninterpretedOption*\t\bè\u0007\u0010\u0080\u0080\u0080\u0080\u0002J\u0004\b\b\u0010\tJ\u0004\b\t\u0010\n\"\u009e\u0003\n\fFieldOptions\u0012:\n\u0005ctype\u0018\u0001 \u0001(\u000e2#.google.protobuf.FieldOptions.CType:\u0006STRING\u0012\u000e\n\u0006packed\u0018\u0002 \u0001(\b\u0012?\n\u0006jstype\u0018\u0006 \u0001(\u000e2$.google.protobuf.FieldOptions.JSType:\tJS_NORMAL\u0012\u0013\n\u0004lazy\u0018\u0005 \u0001(\b:\u0005false\u0012\u0019\n\ndeprecated\u0018\u0003 \u0001(\b:\u0005false\u0012\u0013\n\u0004weak\u0018\n \u0001(\b:\u0005false\u0012C\n\u0014uninterpreted_option\u0018ç\u0007 \u0003(\u000b2$.google.protobuf.UninterpretedOption\"/\n\u0005CType\u0012\n\n\u0006STRING\u0010\u0000\u0012\b\n\u0004CORD\u0010\u0001\u0012\u0010\n\fSTRING_PIECE\u0010\u0002\"5\n\u0006JSType\u0012\r\n\tJS_NORMAL\u0010\u0000\u0012\r\n\tJS_STRING\u0010\u0001\u0012\r\n\tJS_NUMBER\u0010\u0002*\t\bè\u0007\u0010\u0080\u0080\u0080\u0080\u0002J\u0004\b\u0004\u0010\u0005\"^\n\fOneofOptions\u0012C\n\u0014uninterpreted_option\u0018ç\u0007 \u0003(\u000b2$.google.protobuf.UninterpretedOption*\t\bè\u0007\u0010\u0080\u0080\u0080\u0080\u0002\"\u0093\u0001\n\u000bEnumOptions\u0012\u0013\n\u000ballow_alias\u0018\u0002 \u0001(\b\u0012\u0019\n\ndeprecated\u0018\u0003 \u0001(\b:\u0005false\u0012C\n\u0014uninterpreted_option\u0018ç\u0007 \u0003(\u000b2$.google.protobuf.UninterpretedOption*\t\bè\u0007\u0010\u0080\u0080\u0080\u0080\u0002J\u0004\b\u0005\u0010\u0006\"}\n\u0010EnumValueOptions\u0012\u0019\n\ndeprecated\u0018\u0001 \u0001(\b:\u0005false\u0012C\n\u0014uninterpreted_option\u0018ç\u0007 \u0003(\u000b2$.google.protobuf.UninterpretedOption*\t\bè\u0007\u0010\u0080\u0080\u0080\u0080\u0002\"{\n\u000eServiceOptions\u0012\u0019\n\ndeprecated\u0018! \u0001(\b:\u0005false\u0012C\n\u0014uninterpreted_option\u0018ç\u0007 \u0003(\u000b2$.google.protobuf.UninterpretedOption*\t\bè\u0007\u0010\u0080\u0080\u0080\u0080\u0002\"\u00ad\u0002\n\rMethodOptions\u0012\u0019\n\ndeprecated\u0018! \u0001(\b:\u0005false\u0012_\n\u0011idempotency_level\u0018\" \u0001(\u000e2/.google.protobuf.MethodOptions.IdempotencyLevel:\u0013IDEMPOTENCY_UNKNOWN\u0012C\n\u0014uninterpreted_option\u0018ç\u0007 \u0003(\u000b2$.google.protobuf.UninterpretedOption\"P\n\u0010IdempotencyLevel\u0012\u0017\n\u0013IDEMPOTENCY_UNKNOWN\u0010\u0000\u0012\u0013\n\u000fNO_SIDE_EFFECTS\u0010\u0001\u0012\u000e\n\nIDEMPOTENT\u0010\u0002*\t\bè\u0007\u0010\u0080\u0080\u0080\u0080\u0002\"\u009e\u0002\n\u0013UninterpretedOption\u0012;\n\u0004name\u0018\u0002 \u0003(\u000b2-.google.protobuf.UninterpretedOption.NamePart\u0012\u0018\n\u0010identifier_value\u0018\u0003 \u0001(\t\u0012\u001a\n\u0012positive_int_value\u0018\u0004 \u0001(\u0004\u0012\u001a\n\u0012negative_int_value\u0018\u0005 \u0001(\u0003\u0012\u0014\n\fdouble_value\u0018\u0006 \u0001(\u0001\u0012\u0014\n\fstring_value\u0018\u0007 \u0001(\f\u0012\u0017\n\u000faggregate_value\u0018\b \u0001(\t\u001a3\n\bNamePart\u0012\u0011\n\tname_part\u0018\u0001 \u0002(\t\u0012\u0014\n\fis_extension\u0018\u0002 \u0002(\b\"Õ\u0001\n\u000eSourceCodeInfo\u0012:\n\blocation\u0018\u0001 \u0003(\u000b2(.google.protobuf.SourceCodeInfo.Location\u001a\u0086\u0001\n\bLocation\u0012\u0010\n\u0004path\u0018\u0001 \u0003(\u0005B\u0002\u0010\u0001\u0012\u0010\n\u0004span\u0018\u0002 \u0003(\u0005B\u0002\u0010\u0001\u0012\u0018\n\u0010leading_comments\u0018\u0003 \u0001(\t\u0012\u0019\n\u0011trailing_comments\u0018\u0004 \u0001(\t\u0012!\n\u0019leading_detached_comments\u0018\u0006 \u0003(\t\"§\u0001\n\u0011GeneratedCodeInfo\u0012A\n\nannotation\u0018\u0001 \u0003(\u000b2-.google.protobuf.GeneratedCodeInfo.Annotation\u001aO\n\nAnnotation\u0012\u0010\n\u0004path\u0018\u0001 \u0003(\u0005B\u0002\u0010\u0001\u0012\u0013\n\u000bsource_file\u0018\u0002 \u0001(\t\u0012\r\n\u0005begin\u0018\u0003 \u0001(\u0005\u0012\u000b\n\u0003end\u0018\u0004 \u0001(\u0005B\u008f\u0001\n\u0013com.google.protobufB\u0010DescriptorProtosH\u0001Z>github.com/golang/protobuf/protoc-gen-go/descriptor;descriptorø\u0001\u0001¢\u0002\u0003GPBª\u0002\u001aGoogle.Protobuf.Reflection"}, new Descriptors.FileDescriptor[0]);
    public static final Descriptors.b a;
    public static final Descriptors.b b;
    public static final GeneratedMessageV3.e c;
    public static final Descriptors.b d;
    public static final GeneratedMessageV3.e e;
    public static final Descriptors.b f;
    public static final GeneratedMessageV3.e g;
    public static final Descriptors.b h;
    public static final GeneratedMessageV3.e i;
    public static final Descriptors.b j;
    public static final GeneratedMessageV3.e k;
    public static final Descriptors.b l;
    public static final GeneratedMessageV3.e m;
    public static final Descriptors.b n;
    public static final GeneratedMessageV3.e o;
    public static final Descriptors.b p;
    public static final GeneratedMessageV3.e q;
    public static final Descriptors.b r;
    public static final GeneratedMessageV3.e s;
    public static final Descriptors.b t;
    public static final GeneratedMessageV3.e u;
    public static final Descriptors.b v;
    public static final GeneratedMessageV3.e w;
    public static final Descriptors.b x;
    public static final GeneratedMessageV3.e y;
    public static final Descriptors.b z;

    /* loaded from: classes2.dex */
    public static final class DescriptorProto extends GeneratedMessageV3 implements b {
        public static final int ENUM_TYPE_FIELD_NUMBER = 4;
        public static final int EXTENSION_FIELD_NUMBER = 6;
        public static final int EXTENSION_RANGE_FIELD_NUMBER = 5;
        public static final int FIELD_FIELD_NUMBER = 2;
        public static final int NAME_FIELD_NUMBER = 1;
        public static final int NESTED_TYPE_FIELD_NUMBER = 3;
        public static final int ONEOF_DECL_FIELD_NUMBER = 8;
        public static final int OPTIONS_FIELD_NUMBER = 7;
        public static final int RESERVED_NAME_FIELD_NUMBER = 10;
        public static final int RESERVED_RANGE_FIELD_NUMBER = 9;
        private static final long serialVersionUID = 0;
        private int bitField0_;
        private List<EnumDescriptorProto> enumType_;
        private List<ExtensionRange> extensionRange_;
        private List<FieldDescriptorProto> extension_;
        private List<FieldDescriptorProto> field_;
        private byte memoizedIsInitialized;
        private volatile Object name_;
        private List<DescriptorProto> nestedType_;
        private List<OneofDescriptorProto> oneofDecl_;
        private MessageOptions options_;
        private cz1 reservedName_;
        private List<ReservedRange> reservedRange_;
        public static final DescriptorProto a = new DescriptorProto();
        @Deprecated
        public static final t0<DescriptorProto> PARSER = new a();

        /* loaded from: classes2.dex */
        public static final class ExtensionRange extends GeneratedMessageV3 implements c {
            public static final int END_FIELD_NUMBER = 2;
            public static final int OPTIONS_FIELD_NUMBER = 3;
            public static final int START_FIELD_NUMBER = 1;
            private static final long serialVersionUID = 0;
            private int bitField0_;
            private int end_;
            private byte memoizedIsInitialized;
            private ExtensionRangeOptions options_;
            private int start_;
            public static final ExtensionRange a = new ExtensionRange();
            @Deprecated
            public static final t0<ExtensionRange> PARSER = new a();

            /* loaded from: classes2.dex */
            public static class a extends com.google.protobuf.c<ExtensionRange> {
                @Override // com.google.protobuf.t0
                /* renamed from: a */
                public ExtensionRange parsePartialFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
                    return new ExtensionRange(jVar, rVar);
                }
            }

            public static ExtensionRange getDefaultInstance() {
                return a;
            }

            public static final Descriptors.b getDescriptor() {
                return DescriptorProtos.f;
            }

            public static b newBuilder() {
                return a.toBuilder();
            }

            public static ExtensionRange parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (ExtensionRange) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
            }

            public static ExtensionRange parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer);
            }

            public static t0<ExtensionRange> parser() {
                return PARSER;
            }

            @Override // com.google.protobuf.a
            public boolean equals(Object obj) {
                if (obj == this) {
                    return true;
                }
                if (!(obj instanceof ExtensionRange)) {
                    return super.equals(obj);
                }
                ExtensionRange extensionRange = (ExtensionRange) obj;
                if (hasStart() != extensionRange.hasStart()) {
                    return false;
                }
                if ((!hasStart() || getStart() == extensionRange.getStart()) && hasEnd() == extensionRange.hasEnd()) {
                    if ((!hasEnd() || getEnd() == extensionRange.getEnd()) && hasOptions() == extensionRange.hasOptions()) {
                        return (!hasOptions() || getOptions().equals(extensionRange.getOptions())) && this.unknownFields.equals(extensionRange.unknownFields);
                    }
                    return false;
                }
                return false;
            }

            public int getEnd() {
                return this.end_;
            }

            public ExtensionRangeOptions getOptions() {
                ExtensionRangeOptions extensionRangeOptions = this.options_;
                return extensionRangeOptions == null ? ExtensionRangeOptions.getDefaultInstance() : extensionRangeOptions;
            }

            public g getOptionsOrBuilder() {
                ExtensionRangeOptions extensionRangeOptions = this.options_;
                return extensionRangeOptions == null ? ExtensionRangeOptions.getDefaultInstance() : extensionRangeOptions;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
            public t0<ExtensionRange> getParserForType() {
                return PARSER;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public int getSerializedSize() {
                int i = this.memoizedSize;
                if (i != -1) {
                    return i;
                }
                int x = (this.bitField0_ & 1) != 0 ? 0 + CodedOutputStream.x(1, this.start_) : 0;
                if ((this.bitField0_ & 2) != 0) {
                    x += CodedOutputStream.x(2, this.end_);
                }
                if ((this.bitField0_ & 4) != 0) {
                    x += CodedOutputStream.G(3, getOptions());
                }
                int serializedSize = x + this.unknownFields.getSerializedSize();
                this.memoizedSize = serializedSize;
                return serializedSize;
            }

            public int getStart() {
                return this.start_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
            public final g1 getUnknownFields() {
                return this.unknownFields;
            }

            public boolean hasEnd() {
                return (this.bitField0_ & 2) != 0;
            }

            public boolean hasOptions() {
                return (this.bitField0_ & 4) != 0;
            }

            public boolean hasStart() {
                return (this.bitField0_ & 1) != 0;
            }

            @Override // com.google.protobuf.a
            public int hashCode() {
                int i = this.memoizedHashCode;
                if (i != 0) {
                    return i;
                }
                int hashCode = 779 + getDescriptor().hashCode();
                if (hasStart()) {
                    hashCode = (((hashCode * 37) + 1) * 53) + getStart();
                }
                if (hasEnd()) {
                    hashCode = (((hashCode * 37) + 2) * 53) + getEnd();
                }
                if (hasOptions()) {
                    hashCode = (((hashCode * 37) + 3) * 53) + getOptions().hashCode();
                }
                int hashCode2 = (hashCode * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode2;
                return hashCode2;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return DescriptorProtos.g.d(ExtensionRange.class, b.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
            public final boolean isInitialized() {
                byte b2 = this.memoizedIsInitialized;
                if (b2 == 1) {
                    return true;
                }
                if (b2 == 0) {
                    return false;
                }
                if (hasOptions() && !getOptions().isInitialized()) {
                    this.memoizedIsInitialized = (byte) 0;
                    return false;
                }
                this.memoizedIsInitialized = (byte) 1;
                return true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Object newInstance(GeneratedMessageV3.f fVar) {
                return new ExtensionRange();
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
                if ((this.bitField0_ & 1) != 0) {
                    codedOutputStream.G0(1, this.start_);
                }
                if ((this.bitField0_ & 2) != 0) {
                    codedOutputStream.G0(2, this.end_);
                }
                if ((this.bitField0_ & 4) != 0) {
                    codedOutputStream.K0(3, getOptions());
                }
                this.unknownFields.writeTo(codedOutputStream);
            }

            /* loaded from: classes2.dex */
            public static final class b extends GeneratedMessageV3.b<b> implements c {
                public int a;
                public int f0;
                public int g0;
                public ExtensionRangeOptions h0;
                public a1<ExtensionRangeOptions, ExtensionRangeOptions.b, g> i0;

                public b A(int i) {
                    this.a |= 1;
                    this.f0 = i;
                    onChanged();
                    return this;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                /* renamed from: B */
                public final b setUnknownFields(g1 g1Var) {
                    return (b) super.setUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                /* renamed from: a */
                public b addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (b) super.addRepeatedField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                /* renamed from: b */
                public ExtensionRange build() {
                    ExtensionRange buildPartial = buildPartial();
                    if (buildPartial.isInitialized()) {
                        return buildPartial;
                    }
                    throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                /* renamed from: d */
                public ExtensionRange buildPartial() {
                    int i;
                    ExtensionRange extensionRange = new ExtensionRange(this);
                    int i2 = this.a;
                    if ((i2 & 1) != 0) {
                        extensionRange.start_ = this.f0;
                        i = 1;
                    } else {
                        i = 0;
                    }
                    if ((i2 & 2) != 0) {
                        extensionRange.end_ = this.g0;
                        i |= 2;
                    }
                    if ((i2 & 4) != 0) {
                        a1<ExtensionRangeOptions, ExtensionRangeOptions.b, g> a1Var = this.i0;
                        if (a1Var == null) {
                            extensionRange.options_ = this.h0;
                        } else {
                            extensionRange.options_ = a1Var.b();
                        }
                        i |= 4;
                    }
                    extensionRange.bitField0_ = i;
                    onBuilt();
                    return extensionRange;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                /* renamed from: e */
                public b clear() {
                    super.clear();
                    this.f0 = 0;
                    int i = this.a & (-2);
                    this.a = i;
                    this.g0 = 0;
                    this.a = i & (-3);
                    a1<ExtensionRangeOptions, ExtensionRangeOptions.b, g> a1Var = this.i0;
                    if (a1Var == null) {
                        this.h0 = null;
                    } else {
                        a1Var.c();
                    }
                    this.a &= -5;
                    return this;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                /* renamed from: f */
                public b clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                    return (b) super.clearField(fieldDescriptor);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                /* renamed from: g */
                public b clearOneof(Descriptors.g gVar) {
                    return (b) super.clearOneof(gVar);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
                public Descriptors.b getDescriptorForType() {
                    return DescriptorProtos.f;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
                /* renamed from: h */
                public b clone() {
                    return (b) super.clone();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                    return DescriptorProtos.g.d(ExtensionRange.class, b.class);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
                public final boolean isInitialized() {
                    return !o() || k().isInitialized();
                }

                @Override // defpackage.d82, com.google.protobuf.o0
                /* renamed from: j */
                public ExtensionRange getDefaultInstanceForType() {
                    return ExtensionRange.getDefaultInstance();
                }

                public ExtensionRangeOptions k() {
                    a1<ExtensionRangeOptions, ExtensionRangeOptions.b, g> a1Var = this.i0;
                    if (a1Var == null) {
                        ExtensionRangeOptions extensionRangeOptions = this.h0;
                        return extensionRangeOptions == null ? ExtensionRangeOptions.getDefaultInstance() : extensionRangeOptions;
                    }
                    return a1Var.f();
                }

                public final a1<ExtensionRangeOptions, ExtensionRangeOptions.b, g> l() {
                    if (this.i0 == null) {
                        this.i0 = new a1<>(k(), getParentForChildren(), isClean());
                        this.h0 = null;
                    }
                    return this.i0;
                }

                public final void maybeForceBuilderInitialization() {
                    if (GeneratedMessageV3.alwaysUseFieldBuilders) {
                        l();
                    }
                }

                public boolean o() {
                    return (this.a & 4) != 0;
                }

                /* JADX WARN: Removed duplicated region for block: B:16:0x0021  */
                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
                /* renamed from: p */
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct code enable 'Show inconsistent code' option in preferences
                */
                public com.google.protobuf.DescriptorProtos.DescriptorProto.ExtensionRange.b mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                    /*
                        r2 = this;
                        r0 = 0
                        com.google.protobuf.t0<com.google.protobuf.DescriptorProtos$DescriptorProto$ExtensionRange> r1 = com.google.protobuf.DescriptorProtos.DescriptorProto.ExtensionRange.PARSER     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                        java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                        com.google.protobuf.DescriptorProtos$DescriptorProto$ExtensionRange r3 = (com.google.protobuf.DescriptorProtos.DescriptorProto.ExtensionRange) r3     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                        if (r3 == 0) goto Le
                        r2.r(r3)
                    Le:
                        return r2
                    Lf:
                        r3 = move-exception
                        goto L1f
                    L11:
                        r3 = move-exception
                        com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> Lf
                        com.google.protobuf.DescriptorProtos$DescriptorProto$ExtensionRange r4 = (com.google.protobuf.DescriptorProtos.DescriptorProto.ExtensionRange) r4     // Catch: java.lang.Throwable -> Lf
                        java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1d
                        throw r3     // Catch: java.lang.Throwable -> L1d
                    L1d:
                        r3 = move-exception
                        r0 = r4
                    L1f:
                        if (r0 == 0) goto L24
                        r2.r(r0)
                    L24:
                        throw r3
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.DescriptorProtos.DescriptorProto.ExtensionRange.b.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):com.google.protobuf.DescriptorProtos$DescriptorProto$ExtensionRange$b");
                }

                public b r(ExtensionRange extensionRange) {
                    if (extensionRange == ExtensionRange.getDefaultInstance()) {
                        return this;
                    }
                    if (extensionRange.hasStart()) {
                        A(extensionRange.getStart());
                    }
                    if (extensionRange.hasEnd()) {
                        v(extensionRange.getEnd());
                    }
                    if (extensionRange.hasOptions()) {
                        t(extensionRange.getOptions());
                    }
                    mergeUnknownFields(extensionRange.unknownFields);
                    onChanged();
                    return this;
                }

                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
                /* renamed from: s */
                public b mergeFrom(l0 l0Var) {
                    if (l0Var instanceof ExtensionRange) {
                        return r((ExtensionRange) l0Var);
                    }
                    super.mergeFrom(l0Var);
                    return this;
                }

                public b t(ExtensionRangeOptions extensionRangeOptions) {
                    ExtensionRangeOptions extensionRangeOptions2;
                    a1<ExtensionRangeOptions, ExtensionRangeOptions.b, g> a1Var = this.i0;
                    if (a1Var == null) {
                        if ((this.a & 4) != 0 && (extensionRangeOptions2 = this.h0) != null && extensionRangeOptions2 != ExtensionRangeOptions.getDefaultInstance()) {
                            this.h0 = ExtensionRangeOptions.newBuilder(this.h0).G(extensionRangeOptions).buildPartial();
                        } else {
                            this.h0 = extensionRangeOptions;
                        }
                        onChanged();
                    } else {
                        a1Var.h(extensionRangeOptions);
                    }
                    this.a |= 4;
                    return this;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                /* renamed from: u */
                public final b mergeUnknownFields(g1 g1Var) {
                    return (b) super.mergeUnknownFields(g1Var);
                }

                public b v(int i) {
                    this.a |= 2;
                    this.g0 = i;
                    onChanged();
                    return this;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                /* renamed from: x */
                public b setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (b) super.setField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                /* renamed from: y */
                public b setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                    return (b) super.m19setRepeatedField(fieldDescriptor, i, obj);
                }

                public b() {
                    maybeForceBuilderInitialization();
                }

                public b(GeneratedMessageV3.c cVar) {
                    super(cVar);
                    maybeForceBuilderInitialization();
                }
            }

            public static b newBuilder(ExtensionRange extensionRange) {
                return a.toBuilder().r(extensionRange);
            }

            public static ExtensionRange parseFrom(ByteBuffer byteBuffer, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer, rVar);
            }

            public ExtensionRange(GeneratedMessageV3.b<?> bVar) {
                super(bVar);
                this.memoizedIsInitialized = (byte) -1;
            }

            public static ExtensionRange parseDelimitedFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
                return (ExtensionRange) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
            }

            public static ExtensionRange parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
            public ExtensionRange getDefaultInstanceForType() {
                return a;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public b toBuilder() {
                return this == a ? new b() : new b().r(this);
            }

            public static ExtensionRange parseFrom(ByteString byteString, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString, rVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public b newBuilderForType() {
                return newBuilder();
            }

            public ExtensionRange() {
                this.memoizedIsInitialized = (byte) -1;
            }

            public static ExtensionRange parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr);
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public b newBuilderForType(GeneratedMessageV3.c cVar) {
                return new b(cVar);
            }

            public static ExtensionRange parseFrom(byte[] bArr, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr, rVar);
            }

            public ExtensionRange(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
                this();
                Objects.requireNonNull(rVar);
                g1.b g = g1.g();
                boolean z = false;
                while (!z) {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 8) {
                                    this.bitField0_ |= 1;
                                    this.start_ = jVar.x();
                                } else if (J == 16) {
                                    this.bitField0_ |= 2;
                                    this.end_ = jVar.x();
                                } else if (J != 26) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    ExtensionRangeOptions.b builder = (this.bitField0_ & 4) != 0 ? this.options_.toBuilder() : null;
                                    ExtensionRangeOptions extensionRangeOptions = (ExtensionRangeOptions) jVar.z(ExtensionRangeOptions.PARSER, rVar);
                                    this.options_ = extensionRangeOptions;
                                    if (builder != null) {
                                        builder.G(extensionRangeOptions);
                                        this.options_ = builder.buildPartial();
                                    }
                                    this.bitField0_ |= 4;
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        } catch (IOException e2) {
                            throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                        }
                    } finally {
                        this.unknownFields = g.build();
                        makeExtensionsImmutable();
                    }
                }
            }

            public static ExtensionRange parseFrom(InputStream inputStream) throws IOException {
                return (ExtensionRange) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
            }

            public static ExtensionRange parseFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
                return (ExtensionRange) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
            }

            public static ExtensionRange parseFrom(com.google.protobuf.j jVar) throws IOException {
                return (ExtensionRange) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
            }

            public static ExtensionRange parseFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws IOException {
                return (ExtensionRange) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
            }
        }

        /* loaded from: classes2.dex */
        public static final class ReservedRange extends GeneratedMessageV3 implements d {
            public static final int END_FIELD_NUMBER = 2;
            public static final int START_FIELD_NUMBER = 1;
            private static final long serialVersionUID = 0;
            private int bitField0_;
            private int end_;
            private byte memoizedIsInitialized;
            private int start_;
            public static final ReservedRange a = new ReservedRange();
            @Deprecated
            public static final t0<ReservedRange> PARSER = new a();

            /* loaded from: classes2.dex */
            public static class a extends com.google.protobuf.c<ReservedRange> {
                @Override // com.google.protobuf.t0
                /* renamed from: a */
                public ReservedRange parsePartialFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
                    return new ReservedRange(jVar, rVar);
                }
            }

            public static ReservedRange getDefaultInstance() {
                return a;
            }

            public static final Descriptors.b getDescriptor() {
                return DescriptorProtos.h;
            }

            public static b newBuilder() {
                return a.toBuilder();
            }

            public static ReservedRange parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (ReservedRange) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
            }

            public static ReservedRange parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer);
            }

            public static t0<ReservedRange> parser() {
                return PARSER;
            }

            @Override // com.google.protobuf.a
            public boolean equals(Object obj) {
                if (obj == this) {
                    return true;
                }
                if (!(obj instanceof ReservedRange)) {
                    return super.equals(obj);
                }
                ReservedRange reservedRange = (ReservedRange) obj;
                if (hasStart() != reservedRange.hasStart()) {
                    return false;
                }
                if ((!hasStart() || getStart() == reservedRange.getStart()) && hasEnd() == reservedRange.hasEnd()) {
                    return (!hasEnd() || getEnd() == reservedRange.getEnd()) && this.unknownFields.equals(reservedRange.unknownFields);
                }
                return false;
            }

            public int getEnd() {
                return this.end_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
            public t0<ReservedRange> getParserForType() {
                return PARSER;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public int getSerializedSize() {
                int i = this.memoizedSize;
                if (i != -1) {
                    return i;
                }
                int x = (this.bitField0_ & 1) != 0 ? 0 + CodedOutputStream.x(1, this.start_) : 0;
                if ((this.bitField0_ & 2) != 0) {
                    x += CodedOutputStream.x(2, this.end_);
                }
                int serializedSize = x + this.unknownFields.getSerializedSize();
                this.memoizedSize = serializedSize;
                return serializedSize;
            }

            public int getStart() {
                return this.start_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
            public final g1 getUnknownFields() {
                return this.unknownFields;
            }

            public boolean hasEnd() {
                return (this.bitField0_ & 2) != 0;
            }

            public boolean hasStart() {
                return (this.bitField0_ & 1) != 0;
            }

            @Override // com.google.protobuf.a
            public int hashCode() {
                int i = this.memoizedHashCode;
                if (i != 0) {
                    return i;
                }
                int hashCode = 779 + getDescriptor().hashCode();
                if (hasStart()) {
                    hashCode = (((hashCode * 37) + 1) * 53) + getStart();
                }
                if (hasEnd()) {
                    hashCode = (((hashCode * 37) + 2) * 53) + getEnd();
                }
                int hashCode2 = (hashCode * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode2;
                return hashCode2;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return DescriptorProtos.i.d(ReservedRange.class, b.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
            public final boolean isInitialized() {
                byte b2 = this.memoizedIsInitialized;
                if (b2 == 1) {
                    return true;
                }
                if (b2 == 0) {
                    return false;
                }
                this.memoizedIsInitialized = (byte) 1;
                return true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Object newInstance(GeneratedMessageV3.f fVar) {
                return new ReservedRange();
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
                if ((this.bitField0_ & 1) != 0) {
                    codedOutputStream.G0(1, this.start_);
                }
                if ((this.bitField0_ & 2) != 0) {
                    codedOutputStream.G0(2, this.end_);
                }
                this.unknownFields.writeTo(codedOutputStream);
            }

            /* loaded from: classes2.dex */
            public static final class b extends GeneratedMessageV3.b<b> implements d {
                public int a;
                public int f0;
                public int g0;

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                /* renamed from: a */
                public b addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (b) super.addRepeatedField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                /* renamed from: b */
                public ReservedRange build() {
                    ReservedRange buildPartial = buildPartial();
                    if (buildPartial.isInitialized()) {
                        return buildPartial;
                    }
                    throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                /* renamed from: d */
                public ReservedRange buildPartial() {
                    int i;
                    ReservedRange reservedRange = new ReservedRange(this);
                    int i2 = this.a;
                    if ((i2 & 1) != 0) {
                        reservedRange.start_ = this.f0;
                        i = 1;
                    } else {
                        i = 0;
                    }
                    if ((i2 & 2) != 0) {
                        reservedRange.end_ = this.g0;
                        i |= 2;
                    }
                    reservedRange.bitField0_ = i;
                    onBuilt();
                    return reservedRange;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                /* renamed from: e */
                public b clear() {
                    super.clear();
                    this.f0 = 0;
                    int i = this.a & (-2);
                    this.a = i;
                    this.g0 = 0;
                    this.a = i & (-3);
                    return this;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                /* renamed from: f */
                public b clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                    return (b) super.clearField(fieldDescriptor);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                /* renamed from: g */
                public b clearOneof(Descriptors.g gVar) {
                    return (b) super.clearOneof(gVar);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
                public Descriptors.b getDescriptorForType() {
                    return DescriptorProtos.h;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
                /* renamed from: h */
                public b clone() {
                    return (b) super.clone();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                    return DescriptorProtos.i.d(ReservedRange.class, b.class);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
                public final boolean isInitialized() {
                    return true;
                }

                @Override // defpackage.d82, com.google.protobuf.o0
                /* renamed from: j */
                public ReservedRange getDefaultInstanceForType() {
                    return ReservedRange.getDefaultInstance();
                }

                /* JADX WARN: Removed duplicated region for block: B:16:0x0021  */
                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
                /* renamed from: k */
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct code enable 'Show inconsistent code' option in preferences
                */
                public com.google.protobuf.DescriptorProtos.DescriptorProto.ReservedRange.b mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                    /*
                        r2 = this;
                        r0 = 0
                        com.google.protobuf.t0<com.google.protobuf.DescriptorProtos$DescriptorProto$ReservedRange> r1 = com.google.protobuf.DescriptorProtos.DescriptorProto.ReservedRange.PARSER     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                        java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                        com.google.protobuf.DescriptorProtos$DescriptorProto$ReservedRange r3 = (com.google.protobuf.DescriptorProtos.DescriptorProto.ReservedRange) r3     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                        if (r3 == 0) goto Le
                        r2.l(r3)
                    Le:
                        return r2
                    Lf:
                        r3 = move-exception
                        goto L1f
                    L11:
                        r3 = move-exception
                        com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> Lf
                        com.google.protobuf.DescriptorProtos$DescriptorProto$ReservedRange r4 = (com.google.protobuf.DescriptorProtos.DescriptorProto.ReservedRange) r4     // Catch: java.lang.Throwable -> Lf
                        java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1d
                        throw r3     // Catch: java.lang.Throwable -> L1d
                    L1d:
                        r3 = move-exception
                        r0 = r4
                    L1f:
                        if (r0 == 0) goto L24
                        r2.l(r0)
                    L24:
                        throw r3
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.DescriptorProtos.DescriptorProto.ReservedRange.b.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):com.google.protobuf.DescriptorProtos$DescriptorProto$ReservedRange$b");
                }

                public b l(ReservedRange reservedRange) {
                    if (reservedRange == ReservedRange.getDefaultInstance()) {
                        return this;
                    }
                    if (reservedRange.hasStart()) {
                        u(reservedRange.getStart());
                    }
                    if (reservedRange.hasEnd()) {
                        r(reservedRange.getEnd());
                    }
                    mergeUnknownFields(reservedRange.unknownFields);
                    onChanged();
                    return this;
                }

                public final void maybeForceBuilderInitialization() {
                    boolean z = GeneratedMessageV3.alwaysUseFieldBuilders;
                }

                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
                /* renamed from: o */
                public b mergeFrom(l0 l0Var) {
                    if (l0Var instanceof ReservedRange) {
                        return l((ReservedRange) l0Var);
                    }
                    super.mergeFrom(l0Var);
                    return this;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                /* renamed from: p */
                public final b mergeUnknownFields(g1 g1Var) {
                    return (b) super.mergeUnknownFields(g1Var);
                }

                public b r(int i) {
                    this.a |= 2;
                    this.g0 = i;
                    onChanged();
                    return this;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                /* renamed from: s */
                public b setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (b) super.setField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                /* renamed from: t */
                public b setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                    return (b) super.m19setRepeatedField(fieldDescriptor, i, obj);
                }

                public b u(int i) {
                    this.a |= 1;
                    this.f0 = i;
                    onChanged();
                    return this;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                /* renamed from: v */
                public final b setUnknownFields(g1 g1Var) {
                    return (b) super.setUnknownFields(g1Var);
                }

                public b() {
                    maybeForceBuilderInitialization();
                }

                public b(GeneratedMessageV3.c cVar) {
                    super(cVar);
                    maybeForceBuilderInitialization();
                }
            }

            public static b newBuilder(ReservedRange reservedRange) {
                return a.toBuilder().l(reservedRange);
            }

            public static ReservedRange parseFrom(ByteBuffer byteBuffer, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer, rVar);
            }

            public ReservedRange(GeneratedMessageV3.b<?> bVar) {
                super(bVar);
                this.memoizedIsInitialized = (byte) -1;
            }

            public static ReservedRange parseDelimitedFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
                return (ReservedRange) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
            }

            public static ReservedRange parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
            public ReservedRange getDefaultInstanceForType() {
                return a;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public b toBuilder() {
                return this == a ? new b() : new b().l(this);
            }

            public static ReservedRange parseFrom(ByteString byteString, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString, rVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public b newBuilderForType() {
                return newBuilder();
            }

            public ReservedRange() {
                this.memoizedIsInitialized = (byte) -1;
            }

            public static ReservedRange parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr);
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public b newBuilderForType(GeneratedMessageV3.c cVar) {
                return new b(cVar);
            }

            public static ReservedRange parseFrom(byte[] bArr, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr, rVar);
            }

            public ReservedRange(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
                this();
                Objects.requireNonNull(rVar);
                g1.b g = g1.g();
                boolean z = false;
                while (!z) {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 8) {
                                    this.bitField0_ |= 1;
                                    this.start_ = jVar.x();
                                } else if (J != 16) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.bitField0_ |= 2;
                                    this.end_ = jVar.x();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        } catch (IOException e2) {
                            throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                        }
                    } finally {
                        this.unknownFields = g.build();
                        makeExtensionsImmutable();
                    }
                }
            }

            public static ReservedRange parseFrom(InputStream inputStream) throws IOException {
                return (ReservedRange) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
            }

            public static ReservedRange parseFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
                return (ReservedRange) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
            }

            public static ReservedRange parseFrom(com.google.protobuf.j jVar) throws IOException {
                return (ReservedRange) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
            }

            public static ReservedRange parseFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws IOException {
                return (ReservedRange) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
            }
        }

        /* loaded from: classes2.dex */
        public static class a extends com.google.protobuf.c<DescriptorProto> {
            @Override // com.google.protobuf.t0
            /* renamed from: a */
            public DescriptorProto parsePartialFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
                return new DescriptorProto(jVar, rVar);
            }
        }

        /* loaded from: classes2.dex */
        public interface c extends o0 {
        }

        /* loaded from: classes2.dex */
        public interface d extends o0 {
        }

        public static DescriptorProto getDefaultInstance() {
            return a;
        }

        public static final Descriptors.b getDescriptor() {
            return DescriptorProtos.d;
        }

        public static b newBuilder() {
            return a.toBuilder();
        }

        public static DescriptorProto parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (DescriptorProto) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static DescriptorProto parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<DescriptorProto> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof DescriptorProto)) {
                return super.equals(obj);
            }
            DescriptorProto descriptorProto = (DescriptorProto) obj;
            if (hasName() != descriptorProto.hasName()) {
                return false;
            }
            if ((!hasName() || getName().equals(descriptorProto.getName())) && getFieldList().equals(descriptorProto.getFieldList()) && getExtensionList().equals(descriptorProto.getExtensionList()) && getNestedTypeList().equals(descriptorProto.getNestedTypeList()) && getEnumTypeList().equals(descriptorProto.getEnumTypeList()) && getExtensionRangeList().equals(descriptorProto.getExtensionRangeList()) && getOneofDeclList().equals(descriptorProto.getOneofDeclList()) && hasOptions() == descriptorProto.hasOptions()) {
                return (!hasOptions() || getOptions().equals(descriptorProto.getOptions())) && getReservedRangeList().equals(descriptorProto.getReservedRangeList()) && m15getReservedNameList().equals(descriptorProto.m15getReservedNameList()) && this.unknownFields.equals(descriptorProto.unknownFields);
            }
            return false;
        }

        public EnumDescriptorProto getEnumType(int i) {
            return this.enumType_.get(i);
        }

        public int getEnumTypeCount() {
            return this.enumType_.size();
        }

        public List<EnumDescriptorProto> getEnumTypeList() {
            return this.enumType_;
        }

        public c getEnumTypeOrBuilder(int i) {
            return this.enumType_.get(i);
        }

        public List<? extends c> getEnumTypeOrBuilderList() {
            return this.enumType_;
        }

        public FieldDescriptorProto getExtension(int i) {
            return this.extension_.get(i);
        }

        public int getExtensionCount() {
            return this.extension_.size();
        }

        public List<FieldDescriptorProto> getExtensionList() {
            return this.extension_;
        }

        public h getExtensionOrBuilder(int i) {
            return this.extension_.get(i);
        }

        public List<? extends h> getExtensionOrBuilderList() {
            return this.extension_;
        }

        public ExtensionRange getExtensionRange(int i) {
            return this.extensionRange_.get(i);
        }

        public int getExtensionRangeCount() {
            return this.extensionRange_.size();
        }

        public List<ExtensionRange> getExtensionRangeList() {
            return this.extensionRange_;
        }

        public c getExtensionRangeOrBuilder(int i) {
            return this.extensionRange_.get(i);
        }

        public List<? extends c> getExtensionRangeOrBuilderList() {
            return this.extensionRange_;
        }

        public FieldDescriptorProto getField(int i) {
            return this.field_.get(i);
        }

        public int getFieldCount() {
            return this.field_.size();
        }

        public List<FieldDescriptorProto> getFieldList() {
            return this.field_;
        }

        public h getFieldOrBuilder(int i) {
            return this.field_.get(i);
        }

        public List<? extends h> getFieldOrBuilderList() {
            return this.field_;
        }

        public String getName() {
            Object obj = this.name_;
            if (obj instanceof String) {
                return (String) obj;
            }
            ByteString byteString = (ByteString) obj;
            String stringUtf8 = byteString.toStringUtf8();
            if (byteString.isValidUtf8()) {
                this.name_ = stringUtf8;
            }
            return stringUtf8;
        }

        public ByteString getNameBytes() {
            Object obj = this.name_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.name_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        public DescriptorProto getNestedType(int i) {
            return this.nestedType_.get(i);
        }

        public int getNestedTypeCount() {
            return this.nestedType_.size();
        }

        public List<DescriptorProto> getNestedTypeList() {
            return this.nestedType_;
        }

        public b getNestedTypeOrBuilder(int i) {
            return this.nestedType_.get(i);
        }

        public List<? extends b> getNestedTypeOrBuilderList() {
            return this.nestedType_;
        }

        public OneofDescriptorProto getOneofDecl(int i) {
            return this.oneofDecl_.get(i);
        }

        public int getOneofDeclCount() {
            return this.oneofDecl_.size();
        }

        public List<OneofDescriptorProto> getOneofDeclList() {
            return this.oneofDecl_;
        }

        public n getOneofDeclOrBuilder(int i) {
            return this.oneofDecl_.get(i);
        }

        public List<? extends n> getOneofDeclOrBuilderList() {
            return this.oneofDecl_;
        }

        public MessageOptions getOptions() {
            MessageOptions messageOptions = this.options_;
            return messageOptions == null ? MessageOptions.getDefaultInstance() : messageOptions;
        }

        public k getOptionsOrBuilder() {
            MessageOptions messageOptions = this.options_;
            return messageOptions == null ? MessageOptions.getDefaultInstance() : messageOptions;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<DescriptorProto> getParserForType() {
            return PARSER;
        }

        public String getReservedName(int i) {
            return this.reservedName_.get(i);
        }

        public ByteString getReservedNameBytes(int i) {
            return this.reservedName_.e1(i);
        }

        public int getReservedNameCount() {
            return this.reservedName_.size();
        }

        public ReservedRange getReservedRange(int i) {
            return this.reservedRange_.get(i);
        }

        public int getReservedRangeCount() {
            return this.reservedRange_.size();
        }

        public List<ReservedRange> getReservedRangeList() {
            return this.reservedRange_;
        }

        public d getReservedRangeOrBuilder(int i) {
            return this.reservedRange_.get(i);
        }

        public List<? extends d> getReservedRangeOrBuilderList() {
            return this.reservedRange_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = (this.bitField0_ & 1) != 0 ? GeneratedMessageV3.computeStringSize(1, this.name_) + 0 : 0;
            for (int i2 = 0; i2 < this.field_.size(); i2++) {
                computeStringSize += CodedOutputStream.G(2, this.field_.get(i2));
            }
            for (int i3 = 0; i3 < this.nestedType_.size(); i3++) {
                computeStringSize += CodedOutputStream.G(3, this.nestedType_.get(i3));
            }
            for (int i4 = 0; i4 < this.enumType_.size(); i4++) {
                computeStringSize += CodedOutputStream.G(4, this.enumType_.get(i4));
            }
            for (int i5 = 0; i5 < this.extensionRange_.size(); i5++) {
                computeStringSize += CodedOutputStream.G(5, this.extensionRange_.get(i5));
            }
            for (int i6 = 0; i6 < this.extension_.size(); i6++) {
                computeStringSize += CodedOutputStream.G(6, this.extension_.get(i6));
            }
            if ((this.bitField0_ & 2) != 0) {
                computeStringSize += CodedOutputStream.G(7, getOptions());
            }
            for (int i7 = 0; i7 < this.oneofDecl_.size(); i7++) {
                computeStringSize += CodedOutputStream.G(8, this.oneofDecl_.get(i7));
            }
            for (int i8 = 0; i8 < this.reservedRange_.size(); i8++) {
                computeStringSize += CodedOutputStream.G(9, this.reservedRange_.get(i8));
            }
            int i9 = 0;
            for (int i10 = 0; i10 < this.reservedName_.size(); i10++) {
                i9 += GeneratedMessageV3.computeStringSizeNoTag(this.reservedName_.j(i10));
            }
            int size = computeStringSize + i9 + (m15getReservedNameList().size() * 1) + this.unknownFields.getSerializedSize();
            this.memoizedSize = size;
            return size;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        public boolean hasName() {
            return (this.bitField0_ & 1) != 0;
        }

        public boolean hasOptions() {
            return (this.bitField0_ & 2) != 0;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = 779 + getDescriptor().hashCode();
            if (hasName()) {
                hashCode = (((hashCode * 37) + 1) * 53) + getName().hashCode();
            }
            if (getFieldCount() > 0) {
                hashCode = (((hashCode * 37) + 2) * 53) + getFieldList().hashCode();
            }
            if (getExtensionCount() > 0) {
                hashCode = (((hashCode * 37) + 6) * 53) + getExtensionList().hashCode();
            }
            if (getNestedTypeCount() > 0) {
                hashCode = (((hashCode * 37) + 3) * 53) + getNestedTypeList().hashCode();
            }
            if (getEnumTypeCount() > 0) {
                hashCode = (((hashCode * 37) + 4) * 53) + getEnumTypeList().hashCode();
            }
            if (getExtensionRangeCount() > 0) {
                hashCode = (((hashCode * 37) + 5) * 53) + getExtensionRangeList().hashCode();
            }
            if (getOneofDeclCount() > 0) {
                hashCode = (((hashCode * 37) + 8) * 53) + getOneofDeclList().hashCode();
            }
            if (hasOptions()) {
                hashCode = (((hashCode * 37) + 7) * 53) + getOptions().hashCode();
            }
            if (getReservedRangeCount() > 0) {
                hashCode = (((hashCode * 37) + 9) * 53) + getReservedRangeList().hashCode();
            }
            if (getReservedNameCount() > 0) {
                hashCode = (((hashCode * 37) + 10) * 53) + m15getReservedNameList().hashCode();
            }
            int hashCode2 = (hashCode * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode2;
            return hashCode2;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return DescriptorProtos.e.d(DescriptorProto.class, b.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b2 = this.memoizedIsInitialized;
            if (b2 == 1) {
                return true;
            }
            if (b2 == 0) {
                return false;
            }
            for (int i = 0; i < getFieldCount(); i++) {
                if (!getField(i).isInitialized()) {
                    this.memoizedIsInitialized = (byte) 0;
                    return false;
                }
            }
            for (int i2 = 0; i2 < getExtensionCount(); i2++) {
                if (!getExtension(i2).isInitialized()) {
                    this.memoizedIsInitialized = (byte) 0;
                    return false;
                }
            }
            for (int i3 = 0; i3 < getNestedTypeCount(); i3++) {
                if (!getNestedType(i3).isInitialized()) {
                    this.memoizedIsInitialized = (byte) 0;
                    return false;
                }
            }
            for (int i4 = 0; i4 < getEnumTypeCount(); i4++) {
                if (!getEnumType(i4).isInitialized()) {
                    this.memoizedIsInitialized = (byte) 0;
                    return false;
                }
            }
            for (int i5 = 0; i5 < getExtensionRangeCount(); i5++) {
                if (!getExtensionRange(i5).isInitialized()) {
                    this.memoizedIsInitialized = (byte) 0;
                    return false;
                }
            }
            for (int i6 = 0; i6 < getOneofDeclCount(); i6++) {
                if (!getOneofDecl(i6).isInitialized()) {
                    this.memoizedIsInitialized = (byte) 0;
                    return false;
                }
            }
            if (hasOptions() && !getOptions().isInitialized()) {
                this.memoizedIsInitialized = (byte) 0;
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new DescriptorProto();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if ((this.bitField0_ & 1) != 0) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.name_);
            }
            for (int i = 0; i < this.field_.size(); i++) {
                codedOutputStream.K0(2, this.field_.get(i));
            }
            for (int i2 = 0; i2 < this.nestedType_.size(); i2++) {
                codedOutputStream.K0(3, this.nestedType_.get(i2));
            }
            for (int i3 = 0; i3 < this.enumType_.size(); i3++) {
                codedOutputStream.K0(4, this.enumType_.get(i3));
            }
            for (int i4 = 0; i4 < this.extensionRange_.size(); i4++) {
                codedOutputStream.K0(5, this.extensionRange_.get(i4));
            }
            for (int i5 = 0; i5 < this.extension_.size(); i5++) {
                codedOutputStream.K0(6, this.extension_.get(i5));
            }
            if ((this.bitField0_ & 2) != 0) {
                codedOutputStream.K0(7, getOptions());
            }
            for (int i6 = 0; i6 < this.oneofDecl_.size(); i6++) {
                codedOutputStream.K0(8, this.oneofDecl_.get(i6));
            }
            for (int i7 = 0; i7 < this.reservedRange_.size(); i7++) {
                codedOutputStream.K0(9, this.reservedRange_.get(i7));
            }
            for (int i8 = 0; i8 < this.reservedName_.size(); i8++) {
                GeneratedMessageV3.writeString(codedOutputStream, 10, this.reservedName_.j(i8));
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes2.dex */
        public static final class b extends GeneratedMessageV3.b<b> implements b {
            public int a;
            public Object f0;
            public List<FieldDescriptorProto> g0;
            public x0<FieldDescriptorProto, FieldDescriptorProto.b, h> h0;
            public List<FieldDescriptorProto> i0;
            public x0<FieldDescriptorProto, FieldDescriptorProto.b, h> j0;
            public List<DescriptorProto> k0;
            public x0<DescriptorProto, b, b> l0;
            public List<EnumDescriptorProto> m0;
            public x0<EnumDescriptorProto, EnumDescriptorProto.b, c> n0;
            public List<ExtensionRange> o0;
            public x0<ExtensionRange, ExtensionRange.b, c> p0;
            public List<OneofDescriptorProto> q0;
            public x0<OneofDescriptorProto, OneofDescriptorProto.b, n> r0;
            public MessageOptions s0;
            public a1<MessageOptions, MessageOptions.b, k> t0;
            public List<ReservedRange> u0;
            public x0<ReservedRange, ReservedRange.b, d> v0;
            public cz1 w0;

            public final x0<EnumDescriptorProto, EnumDescriptorProto.b, c> A() {
                if (this.n0 == null) {
                    this.n0 = new x0<>(this.m0, (this.a & 16) != 0, getParentForChildren(), isClean());
                    this.m0 = null;
                }
                return this.n0;
            }

            public FieldDescriptorProto B(int i) {
                x0<FieldDescriptorProto, FieldDescriptorProto.b, h> x0Var = this.j0;
                if (x0Var == null) {
                    return this.i0.get(i);
                }
                return x0Var.o(i);
            }

            public int C() {
                x0<FieldDescriptorProto, FieldDescriptorProto.b, h> x0Var = this.j0;
                if (x0Var == null) {
                    return this.i0.size();
                }
                return x0Var.n();
            }

            public final x0<FieldDescriptorProto, FieldDescriptorProto.b, h> D() {
                if (this.j0 == null) {
                    this.j0 = new x0<>(this.i0, (this.a & 4) != 0, getParentForChildren(), isClean());
                    this.i0 = null;
                }
                return this.j0;
            }

            public ExtensionRange E(int i) {
                x0<ExtensionRange, ExtensionRange.b, c> x0Var = this.p0;
                if (x0Var == null) {
                    return this.o0.get(i);
                }
                return x0Var.o(i);
            }

            public int G() {
                x0<ExtensionRange, ExtensionRange.b, c> x0Var = this.p0;
                if (x0Var == null) {
                    return this.o0.size();
                }
                return x0Var.n();
            }

            public final x0<ExtensionRange, ExtensionRange.b, c> H() {
                if (this.p0 == null) {
                    this.p0 = new x0<>(this.o0, (this.a & 32) != 0, getParentForChildren(), isClean());
                    this.o0 = null;
                }
                return this.p0;
            }

            public FieldDescriptorProto I(int i) {
                x0<FieldDescriptorProto, FieldDescriptorProto.b, h> x0Var = this.h0;
                if (x0Var == null) {
                    return this.g0.get(i);
                }
                return x0Var.o(i);
            }

            public int J() {
                x0<FieldDescriptorProto, FieldDescriptorProto.b, h> x0Var = this.h0;
                if (x0Var == null) {
                    return this.g0.size();
                }
                return x0Var.n();
            }

            public final x0<FieldDescriptorProto, FieldDescriptorProto.b, h> K() {
                if (this.h0 == null) {
                    this.h0 = new x0<>(this.g0, (this.a & 2) != 0, getParentForChildren(), isClean());
                    this.g0 = null;
                }
                return this.h0;
            }

            public DescriptorProto L(int i) {
                x0<DescriptorProto, b, b> x0Var = this.l0;
                if (x0Var == null) {
                    return this.k0.get(i);
                }
                return x0Var.o(i);
            }

            public int M() {
                x0<DescriptorProto, b, b> x0Var = this.l0;
                if (x0Var == null) {
                    return this.k0.size();
                }
                return x0Var.n();
            }

            public final x0<DescriptorProto, b, b> N() {
                if (this.l0 == null) {
                    this.l0 = new x0<>(this.k0, (this.a & 8) != 0, getParentForChildren(), isClean());
                    this.k0 = null;
                }
                return this.l0;
            }

            public OneofDescriptorProto O(int i) {
                x0<OneofDescriptorProto, OneofDescriptorProto.b, n> x0Var = this.r0;
                if (x0Var == null) {
                    return this.q0.get(i);
                }
                return x0Var.o(i);
            }

            public int P() {
                x0<OneofDescriptorProto, OneofDescriptorProto.b, n> x0Var = this.r0;
                if (x0Var == null) {
                    return this.q0.size();
                }
                return x0Var.n();
            }

            public final x0<OneofDescriptorProto, OneofDescriptorProto.b, n> Q() {
                if (this.r0 == null) {
                    this.r0 = new x0<>(this.q0, (this.a & 64) != 0, getParentForChildren(), isClean());
                    this.q0 = null;
                }
                return this.r0;
            }

            public MessageOptions R() {
                a1<MessageOptions, MessageOptions.b, k> a1Var = this.t0;
                if (a1Var == null) {
                    MessageOptions messageOptions = this.s0;
                    return messageOptions == null ? MessageOptions.getDefaultInstance() : messageOptions;
                }
                return a1Var.f();
            }

            public final a1<MessageOptions, MessageOptions.b, k> S() {
                if (this.t0 == null) {
                    this.t0 = new a1<>(R(), getParentForChildren(), isClean());
                    this.s0 = null;
                }
                return this.t0;
            }

            public final x0<ReservedRange, ReservedRange.b, d> T() {
                if (this.v0 == null) {
                    this.v0 = new x0<>(this.u0, (this.a & 256) != 0, getParentForChildren(), isClean());
                    this.u0 = null;
                }
                return this.v0;
            }

            public boolean U() {
                return (this.a & 128) != 0;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0021  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /* renamed from: V */
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public com.google.protobuf.DescriptorProtos.DescriptorProto.b mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0<com.google.protobuf.DescriptorProtos$DescriptorProto> r1 = com.google.protobuf.DescriptorProtos.DescriptorProto.PARSER     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    com.google.protobuf.DescriptorProtos$DescriptorProto r3 = (com.google.protobuf.DescriptorProtos.DescriptorProto) r3     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    if (r3 == 0) goto Le
                    r2.W(r3)
                Le:
                    return r2
                Lf:
                    r3 = move-exception
                    goto L1f
                L11:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> Lf
                    com.google.protobuf.DescriptorProtos$DescriptorProto r4 = (com.google.protobuf.DescriptorProtos.DescriptorProto) r4     // Catch: java.lang.Throwable -> Lf
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1d
                    throw r3     // Catch: java.lang.Throwable -> L1d
                L1d:
                    r3 = move-exception
                    r0 = r4
                L1f:
                    if (r0 == 0) goto L24
                    r2.W(r0)
                L24:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.DescriptorProtos.DescriptorProto.b.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):com.google.protobuf.DescriptorProtos$DescriptorProto$b");
            }

            public b W(DescriptorProto descriptorProto) {
                if (descriptorProto == DescriptorProto.getDefaultInstance()) {
                    return this;
                }
                if (descriptorProto.hasName()) {
                    this.a |= 1;
                    this.f0 = descriptorProto.name_;
                    onChanged();
                }
                if (this.h0 == null) {
                    if (!descriptorProto.field_.isEmpty()) {
                        if (this.g0.isEmpty()) {
                            this.g0 = descriptorProto.field_;
                            this.a &= -3;
                        } else {
                            p();
                            this.g0.addAll(descriptorProto.field_);
                        }
                        onChanged();
                    }
                } else if (!descriptorProto.field_.isEmpty()) {
                    if (!this.h0.u()) {
                        this.h0.b(descriptorProto.field_);
                    } else {
                        this.h0.i();
                        this.h0 = null;
                        this.g0 = descriptorProto.field_;
                        this.a &= -3;
                        this.h0 = GeneratedMessageV3.alwaysUseFieldBuilders ? K() : null;
                    }
                }
                if (this.j0 == null) {
                    if (!descriptorProto.extension_.isEmpty()) {
                        if (this.i0.isEmpty()) {
                            this.i0 = descriptorProto.extension_;
                            this.a &= -5;
                        } else {
                            l();
                            this.i0.addAll(descriptorProto.extension_);
                        }
                        onChanged();
                    }
                } else if (!descriptorProto.extension_.isEmpty()) {
                    if (!this.j0.u()) {
                        this.j0.b(descriptorProto.extension_);
                    } else {
                        this.j0.i();
                        this.j0 = null;
                        this.i0 = descriptorProto.extension_;
                        this.a &= -5;
                        this.j0 = GeneratedMessageV3.alwaysUseFieldBuilders ? D() : null;
                    }
                }
                if (this.l0 == null) {
                    if (!descriptorProto.nestedType_.isEmpty()) {
                        if (this.k0.isEmpty()) {
                            this.k0 = descriptorProto.nestedType_;
                            this.a &= -9;
                        } else {
                            r();
                            this.k0.addAll(descriptorProto.nestedType_);
                        }
                        onChanged();
                    }
                } else if (!descriptorProto.nestedType_.isEmpty()) {
                    if (!this.l0.u()) {
                        this.l0.b(descriptorProto.nestedType_);
                    } else {
                        this.l0.i();
                        this.l0 = null;
                        this.k0 = descriptorProto.nestedType_;
                        this.a &= -9;
                        this.l0 = GeneratedMessageV3.alwaysUseFieldBuilders ? N() : null;
                    }
                }
                if (this.n0 == null) {
                    if (!descriptorProto.enumType_.isEmpty()) {
                        if (this.m0.isEmpty()) {
                            this.m0 = descriptorProto.enumType_;
                            this.a &= -17;
                        } else {
                            k();
                            this.m0.addAll(descriptorProto.enumType_);
                        }
                        onChanged();
                    }
                } else if (!descriptorProto.enumType_.isEmpty()) {
                    if (!this.n0.u()) {
                        this.n0.b(descriptorProto.enumType_);
                    } else {
                        this.n0.i();
                        this.n0 = null;
                        this.m0 = descriptorProto.enumType_;
                        this.a &= -17;
                        this.n0 = GeneratedMessageV3.alwaysUseFieldBuilders ? A() : null;
                    }
                }
                if (this.p0 == null) {
                    if (!descriptorProto.extensionRange_.isEmpty()) {
                        if (this.o0.isEmpty()) {
                            this.o0 = descriptorProto.extensionRange_;
                            this.a &= -33;
                        } else {
                            o();
                            this.o0.addAll(descriptorProto.extensionRange_);
                        }
                        onChanged();
                    }
                } else if (!descriptorProto.extensionRange_.isEmpty()) {
                    if (!this.p0.u()) {
                        this.p0.b(descriptorProto.extensionRange_);
                    } else {
                        this.p0.i();
                        this.p0 = null;
                        this.o0 = descriptorProto.extensionRange_;
                        this.a &= -33;
                        this.p0 = GeneratedMessageV3.alwaysUseFieldBuilders ? H() : null;
                    }
                }
                if (this.r0 == null) {
                    if (!descriptorProto.oneofDecl_.isEmpty()) {
                        if (this.q0.isEmpty()) {
                            this.q0 = descriptorProto.oneofDecl_;
                            this.a &= -65;
                        } else {
                            s();
                            this.q0.addAll(descriptorProto.oneofDecl_);
                        }
                        onChanged();
                    }
                } else if (!descriptorProto.oneofDecl_.isEmpty()) {
                    if (!this.r0.u()) {
                        this.r0.b(descriptorProto.oneofDecl_);
                    } else {
                        this.r0.i();
                        this.r0 = null;
                        this.q0 = descriptorProto.oneofDecl_;
                        this.a &= -65;
                        this.r0 = GeneratedMessageV3.alwaysUseFieldBuilders ? Q() : null;
                    }
                }
                if (descriptorProto.hasOptions()) {
                    Y(descriptorProto.getOptions());
                }
                if (this.v0 == null) {
                    if (!descriptorProto.reservedRange_.isEmpty()) {
                        if (this.u0.isEmpty()) {
                            this.u0 = descriptorProto.reservedRange_;
                            this.a &= -257;
                        } else {
                            u();
                            this.u0.addAll(descriptorProto.reservedRange_);
                        }
                        onChanged();
                    }
                } else if (!descriptorProto.reservedRange_.isEmpty()) {
                    if (!this.v0.u()) {
                        this.v0.b(descriptorProto.reservedRange_);
                    } else {
                        this.v0.i();
                        this.v0 = null;
                        this.u0 = descriptorProto.reservedRange_;
                        this.a &= -257;
                        this.v0 = GeneratedMessageV3.alwaysUseFieldBuilders ? T() : null;
                    }
                }
                if (!descriptorProto.reservedName_.isEmpty()) {
                    if (this.w0.isEmpty()) {
                        this.w0 = descriptorProto.reservedName_;
                        this.a &= -513;
                    } else {
                        t();
                        this.w0.addAll(descriptorProto.reservedName_);
                    }
                    onChanged();
                }
                mergeUnknownFields(descriptorProto.unknownFields);
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            /* renamed from: X */
            public b mergeFrom(l0 l0Var) {
                if (l0Var instanceof DescriptorProto) {
                    return W((DescriptorProto) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public b Y(MessageOptions messageOptions) {
                MessageOptions messageOptions2;
                a1<MessageOptions, MessageOptions.b, k> a1Var = this.t0;
                if (a1Var == null) {
                    if ((this.a & 128) != 0 && (messageOptions2 = this.s0) != null && messageOptions2 != MessageOptions.getDefaultInstance()) {
                        this.s0 = MessageOptions.newBuilder(this.s0).G(messageOptions).buildPartial();
                    } else {
                        this.s0 = messageOptions;
                    }
                    onChanged();
                } else {
                    a1Var.h(messageOptions);
                }
                this.a |= 128;
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: Z */
            public final b mergeUnknownFields(g1 g1Var) {
                return (b) super.mergeUnknownFields(g1Var);
            }

            public b a(ExtensionRange extensionRange) {
                x0<ExtensionRange, ExtensionRange.b, c> x0Var = this.p0;
                if (x0Var == null) {
                    Objects.requireNonNull(extensionRange);
                    o();
                    this.o0.add(extensionRange);
                    onChanged();
                } else {
                    x0Var.f(extensionRange);
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: a0 */
            public b setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (b) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: b */
            public b addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (b) super.addRepeatedField(fieldDescriptor, obj);
            }

            public b b0(String str) {
                Objects.requireNonNull(str);
                this.a |= 1;
                this.f0 = str;
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: c0 */
            public b setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (b) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            /* renamed from: d */
            public DescriptorProto build() {
                DescriptorProto buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: d0 */
            public final b setUnknownFields(g1 g1Var) {
                return (b) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            /* renamed from: e */
            public DescriptorProto buildPartial() {
                DescriptorProto descriptorProto = new DescriptorProto(this);
                int i = this.a;
                int i2 = (i & 1) != 0 ? 1 : 0;
                descriptorProto.name_ = this.f0;
                x0<FieldDescriptorProto, FieldDescriptorProto.b, h> x0Var = this.h0;
                if (x0Var != null) {
                    descriptorProto.field_ = x0Var.g();
                } else {
                    if ((this.a & 2) != 0) {
                        this.g0 = Collections.unmodifiableList(this.g0);
                        this.a &= -3;
                    }
                    descriptorProto.field_ = this.g0;
                }
                x0<FieldDescriptorProto, FieldDescriptorProto.b, h> x0Var2 = this.j0;
                if (x0Var2 != null) {
                    descriptorProto.extension_ = x0Var2.g();
                } else {
                    if ((this.a & 4) != 0) {
                        this.i0 = Collections.unmodifiableList(this.i0);
                        this.a &= -5;
                    }
                    descriptorProto.extension_ = this.i0;
                }
                x0<DescriptorProto, b, b> x0Var3 = this.l0;
                if (x0Var3 != null) {
                    descriptorProto.nestedType_ = x0Var3.g();
                } else {
                    if ((this.a & 8) != 0) {
                        this.k0 = Collections.unmodifiableList(this.k0);
                        this.a &= -9;
                    }
                    descriptorProto.nestedType_ = this.k0;
                }
                x0<EnumDescriptorProto, EnumDescriptorProto.b, c> x0Var4 = this.n0;
                if (x0Var4 != null) {
                    descriptorProto.enumType_ = x0Var4.g();
                } else {
                    if ((this.a & 16) != 0) {
                        this.m0 = Collections.unmodifiableList(this.m0);
                        this.a &= -17;
                    }
                    descriptorProto.enumType_ = this.m0;
                }
                x0<ExtensionRange, ExtensionRange.b, c> x0Var5 = this.p0;
                if (x0Var5 != null) {
                    descriptorProto.extensionRange_ = x0Var5.g();
                } else {
                    if ((this.a & 32) != 0) {
                        this.o0 = Collections.unmodifiableList(this.o0);
                        this.a &= -33;
                    }
                    descriptorProto.extensionRange_ = this.o0;
                }
                x0<OneofDescriptorProto, OneofDescriptorProto.b, n> x0Var6 = this.r0;
                if (x0Var6 != null) {
                    descriptorProto.oneofDecl_ = x0Var6.g();
                } else {
                    if ((this.a & 64) != 0) {
                        this.q0 = Collections.unmodifiableList(this.q0);
                        this.a &= -65;
                    }
                    descriptorProto.oneofDecl_ = this.q0;
                }
                if ((i & 128) != 0) {
                    a1<MessageOptions, MessageOptions.b, k> a1Var = this.t0;
                    if (a1Var == null) {
                        descriptorProto.options_ = this.s0;
                    } else {
                        descriptorProto.options_ = a1Var.b();
                    }
                    i2 |= 2;
                }
                x0<ReservedRange, ReservedRange.b, d> x0Var7 = this.v0;
                if (x0Var7 != null) {
                    descriptorProto.reservedRange_ = x0Var7.g();
                } else {
                    if ((this.a & 256) != 0) {
                        this.u0 = Collections.unmodifiableList(this.u0);
                        this.a &= -257;
                    }
                    descriptorProto.reservedRange_ = this.u0;
                }
                if ((this.a & RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN) != 0) {
                    this.w0 = this.w0.x();
                    this.a &= -513;
                }
                descriptorProto.reservedName_ = this.w0;
                descriptorProto.bitField0_ = i2;
                onBuilt();
                return descriptorProto;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: f */
            public b clear() {
                super.clear();
                this.f0 = "";
                this.a &= -2;
                x0<FieldDescriptorProto, FieldDescriptorProto.b, h> x0Var = this.h0;
                if (x0Var == null) {
                    this.g0 = Collections.emptyList();
                    this.a &= -3;
                } else {
                    x0Var.h();
                }
                x0<FieldDescriptorProto, FieldDescriptorProto.b, h> x0Var2 = this.j0;
                if (x0Var2 == null) {
                    this.i0 = Collections.emptyList();
                    this.a &= -5;
                } else {
                    x0Var2.h();
                }
                x0<DescriptorProto, b, b> x0Var3 = this.l0;
                if (x0Var3 == null) {
                    this.k0 = Collections.emptyList();
                    this.a &= -9;
                } else {
                    x0Var3.h();
                }
                x0<EnumDescriptorProto, EnumDescriptorProto.b, c> x0Var4 = this.n0;
                if (x0Var4 == null) {
                    this.m0 = Collections.emptyList();
                    this.a &= -17;
                } else {
                    x0Var4.h();
                }
                x0<ExtensionRange, ExtensionRange.b, c> x0Var5 = this.p0;
                if (x0Var5 == null) {
                    this.o0 = Collections.emptyList();
                    this.a &= -33;
                } else {
                    x0Var5.h();
                }
                x0<OneofDescriptorProto, OneofDescriptorProto.b, n> x0Var6 = this.r0;
                if (x0Var6 == null) {
                    this.q0 = Collections.emptyList();
                    this.a &= -65;
                } else {
                    x0Var6.h();
                }
                a1<MessageOptions, MessageOptions.b, k> a1Var = this.t0;
                if (a1Var == null) {
                    this.s0 = null;
                } else {
                    a1Var.c();
                }
                this.a &= -129;
                x0<ReservedRange, ReservedRange.b, d> x0Var7 = this.v0;
                if (x0Var7 == null) {
                    this.u0 = Collections.emptyList();
                    this.a &= -257;
                } else {
                    x0Var7.h();
                }
                this.w0 = d0.h0;
                this.a &= -513;
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: g */
            public b clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (b) super.clearField(fieldDescriptor);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return DescriptorProtos.d;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: h */
            public b clearOneof(Descriptors.g gVar) {
                return (b) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return DescriptorProtos.e.d(DescriptorProto.class, b.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                for (int i = 0; i < J(); i++) {
                    if (!I(i).isInitialized()) {
                        return false;
                    }
                }
                for (int i2 = 0; i2 < C(); i2++) {
                    if (!B(i2).isInitialized()) {
                        return false;
                    }
                }
                for (int i3 = 0; i3 < M(); i3++) {
                    if (!L(i3).isInitialized()) {
                        return false;
                    }
                }
                for (int i4 = 0; i4 < y(); i4++) {
                    if (!x(i4).isInitialized()) {
                        return false;
                    }
                }
                for (int i5 = 0; i5 < G(); i5++) {
                    if (!E(i5).isInitialized()) {
                        return false;
                    }
                }
                for (int i6 = 0; i6 < P(); i6++) {
                    if (!O(i6).isInitialized()) {
                        return false;
                    }
                }
                return !U() || R().isInitialized();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            /* renamed from: j */
            public b clone() {
                return (b) super.clone();
            }

            public final void k() {
                if ((this.a & 16) == 0) {
                    this.m0 = new ArrayList(this.m0);
                    this.a |= 16;
                }
            }

            public final void l() {
                if ((this.a & 4) == 0) {
                    this.i0 = new ArrayList(this.i0);
                    this.a |= 4;
                }
            }

            public final void maybeForceBuilderInitialization() {
                if (GeneratedMessageV3.alwaysUseFieldBuilders) {
                    K();
                    D();
                    N();
                    A();
                    H();
                    Q();
                    S();
                    T();
                }
            }

            public final void o() {
                if ((this.a & 32) == 0) {
                    this.o0 = new ArrayList(this.o0);
                    this.a |= 32;
                }
            }

            public final void p() {
                if ((this.a & 2) == 0) {
                    this.g0 = new ArrayList(this.g0);
                    this.a |= 2;
                }
            }

            public final void r() {
                if ((this.a & 8) == 0) {
                    this.k0 = new ArrayList(this.k0);
                    this.a |= 8;
                }
            }

            public final void s() {
                if ((this.a & 64) == 0) {
                    this.q0 = new ArrayList(this.q0);
                    this.a |= 64;
                }
            }

            public final void t() {
                if ((this.a & RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN) == 0) {
                    this.w0 = new d0(this.w0);
                    this.a |= RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN;
                }
            }

            public final void u() {
                if ((this.a & 256) == 0) {
                    this.u0 = new ArrayList(this.u0);
                    this.a |= 256;
                }
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            /* renamed from: v */
            public DescriptorProto getDefaultInstanceForType() {
                return DescriptorProto.getDefaultInstance();
            }

            public EnumDescriptorProto x(int i) {
                x0<EnumDescriptorProto, EnumDescriptorProto.b, c> x0Var = this.n0;
                if (x0Var == null) {
                    return this.m0.get(i);
                }
                return x0Var.o(i);
            }

            public int y() {
                x0<EnumDescriptorProto, EnumDescriptorProto.b, c> x0Var = this.n0;
                if (x0Var == null) {
                    return this.m0.size();
                }
                return x0Var.n();
            }

            public b() {
                this.f0 = "";
                this.g0 = Collections.emptyList();
                this.i0 = Collections.emptyList();
                this.k0 = Collections.emptyList();
                this.m0 = Collections.emptyList();
                this.o0 = Collections.emptyList();
                this.q0 = Collections.emptyList();
                this.u0 = Collections.emptyList();
                this.w0 = d0.h0;
                maybeForceBuilderInitialization();
            }

            public b(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.f0 = "";
                this.g0 = Collections.emptyList();
                this.i0 = Collections.emptyList();
                this.k0 = Collections.emptyList();
                this.m0 = Collections.emptyList();
                this.o0 = Collections.emptyList();
                this.q0 = Collections.emptyList();
                this.u0 = Collections.emptyList();
                this.w0 = d0.h0;
                maybeForceBuilderInitialization();
            }
        }

        public static b newBuilder(DescriptorProto descriptorProto) {
            return a.toBuilder().W(descriptorProto);
        }

        public static DescriptorProto parseFrom(ByteBuffer byteBuffer, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        /* renamed from: getReservedNameList */
        public dw2 m15getReservedNameList() {
            return this.reservedName_;
        }

        public DescriptorProto(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static DescriptorProto parseDelimitedFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
            return (DescriptorProto) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static DescriptorProto parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public DescriptorProto getDefaultInstanceForType() {
            return a;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public b toBuilder() {
            return this == a ? new b() : new b().W(this);
        }

        public static DescriptorProto parseFrom(ByteString byteString, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public b newBuilderForType() {
            return newBuilder();
        }

        public DescriptorProto() {
            this.memoizedIsInitialized = (byte) -1;
            this.name_ = "";
            this.field_ = Collections.emptyList();
            this.extension_ = Collections.emptyList();
            this.nestedType_ = Collections.emptyList();
            this.enumType_ = Collections.emptyList();
            this.extensionRange_ = Collections.emptyList();
            this.oneofDecl_ = Collections.emptyList();
            this.reservedRange_ = Collections.emptyList();
            this.reservedName_ = d0.h0;
        }

        public static DescriptorProto parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public b newBuilderForType(GeneratedMessageV3.c cVar) {
            return new b(cVar);
        }

        public static DescriptorProto parseFrom(byte[] bArr, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static DescriptorProto parseFrom(InputStream inputStream) throws IOException {
            return (DescriptorProto) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static DescriptorProto parseFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
            return (DescriptorProto) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static DescriptorProto parseFrom(com.google.protobuf.j jVar) throws IOException {
            return (DescriptorProto) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static DescriptorProto parseFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws IOException {
            return (DescriptorProto) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }

        /* JADX WARN: Multi-variable type inference failed */
        public DescriptorProto(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            boolean z2 = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            switch (J) {
                                case 0:
                                    break;
                                case 10:
                                    ByteString q = jVar.q();
                                    this.bitField0_ = 1 | this.bitField0_;
                                    this.name_ = q;
                                    continue;
                                case 18:
                                    if (!(z2 & true)) {
                                        this.field_ = new ArrayList();
                                        z2 |= true;
                                    }
                                    this.field_.add(jVar.z(FieldDescriptorProto.PARSER, rVar));
                                    continue;
                                case 26:
                                    if (!(z2 & true)) {
                                        this.nestedType_ = new ArrayList();
                                        z2 |= true;
                                    }
                                    this.nestedType_.add(jVar.z(PARSER, rVar));
                                    continue;
                                case 34:
                                    if (!(z2 & true)) {
                                        this.enumType_ = new ArrayList();
                                        z2 |= true;
                                    }
                                    this.enumType_.add(jVar.z(EnumDescriptorProto.PARSER, rVar));
                                    continue;
                                case 42:
                                    if (!(z2 & true)) {
                                        this.extensionRange_ = new ArrayList();
                                        z2 |= true;
                                    }
                                    this.extensionRange_.add(jVar.z(ExtensionRange.PARSER, rVar));
                                    continue;
                                case 50:
                                    if (!(z2 & true)) {
                                        this.extension_ = new ArrayList();
                                        z2 |= true;
                                    }
                                    this.extension_.add(jVar.z(FieldDescriptorProto.PARSER, rVar));
                                    continue;
                                case 58:
                                    MessageOptions.b builder = (this.bitField0_ & 2) != 0 ? this.options_.toBuilder() : null;
                                    MessageOptions messageOptions = (MessageOptions) jVar.z(MessageOptions.PARSER, rVar);
                                    this.options_ = messageOptions;
                                    if (builder != null) {
                                        builder.G(messageOptions);
                                        this.options_ = builder.buildPartial();
                                    }
                                    this.bitField0_ |= 2;
                                    continue;
                                case 66:
                                    if (!(z2 & true)) {
                                        this.oneofDecl_ = new ArrayList();
                                        z2 |= true;
                                    }
                                    this.oneofDecl_.add(jVar.z(OneofDescriptorProto.PARSER, rVar));
                                    continue;
                                case 74:
                                    if (!(z2 & true)) {
                                        this.reservedRange_ = new ArrayList();
                                        z2 |= true;
                                    }
                                    this.reservedRange_.add(jVar.z(ReservedRange.PARSER, rVar));
                                    continue;
                                case 82:
                                    ByteString q2 = jVar.q();
                                    if (!(z2 & true)) {
                                        this.reservedName_ = new d0();
                                        z2 |= true;
                                    }
                                    this.reservedName_.W(q2);
                                    continue;
                                default:
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                        break;
                                    } else {
                                        continue;
                                    }
                            }
                            z = true;
                        } catch (IOException e) {
                            throw new InvalidProtocolBufferException(e).setUnfinishedMessage(this);
                        }
                    } catch (InvalidProtocolBufferException e2) {
                        throw e2.setUnfinishedMessage(this);
                    }
                } finally {
                    if (z2 & true) {
                        this.field_ = Collections.unmodifiableList(this.field_);
                    }
                    if (z2 & true) {
                        this.nestedType_ = Collections.unmodifiableList(this.nestedType_);
                    }
                    if (z2 & true) {
                        this.enumType_ = Collections.unmodifiableList(this.enumType_);
                    }
                    if (z2 & true) {
                        this.extensionRange_ = Collections.unmodifiableList(this.extensionRange_);
                    }
                    if (z2 & true) {
                        this.extension_ = Collections.unmodifiableList(this.extension_);
                    }
                    if (z2 & true) {
                        this.oneofDecl_ = Collections.unmodifiableList(this.oneofDecl_);
                    }
                    if (z2 & true) {
                        this.reservedRange_ = Collections.unmodifiableList(this.reservedRange_);
                    }
                    if (z2 & true) {
                        this.reservedName_ = this.reservedName_.x();
                    }
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }
    }

    /* loaded from: classes2.dex */
    public static final class EnumDescriptorProto extends GeneratedMessageV3 implements c {
        public static final int NAME_FIELD_NUMBER = 1;
        public static final int OPTIONS_FIELD_NUMBER = 3;
        public static final int RESERVED_NAME_FIELD_NUMBER = 5;
        public static final int RESERVED_RANGE_FIELD_NUMBER = 4;
        public static final int VALUE_FIELD_NUMBER = 2;
        private static final long serialVersionUID = 0;
        private int bitField0_;
        private byte memoizedIsInitialized;
        private volatile Object name_;
        private EnumOptions options_;
        private cz1 reservedName_;
        private List<EnumReservedRange> reservedRange_;
        private List<EnumValueDescriptorProto> value_;
        public static final EnumDescriptorProto a = new EnumDescriptorProto();
        @Deprecated
        public static final t0<EnumDescriptorProto> PARSER = new a();

        /* loaded from: classes2.dex */
        public static final class EnumReservedRange extends GeneratedMessageV3 implements c {
            public static final int END_FIELD_NUMBER = 2;
            public static final int START_FIELD_NUMBER = 1;
            private static final long serialVersionUID = 0;
            private int bitField0_;
            private int end_;
            private byte memoizedIsInitialized;
            private int start_;
            public static final EnumReservedRange a = new EnumReservedRange();
            @Deprecated
            public static final t0<EnumReservedRange> PARSER = new a();

            /* loaded from: classes2.dex */
            public static class a extends com.google.protobuf.c<EnumReservedRange> {
                @Override // com.google.protobuf.t0
                /* renamed from: a */
                public EnumReservedRange parsePartialFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
                    return new EnumReservedRange(jVar, rVar);
                }
            }

            public static EnumReservedRange getDefaultInstance() {
                return a;
            }

            public static final Descriptors.b getDescriptor() {
                return DescriptorProtos.r;
            }

            public static b newBuilder() {
                return a.toBuilder();
            }

            public static EnumReservedRange parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (EnumReservedRange) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
            }

            public static EnumReservedRange parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer);
            }

            public static t0<EnumReservedRange> parser() {
                return PARSER;
            }

            @Override // com.google.protobuf.a
            public boolean equals(Object obj) {
                if (obj == this) {
                    return true;
                }
                if (!(obj instanceof EnumReservedRange)) {
                    return super.equals(obj);
                }
                EnumReservedRange enumReservedRange = (EnumReservedRange) obj;
                if (hasStart() != enumReservedRange.hasStart()) {
                    return false;
                }
                if ((!hasStart() || getStart() == enumReservedRange.getStart()) && hasEnd() == enumReservedRange.hasEnd()) {
                    return (!hasEnd() || getEnd() == enumReservedRange.getEnd()) && this.unknownFields.equals(enumReservedRange.unknownFields);
                }
                return false;
            }

            public int getEnd() {
                return this.end_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
            public t0<EnumReservedRange> getParserForType() {
                return PARSER;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public int getSerializedSize() {
                int i = this.memoizedSize;
                if (i != -1) {
                    return i;
                }
                int x = (this.bitField0_ & 1) != 0 ? 0 + CodedOutputStream.x(1, this.start_) : 0;
                if ((this.bitField0_ & 2) != 0) {
                    x += CodedOutputStream.x(2, this.end_);
                }
                int serializedSize = x + this.unknownFields.getSerializedSize();
                this.memoizedSize = serializedSize;
                return serializedSize;
            }

            public int getStart() {
                return this.start_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
            public final g1 getUnknownFields() {
                return this.unknownFields;
            }

            public boolean hasEnd() {
                return (this.bitField0_ & 2) != 0;
            }

            public boolean hasStart() {
                return (this.bitField0_ & 1) != 0;
            }

            @Override // com.google.protobuf.a
            public int hashCode() {
                int i = this.memoizedHashCode;
                if (i != 0) {
                    return i;
                }
                int hashCode = 779 + getDescriptor().hashCode();
                if (hasStart()) {
                    hashCode = (((hashCode * 37) + 1) * 53) + getStart();
                }
                if (hasEnd()) {
                    hashCode = (((hashCode * 37) + 2) * 53) + getEnd();
                }
                int hashCode2 = (hashCode * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode2;
                return hashCode2;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return DescriptorProtos.s.d(EnumReservedRange.class, b.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
            public final boolean isInitialized() {
                byte b2 = this.memoizedIsInitialized;
                if (b2 == 1) {
                    return true;
                }
                if (b2 == 0) {
                    return false;
                }
                this.memoizedIsInitialized = (byte) 1;
                return true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Object newInstance(GeneratedMessageV3.f fVar) {
                return new EnumReservedRange();
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
                if ((this.bitField0_ & 1) != 0) {
                    codedOutputStream.G0(1, this.start_);
                }
                if ((this.bitField0_ & 2) != 0) {
                    codedOutputStream.G0(2, this.end_);
                }
                this.unknownFields.writeTo(codedOutputStream);
            }

            /* loaded from: classes2.dex */
            public static final class b extends GeneratedMessageV3.b<b> implements c {
                public int a;
                public int f0;
                public int g0;

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                /* renamed from: a */
                public b addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (b) super.addRepeatedField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                /* renamed from: b */
                public EnumReservedRange build() {
                    EnumReservedRange buildPartial = buildPartial();
                    if (buildPartial.isInitialized()) {
                        return buildPartial;
                    }
                    throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                /* renamed from: d */
                public EnumReservedRange buildPartial() {
                    int i;
                    EnumReservedRange enumReservedRange = new EnumReservedRange(this);
                    int i2 = this.a;
                    if ((i2 & 1) != 0) {
                        enumReservedRange.start_ = this.f0;
                        i = 1;
                    } else {
                        i = 0;
                    }
                    if ((i2 & 2) != 0) {
                        enumReservedRange.end_ = this.g0;
                        i |= 2;
                    }
                    enumReservedRange.bitField0_ = i;
                    onBuilt();
                    return enumReservedRange;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                /* renamed from: e */
                public b clear() {
                    super.clear();
                    this.f0 = 0;
                    int i = this.a & (-2);
                    this.a = i;
                    this.g0 = 0;
                    this.a = i & (-3);
                    return this;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                /* renamed from: f */
                public b clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                    return (b) super.clearField(fieldDescriptor);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                /* renamed from: g */
                public b clearOneof(Descriptors.g gVar) {
                    return (b) super.clearOneof(gVar);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
                public Descriptors.b getDescriptorForType() {
                    return DescriptorProtos.r;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
                /* renamed from: h */
                public b clone() {
                    return (b) super.clone();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                    return DescriptorProtos.s.d(EnumReservedRange.class, b.class);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
                public final boolean isInitialized() {
                    return true;
                }

                @Override // defpackage.d82, com.google.protobuf.o0
                /* renamed from: j */
                public EnumReservedRange getDefaultInstanceForType() {
                    return EnumReservedRange.getDefaultInstance();
                }

                /* JADX WARN: Removed duplicated region for block: B:16:0x0021  */
                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
                /* renamed from: k */
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct code enable 'Show inconsistent code' option in preferences
                */
                public com.google.protobuf.DescriptorProtos.EnumDescriptorProto.EnumReservedRange.b mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                    /*
                        r2 = this;
                        r0 = 0
                        com.google.protobuf.t0<com.google.protobuf.DescriptorProtos$EnumDescriptorProto$EnumReservedRange> r1 = com.google.protobuf.DescriptorProtos.EnumDescriptorProto.EnumReservedRange.PARSER     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                        java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                        com.google.protobuf.DescriptorProtos$EnumDescriptorProto$EnumReservedRange r3 = (com.google.protobuf.DescriptorProtos.EnumDescriptorProto.EnumReservedRange) r3     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                        if (r3 == 0) goto Le
                        r2.l(r3)
                    Le:
                        return r2
                    Lf:
                        r3 = move-exception
                        goto L1f
                    L11:
                        r3 = move-exception
                        com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> Lf
                        com.google.protobuf.DescriptorProtos$EnumDescriptorProto$EnumReservedRange r4 = (com.google.protobuf.DescriptorProtos.EnumDescriptorProto.EnumReservedRange) r4     // Catch: java.lang.Throwable -> Lf
                        java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1d
                        throw r3     // Catch: java.lang.Throwable -> L1d
                    L1d:
                        r3 = move-exception
                        r0 = r4
                    L1f:
                        if (r0 == 0) goto L24
                        r2.l(r0)
                    L24:
                        throw r3
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.DescriptorProtos.EnumDescriptorProto.EnumReservedRange.b.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):com.google.protobuf.DescriptorProtos$EnumDescriptorProto$EnumReservedRange$b");
                }

                public b l(EnumReservedRange enumReservedRange) {
                    if (enumReservedRange == EnumReservedRange.getDefaultInstance()) {
                        return this;
                    }
                    if (enumReservedRange.hasStart()) {
                        u(enumReservedRange.getStart());
                    }
                    if (enumReservedRange.hasEnd()) {
                        r(enumReservedRange.getEnd());
                    }
                    mergeUnknownFields(enumReservedRange.unknownFields);
                    onChanged();
                    return this;
                }

                public final void maybeForceBuilderInitialization() {
                    boolean z = GeneratedMessageV3.alwaysUseFieldBuilders;
                }

                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
                /* renamed from: o */
                public b mergeFrom(l0 l0Var) {
                    if (l0Var instanceof EnumReservedRange) {
                        return l((EnumReservedRange) l0Var);
                    }
                    super.mergeFrom(l0Var);
                    return this;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                /* renamed from: p */
                public final b mergeUnknownFields(g1 g1Var) {
                    return (b) super.mergeUnknownFields(g1Var);
                }

                public b r(int i) {
                    this.a |= 2;
                    this.g0 = i;
                    onChanged();
                    return this;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                /* renamed from: s */
                public b setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (b) super.setField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                /* renamed from: t */
                public b setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                    return (b) super.m19setRepeatedField(fieldDescriptor, i, obj);
                }

                public b u(int i) {
                    this.a |= 1;
                    this.f0 = i;
                    onChanged();
                    return this;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                /* renamed from: v */
                public final b setUnknownFields(g1 g1Var) {
                    return (b) super.setUnknownFields(g1Var);
                }

                public b() {
                    maybeForceBuilderInitialization();
                }

                public b(GeneratedMessageV3.c cVar) {
                    super(cVar);
                    maybeForceBuilderInitialization();
                }
            }

            public static b newBuilder(EnumReservedRange enumReservedRange) {
                return a.toBuilder().l(enumReservedRange);
            }

            public static EnumReservedRange parseFrom(ByteBuffer byteBuffer, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer, rVar);
            }

            public EnumReservedRange(GeneratedMessageV3.b<?> bVar) {
                super(bVar);
                this.memoizedIsInitialized = (byte) -1;
            }

            public static EnumReservedRange parseDelimitedFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
                return (EnumReservedRange) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
            }

            public static EnumReservedRange parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
            public EnumReservedRange getDefaultInstanceForType() {
                return a;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public b toBuilder() {
                return this == a ? new b() : new b().l(this);
            }

            public static EnumReservedRange parseFrom(ByteString byteString, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString, rVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public b newBuilderForType() {
                return newBuilder();
            }

            public EnumReservedRange() {
                this.memoizedIsInitialized = (byte) -1;
            }

            public static EnumReservedRange parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr);
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public b newBuilderForType(GeneratedMessageV3.c cVar) {
                return new b(cVar);
            }

            public static EnumReservedRange parseFrom(byte[] bArr, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr, rVar);
            }

            public EnumReservedRange(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
                this();
                Objects.requireNonNull(rVar);
                g1.b g = g1.g();
                boolean z = false;
                while (!z) {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 8) {
                                    this.bitField0_ |= 1;
                                    this.start_ = jVar.x();
                                } else if (J != 16) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.bitField0_ |= 2;
                                    this.end_ = jVar.x();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        } catch (IOException e2) {
                            throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                        }
                    } finally {
                        this.unknownFields = g.build();
                        makeExtensionsImmutable();
                    }
                }
            }

            public static EnumReservedRange parseFrom(InputStream inputStream) throws IOException {
                return (EnumReservedRange) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
            }

            public static EnumReservedRange parseFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
                return (EnumReservedRange) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
            }

            public static EnumReservedRange parseFrom(com.google.protobuf.j jVar) throws IOException {
                return (EnumReservedRange) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
            }

            public static EnumReservedRange parseFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws IOException {
                return (EnumReservedRange) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
            }
        }

        /* loaded from: classes2.dex */
        public static class a extends com.google.protobuf.c<EnumDescriptorProto> {
            @Override // com.google.protobuf.t0
            /* renamed from: a */
            public EnumDescriptorProto parsePartialFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
                return new EnumDescriptorProto(jVar, rVar);
            }
        }

        /* loaded from: classes2.dex */
        public interface c extends o0 {
        }

        public static EnumDescriptorProto getDefaultInstance() {
            return a;
        }

        public static final Descriptors.b getDescriptor() {
            return DescriptorProtos.p;
        }

        public static b newBuilder() {
            return a.toBuilder();
        }

        public static EnumDescriptorProto parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (EnumDescriptorProto) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static EnumDescriptorProto parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<EnumDescriptorProto> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof EnumDescriptorProto)) {
                return super.equals(obj);
            }
            EnumDescriptorProto enumDescriptorProto = (EnumDescriptorProto) obj;
            if (hasName() != enumDescriptorProto.hasName()) {
                return false;
            }
            if ((!hasName() || getName().equals(enumDescriptorProto.getName())) && getValueList().equals(enumDescriptorProto.getValueList()) && hasOptions() == enumDescriptorProto.hasOptions()) {
                return (!hasOptions() || getOptions().equals(enumDescriptorProto.getOptions())) && getReservedRangeList().equals(enumDescriptorProto.getReservedRangeList()) && m16getReservedNameList().equals(enumDescriptorProto.m16getReservedNameList()) && this.unknownFields.equals(enumDescriptorProto.unknownFields);
            }
            return false;
        }

        public String getName() {
            Object obj = this.name_;
            if (obj instanceof String) {
                return (String) obj;
            }
            ByteString byteString = (ByteString) obj;
            String stringUtf8 = byteString.toStringUtf8();
            if (byteString.isValidUtf8()) {
                this.name_ = stringUtf8;
            }
            return stringUtf8;
        }

        public ByteString getNameBytes() {
            Object obj = this.name_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.name_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        public EnumOptions getOptions() {
            EnumOptions enumOptions = this.options_;
            return enumOptions == null ? EnumOptions.getDefaultInstance() : enumOptions;
        }

        public d getOptionsOrBuilder() {
            EnumOptions enumOptions = this.options_;
            return enumOptions == null ? EnumOptions.getDefaultInstance() : enumOptions;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<EnumDescriptorProto> getParserForType() {
            return PARSER;
        }

        public String getReservedName(int i) {
            return this.reservedName_.get(i);
        }

        public ByteString getReservedNameBytes(int i) {
            return this.reservedName_.e1(i);
        }

        public int getReservedNameCount() {
            return this.reservedName_.size();
        }

        public EnumReservedRange getReservedRange(int i) {
            return this.reservedRange_.get(i);
        }

        public int getReservedRangeCount() {
            return this.reservedRange_.size();
        }

        public List<EnumReservedRange> getReservedRangeList() {
            return this.reservedRange_;
        }

        public c getReservedRangeOrBuilder(int i) {
            return this.reservedRange_.get(i);
        }

        public List<? extends c> getReservedRangeOrBuilderList() {
            return this.reservedRange_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = (this.bitField0_ & 1) != 0 ? GeneratedMessageV3.computeStringSize(1, this.name_) + 0 : 0;
            for (int i2 = 0; i2 < this.value_.size(); i2++) {
                computeStringSize += CodedOutputStream.G(2, this.value_.get(i2));
            }
            if ((this.bitField0_ & 2) != 0) {
                computeStringSize += CodedOutputStream.G(3, getOptions());
            }
            for (int i3 = 0; i3 < this.reservedRange_.size(); i3++) {
                computeStringSize += CodedOutputStream.G(4, this.reservedRange_.get(i3));
            }
            int i4 = 0;
            for (int i5 = 0; i5 < this.reservedName_.size(); i5++) {
                i4 += GeneratedMessageV3.computeStringSizeNoTag(this.reservedName_.j(i5));
            }
            int size = computeStringSize + i4 + (m16getReservedNameList().size() * 1) + this.unknownFields.getSerializedSize();
            this.memoizedSize = size;
            return size;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        public EnumValueDescriptorProto getValue(int i) {
            return this.value_.get(i);
        }

        public int getValueCount() {
            return this.value_.size();
        }

        public List<EnumValueDescriptorProto> getValueList() {
            return this.value_;
        }

        public e getValueOrBuilder(int i) {
            return this.value_.get(i);
        }

        public List<? extends e> getValueOrBuilderList() {
            return this.value_;
        }

        public boolean hasName() {
            return (this.bitField0_ & 1) != 0;
        }

        public boolean hasOptions() {
            return (this.bitField0_ & 2) != 0;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = 779 + getDescriptor().hashCode();
            if (hasName()) {
                hashCode = (((hashCode * 37) + 1) * 53) + getName().hashCode();
            }
            if (getValueCount() > 0) {
                hashCode = (((hashCode * 37) + 2) * 53) + getValueList().hashCode();
            }
            if (hasOptions()) {
                hashCode = (((hashCode * 37) + 3) * 53) + getOptions().hashCode();
            }
            if (getReservedRangeCount() > 0) {
                hashCode = (((hashCode * 37) + 4) * 53) + getReservedRangeList().hashCode();
            }
            if (getReservedNameCount() > 0) {
                hashCode = (((hashCode * 37) + 5) * 53) + m16getReservedNameList().hashCode();
            }
            int hashCode2 = (hashCode * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode2;
            return hashCode2;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return DescriptorProtos.q.d(EnumDescriptorProto.class, b.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b2 = this.memoizedIsInitialized;
            if (b2 == 1) {
                return true;
            }
            if (b2 == 0) {
                return false;
            }
            for (int i = 0; i < getValueCount(); i++) {
                if (!getValue(i).isInitialized()) {
                    this.memoizedIsInitialized = (byte) 0;
                    return false;
                }
            }
            if (hasOptions() && !getOptions().isInitialized()) {
                this.memoizedIsInitialized = (byte) 0;
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new EnumDescriptorProto();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if ((this.bitField0_ & 1) != 0) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.name_);
            }
            for (int i = 0; i < this.value_.size(); i++) {
                codedOutputStream.K0(2, this.value_.get(i));
            }
            if ((this.bitField0_ & 2) != 0) {
                codedOutputStream.K0(3, getOptions());
            }
            for (int i2 = 0; i2 < this.reservedRange_.size(); i2++) {
                codedOutputStream.K0(4, this.reservedRange_.get(i2));
            }
            for (int i3 = 0; i3 < this.reservedName_.size(); i3++) {
                GeneratedMessageV3.writeString(codedOutputStream, 5, this.reservedName_.j(i3));
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes2.dex */
        public static final class b extends GeneratedMessageV3.b<b> implements c {
            public int a;
            public Object f0;
            public List<EnumValueDescriptorProto> g0;
            public x0<EnumValueDescriptorProto, EnumValueDescriptorProto.b, e> h0;
            public EnumOptions i0;
            public a1<EnumOptions, EnumOptions.b, d> j0;
            public List<EnumReservedRange> k0;
            public x0<EnumReservedRange, EnumReservedRange.b, c> l0;
            public cz1 m0;

            public b A(EnumDescriptorProto enumDescriptorProto) {
                if (enumDescriptorProto == EnumDescriptorProto.getDefaultInstance()) {
                    return this;
                }
                if (enumDescriptorProto.hasName()) {
                    this.a |= 1;
                    this.f0 = enumDescriptorProto.name_;
                    onChanged();
                }
                if (this.h0 == null) {
                    if (!enumDescriptorProto.value_.isEmpty()) {
                        if (this.g0.isEmpty()) {
                            this.g0 = enumDescriptorProto.value_;
                            this.a &= -3;
                        } else {
                            l();
                            this.g0.addAll(enumDescriptorProto.value_);
                        }
                        onChanged();
                    }
                } else if (!enumDescriptorProto.value_.isEmpty()) {
                    if (!this.h0.u()) {
                        this.h0.b(enumDescriptorProto.value_);
                    } else {
                        this.h0.i();
                        this.h0 = null;
                        this.g0 = enumDescriptorProto.value_;
                        this.a &= -3;
                        this.h0 = GeneratedMessageV3.alwaysUseFieldBuilders ? v() : null;
                    }
                }
                if (enumDescriptorProto.hasOptions()) {
                    C(enumDescriptorProto.getOptions());
                }
                if (this.l0 == null) {
                    if (!enumDescriptorProto.reservedRange_.isEmpty()) {
                        if (this.k0.isEmpty()) {
                            this.k0 = enumDescriptorProto.reservedRange_;
                            this.a &= -9;
                        } else {
                            k();
                            this.k0.addAll(enumDescriptorProto.reservedRange_);
                        }
                        onChanged();
                    }
                } else if (!enumDescriptorProto.reservedRange_.isEmpty()) {
                    if (!this.l0.u()) {
                        this.l0.b(enumDescriptorProto.reservedRange_);
                    } else {
                        this.l0.i();
                        this.l0 = null;
                        this.k0 = enumDescriptorProto.reservedRange_;
                        this.a &= -9;
                        this.l0 = GeneratedMessageV3.alwaysUseFieldBuilders ? s() : null;
                    }
                }
                if (!enumDescriptorProto.reservedName_.isEmpty()) {
                    if (this.m0.isEmpty()) {
                        this.m0 = enumDescriptorProto.reservedName_;
                        this.a &= -17;
                    } else {
                        j();
                        this.m0.addAll(enumDescriptorProto.reservedName_);
                    }
                    onChanged();
                }
                mergeUnknownFields(enumDescriptorProto.unknownFields);
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            /* renamed from: B */
            public b mergeFrom(l0 l0Var) {
                if (l0Var instanceof EnumDescriptorProto) {
                    return A((EnumDescriptorProto) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public b C(EnumOptions enumOptions) {
                EnumOptions enumOptions2;
                a1<EnumOptions, EnumOptions.b, d> a1Var = this.j0;
                if (a1Var == null) {
                    if ((this.a & 4) != 0 && (enumOptions2 = this.i0) != null && enumOptions2 != EnumOptions.getDefaultInstance()) {
                        this.i0 = EnumOptions.newBuilder(this.i0).G(enumOptions).buildPartial();
                    } else {
                        this.i0 = enumOptions;
                    }
                    onChanged();
                } else {
                    a1Var.h(enumOptions);
                }
                this.a |= 4;
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: D */
            public final b mergeUnknownFields(g1 g1Var) {
                return (b) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: E */
            public b setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (b) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: G */
            public b setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (b) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: H */
            public final b setUnknownFields(g1 g1Var) {
                return (b) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: a */
            public b addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (b) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            /* renamed from: b */
            public EnumDescriptorProto build() {
                EnumDescriptorProto buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            /* renamed from: d */
            public EnumDescriptorProto buildPartial() {
                EnumDescriptorProto enumDescriptorProto = new EnumDescriptorProto(this);
                int i = this.a;
                int i2 = (i & 1) != 0 ? 1 : 0;
                enumDescriptorProto.name_ = this.f0;
                x0<EnumValueDescriptorProto, EnumValueDescriptorProto.b, e> x0Var = this.h0;
                if (x0Var != null) {
                    enumDescriptorProto.value_ = x0Var.g();
                } else {
                    if ((this.a & 2) != 0) {
                        this.g0 = Collections.unmodifiableList(this.g0);
                        this.a &= -3;
                    }
                    enumDescriptorProto.value_ = this.g0;
                }
                if ((i & 4) != 0) {
                    a1<EnumOptions, EnumOptions.b, d> a1Var = this.j0;
                    if (a1Var == null) {
                        enumDescriptorProto.options_ = this.i0;
                    } else {
                        enumDescriptorProto.options_ = a1Var.b();
                    }
                    i2 |= 2;
                }
                x0<EnumReservedRange, EnumReservedRange.b, c> x0Var2 = this.l0;
                if (x0Var2 != null) {
                    enumDescriptorProto.reservedRange_ = x0Var2.g();
                } else {
                    if ((this.a & 8) != 0) {
                        this.k0 = Collections.unmodifiableList(this.k0);
                        this.a &= -9;
                    }
                    enumDescriptorProto.reservedRange_ = this.k0;
                }
                if ((this.a & 16) != 0) {
                    this.m0 = this.m0.x();
                    this.a &= -17;
                }
                enumDescriptorProto.reservedName_ = this.m0;
                enumDescriptorProto.bitField0_ = i2;
                onBuilt();
                return enumDescriptorProto;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: e */
            public b clear() {
                super.clear();
                this.f0 = "";
                this.a &= -2;
                x0<EnumValueDescriptorProto, EnumValueDescriptorProto.b, e> x0Var = this.h0;
                if (x0Var == null) {
                    this.g0 = Collections.emptyList();
                    this.a &= -3;
                } else {
                    x0Var.h();
                }
                a1<EnumOptions, EnumOptions.b, d> a1Var = this.j0;
                if (a1Var == null) {
                    this.i0 = null;
                } else {
                    a1Var.c();
                }
                this.a &= -5;
                x0<EnumReservedRange, EnumReservedRange.b, c> x0Var2 = this.l0;
                if (x0Var2 == null) {
                    this.k0 = Collections.emptyList();
                    this.a &= -9;
                } else {
                    x0Var2.h();
                }
                this.m0 = d0.h0;
                this.a &= -17;
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: f */
            public b clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (b) super.clearField(fieldDescriptor);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: g */
            public b clearOneof(Descriptors.g gVar) {
                return (b) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return DescriptorProtos.p;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            /* renamed from: h */
            public b clone() {
                return (b) super.clone();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return DescriptorProtos.q.d(EnumDescriptorProto.class, b.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                for (int i = 0; i < u(); i++) {
                    if (!t(i).isInitialized()) {
                        return false;
                    }
                }
                return !x() || p().isInitialized();
            }

            public final void j() {
                if ((this.a & 16) == 0) {
                    this.m0 = new d0(this.m0);
                    this.a |= 16;
                }
            }

            public final void k() {
                if ((this.a & 8) == 0) {
                    this.k0 = new ArrayList(this.k0);
                    this.a |= 8;
                }
            }

            public final void l() {
                if ((this.a & 2) == 0) {
                    this.g0 = new ArrayList(this.g0);
                    this.a |= 2;
                }
            }

            public final void maybeForceBuilderInitialization() {
                if (GeneratedMessageV3.alwaysUseFieldBuilders) {
                    v();
                    r();
                    s();
                }
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            /* renamed from: o */
            public EnumDescriptorProto getDefaultInstanceForType() {
                return EnumDescriptorProto.getDefaultInstance();
            }

            public EnumOptions p() {
                a1<EnumOptions, EnumOptions.b, d> a1Var = this.j0;
                if (a1Var == null) {
                    EnumOptions enumOptions = this.i0;
                    return enumOptions == null ? EnumOptions.getDefaultInstance() : enumOptions;
                }
                return a1Var.f();
            }

            public final a1<EnumOptions, EnumOptions.b, d> r() {
                if (this.j0 == null) {
                    this.j0 = new a1<>(p(), getParentForChildren(), isClean());
                    this.i0 = null;
                }
                return this.j0;
            }

            public final x0<EnumReservedRange, EnumReservedRange.b, c> s() {
                if (this.l0 == null) {
                    this.l0 = new x0<>(this.k0, (this.a & 8) != 0, getParentForChildren(), isClean());
                    this.k0 = null;
                }
                return this.l0;
            }

            public EnumValueDescriptorProto t(int i) {
                x0<EnumValueDescriptorProto, EnumValueDescriptorProto.b, e> x0Var = this.h0;
                if (x0Var == null) {
                    return this.g0.get(i);
                }
                return x0Var.o(i);
            }

            public int u() {
                x0<EnumValueDescriptorProto, EnumValueDescriptorProto.b, e> x0Var = this.h0;
                if (x0Var == null) {
                    return this.g0.size();
                }
                return x0Var.n();
            }

            public final x0<EnumValueDescriptorProto, EnumValueDescriptorProto.b, e> v() {
                if (this.h0 == null) {
                    this.h0 = new x0<>(this.g0, (this.a & 2) != 0, getParentForChildren(), isClean());
                    this.g0 = null;
                }
                return this.h0;
            }

            public boolean x() {
                return (this.a & 4) != 0;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0021  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /* renamed from: y */
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public com.google.protobuf.DescriptorProtos.EnumDescriptorProto.b mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0<com.google.protobuf.DescriptorProtos$EnumDescriptorProto> r1 = com.google.protobuf.DescriptorProtos.EnumDescriptorProto.PARSER     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    com.google.protobuf.DescriptorProtos$EnumDescriptorProto r3 = (com.google.protobuf.DescriptorProtos.EnumDescriptorProto) r3     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    if (r3 == 0) goto Le
                    r2.A(r3)
                Le:
                    return r2
                Lf:
                    r3 = move-exception
                    goto L1f
                L11:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> Lf
                    com.google.protobuf.DescriptorProtos$EnumDescriptorProto r4 = (com.google.protobuf.DescriptorProtos.EnumDescriptorProto) r4     // Catch: java.lang.Throwable -> Lf
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1d
                    throw r3     // Catch: java.lang.Throwable -> L1d
                L1d:
                    r3 = move-exception
                    r0 = r4
                L1f:
                    if (r0 == 0) goto L24
                    r2.A(r0)
                L24:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.DescriptorProtos.EnumDescriptorProto.b.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):com.google.protobuf.DescriptorProtos$EnumDescriptorProto$b");
            }

            public b() {
                this.f0 = "";
                this.g0 = Collections.emptyList();
                this.k0 = Collections.emptyList();
                this.m0 = d0.h0;
                maybeForceBuilderInitialization();
            }

            public b(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.f0 = "";
                this.g0 = Collections.emptyList();
                this.k0 = Collections.emptyList();
                this.m0 = d0.h0;
                maybeForceBuilderInitialization();
            }
        }

        public static b newBuilder(EnumDescriptorProto enumDescriptorProto) {
            return a.toBuilder().A(enumDescriptorProto);
        }

        public static EnumDescriptorProto parseFrom(ByteBuffer byteBuffer, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        /* renamed from: getReservedNameList */
        public dw2 m16getReservedNameList() {
            return this.reservedName_;
        }

        public EnumDescriptorProto(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static EnumDescriptorProto parseDelimitedFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
            return (EnumDescriptorProto) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static EnumDescriptorProto parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public EnumDescriptorProto getDefaultInstanceForType() {
            return a;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public b toBuilder() {
            return this == a ? new b() : new b().A(this);
        }

        public static EnumDescriptorProto parseFrom(ByteString byteString, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public b newBuilderForType() {
            return newBuilder();
        }

        public EnumDescriptorProto() {
            this.memoizedIsInitialized = (byte) -1;
            this.name_ = "";
            this.value_ = Collections.emptyList();
            this.reservedRange_ = Collections.emptyList();
            this.reservedName_ = d0.h0;
        }

        public static EnumDescriptorProto parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public b newBuilderForType(GeneratedMessageV3.c cVar) {
            return new b(cVar);
        }

        public static EnumDescriptorProto parseFrom(byte[] bArr, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static EnumDescriptorProto parseFrom(InputStream inputStream) throws IOException {
            return (EnumDescriptorProto) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static EnumDescriptorProto parseFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
            return (EnumDescriptorProto) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        /* JADX WARN: Multi-variable type inference failed */
        public EnumDescriptorProto(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            boolean z2 = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    ByteString q = jVar.q();
                                    this.bitField0_ = 1 | this.bitField0_;
                                    this.name_ = q;
                                } else if (J == 18) {
                                    if (!(z2 & true)) {
                                        this.value_ = new ArrayList();
                                        z2 |= true;
                                    }
                                    this.value_.add(jVar.z(EnumValueDescriptorProto.PARSER, rVar));
                                } else if (J == 26) {
                                    EnumOptions.b builder = (this.bitField0_ & 2) != 0 ? this.options_.toBuilder() : null;
                                    EnumOptions enumOptions = (EnumOptions) jVar.z(EnumOptions.PARSER, rVar);
                                    this.options_ = enumOptions;
                                    if (builder != null) {
                                        builder.G(enumOptions);
                                        this.options_ = builder.buildPartial();
                                    }
                                    this.bitField0_ |= 2;
                                } else if (J == 34) {
                                    if (!(z2 & true)) {
                                        this.reservedRange_ = new ArrayList();
                                        z2 |= true;
                                    }
                                    this.reservedRange_.add(jVar.z(EnumReservedRange.PARSER, rVar));
                                } else if (J != 42) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    ByteString q2 = jVar.q();
                                    if (!(z2 & true)) {
                                        this.reservedName_ = new d0();
                                        z2 |= true;
                                    }
                                    this.reservedName_.W(q2);
                                }
                            }
                            z = true;
                        } catch (IOException e) {
                            throw new InvalidProtocolBufferException(e).setUnfinishedMessage(this);
                        }
                    } catch (InvalidProtocolBufferException e2) {
                        throw e2.setUnfinishedMessage(this);
                    }
                } finally {
                    if (z2 & true) {
                        this.value_ = Collections.unmodifiableList(this.value_);
                    }
                    if (z2 & true) {
                        this.reservedRange_ = Collections.unmodifiableList(this.reservedRange_);
                    }
                    if (z2 & true) {
                        this.reservedName_ = this.reservedName_.x();
                    }
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static EnumDescriptorProto parseFrom(com.google.protobuf.j jVar) throws IOException {
            return (EnumDescriptorProto) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static EnumDescriptorProto parseFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws IOException {
            return (EnumDescriptorProto) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes2.dex */
    public static final class EnumOptions extends GeneratedMessageV3.ExtendableMessage<EnumOptions> implements d {
        public static final int ALLOW_ALIAS_FIELD_NUMBER = 2;
        public static final int DEPRECATED_FIELD_NUMBER = 3;
        public static final int UNINTERPRETED_OPTION_FIELD_NUMBER = 999;
        private static final long serialVersionUID = 0;
        private boolean allowAlias_;
        private int bitField0_;
        private boolean deprecated_;
        private byte memoizedIsInitialized;
        private List<UninterpretedOption> uninterpretedOption_;
        public static final EnumOptions a = new EnumOptions();
        @Deprecated
        public static final t0<EnumOptions> PARSER = new a();

        /* loaded from: classes2.dex */
        public static class a extends com.google.protobuf.c<EnumOptions> {
            @Override // com.google.protobuf.t0
            /* renamed from: a */
            public EnumOptions parsePartialFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
                return new EnumOptions(jVar, rVar);
            }
        }

        public static EnumOptions getDefaultInstance() {
            return a;
        }

        public static final Descriptors.b getDescriptor() {
            return DescriptorProtos.H;
        }

        public static b newBuilder() {
            return a.toBuilder();
        }

        public static EnumOptions parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (EnumOptions) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static EnumOptions parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<EnumOptions> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof EnumOptions)) {
                return super.equals(obj);
            }
            EnumOptions enumOptions = (EnumOptions) obj;
            if (hasAllowAlias() != enumOptions.hasAllowAlias()) {
                return false;
            }
            if ((!hasAllowAlias() || getAllowAlias() == enumOptions.getAllowAlias()) && hasDeprecated() == enumOptions.hasDeprecated()) {
                return (!hasDeprecated() || getDeprecated() == enumOptions.getDeprecated()) && getUninterpretedOptionList().equals(enumOptions.getUninterpretedOptionList()) && this.unknownFields.equals(enumOptions.unknownFields) && getExtensionFields().equals(enumOptions.getExtensionFields());
            }
            return false;
        }

        public boolean getAllowAlias() {
            return this.allowAlias_;
        }

        public boolean getDeprecated() {
            return this.deprecated_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<EnumOptions> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int e = (this.bitField0_ & 1) != 0 ? CodedOutputStream.e(2, this.allowAlias_) + 0 : 0;
            if ((2 & this.bitField0_) != 0) {
                e += CodedOutputStream.e(3, this.deprecated_);
            }
            for (int i2 = 0; i2 < this.uninterpretedOption_.size(); i2++) {
                e += CodedOutputStream.G(999, this.uninterpretedOption_.get(i2));
            }
            int extensionsSerializedSize = e + extensionsSerializedSize() + this.unknownFields.getSerializedSize();
            this.memoizedSize = extensionsSerializedSize;
            return extensionsSerializedSize;
        }

        public UninterpretedOption getUninterpretedOption(int i) {
            return this.uninterpretedOption_.get(i);
        }

        public int getUninterpretedOptionCount() {
            return this.uninterpretedOption_.size();
        }

        public List<UninterpretedOption> getUninterpretedOptionList() {
            return this.uninterpretedOption_;
        }

        public s getUninterpretedOptionOrBuilder(int i) {
            return this.uninterpretedOption_.get(i);
        }

        public List<? extends s> getUninterpretedOptionOrBuilderList() {
            return this.uninterpretedOption_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        public boolean hasAllowAlias() {
            return (this.bitField0_ & 1) != 0;
        }

        public boolean hasDeprecated() {
            return (this.bitField0_ & 2) != 0;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = 779 + getDescriptor().hashCode();
            if (hasAllowAlias()) {
                hashCode = (((hashCode * 37) + 2) * 53) + a0.c(getAllowAlias());
            }
            if (hasDeprecated()) {
                hashCode = (((hashCode * 37) + 3) * 53) + a0.c(getDeprecated());
            }
            if (getUninterpretedOptionCount() > 0) {
                hashCode = (((hashCode * 37) + 999) * 53) + getUninterpretedOptionList().hashCode();
            }
            int hashFields = (com.google.protobuf.a.hashFields(hashCode, getExtensionFields()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashFields;
            return hashFields;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return DescriptorProtos.I.d(EnumOptions.class, b.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3.ExtendableMessage, com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b2 = this.memoizedIsInitialized;
            if (b2 == 1) {
                return true;
            }
            if (b2 == 0) {
                return false;
            }
            for (int i = 0; i < getUninterpretedOptionCount(); i++) {
                if (!getUninterpretedOption(i).isInitialized()) {
                    this.memoizedIsInitialized = (byte) 0;
                    return false;
                }
            }
            if (!extensionsAreInitialized()) {
                this.memoizedIsInitialized = (byte) 0;
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new EnumOptions();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            GeneratedMessageV3.ExtendableMessage<MessageType>.a newExtensionWriter = newExtensionWriter();
            if ((this.bitField0_ & 1) != 0) {
                codedOutputStream.m0(2, this.allowAlias_);
            }
            if ((this.bitField0_ & 2) != 0) {
                codedOutputStream.m0(3, this.deprecated_);
            }
            for (int i = 0; i < this.uninterpretedOption_.size(); i++) {
                codedOutputStream.K0(999, this.uninterpretedOption_.get(i));
            }
            newExtensionWriter.a(536870912, codedOutputStream);
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes2.dex */
        public static final class b extends GeneratedMessageV3.d<EnumOptions, b> implements d {
            public int f0;
            public boolean g0;
            public boolean h0;
            public List<UninterpretedOption> i0;
            public x0<UninterpretedOption, UninterpretedOption.b, s> j0;

            @Override // defpackage.d82, com.google.protobuf.o0
            /* renamed from: A */
            public EnumOptions getDefaultInstanceForType() {
                return EnumOptions.getDefaultInstance();
            }

            public UninterpretedOption B(int i) {
                x0<UninterpretedOption, UninterpretedOption.b, s> x0Var = this.j0;
                if (x0Var == null) {
                    return this.i0.get(i);
                }
                return x0Var.o(i);
            }

            public int C() {
                x0<UninterpretedOption, UninterpretedOption.b, s> x0Var = this.j0;
                if (x0Var == null) {
                    return this.i0.size();
                }
                return x0Var.n();
            }

            public final x0<UninterpretedOption, UninterpretedOption.b, s> D() {
                if (this.j0 == null) {
                    this.j0 = new x0<>(this.i0, (this.f0 & 4) != 0, getParentForChildren(), isClean());
                    this.i0 = null;
                }
                return this.j0;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0021  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /* renamed from: E */
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public com.google.protobuf.DescriptorProtos.EnumOptions.b mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0<com.google.protobuf.DescriptorProtos$EnumOptions> r1 = com.google.protobuf.DescriptorProtos.EnumOptions.PARSER     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    com.google.protobuf.DescriptorProtos$EnumOptions r3 = (com.google.protobuf.DescriptorProtos.EnumOptions) r3     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    if (r3 == 0) goto Le
                    r2.G(r3)
                Le:
                    return r2
                Lf:
                    r3 = move-exception
                    goto L1f
                L11:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> Lf
                    com.google.protobuf.DescriptorProtos$EnumOptions r4 = (com.google.protobuf.DescriptorProtos.EnumOptions) r4     // Catch: java.lang.Throwable -> Lf
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1d
                    throw r3     // Catch: java.lang.Throwable -> L1d
                L1d:
                    r3 = move-exception
                    r0 = r4
                L1f:
                    if (r0 == 0) goto L24
                    r2.G(r0)
                L24:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.DescriptorProtos.EnumOptions.b.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):com.google.protobuf.DescriptorProtos$EnumOptions$b");
            }

            public b G(EnumOptions enumOptions) {
                if (enumOptions == EnumOptions.getDefaultInstance()) {
                    return this;
                }
                if (enumOptions.hasAllowAlias()) {
                    J(enumOptions.getAllowAlias());
                }
                if (enumOptions.hasDeprecated()) {
                    K(enumOptions.getDeprecated());
                }
                if (this.j0 == null) {
                    if (!enumOptions.uninterpretedOption_.isEmpty()) {
                        if (this.i0.isEmpty()) {
                            this.i0 = enumOptions.uninterpretedOption_;
                            this.f0 &= -5;
                        } else {
                            y();
                            this.i0.addAll(enumOptions.uninterpretedOption_);
                        }
                        onChanged();
                    }
                } else if (!enumOptions.uninterpretedOption_.isEmpty()) {
                    if (!this.j0.u()) {
                        this.j0.b(enumOptions.uninterpretedOption_);
                    } else {
                        this.j0.i();
                        this.j0 = null;
                        this.i0 = enumOptions.uninterpretedOption_;
                        this.f0 &= -5;
                        this.j0 = GeneratedMessageV3.alwaysUseFieldBuilders ? D() : null;
                    }
                }
                j(enumOptions);
                mergeUnknownFields(enumOptions.unknownFields);
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            /* renamed from: H */
            public b mergeFrom(l0 l0Var) {
                if (l0Var instanceof EnumOptions) {
                    return G((EnumOptions) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: I */
            public final b mergeUnknownFields(g1 g1Var) {
                return (b) super.mergeUnknownFields(g1Var);
            }

            public b J(boolean z) {
                this.f0 |= 1;
                this.g0 = z;
                onChanged();
                return this;
            }

            public b K(boolean z) {
                this.f0 |= 2;
                this.h0 = z;
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d, com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: L */
            public b setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (b) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d, com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: M */
            public b setRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (b) super.setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: N */
            public final b setUnknownFields(g1 g1Var) {
                return (b) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return DescriptorProtos.H;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return DescriptorProtos.I.d(EnumOptions.class, b.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d, com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                for (int i = 0; i < C(); i++) {
                    if (!B(i).isInitialized()) {
                        return false;
                    }
                }
                return h();
            }

            public final void maybeForceBuilderInitialization() {
                if (GeneratedMessageV3.alwaysUseFieldBuilders) {
                    D();
                }
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d
            /* renamed from: p */
            public b b(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (b) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            /* renamed from: r */
            public EnumOptions build() {
                EnumOptions buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            /* renamed from: s */
            public EnumOptions buildPartial() {
                int i;
                EnumOptions enumOptions = new EnumOptions(this);
                int i2 = this.f0;
                if ((i2 & 1) != 0) {
                    enumOptions.allowAlias_ = this.g0;
                    i = 1;
                } else {
                    i = 0;
                }
                if ((i2 & 2) != 0) {
                    enumOptions.deprecated_ = this.h0;
                    i |= 2;
                }
                x0<UninterpretedOption, UninterpretedOption.b, s> x0Var = this.j0;
                if (x0Var != null) {
                    enumOptions.uninterpretedOption_ = x0Var.g();
                } else {
                    if ((this.f0 & 4) != 0) {
                        this.i0 = Collections.unmodifiableList(this.i0);
                        this.f0 &= -5;
                    }
                    enumOptions.uninterpretedOption_ = this.i0;
                }
                enumOptions.bitField0_ = i;
                onBuilt();
                return enumOptions;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d
            /* renamed from: t */
            public b e() {
                super.clear();
                this.g0 = false;
                int i = this.f0 & (-2);
                this.f0 = i;
                this.h0 = false;
                this.f0 = i & (-3);
                x0<UninterpretedOption, UninterpretedOption.b, s> x0Var = this.j0;
                if (x0Var == null) {
                    this.i0 = Collections.emptyList();
                    this.f0 &= -5;
                } else {
                    x0Var.h();
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d
            /* renamed from: u */
            public b f(Descriptors.FieldDescriptor fieldDescriptor) {
                return (b) super.clearField(fieldDescriptor);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: v */
            public b clearOneof(Descriptors.g gVar) {
                return (b) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            /* renamed from: x */
            public b clone() {
                return (b) super.clone();
            }

            public final void y() {
                if ((this.f0 & 4) == 0) {
                    this.i0 = new ArrayList(this.i0);
                    this.f0 |= 4;
                }
            }

            public b() {
                this.i0 = Collections.emptyList();
                maybeForceBuilderInitialization();
            }

            public b(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.i0 = Collections.emptyList();
                maybeForceBuilderInitialization();
            }
        }

        public static b newBuilder(EnumOptions enumOptions) {
            return a.toBuilder().G(enumOptions);
        }

        public static EnumOptions parseFrom(ByteBuffer byteBuffer, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        public EnumOptions(GeneratedMessageV3.d<EnumOptions, ?> dVar) {
            super(dVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static EnumOptions parseDelimitedFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
            return (EnumOptions) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static EnumOptions parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3.ExtendableMessage, com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public EnumOptions getDefaultInstanceForType() {
            return a;
        }

        @Override // com.google.protobuf.GeneratedMessageV3.ExtendableMessage, com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public b toBuilder() {
            return this == a ? new b() : new b().G(this);
        }

        public static EnumOptions parseFrom(ByteString byteString, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3.ExtendableMessage, com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public b newBuilderForType() {
            return newBuilder();
        }

        public EnumOptions() {
            this.memoizedIsInitialized = (byte) -1;
            this.uninterpretedOption_ = Collections.emptyList();
        }

        public static EnumOptions parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public b newBuilderForType(GeneratedMessageV3.c cVar) {
            return new b(cVar);
        }

        public static EnumOptions parseFrom(byte[] bArr, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static EnumOptions parseFrom(InputStream inputStream) throws IOException {
            return (EnumOptions) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        /* JADX WARN: Multi-variable type inference failed */
        public EnumOptions(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            boolean z2 = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 16) {
                                    this.bitField0_ |= 1;
                                    this.allowAlias_ = jVar.p();
                                } else if (J == 24) {
                                    this.bitField0_ |= 2;
                                    this.deprecated_ = jVar.p();
                                } else if (J != 7994) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    if (!(z2 & true)) {
                                        this.uninterpretedOption_ = new ArrayList();
                                        z2 |= true;
                                    }
                                    this.uninterpretedOption_.add(jVar.z(UninterpretedOption.PARSER, rVar));
                                }
                            }
                            z = true;
                        } catch (IOException e) {
                            throw new InvalidProtocolBufferException(e).setUnfinishedMessage(this);
                        }
                    } catch (InvalidProtocolBufferException e2) {
                        throw e2.setUnfinishedMessage(this);
                    }
                } finally {
                    if (z2 & true) {
                        this.uninterpretedOption_ = Collections.unmodifiableList(this.uninterpretedOption_);
                    }
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static EnumOptions parseFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
            return (EnumOptions) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static EnumOptions parseFrom(com.google.protobuf.j jVar) throws IOException {
            return (EnumOptions) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static EnumOptions parseFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws IOException {
            return (EnumOptions) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes2.dex */
    public static final class EnumValueDescriptorProto extends GeneratedMessageV3 implements e {
        public static final int NAME_FIELD_NUMBER = 1;
        public static final int NUMBER_FIELD_NUMBER = 2;
        public static final int OPTIONS_FIELD_NUMBER = 3;
        private static final long serialVersionUID = 0;
        private int bitField0_;
        private byte memoizedIsInitialized;
        private volatile Object name_;
        private int number_;
        private EnumValueOptions options_;
        public static final EnumValueDescriptorProto a = new EnumValueDescriptorProto();
        @Deprecated
        public static final t0<EnumValueDescriptorProto> PARSER = new a();

        /* loaded from: classes2.dex */
        public static class a extends com.google.protobuf.c<EnumValueDescriptorProto> {
            @Override // com.google.protobuf.t0
            /* renamed from: a */
            public EnumValueDescriptorProto parsePartialFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
                return new EnumValueDescriptorProto(jVar, rVar);
            }
        }

        public static EnumValueDescriptorProto getDefaultInstance() {
            return a;
        }

        public static final Descriptors.b getDescriptor() {
            return DescriptorProtos.t;
        }

        public static b newBuilder() {
            return a.toBuilder();
        }

        public static EnumValueDescriptorProto parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (EnumValueDescriptorProto) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static EnumValueDescriptorProto parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<EnumValueDescriptorProto> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof EnumValueDescriptorProto)) {
                return super.equals(obj);
            }
            EnumValueDescriptorProto enumValueDescriptorProto = (EnumValueDescriptorProto) obj;
            if (hasName() != enumValueDescriptorProto.hasName()) {
                return false;
            }
            if ((!hasName() || getName().equals(enumValueDescriptorProto.getName())) && hasNumber() == enumValueDescriptorProto.hasNumber()) {
                if ((!hasNumber() || getNumber() == enumValueDescriptorProto.getNumber()) && hasOptions() == enumValueDescriptorProto.hasOptions()) {
                    return (!hasOptions() || getOptions().equals(enumValueDescriptorProto.getOptions())) && this.unknownFields.equals(enumValueDescriptorProto.unknownFields);
                }
                return false;
            }
            return false;
        }

        public String getName() {
            Object obj = this.name_;
            if (obj instanceof String) {
                return (String) obj;
            }
            ByteString byteString = (ByteString) obj;
            String stringUtf8 = byteString.toStringUtf8();
            if (byteString.isValidUtf8()) {
                this.name_ = stringUtf8;
            }
            return stringUtf8;
        }

        public ByteString getNameBytes() {
            Object obj = this.name_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.name_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        public int getNumber() {
            return this.number_;
        }

        public EnumValueOptions getOptions() {
            EnumValueOptions enumValueOptions = this.options_;
            return enumValueOptions == null ? EnumValueOptions.getDefaultInstance() : enumValueOptions;
        }

        public f getOptionsOrBuilder() {
            EnumValueOptions enumValueOptions = this.options_;
            return enumValueOptions == null ? EnumValueOptions.getDefaultInstance() : enumValueOptions;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<EnumValueDescriptorProto> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = (this.bitField0_ & 1) != 0 ? 0 + GeneratedMessageV3.computeStringSize(1, this.name_) : 0;
            if ((this.bitField0_ & 2) != 0) {
                computeStringSize += CodedOutputStream.x(2, this.number_);
            }
            if ((this.bitField0_ & 4) != 0) {
                computeStringSize += CodedOutputStream.G(3, getOptions());
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        public boolean hasName() {
            return (this.bitField0_ & 1) != 0;
        }

        public boolean hasNumber() {
            return (this.bitField0_ & 2) != 0;
        }

        public boolean hasOptions() {
            return (this.bitField0_ & 4) != 0;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = 779 + getDescriptor().hashCode();
            if (hasName()) {
                hashCode = (((hashCode * 37) + 1) * 53) + getName().hashCode();
            }
            if (hasNumber()) {
                hashCode = (((hashCode * 37) + 2) * 53) + getNumber();
            }
            if (hasOptions()) {
                hashCode = (((hashCode * 37) + 3) * 53) + getOptions().hashCode();
            }
            int hashCode2 = (hashCode * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode2;
            return hashCode2;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return DescriptorProtos.u.d(EnumValueDescriptorProto.class, b.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b2 = this.memoizedIsInitialized;
            if (b2 == 1) {
                return true;
            }
            if (b2 == 0) {
                return false;
            }
            if (hasOptions() && !getOptions().isInitialized()) {
                this.memoizedIsInitialized = (byte) 0;
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new EnumValueDescriptorProto();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if ((this.bitField0_ & 1) != 0) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.name_);
            }
            if ((this.bitField0_ & 2) != 0) {
                codedOutputStream.G0(2, this.number_);
            }
            if ((this.bitField0_ & 4) != 0) {
                codedOutputStream.K0(3, getOptions());
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes2.dex */
        public static final class b extends GeneratedMessageV3.b<b> implements e {
            public int a;
            public Object f0;
            public int g0;
            public EnumValueOptions h0;
            public a1<EnumValueOptions, EnumValueOptions.b, f> i0;

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: A */
            public b setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (b) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: B */
            public final b setUnknownFields(g1 g1Var) {
                return (b) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: a */
            public b addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (b) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            /* renamed from: b */
            public EnumValueDescriptorProto build() {
                EnumValueDescriptorProto buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            /* renamed from: d */
            public EnumValueDescriptorProto buildPartial() {
                EnumValueDescriptorProto enumValueDescriptorProto = new EnumValueDescriptorProto(this);
                int i = this.a;
                int i2 = (i & 1) != 0 ? 1 : 0;
                enumValueDescriptorProto.name_ = this.f0;
                if ((i & 2) != 0) {
                    enumValueDescriptorProto.number_ = this.g0;
                    i2 |= 2;
                }
                if ((i & 4) != 0) {
                    a1<EnumValueOptions, EnumValueOptions.b, f> a1Var = this.i0;
                    if (a1Var == null) {
                        enumValueDescriptorProto.options_ = this.h0;
                    } else {
                        enumValueDescriptorProto.options_ = a1Var.b();
                    }
                    i2 |= 4;
                }
                enumValueDescriptorProto.bitField0_ = i2;
                onBuilt();
                return enumValueDescriptorProto;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: e */
            public b clear() {
                super.clear();
                this.f0 = "";
                int i = this.a & (-2);
                this.a = i;
                this.g0 = 0;
                this.a = i & (-3);
                a1<EnumValueOptions, EnumValueOptions.b, f> a1Var = this.i0;
                if (a1Var == null) {
                    this.h0 = null;
                } else {
                    a1Var.c();
                }
                this.a &= -5;
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: f */
            public b clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (b) super.clearField(fieldDescriptor);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: g */
            public b clearOneof(Descriptors.g gVar) {
                return (b) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return DescriptorProtos.t;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            /* renamed from: h */
            public b clone() {
                return (b) super.clone();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return DescriptorProtos.u.d(EnumValueDescriptorProto.class, b.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return !o() || k().isInitialized();
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            /* renamed from: j */
            public EnumValueDescriptorProto getDefaultInstanceForType() {
                return EnumValueDescriptorProto.getDefaultInstance();
            }

            public EnumValueOptions k() {
                a1<EnumValueOptions, EnumValueOptions.b, f> a1Var = this.i0;
                if (a1Var == null) {
                    EnumValueOptions enumValueOptions = this.h0;
                    return enumValueOptions == null ? EnumValueOptions.getDefaultInstance() : enumValueOptions;
                }
                return a1Var.f();
            }

            public final a1<EnumValueOptions, EnumValueOptions.b, f> l() {
                if (this.i0 == null) {
                    this.i0 = new a1<>(k(), getParentForChildren(), isClean());
                    this.h0 = null;
                }
                return this.i0;
            }

            public final void maybeForceBuilderInitialization() {
                if (GeneratedMessageV3.alwaysUseFieldBuilders) {
                    l();
                }
            }

            public boolean o() {
                return (this.a & 4) != 0;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0021  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /* renamed from: p */
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public com.google.protobuf.DescriptorProtos.EnumValueDescriptorProto.b mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0<com.google.protobuf.DescriptorProtos$EnumValueDescriptorProto> r1 = com.google.protobuf.DescriptorProtos.EnumValueDescriptorProto.PARSER     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    com.google.protobuf.DescriptorProtos$EnumValueDescriptorProto r3 = (com.google.protobuf.DescriptorProtos.EnumValueDescriptorProto) r3     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    if (r3 == 0) goto Le
                    r2.r(r3)
                Le:
                    return r2
                Lf:
                    r3 = move-exception
                    goto L1f
                L11:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> Lf
                    com.google.protobuf.DescriptorProtos$EnumValueDescriptorProto r4 = (com.google.protobuf.DescriptorProtos.EnumValueDescriptorProto) r4     // Catch: java.lang.Throwable -> Lf
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1d
                    throw r3     // Catch: java.lang.Throwable -> L1d
                L1d:
                    r3 = move-exception
                    r0 = r4
                L1f:
                    if (r0 == 0) goto L24
                    r2.r(r0)
                L24:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.DescriptorProtos.EnumValueDescriptorProto.b.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):com.google.protobuf.DescriptorProtos$EnumValueDescriptorProto$b");
            }

            public b r(EnumValueDescriptorProto enumValueDescriptorProto) {
                if (enumValueDescriptorProto == EnumValueDescriptorProto.getDefaultInstance()) {
                    return this;
                }
                if (enumValueDescriptorProto.hasName()) {
                    this.a |= 1;
                    this.f0 = enumValueDescriptorProto.name_;
                    onChanged();
                }
                if (enumValueDescriptorProto.hasNumber()) {
                    y(enumValueDescriptorProto.getNumber());
                }
                if (enumValueDescriptorProto.hasOptions()) {
                    t(enumValueDescriptorProto.getOptions());
                }
                mergeUnknownFields(enumValueDescriptorProto.unknownFields);
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            /* renamed from: s */
            public b mergeFrom(l0 l0Var) {
                if (l0Var instanceof EnumValueDescriptorProto) {
                    return r((EnumValueDescriptorProto) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public b t(EnumValueOptions enumValueOptions) {
                EnumValueOptions enumValueOptions2;
                a1<EnumValueOptions, EnumValueOptions.b, f> a1Var = this.i0;
                if (a1Var == null) {
                    if ((this.a & 4) != 0 && (enumValueOptions2 = this.h0) != null && enumValueOptions2 != EnumValueOptions.getDefaultInstance()) {
                        this.h0 = EnumValueOptions.newBuilder(this.h0).G(enumValueOptions).buildPartial();
                    } else {
                        this.h0 = enumValueOptions;
                    }
                    onChanged();
                } else {
                    a1Var.h(enumValueOptions);
                }
                this.a |= 4;
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: u */
            public final b mergeUnknownFields(g1 g1Var) {
                return (b) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: v */
            public b setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (b) super.setField(fieldDescriptor, obj);
            }

            public b x(String str) {
                Objects.requireNonNull(str);
                this.a |= 1;
                this.f0 = str;
                onChanged();
                return this;
            }

            public b y(int i) {
                this.a |= 2;
                this.g0 = i;
                onChanged();
                return this;
            }

            public b() {
                this.f0 = "";
                maybeForceBuilderInitialization();
            }

            public b(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.f0 = "";
                maybeForceBuilderInitialization();
            }
        }

        public static b newBuilder(EnumValueDescriptorProto enumValueDescriptorProto) {
            return a.toBuilder().r(enumValueDescriptorProto);
        }

        public static EnumValueDescriptorProto parseFrom(ByteBuffer byteBuffer, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        public EnumValueDescriptorProto(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static EnumValueDescriptorProto parseDelimitedFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
            return (EnumValueDescriptorProto) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static EnumValueDescriptorProto parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public EnumValueDescriptorProto getDefaultInstanceForType() {
            return a;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public b toBuilder() {
            return this == a ? new b() : new b().r(this);
        }

        public static EnumValueDescriptorProto parseFrom(ByteString byteString, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public b newBuilderForType() {
            return newBuilder();
        }

        public EnumValueDescriptorProto() {
            this.memoizedIsInitialized = (byte) -1;
            this.name_ = "";
        }

        public static EnumValueDescriptorProto parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public b newBuilderForType(GeneratedMessageV3.c cVar) {
            return new b(cVar);
        }

        public static EnumValueDescriptorProto parseFrom(byte[] bArr, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static EnumValueDescriptorProto parseFrom(InputStream inputStream) throws IOException {
            return (EnumValueDescriptorProto) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public EnumValueDescriptorProto(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 10) {
                                ByteString q = jVar.q();
                                this.bitField0_ = 1 | this.bitField0_;
                                this.name_ = q;
                            } else if (J == 16) {
                                this.bitField0_ |= 2;
                                this.number_ = jVar.x();
                            } else if (J != 26) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                EnumValueOptions.b builder = (this.bitField0_ & 4) != 0 ? this.options_.toBuilder() : null;
                                EnumValueOptions enumValueOptions = (EnumValueOptions) jVar.z(EnumValueOptions.PARSER, rVar);
                                this.options_ = enumValueOptions;
                                if (builder != null) {
                                    builder.G(enumValueOptions);
                                    this.options_ = builder.buildPartial();
                                }
                                this.bitField0_ |= 4;
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static EnumValueDescriptorProto parseFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
            return (EnumValueDescriptorProto) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static EnumValueDescriptorProto parseFrom(com.google.protobuf.j jVar) throws IOException {
            return (EnumValueDescriptorProto) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static EnumValueDescriptorProto parseFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws IOException {
            return (EnumValueDescriptorProto) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes2.dex */
    public static final class EnumValueOptions extends GeneratedMessageV3.ExtendableMessage<EnumValueOptions> implements f {
        public static final int DEPRECATED_FIELD_NUMBER = 1;
        public static final int UNINTERPRETED_OPTION_FIELD_NUMBER = 999;
        private static final long serialVersionUID = 0;
        private int bitField0_;
        private boolean deprecated_;
        private byte memoizedIsInitialized;
        private List<UninterpretedOption> uninterpretedOption_;
        public static final EnumValueOptions a = new EnumValueOptions();
        @Deprecated
        public static final t0<EnumValueOptions> PARSER = new a();

        /* loaded from: classes2.dex */
        public static class a extends com.google.protobuf.c<EnumValueOptions> {
            @Override // com.google.protobuf.t0
            /* renamed from: a */
            public EnumValueOptions parsePartialFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
                return new EnumValueOptions(jVar, rVar);
            }
        }

        public static EnumValueOptions getDefaultInstance() {
            return a;
        }

        public static final Descriptors.b getDescriptor() {
            return DescriptorProtos.J;
        }

        public static b newBuilder() {
            return a.toBuilder();
        }

        public static EnumValueOptions parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (EnumValueOptions) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static EnumValueOptions parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<EnumValueOptions> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof EnumValueOptions)) {
                return super.equals(obj);
            }
            EnumValueOptions enumValueOptions = (EnumValueOptions) obj;
            if (hasDeprecated() != enumValueOptions.hasDeprecated()) {
                return false;
            }
            return (!hasDeprecated() || getDeprecated() == enumValueOptions.getDeprecated()) && getUninterpretedOptionList().equals(enumValueOptions.getUninterpretedOptionList()) && this.unknownFields.equals(enumValueOptions.unknownFields) && getExtensionFields().equals(enumValueOptions.getExtensionFields());
        }

        public boolean getDeprecated() {
            return this.deprecated_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<EnumValueOptions> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int e = (this.bitField0_ & 1) != 0 ? CodedOutputStream.e(1, this.deprecated_) + 0 : 0;
            for (int i2 = 0; i2 < this.uninterpretedOption_.size(); i2++) {
                e += CodedOutputStream.G(999, this.uninterpretedOption_.get(i2));
            }
            int extensionsSerializedSize = e + extensionsSerializedSize() + this.unknownFields.getSerializedSize();
            this.memoizedSize = extensionsSerializedSize;
            return extensionsSerializedSize;
        }

        public UninterpretedOption getUninterpretedOption(int i) {
            return this.uninterpretedOption_.get(i);
        }

        public int getUninterpretedOptionCount() {
            return this.uninterpretedOption_.size();
        }

        public List<UninterpretedOption> getUninterpretedOptionList() {
            return this.uninterpretedOption_;
        }

        public s getUninterpretedOptionOrBuilder(int i) {
            return this.uninterpretedOption_.get(i);
        }

        public List<? extends s> getUninterpretedOptionOrBuilderList() {
            return this.uninterpretedOption_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        public boolean hasDeprecated() {
            return (this.bitField0_ & 1) != 0;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = 779 + getDescriptor().hashCode();
            if (hasDeprecated()) {
                hashCode = (((hashCode * 37) + 1) * 53) + a0.c(getDeprecated());
            }
            if (getUninterpretedOptionCount() > 0) {
                hashCode = (((hashCode * 37) + 999) * 53) + getUninterpretedOptionList().hashCode();
            }
            int hashFields = (com.google.protobuf.a.hashFields(hashCode, getExtensionFields()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashFields;
            return hashFields;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return DescriptorProtos.K.d(EnumValueOptions.class, b.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3.ExtendableMessage, com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b2 = this.memoizedIsInitialized;
            if (b2 == 1) {
                return true;
            }
            if (b2 == 0) {
                return false;
            }
            for (int i = 0; i < getUninterpretedOptionCount(); i++) {
                if (!getUninterpretedOption(i).isInitialized()) {
                    this.memoizedIsInitialized = (byte) 0;
                    return false;
                }
            }
            if (!extensionsAreInitialized()) {
                this.memoizedIsInitialized = (byte) 0;
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new EnumValueOptions();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            GeneratedMessageV3.ExtendableMessage<MessageType>.a newExtensionWriter = newExtensionWriter();
            if ((this.bitField0_ & 1) != 0) {
                codedOutputStream.m0(1, this.deprecated_);
            }
            for (int i = 0; i < this.uninterpretedOption_.size(); i++) {
                codedOutputStream.K0(999, this.uninterpretedOption_.get(i));
            }
            newExtensionWriter.a(536870912, codedOutputStream);
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes2.dex */
        public static final class b extends GeneratedMessageV3.d<EnumValueOptions, b> implements f {
            public int f0;
            public boolean g0;
            public List<UninterpretedOption> h0;
            public x0<UninterpretedOption, UninterpretedOption.b, s> i0;

            @Override // defpackage.d82, com.google.protobuf.o0
            /* renamed from: A */
            public EnumValueOptions getDefaultInstanceForType() {
                return EnumValueOptions.getDefaultInstance();
            }

            public UninterpretedOption B(int i) {
                x0<UninterpretedOption, UninterpretedOption.b, s> x0Var = this.i0;
                if (x0Var == null) {
                    return this.h0.get(i);
                }
                return x0Var.o(i);
            }

            public int C() {
                x0<UninterpretedOption, UninterpretedOption.b, s> x0Var = this.i0;
                if (x0Var == null) {
                    return this.h0.size();
                }
                return x0Var.n();
            }

            public final x0<UninterpretedOption, UninterpretedOption.b, s> D() {
                if (this.i0 == null) {
                    this.i0 = new x0<>(this.h0, (this.f0 & 2) != 0, getParentForChildren(), isClean());
                    this.h0 = null;
                }
                return this.i0;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0021  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /* renamed from: E */
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public com.google.protobuf.DescriptorProtos.EnumValueOptions.b mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0<com.google.protobuf.DescriptorProtos$EnumValueOptions> r1 = com.google.protobuf.DescriptorProtos.EnumValueOptions.PARSER     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    com.google.protobuf.DescriptorProtos$EnumValueOptions r3 = (com.google.protobuf.DescriptorProtos.EnumValueOptions) r3     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    if (r3 == 0) goto Le
                    r2.G(r3)
                Le:
                    return r2
                Lf:
                    r3 = move-exception
                    goto L1f
                L11:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> Lf
                    com.google.protobuf.DescriptorProtos$EnumValueOptions r4 = (com.google.protobuf.DescriptorProtos.EnumValueOptions) r4     // Catch: java.lang.Throwable -> Lf
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1d
                    throw r3     // Catch: java.lang.Throwable -> L1d
                L1d:
                    r3 = move-exception
                    r0 = r4
                L1f:
                    if (r0 == 0) goto L24
                    r2.G(r0)
                L24:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.DescriptorProtos.EnumValueOptions.b.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):com.google.protobuf.DescriptorProtos$EnumValueOptions$b");
            }

            public b G(EnumValueOptions enumValueOptions) {
                if (enumValueOptions == EnumValueOptions.getDefaultInstance()) {
                    return this;
                }
                if (enumValueOptions.hasDeprecated()) {
                    J(enumValueOptions.getDeprecated());
                }
                if (this.i0 == null) {
                    if (!enumValueOptions.uninterpretedOption_.isEmpty()) {
                        if (this.h0.isEmpty()) {
                            this.h0 = enumValueOptions.uninterpretedOption_;
                            this.f0 &= -3;
                        } else {
                            y();
                            this.h0.addAll(enumValueOptions.uninterpretedOption_);
                        }
                        onChanged();
                    }
                } else if (!enumValueOptions.uninterpretedOption_.isEmpty()) {
                    if (!this.i0.u()) {
                        this.i0.b(enumValueOptions.uninterpretedOption_);
                    } else {
                        this.i0.i();
                        this.i0 = null;
                        this.h0 = enumValueOptions.uninterpretedOption_;
                        this.f0 &= -3;
                        this.i0 = GeneratedMessageV3.alwaysUseFieldBuilders ? D() : null;
                    }
                }
                j(enumValueOptions);
                mergeUnknownFields(enumValueOptions.unknownFields);
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            /* renamed from: H */
            public b mergeFrom(l0 l0Var) {
                if (l0Var instanceof EnumValueOptions) {
                    return G((EnumValueOptions) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: I */
            public final b mergeUnknownFields(g1 g1Var) {
                return (b) super.mergeUnknownFields(g1Var);
            }

            public b J(boolean z) {
                this.f0 |= 1;
                this.g0 = z;
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d, com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: K */
            public b setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (b) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d, com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: L */
            public b setRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (b) super.setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: M */
            public final b setUnknownFields(g1 g1Var) {
                return (b) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return DescriptorProtos.J;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return DescriptorProtos.K.d(EnumValueOptions.class, b.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d, com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                for (int i = 0; i < C(); i++) {
                    if (!B(i).isInitialized()) {
                        return false;
                    }
                }
                return h();
            }

            public final void maybeForceBuilderInitialization() {
                if (GeneratedMessageV3.alwaysUseFieldBuilders) {
                    D();
                }
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d
            /* renamed from: p */
            public b b(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (b) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            /* renamed from: r */
            public EnumValueOptions build() {
                EnumValueOptions buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            /* renamed from: s */
            public EnumValueOptions buildPartial() {
                EnumValueOptions enumValueOptions = new EnumValueOptions(this);
                int i = 1;
                if ((this.f0 & 1) != 0) {
                    enumValueOptions.deprecated_ = this.g0;
                } else {
                    i = 0;
                }
                x0<UninterpretedOption, UninterpretedOption.b, s> x0Var = this.i0;
                if (x0Var != null) {
                    enumValueOptions.uninterpretedOption_ = x0Var.g();
                } else {
                    if ((this.f0 & 2) != 0) {
                        this.h0 = Collections.unmodifiableList(this.h0);
                        this.f0 &= -3;
                    }
                    enumValueOptions.uninterpretedOption_ = this.h0;
                }
                enumValueOptions.bitField0_ = i;
                onBuilt();
                return enumValueOptions;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d
            /* renamed from: t */
            public b e() {
                super.clear();
                this.g0 = false;
                this.f0 &= -2;
                x0<UninterpretedOption, UninterpretedOption.b, s> x0Var = this.i0;
                if (x0Var == null) {
                    this.h0 = Collections.emptyList();
                    this.f0 &= -3;
                } else {
                    x0Var.h();
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d
            /* renamed from: u */
            public b f(Descriptors.FieldDescriptor fieldDescriptor) {
                return (b) super.clearField(fieldDescriptor);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: v */
            public b clearOneof(Descriptors.g gVar) {
                return (b) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            /* renamed from: x */
            public b clone() {
                return (b) super.clone();
            }

            public final void y() {
                if ((this.f0 & 2) == 0) {
                    this.h0 = new ArrayList(this.h0);
                    this.f0 |= 2;
                }
            }

            public b() {
                this.h0 = Collections.emptyList();
                maybeForceBuilderInitialization();
            }

            public b(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.h0 = Collections.emptyList();
                maybeForceBuilderInitialization();
            }
        }

        public static b newBuilder(EnumValueOptions enumValueOptions) {
            return a.toBuilder().G(enumValueOptions);
        }

        public static EnumValueOptions parseFrom(ByteBuffer byteBuffer, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        public EnumValueOptions(GeneratedMessageV3.d<EnumValueOptions, ?> dVar) {
            super(dVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static EnumValueOptions parseDelimitedFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
            return (EnumValueOptions) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static EnumValueOptions parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3.ExtendableMessage, com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public EnumValueOptions getDefaultInstanceForType() {
            return a;
        }

        @Override // com.google.protobuf.GeneratedMessageV3.ExtendableMessage, com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public b toBuilder() {
            return this == a ? new b() : new b().G(this);
        }

        public static EnumValueOptions parseFrom(ByteString byteString, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3.ExtendableMessage, com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public b newBuilderForType() {
            return newBuilder();
        }

        public EnumValueOptions() {
            this.memoizedIsInitialized = (byte) -1;
            this.uninterpretedOption_ = Collections.emptyList();
        }

        public static EnumValueOptions parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public b newBuilderForType(GeneratedMessageV3.c cVar) {
            return new b(cVar);
        }

        public static EnumValueOptions parseFrom(byte[] bArr, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static EnumValueOptions parseFrom(InputStream inputStream) throws IOException {
            return (EnumValueOptions) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        /* JADX WARN: Multi-variable type inference failed */
        public EnumValueOptions(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            boolean z2 = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 8) {
                                this.bitField0_ |= 1;
                                this.deprecated_ = jVar.p();
                            } else if (J != 7994) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                if (!(z2 & true)) {
                                    this.uninterpretedOption_ = new ArrayList();
                                    z2 |= true;
                                }
                                this.uninterpretedOption_.add(jVar.z(UninterpretedOption.PARSER, rVar));
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    if (z2 & true) {
                        this.uninterpretedOption_ = Collections.unmodifiableList(this.uninterpretedOption_);
                    }
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static EnumValueOptions parseFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
            return (EnumValueOptions) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static EnumValueOptions parseFrom(com.google.protobuf.j jVar) throws IOException {
            return (EnumValueOptions) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static EnumValueOptions parseFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws IOException {
            return (EnumValueOptions) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes2.dex */
    public static final class ExtensionRangeOptions extends GeneratedMessageV3.ExtendableMessage<ExtensionRangeOptions> implements g {
        public static final int UNINTERPRETED_OPTION_FIELD_NUMBER = 999;
        private static final long serialVersionUID = 0;
        private byte memoizedIsInitialized;
        private List<UninterpretedOption> uninterpretedOption_;
        public static final ExtensionRangeOptions a = new ExtensionRangeOptions();
        @Deprecated
        public static final t0<ExtensionRangeOptions> PARSER = new a();

        /* loaded from: classes2.dex */
        public static class a extends com.google.protobuf.c<ExtensionRangeOptions> {
            @Override // com.google.protobuf.t0
            /* renamed from: a */
            public ExtensionRangeOptions parsePartialFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
                return new ExtensionRangeOptions(jVar, rVar);
            }
        }

        public static ExtensionRangeOptions getDefaultInstance() {
            return a;
        }

        public static final Descriptors.b getDescriptor() {
            return DescriptorProtos.j;
        }

        public static b newBuilder() {
            return a.toBuilder();
        }

        public static ExtensionRangeOptions parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (ExtensionRangeOptions) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static ExtensionRangeOptions parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<ExtensionRangeOptions> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof ExtensionRangeOptions)) {
                return super.equals(obj);
            }
            ExtensionRangeOptions extensionRangeOptions = (ExtensionRangeOptions) obj;
            return getUninterpretedOptionList().equals(extensionRangeOptions.getUninterpretedOptionList()) && this.unknownFields.equals(extensionRangeOptions.unknownFields) && getExtensionFields().equals(extensionRangeOptions.getExtensionFields());
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<ExtensionRangeOptions> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            for (int i3 = 0; i3 < this.uninterpretedOption_.size(); i3++) {
                i2 += CodedOutputStream.G(999, this.uninterpretedOption_.get(i3));
            }
            int extensionsSerializedSize = i2 + extensionsSerializedSize() + this.unknownFields.getSerializedSize();
            this.memoizedSize = extensionsSerializedSize;
            return extensionsSerializedSize;
        }

        public UninterpretedOption getUninterpretedOption(int i) {
            return this.uninterpretedOption_.get(i);
        }

        public int getUninterpretedOptionCount() {
            return this.uninterpretedOption_.size();
        }

        public List<UninterpretedOption> getUninterpretedOptionList() {
            return this.uninterpretedOption_;
        }

        public s getUninterpretedOptionOrBuilder(int i) {
            return this.uninterpretedOption_.get(i);
        }

        public List<? extends s> getUninterpretedOptionOrBuilderList() {
            return this.uninterpretedOption_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = 779 + getDescriptor().hashCode();
            if (getUninterpretedOptionCount() > 0) {
                hashCode = (((hashCode * 37) + 999) * 53) + getUninterpretedOptionList().hashCode();
            }
            int hashFields = (com.google.protobuf.a.hashFields(hashCode, getExtensionFields()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashFields;
            return hashFields;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return DescriptorProtos.k.d(ExtensionRangeOptions.class, b.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3.ExtendableMessage, com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b2 = this.memoizedIsInitialized;
            if (b2 == 1) {
                return true;
            }
            if (b2 == 0) {
                return false;
            }
            for (int i = 0; i < getUninterpretedOptionCount(); i++) {
                if (!getUninterpretedOption(i).isInitialized()) {
                    this.memoizedIsInitialized = (byte) 0;
                    return false;
                }
            }
            if (!extensionsAreInitialized()) {
                this.memoizedIsInitialized = (byte) 0;
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new ExtensionRangeOptions();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            GeneratedMessageV3.ExtendableMessage<MessageType>.a newExtensionWriter = newExtensionWriter();
            for (int i = 0; i < this.uninterpretedOption_.size(); i++) {
                codedOutputStream.K0(999, this.uninterpretedOption_.get(i));
            }
            newExtensionWriter.a(536870912, codedOutputStream);
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes2.dex */
        public static final class b extends GeneratedMessageV3.d<ExtensionRangeOptions, b> implements g {
            public int f0;
            public List<UninterpretedOption> g0;
            public x0<UninterpretedOption, UninterpretedOption.b, s> h0;

            @Override // defpackage.d82, com.google.protobuf.o0
            /* renamed from: A */
            public ExtensionRangeOptions getDefaultInstanceForType() {
                return ExtensionRangeOptions.getDefaultInstance();
            }

            public UninterpretedOption B(int i) {
                x0<UninterpretedOption, UninterpretedOption.b, s> x0Var = this.h0;
                if (x0Var == null) {
                    return this.g0.get(i);
                }
                return x0Var.o(i);
            }

            public int C() {
                x0<UninterpretedOption, UninterpretedOption.b, s> x0Var = this.h0;
                if (x0Var == null) {
                    return this.g0.size();
                }
                return x0Var.n();
            }

            public final x0<UninterpretedOption, UninterpretedOption.b, s> D() {
                if (this.h0 == null) {
                    this.h0 = new x0<>(this.g0, (this.f0 & 1) != 0, getParentForChildren(), isClean());
                    this.g0 = null;
                }
                return this.h0;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0021  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /* renamed from: E */
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public com.google.protobuf.DescriptorProtos.ExtensionRangeOptions.b mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0<com.google.protobuf.DescriptorProtos$ExtensionRangeOptions> r1 = com.google.protobuf.DescriptorProtos.ExtensionRangeOptions.PARSER     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    com.google.protobuf.DescriptorProtos$ExtensionRangeOptions r3 = (com.google.protobuf.DescriptorProtos.ExtensionRangeOptions) r3     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    if (r3 == 0) goto Le
                    r2.G(r3)
                Le:
                    return r2
                Lf:
                    r3 = move-exception
                    goto L1f
                L11:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> Lf
                    com.google.protobuf.DescriptorProtos$ExtensionRangeOptions r4 = (com.google.protobuf.DescriptorProtos.ExtensionRangeOptions) r4     // Catch: java.lang.Throwable -> Lf
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1d
                    throw r3     // Catch: java.lang.Throwable -> L1d
                L1d:
                    r3 = move-exception
                    r0 = r4
                L1f:
                    if (r0 == 0) goto L24
                    r2.G(r0)
                L24:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.DescriptorProtos.ExtensionRangeOptions.b.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):com.google.protobuf.DescriptorProtos$ExtensionRangeOptions$b");
            }

            public b G(ExtensionRangeOptions extensionRangeOptions) {
                if (extensionRangeOptions == ExtensionRangeOptions.getDefaultInstance()) {
                    return this;
                }
                if (this.h0 == null) {
                    if (!extensionRangeOptions.uninterpretedOption_.isEmpty()) {
                        if (this.g0.isEmpty()) {
                            this.g0 = extensionRangeOptions.uninterpretedOption_;
                            this.f0 &= -2;
                        } else {
                            y();
                            this.g0.addAll(extensionRangeOptions.uninterpretedOption_);
                        }
                        onChanged();
                    }
                } else if (!extensionRangeOptions.uninterpretedOption_.isEmpty()) {
                    if (!this.h0.u()) {
                        this.h0.b(extensionRangeOptions.uninterpretedOption_);
                    } else {
                        this.h0.i();
                        this.h0 = null;
                        this.g0 = extensionRangeOptions.uninterpretedOption_;
                        this.f0 &= -2;
                        this.h0 = GeneratedMessageV3.alwaysUseFieldBuilders ? D() : null;
                    }
                }
                j(extensionRangeOptions);
                mergeUnknownFields(extensionRangeOptions.unknownFields);
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            /* renamed from: H */
            public b mergeFrom(l0 l0Var) {
                if (l0Var instanceof ExtensionRangeOptions) {
                    return G((ExtensionRangeOptions) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: I */
            public final b mergeUnknownFields(g1 g1Var) {
                return (b) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d, com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: J */
            public b setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (b) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d, com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: K */
            public b setRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (b) super.setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: L */
            public final b setUnknownFields(g1 g1Var) {
                return (b) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return DescriptorProtos.j;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return DescriptorProtos.k.d(ExtensionRangeOptions.class, b.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d, com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                for (int i = 0; i < C(); i++) {
                    if (!B(i).isInitialized()) {
                        return false;
                    }
                }
                return h();
            }

            public final void maybeForceBuilderInitialization() {
                if (GeneratedMessageV3.alwaysUseFieldBuilders) {
                    D();
                }
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d
            /* renamed from: p */
            public b b(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (b) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            /* renamed from: r */
            public ExtensionRangeOptions build() {
                ExtensionRangeOptions buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            /* renamed from: s */
            public ExtensionRangeOptions buildPartial() {
                ExtensionRangeOptions extensionRangeOptions = new ExtensionRangeOptions(this);
                int i = this.f0;
                x0<UninterpretedOption, UninterpretedOption.b, s> x0Var = this.h0;
                if (x0Var == null) {
                    if ((i & 1) != 0) {
                        this.g0 = Collections.unmodifiableList(this.g0);
                        this.f0 &= -2;
                    }
                    extensionRangeOptions.uninterpretedOption_ = this.g0;
                } else {
                    extensionRangeOptions.uninterpretedOption_ = x0Var.g();
                }
                onBuilt();
                return extensionRangeOptions;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d
            /* renamed from: t */
            public b e() {
                super.clear();
                x0<UninterpretedOption, UninterpretedOption.b, s> x0Var = this.h0;
                if (x0Var == null) {
                    this.g0 = Collections.emptyList();
                    this.f0 &= -2;
                } else {
                    x0Var.h();
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d
            /* renamed from: u */
            public b f(Descriptors.FieldDescriptor fieldDescriptor) {
                return (b) super.clearField(fieldDescriptor);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: v */
            public b clearOneof(Descriptors.g gVar) {
                return (b) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            /* renamed from: x */
            public b clone() {
                return (b) super.clone();
            }

            public final void y() {
                if ((this.f0 & 1) == 0) {
                    this.g0 = new ArrayList(this.g0);
                    this.f0 |= 1;
                }
            }

            public b() {
                this.g0 = Collections.emptyList();
                maybeForceBuilderInitialization();
            }

            public b(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.g0 = Collections.emptyList();
                maybeForceBuilderInitialization();
            }
        }

        public static b newBuilder(ExtensionRangeOptions extensionRangeOptions) {
            return a.toBuilder().G(extensionRangeOptions);
        }

        public static ExtensionRangeOptions parseFrom(ByteBuffer byteBuffer, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        public ExtensionRangeOptions(GeneratedMessageV3.d<ExtensionRangeOptions, ?> dVar) {
            super(dVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static ExtensionRangeOptions parseDelimitedFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
            return (ExtensionRangeOptions) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static ExtensionRangeOptions parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3.ExtendableMessage, com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public ExtensionRangeOptions getDefaultInstanceForType() {
            return a;
        }

        @Override // com.google.protobuf.GeneratedMessageV3.ExtendableMessage, com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public b toBuilder() {
            return this == a ? new b() : new b().G(this);
        }

        public static ExtensionRangeOptions parseFrom(ByteString byteString, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3.ExtendableMessage, com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public b newBuilderForType() {
            return newBuilder();
        }

        public ExtensionRangeOptions() {
            this.memoizedIsInitialized = (byte) -1;
            this.uninterpretedOption_ = Collections.emptyList();
        }

        public static ExtensionRangeOptions parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public b newBuilderForType(GeneratedMessageV3.c cVar) {
            return new b(cVar);
        }

        public static ExtensionRangeOptions parseFrom(byte[] bArr, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static ExtensionRangeOptions parseFrom(InputStream inputStream) throws IOException {
            return (ExtensionRangeOptions) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        /* JADX WARN: Multi-variable type inference failed */
        public ExtensionRangeOptions(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            boolean z2 = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J != 7994) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                if (!(z2 & true)) {
                                    this.uninterpretedOption_ = new ArrayList();
                                    z2 |= true;
                                }
                                this.uninterpretedOption_.add(jVar.z(UninterpretedOption.PARSER, rVar));
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    if (z2 & true) {
                        this.uninterpretedOption_ = Collections.unmodifiableList(this.uninterpretedOption_);
                    }
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static ExtensionRangeOptions parseFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
            return (ExtensionRangeOptions) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static ExtensionRangeOptions parseFrom(com.google.protobuf.j jVar) throws IOException {
            return (ExtensionRangeOptions) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static ExtensionRangeOptions parseFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws IOException {
            return (ExtensionRangeOptions) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes2.dex */
    public static final class FieldDescriptorProto extends GeneratedMessageV3 implements h {
        public static final int DEFAULT_VALUE_FIELD_NUMBER = 7;
        public static final int EXTENDEE_FIELD_NUMBER = 2;
        public static final int JSON_NAME_FIELD_NUMBER = 10;
        public static final int LABEL_FIELD_NUMBER = 4;
        public static final int NAME_FIELD_NUMBER = 1;
        public static final int NUMBER_FIELD_NUMBER = 3;
        public static final int ONEOF_INDEX_FIELD_NUMBER = 9;
        public static final int OPTIONS_FIELD_NUMBER = 8;
        public static final int PROTO3_OPTIONAL_FIELD_NUMBER = 17;
        public static final int TYPE_FIELD_NUMBER = 5;
        public static final int TYPE_NAME_FIELD_NUMBER = 6;
        private static final long serialVersionUID = 0;
        private int bitField0_;
        private volatile Object defaultValue_;
        private volatile Object extendee_;
        private volatile Object jsonName_;
        private int label_;
        private byte memoizedIsInitialized;
        private volatile Object name_;
        private int number_;
        private int oneofIndex_;
        private FieldOptions options_;
        private boolean proto3Optional_;
        private volatile Object typeName_;
        private int type_;
        public static final FieldDescriptorProto a = new FieldDescriptorProto();
        @Deprecated
        public static final t0<FieldDescriptorProto> PARSER = new a();

        /* loaded from: classes2.dex */
        public enum Label implements v0 {
            LABEL_OPTIONAL(1),
            LABEL_REQUIRED(2),
            LABEL_REPEATED(3);
            
            public static final int LABEL_OPTIONAL_VALUE = 1;
            public static final int LABEL_REPEATED_VALUE = 3;
            public static final int LABEL_REQUIRED_VALUE = 2;
            public static final a0.d<Label> a = new a();
            public static final Label[] f0 = values();
            private final int value;

            /* loaded from: classes2.dex */
            public static class a implements a0.d<Label> {
                @Override // com.google.protobuf.a0.d
                /* renamed from: a */
                public Label findValueByNumber(int i) {
                    return Label.forNumber(i);
                }
            }

            Label(int i) {
                this.value = i;
            }

            public static Label forNumber(int i) {
                if (i != 1) {
                    if (i != 2) {
                        if (i != 3) {
                            return null;
                        }
                        return LABEL_REPEATED;
                    }
                    return LABEL_REQUIRED;
                }
                return LABEL_OPTIONAL;
            }

            public static final Descriptors.c getDescriptor() {
                return FieldDescriptorProto.getDescriptor().l().get(1);
            }

            public static a0.d<Label> internalGetValueMap() {
                return a;
            }

            public final Descriptors.c getDescriptorForType() {
                return getDescriptor();
            }

            @Override // com.google.protobuf.a0.c
            public final int getNumber() {
                return this.value;
            }

            public final Descriptors.d getValueDescriptor() {
                return getDescriptor().k().get(ordinal());
            }

            @Deprecated
            public static Label valueOf(int i) {
                return forNumber(i);
            }

            public static Label valueOf(Descriptors.d dVar) {
                if (dVar.h() == getDescriptor()) {
                    return f0[dVar.g()];
                }
                throw new IllegalArgumentException("EnumValueDescriptor is not for this type.");
            }
        }

        /* loaded from: classes2.dex */
        public enum Type implements v0 {
            TYPE_DOUBLE(1),
            TYPE_FLOAT(2),
            TYPE_INT64(3),
            TYPE_UINT64(4),
            TYPE_INT32(5),
            TYPE_FIXED64(6),
            TYPE_FIXED32(7),
            TYPE_BOOL(8),
            TYPE_STRING(9),
            TYPE_GROUP(10),
            TYPE_MESSAGE(11),
            TYPE_BYTES(12),
            TYPE_UINT32(13),
            TYPE_ENUM(14),
            TYPE_SFIXED32(15),
            TYPE_SFIXED64(16),
            TYPE_SINT32(17),
            TYPE_SINT64(18);
            
            public static final int TYPE_BOOL_VALUE = 8;
            public static final int TYPE_BYTES_VALUE = 12;
            public static final int TYPE_DOUBLE_VALUE = 1;
            public static final int TYPE_ENUM_VALUE = 14;
            public static final int TYPE_FIXED32_VALUE = 7;
            public static final int TYPE_FIXED64_VALUE = 6;
            public static final int TYPE_FLOAT_VALUE = 2;
            public static final int TYPE_GROUP_VALUE = 10;
            public static final int TYPE_INT32_VALUE = 5;
            public static final int TYPE_INT64_VALUE = 3;
            public static final int TYPE_MESSAGE_VALUE = 11;
            public static final int TYPE_SFIXED32_VALUE = 15;
            public static final int TYPE_SFIXED64_VALUE = 16;
            public static final int TYPE_SINT32_VALUE = 17;
            public static final int TYPE_SINT64_VALUE = 18;
            public static final int TYPE_STRING_VALUE = 9;
            public static final int TYPE_UINT32_VALUE = 13;
            public static final int TYPE_UINT64_VALUE = 4;
            public static final a0.d<Type> a = new a();
            public static final Type[] f0 = values();
            private final int value;

            /* loaded from: classes2.dex */
            public static class a implements a0.d<Type> {
                @Override // com.google.protobuf.a0.d
                /* renamed from: a */
                public Type findValueByNumber(int i) {
                    return Type.forNumber(i);
                }
            }

            Type(int i) {
                this.value = i;
            }

            public static Type forNumber(int i) {
                switch (i) {
                    case 1:
                        return TYPE_DOUBLE;
                    case 2:
                        return TYPE_FLOAT;
                    case 3:
                        return TYPE_INT64;
                    case 4:
                        return TYPE_UINT64;
                    case 5:
                        return TYPE_INT32;
                    case 6:
                        return TYPE_FIXED64;
                    case 7:
                        return TYPE_FIXED32;
                    case 8:
                        return TYPE_BOOL;
                    case 9:
                        return TYPE_STRING;
                    case 10:
                        return TYPE_GROUP;
                    case 11:
                        return TYPE_MESSAGE;
                    case 12:
                        return TYPE_BYTES;
                    case 13:
                        return TYPE_UINT32;
                    case 14:
                        return TYPE_ENUM;
                    case 15:
                        return TYPE_SFIXED32;
                    case 16:
                        return TYPE_SFIXED64;
                    case 17:
                        return TYPE_SINT32;
                    case 18:
                        return TYPE_SINT64;
                    default:
                        return null;
                }
            }

            public static final Descriptors.c getDescriptor() {
                return FieldDescriptorProto.getDescriptor().l().get(0);
            }

            public static a0.d<Type> internalGetValueMap() {
                return a;
            }

            public final Descriptors.c getDescriptorForType() {
                return getDescriptor();
            }

            @Override // com.google.protobuf.a0.c
            public final int getNumber() {
                return this.value;
            }

            public final Descriptors.d getValueDescriptor() {
                return getDescriptor().k().get(ordinal());
            }

            @Deprecated
            public static Type valueOf(int i) {
                return forNumber(i);
            }

            public static Type valueOf(Descriptors.d dVar) {
                if (dVar.h() == getDescriptor()) {
                    return f0[dVar.g()];
                }
                throw new IllegalArgumentException("EnumValueDescriptor is not for this type.");
            }
        }

        /* loaded from: classes2.dex */
        public static class a extends com.google.protobuf.c<FieldDescriptorProto> {
            @Override // com.google.protobuf.t0
            /* renamed from: a */
            public FieldDescriptorProto parsePartialFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
                return new FieldDescriptorProto(jVar, rVar);
            }
        }

        public static FieldDescriptorProto getDefaultInstance() {
            return a;
        }

        public static final Descriptors.b getDescriptor() {
            return DescriptorProtos.l;
        }

        public static b newBuilder() {
            return a.toBuilder();
        }

        public static FieldDescriptorProto parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (FieldDescriptorProto) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static FieldDescriptorProto parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<FieldDescriptorProto> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof FieldDescriptorProto)) {
                return super.equals(obj);
            }
            FieldDescriptorProto fieldDescriptorProto = (FieldDescriptorProto) obj;
            if (hasName() != fieldDescriptorProto.hasName()) {
                return false;
            }
            if ((!hasName() || getName().equals(fieldDescriptorProto.getName())) && hasNumber() == fieldDescriptorProto.hasNumber()) {
                if ((!hasNumber() || getNumber() == fieldDescriptorProto.getNumber()) && hasLabel() == fieldDescriptorProto.hasLabel()) {
                    if ((!hasLabel() || this.label_ == fieldDescriptorProto.label_) && hasType() == fieldDescriptorProto.hasType()) {
                        if ((!hasType() || this.type_ == fieldDescriptorProto.type_) && hasTypeName() == fieldDescriptorProto.hasTypeName()) {
                            if ((!hasTypeName() || getTypeName().equals(fieldDescriptorProto.getTypeName())) && hasExtendee() == fieldDescriptorProto.hasExtendee()) {
                                if ((!hasExtendee() || getExtendee().equals(fieldDescriptorProto.getExtendee())) && hasDefaultValue() == fieldDescriptorProto.hasDefaultValue()) {
                                    if ((!hasDefaultValue() || getDefaultValue().equals(fieldDescriptorProto.getDefaultValue())) && hasOneofIndex() == fieldDescriptorProto.hasOneofIndex()) {
                                        if ((!hasOneofIndex() || getOneofIndex() == fieldDescriptorProto.getOneofIndex()) && hasJsonName() == fieldDescriptorProto.hasJsonName()) {
                                            if ((!hasJsonName() || getJsonName().equals(fieldDescriptorProto.getJsonName())) && hasOptions() == fieldDescriptorProto.hasOptions()) {
                                                if ((!hasOptions() || getOptions().equals(fieldDescriptorProto.getOptions())) && hasProto3Optional() == fieldDescriptorProto.hasProto3Optional()) {
                                                    return (!hasProto3Optional() || getProto3Optional() == fieldDescriptorProto.getProto3Optional()) && this.unknownFields.equals(fieldDescriptorProto.unknownFields);
                                                }
                                                return false;
                                            }
                                            return false;
                                        }
                                        return false;
                                    }
                                    return false;
                                }
                                return false;
                            }
                            return false;
                        }
                        return false;
                    }
                    return false;
                }
                return false;
            }
            return false;
        }

        public String getDefaultValue() {
            Object obj = this.defaultValue_;
            if (obj instanceof String) {
                return (String) obj;
            }
            ByteString byteString = (ByteString) obj;
            String stringUtf8 = byteString.toStringUtf8();
            if (byteString.isValidUtf8()) {
                this.defaultValue_ = stringUtf8;
            }
            return stringUtf8;
        }

        public ByteString getDefaultValueBytes() {
            Object obj = this.defaultValue_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.defaultValue_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        public String getExtendee() {
            Object obj = this.extendee_;
            if (obj instanceof String) {
                return (String) obj;
            }
            ByteString byteString = (ByteString) obj;
            String stringUtf8 = byteString.toStringUtf8();
            if (byteString.isValidUtf8()) {
                this.extendee_ = stringUtf8;
            }
            return stringUtf8;
        }

        public ByteString getExtendeeBytes() {
            Object obj = this.extendee_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.extendee_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        public String getJsonName() {
            Object obj = this.jsonName_;
            if (obj instanceof String) {
                return (String) obj;
            }
            ByteString byteString = (ByteString) obj;
            String stringUtf8 = byteString.toStringUtf8();
            if (byteString.isValidUtf8()) {
                this.jsonName_ = stringUtf8;
            }
            return stringUtf8;
        }

        public ByteString getJsonNameBytes() {
            Object obj = this.jsonName_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.jsonName_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        public Label getLabel() {
            Label valueOf = Label.valueOf(this.label_);
            return valueOf == null ? Label.LABEL_OPTIONAL : valueOf;
        }

        public String getName() {
            Object obj = this.name_;
            if (obj instanceof String) {
                return (String) obj;
            }
            ByteString byteString = (ByteString) obj;
            String stringUtf8 = byteString.toStringUtf8();
            if (byteString.isValidUtf8()) {
                this.name_ = stringUtf8;
            }
            return stringUtf8;
        }

        public ByteString getNameBytes() {
            Object obj = this.name_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.name_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        public int getNumber() {
            return this.number_;
        }

        public int getOneofIndex() {
            return this.oneofIndex_;
        }

        public FieldOptions getOptions() {
            FieldOptions fieldOptions = this.options_;
            return fieldOptions == null ? FieldOptions.getDefaultInstance() : fieldOptions;
        }

        public i getOptionsOrBuilder() {
            FieldOptions fieldOptions = this.options_;
            return fieldOptions == null ? FieldOptions.getDefaultInstance() : fieldOptions;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<FieldDescriptorProto> getParserForType() {
            return PARSER;
        }

        public boolean getProto3Optional() {
            return this.proto3Optional_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = (this.bitField0_ & 1) != 0 ? 0 + GeneratedMessageV3.computeStringSize(1, this.name_) : 0;
            if ((this.bitField0_ & 32) != 0) {
                computeStringSize += GeneratedMessageV3.computeStringSize(2, this.extendee_);
            }
            if ((this.bitField0_ & 2) != 0) {
                computeStringSize += CodedOutputStream.x(3, this.number_);
            }
            if ((this.bitField0_ & 4) != 0) {
                computeStringSize += CodedOutputStream.l(4, this.label_);
            }
            if ((this.bitField0_ & 8) != 0) {
                computeStringSize += CodedOutputStream.l(5, this.type_);
            }
            if ((this.bitField0_ & 16) != 0) {
                computeStringSize += GeneratedMessageV3.computeStringSize(6, this.typeName_);
            }
            if ((this.bitField0_ & 64) != 0) {
                computeStringSize += GeneratedMessageV3.computeStringSize(7, this.defaultValue_);
            }
            if ((this.bitField0_ & RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN) != 0) {
                computeStringSize += CodedOutputStream.G(8, getOptions());
            }
            if ((this.bitField0_ & 128) != 0) {
                computeStringSize += CodedOutputStream.x(9, this.oneofIndex_);
            }
            if ((this.bitField0_ & 256) != 0) {
                computeStringSize += GeneratedMessageV3.computeStringSize(10, this.jsonName_);
            }
            if ((this.bitField0_ & RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE) != 0) {
                computeStringSize += CodedOutputStream.e(17, this.proto3Optional_);
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        public Type getType() {
            Type valueOf = Type.valueOf(this.type_);
            return valueOf == null ? Type.TYPE_DOUBLE : valueOf;
        }

        public String getTypeName() {
            Object obj = this.typeName_;
            if (obj instanceof String) {
                return (String) obj;
            }
            ByteString byteString = (ByteString) obj;
            String stringUtf8 = byteString.toStringUtf8();
            if (byteString.isValidUtf8()) {
                this.typeName_ = stringUtf8;
            }
            return stringUtf8;
        }

        public ByteString getTypeNameBytes() {
            Object obj = this.typeName_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.typeName_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        public boolean hasDefaultValue() {
            return (this.bitField0_ & 64) != 0;
        }

        public boolean hasExtendee() {
            return (this.bitField0_ & 32) != 0;
        }

        public boolean hasJsonName() {
            return (this.bitField0_ & 256) != 0;
        }

        public boolean hasLabel() {
            return (this.bitField0_ & 4) != 0;
        }

        public boolean hasName() {
            return (this.bitField0_ & 1) != 0;
        }

        public boolean hasNumber() {
            return (this.bitField0_ & 2) != 0;
        }

        public boolean hasOneofIndex() {
            return (this.bitField0_ & 128) != 0;
        }

        public boolean hasOptions() {
            return (this.bitField0_ & RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN) != 0;
        }

        public boolean hasProto3Optional() {
            return (this.bitField0_ & RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE) != 0;
        }

        public boolean hasType() {
            return (this.bitField0_ & 8) != 0;
        }

        public boolean hasTypeName() {
            return (this.bitField0_ & 16) != 0;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = 779 + getDescriptor().hashCode();
            if (hasName()) {
                hashCode = (((hashCode * 37) + 1) * 53) + getName().hashCode();
            }
            if (hasNumber()) {
                hashCode = (((hashCode * 37) + 3) * 53) + getNumber();
            }
            if (hasLabel()) {
                hashCode = (((hashCode * 37) + 4) * 53) + this.label_;
            }
            if (hasType()) {
                hashCode = (((hashCode * 37) + 5) * 53) + this.type_;
            }
            if (hasTypeName()) {
                hashCode = (((hashCode * 37) + 6) * 53) + getTypeName().hashCode();
            }
            if (hasExtendee()) {
                hashCode = (((hashCode * 37) + 2) * 53) + getExtendee().hashCode();
            }
            if (hasDefaultValue()) {
                hashCode = (((hashCode * 37) + 7) * 53) + getDefaultValue().hashCode();
            }
            if (hasOneofIndex()) {
                hashCode = (((hashCode * 37) + 9) * 53) + getOneofIndex();
            }
            if (hasJsonName()) {
                hashCode = (((hashCode * 37) + 10) * 53) + getJsonName().hashCode();
            }
            if (hasOptions()) {
                hashCode = (((hashCode * 37) + 8) * 53) + getOptions().hashCode();
            }
            if (hasProto3Optional()) {
                hashCode = (((hashCode * 37) + 17) * 53) + a0.c(getProto3Optional());
            }
            int hashCode2 = (hashCode * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode2;
            return hashCode2;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return DescriptorProtos.m.d(FieldDescriptorProto.class, b.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b2 = this.memoizedIsInitialized;
            if (b2 == 1) {
                return true;
            }
            if (b2 == 0) {
                return false;
            }
            if (hasOptions() && !getOptions().isInitialized()) {
                this.memoizedIsInitialized = (byte) 0;
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new FieldDescriptorProto();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if ((this.bitField0_ & 1) != 0) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.name_);
            }
            if ((this.bitField0_ & 32) != 0) {
                GeneratedMessageV3.writeString(codedOutputStream, 2, this.extendee_);
            }
            if ((this.bitField0_ & 2) != 0) {
                codedOutputStream.G0(3, this.number_);
            }
            if ((this.bitField0_ & 4) != 0) {
                codedOutputStream.u0(4, this.label_);
            }
            if ((this.bitField0_ & 8) != 0) {
                codedOutputStream.u0(5, this.type_);
            }
            if ((this.bitField0_ & 16) != 0) {
                GeneratedMessageV3.writeString(codedOutputStream, 6, this.typeName_);
            }
            if ((this.bitField0_ & 64) != 0) {
                GeneratedMessageV3.writeString(codedOutputStream, 7, this.defaultValue_);
            }
            if ((this.bitField0_ & RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN) != 0) {
                codedOutputStream.K0(8, getOptions());
            }
            if ((this.bitField0_ & 128) != 0) {
                codedOutputStream.G0(9, this.oneofIndex_);
            }
            if ((this.bitField0_ & 256) != 0) {
                GeneratedMessageV3.writeString(codedOutputStream, 10, this.jsonName_);
            }
            if ((this.bitField0_ & RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE) != 0) {
                codedOutputStream.m0(17, this.proto3Optional_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes2.dex */
        public static final class b extends GeneratedMessageV3.b<b> implements h {
            public int a;
            public Object f0;
            public int g0;
            public int h0;
            public int i0;
            public Object j0;
            public Object k0;
            public Object l0;
            public int m0;
            public Object n0;
            public FieldOptions o0;
            public a1<FieldOptions, FieldOptions.b, i> p0;
            public boolean q0;

            public b A(int i) {
                this.a |= 128;
                this.m0 = i;
                onChanged();
                return this;
            }

            public b B(boolean z) {
                this.a |= RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE;
                this.q0 = z;
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: C */
            public b setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (b) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            public b D(Type type) {
                Objects.requireNonNull(type);
                this.a |= 8;
                this.i0 = type.getNumber();
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: E */
            public final b setUnknownFields(g1 g1Var) {
                return (b) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: a */
            public b addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (b) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            /* renamed from: b */
            public FieldDescriptorProto build() {
                FieldDescriptorProto buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            /* renamed from: d */
            public FieldDescriptorProto buildPartial() {
                FieldDescriptorProto fieldDescriptorProto = new FieldDescriptorProto(this);
                int i = this.a;
                int i2 = (i & 1) != 0 ? 1 : 0;
                fieldDescriptorProto.name_ = this.f0;
                if ((i & 2) != 0) {
                    fieldDescriptorProto.number_ = this.g0;
                    i2 |= 2;
                }
                if ((i & 4) != 0) {
                    i2 |= 4;
                }
                fieldDescriptorProto.label_ = this.h0;
                if ((i & 8) != 0) {
                    i2 |= 8;
                }
                fieldDescriptorProto.type_ = this.i0;
                if ((i & 16) != 0) {
                    i2 |= 16;
                }
                fieldDescriptorProto.typeName_ = this.j0;
                if ((i & 32) != 0) {
                    i2 |= 32;
                }
                fieldDescriptorProto.extendee_ = this.k0;
                if ((i & 64) != 0) {
                    i2 |= 64;
                }
                fieldDescriptorProto.defaultValue_ = this.l0;
                if ((i & 128) != 0) {
                    fieldDescriptorProto.oneofIndex_ = this.m0;
                    i2 |= 128;
                }
                if ((i & 256) != 0) {
                    i2 |= 256;
                }
                fieldDescriptorProto.jsonName_ = this.n0;
                if ((i & RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN) != 0) {
                    a1<FieldOptions, FieldOptions.b, i> a1Var = this.p0;
                    if (a1Var == null) {
                        fieldDescriptorProto.options_ = this.o0;
                    } else {
                        fieldDescriptorProto.options_ = a1Var.b();
                    }
                    i2 |= RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN;
                }
                if ((i & RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE) != 0) {
                    fieldDescriptorProto.proto3Optional_ = this.q0;
                    i2 |= RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE;
                }
                fieldDescriptorProto.bitField0_ = i2;
                onBuilt();
                return fieldDescriptorProto;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: e */
            public b clear() {
                super.clear();
                this.f0 = "";
                int i = this.a & (-2);
                this.a = i;
                this.g0 = 0;
                int i2 = i & (-3);
                this.a = i2;
                this.h0 = 1;
                int i3 = i2 & (-5);
                this.a = i3;
                this.i0 = 1;
                int i4 = i3 & (-9);
                this.a = i4;
                this.j0 = "";
                int i5 = i4 & (-17);
                this.a = i5;
                this.k0 = "";
                int i6 = i5 & (-33);
                this.a = i6;
                this.l0 = "";
                int i7 = i6 & (-65);
                this.a = i7;
                this.m0 = 0;
                int i8 = i7 & (-129);
                this.a = i8;
                this.n0 = "";
                this.a = i8 & (-257);
                a1<FieldOptions, FieldOptions.b, i> a1Var = this.p0;
                if (a1Var == null) {
                    this.o0 = null;
                } else {
                    a1Var.c();
                }
                int i9 = this.a & (-513);
                this.a = i9;
                this.q0 = false;
                this.a = i9 & (-1025);
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: f */
            public b clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (b) super.clearField(fieldDescriptor);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: g */
            public b clearOneof(Descriptors.g gVar) {
                return (b) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return DescriptorProtos.l;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            /* renamed from: h */
            public b clone() {
                return (b) super.clone();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return DescriptorProtos.m.d(FieldDescriptorProto.class, b.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return !o() || k().isInitialized();
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            /* renamed from: j */
            public FieldDescriptorProto getDefaultInstanceForType() {
                return FieldDescriptorProto.getDefaultInstance();
            }

            public FieldOptions k() {
                a1<FieldOptions, FieldOptions.b, i> a1Var = this.p0;
                if (a1Var == null) {
                    FieldOptions fieldOptions = this.o0;
                    return fieldOptions == null ? FieldOptions.getDefaultInstance() : fieldOptions;
                }
                return a1Var.f();
            }

            public final a1<FieldOptions, FieldOptions.b, i> l() {
                if (this.p0 == null) {
                    this.p0 = new a1<>(k(), getParentForChildren(), isClean());
                    this.o0 = null;
                }
                return this.p0;
            }

            public final void maybeForceBuilderInitialization() {
                if (GeneratedMessageV3.alwaysUseFieldBuilders) {
                    l();
                }
            }

            public boolean o() {
                return (this.a & RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN) != 0;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0021  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /* renamed from: p */
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public com.google.protobuf.DescriptorProtos.FieldDescriptorProto.b mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0<com.google.protobuf.DescriptorProtos$FieldDescriptorProto> r1 = com.google.protobuf.DescriptorProtos.FieldDescriptorProto.PARSER     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    com.google.protobuf.DescriptorProtos$FieldDescriptorProto r3 = (com.google.protobuf.DescriptorProtos.FieldDescriptorProto) r3     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    if (r3 == 0) goto Le
                    r2.r(r3)
                Le:
                    return r2
                Lf:
                    r3 = move-exception
                    goto L1f
                L11:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> Lf
                    com.google.protobuf.DescriptorProtos$FieldDescriptorProto r4 = (com.google.protobuf.DescriptorProtos.FieldDescriptorProto) r4     // Catch: java.lang.Throwable -> Lf
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1d
                    throw r3     // Catch: java.lang.Throwable -> L1d
                L1d:
                    r3 = move-exception
                    r0 = r4
                L1f:
                    if (r0 == 0) goto L24
                    r2.r(r0)
                L24:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.DescriptorProtos.FieldDescriptorProto.b.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):com.google.protobuf.DescriptorProtos$FieldDescriptorProto$b");
            }

            public b r(FieldDescriptorProto fieldDescriptorProto) {
                if (fieldDescriptorProto == FieldDescriptorProto.getDefaultInstance()) {
                    return this;
                }
                if (fieldDescriptorProto.hasName()) {
                    this.a |= 1;
                    this.f0 = fieldDescriptorProto.name_;
                    onChanged();
                }
                if (fieldDescriptorProto.hasNumber()) {
                    y(fieldDescriptorProto.getNumber());
                }
                if (fieldDescriptorProto.hasLabel()) {
                    x(fieldDescriptorProto.getLabel());
                }
                if (fieldDescriptorProto.hasType()) {
                    D(fieldDescriptorProto.getType());
                }
                if (fieldDescriptorProto.hasTypeName()) {
                    this.a |= 16;
                    this.j0 = fieldDescriptorProto.typeName_;
                    onChanged();
                }
                if (fieldDescriptorProto.hasExtendee()) {
                    this.a |= 32;
                    this.k0 = fieldDescriptorProto.extendee_;
                    onChanged();
                }
                if (fieldDescriptorProto.hasDefaultValue()) {
                    this.a |= 64;
                    this.l0 = fieldDescriptorProto.defaultValue_;
                    onChanged();
                }
                if (fieldDescriptorProto.hasOneofIndex()) {
                    A(fieldDescriptorProto.getOneofIndex());
                }
                if (fieldDescriptorProto.hasJsonName()) {
                    this.a |= 256;
                    this.n0 = fieldDescriptorProto.jsonName_;
                    onChanged();
                }
                if (fieldDescriptorProto.hasOptions()) {
                    t(fieldDescriptorProto.getOptions());
                }
                if (fieldDescriptorProto.hasProto3Optional()) {
                    B(fieldDescriptorProto.getProto3Optional());
                }
                mergeUnknownFields(fieldDescriptorProto.unknownFields);
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            /* renamed from: s */
            public b mergeFrom(l0 l0Var) {
                if (l0Var instanceof FieldDescriptorProto) {
                    return r((FieldDescriptorProto) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public b t(FieldOptions fieldOptions) {
                FieldOptions fieldOptions2;
                a1<FieldOptions, FieldOptions.b, i> a1Var = this.p0;
                if (a1Var == null) {
                    if ((this.a & RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN) != 0 && (fieldOptions2 = this.o0) != null && fieldOptions2 != FieldOptions.getDefaultInstance()) {
                        this.o0 = FieldOptions.newBuilder(this.o0).G(fieldOptions).buildPartial();
                    } else {
                        this.o0 = fieldOptions;
                    }
                    onChanged();
                } else {
                    a1Var.h(fieldOptions);
                }
                this.a |= RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN;
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: u */
            public final b mergeUnknownFields(g1 g1Var) {
                return (b) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: v */
            public b setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (b) super.setField(fieldDescriptor, obj);
            }

            public b x(Label label) {
                Objects.requireNonNull(label);
                this.a |= 4;
                this.h0 = label.getNumber();
                onChanged();
                return this;
            }

            public b y(int i) {
                this.a |= 2;
                this.g0 = i;
                onChanged();
                return this;
            }

            public b() {
                this.f0 = "";
                this.h0 = 1;
                this.i0 = 1;
                this.j0 = "";
                this.k0 = "";
                this.l0 = "";
                this.n0 = "";
                maybeForceBuilderInitialization();
            }

            public b(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.f0 = "";
                this.h0 = 1;
                this.i0 = 1;
                this.j0 = "";
                this.k0 = "";
                this.l0 = "";
                this.n0 = "";
                maybeForceBuilderInitialization();
            }
        }

        public static b newBuilder(FieldDescriptorProto fieldDescriptorProto) {
            return a.toBuilder().r(fieldDescriptorProto);
        }

        public static FieldDescriptorProto parseFrom(ByteBuffer byteBuffer, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        public FieldDescriptorProto(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static FieldDescriptorProto parseDelimitedFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
            return (FieldDescriptorProto) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static FieldDescriptorProto parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public FieldDescriptorProto getDefaultInstanceForType() {
            return a;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public b toBuilder() {
            return this == a ? new b() : new b().r(this);
        }

        public static FieldDescriptorProto parseFrom(ByteString byteString, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public b newBuilderForType() {
            return newBuilder();
        }

        public FieldDescriptorProto() {
            this.memoizedIsInitialized = (byte) -1;
            this.name_ = "";
            this.label_ = 1;
            this.type_ = 1;
            this.typeName_ = "";
            this.extendee_ = "";
            this.defaultValue_ = "";
            this.jsonName_ = "";
        }

        public static FieldDescriptorProto parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public b newBuilderForType(GeneratedMessageV3.c cVar) {
            return new b(cVar);
        }

        public static FieldDescriptorProto parseFrom(byte[] bArr, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static FieldDescriptorProto parseFrom(InputStream inputStream) throws IOException {
            return (FieldDescriptorProto) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static FieldDescriptorProto parseFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
            return (FieldDescriptorProto) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static FieldDescriptorProto parseFrom(com.google.protobuf.j jVar) throws IOException {
            return (FieldDescriptorProto) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static FieldDescriptorProto parseFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws IOException {
            return (FieldDescriptorProto) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }

        public FieldDescriptorProto(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        switch (J) {
                            case 0:
                                break;
                            case 10:
                                ByteString q = jVar.q();
                                this.bitField0_ = 1 | this.bitField0_;
                                this.name_ = q;
                                continue;
                            case 18:
                                ByteString q2 = jVar.q();
                                this.bitField0_ |= 32;
                                this.extendee_ = q2;
                                continue;
                            case 24:
                                this.bitField0_ |= 2;
                                this.number_ = jVar.x();
                                continue;
                            case 32:
                                int s = jVar.s();
                                if (Label.valueOf(s) == null) {
                                    g.x(4, s);
                                } else {
                                    this.bitField0_ |= 4;
                                    this.label_ = s;
                                    continue;
                                }
                            case 40:
                                int s2 = jVar.s();
                                if (Type.valueOf(s2) == null) {
                                    g.x(5, s2);
                                } else {
                                    this.bitField0_ |= 8;
                                    this.type_ = s2;
                                    continue;
                                }
                            case 50:
                                ByteString q3 = jVar.q();
                                this.bitField0_ |= 16;
                                this.typeName_ = q3;
                                continue;
                            case 58:
                                ByteString q4 = jVar.q();
                                this.bitField0_ |= 64;
                                this.defaultValue_ = q4;
                                continue;
                            case 66:
                                FieldOptions.b builder = (this.bitField0_ & RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN) != 0 ? this.options_.toBuilder() : null;
                                FieldOptions fieldOptions = (FieldOptions) jVar.z(FieldOptions.PARSER, rVar);
                                this.options_ = fieldOptions;
                                if (builder != null) {
                                    builder.G(fieldOptions);
                                    this.options_ = builder.buildPartial();
                                }
                                this.bitField0_ |= RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN;
                                continue;
                            case 72:
                                this.bitField0_ |= 128;
                                this.oneofIndex_ = jVar.x();
                                continue;
                            case 82:
                                ByteString q5 = jVar.q();
                                this.bitField0_ |= 256;
                                this.jsonName_ = q5;
                                continue;
                            case 136:
                                this.bitField0_ |= RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE;
                                this.proto3Optional_ = jVar.p();
                                continue;
                            default:
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                    break;
                                } else {
                                    continue;
                                }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }
    }

    /* loaded from: classes2.dex */
    public static final class FieldOptions extends GeneratedMessageV3.ExtendableMessage<FieldOptions> implements i {
        public static final int CTYPE_FIELD_NUMBER = 1;
        public static final int DEPRECATED_FIELD_NUMBER = 3;
        public static final int JSTYPE_FIELD_NUMBER = 6;
        public static final int LAZY_FIELD_NUMBER = 5;
        public static final int PACKED_FIELD_NUMBER = 2;
        public static final int UNINTERPRETED_OPTION_FIELD_NUMBER = 999;
        public static final int WEAK_FIELD_NUMBER = 10;
        private static final long serialVersionUID = 0;
        private int bitField0_;
        private int ctype_;
        private boolean deprecated_;
        private int jstype_;
        private boolean lazy_;
        private byte memoizedIsInitialized;
        private boolean packed_;
        private List<UninterpretedOption> uninterpretedOption_;
        private boolean weak_;
        public static final FieldOptions a = new FieldOptions();
        @Deprecated
        public static final t0<FieldOptions> PARSER = new a();

        /* loaded from: classes2.dex */
        public enum CType implements v0 {
            STRING(0),
            CORD(1),
            STRING_PIECE(2);
            
            public static final int CORD_VALUE = 1;
            public static final int STRING_PIECE_VALUE = 2;
            public static final int STRING_VALUE = 0;
            public static final a0.d<CType> a = new a();
            public static final CType[] f0 = values();
            private final int value;

            /* loaded from: classes2.dex */
            public static class a implements a0.d<CType> {
                @Override // com.google.protobuf.a0.d
                /* renamed from: a */
                public CType findValueByNumber(int i) {
                    return CType.forNumber(i);
                }
            }

            CType(int i) {
                this.value = i;
            }

            public static CType forNumber(int i) {
                if (i != 0) {
                    if (i != 1) {
                        if (i != 2) {
                            return null;
                        }
                        return STRING_PIECE;
                    }
                    return CORD;
                }
                return STRING;
            }

            public static final Descriptors.c getDescriptor() {
                return FieldOptions.getDescriptor().l().get(0);
            }

            public static a0.d<CType> internalGetValueMap() {
                return a;
            }

            public final Descriptors.c getDescriptorForType() {
                return getDescriptor();
            }

            @Override // com.google.protobuf.a0.c
            public final int getNumber() {
                return this.value;
            }

            public final Descriptors.d getValueDescriptor() {
                return getDescriptor().k().get(ordinal());
            }

            @Deprecated
            public static CType valueOf(int i) {
                return forNumber(i);
            }

            public static CType valueOf(Descriptors.d dVar) {
                if (dVar.h() == getDescriptor()) {
                    return f0[dVar.g()];
                }
                throw new IllegalArgumentException("EnumValueDescriptor is not for this type.");
            }
        }

        /* loaded from: classes2.dex */
        public enum JSType implements v0 {
            JS_NORMAL(0),
            JS_STRING(1),
            JS_NUMBER(2);
            
            public static final int JS_NORMAL_VALUE = 0;
            public static final int JS_NUMBER_VALUE = 2;
            public static final int JS_STRING_VALUE = 1;
            public static final a0.d<JSType> a = new a();
            public static final JSType[] f0 = values();
            private final int value;

            /* loaded from: classes2.dex */
            public static class a implements a0.d<JSType> {
                @Override // com.google.protobuf.a0.d
                /* renamed from: a */
                public JSType findValueByNumber(int i) {
                    return JSType.forNumber(i);
                }
            }

            JSType(int i) {
                this.value = i;
            }

            public static JSType forNumber(int i) {
                if (i != 0) {
                    if (i != 1) {
                        if (i != 2) {
                            return null;
                        }
                        return JS_NUMBER;
                    }
                    return JS_STRING;
                }
                return JS_NORMAL;
            }

            public static final Descriptors.c getDescriptor() {
                return FieldOptions.getDescriptor().l().get(1);
            }

            public static a0.d<JSType> internalGetValueMap() {
                return a;
            }

            public final Descriptors.c getDescriptorForType() {
                return getDescriptor();
            }

            @Override // com.google.protobuf.a0.c
            public final int getNumber() {
                return this.value;
            }

            public final Descriptors.d getValueDescriptor() {
                return getDescriptor().k().get(ordinal());
            }

            @Deprecated
            public static JSType valueOf(int i) {
                return forNumber(i);
            }

            public static JSType valueOf(Descriptors.d dVar) {
                if (dVar.h() == getDescriptor()) {
                    return f0[dVar.g()];
                }
                throw new IllegalArgumentException("EnumValueDescriptor is not for this type.");
            }
        }

        /* loaded from: classes2.dex */
        public static class a extends com.google.protobuf.c<FieldOptions> {
            @Override // com.google.protobuf.t0
            /* renamed from: a */
            public FieldOptions parsePartialFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
                return new FieldOptions(jVar, rVar);
            }
        }

        public static FieldOptions getDefaultInstance() {
            return a;
        }

        public static final Descriptors.b getDescriptor() {
            return DescriptorProtos.D;
        }

        public static b newBuilder() {
            return a.toBuilder();
        }

        public static FieldOptions parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (FieldOptions) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static FieldOptions parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<FieldOptions> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof FieldOptions)) {
                return super.equals(obj);
            }
            FieldOptions fieldOptions = (FieldOptions) obj;
            if (hasCtype() != fieldOptions.hasCtype()) {
                return false;
            }
            if ((!hasCtype() || this.ctype_ == fieldOptions.ctype_) && hasPacked() == fieldOptions.hasPacked()) {
                if ((!hasPacked() || getPacked() == fieldOptions.getPacked()) && hasJstype() == fieldOptions.hasJstype()) {
                    if ((!hasJstype() || this.jstype_ == fieldOptions.jstype_) && hasLazy() == fieldOptions.hasLazy()) {
                        if ((!hasLazy() || getLazy() == fieldOptions.getLazy()) && hasDeprecated() == fieldOptions.hasDeprecated()) {
                            if ((!hasDeprecated() || getDeprecated() == fieldOptions.getDeprecated()) && hasWeak() == fieldOptions.hasWeak()) {
                                return (!hasWeak() || getWeak() == fieldOptions.getWeak()) && getUninterpretedOptionList().equals(fieldOptions.getUninterpretedOptionList()) && this.unknownFields.equals(fieldOptions.unknownFields) && getExtensionFields().equals(fieldOptions.getExtensionFields());
                            }
                            return false;
                        }
                        return false;
                    }
                    return false;
                }
                return false;
            }
            return false;
        }

        public CType getCtype() {
            CType valueOf = CType.valueOf(this.ctype_);
            return valueOf == null ? CType.STRING : valueOf;
        }

        public boolean getDeprecated() {
            return this.deprecated_;
        }

        public JSType getJstype() {
            JSType valueOf = JSType.valueOf(this.jstype_);
            return valueOf == null ? JSType.JS_NORMAL : valueOf;
        }

        public boolean getLazy() {
            return this.lazy_;
        }

        public boolean getPacked() {
            return this.packed_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<FieldOptions> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int l = (this.bitField0_ & 1) != 0 ? CodedOutputStream.l(1, this.ctype_) + 0 : 0;
            if ((this.bitField0_ & 2) != 0) {
                l += CodedOutputStream.e(2, this.packed_);
            }
            if ((this.bitField0_ & 16) != 0) {
                l += CodedOutputStream.e(3, this.deprecated_);
            }
            if ((this.bitField0_ & 8) != 0) {
                l += CodedOutputStream.e(5, this.lazy_);
            }
            if ((this.bitField0_ & 4) != 0) {
                l += CodedOutputStream.l(6, this.jstype_);
            }
            if ((this.bitField0_ & 32) != 0) {
                l += CodedOutputStream.e(10, this.weak_);
            }
            for (int i2 = 0; i2 < this.uninterpretedOption_.size(); i2++) {
                l += CodedOutputStream.G(999, this.uninterpretedOption_.get(i2));
            }
            int extensionsSerializedSize = l + extensionsSerializedSize() + this.unknownFields.getSerializedSize();
            this.memoizedSize = extensionsSerializedSize;
            return extensionsSerializedSize;
        }

        public UninterpretedOption getUninterpretedOption(int i) {
            return this.uninterpretedOption_.get(i);
        }

        public int getUninterpretedOptionCount() {
            return this.uninterpretedOption_.size();
        }

        public List<UninterpretedOption> getUninterpretedOptionList() {
            return this.uninterpretedOption_;
        }

        public s getUninterpretedOptionOrBuilder(int i) {
            return this.uninterpretedOption_.get(i);
        }

        public List<? extends s> getUninterpretedOptionOrBuilderList() {
            return this.uninterpretedOption_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        public boolean getWeak() {
            return this.weak_;
        }

        public boolean hasCtype() {
            return (this.bitField0_ & 1) != 0;
        }

        public boolean hasDeprecated() {
            return (this.bitField0_ & 16) != 0;
        }

        public boolean hasJstype() {
            return (this.bitField0_ & 4) != 0;
        }

        public boolean hasLazy() {
            return (this.bitField0_ & 8) != 0;
        }

        public boolean hasPacked() {
            return (this.bitField0_ & 2) != 0;
        }

        public boolean hasWeak() {
            return (this.bitField0_ & 32) != 0;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = 779 + getDescriptor().hashCode();
            if (hasCtype()) {
                hashCode = (((hashCode * 37) + 1) * 53) + this.ctype_;
            }
            if (hasPacked()) {
                hashCode = (((hashCode * 37) + 2) * 53) + a0.c(getPacked());
            }
            if (hasJstype()) {
                hashCode = (((hashCode * 37) + 6) * 53) + this.jstype_;
            }
            if (hasLazy()) {
                hashCode = (((hashCode * 37) + 5) * 53) + a0.c(getLazy());
            }
            if (hasDeprecated()) {
                hashCode = (((hashCode * 37) + 3) * 53) + a0.c(getDeprecated());
            }
            if (hasWeak()) {
                hashCode = (((hashCode * 37) + 10) * 53) + a0.c(getWeak());
            }
            if (getUninterpretedOptionCount() > 0) {
                hashCode = (((hashCode * 37) + 999) * 53) + getUninterpretedOptionList().hashCode();
            }
            int hashFields = (com.google.protobuf.a.hashFields(hashCode, getExtensionFields()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashFields;
            return hashFields;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return DescriptorProtos.E.d(FieldOptions.class, b.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3.ExtendableMessage, com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b2 = this.memoizedIsInitialized;
            if (b2 == 1) {
                return true;
            }
            if (b2 == 0) {
                return false;
            }
            for (int i = 0; i < getUninterpretedOptionCount(); i++) {
                if (!getUninterpretedOption(i).isInitialized()) {
                    this.memoizedIsInitialized = (byte) 0;
                    return false;
                }
            }
            if (!extensionsAreInitialized()) {
                this.memoizedIsInitialized = (byte) 0;
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new FieldOptions();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            GeneratedMessageV3.ExtendableMessage<MessageType>.a newExtensionWriter = newExtensionWriter();
            if ((this.bitField0_ & 1) != 0) {
                codedOutputStream.u0(1, this.ctype_);
            }
            if ((this.bitField0_ & 2) != 0) {
                codedOutputStream.m0(2, this.packed_);
            }
            if ((this.bitField0_ & 16) != 0) {
                codedOutputStream.m0(3, this.deprecated_);
            }
            if ((this.bitField0_ & 8) != 0) {
                codedOutputStream.m0(5, this.lazy_);
            }
            if ((this.bitField0_ & 4) != 0) {
                codedOutputStream.u0(6, this.jstype_);
            }
            if ((this.bitField0_ & 32) != 0) {
                codedOutputStream.m0(10, this.weak_);
            }
            for (int i = 0; i < this.uninterpretedOption_.size(); i++) {
                codedOutputStream.K0(999, this.uninterpretedOption_.get(i));
            }
            newExtensionWriter.a(536870912, codedOutputStream);
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes2.dex */
        public static final class b extends GeneratedMessageV3.d<FieldOptions, b> implements i {
            public int f0;
            public int g0;
            public boolean h0;
            public int i0;
            public boolean j0;
            public boolean k0;
            public boolean l0;
            public List<UninterpretedOption> m0;
            public x0<UninterpretedOption, UninterpretedOption.b, s> n0;

            @Override // defpackage.d82, com.google.protobuf.o0
            /* renamed from: A */
            public FieldOptions getDefaultInstanceForType() {
                return FieldOptions.getDefaultInstance();
            }

            public UninterpretedOption B(int i) {
                x0<UninterpretedOption, UninterpretedOption.b, s> x0Var = this.n0;
                if (x0Var == null) {
                    return this.m0.get(i);
                }
                return x0Var.o(i);
            }

            public int C() {
                x0<UninterpretedOption, UninterpretedOption.b, s> x0Var = this.n0;
                if (x0Var == null) {
                    return this.m0.size();
                }
                return x0Var.n();
            }

            public final x0<UninterpretedOption, UninterpretedOption.b, s> D() {
                if (this.n0 == null) {
                    this.n0 = new x0<>(this.m0, (this.f0 & 64) != 0, getParentForChildren(), isClean());
                    this.m0 = null;
                }
                return this.n0;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0021  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /* renamed from: E */
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public com.google.protobuf.DescriptorProtos.FieldOptions.b mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0<com.google.protobuf.DescriptorProtos$FieldOptions> r1 = com.google.protobuf.DescriptorProtos.FieldOptions.PARSER     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    com.google.protobuf.DescriptorProtos$FieldOptions r3 = (com.google.protobuf.DescriptorProtos.FieldOptions) r3     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    if (r3 == 0) goto Le
                    r2.G(r3)
                Le:
                    return r2
                Lf:
                    r3 = move-exception
                    goto L1f
                L11:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> Lf
                    com.google.protobuf.DescriptorProtos$FieldOptions r4 = (com.google.protobuf.DescriptorProtos.FieldOptions) r4     // Catch: java.lang.Throwable -> Lf
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1d
                    throw r3     // Catch: java.lang.Throwable -> L1d
                L1d:
                    r3 = move-exception
                    r0 = r4
                L1f:
                    if (r0 == 0) goto L24
                    r2.G(r0)
                L24:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.DescriptorProtos.FieldOptions.b.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):com.google.protobuf.DescriptorProtos$FieldOptions$b");
            }

            public b G(FieldOptions fieldOptions) {
                if (fieldOptions == FieldOptions.getDefaultInstance()) {
                    return this;
                }
                if (fieldOptions.hasCtype()) {
                    J(fieldOptions.getCtype());
                }
                if (fieldOptions.hasPacked()) {
                    O(fieldOptions.getPacked());
                }
                if (fieldOptions.hasJstype()) {
                    M(fieldOptions.getJstype());
                }
                if (fieldOptions.hasLazy()) {
                    N(fieldOptions.getLazy());
                }
                if (fieldOptions.hasDeprecated()) {
                    K(fieldOptions.getDeprecated());
                }
                if (fieldOptions.hasWeak()) {
                    R(fieldOptions.getWeak());
                }
                if (this.n0 == null) {
                    if (!fieldOptions.uninterpretedOption_.isEmpty()) {
                        if (this.m0.isEmpty()) {
                            this.m0 = fieldOptions.uninterpretedOption_;
                            this.f0 &= -65;
                        } else {
                            y();
                            this.m0.addAll(fieldOptions.uninterpretedOption_);
                        }
                        onChanged();
                    }
                } else if (!fieldOptions.uninterpretedOption_.isEmpty()) {
                    if (!this.n0.u()) {
                        this.n0.b(fieldOptions.uninterpretedOption_);
                    } else {
                        this.n0.i();
                        this.n0 = null;
                        this.m0 = fieldOptions.uninterpretedOption_;
                        this.f0 &= -65;
                        this.n0 = GeneratedMessageV3.alwaysUseFieldBuilders ? D() : null;
                    }
                }
                j(fieldOptions);
                mergeUnknownFields(fieldOptions.unknownFields);
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            /* renamed from: H */
            public b mergeFrom(l0 l0Var) {
                if (l0Var instanceof FieldOptions) {
                    return G((FieldOptions) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: I */
            public final b mergeUnknownFields(g1 g1Var) {
                return (b) super.mergeUnknownFields(g1Var);
            }

            public b J(CType cType) {
                Objects.requireNonNull(cType);
                this.f0 |= 1;
                this.g0 = cType.getNumber();
                onChanged();
                return this;
            }

            public b K(boolean z) {
                this.f0 |= 16;
                this.k0 = z;
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d, com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: L */
            public b setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (b) super.setField(fieldDescriptor, obj);
            }

            public b M(JSType jSType) {
                Objects.requireNonNull(jSType);
                this.f0 |= 4;
                this.i0 = jSType.getNumber();
                onChanged();
                return this;
            }

            public b N(boolean z) {
                this.f0 |= 8;
                this.j0 = z;
                onChanged();
                return this;
            }

            public b O(boolean z) {
                this.f0 |= 2;
                this.h0 = z;
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d, com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: P */
            public b setRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (b) super.setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: Q */
            public final b setUnknownFields(g1 g1Var) {
                return (b) super.setUnknownFields(g1Var);
            }

            public b R(boolean z) {
                this.f0 |= 32;
                this.l0 = z;
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return DescriptorProtos.D;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return DescriptorProtos.E.d(FieldOptions.class, b.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d, com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                for (int i = 0; i < C(); i++) {
                    if (!B(i).isInitialized()) {
                        return false;
                    }
                }
                return h();
            }

            public final void maybeForceBuilderInitialization() {
                if (GeneratedMessageV3.alwaysUseFieldBuilders) {
                    D();
                }
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d
            /* renamed from: p */
            public b b(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (b) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            /* renamed from: r */
            public FieldOptions build() {
                FieldOptions buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            /* renamed from: s */
            public FieldOptions buildPartial() {
                FieldOptions fieldOptions = new FieldOptions(this);
                int i = this.f0;
                int i2 = (i & 1) != 0 ? 1 : 0;
                fieldOptions.ctype_ = this.g0;
                if ((i & 2) != 0) {
                    fieldOptions.packed_ = this.h0;
                    i2 |= 2;
                }
                if ((i & 4) != 0) {
                    i2 |= 4;
                }
                fieldOptions.jstype_ = this.i0;
                if ((i & 8) != 0) {
                    fieldOptions.lazy_ = this.j0;
                    i2 |= 8;
                }
                if ((i & 16) != 0) {
                    fieldOptions.deprecated_ = this.k0;
                    i2 |= 16;
                }
                if ((i & 32) != 0) {
                    fieldOptions.weak_ = this.l0;
                    i2 |= 32;
                }
                x0<UninterpretedOption, UninterpretedOption.b, s> x0Var = this.n0;
                if (x0Var != null) {
                    fieldOptions.uninterpretedOption_ = x0Var.g();
                } else {
                    if ((this.f0 & 64) != 0) {
                        this.m0 = Collections.unmodifiableList(this.m0);
                        this.f0 &= -65;
                    }
                    fieldOptions.uninterpretedOption_ = this.m0;
                }
                fieldOptions.bitField0_ = i2;
                onBuilt();
                return fieldOptions;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d
            /* renamed from: t */
            public b e() {
                super.clear();
                this.g0 = 0;
                int i = this.f0 & (-2);
                this.f0 = i;
                this.h0 = false;
                int i2 = i & (-3);
                this.f0 = i2;
                this.i0 = 0;
                int i3 = i2 & (-5);
                this.f0 = i3;
                this.j0 = false;
                int i4 = i3 & (-9);
                this.f0 = i4;
                this.k0 = false;
                int i5 = i4 & (-17);
                this.f0 = i5;
                this.l0 = false;
                this.f0 = i5 & (-33);
                x0<UninterpretedOption, UninterpretedOption.b, s> x0Var = this.n0;
                if (x0Var == null) {
                    this.m0 = Collections.emptyList();
                    this.f0 &= -65;
                } else {
                    x0Var.h();
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d
            /* renamed from: u */
            public b f(Descriptors.FieldDescriptor fieldDescriptor) {
                return (b) super.clearField(fieldDescriptor);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: v */
            public b clearOneof(Descriptors.g gVar) {
                return (b) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            /* renamed from: x */
            public b clone() {
                return (b) super.clone();
            }

            public final void y() {
                if ((this.f0 & 64) == 0) {
                    this.m0 = new ArrayList(this.m0);
                    this.f0 |= 64;
                }
            }

            public b() {
                this.g0 = 0;
                this.i0 = 0;
                this.m0 = Collections.emptyList();
                maybeForceBuilderInitialization();
            }

            public b(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.g0 = 0;
                this.i0 = 0;
                this.m0 = Collections.emptyList();
                maybeForceBuilderInitialization();
            }
        }

        public static b newBuilder(FieldOptions fieldOptions) {
            return a.toBuilder().G(fieldOptions);
        }

        public static FieldOptions parseFrom(ByteBuffer byteBuffer, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        public FieldOptions(GeneratedMessageV3.d<FieldOptions, ?> dVar) {
            super(dVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static FieldOptions parseDelimitedFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
            return (FieldOptions) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static FieldOptions parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3.ExtendableMessage, com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public FieldOptions getDefaultInstanceForType() {
            return a;
        }

        @Override // com.google.protobuf.GeneratedMessageV3.ExtendableMessage, com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public b toBuilder() {
            return this == a ? new b() : new b().G(this);
        }

        public static FieldOptions parseFrom(ByteString byteString, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3.ExtendableMessage, com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public b newBuilderForType() {
            return newBuilder();
        }

        public FieldOptions() {
            this.memoizedIsInitialized = (byte) -1;
            this.ctype_ = 0;
            this.jstype_ = 0;
            this.uninterpretedOption_ = Collections.emptyList();
        }

        public static FieldOptions parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public b newBuilderForType(GeneratedMessageV3.c cVar) {
            return new b(cVar);
        }

        public static FieldOptions parseFrom(byte[] bArr, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static FieldOptions parseFrom(InputStream inputStream) throws IOException {
            return (FieldOptions) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static FieldOptions parseFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
            return (FieldOptions) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        /* JADX WARN: Multi-variable type inference failed */
        public FieldOptions(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            boolean z2 = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 8) {
                                int s = jVar.s();
                                if (CType.valueOf(s) == null) {
                                    g.x(1, s);
                                } else {
                                    this.bitField0_ = 1 | this.bitField0_;
                                    this.ctype_ = s;
                                }
                            } else if (J == 16) {
                                this.bitField0_ |= 2;
                                this.packed_ = jVar.p();
                            } else if (J == 24) {
                                this.bitField0_ |= 16;
                                this.deprecated_ = jVar.p();
                            } else if (J == 40) {
                                this.bitField0_ |= 8;
                                this.lazy_ = jVar.p();
                            } else if (J == 48) {
                                int s2 = jVar.s();
                                if (JSType.valueOf(s2) == null) {
                                    g.x(6, s2);
                                } else {
                                    this.bitField0_ |= 4;
                                    this.jstype_ = s2;
                                }
                            } else if (J == 80) {
                                this.bitField0_ |= 32;
                                this.weak_ = jVar.p();
                            } else if (J != 7994) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                if (!(z2 & true)) {
                                    this.uninterpretedOption_ = new ArrayList();
                                    z2 |= true;
                                }
                                this.uninterpretedOption_.add(jVar.z(UninterpretedOption.PARSER, rVar));
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    if (z2 & true) {
                        this.uninterpretedOption_ = Collections.unmodifiableList(this.uninterpretedOption_);
                    }
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static FieldOptions parseFrom(com.google.protobuf.j jVar) throws IOException {
            return (FieldOptions) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static FieldOptions parseFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws IOException {
            return (FieldOptions) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes2.dex */
    public static final class FileDescriptorProto extends GeneratedMessageV3 implements o0 {
        public static final int DEPENDENCY_FIELD_NUMBER = 3;
        public static final int ENUM_TYPE_FIELD_NUMBER = 5;
        public static final int EXTENSION_FIELD_NUMBER = 7;
        public static final int MESSAGE_TYPE_FIELD_NUMBER = 4;
        public static final int NAME_FIELD_NUMBER = 1;
        public static final int OPTIONS_FIELD_NUMBER = 8;
        public static final int PACKAGE_FIELD_NUMBER = 2;
        public static final int PUBLIC_DEPENDENCY_FIELD_NUMBER = 10;
        public static final int SERVICE_FIELD_NUMBER = 6;
        public static final int SOURCE_CODE_INFO_FIELD_NUMBER = 9;
        public static final int SYNTAX_FIELD_NUMBER = 12;
        public static final int WEAK_DEPENDENCY_FIELD_NUMBER = 11;
        private static final long serialVersionUID = 0;
        private int bitField0_;
        private cz1 dependency_;
        private List<EnumDescriptorProto> enumType_;
        private List<FieldDescriptorProto> extension_;
        private byte memoizedIsInitialized;
        private List<DescriptorProto> messageType_;
        private volatile Object name_;
        private FileOptions options_;
        private volatile Object package_;
        private a0.g publicDependency_;
        private List<ServiceDescriptorProto> service_;
        private SourceCodeInfo sourceCodeInfo_;
        private volatile Object syntax_;
        private a0.g weakDependency_;
        public static final FileDescriptorProto a = new FileDescriptorProto();
        @Deprecated
        public static final t0<FileDescriptorProto> PARSER = new a();

        /* loaded from: classes2.dex */
        public static class a extends com.google.protobuf.c<FileDescriptorProto> {
            @Override // com.google.protobuf.t0
            /* renamed from: a */
            public FileDescriptorProto parsePartialFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
                return new FileDescriptorProto(jVar, rVar);
            }
        }

        public static FileDescriptorProto getDefaultInstance() {
            return a;
        }

        public static final Descriptors.b getDescriptor() {
            return DescriptorProtos.b;
        }

        public static b newBuilder() {
            return a.toBuilder();
        }

        public static FileDescriptorProto parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (FileDescriptorProto) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static FileDescriptorProto parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<FileDescriptorProto> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof FileDescriptorProto)) {
                return super.equals(obj);
            }
            FileDescriptorProto fileDescriptorProto = (FileDescriptorProto) obj;
            if (hasName() != fileDescriptorProto.hasName()) {
                return false;
            }
            if ((!hasName() || getName().equals(fileDescriptorProto.getName())) && hasPackage() == fileDescriptorProto.hasPackage()) {
                if ((!hasPackage() || getPackage().equals(fileDescriptorProto.getPackage())) && m17getDependencyList().equals(fileDescriptorProto.m17getDependencyList()) && getPublicDependencyList().equals(fileDescriptorProto.getPublicDependencyList()) && getWeakDependencyList().equals(fileDescriptorProto.getWeakDependencyList()) && getMessageTypeList().equals(fileDescriptorProto.getMessageTypeList()) && getEnumTypeList().equals(fileDescriptorProto.getEnumTypeList()) && getServiceList().equals(fileDescriptorProto.getServiceList()) && getExtensionList().equals(fileDescriptorProto.getExtensionList()) && hasOptions() == fileDescriptorProto.hasOptions()) {
                    if ((!hasOptions() || getOptions().equals(fileDescriptorProto.getOptions())) && hasSourceCodeInfo() == fileDescriptorProto.hasSourceCodeInfo()) {
                        if ((!hasSourceCodeInfo() || getSourceCodeInfo().equals(fileDescriptorProto.getSourceCodeInfo())) && hasSyntax() == fileDescriptorProto.hasSyntax()) {
                            return (!hasSyntax() || getSyntax().equals(fileDescriptorProto.getSyntax())) && this.unknownFields.equals(fileDescriptorProto.unknownFields);
                        }
                        return false;
                    }
                    return false;
                }
                return false;
            }
            return false;
        }

        public String getDependency(int i) {
            return this.dependency_.get(i);
        }

        public ByteString getDependencyBytes(int i) {
            return this.dependency_.e1(i);
        }

        public int getDependencyCount() {
            return this.dependency_.size();
        }

        public EnumDescriptorProto getEnumType(int i) {
            return this.enumType_.get(i);
        }

        public int getEnumTypeCount() {
            return this.enumType_.size();
        }

        public List<EnumDescriptorProto> getEnumTypeList() {
            return this.enumType_;
        }

        public c getEnumTypeOrBuilder(int i) {
            return this.enumType_.get(i);
        }

        public List<? extends c> getEnumTypeOrBuilderList() {
            return this.enumType_;
        }

        public FieldDescriptorProto getExtension(int i) {
            return this.extension_.get(i);
        }

        public int getExtensionCount() {
            return this.extension_.size();
        }

        public List<FieldDescriptorProto> getExtensionList() {
            return this.extension_;
        }

        public h getExtensionOrBuilder(int i) {
            return this.extension_.get(i);
        }

        public List<? extends h> getExtensionOrBuilderList() {
            return this.extension_;
        }

        public DescriptorProto getMessageType(int i) {
            return this.messageType_.get(i);
        }

        public int getMessageTypeCount() {
            return this.messageType_.size();
        }

        public List<DescriptorProto> getMessageTypeList() {
            return this.messageType_;
        }

        public b getMessageTypeOrBuilder(int i) {
            return this.messageType_.get(i);
        }

        public List<? extends b> getMessageTypeOrBuilderList() {
            return this.messageType_;
        }

        public String getName() {
            Object obj = this.name_;
            if (obj instanceof String) {
                return (String) obj;
            }
            ByteString byteString = (ByteString) obj;
            String stringUtf8 = byteString.toStringUtf8();
            if (byteString.isValidUtf8()) {
                this.name_ = stringUtf8;
            }
            return stringUtf8;
        }

        public ByteString getNameBytes() {
            Object obj = this.name_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.name_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        public FileOptions getOptions() {
            FileOptions fileOptions = this.options_;
            return fileOptions == null ? FileOptions.getDefaultInstance() : fileOptions;
        }

        public j getOptionsOrBuilder() {
            FileOptions fileOptions = this.options_;
            return fileOptions == null ? FileOptions.getDefaultInstance() : fileOptions;
        }

        public String getPackage() {
            Object obj = this.package_;
            if (obj instanceof String) {
                return (String) obj;
            }
            ByteString byteString = (ByteString) obj;
            String stringUtf8 = byteString.toStringUtf8();
            if (byteString.isValidUtf8()) {
                this.package_ = stringUtf8;
            }
            return stringUtf8;
        }

        public ByteString getPackageBytes() {
            Object obj = this.package_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.package_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<FileDescriptorProto> getParserForType() {
            return PARSER;
        }

        public int getPublicDependency(int i) {
            return this.publicDependency_.getInt(i);
        }

        public int getPublicDependencyCount() {
            return this.publicDependency_.size();
        }

        public List<Integer> getPublicDependencyList() {
            return this.publicDependency_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = (this.bitField0_ & 1) != 0 ? GeneratedMessageV3.computeStringSize(1, this.name_) + 0 : 0;
            if ((this.bitField0_ & 2) != 0) {
                computeStringSize += GeneratedMessageV3.computeStringSize(2, this.package_);
            }
            int i2 = 0;
            for (int i3 = 0; i3 < this.dependency_.size(); i3++) {
                i2 += GeneratedMessageV3.computeStringSizeNoTag(this.dependency_.j(i3));
            }
            int size = computeStringSize + i2 + (m17getDependencyList().size() * 1);
            for (int i4 = 0; i4 < this.messageType_.size(); i4++) {
                size += CodedOutputStream.G(4, this.messageType_.get(i4));
            }
            for (int i5 = 0; i5 < this.enumType_.size(); i5++) {
                size += CodedOutputStream.G(5, this.enumType_.get(i5));
            }
            for (int i6 = 0; i6 < this.service_.size(); i6++) {
                size += CodedOutputStream.G(6, this.service_.get(i6));
            }
            for (int i7 = 0; i7 < this.extension_.size(); i7++) {
                size += CodedOutputStream.G(7, this.extension_.get(i7));
            }
            if ((this.bitField0_ & 4) != 0) {
                size += CodedOutputStream.G(8, getOptions());
            }
            if ((this.bitField0_ & 8) != 0) {
                size += CodedOutputStream.G(9, getSourceCodeInfo());
            }
            int i8 = 0;
            for (int i9 = 0; i9 < this.publicDependency_.size(); i9++) {
                i8 += CodedOutputStream.y(this.publicDependency_.getInt(i9));
            }
            int size2 = size + i8 + (getPublicDependencyList().size() * 1);
            int i10 = 0;
            for (int i11 = 0; i11 < this.weakDependency_.size(); i11++) {
                i10 += CodedOutputStream.y(this.weakDependency_.getInt(i11));
            }
            int size3 = size2 + i10 + (getWeakDependencyList().size() * 1);
            if ((this.bitField0_ & 16) != 0) {
                size3 += GeneratedMessageV3.computeStringSize(12, this.syntax_);
            }
            int serializedSize = size3 + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        public ServiceDescriptorProto getService(int i) {
            return this.service_.get(i);
        }

        public int getServiceCount() {
            return this.service_.size();
        }

        public List<ServiceDescriptorProto> getServiceList() {
            return this.service_;
        }

        public p getServiceOrBuilder(int i) {
            return this.service_.get(i);
        }

        public List<? extends p> getServiceOrBuilderList() {
            return this.service_;
        }

        public SourceCodeInfo getSourceCodeInfo() {
            SourceCodeInfo sourceCodeInfo = this.sourceCodeInfo_;
            return sourceCodeInfo == null ? SourceCodeInfo.getDefaultInstance() : sourceCodeInfo;
        }

        public r getSourceCodeInfoOrBuilder() {
            SourceCodeInfo sourceCodeInfo = this.sourceCodeInfo_;
            return sourceCodeInfo == null ? SourceCodeInfo.getDefaultInstance() : sourceCodeInfo;
        }

        public String getSyntax() {
            Object obj = this.syntax_;
            if (obj instanceof String) {
                return (String) obj;
            }
            ByteString byteString = (ByteString) obj;
            String stringUtf8 = byteString.toStringUtf8();
            if (byteString.isValidUtf8()) {
                this.syntax_ = stringUtf8;
            }
            return stringUtf8;
        }

        public ByteString getSyntaxBytes() {
            Object obj = this.syntax_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.syntax_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        public int getWeakDependency(int i) {
            return this.weakDependency_.getInt(i);
        }

        public int getWeakDependencyCount() {
            return this.weakDependency_.size();
        }

        public List<Integer> getWeakDependencyList() {
            return this.weakDependency_;
        }

        public boolean hasName() {
            return (this.bitField0_ & 1) != 0;
        }

        public boolean hasOptions() {
            return (this.bitField0_ & 4) != 0;
        }

        public boolean hasPackage() {
            return (this.bitField0_ & 2) != 0;
        }

        public boolean hasSourceCodeInfo() {
            return (this.bitField0_ & 8) != 0;
        }

        public boolean hasSyntax() {
            return (this.bitField0_ & 16) != 0;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = 779 + getDescriptor().hashCode();
            if (hasName()) {
                hashCode = (((hashCode * 37) + 1) * 53) + getName().hashCode();
            }
            if (hasPackage()) {
                hashCode = (((hashCode * 37) + 2) * 53) + getPackage().hashCode();
            }
            if (getDependencyCount() > 0) {
                hashCode = (((hashCode * 37) + 3) * 53) + m17getDependencyList().hashCode();
            }
            if (getPublicDependencyCount() > 0) {
                hashCode = (((hashCode * 37) + 10) * 53) + getPublicDependencyList().hashCode();
            }
            if (getWeakDependencyCount() > 0) {
                hashCode = (((hashCode * 37) + 11) * 53) + getWeakDependencyList().hashCode();
            }
            if (getMessageTypeCount() > 0) {
                hashCode = (((hashCode * 37) + 4) * 53) + getMessageTypeList().hashCode();
            }
            if (getEnumTypeCount() > 0) {
                hashCode = (((hashCode * 37) + 5) * 53) + getEnumTypeList().hashCode();
            }
            if (getServiceCount() > 0) {
                hashCode = (((hashCode * 37) + 6) * 53) + getServiceList().hashCode();
            }
            if (getExtensionCount() > 0) {
                hashCode = (((hashCode * 37) + 7) * 53) + getExtensionList().hashCode();
            }
            if (hasOptions()) {
                hashCode = (((hashCode * 37) + 8) * 53) + getOptions().hashCode();
            }
            if (hasSourceCodeInfo()) {
                hashCode = (((hashCode * 37) + 9) * 53) + getSourceCodeInfo().hashCode();
            }
            if (hasSyntax()) {
                hashCode = (((hashCode * 37) + 12) * 53) + getSyntax().hashCode();
            }
            int hashCode2 = (hashCode * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode2;
            return hashCode2;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return DescriptorProtos.c.d(FileDescriptorProto.class, b.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b2 = this.memoizedIsInitialized;
            if (b2 == 1) {
                return true;
            }
            if (b2 == 0) {
                return false;
            }
            for (int i = 0; i < getMessageTypeCount(); i++) {
                if (!getMessageType(i).isInitialized()) {
                    this.memoizedIsInitialized = (byte) 0;
                    return false;
                }
            }
            for (int i2 = 0; i2 < getEnumTypeCount(); i2++) {
                if (!getEnumType(i2).isInitialized()) {
                    this.memoizedIsInitialized = (byte) 0;
                    return false;
                }
            }
            for (int i3 = 0; i3 < getServiceCount(); i3++) {
                if (!getService(i3).isInitialized()) {
                    this.memoizedIsInitialized = (byte) 0;
                    return false;
                }
            }
            for (int i4 = 0; i4 < getExtensionCount(); i4++) {
                if (!getExtension(i4).isInitialized()) {
                    this.memoizedIsInitialized = (byte) 0;
                    return false;
                }
            }
            if (hasOptions() && !getOptions().isInitialized()) {
                this.memoizedIsInitialized = (byte) 0;
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new FileDescriptorProto();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if ((this.bitField0_ & 1) != 0) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.name_);
            }
            if ((this.bitField0_ & 2) != 0) {
                GeneratedMessageV3.writeString(codedOutputStream, 2, this.package_);
            }
            for (int i = 0; i < this.dependency_.size(); i++) {
                GeneratedMessageV3.writeString(codedOutputStream, 3, this.dependency_.j(i));
            }
            for (int i2 = 0; i2 < this.messageType_.size(); i2++) {
                codedOutputStream.K0(4, this.messageType_.get(i2));
            }
            for (int i3 = 0; i3 < this.enumType_.size(); i3++) {
                codedOutputStream.K0(5, this.enumType_.get(i3));
            }
            for (int i4 = 0; i4 < this.service_.size(); i4++) {
                codedOutputStream.K0(6, this.service_.get(i4));
            }
            for (int i5 = 0; i5 < this.extension_.size(); i5++) {
                codedOutputStream.K0(7, this.extension_.get(i5));
            }
            if ((this.bitField0_ & 4) != 0) {
                codedOutputStream.K0(8, getOptions());
            }
            if ((this.bitField0_ & 8) != 0) {
                codedOutputStream.K0(9, getSourceCodeInfo());
            }
            for (int i6 = 0; i6 < this.publicDependency_.size(); i6++) {
                codedOutputStream.G0(10, this.publicDependency_.getInt(i6));
            }
            for (int i7 = 0; i7 < this.weakDependency_.size(); i7++) {
                codedOutputStream.G0(11, this.weakDependency_.getInt(i7));
            }
            if ((this.bitField0_ & 16) != 0) {
                GeneratedMessageV3.writeString(codedOutputStream, 12, this.syntax_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes2.dex */
        public static final class b extends GeneratedMessageV3.b<b> implements o0 {
            public int a;
            public Object f0;
            public Object g0;
            public cz1 h0;
            public a0.g i0;
            public a0.g j0;
            public List<DescriptorProto> k0;
            public x0<DescriptorProto, DescriptorProto.b, b> l0;
            public List<EnumDescriptorProto> m0;
            public x0<EnumDescriptorProto, EnumDescriptorProto.b, c> n0;
            public List<ServiceDescriptorProto> o0;
            public x0<ServiceDescriptorProto, ServiceDescriptorProto.b, p> p0;
            public List<FieldDescriptorProto> q0;
            public x0<FieldDescriptorProto, FieldDescriptorProto.b, h> r0;
            public FileOptions s0;
            public a1<FileOptions, FileOptions.b, j> t0;
            public SourceCodeInfo u0;
            public a1<SourceCodeInfo, SourceCodeInfo.b, r> v0;
            public Object w0;

            public FieldDescriptorProto A(int i) {
                x0<FieldDescriptorProto, FieldDescriptorProto.b, h> x0Var = this.r0;
                if (x0Var == null) {
                    return this.q0.get(i);
                }
                return x0Var.o(i);
            }

            public int B() {
                x0<FieldDescriptorProto, FieldDescriptorProto.b, h> x0Var = this.r0;
                if (x0Var == null) {
                    return this.q0.size();
                }
                return x0Var.n();
            }

            public final x0<FieldDescriptorProto, FieldDescriptorProto.b, h> C() {
                if (this.r0 == null) {
                    this.r0 = new x0<>(this.q0, (this.a & 256) != 0, getParentForChildren(), isClean());
                    this.q0 = null;
                }
                return this.r0;
            }

            public DescriptorProto D(int i) {
                x0<DescriptorProto, DescriptorProto.b, b> x0Var = this.l0;
                if (x0Var == null) {
                    return this.k0.get(i);
                }
                return x0Var.o(i);
            }

            public int E() {
                x0<DescriptorProto, DescriptorProto.b, b> x0Var = this.l0;
                if (x0Var == null) {
                    return this.k0.size();
                }
                return x0Var.n();
            }

            public final x0<DescriptorProto, DescriptorProto.b, b> G() {
                if (this.l0 == null) {
                    this.l0 = new x0<>(this.k0, (this.a & 32) != 0, getParentForChildren(), isClean());
                    this.k0 = null;
                }
                return this.l0;
            }

            public FileOptions H() {
                a1<FileOptions, FileOptions.b, j> a1Var = this.t0;
                if (a1Var == null) {
                    FileOptions fileOptions = this.s0;
                    return fileOptions == null ? FileOptions.getDefaultInstance() : fileOptions;
                }
                return a1Var.f();
            }

            public final a1<FileOptions, FileOptions.b, j> I() {
                if (this.t0 == null) {
                    this.t0 = new a1<>(H(), getParentForChildren(), isClean());
                    this.s0 = null;
                }
                return this.t0;
            }

            public ServiceDescriptorProto J(int i) {
                x0<ServiceDescriptorProto, ServiceDescriptorProto.b, p> x0Var = this.p0;
                if (x0Var == null) {
                    return this.o0.get(i);
                }
                return x0Var.o(i);
            }

            public int K() {
                x0<ServiceDescriptorProto, ServiceDescriptorProto.b, p> x0Var = this.p0;
                if (x0Var == null) {
                    return this.o0.size();
                }
                return x0Var.n();
            }

            public final x0<ServiceDescriptorProto, ServiceDescriptorProto.b, p> L() {
                if (this.p0 == null) {
                    this.p0 = new x0<>(this.o0, (this.a & 128) != 0, getParentForChildren(), isClean());
                    this.o0 = null;
                }
                return this.p0;
            }

            public SourceCodeInfo M() {
                a1<SourceCodeInfo, SourceCodeInfo.b, r> a1Var = this.v0;
                if (a1Var == null) {
                    SourceCodeInfo sourceCodeInfo = this.u0;
                    return sourceCodeInfo == null ? SourceCodeInfo.getDefaultInstance() : sourceCodeInfo;
                }
                return a1Var.f();
            }

            public final a1<SourceCodeInfo, SourceCodeInfo.b, r> N() {
                if (this.v0 == null) {
                    this.v0 = new a1<>(M(), getParentForChildren(), isClean());
                    this.u0 = null;
                }
                return this.v0;
            }

            public boolean O() {
                return (this.a & RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN) != 0;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0021  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /* renamed from: P */
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public com.google.protobuf.DescriptorProtos.FileDescriptorProto.b mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0<com.google.protobuf.DescriptorProtos$FileDescriptorProto> r1 = com.google.protobuf.DescriptorProtos.FileDescriptorProto.PARSER     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    com.google.protobuf.DescriptorProtos$FileDescriptorProto r3 = (com.google.protobuf.DescriptorProtos.FileDescriptorProto) r3     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    if (r3 == 0) goto Le
                    r2.Q(r3)
                Le:
                    return r2
                Lf:
                    r3 = move-exception
                    goto L1f
                L11:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> Lf
                    com.google.protobuf.DescriptorProtos$FileDescriptorProto r4 = (com.google.protobuf.DescriptorProtos.FileDescriptorProto) r4     // Catch: java.lang.Throwable -> Lf
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1d
                    throw r3     // Catch: java.lang.Throwable -> L1d
                L1d:
                    r3 = move-exception
                    r0 = r4
                L1f:
                    if (r0 == 0) goto L24
                    r2.Q(r0)
                L24:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.DescriptorProtos.FileDescriptorProto.b.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):com.google.protobuf.DescriptorProtos$FileDescriptorProto$b");
            }

            public b Q(FileDescriptorProto fileDescriptorProto) {
                if (fileDescriptorProto == FileDescriptorProto.getDefaultInstance()) {
                    return this;
                }
                if (fileDescriptorProto.hasName()) {
                    this.a |= 1;
                    this.f0 = fileDescriptorProto.name_;
                    onChanged();
                }
                if (fileDescriptorProto.hasPackage()) {
                    this.a |= 2;
                    this.g0 = fileDescriptorProto.package_;
                    onChanged();
                }
                if (!fileDescriptorProto.dependency_.isEmpty()) {
                    if (this.h0.isEmpty()) {
                        this.h0 = fileDescriptorProto.dependency_;
                        this.a &= -5;
                    } else {
                        k();
                        this.h0.addAll(fileDescriptorProto.dependency_);
                    }
                    onChanged();
                }
                if (!fileDescriptorProto.publicDependency_.isEmpty()) {
                    if (this.i0.isEmpty()) {
                        this.i0 = fileDescriptorProto.publicDependency_;
                        this.a &= -9;
                    } else {
                        r();
                        this.i0.addAll(fileDescriptorProto.publicDependency_);
                    }
                    onChanged();
                }
                if (!fileDescriptorProto.weakDependency_.isEmpty()) {
                    if (this.j0.isEmpty()) {
                        this.j0 = fileDescriptorProto.weakDependency_;
                        this.a &= -17;
                    } else {
                        t();
                        this.j0.addAll(fileDescriptorProto.weakDependency_);
                    }
                    onChanged();
                }
                if (this.l0 == null) {
                    if (!fileDescriptorProto.messageType_.isEmpty()) {
                        if (this.k0.isEmpty()) {
                            this.k0 = fileDescriptorProto.messageType_;
                            this.a &= -33;
                        } else {
                            p();
                            this.k0.addAll(fileDescriptorProto.messageType_);
                        }
                        onChanged();
                    }
                } else if (!fileDescriptorProto.messageType_.isEmpty()) {
                    if (!this.l0.u()) {
                        this.l0.b(fileDescriptorProto.messageType_);
                    } else {
                        this.l0.i();
                        this.l0 = null;
                        this.k0 = fileDescriptorProto.messageType_;
                        this.a &= -33;
                        this.l0 = GeneratedMessageV3.alwaysUseFieldBuilders ? G() : null;
                    }
                }
                if (this.n0 == null) {
                    if (!fileDescriptorProto.enumType_.isEmpty()) {
                        if (this.m0.isEmpty()) {
                            this.m0 = fileDescriptorProto.enumType_;
                            this.a &= -65;
                        } else {
                            l();
                            this.m0.addAll(fileDescriptorProto.enumType_);
                        }
                        onChanged();
                    }
                } else if (!fileDescriptorProto.enumType_.isEmpty()) {
                    if (!this.n0.u()) {
                        this.n0.b(fileDescriptorProto.enumType_);
                    } else {
                        this.n0.i();
                        this.n0 = null;
                        this.m0 = fileDescriptorProto.enumType_;
                        this.a &= -65;
                        this.n0 = GeneratedMessageV3.alwaysUseFieldBuilders ? y() : null;
                    }
                }
                if (this.p0 == null) {
                    if (!fileDescriptorProto.service_.isEmpty()) {
                        if (this.o0.isEmpty()) {
                            this.o0 = fileDescriptorProto.service_;
                            this.a &= -129;
                        } else {
                            s();
                            this.o0.addAll(fileDescriptorProto.service_);
                        }
                        onChanged();
                    }
                } else if (!fileDescriptorProto.service_.isEmpty()) {
                    if (!this.p0.u()) {
                        this.p0.b(fileDescriptorProto.service_);
                    } else {
                        this.p0.i();
                        this.p0 = null;
                        this.o0 = fileDescriptorProto.service_;
                        this.a &= -129;
                        this.p0 = GeneratedMessageV3.alwaysUseFieldBuilders ? L() : null;
                    }
                }
                if (this.r0 == null) {
                    if (!fileDescriptorProto.extension_.isEmpty()) {
                        if (this.q0.isEmpty()) {
                            this.q0 = fileDescriptorProto.extension_;
                            this.a &= -257;
                        } else {
                            o();
                            this.q0.addAll(fileDescriptorProto.extension_);
                        }
                        onChanged();
                    }
                } else if (!fileDescriptorProto.extension_.isEmpty()) {
                    if (!this.r0.u()) {
                        this.r0.b(fileDescriptorProto.extension_);
                    } else {
                        this.r0.i();
                        this.r0 = null;
                        this.q0 = fileDescriptorProto.extension_;
                        this.a &= -257;
                        this.r0 = GeneratedMessageV3.alwaysUseFieldBuilders ? C() : null;
                    }
                }
                if (fileDescriptorProto.hasOptions()) {
                    S(fileDescriptorProto.getOptions());
                }
                if (fileDescriptorProto.hasSourceCodeInfo()) {
                    T(fileDescriptorProto.getSourceCodeInfo());
                }
                if (fileDescriptorProto.hasSyntax()) {
                    this.a |= 2048;
                    this.w0 = fileDescriptorProto.syntax_;
                    onChanged();
                }
                mergeUnknownFields(fileDescriptorProto.unknownFields);
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            /* renamed from: R */
            public b mergeFrom(l0 l0Var) {
                if (l0Var instanceof FileDescriptorProto) {
                    return Q((FileDescriptorProto) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public b S(FileOptions fileOptions) {
                FileOptions fileOptions2;
                a1<FileOptions, FileOptions.b, j> a1Var = this.t0;
                if (a1Var == null) {
                    if ((this.a & RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN) != 0 && (fileOptions2 = this.s0) != null && fileOptions2 != FileOptions.getDefaultInstance()) {
                        this.s0 = FileOptions.newBuilder(this.s0).G(fileOptions).buildPartial();
                    } else {
                        this.s0 = fileOptions;
                    }
                    onChanged();
                } else {
                    a1Var.h(fileOptions);
                }
                this.a |= RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN;
                return this;
            }

            public b T(SourceCodeInfo sourceCodeInfo) {
                SourceCodeInfo sourceCodeInfo2;
                a1<SourceCodeInfo, SourceCodeInfo.b, r> a1Var = this.v0;
                if (a1Var == null) {
                    if ((this.a & RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE) != 0 && (sourceCodeInfo2 = this.u0) != null && sourceCodeInfo2 != SourceCodeInfo.getDefaultInstance()) {
                        this.u0 = SourceCodeInfo.newBuilder(this.u0).p(sourceCodeInfo).buildPartial();
                    } else {
                        this.u0 = sourceCodeInfo;
                    }
                    onChanged();
                } else {
                    a1Var.h(sourceCodeInfo);
                }
                this.a |= RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE;
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: U */
            public final b mergeUnknownFields(g1 g1Var) {
                return (b) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: V */
            public b setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (b) super.setField(fieldDescriptor, obj);
            }

            public b W(String str) {
                Objects.requireNonNull(str);
                this.a |= 1;
                this.f0 = str;
                onChanged();
                return this;
            }

            public b X(String str) {
                Objects.requireNonNull(str);
                this.a |= 2;
                this.g0 = str;
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: Y */
            public b setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (b) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: Z */
            public final b setUnknownFields(g1 g1Var) {
                return (b) super.setUnknownFields(g1Var);
            }

            public b a(DescriptorProto descriptorProto) {
                x0<DescriptorProto, DescriptorProto.b, b> x0Var = this.l0;
                if (x0Var == null) {
                    Objects.requireNonNull(descriptorProto);
                    p();
                    this.k0.add(descriptorProto);
                    onChanged();
                } else {
                    x0Var.f(descriptorProto);
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: b */
            public b addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (b) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            /* renamed from: d */
            public FileDescriptorProto build() {
                FileDescriptorProto buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            /* renamed from: e */
            public FileDescriptorProto buildPartial() {
                FileDescriptorProto fileDescriptorProto = new FileDescriptorProto(this);
                int i = this.a;
                int i2 = (i & 1) != 0 ? 1 : 0;
                fileDescriptorProto.name_ = this.f0;
                if ((i & 2) != 0) {
                    i2 |= 2;
                }
                fileDescriptorProto.package_ = this.g0;
                if ((this.a & 4) != 0) {
                    this.h0 = this.h0.x();
                    this.a &= -5;
                }
                fileDescriptorProto.dependency_ = this.h0;
                if ((this.a & 8) != 0) {
                    this.i0.l();
                    this.a &= -9;
                }
                fileDescriptorProto.publicDependency_ = this.i0;
                if ((this.a & 16) != 0) {
                    this.j0.l();
                    this.a &= -17;
                }
                fileDescriptorProto.weakDependency_ = this.j0;
                x0<DescriptorProto, DescriptorProto.b, b> x0Var = this.l0;
                if (x0Var != null) {
                    fileDescriptorProto.messageType_ = x0Var.g();
                } else {
                    if ((this.a & 32) != 0) {
                        this.k0 = Collections.unmodifiableList(this.k0);
                        this.a &= -33;
                    }
                    fileDescriptorProto.messageType_ = this.k0;
                }
                x0<EnumDescriptorProto, EnumDescriptorProto.b, c> x0Var2 = this.n0;
                if (x0Var2 != null) {
                    fileDescriptorProto.enumType_ = x0Var2.g();
                } else {
                    if ((this.a & 64) != 0) {
                        this.m0 = Collections.unmodifiableList(this.m0);
                        this.a &= -65;
                    }
                    fileDescriptorProto.enumType_ = this.m0;
                }
                x0<ServiceDescriptorProto, ServiceDescriptorProto.b, p> x0Var3 = this.p0;
                if (x0Var3 != null) {
                    fileDescriptorProto.service_ = x0Var3.g();
                } else {
                    if ((this.a & 128) != 0) {
                        this.o0 = Collections.unmodifiableList(this.o0);
                        this.a &= -129;
                    }
                    fileDescriptorProto.service_ = this.o0;
                }
                x0<FieldDescriptorProto, FieldDescriptorProto.b, h> x0Var4 = this.r0;
                if (x0Var4 != null) {
                    fileDescriptorProto.extension_ = x0Var4.g();
                } else {
                    if ((this.a & 256) != 0) {
                        this.q0 = Collections.unmodifiableList(this.q0);
                        this.a &= -257;
                    }
                    fileDescriptorProto.extension_ = this.q0;
                }
                if ((i & RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN) != 0) {
                    a1<FileOptions, FileOptions.b, j> a1Var = this.t0;
                    if (a1Var == null) {
                        fileDescriptorProto.options_ = this.s0;
                    } else {
                        fileDescriptorProto.options_ = a1Var.b();
                    }
                    i2 |= 4;
                }
                if ((i & RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE) != 0) {
                    a1<SourceCodeInfo, SourceCodeInfo.b, r> a1Var2 = this.v0;
                    if (a1Var2 == null) {
                        fileDescriptorProto.sourceCodeInfo_ = this.u0;
                    } else {
                        fileDescriptorProto.sourceCodeInfo_ = a1Var2.b();
                    }
                    i2 |= 8;
                }
                if ((i & 2048) != 0) {
                    i2 |= 16;
                }
                fileDescriptorProto.syntax_ = this.w0;
                fileDescriptorProto.bitField0_ = i2;
                onBuilt();
                return fileDescriptorProto;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: f */
            public b clear() {
                super.clear();
                this.f0 = "";
                int i = this.a & (-2);
                this.a = i;
                this.g0 = "";
                int i2 = i & (-3);
                this.a = i2;
                this.h0 = d0.h0;
                this.a = i2 & (-5);
                this.i0 = GeneratedMessageV3.emptyIntList();
                this.a &= -9;
                this.j0 = GeneratedMessageV3.emptyIntList();
                this.a &= -17;
                x0<DescriptorProto, DescriptorProto.b, b> x0Var = this.l0;
                if (x0Var == null) {
                    this.k0 = Collections.emptyList();
                    this.a &= -33;
                } else {
                    x0Var.h();
                }
                x0<EnumDescriptorProto, EnumDescriptorProto.b, c> x0Var2 = this.n0;
                if (x0Var2 == null) {
                    this.m0 = Collections.emptyList();
                    this.a &= -65;
                } else {
                    x0Var2.h();
                }
                x0<ServiceDescriptorProto, ServiceDescriptorProto.b, p> x0Var3 = this.p0;
                if (x0Var3 == null) {
                    this.o0 = Collections.emptyList();
                    this.a &= -129;
                } else {
                    x0Var3.h();
                }
                x0<FieldDescriptorProto, FieldDescriptorProto.b, h> x0Var4 = this.r0;
                if (x0Var4 == null) {
                    this.q0 = Collections.emptyList();
                    this.a &= -257;
                } else {
                    x0Var4.h();
                }
                a1<FileOptions, FileOptions.b, j> a1Var = this.t0;
                if (a1Var == null) {
                    this.s0 = null;
                } else {
                    a1Var.c();
                }
                this.a &= -513;
                a1<SourceCodeInfo, SourceCodeInfo.b, r> a1Var2 = this.v0;
                if (a1Var2 == null) {
                    this.u0 = null;
                } else {
                    a1Var2.c();
                }
                int i3 = this.a & (-1025);
                this.a = i3;
                this.w0 = "";
                this.a = i3 & (-2049);
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: g */
            public b clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (b) super.clearField(fieldDescriptor);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return DescriptorProtos.b;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: h */
            public b clearOneof(Descriptors.g gVar) {
                return (b) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return DescriptorProtos.c.d(FileDescriptorProto.class, b.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                for (int i = 0; i < E(); i++) {
                    if (!D(i).isInitialized()) {
                        return false;
                    }
                }
                for (int i2 = 0; i2 < x(); i2++) {
                    if (!v(i2).isInitialized()) {
                        return false;
                    }
                }
                for (int i3 = 0; i3 < K(); i3++) {
                    if (!J(i3).isInitialized()) {
                        return false;
                    }
                }
                for (int i4 = 0; i4 < B(); i4++) {
                    if (!A(i4).isInitialized()) {
                        return false;
                    }
                }
                return !O() || H().isInitialized();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            /* renamed from: j */
            public b clone() {
                return (b) super.clone();
            }

            public final void k() {
                if ((this.a & 4) == 0) {
                    this.h0 = new d0(this.h0);
                    this.a |= 4;
                }
            }

            public final void l() {
                if ((this.a & 64) == 0) {
                    this.m0 = new ArrayList(this.m0);
                    this.a |= 64;
                }
            }

            public final void maybeForceBuilderInitialization() {
                if (GeneratedMessageV3.alwaysUseFieldBuilders) {
                    G();
                    y();
                    L();
                    C();
                    I();
                    N();
                }
            }

            public final void o() {
                if ((this.a & 256) == 0) {
                    this.q0 = new ArrayList(this.q0);
                    this.a |= 256;
                }
            }

            public final void p() {
                if ((this.a & 32) == 0) {
                    this.k0 = new ArrayList(this.k0);
                    this.a |= 32;
                }
            }

            public final void r() {
                if ((this.a & 8) == 0) {
                    this.i0 = GeneratedMessageV3.mutableCopy(this.i0);
                    this.a |= 8;
                }
            }

            public final void s() {
                if ((this.a & 128) == 0) {
                    this.o0 = new ArrayList(this.o0);
                    this.a |= 128;
                }
            }

            public final void t() {
                if ((this.a & 16) == 0) {
                    this.j0 = GeneratedMessageV3.mutableCopy(this.j0);
                    this.a |= 16;
                }
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            /* renamed from: u */
            public FileDescriptorProto getDefaultInstanceForType() {
                return FileDescriptorProto.getDefaultInstance();
            }

            public EnumDescriptorProto v(int i) {
                x0<EnumDescriptorProto, EnumDescriptorProto.b, c> x0Var = this.n0;
                if (x0Var == null) {
                    return this.m0.get(i);
                }
                return x0Var.o(i);
            }

            public int x() {
                x0<EnumDescriptorProto, EnumDescriptorProto.b, c> x0Var = this.n0;
                if (x0Var == null) {
                    return this.m0.size();
                }
                return x0Var.n();
            }

            public final x0<EnumDescriptorProto, EnumDescriptorProto.b, c> y() {
                if (this.n0 == null) {
                    this.n0 = new x0<>(this.m0, (this.a & 64) != 0, getParentForChildren(), isClean());
                    this.m0 = null;
                }
                return this.n0;
            }

            public b() {
                this.f0 = "";
                this.g0 = "";
                this.h0 = d0.h0;
                this.i0 = GeneratedMessageV3.emptyIntList();
                this.j0 = GeneratedMessageV3.emptyIntList();
                this.k0 = Collections.emptyList();
                this.m0 = Collections.emptyList();
                this.o0 = Collections.emptyList();
                this.q0 = Collections.emptyList();
                this.w0 = "";
                maybeForceBuilderInitialization();
            }

            public b(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.f0 = "";
                this.g0 = "";
                this.h0 = d0.h0;
                this.i0 = GeneratedMessageV3.emptyIntList();
                this.j0 = GeneratedMessageV3.emptyIntList();
                this.k0 = Collections.emptyList();
                this.m0 = Collections.emptyList();
                this.o0 = Collections.emptyList();
                this.q0 = Collections.emptyList();
                this.w0 = "";
                maybeForceBuilderInitialization();
            }
        }

        public static b newBuilder(FileDescriptorProto fileDescriptorProto) {
            return a.toBuilder().Q(fileDescriptorProto);
        }

        public static FileDescriptorProto parseFrom(ByteBuffer byteBuffer, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        /* renamed from: getDependencyList */
        public dw2 m17getDependencyList() {
            return this.dependency_;
        }

        public FileDescriptorProto(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static FileDescriptorProto parseDelimitedFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
            return (FileDescriptorProto) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static FileDescriptorProto parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public FileDescriptorProto getDefaultInstanceForType() {
            return a;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public b toBuilder() {
            return this == a ? new b() : new b().Q(this);
        }

        public static FileDescriptorProto parseFrom(ByteString byteString, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public b newBuilderForType() {
            return newBuilder();
        }

        public FileDescriptorProto() {
            this.memoizedIsInitialized = (byte) -1;
            this.name_ = "";
            this.package_ = "";
            this.dependency_ = d0.h0;
            this.publicDependency_ = GeneratedMessageV3.emptyIntList();
            this.weakDependency_ = GeneratedMessageV3.emptyIntList();
            this.messageType_ = Collections.emptyList();
            this.enumType_ = Collections.emptyList();
            this.service_ = Collections.emptyList();
            this.extension_ = Collections.emptyList();
            this.syntax_ = "";
        }

        public static FileDescriptorProto parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public b newBuilderForType(GeneratedMessageV3.c cVar) {
            return new b(cVar);
        }

        public static FileDescriptorProto parseFrom(byte[] bArr, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static FileDescriptorProto parseFrom(InputStream inputStream) throws IOException {
            return (FileDescriptorProto) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static FileDescriptorProto parseFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
            return (FileDescriptorProto) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static FileDescriptorProto parseFrom(com.google.protobuf.j jVar) throws IOException {
            return (FileDescriptorProto) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static FileDescriptorProto parseFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws IOException {
            return (FileDescriptorProto) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }

        /* JADX WARN: Multi-variable type inference failed */
        public FileDescriptorProto(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            boolean z2 = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        switch (J) {
                            case 0:
                                break;
                            case 10:
                                ByteString q = jVar.q();
                                this.bitField0_ |= 1;
                                this.name_ = q;
                                continue;
                            case 18:
                                ByteString q2 = jVar.q();
                                this.bitField0_ |= 2;
                                this.package_ = q2;
                                continue;
                            case 26:
                                ByteString q3 = jVar.q();
                                boolean z3 = (z2 ? 1 : 0) & true;
                                z2 = z2;
                                if (!z3) {
                                    this.dependency_ = new d0();
                                    z2 = (z2 ? 1 : 0) | true;
                                }
                                this.dependency_.W(q3);
                                continue;
                            case 34:
                                boolean z4 = (z2 ? 1 : 0) & true;
                                z2 = z2;
                                if (!z4) {
                                    this.messageType_ = new ArrayList();
                                    z2 = (z2 ? 1 : 0) | true;
                                }
                                this.messageType_.add(jVar.z(DescriptorProto.PARSER, rVar));
                                continue;
                            case 42:
                                boolean z5 = (z2 ? 1 : 0) & true;
                                z2 = z2;
                                if (!z5) {
                                    this.enumType_ = new ArrayList();
                                    z2 = (z2 ? 1 : 0) | true;
                                }
                                this.enumType_.add(jVar.z(EnumDescriptorProto.PARSER, rVar));
                                continue;
                            case 50:
                                boolean z6 = (z2 ? 1 : 0) & true;
                                z2 = z2;
                                if (!z6) {
                                    this.service_ = new ArrayList();
                                    z2 = (z2 ? 1 : 0) | true;
                                }
                                this.service_.add(jVar.z(ServiceDescriptorProto.PARSER, rVar));
                                continue;
                            case 58:
                                boolean z7 = (z2 ? 1 : 0) & true;
                                z2 = z2;
                                if (!z7) {
                                    this.extension_ = new ArrayList();
                                    z2 = (z2 ? 1 : 0) | true;
                                }
                                this.extension_.add(jVar.z(FieldDescriptorProto.PARSER, rVar));
                                continue;
                            case 66:
                                FileOptions.b builder = (this.bitField0_ & 4) != 0 ? this.options_.toBuilder() : null;
                                FileOptions fileOptions = (FileOptions) jVar.z(FileOptions.PARSER, rVar);
                                this.options_ = fileOptions;
                                if (builder != null) {
                                    builder.G(fileOptions);
                                    this.options_ = builder.buildPartial();
                                }
                                this.bitField0_ |= 4;
                                continue;
                            case 74:
                                SourceCodeInfo.b builder2 = (this.bitField0_ & 8) != 0 ? this.sourceCodeInfo_.toBuilder() : null;
                                SourceCodeInfo sourceCodeInfo = (SourceCodeInfo) jVar.z(SourceCodeInfo.PARSER, rVar);
                                this.sourceCodeInfo_ = sourceCodeInfo;
                                if (builder2 != null) {
                                    builder2.p(sourceCodeInfo);
                                    this.sourceCodeInfo_ = builder2.buildPartial();
                                }
                                this.bitField0_ |= 8;
                                continue;
                            case 80:
                                boolean z8 = (z2 ? 1 : 0) & true;
                                z2 = z2;
                                if (!z8) {
                                    this.publicDependency_ = GeneratedMessageV3.newIntList();
                                    z2 = (z2 ? 1 : 0) | true;
                                }
                                this.publicDependency_.X(jVar.x());
                                continue;
                            case 82:
                                int o = jVar.o(jVar.B());
                                boolean z9 = (z2 ? 1 : 0) & true;
                                z2 = z2;
                                if (!z9) {
                                    z2 = z2;
                                    if (jVar.d() > 0) {
                                        this.publicDependency_ = GeneratedMessageV3.newIntList();
                                        z2 = (z2 ? 1 : 0) | true;
                                    }
                                }
                                while (jVar.d() > 0) {
                                    this.publicDependency_.X(jVar.x());
                                }
                                jVar.n(o);
                                continue;
                            case 88:
                                boolean z10 = (z2 ? 1 : 0) & true;
                                z2 = z2;
                                if (!z10) {
                                    this.weakDependency_ = GeneratedMessageV3.newIntList();
                                    z2 = (z2 ? 1 : 0) | true;
                                }
                                this.weakDependency_.X(jVar.x());
                                continue;
                            case 90:
                                int o2 = jVar.o(jVar.B());
                                boolean z11 = (z2 ? 1 : 0) & true;
                                z2 = z2;
                                if (!z11) {
                                    z2 = z2;
                                    if (jVar.d() > 0) {
                                        this.weakDependency_ = GeneratedMessageV3.newIntList();
                                        z2 = (z2 ? 1 : 0) | true;
                                    }
                                }
                                while (jVar.d() > 0) {
                                    this.weakDependency_.X(jVar.x());
                                }
                                jVar.n(o2);
                                continue;
                            case 98:
                                ByteString q4 = jVar.q();
                                this.bitField0_ |= 16;
                                this.syntax_ = q4;
                                continue;
                            default:
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                    break;
                                } else {
                                    continue;
                                }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    if ((z2 ? 1 : 0) & true) {
                        this.dependency_ = this.dependency_.x();
                    }
                    if ((z2 ? 1 : 0) & true) {
                        this.messageType_ = Collections.unmodifiableList(this.messageType_);
                    }
                    if ((z2 ? 1 : 0) & true) {
                        this.enumType_ = Collections.unmodifiableList(this.enumType_);
                    }
                    if ((z2 ? 1 : 0) & true) {
                        this.service_ = Collections.unmodifiableList(this.service_);
                    }
                    if ((z2 ? 1 : 0) & true) {
                        this.extension_ = Collections.unmodifiableList(this.extension_);
                    }
                    if ((z2 ? 1 : 0) & true) {
                        this.publicDependency_.l();
                    }
                    if ((z2 ? 1 : 0) & true) {
                        this.weakDependency_.l();
                    }
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }
    }

    /* loaded from: classes2.dex */
    public static final class FileOptions extends GeneratedMessageV3.ExtendableMessage<FileOptions> implements j {
        public static final int CC_ENABLE_ARENAS_FIELD_NUMBER = 31;
        public static final int CC_GENERIC_SERVICES_FIELD_NUMBER = 16;
        public static final int CSHARP_NAMESPACE_FIELD_NUMBER = 37;
        public static final int DEPRECATED_FIELD_NUMBER = 23;
        public static final int GO_PACKAGE_FIELD_NUMBER = 11;
        public static final int JAVA_GENERATE_EQUALS_AND_HASH_FIELD_NUMBER = 20;
        public static final int JAVA_GENERIC_SERVICES_FIELD_NUMBER = 17;
        public static final int JAVA_MULTIPLE_FILES_FIELD_NUMBER = 10;
        public static final int JAVA_OUTER_CLASSNAME_FIELD_NUMBER = 8;
        public static final int JAVA_PACKAGE_FIELD_NUMBER = 1;
        public static final int JAVA_STRING_CHECK_UTF8_FIELD_NUMBER = 27;
        public static final int OBJC_CLASS_PREFIX_FIELD_NUMBER = 36;
        public static final int OPTIMIZE_FOR_FIELD_NUMBER = 9;
        public static final int PHP_CLASS_PREFIX_FIELD_NUMBER = 40;
        public static final int PHP_GENERIC_SERVICES_FIELD_NUMBER = 42;
        public static final int PHP_METADATA_NAMESPACE_FIELD_NUMBER = 44;
        public static final int PHP_NAMESPACE_FIELD_NUMBER = 41;
        public static final int PY_GENERIC_SERVICES_FIELD_NUMBER = 18;
        public static final int RUBY_PACKAGE_FIELD_NUMBER = 45;
        public static final int SWIFT_PREFIX_FIELD_NUMBER = 39;
        public static final int UNINTERPRETED_OPTION_FIELD_NUMBER = 999;
        private static final long serialVersionUID = 0;
        private int bitField0_;
        private boolean ccEnableArenas_;
        private boolean ccGenericServices_;
        private volatile Object csharpNamespace_;
        private boolean deprecated_;
        private volatile Object goPackage_;
        private boolean javaGenerateEqualsAndHash_;
        private boolean javaGenericServices_;
        private boolean javaMultipleFiles_;
        private volatile Object javaOuterClassname_;
        private volatile Object javaPackage_;
        private boolean javaStringCheckUtf8_;
        private byte memoizedIsInitialized;
        private volatile Object objcClassPrefix_;
        private int optimizeFor_;
        private volatile Object phpClassPrefix_;
        private boolean phpGenericServices_;
        private volatile Object phpMetadataNamespace_;
        private volatile Object phpNamespace_;
        private boolean pyGenericServices_;
        private volatile Object rubyPackage_;
        private volatile Object swiftPrefix_;
        private List<UninterpretedOption> uninterpretedOption_;
        public static final FileOptions a = new FileOptions();
        @Deprecated
        public static final t0<FileOptions> PARSER = new a();

        /* loaded from: classes2.dex */
        public enum OptimizeMode implements v0 {
            SPEED(1),
            CODE_SIZE(2),
            LITE_RUNTIME(3);
            
            public static final int CODE_SIZE_VALUE = 2;
            public static final int LITE_RUNTIME_VALUE = 3;
            public static final int SPEED_VALUE = 1;
            public static final a0.d<OptimizeMode> a = new a();
            public static final OptimizeMode[] f0 = values();
            private final int value;

            /* loaded from: classes2.dex */
            public static class a implements a0.d<OptimizeMode> {
                @Override // com.google.protobuf.a0.d
                /* renamed from: a */
                public OptimizeMode findValueByNumber(int i) {
                    return OptimizeMode.forNumber(i);
                }
            }

            OptimizeMode(int i) {
                this.value = i;
            }

            public static OptimizeMode forNumber(int i) {
                if (i != 1) {
                    if (i != 2) {
                        if (i != 3) {
                            return null;
                        }
                        return LITE_RUNTIME;
                    }
                    return CODE_SIZE;
                }
                return SPEED;
            }

            public static final Descriptors.c getDescriptor() {
                return FileOptions.getDescriptor().l().get(0);
            }

            public static a0.d<OptimizeMode> internalGetValueMap() {
                return a;
            }

            public final Descriptors.c getDescriptorForType() {
                return getDescriptor();
            }

            @Override // com.google.protobuf.a0.c
            public final int getNumber() {
                return this.value;
            }

            public final Descriptors.d getValueDescriptor() {
                return getDescriptor().k().get(ordinal());
            }

            @Deprecated
            public static OptimizeMode valueOf(int i) {
                return forNumber(i);
            }

            public static OptimizeMode valueOf(Descriptors.d dVar) {
                if (dVar.h() == getDescriptor()) {
                    return f0[dVar.g()];
                }
                throw new IllegalArgumentException("EnumValueDescriptor is not for this type.");
            }
        }

        /* loaded from: classes2.dex */
        public static class a extends com.google.protobuf.c<FileOptions> {
            @Override // com.google.protobuf.t0
            /* renamed from: a */
            public FileOptions parsePartialFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
                return new FileOptions(jVar, rVar);
            }
        }

        public static FileOptions getDefaultInstance() {
            return a;
        }

        public static final Descriptors.b getDescriptor() {
            return DescriptorProtos.z;
        }

        public static b newBuilder() {
            return a.toBuilder();
        }

        public static FileOptions parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (FileOptions) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static FileOptions parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<FileOptions> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof FileOptions)) {
                return super.equals(obj);
            }
            FileOptions fileOptions = (FileOptions) obj;
            if (hasJavaPackage() != fileOptions.hasJavaPackage()) {
                return false;
            }
            if ((!hasJavaPackage() || getJavaPackage().equals(fileOptions.getJavaPackage())) && hasJavaOuterClassname() == fileOptions.hasJavaOuterClassname()) {
                if ((!hasJavaOuterClassname() || getJavaOuterClassname().equals(fileOptions.getJavaOuterClassname())) && hasJavaMultipleFiles() == fileOptions.hasJavaMultipleFiles()) {
                    if ((!hasJavaMultipleFiles() || getJavaMultipleFiles() == fileOptions.getJavaMultipleFiles()) && hasJavaGenerateEqualsAndHash() == fileOptions.hasJavaGenerateEqualsAndHash()) {
                        if ((!hasJavaGenerateEqualsAndHash() || getJavaGenerateEqualsAndHash() == fileOptions.getJavaGenerateEqualsAndHash()) && hasJavaStringCheckUtf8() == fileOptions.hasJavaStringCheckUtf8()) {
                            if ((!hasJavaStringCheckUtf8() || getJavaStringCheckUtf8() == fileOptions.getJavaStringCheckUtf8()) && hasOptimizeFor() == fileOptions.hasOptimizeFor()) {
                                if ((!hasOptimizeFor() || this.optimizeFor_ == fileOptions.optimizeFor_) && hasGoPackage() == fileOptions.hasGoPackage()) {
                                    if ((!hasGoPackage() || getGoPackage().equals(fileOptions.getGoPackage())) && hasCcGenericServices() == fileOptions.hasCcGenericServices()) {
                                        if ((!hasCcGenericServices() || getCcGenericServices() == fileOptions.getCcGenericServices()) && hasJavaGenericServices() == fileOptions.hasJavaGenericServices()) {
                                            if ((!hasJavaGenericServices() || getJavaGenericServices() == fileOptions.getJavaGenericServices()) && hasPyGenericServices() == fileOptions.hasPyGenericServices()) {
                                                if ((!hasPyGenericServices() || getPyGenericServices() == fileOptions.getPyGenericServices()) && hasPhpGenericServices() == fileOptions.hasPhpGenericServices()) {
                                                    if ((!hasPhpGenericServices() || getPhpGenericServices() == fileOptions.getPhpGenericServices()) && hasDeprecated() == fileOptions.hasDeprecated()) {
                                                        if ((!hasDeprecated() || getDeprecated() == fileOptions.getDeprecated()) && hasCcEnableArenas() == fileOptions.hasCcEnableArenas()) {
                                                            if ((!hasCcEnableArenas() || getCcEnableArenas() == fileOptions.getCcEnableArenas()) && hasObjcClassPrefix() == fileOptions.hasObjcClassPrefix()) {
                                                                if ((!hasObjcClassPrefix() || getObjcClassPrefix().equals(fileOptions.getObjcClassPrefix())) && hasCsharpNamespace() == fileOptions.hasCsharpNamespace()) {
                                                                    if ((!hasCsharpNamespace() || getCsharpNamespace().equals(fileOptions.getCsharpNamespace())) && hasSwiftPrefix() == fileOptions.hasSwiftPrefix()) {
                                                                        if ((!hasSwiftPrefix() || getSwiftPrefix().equals(fileOptions.getSwiftPrefix())) && hasPhpClassPrefix() == fileOptions.hasPhpClassPrefix()) {
                                                                            if ((!hasPhpClassPrefix() || getPhpClassPrefix().equals(fileOptions.getPhpClassPrefix())) && hasPhpNamespace() == fileOptions.hasPhpNamespace()) {
                                                                                if ((!hasPhpNamespace() || getPhpNamespace().equals(fileOptions.getPhpNamespace())) && hasPhpMetadataNamespace() == fileOptions.hasPhpMetadataNamespace()) {
                                                                                    if ((!hasPhpMetadataNamespace() || getPhpMetadataNamespace().equals(fileOptions.getPhpMetadataNamespace())) && hasRubyPackage() == fileOptions.hasRubyPackage()) {
                                                                                        return (!hasRubyPackage() || getRubyPackage().equals(fileOptions.getRubyPackage())) && getUninterpretedOptionList().equals(fileOptions.getUninterpretedOptionList()) && this.unknownFields.equals(fileOptions.unknownFields) && getExtensionFields().equals(fileOptions.getExtensionFields());
                                                                                    }
                                                                                    return false;
                                                                                }
                                                                                return false;
                                                                            }
                                                                            return false;
                                                                        }
                                                                        return false;
                                                                    }
                                                                    return false;
                                                                }
                                                                return false;
                                                            }
                                                            return false;
                                                        }
                                                        return false;
                                                    }
                                                    return false;
                                                }
                                                return false;
                                            }
                                            return false;
                                        }
                                        return false;
                                    }
                                    return false;
                                }
                                return false;
                            }
                            return false;
                        }
                        return false;
                    }
                    return false;
                }
                return false;
            }
            return false;
        }

        public boolean getCcEnableArenas() {
            return this.ccEnableArenas_;
        }

        public boolean getCcGenericServices() {
            return this.ccGenericServices_;
        }

        public String getCsharpNamespace() {
            Object obj = this.csharpNamespace_;
            if (obj instanceof String) {
                return (String) obj;
            }
            ByteString byteString = (ByteString) obj;
            String stringUtf8 = byteString.toStringUtf8();
            if (byteString.isValidUtf8()) {
                this.csharpNamespace_ = stringUtf8;
            }
            return stringUtf8;
        }

        public ByteString getCsharpNamespaceBytes() {
            Object obj = this.csharpNamespace_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.csharpNamespace_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        public boolean getDeprecated() {
            return this.deprecated_;
        }

        public String getGoPackage() {
            Object obj = this.goPackage_;
            if (obj instanceof String) {
                return (String) obj;
            }
            ByteString byteString = (ByteString) obj;
            String stringUtf8 = byteString.toStringUtf8();
            if (byteString.isValidUtf8()) {
                this.goPackage_ = stringUtf8;
            }
            return stringUtf8;
        }

        public ByteString getGoPackageBytes() {
            Object obj = this.goPackage_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.goPackage_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Deprecated
        public boolean getJavaGenerateEqualsAndHash() {
            return this.javaGenerateEqualsAndHash_;
        }

        public boolean getJavaGenericServices() {
            return this.javaGenericServices_;
        }

        public boolean getJavaMultipleFiles() {
            return this.javaMultipleFiles_;
        }

        public String getJavaOuterClassname() {
            Object obj = this.javaOuterClassname_;
            if (obj instanceof String) {
                return (String) obj;
            }
            ByteString byteString = (ByteString) obj;
            String stringUtf8 = byteString.toStringUtf8();
            if (byteString.isValidUtf8()) {
                this.javaOuterClassname_ = stringUtf8;
            }
            return stringUtf8;
        }

        public ByteString getJavaOuterClassnameBytes() {
            Object obj = this.javaOuterClassname_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.javaOuterClassname_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        public String getJavaPackage() {
            Object obj = this.javaPackage_;
            if (obj instanceof String) {
                return (String) obj;
            }
            ByteString byteString = (ByteString) obj;
            String stringUtf8 = byteString.toStringUtf8();
            if (byteString.isValidUtf8()) {
                this.javaPackage_ = stringUtf8;
            }
            return stringUtf8;
        }

        public ByteString getJavaPackageBytes() {
            Object obj = this.javaPackage_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.javaPackage_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        public boolean getJavaStringCheckUtf8() {
            return this.javaStringCheckUtf8_;
        }

        public String getObjcClassPrefix() {
            Object obj = this.objcClassPrefix_;
            if (obj instanceof String) {
                return (String) obj;
            }
            ByteString byteString = (ByteString) obj;
            String stringUtf8 = byteString.toStringUtf8();
            if (byteString.isValidUtf8()) {
                this.objcClassPrefix_ = stringUtf8;
            }
            return stringUtf8;
        }

        public ByteString getObjcClassPrefixBytes() {
            Object obj = this.objcClassPrefix_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.objcClassPrefix_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        public OptimizeMode getOptimizeFor() {
            OptimizeMode valueOf = OptimizeMode.valueOf(this.optimizeFor_);
            return valueOf == null ? OptimizeMode.SPEED : valueOf;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<FileOptions> getParserForType() {
            return PARSER;
        }

        public String getPhpClassPrefix() {
            Object obj = this.phpClassPrefix_;
            if (obj instanceof String) {
                return (String) obj;
            }
            ByteString byteString = (ByteString) obj;
            String stringUtf8 = byteString.toStringUtf8();
            if (byteString.isValidUtf8()) {
                this.phpClassPrefix_ = stringUtf8;
            }
            return stringUtf8;
        }

        public ByteString getPhpClassPrefixBytes() {
            Object obj = this.phpClassPrefix_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.phpClassPrefix_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        public boolean getPhpGenericServices() {
            return this.phpGenericServices_;
        }

        public String getPhpMetadataNamespace() {
            Object obj = this.phpMetadataNamespace_;
            if (obj instanceof String) {
                return (String) obj;
            }
            ByteString byteString = (ByteString) obj;
            String stringUtf8 = byteString.toStringUtf8();
            if (byteString.isValidUtf8()) {
                this.phpMetadataNamespace_ = stringUtf8;
            }
            return stringUtf8;
        }

        public ByteString getPhpMetadataNamespaceBytes() {
            Object obj = this.phpMetadataNamespace_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.phpMetadataNamespace_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        public String getPhpNamespace() {
            Object obj = this.phpNamespace_;
            if (obj instanceof String) {
                return (String) obj;
            }
            ByteString byteString = (ByteString) obj;
            String stringUtf8 = byteString.toStringUtf8();
            if (byteString.isValidUtf8()) {
                this.phpNamespace_ = stringUtf8;
            }
            return stringUtf8;
        }

        public ByteString getPhpNamespaceBytes() {
            Object obj = this.phpNamespace_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.phpNamespace_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        public boolean getPyGenericServices() {
            return this.pyGenericServices_;
        }

        public String getRubyPackage() {
            Object obj = this.rubyPackage_;
            if (obj instanceof String) {
                return (String) obj;
            }
            ByteString byteString = (ByteString) obj;
            String stringUtf8 = byteString.toStringUtf8();
            if (byteString.isValidUtf8()) {
                this.rubyPackage_ = stringUtf8;
            }
            return stringUtf8;
        }

        public ByteString getRubyPackageBytes() {
            Object obj = this.rubyPackage_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.rubyPackage_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = (this.bitField0_ & 1) != 0 ? GeneratedMessageV3.computeStringSize(1, this.javaPackage_) + 0 : 0;
            if ((this.bitField0_ & 2) != 0) {
                computeStringSize += GeneratedMessageV3.computeStringSize(8, this.javaOuterClassname_);
            }
            if ((this.bitField0_ & 32) != 0) {
                computeStringSize += CodedOutputStream.l(9, this.optimizeFor_);
            }
            if ((this.bitField0_ & 4) != 0) {
                computeStringSize += CodedOutputStream.e(10, this.javaMultipleFiles_);
            }
            if ((this.bitField0_ & 64) != 0) {
                computeStringSize += GeneratedMessageV3.computeStringSize(11, this.goPackage_);
            }
            if ((this.bitField0_ & 128) != 0) {
                computeStringSize += CodedOutputStream.e(16, this.ccGenericServices_);
            }
            if ((this.bitField0_ & 256) != 0) {
                computeStringSize += CodedOutputStream.e(17, this.javaGenericServices_);
            }
            if ((this.bitField0_ & RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN) != 0) {
                computeStringSize += CodedOutputStream.e(18, this.pyGenericServices_);
            }
            if ((this.bitField0_ & 8) != 0) {
                computeStringSize += CodedOutputStream.e(20, this.javaGenerateEqualsAndHash_);
            }
            if ((this.bitField0_ & 2048) != 0) {
                computeStringSize += CodedOutputStream.e(23, this.deprecated_);
            }
            if ((this.bitField0_ & 16) != 0) {
                computeStringSize += CodedOutputStream.e(27, this.javaStringCheckUtf8_);
            }
            if ((this.bitField0_ & 4096) != 0) {
                computeStringSize += CodedOutputStream.e(31, this.ccEnableArenas_);
            }
            if ((this.bitField0_ & 8192) != 0) {
                computeStringSize += GeneratedMessageV3.computeStringSize(36, this.objcClassPrefix_);
            }
            if ((this.bitField0_ & Http2.INITIAL_MAX_FRAME_SIZE) != 0) {
                computeStringSize += GeneratedMessageV3.computeStringSize(37, this.csharpNamespace_);
            }
            if ((this.bitField0_ & 32768) != 0) {
                computeStringSize += GeneratedMessageV3.computeStringSize(39, this.swiftPrefix_);
            }
            if ((this.bitField0_ & 65536) != 0) {
                computeStringSize += GeneratedMessageV3.computeStringSize(40, this.phpClassPrefix_);
            }
            if ((this.bitField0_ & 131072) != 0) {
                computeStringSize += GeneratedMessageV3.computeStringSize(41, this.phpNamespace_);
            }
            if ((this.bitField0_ & RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE) != 0) {
                computeStringSize += CodedOutputStream.e(42, this.phpGenericServices_);
            }
            if ((this.bitField0_ & 262144) != 0) {
                computeStringSize += GeneratedMessageV3.computeStringSize(44, this.phpMetadataNamespace_);
            }
            if ((this.bitField0_ & 524288) != 0) {
                computeStringSize += GeneratedMessageV3.computeStringSize(45, this.rubyPackage_);
            }
            for (int i2 = 0; i2 < this.uninterpretedOption_.size(); i2++) {
                computeStringSize += CodedOutputStream.G(999, this.uninterpretedOption_.get(i2));
            }
            int extensionsSerializedSize = computeStringSize + extensionsSerializedSize() + this.unknownFields.getSerializedSize();
            this.memoizedSize = extensionsSerializedSize;
            return extensionsSerializedSize;
        }

        public String getSwiftPrefix() {
            Object obj = this.swiftPrefix_;
            if (obj instanceof String) {
                return (String) obj;
            }
            ByteString byteString = (ByteString) obj;
            String stringUtf8 = byteString.toStringUtf8();
            if (byteString.isValidUtf8()) {
                this.swiftPrefix_ = stringUtf8;
            }
            return stringUtf8;
        }

        public ByteString getSwiftPrefixBytes() {
            Object obj = this.swiftPrefix_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.swiftPrefix_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        public UninterpretedOption getUninterpretedOption(int i) {
            return this.uninterpretedOption_.get(i);
        }

        public int getUninterpretedOptionCount() {
            return this.uninterpretedOption_.size();
        }

        public List<UninterpretedOption> getUninterpretedOptionList() {
            return this.uninterpretedOption_;
        }

        public s getUninterpretedOptionOrBuilder(int i) {
            return this.uninterpretedOption_.get(i);
        }

        public List<? extends s> getUninterpretedOptionOrBuilderList() {
            return this.uninterpretedOption_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        public boolean hasCcEnableArenas() {
            return (this.bitField0_ & 4096) != 0;
        }

        public boolean hasCcGenericServices() {
            return (this.bitField0_ & 128) != 0;
        }

        public boolean hasCsharpNamespace() {
            return (this.bitField0_ & Http2.INITIAL_MAX_FRAME_SIZE) != 0;
        }

        public boolean hasDeprecated() {
            return (this.bitField0_ & 2048) != 0;
        }

        public boolean hasGoPackage() {
            return (this.bitField0_ & 64) != 0;
        }

        @Deprecated
        public boolean hasJavaGenerateEqualsAndHash() {
            return (this.bitField0_ & 8) != 0;
        }

        public boolean hasJavaGenericServices() {
            return (this.bitField0_ & 256) != 0;
        }

        public boolean hasJavaMultipleFiles() {
            return (this.bitField0_ & 4) != 0;
        }

        public boolean hasJavaOuterClassname() {
            return (this.bitField0_ & 2) != 0;
        }

        public boolean hasJavaPackage() {
            return (this.bitField0_ & 1) != 0;
        }

        public boolean hasJavaStringCheckUtf8() {
            return (this.bitField0_ & 16) != 0;
        }

        public boolean hasObjcClassPrefix() {
            return (this.bitField0_ & 8192) != 0;
        }

        public boolean hasOptimizeFor() {
            return (this.bitField0_ & 32) != 0;
        }

        public boolean hasPhpClassPrefix() {
            return (this.bitField0_ & 65536) != 0;
        }

        public boolean hasPhpGenericServices() {
            return (this.bitField0_ & RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE) != 0;
        }

        public boolean hasPhpMetadataNamespace() {
            return (this.bitField0_ & 262144) != 0;
        }

        public boolean hasPhpNamespace() {
            return (this.bitField0_ & 131072) != 0;
        }

        public boolean hasPyGenericServices() {
            return (this.bitField0_ & RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN) != 0;
        }

        public boolean hasRubyPackage() {
            return (this.bitField0_ & 524288) != 0;
        }

        public boolean hasSwiftPrefix() {
            return (this.bitField0_ & 32768) != 0;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = 779 + getDescriptor().hashCode();
            if (hasJavaPackage()) {
                hashCode = (((hashCode * 37) + 1) * 53) + getJavaPackage().hashCode();
            }
            if (hasJavaOuterClassname()) {
                hashCode = (((hashCode * 37) + 8) * 53) + getJavaOuterClassname().hashCode();
            }
            if (hasJavaMultipleFiles()) {
                hashCode = (((hashCode * 37) + 10) * 53) + a0.c(getJavaMultipleFiles());
            }
            if (hasJavaGenerateEqualsAndHash()) {
                hashCode = (((hashCode * 37) + 20) * 53) + a0.c(getJavaGenerateEqualsAndHash());
            }
            if (hasJavaStringCheckUtf8()) {
                hashCode = (((hashCode * 37) + 27) * 53) + a0.c(getJavaStringCheckUtf8());
            }
            if (hasOptimizeFor()) {
                hashCode = (((hashCode * 37) + 9) * 53) + this.optimizeFor_;
            }
            if (hasGoPackage()) {
                hashCode = (((hashCode * 37) + 11) * 53) + getGoPackage().hashCode();
            }
            if (hasCcGenericServices()) {
                hashCode = (((hashCode * 37) + 16) * 53) + a0.c(getCcGenericServices());
            }
            if (hasJavaGenericServices()) {
                hashCode = (((hashCode * 37) + 17) * 53) + a0.c(getJavaGenericServices());
            }
            if (hasPyGenericServices()) {
                hashCode = (((hashCode * 37) + 18) * 53) + a0.c(getPyGenericServices());
            }
            if (hasPhpGenericServices()) {
                hashCode = (((hashCode * 37) + 42) * 53) + a0.c(getPhpGenericServices());
            }
            if (hasDeprecated()) {
                hashCode = (((hashCode * 37) + 23) * 53) + a0.c(getDeprecated());
            }
            if (hasCcEnableArenas()) {
                hashCode = (((hashCode * 37) + 31) * 53) + a0.c(getCcEnableArenas());
            }
            if (hasObjcClassPrefix()) {
                hashCode = (((hashCode * 37) + 36) * 53) + getObjcClassPrefix().hashCode();
            }
            if (hasCsharpNamespace()) {
                hashCode = (((hashCode * 37) + 37) * 53) + getCsharpNamespace().hashCode();
            }
            if (hasSwiftPrefix()) {
                hashCode = (((hashCode * 37) + 39) * 53) + getSwiftPrefix().hashCode();
            }
            if (hasPhpClassPrefix()) {
                hashCode = (((hashCode * 37) + 40) * 53) + getPhpClassPrefix().hashCode();
            }
            if (hasPhpNamespace()) {
                hashCode = (((hashCode * 37) + 41) * 53) + getPhpNamespace().hashCode();
            }
            if (hasPhpMetadataNamespace()) {
                hashCode = (((hashCode * 37) + 44) * 53) + getPhpMetadataNamespace().hashCode();
            }
            if (hasRubyPackage()) {
                hashCode = (((hashCode * 37) + 45) * 53) + getRubyPackage().hashCode();
            }
            if (getUninterpretedOptionCount() > 0) {
                hashCode = (((hashCode * 37) + 999) * 53) + getUninterpretedOptionList().hashCode();
            }
            int hashFields = (com.google.protobuf.a.hashFields(hashCode, getExtensionFields()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashFields;
            return hashFields;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return DescriptorProtos.A.d(FileOptions.class, b.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3.ExtendableMessage, com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b2 = this.memoizedIsInitialized;
            if (b2 == 1) {
                return true;
            }
            if (b2 == 0) {
                return false;
            }
            for (int i = 0; i < getUninterpretedOptionCount(); i++) {
                if (!getUninterpretedOption(i).isInitialized()) {
                    this.memoizedIsInitialized = (byte) 0;
                    return false;
                }
            }
            if (!extensionsAreInitialized()) {
                this.memoizedIsInitialized = (byte) 0;
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new FileOptions();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            GeneratedMessageV3.ExtendableMessage<MessageType>.a newExtensionWriter = newExtensionWriter();
            if ((this.bitField0_ & 1) != 0) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.javaPackage_);
            }
            if ((this.bitField0_ & 2) != 0) {
                GeneratedMessageV3.writeString(codedOutputStream, 8, this.javaOuterClassname_);
            }
            if ((this.bitField0_ & 32) != 0) {
                codedOutputStream.u0(9, this.optimizeFor_);
            }
            if ((this.bitField0_ & 4) != 0) {
                codedOutputStream.m0(10, this.javaMultipleFiles_);
            }
            if ((this.bitField0_ & 64) != 0) {
                GeneratedMessageV3.writeString(codedOutputStream, 11, this.goPackage_);
            }
            if ((this.bitField0_ & 128) != 0) {
                codedOutputStream.m0(16, this.ccGenericServices_);
            }
            if ((this.bitField0_ & 256) != 0) {
                codedOutputStream.m0(17, this.javaGenericServices_);
            }
            if ((this.bitField0_ & RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN) != 0) {
                codedOutputStream.m0(18, this.pyGenericServices_);
            }
            if ((this.bitField0_ & 8) != 0) {
                codedOutputStream.m0(20, this.javaGenerateEqualsAndHash_);
            }
            if ((this.bitField0_ & 2048) != 0) {
                codedOutputStream.m0(23, this.deprecated_);
            }
            if ((this.bitField0_ & 16) != 0) {
                codedOutputStream.m0(27, this.javaStringCheckUtf8_);
            }
            if ((this.bitField0_ & 4096) != 0) {
                codedOutputStream.m0(31, this.ccEnableArenas_);
            }
            if ((this.bitField0_ & 8192) != 0) {
                GeneratedMessageV3.writeString(codedOutputStream, 36, this.objcClassPrefix_);
            }
            if ((this.bitField0_ & Http2.INITIAL_MAX_FRAME_SIZE) != 0) {
                GeneratedMessageV3.writeString(codedOutputStream, 37, this.csharpNamespace_);
            }
            if ((this.bitField0_ & 32768) != 0) {
                GeneratedMessageV3.writeString(codedOutputStream, 39, this.swiftPrefix_);
            }
            if ((this.bitField0_ & 65536) != 0) {
                GeneratedMessageV3.writeString(codedOutputStream, 40, this.phpClassPrefix_);
            }
            if ((this.bitField0_ & 131072) != 0) {
                GeneratedMessageV3.writeString(codedOutputStream, 41, this.phpNamespace_);
            }
            if ((this.bitField0_ & RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE) != 0) {
                codedOutputStream.m0(42, this.phpGenericServices_);
            }
            if ((this.bitField0_ & 262144) != 0) {
                GeneratedMessageV3.writeString(codedOutputStream, 44, this.phpMetadataNamespace_);
            }
            if ((this.bitField0_ & 524288) != 0) {
                GeneratedMessageV3.writeString(codedOutputStream, 45, this.rubyPackage_);
            }
            for (int i = 0; i < this.uninterpretedOption_.size(); i++) {
                codedOutputStream.K0(999, this.uninterpretedOption_.get(i));
            }
            newExtensionWriter.a(536870912, codedOutputStream);
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes2.dex */
        public static final class b extends GeneratedMessageV3.d<FileOptions, b> implements j {
            public List<UninterpretedOption> A0;
            public x0<UninterpretedOption, UninterpretedOption.b, s> B0;
            public int f0;
            public Object g0;
            public Object h0;
            public boolean i0;
            public boolean j0;
            public boolean k0;
            public int l0;
            public Object m0;
            public boolean n0;
            public boolean o0;
            public boolean p0;
            public boolean q0;
            public boolean r0;
            public boolean s0;
            public Object t0;
            public Object u0;
            public Object v0;
            public Object w0;
            public Object x0;
            public Object y0;
            public Object z0;

            @Override // defpackage.d82, com.google.protobuf.o0
            /* renamed from: A */
            public FileOptions getDefaultInstanceForType() {
                return FileOptions.getDefaultInstance();
            }

            public UninterpretedOption B(int i) {
                x0<UninterpretedOption, UninterpretedOption.b, s> x0Var = this.B0;
                if (x0Var == null) {
                    return this.A0.get(i);
                }
                return x0Var.o(i);
            }

            public int C() {
                x0<UninterpretedOption, UninterpretedOption.b, s> x0Var = this.B0;
                if (x0Var == null) {
                    return this.A0.size();
                }
                return x0Var.n();
            }

            public final x0<UninterpretedOption, UninterpretedOption.b, s> D() {
                if (this.B0 == null) {
                    this.B0 = new x0<>(this.A0, (this.f0 & 1048576) != 0, getParentForChildren(), isClean());
                    this.A0 = null;
                }
                return this.B0;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0021  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /* renamed from: E */
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public com.google.protobuf.DescriptorProtos.FileOptions.b mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0<com.google.protobuf.DescriptorProtos$FileOptions> r1 = com.google.protobuf.DescriptorProtos.FileOptions.PARSER     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    com.google.protobuf.DescriptorProtos$FileOptions r3 = (com.google.protobuf.DescriptorProtos.FileOptions) r3     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    if (r3 == 0) goto Le
                    r2.G(r3)
                Le:
                    return r2
                Lf:
                    r3 = move-exception
                    goto L1f
                L11:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> Lf
                    com.google.protobuf.DescriptorProtos$FileOptions r4 = (com.google.protobuf.DescriptorProtos.FileOptions) r4     // Catch: java.lang.Throwable -> Lf
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1d
                    throw r3     // Catch: java.lang.Throwable -> L1d
                L1d:
                    r3 = move-exception
                    r0 = r4
                L1f:
                    if (r0 == 0) goto L24
                    r2.G(r0)
                L24:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.DescriptorProtos.FileOptions.b.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):com.google.protobuf.DescriptorProtos$FileOptions$b");
            }

            public b G(FileOptions fileOptions) {
                if (fileOptions == FileOptions.getDefaultInstance()) {
                    return this;
                }
                if (fileOptions.hasJavaPackage()) {
                    this.f0 |= 1;
                    this.g0 = fileOptions.javaPackage_;
                    onChanged();
                }
                if (fileOptions.hasJavaOuterClassname()) {
                    this.f0 |= 2;
                    this.h0 = fileOptions.javaOuterClassname_;
                    onChanged();
                }
                if (fileOptions.hasJavaMultipleFiles()) {
                    P(fileOptions.getJavaMultipleFiles());
                }
                if (fileOptions.hasJavaGenerateEqualsAndHash()) {
                    N(fileOptions.getJavaGenerateEqualsAndHash());
                }
                if (fileOptions.hasJavaStringCheckUtf8()) {
                    Q(fileOptions.getJavaStringCheckUtf8());
                }
                if (fileOptions.hasOptimizeFor()) {
                    R(fileOptions.getOptimizeFor());
                }
                if (fileOptions.hasGoPackage()) {
                    this.f0 |= 64;
                    this.m0 = fileOptions.goPackage_;
                    onChanged();
                }
                if (fileOptions.hasCcGenericServices()) {
                    K(fileOptions.getCcGenericServices());
                }
                if (fileOptions.hasJavaGenericServices()) {
                    O(fileOptions.getJavaGenericServices());
                }
                if (fileOptions.hasPyGenericServices()) {
                    T(fileOptions.getPyGenericServices());
                }
                if (fileOptions.hasPhpGenericServices()) {
                    S(fileOptions.getPhpGenericServices());
                }
                if (fileOptions.hasDeprecated()) {
                    L(fileOptions.getDeprecated());
                }
                if (fileOptions.hasCcEnableArenas()) {
                    J(fileOptions.getCcEnableArenas());
                }
                if (fileOptions.hasObjcClassPrefix()) {
                    this.f0 |= 8192;
                    this.t0 = fileOptions.objcClassPrefix_;
                    onChanged();
                }
                if (fileOptions.hasCsharpNamespace()) {
                    this.f0 |= Http2.INITIAL_MAX_FRAME_SIZE;
                    this.u0 = fileOptions.csharpNamespace_;
                    onChanged();
                }
                if (fileOptions.hasSwiftPrefix()) {
                    this.f0 |= 32768;
                    this.v0 = fileOptions.swiftPrefix_;
                    onChanged();
                }
                if (fileOptions.hasPhpClassPrefix()) {
                    this.f0 |= 65536;
                    this.w0 = fileOptions.phpClassPrefix_;
                    onChanged();
                }
                if (fileOptions.hasPhpNamespace()) {
                    this.f0 |= 131072;
                    this.x0 = fileOptions.phpNamespace_;
                    onChanged();
                }
                if (fileOptions.hasPhpMetadataNamespace()) {
                    this.f0 |= 262144;
                    this.y0 = fileOptions.phpMetadataNamespace_;
                    onChanged();
                }
                if (fileOptions.hasRubyPackage()) {
                    this.f0 |= 524288;
                    this.z0 = fileOptions.rubyPackage_;
                    onChanged();
                }
                if (this.B0 == null) {
                    if (!fileOptions.uninterpretedOption_.isEmpty()) {
                        if (this.A0.isEmpty()) {
                            this.A0 = fileOptions.uninterpretedOption_;
                            this.f0 &= -1048577;
                        } else {
                            y();
                            this.A0.addAll(fileOptions.uninterpretedOption_);
                        }
                        onChanged();
                    }
                } else if (!fileOptions.uninterpretedOption_.isEmpty()) {
                    if (!this.B0.u()) {
                        this.B0.b(fileOptions.uninterpretedOption_);
                    } else {
                        this.B0.i();
                        this.B0 = null;
                        this.A0 = fileOptions.uninterpretedOption_;
                        this.f0 = (-1048577) & this.f0;
                        this.B0 = GeneratedMessageV3.alwaysUseFieldBuilders ? D() : null;
                    }
                }
                j(fileOptions);
                mergeUnknownFields(fileOptions.unknownFields);
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            /* renamed from: H */
            public b mergeFrom(l0 l0Var) {
                if (l0Var instanceof FileOptions) {
                    return G((FileOptions) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: I */
            public final b mergeUnknownFields(g1 g1Var) {
                return (b) super.mergeUnknownFields(g1Var);
            }

            public b J(boolean z) {
                this.f0 |= 4096;
                this.s0 = z;
                onChanged();
                return this;
            }

            public b K(boolean z) {
                this.f0 |= 128;
                this.n0 = z;
                onChanged();
                return this;
            }

            public b L(boolean z) {
                this.f0 |= 2048;
                this.r0 = z;
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d, com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: M */
            public b setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (b) super.setField(fieldDescriptor, obj);
            }

            @Deprecated
            public b N(boolean z) {
                this.f0 |= 8;
                this.j0 = z;
                onChanged();
                return this;
            }

            public b O(boolean z) {
                this.f0 |= 256;
                this.o0 = z;
                onChanged();
                return this;
            }

            public b P(boolean z) {
                this.f0 |= 4;
                this.i0 = z;
                onChanged();
                return this;
            }

            public b Q(boolean z) {
                this.f0 |= 16;
                this.k0 = z;
                onChanged();
                return this;
            }

            public b R(OptimizeMode optimizeMode) {
                Objects.requireNonNull(optimizeMode);
                this.f0 |= 32;
                this.l0 = optimizeMode.getNumber();
                onChanged();
                return this;
            }

            public b S(boolean z) {
                this.f0 |= RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE;
                this.q0 = z;
                onChanged();
                return this;
            }

            public b T(boolean z) {
                this.f0 |= RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN;
                this.p0 = z;
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d, com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: U */
            public b setRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (b) super.setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: V */
            public final b setUnknownFields(g1 g1Var) {
                return (b) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return DescriptorProtos.z;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return DescriptorProtos.A.d(FileOptions.class, b.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d, com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                for (int i = 0; i < C(); i++) {
                    if (!B(i).isInitialized()) {
                        return false;
                    }
                }
                return h();
            }

            public final void maybeForceBuilderInitialization() {
                if (GeneratedMessageV3.alwaysUseFieldBuilders) {
                    D();
                }
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d
            /* renamed from: p */
            public b b(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (b) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            /* renamed from: r */
            public FileOptions build() {
                FileOptions buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            /* renamed from: s */
            public FileOptions buildPartial() {
                FileOptions fileOptions = new FileOptions(this);
                int i = this.f0;
                int i2 = (i & 1) != 0 ? 1 : 0;
                fileOptions.javaPackage_ = this.g0;
                if ((i & 2) != 0) {
                    i2 |= 2;
                }
                fileOptions.javaOuterClassname_ = this.h0;
                if ((i & 4) != 0) {
                    fileOptions.javaMultipleFiles_ = this.i0;
                    i2 |= 4;
                }
                if ((i & 8) != 0) {
                    fileOptions.javaGenerateEqualsAndHash_ = this.j0;
                    i2 |= 8;
                }
                if ((i & 16) != 0) {
                    fileOptions.javaStringCheckUtf8_ = this.k0;
                    i2 |= 16;
                }
                if ((i & 32) != 0) {
                    i2 |= 32;
                }
                fileOptions.optimizeFor_ = this.l0;
                if ((i & 64) != 0) {
                    i2 |= 64;
                }
                fileOptions.goPackage_ = this.m0;
                if ((i & 128) != 0) {
                    fileOptions.ccGenericServices_ = this.n0;
                    i2 |= 128;
                }
                if ((i & 256) != 0) {
                    fileOptions.javaGenericServices_ = this.o0;
                    i2 |= 256;
                }
                if ((i & RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN) != 0) {
                    fileOptions.pyGenericServices_ = this.p0;
                    i2 |= RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN;
                }
                if ((i & RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE) != 0) {
                    fileOptions.phpGenericServices_ = this.q0;
                    i2 |= RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE;
                }
                if ((i & 2048) != 0) {
                    fileOptions.deprecated_ = this.r0;
                    i2 |= 2048;
                }
                if ((i & 4096) != 0) {
                    i2 |= 4096;
                }
                fileOptions.ccEnableArenas_ = this.s0;
                if ((i & 8192) != 0) {
                    i2 |= 8192;
                }
                fileOptions.objcClassPrefix_ = this.t0;
                if ((i & Http2.INITIAL_MAX_FRAME_SIZE) != 0) {
                    i2 |= Http2.INITIAL_MAX_FRAME_SIZE;
                }
                fileOptions.csharpNamespace_ = this.u0;
                if ((i & 32768) != 0) {
                    i2 |= 32768;
                }
                fileOptions.swiftPrefix_ = this.v0;
                if ((i & 65536) != 0) {
                    i2 |= 65536;
                }
                fileOptions.phpClassPrefix_ = this.w0;
                if ((i & 131072) != 0) {
                    i2 |= 131072;
                }
                fileOptions.phpNamespace_ = this.x0;
                if ((i & 262144) != 0) {
                    i2 |= 262144;
                }
                fileOptions.phpMetadataNamespace_ = this.y0;
                if ((i & 524288) != 0) {
                    i2 |= 524288;
                }
                fileOptions.rubyPackage_ = this.z0;
                x0<UninterpretedOption, UninterpretedOption.b, s> x0Var = this.B0;
                if (x0Var != null) {
                    fileOptions.uninterpretedOption_ = x0Var.g();
                } else {
                    if ((this.f0 & 1048576) != 0) {
                        this.A0 = Collections.unmodifiableList(this.A0);
                        this.f0 &= -1048577;
                    }
                    fileOptions.uninterpretedOption_ = this.A0;
                }
                fileOptions.bitField0_ = i2;
                onBuilt();
                return fileOptions;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d
            /* renamed from: t */
            public b e() {
                super.clear();
                this.g0 = "";
                int i = this.f0 & (-2);
                this.f0 = i;
                this.h0 = "";
                int i2 = i & (-3);
                this.f0 = i2;
                this.i0 = false;
                int i3 = i2 & (-5);
                this.f0 = i3;
                this.j0 = false;
                int i4 = i3 & (-9);
                this.f0 = i4;
                this.k0 = false;
                int i5 = i4 & (-17);
                this.f0 = i5;
                this.l0 = 1;
                int i6 = i5 & (-33);
                this.f0 = i6;
                this.m0 = "";
                int i7 = i6 & (-65);
                this.f0 = i7;
                this.n0 = false;
                int i8 = i7 & (-129);
                this.f0 = i8;
                this.o0 = false;
                int i9 = i8 & (-257);
                this.f0 = i9;
                this.p0 = false;
                int i10 = i9 & (-513);
                this.f0 = i10;
                this.q0 = false;
                int i11 = i10 & (-1025);
                this.f0 = i11;
                this.r0 = false;
                int i12 = i11 & (-2049);
                this.f0 = i12;
                this.s0 = true;
                int i13 = i12 & (-4097);
                this.f0 = i13;
                this.t0 = "";
                int i14 = i13 & (-8193);
                this.f0 = i14;
                this.u0 = "";
                int i15 = i14 & (-16385);
                this.f0 = i15;
                this.v0 = "";
                int i16 = i15 & (-32769);
                this.f0 = i16;
                this.w0 = "";
                int i17 = i16 & (-65537);
                this.f0 = i17;
                this.x0 = "";
                int i18 = i17 & (-131073);
                this.f0 = i18;
                this.y0 = "";
                int i19 = i18 & (-262145);
                this.f0 = i19;
                this.z0 = "";
                this.f0 = (-524289) & i19;
                x0<UninterpretedOption, UninterpretedOption.b, s> x0Var = this.B0;
                if (x0Var == null) {
                    this.A0 = Collections.emptyList();
                    this.f0 &= -1048577;
                } else {
                    x0Var.h();
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d
            /* renamed from: u */
            public b f(Descriptors.FieldDescriptor fieldDescriptor) {
                return (b) super.clearField(fieldDescriptor);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: v */
            public b clearOneof(Descriptors.g gVar) {
                return (b) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            /* renamed from: x */
            public b clone() {
                return (b) super.clone();
            }

            public final void y() {
                if ((this.f0 & 1048576) == 0) {
                    this.A0 = new ArrayList(this.A0);
                    this.f0 |= 1048576;
                }
            }

            public b() {
                this.g0 = "";
                this.h0 = "";
                this.l0 = 1;
                this.m0 = "";
                this.s0 = true;
                this.t0 = "";
                this.u0 = "";
                this.v0 = "";
                this.w0 = "";
                this.x0 = "";
                this.y0 = "";
                this.z0 = "";
                this.A0 = Collections.emptyList();
                maybeForceBuilderInitialization();
            }

            public b(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.g0 = "";
                this.h0 = "";
                this.l0 = 1;
                this.m0 = "";
                this.s0 = true;
                this.t0 = "";
                this.u0 = "";
                this.v0 = "";
                this.w0 = "";
                this.x0 = "";
                this.y0 = "";
                this.z0 = "";
                this.A0 = Collections.emptyList();
                maybeForceBuilderInitialization();
            }
        }

        public static b newBuilder(FileOptions fileOptions) {
            return a.toBuilder().G(fileOptions);
        }

        public static FileOptions parseFrom(ByteBuffer byteBuffer, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        public FileOptions(GeneratedMessageV3.d<FileOptions, ?> dVar) {
            super(dVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static FileOptions parseDelimitedFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
            return (FileOptions) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static FileOptions parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3.ExtendableMessage, com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public FileOptions getDefaultInstanceForType() {
            return a;
        }

        @Override // com.google.protobuf.GeneratedMessageV3.ExtendableMessage, com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public b toBuilder() {
            return this == a ? new b() : new b().G(this);
        }

        public static FileOptions parseFrom(ByteString byteString, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3.ExtendableMessage, com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public b newBuilderForType() {
            return newBuilder();
        }

        public FileOptions() {
            this.memoizedIsInitialized = (byte) -1;
            this.javaPackage_ = "";
            this.javaOuterClassname_ = "";
            this.optimizeFor_ = 1;
            this.goPackage_ = "";
            this.ccEnableArenas_ = true;
            this.objcClassPrefix_ = "";
            this.csharpNamespace_ = "";
            this.swiftPrefix_ = "";
            this.phpClassPrefix_ = "";
            this.phpNamespace_ = "";
            this.phpMetadataNamespace_ = "";
            this.rubyPackage_ = "";
            this.uninterpretedOption_ = Collections.emptyList();
        }

        public static FileOptions parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public b newBuilderForType(GeneratedMessageV3.c cVar) {
            return new b(cVar);
        }

        public static FileOptions parseFrom(byte[] bArr, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static FileOptions parseFrom(InputStream inputStream) throws IOException {
            return (FileOptions) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static FileOptions parseFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
            return (FileOptions) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static FileOptions parseFrom(com.google.protobuf.j jVar) throws IOException {
            return (FileOptions) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static FileOptions parseFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws IOException {
            return (FileOptions) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }

        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r3v0 */
        /* JADX WARN: Type inference failed for: r3v1 */
        /* JADX WARN: Type inference failed for: r3v2, types: [boolean] */
        public FileOptions(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            int i = 0;
            while (true) {
                ?? r3 = 1048576;
                if (z) {
                    return;
                }
                try {
                    try {
                        try {
                            int J = jVar.J();
                            switch (J) {
                                case 0:
                                    break;
                                case 10:
                                    ByteString q = jVar.q();
                                    this.bitField0_ = 1 | this.bitField0_;
                                    this.javaPackage_ = q;
                                    continue;
                                case 66:
                                    ByteString q2 = jVar.q();
                                    this.bitField0_ |= 2;
                                    this.javaOuterClassname_ = q2;
                                    continue;
                                case 72:
                                    int s = jVar.s();
                                    if (OptimizeMode.valueOf(s) == null) {
                                        g.x(9, s);
                                    } else {
                                        this.bitField0_ |= 32;
                                        this.optimizeFor_ = s;
                                        continue;
                                    }
                                case 80:
                                    this.bitField0_ |= 4;
                                    this.javaMultipleFiles_ = jVar.p();
                                    continue;
                                case 90:
                                    ByteString q3 = jVar.q();
                                    this.bitField0_ |= 64;
                                    this.goPackage_ = q3;
                                    continue;
                                case 128:
                                    this.bitField0_ |= 128;
                                    this.ccGenericServices_ = jVar.p();
                                    continue;
                                case 136:
                                    this.bitField0_ |= 256;
                                    this.javaGenericServices_ = jVar.p();
                                    continue;
                                case 144:
                                    this.bitField0_ |= RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN;
                                    this.pyGenericServices_ = jVar.p();
                                    continue;
                                case 160:
                                    this.bitField0_ |= 8;
                                    this.javaGenerateEqualsAndHash_ = jVar.p();
                                    continue;
                                case 184:
                                    this.bitField0_ |= 2048;
                                    this.deprecated_ = jVar.p();
                                    continue;
                                case 216:
                                    this.bitField0_ |= 16;
                                    this.javaStringCheckUtf8_ = jVar.p();
                                    continue;
                                case 248:
                                    this.bitField0_ |= 4096;
                                    this.ccEnableArenas_ = jVar.p();
                                    continue;
                                case 290:
                                    ByteString q4 = jVar.q();
                                    this.bitField0_ |= 8192;
                                    this.objcClassPrefix_ = q4;
                                    continue;
                                case 298:
                                    ByteString q5 = jVar.q();
                                    this.bitField0_ |= Http2.INITIAL_MAX_FRAME_SIZE;
                                    this.csharpNamespace_ = q5;
                                    continue;
                                case 314:
                                    ByteString q6 = jVar.q();
                                    this.bitField0_ |= 32768;
                                    this.swiftPrefix_ = q6;
                                    continue;
                                case 322:
                                    ByteString q7 = jVar.q();
                                    this.bitField0_ |= 65536;
                                    this.phpClassPrefix_ = q7;
                                    continue;
                                case 330:
                                    ByteString q8 = jVar.q();
                                    this.bitField0_ |= 131072;
                                    this.phpNamespace_ = q8;
                                    continue;
                                case 336:
                                    this.bitField0_ |= RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE;
                                    this.phpGenericServices_ = jVar.p();
                                    continue;
                                case 354:
                                    ByteString q9 = jVar.q();
                                    this.bitField0_ |= 262144;
                                    this.phpMetadataNamespace_ = q9;
                                    continue;
                                case 362:
                                    ByteString q10 = jVar.q();
                                    this.bitField0_ |= 524288;
                                    this.rubyPackage_ = q10;
                                    continue;
                                case 7994:
                                    if ((i & 1048576) == 0) {
                                        this.uninterpretedOption_ = new ArrayList();
                                        i |= 1048576;
                                    }
                                    this.uninterpretedOption_.add(jVar.z(UninterpretedOption.PARSER, rVar));
                                    continue;
                                default:
                                    r3 = parseUnknownField(jVar, g, rVar, J);
                                    if (r3 == 0) {
                                        break;
                                    } else {
                                        continue;
                                    }
                            }
                            z = true;
                        } catch (IOException e) {
                            throw new InvalidProtocolBufferException(e).setUnfinishedMessage(this);
                        }
                    } catch (InvalidProtocolBufferException e2) {
                        throw e2.setUnfinishedMessage(this);
                    }
                } finally {
                    if ((i & r3) != 0) {
                        this.uninterpretedOption_ = Collections.unmodifiableList(this.uninterpretedOption_);
                    }
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }
    }

    /* loaded from: classes2.dex */
    public static final class MessageOptions extends GeneratedMessageV3.ExtendableMessage<MessageOptions> implements k {
        public static final int DEPRECATED_FIELD_NUMBER = 3;
        public static final int MAP_ENTRY_FIELD_NUMBER = 7;
        public static final int MESSAGE_SET_WIRE_FORMAT_FIELD_NUMBER = 1;
        public static final int NO_STANDARD_DESCRIPTOR_ACCESSOR_FIELD_NUMBER = 2;
        public static final int UNINTERPRETED_OPTION_FIELD_NUMBER = 999;
        private static final long serialVersionUID = 0;
        private int bitField0_;
        private boolean deprecated_;
        private boolean mapEntry_;
        private byte memoizedIsInitialized;
        private boolean messageSetWireFormat_;
        private boolean noStandardDescriptorAccessor_;
        private List<UninterpretedOption> uninterpretedOption_;
        public static final MessageOptions a = new MessageOptions();
        @Deprecated
        public static final t0<MessageOptions> PARSER = new a();

        /* loaded from: classes2.dex */
        public static class a extends com.google.protobuf.c<MessageOptions> {
            @Override // com.google.protobuf.t0
            /* renamed from: a */
            public MessageOptions parsePartialFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
                return new MessageOptions(jVar, rVar);
            }
        }

        public static MessageOptions getDefaultInstance() {
            return a;
        }

        public static final Descriptors.b getDescriptor() {
            return DescriptorProtos.B;
        }

        public static b newBuilder() {
            return a.toBuilder();
        }

        public static MessageOptions parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (MessageOptions) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static MessageOptions parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<MessageOptions> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof MessageOptions)) {
                return super.equals(obj);
            }
            MessageOptions messageOptions = (MessageOptions) obj;
            if (hasMessageSetWireFormat() != messageOptions.hasMessageSetWireFormat()) {
                return false;
            }
            if ((!hasMessageSetWireFormat() || getMessageSetWireFormat() == messageOptions.getMessageSetWireFormat()) && hasNoStandardDescriptorAccessor() == messageOptions.hasNoStandardDescriptorAccessor()) {
                if ((!hasNoStandardDescriptorAccessor() || getNoStandardDescriptorAccessor() == messageOptions.getNoStandardDescriptorAccessor()) && hasDeprecated() == messageOptions.hasDeprecated()) {
                    if ((!hasDeprecated() || getDeprecated() == messageOptions.getDeprecated()) && hasMapEntry() == messageOptions.hasMapEntry()) {
                        return (!hasMapEntry() || getMapEntry() == messageOptions.getMapEntry()) && getUninterpretedOptionList().equals(messageOptions.getUninterpretedOptionList()) && this.unknownFields.equals(messageOptions.unknownFields) && getExtensionFields().equals(messageOptions.getExtensionFields());
                    }
                    return false;
                }
                return false;
            }
            return false;
        }

        public boolean getDeprecated() {
            return this.deprecated_;
        }

        public boolean getMapEntry() {
            return this.mapEntry_;
        }

        public boolean getMessageSetWireFormat() {
            return this.messageSetWireFormat_;
        }

        public boolean getNoStandardDescriptorAccessor() {
            return this.noStandardDescriptorAccessor_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<MessageOptions> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int e = (this.bitField0_ & 1) != 0 ? CodedOutputStream.e(1, this.messageSetWireFormat_) + 0 : 0;
            if ((this.bitField0_ & 2) != 0) {
                e += CodedOutputStream.e(2, this.noStandardDescriptorAccessor_);
            }
            if ((this.bitField0_ & 4) != 0) {
                e += CodedOutputStream.e(3, this.deprecated_);
            }
            if ((this.bitField0_ & 8) != 0) {
                e += CodedOutputStream.e(7, this.mapEntry_);
            }
            for (int i2 = 0; i2 < this.uninterpretedOption_.size(); i2++) {
                e += CodedOutputStream.G(999, this.uninterpretedOption_.get(i2));
            }
            int extensionsSerializedSize = e + extensionsSerializedSize() + this.unknownFields.getSerializedSize();
            this.memoizedSize = extensionsSerializedSize;
            return extensionsSerializedSize;
        }

        public UninterpretedOption getUninterpretedOption(int i) {
            return this.uninterpretedOption_.get(i);
        }

        public int getUninterpretedOptionCount() {
            return this.uninterpretedOption_.size();
        }

        public List<UninterpretedOption> getUninterpretedOptionList() {
            return this.uninterpretedOption_;
        }

        public s getUninterpretedOptionOrBuilder(int i) {
            return this.uninterpretedOption_.get(i);
        }

        public List<? extends s> getUninterpretedOptionOrBuilderList() {
            return this.uninterpretedOption_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        public boolean hasDeprecated() {
            return (this.bitField0_ & 4) != 0;
        }

        public boolean hasMapEntry() {
            return (this.bitField0_ & 8) != 0;
        }

        public boolean hasMessageSetWireFormat() {
            return (this.bitField0_ & 1) != 0;
        }

        public boolean hasNoStandardDescriptorAccessor() {
            return (this.bitField0_ & 2) != 0;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = 779 + getDescriptor().hashCode();
            if (hasMessageSetWireFormat()) {
                hashCode = (((hashCode * 37) + 1) * 53) + a0.c(getMessageSetWireFormat());
            }
            if (hasNoStandardDescriptorAccessor()) {
                hashCode = (((hashCode * 37) + 2) * 53) + a0.c(getNoStandardDescriptorAccessor());
            }
            if (hasDeprecated()) {
                hashCode = (((hashCode * 37) + 3) * 53) + a0.c(getDeprecated());
            }
            if (hasMapEntry()) {
                hashCode = (((hashCode * 37) + 7) * 53) + a0.c(getMapEntry());
            }
            if (getUninterpretedOptionCount() > 0) {
                hashCode = (((hashCode * 37) + 999) * 53) + getUninterpretedOptionList().hashCode();
            }
            int hashFields = (com.google.protobuf.a.hashFields(hashCode, getExtensionFields()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashFields;
            return hashFields;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return DescriptorProtos.C.d(MessageOptions.class, b.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3.ExtendableMessage, com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b2 = this.memoizedIsInitialized;
            if (b2 == 1) {
                return true;
            }
            if (b2 == 0) {
                return false;
            }
            for (int i = 0; i < getUninterpretedOptionCount(); i++) {
                if (!getUninterpretedOption(i).isInitialized()) {
                    this.memoizedIsInitialized = (byte) 0;
                    return false;
                }
            }
            if (!extensionsAreInitialized()) {
                this.memoizedIsInitialized = (byte) 0;
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new MessageOptions();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            GeneratedMessageV3.ExtendableMessage<MessageType>.a newExtensionWriter = newExtensionWriter();
            if ((this.bitField0_ & 1) != 0) {
                codedOutputStream.m0(1, this.messageSetWireFormat_);
            }
            if ((this.bitField0_ & 2) != 0) {
                codedOutputStream.m0(2, this.noStandardDescriptorAccessor_);
            }
            if ((this.bitField0_ & 4) != 0) {
                codedOutputStream.m0(3, this.deprecated_);
            }
            if ((this.bitField0_ & 8) != 0) {
                codedOutputStream.m0(7, this.mapEntry_);
            }
            for (int i = 0; i < this.uninterpretedOption_.size(); i++) {
                codedOutputStream.K0(999, this.uninterpretedOption_.get(i));
            }
            newExtensionWriter.a(536870912, codedOutputStream);
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes2.dex */
        public static final class b extends GeneratedMessageV3.d<MessageOptions, b> implements k {
            public int f0;
            public boolean g0;
            public boolean h0;
            public boolean i0;
            public boolean j0;
            public List<UninterpretedOption> k0;
            public x0<UninterpretedOption, UninterpretedOption.b, s> l0;

            @Override // defpackage.d82, com.google.protobuf.o0
            /* renamed from: A */
            public MessageOptions getDefaultInstanceForType() {
                return MessageOptions.getDefaultInstance();
            }

            public UninterpretedOption B(int i) {
                x0<UninterpretedOption, UninterpretedOption.b, s> x0Var = this.l0;
                if (x0Var == null) {
                    return this.k0.get(i);
                }
                return x0Var.o(i);
            }

            public int C() {
                x0<UninterpretedOption, UninterpretedOption.b, s> x0Var = this.l0;
                if (x0Var == null) {
                    return this.k0.size();
                }
                return x0Var.n();
            }

            public final x0<UninterpretedOption, UninterpretedOption.b, s> D() {
                if (this.l0 == null) {
                    this.l0 = new x0<>(this.k0, (this.f0 & 16) != 0, getParentForChildren(), isClean());
                    this.k0 = null;
                }
                return this.l0;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0021  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /* renamed from: E */
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public com.google.protobuf.DescriptorProtos.MessageOptions.b mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0<com.google.protobuf.DescriptorProtos$MessageOptions> r1 = com.google.protobuf.DescriptorProtos.MessageOptions.PARSER     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    com.google.protobuf.DescriptorProtos$MessageOptions r3 = (com.google.protobuf.DescriptorProtos.MessageOptions) r3     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    if (r3 == 0) goto Le
                    r2.G(r3)
                Le:
                    return r2
                Lf:
                    r3 = move-exception
                    goto L1f
                L11:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> Lf
                    com.google.protobuf.DescriptorProtos$MessageOptions r4 = (com.google.protobuf.DescriptorProtos.MessageOptions) r4     // Catch: java.lang.Throwable -> Lf
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1d
                    throw r3     // Catch: java.lang.Throwable -> L1d
                L1d:
                    r3 = move-exception
                    r0 = r4
                L1f:
                    if (r0 == 0) goto L24
                    r2.G(r0)
                L24:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.DescriptorProtos.MessageOptions.b.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):com.google.protobuf.DescriptorProtos$MessageOptions$b");
            }

            public b G(MessageOptions messageOptions) {
                if (messageOptions == MessageOptions.getDefaultInstance()) {
                    return this;
                }
                if (messageOptions.hasMessageSetWireFormat()) {
                    M(messageOptions.getMessageSetWireFormat());
                }
                if (messageOptions.hasNoStandardDescriptorAccessor()) {
                    N(messageOptions.getNoStandardDescriptorAccessor());
                }
                if (messageOptions.hasDeprecated()) {
                    J(messageOptions.getDeprecated());
                }
                if (messageOptions.hasMapEntry()) {
                    L(messageOptions.getMapEntry());
                }
                if (this.l0 == null) {
                    if (!messageOptions.uninterpretedOption_.isEmpty()) {
                        if (this.k0.isEmpty()) {
                            this.k0 = messageOptions.uninterpretedOption_;
                            this.f0 &= -17;
                        } else {
                            y();
                            this.k0.addAll(messageOptions.uninterpretedOption_);
                        }
                        onChanged();
                    }
                } else if (!messageOptions.uninterpretedOption_.isEmpty()) {
                    if (!this.l0.u()) {
                        this.l0.b(messageOptions.uninterpretedOption_);
                    } else {
                        this.l0.i();
                        this.l0 = null;
                        this.k0 = messageOptions.uninterpretedOption_;
                        this.f0 &= -17;
                        this.l0 = GeneratedMessageV3.alwaysUseFieldBuilders ? D() : null;
                    }
                }
                j(messageOptions);
                mergeUnknownFields(messageOptions.unknownFields);
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            /* renamed from: H */
            public b mergeFrom(l0 l0Var) {
                if (l0Var instanceof MessageOptions) {
                    return G((MessageOptions) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: I */
            public final b mergeUnknownFields(g1 g1Var) {
                return (b) super.mergeUnknownFields(g1Var);
            }

            public b J(boolean z) {
                this.f0 |= 4;
                this.i0 = z;
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d, com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: K */
            public b setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (b) super.setField(fieldDescriptor, obj);
            }

            public b L(boolean z) {
                this.f0 |= 8;
                this.j0 = z;
                onChanged();
                return this;
            }

            public b M(boolean z) {
                this.f0 |= 1;
                this.g0 = z;
                onChanged();
                return this;
            }

            public b N(boolean z) {
                this.f0 |= 2;
                this.h0 = z;
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d, com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: O */
            public b setRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (b) super.setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: P */
            public final b setUnknownFields(g1 g1Var) {
                return (b) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return DescriptorProtos.B;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return DescriptorProtos.C.d(MessageOptions.class, b.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d, com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                for (int i = 0; i < C(); i++) {
                    if (!B(i).isInitialized()) {
                        return false;
                    }
                }
                return h();
            }

            public final void maybeForceBuilderInitialization() {
                if (GeneratedMessageV3.alwaysUseFieldBuilders) {
                    D();
                }
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d
            /* renamed from: p */
            public b b(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (b) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            /* renamed from: r */
            public MessageOptions build() {
                MessageOptions buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            /* renamed from: s */
            public MessageOptions buildPartial() {
                int i;
                MessageOptions messageOptions = new MessageOptions(this);
                int i2 = this.f0;
                if ((i2 & 1) != 0) {
                    messageOptions.messageSetWireFormat_ = this.g0;
                    i = 1;
                } else {
                    i = 0;
                }
                if ((i2 & 2) != 0) {
                    messageOptions.noStandardDescriptorAccessor_ = this.h0;
                    i |= 2;
                }
                if ((i2 & 4) != 0) {
                    messageOptions.deprecated_ = this.i0;
                    i |= 4;
                }
                if ((i2 & 8) != 0) {
                    messageOptions.mapEntry_ = this.j0;
                    i |= 8;
                }
                x0<UninterpretedOption, UninterpretedOption.b, s> x0Var = this.l0;
                if (x0Var != null) {
                    messageOptions.uninterpretedOption_ = x0Var.g();
                } else {
                    if ((this.f0 & 16) != 0) {
                        this.k0 = Collections.unmodifiableList(this.k0);
                        this.f0 &= -17;
                    }
                    messageOptions.uninterpretedOption_ = this.k0;
                }
                messageOptions.bitField0_ = i;
                onBuilt();
                return messageOptions;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d
            /* renamed from: t */
            public b e() {
                super.clear();
                this.g0 = false;
                int i = this.f0 & (-2);
                this.f0 = i;
                this.h0 = false;
                int i2 = i & (-3);
                this.f0 = i2;
                this.i0 = false;
                int i3 = i2 & (-5);
                this.f0 = i3;
                this.j0 = false;
                this.f0 = i3 & (-9);
                x0<UninterpretedOption, UninterpretedOption.b, s> x0Var = this.l0;
                if (x0Var == null) {
                    this.k0 = Collections.emptyList();
                    this.f0 &= -17;
                } else {
                    x0Var.h();
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d
            /* renamed from: u */
            public b f(Descriptors.FieldDescriptor fieldDescriptor) {
                return (b) super.clearField(fieldDescriptor);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: v */
            public b clearOneof(Descriptors.g gVar) {
                return (b) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            /* renamed from: x */
            public b clone() {
                return (b) super.clone();
            }

            public final void y() {
                if ((this.f0 & 16) == 0) {
                    this.k0 = new ArrayList(this.k0);
                    this.f0 |= 16;
                }
            }

            public b() {
                this.k0 = Collections.emptyList();
                maybeForceBuilderInitialization();
            }

            public b(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.k0 = Collections.emptyList();
                maybeForceBuilderInitialization();
            }
        }

        public static b newBuilder(MessageOptions messageOptions) {
            return a.toBuilder().G(messageOptions);
        }

        public static MessageOptions parseFrom(ByteBuffer byteBuffer, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        public MessageOptions(GeneratedMessageV3.d<MessageOptions, ?> dVar) {
            super(dVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static MessageOptions parseDelimitedFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
            return (MessageOptions) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static MessageOptions parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3.ExtendableMessage, com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public MessageOptions getDefaultInstanceForType() {
            return a;
        }

        @Override // com.google.protobuf.GeneratedMessageV3.ExtendableMessage, com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public b toBuilder() {
            return this == a ? new b() : new b().G(this);
        }

        public static MessageOptions parseFrom(ByteString byteString, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3.ExtendableMessage, com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public b newBuilderForType() {
            return newBuilder();
        }

        public MessageOptions() {
            this.memoizedIsInitialized = (byte) -1;
            this.uninterpretedOption_ = Collections.emptyList();
        }

        public static MessageOptions parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public b newBuilderForType(GeneratedMessageV3.c cVar) {
            return new b(cVar);
        }

        public static MessageOptions parseFrom(byte[] bArr, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static MessageOptions parseFrom(InputStream inputStream) throws IOException {
            return (MessageOptions) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        /* JADX WARN: Multi-variable type inference failed */
        public MessageOptions(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            boolean z2 = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 8) {
                                this.bitField0_ |= 1;
                                this.messageSetWireFormat_ = jVar.p();
                            } else if (J == 16) {
                                this.bitField0_ |= 2;
                                this.noStandardDescriptorAccessor_ = jVar.p();
                            } else if (J == 24) {
                                this.bitField0_ |= 4;
                                this.deprecated_ = jVar.p();
                            } else if (J == 56) {
                                this.bitField0_ |= 8;
                                this.mapEntry_ = jVar.p();
                            } else if (J != 7994) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                if (!(z2 & true)) {
                                    this.uninterpretedOption_ = new ArrayList();
                                    z2 |= true;
                                }
                                this.uninterpretedOption_.add(jVar.z(UninterpretedOption.PARSER, rVar));
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    if (z2 & true) {
                        this.uninterpretedOption_ = Collections.unmodifiableList(this.uninterpretedOption_);
                    }
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static MessageOptions parseFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
            return (MessageOptions) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static MessageOptions parseFrom(com.google.protobuf.j jVar) throws IOException {
            return (MessageOptions) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static MessageOptions parseFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws IOException {
            return (MessageOptions) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes2.dex */
    public static final class MethodDescriptorProto extends GeneratedMessageV3 implements l {
        public static final int CLIENT_STREAMING_FIELD_NUMBER = 5;
        public static final int INPUT_TYPE_FIELD_NUMBER = 2;
        public static final int NAME_FIELD_NUMBER = 1;
        public static final int OPTIONS_FIELD_NUMBER = 4;
        public static final int OUTPUT_TYPE_FIELD_NUMBER = 3;
        public static final int SERVER_STREAMING_FIELD_NUMBER = 6;
        private static final long serialVersionUID = 0;
        private int bitField0_;
        private boolean clientStreaming_;
        private volatile Object inputType_;
        private byte memoizedIsInitialized;
        private volatile Object name_;
        private MethodOptions options_;
        private volatile Object outputType_;
        private boolean serverStreaming_;
        public static final MethodDescriptorProto a = new MethodDescriptorProto();
        @Deprecated
        public static final t0<MethodDescriptorProto> PARSER = new a();

        /* loaded from: classes2.dex */
        public static class a extends com.google.protobuf.c<MethodDescriptorProto> {
            @Override // com.google.protobuf.t0
            /* renamed from: a */
            public MethodDescriptorProto parsePartialFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
                return new MethodDescriptorProto(jVar, rVar);
            }
        }

        public static MethodDescriptorProto getDefaultInstance() {
            return a;
        }

        public static final Descriptors.b getDescriptor() {
            return DescriptorProtos.x;
        }

        public static b newBuilder() {
            return a.toBuilder();
        }

        public static MethodDescriptorProto parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (MethodDescriptorProto) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static MethodDescriptorProto parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<MethodDescriptorProto> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof MethodDescriptorProto)) {
                return super.equals(obj);
            }
            MethodDescriptorProto methodDescriptorProto = (MethodDescriptorProto) obj;
            if (hasName() != methodDescriptorProto.hasName()) {
                return false;
            }
            if ((!hasName() || getName().equals(methodDescriptorProto.getName())) && hasInputType() == methodDescriptorProto.hasInputType()) {
                if ((!hasInputType() || getInputType().equals(methodDescriptorProto.getInputType())) && hasOutputType() == methodDescriptorProto.hasOutputType()) {
                    if ((!hasOutputType() || getOutputType().equals(methodDescriptorProto.getOutputType())) && hasOptions() == methodDescriptorProto.hasOptions()) {
                        if ((!hasOptions() || getOptions().equals(methodDescriptorProto.getOptions())) && hasClientStreaming() == methodDescriptorProto.hasClientStreaming()) {
                            if ((!hasClientStreaming() || getClientStreaming() == methodDescriptorProto.getClientStreaming()) && hasServerStreaming() == methodDescriptorProto.hasServerStreaming()) {
                                return (!hasServerStreaming() || getServerStreaming() == methodDescriptorProto.getServerStreaming()) && this.unknownFields.equals(methodDescriptorProto.unknownFields);
                            }
                            return false;
                        }
                        return false;
                    }
                    return false;
                }
                return false;
            }
            return false;
        }

        public boolean getClientStreaming() {
            return this.clientStreaming_;
        }

        public String getInputType() {
            Object obj = this.inputType_;
            if (obj instanceof String) {
                return (String) obj;
            }
            ByteString byteString = (ByteString) obj;
            String stringUtf8 = byteString.toStringUtf8();
            if (byteString.isValidUtf8()) {
                this.inputType_ = stringUtf8;
            }
            return stringUtf8;
        }

        public ByteString getInputTypeBytes() {
            Object obj = this.inputType_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.inputType_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        public String getName() {
            Object obj = this.name_;
            if (obj instanceof String) {
                return (String) obj;
            }
            ByteString byteString = (ByteString) obj;
            String stringUtf8 = byteString.toStringUtf8();
            if (byteString.isValidUtf8()) {
                this.name_ = stringUtf8;
            }
            return stringUtf8;
        }

        public ByteString getNameBytes() {
            Object obj = this.name_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.name_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        public MethodOptions getOptions() {
            MethodOptions methodOptions = this.options_;
            return methodOptions == null ? MethodOptions.getDefaultInstance() : methodOptions;
        }

        public m getOptionsOrBuilder() {
            MethodOptions methodOptions = this.options_;
            return methodOptions == null ? MethodOptions.getDefaultInstance() : methodOptions;
        }

        public String getOutputType() {
            Object obj = this.outputType_;
            if (obj instanceof String) {
                return (String) obj;
            }
            ByteString byteString = (ByteString) obj;
            String stringUtf8 = byteString.toStringUtf8();
            if (byteString.isValidUtf8()) {
                this.outputType_ = stringUtf8;
            }
            return stringUtf8;
        }

        public ByteString getOutputTypeBytes() {
            Object obj = this.outputType_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.outputType_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<MethodDescriptorProto> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = (this.bitField0_ & 1) != 0 ? 0 + GeneratedMessageV3.computeStringSize(1, this.name_) : 0;
            if ((this.bitField0_ & 2) != 0) {
                computeStringSize += GeneratedMessageV3.computeStringSize(2, this.inputType_);
            }
            if ((this.bitField0_ & 4) != 0) {
                computeStringSize += GeneratedMessageV3.computeStringSize(3, this.outputType_);
            }
            if ((this.bitField0_ & 8) != 0) {
                computeStringSize += CodedOutputStream.G(4, getOptions());
            }
            if ((this.bitField0_ & 16) != 0) {
                computeStringSize += CodedOutputStream.e(5, this.clientStreaming_);
            }
            if ((this.bitField0_ & 32) != 0) {
                computeStringSize += CodedOutputStream.e(6, this.serverStreaming_);
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        public boolean getServerStreaming() {
            return this.serverStreaming_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        public boolean hasClientStreaming() {
            return (this.bitField0_ & 16) != 0;
        }

        public boolean hasInputType() {
            return (this.bitField0_ & 2) != 0;
        }

        public boolean hasName() {
            return (this.bitField0_ & 1) != 0;
        }

        public boolean hasOptions() {
            return (this.bitField0_ & 8) != 0;
        }

        public boolean hasOutputType() {
            return (this.bitField0_ & 4) != 0;
        }

        public boolean hasServerStreaming() {
            return (this.bitField0_ & 32) != 0;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = 779 + getDescriptor().hashCode();
            if (hasName()) {
                hashCode = (((hashCode * 37) + 1) * 53) + getName().hashCode();
            }
            if (hasInputType()) {
                hashCode = (((hashCode * 37) + 2) * 53) + getInputType().hashCode();
            }
            if (hasOutputType()) {
                hashCode = (((hashCode * 37) + 3) * 53) + getOutputType().hashCode();
            }
            if (hasOptions()) {
                hashCode = (((hashCode * 37) + 4) * 53) + getOptions().hashCode();
            }
            if (hasClientStreaming()) {
                hashCode = (((hashCode * 37) + 5) * 53) + a0.c(getClientStreaming());
            }
            if (hasServerStreaming()) {
                hashCode = (((hashCode * 37) + 6) * 53) + a0.c(getServerStreaming());
            }
            int hashCode2 = (hashCode * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode2;
            return hashCode2;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return DescriptorProtos.y.d(MethodDescriptorProto.class, b.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b2 = this.memoizedIsInitialized;
            if (b2 == 1) {
                return true;
            }
            if (b2 == 0) {
                return false;
            }
            if (hasOptions() && !getOptions().isInitialized()) {
                this.memoizedIsInitialized = (byte) 0;
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new MethodDescriptorProto();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if ((this.bitField0_ & 1) != 0) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.name_);
            }
            if ((this.bitField0_ & 2) != 0) {
                GeneratedMessageV3.writeString(codedOutputStream, 2, this.inputType_);
            }
            if ((this.bitField0_ & 4) != 0) {
                GeneratedMessageV3.writeString(codedOutputStream, 3, this.outputType_);
            }
            if ((this.bitField0_ & 8) != 0) {
                codedOutputStream.K0(4, getOptions());
            }
            if ((this.bitField0_ & 16) != 0) {
                codedOutputStream.m0(5, this.clientStreaming_);
            }
            if ((this.bitField0_ & 32) != 0) {
                codedOutputStream.m0(6, this.serverStreaming_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes2.dex */
        public static final class b extends GeneratedMessageV3.b<b> implements l {
            public int a;
            public Object f0;
            public Object g0;
            public Object h0;
            public MethodOptions i0;
            public a1<MethodOptions, MethodOptions.b, m> j0;
            public boolean k0;
            public boolean l0;

            public b A(boolean z) {
                this.a |= 32;
                this.l0 = z;
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: B */
            public final b setUnknownFields(g1 g1Var) {
                return (b) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: a */
            public b addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (b) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            /* renamed from: b */
            public MethodDescriptorProto build() {
                MethodDescriptorProto buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            /* renamed from: d */
            public MethodDescriptorProto buildPartial() {
                MethodDescriptorProto methodDescriptorProto = new MethodDescriptorProto(this);
                int i = this.a;
                int i2 = (i & 1) != 0 ? 1 : 0;
                methodDescriptorProto.name_ = this.f0;
                if ((i & 2) != 0) {
                    i2 |= 2;
                }
                methodDescriptorProto.inputType_ = this.g0;
                if ((i & 4) != 0) {
                    i2 |= 4;
                }
                methodDescriptorProto.outputType_ = this.h0;
                if ((i & 8) != 0) {
                    a1<MethodOptions, MethodOptions.b, m> a1Var = this.j0;
                    if (a1Var == null) {
                        methodDescriptorProto.options_ = this.i0;
                    } else {
                        methodDescriptorProto.options_ = a1Var.b();
                    }
                    i2 |= 8;
                }
                if ((i & 16) != 0) {
                    methodDescriptorProto.clientStreaming_ = this.k0;
                    i2 |= 16;
                }
                if ((i & 32) != 0) {
                    methodDescriptorProto.serverStreaming_ = this.l0;
                    i2 |= 32;
                }
                methodDescriptorProto.bitField0_ = i2;
                onBuilt();
                return methodDescriptorProto;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: e */
            public b clear() {
                super.clear();
                this.f0 = "";
                int i = this.a & (-2);
                this.a = i;
                this.g0 = "";
                int i2 = i & (-3);
                this.a = i2;
                this.h0 = "";
                this.a = i2 & (-5);
                a1<MethodOptions, MethodOptions.b, m> a1Var = this.j0;
                if (a1Var == null) {
                    this.i0 = null;
                } else {
                    a1Var.c();
                }
                int i3 = this.a & (-9);
                this.a = i3;
                this.k0 = false;
                int i4 = i3 & (-17);
                this.a = i4;
                this.l0 = false;
                this.a = i4 & (-33);
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: f */
            public b clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (b) super.clearField(fieldDescriptor);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: g */
            public b clearOneof(Descriptors.g gVar) {
                return (b) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return DescriptorProtos.x;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            /* renamed from: h */
            public b clone() {
                return (b) super.clone();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return DescriptorProtos.y.d(MethodDescriptorProto.class, b.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return !o() || k().isInitialized();
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            /* renamed from: j */
            public MethodDescriptorProto getDefaultInstanceForType() {
                return MethodDescriptorProto.getDefaultInstance();
            }

            public MethodOptions k() {
                a1<MethodOptions, MethodOptions.b, m> a1Var = this.j0;
                if (a1Var == null) {
                    MethodOptions methodOptions = this.i0;
                    return methodOptions == null ? MethodOptions.getDefaultInstance() : methodOptions;
                }
                return a1Var.f();
            }

            public final a1<MethodOptions, MethodOptions.b, m> l() {
                if (this.j0 == null) {
                    this.j0 = new a1<>(k(), getParentForChildren(), isClean());
                    this.i0 = null;
                }
                return this.j0;
            }

            public final void maybeForceBuilderInitialization() {
                if (GeneratedMessageV3.alwaysUseFieldBuilders) {
                    l();
                }
            }

            public boolean o() {
                return (this.a & 8) != 0;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0021  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /* renamed from: p */
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public com.google.protobuf.DescriptorProtos.MethodDescriptorProto.b mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0<com.google.protobuf.DescriptorProtos$MethodDescriptorProto> r1 = com.google.protobuf.DescriptorProtos.MethodDescriptorProto.PARSER     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    com.google.protobuf.DescriptorProtos$MethodDescriptorProto r3 = (com.google.protobuf.DescriptorProtos.MethodDescriptorProto) r3     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    if (r3 == 0) goto Le
                    r2.r(r3)
                Le:
                    return r2
                Lf:
                    r3 = move-exception
                    goto L1f
                L11:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> Lf
                    com.google.protobuf.DescriptorProtos$MethodDescriptorProto r4 = (com.google.protobuf.DescriptorProtos.MethodDescriptorProto) r4     // Catch: java.lang.Throwable -> Lf
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1d
                    throw r3     // Catch: java.lang.Throwable -> L1d
                L1d:
                    r3 = move-exception
                    r0 = r4
                L1f:
                    if (r0 == 0) goto L24
                    r2.r(r0)
                L24:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.DescriptorProtos.MethodDescriptorProto.b.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):com.google.protobuf.DescriptorProtos$MethodDescriptorProto$b");
            }

            public b r(MethodDescriptorProto methodDescriptorProto) {
                if (methodDescriptorProto == MethodDescriptorProto.getDefaultInstance()) {
                    return this;
                }
                if (methodDescriptorProto.hasName()) {
                    this.a |= 1;
                    this.f0 = methodDescriptorProto.name_;
                    onChanged();
                }
                if (methodDescriptorProto.hasInputType()) {
                    this.a |= 2;
                    this.g0 = methodDescriptorProto.inputType_;
                    onChanged();
                }
                if (methodDescriptorProto.hasOutputType()) {
                    this.a |= 4;
                    this.h0 = methodDescriptorProto.outputType_;
                    onChanged();
                }
                if (methodDescriptorProto.hasOptions()) {
                    t(methodDescriptorProto.getOptions());
                }
                if (methodDescriptorProto.hasClientStreaming()) {
                    v(methodDescriptorProto.getClientStreaming());
                }
                if (methodDescriptorProto.hasServerStreaming()) {
                    A(methodDescriptorProto.getServerStreaming());
                }
                mergeUnknownFields(methodDescriptorProto.unknownFields);
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            /* renamed from: s */
            public b mergeFrom(l0 l0Var) {
                if (l0Var instanceof MethodDescriptorProto) {
                    return r((MethodDescriptorProto) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public b t(MethodOptions methodOptions) {
                MethodOptions methodOptions2;
                a1<MethodOptions, MethodOptions.b, m> a1Var = this.j0;
                if (a1Var == null) {
                    if ((this.a & 8) != 0 && (methodOptions2 = this.i0) != null && methodOptions2 != MethodOptions.getDefaultInstance()) {
                        this.i0 = MethodOptions.newBuilder(this.i0).G(methodOptions).buildPartial();
                    } else {
                        this.i0 = methodOptions;
                    }
                    onChanged();
                } else {
                    a1Var.h(methodOptions);
                }
                this.a |= 8;
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: u */
            public final b mergeUnknownFields(g1 g1Var) {
                return (b) super.mergeUnknownFields(g1Var);
            }

            public b v(boolean z) {
                this.a |= 16;
                this.k0 = z;
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: x */
            public b setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (b) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: y */
            public b setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (b) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            public b() {
                this.f0 = "";
                this.g0 = "";
                this.h0 = "";
                maybeForceBuilderInitialization();
            }

            public b(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.f0 = "";
                this.g0 = "";
                this.h0 = "";
                maybeForceBuilderInitialization();
            }
        }

        public static b newBuilder(MethodDescriptorProto methodDescriptorProto) {
            return a.toBuilder().r(methodDescriptorProto);
        }

        public static MethodDescriptorProto parseFrom(ByteBuffer byteBuffer, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        public MethodDescriptorProto(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static MethodDescriptorProto parseDelimitedFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
            return (MethodDescriptorProto) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static MethodDescriptorProto parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public MethodDescriptorProto getDefaultInstanceForType() {
            return a;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public b toBuilder() {
            return this == a ? new b() : new b().r(this);
        }

        public static MethodDescriptorProto parseFrom(ByteString byteString, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public b newBuilderForType() {
            return newBuilder();
        }

        public MethodDescriptorProto() {
            this.memoizedIsInitialized = (byte) -1;
            this.name_ = "";
            this.inputType_ = "";
            this.outputType_ = "";
        }

        public static MethodDescriptorProto parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public b newBuilderForType(GeneratedMessageV3.c cVar) {
            return new b(cVar);
        }

        public static MethodDescriptorProto parseFrom(byte[] bArr, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static MethodDescriptorProto parseFrom(InputStream inputStream) throws IOException {
            return (MethodDescriptorProto) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static MethodDescriptorProto parseFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
            return (MethodDescriptorProto) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public MethodDescriptorProto(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    ByteString q = jVar.q();
                                    this.bitField0_ = 1 | this.bitField0_;
                                    this.name_ = q;
                                } else if (J == 18) {
                                    ByteString q2 = jVar.q();
                                    this.bitField0_ |= 2;
                                    this.inputType_ = q2;
                                } else if (J == 26) {
                                    ByteString q3 = jVar.q();
                                    this.bitField0_ |= 4;
                                    this.outputType_ = q3;
                                } else if (J == 34) {
                                    MethodOptions.b builder = (this.bitField0_ & 8) != 0 ? this.options_.toBuilder() : null;
                                    MethodOptions methodOptions = (MethodOptions) jVar.z(MethodOptions.PARSER, rVar);
                                    this.options_ = methodOptions;
                                    if (builder != null) {
                                        builder.G(methodOptions);
                                        this.options_ = builder.buildPartial();
                                    }
                                    this.bitField0_ |= 8;
                                } else if (J == 40) {
                                    this.bitField0_ |= 16;
                                    this.clientStreaming_ = jVar.p();
                                } else if (J != 48) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.bitField0_ |= 32;
                                    this.serverStreaming_ = jVar.p();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        }
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static MethodDescriptorProto parseFrom(com.google.protobuf.j jVar) throws IOException {
            return (MethodDescriptorProto) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static MethodDescriptorProto parseFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws IOException {
            return (MethodDescriptorProto) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes2.dex */
    public static final class MethodOptions extends GeneratedMessageV3.ExtendableMessage<MethodOptions> implements m {
        public static final int DEPRECATED_FIELD_NUMBER = 33;
        public static final int IDEMPOTENCY_LEVEL_FIELD_NUMBER = 34;
        public static final int UNINTERPRETED_OPTION_FIELD_NUMBER = 999;
        private static final long serialVersionUID = 0;
        private int bitField0_;
        private boolean deprecated_;
        private int idempotencyLevel_;
        private byte memoizedIsInitialized;
        private List<UninterpretedOption> uninterpretedOption_;
        public static final MethodOptions a = new MethodOptions();
        @Deprecated
        public static final t0<MethodOptions> PARSER = new a();

        /* loaded from: classes2.dex */
        public enum IdempotencyLevel implements v0 {
            IDEMPOTENCY_UNKNOWN(0),
            NO_SIDE_EFFECTS(1),
            IDEMPOTENT(2);
            
            public static final int IDEMPOTENCY_UNKNOWN_VALUE = 0;
            public static final int IDEMPOTENT_VALUE = 2;
            public static final int NO_SIDE_EFFECTS_VALUE = 1;
            public static final a0.d<IdempotencyLevel> a = new a();
            public static final IdempotencyLevel[] f0 = values();
            private final int value;

            /* loaded from: classes2.dex */
            public static class a implements a0.d<IdempotencyLevel> {
                @Override // com.google.protobuf.a0.d
                /* renamed from: a */
                public IdempotencyLevel findValueByNumber(int i) {
                    return IdempotencyLevel.forNumber(i);
                }
            }

            IdempotencyLevel(int i) {
                this.value = i;
            }

            public static IdempotencyLevel forNumber(int i) {
                if (i != 0) {
                    if (i != 1) {
                        if (i != 2) {
                            return null;
                        }
                        return IDEMPOTENT;
                    }
                    return NO_SIDE_EFFECTS;
                }
                return IDEMPOTENCY_UNKNOWN;
            }

            public static final Descriptors.c getDescriptor() {
                return MethodOptions.getDescriptor().l().get(0);
            }

            public static a0.d<IdempotencyLevel> internalGetValueMap() {
                return a;
            }

            public final Descriptors.c getDescriptorForType() {
                return getDescriptor();
            }

            @Override // com.google.protobuf.a0.c
            public final int getNumber() {
                return this.value;
            }

            public final Descriptors.d getValueDescriptor() {
                return getDescriptor().k().get(ordinal());
            }

            @Deprecated
            public static IdempotencyLevel valueOf(int i) {
                return forNumber(i);
            }

            public static IdempotencyLevel valueOf(Descriptors.d dVar) {
                if (dVar.h() == getDescriptor()) {
                    return f0[dVar.g()];
                }
                throw new IllegalArgumentException("EnumValueDescriptor is not for this type.");
            }
        }

        /* loaded from: classes2.dex */
        public static class a extends com.google.protobuf.c<MethodOptions> {
            @Override // com.google.protobuf.t0
            /* renamed from: a */
            public MethodOptions parsePartialFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
                return new MethodOptions(jVar, rVar);
            }
        }

        public static MethodOptions getDefaultInstance() {
            return a;
        }

        public static final Descriptors.b getDescriptor() {
            return DescriptorProtos.N;
        }

        public static b newBuilder() {
            return a.toBuilder();
        }

        public static MethodOptions parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (MethodOptions) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static MethodOptions parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<MethodOptions> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof MethodOptions)) {
                return super.equals(obj);
            }
            MethodOptions methodOptions = (MethodOptions) obj;
            if (hasDeprecated() != methodOptions.hasDeprecated()) {
                return false;
            }
            if ((!hasDeprecated() || getDeprecated() == methodOptions.getDeprecated()) && hasIdempotencyLevel() == methodOptions.hasIdempotencyLevel()) {
                return (!hasIdempotencyLevel() || this.idempotencyLevel_ == methodOptions.idempotencyLevel_) && getUninterpretedOptionList().equals(methodOptions.getUninterpretedOptionList()) && this.unknownFields.equals(methodOptions.unknownFields) && getExtensionFields().equals(methodOptions.getExtensionFields());
            }
            return false;
        }

        public boolean getDeprecated() {
            return this.deprecated_;
        }

        public IdempotencyLevel getIdempotencyLevel() {
            IdempotencyLevel valueOf = IdempotencyLevel.valueOf(this.idempotencyLevel_);
            return valueOf == null ? IdempotencyLevel.IDEMPOTENCY_UNKNOWN : valueOf;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<MethodOptions> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int e = (this.bitField0_ & 1) != 0 ? CodedOutputStream.e(33, this.deprecated_) + 0 : 0;
            if ((this.bitField0_ & 2) != 0) {
                e += CodedOutputStream.l(34, this.idempotencyLevel_);
            }
            for (int i2 = 0; i2 < this.uninterpretedOption_.size(); i2++) {
                e += CodedOutputStream.G(999, this.uninterpretedOption_.get(i2));
            }
            int extensionsSerializedSize = e + extensionsSerializedSize() + this.unknownFields.getSerializedSize();
            this.memoizedSize = extensionsSerializedSize;
            return extensionsSerializedSize;
        }

        public UninterpretedOption getUninterpretedOption(int i) {
            return this.uninterpretedOption_.get(i);
        }

        public int getUninterpretedOptionCount() {
            return this.uninterpretedOption_.size();
        }

        public List<UninterpretedOption> getUninterpretedOptionList() {
            return this.uninterpretedOption_;
        }

        public s getUninterpretedOptionOrBuilder(int i) {
            return this.uninterpretedOption_.get(i);
        }

        public List<? extends s> getUninterpretedOptionOrBuilderList() {
            return this.uninterpretedOption_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        public boolean hasDeprecated() {
            return (this.bitField0_ & 1) != 0;
        }

        public boolean hasIdempotencyLevel() {
            return (this.bitField0_ & 2) != 0;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = 779 + getDescriptor().hashCode();
            if (hasDeprecated()) {
                hashCode = (((hashCode * 37) + 33) * 53) + a0.c(getDeprecated());
            }
            if (hasIdempotencyLevel()) {
                hashCode = (((hashCode * 37) + 34) * 53) + this.idempotencyLevel_;
            }
            if (getUninterpretedOptionCount() > 0) {
                hashCode = (((hashCode * 37) + 999) * 53) + getUninterpretedOptionList().hashCode();
            }
            int hashFields = (com.google.protobuf.a.hashFields(hashCode, getExtensionFields()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashFields;
            return hashFields;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return DescriptorProtos.O.d(MethodOptions.class, b.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3.ExtendableMessage, com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b2 = this.memoizedIsInitialized;
            if (b2 == 1) {
                return true;
            }
            if (b2 == 0) {
                return false;
            }
            for (int i = 0; i < getUninterpretedOptionCount(); i++) {
                if (!getUninterpretedOption(i).isInitialized()) {
                    this.memoizedIsInitialized = (byte) 0;
                    return false;
                }
            }
            if (!extensionsAreInitialized()) {
                this.memoizedIsInitialized = (byte) 0;
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new MethodOptions();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            GeneratedMessageV3.ExtendableMessage<MessageType>.a newExtensionWriter = newExtensionWriter();
            if ((this.bitField0_ & 1) != 0) {
                codedOutputStream.m0(33, this.deprecated_);
            }
            if ((this.bitField0_ & 2) != 0) {
                codedOutputStream.u0(34, this.idempotencyLevel_);
            }
            for (int i = 0; i < this.uninterpretedOption_.size(); i++) {
                codedOutputStream.K0(999, this.uninterpretedOption_.get(i));
            }
            newExtensionWriter.a(536870912, codedOutputStream);
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes2.dex */
        public static final class b extends GeneratedMessageV3.d<MethodOptions, b> implements m {
            public int f0;
            public boolean g0;
            public int h0;
            public List<UninterpretedOption> i0;
            public x0<UninterpretedOption, UninterpretedOption.b, s> j0;

            @Override // defpackage.d82, com.google.protobuf.o0
            /* renamed from: A */
            public MethodOptions getDefaultInstanceForType() {
                return MethodOptions.getDefaultInstance();
            }

            public UninterpretedOption B(int i) {
                x0<UninterpretedOption, UninterpretedOption.b, s> x0Var = this.j0;
                if (x0Var == null) {
                    return this.i0.get(i);
                }
                return x0Var.o(i);
            }

            public int C() {
                x0<UninterpretedOption, UninterpretedOption.b, s> x0Var = this.j0;
                if (x0Var == null) {
                    return this.i0.size();
                }
                return x0Var.n();
            }

            public final x0<UninterpretedOption, UninterpretedOption.b, s> D() {
                if (this.j0 == null) {
                    this.j0 = new x0<>(this.i0, (this.f0 & 4) != 0, getParentForChildren(), isClean());
                    this.i0 = null;
                }
                return this.j0;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0021  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /* renamed from: E */
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public com.google.protobuf.DescriptorProtos.MethodOptions.b mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0<com.google.protobuf.DescriptorProtos$MethodOptions> r1 = com.google.protobuf.DescriptorProtos.MethodOptions.PARSER     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    com.google.protobuf.DescriptorProtos$MethodOptions r3 = (com.google.protobuf.DescriptorProtos.MethodOptions) r3     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    if (r3 == 0) goto Le
                    r2.G(r3)
                Le:
                    return r2
                Lf:
                    r3 = move-exception
                    goto L1f
                L11:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> Lf
                    com.google.protobuf.DescriptorProtos$MethodOptions r4 = (com.google.protobuf.DescriptorProtos.MethodOptions) r4     // Catch: java.lang.Throwable -> Lf
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1d
                    throw r3     // Catch: java.lang.Throwable -> L1d
                L1d:
                    r3 = move-exception
                    r0 = r4
                L1f:
                    if (r0 == 0) goto L24
                    r2.G(r0)
                L24:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.DescriptorProtos.MethodOptions.b.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):com.google.protobuf.DescriptorProtos$MethodOptions$b");
            }

            public b G(MethodOptions methodOptions) {
                if (methodOptions == MethodOptions.getDefaultInstance()) {
                    return this;
                }
                if (methodOptions.hasDeprecated()) {
                    J(methodOptions.getDeprecated());
                }
                if (methodOptions.hasIdempotencyLevel()) {
                    L(methodOptions.getIdempotencyLevel());
                }
                if (this.j0 == null) {
                    if (!methodOptions.uninterpretedOption_.isEmpty()) {
                        if (this.i0.isEmpty()) {
                            this.i0 = methodOptions.uninterpretedOption_;
                            this.f0 &= -5;
                        } else {
                            y();
                            this.i0.addAll(methodOptions.uninterpretedOption_);
                        }
                        onChanged();
                    }
                } else if (!methodOptions.uninterpretedOption_.isEmpty()) {
                    if (!this.j0.u()) {
                        this.j0.b(methodOptions.uninterpretedOption_);
                    } else {
                        this.j0.i();
                        this.j0 = null;
                        this.i0 = methodOptions.uninterpretedOption_;
                        this.f0 &= -5;
                        this.j0 = GeneratedMessageV3.alwaysUseFieldBuilders ? D() : null;
                    }
                }
                j(methodOptions);
                mergeUnknownFields(methodOptions.unknownFields);
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            /* renamed from: H */
            public b mergeFrom(l0 l0Var) {
                if (l0Var instanceof MethodOptions) {
                    return G((MethodOptions) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: I */
            public final b mergeUnknownFields(g1 g1Var) {
                return (b) super.mergeUnknownFields(g1Var);
            }

            public b J(boolean z) {
                this.f0 |= 1;
                this.g0 = z;
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d, com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: K */
            public b setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (b) super.setField(fieldDescriptor, obj);
            }

            public b L(IdempotencyLevel idempotencyLevel) {
                Objects.requireNonNull(idempotencyLevel);
                this.f0 |= 2;
                this.h0 = idempotencyLevel.getNumber();
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d, com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: M */
            public b setRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (b) super.setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: N */
            public final b setUnknownFields(g1 g1Var) {
                return (b) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return DescriptorProtos.N;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return DescriptorProtos.O.d(MethodOptions.class, b.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d, com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                for (int i = 0; i < C(); i++) {
                    if (!B(i).isInitialized()) {
                        return false;
                    }
                }
                return h();
            }

            public final void maybeForceBuilderInitialization() {
                if (GeneratedMessageV3.alwaysUseFieldBuilders) {
                    D();
                }
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d
            /* renamed from: p */
            public b b(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (b) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            /* renamed from: r */
            public MethodOptions build() {
                MethodOptions buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            /* renamed from: s */
            public MethodOptions buildPartial() {
                int i;
                MethodOptions methodOptions = new MethodOptions(this);
                int i2 = this.f0;
                if ((i2 & 1) != 0) {
                    methodOptions.deprecated_ = this.g0;
                    i = 1;
                } else {
                    i = 0;
                }
                if ((i2 & 2) != 0) {
                    i |= 2;
                }
                methodOptions.idempotencyLevel_ = this.h0;
                x0<UninterpretedOption, UninterpretedOption.b, s> x0Var = this.j0;
                if (x0Var != null) {
                    methodOptions.uninterpretedOption_ = x0Var.g();
                } else {
                    if ((this.f0 & 4) != 0) {
                        this.i0 = Collections.unmodifiableList(this.i0);
                        this.f0 &= -5;
                    }
                    methodOptions.uninterpretedOption_ = this.i0;
                }
                methodOptions.bitField0_ = i;
                onBuilt();
                return methodOptions;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d
            /* renamed from: t */
            public b e() {
                super.clear();
                this.g0 = false;
                int i = this.f0 & (-2);
                this.f0 = i;
                this.h0 = 0;
                this.f0 = i & (-3);
                x0<UninterpretedOption, UninterpretedOption.b, s> x0Var = this.j0;
                if (x0Var == null) {
                    this.i0 = Collections.emptyList();
                    this.f0 &= -5;
                } else {
                    x0Var.h();
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d
            /* renamed from: u */
            public b f(Descriptors.FieldDescriptor fieldDescriptor) {
                return (b) super.clearField(fieldDescriptor);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: v */
            public b clearOneof(Descriptors.g gVar) {
                return (b) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            /* renamed from: x */
            public b clone() {
                return (b) super.clone();
            }

            public final void y() {
                if ((this.f0 & 4) == 0) {
                    this.i0 = new ArrayList(this.i0);
                    this.f0 |= 4;
                }
            }

            public b() {
                this.h0 = 0;
                this.i0 = Collections.emptyList();
                maybeForceBuilderInitialization();
            }

            public b(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.h0 = 0;
                this.i0 = Collections.emptyList();
                maybeForceBuilderInitialization();
            }
        }

        public static b newBuilder(MethodOptions methodOptions) {
            return a.toBuilder().G(methodOptions);
        }

        public static MethodOptions parseFrom(ByteBuffer byteBuffer, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        public MethodOptions(GeneratedMessageV3.d<MethodOptions, ?> dVar) {
            super(dVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static MethodOptions parseDelimitedFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
            return (MethodOptions) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static MethodOptions parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3.ExtendableMessage, com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public MethodOptions getDefaultInstanceForType() {
            return a;
        }

        @Override // com.google.protobuf.GeneratedMessageV3.ExtendableMessage, com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public b toBuilder() {
            return this == a ? new b() : new b().G(this);
        }

        public static MethodOptions parseFrom(ByteString byteString, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3.ExtendableMessage, com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public b newBuilderForType() {
            return newBuilder();
        }

        public MethodOptions() {
            this.memoizedIsInitialized = (byte) -1;
            this.idempotencyLevel_ = 0;
            this.uninterpretedOption_ = Collections.emptyList();
        }

        public static MethodOptions parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public b newBuilderForType(GeneratedMessageV3.c cVar) {
            return new b(cVar);
        }

        public static MethodOptions parseFrom(byte[] bArr, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static MethodOptions parseFrom(InputStream inputStream) throws IOException {
            return (MethodOptions) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        /* JADX WARN: Multi-variable type inference failed */
        public MethodOptions(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            boolean z2 = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 264) {
                                this.bitField0_ |= 1;
                                this.deprecated_ = jVar.p();
                            } else if (J == 272) {
                                int s = jVar.s();
                                if (IdempotencyLevel.valueOf(s) == null) {
                                    g.x(34, s);
                                } else {
                                    this.bitField0_ |= 2;
                                    this.idempotencyLevel_ = s;
                                }
                            } else if (J != 7994) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                if (!(z2 & true)) {
                                    this.uninterpretedOption_ = new ArrayList();
                                    z2 |= true;
                                }
                                this.uninterpretedOption_.add(jVar.z(UninterpretedOption.PARSER, rVar));
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    if (z2 & true) {
                        this.uninterpretedOption_ = Collections.unmodifiableList(this.uninterpretedOption_);
                    }
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static MethodOptions parseFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
            return (MethodOptions) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static MethodOptions parseFrom(com.google.protobuf.j jVar) throws IOException {
            return (MethodOptions) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static MethodOptions parseFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws IOException {
            return (MethodOptions) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes2.dex */
    public static final class OneofDescriptorProto extends GeneratedMessageV3 implements n {
        public static final int NAME_FIELD_NUMBER = 1;
        public static final int OPTIONS_FIELD_NUMBER = 2;
        private static final long serialVersionUID = 0;
        private int bitField0_;
        private byte memoizedIsInitialized;
        private volatile Object name_;
        private OneofOptions options_;
        public static final OneofDescriptorProto a = new OneofDescriptorProto();
        @Deprecated
        public static final t0<OneofDescriptorProto> PARSER = new a();

        /* loaded from: classes2.dex */
        public static class a extends com.google.protobuf.c<OneofDescriptorProto> {
            @Override // com.google.protobuf.t0
            /* renamed from: a */
            public OneofDescriptorProto parsePartialFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
                return new OneofDescriptorProto(jVar, rVar);
            }
        }

        public static OneofDescriptorProto getDefaultInstance() {
            return a;
        }

        public static final Descriptors.b getDescriptor() {
            return DescriptorProtos.n;
        }

        public static b newBuilder() {
            return a.toBuilder();
        }

        public static OneofDescriptorProto parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (OneofDescriptorProto) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static OneofDescriptorProto parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<OneofDescriptorProto> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof OneofDescriptorProto)) {
                return super.equals(obj);
            }
            OneofDescriptorProto oneofDescriptorProto = (OneofDescriptorProto) obj;
            if (hasName() != oneofDescriptorProto.hasName()) {
                return false;
            }
            if ((!hasName() || getName().equals(oneofDescriptorProto.getName())) && hasOptions() == oneofDescriptorProto.hasOptions()) {
                return (!hasOptions() || getOptions().equals(oneofDescriptorProto.getOptions())) && this.unknownFields.equals(oneofDescriptorProto.unknownFields);
            }
            return false;
        }

        public String getName() {
            Object obj = this.name_;
            if (obj instanceof String) {
                return (String) obj;
            }
            ByteString byteString = (ByteString) obj;
            String stringUtf8 = byteString.toStringUtf8();
            if (byteString.isValidUtf8()) {
                this.name_ = stringUtf8;
            }
            return stringUtf8;
        }

        public ByteString getNameBytes() {
            Object obj = this.name_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.name_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        public OneofOptions getOptions() {
            OneofOptions oneofOptions = this.options_;
            return oneofOptions == null ? OneofOptions.getDefaultInstance() : oneofOptions;
        }

        public o getOptionsOrBuilder() {
            OneofOptions oneofOptions = this.options_;
            return oneofOptions == null ? OneofOptions.getDefaultInstance() : oneofOptions;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<OneofDescriptorProto> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = (this.bitField0_ & 1) != 0 ? 0 + GeneratedMessageV3.computeStringSize(1, this.name_) : 0;
            if ((this.bitField0_ & 2) != 0) {
                computeStringSize += CodedOutputStream.G(2, getOptions());
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        public boolean hasName() {
            return (this.bitField0_ & 1) != 0;
        }

        public boolean hasOptions() {
            return (this.bitField0_ & 2) != 0;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = 779 + getDescriptor().hashCode();
            if (hasName()) {
                hashCode = (((hashCode * 37) + 1) * 53) + getName().hashCode();
            }
            if (hasOptions()) {
                hashCode = (((hashCode * 37) + 2) * 53) + getOptions().hashCode();
            }
            int hashCode2 = (hashCode * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode2;
            return hashCode2;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return DescriptorProtos.o.d(OneofDescriptorProto.class, b.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b2 = this.memoizedIsInitialized;
            if (b2 == 1) {
                return true;
            }
            if (b2 == 0) {
                return false;
            }
            if (hasOptions() && !getOptions().isInitialized()) {
                this.memoizedIsInitialized = (byte) 0;
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new OneofDescriptorProto();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if ((this.bitField0_ & 1) != 0) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.name_);
            }
            if ((this.bitField0_ & 2) != 0) {
                codedOutputStream.K0(2, getOptions());
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes2.dex */
        public static final class b extends GeneratedMessageV3.b<b> implements n {
            public int a;
            public Object f0;
            public OneofOptions g0;
            public a1<OneofOptions, OneofOptions.b, o> h0;

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: a */
            public b addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (b) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            /* renamed from: b */
            public OneofDescriptorProto build() {
                OneofDescriptorProto buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            /* renamed from: d */
            public OneofDescriptorProto buildPartial() {
                OneofDescriptorProto oneofDescriptorProto = new OneofDescriptorProto(this);
                int i = this.a;
                int i2 = (i & 1) != 0 ? 1 : 0;
                oneofDescriptorProto.name_ = this.f0;
                if ((i & 2) != 0) {
                    a1<OneofOptions, OneofOptions.b, o> a1Var = this.h0;
                    if (a1Var == null) {
                        oneofDescriptorProto.options_ = this.g0;
                    } else {
                        oneofDescriptorProto.options_ = a1Var.b();
                    }
                    i2 |= 2;
                }
                oneofDescriptorProto.bitField0_ = i2;
                onBuilt();
                return oneofDescriptorProto;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: e */
            public b clear() {
                super.clear();
                this.f0 = "";
                this.a &= -2;
                a1<OneofOptions, OneofOptions.b, o> a1Var = this.h0;
                if (a1Var == null) {
                    this.g0 = null;
                } else {
                    a1Var.c();
                }
                this.a &= -3;
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: f */
            public b clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (b) super.clearField(fieldDescriptor);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: g */
            public b clearOneof(Descriptors.g gVar) {
                return (b) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return DescriptorProtos.n;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            /* renamed from: h */
            public b clone() {
                return (b) super.clone();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return DescriptorProtos.o.d(OneofDescriptorProto.class, b.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return !o() || k().isInitialized();
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            /* renamed from: j */
            public OneofDescriptorProto getDefaultInstanceForType() {
                return OneofDescriptorProto.getDefaultInstance();
            }

            public OneofOptions k() {
                a1<OneofOptions, OneofOptions.b, o> a1Var = this.h0;
                if (a1Var == null) {
                    OneofOptions oneofOptions = this.g0;
                    return oneofOptions == null ? OneofOptions.getDefaultInstance() : oneofOptions;
                }
                return a1Var.f();
            }

            public final a1<OneofOptions, OneofOptions.b, o> l() {
                if (this.h0 == null) {
                    this.h0 = new a1<>(k(), getParentForChildren(), isClean());
                    this.g0 = null;
                }
                return this.h0;
            }

            public final void maybeForceBuilderInitialization() {
                if (GeneratedMessageV3.alwaysUseFieldBuilders) {
                    l();
                }
            }

            public boolean o() {
                return (this.a & 2) != 0;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0021  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /* renamed from: p */
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public com.google.protobuf.DescriptorProtos.OneofDescriptorProto.b mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0<com.google.protobuf.DescriptorProtos$OneofDescriptorProto> r1 = com.google.protobuf.DescriptorProtos.OneofDescriptorProto.PARSER     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    com.google.protobuf.DescriptorProtos$OneofDescriptorProto r3 = (com.google.protobuf.DescriptorProtos.OneofDescriptorProto) r3     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    if (r3 == 0) goto Le
                    r2.r(r3)
                Le:
                    return r2
                Lf:
                    r3 = move-exception
                    goto L1f
                L11:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> Lf
                    com.google.protobuf.DescriptorProtos$OneofDescriptorProto r4 = (com.google.protobuf.DescriptorProtos.OneofDescriptorProto) r4     // Catch: java.lang.Throwable -> Lf
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1d
                    throw r3     // Catch: java.lang.Throwable -> L1d
                L1d:
                    r3 = move-exception
                    r0 = r4
                L1f:
                    if (r0 == 0) goto L24
                    r2.r(r0)
                L24:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.DescriptorProtos.OneofDescriptorProto.b.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):com.google.protobuf.DescriptorProtos$OneofDescriptorProto$b");
            }

            public b r(OneofDescriptorProto oneofDescriptorProto) {
                if (oneofDescriptorProto == OneofDescriptorProto.getDefaultInstance()) {
                    return this;
                }
                if (oneofDescriptorProto.hasName()) {
                    this.a |= 1;
                    this.f0 = oneofDescriptorProto.name_;
                    onChanged();
                }
                if (oneofDescriptorProto.hasOptions()) {
                    t(oneofDescriptorProto.getOptions());
                }
                mergeUnknownFields(oneofDescriptorProto.unknownFields);
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            /* renamed from: s */
            public b mergeFrom(l0 l0Var) {
                if (l0Var instanceof OneofDescriptorProto) {
                    return r((OneofDescriptorProto) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public b t(OneofOptions oneofOptions) {
                OneofOptions oneofOptions2;
                a1<OneofOptions, OneofOptions.b, o> a1Var = this.h0;
                if (a1Var == null) {
                    if ((this.a & 2) != 0 && (oneofOptions2 = this.g0) != null && oneofOptions2 != OneofOptions.getDefaultInstance()) {
                        this.g0 = OneofOptions.newBuilder(this.g0).G(oneofOptions).buildPartial();
                    } else {
                        this.g0 = oneofOptions;
                    }
                    onChanged();
                } else {
                    a1Var.h(oneofOptions);
                }
                this.a |= 2;
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: u */
            public final b mergeUnknownFields(g1 g1Var) {
                return (b) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: v */
            public b setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (b) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: x */
            public b setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (b) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: y */
            public final b setUnknownFields(g1 g1Var) {
                return (b) super.setUnknownFields(g1Var);
            }

            public b() {
                this.f0 = "";
                maybeForceBuilderInitialization();
            }

            public b(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.f0 = "";
                maybeForceBuilderInitialization();
            }
        }

        public static b newBuilder(OneofDescriptorProto oneofDescriptorProto) {
            return a.toBuilder().r(oneofDescriptorProto);
        }

        public static OneofDescriptorProto parseFrom(ByteBuffer byteBuffer, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        public OneofDescriptorProto(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static OneofDescriptorProto parseDelimitedFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
            return (OneofDescriptorProto) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static OneofDescriptorProto parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public OneofDescriptorProto getDefaultInstanceForType() {
            return a;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public b toBuilder() {
            return this == a ? new b() : new b().r(this);
        }

        public static OneofDescriptorProto parseFrom(ByteString byteString, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public b newBuilderForType() {
            return newBuilder();
        }

        public OneofDescriptorProto() {
            this.memoizedIsInitialized = (byte) -1;
            this.name_ = "";
        }

        public static OneofDescriptorProto parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public b newBuilderForType(GeneratedMessageV3.c cVar) {
            return new b(cVar);
        }

        public static OneofDescriptorProto parseFrom(byte[] bArr, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static OneofDescriptorProto parseFrom(InputStream inputStream) throws IOException {
            return (OneofDescriptorProto) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public OneofDescriptorProto(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 10) {
                                ByteString q = jVar.q();
                                this.bitField0_ = 1 | this.bitField0_;
                                this.name_ = q;
                            } else if (J != 18) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                OneofOptions.b builder = (this.bitField0_ & 2) != 0 ? this.options_.toBuilder() : null;
                                OneofOptions oneofOptions = (OneofOptions) jVar.z(OneofOptions.PARSER, rVar);
                                this.options_ = oneofOptions;
                                if (builder != null) {
                                    builder.G(oneofOptions);
                                    this.options_ = builder.buildPartial();
                                }
                                this.bitField0_ |= 2;
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static OneofDescriptorProto parseFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
            return (OneofDescriptorProto) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static OneofDescriptorProto parseFrom(com.google.protobuf.j jVar) throws IOException {
            return (OneofDescriptorProto) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static OneofDescriptorProto parseFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws IOException {
            return (OneofDescriptorProto) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes2.dex */
    public static final class OneofOptions extends GeneratedMessageV3.ExtendableMessage<OneofOptions> implements o {
        public static final int UNINTERPRETED_OPTION_FIELD_NUMBER = 999;
        private static final long serialVersionUID = 0;
        private byte memoizedIsInitialized;
        private List<UninterpretedOption> uninterpretedOption_;
        public static final OneofOptions a = new OneofOptions();
        @Deprecated
        public static final t0<OneofOptions> PARSER = new a();

        /* loaded from: classes2.dex */
        public static class a extends com.google.protobuf.c<OneofOptions> {
            @Override // com.google.protobuf.t0
            /* renamed from: a */
            public OneofOptions parsePartialFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
                return new OneofOptions(jVar, rVar);
            }
        }

        public static OneofOptions getDefaultInstance() {
            return a;
        }

        public static final Descriptors.b getDescriptor() {
            return DescriptorProtos.F;
        }

        public static b newBuilder() {
            return a.toBuilder();
        }

        public static OneofOptions parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (OneofOptions) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static OneofOptions parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<OneofOptions> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof OneofOptions)) {
                return super.equals(obj);
            }
            OneofOptions oneofOptions = (OneofOptions) obj;
            return getUninterpretedOptionList().equals(oneofOptions.getUninterpretedOptionList()) && this.unknownFields.equals(oneofOptions.unknownFields) && getExtensionFields().equals(oneofOptions.getExtensionFields());
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<OneofOptions> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            for (int i3 = 0; i3 < this.uninterpretedOption_.size(); i3++) {
                i2 += CodedOutputStream.G(999, this.uninterpretedOption_.get(i3));
            }
            int extensionsSerializedSize = i2 + extensionsSerializedSize() + this.unknownFields.getSerializedSize();
            this.memoizedSize = extensionsSerializedSize;
            return extensionsSerializedSize;
        }

        public UninterpretedOption getUninterpretedOption(int i) {
            return this.uninterpretedOption_.get(i);
        }

        public int getUninterpretedOptionCount() {
            return this.uninterpretedOption_.size();
        }

        public List<UninterpretedOption> getUninterpretedOptionList() {
            return this.uninterpretedOption_;
        }

        public s getUninterpretedOptionOrBuilder(int i) {
            return this.uninterpretedOption_.get(i);
        }

        public List<? extends s> getUninterpretedOptionOrBuilderList() {
            return this.uninterpretedOption_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = 779 + getDescriptor().hashCode();
            if (getUninterpretedOptionCount() > 0) {
                hashCode = (((hashCode * 37) + 999) * 53) + getUninterpretedOptionList().hashCode();
            }
            int hashFields = (com.google.protobuf.a.hashFields(hashCode, getExtensionFields()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashFields;
            return hashFields;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return DescriptorProtos.G.d(OneofOptions.class, b.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3.ExtendableMessage, com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b2 = this.memoizedIsInitialized;
            if (b2 == 1) {
                return true;
            }
            if (b2 == 0) {
                return false;
            }
            for (int i = 0; i < getUninterpretedOptionCount(); i++) {
                if (!getUninterpretedOption(i).isInitialized()) {
                    this.memoizedIsInitialized = (byte) 0;
                    return false;
                }
            }
            if (!extensionsAreInitialized()) {
                this.memoizedIsInitialized = (byte) 0;
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new OneofOptions();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            GeneratedMessageV3.ExtendableMessage<MessageType>.a newExtensionWriter = newExtensionWriter();
            for (int i = 0; i < this.uninterpretedOption_.size(); i++) {
                codedOutputStream.K0(999, this.uninterpretedOption_.get(i));
            }
            newExtensionWriter.a(536870912, codedOutputStream);
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes2.dex */
        public static final class b extends GeneratedMessageV3.d<OneofOptions, b> implements o {
            public int f0;
            public List<UninterpretedOption> g0;
            public x0<UninterpretedOption, UninterpretedOption.b, s> h0;

            @Override // defpackage.d82, com.google.protobuf.o0
            /* renamed from: A */
            public OneofOptions getDefaultInstanceForType() {
                return OneofOptions.getDefaultInstance();
            }

            public UninterpretedOption B(int i) {
                x0<UninterpretedOption, UninterpretedOption.b, s> x0Var = this.h0;
                if (x0Var == null) {
                    return this.g0.get(i);
                }
                return x0Var.o(i);
            }

            public int C() {
                x0<UninterpretedOption, UninterpretedOption.b, s> x0Var = this.h0;
                if (x0Var == null) {
                    return this.g0.size();
                }
                return x0Var.n();
            }

            public final x0<UninterpretedOption, UninterpretedOption.b, s> D() {
                if (this.h0 == null) {
                    this.h0 = new x0<>(this.g0, (this.f0 & 1) != 0, getParentForChildren(), isClean());
                    this.g0 = null;
                }
                return this.h0;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0021  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /* renamed from: E */
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public com.google.protobuf.DescriptorProtos.OneofOptions.b mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0<com.google.protobuf.DescriptorProtos$OneofOptions> r1 = com.google.protobuf.DescriptorProtos.OneofOptions.PARSER     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    com.google.protobuf.DescriptorProtos$OneofOptions r3 = (com.google.protobuf.DescriptorProtos.OneofOptions) r3     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    if (r3 == 0) goto Le
                    r2.G(r3)
                Le:
                    return r2
                Lf:
                    r3 = move-exception
                    goto L1f
                L11:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> Lf
                    com.google.protobuf.DescriptorProtos$OneofOptions r4 = (com.google.protobuf.DescriptorProtos.OneofOptions) r4     // Catch: java.lang.Throwable -> Lf
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1d
                    throw r3     // Catch: java.lang.Throwable -> L1d
                L1d:
                    r3 = move-exception
                    r0 = r4
                L1f:
                    if (r0 == 0) goto L24
                    r2.G(r0)
                L24:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.DescriptorProtos.OneofOptions.b.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):com.google.protobuf.DescriptorProtos$OneofOptions$b");
            }

            public b G(OneofOptions oneofOptions) {
                if (oneofOptions == OneofOptions.getDefaultInstance()) {
                    return this;
                }
                if (this.h0 == null) {
                    if (!oneofOptions.uninterpretedOption_.isEmpty()) {
                        if (this.g0.isEmpty()) {
                            this.g0 = oneofOptions.uninterpretedOption_;
                            this.f0 &= -2;
                        } else {
                            y();
                            this.g0.addAll(oneofOptions.uninterpretedOption_);
                        }
                        onChanged();
                    }
                } else if (!oneofOptions.uninterpretedOption_.isEmpty()) {
                    if (!this.h0.u()) {
                        this.h0.b(oneofOptions.uninterpretedOption_);
                    } else {
                        this.h0.i();
                        this.h0 = null;
                        this.g0 = oneofOptions.uninterpretedOption_;
                        this.f0 &= -2;
                        this.h0 = GeneratedMessageV3.alwaysUseFieldBuilders ? D() : null;
                    }
                }
                j(oneofOptions);
                mergeUnknownFields(oneofOptions.unknownFields);
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            /* renamed from: H */
            public b mergeFrom(l0 l0Var) {
                if (l0Var instanceof OneofOptions) {
                    return G((OneofOptions) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: I */
            public final b mergeUnknownFields(g1 g1Var) {
                return (b) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d, com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: J */
            public b setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (b) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d, com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: K */
            public b setRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (b) super.setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: L */
            public final b setUnknownFields(g1 g1Var) {
                return (b) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return DescriptorProtos.F;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return DescriptorProtos.G.d(OneofOptions.class, b.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d, com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                for (int i = 0; i < C(); i++) {
                    if (!B(i).isInitialized()) {
                        return false;
                    }
                }
                return h();
            }

            public final void maybeForceBuilderInitialization() {
                if (GeneratedMessageV3.alwaysUseFieldBuilders) {
                    D();
                }
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d
            /* renamed from: p */
            public b b(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (b) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            /* renamed from: r */
            public OneofOptions build() {
                OneofOptions buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            /* renamed from: s */
            public OneofOptions buildPartial() {
                OneofOptions oneofOptions = new OneofOptions(this);
                int i = this.f0;
                x0<UninterpretedOption, UninterpretedOption.b, s> x0Var = this.h0;
                if (x0Var == null) {
                    if ((i & 1) != 0) {
                        this.g0 = Collections.unmodifiableList(this.g0);
                        this.f0 &= -2;
                    }
                    oneofOptions.uninterpretedOption_ = this.g0;
                } else {
                    oneofOptions.uninterpretedOption_ = x0Var.g();
                }
                onBuilt();
                return oneofOptions;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d
            /* renamed from: t */
            public b e() {
                super.clear();
                x0<UninterpretedOption, UninterpretedOption.b, s> x0Var = this.h0;
                if (x0Var == null) {
                    this.g0 = Collections.emptyList();
                    this.f0 &= -2;
                } else {
                    x0Var.h();
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d
            /* renamed from: u */
            public b f(Descriptors.FieldDescriptor fieldDescriptor) {
                return (b) super.clearField(fieldDescriptor);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: v */
            public b clearOneof(Descriptors.g gVar) {
                return (b) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            /* renamed from: x */
            public b clone() {
                return (b) super.clone();
            }

            public final void y() {
                if ((this.f0 & 1) == 0) {
                    this.g0 = new ArrayList(this.g0);
                    this.f0 |= 1;
                }
            }

            public b() {
                this.g0 = Collections.emptyList();
                maybeForceBuilderInitialization();
            }

            public b(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.g0 = Collections.emptyList();
                maybeForceBuilderInitialization();
            }
        }

        public static b newBuilder(OneofOptions oneofOptions) {
            return a.toBuilder().G(oneofOptions);
        }

        public static OneofOptions parseFrom(ByteBuffer byteBuffer, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        public OneofOptions(GeneratedMessageV3.d<OneofOptions, ?> dVar) {
            super(dVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static OneofOptions parseDelimitedFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
            return (OneofOptions) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static OneofOptions parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3.ExtendableMessage, com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public OneofOptions getDefaultInstanceForType() {
            return a;
        }

        @Override // com.google.protobuf.GeneratedMessageV3.ExtendableMessage, com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public b toBuilder() {
            return this == a ? new b() : new b().G(this);
        }

        public static OneofOptions parseFrom(ByteString byteString, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3.ExtendableMessage, com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public b newBuilderForType() {
            return newBuilder();
        }

        public OneofOptions() {
            this.memoizedIsInitialized = (byte) -1;
            this.uninterpretedOption_ = Collections.emptyList();
        }

        public static OneofOptions parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public b newBuilderForType(GeneratedMessageV3.c cVar) {
            return new b(cVar);
        }

        public static OneofOptions parseFrom(byte[] bArr, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static OneofOptions parseFrom(InputStream inputStream) throws IOException {
            return (OneofOptions) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        /* JADX WARN: Multi-variable type inference failed */
        public OneofOptions(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            boolean z2 = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J != 7994) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                if (!(z2 & true)) {
                                    this.uninterpretedOption_ = new ArrayList();
                                    z2 |= true;
                                }
                                this.uninterpretedOption_.add(jVar.z(UninterpretedOption.PARSER, rVar));
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    if (z2 & true) {
                        this.uninterpretedOption_ = Collections.unmodifiableList(this.uninterpretedOption_);
                    }
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static OneofOptions parseFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
            return (OneofOptions) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static OneofOptions parseFrom(com.google.protobuf.j jVar) throws IOException {
            return (OneofOptions) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static OneofOptions parseFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws IOException {
            return (OneofOptions) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes2.dex */
    public static final class ServiceDescriptorProto extends GeneratedMessageV3 implements p {
        public static final int METHOD_FIELD_NUMBER = 2;
        public static final int NAME_FIELD_NUMBER = 1;
        public static final int OPTIONS_FIELD_NUMBER = 3;
        private static final long serialVersionUID = 0;
        private int bitField0_;
        private byte memoizedIsInitialized;
        private List<MethodDescriptorProto> method_;
        private volatile Object name_;
        private ServiceOptions options_;
        public static final ServiceDescriptorProto a = new ServiceDescriptorProto();
        @Deprecated
        public static final t0<ServiceDescriptorProto> PARSER = new a();

        /* loaded from: classes2.dex */
        public static class a extends com.google.protobuf.c<ServiceDescriptorProto> {
            @Override // com.google.protobuf.t0
            /* renamed from: a */
            public ServiceDescriptorProto parsePartialFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
                return new ServiceDescriptorProto(jVar, rVar);
            }
        }

        public static ServiceDescriptorProto getDefaultInstance() {
            return a;
        }

        public static final Descriptors.b getDescriptor() {
            return DescriptorProtos.v;
        }

        public static b newBuilder() {
            return a.toBuilder();
        }

        public static ServiceDescriptorProto parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (ServiceDescriptorProto) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static ServiceDescriptorProto parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<ServiceDescriptorProto> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof ServiceDescriptorProto)) {
                return super.equals(obj);
            }
            ServiceDescriptorProto serviceDescriptorProto = (ServiceDescriptorProto) obj;
            if (hasName() != serviceDescriptorProto.hasName()) {
                return false;
            }
            if ((!hasName() || getName().equals(serviceDescriptorProto.getName())) && getMethodList().equals(serviceDescriptorProto.getMethodList()) && hasOptions() == serviceDescriptorProto.hasOptions()) {
                return (!hasOptions() || getOptions().equals(serviceDescriptorProto.getOptions())) && this.unknownFields.equals(serviceDescriptorProto.unknownFields);
            }
            return false;
        }

        public MethodDescriptorProto getMethod(int i) {
            return this.method_.get(i);
        }

        public int getMethodCount() {
            return this.method_.size();
        }

        public List<MethodDescriptorProto> getMethodList() {
            return this.method_;
        }

        public l getMethodOrBuilder(int i) {
            return this.method_.get(i);
        }

        public List<? extends l> getMethodOrBuilderList() {
            return this.method_;
        }

        public String getName() {
            Object obj = this.name_;
            if (obj instanceof String) {
                return (String) obj;
            }
            ByteString byteString = (ByteString) obj;
            String stringUtf8 = byteString.toStringUtf8();
            if (byteString.isValidUtf8()) {
                this.name_ = stringUtf8;
            }
            return stringUtf8;
        }

        public ByteString getNameBytes() {
            Object obj = this.name_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.name_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        public ServiceOptions getOptions() {
            ServiceOptions serviceOptions = this.options_;
            return serviceOptions == null ? ServiceOptions.getDefaultInstance() : serviceOptions;
        }

        public q getOptionsOrBuilder() {
            ServiceOptions serviceOptions = this.options_;
            return serviceOptions == null ? ServiceOptions.getDefaultInstance() : serviceOptions;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<ServiceDescriptorProto> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = (this.bitField0_ & 1) != 0 ? GeneratedMessageV3.computeStringSize(1, this.name_) + 0 : 0;
            for (int i2 = 0; i2 < this.method_.size(); i2++) {
                computeStringSize += CodedOutputStream.G(2, this.method_.get(i2));
            }
            if ((this.bitField0_ & 2) != 0) {
                computeStringSize += CodedOutputStream.G(3, getOptions());
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        public boolean hasName() {
            return (this.bitField0_ & 1) != 0;
        }

        public boolean hasOptions() {
            return (this.bitField0_ & 2) != 0;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = 779 + getDescriptor().hashCode();
            if (hasName()) {
                hashCode = (((hashCode * 37) + 1) * 53) + getName().hashCode();
            }
            if (getMethodCount() > 0) {
                hashCode = (((hashCode * 37) + 2) * 53) + getMethodList().hashCode();
            }
            if (hasOptions()) {
                hashCode = (((hashCode * 37) + 3) * 53) + getOptions().hashCode();
            }
            int hashCode2 = (hashCode * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode2;
            return hashCode2;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return DescriptorProtos.w.d(ServiceDescriptorProto.class, b.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b2 = this.memoizedIsInitialized;
            if (b2 == 1) {
                return true;
            }
            if (b2 == 0) {
                return false;
            }
            for (int i = 0; i < getMethodCount(); i++) {
                if (!getMethod(i).isInitialized()) {
                    this.memoizedIsInitialized = (byte) 0;
                    return false;
                }
            }
            if (hasOptions() && !getOptions().isInitialized()) {
                this.memoizedIsInitialized = (byte) 0;
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new ServiceDescriptorProto();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if ((this.bitField0_ & 1) != 0) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.name_);
            }
            for (int i = 0; i < this.method_.size(); i++) {
                codedOutputStream.K0(2, this.method_.get(i));
            }
            if ((this.bitField0_ & 2) != 0) {
                codedOutputStream.K0(3, getOptions());
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes2.dex */
        public static final class b extends GeneratedMessageV3.b<b> implements p {
            public int a;
            public Object f0;
            public List<MethodDescriptorProto> g0;
            public x0<MethodDescriptorProto, MethodDescriptorProto.b, l> h0;
            public ServiceOptions i0;
            public a1<ServiceOptions, ServiceOptions.b, q> j0;

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: A */
            public final b mergeUnknownFields(g1 g1Var) {
                return (b) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: B */
            public b setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (b) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: C */
            public b setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (b) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: D */
            public final b setUnknownFields(g1 g1Var) {
                return (b) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: a */
            public b addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (b) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            /* renamed from: b */
            public ServiceDescriptorProto build() {
                ServiceDescriptorProto buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            /* renamed from: d */
            public ServiceDescriptorProto buildPartial() {
                ServiceDescriptorProto serviceDescriptorProto = new ServiceDescriptorProto(this);
                int i = this.a;
                int i2 = (i & 1) != 0 ? 1 : 0;
                serviceDescriptorProto.name_ = this.f0;
                x0<MethodDescriptorProto, MethodDescriptorProto.b, l> x0Var = this.h0;
                if (x0Var != null) {
                    serviceDescriptorProto.method_ = x0Var.g();
                } else {
                    if ((this.a & 2) != 0) {
                        this.g0 = Collections.unmodifiableList(this.g0);
                        this.a &= -3;
                    }
                    serviceDescriptorProto.method_ = this.g0;
                }
                if ((i & 4) != 0) {
                    a1<ServiceOptions, ServiceOptions.b, q> a1Var = this.j0;
                    if (a1Var == null) {
                        serviceDescriptorProto.options_ = this.i0;
                    } else {
                        serviceDescriptorProto.options_ = a1Var.b();
                    }
                    i2 |= 2;
                }
                serviceDescriptorProto.bitField0_ = i2;
                onBuilt();
                return serviceDescriptorProto;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: e */
            public b clear() {
                super.clear();
                this.f0 = "";
                this.a &= -2;
                x0<MethodDescriptorProto, MethodDescriptorProto.b, l> x0Var = this.h0;
                if (x0Var == null) {
                    this.g0 = Collections.emptyList();
                    this.a &= -3;
                } else {
                    x0Var.h();
                }
                a1<ServiceOptions, ServiceOptions.b, q> a1Var = this.j0;
                if (a1Var == null) {
                    this.i0 = null;
                } else {
                    a1Var.c();
                }
                this.a &= -5;
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: f */
            public b clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (b) super.clearField(fieldDescriptor);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: g */
            public b clearOneof(Descriptors.g gVar) {
                return (b) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return DescriptorProtos.v;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            /* renamed from: h */
            public b clone() {
                return (b) super.clone();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return DescriptorProtos.w.d(ServiceDescriptorProto.class, b.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                for (int i = 0; i < o(); i++) {
                    if (!l(i).isInitialized()) {
                        return false;
                    }
                }
                return !t() || r().isInitialized();
            }

            public final void j() {
                if ((this.a & 2) == 0) {
                    this.g0 = new ArrayList(this.g0);
                    this.a |= 2;
                }
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            /* renamed from: k */
            public ServiceDescriptorProto getDefaultInstanceForType() {
                return ServiceDescriptorProto.getDefaultInstance();
            }

            public MethodDescriptorProto l(int i) {
                x0<MethodDescriptorProto, MethodDescriptorProto.b, l> x0Var = this.h0;
                if (x0Var == null) {
                    return this.g0.get(i);
                }
                return x0Var.o(i);
            }

            public final void maybeForceBuilderInitialization() {
                if (GeneratedMessageV3.alwaysUseFieldBuilders) {
                    p();
                    s();
                }
            }

            public int o() {
                x0<MethodDescriptorProto, MethodDescriptorProto.b, l> x0Var = this.h0;
                if (x0Var == null) {
                    return this.g0.size();
                }
                return x0Var.n();
            }

            public final x0<MethodDescriptorProto, MethodDescriptorProto.b, l> p() {
                if (this.h0 == null) {
                    this.h0 = new x0<>(this.g0, (this.a & 2) != 0, getParentForChildren(), isClean());
                    this.g0 = null;
                }
                return this.h0;
            }

            public ServiceOptions r() {
                a1<ServiceOptions, ServiceOptions.b, q> a1Var = this.j0;
                if (a1Var == null) {
                    ServiceOptions serviceOptions = this.i0;
                    return serviceOptions == null ? ServiceOptions.getDefaultInstance() : serviceOptions;
                }
                return a1Var.f();
            }

            public final a1<ServiceOptions, ServiceOptions.b, q> s() {
                if (this.j0 == null) {
                    this.j0 = new a1<>(r(), getParentForChildren(), isClean());
                    this.i0 = null;
                }
                return this.j0;
            }

            public boolean t() {
                return (this.a & 4) != 0;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0021  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /* renamed from: u */
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public com.google.protobuf.DescriptorProtos.ServiceDescriptorProto.b mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0<com.google.protobuf.DescriptorProtos$ServiceDescriptorProto> r1 = com.google.protobuf.DescriptorProtos.ServiceDescriptorProto.PARSER     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    com.google.protobuf.DescriptorProtos$ServiceDescriptorProto r3 = (com.google.protobuf.DescriptorProtos.ServiceDescriptorProto) r3     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    if (r3 == 0) goto Le
                    r2.v(r3)
                Le:
                    return r2
                Lf:
                    r3 = move-exception
                    goto L1f
                L11:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> Lf
                    com.google.protobuf.DescriptorProtos$ServiceDescriptorProto r4 = (com.google.protobuf.DescriptorProtos.ServiceDescriptorProto) r4     // Catch: java.lang.Throwable -> Lf
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1d
                    throw r3     // Catch: java.lang.Throwable -> L1d
                L1d:
                    r3 = move-exception
                    r0 = r4
                L1f:
                    if (r0 == 0) goto L24
                    r2.v(r0)
                L24:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.DescriptorProtos.ServiceDescriptorProto.b.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):com.google.protobuf.DescriptorProtos$ServiceDescriptorProto$b");
            }

            public b v(ServiceDescriptorProto serviceDescriptorProto) {
                if (serviceDescriptorProto == ServiceDescriptorProto.getDefaultInstance()) {
                    return this;
                }
                if (serviceDescriptorProto.hasName()) {
                    this.a |= 1;
                    this.f0 = serviceDescriptorProto.name_;
                    onChanged();
                }
                if (this.h0 == null) {
                    if (!serviceDescriptorProto.method_.isEmpty()) {
                        if (this.g0.isEmpty()) {
                            this.g0 = serviceDescriptorProto.method_;
                            this.a &= -3;
                        } else {
                            j();
                            this.g0.addAll(serviceDescriptorProto.method_);
                        }
                        onChanged();
                    }
                } else if (!serviceDescriptorProto.method_.isEmpty()) {
                    if (!this.h0.u()) {
                        this.h0.b(serviceDescriptorProto.method_);
                    } else {
                        this.h0.i();
                        this.h0 = null;
                        this.g0 = serviceDescriptorProto.method_;
                        this.a &= -3;
                        this.h0 = GeneratedMessageV3.alwaysUseFieldBuilders ? p() : null;
                    }
                }
                if (serviceDescriptorProto.hasOptions()) {
                    y(serviceDescriptorProto.getOptions());
                }
                mergeUnknownFields(serviceDescriptorProto.unknownFields);
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            /* renamed from: x */
            public b mergeFrom(l0 l0Var) {
                if (l0Var instanceof ServiceDescriptorProto) {
                    return v((ServiceDescriptorProto) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public b y(ServiceOptions serviceOptions) {
                ServiceOptions serviceOptions2;
                a1<ServiceOptions, ServiceOptions.b, q> a1Var = this.j0;
                if (a1Var == null) {
                    if ((this.a & 4) != 0 && (serviceOptions2 = this.i0) != null && serviceOptions2 != ServiceOptions.getDefaultInstance()) {
                        this.i0 = ServiceOptions.newBuilder(this.i0).G(serviceOptions).buildPartial();
                    } else {
                        this.i0 = serviceOptions;
                    }
                    onChanged();
                } else {
                    a1Var.h(serviceOptions);
                }
                this.a |= 4;
                return this;
            }

            public b() {
                this.f0 = "";
                this.g0 = Collections.emptyList();
                maybeForceBuilderInitialization();
            }

            public b(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.f0 = "";
                this.g0 = Collections.emptyList();
                maybeForceBuilderInitialization();
            }
        }

        public static b newBuilder(ServiceDescriptorProto serviceDescriptorProto) {
            return a.toBuilder().v(serviceDescriptorProto);
        }

        public static ServiceDescriptorProto parseFrom(ByteBuffer byteBuffer, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        public ServiceDescriptorProto(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static ServiceDescriptorProto parseDelimitedFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
            return (ServiceDescriptorProto) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static ServiceDescriptorProto parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public ServiceDescriptorProto getDefaultInstanceForType() {
            return a;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public b toBuilder() {
            return this == a ? new b() : new b().v(this);
        }

        public static ServiceDescriptorProto parseFrom(ByteString byteString, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public b newBuilderForType() {
            return newBuilder();
        }

        public ServiceDescriptorProto() {
            this.memoizedIsInitialized = (byte) -1;
            this.name_ = "";
            this.method_ = Collections.emptyList();
        }

        public static ServiceDescriptorProto parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public b newBuilderForType(GeneratedMessageV3.c cVar) {
            return new b(cVar);
        }

        public static ServiceDescriptorProto parseFrom(byte[] bArr, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static ServiceDescriptorProto parseFrom(InputStream inputStream) throws IOException {
            return (ServiceDescriptorProto) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        /* JADX WARN: Multi-variable type inference failed */
        public ServiceDescriptorProto(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            boolean z2 = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 10) {
                                ByteString q = jVar.q();
                                this.bitField0_ = 1 | this.bitField0_;
                                this.name_ = q;
                            } else if (J == 18) {
                                if (!(z2 & true)) {
                                    this.method_ = new ArrayList();
                                    z2 |= true;
                                }
                                this.method_.add(jVar.z(MethodDescriptorProto.PARSER, rVar));
                            } else if (J != 26) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                ServiceOptions.b builder = (this.bitField0_ & 2) != 0 ? this.options_.toBuilder() : null;
                                ServiceOptions serviceOptions = (ServiceOptions) jVar.z(ServiceOptions.PARSER, rVar);
                                this.options_ = serviceOptions;
                                if (builder != null) {
                                    builder.G(serviceOptions);
                                    this.options_ = builder.buildPartial();
                                }
                                this.bitField0_ |= 2;
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    if (z2 & true) {
                        this.method_ = Collections.unmodifiableList(this.method_);
                    }
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static ServiceDescriptorProto parseFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
            return (ServiceDescriptorProto) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static ServiceDescriptorProto parseFrom(com.google.protobuf.j jVar) throws IOException {
            return (ServiceDescriptorProto) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static ServiceDescriptorProto parseFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws IOException {
            return (ServiceDescriptorProto) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes2.dex */
    public static final class ServiceOptions extends GeneratedMessageV3.ExtendableMessage<ServiceOptions> implements q {
        public static final int DEPRECATED_FIELD_NUMBER = 33;
        public static final int UNINTERPRETED_OPTION_FIELD_NUMBER = 999;
        private static final long serialVersionUID = 0;
        private int bitField0_;
        private boolean deprecated_;
        private byte memoizedIsInitialized;
        private List<UninterpretedOption> uninterpretedOption_;
        public static final ServiceOptions a = new ServiceOptions();
        @Deprecated
        public static final t0<ServiceOptions> PARSER = new a();

        /* loaded from: classes2.dex */
        public static class a extends com.google.protobuf.c<ServiceOptions> {
            @Override // com.google.protobuf.t0
            /* renamed from: a */
            public ServiceOptions parsePartialFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
                return new ServiceOptions(jVar, rVar);
            }
        }

        public static ServiceOptions getDefaultInstance() {
            return a;
        }

        public static final Descriptors.b getDescriptor() {
            return DescriptorProtos.L;
        }

        public static b newBuilder() {
            return a.toBuilder();
        }

        public static ServiceOptions parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (ServiceOptions) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static ServiceOptions parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<ServiceOptions> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof ServiceOptions)) {
                return super.equals(obj);
            }
            ServiceOptions serviceOptions = (ServiceOptions) obj;
            if (hasDeprecated() != serviceOptions.hasDeprecated()) {
                return false;
            }
            return (!hasDeprecated() || getDeprecated() == serviceOptions.getDeprecated()) && getUninterpretedOptionList().equals(serviceOptions.getUninterpretedOptionList()) && this.unknownFields.equals(serviceOptions.unknownFields) && getExtensionFields().equals(serviceOptions.getExtensionFields());
        }

        public boolean getDeprecated() {
            return this.deprecated_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<ServiceOptions> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int e = (this.bitField0_ & 1) != 0 ? CodedOutputStream.e(33, this.deprecated_) + 0 : 0;
            for (int i2 = 0; i2 < this.uninterpretedOption_.size(); i2++) {
                e += CodedOutputStream.G(999, this.uninterpretedOption_.get(i2));
            }
            int extensionsSerializedSize = e + extensionsSerializedSize() + this.unknownFields.getSerializedSize();
            this.memoizedSize = extensionsSerializedSize;
            return extensionsSerializedSize;
        }

        public UninterpretedOption getUninterpretedOption(int i) {
            return this.uninterpretedOption_.get(i);
        }

        public int getUninterpretedOptionCount() {
            return this.uninterpretedOption_.size();
        }

        public List<UninterpretedOption> getUninterpretedOptionList() {
            return this.uninterpretedOption_;
        }

        public s getUninterpretedOptionOrBuilder(int i) {
            return this.uninterpretedOption_.get(i);
        }

        public List<? extends s> getUninterpretedOptionOrBuilderList() {
            return this.uninterpretedOption_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        public boolean hasDeprecated() {
            return (this.bitField0_ & 1) != 0;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = 779 + getDescriptor().hashCode();
            if (hasDeprecated()) {
                hashCode = (((hashCode * 37) + 33) * 53) + a0.c(getDeprecated());
            }
            if (getUninterpretedOptionCount() > 0) {
                hashCode = (((hashCode * 37) + 999) * 53) + getUninterpretedOptionList().hashCode();
            }
            int hashFields = (com.google.protobuf.a.hashFields(hashCode, getExtensionFields()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashFields;
            return hashFields;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return DescriptorProtos.M.d(ServiceOptions.class, b.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3.ExtendableMessage, com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b2 = this.memoizedIsInitialized;
            if (b2 == 1) {
                return true;
            }
            if (b2 == 0) {
                return false;
            }
            for (int i = 0; i < getUninterpretedOptionCount(); i++) {
                if (!getUninterpretedOption(i).isInitialized()) {
                    this.memoizedIsInitialized = (byte) 0;
                    return false;
                }
            }
            if (!extensionsAreInitialized()) {
                this.memoizedIsInitialized = (byte) 0;
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new ServiceOptions();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            GeneratedMessageV3.ExtendableMessage<MessageType>.a newExtensionWriter = newExtensionWriter();
            if ((this.bitField0_ & 1) != 0) {
                codedOutputStream.m0(33, this.deprecated_);
            }
            for (int i = 0; i < this.uninterpretedOption_.size(); i++) {
                codedOutputStream.K0(999, this.uninterpretedOption_.get(i));
            }
            newExtensionWriter.a(536870912, codedOutputStream);
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes2.dex */
        public static final class b extends GeneratedMessageV3.d<ServiceOptions, b> implements q {
            public int f0;
            public boolean g0;
            public List<UninterpretedOption> h0;
            public x0<UninterpretedOption, UninterpretedOption.b, s> i0;

            @Override // defpackage.d82, com.google.protobuf.o0
            /* renamed from: A */
            public ServiceOptions getDefaultInstanceForType() {
                return ServiceOptions.getDefaultInstance();
            }

            public UninterpretedOption B(int i) {
                x0<UninterpretedOption, UninterpretedOption.b, s> x0Var = this.i0;
                if (x0Var == null) {
                    return this.h0.get(i);
                }
                return x0Var.o(i);
            }

            public int C() {
                x0<UninterpretedOption, UninterpretedOption.b, s> x0Var = this.i0;
                if (x0Var == null) {
                    return this.h0.size();
                }
                return x0Var.n();
            }

            public final x0<UninterpretedOption, UninterpretedOption.b, s> D() {
                if (this.i0 == null) {
                    this.i0 = new x0<>(this.h0, (this.f0 & 2) != 0, getParentForChildren(), isClean());
                    this.h0 = null;
                }
                return this.i0;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0021  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /* renamed from: E */
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public com.google.protobuf.DescriptorProtos.ServiceOptions.b mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0<com.google.protobuf.DescriptorProtos$ServiceOptions> r1 = com.google.protobuf.DescriptorProtos.ServiceOptions.PARSER     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    com.google.protobuf.DescriptorProtos$ServiceOptions r3 = (com.google.protobuf.DescriptorProtos.ServiceOptions) r3     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    if (r3 == 0) goto Le
                    r2.G(r3)
                Le:
                    return r2
                Lf:
                    r3 = move-exception
                    goto L1f
                L11:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> Lf
                    com.google.protobuf.DescriptorProtos$ServiceOptions r4 = (com.google.protobuf.DescriptorProtos.ServiceOptions) r4     // Catch: java.lang.Throwable -> Lf
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1d
                    throw r3     // Catch: java.lang.Throwable -> L1d
                L1d:
                    r3 = move-exception
                    r0 = r4
                L1f:
                    if (r0 == 0) goto L24
                    r2.G(r0)
                L24:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.DescriptorProtos.ServiceOptions.b.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):com.google.protobuf.DescriptorProtos$ServiceOptions$b");
            }

            public b G(ServiceOptions serviceOptions) {
                if (serviceOptions == ServiceOptions.getDefaultInstance()) {
                    return this;
                }
                if (serviceOptions.hasDeprecated()) {
                    J(serviceOptions.getDeprecated());
                }
                if (this.i0 == null) {
                    if (!serviceOptions.uninterpretedOption_.isEmpty()) {
                        if (this.h0.isEmpty()) {
                            this.h0 = serviceOptions.uninterpretedOption_;
                            this.f0 &= -3;
                        } else {
                            y();
                            this.h0.addAll(serviceOptions.uninterpretedOption_);
                        }
                        onChanged();
                    }
                } else if (!serviceOptions.uninterpretedOption_.isEmpty()) {
                    if (!this.i0.u()) {
                        this.i0.b(serviceOptions.uninterpretedOption_);
                    } else {
                        this.i0.i();
                        this.i0 = null;
                        this.h0 = serviceOptions.uninterpretedOption_;
                        this.f0 &= -3;
                        this.i0 = GeneratedMessageV3.alwaysUseFieldBuilders ? D() : null;
                    }
                }
                j(serviceOptions);
                mergeUnknownFields(serviceOptions.unknownFields);
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            /* renamed from: H */
            public b mergeFrom(l0 l0Var) {
                if (l0Var instanceof ServiceOptions) {
                    return G((ServiceOptions) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: I */
            public final b mergeUnknownFields(g1 g1Var) {
                return (b) super.mergeUnknownFields(g1Var);
            }

            public b J(boolean z) {
                this.f0 |= 1;
                this.g0 = z;
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d, com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: K */
            public b setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (b) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d, com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: L */
            public b setRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (b) super.setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: M */
            public final b setUnknownFields(g1 g1Var) {
                return (b) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return DescriptorProtos.L;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return DescriptorProtos.M.d(ServiceOptions.class, b.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d, com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                for (int i = 0; i < C(); i++) {
                    if (!B(i).isInitialized()) {
                        return false;
                    }
                }
                return h();
            }

            public final void maybeForceBuilderInitialization() {
                if (GeneratedMessageV3.alwaysUseFieldBuilders) {
                    D();
                }
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d
            /* renamed from: p */
            public b b(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (b) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            /* renamed from: r */
            public ServiceOptions build() {
                ServiceOptions buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            /* renamed from: s */
            public ServiceOptions buildPartial() {
                ServiceOptions serviceOptions = new ServiceOptions(this);
                int i = 1;
                if ((this.f0 & 1) != 0) {
                    serviceOptions.deprecated_ = this.g0;
                } else {
                    i = 0;
                }
                x0<UninterpretedOption, UninterpretedOption.b, s> x0Var = this.i0;
                if (x0Var != null) {
                    serviceOptions.uninterpretedOption_ = x0Var.g();
                } else {
                    if ((this.f0 & 2) != 0) {
                        this.h0 = Collections.unmodifiableList(this.h0);
                        this.f0 &= -3;
                    }
                    serviceOptions.uninterpretedOption_ = this.h0;
                }
                serviceOptions.bitField0_ = i;
                onBuilt();
                return serviceOptions;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d
            /* renamed from: t */
            public b e() {
                super.clear();
                this.g0 = false;
                this.f0 &= -2;
                x0<UninterpretedOption, UninterpretedOption.b, s> x0Var = this.i0;
                if (x0Var == null) {
                    this.h0 = Collections.emptyList();
                    this.f0 &= -3;
                } else {
                    x0Var.h();
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.d
            /* renamed from: u */
            public b f(Descriptors.FieldDescriptor fieldDescriptor) {
                return (b) super.clearField(fieldDescriptor);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: v */
            public b clearOneof(Descriptors.g gVar) {
                return (b) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            /* renamed from: x */
            public b clone() {
                return (b) super.clone();
            }

            public final void y() {
                if ((this.f0 & 2) == 0) {
                    this.h0 = new ArrayList(this.h0);
                    this.f0 |= 2;
                }
            }

            public b() {
                this.h0 = Collections.emptyList();
                maybeForceBuilderInitialization();
            }

            public b(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.h0 = Collections.emptyList();
                maybeForceBuilderInitialization();
            }
        }

        public static b newBuilder(ServiceOptions serviceOptions) {
            return a.toBuilder().G(serviceOptions);
        }

        public static ServiceOptions parseFrom(ByteBuffer byteBuffer, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        public ServiceOptions(GeneratedMessageV3.d<ServiceOptions, ?> dVar) {
            super(dVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static ServiceOptions parseDelimitedFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
            return (ServiceOptions) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static ServiceOptions parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3.ExtendableMessage, com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public ServiceOptions getDefaultInstanceForType() {
            return a;
        }

        @Override // com.google.protobuf.GeneratedMessageV3.ExtendableMessage, com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public b toBuilder() {
            return this == a ? new b() : new b().G(this);
        }

        public static ServiceOptions parseFrom(ByteString byteString, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3.ExtendableMessage, com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public b newBuilderForType() {
            return newBuilder();
        }

        public ServiceOptions() {
            this.memoizedIsInitialized = (byte) -1;
            this.uninterpretedOption_ = Collections.emptyList();
        }

        public static ServiceOptions parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public b newBuilderForType(GeneratedMessageV3.c cVar) {
            return new b(cVar);
        }

        public static ServiceOptions parseFrom(byte[] bArr, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static ServiceOptions parseFrom(InputStream inputStream) throws IOException {
            return (ServiceOptions) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        /* JADX WARN: Multi-variable type inference failed */
        public ServiceOptions(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            boolean z2 = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 264) {
                                this.bitField0_ |= 1;
                                this.deprecated_ = jVar.p();
                            } else if (J != 7994) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                if (!(z2 & true)) {
                                    this.uninterpretedOption_ = new ArrayList();
                                    z2 |= true;
                                }
                                this.uninterpretedOption_.add(jVar.z(UninterpretedOption.PARSER, rVar));
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    if (z2 & true) {
                        this.uninterpretedOption_ = Collections.unmodifiableList(this.uninterpretedOption_);
                    }
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static ServiceOptions parseFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
            return (ServiceOptions) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static ServiceOptions parseFrom(com.google.protobuf.j jVar) throws IOException {
            return (ServiceOptions) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static ServiceOptions parseFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws IOException {
            return (ServiceOptions) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes2.dex */
    public static final class SourceCodeInfo extends GeneratedMessageV3 implements r {
        public static final int LOCATION_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private List<Location> location_;
        private byte memoizedIsInitialized;
        public static final SourceCodeInfo a = new SourceCodeInfo();
        @Deprecated
        public static final t0<SourceCodeInfo> PARSER = new a();

        /* loaded from: classes2.dex */
        public static final class Location extends GeneratedMessageV3 implements c {
            public static final int LEADING_COMMENTS_FIELD_NUMBER = 3;
            public static final int LEADING_DETACHED_COMMENTS_FIELD_NUMBER = 6;
            public static final int PATH_FIELD_NUMBER = 1;
            public static final int SPAN_FIELD_NUMBER = 2;
            public static final int TRAILING_COMMENTS_FIELD_NUMBER = 4;
            private static final long serialVersionUID = 0;
            private int bitField0_;
            private volatile Object leadingComments_;
            private cz1 leadingDetachedComments_;
            private byte memoizedIsInitialized;
            private int pathMemoizedSerializedSize;
            private a0.g path_;
            private int spanMemoizedSerializedSize;
            private a0.g span_;
            private volatile Object trailingComments_;
            public static final Location a = new Location();
            @Deprecated
            public static final t0<Location> PARSER = new a();

            /* loaded from: classes2.dex */
            public static class a extends com.google.protobuf.c<Location> {
                @Override // com.google.protobuf.t0
                /* renamed from: a */
                public Location parsePartialFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
                    return new Location(jVar, rVar);
                }
            }

            public static Location getDefaultInstance() {
                return a;
            }

            public static final Descriptors.b getDescriptor() {
                return DescriptorProtos.V;
            }

            public static b newBuilder() {
                return a.toBuilder();
            }

            public static Location parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (Location) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
            }

            public static Location parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer);
            }

            public static t0<Location> parser() {
                return PARSER;
            }

            @Override // com.google.protobuf.a
            public boolean equals(Object obj) {
                if (obj == this) {
                    return true;
                }
                if (!(obj instanceof Location)) {
                    return super.equals(obj);
                }
                Location location = (Location) obj;
                if (getPathList().equals(location.getPathList()) && getSpanList().equals(location.getSpanList()) && hasLeadingComments() == location.hasLeadingComments()) {
                    if ((!hasLeadingComments() || getLeadingComments().equals(location.getLeadingComments())) && hasTrailingComments() == location.hasTrailingComments()) {
                        return (!hasTrailingComments() || getTrailingComments().equals(location.getTrailingComments())) && m18getLeadingDetachedCommentsList().equals(location.m18getLeadingDetachedCommentsList()) && this.unknownFields.equals(location.unknownFields);
                    }
                    return false;
                }
                return false;
            }

            public String getLeadingComments() {
                Object obj = this.leadingComments_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                ByteString byteString = (ByteString) obj;
                String stringUtf8 = byteString.toStringUtf8();
                if (byteString.isValidUtf8()) {
                    this.leadingComments_ = stringUtf8;
                }
                return stringUtf8;
            }

            public ByteString getLeadingCommentsBytes() {
                Object obj = this.leadingComments_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.leadingComments_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            public String getLeadingDetachedComments(int i) {
                return this.leadingDetachedComments_.get(i);
            }

            public ByteString getLeadingDetachedCommentsBytes(int i) {
                return this.leadingDetachedComments_.e1(i);
            }

            public int getLeadingDetachedCommentsCount() {
                return this.leadingDetachedComments_.size();
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
            public t0<Location> getParserForType() {
                return PARSER;
            }

            public int getPath(int i) {
                return this.path_.getInt(i);
            }

            public int getPathCount() {
                return this.path_.size();
            }

            public List<Integer> getPathList() {
                return this.path_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public int getSerializedSize() {
                int i = this.memoizedSize;
                if (i != -1) {
                    return i;
                }
                int i2 = 0;
                for (int i3 = 0; i3 < this.path_.size(); i3++) {
                    i2 += CodedOutputStream.y(this.path_.getInt(i3));
                }
                int i4 = 0 + i2;
                if (!getPathList().isEmpty()) {
                    i4 = i4 + 1 + CodedOutputStream.y(i2);
                }
                this.pathMemoizedSerializedSize = i2;
                int i5 = 0;
                for (int i6 = 0; i6 < this.span_.size(); i6++) {
                    i5 += CodedOutputStream.y(this.span_.getInt(i6));
                }
                int i7 = i4 + i5;
                if (!getSpanList().isEmpty()) {
                    i7 = i7 + 1 + CodedOutputStream.y(i5);
                }
                this.spanMemoizedSerializedSize = i5;
                if ((this.bitField0_ & 1) != 0) {
                    i7 += GeneratedMessageV3.computeStringSize(3, this.leadingComments_);
                }
                if ((this.bitField0_ & 2) != 0) {
                    i7 += GeneratedMessageV3.computeStringSize(4, this.trailingComments_);
                }
                int i8 = 0;
                for (int i9 = 0; i9 < this.leadingDetachedComments_.size(); i9++) {
                    i8 += GeneratedMessageV3.computeStringSizeNoTag(this.leadingDetachedComments_.j(i9));
                }
                int size = i7 + i8 + (m18getLeadingDetachedCommentsList().size() * 1) + this.unknownFields.getSerializedSize();
                this.memoizedSize = size;
                return size;
            }

            public int getSpan(int i) {
                return this.span_.getInt(i);
            }

            public int getSpanCount() {
                return this.span_.size();
            }

            public List<Integer> getSpanList() {
                return this.span_;
            }

            public String getTrailingComments() {
                Object obj = this.trailingComments_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                ByteString byteString = (ByteString) obj;
                String stringUtf8 = byteString.toStringUtf8();
                if (byteString.isValidUtf8()) {
                    this.trailingComments_ = stringUtf8;
                }
                return stringUtf8;
            }

            public ByteString getTrailingCommentsBytes() {
                Object obj = this.trailingComments_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.trailingComments_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
            public final g1 getUnknownFields() {
                return this.unknownFields;
            }

            public boolean hasLeadingComments() {
                return (this.bitField0_ & 1) != 0;
            }

            public boolean hasTrailingComments() {
                return (this.bitField0_ & 2) != 0;
            }

            @Override // com.google.protobuf.a
            public int hashCode() {
                int i = this.memoizedHashCode;
                if (i != 0) {
                    return i;
                }
                int hashCode = 779 + getDescriptor().hashCode();
                if (getPathCount() > 0) {
                    hashCode = (((hashCode * 37) + 1) * 53) + getPathList().hashCode();
                }
                if (getSpanCount() > 0) {
                    hashCode = (((hashCode * 37) + 2) * 53) + getSpanList().hashCode();
                }
                if (hasLeadingComments()) {
                    hashCode = (((hashCode * 37) + 3) * 53) + getLeadingComments().hashCode();
                }
                if (hasTrailingComments()) {
                    hashCode = (((hashCode * 37) + 4) * 53) + getTrailingComments().hashCode();
                }
                if (getLeadingDetachedCommentsCount() > 0) {
                    hashCode = (((hashCode * 37) + 6) * 53) + m18getLeadingDetachedCommentsList().hashCode();
                }
                int hashCode2 = (hashCode * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode2;
                return hashCode2;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return DescriptorProtos.W.d(Location.class, b.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
            public final boolean isInitialized() {
                byte b2 = this.memoizedIsInitialized;
                if (b2 == 1) {
                    return true;
                }
                if (b2 == 0) {
                    return false;
                }
                this.memoizedIsInitialized = (byte) 1;
                return true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Object newInstance(GeneratedMessageV3.f fVar) {
                return new Location();
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
                getSerializedSize();
                if (getPathList().size() > 0) {
                    codedOutputStream.c1(10);
                    codedOutputStream.c1(this.pathMemoizedSerializedSize);
                }
                for (int i = 0; i < this.path_.size(); i++) {
                    codedOutputStream.H0(this.path_.getInt(i));
                }
                if (getSpanList().size() > 0) {
                    codedOutputStream.c1(18);
                    codedOutputStream.c1(this.spanMemoizedSerializedSize);
                }
                for (int i2 = 0; i2 < this.span_.size(); i2++) {
                    codedOutputStream.H0(this.span_.getInt(i2));
                }
                if ((this.bitField0_ & 1) != 0) {
                    GeneratedMessageV3.writeString(codedOutputStream, 3, this.leadingComments_);
                }
                if ((this.bitField0_ & 2) != 0) {
                    GeneratedMessageV3.writeString(codedOutputStream, 4, this.trailingComments_);
                }
                for (int i3 = 0; i3 < this.leadingDetachedComments_.size(); i3++) {
                    GeneratedMessageV3.writeString(codedOutputStream, 6, this.leadingDetachedComments_.j(i3));
                }
                this.unknownFields.writeTo(codedOutputStream);
            }

            /* loaded from: classes2.dex */
            public static final class b extends GeneratedMessageV3.b<b> implements c {
                public int a;
                public a0.g f0;
                public a0.g g0;
                public Object h0;
                public Object i0;
                public cz1 j0;

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                /* renamed from: a */
                public b addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (b) super.addRepeatedField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                /* renamed from: b */
                public Location build() {
                    Location buildPartial = buildPartial();
                    if (buildPartial.isInitialized()) {
                        return buildPartial;
                    }
                    throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                /* renamed from: d */
                public Location buildPartial() {
                    Location location = new Location(this);
                    int i = this.a;
                    if ((i & 1) != 0) {
                        this.f0.l();
                        this.a &= -2;
                    }
                    location.path_ = this.f0;
                    if ((this.a & 2) != 0) {
                        this.g0.l();
                        this.a &= -3;
                    }
                    location.span_ = this.g0;
                    int i2 = (i & 4) != 0 ? 1 : 0;
                    location.leadingComments_ = this.h0;
                    if ((i & 8) != 0) {
                        i2 |= 2;
                    }
                    location.trailingComments_ = this.i0;
                    if ((this.a & 16) != 0) {
                        this.j0 = this.j0.x();
                        this.a &= -17;
                    }
                    location.leadingDetachedComments_ = this.j0;
                    location.bitField0_ = i2;
                    onBuilt();
                    return location;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                /* renamed from: e */
                public b clear() {
                    super.clear();
                    this.f0 = GeneratedMessageV3.emptyIntList();
                    this.a &= -2;
                    this.g0 = GeneratedMessageV3.emptyIntList();
                    int i = this.a & (-3);
                    this.a = i;
                    this.h0 = "";
                    int i2 = i & (-5);
                    this.a = i2;
                    this.i0 = "";
                    int i3 = i2 & (-9);
                    this.a = i3;
                    this.j0 = d0.h0;
                    this.a = i3 & (-17);
                    return this;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                /* renamed from: f */
                public b clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                    return (b) super.clearField(fieldDescriptor);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                /* renamed from: g */
                public b clearOneof(Descriptors.g gVar) {
                    return (b) super.clearOneof(gVar);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
                public Descriptors.b getDescriptorForType() {
                    return DescriptorProtos.V;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
                /* renamed from: h */
                public b clone() {
                    return (b) super.clone();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                    return DescriptorProtos.W.d(Location.class, b.class);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
                public final boolean isInitialized() {
                    return true;
                }

                public final void j() {
                    if ((this.a & 16) == 0) {
                        this.j0 = new d0(this.j0);
                        this.a |= 16;
                    }
                }

                public final void k() {
                    if ((this.a & 1) == 0) {
                        this.f0 = GeneratedMessageV3.mutableCopy(this.f0);
                        this.a |= 1;
                    }
                }

                public final void l() {
                    if ((this.a & 2) == 0) {
                        this.g0 = GeneratedMessageV3.mutableCopy(this.g0);
                        this.a |= 2;
                    }
                }

                public final void maybeForceBuilderInitialization() {
                    boolean z = GeneratedMessageV3.alwaysUseFieldBuilders;
                }

                @Override // defpackage.d82, com.google.protobuf.o0
                /* renamed from: o */
                public Location getDefaultInstanceForType() {
                    return Location.getDefaultInstance();
                }

                /* JADX WARN: Removed duplicated region for block: B:16:0x0021  */
                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
                /* renamed from: p */
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct code enable 'Show inconsistent code' option in preferences
                */
                public com.google.protobuf.DescriptorProtos.SourceCodeInfo.Location.b mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                    /*
                        r2 = this;
                        r0 = 0
                        com.google.protobuf.t0<com.google.protobuf.DescriptorProtos$SourceCodeInfo$Location> r1 = com.google.protobuf.DescriptorProtos.SourceCodeInfo.Location.PARSER     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                        java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                        com.google.protobuf.DescriptorProtos$SourceCodeInfo$Location r3 = (com.google.protobuf.DescriptorProtos.SourceCodeInfo.Location) r3     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                        if (r3 == 0) goto Le
                        r2.r(r3)
                    Le:
                        return r2
                    Lf:
                        r3 = move-exception
                        goto L1f
                    L11:
                        r3 = move-exception
                        com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> Lf
                        com.google.protobuf.DescriptorProtos$SourceCodeInfo$Location r4 = (com.google.protobuf.DescriptorProtos.SourceCodeInfo.Location) r4     // Catch: java.lang.Throwable -> Lf
                        java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1d
                        throw r3     // Catch: java.lang.Throwable -> L1d
                    L1d:
                        r3 = move-exception
                        r0 = r4
                    L1f:
                        if (r0 == 0) goto L24
                        r2.r(r0)
                    L24:
                        throw r3
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.DescriptorProtos.SourceCodeInfo.Location.b.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):com.google.protobuf.DescriptorProtos$SourceCodeInfo$Location$b");
                }

                public b r(Location location) {
                    if (location == Location.getDefaultInstance()) {
                        return this;
                    }
                    if (!location.path_.isEmpty()) {
                        if (this.f0.isEmpty()) {
                            this.f0 = location.path_;
                            this.a &= -2;
                        } else {
                            k();
                            this.f0.addAll(location.path_);
                        }
                        onChanged();
                    }
                    if (!location.span_.isEmpty()) {
                        if (this.g0.isEmpty()) {
                            this.g0 = location.span_;
                            this.a &= -3;
                        } else {
                            l();
                            this.g0.addAll(location.span_);
                        }
                        onChanged();
                    }
                    if (location.hasLeadingComments()) {
                        this.a |= 4;
                        this.h0 = location.leadingComments_;
                        onChanged();
                    }
                    if (location.hasTrailingComments()) {
                        this.a |= 8;
                        this.i0 = location.trailingComments_;
                        onChanged();
                    }
                    if (!location.leadingDetachedComments_.isEmpty()) {
                        if (this.j0.isEmpty()) {
                            this.j0 = location.leadingDetachedComments_;
                            this.a &= -17;
                        } else {
                            j();
                            this.j0.addAll(location.leadingDetachedComments_);
                        }
                        onChanged();
                    }
                    mergeUnknownFields(location.unknownFields);
                    onChanged();
                    return this;
                }

                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
                /* renamed from: s */
                public b mergeFrom(l0 l0Var) {
                    if (l0Var instanceof Location) {
                        return r((Location) l0Var);
                    }
                    super.mergeFrom(l0Var);
                    return this;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                /* renamed from: t */
                public final b mergeUnknownFields(g1 g1Var) {
                    return (b) super.mergeUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                /* renamed from: u */
                public b setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (b) super.setField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                /* renamed from: v */
                public b setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                    return (b) super.m19setRepeatedField(fieldDescriptor, i, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                /* renamed from: x */
                public final b setUnknownFields(g1 g1Var) {
                    return (b) super.setUnknownFields(g1Var);
                }

                public b() {
                    this.f0 = GeneratedMessageV3.emptyIntList();
                    this.g0 = GeneratedMessageV3.emptyIntList();
                    this.h0 = "";
                    this.i0 = "";
                    this.j0 = d0.h0;
                    maybeForceBuilderInitialization();
                }

                public b(GeneratedMessageV3.c cVar) {
                    super(cVar);
                    this.f0 = GeneratedMessageV3.emptyIntList();
                    this.g0 = GeneratedMessageV3.emptyIntList();
                    this.h0 = "";
                    this.i0 = "";
                    this.j0 = d0.h0;
                    maybeForceBuilderInitialization();
                }
            }

            public static b newBuilder(Location location) {
                return a.toBuilder().r(location);
            }

            public static Location parseFrom(ByteBuffer byteBuffer, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer, rVar);
            }

            /* renamed from: getLeadingDetachedCommentsList */
            public dw2 m18getLeadingDetachedCommentsList() {
                return this.leadingDetachedComments_;
            }

            public Location(GeneratedMessageV3.b<?> bVar) {
                super(bVar);
                this.pathMemoizedSerializedSize = -1;
                this.spanMemoizedSerializedSize = -1;
                this.memoizedIsInitialized = (byte) -1;
            }

            public static Location parseDelimitedFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
                return (Location) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
            }

            public static Location parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
            public Location getDefaultInstanceForType() {
                return a;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public b toBuilder() {
                return this == a ? new b() : new b().r(this);
            }

            public static Location parseFrom(ByteString byteString, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString, rVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public b newBuilderForType() {
                return newBuilder();
            }

            public static Location parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr);
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public b newBuilderForType(GeneratedMessageV3.c cVar) {
                return new b(cVar);
            }

            public static Location parseFrom(byte[] bArr, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr, rVar);
            }

            public Location() {
                this.pathMemoizedSerializedSize = -1;
                this.spanMemoizedSerializedSize = -1;
                this.memoizedIsInitialized = (byte) -1;
                this.path_ = GeneratedMessageV3.emptyIntList();
                this.span_ = GeneratedMessageV3.emptyIntList();
                this.leadingComments_ = "";
                this.trailingComments_ = "";
                this.leadingDetachedComments_ = d0.h0;
            }

            public static Location parseFrom(InputStream inputStream) throws IOException {
                return (Location) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
            }

            public static Location parseFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
                return (Location) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
            }

            public static Location parseFrom(com.google.protobuf.j jVar) throws IOException {
                return (Location) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
            }

            public static Location parseFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws IOException {
                return (Location) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
            }

            public Location(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
                this();
                Objects.requireNonNull(rVar);
                g1.b g = g1.g();
                boolean z = false;
                boolean z2 = false;
                while (!z) {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 8) {
                                    if (!(z2 & true)) {
                                        this.path_ = GeneratedMessageV3.newIntList();
                                        z2 |= true;
                                    }
                                    this.path_.X(jVar.x());
                                } else if (J == 10) {
                                    int o = jVar.o(jVar.B());
                                    if (!(z2 & true) && jVar.d() > 0) {
                                        this.path_ = GeneratedMessageV3.newIntList();
                                        z2 |= true;
                                    }
                                    while (jVar.d() > 0) {
                                        this.path_.X(jVar.x());
                                    }
                                    jVar.n(o);
                                } else if (J == 16) {
                                    if (!(z2 & true)) {
                                        this.span_ = GeneratedMessageV3.newIntList();
                                        z2 |= true;
                                    }
                                    this.span_.X(jVar.x());
                                } else if (J == 18) {
                                    int o2 = jVar.o(jVar.B());
                                    if (!(z2 & true) && jVar.d() > 0) {
                                        this.span_ = GeneratedMessageV3.newIntList();
                                        z2 |= true;
                                    }
                                    while (jVar.d() > 0) {
                                        this.span_.X(jVar.x());
                                    }
                                    jVar.n(o2);
                                } else if (J == 26) {
                                    ByteString q = jVar.q();
                                    this.bitField0_ = 1 | this.bitField0_;
                                    this.leadingComments_ = q;
                                } else if (J == 34) {
                                    ByteString q2 = jVar.q();
                                    this.bitField0_ |= 2;
                                    this.trailingComments_ = q2;
                                } else if (J != 50) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    ByteString q3 = jVar.q();
                                    if (!(z2 & true)) {
                                        this.leadingDetachedComments_ = new d0();
                                        z2 |= true;
                                    }
                                    this.leadingDetachedComments_.W(q3);
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        } catch (IOException e2) {
                            throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                        }
                    } finally {
                        if (z2 & true) {
                            this.path_.l();
                        }
                        if (z2 & true) {
                            this.span_.l();
                        }
                        if (z2 & true) {
                            this.leadingDetachedComments_ = this.leadingDetachedComments_.x();
                        }
                        this.unknownFields = g.build();
                        makeExtensionsImmutable();
                    }
                }
            }
        }

        /* loaded from: classes2.dex */
        public static class a extends com.google.protobuf.c<SourceCodeInfo> {
            @Override // com.google.protobuf.t0
            /* renamed from: a */
            public SourceCodeInfo parsePartialFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
                return new SourceCodeInfo(jVar, rVar);
            }
        }

        /* loaded from: classes2.dex */
        public interface c extends o0 {
        }

        public static SourceCodeInfo getDefaultInstance() {
            return a;
        }

        public static final Descriptors.b getDescriptor() {
            return DescriptorProtos.T;
        }

        public static b newBuilder() {
            return a.toBuilder();
        }

        public static SourceCodeInfo parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SourceCodeInfo) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SourceCodeInfo parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SourceCodeInfo> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SourceCodeInfo)) {
                return super.equals(obj);
            }
            SourceCodeInfo sourceCodeInfo = (SourceCodeInfo) obj;
            return getLocationList().equals(sourceCodeInfo.getLocationList()) && this.unknownFields.equals(sourceCodeInfo.unknownFields);
        }

        public Location getLocation(int i) {
            return this.location_.get(i);
        }

        public int getLocationCount() {
            return this.location_.size();
        }

        public List<Location> getLocationList() {
            return this.location_;
        }

        public c getLocationOrBuilder(int i) {
            return this.location_.get(i);
        }

        public List<? extends c> getLocationOrBuilderList() {
            return this.location_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SourceCodeInfo> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            for (int i3 = 0; i3 < this.location_.size(); i3++) {
                i2 += CodedOutputStream.G(1, this.location_.get(i3));
            }
            int serializedSize = i2 + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = 779 + getDescriptor().hashCode();
            if (getLocationCount() > 0) {
                hashCode = (((hashCode * 37) + 1) * 53) + getLocationList().hashCode();
            }
            int hashCode2 = (hashCode * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode2;
            return hashCode2;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return DescriptorProtos.U.d(SourceCodeInfo.class, b.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b2 = this.memoizedIsInitialized;
            if (b2 == 1) {
                return true;
            }
            if (b2 == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SourceCodeInfo();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            for (int i = 0; i < this.location_.size(); i++) {
                codedOutputStream.K0(1, this.location_.get(i));
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes2.dex */
        public static final class b extends GeneratedMessageV3.b<b> implements r {
            public int a;
            public List<Location> f0;
            public x0<Location, Location.b, c> g0;

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: a */
            public b addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (b) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            /* renamed from: b */
            public SourceCodeInfo build() {
                SourceCodeInfo buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            /* renamed from: d */
            public SourceCodeInfo buildPartial() {
                SourceCodeInfo sourceCodeInfo = new SourceCodeInfo(this);
                int i = this.a;
                x0<Location, Location.b, c> x0Var = this.g0;
                if (x0Var == null) {
                    if ((i & 1) != 0) {
                        this.f0 = Collections.unmodifiableList(this.f0);
                        this.a &= -2;
                    }
                    sourceCodeInfo.location_ = this.f0;
                } else {
                    sourceCodeInfo.location_ = x0Var.g();
                }
                onBuilt();
                return sourceCodeInfo;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: e */
            public b clear() {
                super.clear();
                x0<Location, Location.b, c> x0Var = this.g0;
                if (x0Var == null) {
                    this.f0 = Collections.emptyList();
                    this.a &= -2;
                } else {
                    x0Var.h();
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: f */
            public b clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (b) super.clearField(fieldDescriptor);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: g */
            public b clearOneof(Descriptors.g gVar) {
                return (b) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return DescriptorProtos.T;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            /* renamed from: h */
            public b clone() {
                return (b) super.clone();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return DescriptorProtos.U.d(SourceCodeInfo.class, b.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public final void j() {
                if ((this.a & 1) == 0) {
                    this.f0 = new ArrayList(this.f0);
                    this.a |= 1;
                }
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            /* renamed from: k */
            public SourceCodeInfo getDefaultInstanceForType() {
                return SourceCodeInfo.getDefaultInstance();
            }

            public final x0<Location, Location.b, c> l() {
                if (this.g0 == null) {
                    this.g0 = new x0<>(this.f0, (this.a & 1) != 0, getParentForChildren(), isClean());
                    this.f0 = null;
                }
                return this.g0;
            }

            public final void maybeForceBuilderInitialization() {
                if (GeneratedMessageV3.alwaysUseFieldBuilders) {
                    l();
                }
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0021  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /* renamed from: o */
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public com.google.protobuf.DescriptorProtos.SourceCodeInfo.b mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0<com.google.protobuf.DescriptorProtos$SourceCodeInfo> r1 = com.google.protobuf.DescriptorProtos.SourceCodeInfo.PARSER     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    com.google.protobuf.DescriptorProtos$SourceCodeInfo r3 = (com.google.protobuf.DescriptorProtos.SourceCodeInfo) r3     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    if (r3 == 0) goto Le
                    r2.p(r3)
                Le:
                    return r2
                Lf:
                    r3 = move-exception
                    goto L1f
                L11:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> Lf
                    com.google.protobuf.DescriptorProtos$SourceCodeInfo r4 = (com.google.protobuf.DescriptorProtos.SourceCodeInfo) r4     // Catch: java.lang.Throwable -> Lf
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1d
                    throw r3     // Catch: java.lang.Throwable -> L1d
                L1d:
                    r3 = move-exception
                    r0 = r4
                L1f:
                    if (r0 == 0) goto L24
                    r2.p(r0)
                L24:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.DescriptorProtos.SourceCodeInfo.b.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):com.google.protobuf.DescriptorProtos$SourceCodeInfo$b");
            }

            public b p(SourceCodeInfo sourceCodeInfo) {
                if (sourceCodeInfo == SourceCodeInfo.getDefaultInstance()) {
                    return this;
                }
                if (this.g0 == null) {
                    if (!sourceCodeInfo.location_.isEmpty()) {
                        if (this.f0.isEmpty()) {
                            this.f0 = sourceCodeInfo.location_;
                            this.a &= -2;
                        } else {
                            j();
                            this.f0.addAll(sourceCodeInfo.location_);
                        }
                        onChanged();
                    }
                } else if (!sourceCodeInfo.location_.isEmpty()) {
                    if (!this.g0.u()) {
                        this.g0.b(sourceCodeInfo.location_);
                    } else {
                        this.g0.i();
                        this.g0 = null;
                        this.f0 = sourceCodeInfo.location_;
                        this.a &= -2;
                        this.g0 = GeneratedMessageV3.alwaysUseFieldBuilders ? l() : null;
                    }
                }
                mergeUnknownFields(sourceCodeInfo.unknownFields);
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            /* renamed from: r */
            public b mergeFrom(l0 l0Var) {
                if (l0Var instanceof SourceCodeInfo) {
                    return p((SourceCodeInfo) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: s */
            public final b mergeUnknownFields(g1 g1Var) {
                return (b) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: t */
            public b setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (b) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: u */
            public b setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (b) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: v */
            public final b setUnknownFields(g1 g1Var) {
                return (b) super.setUnknownFields(g1Var);
            }

            public b() {
                this.f0 = Collections.emptyList();
                maybeForceBuilderInitialization();
            }

            public b(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.f0 = Collections.emptyList();
                maybeForceBuilderInitialization();
            }
        }

        public static b newBuilder(SourceCodeInfo sourceCodeInfo) {
            return a.toBuilder().p(sourceCodeInfo);
        }

        public static SourceCodeInfo parseFrom(ByteBuffer byteBuffer, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        public SourceCodeInfo(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SourceCodeInfo parseDelimitedFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
            return (SourceCodeInfo) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SourceCodeInfo parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SourceCodeInfo getDefaultInstanceForType() {
            return a;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public b toBuilder() {
            return this == a ? new b() : new b().p(this);
        }

        public static SourceCodeInfo parseFrom(ByteString byteString, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public b newBuilderForType() {
            return newBuilder();
        }

        public SourceCodeInfo() {
            this.memoizedIsInitialized = (byte) -1;
            this.location_ = Collections.emptyList();
        }

        public static SourceCodeInfo parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public b newBuilderForType(GeneratedMessageV3.c cVar) {
            return new b(cVar);
        }

        public static SourceCodeInfo parseFrom(byte[] bArr, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SourceCodeInfo parseFrom(InputStream inputStream) throws IOException {
            return (SourceCodeInfo) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        /* JADX WARN: Multi-variable type inference failed */
        public SourceCodeInfo(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            boolean z2 = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J != 10) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                if (!(z2 & true)) {
                                    this.location_ = new ArrayList();
                                    z2 |= true;
                                }
                                this.location_.add(jVar.z(Location.PARSER, rVar));
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    if (z2 & true) {
                        this.location_ = Collections.unmodifiableList(this.location_);
                    }
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static SourceCodeInfo parseFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
            return (SourceCodeInfo) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static SourceCodeInfo parseFrom(com.google.protobuf.j jVar) throws IOException {
            return (SourceCodeInfo) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static SourceCodeInfo parseFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws IOException {
            return (SourceCodeInfo) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes2.dex */
    public static final class UninterpretedOption extends GeneratedMessageV3 implements s {
        public static final int AGGREGATE_VALUE_FIELD_NUMBER = 8;
        public static final int DOUBLE_VALUE_FIELD_NUMBER = 6;
        public static final int IDENTIFIER_VALUE_FIELD_NUMBER = 3;
        public static final int NAME_FIELD_NUMBER = 2;
        public static final int NEGATIVE_INT_VALUE_FIELD_NUMBER = 5;
        public static final int POSITIVE_INT_VALUE_FIELD_NUMBER = 4;
        public static final int STRING_VALUE_FIELD_NUMBER = 7;
        private static final long serialVersionUID = 0;
        private volatile Object aggregateValue_;
        private int bitField0_;
        private double doubleValue_;
        private volatile Object identifierValue_;
        private byte memoizedIsInitialized;
        private List<NamePart> name_;
        private long negativeIntValue_;
        private long positiveIntValue_;
        private ByteString stringValue_;
        public static final UninterpretedOption a = new UninterpretedOption();
        @Deprecated
        public static final t0<UninterpretedOption> PARSER = new a();

        /* loaded from: classes2.dex */
        public static final class NamePart extends GeneratedMessageV3 implements c {
            public static final int IS_EXTENSION_FIELD_NUMBER = 2;
            public static final int NAME_PART_FIELD_NUMBER = 1;
            private static final long serialVersionUID = 0;
            private int bitField0_;
            private boolean isExtension_;
            private byte memoizedIsInitialized;
            private volatile Object namePart_;
            public static final NamePart a = new NamePart();
            @Deprecated
            public static final t0<NamePart> PARSER = new a();

            /* loaded from: classes2.dex */
            public static class a extends com.google.protobuf.c<NamePart> {
                @Override // com.google.protobuf.t0
                /* renamed from: a */
                public NamePart parsePartialFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
                    return new NamePart(jVar, rVar);
                }
            }

            public static NamePart getDefaultInstance() {
                return a;
            }

            public static final Descriptors.b getDescriptor() {
                return DescriptorProtos.R;
            }

            public static b newBuilder() {
                return a.toBuilder();
            }

            public static NamePart parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (NamePart) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
            }

            public static NamePart parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer);
            }

            public static t0<NamePart> parser() {
                return PARSER;
            }

            @Override // com.google.protobuf.a
            public boolean equals(Object obj) {
                if (obj == this) {
                    return true;
                }
                if (!(obj instanceof NamePart)) {
                    return super.equals(obj);
                }
                NamePart namePart = (NamePart) obj;
                if (hasNamePart() != namePart.hasNamePart()) {
                    return false;
                }
                if ((!hasNamePart() || getNamePart().equals(namePart.getNamePart())) && hasIsExtension() == namePart.hasIsExtension()) {
                    return (!hasIsExtension() || getIsExtension() == namePart.getIsExtension()) && this.unknownFields.equals(namePart.unknownFields);
                }
                return false;
            }

            public boolean getIsExtension() {
                return this.isExtension_;
            }

            public String getNamePart() {
                Object obj = this.namePart_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                ByteString byteString = (ByteString) obj;
                String stringUtf8 = byteString.toStringUtf8();
                if (byteString.isValidUtf8()) {
                    this.namePart_ = stringUtf8;
                }
                return stringUtf8;
            }

            public ByteString getNamePartBytes() {
                Object obj = this.namePart_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.namePart_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
            public t0<NamePart> getParserForType() {
                return PARSER;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public int getSerializedSize() {
                int i = this.memoizedSize;
                if (i != -1) {
                    return i;
                }
                int computeStringSize = (this.bitField0_ & 1) != 0 ? 0 + GeneratedMessageV3.computeStringSize(1, this.namePart_) : 0;
                if ((this.bitField0_ & 2) != 0) {
                    computeStringSize += CodedOutputStream.e(2, this.isExtension_);
                }
                int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
                this.memoizedSize = serializedSize;
                return serializedSize;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
            public final g1 getUnknownFields() {
                return this.unknownFields;
            }

            public boolean hasIsExtension() {
                return (this.bitField0_ & 2) != 0;
            }

            public boolean hasNamePart() {
                return (this.bitField0_ & 1) != 0;
            }

            @Override // com.google.protobuf.a
            public int hashCode() {
                int i = this.memoizedHashCode;
                if (i != 0) {
                    return i;
                }
                int hashCode = 779 + getDescriptor().hashCode();
                if (hasNamePart()) {
                    hashCode = (((hashCode * 37) + 1) * 53) + getNamePart().hashCode();
                }
                if (hasIsExtension()) {
                    hashCode = (((hashCode * 37) + 2) * 53) + a0.c(getIsExtension());
                }
                int hashCode2 = (hashCode * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode2;
                return hashCode2;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return DescriptorProtos.S.d(NamePart.class, b.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
            public final boolean isInitialized() {
                byte b2 = this.memoizedIsInitialized;
                if (b2 == 1) {
                    return true;
                }
                if (b2 == 0) {
                    return false;
                }
                if (!hasNamePart()) {
                    this.memoizedIsInitialized = (byte) 0;
                    return false;
                } else if (!hasIsExtension()) {
                    this.memoizedIsInitialized = (byte) 0;
                    return false;
                } else {
                    this.memoizedIsInitialized = (byte) 1;
                    return true;
                }
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Object newInstance(GeneratedMessageV3.f fVar) {
                return new NamePart();
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
                if ((this.bitField0_ & 1) != 0) {
                    GeneratedMessageV3.writeString(codedOutputStream, 1, this.namePart_);
                }
                if ((this.bitField0_ & 2) != 0) {
                    codedOutputStream.m0(2, this.isExtension_);
                }
                this.unknownFields.writeTo(codedOutputStream);
            }

            /* loaded from: classes2.dex */
            public static final class b extends GeneratedMessageV3.b<b> implements c {
                public int a;
                public Object f0;
                public boolean g0;

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                /* renamed from: a */
                public b addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (b) super.addRepeatedField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                /* renamed from: b */
                public NamePart build() {
                    NamePart buildPartial = buildPartial();
                    if (buildPartial.isInitialized()) {
                        return buildPartial;
                    }
                    throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                /* renamed from: d */
                public NamePart buildPartial() {
                    NamePart namePart = new NamePart(this);
                    int i = this.a;
                    int i2 = (i & 1) != 0 ? 1 : 0;
                    namePart.namePart_ = this.f0;
                    if ((i & 2) != 0) {
                        namePart.isExtension_ = this.g0;
                        i2 |= 2;
                    }
                    namePart.bitField0_ = i2;
                    onBuilt();
                    return namePart;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                /* renamed from: e */
                public b clear() {
                    super.clear();
                    this.f0 = "";
                    int i = this.a & (-2);
                    this.a = i;
                    this.g0 = false;
                    this.a = i & (-3);
                    return this;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                /* renamed from: f */
                public b clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                    return (b) super.clearField(fieldDescriptor);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                /* renamed from: g */
                public b clearOneof(Descriptors.g gVar) {
                    return (b) super.clearOneof(gVar);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
                public Descriptors.b getDescriptorForType() {
                    return DescriptorProtos.R;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
                /* renamed from: h */
                public b clone() {
                    return (b) super.clone();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                    return DescriptorProtos.S.d(NamePart.class, b.class);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
                public final boolean isInitialized() {
                    return l() && k();
                }

                @Override // defpackage.d82, com.google.protobuf.o0
                /* renamed from: j */
                public NamePart getDefaultInstanceForType() {
                    return NamePart.getDefaultInstance();
                }

                public boolean k() {
                    return (this.a & 2) != 0;
                }

                public boolean l() {
                    return (this.a & 1) != 0;
                }

                public final void maybeForceBuilderInitialization() {
                    boolean z = GeneratedMessageV3.alwaysUseFieldBuilders;
                }

                /* JADX WARN: Removed duplicated region for block: B:16:0x0021  */
                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
                /* renamed from: o */
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct code enable 'Show inconsistent code' option in preferences
                */
                public com.google.protobuf.DescriptorProtos.UninterpretedOption.NamePart.b mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                    /*
                        r2 = this;
                        r0 = 0
                        com.google.protobuf.t0<com.google.protobuf.DescriptorProtos$UninterpretedOption$NamePart> r1 = com.google.protobuf.DescriptorProtos.UninterpretedOption.NamePart.PARSER     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                        java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                        com.google.protobuf.DescriptorProtos$UninterpretedOption$NamePart r3 = (com.google.protobuf.DescriptorProtos.UninterpretedOption.NamePart) r3     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                        if (r3 == 0) goto Le
                        r2.p(r3)
                    Le:
                        return r2
                    Lf:
                        r3 = move-exception
                        goto L1f
                    L11:
                        r3 = move-exception
                        com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> Lf
                        com.google.protobuf.DescriptorProtos$UninterpretedOption$NamePart r4 = (com.google.protobuf.DescriptorProtos.UninterpretedOption.NamePart) r4     // Catch: java.lang.Throwable -> Lf
                        java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1d
                        throw r3     // Catch: java.lang.Throwable -> L1d
                    L1d:
                        r3 = move-exception
                        r0 = r4
                    L1f:
                        if (r0 == 0) goto L24
                        r2.p(r0)
                    L24:
                        throw r3
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.DescriptorProtos.UninterpretedOption.NamePart.b.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):com.google.protobuf.DescriptorProtos$UninterpretedOption$NamePart$b");
                }

                public b p(NamePart namePart) {
                    if (namePart == NamePart.getDefaultInstance()) {
                        return this;
                    }
                    if (namePart.hasNamePart()) {
                        this.a |= 1;
                        this.f0 = namePart.namePart_;
                        onChanged();
                    }
                    if (namePart.hasIsExtension()) {
                        u(namePart.getIsExtension());
                    }
                    mergeUnknownFields(namePart.unknownFields);
                    onChanged();
                    return this;
                }

                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
                /* renamed from: r */
                public b mergeFrom(l0 l0Var) {
                    if (l0Var instanceof NamePart) {
                        return p((NamePart) l0Var);
                    }
                    super.mergeFrom(l0Var);
                    return this;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                /* renamed from: s */
                public final b mergeUnknownFields(g1 g1Var) {
                    return (b) super.mergeUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                /* renamed from: t */
                public b setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (b) super.setField(fieldDescriptor, obj);
                }

                public b u(boolean z) {
                    this.a |= 2;
                    this.g0 = z;
                    onChanged();
                    return this;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                /* renamed from: v */
                public b setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                    return (b) super.m19setRepeatedField(fieldDescriptor, i, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                /* renamed from: x */
                public final b setUnknownFields(g1 g1Var) {
                    return (b) super.setUnknownFields(g1Var);
                }

                public b() {
                    this.f0 = "";
                    maybeForceBuilderInitialization();
                }

                public b(GeneratedMessageV3.c cVar) {
                    super(cVar);
                    this.f0 = "";
                    maybeForceBuilderInitialization();
                }
            }

            public static b newBuilder(NamePart namePart) {
                return a.toBuilder().p(namePart);
            }

            public static NamePart parseFrom(ByteBuffer byteBuffer, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer, rVar);
            }

            public NamePart(GeneratedMessageV3.b<?> bVar) {
                super(bVar);
                this.memoizedIsInitialized = (byte) -1;
            }

            public static NamePart parseDelimitedFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
                return (NamePart) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
            }

            public static NamePart parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
            public NamePart getDefaultInstanceForType() {
                return a;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public b toBuilder() {
                return this == a ? new b() : new b().p(this);
            }

            public static NamePart parseFrom(ByteString byteString, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString, rVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public b newBuilderForType() {
                return newBuilder();
            }

            public NamePart() {
                this.memoizedIsInitialized = (byte) -1;
                this.namePart_ = "";
            }

            public static NamePart parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr);
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public b newBuilderForType(GeneratedMessageV3.c cVar) {
                return new b(cVar);
            }

            public static NamePart parseFrom(byte[] bArr, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr, rVar);
            }

            public static NamePart parseFrom(InputStream inputStream) throws IOException {
                return (NamePart) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
            }

            public NamePart(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
                this();
                Objects.requireNonNull(rVar);
                g1.b g = g1.g();
                boolean z = false;
                while (!z) {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    ByteString q = jVar.q();
                                    this.bitField0_ = 1 | this.bitField0_;
                                    this.namePart_ = q;
                                } else if (J != 16) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.bitField0_ |= 2;
                                    this.isExtension_ = jVar.p();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        } catch (IOException e2) {
                            throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                        }
                    } finally {
                        this.unknownFields = g.build();
                        makeExtensionsImmutable();
                    }
                }
            }

            public static NamePart parseFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
                return (NamePart) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
            }

            public static NamePart parseFrom(com.google.protobuf.j jVar) throws IOException {
                return (NamePart) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
            }

            public static NamePart parseFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws IOException {
                return (NamePart) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
            }
        }

        /* loaded from: classes2.dex */
        public static class a extends com.google.protobuf.c<UninterpretedOption> {
            @Override // com.google.protobuf.t0
            /* renamed from: a */
            public UninterpretedOption parsePartialFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
                return new UninterpretedOption(jVar, rVar);
            }
        }

        /* loaded from: classes2.dex */
        public interface c extends o0 {
        }

        public static UninterpretedOption getDefaultInstance() {
            return a;
        }

        public static final Descriptors.b getDescriptor() {
            return DescriptorProtos.P;
        }

        public static b newBuilder() {
            return a.toBuilder();
        }

        public static UninterpretedOption parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (UninterpretedOption) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static UninterpretedOption parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<UninterpretedOption> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof UninterpretedOption)) {
                return super.equals(obj);
            }
            UninterpretedOption uninterpretedOption = (UninterpretedOption) obj;
            if (getNameList().equals(uninterpretedOption.getNameList()) && hasIdentifierValue() == uninterpretedOption.hasIdentifierValue()) {
                if ((!hasIdentifierValue() || getIdentifierValue().equals(uninterpretedOption.getIdentifierValue())) && hasPositiveIntValue() == uninterpretedOption.hasPositiveIntValue()) {
                    if ((!hasPositiveIntValue() || getPositiveIntValue() == uninterpretedOption.getPositiveIntValue()) && hasNegativeIntValue() == uninterpretedOption.hasNegativeIntValue()) {
                        if ((!hasNegativeIntValue() || getNegativeIntValue() == uninterpretedOption.getNegativeIntValue()) && hasDoubleValue() == uninterpretedOption.hasDoubleValue()) {
                            if ((!hasDoubleValue() || Double.doubleToLongBits(getDoubleValue()) == Double.doubleToLongBits(uninterpretedOption.getDoubleValue())) && hasStringValue() == uninterpretedOption.hasStringValue()) {
                                if ((!hasStringValue() || getStringValue().equals(uninterpretedOption.getStringValue())) && hasAggregateValue() == uninterpretedOption.hasAggregateValue()) {
                                    return (!hasAggregateValue() || getAggregateValue().equals(uninterpretedOption.getAggregateValue())) && this.unknownFields.equals(uninterpretedOption.unknownFields);
                                }
                                return false;
                            }
                            return false;
                        }
                        return false;
                    }
                    return false;
                }
                return false;
            }
            return false;
        }

        public String getAggregateValue() {
            Object obj = this.aggregateValue_;
            if (obj instanceof String) {
                return (String) obj;
            }
            ByteString byteString = (ByteString) obj;
            String stringUtf8 = byteString.toStringUtf8();
            if (byteString.isValidUtf8()) {
                this.aggregateValue_ = stringUtf8;
            }
            return stringUtf8;
        }

        public ByteString getAggregateValueBytes() {
            Object obj = this.aggregateValue_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.aggregateValue_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        public double getDoubleValue() {
            return this.doubleValue_;
        }

        public String getIdentifierValue() {
            Object obj = this.identifierValue_;
            if (obj instanceof String) {
                return (String) obj;
            }
            ByteString byteString = (ByteString) obj;
            String stringUtf8 = byteString.toStringUtf8();
            if (byteString.isValidUtf8()) {
                this.identifierValue_ = stringUtf8;
            }
            return stringUtf8;
        }

        public ByteString getIdentifierValueBytes() {
            Object obj = this.identifierValue_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.identifierValue_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        public NamePart getName(int i) {
            return this.name_.get(i);
        }

        public int getNameCount() {
            return this.name_.size();
        }

        public List<NamePart> getNameList() {
            return this.name_;
        }

        public c getNameOrBuilder(int i) {
            return this.name_.get(i);
        }

        public List<? extends c> getNameOrBuilderList() {
            return this.name_;
        }

        public long getNegativeIntValue() {
            return this.negativeIntValue_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<UninterpretedOption> getParserForType() {
            return PARSER;
        }

        public long getPositiveIntValue() {
            return this.positiveIntValue_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            for (int i3 = 0; i3 < this.name_.size(); i3++) {
                i2 += CodedOutputStream.G(2, this.name_.get(i3));
            }
            if ((this.bitField0_ & 1) != 0) {
                i2 += GeneratedMessageV3.computeStringSize(3, this.identifierValue_);
            }
            if ((this.bitField0_ & 2) != 0) {
                i2 += CodedOutputStream.a0(4, this.positiveIntValue_);
            }
            if ((this.bitField0_ & 4) != 0) {
                i2 += CodedOutputStream.z(5, this.negativeIntValue_);
            }
            if ((this.bitField0_ & 8) != 0) {
                i2 += CodedOutputStream.j(6, this.doubleValue_);
            }
            if ((this.bitField0_ & 16) != 0) {
                i2 += CodedOutputStream.h(7, this.stringValue_);
            }
            if ((this.bitField0_ & 32) != 0) {
                i2 += GeneratedMessageV3.computeStringSize(8, this.aggregateValue_);
            }
            int serializedSize = i2 + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        public ByteString getStringValue() {
            return this.stringValue_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        public boolean hasAggregateValue() {
            return (this.bitField0_ & 32) != 0;
        }

        public boolean hasDoubleValue() {
            return (this.bitField0_ & 8) != 0;
        }

        public boolean hasIdentifierValue() {
            return (this.bitField0_ & 1) != 0;
        }

        public boolean hasNegativeIntValue() {
            return (this.bitField0_ & 4) != 0;
        }

        public boolean hasPositiveIntValue() {
            return (this.bitField0_ & 2) != 0;
        }

        public boolean hasStringValue() {
            return (this.bitField0_ & 16) != 0;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = 779 + getDescriptor().hashCode();
            if (getNameCount() > 0) {
                hashCode = (((hashCode * 37) + 2) * 53) + getNameList().hashCode();
            }
            if (hasIdentifierValue()) {
                hashCode = (((hashCode * 37) + 3) * 53) + getIdentifierValue().hashCode();
            }
            if (hasPositiveIntValue()) {
                hashCode = (((hashCode * 37) + 4) * 53) + a0.h(getPositiveIntValue());
            }
            if (hasNegativeIntValue()) {
                hashCode = (((hashCode * 37) + 5) * 53) + a0.h(getNegativeIntValue());
            }
            if (hasDoubleValue()) {
                hashCode = (((hashCode * 37) + 6) * 53) + a0.h(Double.doubleToLongBits(getDoubleValue()));
            }
            if (hasStringValue()) {
                hashCode = (((hashCode * 37) + 7) * 53) + getStringValue().hashCode();
            }
            if (hasAggregateValue()) {
                hashCode = (((hashCode * 37) + 8) * 53) + getAggregateValue().hashCode();
            }
            int hashCode2 = (hashCode * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode2;
            return hashCode2;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return DescriptorProtos.Q.d(UninterpretedOption.class, b.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b2 = this.memoizedIsInitialized;
            if (b2 == 1) {
                return true;
            }
            if (b2 == 0) {
                return false;
            }
            for (int i = 0; i < getNameCount(); i++) {
                if (!getName(i).isInitialized()) {
                    this.memoizedIsInitialized = (byte) 0;
                    return false;
                }
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new UninterpretedOption();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            for (int i = 0; i < this.name_.size(); i++) {
                codedOutputStream.K0(2, this.name_.get(i));
            }
            if ((this.bitField0_ & 1) != 0) {
                GeneratedMessageV3.writeString(codedOutputStream, 3, this.identifierValue_);
            }
            if ((this.bitField0_ & 2) != 0) {
                codedOutputStream.d1(4, this.positiveIntValue_);
            }
            if ((this.bitField0_ & 4) != 0) {
                codedOutputStream.I0(5, this.negativeIntValue_);
            }
            if ((this.bitField0_ & 8) != 0) {
                codedOutputStream.s0(6, this.doubleValue_);
            }
            if ((this.bitField0_ & 16) != 0) {
                codedOutputStream.q0(7, this.stringValue_);
            }
            if ((this.bitField0_ & 32) != 0) {
                GeneratedMessageV3.writeString(codedOutputStream, 8, this.aggregateValue_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes2.dex */
        public static final class b extends GeneratedMessageV3.b<b> implements s {
            public int a;
            public List<NamePart> f0;
            public x0<NamePart, NamePart.b, c> g0;
            public Object h0;
            public long i0;
            public long j0;
            public double k0;
            public ByteString l0;
            public Object m0;

            public b A(long j) {
                this.a |= 4;
                this.i0 = j;
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: B */
            public b setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (b) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            public b C(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.a |= 32;
                this.l0 = byteString;
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: D */
            public final b setUnknownFields(g1 g1Var) {
                return (b) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: a */
            public b addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (b) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            /* renamed from: b */
            public UninterpretedOption build() {
                UninterpretedOption buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            /* renamed from: d */
            public UninterpretedOption buildPartial() {
                UninterpretedOption uninterpretedOption = new UninterpretedOption(this);
                int i = this.a;
                x0<NamePart, NamePart.b, c> x0Var = this.g0;
                if (x0Var == null) {
                    if ((i & 1) != 0) {
                        this.f0 = Collections.unmodifiableList(this.f0);
                        this.a &= -2;
                    }
                    uninterpretedOption.name_ = this.f0;
                } else {
                    uninterpretedOption.name_ = x0Var.g();
                }
                int i2 = (i & 2) != 0 ? 1 : 0;
                uninterpretedOption.identifierValue_ = this.h0;
                if ((i & 4) != 0) {
                    uninterpretedOption.positiveIntValue_ = this.i0;
                    i2 |= 2;
                }
                if ((i & 8) != 0) {
                    uninterpretedOption.negativeIntValue_ = this.j0;
                    i2 |= 4;
                }
                if ((i & 16) != 0) {
                    uninterpretedOption.doubleValue_ = this.k0;
                    i2 |= 8;
                }
                if ((i & 32) != 0) {
                    i2 |= 16;
                }
                uninterpretedOption.stringValue_ = this.l0;
                if ((i & 64) != 0) {
                    i2 |= 32;
                }
                uninterpretedOption.aggregateValue_ = this.m0;
                uninterpretedOption.bitField0_ = i2;
                onBuilt();
                return uninterpretedOption;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: e */
            public b clear() {
                super.clear();
                x0<NamePart, NamePart.b, c> x0Var = this.g0;
                if (x0Var == null) {
                    this.f0 = Collections.emptyList();
                    this.a &= -2;
                } else {
                    x0Var.h();
                }
                this.h0 = "";
                int i = this.a & (-3);
                this.a = i;
                this.i0 = 0L;
                int i2 = i & (-5);
                this.a = i2;
                this.j0 = 0L;
                int i3 = i2 & (-9);
                this.a = i3;
                this.k0 = Utils.DOUBLE_EPSILON;
                int i4 = i3 & (-17);
                this.a = i4;
                this.l0 = ByteString.EMPTY;
                int i5 = i4 & (-33);
                this.a = i5;
                this.m0 = "";
                this.a = i5 & (-65);
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: f */
            public b clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (b) super.clearField(fieldDescriptor);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: g */
            public b clearOneof(Descriptors.g gVar) {
                return (b) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return DescriptorProtos.P;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            /* renamed from: h */
            public b clone() {
                return (b) super.clone();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return DescriptorProtos.Q.d(UninterpretedOption.class, b.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                for (int i = 0; i < o(); i++) {
                    if (!l(i).isInitialized()) {
                        return false;
                    }
                }
                return true;
            }

            public final void j() {
                if ((this.a & 1) == 0) {
                    this.f0 = new ArrayList(this.f0);
                    this.a |= 1;
                }
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            /* renamed from: k */
            public UninterpretedOption getDefaultInstanceForType() {
                return UninterpretedOption.getDefaultInstance();
            }

            public NamePart l(int i) {
                x0<NamePart, NamePart.b, c> x0Var = this.g0;
                if (x0Var == null) {
                    return this.f0.get(i);
                }
                return x0Var.o(i);
            }

            public final void maybeForceBuilderInitialization() {
                if (GeneratedMessageV3.alwaysUseFieldBuilders) {
                    p();
                }
            }

            public int o() {
                x0<NamePart, NamePart.b, c> x0Var = this.g0;
                if (x0Var == null) {
                    return this.f0.size();
                }
                return x0Var.n();
            }

            public final x0<NamePart, NamePart.b, c> p() {
                if (this.g0 == null) {
                    this.g0 = new x0<>(this.f0, (this.a & 1) != 0, getParentForChildren(), isClean());
                    this.f0 = null;
                }
                return this.g0;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0021  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /* renamed from: r */
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public com.google.protobuf.DescriptorProtos.UninterpretedOption.b mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0<com.google.protobuf.DescriptorProtos$UninterpretedOption> r1 = com.google.protobuf.DescriptorProtos.UninterpretedOption.PARSER     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    com.google.protobuf.DescriptorProtos$UninterpretedOption r3 = (com.google.protobuf.DescriptorProtos.UninterpretedOption) r3     // Catch: java.lang.Throwable -> Lf com.google.protobuf.InvalidProtocolBufferException -> L11
                    if (r3 == 0) goto Le
                    r2.s(r3)
                Le:
                    return r2
                Lf:
                    r3 = move-exception
                    goto L1f
                L11:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> Lf
                    com.google.protobuf.DescriptorProtos$UninterpretedOption r4 = (com.google.protobuf.DescriptorProtos.UninterpretedOption) r4     // Catch: java.lang.Throwable -> Lf
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1d
                    throw r3     // Catch: java.lang.Throwable -> L1d
                L1d:
                    r3 = move-exception
                    r0 = r4
                L1f:
                    if (r0 == 0) goto L24
                    r2.s(r0)
                L24:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.DescriptorProtos.UninterpretedOption.b.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):com.google.protobuf.DescriptorProtos$UninterpretedOption$b");
            }

            public b s(UninterpretedOption uninterpretedOption) {
                if (uninterpretedOption == UninterpretedOption.getDefaultInstance()) {
                    return this;
                }
                if (this.g0 == null) {
                    if (!uninterpretedOption.name_.isEmpty()) {
                        if (this.f0.isEmpty()) {
                            this.f0 = uninterpretedOption.name_;
                            this.a &= -2;
                        } else {
                            j();
                            this.f0.addAll(uninterpretedOption.name_);
                        }
                        onChanged();
                    }
                } else if (!uninterpretedOption.name_.isEmpty()) {
                    if (!this.g0.u()) {
                        this.g0.b(uninterpretedOption.name_);
                    } else {
                        this.g0.i();
                        this.g0 = null;
                        this.f0 = uninterpretedOption.name_;
                        this.a &= -2;
                        this.g0 = GeneratedMessageV3.alwaysUseFieldBuilders ? p() : null;
                    }
                }
                if (uninterpretedOption.hasIdentifierValue()) {
                    this.a |= 2;
                    this.h0 = uninterpretedOption.identifierValue_;
                    onChanged();
                }
                if (uninterpretedOption.hasPositiveIntValue()) {
                    A(uninterpretedOption.getPositiveIntValue());
                }
                if (uninterpretedOption.hasNegativeIntValue()) {
                    y(uninterpretedOption.getNegativeIntValue());
                }
                if (uninterpretedOption.hasDoubleValue()) {
                    v(uninterpretedOption.getDoubleValue());
                }
                if (uninterpretedOption.hasStringValue()) {
                    C(uninterpretedOption.getStringValue());
                }
                if (uninterpretedOption.hasAggregateValue()) {
                    this.a |= 64;
                    this.m0 = uninterpretedOption.aggregateValue_;
                    onChanged();
                }
                mergeUnknownFields(uninterpretedOption.unknownFields);
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            /* renamed from: t */
            public b mergeFrom(l0 l0Var) {
                if (l0Var instanceof UninterpretedOption) {
                    return s((UninterpretedOption) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            /* renamed from: u */
            public final b mergeUnknownFields(g1 g1Var) {
                return (b) super.mergeUnknownFields(g1Var);
            }

            public b v(double d) {
                this.a |= 16;
                this.k0 = d;
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            /* renamed from: x */
            public b setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (b) super.setField(fieldDescriptor, obj);
            }

            public b y(long j) {
                this.a |= 8;
                this.j0 = j;
                onChanged();
                return this;
            }

            public b() {
                this.f0 = Collections.emptyList();
                this.h0 = "";
                this.l0 = ByteString.EMPTY;
                this.m0 = "";
                maybeForceBuilderInitialization();
            }

            public b(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.f0 = Collections.emptyList();
                this.h0 = "";
                this.l0 = ByteString.EMPTY;
                this.m0 = "";
                maybeForceBuilderInitialization();
            }
        }

        public static b newBuilder(UninterpretedOption uninterpretedOption) {
            return a.toBuilder().s(uninterpretedOption);
        }

        public static UninterpretedOption parseFrom(ByteBuffer byteBuffer, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        public UninterpretedOption(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static UninterpretedOption parseDelimitedFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
            return (UninterpretedOption) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static UninterpretedOption parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public UninterpretedOption getDefaultInstanceForType() {
            return a;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public b toBuilder() {
            return this == a ? new b() : new b().s(this);
        }

        public static UninterpretedOption parseFrom(ByteString byteString, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public b newBuilderForType() {
            return newBuilder();
        }

        public UninterpretedOption() {
            this.memoizedIsInitialized = (byte) -1;
            this.name_ = Collections.emptyList();
            this.identifierValue_ = "";
            this.stringValue_ = ByteString.EMPTY;
            this.aggregateValue_ = "";
        }

        public static UninterpretedOption parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public b newBuilderForType(GeneratedMessageV3.c cVar) {
            return new b(cVar);
        }

        public static UninterpretedOption parseFrom(byte[] bArr, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static UninterpretedOption parseFrom(InputStream inputStream) throws IOException {
            return (UninterpretedOption) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static UninterpretedOption parseFrom(InputStream inputStream, com.google.protobuf.r rVar) throws IOException {
            return (UninterpretedOption) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        /* JADX WARN: Multi-variable type inference failed */
        public UninterpretedOption(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            boolean z2 = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 18) {
                                    if (!(z2 & true)) {
                                        this.name_ = new ArrayList();
                                        z2 |= true;
                                    }
                                    this.name_.add(jVar.z(NamePart.PARSER, rVar));
                                } else if (J == 26) {
                                    ByteString q = jVar.q();
                                    this.bitField0_ |= 1;
                                    this.identifierValue_ = q;
                                } else if (J == 32) {
                                    this.bitField0_ |= 2;
                                    this.positiveIntValue_ = jVar.L();
                                } else if (J == 40) {
                                    this.bitField0_ |= 4;
                                    this.negativeIntValue_ = jVar.y();
                                } else if (J == 49) {
                                    this.bitField0_ |= 8;
                                    this.doubleValue_ = jVar.r();
                                } else if (J == 58) {
                                    this.bitField0_ |= 16;
                                    this.stringValue_ = jVar.q();
                                } else if (J != 66) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    ByteString q2 = jVar.q();
                                    this.bitField0_ = 32 | this.bitField0_;
                                    this.aggregateValue_ = q2;
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        }
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    if (z2 & true) {
                        this.name_ = Collections.unmodifiableList(this.name_);
                    }
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static UninterpretedOption parseFrom(com.google.protobuf.j jVar) throws IOException {
            return (UninterpretedOption) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static UninterpretedOption parseFrom(com.google.protobuf.j jVar, com.google.protobuf.r rVar) throws IOException {
            return (UninterpretedOption) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes2.dex */
    public interface b extends o0 {
    }

    /* loaded from: classes2.dex */
    public interface c extends o0 {
    }

    /* loaded from: classes2.dex */
    public interface d extends o0 {
    }

    /* loaded from: classes2.dex */
    public interface e extends o0 {
    }

    /* loaded from: classes2.dex */
    public interface f extends o0 {
    }

    /* loaded from: classes2.dex */
    public interface g extends o0 {
    }

    /* loaded from: classes2.dex */
    public interface h extends o0 {
    }

    /* loaded from: classes2.dex */
    public interface i extends o0 {
    }

    /* loaded from: classes2.dex */
    public interface j extends o0 {
    }

    /* loaded from: classes2.dex */
    public interface k extends o0 {
    }

    /* loaded from: classes2.dex */
    public interface l extends o0 {
    }

    /* loaded from: classes2.dex */
    public interface m extends o0 {
    }

    /* loaded from: classes2.dex */
    public interface n extends o0 {
    }

    /* loaded from: classes2.dex */
    public interface o extends o0 {
    }

    /* loaded from: classes2.dex */
    public interface p extends o0 {
    }

    /* loaded from: classes2.dex */
    public interface q extends o0 {
    }

    /* loaded from: classes2.dex */
    public interface r extends o0 {
    }

    /* loaded from: classes2.dex */
    public interface s extends o0 {
    }

    static {
        Descriptors.b bVar = W().o().get(0);
        a = bVar;
        new GeneratedMessageV3.e(bVar, new String[]{"File"});
        Descriptors.b bVar2 = W().o().get(1);
        b = bVar2;
        c = new GeneratedMessageV3.e(bVar2, new String[]{"Name", "Package", "Dependency", "PublicDependency", "WeakDependency", "MessageType", "EnumType", "Service", "Extension", "Options", "SourceCodeInfo", "Syntax"});
        Descriptors.b bVar3 = W().o().get(2);
        d = bVar3;
        e = new GeneratedMessageV3.e(bVar3, new String[]{"Name", "Field", "Extension", "NestedType", "EnumType", "ExtensionRange", "OneofDecl", "Options", "ReservedRange", "ReservedName"});
        Descriptors.b bVar4 = bVar3.r().get(0);
        f = bVar4;
        g = new GeneratedMessageV3.e(bVar4, new String[]{"Start", "End", "Options"});
        Descriptors.b bVar5 = bVar3.r().get(1);
        h = bVar5;
        i = new GeneratedMessageV3.e(bVar5, new String[]{"Start", "End"});
        Descriptors.b bVar6 = W().o().get(3);
        j = bVar6;
        k = new GeneratedMessageV3.e(bVar6, new String[]{"UninterpretedOption"});
        Descriptors.b bVar7 = W().o().get(4);
        l = bVar7;
        m = new GeneratedMessageV3.e(bVar7, new String[]{"Name", "Number", "Label", "Type", "TypeName", "Extendee", "DefaultValue", "OneofIndex", "JsonName", "Options", "Proto3Optional"});
        Descriptors.b bVar8 = W().o().get(5);
        n = bVar8;
        o = new GeneratedMessageV3.e(bVar8, new String[]{"Name", "Options"});
        Descriptors.b bVar9 = W().o().get(6);
        p = bVar9;
        q = new GeneratedMessageV3.e(bVar9, new String[]{"Name", "Value", "Options", "ReservedRange", "ReservedName"});
        Descriptors.b bVar10 = bVar9.r().get(0);
        r = bVar10;
        s = new GeneratedMessageV3.e(bVar10, new String[]{"Start", "End"});
        Descriptors.b bVar11 = W().o().get(7);
        t = bVar11;
        u = new GeneratedMessageV3.e(bVar11, new String[]{"Name", "Number", "Options"});
        Descriptors.b bVar12 = W().o().get(8);
        v = bVar12;
        w = new GeneratedMessageV3.e(bVar12, new String[]{"Name", "Method", "Options"});
        Descriptors.b bVar13 = W().o().get(9);
        x = bVar13;
        y = new GeneratedMessageV3.e(bVar13, new String[]{"Name", "InputType", "OutputType", "Options", "ClientStreaming", "ServerStreaming"});
        Descriptors.b bVar14 = W().o().get(10);
        z = bVar14;
        A = new GeneratedMessageV3.e(bVar14, new String[]{"JavaPackage", "JavaOuterClassname", "JavaMultipleFiles", "JavaGenerateEqualsAndHash", "JavaStringCheckUtf8", "OptimizeFor", "GoPackage", "CcGenericServices", "JavaGenericServices", "PyGenericServices", "PhpGenericServices", "Deprecated", "CcEnableArenas", "ObjcClassPrefix", "CsharpNamespace", "SwiftPrefix", "PhpClassPrefix", "PhpNamespace", "PhpMetadataNamespace", "RubyPackage", "UninterpretedOption"});
        Descriptors.b bVar15 = W().o().get(11);
        B = bVar15;
        C = new GeneratedMessageV3.e(bVar15, new String[]{"MessageSetWireFormat", "NoStandardDescriptorAccessor", "Deprecated", "MapEntry", "UninterpretedOption"});
        Descriptors.b bVar16 = W().o().get(12);
        D = bVar16;
        E = new GeneratedMessageV3.e(bVar16, new String[]{"Ctype", "Packed", "Jstype", "Lazy", "Deprecated", "Weak", "UninterpretedOption"});
        Descriptors.b bVar17 = W().o().get(13);
        F = bVar17;
        G = new GeneratedMessageV3.e(bVar17, new String[]{"UninterpretedOption"});
        Descriptors.b bVar18 = W().o().get(14);
        H = bVar18;
        I = new GeneratedMessageV3.e(bVar18, new String[]{"AllowAlias", "Deprecated", "UninterpretedOption"});
        Descriptors.b bVar19 = W().o().get(15);
        J = bVar19;
        K = new GeneratedMessageV3.e(bVar19, new String[]{"Deprecated", "UninterpretedOption"});
        Descriptors.b bVar20 = W().o().get(16);
        L = bVar20;
        M = new GeneratedMessageV3.e(bVar20, new String[]{"Deprecated", "UninterpretedOption"});
        Descriptors.b bVar21 = W().o().get(17);
        N = bVar21;
        O = new GeneratedMessageV3.e(bVar21, new String[]{"Deprecated", "IdempotencyLevel", "UninterpretedOption"});
        Descriptors.b bVar22 = W().o().get(18);
        P = bVar22;
        Q = new GeneratedMessageV3.e(bVar22, new String[]{"Name", "IdentifierValue", "PositiveIntValue", "NegativeIntValue", "DoubleValue", "StringValue", "AggregateValue"});
        Descriptors.b bVar23 = bVar22.r().get(0);
        R = bVar23;
        S = new GeneratedMessageV3.e(bVar23, new String[]{"NamePart", "IsExtension"});
        Descriptors.b bVar24 = W().o().get(19);
        T = bVar24;
        U = new GeneratedMessageV3.e(bVar24, new String[]{"Location"});
        Descriptors.b bVar25 = bVar24.r().get(0);
        V = bVar25;
        W = new GeneratedMessageV3.e(bVar25, new String[]{"Path", "Span", "LeadingComments", "TrailingComments", "LeadingDetachedComments"});
        Descriptors.b bVar26 = W().o().get(20);
        X = bVar26;
        new GeneratedMessageV3.e(bVar26, new String[]{"Annotation"});
        Descriptors.b bVar27 = bVar26.r().get(0);
        Y = bVar27;
        new GeneratedMessageV3.e(bVar27, new String[]{"Path", "SourceFile", "Begin", "End"});
    }

    public static Descriptors.FileDescriptor W() {
        return Z;
    }
}
