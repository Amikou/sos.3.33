package com.google.protobuf;

import com.google.protobuf.a0;
import com.google.protobuf.w;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.List;
import java.util.RandomAccess;

/* compiled from: SchemaUtil.java */
/* loaded from: classes2.dex */
public final class z0 {
    public static final Class<?> a = B();
    public static final f1<?, ?> b = D(false);
    public static final f1<?, ?> c = D(true);
    public static final f1<?, ?> d = new i1();

    public static <UT, UB> UB A(int i, List<Integer> list, a0.e eVar, UB ub, f1<UT, UB> f1Var) {
        if (eVar == null) {
            return ub;
        }
        if (list instanceof RandomAccess) {
            int size = list.size();
            int i2 = 0;
            for (int i3 = 0; i3 < size; i3++) {
                int intValue = list.get(i3).intValue();
                if (eVar.a(intValue)) {
                    if (i3 != i2) {
                        list.set(i2, Integer.valueOf(intValue));
                    }
                    i2++;
                } else {
                    ub = (UB) M(i, intValue, ub, f1Var);
                }
            }
            if (i2 != size) {
                list.subList(i2, size).clear();
            }
        } else {
            Iterator<Integer> it = list.iterator();
            while (it.hasNext()) {
                int intValue2 = it.next().intValue();
                if (!eVar.a(intValue2)) {
                    ub = (UB) M(i, intValue2, ub, f1Var);
                    it.remove();
                }
            }
        }
        return ub;
    }

    public static Class<?> B() {
        try {
            return Class.forName("com.google.protobuf.GeneratedMessageV3");
        } catch (Throwable unused) {
            return null;
        }
    }

    public static Object C(Class<?> cls, String str) {
        try {
            Field[] declaredFields = Class.forName(cls.getName() + "$" + N(str, true) + "DefaultEntryHolder").getDeclaredFields();
            if (declaredFields.length == 1) {
                return k1.F(declaredFields[0]);
            }
            throw new IllegalStateException("Unable to look up map field default entry holder class for " + str + " in " + cls.getName());
        } catch (Throwable th) {
            throw new RuntimeException(th);
        }
    }

    public static f1<?, ?> D(boolean z) {
        try {
            Class<?> E = E();
            if (E == null) {
                return null;
            }
            return (f1) E.getConstructor(Boolean.TYPE).newInstance(Boolean.valueOf(z));
        } catch (Throwable unused) {
            return null;
        }
    }

    public static Class<?> E() {
        return j1.class;
    }

    public static <T, FT extends w.c<FT>> void F(s<FT> sVar, T t, T t2) {
        w<FT> c2 = sVar.c(t2);
        if (c2.B()) {
            return;
        }
        sVar.d(t).I(c2);
    }

    public static <T> void G(i0 i0Var, T t, T t2, long j) {
        k1.U(t, j, i0Var.a(k1.E(t, j), k1.E(t2, j)));
    }

    public static <T, UT, UB> void H(f1<UT, UB> f1Var, T t, T t2) {
        f1Var.p(t, f1Var.k(f1Var.g(t), f1Var.g(t2)));
    }

    public static f1<?, ?> I() {
        return b;
    }

    public static f1<?, ?> J() {
        return c;
    }

    public static void K(Class<?> cls) {
        Class<?> cls2;
        if (!GeneratedMessageLite.class.isAssignableFrom(cls) && (cls2 = a) != null && !cls2.isAssignableFrom(cls)) {
            throw new IllegalArgumentException("Message classes must extend GeneratedMessage or GeneratedMessageLite");
        }
    }

    public static boolean L(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    public static <UT, UB> UB M(int i, int i2, UB ub, f1<UT, UB> f1Var) {
        if (ub == null) {
            ub = f1Var.n();
        }
        f1Var.e(ub, i, i2);
        return ub;
    }

    public static String N(String str, boolean z) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if ('a' > charAt || charAt > 'z') {
                if ('A' > charAt || charAt > 'Z') {
                    if ('0' <= charAt && charAt <= '9') {
                        sb.append(charAt);
                    }
                    z = true;
                } else if (i == 0 && !z) {
                    sb.append((char) (charAt + ' '));
                } else {
                    sb.append(charAt);
                }
            } else if (z) {
                sb.append((char) (charAt - ' '));
            } else {
                sb.append(charAt);
            }
            z = false;
        }
        return sb.toString();
    }

    public static f1<?, ?> O() {
        return d;
    }

    public static void P(int i, List<Boolean> list, Writer writer, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        writer.w(i, list, z);
    }

    public static void Q(int i, List<ByteString> list, Writer writer) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        writer.I(i, list);
    }

    public static void R(int i, List<Double> list, Writer writer, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        writer.G(i, list, z);
    }

    public static void S(int i, List<Integer> list, Writer writer, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        writer.F(i, list, z);
    }

    public static void T(int i, List<Integer> list, Writer writer, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        writer.v(i, list, z);
    }

    public static void U(int i, List<Long> list, Writer writer, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        writer.s(i, list, z);
    }

    public static void V(int i, List<Float> list, Writer writer, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        writer.a(i, list, z);
    }

    public static void W(int i, List<?> list, Writer writer) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        writer.M(i, list);
    }

    public static void X(int i, List<?> list, Writer writer, y0 y0Var) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        writer.K(i, list, y0Var);
    }

    public static void Y(int i, List<Integer> list, Writer writer, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        writer.m(i, list, z);
    }

    public static void Z(int i, List<Long> list, Writer writer, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        writer.E(i, list, z);
    }

    public static int a(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        if (z) {
            return CodedOutputStream.X(i) + CodedOutputStream.E(size);
        }
        return size * CodedOutputStream.e(i, true);
    }

    public static void a0(int i, List<?> list, Writer writer) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        writer.N(i, list);
    }

    public static int b(List<?> list) {
        return list.size();
    }

    public static void b0(int i, List<?> list, Writer writer, y0 y0Var) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        writer.J(i, list, y0Var);
    }

    public static int c(int i, List<ByteString> list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int X = size * CodedOutputStream.X(i);
        for (int i2 = 0; i2 < list.size(); i2++) {
            X += CodedOutputStream.i(list.get(i2));
        }
        return X;
    }

    public static void c0(int i, List<Integer> list, Writer writer, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        writer.t(i, list, z);
    }

    public static int d(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int e = e(list);
        if (z) {
            return CodedOutputStream.X(i) + CodedOutputStream.E(e);
        }
        return e + (size * CodedOutputStream.X(i));
    }

    public static void d0(int i, List<Long> list, Writer writer, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        writer.f(i, list, z);
    }

    public static int e(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof z) {
            z zVar = (z) list;
            i = 0;
            while (i2 < size) {
                i += CodedOutputStream.m(zVar.getInt(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += CodedOutputStream.m(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    public static void e0(int i, List<Integer> list, Writer writer, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        writer.C(i, list, z);
    }

    public static int f(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        if (z) {
            return CodedOutputStream.X(i) + CodedOutputStream.E(size * 4);
        }
        return size * CodedOutputStream.n(i, 0);
    }

    public static void f0(int i, List<Long> list, Writer writer, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        writer.y(i, list, z);
    }

    public static int g(List<?> list) {
        return list.size() * 4;
    }

    public static void g0(int i, List<String> list, Writer writer) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        writer.j(i, list);
    }

    public static int h(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        if (z) {
            return CodedOutputStream.X(i) + CodedOutputStream.E(size * 8);
        }
        return size * CodedOutputStream.p(i, 0L);
    }

    public static void h0(int i, List<Integer> list, Writer writer, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        writer.x(i, list, z);
    }

    public static int i(List<?> list) {
        return list.size() * 8;
    }

    public static void i0(int i, List<Long> list, Writer writer, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        writer.g(i, list, z);
    }

    public static int j(int i, List<m0> list, y0 y0Var) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < size; i3++) {
            i2 += CodedOutputStream.u(i, list.get(i3), y0Var);
        }
        return i2;
    }

    public static int k(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int l = l(list);
        if (z) {
            return CodedOutputStream.X(i) + CodedOutputStream.E(l);
        }
        return l + (size * CodedOutputStream.X(i));
    }

    public static int l(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof z) {
            z zVar = (z) list;
            i = 0;
            while (i2 < size) {
                i += CodedOutputStream.y(zVar.getInt(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += CodedOutputStream.y(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    public static int m(int i, List<Long> list, boolean z) {
        if (list.size() == 0) {
            return 0;
        }
        int n = n(list);
        if (z) {
            return CodedOutputStream.X(i) + CodedOutputStream.E(n);
        }
        return n + (list.size() * CodedOutputStream.X(i));
    }

    public static int n(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof f0) {
            f0 f0Var = (f0) list;
            i = 0;
            while (i2 < size) {
                i += CodedOutputStream.A(f0Var.s(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += CodedOutputStream.A(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    public static int o(int i, Object obj, y0 y0Var) {
        if (obj instanceof c0) {
            return CodedOutputStream.C(i, (c0) obj);
        }
        return CodedOutputStream.H(i, (m0) obj, y0Var);
    }

    public static int p(int i, List<?> list, y0 y0Var) {
        int J;
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int X = CodedOutputStream.X(i) * size;
        for (int i2 = 0; i2 < size; i2++) {
            Object obj = list.get(i2);
            if (obj instanceof c0) {
                J = CodedOutputStream.D((c0) obj);
            } else {
                J = CodedOutputStream.J((m0) obj, y0Var);
            }
            X += J;
        }
        return X;
    }

    public static int q(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int r = r(list);
        if (z) {
            return CodedOutputStream.X(i) + CodedOutputStream.E(r);
        }
        return r + (size * CodedOutputStream.X(i));
    }

    public static int r(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof z) {
            z zVar = (z) list;
            i = 0;
            while (i2 < size) {
                i += CodedOutputStream.S(zVar.getInt(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += CodedOutputStream.S(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    public static int s(int i, List<Long> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int t = t(list);
        if (z) {
            return CodedOutputStream.X(i) + CodedOutputStream.E(t);
        }
        return t + (size * CodedOutputStream.X(i));
    }

    public static int t(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof f0) {
            f0 f0Var = (f0) list;
            i = 0;
            while (i2 < size) {
                i += CodedOutputStream.U(f0Var.s(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += CodedOutputStream.U(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    public static int u(int i, List<?> list) {
        int W;
        int W2;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        int X = CodedOutputStream.X(i) * size;
        if (list instanceof cz1) {
            cz1 cz1Var = (cz1) list;
            while (i2 < size) {
                Object j = cz1Var.j(i2);
                if (j instanceof ByteString) {
                    W2 = CodedOutputStream.i((ByteString) j);
                } else {
                    W2 = CodedOutputStream.W((String) j);
                }
                X += W2;
                i2++;
            }
        } else {
            while (i2 < size) {
                Object obj = list.get(i2);
                if (obj instanceof ByteString) {
                    W = CodedOutputStream.i((ByteString) obj);
                } else {
                    W = CodedOutputStream.W((String) obj);
                }
                X += W;
                i2++;
            }
        }
        return X;
    }

    public static int v(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int w = w(list);
        if (z) {
            return CodedOutputStream.X(i) + CodedOutputStream.E(w);
        }
        return w + (size * CodedOutputStream.X(i));
    }

    public static int w(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof z) {
            z zVar = (z) list;
            i = 0;
            while (i2 < size) {
                i += CodedOutputStream.Z(zVar.getInt(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += CodedOutputStream.Z(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    public static int x(int i, List<Long> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int y = y(list);
        if (z) {
            return CodedOutputStream.X(i) + CodedOutputStream.E(y);
        }
        return y + (size * CodedOutputStream.X(i));
    }

    public static int y(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof f0) {
            f0 f0Var = (f0) list;
            i = 0;
            while (i2 < size) {
                i += CodedOutputStream.b0(f0Var.s(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += CodedOutputStream.b0(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    public static <UT, UB> UB z(int i, List<Integer> list, a0.d<?> dVar, UB ub, f1<UT, UB> f1Var) {
        if (dVar == null) {
            return ub;
        }
        if (list instanceof RandomAccess) {
            int size = list.size();
            int i2 = 0;
            for (int i3 = 0; i3 < size; i3++) {
                int intValue = list.get(i3).intValue();
                if (dVar.findValueByNumber(intValue) != null) {
                    if (i3 != i2) {
                        list.set(i2, Integer.valueOf(intValue));
                    }
                    i2++;
                } else {
                    ub = (UB) M(i, intValue, ub, f1Var);
                }
            }
            if (i2 != size) {
                list.subList(i2, size).clear();
            }
        } else {
            Iterator<Integer> it = list.iterator();
            while (it.hasNext()) {
                int intValue2 = it.next().intValue();
                if (dVar.findValueByNumber(intValue2) == null) {
                    ub = (UB) M(i, intValue2, ub, f1Var);
                    it.remove();
                }
            }
        }
        return ub;
    }
}
