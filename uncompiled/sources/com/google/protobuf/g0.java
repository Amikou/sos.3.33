package com.google.protobuf;

import com.google.protobuf.Descriptors;
import com.google.protobuf.WireFormat;
import com.google.protobuf.a;
import com.google.protobuf.h0;
import com.google.protobuf.l0;
import java.io.IOException;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

/* compiled from: MapEntry.java */
/* loaded from: classes2.dex */
public final class g0<K, V> extends com.google.protobuf.a {
    public final K a;
    public final V f0;
    public final c<K, V> g0;
    public volatile int h0;

    /* compiled from: MapEntry.java */
    /* loaded from: classes2.dex */
    public static final class c<K, V> extends h0.b<K, V> {
        public final Descriptors.b e;
        public final t0<g0<K, V>> f;

        /* compiled from: MapEntry.java */
        /* loaded from: classes2.dex */
        public class a extends com.google.protobuf.c<g0<K, V>> {
            public a() {
            }

            @Override // com.google.protobuf.t0
            /* renamed from: a */
            public g0<K, V> parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new g0<>(c.this, jVar, rVar);
            }
        }

        public c(Descriptors.b bVar, g0<K, V> g0Var, WireFormat.FieldType fieldType, WireFormat.FieldType fieldType2) {
            super(fieldType, g0Var.a, fieldType2, g0Var.f0);
            this.e = bVar;
            this.f = new a();
        }
    }

    public static <V> boolean i(c cVar, V v) {
        if (cVar.c.getJavaType() == WireFormat.JavaType.MESSAGE) {
            return ((m0) v).isInitialized();
        }
        return true;
    }

    public static <K, V> g0<K, V> k(Descriptors.b bVar, WireFormat.FieldType fieldType, K k, WireFormat.FieldType fieldType2, V v) {
        return new g0<>(bVar, fieldType, k, fieldType2, v);
    }

    public final void d(Descriptors.FieldDescriptor fieldDescriptor) {
        if (fieldDescriptor.p() == this.g0.e) {
            return;
        }
        throw new RuntimeException("Wrong FieldDescriptor \"" + fieldDescriptor.d() + "\" used in message \"" + this.g0.e.d());
    }

    @Override // defpackage.d82, com.google.protobuf.o0
    /* renamed from: e */
    public g0<K, V> getDefaultInstanceForType() {
        c<K, V> cVar = this.g0;
        return new g0<>(cVar, cVar.b, cVar.d);
    }

    public K f() {
        return this.a;
    }

    public final c<K, V> g() {
        return this.g0;
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.google.protobuf.o0
    public Map<Descriptors.FieldDescriptor, Object> getAllFields() {
        TreeMap treeMap = new TreeMap();
        for (Descriptors.FieldDescriptor fieldDescriptor : this.g0.e.p()) {
            if (hasField(fieldDescriptor)) {
                treeMap.put(fieldDescriptor, getField(fieldDescriptor));
            }
        }
        return Collections.unmodifiableMap(treeMap);
    }

    @Override // com.google.protobuf.o0
    public Descriptors.b getDescriptorForType() {
        return this.g0.e;
    }

    @Override // com.google.protobuf.o0
    public Object getField(Descriptors.FieldDescriptor fieldDescriptor) {
        d(fieldDescriptor);
        Object f = fieldDescriptor.getNumber() == 1 ? f() : h();
        return fieldDescriptor.A() == Descriptors.FieldDescriptor.Type.ENUM ? fieldDescriptor.s().j(((Integer) f).intValue()) : f;
    }

    @Override // com.google.protobuf.m0
    public t0<g0<K, V>> getParserForType() {
        return this.g0.f;
    }

    @Override // com.google.protobuf.a, com.google.protobuf.m0
    public int getSerializedSize() {
        if (this.h0 != -1) {
            return this.h0;
        }
        int b2 = h0.b(this.g0, this.a, this.f0);
        this.h0 = b2;
        return b2;
    }

    @Override // com.google.protobuf.o0
    public g1 getUnknownFields() {
        return g1.c();
    }

    public V h() {
        return this.f0;
    }

    @Override // com.google.protobuf.o0
    public boolean hasField(Descriptors.FieldDescriptor fieldDescriptor) {
        d(fieldDescriptor);
        return true;
    }

    @Override // com.google.protobuf.a, defpackage.d82
    public boolean isInitialized() {
        return i(this.g0, this.f0);
    }

    @Override // com.google.protobuf.m0, com.google.protobuf.l0
    /* renamed from: j */
    public b<K, V> newBuilderForType() {
        return new b<>(this.g0);
    }

    @Override // com.google.protobuf.m0, com.google.protobuf.l0
    /* renamed from: l */
    public b<K, V> toBuilder() {
        return new b<>(this.g0, this.a, this.f0, true, true);
    }

    @Override // com.google.protobuf.a, com.google.protobuf.m0
    public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
        h0.f(codedOutputStream, this.g0, this.a, this.f0);
    }

    /* compiled from: MapEntry.java */
    /* loaded from: classes2.dex */
    public static class b<K, V> extends a.AbstractC0151a<b<K, V>> {
        public final c<K, V> a;
        public K f0;
        public V g0;
        public boolean h0;
        public boolean i0;

        @Override // com.google.protobuf.l0.a
        /* renamed from: a */
        public b<K, V> addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
            throw new RuntimeException("There is no repeated field in a map entry message.");
        }

        @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
        /* renamed from: b */
        public g0<K, V> build() {
            g0<K, V> buildPartial = buildPartial();
            if (buildPartial.isInitialized()) {
                return buildPartial;
            }
            throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
        }

        @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
        /* renamed from: d */
        public g0<K, V> buildPartial() {
            return new g0<>(this.a, this.f0, this.g0);
        }

        public final void e(Descriptors.FieldDescriptor fieldDescriptor) {
            if (fieldDescriptor.p() == this.a.e) {
                return;
            }
            throw new RuntimeException("Wrong FieldDescriptor \"" + fieldDescriptor.d() + "\" used in message \"" + this.a.e.d());
        }

        @Override // com.google.protobuf.l0.a
        /* renamed from: f */
        public b<K, V> clearField(Descriptors.FieldDescriptor fieldDescriptor) {
            e(fieldDescriptor);
            if (fieldDescriptor.getNumber() == 1) {
                g();
            } else {
                h();
            }
            return this;
        }

        public b<K, V> g() {
            this.f0 = this.a.b;
            this.h0 = false;
            return this;
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.google.protobuf.o0
        public Map<Descriptors.FieldDescriptor, Object> getAllFields() {
            TreeMap treeMap = new TreeMap();
            for (Descriptors.FieldDescriptor fieldDescriptor : this.a.e.p()) {
                if (hasField(fieldDescriptor)) {
                    treeMap.put(fieldDescriptor, getField(fieldDescriptor));
                }
            }
            return Collections.unmodifiableMap(treeMap);
        }

        @Override // com.google.protobuf.l0.a, com.google.protobuf.o0
        public Descriptors.b getDescriptorForType() {
            return this.a.e;
        }

        @Override // com.google.protobuf.o0
        public Object getField(Descriptors.FieldDescriptor fieldDescriptor) {
            e(fieldDescriptor);
            Object l = fieldDescriptor.getNumber() == 1 ? l() : o();
            return fieldDescriptor.A() == Descriptors.FieldDescriptor.Type.ENUM ? fieldDescriptor.s().j(((Integer) l).intValue()) : l;
        }

        @Override // com.google.protobuf.o0
        public g1 getUnknownFields() {
            return g1.c();
        }

        public b<K, V> h() {
            this.g0 = this.a.d;
            this.i0 = false;
            return this;
        }

        @Override // com.google.protobuf.o0
        public boolean hasField(Descriptors.FieldDescriptor fieldDescriptor) {
            e(fieldDescriptor);
            return fieldDescriptor.getNumber() == 1 ? this.h0 : this.i0;
        }

        @Override // defpackage.d82
        public boolean isInitialized() {
            return g0.i(this.a, this.g0);
        }

        @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
        /* renamed from: j */
        public b<K, V> clone() {
            return new b<>(this.a, this.f0, this.g0, this.h0, this.i0);
        }

        @Override // defpackage.d82, com.google.protobuf.o0
        /* renamed from: k */
        public g0<K, V> getDefaultInstanceForType() {
            c<K, V> cVar = this.a;
            return new g0<>(cVar, cVar.b, cVar.d);
        }

        public K l() {
            return this.f0;
        }

        @Override // com.google.protobuf.l0.a
        public l0.a newBuilderForField(Descriptors.FieldDescriptor fieldDescriptor) {
            e(fieldDescriptor);
            if (fieldDescriptor.getNumber() == 2 && fieldDescriptor.v() == Descriptors.FieldDescriptor.JavaType.MESSAGE) {
                return ((l0) this.g0).newBuilderForType();
            }
            throw new RuntimeException("\"" + fieldDescriptor.d() + "\" is not a message value field.");
        }

        public V o() {
            return this.g0;
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.google.protobuf.l0.a
        /* renamed from: p */
        public b<K, V> setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
            e(fieldDescriptor);
            if (fieldDescriptor.getNumber() == 1) {
                r(obj);
            } else {
                if (fieldDescriptor.A() == Descriptors.FieldDescriptor.Type.ENUM) {
                    obj = Integer.valueOf(((Descriptors.d) obj).getNumber());
                } else if (fieldDescriptor.A() == Descriptors.FieldDescriptor.Type.MESSAGE && obj != null && !this.a.d.getClass().isInstance(obj)) {
                    obj = ((l0) this.a.d).toBuilder().mergeFrom((l0) obj).build();
                }
                t(obj);
            }
            return this;
        }

        public b<K, V> r(K k) {
            this.f0 = k;
            this.h0 = true;
            return this;
        }

        @Override // com.google.protobuf.l0.a
        /* renamed from: s */
        public b<K, V> setUnknownFields(g1 g1Var) {
            return this;
        }

        public b<K, V> t(V v) {
            this.g0 = v;
            this.i0 = true;
            return this;
        }

        public b(c<K, V> cVar) {
            this(cVar, cVar.b, cVar.d, false, false);
        }

        public b(c<K, V> cVar, K k, V v, boolean z, boolean z2) {
            this.a = cVar;
            this.f0 = k;
            this.g0 = v;
            this.h0 = z;
            this.i0 = z2;
        }
    }

    public g0(Descriptors.b bVar, WireFormat.FieldType fieldType, K k, WireFormat.FieldType fieldType2, V v) {
        this.h0 = -1;
        this.a = k;
        this.f0 = v;
        this.g0 = new c<>(bVar, this, fieldType, fieldType2);
    }

    public g0(c cVar, K k, V v) {
        this.h0 = -1;
        this.a = k;
        this.f0 = v;
        this.g0 = cVar;
    }

    public g0(c<K, V> cVar, j jVar, r rVar) throws InvalidProtocolBufferException {
        this.h0 = -1;
        try {
            this.g0 = cVar;
            Map.Entry d = h0.d(jVar, cVar, rVar);
            this.a = (K) d.getKey();
            this.f0 = (V) d.getValue();
        } catch (InvalidProtocolBufferException e) {
            throw e.setUnfinishedMessage(this);
        } catch (IOException e2) {
            throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
        }
    }
}
