package com.google.protobuf;

import com.google.protobuf.Writer;
import java.io.IOException;
import java.util.Arrays;

/* compiled from: UnknownFieldSetLite.java */
/* loaded from: classes2.dex */
public final class h1 {
    public static final h1 f = new h1(0, new int[0], new Object[0], false);
    public int a;
    public int[] b;
    public Object[] c;
    public int d;
    public boolean e;

    public h1() {
        this(0, new int[8], new Object[8], true);
    }

    public static h1 c() {
        return f;
    }

    public static int f(int[] iArr, int i) {
        int i2 = 17;
        for (int i3 = 0; i3 < i; i3++) {
            i2 = (i2 * 31) + iArr[i3];
        }
        return i2;
    }

    public static int g(Object[] objArr, int i) {
        int i2 = 17;
        for (int i3 = 0; i3 < i; i3++) {
            i2 = (i2 * 31) + objArr[i3].hashCode();
        }
        return i2;
    }

    public static h1 i(h1 h1Var, h1 h1Var2) {
        int i = h1Var.a + h1Var2.a;
        int[] copyOf = Arrays.copyOf(h1Var.b, i);
        System.arraycopy(h1Var2.b, 0, copyOf, h1Var.a, h1Var2.a);
        Object[] copyOf2 = Arrays.copyOf(h1Var.c, i);
        System.arraycopy(h1Var2.c, 0, copyOf2, h1Var.a, h1Var2.a);
        return new h1(i, copyOf, copyOf2, true);
    }

    public static h1 j() {
        return new h1();
    }

    public static boolean k(Object[] objArr, Object[] objArr2, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            if (!objArr[i2].equals(objArr2[i2])) {
                return false;
            }
        }
        return true;
    }

    public static boolean n(int[] iArr, int[] iArr2, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            if (iArr[i2] != iArr2[i2]) {
                return false;
            }
        }
        return true;
    }

    public static void p(int i, Object obj, Writer writer) throws IOException {
        int a = WireFormat.a(i);
        int b = WireFormat.b(i);
        if (b == 0) {
            writer.n(a, ((Long) obj).longValue());
        } else if (b == 1) {
            writer.h(a, ((Long) obj).longValue());
        } else if (b == 2) {
            writer.Q(a, (ByteString) obj);
        } else if (b != 3) {
            if (b == 5) {
                writer.d(a, ((Integer) obj).intValue());
                return;
            }
            throw new RuntimeException(InvalidProtocolBufferException.invalidWireType());
        } else if (writer.i() == Writer.FieldOrder.ASCENDING) {
            writer.q(a);
            ((h1) obj).q(writer);
            writer.B(a);
        } else {
            writer.B(a);
            ((h1) obj).q(writer);
            writer.q(a);
        }
    }

    public void a() {
        if (!this.e) {
            throw new UnsupportedOperationException();
        }
    }

    public final void b() {
        int i = this.a;
        int[] iArr = this.b;
        if (i == iArr.length) {
            int i2 = i + (i < 4 ? 8 : i >> 1);
            this.b = Arrays.copyOf(iArr, i2);
            this.c = Arrays.copyOf(this.c, i2);
        }
    }

    public int d() {
        int a0;
        int i = this.d;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < this.a; i3++) {
            int i4 = this.b[i3];
            int a = WireFormat.a(i4);
            int b = WireFormat.b(i4);
            if (b == 0) {
                a0 = CodedOutputStream.a0(a, ((Long) this.c[i3]).longValue());
            } else if (b == 1) {
                a0 = CodedOutputStream.p(a, ((Long) this.c[i3]).longValue());
            } else if (b == 2) {
                a0 = CodedOutputStream.h(a, (ByteString) this.c[i3]);
            } else if (b == 3) {
                a0 = (CodedOutputStream.X(a) * 2) + ((h1) this.c[i3]).d();
            } else if (b == 5) {
                a0 = CodedOutputStream.n(a, ((Integer) this.c[i3]).intValue());
            } else {
                throw new IllegalStateException(InvalidProtocolBufferException.invalidWireType());
            }
            i2 += a0;
        }
        this.d = i2;
        return i2;
    }

    public int e() {
        int i = this.d;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < this.a; i3++) {
            i2 += CodedOutputStream.L(WireFormat.a(this.b[i3]), (ByteString) this.c[i3]);
        }
        this.d = i2;
        return i2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && (obj instanceof h1)) {
            h1 h1Var = (h1) obj;
            int i = this.a;
            return i == h1Var.a && n(this.b, h1Var.b, i) && k(this.c, h1Var.c, this.a);
        }
        return false;
    }

    public void h() {
        this.e = false;
    }

    public int hashCode() {
        int i = this.a;
        return ((((527 + i) * 31) + f(this.b, i)) * 31) + g(this.c, this.a);
    }

    public final void l(StringBuilder sb, int i) {
        for (int i2 = 0; i2 < this.a; i2++) {
            n0.c(sb, i, String.valueOf(WireFormat.a(this.b[i2])), this.c[i2]);
        }
    }

    public void m(int i, Object obj) {
        a();
        b();
        int[] iArr = this.b;
        int i2 = this.a;
        iArr[i2] = i;
        this.c[i2] = obj;
        this.a = i2 + 1;
    }

    public void o(Writer writer) throws IOException {
        if (writer.i() == Writer.FieldOrder.DESCENDING) {
            for (int i = this.a - 1; i >= 0; i--) {
                writer.c(WireFormat.a(this.b[i]), this.c[i]);
            }
            return;
        }
        for (int i2 = 0; i2 < this.a; i2++) {
            writer.c(WireFormat.a(this.b[i2]), this.c[i2]);
        }
    }

    public void q(Writer writer) throws IOException {
        if (this.a == 0) {
            return;
        }
        if (writer.i() == Writer.FieldOrder.ASCENDING) {
            for (int i = 0; i < this.a; i++) {
                p(this.b[i], this.c[i], writer);
            }
            return;
        }
        for (int i2 = this.a - 1; i2 >= 0; i2--) {
            p(this.b[i2], this.c[i2], writer);
        }
    }

    public h1(int i, int[] iArr, Object[] objArr, boolean z) {
        this.d = -1;
        this.a = i;
        this.b = iArr;
        this.c = objArr;
        this.e = z;
    }
}
