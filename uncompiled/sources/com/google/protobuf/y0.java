package com.google.protobuf;

import com.google.protobuf.e;
import java.io.IOException;

/* compiled from: Schema.java */
/* loaded from: classes2.dex */
public interface y0<T> {
    void a(T t, T t2);

    boolean b(T t, T t2);

    T c();

    int d(T t);

    void e(T t);

    boolean f(T t);

    int g(T t);

    void h(T t, Writer writer) throws IOException;

    void i(T t, w0 w0Var, r rVar) throws IOException;

    void j(T t, byte[] bArr, int i, int i2, e.b bVar) throws IOException;
}
