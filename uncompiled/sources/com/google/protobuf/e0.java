package com.google.protobuf;

import com.google.protobuf.a0;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* compiled from: ListFieldSchema.java */
/* loaded from: classes2.dex */
public abstract class e0 {
    public static final e0 a = new b();
    public static final e0 b = new c();

    /* compiled from: ListFieldSchema.java */
    /* loaded from: classes2.dex */
    public static final class b extends e0 {
        public static final Class<?> c = Collections.unmodifiableList(Collections.emptyList()).getClass();

        public b() {
            super();
        }

        public static <E> List<E> f(Object obj, long j) {
            return (List) k1.E(obj, j);
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static <L> List<L> g(Object obj, long j, int i) {
            d0 d0Var;
            List<L> arrayList;
            List<L> f = f(obj, j);
            if (f.isEmpty()) {
                if (f instanceof cz1) {
                    arrayList = new d0(i);
                } else if ((f instanceof uu2) && (f instanceof a0.i)) {
                    arrayList = ((a0.i) f).a(i);
                } else {
                    arrayList = new ArrayList<>(i);
                }
                k1.U(obj, j, arrayList);
                return arrayList;
            }
            if (c.isAssignableFrom(f.getClass())) {
                ArrayList arrayList2 = new ArrayList(f.size() + i);
                arrayList2.addAll(f);
                k1.U(obj, j, arrayList2);
                d0Var = arrayList2;
            } else if (f instanceof bf4) {
                d0 d0Var2 = new d0(f.size() + i);
                d0Var2.addAll((bf4) f);
                k1.U(obj, j, d0Var2);
                d0Var = d0Var2;
            } else if ((f instanceof uu2) && (f instanceof a0.i)) {
                a0.i iVar = (a0.i) f;
                if (iVar.A()) {
                    return f;
                }
                a0.i a = iVar.a(f.size() + i);
                k1.U(obj, j, a);
                return a;
            } else {
                return f;
            }
            return d0Var;
        }

        @Override // com.google.protobuf.e0
        public void c(Object obj, long j) {
            Object unmodifiableList;
            List list = (List) k1.E(obj, j);
            if (list instanceof cz1) {
                unmodifiableList = ((cz1) list).x();
            } else if (c.isAssignableFrom(list.getClass())) {
                return;
            } else {
                if ((list instanceof uu2) && (list instanceof a0.i)) {
                    a0.i iVar = (a0.i) list;
                    if (iVar.A()) {
                        iVar.l();
                        return;
                    }
                    return;
                }
                unmodifiableList = Collections.unmodifiableList(list);
            }
            k1.U(obj, j, unmodifiableList);
        }

        @Override // com.google.protobuf.e0
        public <E> void d(Object obj, Object obj2, long j) {
            List f = f(obj2, j);
            List g = g(obj, j, f.size());
            int size = g.size();
            int size2 = f.size();
            if (size > 0 && size2 > 0) {
                g.addAll(f);
            }
            if (size > 0) {
                f = g;
            }
            k1.U(obj, j, f);
        }

        @Override // com.google.protobuf.e0
        public <L> List<L> e(Object obj, long j) {
            return g(obj, j, 10);
        }
    }

    /* compiled from: ListFieldSchema.java */
    /* loaded from: classes2.dex */
    public static final class c extends e0 {
        public c() {
            super();
        }

        public static <E> a0.i<E> f(Object obj, long j) {
            return (a0.i) k1.E(obj, j);
        }

        @Override // com.google.protobuf.e0
        public void c(Object obj, long j) {
            f(obj, j).l();
        }

        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r0v2, types: [java.util.List] */
        @Override // com.google.protobuf.e0
        public <E> void d(Object obj, Object obj2, long j) {
            a0.i<E> f = f(obj, j);
            a0.i<E> f2 = f(obj2, j);
            int size = f.size();
            int size2 = f2.size();
            a0.i<E> iVar = f;
            iVar = f;
            if (size > 0 && size2 > 0) {
                boolean A = f.A();
                a0.i<E> iVar2 = f;
                if (!A) {
                    iVar2 = f.a(size2 + size);
                }
                iVar2.addAll(f2);
                iVar = iVar2;
            }
            if (size > 0) {
                f2 = iVar;
            }
            k1.U(obj, j, f2);
        }

        @Override // com.google.protobuf.e0
        public <L> List<L> e(Object obj, long j) {
            a0.i f = f(obj, j);
            if (f.A()) {
                return f;
            }
            int size = f.size();
            a0.i a = f.a(size == 0 ? 10 : size * 2);
            k1.U(obj, j, a);
            return a;
        }
    }

    public static e0 a() {
        return a;
    }

    public static e0 b() {
        return b;
    }

    public abstract void c(Object obj, long j);

    public abstract <L> void d(Object obj, Object obj2, long j);

    public abstract <L> List<L> e(Object obj, long j);

    public e0() {
    }
}
