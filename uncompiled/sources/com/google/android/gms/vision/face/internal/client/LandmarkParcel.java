package com.google.android.gms.vision.face.internal.client;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.RecentlyNonNull;
import com.google.android.apps.common.proguard.UsedByNative;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

/* compiled from: com.google.android.gms:play-services-vision@@20.1.3 */
@UsedByNative("wrapper.cc")
/* loaded from: classes.dex */
public final class LandmarkParcel extends AbstractSafeParcelable {
    @RecentlyNonNull
    public static final Parcelable.Creator<LandmarkParcel> CREATOR = new bz5();
    public final int a;
    public final float f0;
    public final float g0;
    public final int h0;

    @UsedByNative("wrapper.cc")
    public LandmarkParcel(int i, float f, float f2, int i2) {
        this.a = i;
        this.f0 = f;
        this.g0 = f2;
        this.h0 = i2;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(@RecentlyNonNull Parcel parcel, int i) {
        int a = yb3.a(parcel);
        yb3.m(parcel, 1, this.a);
        yb3.j(parcel, 2, this.f0);
        yb3.j(parcel, 3, this.g0);
        yb3.m(parcel, 4, this.h0);
        yb3.b(parcel, a);
    }
}
