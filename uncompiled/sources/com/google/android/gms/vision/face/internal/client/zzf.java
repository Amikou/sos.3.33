package com.google.android.gms.vision.face.internal.client;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

/* compiled from: com.google.android.gms:play-services-vision@@20.1.3 */
/* loaded from: classes.dex */
public final class zzf extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzf> CREATOR = new ef5();
    public int a;
    public int f0;
    public int g0;
    public boolean h0;
    public boolean i0;
    public float j0;

    public zzf() {
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int a = yb3.a(parcel);
        yb3.m(parcel, 2, this.a);
        yb3.m(parcel, 3, this.f0);
        yb3.m(parcel, 4, this.g0);
        yb3.c(parcel, 5, this.h0);
        yb3.c(parcel, 6, this.i0);
        yb3.j(parcel, 7, this.j0);
        yb3.b(parcel, a);
    }

    public zzf(int i, int i2, int i3, boolean z, boolean z2, float f) {
        this.a = i;
        this.f0 = i2;
        this.g0 = i3;
        this.h0 = z;
        this.i0 = z2;
        this.j0 = f;
    }
}
