package com.google.android.gms.vision.face.internal.client;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.RecentlyNonNull;
import com.google.android.apps.common.proguard.UsedByNative;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

/* compiled from: com.google.android.gms:play-services-vision@@20.1.3 */
@UsedByNative("wrapper.cc")
/* loaded from: classes.dex */
public class FaceParcel extends AbstractSafeParcelable {
    @RecentlyNonNull
    public static final Parcelable.Creator<FaceParcel> CREATOR = new sc5();
    public final int a;
    public final int f0;
    public final float g0;
    public final float h0;
    public final float i0;
    public final float j0;
    public final float k0;
    public final float l0;
    public final float m0;
    @RecentlyNonNull
    public final LandmarkParcel[] n0;
    public final float o0;
    public final float p0;
    public final float q0;
    public final zza[] r0;
    public final float s0;

    public FaceParcel(int i, int i2, float f, float f2, float f3, float f4, float f5, float f6, float f7, LandmarkParcel[] landmarkParcelArr, float f8, float f9, float f10, zza[] zzaVarArr, float f11) {
        this.a = i;
        this.f0 = i2;
        this.g0 = f;
        this.h0 = f2;
        this.i0 = f3;
        this.j0 = f4;
        this.k0 = f5;
        this.l0 = f6;
        this.m0 = f7;
        this.n0 = landmarkParcelArr;
        this.o0 = f8;
        this.p0 = f9;
        this.q0 = f10;
        this.r0 = zzaVarArr;
        this.s0 = f11;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(@RecentlyNonNull Parcel parcel, int i) {
        int a = yb3.a(parcel);
        yb3.m(parcel, 1, this.a);
        yb3.m(parcel, 2, this.f0);
        yb3.j(parcel, 3, this.g0);
        yb3.j(parcel, 4, this.h0);
        yb3.j(parcel, 5, this.i0);
        yb3.j(parcel, 6, this.j0);
        yb3.j(parcel, 7, this.k0);
        yb3.j(parcel, 8, this.l0);
        yb3.v(parcel, 9, this.n0, i, false);
        yb3.j(parcel, 10, this.o0);
        yb3.j(parcel, 11, this.p0);
        yb3.j(parcel, 12, this.q0);
        yb3.v(parcel, 13, this.r0, i, false);
        yb3.j(parcel, 14, this.m0);
        yb3.j(parcel, 15, this.s0);
        yb3.b(parcel, a);
    }
}
