package com.google.android.gms.vision.face.internal.client;

import android.graphics.PointF;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

/* compiled from: com.google.android.gms:play-services-vision@@20.1.3 */
/* loaded from: classes.dex */
public final class zza extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zza> CREATOR = new fa5();
    public final PointF[] a;
    public final int f0;

    public zza(PointF[] pointFArr, int i) {
        this.a = pointFArr;
        this.f0 = i;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int a = yb3.a(parcel);
        yb3.v(parcel, 2, this.a, i, false);
        yb3.m(parcel, 3, this.f0);
        yb3.b(parcel, a);
    }
}
