package com.google.android.gms.vision.clearcut;

import android.content.Context;
import androidx.annotation.Keep;
import androidx.annotation.RecentlyNonNull;
import com.google.android.gms.internal.vision.l;
import java.util.concurrent.ExecutorService;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
@Keep
/* loaded from: classes.dex */
public class DynamiteClearcutLogger {
    private static final ExecutorService zza = af5.a().i(2, sp5.a);
    private i75 zzb = new i75(0.03333333333333333d);
    private VisionClearcutLogger zzc;

    public DynamiteClearcutLogger(@RecentlyNonNull Context context) {
        this.zzc = new VisionClearcutLogger(context);
    }

    public final void zza(int i, l lVar) {
        if (i == 3 && !this.zzb.a()) {
            zx1.e("Skipping image analysis log due to rate limiting", new Object[0]);
        } else {
            zza.execute(new r35(this, i, lVar));
        }
    }
}
