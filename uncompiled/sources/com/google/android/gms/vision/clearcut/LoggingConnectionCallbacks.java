package com.google.android.gms.vision.clearcut;

import android.os.Bundle;
import androidx.annotation.Keep;
import androidx.annotation.RecentlyNonNull;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
@Keep
/* loaded from: classes.dex */
public class LoggingConnectionCallbacks implements GoogleApiClient.b, GoogleApiClient.c {
    @Override // defpackage.u50
    public void onConnected(@RecentlyNonNull Bundle bundle) {
        throw new NoSuchMethodError();
    }

    @Override // defpackage.jm2
    public void onConnectionFailed(@RecentlyNonNull ConnectionResult connectionResult) {
        throw new NoSuchMethodError();
    }

    @Override // defpackage.u50
    public void onConnectionSuspended(int i) {
        throw new NoSuchMethodError();
    }
}
