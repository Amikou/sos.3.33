package com.google.android.gms.vision.clearcut;

import android.content.Context;
import androidx.annotation.Keep;
import androidx.annotation.RecentlyNonNull;
import com.google.android.gms.clearcut.a;
import com.google.android.gms.internal.vision.h0;
import com.google.android.gms.internal.vision.l;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
@Keep
/* loaded from: classes.dex */
public class VisionClearcutLogger {
    private final a zza;
    private boolean zzb = true;

    public VisionClearcutLogger(@RecentlyNonNull Context context) {
        this.zza = new a(context, "VISION", null);
    }

    public final void zza(int i, l lVar) {
        byte[] g = lVar.g();
        if (i < 0 || i > 3) {
            zx1.d("Illegal event code: %d", Integer.valueOf(i));
            return;
        }
        try {
            if (this.zzb) {
                this.zza.a(g).b(i).a();
                return;
            }
            l.a x = l.x();
            try {
                x.f(g, 0, g.length, h0.c());
                zx1.b("Would have logged:\n%s", x.toString());
            } catch (Exception e) {
                zx1.c(e, "Parsing error", new Object[0]);
            }
        } catch (Exception e2) {
            ti5.b(e2);
            zx1.c(e2, "Failed to log", new Object[0]);
        }
    }
}
