package com.google.android.gms.vision.clearcut;

import android.content.Context;
import android.content.pm.PackageManager;
import androidx.annotation.Keep;
import com.google.android.gms.internal.vision.b;
import com.google.android.gms.internal.vision.g;
import com.google.android.gms.internal.vision.k;
import com.google.android.gms.internal.vision.l;
import com.google.android.gms.internal.vision.l0;
import com.google.android.gms.internal.vision.zzfi$zzf;
import com.google.android.gms.internal.vision.zzfi$zzj;
import com.google.android.gms.internal.vision.zzs;
import java.util.ArrayList;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
@Keep
/* loaded from: classes.dex */
public class LogUtils {
    public static l zza(long j, int i, String str, String str2, List<k> list, zzs zzsVar) {
        g.a x = g.x();
        zzfi$zzf.a y = zzfi$zzf.x().x(str2).u(j).y(i);
        y.v(list);
        ArrayList arrayList = new ArrayList();
        arrayList.add((zzfi$zzf) ((l0) y.i()));
        return (l) ((l0) l.x().u((g) ((l0) x.v(arrayList).u((zzfi$zzj) ((l0) zzfi$zzj.x().v(zzsVar.f0).u(zzsVar.a).x(zzsVar.g0).y(zzsVar.h0).i())).i())).i());
    }

    private static String zzb(Context context) {
        try {
            return kr4.a(context).e(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            zx1.c(e, "Unable to find calling package info for %s", context.getPackageName());
            return null;
        }
    }

    public static b zza(Context context) {
        b.a u = b.x().u(context.getPackageName());
        String zzb = zzb(context);
        if (zzb != null) {
            u.v(zzb);
        }
        return (b) ((l0) u.i());
    }
}
