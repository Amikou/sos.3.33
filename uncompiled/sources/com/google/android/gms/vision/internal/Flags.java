package com.google.android.gms.vision.internal;

import androidx.annotation.Keep;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
@Keep
/* loaded from: classes.dex */
public class Flags {
    private static final t61<Boolean> zza = t61.a(0, "vision:product_barcode_value_logging_enabled", Boolean.TRUE);

    private Flags() {
    }
}
