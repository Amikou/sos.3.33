package com.google.android.gms.vision.barcode;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.vision.barcode.Barcode;

/* compiled from: com.google.android.gms:play-services-vision@@20.1.3 */
/* loaded from: classes.dex */
public final class d implements Parcelable.Creator<Barcode.ContactInfo> {
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Barcode.ContactInfo createFromParcel(Parcel parcel) {
        int J = SafeParcelReader.J(parcel);
        Barcode.PersonName personName = null;
        String str = null;
        String str2 = null;
        Barcode.Phone[] phoneArr = null;
        Barcode.Email[] emailArr = null;
        String[] strArr = null;
        Barcode.Address[] addressArr = null;
        while (parcel.dataPosition() < J) {
            int C = SafeParcelReader.C(parcel);
            switch (SafeParcelReader.v(C)) {
                case 2:
                    personName = (Barcode.PersonName) SafeParcelReader.o(parcel, C, Barcode.PersonName.CREATOR);
                    break;
                case 3:
                    str = SafeParcelReader.p(parcel, C);
                    break;
                case 4:
                    str2 = SafeParcelReader.p(parcel, C);
                    break;
                case 5:
                    phoneArr = (Barcode.Phone[]) SafeParcelReader.s(parcel, C, Barcode.Phone.CREATOR);
                    break;
                case 6:
                    emailArr = (Barcode.Email[]) SafeParcelReader.s(parcel, C, Barcode.Email.CREATOR);
                    break;
                case 7:
                    strArr = SafeParcelReader.q(parcel, C);
                    break;
                case 8:
                    addressArr = (Barcode.Address[]) SafeParcelReader.s(parcel, C, Barcode.Address.CREATOR);
                    break;
                default:
                    SafeParcelReader.I(parcel, C);
                    break;
            }
        }
        SafeParcelReader.u(parcel, J);
        return new Barcode.ContactInfo(personName, str, str2, phoneArr, emailArr, strArr, addressArr);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Barcode.ContactInfo[] newArray(int i) {
        return new Barcode.ContactInfo[i];
    }
}
