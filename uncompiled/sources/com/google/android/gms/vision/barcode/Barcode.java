package com.google.android.gms.vision.barcode;

import android.graphics.Point;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.RecentlyNonNull;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

/* compiled from: com.google.android.gms:play-services-vision@@20.1.3 */
/* loaded from: classes.dex */
public class Barcode extends AbstractSafeParcelable {
    @RecentlyNonNull
    public static final Parcelable.Creator<Barcode> CREATOR = new b();
    public int a;
    @RecentlyNonNull
    public String f0;
    @RecentlyNonNull
    public String g0;
    public int h0;
    @RecentlyNonNull
    public Point[] i0;
    @RecentlyNonNull
    public Email j0;
    @RecentlyNonNull
    public Phone k0;
    @RecentlyNonNull
    public Sms l0;
    @RecentlyNonNull
    public WiFi m0;
    @RecentlyNonNull
    public UrlBookmark n0;
    @RecentlyNonNull
    public GeoPoint o0;
    @RecentlyNonNull
    public CalendarEvent p0;
    @RecentlyNonNull
    public ContactInfo q0;
    @RecentlyNonNull
    public DriverLicense r0;
    @RecentlyNonNull
    public byte[] s0;
    public boolean t0;

    /* compiled from: com.google.android.gms:play-services-vision@@20.1.3 */
    /* loaded from: classes.dex */
    public static class Address extends AbstractSafeParcelable {
        @RecentlyNonNull
        public static final Parcelable.Creator<Address> CREATOR = new a();
        public int a;
        @RecentlyNonNull
        public String[] f0;

        public Address() {
        }

        @Override // android.os.Parcelable
        public void writeToParcel(@RecentlyNonNull Parcel parcel, int i) {
            int a = yb3.a(parcel);
            yb3.m(parcel, 2, this.a);
            yb3.t(parcel, 3, this.f0, false);
            yb3.b(parcel, a);
        }

        public Address(int i, @RecentlyNonNull String[] strArr) {
            this.a = i;
            this.f0 = strArr;
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision@@20.1.3 */
    /* loaded from: classes.dex */
    public static class CalendarDateTime extends AbstractSafeParcelable {
        @RecentlyNonNull
        public static final Parcelable.Creator<CalendarDateTime> CREATOR = new c();
        public int a;
        public int f0;
        public int g0;
        public int h0;
        public int i0;
        public int j0;
        public boolean k0;
        @RecentlyNonNull
        public String l0;

        public CalendarDateTime() {
        }

        @Override // android.os.Parcelable
        public void writeToParcel(@RecentlyNonNull Parcel parcel, int i) {
            int a = yb3.a(parcel);
            yb3.m(parcel, 2, this.a);
            yb3.m(parcel, 3, this.f0);
            yb3.m(parcel, 4, this.g0);
            yb3.m(parcel, 5, this.h0);
            yb3.m(parcel, 6, this.i0);
            yb3.m(parcel, 7, this.j0);
            yb3.c(parcel, 8, this.k0);
            yb3.s(parcel, 9, this.l0, false);
            yb3.b(parcel, a);
        }

        public CalendarDateTime(int i, int i2, int i3, int i4, int i5, int i6, boolean z, @RecentlyNonNull String str) {
            this.a = i;
            this.f0 = i2;
            this.g0 = i3;
            this.h0 = i4;
            this.i0 = i5;
            this.j0 = i6;
            this.k0 = z;
            this.l0 = str;
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision@@20.1.3 */
    /* loaded from: classes.dex */
    public static class CalendarEvent extends AbstractSafeParcelable {
        @RecentlyNonNull
        public static final Parcelable.Creator<CalendarEvent> CREATOR = new e();
        @RecentlyNonNull
        public String a;
        @RecentlyNonNull
        public String f0;
        @RecentlyNonNull
        public String g0;
        @RecentlyNonNull
        public String h0;
        @RecentlyNonNull
        public String i0;
        @RecentlyNonNull
        public CalendarDateTime j0;
        @RecentlyNonNull
        public CalendarDateTime k0;

        public CalendarEvent() {
        }

        @Override // android.os.Parcelable
        public void writeToParcel(@RecentlyNonNull Parcel parcel, int i) {
            int a = yb3.a(parcel);
            yb3.s(parcel, 2, this.a, false);
            yb3.s(parcel, 3, this.f0, false);
            yb3.s(parcel, 4, this.g0, false);
            yb3.s(parcel, 5, this.h0, false);
            yb3.s(parcel, 6, this.i0, false);
            yb3.r(parcel, 7, this.j0, i, false);
            yb3.r(parcel, 8, this.k0, i, false);
            yb3.b(parcel, a);
        }

        public CalendarEvent(@RecentlyNonNull String str, @RecentlyNonNull String str2, @RecentlyNonNull String str3, @RecentlyNonNull String str4, @RecentlyNonNull String str5, @RecentlyNonNull CalendarDateTime calendarDateTime, @RecentlyNonNull CalendarDateTime calendarDateTime2) {
            this.a = str;
            this.f0 = str2;
            this.g0 = str3;
            this.h0 = str4;
            this.i0 = str5;
            this.j0 = calendarDateTime;
            this.k0 = calendarDateTime2;
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision@@20.1.3 */
    /* loaded from: classes.dex */
    public static class ContactInfo extends AbstractSafeParcelable {
        @RecentlyNonNull
        public static final Parcelable.Creator<ContactInfo> CREATOR = new d();
        @RecentlyNonNull
        public PersonName a;
        @RecentlyNonNull
        public String f0;
        @RecentlyNonNull
        public String g0;
        @RecentlyNonNull
        public Phone[] h0;
        @RecentlyNonNull
        public Email[] i0;
        @RecentlyNonNull
        public String[] j0;
        @RecentlyNonNull
        public Address[] k0;

        public ContactInfo() {
        }

        @Override // android.os.Parcelable
        public void writeToParcel(@RecentlyNonNull Parcel parcel, int i) {
            int a = yb3.a(parcel);
            yb3.r(parcel, 2, this.a, i, false);
            yb3.s(parcel, 3, this.f0, false);
            yb3.s(parcel, 4, this.g0, false);
            yb3.v(parcel, 5, this.h0, i, false);
            yb3.v(parcel, 6, this.i0, i, false);
            yb3.t(parcel, 7, this.j0, false);
            yb3.v(parcel, 8, this.k0, i, false);
            yb3.b(parcel, a);
        }

        public ContactInfo(@RecentlyNonNull PersonName personName, @RecentlyNonNull String str, @RecentlyNonNull String str2, @RecentlyNonNull Phone[] phoneArr, @RecentlyNonNull Email[] emailArr, @RecentlyNonNull String[] strArr, @RecentlyNonNull Address[] addressArr) {
            this.a = personName;
            this.f0 = str;
            this.g0 = str2;
            this.h0 = phoneArr;
            this.i0 = emailArr;
            this.j0 = strArr;
            this.k0 = addressArr;
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision@@20.1.3 */
    /* loaded from: classes.dex */
    public static class DriverLicense extends AbstractSafeParcelable {
        @RecentlyNonNull
        public static final Parcelable.Creator<DriverLicense> CREATOR = new g();
        @RecentlyNonNull
        public String a;
        @RecentlyNonNull
        public String f0;
        @RecentlyNonNull
        public String g0;
        @RecentlyNonNull
        public String h0;
        @RecentlyNonNull
        public String i0;
        @RecentlyNonNull
        public String j0;
        @RecentlyNonNull
        public String k0;
        @RecentlyNonNull
        public String l0;
        @RecentlyNonNull
        public String m0;
        @RecentlyNonNull
        public String n0;
        @RecentlyNonNull
        public String o0;
        @RecentlyNonNull
        public String p0;
        @RecentlyNonNull
        public String q0;
        @RecentlyNonNull
        public String r0;

        public DriverLicense() {
        }

        @Override // android.os.Parcelable
        public void writeToParcel(@RecentlyNonNull Parcel parcel, int i) {
            int a = yb3.a(parcel);
            yb3.s(parcel, 2, this.a, false);
            yb3.s(parcel, 3, this.f0, false);
            yb3.s(parcel, 4, this.g0, false);
            yb3.s(parcel, 5, this.h0, false);
            yb3.s(parcel, 6, this.i0, false);
            yb3.s(parcel, 7, this.j0, false);
            yb3.s(parcel, 8, this.k0, false);
            yb3.s(parcel, 9, this.l0, false);
            yb3.s(parcel, 10, this.m0, false);
            yb3.s(parcel, 11, this.n0, false);
            yb3.s(parcel, 12, this.o0, false);
            yb3.s(parcel, 13, this.p0, false);
            yb3.s(parcel, 14, this.q0, false);
            yb3.s(parcel, 15, this.r0, false);
            yb3.b(parcel, a);
        }

        public DriverLicense(@RecentlyNonNull String str, @RecentlyNonNull String str2, @RecentlyNonNull String str3, @RecentlyNonNull String str4, @RecentlyNonNull String str5, @RecentlyNonNull String str6, @RecentlyNonNull String str7, @RecentlyNonNull String str8, @RecentlyNonNull String str9, @RecentlyNonNull String str10, @RecentlyNonNull String str11, @RecentlyNonNull String str12, @RecentlyNonNull String str13, @RecentlyNonNull String str14) {
            this.a = str;
            this.f0 = str2;
            this.g0 = str3;
            this.h0 = str4;
            this.i0 = str5;
            this.j0 = str6;
            this.k0 = str7;
            this.l0 = str8;
            this.m0 = str9;
            this.n0 = str10;
            this.o0 = str11;
            this.p0 = str12;
            this.q0 = str13;
            this.r0 = str14;
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision@@20.1.3 */
    /* loaded from: classes.dex */
    public static class Email extends AbstractSafeParcelable {
        @RecentlyNonNull
        public static final Parcelable.Creator<Email> CREATOR = new f();
        public int a;
        @RecentlyNonNull
        public String f0;
        @RecentlyNonNull
        public String g0;
        @RecentlyNonNull
        public String h0;

        public Email() {
        }

        @Override // android.os.Parcelable
        public void writeToParcel(@RecentlyNonNull Parcel parcel, int i) {
            int a = yb3.a(parcel);
            yb3.m(parcel, 2, this.a);
            yb3.s(parcel, 3, this.f0, false);
            yb3.s(parcel, 4, this.g0, false);
            yb3.s(parcel, 5, this.h0, false);
            yb3.b(parcel, a);
        }

        public Email(int i, @RecentlyNonNull String str, @RecentlyNonNull String str2, @RecentlyNonNull String str3) {
            this.a = i;
            this.f0 = str;
            this.g0 = str2;
            this.h0 = str3;
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision@@20.1.3 */
    /* loaded from: classes.dex */
    public static class GeoPoint extends AbstractSafeParcelable {
        @RecentlyNonNull
        public static final Parcelable.Creator<GeoPoint> CREATOR = new i();
        public double a;
        public double f0;

        public GeoPoint() {
        }

        @Override // android.os.Parcelable
        public void writeToParcel(@RecentlyNonNull Parcel parcel, int i) {
            int a = yb3.a(parcel);
            yb3.h(parcel, 2, this.a);
            yb3.h(parcel, 3, this.f0);
            yb3.b(parcel, a);
        }

        public GeoPoint(double d, double d2) {
            this.a = d;
            this.f0 = d2;
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision@@20.1.3 */
    /* loaded from: classes.dex */
    public static class PersonName extends AbstractSafeParcelable {
        @RecentlyNonNull
        public static final Parcelable.Creator<PersonName> CREATOR = new h();
        @RecentlyNonNull
        public String a;
        @RecentlyNonNull
        public String f0;
        @RecentlyNonNull
        public String g0;
        @RecentlyNonNull
        public String h0;
        @RecentlyNonNull
        public String i0;
        @RecentlyNonNull
        public String j0;
        @RecentlyNonNull
        public String k0;

        public PersonName() {
        }

        @Override // android.os.Parcelable
        public void writeToParcel(@RecentlyNonNull Parcel parcel, int i) {
            int a = yb3.a(parcel);
            yb3.s(parcel, 2, this.a, false);
            yb3.s(parcel, 3, this.f0, false);
            yb3.s(parcel, 4, this.g0, false);
            yb3.s(parcel, 5, this.h0, false);
            yb3.s(parcel, 6, this.i0, false);
            yb3.s(parcel, 7, this.j0, false);
            yb3.s(parcel, 8, this.k0, false);
            yb3.b(parcel, a);
        }

        public PersonName(@RecentlyNonNull String str, @RecentlyNonNull String str2, @RecentlyNonNull String str3, @RecentlyNonNull String str4, @RecentlyNonNull String str5, @RecentlyNonNull String str6, @RecentlyNonNull String str7) {
            this.a = str;
            this.f0 = str2;
            this.g0 = str3;
            this.h0 = str4;
            this.i0 = str5;
            this.j0 = str6;
            this.k0 = str7;
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision@@20.1.3 */
    /* loaded from: classes.dex */
    public static class Phone extends AbstractSafeParcelable {
        @RecentlyNonNull
        public static final Parcelable.Creator<Phone> CREATOR = new k();
        public int a;
        @RecentlyNonNull
        public String f0;

        public Phone() {
        }

        @Override // android.os.Parcelable
        public void writeToParcel(@RecentlyNonNull Parcel parcel, int i) {
            int a = yb3.a(parcel);
            yb3.m(parcel, 2, this.a);
            yb3.s(parcel, 3, this.f0, false);
            yb3.b(parcel, a);
        }

        public Phone(int i, @RecentlyNonNull String str) {
            this.a = i;
            this.f0 = str;
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision@@20.1.3 */
    /* loaded from: classes.dex */
    public static class Sms extends AbstractSafeParcelable {
        @RecentlyNonNull
        public static final Parcelable.Creator<Sms> CREATOR = new j();
        @RecentlyNonNull
        public String a;
        @RecentlyNonNull
        public String f0;

        public Sms() {
        }

        @Override // android.os.Parcelable
        public void writeToParcel(@RecentlyNonNull Parcel parcel, int i) {
            int a = yb3.a(parcel);
            yb3.s(parcel, 2, this.a, false);
            yb3.s(parcel, 3, this.f0, false);
            yb3.b(parcel, a);
        }

        public Sms(@RecentlyNonNull String str, @RecentlyNonNull String str2) {
            this.a = str;
            this.f0 = str2;
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision@@20.1.3 */
    /* loaded from: classes.dex */
    public static class UrlBookmark extends AbstractSafeParcelable {
        @RecentlyNonNull
        public static final Parcelable.Creator<UrlBookmark> CREATOR = new m();
        @RecentlyNonNull
        public String a;
        @RecentlyNonNull
        public String f0;

        public UrlBookmark() {
        }

        @Override // android.os.Parcelable
        public void writeToParcel(@RecentlyNonNull Parcel parcel, int i) {
            int a = yb3.a(parcel);
            yb3.s(parcel, 2, this.a, false);
            yb3.s(parcel, 3, this.f0, false);
            yb3.b(parcel, a);
        }

        public UrlBookmark(@RecentlyNonNull String str, @RecentlyNonNull String str2) {
            this.a = str;
            this.f0 = str2;
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision@@20.1.3 */
    /* loaded from: classes.dex */
    public static class WiFi extends AbstractSafeParcelable {
        @RecentlyNonNull
        public static final Parcelable.Creator<WiFi> CREATOR = new l();
        @RecentlyNonNull
        public String a;
        @RecentlyNonNull
        public String f0;
        public int g0;

        public WiFi() {
        }

        @Override // android.os.Parcelable
        public void writeToParcel(@RecentlyNonNull Parcel parcel, int i) {
            int a = yb3.a(parcel);
            yb3.s(parcel, 2, this.a, false);
            yb3.s(parcel, 3, this.f0, false);
            yb3.m(parcel, 4, this.g0);
            yb3.b(parcel, a);
        }

        public WiFi(@RecentlyNonNull String str, @RecentlyNonNull String str2, int i) {
            this.a = str;
            this.f0 = str2;
            this.g0 = i;
        }
    }

    public Barcode() {
    }

    @Override // android.os.Parcelable
    public void writeToParcel(@RecentlyNonNull Parcel parcel, int i) {
        int a = yb3.a(parcel);
        yb3.m(parcel, 2, this.a);
        yb3.s(parcel, 3, this.f0, false);
        yb3.s(parcel, 4, this.g0, false);
        yb3.m(parcel, 5, this.h0);
        yb3.v(parcel, 6, this.i0, i, false);
        yb3.r(parcel, 7, this.j0, i, false);
        yb3.r(parcel, 8, this.k0, i, false);
        yb3.r(parcel, 9, this.l0, i, false);
        yb3.r(parcel, 10, this.m0, i, false);
        yb3.r(parcel, 11, this.n0, i, false);
        yb3.r(parcel, 12, this.o0, i, false);
        yb3.r(parcel, 13, this.p0, i, false);
        yb3.r(parcel, 14, this.q0, i, false);
        yb3.r(parcel, 15, this.r0, i, false);
        yb3.f(parcel, 16, this.s0, false);
        yb3.c(parcel, 17, this.t0);
        yb3.b(parcel, a);
    }

    public Barcode(int i, @RecentlyNonNull String str, @RecentlyNonNull String str2, int i2, @RecentlyNonNull Point[] pointArr, @RecentlyNonNull Email email, @RecentlyNonNull Phone phone, @RecentlyNonNull Sms sms, @RecentlyNonNull WiFi wiFi, @RecentlyNonNull UrlBookmark urlBookmark, @RecentlyNonNull GeoPoint geoPoint, @RecentlyNonNull CalendarEvent calendarEvent, @RecentlyNonNull ContactInfo contactInfo, @RecentlyNonNull DriverLicense driverLicense, @RecentlyNonNull byte[] bArr, boolean z) {
        this.a = i;
        this.f0 = str;
        this.s0 = bArr;
        this.g0 = str2;
        this.h0 = i2;
        this.i0 = pointArr;
        this.t0 = z;
        this.j0 = email;
        this.k0 = phone;
        this.l0 = sms;
        this.m0 = wiFi;
        this.n0 = urlBookmark;
        this.o0 = geoPoint;
        this.p0 = calendarEvent;
        this.q0 = contactInfo;
        this.r0 = driverLicense;
    }
}
