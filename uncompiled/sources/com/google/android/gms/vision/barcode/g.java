package com.google.android.gms.vision.barcode;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.vision.barcode.Barcode;

/* compiled from: com.google.android.gms:play-services-vision@@20.1.3 */
/* loaded from: classes.dex */
public final class g implements Parcelable.Creator<Barcode.DriverLicense> {
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Barcode.DriverLicense createFromParcel(Parcel parcel) {
        int J = SafeParcelReader.J(parcel);
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        String str6 = null;
        String str7 = null;
        String str8 = null;
        String str9 = null;
        String str10 = null;
        String str11 = null;
        String str12 = null;
        String str13 = null;
        String str14 = null;
        while (parcel.dataPosition() < J) {
            int C = SafeParcelReader.C(parcel);
            switch (SafeParcelReader.v(C)) {
                case 2:
                    str = SafeParcelReader.p(parcel, C);
                    break;
                case 3:
                    str2 = SafeParcelReader.p(parcel, C);
                    break;
                case 4:
                    str3 = SafeParcelReader.p(parcel, C);
                    break;
                case 5:
                    str4 = SafeParcelReader.p(parcel, C);
                    break;
                case 6:
                    str5 = SafeParcelReader.p(parcel, C);
                    break;
                case 7:
                    str6 = SafeParcelReader.p(parcel, C);
                    break;
                case 8:
                    str7 = SafeParcelReader.p(parcel, C);
                    break;
                case 9:
                    str8 = SafeParcelReader.p(parcel, C);
                    break;
                case 10:
                    str9 = SafeParcelReader.p(parcel, C);
                    break;
                case 11:
                    str10 = SafeParcelReader.p(parcel, C);
                    break;
                case 12:
                    str11 = SafeParcelReader.p(parcel, C);
                    break;
                case 13:
                    str12 = SafeParcelReader.p(parcel, C);
                    break;
                case 14:
                    str13 = SafeParcelReader.p(parcel, C);
                    break;
                case 15:
                    str14 = SafeParcelReader.p(parcel, C);
                    break;
                default:
                    SafeParcelReader.I(parcel, C);
                    break;
            }
        }
        SafeParcelReader.u(parcel, J);
        return new Barcode.DriverLicense(str, str2, str3, str4, str5, str6, str7, str8, str9, str10, str11, str12, str13, str14);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Barcode.DriverLicense[] newArray(int i) {
        return new Barcode.DriverLicense[i];
    }
}
