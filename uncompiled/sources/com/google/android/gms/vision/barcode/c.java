package com.google.android.gms.vision.barcode;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.vision.barcode.Barcode;

/* compiled from: com.google.android.gms:play-services-vision@@20.1.3 */
/* loaded from: classes.dex */
public final class c implements Parcelable.Creator<Barcode.CalendarDateTime> {
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Barcode.CalendarDateTime createFromParcel(Parcel parcel) {
        int J = SafeParcelReader.J(parcel);
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        int i6 = 0;
        boolean z = false;
        String str = null;
        while (parcel.dataPosition() < J) {
            int C = SafeParcelReader.C(parcel);
            switch (SafeParcelReader.v(C)) {
                case 2:
                    i = SafeParcelReader.E(parcel, C);
                    break;
                case 3:
                    i2 = SafeParcelReader.E(parcel, C);
                    break;
                case 4:
                    i3 = SafeParcelReader.E(parcel, C);
                    break;
                case 5:
                    i4 = SafeParcelReader.E(parcel, C);
                    break;
                case 6:
                    i5 = SafeParcelReader.E(parcel, C);
                    break;
                case 7:
                    i6 = SafeParcelReader.E(parcel, C);
                    break;
                case 8:
                    z = SafeParcelReader.w(parcel, C);
                    break;
                case 9:
                    str = SafeParcelReader.p(parcel, C);
                    break;
                default:
                    SafeParcelReader.I(parcel, C);
                    break;
            }
        }
        SafeParcelReader.u(parcel, J);
        return new Barcode.CalendarDateTime(i, i2, i3, i4, i5, i6, z, str);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Barcode.CalendarDateTime[] newArray(int i) {
        return new Barcode.CalendarDateTime[i];
    }
}
