package com.google.android.gms.cloudmessaging;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Message;
import android.os.Parcel;
import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-cloud-messaging@@16.0.0 */
/* loaded from: classes.dex */
public interface a extends IInterface {

    /* compiled from: com.google.android.gms:play-services-cloud-messaging@@16.0.0 */
    /* renamed from: com.google.android.gms.cloudmessaging.a$a  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static class C0105a implements a {
        public final IBinder a;

        public C0105a(IBinder iBinder) {
            this.a = iBinder;
        }

        @Override // android.os.IInterface
        public IBinder asBinder() {
            return this.a;
        }

        @Override // com.google.android.gms.cloudmessaging.a
        public void w(Message message) throws RemoteException {
            Parcel obtain = Parcel.obtain();
            obtain.writeInterfaceToken("com.google.android.gms.iid.IMessengerCompat");
            obtain.writeInt(1);
            message.writeToParcel(obtain, 0);
            try {
                this.a.transact(1, obtain, null, 1);
            } finally {
                obtain.recycle();
            }
        }
    }

    void w(Message message) throws RemoteException;
}
