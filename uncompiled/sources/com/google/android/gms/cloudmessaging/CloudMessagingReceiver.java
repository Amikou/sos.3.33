package com.google.android.gms.cloudmessaging;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import com.google.android.gms.cloudmessaging.CloudMessagingReceiver;
import com.google.android.gms.tasks.d;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/* compiled from: com.google.android.gms:play-services-cloud-messaging@@16.0.0 */
/* loaded from: classes.dex */
public abstract class CloudMessagingReceiver extends BroadcastReceiver {
    public final ExecutorService a = i35.a().b(new yc2("firebase-iid-executor"), fi5.a);

    public Executor a() {
        return this.a;
    }

    public abstract int b(Context context, CloudMessage cloudMessage);

    public void c(Context context, Bundle bundle) {
    }

    public void d(Context context, Bundle bundle) {
    }

    public final int e(Context context, Intent intent) {
        PendingIntent pendingIntent = (PendingIntent) intent.getParcelableExtra("pending_intent");
        if (pendingIntent != null) {
            try {
                pendingIntent.send();
            } catch (PendingIntent.CanceledException unused) {
            }
        }
        Bundle extras = intent.getExtras();
        if (extras != null) {
            extras.remove("pending_intent");
        } else {
            extras = new Bundle();
        }
        if ("com.google.firebase.messaging.NOTIFICATION_OPEN".equals(intent.getAction())) {
            d(context, extras);
            return -1;
        } else if ("com.google.firebase.messaging.NOTIFICATION_DISMISS".equals(intent.getAction())) {
            c(context, extras);
            return -1;
        } else {
            return 500;
        }
    }

    public final /* synthetic */ void f(Intent intent, Context context, boolean z, BroadcastReceiver.PendingResult pendingResult) {
        int g;
        try {
            Parcelable parcelableExtra = intent.getParcelableExtra("wrapped_intent");
            Intent intent2 = parcelableExtra instanceof Intent ? (Intent) parcelableExtra : null;
            if (intent2 != null) {
                g = e(context, intent2);
            } else {
                g = g(context, intent);
            }
            if (z) {
                pendingResult.setResultCode(g);
            }
        } finally {
            pendingResult.finish();
        }
    }

    public final int g(Context context, Intent intent) {
        com.google.android.gms.tasks.c<Void> c;
        if (intent.getExtras() == null) {
            return 500;
        }
        String stringExtra = intent.getStringExtra("google.message_id");
        if (TextUtils.isEmpty(stringExtra)) {
            c = d.e(null);
        } else {
            Bundle bundle = new Bundle();
            bundle.putString("google.message_id", stringExtra);
            c = pf5.e(context).c(2, bundle);
        }
        int b = b(context, new CloudMessage(intent));
        try {
            d.b(c, TimeUnit.SECONDS.toMillis(1L), TimeUnit.MILLISECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            String valueOf = String.valueOf(e);
            StringBuilder sb = new StringBuilder(valueOf.length() + 20);
            sb.append("Message ack failed: ");
            sb.append(valueOf);
        }
        return b;
    }

    @Override // android.content.BroadcastReceiver
    public final void onReceive(final Context context, final Intent intent) {
        if (intent == null) {
            return;
        }
        final boolean isOrderedBroadcast = isOrderedBroadcast();
        final BroadcastReceiver.PendingResult goAsync = goAsync();
        a().execute(new Runnable(this, intent, context, isOrderedBroadcast, goAsync) { // from class: uc5
            public final CloudMessagingReceiver a;
            public final Intent f0;
            public final Context g0;
            public final boolean h0;
            public final BroadcastReceiver.PendingResult i0;

            {
                this.a = this;
                this.f0 = intent;
                this.g0 = context;
                this.h0 = isOrderedBroadcast;
                this.i0 = goAsync;
            }

            @Override // java.lang.Runnable
            public final void run() {
                this.a.f(this.f0, this.g0, this.h0, this.i0);
            }
        });
    }
}
