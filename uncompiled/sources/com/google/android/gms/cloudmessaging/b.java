package com.google.android.gms.cloudmessaging;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcelable;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.cloudmessaging.b;
import com.google.android.gms.cloudmessaging.zza;
import com.google.android.gms.tasks.c;
import com.google.android.gms.tasks.d;
import java.io.IOException;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* compiled from: com.google.android.gms:play-services-cloud-messaging@@16.0.0 */
/* loaded from: classes.dex */
public class b {
    public static int h;
    public static PendingIntent i;
    public static final Executor j = d66.a;
    public final Context b;
    public final m46 c;
    public final ScheduledExecutorService d;
    public Messenger f;
    public zza g;
    public final vo3<String, n34<Bundle>> a = new vo3<>();
    public Messenger e = new Messenger(new z56(this, Looper.getMainLooper()));

    public b(Context context) {
        this.b = context;
        this.c = new m46(context);
        ScheduledThreadPoolExecutor scheduledThreadPoolExecutor = new ScheduledThreadPoolExecutor(1);
        scheduledThreadPoolExecutor.setKeepAliveTime(60L, TimeUnit.SECONDS);
        scheduledThreadPoolExecutor.allowCoreThreadTimeOut(true);
        this.d = scheduledThreadPoolExecutor;
    }

    public static final /* synthetic */ Bundle b(com.google.android.gms.tasks.c cVar) throws Exception {
        if (cVar.p()) {
            return (Bundle) cVar.l();
        }
        if (Log.isLoggable("Rpc", 3)) {
            String valueOf = String.valueOf(cVar.k());
            StringBuilder sb = new StringBuilder(valueOf.length() + 22);
            sb.append("Error making request: ");
            sb.append(valueOf);
        }
        throw new IOException("SERVICE_NOT_AVAILABLE", cVar.k());
    }

    public static final /* synthetic */ com.google.android.gms.tasks.c c(Bundle bundle) throws Exception {
        if (l(bundle)) {
            return d.e(null);
        }
        return d.e(bundle);
    }

    public static synchronized String e() {
        String num;
        synchronized (b.class) {
            int i2 = h;
            h = i2 + 1;
            num = Integer.toString(i2);
        }
        return num;
    }

    public static synchronized void g(Context context, Intent intent) {
        synchronized (b.class) {
            if (i == null) {
                Intent intent2 = new Intent();
                intent2.setPackage("com.google.example.invalidpackage");
                i = PendingIntent.getBroadcast(context, 0, intent2, 0);
            }
            intent.putExtra("app", i);
        }
    }

    public static boolean l(Bundle bundle) {
        return bundle != null && bundle.containsKey("google.messenger");
    }

    public com.google.android.gms.tasks.c<Bundle> a(final Bundle bundle) {
        if (this.c.c() >= 12000000) {
            return pf5.e(this.b).f(1, bundle).i(j, x46.a);
        }
        if (!(this.c.a() != 0)) {
            return d.d(new IOException("MISSING_INSTANCEID_SERVICE"));
        }
        return m(bundle).j(j, new com.google.android.gms.tasks.a(this, bundle) { // from class: j56
            public final b a;
            public final Bundle b;

            {
                this.a = this;
                this.b = bundle;
            }

            @Override // com.google.android.gms.tasks.a
            public final Object a(c cVar) {
                return this.a.d(this.b, cVar);
            }
        });
    }

    public final /* synthetic */ com.google.android.gms.tasks.c d(Bundle bundle, com.google.android.gms.tasks.c cVar) throws Exception {
        return (cVar.p() && l((Bundle) cVar.l())) ? m(bundle).r(j, p56.a) : cVar;
    }

    public final void h(Message message) {
        if (message != null) {
            Object obj = message.obj;
            if (obj instanceof Intent) {
                Intent intent = (Intent) obj;
                intent.setExtrasClassLoader(new zza.a());
                if (intent.hasExtra("google.messenger")) {
                    Parcelable parcelableExtra = intent.getParcelableExtra("google.messenger");
                    if (parcelableExtra instanceof zza) {
                        this.g = (zza) parcelableExtra;
                    }
                    if (parcelableExtra instanceof Messenger) {
                        this.f = (Messenger) parcelableExtra;
                    }
                }
                Intent intent2 = (Intent) message.obj;
                String action = intent2.getAction();
                if (!"com.google.android.c2dm.intent.REGISTRATION".equals(action)) {
                    if (Log.isLoggable("Rpc", 3)) {
                        String valueOf = String.valueOf(action);
                        if (valueOf.length() != 0) {
                            "Unexpected response action: ".concat(valueOf);
                            return;
                        }
                        return;
                    }
                    return;
                }
                String stringExtra = intent2.getStringExtra("registration_id");
                if (stringExtra == null) {
                    stringExtra = intent2.getStringExtra("unregistered");
                }
                if (stringExtra == null) {
                    String stringExtra2 = intent2.getStringExtra("error");
                    if (stringExtra2 == null) {
                        String valueOf2 = String.valueOf(intent2.getExtras());
                        StringBuilder sb = new StringBuilder(valueOf2.length() + 49);
                        sb.append("Unexpected response, no error or registration id ");
                        sb.append(valueOf2);
                        return;
                    }
                    if (Log.isLoggable("Rpc", 3) && stringExtra2.length() != 0) {
                        "Received InstanceID error ".concat(stringExtra2);
                    }
                    if (stringExtra2.startsWith("|")) {
                        String[] split = stringExtra2.split("\\|");
                        if (split.length > 2 && "ID".equals(split[1])) {
                            String str = split[2];
                            String str2 = split[3];
                            if (str2.startsWith(":")) {
                                str2 = str2.substring(1);
                            }
                            j(str, intent2.putExtra("error", str2).getExtras());
                            return;
                        } else if (stringExtra2.length() != 0) {
                            "Unexpected structured response ".concat(stringExtra2);
                            return;
                        } else {
                            return;
                        }
                    }
                    synchronized (this.a) {
                        for (int i2 = 0; i2 < this.a.size(); i2++) {
                            j(this.a.i(i2), intent2.getExtras());
                        }
                    }
                    return;
                }
                Matcher matcher = Pattern.compile("\\|ID\\|([^|]+)\\|:?+(.*)").matcher(stringExtra);
                if (!matcher.matches()) {
                    if (!Log.isLoggable("Rpc", 3) || stringExtra.length() == 0) {
                        return;
                    }
                    "Unexpected response string: ".concat(stringExtra);
                    return;
                }
                String group = matcher.group(1);
                String group2 = matcher.group(2);
                if (group != null) {
                    Bundle extras = intent2.getExtras();
                    extras.putString("registration_id", group2);
                    j(group, extras);
                }
            }
        }
    }

    public final void j(String str, Bundle bundle) {
        synchronized (this.a) {
            n34<Bundle> remove = this.a.remove(str);
            if (remove == null) {
                String valueOf = String.valueOf(str);
                if (valueOf.length() != 0) {
                    "Missing callback for ".concat(valueOf);
                }
                return;
            }
            remove.c(bundle);
        }
    }

    public final /* synthetic */ void k(String str, ScheduledFuture scheduledFuture, com.google.android.gms.tasks.c cVar) {
        synchronized (this.a) {
            this.a.remove(str);
        }
        scheduledFuture.cancel(false);
    }

    public final com.google.android.gms.tasks.c<Bundle> m(Bundle bundle) {
        final String e = e();
        final n34<Bundle> n34Var = new n34<>();
        synchronized (this.a) {
            this.a.put(e, n34Var);
        }
        Intent intent = new Intent();
        intent.setPackage("com.google.android.gms");
        if (this.c.a() == 2) {
            intent.setAction("com.google.iid.TOKEN_REQUEST");
        } else {
            intent.setAction("com.google.android.c2dm.intent.REGISTER");
        }
        intent.putExtras(bundle);
        g(this.b, intent);
        StringBuilder sb = new StringBuilder(String.valueOf(e).length() + 5);
        sb.append("|ID|");
        sb.append(e);
        sb.append("|");
        intent.putExtra("kid", sb.toString());
        if (Log.isLoggable("Rpc", 3)) {
            String valueOf = String.valueOf(intent.getExtras());
            StringBuilder sb2 = new StringBuilder(valueOf.length() + 8);
            sb2.append("Sending ");
            sb2.append(valueOf);
        }
        intent.putExtra("google.messenger", this.e);
        if (this.f != null || this.g != null) {
            Message obtain = Message.obtain();
            obtain.obj = intent;
            try {
                Messenger messenger = this.f;
                if (messenger != null) {
                    messenger.send(obtain);
                } else {
                    this.g.b(obtain);
                }
            } catch (RemoteException unused) {
            }
            final ScheduledFuture<?> schedule = this.d.schedule(new Runnable(n34Var) { // from class: c56
                public final n34 a;

                {
                    this.a = n34Var;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    this.a.d(new IOException("TIMEOUT"));
                }
            }, 30L, TimeUnit.SECONDS);
            n34Var.a().c(j, new im2(this, e, schedule) { // from class: u56
                public final b a;
                public final String b;
                public final ScheduledFuture c;

                {
                    this.a = this;
                    this.b = e;
                    this.c = schedule;
                }

                @Override // defpackage.im2
                public final void a(c cVar) {
                    this.a.k(this.b, this.c, cVar);
                }
            });
            return n34Var.a();
        }
        if (this.c.a() == 2) {
            this.b.sendBroadcast(intent);
        } else {
            this.b.startService(intent);
        }
        final ScheduledFuture schedule2 = this.d.schedule(new Runnable(n34Var) { // from class: c56
            public final n34 a;

            {
                this.a = n34Var;
            }

            @Override // java.lang.Runnable
            public final void run() {
                this.a.d(new IOException("TIMEOUT"));
            }
        }, 30L, TimeUnit.SECONDS);
        n34Var.a().c(j, new im2(this, e, schedule2) { // from class: u56
            public final b a;
            public final String b;
            public final ScheduledFuture c;

            {
                this.a = this;
                this.b = e;
                this.c = schedule2;
            }

            @Override // defpackage.im2
            public final void a(c cVar) {
                this.a.k(this.b, this.c, cVar);
            }
        });
        return n34Var.a();
    }
}
