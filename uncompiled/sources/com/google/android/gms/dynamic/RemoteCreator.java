package com.google.android.gms.dynamic;

import android.content.Context;
import android.os.IBinder;
import androidx.annotation.RecentlyNonNull;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* loaded from: classes.dex */
public abstract class RemoteCreator<T> {
    public final String a;
    public T b;

    /* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
    /* loaded from: classes.dex */
    public static class RemoteCreatorException extends Exception {
        public RemoteCreatorException(@RecentlyNonNull String str) {
            super(str);
        }

        public RemoteCreatorException(@RecentlyNonNull String str, @RecentlyNonNull Throwable th) {
            super(str, th);
        }
    }

    public RemoteCreator(@RecentlyNonNull String str) {
        this.a = str;
    }

    @RecentlyNonNull
    public abstract T a(@RecentlyNonNull IBinder iBinder);

    @RecentlyNonNull
    public final T b(@RecentlyNonNull Context context) throws RemoteCreatorException {
        if (this.b == null) {
            zt2.j(context);
            Context d = qh1.d(context);
            if (d != null) {
                try {
                    this.b = a((IBinder) d.getClassLoader().loadClass(this.a).newInstance());
                } catch (ClassNotFoundException e) {
                    throw new RemoteCreatorException("Could not load creator class.", e);
                } catch (IllegalAccessException e2) {
                    throw new RemoteCreatorException("Could not access creator.", e2);
                } catch (InstantiationException e3) {
                    throw new RemoteCreatorException("Could not instantiate creator.", e3);
                }
            } else {
                throw new RemoteCreatorException("Could not get remote context.");
            }
        }
        return this.b;
    }
}
