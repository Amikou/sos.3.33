package com.google.android.gms.signin.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public final class e extends com.google.android.gms.internal.base.b implements c {
    public e(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.signin.internal.ISignInService");
    }

    @Override // com.google.android.gms.signin.internal.c
    public final void M0(zak zakVar, a aVar) throws RemoteException {
        Parcel b = b();
        o15.c(b, zakVar);
        o15.b(b, aVar);
        G1(12, b);
    }

    @Override // com.google.android.gms.signin.internal.c
    public final void S(com.google.android.gms.common.internal.d dVar, int i, boolean z) throws RemoteException {
        Parcel b = b();
        o15.b(b, dVar);
        b.writeInt(i);
        o15.d(b, z);
        G1(9, b);
    }

    @Override // com.google.android.gms.signin.internal.c
    public final void e(int i) throws RemoteException {
        Parcel b = b();
        b.writeInt(i);
        G1(7, b);
    }
}
