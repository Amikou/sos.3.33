package com.google.android.gms.signin.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public final class zag extends AbstractSafeParcelable implements l83 {
    public static final Parcelable.Creator<zag> CREATOR = new x15();
    public final List<String> a;
    public final String f0;

    public zag(List<String> list, String str) {
        this.a = list;
        this.f0 = str;
    }

    @Override // defpackage.l83
    public final Status e() {
        if (this.f0 != null) {
            return Status.j0;
        }
        return Status.m0;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int a = yb3.a(parcel);
        yb3.u(parcel, 1, this.a, false);
        yb3.s(parcel, 2, this.f0, false);
        yb3.b(parcel, a);
    }
}
