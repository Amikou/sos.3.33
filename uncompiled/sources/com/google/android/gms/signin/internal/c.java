package com.google.android.gms.signin.internal;

import android.os.IInterface;
import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public interface c extends IInterface {
    void M0(zak zakVar, a aVar) throws RemoteException;

    void S(com.google.android.gms.common.internal.d dVar, int i, boolean z) throws RemoteException;

    void e(int i) throws RemoteException;
}
