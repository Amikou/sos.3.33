package com.google.android.gms.signin.internal;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Status;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public interface a extends IInterface {
    void J0(zag zagVar) throws RemoteException;

    void R0(Status status) throws RemoteException;

    void b1(ConnectionResult connectionResult, zab zabVar) throws RemoteException;

    void d1(zam zamVar) throws RemoteException;

    void e1(Status status) throws RemoteException;

    void j(Status status, GoogleSignInAccount googleSignInAccount) throws RemoteException;
}
