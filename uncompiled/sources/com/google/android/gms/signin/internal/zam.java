package com.google.android.gms.signin.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zas;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public final class zam extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zam> CREATOR = new f25();
    public final int a;
    public final ConnectionResult f0;
    public final zas g0;

    public zam(int i, ConnectionResult connectionResult, zas zasVar) {
        this.a = i;
        this.f0 = connectionResult;
        this.g0 = zasVar;
    }

    public final ConnectionResult I1() {
        return this.f0;
    }

    public final zas J1() {
        return this.g0;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int a = yb3.a(parcel);
        yb3.m(parcel, 1, this.a);
        yb3.r(parcel, 2, this.f0, i, false);
        yb3.r(parcel, 3, this.g0, i, false);
        yb3.b(parcel, a);
    }

    public zam(int i) {
        this(new ConnectionResult(8, null), null);
    }

    public zam(ConnectionResult connectionResult, zas zasVar) {
        this(1, connectionResult, null);
    }
}
