package com.google.android.gms.signin.internal;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public final class zab extends AbstractSafeParcelable implements l83 {
    public static final Parcelable.Creator<zab> CREATOR = new mz4();
    public final int a;
    public int f0;
    public Intent g0;

    public zab(int i, int i2, Intent intent) {
        this.a = i;
        this.f0 = i2;
        this.g0 = intent;
    }

    @Override // defpackage.l83
    public final Status e() {
        if (this.f0 == 0) {
            return Status.j0;
        }
        return Status.m0;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int a = yb3.a(parcel);
        yb3.m(parcel, 1, this.a);
        yb3.m(parcel, 2, this.f0);
        yb3.r(parcel, 3, this.g0, i, false);
        yb3.b(parcel, a);
    }

    public zab() {
        this(0, null);
    }

    public zab(int i, Intent intent) {
        this(2, 0, null);
    }
}
