package com.google.android.gms.signin.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zar;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public final class zak extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zak> CREATOR = new a25();
    public final int a;
    public final zar f0;

    public zak(int i, zar zarVar) {
        this.a = i;
        this.f0 = zarVar;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int a = yb3.a(parcel);
        yb3.m(parcel, 1, this.a);
        yb3.r(parcel, 2, this.f0, i, false);
        yb3.b(parcel, a);
    }

    public zak(zar zarVar) {
        this(1, zarVar);
    }
}
