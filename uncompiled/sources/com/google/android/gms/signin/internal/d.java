package com.google.android.gms.signin.internal;

import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Status;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public abstract class d extends com.google.android.gms.internal.base.a implements a {
    public d() {
        super("com.google.android.gms.signin.internal.ISignInCallbacks");
    }

    @Override // com.google.android.gms.internal.base.a
    public final boolean F1(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        switch (i) {
            case 3:
                b1((ConnectionResult) o15.a(parcel, ConnectionResult.CREATOR), (zab) o15.a(parcel, zab.CREATOR));
                break;
            case 4:
                R0((Status) o15.a(parcel, Status.CREATOR));
                break;
            case 5:
            default:
                return false;
            case 6:
                e1((Status) o15.a(parcel, Status.CREATOR));
                break;
            case 7:
                j((Status) o15.a(parcel, Status.CREATOR), (GoogleSignInAccount) o15.a(parcel, GoogleSignInAccount.CREATOR));
                break;
            case 8:
                d1((zam) o15.a(parcel, zam.CREATOR));
                break;
            case 9:
                J0((zag) o15.a(parcel, zag.CREATOR));
                break;
        }
        parcel2.writeNoException();
        return true;
    }
}
