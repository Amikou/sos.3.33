package com.google.android.gms.tasks;

import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/* compiled from: com.google.android.gms:play-services-tasks@@17.2.0 */
/* loaded from: classes.dex */
public final class d {

    /* compiled from: com.google.android.gms:play-services-tasks@@17.2.0 */
    /* loaded from: classes.dex */
    public interface a<T> extends hm2, nm2, um2<T> {
    }

    /* compiled from: com.google.android.gms:play-services-tasks@@17.2.0 */
    /* loaded from: classes.dex */
    public static final class c implements a {
        public final Object a = new Object();
        public final int b;
        public final e<Void> c;
        public int d;
        public int e;
        public int f;
        public Exception g;
        public boolean h;

        public c(int i, e<Void> eVar) {
            this.b = i;
            this.c = eVar;
        }

        @Override // defpackage.um2
        public final void a(Object obj) {
            synchronized (this.a) {
                this.d++;
                c();
            }
        }

        @Override // defpackage.nm2
        public final void b(Exception exc) {
            synchronized (this.a) {
                this.e++;
                this.g = exc;
                c();
            }
        }

        public final void c() {
            if (this.d + this.e + this.f == this.b) {
                if (this.g != null) {
                    e<Void> eVar = this.c;
                    int i = this.e;
                    int i2 = this.b;
                    StringBuilder sb = new StringBuilder(54);
                    sb.append(i);
                    sb.append(" out of ");
                    sb.append(i2);
                    sb.append(" underlying tasks failed");
                    eVar.s(new ExecutionException(sb.toString(), this.g));
                } else if (this.h) {
                    this.c.u();
                } else {
                    this.c.t(null);
                }
            }
        }

        @Override // defpackage.hm2
        public final void d() {
            synchronized (this.a) {
                this.f++;
                this.h = true;
                c();
            }
        }
    }

    public static <TResult> TResult a(com.google.android.gms.tasks.c<TResult> cVar) throws ExecutionException, InterruptedException {
        zt2.h();
        zt2.k(cVar, "Task must not be null");
        if (cVar.o()) {
            return (TResult) h(cVar);
        }
        b bVar = new b(null);
        i(cVar, bVar);
        bVar.c();
        return (TResult) h(cVar);
    }

    public static <TResult> TResult b(com.google.android.gms.tasks.c<TResult> cVar, long j, TimeUnit timeUnit) throws ExecutionException, InterruptedException, TimeoutException {
        zt2.h();
        zt2.k(cVar, "Task must not be null");
        zt2.k(timeUnit, "TimeUnit must not be null");
        if (cVar.o()) {
            return (TResult) h(cVar);
        }
        b bVar = new b(null);
        i(cVar, bVar);
        if (bVar.e(j, timeUnit)) {
            return (TResult) h(cVar);
        }
        throw new TimeoutException("Timed out waiting for Task");
    }

    @Deprecated
    public static <TResult> com.google.android.gms.tasks.c<TResult> c(Executor executor, Callable<TResult> callable) {
        zt2.k(executor, "Executor must not be null");
        zt2.k(callable, "Callback must not be null");
        e eVar = new e();
        executor.execute(new y56(eVar, callable));
        return eVar;
    }

    public static <TResult> com.google.android.gms.tasks.c<TResult> d(Exception exc) {
        e eVar = new e();
        eVar.s(exc);
        return eVar;
    }

    public static <TResult> com.google.android.gms.tasks.c<TResult> e(TResult tresult) {
        e eVar = new e();
        eVar.t(tresult);
        return eVar;
    }

    public static com.google.android.gms.tasks.c<Void> f(Collection<? extends com.google.android.gms.tasks.c<?>> collection) {
        if (collection != null && !collection.isEmpty()) {
            for (com.google.android.gms.tasks.c<?> cVar : collection) {
                Objects.requireNonNull(cVar, "null tasks are not accepted");
            }
            e eVar = new e();
            c cVar2 = new c(collection.size(), eVar);
            for (com.google.android.gms.tasks.c<?> cVar3 : collection) {
                i(cVar3, cVar2);
            }
            return eVar;
        }
        return e(null);
    }

    public static com.google.android.gms.tasks.c<Void> g(com.google.android.gms.tasks.c<?>... cVarArr) {
        if (cVarArr != null && cVarArr.length != 0) {
            return f(Arrays.asList(cVarArr));
        }
        return e(null);
    }

    public static <TResult> TResult h(com.google.android.gms.tasks.c<TResult> cVar) throws ExecutionException {
        if (cVar.p()) {
            return cVar.l();
        }
        if (cVar.n()) {
            throw new CancellationException("Task is already canceled");
        }
        throw new ExecutionException(cVar.k());
    }

    public static <T> void i(com.google.android.gms.tasks.c<T> cVar, a<? super T> aVar) {
        Executor executor = s34.b;
        cVar.g(executor, aVar);
        cVar.e(executor, aVar);
        cVar.a(executor, aVar);
    }

    /* compiled from: com.google.android.gms:play-services-tasks@@17.2.0 */
    /* loaded from: classes.dex */
    public static final class b implements a {
        public final CountDownLatch a;

        public b() {
            this.a = new CountDownLatch(1);
        }

        @Override // defpackage.um2
        public final void a(Object obj) {
            this.a.countDown();
        }

        @Override // defpackage.nm2
        public final void b(Exception exc) {
            this.a.countDown();
        }

        public final void c() throws InterruptedException {
            this.a.await();
        }

        @Override // defpackage.hm2
        public final void d() {
            this.a.countDown();
        }

        public final boolean e(long j, TimeUnit timeUnit) throws InterruptedException {
            return this.a.await(j, timeUnit);
        }

        public /* synthetic */ b(y56 y56Var) {
            this();
        }
    }
}
