package com.google.android.gms.tasks;

import java.util.concurrent.Executor;

/* compiled from: com.google.android.gms:play-services-tasks@@17.2.0 */
/* loaded from: classes.dex */
public abstract class c<TResult> {
    public c<TResult> a(Executor executor, hm2 hm2Var) {
        throw new UnsupportedOperationException("addOnCanceledListener is not implemented");
    }

    public c<TResult> b(im2<TResult> im2Var) {
        throw new UnsupportedOperationException("addOnCompleteListener is not implemented");
    }

    public c<TResult> c(Executor executor, im2<TResult> im2Var) {
        throw new UnsupportedOperationException("addOnCompleteListener is not implemented");
    }

    public abstract c<TResult> d(nm2 nm2Var);

    public abstract c<TResult> e(Executor executor, nm2 nm2Var);

    public abstract c<TResult> f(um2<? super TResult> um2Var);

    public abstract c<TResult> g(Executor executor, um2<? super TResult> um2Var);

    public <TContinuationResult> c<TContinuationResult> h(a<TResult, TContinuationResult> aVar) {
        throw new UnsupportedOperationException("continueWith is not implemented");
    }

    public <TContinuationResult> c<TContinuationResult> i(Executor executor, a<TResult, TContinuationResult> aVar) {
        throw new UnsupportedOperationException("continueWith is not implemented");
    }

    public <TContinuationResult> c<TContinuationResult> j(Executor executor, a<TResult, c<TContinuationResult>> aVar) {
        throw new UnsupportedOperationException("continueWithTask is not implemented");
    }

    public abstract Exception k();

    public abstract TResult l();

    public abstract <X extends Throwable> TResult m(Class<X> cls) throws Throwable;

    public abstract boolean n();

    public abstract boolean o();

    public abstract boolean p();

    public <TContinuationResult> c<TContinuationResult> q(b<TResult, TContinuationResult> bVar) {
        throw new UnsupportedOperationException("onSuccessTask is not implemented");
    }

    public <TContinuationResult> c<TContinuationResult> r(Executor executor, b<TResult, TContinuationResult> bVar) {
        throw new UnsupportedOperationException("onSuccessTask is not implemented");
    }
}
