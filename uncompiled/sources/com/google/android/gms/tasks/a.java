package com.google.android.gms.tasks;

/* compiled from: com.google.android.gms:play-services-tasks@@17.2.0 */
/* loaded from: classes.dex */
public interface a<TResult, TContinuationResult> {
    TContinuationResult a(c<TResult> cVar) throws Exception;
}
