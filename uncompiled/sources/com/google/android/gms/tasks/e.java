package com.google.android.gms.tasks;

import java.util.concurrent.CancellationException;
import java.util.concurrent.Executor;

/* compiled from: com.google.android.gms:play-services-tasks@@17.2.0 */
/* loaded from: classes.dex */
public final class e<TResult> extends c<TResult> {
    public final Object a = new Object();
    public final m36<TResult> b = new m36<>();
    public boolean c;
    public volatile boolean d;
    public TResult e;
    public Exception f;

    public final void A() {
        synchronized (this.a) {
            if (this.c) {
                this.b.a(this);
            }
        }
    }

    @Override // com.google.android.gms.tasks.c
    public final c<TResult> a(Executor executor, hm2 hm2Var) {
        this.b.b(new pm5(i56.a(executor), hm2Var));
        A();
        return this;
    }

    @Override // com.google.android.gms.tasks.c
    public final c<TResult> b(im2<TResult> im2Var) {
        return c(s34.a, im2Var);
    }

    @Override // com.google.android.gms.tasks.c
    public final c<TResult> c(Executor executor, im2<TResult> im2Var) {
        this.b.b(new up5(i56.a(executor), im2Var));
        A();
        return this;
    }

    @Override // com.google.android.gms.tasks.c
    public final c<TResult> d(nm2 nm2Var) {
        return e(s34.a, nm2Var);
    }

    @Override // com.google.android.gms.tasks.c
    public final c<TResult> e(Executor executor, nm2 nm2Var) {
        this.b.b(new ix5(i56.a(executor), nm2Var));
        A();
        return this;
    }

    @Override // com.google.android.gms.tasks.c
    public final c<TResult> f(um2<? super TResult> um2Var) {
        return g(s34.a, um2Var);
    }

    @Override // com.google.android.gms.tasks.c
    public final c<TResult> g(Executor executor, um2<? super TResult> um2Var) {
        this.b.b(new az5(i56.a(executor), um2Var));
        A();
        return this;
    }

    @Override // com.google.android.gms.tasks.c
    public final <TContinuationResult> c<TContinuationResult> h(a<TResult, TContinuationResult> aVar) {
        return i(s34.a, aVar);
    }

    @Override // com.google.android.gms.tasks.c
    public final <TContinuationResult> c<TContinuationResult> i(Executor executor, a<TResult, TContinuationResult> aVar) {
        e eVar = new e();
        this.b.b(new da5(i56.a(executor), aVar, eVar));
        A();
        return eVar;
    }

    @Override // com.google.android.gms.tasks.c
    public final <TContinuationResult> c<TContinuationResult> j(Executor executor, a<TResult, c<TContinuationResult>> aVar) {
        e eVar = new e();
        this.b.b(new rc5(i56.a(executor), aVar, eVar));
        A();
        return eVar;
    }

    @Override // com.google.android.gms.tasks.c
    public final Exception k() {
        Exception exc;
        synchronized (this.a) {
            exc = this.f;
        }
        return exc;
    }

    @Override // com.google.android.gms.tasks.c
    public final TResult l() {
        TResult tresult;
        synchronized (this.a) {
            v();
            z();
            if (this.f == null) {
                tresult = this.e;
            } else {
                throw new RuntimeExecutionException(this.f);
            }
        }
        return tresult;
    }

    @Override // com.google.android.gms.tasks.c
    public final <X extends Throwable> TResult m(Class<X> cls) throws Throwable {
        TResult tresult;
        synchronized (this.a) {
            v();
            z();
            if (!cls.isInstance(this.f)) {
                if (this.f == null) {
                    tresult = this.e;
                } else {
                    throw new RuntimeExecutionException(this.f);
                }
            } else {
                throw cls.cast(this.f);
            }
        }
        return tresult;
    }

    @Override // com.google.android.gms.tasks.c
    public final boolean n() {
        return this.d;
    }

    @Override // com.google.android.gms.tasks.c
    public final boolean o() {
        boolean z;
        synchronized (this.a) {
            z = this.c;
        }
        return z;
    }

    @Override // com.google.android.gms.tasks.c
    public final boolean p() {
        boolean z;
        synchronized (this.a) {
            z = this.c && !this.d && this.f == null;
        }
        return z;
    }

    @Override // com.google.android.gms.tasks.c
    public final <TContinuationResult> c<TContinuationResult> q(b<TResult, TContinuationResult> bVar) {
        return r(s34.a, bVar);
    }

    @Override // com.google.android.gms.tasks.c
    public final <TContinuationResult> c<TContinuationResult> r(Executor executor, b<TResult, TContinuationResult> bVar) {
        e eVar = new e();
        this.b.b(new f26(i56.a(executor), bVar, eVar));
        A();
        return eVar;
    }

    public final void s(Exception exc) {
        zt2.k(exc, "Exception must not be null");
        synchronized (this.a) {
            y();
            this.c = true;
            this.f = exc;
        }
        this.b.a(this);
    }

    public final void t(TResult tresult) {
        synchronized (this.a) {
            y();
            this.c = true;
            this.e = tresult;
        }
        this.b.a(this);
    }

    public final boolean u() {
        synchronized (this.a) {
            if (this.c) {
                return false;
            }
            this.c = true;
            this.d = true;
            this.b.a(this);
            return true;
        }
    }

    public final void v() {
        zt2.n(this.c, "Task is not yet complete");
    }

    public final boolean w(Exception exc) {
        zt2.k(exc, "Exception must not be null");
        synchronized (this.a) {
            if (this.c) {
                return false;
            }
            this.c = true;
            this.f = exc;
            this.b.a(this);
            return true;
        }
    }

    public final boolean x(TResult tresult) {
        synchronized (this.a) {
            if (this.c) {
                return false;
            }
            this.c = true;
            this.e = tresult;
            this.b.a(this);
            return true;
        }
    }

    public final void y() {
        if (this.c) {
            throw DuplicateTaskCompletionException.of(this);
        }
    }

    public final void z() {
        if (this.d) {
            throw new CancellationException("Task is already canceled.");
        }
    }
}
