package com.google.android.gms.clearcut;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.clearcut.a;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.internal.clearcut.q0;
import com.google.android.gms.internal.clearcut.zzr;
import com.google.android.gms.phenotype.ExperimentTokens;
import java.util.Arrays;

/* loaded from: classes.dex */
public final class zze extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zze> CREATOR = new ii5();
    public zzr a;
    public byte[] f0;
    public int[] g0;
    public String[] h0;
    public int[] i0;
    public byte[][] j0;
    public ExperimentTokens[] k0;
    public boolean l0;
    public final q0 m0;
    public final a.c n0;
    public final a.c o0;

    public zze(zzr zzrVar, q0 q0Var, a.c cVar, a.c cVar2, int[] iArr, String[] strArr, int[] iArr2, byte[][] bArr, ExperimentTokens[] experimentTokensArr, boolean z) {
        this.a = zzrVar;
        this.m0 = q0Var;
        this.n0 = cVar;
        this.o0 = null;
        this.g0 = iArr;
        this.h0 = null;
        this.i0 = iArr2;
        this.j0 = null;
        this.k0 = null;
        this.l0 = z;
    }

    public zze(zzr zzrVar, byte[] bArr, int[] iArr, String[] strArr, int[] iArr2, byte[][] bArr2, boolean z, ExperimentTokens[] experimentTokensArr) {
        this.a = zzrVar;
        this.f0 = bArr;
        this.g0 = iArr;
        this.h0 = strArr;
        this.m0 = null;
        this.n0 = null;
        this.o0 = null;
        this.i0 = iArr2;
        this.j0 = bArr2;
        this.k0 = experimentTokensArr;
        this.l0 = z;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof zze) {
            zze zzeVar = (zze) obj;
            if (pl2.a(this.a, zzeVar.a) && Arrays.equals(this.f0, zzeVar.f0) && Arrays.equals(this.g0, zzeVar.g0) && Arrays.equals(this.h0, zzeVar.h0) && pl2.a(this.m0, zzeVar.m0) && pl2.a(this.n0, zzeVar.n0) && pl2.a(this.o0, zzeVar.o0) && Arrays.equals(this.i0, zzeVar.i0) && Arrays.deepEquals(this.j0, zzeVar.j0) && Arrays.equals(this.k0, zzeVar.k0) && this.l0 == zzeVar.l0) {
                return true;
            }
        }
        return false;
    }

    public final int hashCode() {
        return pl2.b(this.a, this.f0, this.g0, this.h0, this.m0, this.n0, this.o0, this.i0, this.j0, this.k0, Boolean.valueOf(this.l0));
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("LogEventParcelable[");
        sb.append(this.a);
        sb.append(", LogEventBytes: ");
        byte[] bArr = this.f0;
        sb.append(bArr == null ? null : new String(bArr));
        sb.append(", TestCodes: ");
        sb.append(Arrays.toString(this.g0));
        sb.append(", MendelPackages: ");
        sb.append(Arrays.toString(this.h0));
        sb.append(", LogEvent: ");
        sb.append(this.m0);
        sb.append(", ExtensionProducer: ");
        sb.append(this.n0);
        sb.append(", VeProducer: ");
        sb.append(this.o0);
        sb.append(", ExperimentIDs: ");
        sb.append(Arrays.toString(this.i0));
        sb.append(", ExperimentTokens: ");
        sb.append(Arrays.toString(this.j0));
        sb.append(", ExperimentTokensParcelables: ");
        sb.append(Arrays.toString(this.k0));
        sb.append(", AddPhenotypeExperimentTokens: ");
        sb.append(this.l0);
        sb.append("]");
        return sb.toString();
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int a = yb3.a(parcel);
        yb3.r(parcel, 2, this.a, i, false);
        yb3.f(parcel, 3, this.f0, false);
        yb3.n(parcel, 4, this.g0, false);
        yb3.t(parcel, 5, this.h0, false);
        yb3.n(parcel, 6, this.i0, false);
        yb3.g(parcel, 7, this.j0, false);
        yb3.c(parcel, 8, this.l0);
        yb3.v(parcel, 9, this.k0, i, false);
        yb3.b(parcel, a);
    }
}
