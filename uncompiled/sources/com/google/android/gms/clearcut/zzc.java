package com.google.android.gms.clearcut;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

/* loaded from: classes.dex */
public final class zzc extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzc> CREATOR = new dd5();
    public final boolean a;
    public final long f0;
    public final long g0;

    public zzc(boolean z, long j, long j2) {
        this.a = z;
        this.f0 = j;
        this.g0 = j2;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof zzc) {
            zzc zzcVar = (zzc) obj;
            if (this.a == zzcVar.a && this.f0 == zzcVar.f0 && this.g0 == zzcVar.g0) {
                return true;
            }
        }
        return false;
    }

    public final int hashCode() {
        return pl2.b(Boolean.valueOf(this.a), Long.valueOf(this.f0), Long.valueOf(this.g0));
    }

    public final String toString() {
        return "CollectForDebugParcelable[skipPersistentStorage: " + this.a + ",collectForDebugStartTimeMillis: " + this.f0 + ",collectForDebugExpiryTimeMillis: " + this.g0 + "]";
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int a = yb3.a(parcel);
        yb3.c(parcel, 1, this.a);
        yb3.o(parcel, 2, this.g0);
        yb3.o(parcel, 3, this.f0);
        yb3.b(parcel, a);
    }
}
