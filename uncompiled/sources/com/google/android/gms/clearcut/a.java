package com.google.android.gms.clearcut;

import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.a;
import com.google.android.gms.internal.clearcut.q0;
import com.google.android.gms.internal.clearcut.w0;
import com.google.android.gms.internal.clearcut.zzge$zzv$zzb;
import com.google.android.gms.internal.clearcut.zzr;
import java.util.ArrayList;
import java.util.TimeZone;

/* loaded from: classes.dex */
public final class a {
    public static final a.g<as5> m;
    public static final a.AbstractC0106a<as5, Object> n;
    @Deprecated
    public static final com.google.android.gms.common.api.a<Object> o;
    public final Context a;
    public final String b;
    public final int c;
    public String d;
    public int e;
    public String f;
    public final boolean g;
    public zzge$zzv$zzb h;
    public final j75 i;
    public final rz j;
    public d k;
    public final b l;

    /* renamed from: com.google.android.gms.clearcut.a$a  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public class C0104a {
        public int a;
        public String b;
        public String c;
        public String d;
        public zzge$zzv$zzb e;
        public boolean f;
        public final q0 g;
        public boolean h;

        public C0104a(a aVar, byte[] bArr) {
            this(bArr, (c) null);
        }

        public C0104a(byte[] bArr, c cVar) {
            this.a = a.this.e;
            this.b = a.this.d;
            this.c = a.this.f;
            this.d = null;
            this.e = a.this.h;
            this.f = true;
            q0 q0Var = new q0();
            this.g = q0Var;
            this.h = false;
            this.c = a.this.f;
            this.d = null;
            q0Var.z0 = e45.a(a.this.a);
            q0Var.g0 = a.this.j.a();
            q0Var.h0 = a.this.j.b();
            d unused = a.this.k;
            q0Var.t0 = TimeZone.getDefault().getOffset(q0Var.g0) / 1000;
            if (bArr != null) {
                q0Var.o0 = bArr;
            }
        }

        public /* synthetic */ C0104a(a aVar, byte[] bArr, p35 p35Var) {
            this(aVar, bArr);
        }

        public void a() {
            if (this.h) {
                throw new IllegalStateException("do not reuse LogEventBuilder");
            }
            this.h = true;
            zze zzeVar = new zze(new zzr(a.this.b, a.this.c, this.a, this.b, this.c, this.d, a.this.g, this.e), this.g, null, null, a.d(null), null, a.d(null), null, null, this.f);
            if (a.this.l.a(zzeVar)) {
                a.this.i.a(zzeVar);
            } else {
                hq2.a(Status.j0, null);
            }
        }

        public C0104a b(int i) {
            this.g.j0 = i;
            return this;
        }
    }

    /* loaded from: classes.dex */
    public interface b {
        boolean a(zze zzeVar);
    }

    /* loaded from: classes.dex */
    public interface c {
        byte[] zza();
    }

    /* loaded from: classes.dex */
    public static class d {
    }

    static {
        a.g<as5> gVar = new a.g<>();
        m = gVar;
        p35 p35Var = new p35();
        n = p35Var;
        o = new com.google.android.gms.common.api.a<>("ClearcutLogger.API", p35Var, gVar);
    }

    public a(Context context, int i, String str, String str2, String str3, boolean z, j75 j75Var, rz rzVar, d dVar, b bVar) {
        this.e = -1;
        zzge$zzv$zzb zzge_zzv_zzb = zzge$zzv$zzb.DEFAULT;
        this.h = zzge_zzv_zzb;
        this.a = context;
        this.b = context.getPackageName();
        this.c = b(context);
        this.e = -1;
        this.d = str;
        this.f = str2;
        this.g = z;
        this.i = j75Var;
        this.j = rzVar;
        this.k = new d();
        this.h = zzge_zzv_zzb;
        this.l = bVar;
        if (z) {
            zt2.b(str2 == null, "can't be anonymous with an upload account");
        }
    }

    public a(Context context, String str, String str2) {
        this(context, -1, str, str2, null, false, kf5.m(context), mi0.c(), null, new w0(context));
    }

    public static int b(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            Log.wtf("ClearcutLogger", "This can't happen.", e);
            return 0;
        }
    }

    public static int[] d(ArrayList<Integer> arrayList) {
        if (arrayList == null) {
            return null;
        }
        int[] iArr = new int[arrayList.size()];
        int size = arrayList.size();
        int i = 0;
        int i2 = 0;
        while (i < size) {
            Integer num = arrayList.get(i);
            i++;
            iArr[i2] = num.intValue();
            i2++;
        }
        return iArr;
    }

    public final C0104a a(byte[] bArr) {
        return new C0104a(this, bArr, (p35) null);
    }
}
