package com.google.android.gms.auth.api.signin;

import android.accounts.Account;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import androidx.annotation.RecentlyNonNull;
import androidx.annotation.RecentlyNullable;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public class GoogleSignInAccount extends AbstractSafeParcelable implements ReflectedParcelable {
    @RecentlyNonNull
    public static final Parcelable.Creator<GoogleSignInAccount> CREATOR = new p05();
    public static rz r0 = mi0.c();
    public final int a;
    public String f0;
    public String g0;
    public String h0;
    public String i0;
    public Uri j0;
    public String k0;
    public long l0;
    public String m0;
    public List<Scope> n0;
    public String o0;
    public String p0;
    public Set<Scope> q0 = new HashSet();

    public GoogleSignInAccount(int i, String str, String str2, String str3, String str4, Uri uri, String str5, long j, String str6, List<Scope> list, String str7, String str8) {
        this.a = i;
        this.f0 = str;
        this.g0 = str2;
        this.h0 = str3;
        this.i0 = str4;
        this.j0 = uri;
        this.k0 = str5;
        this.l0 = j;
        this.m0 = str6;
        this.n0 = list;
        this.o0 = str7;
        this.p0 = str8;
    }

    @RecentlyNullable
    public static GoogleSignInAccount R1(String str) throws JSONException {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        JSONObject jSONObject = new JSONObject(str);
        String optString = jSONObject.optString("photoUrl");
        Uri parse = !TextUtils.isEmpty(optString) ? Uri.parse(optString) : null;
        long parseLong = Long.parseLong(jSONObject.getString("expirationTime"));
        HashSet hashSet = new HashSet();
        JSONArray jSONArray = jSONObject.getJSONArray("grantedScopes");
        int length = jSONArray.length();
        for (int i = 0; i < length; i++) {
            hashSet.add(new Scope(jSONArray.getString(i)));
        }
        GoogleSignInAccount S1 = S1(jSONObject.optString("id"), jSONObject.has("tokenId") ? jSONObject.optString("tokenId") : null, jSONObject.has("email") ? jSONObject.optString("email") : null, jSONObject.has("displayName") ? jSONObject.optString("displayName") : null, jSONObject.has("givenName") ? jSONObject.optString("givenName") : null, jSONObject.has("familyName") ? jSONObject.optString("familyName") : null, parse, Long.valueOf(parseLong), jSONObject.getString("obfuscatedIdentifier"), hashSet);
        S1.k0 = jSONObject.has("serverAuthCode") ? jSONObject.optString("serverAuthCode") : null;
        return S1;
    }

    public static GoogleSignInAccount S1(String str, String str2, String str3, String str4, String str5, String str6, Uri uri, Long l, String str7, Set<Scope> set) {
        return new GoogleSignInAccount(3, str, str2, str3, str4, uri, null, (l == null ? Long.valueOf(r0.a() / 1000) : l).longValue(), zt2.f(str7), new ArrayList((Collection) zt2.j(set)), str5, str6);
    }

    @RecentlyNullable
    public String I1() {
        return this.i0;
    }

    @RecentlyNullable
    public String J1() {
        return this.h0;
    }

    @RecentlyNullable
    public String K1() {
        return this.p0;
    }

    @RecentlyNullable
    public String L1() {
        return this.o0;
    }

    @RecentlyNullable
    public String M1() {
        return this.f0;
    }

    @RecentlyNullable
    public String N1() {
        return this.g0;
    }

    @RecentlyNullable
    public Uri O1() {
        return this.j0;
    }

    public Set<Scope> P1() {
        HashSet hashSet = new HashSet(this.n0);
        hashSet.addAll(this.q0);
        return hashSet;
    }

    @RecentlyNullable
    public String Q1() {
        return this.k0;
    }

    @RecentlyNonNull
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj instanceof GoogleSignInAccount) {
            GoogleSignInAccount googleSignInAccount = (GoogleSignInAccount) obj;
            return googleSignInAccount.m0.equals(this.m0) && googleSignInAccount.P1().equals(P1());
        }
        return false;
    }

    @RecentlyNullable
    public Account getAccount() {
        if (this.h0 == null) {
            return null;
        }
        return new Account(this.h0, "com.google");
    }

    @RecentlyNonNull
    public int hashCode() {
        return ((this.m0.hashCode() + 527) * 31) + P1().hashCode();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        int a = yb3.a(parcel);
        yb3.m(parcel, 1, this.a);
        yb3.s(parcel, 2, M1(), false);
        yb3.s(parcel, 3, N1(), false);
        yb3.s(parcel, 4, J1(), false);
        yb3.s(parcel, 5, I1(), false);
        yb3.r(parcel, 6, O1(), i, false);
        yb3.s(parcel, 7, Q1(), false);
        yb3.o(parcel, 8, this.l0);
        yb3.s(parcel, 9, this.m0, false);
        yb3.w(parcel, 10, this.n0, false);
        yb3.s(parcel, 11, L1(), false);
        yb3.s(parcel, 12, K1(), false);
        yb3.b(parcel, a);
    }
}
