package com.google.android.gms.auth.api.signin.internal;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.RecentlyNonNull;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public class GoogleSignInOptionsExtensionParcelable extends AbstractSafeParcelable {
    @RecentlyNonNull
    public static final Parcelable.Creator<GoogleSignInOptionsExtensionParcelable> CREATOR = new nz4();
    public final int a;
    public int f0;
    public Bundle g0;

    public GoogleSignInOptionsExtensionParcelable(int i, int i2, Bundle bundle) {
        this.a = i;
        this.f0 = i2;
        this.g0 = bundle;
    }

    @RecentlyNonNull
    public int I1() {
        return this.f0;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        int a = yb3.a(parcel);
        yb3.m(parcel, 1, this.a);
        yb3.m(parcel, 2, I1());
        yb3.e(parcel, 3, this.g0, false);
        yb3.b(parcel, a);
    }
}
