package com.google.android.gms.auth.api.signin;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.RecentlyNonNull;
import androidx.annotation.RecentlyNullable;
import com.google.android.gms.auth.api.signin.internal.GoogleSignInOptionsExtensionParcelable;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public class GoogleSignInOptions extends AbstractSafeParcelable implements a.d, ReflectedParcelable {
    @RecentlyNonNull
    public static final Parcelable.Creator<GoogleSignInOptions> CREATOR;
    @RecentlyNonNull
    public static final Scope o0 = new Scope("profile");
    @RecentlyNonNull
    public static final Scope p0;
    @RecentlyNonNull
    public static final Scope q0;
    @RecentlyNonNull
    public static final Scope r0;
    public final int a;
    public final ArrayList<Scope> f0;
    public Account g0;
    public boolean h0;
    public final boolean i0;
    public final boolean j0;
    public String k0;
    public String l0;
    public ArrayList<GoogleSignInOptionsExtensionParcelable> m0;
    public String n0;

    /* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
    /* loaded from: classes.dex */
    public static final class a {
        public boolean b;
        public boolean c;
        public boolean d;
        public String e;
        public Account f;
        public String g;
        public String i;
        public Set<Scope> a = new HashSet();
        public Map<Integer, GoogleSignInOptionsExtensionParcelable> h = new HashMap();

        @RecentlyNonNull
        public final GoogleSignInOptions a() {
            if (this.a.contains(GoogleSignInOptions.r0)) {
                Set<Scope> set = this.a;
                Scope scope = GoogleSignInOptions.q0;
                if (set.contains(scope)) {
                    this.a.remove(scope);
                }
            }
            if (this.d && (this.f == null || !this.a.isEmpty())) {
                b();
            }
            return new GoogleSignInOptions(3, new ArrayList(this.a), this.f, this.d, this.b, this.c, this.e, this.g, this.h, this.i, null);
        }

        @RecentlyNonNull
        public final a b() {
            this.a.add(GoogleSignInOptions.p0);
            return this;
        }

        @RecentlyNonNull
        public final a c() {
            this.a.add(GoogleSignInOptions.o0);
            return this;
        }

        @RecentlyNonNull
        public final a d(@RecentlyNonNull Scope scope, @RecentlyNonNull Scope... scopeArr) {
            this.a.add(scope);
            this.a.addAll(Arrays.asList(scopeArr));
            return this;
        }
    }

    static {
        new Scope("email");
        p0 = new Scope("openid");
        Scope scope = new Scope("https://www.googleapis.com/auth/games_lite");
        q0 = scope;
        r0 = new Scope("https://www.googleapis.com/auth/games");
        new a().b().c().a();
        new a().d(scope, new Scope[0]).a();
        CREATOR = new q15();
        new c15();
    }

    public GoogleSignInOptions(int i, ArrayList<Scope> arrayList, Account account, boolean z, boolean z2, boolean z3, String str, String str2, ArrayList<GoogleSignInOptionsExtensionParcelable> arrayList2, String str3) {
        this(i, arrayList, account, z, z2, z3, str, str2, P1(arrayList2), str3);
    }

    public static Map<Integer, GoogleSignInOptionsExtensionParcelable> P1(List<GoogleSignInOptionsExtensionParcelable> list) {
        HashMap hashMap = new HashMap();
        if (list == null) {
            return hashMap;
        }
        for (GoogleSignInOptionsExtensionParcelable googleSignInOptionsExtensionParcelable : list) {
            hashMap.put(Integer.valueOf(googleSignInOptionsExtensionParcelable.I1()), googleSignInOptionsExtensionParcelable);
        }
        return hashMap;
    }

    @RecentlyNonNull
    public ArrayList<GoogleSignInOptionsExtensionParcelable> I1() {
        return this.m0;
    }

    @RecentlyNullable
    public String J1() {
        return this.n0;
    }

    @RecentlyNonNull
    public ArrayList<Scope> K1() {
        return new ArrayList<>(this.f0);
    }

    @RecentlyNullable
    public String L1() {
        return this.k0;
    }

    @RecentlyNonNull
    public boolean M1() {
        return this.j0;
    }

    @RecentlyNonNull
    public boolean N1() {
        return this.h0;
    }

    @RecentlyNonNull
    public boolean O1() {
        return this.i0;
    }

    /* JADX WARN: Code restructure failed: missing block: B:21:0x0048, code lost:
        if (r1.equals(r4.getAccount()) != false) goto L21;
     */
    /* JADX WARN: Code restructure failed: missing block: B:28:0x0067, code lost:
        if (r3.k0.equals(r4.L1()) != false) goto L26;
     */
    @androidx.annotation.RecentlyNonNull
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public boolean equals(java.lang.Object r4) {
        /*
            r3 = this;
            r0 = 0
            if (r4 != 0) goto L4
            return r0
        L4:
            com.google.android.gms.auth.api.signin.GoogleSignInOptions r4 = (com.google.android.gms.auth.api.signin.GoogleSignInOptions) r4     // Catch: java.lang.ClassCastException -> L8f
            java.util.ArrayList<com.google.android.gms.auth.api.signin.internal.GoogleSignInOptionsExtensionParcelable> r1 = r3.m0     // Catch: java.lang.ClassCastException -> L8f
            int r1 = r1.size()     // Catch: java.lang.ClassCastException -> L8f
            if (r1 > 0) goto L8f
            java.util.ArrayList<com.google.android.gms.auth.api.signin.internal.GoogleSignInOptionsExtensionParcelable> r1 = r4.m0     // Catch: java.lang.ClassCastException -> L8f
            int r1 = r1.size()     // Catch: java.lang.ClassCastException -> L8f
            if (r1 <= 0) goto L18
            goto L8f
        L18:
            java.util.ArrayList<com.google.android.gms.common.api.Scope> r1 = r3.f0     // Catch: java.lang.ClassCastException -> L8f
            int r1 = r1.size()     // Catch: java.lang.ClassCastException -> L8f
            java.util.ArrayList r2 = r4.K1()     // Catch: java.lang.ClassCastException -> L8f
            int r2 = r2.size()     // Catch: java.lang.ClassCastException -> L8f
            if (r1 != r2) goto L8f
            java.util.ArrayList<com.google.android.gms.common.api.Scope> r1 = r3.f0     // Catch: java.lang.ClassCastException -> L8f
            java.util.ArrayList r2 = r4.K1()     // Catch: java.lang.ClassCastException -> L8f
            boolean r1 = r1.containsAll(r2)     // Catch: java.lang.ClassCastException -> L8f
            if (r1 != 0) goto L35
            goto L8f
        L35:
            android.accounts.Account r1 = r3.g0     // Catch: java.lang.ClassCastException -> L8f
            if (r1 != 0) goto L40
            android.accounts.Account r1 = r4.getAccount()     // Catch: java.lang.ClassCastException -> L8f
            if (r1 != 0) goto L8f
            goto L4a
        L40:
            android.accounts.Account r2 = r4.getAccount()     // Catch: java.lang.ClassCastException -> L8f
            boolean r1 = r1.equals(r2)     // Catch: java.lang.ClassCastException -> L8f
            if (r1 == 0) goto L8f
        L4a:
            java.lang.String r1 = r3.k0     // Catch: java.lang.ClassCastException -> L8f
            boolean r1 = android.text.TextUtils.isEmpty(r1)     // Catch: java.lang.ClassCastException -> L8f
            if (r1 == 0) goto L5d
            java.lang.String r1 = r4.L1()     // Catch: java.lang.ClassCastException -> L8f
            boolean r1 = android.text.TextUtils.isEmpty(r1)     // Catch: java.lang.ClassCastException -> L8f
            if (r1 == 0) goto L8f
            goto L69
        L5d:
            java.lang.String r1 = r3.k0     // Catch: java.lang.ClassCastException -> L8f
            java.lang.String r2 = r4.L1()     // Catch: java.lang.ClassCastException -> L8f
            boolean r1 = r1.equals(r2)     // Catch: java.lang.ClassCastException -> L8f
            if (r1 == 0) goto L8f
        L69:
            boolean r1 = r3.j0     // Catch: java.lang.ClassCastException -> L8f
            boolean r2 = r4.M1()     // Catch: java.lang.ClassCastException -> L8f
            if (r1 != r2) goto L8f
            boolean r1 = r3.h0     // Catch: java.lang.ClassCastException -> L8f
            boolean r2 = r4.N1()     // Catch: java.lang.ClassCastException -> L8f
            if (r1 != r2) goto L8f
            boolean r1 = r3.i0     // Catch: java.lang.ClassCastException -> L8f
            boolean r2 = r4.O1()     // Catch: java.lang.ClassCastException -> L8f
            if (r1 != r2) goto L8f
            java.lang.String r1 = r3.n0     // Catch: java.lang.ClassCastException -> L8f
            java.lang.String r4 = r4.J1()     // Catch: java.lang.ClassCastException -> L8f
            boolean r4 = android.text.TextUtils.equals(r1, r4)     // Catch: java.lang.ClassCastException -> L8f
            if (r4 == 0) goto L8f
            r4 = 1
            return r4
        L8f:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.auth.api.signin.GoogleSignInOptions.equals(java.lang.Object):boolean");
    }

    @RecentlyNullable
    public Account getAccount() {
        return this.g0;
    }

    @RecentlyNonNull
    public int hashCode() {
        ArrayList arrayList = new ArrayList();
        ArrayList<Scope> arrayList2 = this.f0;
        int size = arrayList2.size();
        int i = 0;
        while (i < size) {
            Scope scope = arrayList2.get(i);
            i++;
            arrayList.add(scope.I1());
        }
        Collections.sort(arrayList);
        return new bk1().a(arrayList).a(this.g0).a(this.k0).c(this.j0).c(this.h0).c(this.i0).a(this.n0).b();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        int a2 = yb3.a(parcel);
        yb3.m(parcel, 1, this.a);
        yb3.w(parcel, 2, K1(), false);
        yb3.r(parcel, 3, getAccount(), i, false);
        yb3.c(parcel, 4, N1());
        yb3.c(parcel, 5, O1());
        yb3.c(parcel, 6, M1());
        yb3.s(parcel, 7, L1(), false);
        yb3.s(parcel, 8, this.l0, false);
        yb3.w(parcel, 9, I1(), false);
        yb3.s(parcel, 10, J1(), false);
        yb3.b(parcel, a2);
    }

    public GoogleSignInOptions(int i, ArrayList<Scope> arrayList, Account account, boolean z, boolean z2, boolean z3, String str, String str2, Map<Integer, GoogleSignInOptionsExtensionParcelable> map, String str3) {
        this.a = i;
        this.f0 = arrayList;
        this.g0 = account;
        this.h0 = z;
        this.i0 = z2;
        this.j0 = z3;
        this.k0 = str;
        this.l0 = str2;
        this.m0 = new ArrayList<>(map.values());
        this.n0 = str3;
    }

    public /* synthetic */ GoogleSignInOptions(int i, ArrayList arrayList, Account account, boolean z, boolean z2, boolean z3, String str, String str2, Map map, String str3, c15 c15Var) {
        this(3, arrayList, account, z, z2, z3, str, str2, map, str3);
    }
}
