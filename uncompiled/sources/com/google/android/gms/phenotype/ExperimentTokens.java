package com.google.android.gms.phenotype;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Base64;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/* loaded from: classes.dex */
public class ExperimentTokens extends AbstractSafeParcelable {
    public static final Parcelable.Creator<ExperimentTokens> CREATOR = new ym5();
    public static final byte[][] m0;
    public final String a;
    public final byte[] f0;
    public final byte[][] g0;
    public final byte[][] h0;
    public final byte[][] i0;
    public final byte[][] j0;
    public final int[] k0;
    public final byte[][] l0;

    static {
        byte[][] bArr = new byte[0];
        m0 = bArr;
        new ExperimentTokens("", null, bArr, bArr, bArr, bArr, null, null);
        new qc5();
        new cf5();
        new wh5();
        new nk5();
    }

    public ExperimentTokens(String str, byte[] bArr, byte[][] bArr2, byte[][] bArr3, byte[][] bArr4, byte[][] bArr5, int[] iArr, byte[][] bArr6) {
        this.a = str;
        this.f0 = bArr;
        this.g0 = bArr2;
        this.h0 = bArr3;
        this.i0 = bArr4;
        this.j0 = bArr5;
        this.k0 = iArr;
        this.l0 = bArr6;
    }

    public static List<Integer> I1(int[] iArr) {
        if (iArr == null) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList(iArr.length);
        for (int i : iArr) {
            arrayList.add(Integer.valueOf(i));
        }
        Collections.sort(arrayList);
        return arrayList;
    }

    public static List<String> J1(byte[][] bArr) {
        if (bArr == null) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList(bArr.length);
        for (byte[] bArr2 : bArr) {
            arrayList.add(Base64.encodeToString(bArr2, 3));
        }
        Collections.sort(arrayList);
        return arrayList;
    }

    public static void K1(StringBuilder sb, String str, byte[][] bArr) {
        String str2;
        sb.append(str);
        sb.append("=");
        if (bArr == null) {
            str2 = "null";
        } else {
            sb.append("(");
            int length = bArr.length;
            boolean z = true;
            int i = 0;
            while (i < length) {
                byte[] bArr2 = bArr[i];
                if (!z) {
                    sb.append(", ");
                }
                sb.append("'");
                sb.append(Base64.encodeToString(bArr2, 3));
                sb.append("'");
                i++;
                z = false;
            }
            str2 = ")";
        }
        sb.append(str2);
    }

    public boolean equals(Object obj) {
        if (obj instanceof ExperimentTokens) {
            ExperimentTokens experimentTokens = (ExperimentTokens) obj;
            if (wz5.a(this.a, experimentTokens.a) && Arrays.equals(this.f0, experimentTokens.f0) && wz5.a(J1(this.g0), J1(experimentTokens.g0)) && wz5.a(J1(this.h0), J1(experimentTokens.h0)) && wz5.a(J1(this.i0), J1(experimentTokens.i0)) && wz5.a(J1(this.j0), J1(experimentTokens.j0)) && wz5.a(I1(this.k0), I1(experimentTokens.k0)) && wz5.a(J1(this.l0), J1(experimentTokens.l0))) {
                return true;
            }
        }
        return false;
    }

    public String toString() {
        String sb;
        StringBuilder sb2 = new StringBuilder("ExperimentTokens");
        sb2.append("(");
        String str = this.a;
        if (str == null) {
            sb = "null";
        } else {
            StringBuilder sb3 = new StringBuilder(String.valueOf(str).length() + 2);
            sb3.append("'");
            sb3.append(str);
            sb3.append("'");
            sb = sb3.toString();
        }
        sb2.append(sb);
        sb2.append(", ");
        byte[] bArr = this.f0;
        sb2.append("direct");
        sb2.append("=");
        if (bArr == null) {
            sb2.append("null");
        } else {
            sb2.append("'");
            sb2.append(Base64.encodeToString(bArr, 3));
            sb2.append("'");
        }
        sb2.append(", ");
        K1(sb2, "GAIA", this.g0);
        sb2.append(", ");
        K1(sb2, "PSEUDO", this.h0);
        sb2.append(", ");
        K1(sb2, "ALWAYS", this.i0);
        sb2.append(", ");
        K1(sb2, "OTHER", this.j0);
        sb2.append(", ");
        int[] iArr = this.k0;
        sb2.append("weak");
        sb2.append("=");
        if (iArr == null) {
            sb2.append("null");
        } else {
            sb2.append("(");
            int length = iArr.length;
            boolean z = true;
            int i = 0;
            while (i < length) {
                int i2 = iArr[i];
                if (!z) {
                    sb2.append(", ");
                }
                sb2.append(i2);
                i++;
                z = false;
            }
            sb2.append(")");
        }
        sb2.append(", ");
        K1(sb2, "directs", this.l0);
        sb2.append(")");
        return sb2.toString();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        int a = yb3.a(parcel);
        yb3.s(parcel, 2, this.a, false);
        yb3.f(parcel, 3, this.f0, false);
        yb3.g(parcel, 4, this.g0, false);
        yb3.g(parcel, 5, this.h0, false);
        yb3.g(parcel, 6, this.i0, false);
        yb3.g(parcel, 7, this.j0, false);
        yb3.n(parcel, 8, this.k0, false);
        yb3.g(parcel, 9, this.l0, false);
        yb3.b(parcel, a);
    }
}
