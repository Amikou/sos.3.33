package com.google.android.gms.phenotype;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

/* loaded from: classes.dex */
public class Configuration extends AbstractSafeParcelable implements Comparable<Configuration> {
    public static final Parcelable.Creator<Configuration> CREATOR = new qa5();
    public final int a;
    public final zzi[] f0;
    public final String[] g0;
    public final Map<String, zzi> h0 = new TreeMap();

    public Configuration(int i, zzi[] zziVarArr, String[] strArr) {
        this.a = i;
        this.f0 = zziVarArr;
        for (zzi zziVar : zziVarArr) {
            this.h0.put(zziVar.a, zziVar);
        }
        this.g0 = strArr;
        if (strArr != null) {
            Arrays.sort(strArr);
        }
    }

    @Override // java.lang.Comparable
    public /* synthetic */ int compareTo(Configuration configuration) {
        return this.a - configuration.a;
    }

    public boolean equals(Object obj) {
        if (obj instanceof Configuration) {
            Configuration configuration = (Configuration) obj;
            if (this.a == configuration.a && wz5.a(this.h0, configuration.h0) && Arrays.equals(this.g0, configuration.g0)) {
                return true;
            }
        }
        return false;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Configuration(");
        sb.append(this.a);
        sb.append(", ");
        sb.append("(");
        for (zzi zziVar : this.h0.values()) {
            sb.append(zziVar);
            sb.append(", ");
        }
        sb.append(")");
        sb.append(", ");
        sb.append("(");
        String[] strArr = this.g0;
        if (strArr != null) {
            for (String str : strArr) {
                sb.append(str);
                sb.append(", ");
            }
        } else {
            sb.append("null");
        }
        sb.append(")");
        sb.append(")");
        return sb.toString();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        int a = yb3.a(parcel);
        yb3.m(parcel, 2, this.a);
        yb3.v(parcel, 3, this.f0, i, false);
        yb3.t(parcel, 4, this.g0, false);
        yb3.b(parcel, a);
    }
}
