package com.google.android.gms.phenotype;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Base64;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.util.Arrays;

/* loaded from: classes.dex */
public final class zzi extends AbstractSafeParcelable implements Comparable<zzi> {
    public static final Parcelable.Creator<zzi> CREATOR = new xu5();
    public final String a;
    public final long f0;
    public final boolean g0;
    public final double h0;
    public final String i0;
    public final byte[] j0;
    public final int k0;
    public final int l0;

    static {
        new gs5();
    }

    public zzi(String str, long j, boolean z, double d, String str2, byte[] bArr, int i, int i2) {
        this.a = str;
        this.f0 = j;
        this.g0 = z;
        this.h0 = d;
        this.i0 = str2;
        this.j0 = bArr;
        this.k0 = i;
        this.l0 = i2;
    }

    public static int I1(int i, int i2) {
        if (i < i2) {
            return -1;
        }
        return i == i2 ? 0 : 1;
    }

    @Override // java.lang.Comparable
    public final /* synthetic */ int compareTo(zzi zziVar) {
        zzi zziVar2 = zziVar;
        int compareTo = this.a.compareTo(zziVar2.a);
        if (compareTo != 0) {
            return compareTo;
        }
        int I1 = I1(this.k0, zziVar2.k0);
        if (I1 != 0) {
            return I1;
        }
        int i = this.k0;
        if (i == 1) {
            int i2 = (this.f0 > zziVar2.f0 ? 1 : (this.f0 == zziVar2.f0 ? 0 : -1));
            if (i2 < 0) {
                return -1;
            }
            return i2 == 0 ? 0 : 1;
        } else if (i == 2) {
            boolean z = this.g0;
            if (z == zziVar2.g0) {
                return 0;
            }
            return z ? 1 : -1;
        } else if (i != 3) {
            if (i == 4) {
                String str = this.i0;
                String str2 = zziVar2.i0;
                if (str == str2) {
                    return 0;
                }
                if (str == null) {
                    return -1;
                }
                if (str2 == null) {
                    return 1;
                }
                return str.compareTo(str2);
            } else if (i != 5) {
                int i3 = this.k0;
                StringBuilder sb = new StringBuilder(31);
                sb.append("Invalid enum value: ");
                sb.append(i3);
                throw new AssertionError(sb.toString());
            } else {
                byte[] bArr = this.j0;
                byte[] bArr2 = zziVar2.j0;
                if (bArr == bArr2) {
                    return 0;
                }
                if (bArr == null) {
                    return -1;
                }
                if (bArr2 == null) {
                    return 1;
                }
                for (int i4 = 0; i4 < Math.min(this.j0.length, zziVar2.j0.length); i4++) {
                    int i5 = this.j0[i4] - zziVar2.j0[i4];
                    if (i5 != 0) {
                        return i5;
                    }
                }
                return I1(this.j0.length, zziVar2.j0.length);
            }
        } else {
            return Double.compare(this.h0, zziVar2.h0);
        }
    }

    public final boolean equals(Object obj) {
        int i;
        if (obj instanceof zzi) {
            zzi zziVar = (zzi) obj;
            if (wz5.a(this.a, zziVar.a) && (i = this.k0) == zziVar.k0 && this.l0 == zziVar.l0) {
                if (i != 1) {
                    if (i == 2) {
                        return this.g0 == zziVar.g0;
                    } else if (i == 3) {
                        return this.h0 == zziVar.h0;
                    } else if (i != 4) {
                        if (i == 5) {
                            return Arrays.equals(this.j0, zziVar.j0);
                        }
                        int i2 = this.k0;
                        StringBuilder sb = new StringBuilder(31);
                        sb.append("Invalid enum value: ");
                        sb.append(i2);
                        throw new AssertionError(sb.toString());
                    } else {
                        return wz5.a(this.i0, zziVar.i0);
                    }
                } else if (this.f0 == zziVar.f0) {
                    return true;
                }
            }
        }
        return false;
    }

    public final String toString() {
        String str;
        StringBuilder sb = new StringBuilder();
        sb.append("Flag(");
        sb.append(this.a);
        sb.append(", ");
        int i = this.k0;
        if (i == 1) {
            sb.append(this.f0);
        } else if (i == 2) {
            sb.append(this.g0);
        } else if (i != 3) {
            if (i == 4) {
                sb.append("'");
                str = this.i0;
            } else if (i != 5) {
                String str2 = this.a;
                int i2 = this.k0;
                StringBuilder sb2 = new StringBuilder(String.valueOf(str2).length() + 27);
                sb2.append("Invalid type: ");
                sb2.append(str2);
                sb2.append(", ");
                sb2.append(i2);
                throw new AssertionError(sb2.toString());
            } else if (this.j0 == null) {
                sb.append("null");
            } else {
                sb.append("'");
                str = Base64.encodeToString(this.j0, 3);
            }
            sb.append(str);
            sb.append("'");
        } else {
            sb.append(this.h0);
        }
        sb.append(", ");
        sb.append(this.k0);
        sb.append(", ");
        sb.append(this.l0);
        sb.append(")");
        return sb.toString();
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int a = yb3.a(parcel);
        yb3.s(parcel, 2, this.a, false);
        yb3.o(parcel, 3, this.f0);
        yb3.c(parcel, 4, this.g0);
        yb3.h(parcel, 5, this.h0);
        yb3.s(parcel, 6, this.i0, false);
        yb3.f(parcel, 7, this.j0, false);
        yb3.m(parcel, 8, this.k0);
        yb3.m(parcel, 9, this.l0);
        yb3.b(parcel, a);
    }
}
