package com.google.android.gms.dynamite;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import defpackage.lm1;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* loaded from: classes.dex */
public final class j extends a75 implements k {
    public j(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.dynamite.IDynamiteLoader");
    }

    @Override // com.google.android.gms.dynamite.k
    public final lm1 a(lm1 lm1Var, String str, int i, lm1 lm1Var2) throws RemoteException {
        Parcel b = b();
        ad5.b(b, lm1Var);
        b.writeString(str);
        b.writeInt(i);
        ad5.b(b, lm1Var2);
        Parcel F1 = F1(8, b);
        lm1 F12 = lm1.a.F1(F1.readStrongBinder());
        F1.recycle();
        return F12;
    }

    @Override // com.google.android.gms.dynamite.k
    public final lm1 c1(lm1 lm1Var, String str, boolean z) throws RemoteException {
        Parcel b = b();
        ad5.b(b, lm1Var);
        b.writeString(str);
        ad5.d(b, z);
        Parcel F1 = F1(7, b);
        lm1 F12 = lm1.a.F1(F1.readStrongBinder());
        F1.recycle();
        return F12;
    }

    @Override // com.google.android.gms.dynamite.k
    public final int f(lm1 lm1Var, String str, boolean z) throws RemoteException {
        Parcel b = b();
        ad5.b(b, lm1Var);
        b.writeString(str);
        ad5.d(b, z);
        Parcel F1 = F1(3, b);
        int readInt = F1.readInt();
        F1.recycle();
        return readInt;
    }

    @Override // com.google.android.gms.dynamite.k
    public final int k1(lm1 lm1Var, String str, boolean z) throws RemoteException {
        Parcel b = b();
        ad5.b(b, lm1Var);
        b.writeString(str);
        ad5.d(b, z);
        Parcel F1 = F1(5, b);
        int readInt = F1.readInt();
        F1.recycle();
        return readInt;
    }

    @Override // com.google.android.gms.dynamite.k
    public final lm1 q(lm1 lm1Var, String str, int i) throws RemoteException {
        Parcel b = b();
        ad5.b(b, lm1Var);
        b.writeString(str);
        b.writeInt(i);
        Parcel F1 = F1(2, b);
        lm1 F12 = lm1.a.F1(F1.readStrongBinder());
        F1.recycle();
        return F12;
    }

    @Override // com.google.android.gms.dynamite.k
    public final lm1 v1(lm1 lm1Var, String str, int i) throws RemoteException {
        Parcel b = b();
        ad5.b(b, lm1Var);
        b.writeString(str);
        b.writeInt(i);
        Parcel F1 = F1(4, b);
        lm1 F12 = lm1.a.F1(F1.readStrongBinder());
        F1.recycle();
        return F12;
    }

    @Override // com.google.android.gms.dynamite.k
    public final int zzb() throws RemoteException {
        Parcel F1 = F1(6, b());
        int readInt = F1.readInt();
        F1.recycle();
        return readInt;
    }
}
