package com.google.android.gms.dynamite;

import android.os.IInterface;
import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* loaded from: classes.dex */
public interface k extends IInterface {
    lm1 a(lm1 lm1Var, String str, int i, lm1 lm1Var2) throws RemoteException;

    lm1 c1(lm1 lm1Var, String str, boolean z) throws RemoteException;

    int f(lm1 lm1Var, String str, boolean z) throws RemoteException;

    int k1(lm1 lm1Var, String str, boolean z) throws RemoteException;

    lm1 q(lm1 lm1Var, String str, int i) throws RemoteException;

    lm1 v1(lm1 lm1Var, String str, int i) throws RemoteException;

    int zzb() throws RemoteException;
}
