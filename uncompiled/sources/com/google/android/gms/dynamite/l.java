package com.google.android.gms.dynamite;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import defpackage.lm1;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* loaded from: classes.dex */
public final class l extends a75 implements m {
    public l(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.dynamite.IDynamiteLoaderV2");
    }

    @Override // com.google.android.gms.dynamite.m
    public final lm1 a(lm1 lm1Var, String str, int i, lm1 lm1Var2) throws RemoteException {
        Parcel b = b();
        ad5.b(b, lm1Var);
        b.writeString(str);
        b.writeInt(i);
        ad5.b(b, lm1Var2);
        Parcel F1 = F1(2, b);
        lm1 F12 = lm1.a.F1(F1.readStrongBinder());
        F1.recycle();
        return F12;
    }

    @Override // com.google.android.gms.dynamite.m
    public final lm1 l1(lm1 lm1Var, String str, int i, lm1 lm1Var2) throws RemoteException {
        Parcel b = b();
        ad5.b(b, lm1Var);
        b.writeString(str);
        b.writeInt(i);
        ad5.b(b, lm1Var2);
        Parcel F1 = F1(3, b);
        lm1 F12 = lm1.a.F1(F1.readStrongBinder());
        F1.recycle();
        return F12;
    }
}
