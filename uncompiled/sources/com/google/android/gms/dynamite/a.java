package com.google.android.gms.dynamite;

import android.content.Context;
import com.google.android.gms.dynamite.DynamiteModule;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* loaded from: classes.dex */
public final class a implements DynamiteModule.a {
    @Override // com.google.android.gms.dynamite.DynamiteModule.a
    public final DynamiteModule.a.C0113a a(Context context, String str, DynamiteModule.a.b bVar) throws DynamiteModule.LoadingException {
        DynamiteModule.a.C0113a c0113a = new DynamiteModule.a.C0113a();
        int a = bVar.a(context, str, true);
        c0113a.b = a;
        if (a != 0) {
            c0113a.c = 1;
        } else {
            int b = bVar.b(context, str);
            c0113a.a = b;
            if (b != 0) {
                c0113a.c = -1;
            }
        }
        return c0113a;
    }
}
