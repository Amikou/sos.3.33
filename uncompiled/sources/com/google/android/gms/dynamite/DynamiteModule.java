package com.google.android.gms.dynamite;

import android.content.Context;
import android.database.Cursor;
import android.os.Build;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import androidx.annotation.RecentlyNonNull;
import androidx.annotation.RecentlyNullable;
import com.google.android.gms.common.util.DynamiteApi;
import dalvik.system.DelegateLastClassLoader;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* loaded from: classes.dex */
public final class DynamiteModule {
    public static Boolean b = null;
    public static k c = null;
    public static m d = null;
    public static String e = null;
    public static int f = -1;
    public static final ThreadLocal<b> g = new ThreadLocal<>();
    public static final a.b h = new com.google.android.gms.dynamite.b();
    @RecentlyNonNull
    public static final a i = new com.google.android.gms.dynamite.a();
    @RecentlyNonNull
    public static final a j;
    @RecentlyNonNull
    public static final a k;
    public final Context a;

    /* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
    @DynamiteApi
    /* loaded from: classes.dex */
    public static class DynamiteLoaderClassLoader {
        @RecentlyNullable
        public static ClassLoader sClassLoader;
    }

    /* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
    /* loaded from: classes.dex */
    public static class LoadingException extends Exception {
        public LoadingException(String str) {
            super(str);
        }

        public LoadingException(String str, Throwable th) {
            super(str, th);
        }

        public /* synthetic */ LoadingException(String str, com.google.android.gms.dynamite.b bVar) {
            this(str);
        }

        public /* synthetic */ LoadingException(String str, Throwable th, com.google.android.gms.dynamite.b bVar) {
            this(str, th);
        }
    }

    /* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
    /* loaded from: classes.dex */
    public interface a {

        /* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
        /* renamed from: com.google.android.gms.dynamite.DynamiteModule$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public static class C0113a {
            public int a = 0;
            public int b = 0;
            public int c = 0;
        }

        /* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
        /* loaded from: classes.dex */
        public interface b {
            int a(Context context, String str, boolean z) throws LoadingException;

            int b(Context context, String str);
        }

        C0113a a(Context context, String str, b bVar) throws LoadingException;
    }

    /* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
    /* loaded from: classes.dex */
    public static class b {
        public Cursor a;

        public b() {
        }

        public /* synthetic */ b(com.google.android.gms.dynamite.b bVar) {
            this();
        }
    }

    /* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
    /* loaded from: classes.dex */
    public static class c implements a.b {
        public final int a;

        public c(int i, int i2) {
            this.a = i;
        }

        @Override // com.google.android.gms.dynamite.DynamiteModule.a.b
        public final int a(Context context, String str, boolean z) {
            return 0;
        }

        @Override // com.google.android.gms.dynamite.DynamiteModule.a.b
        public final int b(Context context, String str) {
            return this.a;
        }
    }

    static {
        new d();
        new com.google.android.gms.dynamite.c();
        new f();
        j = new e();
        k = new h();
        new g();
    }

    public DynamiteModule(Context context) {
        this.a = (Context) zt2.j(context);
    }

    @RecentlyNonNull
    public static int a(@RecentlyNonNull Context context, @RecentlyNonNull String str) {
        try {
            ClassLoader classLoader = context.getApplicationContext().getClassLoader();
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 61);
            sb.append("com.google.android.gms.dynamite.descriptors.");
            sb.append(str);
            sb.append(".ModuleDescriptor");
            Class<?> loadClass = classLoader.loadClass(sb.toString());
            Field declaredField = loadClass.getDeclaredField("MODULE_ID");
            Field declaredField2 = loadClass.getDeclaredField("MODULE_VERSION");
            if (!pl2.a(declaredField.get(null), str)) {
                String valueOf = String.valueOf(declaredField.get(null));
                StringBuilder sb2 = new StringBuilder(valueOf.length() + 51 + String.valueOf(str).length());
                sb2.append("Module descriptor id '");
                sb2.append(valueOf);
                sb2.append("' didn't match expected id '");
                sb2.append(str);
                sb2.append("'");
                return 0;
            }
            return declaredField2.getInt(null);
        } catch (ClassNotFoundException unused) {
            StringBuilder sb3 = new StringBuilder(String.valueOf(str).length() + 45);
            sb3.append("Local module descriptor class for ");
            sb3.append(str);
            sb3.append(" not found.");
            return 0;
        } catch (Exception e2) {
            String valueOf2 = String.valueOf(e2.getMessage());
            if (valueOf2.length() != 0) {
                "Failed to load module descriptor class: ".concat(valueOf2);
            }
            return 0;
        }
    }

    @RecentlyNonNull
    public static int b(@RecentlyNonNull Context context, @RecentlyNonNull String str) {
        return e(context, str, false);
    }

    @RecentlyNonNull
    public static DynamiteModule d(@RecentlyNonNull Context context, @RecentlyNonNull a aVar, @RecentlyNonNull String str) throws LoadingException {
        ThreadLocal<b> threadLocal = g;
        b bVar = threadLocal.get();
        b bVar2 = new b(null);
        threadLocal.set(bVar2);
        try {
            a.C0113a a2 = aVar.a(context, str, h);
            int i2 = a2.a;
            int i3 = a2.b;
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 68 + String.valueOf(str).length());
            sb.append("Considering local module ");
            sb.append(str);
            sb.append(":");
            sb.append(i2);
            sb.append(" and remote module ");
            sb.append(str);
            sb.append(":");
            sb.append(i3);
            int i4 = a2.c;
            if (i4 == 0 || ((i4 == -1 && a2.a == 0) || (i4 == 1 && a2.b == 0))) {
                int i5 = a2.a;
                int i6 = a2.b;
                StringBuilder sb2 = new StringBuilder(91);
                sb2.append("No acceptable module found. Local version is ");
                sb2.append(i5);
                sb2.append(" and remote version is ");
                sb2.append(i6);
                sb2.append(".");
                throw new LoadingException(sb2.toString(), (com.google.android.gms.dynamite.b) null);
            } else if (i4 == -1) {
                DynamiteModule f2 = f(context, str);
                Cursor cursor = bVar2.a;
                if (cursor != null) {
                    cursor.close();
                }
                threadLocal.set(bVar);
                return f2;
            } else if (i4 == 1) {
                try {
                    DynamiteModule g2 = g(context, str, a2.b);
                    Cursor cursor2 = bVar2.a;
                    if (cursor2 != null) {
                        cursor2.close();
                    }
                    threadLocal.set(bVar);
                    return g2;
                } catch (LoadingException e2) {
                    String valueOf = String.valueOf(e2.getMessage());
                    if (valueOf.length() != 0) {
                        "Failed to load remote module: ".concat(valueOf);
                    }
                    int i7 = a2.a;
                    if (i7 != 0 && aVar.a(context, str, new c(i7, 0)).c == -1) {
                        DynamiteModule f3 = f(context, str);
                        Cursor cursor3 = bVar2.a;
                        if (cursor3 != null) {
                            cursor3.close();
                        }
                        g.set(bVar);
                        return f3;
                    }
                    throw new LoadingException("Remote load failed. No local fallback found.", e2, null);
                }
            } else {
                int i8 = a2.c;
                StringBuilder sb3 = new StringBuilder(47);
                sb3.append("VersionPolicy returned invalid code:");
                sb3.append(i8);
                throw new LoadingException(sb3.toString(), (com.google.android.gms.dynamite.b) null);
            }
        } catch (Throwable th) {
            Cursor cursor4 = bVar2.a;
            if (cursor4 != null) {
                cursor4.close();
            }
            g.set(bVar);
            throw th;
        }
    }

    @RecentlyNonNull
    public static int e(@RecentlyNonNull Context context, @RecentlyNonNull String str, @RecentlyNonNull boolean z) {
        Field declaredField;
        ClassLoader iVar;
        try {
            synchronized (DynamiteModule.class) {
                Boolean bool = b;
                if (bool == null) {
                    try {
                        declaredField = context.getApplicationContext().getClassLoader().loadClass(DynamiteLoaderClassLoader.class.getName()).getDeclaredField("sClassLoader");
                    } catch (ClassNotFoundException | IllegalAccessException | NoSuchFieldException e2) {
                        String valueOf = String.valueOf(e2);
                        StringBuilder sb = new StringBuilder(valueOf.length() + 30);
                        sb.append("Failed to load module via V2: ");
                        sb.append(valueOf);
                        bool = Boolean.FALSE;
                    }
                    synchronized (declaredField.getDeclaringClass()) {
                        ClassLoader classLoader = (ClassLoader) declaredField.get(null);
                        if (classLoader != null) {
                            if (classLoader == ClassLoader.getSystemClassLoader()) {
                                bool = Boolean.FALSE;
                            } else {
                                try {
                                    j(classLoader);
                                } catch (LoadingException unused) {
                                }
                                bool = Boolean.TRUE;
                            }
                        } else if ("com.google.android.gms".equals(context.getApplicationContext().getPackageName())) {
                            declaredField.set(null, ClassLoader.getSystemClassLoader());
                            bool = Boolean.FALSE;
                        } else {
                            try {
                                int n = n(context, str, z);
                                String str2 = e;
                                if (str2 != null && !str2.isEmpty()) {
                                    if (Build.VERSION.SDK_INT >= 29) {
                                        iVar = new DelegateLastClassLoader((String) zt2.j(e), ClassLoader.getSystemClassLoader());
                                    } else {
                                        iVar = new i((String) zt2.j(e), ClassLoader.getSystemClassLoader());
                                    }
                                    j(iVar);
                                    declaredField.set(null, iVar);
                                    b = Boolean.TRUE;
                                    return n;
                                }
                                return n;
                            } catch (LoadingException unused2) {
                                declaredField.set(null, ClassLoader.getSystemClassLoader());
                                bool = Boolean.FALSE;
                            }
                        }
                        b = bool;
                    }
                }
                if (bool.booleanValue()) {
                    try {
                        return n(context, str, z);
                    } catch (LoadingException e3) {
                        String valueOf2 = String.valueOf(e3.getMessage());
                        if (valueOf2.length() != 0) {
                            "Failed to retrieve remote module version: ".concat(valueOf2);
                            return 0;
                        }
                        return 0;
                    }
                }
                return l(context, str, z);
            }
        } catch (Throwable th) {
            j90.a(context, th);
            throw th;
        }
    }

    public static DynamiteModule f(Context context, String str) {
        String valueOf = String.valueOf(str);
        if (valueOf.length() != 0) {
            "Selected local version of ".concat(valueOf);
        }
        return new DynamiteModule(context.getApplicationContext());
    }

    public static DynamiteModule g(Context context, String str, int i2) throws LoadingException {
        Boolean bool;
        lm1 q;
        try {
            synchronized (DynamiteModule.class) {
                bool = b;
            }
            if (bool != null) {
                if (bool.booleanValue()) {
                    return m(context, str, i2);
                }
                StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 51);
                sb.append("Selected remote version of ");
                sb.append(str);
                sb.append(", version >= ");
                sb.append(i2);
                k h2 = h(context);
                if (h2 != null) {
                    int zzb = h2.zzb();
                    if (zzb >= 3) {
                        b bVar = g.get();
                        if (bVar != null) {
                            q = h2.a(nl2.H1(context), str, i2, nl2.H1(bVar.a));
                        } else {
                            throw new LoadingException("No cached result cursor holder", (com.google.android.gms.dynamite.b) null);
                        }
                    } else if (zzb == 2) {
                        q = h2.v1(nl2.H1(context), str, i2);
                    } else {
                        q = h2.q(nl2.H1(context), str, i2);
                    }
                    if (nl2.G1(q) != null) {
                        return new DynamiteModule((Context) nl2.G1(q));
                    }
                    throw new LoadingException("Failed to load remote module.", (com.google.android.gms.dynamite.b) null);
                }
                throw new LoadingException("Failed to create IDynamiteLoader.", (com.google.android.gms.dynamite.b) null);
            }
            throw new LoadingException("Failed to determine which loading route to use.", (com.google.android.gms.dynamite.b) null);
        } catch (RemoteException e2) {
            throw new LoadingException("Failed to load remote module.", e2, null);
        } catch (LoadingException e3) {
            throw e3;
        } catch (Throwable th) {
            j90.a(context, th);
            throw new LoadingException("Failed to load remote module.", th, null);
        }
    }

    public static k h(Context context) {
        k jVar;
        synchronized (DynamiteModule.class) {
            k kVar = c;
            if (kVar != null) {
                return kVar;
            }
            try {
                IBinder iBinder = (IBinder) context.createPackageContext("com.google.android.gms", 3).getClassLoader().loadClass("com.google.android.gms.chimera.container.DynamiteLoaderImpl").newInstance();
                if (iBinder == null) {
                    jVar = null;
                } else {
                    IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.dynamite.IDynamiteLoader");
                    if (queryLocalInterface instanceof k) {
                        jVar = (k) queryLocalInterface;
                    } else {
                        jVar = new j(iBinder);
                    }
                }
                if (jVar != null) {
                    c = jVar;
                    return jVar;
                }
            } catch (Exception e2) {
                String valueOf = String.valueOf(e2.getMessage());
                if (valueOf.length() != 0) {
                    "Failed to load IDynamiteLoader from GmsCore: ".concat(valueOf);
                }
            }
            return null;
        }
    }

    public static Boolean i() {
        Boolean valueOf;
        synchronized (DynamiteModule.class) {
            valueOf = Boolean.valueOf(f >= 2);
        }
        return valueOf;
    }

    public static void j(ClassLoader classLoader) throws LoadingException {
        m lVar;
        try {
            IBinder iBinder = (IBinder) classLoader.loadClass("com.google.android.gms.dynamiteloader.DynamiteLoaderV2").getConstructor(new Class[0]).newInstance(new Object[0]);
            if (iBinder == null) {
                lVar = null;
            } else {
                IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.dynamite.IDynamiteLoaderV2");
                if (queryLocalInterface instanceof m) {
                    lVar = (m) queryLocalInterface;
                } else {
                    lVar = new l(iBinder);
                }
            }
            d = lVar;
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e2) {
            throw new LoadingException("Failed to instantiate dynamite loader", e2, null);
        }
    }

    public static boolean k(Cursor cursor) {
        b bVar = g.get();
        if (bVar == null || bVar.a != null) {
            return false;
        }
        bVar.a = cursor;
        return true;
    }

    /* JADX WARN: Code restructure failed: missing block: B:16:0x0031, code lost:
        if (k(r5) != false) goto L23;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static int l(android.content.Context r5, java.lang.String r6, boolean r7) {
        /*
            com.google.android.gms.dynamite.k r0 = h(r5)
            r1 = 0
            if (r0 != 0) goto L8
            return r1
        L8:
            r2 = 0
            int r3 = r0.zzb()     // Catch: java.lang.Throwable -> L5c android.os.RemoteException -> L5e
            r4 = 3
            if (r3 < r4) goto L47
            lm1 r5 = defpackage.nl2.H1(r5)     // Catch: java.lang.Throwable -> L5c android.os.RemoteException -> L5e
            lm1 r5 = r0.c1(r5, r6, r7)     // Catch: java.lang.Throwable -> L5c android.os.RemoteException -> L5e
            java.lang.Object r5 = defpackage.nl2.G1(r5)     // Catch: java.lang.Throwable -> L5c android.os.RemoteException -> L5e
            android.database.Cursor r5 = (android.database.Cursor) r5     // Catch: java.lang.Throwable -> L5c android.os.RemoteException -> L5e
            if (r5 == 0) goto L41
            boolean r6 = r5.moveToFirst()     // Catch: java.lang.Throwable -> L3b android.os.RemoteException -> L3e
            if (r6 != 0) goto L27
            goto L41
        L27:
            int r6 = r5.getInt(r1)     // Catch: java.lang.Throwable -> L3b android.os.RemoteException -> L3e
            if (r6 <= 0) goto L34
            boolean r7 = k(r5)     // Catch: java.lang.Throwable -> L3b android.os.RemoteException -> L3e
            if (r7 == 0) goto L34
            goto L35
        L34:
            r2 = r5
        L35:
            if (r2 == 0) goto L3a
            r2.close()
        L3a:
            return r6
        L3b:
            r6 = move-exception
            r2 = r5
            goto L78
        L3e:
            r6 = move-exception
            r2 = r5
            goto L5f
        L41:
            if (r5 == 0) goto L46
            r5.close()
        L46:
            return r1
        L47:
            r4 = 2
            if (r3 != r4) goto L53
            lm1 r5 = defpackage.nl2.H1(r5)     // Catch: java.lang.Throwable -> L5c android.os.RemoteException -> L5e
            int r5 = r0.k1(r5, r6, r7)     // Catch: java.lang.Throwable -> L5c android.os.RemoteException -> L5e
            return r5
        L53:
            lm1 r5 = defpackage.nl2.H1(r5)     // Catch: java.lang.Throwable -> L5c android.os.RemoteException -> L5e
            int r5 = r0.f(r5, r6, r7)     // Catch: java.lang.Throwable -> L5c android.os.RemoteException -> L5e
            return r5
        L5c:
            r6 = move-exception
            goto L78
        L5e:
            r6 = move-exception
        L5f:
            java.lang.String r5 = "Failed to retrieve remote module version: "
            java.lang.String r6 = r6.getMessage()     // Catch: java.lang.Throwable -> L5c
            java.lang.String r6 = java.lang.String.valueOf(r6)     // Catch: java.lang.Throwable -> L5c
            int r7 = r6.length()     // Catch: java.lang.Throwable -> L5c
            if (r7 == 0) goto L72
            r5.concat(r6)     // Catch: java.lang.Throwable -> L5c
        L72:
            if (r2 == 0) goto L77
            r2.close()
        L77:
            return r1
        L78:
            if (r2 == 0) goto L7d
            r2.close()
        L7d:
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.dynamite.DynamiteModule.l(android.content.Context, java.lang.String, boolean):int");
    }

    public static DynamiteModule m(Context context, String str, int i2) throws LoadingException, RemoteException {
        m mVar;
        lm1 a2;
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 51);
        sb.append("Selected remote version of ");
        sb.append(str);
        sb.append(", version >= ");
        sb.append(i2);
        synchronized (DynamiteModule.class) {
            mVar = d;
        }
        if (mVar != null) {
            b bVar = g.get();
            if (bVar != null && bVar.a != null) {
                Context applicationContext = context.getApplicationContext();
                Cursor cursor = bVar.a;
                nl2.H1(null);
                if (i().booleanValue()) {
                    a2 = mVar.l1(nl2.H1(applicationContext), str, i2, nl2.H1(cursor));
                } else {
                    a2 = mVar.a(nl2.H1(applicationContext), str, i2, nl2.H1(cursor));
                }
                Context context2 = (Context) nl2.G1(a2);
                if (context2 != null) {
                    return new DynamiteModule(context2);
                }
                throw new LoadingException("Failed to get module context", (com.google.android.gms.dynamite.b) null);
            }
            throw new LoadingException("No result cursor", (com.google.android.gms.dynamite.b) null);
        }
        throw new LoadingException("DynamiteLoaderV2 was not cached.", (com.google.android.gms.dynamite.b) null);
    }

    /* JADX WARN: Code restructure failed: missing block: B:21:0x006c, code lost:
        if (k(r8) != false) goto L26;
     */
    /* JADX WARN: Removed duplicated region for block: B:48:0x009f  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static int n(android.content.Context r8, java.lang.String r9, boolean r10) throws com.google.android.gms.dynamite.DynamiteModule.LoadingException {
        /*
            r0 = 0
            android.content.ContentResolver r1 = r8.getContentResolver()     // Catch: java.lang.Throwable -> L8a java.lang.Exception -> L8c
            if (r10 == 0) goto La
            java.lang.String r8 = "api_force_staging"
            goto Lc
        La:
            java.lang.String r8 = "api"
        Lc:
            int r10 = r8.length()     // Catch: java.lang.Throwable -> L8a java.lang.Exception -> L8c
            int r10 = r10 + 42
            java.lang.String r2 = java.lang.String.valueOf(r9)     // Catch: java.lang.Throwable -> L8a java.lang.Exception -> L8c
            int r2 = r2.length()     // Catch: java.lang.Throwable -> L8a java.lang.Exception -> L8c
            int r10 = r10 + r2
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch: java.lang.Throwable -> L8a java.lang.Exception -> L8c
            r2.<init>(r10)     // Catch: java.lang.Throwable -> L8a java.lang.Exception -> L8c
            java.lang.String r10 = "content://com.google.android.gms.chimera/"
            r2.append(r10)     // Catch: java.lang.Throwable -> L8a java.lang.Exception -> L8c
            r2.append(r8)     // Catch: java.lang.Throwable -> L8a java.lang.Exception -> L8c
            java.lang.String r8 = "/"
            r2.append(r8)     // Catch: java.lang.Throwable -> L8a java.lang.Exception -> L8c
            r2.append(r9)     // Catch: java.lang.Throwable -> L8a java.lang.Exception -> L8c
            java.lang.String r8 = r2.toString()     // Catch: java.lang.Throwable -> L8a java.lang.Exception -> L8c
            android.net.Uri r2 = android.net.Uri.parse(r8)     // Catch: java.lang.Throwable -> L8a java.lang.Exception -> L8c
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            android.database.Cursor r8 = r1.query(r2, r3, r4, r5, r6)     // Catch: java.lang.Throwable -> L8a java.lang.Exception -> L8c
            if (r8 == 0) goto L82
            boolean r9 = r8.moveToFirst()     // Catch: java.lang.Throwable -> L79 java.lang.Exception -> L7d
            if (r9 == 0) goto L82
            r9 = 0
            int r9 = r8.getInt(r9)     // Catch: java.lang.Throwable -> L79 java.lang.Exception -> L7d
            if (r9 <= 0) goto L72
            java.lang.Class<com.google.android.gms.dynamite.DynamiteModule> r10 = com.google.android.gms.dynamite.DynamiteModule.class
            monitor-enter(r10)     // Catch: java.lang.Throwable -> L79 java.lang.Exception -> L7d
            r1 = 2
            java.lang.String r1 = r8.getString(r1)     // Catch: java.lang.Throwable -> L6f
            com.google.android.gms.dynamite.DynamiteModule.e = r1     // Catch: java.lang.Throwable -> L6f
            java.lang.String r1 = "loaderVersion"
            int r1 = r8.getColumnIndex(r1)     // Catch: java.lang.Throwable -> L6f
            if (r1 < 0) goto L67
            int r1 = r8.getInt(r1)     // Catch: java.lang.Throwable -> L6f
            com.google.android.gms.dynamite.DynamiteModule.f = r1     // Catch: java.lang.Throwable -> L6f
        L67:
            monitor-exit(r10)     // Catch: java.lang.Throwable -> L6f
            boolean r10 = k(r8)     // Catch: java.lang.Throwable -> L79 java.lang.Exception -> L7d
            if (r10 == 0) goto L72
            goto L73
        L6f:
            r9 = move-exception
            monitor-exit(r10)     // Catch: java.lang.Throwable -> L6f
            throw r9     // Catch: java.lang.Throwable -> L79 java.lang.Exception -> L7d
        L72:
            r0 = r8
        L73:
            if (r0 == 0) goto L78
            r0.close()
        L78:
            return r9
        L79:
            r9 = move-exception
            r0 = r8
            r8 = r9
            goto L9d
        L7d:
            r9 = move-exception
            r7 = r9
            r9 = r8
            r8 = r7
            goto L8e
        L82:
            com.google.android.gms.dynamite.DynamiteModule$LoadingException r9 = new com.google.android.gms.dynamite.DynamiteModule$LoadingException     // Catch: java.lang.Throwable -> L79 java.lang.Exception -> L7d
            java.lang.String r10 = "Failed to connect to dynamite module ContentResolver."
            r9.<init>(r10, r0)     // Catch: java.lang.Throwable -> L79 java.lang.Exception -> L7d
            throw r9     // Catch: java.lang.Throwable -> L79 java.lang.Exception -> L7d
        L8a:
            r8 = move-exception
            goto L9d
        L8c:
            r8 = move-exception
            r9 = r0
        L8e:
            boolean r10 = r8 instanceof com.google.android.gms.dynamite.DynamiteModule.LoadingException     // Catch: java.lang.Throwable -> L9b
            if (r10 == 0) goto L93
            throw r8     // Catch: java.lang.Throwable -> L9b
        L93:
            com.google.android.gms.dynamite.DynamiteModule$LoadingException r10 = new com.google.android.gms.dynamite.DynamiteModule$LoadingException     // Catch: java.lang.Throwable -> L9b
            java.lang.String r1 = "V2 version check failed"
            r10.<init>(r1, r8, r0)     // Catch: java.lang.Throwable -> L9b
            throw r10     // Catch: java.lang.Throwable -> L9b
        L9b:
            r8 = move-exception
            r0 = r9
        L9d:
            if (r0 == 0) goto La2
            r0.close()
        La2:
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.dynamite.DynamiteModule.n(android.content.Context, java.lang.String, boolean):int");
    }

    @RecentlyNonNull
    public final IBinder c(@RecentlyNonNull String str) throws LoadingException {
        try {
            return (IBinder) this.a.getClassLoader().loadClass(str).newInstance();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e2) {
            String valueOf = String.valueOf(str);
            throw new LoadingException(valueOf.length() != 0 ? "Failed to instantiate module class: ".concat(valueOf) : new String("Failed to instantiate module class: "), e2, null);
        }
    }
}
