package com.google.android.gms.dynamite;

import android.content.Context;
import com.google.android.gms.dynamite.DynamiteModule;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* loaded from: classes.dex */
public final class e implements DynamiteModule.a {
    @Override // com.google.android.gms.dynamite.DynamiteModule.a
    public final DynamiteModule.a.C0113a a(Context context, String str, DynamiteModule.a.b bVar) throws DynamiteModule.LoadingException {
        DynamiteModule.a.C0113a c0113a = new DynamiteModule.a.C0113a();
        int b = bVar.b(context, str);
        c0113a.a = b;
        if (b != 0) {
            c0113a.b = bVar.a(context, str, false);
        } else {
            c0113a.b = bVar.a(context, str, true);
        }
        int i = c0113a.a;
        if (i == 0 && c0113a.b == 0) {
            c0113a.c = 0;
        } else if (i >= c0113a.b) {
            c0113a.c = -1;
        } else {
            c0113a.c = 1;
        }
        return c0113a;
    }
}
