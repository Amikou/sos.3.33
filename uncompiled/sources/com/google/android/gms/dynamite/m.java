package com.google.android.gms.dynamite;

import android.os.IInterface;
import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* loaded from: classes.dex */
public interface m extends IInterface {
    lm1 a(lm1 lm1Var, String str, int i, lm1 lm1Var2) throws RemoteException;

    lm1 l1(lm1 lm1Var, String str, int i, lm1 lm1Var2) throws RemoteException;
}
