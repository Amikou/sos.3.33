package com.google.android.gms.measurement;

import android.app.Service;
import android.app.job.JobParameters;
import android.content.Intent;
import android.os.IBinder;
import androidx.legacy.content.WakefulBroadcastReceiver;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* loaded from: classes.dex */
public final class AppMeasurementService extends Service implements pt5 {
    public st5<AppMeasurementService> a;

    @Override // defpackage.pt5
    public final void a(Intent intent) {
        WakefulBroadcastReceiver.b(intent);
    }

    @Override // defpackage.pt5
    public final void b(JobParameters jobParameters, boolean z) {
        throw new UnsupportedOperationException();
    }

    public final st5<AppMeasurementService> c() {
        if (this.a == null) {
            this.a = new st5<>(this);
        }
        return this.a;
    }

    @Override // defpackage.pt5
    public final boolean d(int i) {
        return stopSelfResult(i);
    }

    @Override // android.app.Service
    public IBinder onBind(Intent intent) {
        return c().e(intent);
    }

    @Override // android.app.Service
    public void onCreate() {
        super.onCreate();
        c().a();
    }

    @Override // android.app.Service
    public void onDestroy() {
        c().b();
        super.onDestroy();
    }

    @Override // android.app.Service
    public void onRebind(Intent intent) {
        c().h(intent);
    }

    @Override // android.app.Service
    public int onStartCommand(Intent intent, int i, int i2) {
        c().c(intent, i, i2);
        return 2;
    }

    @Override // android.app.Service
    public boolean onUnbind(Intent intent) {
        c().f(intent);
        return true;
    }
}
