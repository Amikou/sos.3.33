package com.google.android.gms.measurement;

import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
@TargetApi(24)
/* loaded from: classes.dex */
public final class AppMeasurementJobService extends JobService implements pt5 {
    public st5<AppMeasurementJobService> a;

    @Override // defpackage.pt5
    public final void a(Intent intent) {
    }

    @Override // defpackage.pt5
    @TargetApi(24)
    public final void b(JobParameters jobParameters, boolean z) {
        jobFinished(jobParameters, false);
    }

    public final st5<AppMeasurementJobService> c() {
        if (this.a == null) {
            this.a = new st5<>(this);
        }
        return this.a;
    }

    @Override // defpackage.pt5
    public final boolean d(int i) {
        throw new UnsupportedOperationException();
    }

    @Override // android.app.Service
    public void onCreate() {
        super.onCreate();
        c().a();
    }

    @Override // android.app.Service
    public void onDestroy() {
        c().b();
        super.onDestroy();
    }

    @Override // android.app.Service
    public void onRebind(Intent intent) {
        c().h(intent);
    }

    @Override // android.app.job.JobService
    public boolean onStartJob(JobParameters jobParameters) {
        c().g(jobParameters);
        return true;
    }

    @Override // android.app.job.JobService
    public boolean onStopJob(JobParameters jobParameters) {
        return false;
    }

    @Override // android.app.Service
    public boolean onUnbind(Intent intent) {
        c().f(intent);
        return true;
    }
}
