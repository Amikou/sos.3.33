package com.google.android.gms.measurement.internal;

import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Pair;
import com.google.android.gms.internal.measurement.b1;
import com.google.android.gms.internal.measurement.c1;
import com.google.android.gms.internal.measurement.d1;
import com.google.android.gms.internal.measurement.e1;
import com.google.android.gms.internal.measurement.f1;
import com.google.android.gms.internal.measurement.g1;
import com.google.android.gms.internal.measurement.j1;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* loaded from: classes.dex */
public final class m implements Callable<byte[]> {
    public final /* synthetic */ zzas a;
    public final /* synthetic */ String b;
    public final /* synthetic */ pl5 c;

    public m(pl5 pl5Var, zzas zzasVar, String str) {
        this.c = pl5Var;
        this.a = zzasVar;
        this.b = str;
    }

    @Override // java.util.concurrent.Callable
    public final /* bridge */ /* synthetic */ byte[] call() throws Exception {
        fw5 fw5Var;
        fw5 fw5Var2;
        byte[] bArr;
        fw5 fw5Var3;
        mw5 mw5Var;
        yk5 yk5Var;
        bk5 bk5Var;
        String str;
        Bundle bundle;
        dk5 dk5Var;
        byte[] bArr2;
        v55 a;
        long j;
        fw5Var = this.c.a;
        fw5Var.i();
        fw5Var2 = this.c.a;
        xp5 Y = fw5Var2.Y();
        zzas zzasVar = this.a;
        String str2 = this.b;
        Y.e();
        ck5.t();
        zt2.j(zzasVar);
        zt2.f(str2);
        if (!Y.a.z().v(str2, qf5.V)) {
            Y.a.w().u().b("Generating ScionPayload disabled. packageName", str2);
            return new byte[0];
        } else if (!"_iap".equals(zzasVar.a) && !"_iapx".equals(zzasVar.a)) {
            Y.a.w().u().c("Generating a payload for this event is not available. package_name, event_name", str2, zzasVar.a);
            return null;
        } else {
            bk5 z = e1.z();
            Y.b.V().M();
            try {
                yk5 c0 = Y.b.V().c0(str2);
                if (c0 == null) {
                    Y.a.w().u().b("Log and bundle not available. package_name", str2);
                    bArr = new byte[0];
                    fw5Var3 = Y.b;
                } else if (!c0.f()) {
                    Y.a.w().u().b("Log and bundle disabled. package_name", str2);
                    bArr = new byte[0];
                    fw5Var3 = Y.b;
                } else {
                    dk5 K0 = f1.K0();
                    K0.Y(1);
                    K0.v("android");
                    if (!TextUtils.isEmpty(c0.N())) {
                        K0.E(c0.N());
                    }
                    if (!TextUtils.isEmpty(c0.i0())) {
                        K0.C((String) zt2.j(c0.i0()));
                    }
                    if (!TextUtils.isEmpty(c0.e0())) {
                        K0.G((String) zt2.j(c0.e0()));
                    }
                    if (c0.g0() != -2147483648L) {
                        K0.a0((int) c0.g0());
                    }
                    K0.H(c0.k0());
                    K0.l0(c0.d());
                    String Q = c0.Q();
                    String S = c0.S();
                    z16.a();
                    if (Y.a.z().v(c0.N(), qf5.h0)) {
                        String U = c0.U();
                        if (!TextUtils.isEmpty(Q)) {
                            K0.U(Q);
                        } else if (!TextUtils.isEmpty(U)) {
                            K0.o0(U);
                        } else if (!TextUtils.isEmpty(S)) {
                            K0.h0(S);
                        }
                    } else if (!TextUtils.isEmpty(Q)) {
                        K0.U(Q);
                    } else if (!TextUtils.isEmpty(S)) {
                        K0.h0(S);
                    }
                    t45 f0 = Y.b.f0(str2);
                    K0.P(c0.b());
                    if (Y.a.h() && Y.a.z().F(K0.D()) && f0.f() && !TextUtils.isEmpty(null)) {
                        K0.e0(null);
                    }
                    K0.p0(f0.d());
                    if (f0.f()) {
                        Pair<String, Boolean> j2 = Y.b.a0().j(c0.N(), f0);
                        if (c0.G() && !TextUtils.isEmpty((CharSequence) j2.first)) {
                            try {
                                K0.J(xp5.j((String) j2.first, Long.toString(zzasVar.h0)));
                                Object obj = j2.second;
                                if (obj != null) {
                                    K0.L(((Boolean) obj).booleanValue());
                                }
                            } catch (SecurityException e) {
                                Y.a.w().u().b("Resettable device id encryption failed", e.getMessage());
                                bArr = new byte[0];
                                fw5Var3 = Y.b;
                            }
                        }
                    }
                    Y.a.S().i();
                    K0.y(Build.MODEL);
                    Y.a.S().i();
                    K0.x(Build.VERSION.RELEASE);
                    K0.B((int) Y.a.S().l());
                    K0.A(Y.a.S().n());
                    try {
                        if (f0.h() && c0.O() != null) {
                            K0.N(xp5.j((String) zt2.j(c0.O()), Long.toString(zzasVar.h0)));
                        }
                        if (!TextUtils.isEmpty(c0.Y())) {
                            K0.Z((String) zt2.j(c0.Y()));
                        }
                        String N = c0.N();
                        List<mw5> V = Y.b.V().V(N);
                        Iterator<mw5> it = V.iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                mw5Var = null;
                                break;
                            }
                            mw5Var = it.next();
                            if ("_lte".equals(mw5Var.c)) {
                                break;
                            }
                        }
                        if (mw5Var == null || mw5Var.e == null) {
                            mw5 mw5Var2 = new mw5(N, "auto", "_lte", Y.a.a().a(), 0L);
                            V.add(mw5Var2);
                            Y.b.V().T(mw5Var2);
                        }
                        r Z = Y.b.Z();
                        Z.a.w().v().a("Checking account type status for ad personalization signals");
                        if (Z.a.S().r()) {
                            String N2 = c0.N();
                            zt2.j(N2);
                            if (c0.G() && Z.b.T().o(N2)) {
                                Z.a.w().u().a("Turning off ad personalization due to account type");
                                Iterator<mw5> it2 = V.iterator();
                                while (true) {
                                    if (!it2.hasNext()) {
                                        break;
                                    } else if ("_npa".equals(it2.next().c)) {
                                        it2.remove();
                                        break;
                                    }
                                }
                                V.add(new mw5(N2, "auto", "_npa", Z.a.a().a(), 1L));
                            }
                        }
                        j1[] j1VarArr = new j1[V.size()];
                        for (int i = 0; i < V.size(); i++) {
                            il5 G = j1.G();
                            G.x(V.get(i).c);
                            G.v(V.get(i).d);
                            Y.b.Z().u(G, V.get(i).e);
                            j1VarArr[i] = G.o();
                        }
                        K0.G0(Arrays.asList(j1VarArr));
                        rg5 a2 = rg5.a(zzasVar);
                        Y.a.G().u(a2.d, Y.b.V().v(str2));
                        Y.a.G().t(a2, Y.a.z().k(str2));
                        Bundle bundle2 = a2.d;
                        bundle2.putLong("_c", 1L);
                        Y.a.w().u().a("Marking in-app purchase as real-time");
                        bundle2.putLong("_r", 1L);
                        bundle2.putString("_o", zzasVar.g0);
                        if (Y.a.G().H(K0.D())) {
                            Y.a.G().z(bundle2, "_dbg", 1L);
                            Y.a.G().z(bundle2, "_r", 1L);
                        }
                        v55 Q2 = Y.b.V().Q(str2, zzasVar.a);
                        if (Q2 == null) {
                            dk5Var = K0;
                            yk5Var = c0;
                            bk5Var = z;
                            str = str2;
                            bundle = bundle2;
                            bArr2 = null;
                            a = new v55(str2, zzasVar.a, 0L, 0L, 0L, zzasVar.h0, 0L, null, null, null, null);
                            j = 0;
                        } else {
                            yk5Var = c0;
                            bk5Var = z;
                            str = str2;
                            bundle = bundle2;
                            dk5Var = K0;
                            bArr2 = null;
                            long j3 = Q2.f;
                            a = Q2.a(zzasVar.h0);
                            j = j3;
                        }
                        Y.b.V().R(a);
                        s55 s55Var = new s55(Y.a, zzasVar.g0, str, zzasVar.a, zzasVar.h0, j, bundle);
                        tj5 H = b1.H();
                        H.M(s55Var.d);
                        H.J(s55Var.b);
                        H.O(s55Var.e);
                        y55 y55Var = new y55(s55Var.f);
                        while (y55Var.hasNext()) {
                            String next = y55Var.next();
                            yj5 J = d1.J();
                            J.v(next);
                            Object I1 = s55Var.f.I1(next);
                            if (I1 != null) {
                                Y.b.Z().v(J, I1);
                                H.D(J);
                            }
                        }
                        dk5 dk5Var2 = dk5Var;
                        dk5Var2.u0(H);
                        ik5 x = g1.x();
                        wj5 x2 = c1.x();
                        x2.x(a.c);
                        x2.v(zzasVar.a);
                        x.v(x2);
                        dk5Var2.j0(x);
                        dk5Var2.W(Y.b.X().j(yk5Var.N(), Collections.emptyList(), dk5Var2.z0(), Long.valueOf(H.L()), Long.valueOf(H.L())));
                        if (H.K()) {
                            dk5Var2.M0(H.L());
                            dk5Var2.O0(H.L());
                        }
                        long c02 = yk5Var.c0();
                        int i2 = (c02 > 0L ? 1 : (c02 == 0L ? 0 : -1));
                        if (i2 != 0) {
                            dk5Var2.R0(c02);
                        }
                        long a0 = yk5Var.a0();
                        if (a0 != 0) {
                            dk5Var2.P0(a0);
                        } else if (i2 != 0) {
                            dk5Var2.P0(c02);
                        }
                        yk5Var.n();
                        dk5Var2.Q((int) yk5Var.i());
                        Y.a.z().n();
                        dk5Var2.I(42004L);
                        dk5Var2.K0(Y.a.a().a());
                        dk5Var2.V(true);
                        bk5 bk5Var2 = bk5Var;
                        bk5Var2.x(dk5Var2);
                        yk5 yk5Var2 = yk5Var;
                        yk5Var2.b0(dk5Var2.L0());
                        yk5Var2.d0(dk5Var2.N0());
                        Y.b.V().d0(yk5Var2);
                        Y.b.V().N();
                        try {
                            return Y.b.Z().I(bk5Var2.o().c());
                        } catch (IOException e2) {
                            Y.a.w().l().c("Data loss. Failed to bundle and serialize. appId", og5.x(str), e2);
                            return bArr2;
                        }
                    } catch (SecurityException e3) {
                        Y.a.w().u().b("app instance id encryption failed", e3.getMessage());
                        bArr = new byte[0];
                        fw5Var3 = Y.b;
                    }
                }
                fw5Var3.V().O();
                return bArr;
            } finally {
                Y.b.V().O();
            }
        }
    }
}
