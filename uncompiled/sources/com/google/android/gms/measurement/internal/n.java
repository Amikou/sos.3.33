package com.google.android.gms.measurement.internal;

import java.util.List;
import java.util.concurrent.Callable;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* loaded from: classes.dex */
public final class n implements Callable<List<mw5>> {
    public final /* synthetic */ String a;
    public final /* synthetic */ pl5 b;

    public n(pl5 pl5Var, String str) {
        this.b = pl5Var;
        this.a = str;
    }

    @Override // java.util.concurrent.Callable
    public final /* bridge */ /* synthetic */ List<mw5> call() throws Exception {
        fw5 fw5Var;
        fw5 fw5Var2;
        fw5Var = this.b.a;
        fw5Var.i();
        fw5Var2 = this.b.a;
        return fw5Var2.V().V(this.a);
    }
}
