package com.google.android.gms.measurement.internal;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.util.Iterator;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* loaded from: classes.dex */
public final class zzaq extends AbstractSafeParcelable implements Iterable<String> {
    public static final Parcelable.Creator<zzaq> CREATOR = new c65();
    public final Bundle a;

    public zzaq(Bundle bundle) {
        this.a = bundle;
    }

    public final Object I1(String str) {
        return this.a.get(str);
    }

    public final Long J1(String str) {
        return Long.valueOf(this.a.getLong("value"));
    }

    public final Double K1(String str) {
        return Double.valueOf(this.a.getDouble("value"));
    }

    public final String L1(String str) {
        return this.a.getString(str);
    }

    public final int M1() {
        return this.a.size();
    }

    public final Bundle N1() {
        return new Bundle(this.a);
    }

    @Override // java.lang.Iterable
    public final Iterator<String> iterator() {
        return new y55(this);
    }

    public final String toString() {
        return this.a.toString();
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int a = yb3.a(parcel);
        yb3.e(parcel, 2, N1(), false);
        yb3.b(parcel, a);
    }
}
