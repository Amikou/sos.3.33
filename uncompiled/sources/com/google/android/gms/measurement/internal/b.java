package com.google.android.gms.measurement.internal;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* loaded from: classes.dex */
public final class b extends com.google.android.gms.internal.measurement.c implements d {
    public b(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.measurement.internal.IMeasurementService");
    }

    @Override // com.google.android.gms.measurement.internal.d
    public final List<zzkq> B1(String str, String str2, String str3, boolean z) throws RemoteException {
        Parcel F1 = F1();
        F1.writeString(null);
        F1.writeString(str2);
        F1.writeString(str3);
        a95.b(F1, z);
        Parcel b = b(15, F1);
        ArrayList createTypedArrayList = b.createTypedArrayList(zzkq.CREATOR);
        b.recycle();
        return createTypedArrayList;
    }

    @Override // com.google.android.gms.measurement.internal.d
    public final void F0(zzp zzpVar) throws RemoteException {
        Parcel F1 = F1();
        a95.d(F1, zzpVar);
        G1(6, F1);
    }

    @Override // com.google.android.gms.measurement.internal.d
    public final void P(zzaa zzaaVar, zzp zzpVar) throws RemoteException {
        Parcel F1 = F1();
        a95.d(F1, zzaaVar);
        a95.d(F1, zzpVar);
        G1(12, F1);
    }

    @Override // com.google.android.gms.measurement.internal.d
    public final void Q(long j, String str, String str2, String str3) throws RemoteException {
        Parcel F1 = F1();
        F1.writeLong(j);
        F1.writeString(str);
        F1.writeString(str2);
        F1.writeString(str3);
        G1(10, F1);
    }

    @Override // com.google.android.gms.measurement.internal.d
    public final List<zzkq> X(String str, String str2, boolean z, zzp zzpVar) throws RemoteException {
        Parcel F1 = F1();
        F1.writeString(str);
        F1.writeString(str2);
        a95.b(F1, z);
        a95.d(F1, zzpVar);
        Parcel b = b(14, F1);
        ArrayList createTypedArrayList = b.createTypedArrayList(zzkq.CREATOR);
        b.recycle();
        return createTypedArrayList;
    }

    @Override // com.google.android.gms.measurement.internal.d
    public final void X0(zzkq zzkqVar, zzp zzpVar) throws RemoteException {
        Parcel F1 = F1();
        a95.d(F1, zzkqVar);
        a95.d(F1, zzpVar);
        G1(2, F1);
    }

    @Override // com.google.android.gms.measurement.internal.d
    public final List<zzaa> Y(String str, String str2, String str3) throws RemoteException {
        Parcel F1 = F1();
        F1.writeString(null);
        F1.writeString(str2);
        F1.writeString(str3);
        Parcel b = b(17, F1);
        ArrayList createTypedArrayList = b.createTypedArrayList(zzaa.CREATOR);
        b.recycle();
        return createTypedArrayList;
    }

    @Override // com.google.android.gms.measurement.internal.d
    public final void e0(zzp zzpVar) throws RemoteException {
        Parcel F1 = F1();
        a95.d(F1, zzpVar);
        G1(18, F1);
    }

    @Override // com.google.android.gms.measurement.internal.d
    public final void f1(zzp zzpVar) throws RemoteException {
        Parcel F1 = F1();
        a95.d(F1, zzpVar);
        G1(4, F1);
    }

    @Override // com.google.android.gms.measurement.internal.d
    public final List<zzaa> g(String str, String str2, zzp zzpVar) throws RemoteException {
        Parcel F1 = F1();
        F1.writeString(str);
        F1.writeString(str2);
        a95.d(F1, zzpVar);
        Parcel b = b(16, F1);
        ArrayList createTypedArrayList = b.createTypedArrayList(zzaa.CREATOR);
        b.recycle();
        return createTypedArrayList;
    }

    @Override // com.google.android.gms.measurement.internal.d
    public final void h0(Bundle bundle, zzp zzpVar) throws RemoteException {
        Parcel F1 = F1();
        a95.d(F1, bundle);
        a95.d(F1, zzpVar);
        G1(19, F1);
    }

    @Override // com.google.android.gms.measurement.internal.d
    public final byte[] l0(zzas zzasVar, String str) throws RemoteException {
        Parcel F1 = F1();
        a95.d(F1, zzasVar);
        F1.writeString(str);
        Parcel b = b(9, F1);
        byte[] createByteArray = b.createByteArray();
        b.recycle();
        return createByteArray;
    }

    @Override // com.google.android.gms.measurement.internal.d
    public final void o(zzp zzpVar) throws RemoteException {
        Parcel F1 = F1();
        a95.d(F1, zzpVar);
        G1(20, F1);
    }

    @Override // com.google.android.gms.measurement.internal.d
    public final String u(zzp zzpVar) throws RemoteException {
        Parcel F1 = F1();
        a95.d(F1, zzpVar);
        Parcel b = b(11, F1);
        String readString = b.readString();
        b.recycle();
        return readString;
    }

    @Override // com.google.android.gms.measurement.internal.d
    public final void w1(zzas zzasVar, zzp zzpVar) throws RemoteException {
        Parcel F1 = F1();
        a95.d(F1, zzasVar);
        a95.d(F1, zzpVar);
        G1(1, F1);
    }
}
