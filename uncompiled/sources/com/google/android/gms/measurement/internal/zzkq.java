package com.google.android.gms.measurement.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* loaded from: classes.dex */
public final class zzkq extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzkq> CREATOR = new jw5();
    public final int a;
    public final String f0;
    public final long g0;
    public final Long h0;
    public final String i0;
    public final String j0;
    public final Double k0;

    public zzkq(int i, String str, long j, Long l, Float f, String str2, String str3, Double d) {
        this.a = i;
        this.f0 = str;
        this.g0 = j;
        this.h0 = l;
        if (i == 1) {
            this.k0 = f != null ? Double.valueOf(f.doubleValue()) : null;
        } else {
            this.k0 = d;
        }
        this.i0 = str2;
        this.j0 = str3;
    }

    public final Object I1() {
        Long l = this.h0;
        if (l != null) {
            return l;
        }
        Double d = this.k0;
        if (d != null) {
            return d;
        }
        String str = this.i0;
        if (str != null) {
            return str;
        }
        return null;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        jw5.a(this, parcel, i);
    }

    public zzkq(mw5 mw5Var) {
        this(mw5Var.c, mw5Var.d, mw5Var.e, mw5Var.b);
    }

    public zzkq(String str, long j, Object obj, String str2) {
        zt2.f(str);
        this.a = 2;
        this.f0 = str;
        this.g0 = j;
        this.j0 = str2;
        if (obj == null) {
            this.h0 = null;
            this.k0 = null;
            this.i0 = null;
        } else if (obj instanceof Long) {
            this.h0 = (Long) obj;
            this.k0 = null;
            this.i0 = null;
        } else if (obj instanceof String) {
            this.h0 = null;
            this.k0 = null;
            this.i0 = (String) obj;
        } else if (obj instanceof Double) {
            this.h0 = null;
            this.k0 = (Double) obj;
            this.i0 = null;
        } else {
            throw new IllegalArgumentException("User attribute given of un-supported type");
        }
    }
}
