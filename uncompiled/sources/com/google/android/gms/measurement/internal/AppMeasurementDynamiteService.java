package com.google.android.gms.measurement.internal;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import android.text.TextUtils;
import com.google.android.gms.common.util.DynamiteApi;
import com.google.android.gms.internal.measurement.zzcl;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement-sdk@@19.0.0 */
@DynamiteApi
/* loaded from: classes.dex */
public class AppMeasurementDynamiteService extends com.google.android.gms.internal.measurement.i {
    public ck5 a = null;
    public final Map<Integer, em5> b = new rh();

    public final void F1() {
        if (this.a == null) {
            throw new IllegalStateException("Attempting to perform action before initialize.");
        }
    }

    public final void G1(com.google.android.gms.internal.measurement.m mVar, String str) {
        F1();
        this.a.G().R(mVar, str);
    }

    @Override // com.google.android.gms.internal.measurement.j
    public void beginAdUnitExposure(String str, long j) throws RemoteException {
        F1();
        this.a.d().f(str, j);
    }

    @Override // com.google.android.gms.internal.measurement.j
    public void clearConditionalUserProperty(String str, String str2, Bundle bundle) throws RemoteException {
        F1();
        this.a.F().B(str, str2, bundle);
    }

    @Override // com.google.android.gms.internal.measurement.j
    public void clearMeasurementEnabled(long j) throws RemoteException {
        F1();
        this.a.F().T(null);
    }

    @Override // com.google.android.gms.internal.measurement.j
    public void endAdUnitExposure(String str, long j) throws RemoteException {
        F1();
        this.a.d().g(str, j);
    }

    @Override // com.google.android.gms.internal.measurement.j
    public void generateEventId(com.google.android.gms.internal.measurement.m mVar) throws RemoteException {
        F1();
        long h0 = this.a.G().h0();
        F1();
        this.a.G().S(mVar, h0);
    }

    @Override // com.google.android.gms.internal.measurement.j
    public void getAppInstanceId(com.google.android.gms.internal.measurement.m mVar) throws RemoteException {
        F1();
        this.a.q().p(new xm5(this, mVar));
    }

    @Override // com.google.android.gms.internal.measurement.j
    public void getCachedAppInstanceId(com.google.android.gms.internal.measurement.m mVar) throws RemoteException {
        F1();
        G1(mVar, this.a.F().o());
    }

    @Override // com.google.android.gms.internal.measurement.j
    public void getConditionalUserProperties(String str, String str2, com.google.android.gms.internal.measurement.m mVar) throws RemoteException {
        F1();
        this.a.q().p(new gx5(this, mVar, str, str2));
    }

    @Override // com.google.android.gms.internal.measurement.j
    public void getCurrentScreenClass(com.google.android.gms.internal.measurement.m mVar) throws RemoteException {
        F1();
        G1(mVar, this.a.F().F());
    }

    @Override // com.google.android.gms.internal.measurement.j
    public void getCurrentScreenName(com.google.android.gms.internal.measurement.m mVar) throws RemoteException {
        F1();
        G1(mVar, this.a.F().E());
    }

    @Override // com.google.android.gms.internal.measurement.j
    public void getGmpAppId(com.google.android.gms.internal.measurement.m mVar) throws RemoteException {
        F1();
        G1(mVar, this.a.F().G());
    }

    @Override // com.google.android.gms.internal.measurement.j
    public void getMaxUserProperties(String str, com.google.android.gms.internal.measurement.m mVar) throws RemoteException {
        F1();
        this.a.F().y(str);
        F1();
        this.a.G().T(mVar, 25);
    }

    @Override // com.google.android.gms.internal.measurement.j
    public void getTestFlag(com.google.android.gms.internal.measurement.m mVar, int i) throws RemoteException {
        F1();
        if (i == 0) {
            this.a.G().R(mVar, this.a.F().P());
        } else if (i == 1) {
            this.a.G().S(mVar, this.a.F().Q().longValue());
        } else if (i != 2) {
            if (i == 3) {
                this.a.G().T(mVar, this.a.F().R().intValue());
            } else if (i != 4) {
            } else {
                this.a.G().V(mVar, this.a.F().O().booleanValue());
            }
        } else {
            sw5 G = this.a.G();
            double doubleValue = this.a.F().S().doubleValue();
            Bundle bundle = new Bundle();
            bundle.putDouble("r", doubleValue);
            try {
                mVar.K0(bundle);
            } catch (RemoteException e) {
                G.a.w().p().b("Error returning double value to wrapper", e);
            }
        }
    }

    @Override // com.google.android.gms.internal.measurement.j
    public void getUserProperties(String str, String str2, boolean z, com.google.android.gms.internal.measurement.m mVar) throws RemoteException {
        F1();
        this.a.q().p(new fs5(this, mVar, str, str2, z));
    }

    @Override // com.google.android.gms.internal.measurement.j
    public void initForTests(Map map) throws RemoteException {
        F1();
    }

    @Override // com.google.android.gms.internal.measurement.j
    public void initialize(lm1 lm1Var, zzcl zzclVar, long j) throws RemoteException {
        ck5 ck5Var = this.a;
        if (ck5Var == null) {
            this.a = ck5.e((Context) zt2.j((Context) nl2.G1(lm1Var)), zzclVar, Long.valueOf(j));
        } else {
            ck5Var.w().p().a("Attempting to initialize multiple times");
        }
    }

    @Override // com.google.android.gms.internal.measurement.j
    public void isDataCollectionEnabled(com.google.android.gms.internal.measurement.m mVar) throws RemoteException {
        F1();
        this.a.q().p(new zy5(this, mVar));
    }

    @Override // com.google.android.gms.internal.measurement.j
    public void logEvent(String str, String str2, Bundle bundle, boolean z, boolean z2, long j) throws RemoteException {
        F1();
        this.a.F().a0(str, str2, bundle, z, z2, j);
    }

    @Override // com.google.android.gms.internal.measurement.j
    public void logEventAndBundle(String str, String str2, Bundle bundle, com.google.android.gms.internal.measurement.m mVar, long j) throws RemoteException {
        Bundle bundle2;
        F1();
        zt2.f(str2);
        if (bundle != null) {
            bundle2 = new Bundle(bundle);
        } else {
            bundle2 = new Bundle();
        }
        bundle2.putString("_o", "app");
        this.a.q().p(new tp5(this, mVar, new zzas(str2, new zzaq(bundle), "app", j), str));
    }

    @Override // com.google.android.gms.internal.measurement.j
    public void logHealthData(int i, String str, lm1 lm1Var, lm1 lm1Var2, lm1 lm1Var3) throws RemoteException {
        F1();
        this.a.w().y(i, true, false, str, lm1Var == null ? null : nl2.G1(lm1Var), lm1Var2 == null ? null : nl2.G1(lm1Var2), lm1Var3 != null ? nl2.G1(lm1Var3) : null);
    }

    @Override // com.google.android.gms.internal.measurement.j
    public void onActivityCreated(lm1 lm1Var, Bundle bundle, long j) throws RemoteException {
        F1();
        bp5 bp5Var = this.a.F().c;
        if (bp5Var != null) {
            this.a.F().N();
            bp5Var.onActivityCreated((Activity) nl2.G1(lm1Var), bundle);
        }
    }

    @Override // com.google.android.gms.internal.measurement.j
    public void onActivityDestroyed(lm1 lm1Var, long j) throws RemoteException {
        F1();
        bp5 bp5Var = this.a.F().c;
        if (bp5Var != null) {
            this.a.F().N();
            bp5Var.onActivityDestroyed((Activity) nl2.G1(lm1Var));
        }
    }

    @Override // com.google.android.gms.internal.measurement.j
    public void onActivityPaused(lm1 lm1Var, long j) throws RemoteException {
        F1();
        bp5 bp5Var = this.a.F().c;
        if (bp5Var != null) {
            this.a.F().N();
            bp5Var.onActivityPaused((Activity) nl2.G1(lm1Var));
        }
    }

    @Override // com.google.android.gms.internal.measurement.j
    public void onActivityResumed(lm1 lm1Var, long j) throws RemoteException {
        F1();
        bp5 bp5Var = this.a.F().c;
        if (bp5Var != null) {
            this.a.F().N();
            bp5Var.onActivityResumed((Activity) nl2.G1(lm1Var));
        }
    }

    @Override // com.google.android.gms.internal.measurement.j
    public void onActivitySaveInstanceState(lm1 lm1Var, com.google.android.gms.internal.measurement.m mVar, long j) throws RemoteException {
        F1();
        bp5 bp5Var = this.a.F().c;
        Bundle bundle = new Bundle();
        if (bp5Var != null) {
            this.a.F().N();
            bp5Var.onActivitySaveInstanceState((Activity) nl2.G1(lm1Var), bundle);
        }
        try {
            mVar.K0(bundle);
        } catch (RemoteException e) {
            this.a.w().p().b("Error returning bundle value to wrapper", e);
        }
    }

    @Override // com.google.android.gms.internal.measurement.j
    public void onActivityStarted(lm1 lm1Var, long j) throws RemoteException {
        F1();
        if (this.a.F().c != null) {
            this.a.F().N();
            Activity activity = (Activity) nl2.G1(lm1Var);
        }
    }

    @Override // com.google.android.gms.internal.measurement.j
    public void onActivityStopped(lm1 lm1Var, long j) throws RemoteException {
        F1();
        if (this.a.F().c != null) {
            this.a.F().N();
            Activity activity = (Activity) nl2.G1(lm1Var);
        }
    }

    @Override // com.google.android.gms.internal.measurement.j
    public void performAction(Bundle bundle, com.google.android.gms.internal.measurement.m mVar, long j) throws RemoteException {
        F1();
        mVar.K0(null);
    }

    @Override // com.google.android.gms.internal.measurement.j
    public void registerOnMeasurementEventListener(com.google.android.gms.internal.measurement.p pVar) throws RemoteException {
        em5 em5Var;
        F1();
        synchronized (this.b) {
            em5Var = this.b.get(Integer.valueOf(pVar.c()));
            if (em5Var == null) {
                em5Var = new b16(this, pVar);
                this.b.put(Integer.valueOf(pVar.c()), em5Var);
            }
        }
        this.a.F().v(em5Var);
    }

    @Override // com.google.android.gms.internal.measurement.j
    public void resetAnalyticsData(long j) throws RemoteException {
        F1();
        this.a.F().r(j);
    }

    @Override // com.google.android.gms.internal.measurement.j
    public void setConditionalUserProperty(Bundle bundle, long j) throws RemoteException {
        F1();
        if (bundle == null) {
            this.a.w().l().a("Conditional user property must not be null");
        } else {
            this.a.F().A(bundle, j);
        }
    }

    @Override // com.google.android.gms.internal.measurement.j
    public void setConsent(Bundle bundle, long j) throws RemoteException {
        F1();
        dp5 F = this.a.F();
        h16.a();
        if (F.a.z().v(null, qf5.A0) && !TextUtils.isEmpty(F.a.c().o())) {
            F.a.w().s().a("Using developer consent only; google app id found");
        } else {
            F.U(bundle, 0, j);
        }
    }

    @Override // com.google.android.gms.internal.measurement.j
    public void setConsentThirdParty(Bundle bundle, long j) throws RemoteException {
        F1();
        this.a.F().U(bundle, -20, j);
    }

    @Override // com.google.android.gms.internal.measurement.j
    public void setCurrentScreen(lm1 lm1Var, String str, String str2, long j) throws RemoteException {
        F1();
        this.a.Q().u((Activity) nl2.G1(lm1Var), str, str2);
    }

    @Override // com.google.android.gms.internal.measurement.j
    public void setDataCollectionEnabled(boolean z) throws RemoteException {
        F1();
        dp5 F = this.a.F();
        F.g();
        F.a.q().p(new nm5(F, z));
    }

    @Override // com.google.android.gms.internal.measurement.j
    public void setDefaultEventParameters(Bundle bundle) {
        F1();
        final dp5 F = this.a.F();
        final Bundle bundle2 = bundle == null ? null : new Bundle(bundle);
        F.a.q().p(new Runnable(F, bundle2) { // from class: im5
            public final dp5 a;
            public final Bundle f0;

            {
                this.a = F;
                this.f0 = bundle2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                this.a.H(this.f0);
            }
        });
    }

    @Override // com.google.android.gms.internal.measurement.j
    public void setEventInterceptor(com.google.android.gms.internal.measurement.p pVar) throws RemoteException {
        F1();
        vz5 vz5Var = new vz5(this, pVar);
        if (this.a.q().l()) {
            this.a.F().u(vz5Var);
        } else {
            this.a.q().p(new wu5(this, vz5Var));
        }
    }

    @Override // com.google.android.gms.internal.measurement.j
    public void setInstanceIdProvider(kb5 kb5Var) throws RemoteException {
        F1();
    }

    @Override // com.google.android.gms.internal.measurement.j
    public void setMeasurementEnabled(boolean z, long j) throws RemoteException {
        F1();
        this.a.F().T(Boolean.valueOf(z));
    }

    @Override // com.google.android.gms.internal.measurement.j
    public void setMinimumSessionDuration(long j) throws RemoteException {
        F1();
    }

    @Override // com.google.android.gms.internal.measurement.j
    public void setSessionTimeoutDuration(long j) throws RemoteException {
        F1();
        dp5 F = this.a.F();
        F.a.q().p(new dn5(F, j));
    }

    @Override // com.google.android.gms.internal.measurement.j
    public void setUserId(String str, long j) throws RemoteException {
        F1();
        if (this.a.z().v(null, qf5.y0) && str != null && str.length() == 0) {
            this.a.w().p().a("User ID must be non-empty");
        } else {
            this.a.F().d0(null, "_id", str, true, j);
        }
    }

    @Override // com.google.android.gms.internal.measurement.j
    public void setUserProperty(String str, String str2, lm1 lm1Var, boolean z, long j) throws RemoteException {
        F1();
        this.a.F().d0(str, str2, nl2.G1(lm1Var), z, j);
    }

    @Override // com.google.android.gms.internal.measurement.j
    public void unregisterOnMeasurementEventListener(com.google.android.gms.internal.measurement.p pVar) throws RemoteException {
        em5 remove;
        F1();
        synchronized (this.b) {
            remove = this.b.remove(Integer.valueOf(pVar.c()));
        }
        if (remove == null) {
            remove = new b16(this, pVar);
        }
        this.a.F().x(remove);
    }
}
