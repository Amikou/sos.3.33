package com.google.android.gms.measurement.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* loaded from: classes.dex */
public final class zzp extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzp> CREATOR = new l36();
    public final String a;
    public final String f0;
    public final String g0;
    public final String h0;
    public final long i0;
    public final long j0;
    public final String k0;
    public final boolean l0;
    public final boolean m0;
    public final long n0;
    public final String o0;
    public final long p0;
    public final long q0;
    public final int r0;
    public final boolean s0;
    public final boolean t0;
    public final String u0;
    public final Boolean v0;
    public final long w0;
    public final List<String> x0;
    public final String y0;
    public final String z0;

    public zzp(String str, String str2, String str3, long j, String str4, long j2, long j3, String str5, boolean z, boolean z2, String str6, long j4, long j5, int i, boolean z3, boolean z4, String str7, Boolean bool, long j6, List<String> list, String str8, String str9) {
        zt2.f(str);
        this.a = str;
        this.f0 = true != TextUtils.isEmpty(str2) ? str2 : null;
        this.g0 = str3;
        this.n0 = j;
        this.h0 = str4;
        this.i0 = j2;
        this.j0 = j3;
        this.k0 = str5;
        this.l0 = z;
        this.m0 = z2;
        this.o0 = str6;
        this.p0 = j4;
        this.q0 = j5;
        this.r0 = i;
        this.s0 = z3;
        this.t0 = z4;
        this.u0 = str7;
        this.v0 = bool;
        this.w0 = j6;
        this.x0 = list;
        this.y0 = str8;
        this.z0 = str9;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int a = yb3.a(parcel);
        yb3.s(parcel, 2, this.a, false);
        yb3.s(parcel, 3, this.f0, false);
        yb3.s(parcel, 4, this.g0, false);
        yb3.s(parcel, 5, this.h0, false);
        yb3.o(parcel, 6, this.i0);
        yb3.o(parcel, 7, this.j0);
        yb3.s(parcel, 8, this.k0, false);
        yb3.c(parcel, 9, this.l0);
        yb3.c(parcel, 10, this.m0);
        yb3.o(parcel, 11, this.n0);
        yb3.s(parcel, 12, this.o0, false);
        yb3.o(parcel, 13, this.p0);
        yb3.o(parcel, 14, this.q0);
        yb3.m(parcel, 15, this.r0);
        yb3.c(parcel, 16, this.s0);
        yb3.c(parcel, 18, this.t0);
        yb3.s(parcel, 19, this.u0, false);
        yb3.d(parcel, 21, this.v0, false);
        yb3.o(parcel, 22, this.w0);
        yb3.u(parcel, 23, this.x0, false);
        yb3.s(parcel, 24, this.y0, false);
        yb3.s(parcel, 25, this.z0, false);
        yb3.b(parcel, a);
    }

    public zzp(String str, String str2, String str3, String str4, long j, long j2, String str5, boolean z, boolean z2, long j3, String str6, long j4, long j5, int i, boolean z3, boolean z4, String str7, Boolean bool, long j6, List<String> list, String str8, String str9) {
        this.a = str;
        this.f0 = str2;
        this.g0 = str3;
        this.n0 = j3;
        this.h0 = str4;
        this.i0 = j;
        this.j0 = j2;
        this.k0 = str5;
        this.l0 = z;
        this.m0 = z2;
        this.o0 = str6;
        this.p0 = j4;
        this.q0 = j5;
        this.r0 = i;
        this.s0 = z3;
        this.t0 = z4;
        this.u0 = str7;
        this.v0 = bool;
        this.w0 = j6;
        this.x0 = list;
        this.y0 = str8;
        this.z0 = str9;
    }
}
