package com.google.android.gms.measurement.internal;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* loaded from: classes.dex */
public interface d extends IInterface {
    List<zzkq> B1(String str, String str2, String str3, boolean z) throws RemoteException;

    void F0(zzp zzpVar) throws RemoteException;

    void P(zzaa zzaaVar, zzp zzpVar) throws RemoteException;

    void Q(long j, String str, String str2, String str3) throws RemoteException;

    List<zzkq> T(zzp zzpVar, boolean z) throws RemoteException;

    List<zzkq> X(String str, String str2, boolean z, zzp zzpVar) throws RemoteException;

    void X0(zzkq zzkqVar, zzp zzpVar) throws RemoteException;

    List<zzaa> Y(String str, String str2, String str3) throws RemoteException;

    void e0(zzp zzpVar) throws RemoteException;

    void f1(zzp zzpVar) throws RemoteException;

    List<zzaa> g(String str, String str2, zzp zzpVar) throws RemoteException;

    void h0(Bundle bundle, zzp zzpVar) throws RemoteException;

    void i0(zzaa zzaaVar) throws RemoteException;

    void j0(zzas zzasVar, String str, String str2) throws RemoteException;

    byte[] l0(zzas zzasVar, String str) throws RemoteException;

    void o(zzp zzpVar) throws RemoteException;

    String u(zzp zzpVar) throws RemoteException;

    void w1(zzas zzasVar, zzp zzpVar) throws RemoteException;
}
