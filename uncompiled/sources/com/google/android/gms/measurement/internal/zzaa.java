package com.google.android.gms.measurement.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* loaded from: classes.dex */
public final class zzaa extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzaa> CREATOR = new f45();
    public String a;
    public String f0;
    public zzkq g0;
    public long h0;
    public boolean i0;
    public String j0;
    public final zzas k0;
    public long l0;
    public zzas m0;
    public final long n0;
    public final zzas o0;

    public zzaa(zzaa zzaaVar) {
        zt2.j(zzaaVar);
        this.a = zzaaVar.a;
        this.f0 = zzaaVar.f0;
        this.g0 = zzaaVar.g0;
        this.h0 = zzaaVar.h0;
        this.i0 = zzaaVar.i0;
        this.j0 = zzaaVar.j0;
        this.k0 = zzaaVar.k0;
        this.l0 = zzaaVar.l0;
        this.m0 = zzaaVar.m0;
        this.n0 = zzaaVar.n0;
        this.o0 = zzaaVar.o0;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int a = yb3.a(parcel);
        yb3.s(parcel, 2, this.a, false);
        yb3.s(parcel, 3, this.f0, false);
        yb3.r(parcel, 4, this.g0, i, false);
        yb3.o(parcel, 5, this.h0);
        yb3.c(parcel, 6, this.i0);
        yb3.s(parcel, 7, this.j0, false);
        yb3.r(parcel, 8, this.k0, i, false);
        yb3.o(parcel, 9, this.l0);
        yb3.r(parcel, 10, this.m0, i, false);
        yb3.o(parcel, 11, this.n0);
        yb3.r(parcel, 12, this.o0, i, false);
        yb3.b(parcel, a);
    }

    public zzaa(String str, String str2, zzkq zzkqVar, long j, boolean z, String str3, zzas zzasVar, long j2, zzas zzasVar2, long j3, zzas zzasVar3) {
        this.a = str;
        this.f0 = str2;
        this.g0 = zzkqVar;
        this.h0 = j;
        this.i0 = z;
        this.j0 = str3;
        this.k0 = zzasVar;
        this.l0 = j2;
        this.m0 = zzasVar2;
        this.n0 = j3;
        this.o0 = zzasVar3;
    }
}
