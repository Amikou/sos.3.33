package com.google.android.gms.measurement.internal;

import java.util.List;
import java.util.concurrent.Callable;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* loaded from: classes.dex */
public final class k implements Callable<List<zzaa>> {
    public final /* synthetic */ String a;
    public final /* synthetic */ String b;
    public final /* synthetic */ String c;
    public final /* synthetic */ pl5 d;

    public k(pl5 pl5Var, String str, String str2, String str3) {
        this.d = pl5Var;
        this.a = str;
        this.b = str2;
        this.c = str3;
    }

    @Override // java.util.concurrent.Callable
    public final /* bridge */ /* synthetic */ List<zzaa> call() throws Exception {
        fw5 fw5Var;
        fw5 fw5Var2;
        fw5Var = this.d.a;
        fw5Var.i();
        fw5Var2 = this.d.a;
        return fw5Var2.V().a0(this.a, this.b, this.c);
    }
}
