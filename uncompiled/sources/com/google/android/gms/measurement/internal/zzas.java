package com.google.android.gms.measurement.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* loaded from: classes.dex */
public final class zzas extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzas> CREATOR = new g65();
    public final String a;
    public final zzaq f0;
    public final String g0;
    public final long h0;

    public zzas(zzas zzasVar, long j) {
        zt2.j(zzasVar);
        this.a = zzasVar.a;
        this.f0 = zzasVar.f0;
        this.g0 = zzasVar.g0;
        this.h0 = j;
    }

    public final String toString() {
        String str = this.g0;
        String str2 = this.a;
        String valueOf = String.valueOf(this.f0);
        int length = String.valueOf(str).length();
        StringBuilder sb = new StringBuilder(length + 21 + String.valueOf(str2).length() + valueOf.length());
        sb.append("origin=");
        sb.append(str);
        sb.append(",name=");
        sb.append(str2);
        sb.append(",params=");
        sb.append(valueOf);
        return sb.toString();
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        g65.a(this, parcel, i);
    }

    public zzas(String str, zzaq zzaqVar, String str2, long j) {
        this.a = str;
        this.f0 = zzaqVar;
        this.g0 = str2;
        this.h0 = j;
    }
}
