package com.google.android.gms.measurement.internal;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.internal.measurement.a1;
import com.google.android.gms.internal.measurement.b1;
import com.google.android.gms.internal.measurement.d1;
import com.google.android.gms.internal.measurement.e1;
import com.google.android.gms.internal.measurement.f1;
import com.google.android.gms.internal.measurement.h1;
import com.google.android.gms.internal.measurement.i1;
import com.google.android.gms.internal.measurement.j1;
import com.google.android.gms.internal.measurement.r0;
import com.google.android.gms.internal.measurement.s0;
import com.google.android.gms.internal.measurement.t0;
import com.google.android.gms.internal.measurement.u0;
import com.google.android.gms.internal.measurement.v0;
import com.google.android.gms.internal.measurement.y1;
import com.google.android.gms.internal.measurement.z0;
import com.google.android.gms.internal.measurement.zzkn;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;
import java.util.zip.GZIPOutputStream;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* loaded from: classes.dex */
public final class r extends jv5 {
    public r(fw5 fw5Var) {
        super(fw5Var);
    }

    public static boolean C(String str) {
        return str != null && str.matches("([+-])?([0-9]+\\.?[0-9]*|[0-9]*\\.?[0-9]+)") && str.length() <= 310;
    }

    public static boolean D(List<Long> list, int i) {
        if (i < list.size() * 64) {
            return ((1 << (i % 64)) & list.get(i / 64).longValue()) != 0;
        }
        return false;
    }

    public static List<Long> E(BitSet bitSet) {
        int length = (bitSet.length() + 63) / 64;
        ArrayList arrayList = new ArrayList(length);
        for (int i = 0; i < length; i++) {
            long j = 0;
            for (int i2 = 0; i2 < 64; i2++) {
                int i3 = (i * 64) + i2;
                if (i3 >= bitSet.length()) {
                    break;
                }
                if (bitSet.get(i3)) {
                    j |= 1 << i2;
                }
            }
            arrayList.add(Long.valueOf(j));
        }
        return arrayList;
    }

    public static <Builder extends y1> Builder J(Builder builder, byte[] bArr) throws zzkn {
        qt5 b = qt5.b();
        if (b != null) {
            return (Builder) builder.J0(bArr, b);
        }
        return (Builder) builder.H0(bArr);
    }

    public static int K(dk5 dk5Var, String str) {
        for (int i = 0; i < dk5Var.A0(); i++) {
            if (str.equals(dk5Var.B0(i).z())) {
                return i;
            }
        }
        return -1;
    }

    public static List<d1> L(Bundle[] bundleArr) {
        ArrayList arrayList = new ArrayList();
        for (Bundle bundle : bundleArr) {
            if (bundle != null) {
                yj5 J = d1.J();
                for (String str : bundle.keySet()) {
                    yj5 J2 = d1.J();
                    J2.v(str);
                    Object obj = bundle.get(str);
                    if (obj instanceof Long) {
                        J2.A(((Long) obj).longValue());
                    } else if (obj instanceof String) {
                        J2.x((String) obj);
                    } else if (obj instanceof Double) {
                        J2.C(((Double) obj).doubleValue());
                    }
                    J.G(J2);
                }
                if (J.E() > 0) {
                    arrayList.add(J.o());
                }
            }
        }
        return arrayList;
    }

    public static zzas M(com.google.android.gms.internal.measurement.a aVar) {
        Bundle bundle = new Bundle();
        String str = "app";
        for (String str2 : aVar.f().keySet()) {
            Object e = aVar.e(str2);
            if ("_o".equals(str2) && e != null) {
                str = e.toString();
            }
            if (e == null) {
                bundle.putString(str2, null);
            } else if (e instanceof Long) {
                bundle.putLong(str2, ((Long) e).longValue());
            } else if (e instanceof Double) {
                bundle.putDouble(str2, ((Double) e).doubleValue());
            } else {
                bundle.putString(str2, e.toString());
            }
        }
        String b = yl5.b(aVar.b());
        if (b == null) {
            b = aVar.b();
        }
        return new zzas(b, new zzaq(bundle), str, aVar.a());
    }

    public static final void N(tj5 tj5Var, String str, Object obj) {
        List<d1> v = tj5Var.v();
        int i = 0;
        while (true) {
            if (i >= v.size()) {
                i = -1;
                break;
            } else if (str.equals(v.get(i).y())) {
                break;
            } else {
                i++;
            }
        }
        yj5 J = d1.J();
        J.v(str);
        if (obj instanceof Long) {
            J.A(((Long) obj).longValue());
        } else if (obj instanceof String) {
            J.x((String) obj);
        } else if (obj instanceof Double) {
            J.C(((Double) obj).doubleValue());
        } else if (obj instanceof Bundle[]) {
            J.H(L((Bundle[]) obj));
        }
        if (i >= 0) {
            tj5Var.B(i, J);
        } else {
            tj5Var.D(J);
        }
    }

    public static final boolean O(zzas zzasVar, zzp zzpVar) {
        zt2.j(zzasVar);
        zt2.j(zzpVar);
        return (TextUtils.isEmpty(zzpVar.f0) && TextUtils.isEmpty(zzpVar.u0)) ? false : true;
    }

    public static final d1 j(b1 b1Var, String str) {
        for (d1 d1Var : b1Var.x()) {
            if (d1Var.y().equals(str)) {
                return d1Var;
            }
        }
        return null;
    }

    public static final Object k(b1 b1Var, String str) {
        d1 j = j(b1Var, str);
        if (j != null) {
            if (j.z()) {
                return j.A();
            }
            if (j.B()) {
                return Long.valueOf(j.C());
            }
            if (j.F()) {
                return Double.valueOf(j.G());
            }
            if (j.I() > 0) {
                List<d1> H = j.H();
                ArrayList arrayList = new ArrayList();
                for (d1 d1Var : H) {
                    if (d1Var != null) {
                        Bundle bundle = new Bundle();
                        for (d1 d1Var2 : d1Var.H()) {
                            if (d1Var2.z()) {
                                bundle.putString(d1Var2.y(), d1Var2.A());
                            } else if (d1Var2.B()) {
                                bundle.putLong(d1Var2.y(), d1Var2.C());
                            } else if (d1Var2.F()) {
                                bundle.putDouble(d1Var2.y(), d1Var2.G());
                            }
                        }
                        if (!bundle.isEmpty()) {
                            arrayList.add(bundle);
                        }
                    }
                }
                return (Bundle[]) arrayList.toArray(new Bundle[arrayList.size()]);
            }
            return null;
        }
        return null;
    }

    public static final void o(StringBuilder sb, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            sb.append("  ");
        }
    }

    public static final String p(boolean z, boolean z2, boolean z3) {
        StringBuilder sb = new StringBuilder();
        if (z) {
            sb.append("Dynamic ");
        }
        if (z2) {
            sb.append("Sequence ");
        }
        if (z3) {
            sb.append("Session-Scoped ");
        }
        return sb.toString();
    }

    public static final void r(StringBuilder sb, int i, String str, h1 h1Var) {
        if (h1Var == null) {
            return;
        }
        o(sb, 3);
        sb.append(str);
        sb.append(" {\n");
        if (h1Var.A() != 0) {
            o(sb, 4);
            sb.append("results: ");
            int i2 = 0;
            for (Long l : h1Var.z()) {
                int i3 = i2 + 1;
                if (i2 != 0) {
                    sb.append(", ");
                }
                sb.append(l);
                i2 = i3;
            }
            sb.append('\n');
        }
        if (h1Var.y() != 0) {
            o(sb, 4);
            sb.append("status: ");
            int i4 = 0;
            for (Long l2 : h1Var.x()) {
                int i5 = i4 + 1;
                if (i4 != 0) {
                    sb.append(", ");
                }
                sb.append(l2);
                i4 = i5;
            }
            sb.append('\n');
        }
        if (h1Var.C() != 0) {
            o(sb, 4);
            sb.append("dynamic_filter_timestamps: {");
            int i6 = 0;
            for (a1 a1Var : h1Var.B()) {
                int i7 = i6 + 1;
                if (i6 != 0) {
                    sb.append(", ");
                }
                sb.append(a1Var.x() ? Integer.valueOf(a1Var.y()) : null);
                sb.append(":");
                sb.append(a1Var.z() ? Long.valueOf(a1Var.A()) : null);
                i6 = i7;
            }
            sb.append("}\n");
        }
        if (h1Var.F() != 0) {
            o(sb, 4);
            sb.append("sequence_filter_timestamps: {");
            int i8 = 0;
            for (i1 i1Var : h1Var.E()) {
                int i9 = i8 + 1;
                if (i8 != 0) {
                    sb.append(", ");
                }
                sb.append(i1Var.x() ? Integer.valueOf(i1Var.y()) : null);
                sb.append(": [");
                int i10 = 0;
                for (Long l3 : i1Var.z()) {
                    long longValue = l3.longValue();
                    int i11 = i10 + 1;
                    if (i10 != 0) {
                        sb.append(", ");
                    }
                    sb.append(longValue);
                    i10 = i11;
                }
                sb.append("]");
                i8 = i9;
            }
            sb.append("}\n");
        }
        o(sb, 3);
        sb.append("}\n");
    }

    public static final void s(StringBuilder sb, int i, String str, Object obj) {
        if (obj == null) {
            return;
        }
        o(sb, i + 1);
        sb.append(str);
        sb.append(": ");
        sb.append(obj);
        sb.append('\n');
    }

    public static final void t(StringBuilder sb, int i, String str, t0 t0Var) {
        if (t0Var == null) {
            return;
        }
        o(sb, i);
        sb.append(str);
        sb.append(" {\n");
        if (t0Var.x()) {
            s(sb, i, "comparison_type", t0Var.y().name());
        }
        if (t0Var.z()) {
            s(sb, i, "match_as_float", Boolean.valueOf(t0Var.A()));
        }
        if (t0Var.B()) {
            s(sb, i, "comparison_value", t0Var.C());
        }
        if (t0Var.D()) {
            s(sb, i, "min_comparison_value", t0Var.E());
        }
        if (t0Var.F()) {
            s(sb, i, "max_comparison_value", t0Var.G());
        }
        o(sb, i);
        sb.append("}\n");
    }

    public final String A(u0 u0Var) {
        if (u0Var == null) {
            return "null";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("\nproperty_filter {\n");
        if (u0Var.x()) {
            s(sb, 0, "filter_id", Integer.valueOf(u0Var.y()));
        }
        s(sb, 0, "property_name", this.a.H().p(u0Var.z()));
        String p = p(u0Var.B(), u0Var.C(), u0Var.E());
        if (!p.isEmpty()) {
            s(sb, 0, "filter_type", p);
        }
        n(sb, 1, u0Var.A());
        sb.append("}\n");
        return sb.toString();
    }

    public final <T extends Parcelable> T B(byte[] bArr, Parcelable.Creator<T> creator) {
        if (bArr == null) {
            return null;
        }
        Parcel obtain = Parcel.obtain();
        try {
            obtain.unmarshall(bArr, 0, bArr.length);
            obtain.setDataPosition(0);
            return creator.createFromParcel(obtain);
        } catch (SafeParcelReader.ParseException unused) {
            this.a.w().l().a("Failed to load parcelable from buffer");
            return null;
        } finally {
            obtain.recycle();
        }
    }

    public final List<Long> F(List<Long> list, List<Integer> list2) {
        int i;
        ArrayList arrayList = new ArrayList(list);
        for (Integer num : list2) {
            if (num.intValue() < 0) {
                this.a.w().p().b("Ignoring negative bit index to be cleared", num);
            } else {
                int intValue = num.intValue() / 64;
                if (intValue >= arrayList.size()) {
                    this.a.w().p().c("Ignoring bit index greater than bitSet size", num, Integer.valueOf(arrayList.size()));
                } else {
                    arrayList.set(intValue, Long.valueOf(((Long) arrayList.get(intValue)).longValue() & (~(1 << (num.intValue() % 64)))));
                }
            }
        }
        int size = arrayList.size();
        int size2 = arrayList.size() - 1;
        while (true) {
            int i2 = size2;
            i = size;
            size = i2;
            if (size < 0 || ((Long) arrayList.get(size)).longValue() != 0) {
                break;
            }
            size2 = size - 1;
        }
        return arrayList.subList(0, i);
    }

    public final boolean G(long j, long j2) {
        return j == 0 || j2 <= 0 || Math.abs(this.a.a().a() - j) > j2;
    }

    public final long H(byte[] bArr) {
        zt2.j(bArr);
        this.a.G().e();
        MessageDigest B = sw5.B();
        if (B == null) {
            this.a.w().l().a("Failed to get MD5");
            return 0L;
        }
        return sw5.C(B.digest(bArr));
    }

    public final byte[] I(byte[] bArr) throws IOException {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
            gZIPOutputStream.write(bArr);
            gZIPOutputStream.close();
            byteArrayOutputStream.close();
            return byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            this.a.w().l().b("Failed to gzip content", e);
            throw e;
        }
    }

    @Override // defpackage.jv5
    public final boolean h() {
        return false;
    }

    public final void l(StringBuilder sb, int i, List<d1> list) {
        if (list == null) {
            return;
        }
        int i2 = i + 1;
        for (d1 d1Var : list) {
            if (d1Var != null) {
                o(sb, i2);
                sb.append("param {\n");
                s(sb, i2, PublicResolver.FUNC_NAME, d1Var.x() ? this.a.H().o(d1Var.y()) : null);
                s(sb, i2, "string_value", d1Var.z() ? d1Var.A() : null);
                s(sb, i2, "int_value", d1Var.B() ? Long.valueOf(d1Var.C()) : null);
                s(sb, i2, "double_value", d1Var.F() ? Double.valueOf(d1Var.G()) : null);
                if (d1Var.I() > 0) {
                    l(sb, i2, d1Var.H());
                }
                o(sb, i2);
                sb.append("}\n");
            }
        }
    }

    public final void n(StringBuilder sb, int i, s0 s0Var) {
        if (s0Var == null) {
            return;
        }
        o(sb, i);
        sb.append("filter {\n");
        if (s0Var.B()) {
            s(sb, i, "complement", Boolean.valueOf(s0Var.C()));
        }
        if (s0Var.D()) {
            s(sb, i, "param_name", this.a.H().o(s0Var.E()));
        }
        if (s0Var.x()) {
            int i2 = i + 1;
            v0 y = s0Var.y();
            if (y != null) {
                o(sb, i2);
                sb.append("string_filter {\n");
                if (y.x()) {
                    s(sb, i2, "match_type", y.y().name());
                }
                if (y.z()) {
                    s(sb, i2, "expression", y.A());
                }
                if (y.B()) {
                    s(sb, i2, "case_sensitive", Boolean.valueOf(y.C()));
                }
                if (y.E() > 0) {
                    o(sb, i2 + 1);
                    sb.append("expression_list {\n");
                    for (String str : y.D()) {
                        o(sb, i2 + 2);
                        sb.append(str);
                        sb.append("\n");
                    }
                    sb.append("}\n");
                }
                o(sb, i2);
                sb.append("}\n");
            }
        }
        if (s0Var.z()) {
            t(sb, i + 1, "number_filter", s0Var.A());
        }
        o(sb, i);
        sb.append("}\n");
    }

    public final void u(il5 il5Var, Object obj) {
        zt2.j(obj);
        il5Var.A();
        il5Var.C();
        il5Var.E();
        if (obj instanceof String) {
            il5Var.y((String) obj);
        } else if (obj instanceof Long) {
            il5Var.B(((Long) obj).longValue());
        } else if (obj instanceof Double) {
            il5Var.D(((Double) obj).doubleValue());
        } else {
            this.a.w().l().b("Ignoring invalid (type) user attribute value", obj);
        }
    }

    public final void v(yj5 yj5Var, Object obj) {
        zt2.j(obj);
        yj5Var.y();
        yj5Var.B();
        yj5Var.D();
        yj5Var.I();
        if (obj instanceof String) {
            yj5Var.x((String) obj);
        } else if (obj instanceof Long) {
            yj5Var.A(((Long) obj).longValue());
        } else if (obj instanceof Double) {
            yj5Var.C(((Double) obj).doubleValue());
        } else if (obj instanceof Bundle[]) {
            yj5Var.H(L((Bundle[]) obj));
        } else {
            this.a.w().l().b("Ignoring invalid (type) event param value", obj);
        }
    }

    public final b1 x(s55 s55Var) {
        tj5 H = b1.H();
        H.O(s55Var.e);
        y55 y55Var = new y55(s55Var.f);
        while (y55Var.hasNext()) {
            String next = y55Var.next();
            yj5 J = d1.J();
            J.v(next);
            Object I1 = s55Var.f.I1(next);
            zt2.j(I1);
            v(J, I1);
            H.D(J);
        }
        return H.o();
    }

    public final String y(e1 e1Var) {
        if (e1Var == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("\nbatch {\n");
        for (f1 f1Var : e1Var.x()) {
            if (f1Var != null) {
                o(sb, 1);
                sb.append("bundle {\n");
                if (f1Var.X()) {
                    s(sb, 1, "protocol_version", Integer.valueOf(f1Var.b1()));
                }
                s(sb, 1, "platform", f1Var.I1());
                if (f1Var.z()) {
                    s(sb, 1, "gmp_version", Long.valueOf(f1Var.A()));
                }
                if (f1Var.B()) {
                    s(sb, 1, "uploading_gmp_version", Long.valueOf(f1Var.C()));
                }
                if (f1Var.D0()) {
                    s(sb, 1, "dynamite_version", Long.valueOf(f1Var.E0()));
                }
                if (f1Var.T()) {
                    s(sb, 1, "config_version", Long.valueOf(f1Var.U()));
                }
                s(sb, 1, "gmp_app_id", f1Var.M());
                s(sb, 1, "admob_app_id", f1Var.C0());
                s(sb, 1, "app_id", f1Var.x());
                s(sb, 1, "app_version", f1Var.y());
                if (f1Var.R()) {
                    s(sb, 1, "app_version_major", Integer.valueOf(f1Var.S()));
                }
                s(sb, 1, "firebase_instance_id", f1Var.Q());
                if (f1Var.H()) {
                    s(sb, 1, "dev_cert_hash", Long.valueOf(f1Var.I()));
                }
                s(sb, 1, "app_store", f1Var.O1());
                if (f1Var.y1()) {
                    s(sb, 1, "upload_timestamp_millis", Long.valueOf(f1Var.z1()));
                }
                if (f1Var.A1()) {
                    s(sb, 1, "start_timestamp_millis", Long.valueOf(f1Var.B1()));
                }
                if (f1Var.C1()) {
                    s(sb, 1, "end_timestamp_millis", Long.valueOf(f1Var.D1()));
                }
                if (f1Var.E1()) {
                    s(sb, 1, "previous_bundle_start_timestamp_millis", Long.valueOf(f1Var.F1()));
                }
                if (f1Var.G1()) {
                    s(sb, 1, "previous_bundle_end_timestamp_millis", Long.valueOf(f1Var.H1()));
                }
                s(sb, 1, "app_instance_id", f1Var.G());
                s(sb, 1, "resettable_device_id", f1Var.D());
                s(sb, 1, "ds_id", f1Var.z0());
                if (f1Var.E()) {
                    s(sb, 1, "limited_ad_tracking", Boolean.valueOf(f1Var.F()));
                }
                s(sb, 1, "os_version", f1Var.J1());
                s(sb, 1, "device_model", f1Var.K1());
                s(sb, 1, "user_default_language", f1Var.L1());
                if (f1Var.M1()) {
                    s(sb, 1, "time_zone_offset_minutes", Integer.valueOf(f1Var.N1()));
                }
                if (f1Var.J()) {
                    s(sb, 1, "bundle_sequential_index", Integer.valueOf(f1Var.K()));
                }
                if (f1Var.N()) {
                    s(sb, 1, "service_upload", Boolean.valueOf(f1Var.O()));
                }
                s(sb, 1, "health_monitor", f1Var.L());
                if (!this.a.z().v(null, qf5.t0) && f1Var.V() && f1Var.W() != 0) {
                    s(sb, 1, "android_id", Long.valueOf(f1Var.W()));
                }
                if (f1Var.A0()) {
                    s(sb, 1, "retry_counter", Integer.valueOf(f1Var.B0()));
                }
                if (f1Var.G0()) {
                    s(sb, 1, "consent_signals", f1Var.I0());
                }
                List<j1> v1 = f1Var.v1();
                if (v1 != null) {
                    for (j1 j1Var : v1) {
                        if (j1Var != null) {
                            o(sb, 2);
                            sb.append("user_property {\n");
                            s(sb, 2, "set_timestamp_millis", j1Var.x() ? Long.valueOf(j1Var.y()) : null);
                            s(sb, 2, PublicResolver.FUNC_NAME, this.a.H().p(j1Var.z()));
                            s(sb, 2, "string_value", j1Var.B());
                            s(sb, 2, "int_value", j1Var.C() ? Long.valueOf(j1Var.D()) : null);
                            s(sb, 2, "double_value", j1Var.E() ? Double.valueOf(j1Var.F()) : null);
                            o(sb, 2);
                            sb.append("}\n");
                        }
                    }
                }
                List<z0> P = f1Var.P();
                if (P != null) {
                    for (z0 z0Var : P) {
                        if (z0Var != null) {
                            o(sb, 2);
                            sb.append("audience_membership {\n");
                            if (z0Var.x()) {
                                s(sb, 2, "audience_id", Integer.valueOf(z0Var.y()));
                            }
                            if (z0Var.C()) {
                                s(sb, 2, "new_audience", Boolean.valueOf(z0Var.D()));
                            }
                            r(sb, 2, "current_data", z0Var.z());
                            if (z0Var.A()) {
                                r(sb, 2, "previous_data", z0Var.B());
                            }
                            o(sb, 2);
                            sb.append("}\n");
                        }
                    }
                }
                List<b1> s1 = f1Var.s1();
                if (s1 != null) {
                    for (b1 b1Var : s1) {
                        if (b1Var != null) {
                            o(sb, 2);
                            sb.append("event {\n");
                            s(sb, 2, PublicResolver.FUNC_NAME, this.a.H().n(b1Var.A()));
                            if (b1Var.B()) {
                                s(sb, 2, "timestamp_millis", Long.valueOf(b1Var.C()));
                            }
                            if (b1Var.D()) {
                                s(sb, 2, "previous_timestamp_millis", Long.valueOf(b1Var.E()));
                            }
                            if (b1Var.F()) {
                                s(sb, 2, "count", Integer.valueOf(b1Var.G()));
                            }
                            if (b1Var.y() != 0) {
                                l(sb, 2, b1Var.x());
                            }
                            o(sb, 2);
                            sb.append("}\n");
                        }
                    }
                }
                o(sb, 1);
                sb.append("}\n");
            }
        }
        sb.append("}\n");
        return sb.toString();
    }

    public final String z(r0 r0Var) {
        if (r0Var == null) {
            return "null";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("\nevent_filter {\n");
        if (r0Var.x()) {
            s(sb, 0, "filter_id", Integer.valueOf(r0Var.y()));
        }
        s(sb, 0, "event_name", this.a.H().n(r0Var.z()));
        String p = p(r0Var.F(), r0Var.G(), r0Var.I());
        if (!p.isEmpty()) {
            s(sb, 0, "filter_type", p);
        }
        if (r0Var.D()) {
            t(sb, 1, "event_count_filter", r0Var.E());
        }
        if (r0Var.B() > 0) {
            sb.append("  filters {\n");
            for (s0 s0Var : r0Var.A()) {
                n(sb, 2, s0Var);
            }
        }
        o(sb, 1);
        sb.append("}\n}\n");
        return sb.toString();
    }
}
