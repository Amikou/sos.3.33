package com.google.android.gms.measurement.internal;

import android.os.Bundle;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* loaded from: classes.dex */
public abstract class c extends com.google.android.gms.internal.measurement.d implements d {
    public c() {
        super("com.google.android.gms.measurement.internal.IMeasurementService");
    }

    @Override // com.google.android.gms.internal.measurement.d
    public final boolean b(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        switch (i) {
            case 1:
                w1((zzas) a95.c(parcel, zzas.CREATOR), (zzp) a95.c(parcel, zzp.CREATOR));
                parcel2.writeNoException();
                return true;
            case 2:
                X0((zzkq) a95.c(parcel, zzkq.CREATOR), (zzp) a95.c(parcel, zzp.CREATOR));
                parcel2.writeNoException();
                return true;
            case 3:
            case 8:
            default:
                return false;
            case 4:
                f1((zzp) a95.c(parcel, zzp.CREATOR));
                parcel2.writeNoException();
                return true;
            case 5:
                j0((zzas) a95.c(parcel, zzas.CREATOR), parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                return true;
            case 6:
                F0((zzp) a95.c(parcel, zzp.CREATOR));
                parcel2.writeNoException();
                return true;
            case 7:
                List<zzkq> T = T((zzp) a95.c(parcel, zzp.CREATOR), a95.a(parcel));
                parcel2.writeNoException();
                parcel2.writeTypedList(T);
                return true;
            case 9:
                byte[] l0 = l0((zzas) a95.c(parcel, zzas.CREATOR), parcel.readString());
                parcel2.writeNoException();
                parcel2.writeByteArray(l0);
                return true;
            case 10:
                Q(parcel.readLong(), parcel.readString(), parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                return true;
            case 11:
                String u = u((zzp) a95.c(parcel, zzp.CREATOR));
                parcel2.writeNoException();
                parcel2.writeString(u);
                return true;
            case 12:
                P((zzaa) a95.c(parcel, zzaa.CREATOR), (zzp) a95.c(parcel, zzp.CREATOR));
                parcel2.writeNoException();
                return true;
            case 13:
                i0((zzaa) a95.c(parcel, zzaa.CREATOR));
                parcel2.writeNoException();
                return true;
            case 14:
                List<zzkq> X = X(parcel.readString(), parcel.readString(), a95.a(parcel), (zzp) a95.c(parcel, zzp.CREATOR));
                parcel2.writeNoException();
                parcel2.writeTypedList(X);
                return true;
            case 15:
                List<zzkq> B1 = B1(parcel.readString(), parcel.readString(), parcel.readString(), a95.a(parcel));
                parcel2.writeNoException();
                parcel2.writeTypedList(B1);
                return true;
            case 16:
                List<zzaa> g = g(parcel.readString(), parcel.readString(), (zzp) a95.c(parcel, zzp.CREATOR));
                parcel2.writeNoException();
                parcel2.writeTypedList(g);
                return true;
            case 17:
                List<zzaa> Y = Y(parcel.readString(), parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                parcel2.writeTypedList(Y);
                return true;
            case 18:
                e0((zzp) a95.c(parcel, zzp.CREATOR));
                parcel2.writeNoException();
                return true;
            case 19:
                h0((Bundle) a95.c(parcel, Bundle.CREATOR), (zzp) a95.c(parcel, zzp.CREATOR));
                parcel2.writeNoException();
                return true;
            case 20:
                o((zzp) a95.c(parcel, zzp.CREATOR));
                parcel2.writeNoException();
                return true;
        }
    }
}
