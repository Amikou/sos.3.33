package com.google.android.gms.measurement.internal;

import java.util.concurrent.Callable;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* loaded from: classes.dex */
public final class q implements Callable<String> {
    public final /* synthetic */ zzp a;
    public final /* synthetic */ fw5 b;

    public q(fw5 fw5Var, zzp zzpVar) {
        this.b = fw5Var;
        this.a = zzpVar;
    }

    @Override // java.util.concurrent.Callable
    public final /* bridge */ /* synthetic */ String call() throws Exception {
        if (this.b.f0((String) zt2.j(this.a.a)).h() && t45.c(this.a.z0).h()) {
            return this.b.y(this.a).O();
        }
        this.b.w().v().a("Analytics storage consent denied. Returning null app instance id");
        return null;
    }
}
