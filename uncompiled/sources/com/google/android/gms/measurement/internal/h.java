package com.google.android.gms.measurement.internal;

import java.lang.Thread;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* loaded from: classes.dex */
public final class h extends sl5 {
    public static final AtomicLong l = new AtomicLong(Long.MIN_VALUE);
    public xj5 c;
    public xj5 d;
    public final PriorityBlockingQueue<vj5<?>> e;
    public final BlockingQueue<vj5<?>> f;
    public final Thread.UncaughtExceptionHandler g;
    public final Thread.UncaughtExceptionHandler h;
    public final Object i;
    public final Semaphore j;
    public volatile boolean k;

    public h(ck5 ck5Var) {
        super(ck5Var);
        this.i = new Object();
        this.j = new Semaphore(2);
        this.e = new PriorityBlockingQueue<>();
        this.f = new LinkedBlockingQueue();
        this.g = new uj5(this, "Thread death: Uncaught exception on worker thread");
        this.h = new uj5(this, "Thread death: Uncaught exception on network thread");
    }

    public final void D(vj5<?> vj5Var) {
        synchronized (this.i) {
            this.e.add(vj5Var);
            xj5 xj5Var = this.c;
            if (xj5Var == null) {
                xj5 xj5Var2 = new xj5(this, "Measurement Worker", this.e);
                this.c = xj5Var2;
                xj5Var2.setUncaughtExceptionHandler(this.g);
                this.c.start();
            } else {
                xj5Var.a();
            }
        }
    }

    @Override // defpackage.ql5
    public final void d() {
        if (Thread.currentThread() != this.d) {
            throw new IllegalStateException("Call expected from network thread");
        }
    }

    @Override // defpackage.ql5
    public final void e() {
        if (Thread.currentThread() != this.c) {
            throw new IllegalStateException("Call expected from worker thread");
        }
    }

    @Override // defpackage.sl5
    public final boolean f() {
        return false;
    }

    public final boolean l() {
        return Thread.currentThread() == this.c;
    }

    public final <V> Future<V> n(Callable<V> callable) throws IllegalStateException {
        i();
        zt2.j(callable);
        vj5<?> vj5Var = new vj5<>(this, (Callable<?>) callable, false, "Task exception on worker thread");
        if (Thread.currentThread() == this.c) {
            if (!this.e.isEmpty()) {
                this.a.w().p().a("Callable skipped the worker queue.");
            }
            vj5Var.run();
        } else {
            D(vj5Var);
        }
        return vj5Var;
    }

    public final <V> Future<V> o(Callable<V> callable) throws IllegalStateException {
        i();
        zt2.j(callable);
        vj5<?> vj5Var = new vj5<>(this, (Callable<?>) callable, true, "Task exception on worker thread");
        if (Thread.currentThread() == this.c) {
            vj5Var.run();
        } else {
            D(vj5Var);
        }
        return vj5Var;
    }

    public final void p(Runnable runnable) throws IllegalStateException {
        i();
        zt2.j(runnable);
        D(new vj5<>(this, runnable, false, "Task exception on worker thread"));
    }

    public final <T> T r(AtomicReference<T> atomicReference, long j, String str, Runnable runnable) {
        synchronized (atomicReference) {
            this.a.q().p(runnable);
            try {
                atomicReference.wait(j);
            } catch (InterruptedException unused) {
                this.a.w().p().a(str.length() != 0 ? "Interrupted waiting for ".concat(str) : new String("Interrupted waiting for "));
                return null;
            }
        }
        T t = atomicReference.get();
        if (t == null) {
            this.a.w().p().a(str.length() != 0 ? "Timed out waiting for ".concat(str) : new String("Timed out waiting for "));
        }
        return t;
    }

    public final void s(Runnable runnable) throws IllegalStateException {
        i();
        zt2.j(runnable);
        D(new vj5<>(this, runnable, true, "Task exception on worker thread"));
    }

    public final void t(Runnable runnable) throws IllegalStateException {
        i();
        zt2.j(runnable);
        vj5<?> vj5Var = new vj5<>(this, runnable, false, "Task exception on network thread");
        synchronized (this.i) {
            this.f.add(vj5Var);
            xj5 xj5Var = this.d;
            if (xj5Var == null) {
                xj5 xj5Var2 = new xj5(this, "Measurement Network", this.f);
                this.d = xj5Var2;
                xj5Var2.setUncaughtExceptionHandler(this.h);
                this.d.start();
            } else {
                xj5Var.a();
            }
        }
    }
}
