package com.google.android.gms.measurement.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Pair;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* loaded from: classes.dex */
public final class p extends vh5 {
    public final dt5 c;
    public d d;
    public volatile Boolean e;
    public final n55 f;
    public final bv5 g;
    public final List<Runnable> h;
    public final n55 i;

    public p(ck5 ck5Var) {
        super(ck5Var);
        this.h = new ArrayList();
        this.g = new bv5(ck5Var.a());
        this.c = new dt5(this);
        this.f = new lr5(this, ck5Var);
        this.i = new qr5(this, ck5Var);
    }

    public static /* synthetic */ void x(p pVar, ComponentName componentName) {
        pVar.e();
        if (pVar.d != null) {
            pVar.d = null;
            pVar.a.w().v().b("Disconnected from device MeasurementService", componentName);
            pVar.e();
            pVar.n();
        }
    }

    public final boolean C() {
        this.a.b();
        return true;
    }

    public final void D() {
        e();
        this.g.a();
        n55 n55Var = this.f;
        this.a.z();
        n55Var.b(qf5.J.b(null).longValue());
    }

    public final void E(Runnable runnable) throws IllegalStateException {
        e();
        if (H()) {
            runnable.run();
            return;
        }
        int size = this.h.size();
        this.a.z();
        if (size >= 1000) {
            this.a.w().l().a("Discarding data. Max runnable queue size reached");
            return;
        }
        this.h.add(runnable);
        this.i.b(60000L);
        n();
    }

    public final void F() {
        e();
        this.a.w().v().b("Processing queued up service tasks", Integer.valueOf(this.h.size()));
        for (Runnable runnable : this.h) {
            try {
                runnable.run();
            } catch (RuntimeException e) {
                this.a.w().l().b("Task exception while flushing queue", e);
            }
        }
        this.h.clear();
        this.i.d();
    }

    public final zzp G(boolean z) {
        Pair<String, Long> b;
        this.a.b();
        xf5 c = this.a.c();
        String str = null;
        if (z) {
            og5 w = this.a.w();
            if (w.a.A().d != null && (b = w.a.A().d.b()) != null && b != mi5.x) {
                String valueOf = String.valueOf(b.second);
                String str2 = (String) b.first;
                StringBuilder sb = new StringBuilder(valueOf.length() + 1 + String.valueOf(str2).length());
                sb.append(valueOf);
                sb.append(":");
                sb.append(str2);
                str = sb.toString();
            }
        }
        return c.l(str);
    }

    public final boolean H() {
        e();
        g();
        return this.d != null;
    }

    public final void I() {
        e();
        g();
        E(new sr5(this, G(true)));
    }

    public final void J(boolean z) {
        e();
        g();
        if (z) {
            C();
            this.a.I().l();
        }
        if (u()) {
            E(new ur5(this, G(false)));
        }
    }

    public final void K(d dVar, AbstractSafeParcelable abstractSafeParcelable, zzp zzpVar) {
        int i;
        e();
        g();
        C();
        this.a.z();
        int i2 = 0;
        int i3 = 100;
        while (i2 < 1001 && i3 == 100) {
            ArrayList arrayList = new ArrayList();
            List<AbstractSafeParcelable> r = this.a.I().r(100);
            if (r != null) {
                arrayList.addAll(r);
                i = r.size();
            } else {
                i = 0;
            }
            if (abstractSafeParcelable != null && i < 100) {
                arrayList.add(abstractSafeParcelable);
            }
            int size = arrayList.size();
            for (int i4 = 0; i4 < size; i4++) {
                AbstractSafeParcelable abstractSafeParcelable2 = (AbstractSafeParcelable) arrayList.get(i4);
                if (abstractSafeParcelable2 instanceof zzas) {
                    try {
                        dVar.w1((zzas) abstractSafeParcelable2, zzpVar);
                    } catch (RemoteException e) {
                        this.a.w().l().b("Failed to send event to the service", e);
                    }
                } else if (abstractSafeParcelable2 instanceof zzkq) {
                    try {
                        dVar.X0((zzkq) abstractSafeParcelable2, zzpVar);
                    } catch (RemoteException e2) {
                        this.a.w().l().b("Failed to send user property to the service", e2);
                    }
                } else if (abstractSafeParcelable2 instanceof zzaa) {
                    try {
                        dVar.P((zzaa) abstractSafeParcelable2, zzpVar);
                    } catch (RemoteException e3) {
                        this.a.w().l().b("Failed to send conditional user property to the service", e3);
                    }
                } else {
                    this.a.w().l().a("Discarding data. Unrecognized parcel type.");
                }
            }
            i2++;
            i3 = i;
        }
    }

    public final void L(zzas zzasVar, String str) {
        zt2.j(zzasVar);
        e();
        g();
        C();
        E(new xr5(this, true, G(true), this.a.I().n(zzasVar), zzasVar, str));
    }

    public final void M(zzaa zzaaVar) {
        zt2.j(zzaaVar);
        e();
        g();
        this.a.b();
        E(new js5(this, true, G(true), this.a.I().p(zzaaVar), new zzaa(zzaaVar), zzaaVar));
    }

    public final void N(AtomicReference<List<zzaa>> atomicReference, String str, String str2, String str3) {
        e();
        g();
        E(new ls5(this, atomicReference, null, str2, str3, G(false)));
    }

    public final void O(com.google.android.gms.internal.measurement.m mVar, String str, String str2) {
        e();
        g();
        E(new ns5(this, str, str2, G(false), mVar));
    }

    public final void P(AtomicReference<List<zzkq>> atomicReference, String str, String str2, String str3, boolean z) {
        e();
        g();
        E(new ps5(this, atomicReference, null, str2, str3, G(false), z));
    }

    public final void Q(com.google.android.gms.internal.measurement.m mVar, String str, String str2, boolean z) {
        e();
        g();
        E(new sq5(this, str, str2, G(false), z, mVar));
    }

    public final void R(zzkq zzkqVar) {
        e();
        g();
        C();
        E(new vq5(this, G(true), this.a.I().o(zzkqVar), zzkqVar));
    }

    public final void S() {
        e();
        g();
        zzp G = G(false);
        C();
        this.a.I().l();
        E(new yq5(this, G));
    }

    public final void T(AtomicReference<String> atomicReference) {
        e();
        g();
        E(new ar5(this, atomicReference, G(false)));
    }

    public final void U(com.google.android.gms.internal.measurement.m mVar) {
        e();
        g();
        E(new cr5(this, G(false), mVar));
    }

    public final void V() {
        e();
        g();
        zzp G = G(true);
        this.a.I().s();
        E(new er5(this, G));
    }

    public final void W(bq5 bq5Var) {
        e();
        g();
        E(new gr5(this, bq5Var));
    }

    @Override // defpackage.vh5
    public final boolean j() {
        return false;
    }

    public final void l(Bundle bundle) {
        e();
        g();
        E(new jr5(this, G(false), bundle));
    }

    public final void n() {
        e();
        g();
        if (H()) {
            return;
        }
        if (!p()) {
            if (this.a.z().H()) {
                return;
            }
            this.a.b();
            List<ResolveInfo> queryIntentServices = this.a.m().getPackageManager().queryIntentServices(new Intent().setClassName(this.a.m(), "com.google.android.gms.measurement.AppMeasurementService"), 65536);
            if (queryIntentServices != null && queryIntentServices.size() > 0) {
                Intent intent = new Intent("com.google.android.gms.measurement.START");
                Context m = this.a.m();
                this.a.b();
                intent.setComponent(new ComponentName(m, "com.google.android.gms.measurement.AppMeasurementService"));
                this.c.a(intent);
                return;
            }
            this.a.w().l().a("Unable to use remote or local measurement implementation. Please register the AppMeasurementService service in the app manifest");
            return;
        }
        this.c.c();
    }

    public final Boolean o() {
        return this.e;
    }

    /* JADX WARN: Removed duplicated region for block: B:46:0x012d  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final boolean p() {
        /*
            Method dump skipped, instructions count: 338
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.p.p():boolean");
    }

    public final void r(d dVar) {
        e();
        zt2.j(dVar);
        this.d = dVar;
        D();
        F();
    }

    public final void s() {
        e();
        g();
        this.c.b();
        try {
            v50.b().c(this.a.m(), this.c);
        } catch (IllegalArgumentException | IllegalStateException unused) {
        }
        this.d = null;
    }

    public final void t(com.google.android.gms.internal.measurement.m mVar, zzas zzasVar, String str) {
        e();
        g();
        if (this.a.G().O(qh1.a) != 0) {
            this.a.w().p().a("Not bundling data. Service unavailable or out of date");
            this.a.G().U(mVar, new byte[0]);
            return;
        }
        E(new nr5(this, zzasVar, str, mVar));
    }

    public final boolean u() {
        e();
        g();
        return !p() || this.a.G().N() >= qf5.u0.b(null).intValue();
    }
}
