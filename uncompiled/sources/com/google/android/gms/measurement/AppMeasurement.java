package com.google.android.gms.measurement;

import android.os.Bundle;
import androidx.annotation.Keep;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
@Deprecated
/* loaded from: classes.dex */
public class AppMeasurement {
    public static volatile AppMeasurement c;
    public final ck5 a;
    public final fp5 b;

    /* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
    /* loaded from: classes.dex */
    public static class ConditionalUserProperty {
        @Keep
        public boolean mActive;
        @Keep
        public String mAppId;
        @Keep
        public long mCreationTimestamp;
        @Keep
        public String mExpiredEventName;
        @Keep
        public Bundle mExpiredEventParams;
        @Keep
        public String mName;
        @Keep
        public String mOrigin;
        @Keep
        public long mTimeToLive;
        @Keep
        public String mTimedOutEventName;
        @Keep
        public Bundle mTimedOutEventParams;
        @Keep
        public String mTriggerEventName;
        @Keep
        public long mTriggerTimeout;
        @Keep
        public String mTriggeredEventName;
        @Keep
        public Bundle mTriggeredEventParams;
        @Keep
        public long mTriggeredTimestamp;
        @Keep
        public Object mValue;

        public ConditionalUserProperty() {
        }

        public ConditionalUserProperty(Bundle bundle) {
            zt2.j(bundle);
            this.mAppId = (String) vl5.b(bundle, "app_id", String.class, null);
            this.mOrigin = (String) vl5.b(bundle, "origin", String.class, null);
            this.mName = (String) vl5.b(bundle, PublicResolver.FUNC_NAME, String.class, null);
            this.mValue = vl5.b(bundle, "value", Object.class, null);
            this.mTriggerEventName = (String) vl5.b(bundle, "trigger_event_name", String.class, null);
            this.mTriggerTimeout = ((Long) vl5.b(bundle, "trigger_timeout", Long.class, 0L)).longValue();
            this.mTimedOutEventName = (String) vl5.b(bundle, "timed_out_event_name", String.class, null);
            this.mTimedOutEventParams = (Bundle) vl5.b(bundle, "timed_out_event_params", Bundle.class, null);
            this.mTriggeredEventName = (String) vl5.b(bundle, "triggered_event_name", String.class, null);
            this.mTriggeredEventParams = (Bundle) vl5.b(bundle, "triggered_event_params", Bundle.class, null);
            this.mTimeToLive = ((Long) vl5.b(bundle, "time_to_live", Long.class, 0L)).longValue();
            this.mExpiredEventName = (String) vl5.b(bundle, "expired_event_name", String.class, null);
            this.mExpiredEventParams = (Bundle) vl5.b(bundle, "expired_event_params", Bundle.class, null);
            this.mActive = ((Boolean) vl5.b(bundle, "active", Boolean.class, Boolean.FALSE)).booleanValue();
            this.mCreationTimestamp = ((Long) vl5.b(bundle, "creation_timestamp", Long.class, 0L)).longValue();
            this.mTriggeredTimestamp = ((Long) vl5.b(bundle, "triggered_timestamp", Long.class, 0L)).longValue();
        }

        public final Bundle a() {
            Bundle bundle = new Bundle();
            String str = this.mAppId;
            if (str != null) {
                bundle.putString("app_id", str);
            }
            String str2 = this.mOrigin;
            if (str2 != null) {
                bundle.putString("origin", str2);
            }
            String str3 = this.mName;
            if (str3 != null) {
                bundle.putString(PublicResolver.FUNC_NAME, str3);
            }
            Object obj = this.mValue;
            if (obj != null) {
                vl5.a(bundle, obj);
            }
            String str4 = this.mTriggerEventName;
            if (str4 != null) {
                bundle.putString("trigger_event_name", str4);
            }
            bundle.putLong("trigger_timeout", this.mTriggerTimeout);
            String str5 = this.mTimedOutEventName;
            if (str5 != null) {
                bundle.putString("timed_out_event_name", str5);
            }
            Bundle bundle2 = this.mTimedOutEventParams;
            if (bundle2 != null) {
                bundle.putBundle("timed_out_event_params", bundle2);
            }
            String str6 = this.mTriggeredEventName;
            if (str6 != null) {
                bundle.putString("triggered_event_name", str6);
            }
            Bundle bundle3 = this.mTriggeredEventParams;
            if (bundle3 != null) {
                bundle.putBundle("triggered_event_params", bundle3);
            }
            bundle.putLong("time_to_live", this.mTimeToLive);
            String str7 = this.mExpiredEventName;
            if (str7 != null) {
                bundle.putString("expired_event_name", str7);
            }
            Bundle bundle4 = this.mExpiredEventParams;
            if (bundle4 != null) {
                bundle.putBundle("expired_event_params", bundle4);
            }
            bundle.putLong("creation_timestamp", this.mCreationTimestamp);
            bundle.putBoolean("active", this.mActive);
            bundle.putLong("triggered_timestamp", this.mTriggeredTimestamp);
            return bundle;
        }
    }

    public AppMeasurement(ck5 ck5Var) {
        zt2.j(ck5Var);
        this.a = ck5Var;
        this.b = null;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: BlockProcessor
        jadx.core.utils.exceptions.JadxRuntimeException: Unreachable block: B:13:0x002e
        	at jadx.core.dex.visitors.blocks.BlockProcessor.checkForUnreachableBlocks(BlockProcessor.java:81)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.processBlocksTree(BlockProcessor.java:47)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.visit(BlockProcessor.java:39)
        */
    @androidx.annotation.Keep
    @java.lang.Deprecated
    public static com.google.android.gms.measurement.AppMeasurement getInstance(android.content.Context r14) {
        /*
            com.google.android.gms.measurement.AppMeasurement r0 = com.google.android.gms.measurement.AppMeasurement.c
            if (r0 != 0) goto L59
            java.lang.Class<com.google.android.gms.measurement.AppMeasurement> r0 = com.google.android.gms.measurement.AppMeasurement.class
            monitor-enter(r0)
            com.google.android.gms.measurement.AppMeasurement r1 = com.google.android.gms.measurement.AppMeasurement.c     // Catch: java.lang.Throwable -> L56
            if (r1 != 0) goto L54
            r1 = 0
            java.lang.Class<com.google.firebase.analytics.FirebaseAnalytics> r2 = com.google.firebase.analytics.FirebaseAnalytics.class
            r3 = 2
            java.lang.Class[] r4 = new java.lang.Class[r3]     // Catch: java.lang.Throwable -> L56
            java.lang.Class<android.content.Context> r5 = android.content.Context.class
            r6 = 0
            r4[r6] = r5     // Catch: java.lang.Throwable -> L56
            java.lang.Class<android.os.Bundle> r5 = android.os.Bundle.class
            r7 = 1
            r4[r7] = r5     // Catch: java.lang.Throwable -> L56
            java.lang.String r5 = "getScionFrontendApiImplementation"
            java.lang.reflect.Method r2 = r2.getDeclaredMethod(r5, r4)     // Catch: java.lang.Throwable -> L56
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch: java.lang.Throwable -> L56
            r3[r6] = r14     // Catch: java.lang.Throwable -> L56
            r3[r7] = r1     // Catch: java.lang.Throwable -> L56
            java.lang.Object r2 = r2.invoke(r1, r3)     // Catch: java.lang.Throwable -> L56
            fp5 r2 = (defpackage.fp5) r2     // Catch: java.lang.Throwable -> L56
            goto L2f
        L2e:
            r2 = r1
        L2f:
            if (r2 == 0) goto L39
            com.google.android.gms.measurement.AppMeasurement r14 = new com.google.android.gms.measurement.AppMeasurement     // Catch: java.lang.Throwable -> L56
            r14.<init>(r2)     // Catch: java.lang.Throwable -> L56
            com.google.android.gms.measurement.AppMeasurement.c = r14     // Catch: java.lang.Throwable -> L56
            goto L54
        L39:
            com.google.android.gms.internal.measurement.zzcl r13 = new com.google.android.gms.internal.measurement.zzcl     // Catch: java.lang.Throwable -> L56
            r3 = 0
            r5 = 0
            r7 = 1
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 0
            r12 = 0
            r2 = r13
            r2.<init>(r3, r5, r7, r8, r9, r10, r11, r12)     // Catch: java.lang.Throwable -> L56
            ck5 r14 = defpackage.ck5.e(r14, r13, r1)     // Catch: java.lang.Throwable -> L56
            com.google.android.gms.measurement.AppMeasurement r1 = new com.google.android.gms.measurement.AppMeasurement     // Catch: java.lang.Throwable -> L56
            r1.<init>(r14)     // Catch: java.lang.Throwable -> L56
            com.google.android.gms.measurement.AppMeasurement.c = r1     // Catch: java.lang.Throwable -> L56
        L54:
            monitor-exit(r0)     // Catch: java.lang.Throwable -> L56
            goto L59
        L56:
            r14 = move-exception
            monitor-exit(r0)     // Catch: java.lang.Throwable -> L56
            throw r14
        L59:
            com.google.android.gms.measurement.AppMeasurement r14 = com.google.android.gms.measurement.AppMeasurement.c
            return r14
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.AppMeasurement.getInstance(android.content.Context):com.google.android.gms.measurement.AppMeasurement");
    }

    @Keep
    public void beginAdUnitExposure(String str) {
        fp5 fp5Var = this.b;
        if (fp5Var != null) {
            fp5Var.m(str);
            return;
        }
        zt2.j(this.a);
        this.a.d().f(str, this.a.a().b());
    }

    @Keep
    public void clearConditionalUserProperty(String str, String str2, Bundle bundle) {
        fp5 fp5Var = this.b;
        if (fp5Var != null) {
            fp5Var.q(str, str2, bundle);
            return;
        }
        zt2.j(this.a);
        this.a.F().B(str, str2, bundle);
    }

    @Keep
    public void endAdUnitExposure(String str) {
        fp5 fp5Var = this.b;
        if (fp5Var != null) {
            fp5Var.p(str);
            return;
        }
        zt2.j(this.a);
        this.a.d().g(str, this.a.a().b());
    }

    @Keep
    public long generateEventId() {
        fp5 fp5Var = this.b;
        if (fp5Var != null) {
            return fp5Var.a();
        }
        zt2.j(this.a);
        return this.a.G().h0();
    }

    @Keep
    public String getAppInstanceId() {
        fp5 fp5Var = this.b;
        if (fp5Var != null) {
            return fp5Var.g();
        }
        zt2.j(this.a);
        return this.a.F().o();
    }

    @Keep
    public List<ConditionalUserProperty> getConditionalUserProperties(String str, String str2) {
        List<Bundle> C;
        fp5 fp5Var = this.b;
        if (fp5Var != null) {
            C = fp5Var.n(str, str2);
        } else {
            zt2.j(this.a);
            C = this.a.F().C(str, str2);
        }
        ArrayList arrayList = new ArrayList(C == null ? 0 : C.size());
        for (Bundle bundle : C) {
            arrayList.add(new ConditionalUserProperty(bundle));
        }
        return arrayList;
    }

    @Keep
    public String getCurrentScreenClass() {
        fp5 fp5Var = this.b;
        if (fp5Var != null) {
            return fp5Var.f();
        }
        zt2.j(this.a);
        return this.a.F().F();
    }

    @Keep
    public String getCurrentScreenName() {
        fp5 fp5Var = this.b;
        if (fp5Var != null) {
            return fp5Var.h();
        }
        zt2.j(this.a);
        return this.a.F().E();
    }

    @Keep
    public String getGmpAppId() {
        fp5 fp5Var = this.b;
        if (fp5Var != null) {
            return fp5Var.l();
        }
        zt2.j(this.a);
        return this.a.F().G();
    }

    @Keep
    public int getMaxUserProperties(String str) {
        fp5 fp5Var = this.b;
        if (fp5Var != null) {
            return fp5Var.r(str);
        }
        zt2.j(this.a);
        this.a.F().y(str);
        return 25;
    }

    @Keep
    public Map<String, Object> getUserProperties(String str, String str2, boolean z) {
        fp5 fp5Var = this.b;
        if (fp5Var != null) {
            return fp5Var.s(str, str2, z);
        }
        zt2.j(this.a);
        return this.a.F().D(str, str2, z);
    }

    @Keep
    public void logEventInternal(String str, String str2, Bundle bundle) {
        fp5 fp5Var = this.b;
        if (fp5Var != null) {
            fp5Var.i(str, str2, bundle);
            return;
        }
        zt2.j(this.a);
        this.a.F().X(str, str2, bundle);
    }

    @Keep
    public void setConditionalUserProperty(ConditionalUserProperty conditionalUserProperty) {
        zt2.j(conditionalUserProperty);
        fp5 fp5Var = this.b;
        if (fp5Var != null) {
            fp5Var.o(conditionalUserProperty.a());
            return;
        }
        zt2.j(this.a);
        this.a.F().z(conditionalUserProperty.a());
    }

    public AppMeasurement(fp5 fp5Var) {
        zt2.j(fp5Var);
        this.b = fp5Var;
        this.a = null;
    }
}
