package com.google.android.gms.flags.impl;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import com.google.android.gms.common.util.DynamiteApi;

@DynamiteApi
/* loaded from: classes.dex */
public class FlagProviderImpl extends xc5 {
    public boolean a = false;
    public SharedPreferences b;

    @Override // defpackage.ja5
    public boolean getBooleanFlagValue(String str, boolean z, int i) {
        return !this.a ? z : s75.a(this.b, str, Boolean.valueOf(z)).booleanValue();
    }

    @Override // defpackage.ja5
    public int getIntFlagValue(String str, int i, int i2) {
        return !this.a ? i : yc5.a(this.b, str, Integer.valueOf(i)).intValue();
    }

    @Override // defpackage.ja5
    public long getLongFlagValue(String str, long j, int i) {
        return !this.a ? j : ci5.a(this.b, str, Long.valueOf(j)).longValue();
    }

    @Override // defpackage.ja5
    public String getStringFlagValue(String str, String str2, int i) {
        return !this.a ? str2 : sm5.a(this.b, str, str2);
    }

    @Override // defpackage.ja5
    public void init(lm1 lm1Var) {
        Context context = (Context) nl2.G1(lm1Var);
        if (this.a) {
            return;
        }
        try {
            this.b = e.a(context.createPackageContext("com.google.android.gms", 0));
            this.a = true;
        } catch (PackageManager.NameNotFoundException unused) {
        } catch (Exception e) {
            String valueOf = String.valueOf(e.getMessage());
            if (valueOf.length() != 0) {
                "Could not retrieve sdk flags, continuing with defaults: ".concat(valueOf);
            }
        }
    }
}
