package com.google.android.gms.flags.impl;

import android.content.Context;
import android.content.SharedPreferences;
import java.util.concurrent.Callable;

/* loaded from: classes.dex */
public final class f implements Callable<SharedPreferences> {
    public final /* synthetic */ Context a;

    public f(Context context) {
        this.a = context;
    }

    @Override // java.util.concurrent.Callable
    public final /* synthetic */ SharedPreferences call() throws Exception {
        return this.a.getSharedPreferences("google_sdk_flags", 0);
    }
}
