package com.google.android.gms.flags.impl;

import android.content.Context;
import android.content.SharedPreferences;

/* loaded from: classes.dex */
public final class e {
    public static SharedPreferences a;

    public static SharedPreferences a(Context context) throws Exception {
        SharedPreferences sharedPreferences;
        synchronized (SharedPreferences.class) {
            if (a == null) {
                a = (SharedPreferences) of5.a(new f(context));
            }
            sharedPreferences = a;
        }
        return sharedPreferences;
    }
}
