package com.google.android.gms.common;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.common.internal.p;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* loaded from: classes.dex */
public final class zzj extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzj> CREATOR = new wy5();
    public final String a;
    public final vc5 f0;
    public final boolean g0;
    public final boolean h0;

    public zzj(String str, IBinder iBinder, boolean z, boolean z2) {
        this.a = str;
        this.f0 = I1(iBinder);
        this.g0 = z;
        this.h0 = z2;
    }

    public static vc5 I1(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        try {
            lm1 zzb = p.F1(iBinder).zzb();
            byte[] bArr = zzb == null ? null : (byte[]) nl2.G1(zzb);
            if (bArr != null) {
                return new qk5(bArr);
            }
            return null;
        } catch (RemoteException unused) {
            return null;
        }
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int a = yb3.a(parcel);
        yb3.s(parcel, 1, this.a, false);
        vc5 vc5Var = this.f0;
        yb3.l(parcel, 2, vc5Var == null ? null : vc5Var.asBinder(), false);
        yb3.c(parcel, 3, this.g0);
        yb3.c(parcel, 4, this.h0);
        yb3.b(parcel, a);
    }

    public zzj(String str, vc5 vc5Var, boolean z, boolean z2) {
        this.a = str;
        this.f0 = vc5Var;
        this.g0 = z;
        this.h0 = z2;
    }
}
