package com.google.android.gms.common.images;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ParcelFileDescriptor;
import android.os.ResultReceiver;
import android.os.SystemClock;
import com.google.android.gms.common.annotation.KeepName;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public final class ImageManager {
    public static final Object a = new Object();
    public static HashSet<Uri> b = new HashSet<>();

    /* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
    @KeepName
    /* loaded from: classes.dex */
    public final class ImageReceiver extends ResultReceiver {
        public final Uri a;
        public final ArrayList<l05> f0;
        public final /* synthetic */ ImageManager g0;

        @Override // android.os.ResultReceiver
        public final void onReceiveResult(int i, Bundle bundle) {
            ImageManager.h(this.g0).execute(new a(this.g0, this.a, (ParcelFileDescriptor) bundle.getParcelable("com.google.android.gms.extra.fileDescriptor")));
        }
    }

    /* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
    /* loaded from: classes.dex */
    public final class a implements Runnable {
        public final Uri a;
        public final ParcelFileDescriptor f0;
        public final /* synthetic */ ImageManager g0;

        public a(ImageManager imageManager, Uri uri, ParcelFileDescriptor parcelFileDescriptor) {
            this.a = uri;
            this.f0 = parcelFileDescriptor;
        }

        @Override // java.lang.Runnable
        public final void run() {
            ji.b("LoadBitmapFromDiskRunnable can't be executed in the main thread");
            ParcelFileDescriptor parcelFileDescriptor = this.f0;
            boolean z = false;
            Bitmap bitmap = null;
            if (parcelFileDescriptor != null) {
                try {
                    bitmap = BitmapFactory.decodeFileDescriptor(parcelFileDescriptor.getFileDescriptor());
                } catch (OutOfMemoryError unused) {
                    String valueOf = String.valueOf(this.a);
                    StringBuilder sb = new StringBuilder(valueOf.length() + 34);
                    sb.append("OOM while loading bitmap for uri: ");
                    sb.append(valueOf);
                    z = true;
                }
                try {
                    this.f0.close();
                } catch (IOException unused2) {
                }
            }
            CountDownLatch countDownLatch = new CountDownLatch(1);
            ImageManager.i(this.g0).post(new b(this.g0, this.a, bitmap, z, countDownLatch));
            try {
                countDownLatch.await();
            } catch (InterruptedException unused3) {
                String valueOf2 = String.valueOf(this.a);
                StringBuilder sb2 = new StringBuilder(valueOf2.length() + 32);
                sb2.append("Latch interrupted while posting ");
                sb2.append(valueOf2);
            }
        }
    }

    /* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
    /* loaded from: classes.dex */
    public final class b implements Runnable {
        public final Uri a;
        public final Bitmap f0;
        public final CountDownLatch g0;
        public final /* synthetic */ ImageManager h0;

        public b(ImageManager imageManager, Uri uri, Bitmap bitmap, boolean z, CountDownLatch countDownLatch) {
            this.a = uri;
            this.f0 = bitmap;
            this.g0 = countDownLatch;
        }

        @Override // java.lang.Runnable
        public final void run() {
            ji.a("OnBitmapLoadedRunnable must be executed in the main thread");
            boolean z = this.f0 != null;
            ImageReceiver imageReceiver = (ImageReceiver) ImageManager.g(this.h0).remove(this.a);
            if (imageReceiver != null) {
                ArrayList arrayList = imageReceiver.f0;
                int size = arrayList.size();
                for (int i = 0; i < size; i++) {
                    l05 l05Var = (l05) arrayList.get(i);
                    if (this.f0 != null && z) {
                        l05Var.a(ImageManager.c(this.h0), this.f0, false);
                    } else {
                        ImageManager.f(this.h0).put(this.a, Long.valueOf(SystemClock.elapsedRealtime()));
                        l05Var.b(ImageManager.c(this.h0), ImageManager.e(this.h0), false);
                    }
                    ImageManager.b(this.h0).remove(l05Var);
                }
            }
            this.g0.countDown();
            synchronized (ImageManager.a) {
                ImageManager.b.remove(this.a);
            }
        }
    }

    public static /* synthetic */ Map b(ImageManager imageManager) {
        throw null;
    }

    public static /* synthetic */ Context c(ImageManager imageManager) {
        throw null;
    }

    public static /* synthetic */ z15 e(ImageManager imageManager) {
        throw null;
    }

    public static /* synthetic */ Map f(ImageManager imageManager) {
        throw null;
    }

    public static /* synthetic */ Map g(ImageManager imageManager) {
        throw null;
    }

    public static /* synthetic */ ExecutorService h(ImageManager imageManager) {
        throw null;
    }

    public static /* synthetic */ Handler i(ImageManager imageManager) {
        throw null;
    }
}
