package com.google.android.gms.common.images;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.RecentlyNonNull;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.util.Locale;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public final class WebImage extends AbstractSafeParcelable {
    @RecentlyNonNull
    public static final Parcelable.Creator<WebImage> CREATOR = new t15();
    public final int a;
    public final Uri f0;
    public final int g0;
    public final int h0;

    public WebImage(int i, Uri uri, int i2, int i3) {
        this.a = i;
        this.f0 = uri;
        this.g0 = i2;
        this.h0 = i3;
    }

    @RecentlyNonNull
    public final Uri I1() {
        return this.f0;
    }

    @RecentlyNonNull
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && (obj instanceof WebImage)) {
            WebImage webImage = (WebImage) obj;
            if (pl2.a(this.f0, webImage.f0) && this.g0 == webImage.g0 && this.h0 == webImage.h0) {
                return true;
            }
        }
        return false;
    }

    @RecentlyNonNull
    public final int getHeight() {
        return this.h0;
    }

    @RecentlyNonNull
    public final int getWidth() {
        return this.g0;
    }

    @RecentlyNonNull
    public final int hashCode() {
        return pl2.b(this.f0, Integer.valueOf(this.g0), Integer.valueOf(this.h0));
    }

    @RecentlyNonNull
    public final String toString() {
        return String.format(Locale.US, "Image %dx%d %s", Integer.valueOf(this.g0), Integer.valueOf(this.h0), this.f0.toString());
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        int a = yb3.a(parcel);
        yb3.m(parcel, 1, this.a);
        yb3.r(parcel, 2, I1(), i, false);
        yb3.m(parcel, 3, getWidth());
        yb3.m(parcel, 4, getHeight());
        yb3.b(parcel, a);
    }
}
