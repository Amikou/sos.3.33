package com.google.android.gms.common.internal;

import android.os.IInterface;
import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public interface l extends IInterface {
    lm1 l(lm1 lm1Var, zau zauVar) throws RemoteException;
}
