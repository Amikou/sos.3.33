package com.google.android.gms.common.internal;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.Feature;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* loaded from: classes.dex */
public final class zzc extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzc> CREATOR = new p75();
    public Bundle a;
    public Feature[] f0;
    public int g0;

    public zzc(Bundle bundle, Feature[] featureArr, int i, ConnectionTelemetryConfiguration connectionTelemetryConfiguration) {
        this.a = bundle;
        this.f0 = featureArr;
        this.g0 = i;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int a = yb3.a(parcel);
        yb3.e(parcel, 1, this.a, false);
        yb3.v(parcel, 2, this.f0, i, false);
        yb3.m(parcel, 3, this.g0);
        yb3.r(parcel, 4, null, i, false);
        yb3.b(parcel, a);
    }

    public zzc() {
    }
}
