package com.google.android.gms.common.internal;

import android.os.IInterface;
import android.os.RemoteException;
import androidx.annotation.RecentlyNonNull;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* loaded from: classes.dex */
public interface h extends IInterface {
    void C(@RecentlyNonNull f fVar, GetServiceRequest getServiceRequest) throws RemoteException;
}
