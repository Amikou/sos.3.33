package com.google.android.gms.common.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public final class zau extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zau> CREATOR = new c35();
    public final int a;
    public final int f0;
    public final int g0;
    @Deprecated
    public final Scope[] h0;

    public zau(int i, int i2, int i3, Scope[] scopeArr) {
        this.a = i;
        this.f0 = i2;
        this.g0 = i3;
        this.h0 = scopeArr;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int a = yb3.a(parcel);
        yb3.m(parcel, 1, this.a);
        yb3.m(parcel, 2, this.f0);
        yb3.m(parcel, 3, this.g0);
        yb3.v(parcel, 4, this.h0, i, false);
        yb3.b(parcel, a);
    }

    public zau(int i, int i2, Scope[] scopeArr) {
        this(1, i, i2, null);
    }
}
