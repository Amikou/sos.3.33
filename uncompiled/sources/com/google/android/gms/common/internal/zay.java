package com.google.android.gms.common.internal;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.Button;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public final class zay extends Button {
    public zay(Context context) {
        this(context, null);
    }

    public static int a(int i, int i2, int i3, int i4) {
        if (i != 0) {
            if (i != 1) {
                if (i == 2) {
                    return i4;
                }
                StringBuilder sb = new StringBuilder(33);
                sb.append("Unknown color scheme: ");
                sb.append(i);
                throw new IllegalStateException(sb.toString());
            }
            return i3;
        }
        return i2;
    }

    public final void b(Resources resources, int i, int i2) {
        setTypeface(Typeface.DEFAULT_BOLD);
        setTextSize(14.0f);
        int i3 = (int) ((resources.getDisplayMetrics().density * 48.0f) + 0.5f);
        setMinHeight(i3);
        setMinWidth(i3);
        int i4 = pz2.common_google_signin_btn_icon_dark;
        int i5 = pz2.common_google_signin_btn_icon_light;
        int a = a(i2, i4, i5, i5);
        int i6 = pz2.common_google_signin_btn_text_dark;
        int i7 = pz2.common_google_signin_btn_text_light;
        int a2 = a(i2, i6, i7, i7);
        if (i == 0 || i == 1) {
            a = a2;
        } else if (i != 2) {
            StringBuilder sb = new StringBuilder(32);
            sb.append("Unknown button size: ");
            sb.append(i);
            throw new IllegalStateException(sb.toString());
        }
        Drawable r = androidx.core.graphics.drawable.a.r(resources.getDrawable(a));
        androidx.core.graphics.drawable.a.o(r, resources.getColorStateList(sy2.common_google_signin_btn_tint));
        androidx.core.graphics.drawable.a.p(r, PorterDuff.Mode.SRC_ATOP);
        setBackgroundDrawable(r);
        int i8 = sy2.common_google_signin_btn_text_dark;
        int i9 = sy2.common_google_signin_btn_text_light;
        setTextColor((ColorStateList) zt2.j(resources.getColorStateList(a(i2, i8, i9, i9))));
        if (i == 0) {
            setText(resources.getString(p13.common_signin_button_text));
        } else if (i == 1) {
            setText(resources.getString(p13.common_signin_button_text_long));
        } else if (i == 2) {
            setText((CharSequence) null);
        } else {
            StringBuilder sb2 = new StringBuilder(32);
            sb2.append("Unknown button size: ");
            sb2.append(i);
            throw new IllegalStateException(sb2.toString());
        }
        setTransformationMethod(null);
        if (vm0.d(getContext())) {
            setGravity(19);
        }
    }

    public zay(Context context, AttributeSet attributeSet) {
        super(context, null, 16842824);
    }
}
