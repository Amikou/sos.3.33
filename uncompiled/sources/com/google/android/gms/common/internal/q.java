package com.google.android.gms.common.internal;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.common.zzj;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* loaded from: classes.dex */
public interface q extends IInterface {
    boolean p0(zzj zzjVar, lm1 lm1Var) throws RemoteException;
}
