package com.google.android.gms.common.internal;

import android.os.IInterface;
import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* loaded from: classes.dex */
public interface n extends IInterface {
    lm1 zzb() throws RemoteException;

    int zzc() throws RemoteException;
}
