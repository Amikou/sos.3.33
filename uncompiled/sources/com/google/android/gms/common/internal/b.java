package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Log;
import androidx.annotation.RecentlyNonNull;
import androidx.annotation.RecentlyNullable;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Feature;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.f;
import defpackage.rg1;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* loaded from: classes.dex */
public abstract class b<T extends IInterface> {
    public static final Feature[] B = new Feature[0];
    @RecentlyNonNull
    public AtomicInteger A;
    public int a;
    public long b;
    public long c;
    public int d;
    public long e;
    public volatile String f;
    public ex5 g;
    public final Context h;
    public final rg1 i;
    public final eh1 j;
    public final Handler k;
    public final Object l;
    public final Object m;
    public com.google.android.gms.common.internal.h n;
    @RecentlyNonNull
    public c o;
    public T p;
    public final ArrayList<h<?>> q;
    public i r;
    public int s;
    public final a t;
    public final InterfaceC0111b u;
    public final int v;
    public final String w;
    public ConnectionResult x;
    public boolean y;
    public volatile zzc z;

    /* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
    /* loaded from: classes.dex */
    public interface a {
        void onConnected(Bundle bundle);

        void onConnectionSuspended(@RecentlyNonNull int i);
    }

    /* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
    /* renamed from: com.google.android.gms.common.internal.b$b  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public interface InterfaceC0111b {
        void onConnectionFailed(@RecentlyNonNull ConnectionResult connectionResult);
    }

    /* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
    /* loaded from: classes.dex */
    public interface c {
        void b(@RecentlyNonNull ConnectionResult connectionResult);
    }

    /* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
    /* loaded from: classes.dex */
    public class d implements c {
        public d() {
        }

        @Override // com.google.android.gms.common.internal.b.c
        public void b(@RecentlyNonNull ConnectionResult connectionResult) {
            if (!connectionResult.M1()) {
                if (b.this.u != null) {
                    b.this.u.onConnectionFailed(connectionResult);
                    return;
                }
                return;
            }
            b bVar = b.this;
            bVar.h(null, bVar.F());
        }
    }

    /* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
    /* loaded from: classes.dex */
    public interface e {
        void a();
    }

    /* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
    /* loaded from: classes.dex */
    public abstract class f extends h<Boolean> {
        public final int d;
        public final Bundle e;

        public f(int i, Bundle bundle) {
            super(Boolean.TRUE);
            this.d = i;
            this.e = bundle;
        }

        @Override // com.google.android.gms.common.internal.b.h
        public final /* synthetic */ void a(Boolean bool) {
            if (bool == null) {
                b.this.Z(1, null);
            } else if (this.d != 0) {
                b.this.Z(1, null);
                Bundle bundle = this.e;
                f(new ConnectionResult(this.d, bundle != null ? (PendingIntent) bundle.getParcelable("pendingIntent") : null));
            } else if (g()) {
            } else {
                b.this.Z(1, null);
                f(new ConnectionResult(8, null));
            }
        }

        @Override // com.google.android.gms.common.internal.b.h
        public final void b() {
        }

        public abstract void f(ConnectionResult connectionResult);

        public abstract boolean g();
    }

    /* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
    /* loaded from: classes.dex */
    public final class g extends pp5 {
        public g(Looper looper) {
            super(looper);
        }

        public static void a(Message message) {
            h hVar = (h) message.obj;
            hVar.b();
            hVar.d();
        }

        public static boolean b(Message message) {
            int i = message.what;
            return i == 2 || i == 1 || i == 7;
        }

        @Override // android.os.Handler
        public final void handleMessage(Message message) {
            ConnectionResult connectionResult;
            ConnectionResult connectionResult2;
            if (b.this.A.get() != message.arg1) {
                if (b(message)) {
                    a(message);
                    return;
                }
                return;
            }
            int i = message.what;
            if ((i == 1 || i == 7 || ((i == 4 && !b.this.z()) || message.what == 5)) && !b.this.k()) {
                a(message);
                return;
            }
            int i2 = message.what;
            if (i2 == 4) {
                b.this.x = new ConnectionResult(message.arg2);
                if (!b.this.i0() || b.this.y) {
                    if (b.this.x != null) {
                        connectionResult2 = b.this.x;
                    } else {
                        connectionResult2 = new ConnectionResult(8);
                    }
                    b.this.o.b(connectionResult2);
                    b.this.M(connectionResult2);
                    return;
                }
                b.this.Z(3, null);
            } else if (i2 == 5) {
                if (b.this.x != null) {
                    connectionResult = b.this.x;
                } else {
                    connectionResult = new ConnectionResult(8);
                }
                b.this.o.b(connectionResult);
                b.this.M(connectionResult);
            } else if (i2 == 3) {
                Object obj = message.obj;
                ConnectionResult connectionResult3 = new ConnectionResult(message.arg2, obj instanceof PendingIntent ? (PendingIntent) obj : null);
                b.this.o.b(connectionResult3);
                b.this.M(connectionResult3);
            } else if (i2 == 6) {
                b.this.Z(5, null);
                if (b.this.t != null) {
                    b.this.t.onConnectionSuspended(message.arg2);
                }
                b.this.N(message.arg2);
                b.this.e0(5, 1, null);
            } else if (i2 == 2 && !b.this.c()) {
                a(message);
            } else if (b(message)) {
                ((h) message.obj).c();
            } else {
                int i3 = message.what;
                StringBuilder sb = new StringBuilder(45);
                sb.append("Don't know how to handle message: ");
                sb.append(i3);
                Log.wtf("GmsClient", sb.toString(), new Exception());
            }
        }
    }

    /* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
    /* loaded from: classes.dex */
    public abstract class h<TListener> {
        public TListener a;
        public boolean b = false;

        public h(TListener tlistener) {
            this.a = tlistener;
        }

        public abstract void a(TListener tlistener);

        public abstract void b();

        public final void c() {
            TListener tlistener;
            synchronized (this) {
                tlistener = this.a;
                if (this.b) {
                    String valueOf = String.valueOf(this);
                    StringBuilder sb = new StringBuilder(valueOf.length() + 47);
                    sb.append("Callback proxy ");
                    sb.append(valueOf);
                    sb.append(" being reused. This is not safe.");
                }
            }
            if (tlistener != null) {
                try {
                    a(tlistener);
                } catch (RuntimeException e) {
                    b();
                    throw e;
                }
            } else {
                b();
            }
            synchronized (this) {
                this.b = true;
            }
            d();
        }

        public final void d() {
            e();
            synchronized (b.this.q) {
                b.this.q.remove(this);
            }
        }

        public final void e() {
            synchronized (this) {
                this.a = null;
            }
        }
    }

    /* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
    /* loaded from: classes.dex */
    public final class i implements ServiceConnection {
        public final int a;

        public i(int i) {
            this.a = i;
        }

        @Override // android.content.ServiceConnection
        public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            com.google.android.gms.common.internal.h gVar;
            if (iBinder == null) {
                b.this.X(16);
                return;
            }
            synchronized (b.this.m) {
                b bVar = b.this;
                IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                if (queryLocalInterface != null && (queryLocalInterface instanceof com.google.android.gms.common.internal.h)) {
                    gVar = (com.google.android.gms.common.internal.h) queryLocalInterface;
                } else {
                    gVar = new com.google.android.gms.common.internal.g(iBinder);
                }
                bVar.n = gVar;
            }
            b.this.Y(0, null, this.a);
        }

        @Override // android.content.ServiceConnection
        public final void onServiceDisconnected(ComponentName componentName) {
            synchronized (b.this.m) {
                b.this.n = null;
            }
            Handler handler = b.this.k;
            handler.sendMessage(handler.obtainMessage(6, this.a, 1));
        }
    }

    /* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
    /* loaded from: classes.dex */
    public static final class j extends f.a {
        public b a;
        public final int b;

        public j(b bVar, int i) {
            this.a = bVar;
            this.b = i;
        }

        @Override // com.google.android.gms.common.internal.f
        public final void U0(int i, IBinder iBinder, zzc zzcVar) {
            b bVar = this.a;
            zt2.k(bVar, "onPostInitCompleteWithConnectionInfo can be called only once per call togetRemoteService");
            zt2.j(zzcVar);
            bVar.d0(zzcVar);
            k0(i, iBinder, zzcVar.a);
        }

        @Override // com.google.android.gms.common.internal.f
        public final void f0(int i, Bundle bundle) {
            Log.wtf("GmsClient", "received deprecated onAccountValidationComplete callback, ignoring", new Exception());
        }

        @Override // com.google.android.gms.common.internal.f
        public final void k0(int i, IBinder iBinder, Bundle bundle) {
            zt2.k(this.a, "onPostInitComplete can be called only once per call to getRemoteService");
            this.a.O(i, iBinder, bundle, this.b);
            this.a = null;
        }
    }

    /* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
    /* loaded from: classes.dex */
    public final class k extends f {
        public final IBinder g;

        public k(int i, IBinder iBinder, Bundle bundle) {
            super(i, bundle);
            this.g = iBinder;
        }

        @Override // com.google.android.gms.common.internal.b.f
        public final void f(ConnectionResult connectionResult) {
            if (b.this.u != null) {
                b.this.u.onConnectionFailed(connectionResult);
            }
            b.this.M(connectionResult);
        }

        @Override // com.google.android.gms.common.internal.b.f
        public final boolean g() {
            try {
                String interfaceDescriptor = ((IBinder) zt2.j(this.g)).getInterfaceDescriptor();
                if (!b.this.H().equals(interfaceDescriptor)) {
                    String H = b.this.H();
                    StringBuilder sb = new StringBuilder(String.valueOf(H).length() + 34 + String.valueOf(interfaceDescriptor).length());
                    sb.append("service descriptor mismatch: ");
                    sb.append(H);
                    sb.append(" vs. ");
                    sb.append(interfaceDescriptor);
                    return false;
                }
                IInterface y = b.this.y(this.g);
                if (y != null) {
                    if (b.this.e0(2, 4, y) || b.this.e0(3, 4, y)) {
                        b.this.x = null;
                        Bundle v = b.this.v();
                        if (b.this.t != null) {
                            b.this.t.onConnected(v);
                        }
                        return true;
                    }
                    return false;
                }
                return false;
            } catch (RemoteException unused) {
                return false;
            }
        }
    }

    /* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
    /* loaded from: classes.dex */
    public final class l extends f {
        public l(int i, Bundle bundle) {
            super(i, null);
        }

        @Override // com.google.android.gms.common.internal.b.f
        public final void f(ConnectionResult connectionResult) {
            if (b.this.z() && b.this.i0()) {
                b.this.X(16);
                return;
            }
            b.this.o.b(connectionResult);
            b.this.M(connectionResult);
        }

        @Override // com.google.android.gms.common.internal.b.f
        public final boolean g() {
            b.this.o.b(ConnectionResult.i0);
            return true;
        }
    }

    public b(@RecentlyNonNull Context context, @RecentlyNonNull Looper looper, @RecentlyNonNull int i2, @RecentlyNonNull a aVar, @RecentlyNonNull InterfaceC0111b interfaceC0111b, @RecentlyNonNull String str) {
        this(context, looper, rg1.b(context), eh1.h(), i2, (a) zt2.j(aVar), (InterfaceC0111b) zt2.j(interfaceC0111b), str);
    }

    @RecentlyNullable
    public Account A() {
        return null;
    }

    @RecentlyNonNull
    public Feature[] B() {
        return B;
    }

    @RecentlyNonNull
    public final Context C() {
        return this.h;
    }

    @RecentlyNonNull
    public Bundle D() {
        return new Bundle();
    }

    @RecentlyNullable
    public String E() {
        return null;
    }

    @RecentlyNonNull
    public Set<Scope> F() {
        return Collections.emptySet();
    }

    @RecentlyNonNull
    public final T G() throws DeadObjectException {
        T t;
        synchronized (this.l) {
            if (this.s != 5) {
                x();
                t = (T) zt2.k(this.p, "Client is connected but service is null");
            } else {
                throw new DeadObjectException();
            }
        }
        return t;
    }

    public abstract String H();

    public abstract String I();

    @RecentlyNonNull
    public String J() {
        return "com.google.android.gms";
    }

    @RecentlyNonNull
    public boolean K() {
        return false;
    }

    public void L(@RecentlyNonNull T t) {
        this.c = System.currentTimeMillis();
    }

    public void M(@RecentlyNonNull ConnectionResult connectionResult) {
        this.d = connectionResult.I1();
        this.e = System.currentTimeMillis();
    }

    public void N(@RecentlyNonNull int i2) {
        this.a = i2;
        this.b = System.currentTimeMillis();
    }

    public void O(@RecentlyNonNull int i2, IBinder iBinder, Bundle bundle, @RecentlyNonNull int i3) {
        Handler handler = this.k;
        handler.sendMessage(handler.obtainMessage(1, i3, -1, new k(i2, iBinder, bundle)));
    }

    @RecentlyNonNull
    public boolean P() {
        return false;
    }

    public void Q(@RecentlyNonNull int i2) {
        Handler handler = this.k;
        handler.sendMessage(handler.obtainMessage(6, this.A.get(), i2));
    }

    public void R(@RecentlyNonNull c cVar, @RecentlyNonNull int i2, PendingIntent pendingIntent) {
        this.o = (c) zt2.k(cVar, "Connection progress callbacks cannot be null.");
        Handler handler = this.k;
        handler.sendMessage(handler.obtainMessage(3, this.A.get(), i2, pendingIntent));
    }

    @RecentlyNonNull
    public boolean S() {
        return false;
    }

    public final String W() {
        String str = this.w;
        return str == null ? this.h.getClass().getName() : str;
    }

    public final void X(int i2) {
        int i3;
        if (g0()) {
            i3 = 5;
            this.y = true;
        } else {
            i3 = 4;
        }
        Handler handler = this.k;
        handler.sendMessage(handler.obtainMessage(i3, this.A.get(), 16));
    }

    public final void Y(@RecentlyNonNull int i2, Bundle bundle, @RecentlyNonNull int i3) {
        Handler handler = this.k;
        handler.sendMessage(handler.obtainMessage(7, i3, -1, new l(i2, null)));
    }

    /* JADX WARN: Multi-variable type inference failed */
    public final void Z(int i2, T t) {
        ex5 ex5Var;
        ex5 ex5Var2;
        zt2.a((i2 == 4) == (t != null));
        synchronized (this.l) {
            this.s = i2;
            this.p = t;
            if (i2 == 1) {
                i iVar = this.r;
                if (iVar != null) {
                    this.i.c((String) zt2.j(this.g.a()), this.g.b(), this.g.c(), iVar, W(), this.g.d());
                    this.r = null;
                }
            } else if (i2 == 2 || i2 == 3) {
                i iVar2 = this.r;
                if (iVar2 != null && (ex5Var2 = this.g) != null) {
                    String a2 = ex5Var2.a();
                    String b = this.g.b();
                    StringBuilder sb = new StringBuilder(String.valueOf(a2).length() + 70 + String.valueOf(b).length());
                    sb.append("Calling connect() while still connected, missing disconnect() for ");
                    sb.append(a2);
                    sb.append(" on ");
                    sb.append(b);
                    this.i.c((String) zt2.j(this.g.a()), this.g.b(), this.g.c(), iVar2, W(), this.g.d());
                    this.A.incrementAndGet();
                }
                i iVar3 = new i(this.A.get());
                this.r = iVar3;
                if (this.s == 3 && E() != null) {
                    ex5Var = new ex5(C().getPackageName(), E(), true, rg1.a(), false);
                } else {
                    ex5Var = new ex5(J(), I(), false, rg1.a(), K());
                }
                this.g = ex5Var;
                if (ex5Var.d() && q() < 17895000) {
                    String valueOf = String.valueOf(this.g.a());
                    throw new IllegalStateException(valueOf.length() != 0 ? "Internal Error, the minimum apk version of this BaseGmsClient is too low to support dynamic lookup. Start service action: ".concat(valueOf) : new String("Internal Error, the minimum apk version of this BaseGmsClient is too low to support dynamic lookup. Start service action: "));
                }
                if (!this.i.d(new rg1.a((String) zt2.j(this.g.a()), this.g.b(), this.g.c(), this.g.d()), iVar3, W())) {
                    String a3 = this.g.a();
                    String b2 = this.g.b();
                    StringBuilder sb2 = new StringBuilder(String.valueOf(a3).length() + 34 + String.valueOf(b2).length());
                    sb2.append("unable to connect to service: ");
                    sb2.append(a3);
                    sb2.append(" on ");
                    sb2.append(b2);
                    Y(16, null, this.A.get());
                }
            } else if (i2 == 4) {
                L((IInterface) zt2.j(t));
            }
        }
    }

    public void b() {
        this.A.incrementAndGet();
        synchronized (this.q) {
            int size = this.q.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.q.get(i2).e();
            }
            this.q.clear();
        }
        synchronized (this.m) {
            this.n = null;
        }
        Z(1, null);
    }

    @RecentlyNonNull
    public boolean c() {
        boolean z;
        synchronized (this.l) {
            z = this.s == 4;
        }
        return z;
    }

    public final void d0(zzc zzcVar) {
        this.z = zzcVar;
    }

    @RecentlyNonNull
    public boolean e() {
        return false;
    }

    public final boolean e0(int i2, int i3, T t) {
        synchronized (this.l) {
            if (this.s != i2) {
                return false;
            }
            Z(i3, t);
            return true;
        }
    }

    public final boolean g0() {
        boolean z;
        synchronized (this.l) {
            z = this.s == 3;
        }
        return z;
    }

    public void h(com.google.android.gms.common.internal.d dVar, @RecentlyNonNull Set<Scope> set) {
        Bundle D = D();
        GetServiceRequest getServiceRequest = new GetServiceRequest(this.v);
        getServiceRequest.h0 = this.h.getPackageName();
        getServiceRequest.k0 = D;
        if (set != null) {
            getServiceRequest.j0 = (Scope[]) set.toArray(new Scope[set.size()]);
        }
        if (u()) {
            Account A = A();
            if (A == null) {
                A = new Account("<<default account>>", "com.google");
            }
            getServiceRequest.l0 = A;
            if (dVar != null) {
                getServiceRequest.i0 = dVar.asBinder();
            }
        } else if (P()) {
            getServiceRequest.l0 = A();
        }
        getServiceRequest.m0 = B;
        getServiceRequest.n0 = B();
        if (S()) {
            getServiceRequest.q0 = true;
        }
        try {
            try {
                synchronized (this.m) {
                    com.google.android.gms.common.internal.h hVar = this.n;
                    if (hVar != null) {
                        hVar.C(new j(this, this.A.get()), getServiceRequest);
                    }
                }
            } catch (DeadObjectException unused) {
                Q(3);
            } catch (SecurityException e2) {
                throw e2;
            }
        } catch (RemoteException | RuntimeException unused2) {
            O(8, null, null, this.A.get());
        }
    }

    public void i(@RecentlyNonNull String str) {
        this.f = str;
        b();
    }

    public final boolean i0() {
        if (this.y || TextUtils.isEmpty(H()) || TextUtils.isEmpty(E())) {
            return false;
        }
        try {
            Class.forName(H());
            return true;
        } catch (ClassNotFoundException unused) {
            return false;
        }
    }

    @RecentlyNonNull
    public boolean k() {
        boolean z;
        synchronized (this.l) {
            int i2 = this.s;
            z = i2 == 2 || i2 == 3;
        }
        return z;
    }

    @RecentlyNonNull
    public String l() {
        ex5 ex5Var;
        if (c() && (ex5Var = this.g) != null) {
            return ex5Var.b();
        }
        throw new RuntimeException("Failed to connect when checking package");
    }

    public void m(@RecentlyNonNull c cVar) {
        this.o = (c) zt2.k(cVar, "Connection progress callbacks cannot be null.");
        Z(2, null);
    }

    public void n(@RecentlyNonNull e eVar) {
        eVar.a();
    }

    public void o(@RecentlyNonNull String str, @RecentlyNonNull FileDescriptor fileDescriptor, @RecentlyNonNull PrintWriter printWriter, @RecentlyNonNull String[] strArr) {
        int i2;
        T t;
        com.google.android.gms.common.internal.h hVar;
        synchronized (this.l) {
            i2 = this.s;
            t = this.p;
        }
        synchronized (this.m) {
            hVar = this.n;
        }
        printWriter.append((CharSequence) str).append("mConnectState=");
        if (i2 == 1) {
            printWriter.print("DISCONNECTED");
        } else if (i2 == 2) {
            printWriter.print("REMOTE_CONNECTING");
        } else if (i2 == 3) {
            printWriter.print("LOCAL_CONNECTING");
        } else if (i2 == 4) {
            printWriter.print("CONNECTED");
        } else if (i2 != 5) {
            printWriter.print("UNKNOWN");
        } else {
            printWriter.print("DISCONNECTING");
        }
        printWriter.append(" mService=");
        if (t == null) {
            printWriter.append("null");
        } else {
            printWriter.append((CharSequence) H()).append("@").append((CharSequence) Integer.toHexString(System.identityHashCode(t.asBinder())));
        }
        printWriter.append(" mServiceBroker=");
        if (hVar == null) {
            printWriter.println("null");
        } else {
            printWriter.append("IGmsServiceBroker@").println(Integer.toHexString(System.identityHashCode(hVar.asBinder())));
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.US);
        if (this.c > 0) {
            PrintWriter append = printWriter.append((CharSequence) str).append("lastConnectedTime=");
            long j2 = this.c;
            String format = simpleDateFormat.format(new Date(this.c));
            StringBuilder sb = new StringBuilder(String.valueOf(format).length() + 21);
            sb.append(j2);
            sb.append(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
            sb.append(format);
            append.println(sb.toString());
        }
        if (this.b > 0) {
            printWriter.append((CharSequence) str).append("lastSuspendedCause=");
            int i3 = this.a;
            if (i3 == 1) {
                printWriter.append("CAUSE_SERVICE_DISCONNECTED");
            } else if (i3 == 2) {
                printWriter.append("CAUSE_NETWORK_LOST");
            } else if (i3 != 3) {
                printWriter.append((CharSequence) String.valueOf(i3));
            } else {
                printWriter.append("CAUSE_DEAD_OBJECT_EXCEPTION");
            }
            PrintWriter append2 = printWriter.append(" lastSuspendedTime=");
            long j3 = this.b;
            String format2 = simpleDateFormat.format(new Date(this.b));
            StringBuilder sb2 = new StringBuilder(String.valueOf(format2).length() + 21);
            sb2.append(j3);
            sb2.append(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
            sb2.append(format2);
            append2.println(sb2.toString());
        }
        if (this.e > 0) {
            printWriter.append((CharSequence) str).append("lastFailedStatus=").append((CharSequence) i30.a(this.d));
            PrintWriter append3 = printWriter.append(" lastFailedTime=");
            long j4 = this.e;
            String format3 = simpleDateFormat.format(new Date(this.e));
            StringBuilder sb3 = new StringBuilder(String.valueOf(format3).length() + 21);
            sb3.append(j4);
            sb3.append(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
            sb3.append(format3);
            append3.println(sb3.toString());
        }
    }

    @RecentlyNonNull
    public boolean p() {
        return true;
    }

    @RecentlyNonNull
    public abstract int q();

    @RecentlyNullable
    public final Feature[] r() {
        zzc zzcVar = this.z;
        if (zzcVar == null) {
            return null;
        }
        return zzcVar.f0;
    }

    @RecentlyNullable
    public String s() {
        return this.f;
    }

    @RecentlyNonNull
    public Intent t() {
        throw new UnsupportedOperationException("Not a sign in API");
    }

    @RecentlyNonNull
    public boolean u() {
        return false;
    }

    @RecentlyNullable
    public Bundle v() {
        return null;
    }

    public void w() {
        int j2 = this.j.j(this.h, q());
        if (j2 != 0) {
            Z(1, null);
            R(new d(), j2, null);
            return;
        }
        m(new d());
    }

    public final void x() {
        if (!c()) {
            throw new IllegalStateException("Not connected. Call connect() and wait for onConnected() to be called.");
        }
    }

    @RecentlyNullable
    public abstract T y(@RecentlyNonNull IBinder iBinder);

    @RecentlyNonNull
    public boolean z() {
        return false;
    }

    public b(@RecentlyNonNull Context context, @RecentlyNonNull Looper looper, @RecentlyNonNull rg1 rg1Var, @RecentlyNonNull eh1 eh1Var, @RecentlyNonNull int i2, a aVar, InterfaceC0111b interfaceC0111b, String str) {
        this.f = null;
        this.l = new Object();
        this.m = new Object();
        this.q = new ArrayList<>();
        this.s = 1;
        this.x = null;
        this.y = false;
        this.z = null;
        this.A = new AtomicInteger(0);
        this.h = (Context) zt2.k(context, "Context must not be null");
        Looper looper2 = (Looper) zt2.k(looper, "Looper must not be null");
        this.i = (rg1) zt2.k(rg1Var, "Supervisor must not be null");
        this.j = (eh1) zt2.k(eh1Var, "API availability must not be null");
        this.k = new g(looper);
        this.v = i2;
        this.t = aVar;
        this.u = interfaceC0111b;
        this.w = str;
    }
}
