package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.os.Binder;
import android.os.RemoteException;
import androidx.annotation.RecentlyNonNull;
import androidx.annotation.RecentlyNullable;
import com.google.android.gms.common.internal.d;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* loaded from: classes.dex */
public class a extends d.a {
    @RecentlyNullable
    public static Account G1(@RecentlyNonNull d dVar) {
        if (dVar != null) {
            long clearCallingIdentity = Binder.clearCallingIdentity();
            try {
                return dVar.zza();
            } catch (RemoteException unused) {
            } finally {
                Binder.restoreCallingIdentity(clearCallingIdentity);
            }
        }
        return null;
    }
}
