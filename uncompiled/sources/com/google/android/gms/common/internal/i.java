package com.google.android.gms.common.internal;

import android.os.Bundle;
import com.google.android.gms.common.internal.b;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public final class i implements b.a {
    public final /* synthetic */ u50 a;

    public i(u50 u50Var) {
        this.a = u50Var;
    }

    @Override // com.google.android.gms.common.internal.b.a
    public final void onConnected(Bundle bundle) {
        this.a.onConnected(bundle);
    }

    @Override // com.google.android.gms.common.internal.b.a
    public final void onConnectionSuspended(int i) {
        this.a.onConnectionSuspended(i);
    }
}
