package com.google.android.gms.common.internal.safeparcel;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.RecentlyNonNull;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* loaded from: classes.dex */
public class SafeParcelReader {

    /* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
    /* loaded from: classes.dex */
    public static class ParseException extends RuntimeException {
        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public ParseException(@androidx.annotation.RecentlyNonNull java.lang.String r4, @androidx.annotation.RecentlyNonNull android.os.Parcel r5) {
            /*
                r3 = this;
                int r0 = r5.dataPosition()
                int r5 = r5.dataSize()
                java.lang.String r1 = java.lang.String.valueOf(r4)
                int r1 = r1.length()
                int r1 = r1 + 41
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>(r1)
                r2.append(r4)
                java.lang.String r4 = " Parcel: pos="
                r2.append(r4)
                r2.append(r0)
                java.lang.String r4 = " size="
                r2.append(r4)
                r2.append(r5)
                java.lang.String r4 = r2.toString()
                r3.<init>(r4)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.internal.safeparcel.SafeParcelReader.ParseException.<init>(java.lang.String, android.os.Parcel):void");
        }
    }

    @RecentlyNonNull
    public static float A(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        K(parcel, i, 4);
        return parcel.readFloat();
    }

    @RecentlyNonNull
    public static Float B(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        int H = H(parcel, i);
        if (H == 0) {
            return null;
        }
        L(parcel, i, H, 4);
        return Float.valueOf(parcel.readFloat());
    }

    @RecentlyNonNull
    public static int C(@RecentlyNonNull Parcel parcel) {
        return parcel.readInt();
    }

    @RecentlyNonNull
    public static IBinder D(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        int H = H(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (H == 0) {
            return null;
        }
        IBinder readStrongBinder = parcel.readStrongBinder();
        parcel.setDataPosition(dataPosition + H);
        return readStrongBinder;
    }

    @RecentlyNonNull
    public static int E(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        K(parcel, i, 4);
        return parcel.readInt();
    }

    @RecentlyNonNull
    public static long F(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        K(parcel, i, 8);
        return parcel.readLong();
    }

    @RecentlyNonNull
    public static Long G(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        int H = H(parcel, i);
        if (H == 0) {
            return null;
        }
        L(parcel, i, H, 8);
        return Long.valueOf(parcel.readLong());
    }

    @RecentlyNonNull
    public static int H(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        return (i & (-65536)) != -65536 ? (i >> 16) & 65535 : parcel.readInt();
    }

    public static void I(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        parcel.setDataPosition(parcel.dataPosition() + H(parcel, i));
    }

    @RecentlyNonNull
    public static int J(@RecentlyNonNull Parcel parcel) {
        int C = C(parcel);
        int H = H(parcel, C);
        int dataPosition = parcel.dataPosition();
        if (v(C) != 20293) {
            String valueOf = String.valueOf(Integer.toHexString(C));
            throw new ParseException(valueOf.length() != 0 ? "Expected object header. Got 0x".concat(valueOf) : new String("Expected object header. Got 0x"), parcel);
        }
        int i = H + dataPosition;
        if (i < dataPosition || i > parcel.dataSize()) {
            StringBuilder sb = new StringBuilder(54);
            sb.append("Size read is invalid start=");
            sb.append(dataPosition);
            sb.append(" end=");
            sb.append(i);
            throw new ParseException(sb.toString(), parcel);
        }
        return i;
    }

    public static void K(Parcel parcel, int i, int i2) {
        int H = H(parcel, i);
        if (H == i2) {
            return;
        }
        String hexString = Integer.toHexString(H);
        StringBuilder sb = new StringBuilder(String.valueOf(hexString).length() + 46);
        sb.append("Expected size ");
        sb.append(i2);
        sb.append(" got ");
        sb.append(H);
        sb.append(" (0x");
        sb.append(hexString);
        sb.append(")");
        throw new ParseException(sb.toString(), parcel);
    }

    public static void L(Parcel parcel, int i, int i2, int i3) {
        if (i2 == i3) {
            return;
        }
        String hexString = Integer.toHexString(i2);
        StringBuilder sb = new StringBuilder(String.valueOf(hexString).length() + 46);
        sb.append("Expected size ");
        sb.append(i3);
        sb.append(" got ");
        sb.append(i2);
        sb.append(" (0x");
        sb.append(hexString);
        sb.append(")");
        throw new ParseException(sb.toString(), parcel);
    }

    @RecentlyNonNull
    public static BigDecimal a(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        int H = H(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (H == 0) {
            return null;
        }
        byte[] createByteArray = parcel.createByteArray();
        int readInt = parcel.readInt();
        parcel.setDataPosition(dataPosition + H);
        return new BigDecimal(new BigInteger(createByteArray), readInt);
    }

    @RecentlyNonNull
    public static BigDecimal[] b(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        int H = H(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (H == 0) {
            return null;
        }
        int readInt = parcel.readInt();
        BigDecimal[] bigDecimalArr = new BigDecimal[readInt];
        for (int i2 = 0; i2 < readInt; i2++) {
            byte[] createByteArray = parcel.createByteArray();
            bigDecimalArr[i2] = new BigDecimal(new BigInteger(createByteArray), parcel.readInt());
        }
        parcel.setDataPosition(dataPosition + H);
        return bigDecimalArr;
    }

    @RecentlyNonNull
    public static BigInteger c(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        int H = H(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (H == 0) {
            return null;
        }
        byte[] createByteArray = parcel.createByteArray();
        parcel.setDataPosition(dataPosition + H);
        return new BigInteger(createByteArray);
    }

    @RecentlyNonNull
    public static BigInteger[] d(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        int H = H(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (H == 0) {
            return null;
        }
        int readInt = parcel.readInt();
        BigInteger[] bigIntegerArr = new BigInteger[readInt];
        for (int i2 = 0; i2 < readInt; i2++) {
            bigIntegerArr[i2] = new BigInteger(parcel.createByteArray());
        }
        parcel.setDataPosition(dataPosition + H);
        return bigIntegerArr;
    }

    @RecentlyNonNull
    public static boolean[] e(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        int H = H(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (H == 0) {
            return null;
        }
        boolean[] createBooleanArray = parcel.createBooleanArray();
        parcel.setDataPosition(dataPosition + H);
        return createBooleanArray;
    }

    @RecentlyNonNull
    public static Bundle f(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        int H = H(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (H == 0) {
            return null;
        }
        Bundle readBundle = parcel.readBundle();
        parcel.setDataPosition(dataPosition + H);
        return readBundle;
    }

    @RecentlyNonNull
    public static byte[] g(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        int H = H(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (H == 0) {
            return null;
        }
        byte[] createByteArray = parcel.createByteArray();
        parcel.setDataPosition(dataPosition + H);
        return createByteArray;
    }

    @RecentlyNonNull
    public static byte[][] h(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        int H = H(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (H == 0) {
            return null;
        }
        int readInt = parcel.readInt();
        byte[][] bArr = new byte[readInt];
        for (int i2 = 0; i2 < readInt; i2++) {
            bArr[i2] = parcel.createByteArray();
        }
        parcel.setDataPosition(dataPosition + H);
        return bArr;
    }

    @RecentlyNonNull
    public static double[] i(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        int H = H(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (H == 0) {
            return null;
        }
        double[] createDoubleArray = parcel.createDoubleArray();
        parcel.setDataPosition(dataPosition + H);
        return createDoubleArray;
    }

    @RecentlyNonNull
    public static float[] j(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        int H = H(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (H == 0) {
            return null;
        }
        float[] createFloatArray = parcel.createFloatArray();
        parcel.setDataPosition(dataPosition + H);
        return createFloatArray;
    }

    @RecentlyNonNull
    public static int[] k(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        int H = H(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (H == 0) {
            return null;
        }
        int[] createIntArray = parcel.createIntArray();
        parcel.setDataPosition(dataPosition + H);
        return createIntArray;
    }

    @RecentlyNonNull
    public static long[] l(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        int H = H(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (H == 0) {
            return null;
        }
        long[] createLongArray = parcel.createLongArray();
        parcel.setDataPosition(dataPosition + H);
        return createLongArray;
    }

    @RecentlyNonNull
    public static Parcel m(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        int H = H(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (H == 0) {
            return null;
        }
        Parcel obtain = Parcel.obtain();
        obtain.appendFrom(parcel, dataPosition, H);
        parcel.setDataPosition(dataPosition + H);
        return obtain;
    }

    @RecentlyNonNull
    public static Parcel[] n(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        int H = H(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (H == 0) {
            return null;
        }
        int readInt = parcel.readInt();
        Parcel[] parcelArr = new Parcel[readInt];
        for (int i2 = 0; i2 < readInt; i2++) {
            int readInt2 = parcel.readInt();
            if (readInt2 != 0) {
                int dataPosition2 = parcel.dataPosition();
                Parcel obtain = Parcel.obtain();
                obtain.appendFrom(parcel, dataPosition2, readInt2);
                parcelArr[i2] = obtain;
                parcel.setDataPosition(dataPosition2 + readInt2);
            } else {
                parcelArr[i2] = null;
            }
        }
        parcel.setDataPosition(dataPosition + H);
        return parcelArr;
    }

    @RecentlyNonNull
    public static <T extends Parcelable> T o(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i, @RecentlyNonNull Parcelable.Creator<T> creator) {
        int H = H(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (H == 0) {
            return null;
        }
        T createFromParcel = creator.createFromParcel(parcel);
        parcel.setDataPosition(dataPosition + H);
        return createFromParcel;
    }

    @RecentlyNonNull
    public static String p(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        int H = H(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (H == 0) {
            return null;
        }
        String readString = parcel.readString();
        parcel.setDataPosition(dataPosition + H);
        return readString;
    }

    @RecentlyNonNull
    public static String[] q(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        int H = H(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (H == 0) {
            return null;
        }
        String[] createStringArray = parcel.createStringArray();
        parcel.setDataPosition(dataPosition + H);
        return createStringArray;
    }

    @RecentlyNonNull
    public static ArrayList<String> r(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        int H = H(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (H == 0) {
            return null;
        }
        ArrayList<String> createStringArrayList = parcel.createStringArrayList();
        parcel.setDataPosition(dataPosition + H);
        return createStringArrayList;
    }

    @RecentlyNonNull
    public static <T> T[] s(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i, @RecentlyNonNull Parcelable.Creator<T> creator) {
        int H = H(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (H == 0) {
            return null;
        }
        T[] tArr = (T[]) parcel.createTypedArray(creator);
        parcel.setDataPosition(dataPosition + H);
        return tArr;
    }

    @RecentlyNonNull
    public static <T> ArrayList<T> t(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i, @RecentlyNonNull Parcelable.Creator<T> creator) {
        int H = H(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (H == 0) {
            return null;
        }
        ArrayList<T> createTypedArrayList = parcel.createTypedArrayList(creator);
        parcel.setDataPosition(dataPosition + H);
        return createTypedArrayList;
    }

    public static void u(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        if (parcel.dataPosition() == i) {
            return;
        }
        StringBuilder sb = new StringBuilder(37);
        sb.append("Overread allowed size end=");
        sb.append(i);
        throw new ParseException(sb.toString(), parcel);
    }

    @RecentlyNonNull
    public static int v(@RecentlyNonNull int i) {
        return i & 65535;
    }

    @RecentlyNonNull
    public static boolean w(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        K(parcel, i, 4);
        return parcel.readInt() != 0;
    }

    @RecentlyNonNull
    public static Boolean x(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        int H = H(parcel, i);
        if (H == 0) {
            return null;
        }
        L(parcel, i, H, 4);
        return Boolean.valueOf(parcel.readInt() != 0);
    }

    @RecentlyNonNull
    public static double y(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        K(parcel, i, 8);
        return parcel.readDouble();
    }

    @RecentlyNonNull
    public static Double z(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        int H = H(parcel, i);
        if (H == 0) {
            return null;
        }
        L(parcel, i, H, 8);
        return Double.valueOf(parcel.readDouble());
    }
}
