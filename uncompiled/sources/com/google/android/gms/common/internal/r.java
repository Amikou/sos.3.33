package com.google.android.gms.common.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.common.zzj;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* loaded from: classes.dex */
public final class r extends a75 implements q {
    public r(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.common.internal.IGoogleCertificatesApi");
    }

    @Override // com.google.android.gms.common.internal.q
    public final boolean p0(zzj zzjVar, lm1 lm1Var) throws RemoteException {
        Parcel b = b();
        ad5.c(b, zzjVar);
        ad5.b(b, lm1Var);
        Parcel F1 = F1(5, b);
        boolean e = ad5.e(F1);
        F1.recycle();
        return e;
    }
}
