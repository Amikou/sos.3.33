package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.content.Context;
import android.os.IInterface;
import android.os.Looper;
import androidx.annotation.RecentlyNonNull;
import androidx.annotation.RecentlyNullable;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.internal.b;
import java.util.Collections;
import java.util.Set;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public abstract class c<T extends IInterface> extends b<T> implements a.f, c25 {
    public final Set<Scope> C;
    public final Account D;

    public c(@RecentlyNonNull Context context, @RecentlyNonNull Looper looper, @RecentlyNonNull int i, @RecentlyNonNull kz kzVar, @RecentlyNonNull u50 u50Var, @RecentlyNonNull jm2 jm2Var) {
        this(context, looper, rg1.b(context), dh1.q(), i, kzVar, (u50) zt2.j(u50Var), (jm2) zt2.j(jm2Var));
    }

    public static b.a p0(u50 u50Var) {
        if (u50Var == null) {
            return null;
        }
        return new i(u50Var);
    }

    public static b.InterfaceC0111b q0(jm2 jm2Var) {
        if (jm2Var == null) {
            return null;
        }
        return new j(jm2Var);
    }

    @Override // com.google.android.gms.common.internal.b
    @RecentlyNullable
    public final Account A() {
        return this.D;
    }

    @Override // com.google.android.gms.common.internal.b
    @RecentlyNonNull
    public final Set<Scope> F() {
        return this.C;
    }

    @Override // com.google.android.gms.common.api.a.f
    public Set<Scope> g() {
        return u() ? this.C : Collections.emptySet();
    }

    public Set<Scope> o0(@RecentlyNonNull Set<Scope> set) {
        return set;
    }

    public final Set<Scope> r0(Set<Scope> set) {
        Set<Scope> o0 = o0(set);
        for (Scope scope : o0) {
            if (!set.contains(scope)) {
                throw new IllegalStateException("Expanding scopes is not permitted, use implied scopes instead");
            }
        }
        return o0;
    }

    @Deprecated
    public c(@RecentlyNonNull Context context, @RecentlyNonNull Looper looper, @RecentlyNonNull int i, @RecentlyNonNull kz kzVar, @RecentlyNonNull GoogleApiClient.b bVar, @RecentlyNonNull GoogleApiClient.c cVar) {
        this(context, looper, i, kzVar, (u50) bVar, (jm2) cVar);
    }

    public c(Context context, Looper looper, rg1 rg1Var, dh1 dh1Var, int i, kz kzVar, u50 u50Var, jm2 jm2Var) {
        super(context, looper, rg1Var, dh1Var, i, p0(u50Var), q0(jm2Var), kzVar.h());
        this.D = kzVar.a();
        this.C = r0(kzVar.c());
    }
}
