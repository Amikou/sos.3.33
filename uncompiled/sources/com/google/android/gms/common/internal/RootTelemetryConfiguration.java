package com.google.android.gms.common.internal;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.RecentlyNonNull;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* loaded from: classes.dex */
public class RootTelemetryConfiguration extends AbstractSafeParcelable {
    @RecentlyNonNull
    public static final Parcelable.Creator<RootTelemetryConfiguration> CREATOR = new d56();
    public final int a;
    public final boolean f0;
    public final boolean g0;
    public final int h0;
    public final int i0;

    public RootTelemetryConfiguration(@RecentlyNonNull int i, @RecentlyNonNull boolean z, @RecentlyNonNull boolean z2, @RecentlyNonNull int i2, @RecentlyNonNull int i3) {
        this.a = i;
        this.f0 = z;
        this.g0 = z2;
        this.h0 = i2;
        this.i0 = i3;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        int a = yb3.a(parcel);
        yb3.m(parcel, 1, this.a);
        yb3.c(parcel, 2, this.f0);
        yb3.c(parcel, 3, this.g0);
        yb3.m(parcel, 4, this.h0);
        yb3.m(parcel, 5, this.i0);
        yb3.b(parcel, a);
    }
}
