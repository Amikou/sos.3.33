package com.google.android.gms.common.internal;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.b;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public final class j implements b.InterfaceC0111b {
    public final /* synthetic */ jm2 a;

    public j(jm2 jm2Var) {
        this.a = jm2Var;
    }

    @Override // com.google.android.gms.common.internal.b.InterfaceC0111b
    public final void onConnectionFailed(ConnectionResult connectionResult) {
        this.a.onConnectionFailed(connectionResult);
    }
}
