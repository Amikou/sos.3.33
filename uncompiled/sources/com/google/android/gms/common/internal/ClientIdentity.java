package com.google.android.gms.common.internal;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.RecentlyNonNull;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public class ClientIdentity extends AbstractSafeParcelable {
    @RecentlyNonNull
    public static final Parcelable.Creator<ClientIdentity> CREATOR = new jz4();
    public final int a;
    public final String f0;

    public ClientIdentity(@RecentlyNonNull int i, String str) {
        this.a = i;
        this.f0 = str;
    }

    @RecentlyNonNull
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof ClientIdentity) {
            ClientIdentity clientIdentity = (ClientIdentity) obj;
            return clientIdentity.a == this.a && pl2.a(clientIdentity.f0, this.f0);
        }
        return false;
    }

    @RecentlyNonNull
    public int hashCode() {
        return this.a;
    }

    @RecentlyNonNull
    public String toString() {
        int i = this.a;
        String str = this.f0;
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 12);
        sb.append(i);
        sb.append(":");
        sb.append(str);
        return sb.toString();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        int a = yb3.a(parcel);
        yb3.m(parcel, 1, this.a);
        yb3.s(parcel, 2, this.f0, false);
        yb3.b(parcel, a);
    }
}
