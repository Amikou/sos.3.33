package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.RecentlyNonNull;
import com.google.android.gms.common.Feature;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.d;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* loaded from: classes.dex */
public class GetServiceRequest extends AbstractSafeParcelable {
    @RecentlyNonNull
    public static final Parcelable.Creator<GetServiceRequest> CREATOR = new hf5();
    public final int a;
    public final int f0;
    public int g0;
    public String h0;
    public IBinder i0;
    public Scope[] j0;
    public Bundle k0;
    public Account l0;
    public Feature[] m0;
    public Feature[] n0;
    public boolean o0;
    public int p0;
    public boolean q0;

    public GetServiceRequest(@RecentlyNonNull int i) {
        this.a = 5;
        this.g0 = eh1.a;
        this.f0 = i;
        this.o0 = true;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        int a = yb3.a(parcel);
        yb3.m(parcel, 1, this.a);
        yb3.m(parcel, 2, this.f0);
        yb3.m(parcel, 3, this.g0);
        yb3.s(parcel, 4, this.h0, false);
        yb3.l(parcel, 5, this.i0, false);
        yb3.v(parcel, 6, this.j0, i, false);
        yb3.e(parcel, 7, this.k0, false);
        yb3.r(parcel, 8, this.l0, i, false);
        yb3.v(parcel, 10, this.m0, i, false);
        yb3.v(parcel, 11, this.n0, i, false);
        yb3.c(parcel, 12, this.o0);
        yb3.m(parcel, 13, this.p0);
        yb3.c(parcel, 14, this.q0);
        yb3.b(parcel, a);
    }

    public GetServiceRequest(int i, int i2, int i3, String str, IBinder iBinder, Scope[] scopeArr, Bundle bundle, Account account, Feature[] featureArr, Feature[] featureArr2, boolean z, int i4, boolean z2) {
        this.a = i;
        this.f0 = i2;
        this.g0 = i3;
        if ("com.google.android.gms".equals(str)) {
            this.h0 = "com.google.android.gms";
        } else {
            this.h0 = str;
        }
        if (i < 2) {
            this.l0 = iBinder != null ? a.G1(d.a.F1(iBinder)) : null;
        } else {
            this.i0 = iBinder;
            this.l0 = account;
        }
        this.j0 = scopeArr;
        this.k0 = bundle;
        this.m0 = featureArr;
        this.n0 = featureArr2;
        this.o0 = z;
        this.p0 = i4;
        this.q0 = z2;
    }
}
