package com.google.android.gms.common.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.d;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public final class zas extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zas> CREATOR = new z25();
    public final int a;
    public IBinder f0;
    public ConnectionResult g0;
    public boolean h0;
    public boolean i0;

    public zas(int i, IBinder iBinder, ConnectionResult connectionResult, boolean z, boolean z2) {
        this.a = i;
        this.f0 = iBinder;
        this.g0 = connectionResult;
        this.h0 = z;
        this.i0 = z2;
    }

    public final d I1() {
        IBinder iBinder = this.f0;
        if (iBinder == null) {
            return null;
        }
        return d.a.F1(iBinder);
    }

    public final ConnectionResult J1() {
        return this.g0;
    }

    public final boolean K1() {
        return this.h0;
    }

    public final boolean L1() {
        return this.i0;
    }

    public final boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (obj instanceof zas) {
            zas zasVar = (zas) obj;
            return this.g0.equals(zasVar.g0) && pl2.a(I1(), zasVar.I1());
        }
        return false;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int a = yb3.a(parcel);
        yb3.m(parcel, 1, this.a);
        yb3.l(parcel, 2, this.f0, false);
        yb3.r(parcel, 3, this.g0, i, false);
        yb3.c(parcel, 4, this.h0);
        yb3.c(parcel, 5, this.i0);
        yb3.b(parcel, a);
    }
}
