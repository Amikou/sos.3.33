package com.google.android.gms.common.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import defpackage.lm1;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public final class k extends com.google.android.gms.internal.base.b implements l {
    public k(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.common.internal.ISignInButtonCreator");
    }

    @Override // com.google.android.gms.common.internal.l
    public final lm1 l(lm1 lm1Var, zau zauVar) throws RemoteException {
        Parcel b = b();
        o15.b(b, lm1Var);
        o15.c(b, zauVar);
        Parcel F1 = F1(2, b);
        lm1 F12 = lm1.a.F1(F1.readStrongBinder());
        F1.recycle();
        return F12;
    }
}
