package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public final class zar extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zar> CREATOR = new w25();
    public final int a;
    public final Account f0;
    public final int g0;
    public final GoogleSignInAccount h0;

    public zar(int i, Account account, int i2, GoogleSignInAccount googleSignInAccount) {
        this.a = i;
        this.f0 = account;
        this.g0 = i2;
        this.h0 = googleSignInAccount;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int a = yb3.a(parcel);
        yb3.m(parcel, 1, this.a);
        yb3.r(parcel, 2, this.f0, i, false);
        yb3.m(parcel, 3, this.g0);
        yb3.r(parcel, 4, this.h0, i, false);
        yb3.b(parcel, a);
    }

    public zar(Account account, int i, GoogleSignInAccount googleSignInAccount) {
        this(2, account, i, googleSignInAccount);
    }
}
