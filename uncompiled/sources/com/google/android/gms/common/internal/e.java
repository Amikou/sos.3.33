package com.google.android.gms.common.internal;

import android.os.IInterface;
import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* loaded from: classes.dex */
public interface e extends IInterface {
    void cancel() throws RemoteException;
}
