package com.google.android.gms.common.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import defpackage.lm1;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* loaded from: classes.dex */
public final class o extends a75 implements n {
    public o(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.common.internal.ICertData");
    }

    @Override // com.google.android.gms.common.internal.n
    public final lm1 zzb() throws RemoteException {
        Parcel F1 = F1(1, b());
        lm1 F12 = lm1.a.F1(F1.readStrongBinder());
        F1.recycle();
        return F12;
    }

    @Override // com.google.android.gms.common.internal.n
    public final int zzc() throws RemoteException {
        Parcel F1 = F1(2, b());
        int readInt = F1.readInt();
        F1.recycle();
        return readInt;
    }
}
