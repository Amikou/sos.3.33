package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import androidx.annotation.RecentlyNonNull;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* loaded from: classes.dex */
public interface d extends IInterface {

    /* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
    /* loaded from: classes.dex */
    public static abstract class a extends j35 implements d {

        /* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
        /* renamed from: com.google.android.gms.common.internal.d$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public static class C0112a extends a75 implements d {
            public C0112a(IBinder iBinder) {
                super(iBinder, "com.google.android.gms.common.internal.IAccountAccessor");
            }

            @Override // com.google.android.gms.common.internal.d
            public final Account zza() throws RemoteException {
                Parcel F1 = F1(2, b());
                Account account = (Account) ad5.a(F1, Account.CREATOR);
                F1.recycle();
                return account;
            }
        }

        @RecentlyNonNull
        public static d F1(@RecentlyNonNull IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.IAccountAccessor");
            if (queryLocalInterface instanceof d) {
                return (d) queryLocalInterface;
            }
            return new C0112a(iBinder);
        }
    }

    @RecentlyNonNull
    Account zza() throws RemoteException;
}
