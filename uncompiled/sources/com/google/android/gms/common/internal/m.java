package com.google.android.gms.common.internal;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.view.View;
import com.google.android.gms.dynamic.RemoteCreator;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public final class m extends RemoteCreator<l> {
    public static final m c = new m();

    public m() {
        super("com.google.android.gms.common.ui.SignInButtonCreatorImpl");
    }

    public static View c(Context context, int i, int i2) throws RemoteCreator.RemoteCreatorException {
        return c.d(context, i, i2);
    }

    @Override // com.google.android.gms.dynamic.RemoteCreator
    public final /* synthetic */ l a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.ISignInButtonCreator");
        if (queryLocalInterface instanceof l) {
            return (l) queryLocalInterface;
        }
        return new k(iBinder);
    }

    public final View d(Context context, int i, int i2) throws RemoteCreator.RemoteCreatorException {
        try {
            zau zauVar = new zau(i, i2, null);
            return (View) nl2.G1(b(context).l(nl2.H1(context), zauVar));
        } catch (Exception e) {
            StringBuilder sb = new StringBuilder(64);
            sb.append("Could not get button with size ");
            sb.append(i);
            sb.append(" and color ");
            sb.append(i2);
            throw new RemoteCreator.RemoteCreatorException(sb.toString(), e);
        }
    }
}
