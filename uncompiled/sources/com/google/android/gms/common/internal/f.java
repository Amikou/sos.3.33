package com.google.android.gms.common.internal;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import androidx.annotation.RecentlyNonNull;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* loaded from: classes.dex */
public interface f extends IInterface {

    /* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
    /* loaded from: classes.dex */
    public static abstract class a extends j35 implements f {
        public a() {
            super("com.google.android.gms.common.internal.IGmsCallbacks");
        }

        @Override // defpackage.j35
        public final boolean b(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            if (i == 1) {
                k0(parcel.readInt(), parcel.readStrongBinder(), (Bundle) ad5.a(parcel, Bundle.CREATOR));
            } else if (i == 2) {
                f0(parcel.readInt(), (Bundle) ad5.a(parcel, Bundle.CREATOR));
            } else if (i != 3) {
                return false;
            } else {
                U0(parcel.readInt(), parcel.readStrongBinder(), (zzc) ad5.a(parcel, zzc.CREATOR));
            }
            parcel2.writeNoException();
            return true;
        }
    }

    void U0(int i, IBinder iBinder, zzc zzcVar) throws RemoteException;

    void f0(@RecentlyNonNull int i, @RecentlyNonNull Bundle bundle) throws RemoteException;

    void k0(@RecentlyNonNull int i, @RecentlyNonNull IBinder iBinder, @RecentlyNonNull Bundle bundle) throws RemoteException;
}
