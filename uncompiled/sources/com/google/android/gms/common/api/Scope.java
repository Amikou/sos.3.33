package com.google.android.gms.common.api;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.RecentlyNonNull;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* loaded from: classes.dex */
public final class Scope extends AbstractSafeParcelable implements ReflectedParcelable {
    @RecentlyNonNull
    public static final Parcelable.Creator<Scope> CREATOR = new u35();
    public final int a;
    public final String f0;

    public Scope(int i, String str) {
        zt2.g(str, "scopeUri must not be null or empty");
        this.a = i;
        this.f0 = str;
    }

    @RecentlyNonNull
    public final String I1() {
        return this.f0;
    }

    @RecentlyNonNull
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof Scope) {
            return this.f0.equals(((Scope) obj).f0);
        }
        return false;
    }

    @RecentlyNonNull
    public final int hashCode() {
        return this.f0.hashCode();
    }

    @RecentlyNonNull
    public final String toString() {
        return this.f0;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        int a = yb3.a(parcel);
        yb3.m(parcel, 1, this.a);
        yb3.s(parcel, 2, I1(), false);
        yb3.b(parcel, a);
    }

    public Scope(@RecentlyNonNull String str) {
        this(1, str);
    }
}
