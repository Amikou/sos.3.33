package com.google.android.gms.common.api.internal;

import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import android.util.Pair;
import androidx.annotation.RecentlyNonNull;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import defpackage.gq2;
import defpackage.l83;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
@KeepName
/* loaded from: classes.dex */
public abstract class BasePendingResult<R extends l83> extends gq2<R> {
    public static final ThreadLocal<Boolean> o = new o25();
    public final Object a;
    public final a<R> b;
    public final WeakReference<GoogleApiClient> c;
    public final CountDownLatch d;
    public final ArrayList<gq2.a> e;
    public m83<? super R> f;
    public final AtomicReference<m15> g;
    public R h;
    public Status i;
    public volatile boolean j;
    public boolean k;
    public boolean l;
    public com.google.android.gms.common.internal.e m;
    @KeepName
    public b mResultGuardian;
    public boolean n;

    /* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
    /* loaded from: classes.dex */
    public static class a<R extends l83> extends q25 {
        public a(@RecentlyNonNull Looper looper) {
            super(looper);
        }

        public final void a(@RecentlyNonNull m83<? super R> m83Var, @RecentlyNonNull R r) {
            sendMessage(obtainMessage(1, new Pair((m83) zt2.j(BasePendingResult.m(m83Var)), r)));
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // android.os.Handler
        public void handleMessage(@RecentlyNonNull Message message) {
            int i = message.what;
            if (i != 1) {
                if (i != 2) {
                    StringBuilder sb = new StringBuilder(45);
                    sb.append("Don't know how to handle message: ");
                    sb.append(i);
                    Log.wtf("BasePendingResult", sb.toString(), new Exception());
                    return;
                }
                ((BasePendingResult) message.obj).e(Status.l0);
                return;
            }
            Pair pair = (Pair) message.obj;
            m83 m83Var = (m83) pair.first;
            l83 l83Var = (l83) pair.second;
            try {
                m83Var.a(l83Var);
            } catch (RuntimeException e) {
                BasePendingResult.j(l83Var);
                throw e;
            }
        }
    }

    /* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
    /* loaded from: classes.dex */
    public final class b {
        public b() {
        }

        public final void finalize() throws Throwable {
            BasePendingResult.j(BasePendingResult.this.h);
            super.finalize();
        }

        public /* synthetic */ b(BasePendingResult basePendingResult, o25 o25Var) {
            this();
        }
    }

    @Deprecated
    public BasePendingResult() {
        this.a = new Object();
        this.d = new CountDownLatch(1);
        this.e = new ArrayList<>();
        this.g = new AtomicReference<>();
        this.n = false;
        this.b = new a<>(Looper.getMainLooper());
        this.c = new WeakReference<>(null);
    }

    public static void j(l83 l83Var) {
        if (l83Var instanceof k63) {
            try {
                ((k63) l83Var).a();
            } catch (RuntimeException unused) {
                String valueOf = String.valueOf(l83Var);
                StringBuilder sb = new StringBuilder(valueOf.length() + 18);
                sb.append("Unable to release ");
                sb.append(valueOf);
            }
        }
    }

    public static <R extends l83> m83<R> m(m83<R> m83Var) {
        return m83Var;
    }

    @Override // defpackage.gq2
    public final void a(@RecentlyNonNull gq2.a aVar) {
        zt2.b(aVar != null, "Callback cannot be null.");
        synchronized (this.a) {
            if (f()) {
                aVar.a(this.i);
            } else {
                this.e.add(aVar);
            }
        }
    }

    @Override // defpackage.gq2
    public void b() {
        synchronized (this.a) {
            if (!this.k && !this.j) {
                com.google.android.gms.common.internal.e eVar = this.m;
                if (eVar != null) {
                    try {
                        eVar.cancel();
                    } catch (RemoteException unused) {
                    }
                }
                j(this.h);
                this.k = true;
                o(d(Status.m0));
            }
        }
    }

    @Override // defpackage.gq2
    @RecentlyNonNull
    public boolean c() {
        boolean z;
        synchronized (this.a) {
            z = this.k;
        }
        return z;
    }

    public abstract R d(@RecentlyNonNull Status status);

    @Deprecated
    public final void e(@RecentlyNonNull Status status) {
        synchronized (this.a) {
            if (!f()) {
                g(d(status));
                this.l = true;
            }
        }
    }

    @RecentlyNonNull
    public final boolean f() {
        return this.d.getCount() == 0;
    }

    public final void g(@RecentlyNonNull R r) {
        synchronized (this.a) {
            if (!this.l && !this.k) {
                f();
                boolean z = true;
                zt2.n(!f(), "Results have already been set");
                if (this.j) {
                    z = false;
                }
                zt2.n(z, "Result has already been consumed");
                o(r);
                return;
            }
            j(r);
        }
    }

    public final void k(m15 m15Var) {
        this.g.set(m15Var);
    }

    @RecentlyNonNull
    public final boolean l() {
        boolean c;
        synchronized (this.a) {
            if (this.c.get() == null || !this.n) {
                b();
            }
            c = c();
        }
        return c;
    }

    public final void n() {
        this.n = this.n || o.get().booleanValue();
    }

    public final void o(R r) {
        this.h = r;
        this.i = r.e();
        this.m = null;
        this.d.countDown();
        if (this.k) {
            this.f = null;
        } else {
            m83<? super R> m83Var = this.f;
            if (m83Var == null) {
                if (this.h instanceof k63) {
                    this.mResultGuardian = new b(this, null);
                }
            } else {
                this.b.removeMessages(2);
                this.b.a(m83Var, p());
            }
        }
        ArrayList<gq2.a> arrayList = this.e;
        int size = arrayList.size();
        int i = 0;
        while (i < size) {
            gq2.a aVar = arrayList.get(i);
            i++;
            aVar.a(this.i);
        }
        this.e.clear();
    }

    public final R p() {
        R r;
        synchronized (this.a) {
            zt2.n(!this.j, "Result has already been consumed.");
            zt2.n(f(), "Result is not ready.");
            r = this.h;
            this.h = null;
            this.f = null;
            this.j = true;
        }
        m15 andSet = this.g.getAndSet(null);
        if (andSet != null) {
            andSet.a(this);
        }
        return (R) zt2.j(r);
    }

    public BasePendingResult(GoogleApiClient googleApiClient) {
        this.a = new Object();
        this.d = new CountDownLatch(1);
        this.e = new ArrayList<>();
        this.g = new AtomicReference<>();
        this.n = false;
        this.b = new a<>(googleApiClient != null ? googleApiClient.g() : Looper.getMainLooper());
        this.c = new WeakReference<>(googleApiClient);
    }
}
