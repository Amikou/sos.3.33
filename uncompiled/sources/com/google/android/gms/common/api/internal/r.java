package com.google.android.gms.common.api.internal;

import android.os.RemoteException;
import com.google.android.gms.common.Feature;
import com.google.android.gms.common.api.internal.c;
import com.google.android.gms.common.api.internal.d;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public final class r extends q<Boolean> {
    public final d.a<?> c;

    public r(d.a<?> aVar, n34<Boolean> n34Var) {
        super(4, n34Var);
        this.c = aVar;
    }

    @Override // com.google.android.gms.common.api.internal.o
    public final /* bridge */ /* synthetic */ void d(b35 b35Var, boolean z) {
    }

    @Override // com.google.android.gms.common.api.internal.g
    public final Feature[] g(c.a<?> aVar) {
        if (aVar.y().get(this.c) == null) {
            return null;
        }
        throw null;
    }

    @Override // com.google.android.gms.common.api.internal.g
    public final boolean h(c.a<?> aVar) {
        if (aVar.y().get(this.c) == null) {
            return false;
        }
        throw null;
    }

    @Override // com.google.android.gms.common.api.internal.q
    public final void i(c.a<?> aVar) throws RemoteException {
        if (aVar.y().remove(this.c) == null) {
            this.b.e(Boolean.FALSE);
        } else {
            aVar.r();
            throw null;
        }
    }
}
