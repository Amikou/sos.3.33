package com.google.android.gms.common.api.internal;

import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import androidx.annotation.RecentlyNonNull;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Feature;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.UnsupportedApiCallException;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.internal.d;
import com.google.android.gms.common.internal.b;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public class c implements Handler.Callback {
    @RecentlyNonNull
    public static final Status s0 = new Status(4, "Sign-out occurred while this API call was in progress.");
    public static final Status t0 = new Status(4, "The user must be signed in to make this API call.");
    public static final Object u0 = new Object();
    public static c v0;
    public final Context h0;
    public final dh1 i0;
    public final y15 j0;
    public d35 n0;
    public final Handler q0;
    public volatile boolean r0;
    public long a = 5000;
    public long f0 = 120000;
    public long g0 = 10000;
    public final AtomicInteger k0 = new AtomicInteger(1);
    public final AtomicInteger l0 = new AtomicInteger(0);
    public final Map<ze<?>, a<?>> m0 = new ConcurrentHashMap(5, 0.75f, 1);
    public final Set<ze<?>> o0 = new uh();
    public final Set<ze<?>> p0 = new uh();

    /* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
    /* loaded from: classes.dex */
    public class a<O extends a.d> implements GoogleApiClient.b, GoogleApiClient.c, u25 {
        public final a.f b;
        public final ze<O> c;
        public final b35 d;
        public final int g;
        public final e15 h;
        public boolean i;
        public final Queue<o> a = new LinkedList();
        public final Set<b25> e = new HashSet();
        public final Map<d.a<?>, x05> f = new HashMap();
        public final List<b> j = new ArrayList();
        public ConnectionResult k = null;

        public a(com.google.android.gms.common.api.b<O> bVar) {
            a.f i = bVar.i(c.this.q0.getLooper(), this);
            this.b = i;
            this.c = bVar.b();
            this.d = new b35();
            this.g = bVar.h();
            if (i.u()) {
                this.h = bVar.l(c.this.h0, c.this.q0);
            } else {
                this.h = null;
            }
        }

        public final void A(o oVar) {
            oVar.d(this.d, J());
            try {
                oVar.f(this);
            } catch (DeadObjectException unused) {
                onConnectionSuspended(1);
                this.b.i("DeadObjectException thrown while running ApiCallRunner.");
            } catch (Throwable th) {
                throw new IllegalStateException(String.format("Error in GoogleApi implementation for client %s.", this.b.getClass().getName()), th);
            }
        }

        public final Status B(ConnectionResult connectionResult) {
            return c.i(this.c, connectionResult);
        }

        public final void C() {
            zt2.c(c.this.q0);
            this.k = null;
        }

        public final ConnectionResult D() {
            zt2.c(c.this.q0);
            return this.k;
        }

        public final void E() {
            zt2.c(c.this.q0);
            if (this.i) {
                H();
            }
        }

        public final void F() {
            Status status;
            zt2.c(c.this.q0);
            if (this.i) {
                N();
                if (c.this.i0.i(c.this.h0) == 18) {
                    status = new Status(21, "Connection timed out waiting for Google Play services update to complete.");
                } else {
                    status = new Status(22, "API failed to connect while resuming due to an unknown error.");
                }
                h(status);
                this.b.i("Timing out connection while resuming.");
            }
        }

        public final boolean G() {
            return q(true);
        }

        public final void H() {
            zt2.c(c.this.q0);
            if (this.b.c() || this.b.k()) {
                return;
            }
            try {
                int a = c.this.j0.a(c.this.h0, this.b);
                if (a != 0) {
                    ConnectionResult connectionResult = new ConnectionResult(a, null);
                    String name = this.b.getClass().getName();
                    String valueOf = String.valueOf(connectionResult);
                    StringBuilder sb = new StringBuilder(name.length() + 35 + valueOf.length());
                    sb.append("The service for ");
                    sb.append(name);
                    sb.append(" is not available: ");
                    sb.append(valueOf);
                    onConnectionFailed(connectionResult);
                    return;
                }
                C0110c c0110c = new C0110c(this.b, this.c);
                if (this.b.u()) {
                    ((e15) zt2.j(this.h)).J1(c0110c);
                }
                try {
                    this.b.m(c0110c);
                } catch (SecurityException e) {
                    g(new ConnectionResult(10), e);
                }
            } catch (IllegalStateException e2) {
                g(new ConnectionResult(10), e2);
            }
        }

        public final boolean I() {
            return this.b.c();
        }

        public final boolean J() {
            return this.b.u();
        }

        public final int K() {
            return this.g;
        }

        public final void L() {
            C();
            z(ConnectionResult.i0);
            N();
            Iterator<x05> it = this.f.values().iterator();
            if (!it.hasNext()) {
                M();
                O();
                return;
            }
            f<a.b, ?> fVar = it.next().a;
            throw null;
        }

        public final void M() {
            ArrayList arrayList = new ArrayList(this.a);
            int size = arrayList.size();
            int i = 0;
            while (i < size) {
                Object obj = arrayList.get(i);
                i++;
                o oVar = (o) obj;
                if (!this.b.c()) {
                    return;
                }
                if (w(oVar)) {
                    this.a.remove(oVar);
                }
            }
        }

        public final void N() {
            if (this.i) {
                c.this.q0.removeMessages(11, this.c);
                c.this.q0.removeMessages(9, this.c);
                this.i = false;
            }
        }

        public final void O() {
            c.this.q0.removeMessages(12, this.c);
            c.this.q0.sendMessageDelayed(c.this.q0.obtainMessage(12, this.c), c.this.g0);
        }

        @Override // defpackage.u25
        public final void b(ConnectionResult connectionResult, com.google.android.gms.common.api.a<?> aVar, boolean z) {
            if (Looper.myLooper() != c.this.q0.getLooper()) {
                c.this.q0.post(new l(this, connectionResult));
            } else {
                onConnectionFailed(connectionResult);
            }
        }

        public final Feature c(Feature[] featureArr) {
            if (featureArr != null && featureArr.length != 0) {
                Feature[] r = this.b.r();
                if (r == null) {
                    r = new Feature[0];
                }
                rh rhVar = new rh(r.length);
                for (Feature feature : r) {
                    rhVar.put(feature.I1(), Long.valueOf(feature.J1()));
                }
                for (Feature feature2 : featureArr) {
                    Long l = (Long) rhVar.get(feature2.I1());
                    if (l == null || l.longValue() < feature2.J1()) {
                        return feature2;
                    }
                }
            }
            return null;
        }

        public final void d() {
            zt2.c(c.this.q0);
            h(c.s0);
            this.d.f();
            for (d.a aVar : (d.a[]) this.f.keySet().toArray(new d.a[0])) {
                n(new r(aVar, new n34()));
            }
            z(new ConnectionResult(4));
            if (this.b.c()) {
                this.b.n(new k(this));
            }
        }

        public final void e(int i) {
            C();
            this.i = true;
            this.d.b(i, this.b.s());
            c.this.q0.sendMessageDelayed(Message.obtain(c.this.q0, 9, this.c), c.this.a);
            c.this.q0.sendMessageDelayed(Message.obtain(c.this.q0, 11, this.c), c.this.f0);
            c.this.j0.b();
            for (x05 x05Var : this.f.values()) {
                x05Var.b.run();
            }
        }

        public final void f(ConnectionResult connectionResult) {
            zt2.c(c.this.q0);
            a.f fVar = this.b;
            String name = fVar.getClass().getName();
            String valueOf = String.valueOf(connectionResult);
            StringBuilder sb = new StringBuilder(name.length() + 25 + valueOf.length());
            sb.append("onSignInFailed for ");
            sb.append(name);
            sb.append(" with ");
            sb.append(valueOf);
            fVar.i(sb.toString());
            onConnectionFailed(connectionResult);
        }

        public final void g(ConnectionResult connectionResult, Exception exc) {
            zt2.c(c.this.q0);
            e15 e15Var = this.h;
            if (e15Var != null) {
                e15Var.H1();
            }
            C();
            c.this.j0.b();
            z(connectionResult);
            if (connectionResult.I1() == 4) {
                h(c.t0);
            } else if (this.a.isEmpty()) {
                this.k = connectionResult;
            } else if (exc != null) {
                zt2.c(c.this.q0);
                i(null, exc, false);
            } else if (!c.this.r0) {
                h(B(connectionResult));
            } else {
                i(B(connectionResult), null, true);
                if (this.a.isEmpty() || v(connectionResult) || c.this.f(connectionResult, this.g)) {
                    return;
                }
                if (connectionResult.I1() == 18) {
                    this.i = true;
                }
                if (this.i) {
                    c.this.q0.sendMessageDelayed(Message.obtain(c.this.q0, 9, this.c), c.this.a);
                } else {
                    h(B(connectionResult));
                }
            }
        }

        public final void h(Status status) {
            zt2.c(c.this.q0);
            i(status, null, false);
        }

        public final void i(Status status, Exception exc, boolean z) {
            zt2.c(c.this.q0);
            if ((status == null) != (exc == null)) {
                Iterator<o> it = this.a.iterator();
                while (it.hasNext()) {
                    o next = it.next();
                    if (!z || next.a == 2) {
                        if (status != null) {
                            next.b(status);
                        } else {
                            next.c(exc);
                        }
                        it.remove();
                    }
                }
                return;
            }
            throw new IllegalArgumentException("Status XOR exception should be null");
        }

        public final void m(b bVar) {
            if (this.j.contains(bVar) && !this.i) {
                if (!this.b.c()) {
                    H();
                } else {
                    M();
                }
            }
        }

        public final void n(o oVar) {
            zt2.c(c.this.q0);
            if (this.b.c()) {
                if (w(oVar)) {
                    O();
                    return;
                } else {
                    this.a.add(oVar);
                    return;
                }
            }
            this.a.add(oVar);
            ConnectionResult connectionResult = this.k;
            if (connectionResult != null && connectionResult.L1()) {
                onConnectionFailed(this.k);
            } else {
                H();
            }
        }

        public final void o(b25 b25Var) {
            zt2.c(c.this.q0);
            this.e.add(b25Var);
        }

        @Override // defpackage.u50
        public final void onConnected(Bundle bundle) {
            if (Looper.myLooper() != c.this.q0.getLooper()) {
                c.this.q0.post(new j(this));
            } else {
                L();
            }
        }

        @Override // defpackage.jm2
        public final void onConnectionFailed(ConnectionResult connectionResult) {
            g(connectionResult, null);
        }

        @Override // defpackage.u50
        public final void onConnectionSuspended(int i) {
            if (Looper.myLooper() != c.this.q0.getLooper()) {
                c.this.q0.post(new i(this, i));
            } else {
                e(i);
            }
        }

        public final boolean q(boolean z) {
            zt2.c(c.this.q0);
            if (this.b.c() && this.f.size() == 0) {
                if (!this.d.e()) {
                    this.b.i("Timing out service connection.");
                    return true;
                }
                if (z) {
                    O();
                }
                return false;
            }
            return false;
        }

        public final a.f r() {
            return this.b;
        }

        public final void u(b bVar) {
            Feature[] g;
            if (this.j.remove(bVar)) {
                c.this.q0.removeMessages(15, bVar);
                c.this.q0.removeMessages(16, bVar);
                Feature feature = bVar.b;
                ArrayList arrayList = new ArrayList(this.a.size());
                for (o oVar : this.a) {
                    if ((oVar instanceof g) && (g = ((g) oVar).g(this)) != null && vh.b(g, feature)) {
                        arrayList.add(oVar);
                    }
                }
                int size = arrayList.size();
                int i = 0;
                while (i < size) {
                    Object obj = arrayList.get(i);
                    i++;
                    o oVar2 = (o) obj;
                    this.a.remove(oVar2);
                    oVar2.c(new UnsupportedApiCallException(feature));
                }
            }
        }

        public final boolean v(ConnectionResult connectionResult) {
            synchronized (c.u0) {
                d35 unused = c.this.n0;
            }
            return false;
        }

        public final boolean w(o oVar) {
            if (!(oVar instanceof g)) {
                A(oVar);
                return true;
            }
            g gVar = (g) oVar;
            Feature c = c(gVar.g(this));
            if (c == null) {
                A(oVar);
                return true;
            }
            String name = this.b.getClass().getName();
            String I1 = c.I1();
            long J1 = c.J1();
            StringBuilder sb = new StringBuilder(name.length() + 77 + String.valueOf(I1).length());
            sb.append(name);
            sb.append(" could not execute call because it requires feature (");
            sb.append(I1);
            sb.append(", ");
            sb.append(J1);
            sb.append(").");
            if (c.this.r0 && gVar.h(this)) {
                b bVar = new b(this.c, c, null);
                int indexOf = this.j.indexOf(bVar);
                if (indexOf >= 0) {
                    b bVar2 = this.j.get(indexOf);
                    c.this.q0.removeMessages(15, bVar2);
                    c.this.q0.sendMessageDelayed(Message.obtain(c.this.q0, 15, bVar2), c.this.a);
                    return false;
                }
                this.j.add(bVar);
                c.this.q0.sendMessageDelayed(Message.obtain(c.this.q0, 15, bVar), c.this.a);
                c.this.q0.sendMessageDelayed(Message.obtain(c.this.q0, 16, bVar), c.this.f0);
                ConnectionResult connectionResult = new ConnectionResult(2, null);
                if (v(connectionResult)) {
                    return false;
                }
                c.this.f(connectionResult, this.g);
                return false;
            }
            gVar.c(new UnsupportedApiCallException(c));
            return true;
        }

        public final Map<d.a<?>, x05> y() {
            return this.f;
        }

        public final void z(ConnectionResult connectionResult) {
            for (b25 b25Var : this.e) {
                String str = null;
                if (pl2.a(connectionResult, ConnectionResult.i0)) {
                    str = this.b.l();
                }
                b25Var.b(this.c, connectionResult, str);
            }
            this.e.clear();
        }
    }

    /* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
    /* renamed from: com.google.android.gms.common.api.internal.c$c  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public class C0110c implements h15, b.c {
        public final a.f a;
        public final ze<?> b;
        public com.google.android.gms.common.internal.d c = null;
        public Set<Scope> d = null;
        public boolean e = false;

        public C0110c(a.f fVar, ze<?> zeVar) {
            this.a = fVar;
            this.b = zeVar;
        }

        @Override // defpackage.h15
        public final void a(ConnectionResult connectionResult) {
            a aVar = (a) c.this.m0.get(this.b);
            if (aVar != null) {
                aVar.f(connectionResult);
            }
        }

        @Override // com.google.android.gms.common.internal.b.c
        public final void b(ConnectionResult connectionResult) {
            c.this.q0.post(new n(this, connectionResult));
        }

        @Override // defpackage.h15
        public final void c(com.google.android.gms.common.internal.d dVar, Set<Scope> set) {
            if (dVar != null && set != null) {
                this.c = dVar;
                this.d = set;
                e();
                return;
            }
            Log.wtf("GoogleApiManager", "Received null response from onSignInSuccess", new Exception());
            a(new ConnectionResult(4));
        }

        public final void e() {
            com.google.android.gms.common.internal.d dVar;
            if (!this.e || (dVar = this.c) == null) {
                return;
            }
            this.a.h(dVar, this.d);
        }
    }

    public c(Context context, Looper looper, dh1 dh1Var) {
        this.r0 = true;
        this.h0 = context;
        q25 q25Var = new q25(looper, this);
        this.q0 = q25Var;
        this.i0 = dh1Var;
        this.j0 = new y15(dh1Var);
        if (vm0.a(context)) {
            this.r0 = false;
        }
        q25Var.sendMessage(q25Var.obtainMessage(6));
    }

    @RecentlyNonNull
    public static c c(@RecentlyNonNull Context context) {
        c cVar;
        synchronized (u0) {
            if (v0 == null) {
                HandlerThread handlerThread = new HandlerThread("GoogleApiHandler", 9);
                handlerThread.start();
                v0 = new c(context.getApplicationContext(), handlerThread.getLooper(), dh1.q());
            }
            cVar = v0;
        }
        return cVar;
    }

    public static Status i(ze<?> zeVar, ConnectionResult connectionResult) {
        String a2 = zeVar.a();
        String valueOf = String.valueOf(connectionResult);
        StringBuilder sb = new StringBuilder(String.valueOf(a2).length() + 63 + valueOf.length());
        sb.append("API: ");
        sb.append(a2);
        sb.append(" is not available on this device. Connection failed with: ");
        sb.append(valueOf);
        return new Status(connectionResult, sb.toString());
    }

    public final void d(@RecentlyNonNull com.google.android.gms.common.api.b<?> bVar) {
        Handler handler = this.q0;
        handler.sendMessage(handler.obtainMessage(7, bVar));
    }

    public final <O extends a.d> void e(@RecentlyNonNull com.google.android.gms.common.api.b<O> bVar, @RecentlyNonNull int i, @RecentlyNonNull com.google.android.gms.common.api.internal.b<? extends l83, a.b> bVar2) {
        p pVar = new p(i, bVar2);
        Handler handler = this.q0;
        handler.sendMessage(handler.obtainMessage(4, new w05(pVar, this.l0.get(), bVar)));
    }

    public final boolean f(ConnectionResult connectionResult, int i) {
        return this.i0.B(this.h0, connectionResult, i);
    }

    @RecentlyNonNull
    public final int g() {
        return this.k0.getAndIncrement();
    }

    @Override // android.os.Handler.Callback
    @RecentlyNonNull
    public boolean handleMessage(@RecentlyNonNull Message message) {
        a<?> aVar;
        int i = message.what;
        switch (i) {
            case 1:
                this.g0 = ((Boolean) message.obj).booleanValue() ? 10000L : 300000L;
                this.q0.removeMessages(12);
                for (ze<?> zeVar : this.m0.keySet()) {
                    Handler handler = this.q0;
                    handler.sendMessageDelayed(handler.obtainMessage(12, zeVar), this.g0);
                }
                break;
            case 2:
                b25 b25Var = (b25) message.obj;
                Iterator<ze<?>> it = b25Var.a().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    } else {
                        ze<?> next = it.next();
                        a<?> aVar2 = this.m0.get(next);
                        if (aVar2 == null) {
                            b25Var.b(next, new ConnectionResult(13), null);
                            break;
                        } else if (aVar2.I()) {
                            b25Var.b(next, ConnectionResult.i0, aVar2.r().l());
                        } else {
                            ConnectionResult D = aVar2.D();
                            if (D != null) {
                                b25Var.b(next, D, null);
                            } else {
                                aVar2.o(b25Var);
                                aVar2.H();
                            }
                        }
                    }
                }
            case 3:
                for (a<?> aVar3 : this.m0.values()) {
                    aVar3.C();
                    aVar3.H();
                }
                break;
            case 4:
            case 8:
            case 13:
                w05 w05Var = (w05) message.obj;
                a<?> aVar4 = this.m0.get(w05Var.c.b());
                if (aVar4 == null) {
                    aVar4 = l(w05Var.c);
                }
                if (aVar4.J() && this.l0.get() != w05Var.b) {
                    w05Var.a.b(s0);
                    aVar4.d();
                    break;
                } else {
                    aVar4.n(w05Var.a);
                    break;
                }
            case 5:
                int i2 = message.arg1;
                ConnectionResult connectionResult = (ConnectionResult) message.obj;
                Iterator<a<?>> it2 = this.m0.values().iterator();
                while (true) {
                    if (it2.hasNext()) {
                        a<?> next2 = it2.next();
                        aVar = next2.K() == i2 ? next2 : null;
                    }
                }
                if (aVar != null) {
                    if (connectionResult.I1() != 13) {
                        aVar.h(i(aVar.c, connectionResult));
                        break;
                    } else {
                        String g = this.i0.g(connectionResult.I1());
                        String J1 = connectionResult.J1();
                        StringBuilder sb = new StringBuilder(String.valueOf(g).length() + 69 + String.valueOf(J1).length());
                        sb.append("Error resolution was canceled by the user, original error message: ");
                        sb.append(g);
                        sb.append(": ");
                        sb.append(J1);
                        aVar.h(new Status(17, sb.toString()));
                        break;
                    }
                } else {
                    StringBuilder sb2 = new StringBuilder(76);
                    sb2.append("Could not find API instance ");
                    sb2.append(i2);
                    sb2.append(" while trying to fail enqueued calls.");
                    Log.wtf("GoogleApiManager", sb2.toString(), new Exception());
                    break;
                }
            case 6:
                if (this.h0.getApplicationContext() instanceof Application) {
                    com.google.android.gms.common.api.internal.a.c((Application) this.h0.getApplicationContext());
                    com.google.android.gms.common.api.internal.a.b().a(new h(this));
                    if (!com.google.android.gms.common.api.internal.a.b().e(true)) {
                        this.g0 = 300000L;
                        break;
                    }
                }
                break;
            case 7:
                l((com.google.android.gms.common.api.b) message.obj);
                break;
            case 9:
                if (this.m0.containsKey(message.obj)) {
                    this.m0.get(message.obj).E();
                    break;
                }
                break;
            case 10:
                for (ze<?> zeVar2 : this.p0) {
                    a<?> remove = this.m0.remove(zeVar2);
                    if (remove != null) {
                        remove.d();
                    }
                }
                this.p0.clear();
                break;
            case 11:
                if (this.m0.containsKey(message.obj)) {
                    this.m0.get(message.obj).F();
                    break;
                }
                break;
            case 12:
                if (this.m0.containsKey(message.obj)) {
                    this.m0.get(message.obj).G();
                    break;
                }
                break;
            case 14:
                oz4 oz4Var = (oz4) message.obj;
                ze<?> a2 = oz4Var.a();
                if (!this.m0.containsKey(a2)) {
                    oz4Var.b().c(Boolean.FALSE);
                    break;
                } else {
                    oz4Var.b().c(Boolean.valueOf(this.m0.get(a2).q(false)));
                    break;
                }
            case 15:
                b bVar = (b) message.obj;
                if (this.m0.containsKey(bVar.a)) {
                    this.m0.get(bVar.a).m(bVar);
                    break;
                }
                break;
            case 16:
                b bVar2 = (b) message.obj;
                if (this.m0.containsKey(bVar2.a)) {
                    this.m0.get(bVar2.a).u(bVar2);
                    break;
                }
                break;
            default:
                StringBuilder sb3 = new StringBuilder(31);
                sb3.append("Unknown message id: ");
                sb3.append(i);
                return false;
        }
        return true;
    }

    public final void j(@RecentlyNonNull ConnectionResult connectionResult, @RecentlyNonNull int i) {
        if (f(connectionResult, i)) {
            return;
        }
        Handler handler = this.q0;
        handler.sendMessage(handler.obtainMessage(5, i, 0, connectionResult));
    }

    public final a<?> l(com.google.android.gms.common.api.b<?> bVar) {
        ze<?> b2 = bVar.b();
        a<?> aVar = this.m0.get(b2);
        if (aVar == null) {
            aVar = new a<>(bVar);
            this.m0.put(b2, aVar);
        }
        if (aVar.J()) {
            this.p0.add(b2);
        }
        aVar.H();
        return aVar;
    }

    public final void m() {
        Handler handler = this.q0;
        handler.sendMessage(handler.obtainMessage(3));
    }

    /* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
    /* loaded from: classes.dex */
    public static class b {
        public final ze<?> a;
        public final Feature b;

        public b(ze<?> zeVar, Feature feature) {
            this.a = zeVar;
            this.b = feature;
        }

        public final boolean equals(Object obj) {
            if (obj != null && (obj instanceof b)) {
                b bVar = (b) obj;
                if (pl2.a(this.a, bVar.a) && pl2.a(this.b, bVar.b)) {
                    return true;
                }
            }
            return false;
        }

        public final int hashCode() {
            return pl2.b(this.a, this.b);
        }

        public final String toString() {
            return pl2.c(this).a("key", this.a).a("feature", this.b).toString();
        }

        public /* synthetic */ b(ze zeVar, Feature feature, h hVar) {
            this(zeVar, feature);
        }
    }
}
