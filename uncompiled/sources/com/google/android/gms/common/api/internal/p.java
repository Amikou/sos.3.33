package com.google.android.gms.common.api.internal;

import android.os.DeadObjectException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.internal.b;
import com.google.android.gms.common.api.internal.c;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public final class p<A extends b<? extends l83, a.b>> extends o {
    public final A b;

    public p(int i, A a) {
        super(i);
        this.b = (A) zt2.k(a, "Null methods are not runnable.");
    }

    @Override // com.google.android.gms.common.api.internal.o
    public final void b(Status status) {
        try {
            this.b.w(status);
        } catch (IllegalStateException unused) {
        }
    }

    @Override // com.google.android.gms.common.api.internal.o
    public final void c(Exception exc) {
        String simpleName = exc.getClass().getSimpleName();
        String localizedMessage = exc.getLocalizedMessage();
        StringBuilder sb = new StringBuilder(simpleName.length() + 2 + String.valueOf(localizedMessage).length());
        sb.append(simpleName);
        sb.append(": ");
        sb.append(localizedMessage);
        try {
            this.b.w(new Status(10, sb.toString()));
        } catch (IllegalStateException unused) {
        }
    }

    @Override // com.google.android.gms.common.api.internal.o
    public final void d(b35 b35Var, boolean z) {
        b35Var.c(this.b, z);
    }

    @Override // com.google.android.gms.common.api.internal.o
    public final void f(c.a<?> aVar) throws DeadObjectException {
        try {
            this.b.u(aVar.r());
        } catch (RuntimeException e) {
            c(e);
        }
    }
}
