package com.google.android.gms.common.api.internal;

import android.os.DeadObjectException;
import android.os.RemoteException;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.c;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public abstract class q<T> extends g {
    public final n34<T> b;

    public q(int i, n34<T> n34Var) {
        super(i);
        this.b = n34Var;
    }

    @Override // com.google.android.gms.common.api.internal.o
    public void b(Status status) {
        this.b.d(new ApiException(status));
    }

    @Override // com.google.android.gms.common.api.internal.o
    public void c(Exception exc) {
        this.b.d(exc);
    }

    @Override // com.google.android.gms.common.api.internal.o
    public final void f(c.a<?> aVar) throws DeadObjectException {
        Status e;
        Status e2;
        try {
            i(aVar);
        } catch (DeadObjectException e3) {
            e2 = o.e(e3);
            b(e2);
            throw e3;
        } catch (RemoteException e4) {
            e = o.e(e4);
            b(e);
        } catch (RuntimeException e5) {
            c(e5);
        }
    }

    public abstract void i(c.a<?> aVar) throws RemoteException;
}
