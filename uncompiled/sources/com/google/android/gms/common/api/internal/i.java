package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.internal.c;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public final class i implements Runnable {
    public final /* synthetic */ int a;
    public final /* synthetic */ c.a f0;

    public i(c.a aVar, int i) {
        this.f0 = aVar;
        this.a = i;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f0.e(this.a);
    }
}
