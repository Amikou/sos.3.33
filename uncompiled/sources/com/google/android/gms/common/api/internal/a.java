package com.google.android.gms.common.api.internal;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.content.ComponentCallbacks2;
import android.content.res.Configuration;
import android.os.Bundle;
import androidx.annotation.RecentlyNonNull;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* loaded from: classes.dex */
public final class a implements Application.ActivityLifecycleCallbacks, ComponentCallbacks2 {
    public static final a i0 = new a();
    public final AtomicBoolean a = new AtomicBoolean();
    public final AtomicBoolean f0 = new AtomicBoolean();
    public final ArrayList<InterfaceC0109a> g0 = new ArrayList<>();
    public boolean h0 = false;

    /* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
    /* renamed from: com.google.android.gms.common.api.internal.a$a  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public interface InterfaceC0109a {
        void a(@RecentlyNonNull boolean z);
    }

    @RecentlyNonNull
    public static a b() {
        return i0;
    }

    public static void c(@RecentlyNonNull Application application) {
        a aVar = i0;
        synchronized (aVar) {
            if (!aVar.h0) {
                application.registerActivityLifecycleCallbacks(aVar);
                application.registerComponentCallbacks(aVar);
                aVar.h0 = true;
            }
        }
    }

    public final void a(@RecentlyNonNull InterfaceC0109a interfaceC0109a) {
        synchronized (i0) {
            this.g0.add(interfaceC0109a);
        }
    }

    @RecentlyNonNull
    public final boolean d() {
        return this.a.get();
    }

    @RecentlyNonNull
    @TargetApi(16)
    public final boolean e(@RecentlyNonNull boolean z) {
        if (!this.f0.get()) {
            if (!jr2.b()) {
                return z;
            }
            ActivityManager.RunningAppProcessInfo runningAppProcessInfo = new ActivityManager.RunningAppProcessInfo();
            ActivityManager.getMyMemoryState(runningAppProcessInfo);
            if (!this.f0.getAndSet(true) && runningAppProcessInfo.importance > 100) {
                this.a.set(true);
            }
        }
        return d();
    }

    public final void f(boolean z) {
        synchronized (i0) {
            ArrayList<InterfaceC0109a> arrayList = this.g0;
            int size = arrayList.size();
            int i = 0;
            while (i < size) {
                InterfaceC0109a interfaceC0109a = arrayList.get(i);
                i++;
                interfaceC0109a.a(z);
            }
        }
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public final void onActivityCreated(@RecentlyNonNull Activity activity, Bundle bundle) {
        boolean compareAndSet = this.a.compareAndSet(true, false);
        this.f0.set(true);
        if (compareAndSet) {
            f(false);
        }
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public final void onActivityDestroyed(@RecentlyNonNull Activity activity) {
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public final void onActivityPaused(@RecentlyNonNull Activity activity) {
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public final void onActivityResumed(@RecentlyNonNull Activity activity) {
        boolean compareAndSet = this.a.compareAndSet(true, false);
        this.f0.set(true);
        if (compareAndSet) {
            f(false);
        }
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public final void onActivitySaveInstanceState(@RecentlyNonNull Activity activity, @RecentlyNonNull Bundle bundle) {
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public final void onActivityStarted(@RecentlyNonNull Activity activity) {
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public final void onActivityStopped(@RecentlyNonNull Activity activity) {
    }

    @Override // android.content.ComponentCallbacks
    public final void onConfigurationChanged(@RecentlyNonNull Configuration configuration) {
    }

    @Override // android.content.ComponentCallbacks
    public final void onLowMemory() {
    }

    @Override // android.content.ComponentCallbacks2
    public final void onTrimMemory(@RecentlyNonNull int i) {
        if (i == 20 && this.a.compareAndSet(false, true)) {
            this.f0.set(true);
            f(true);
        }
    }
}
