package com.google.android.gms.common.api.internal;

import android.app.PendingIntent;
import android.os.DeadObjectException;
import android.os.RemoteException;
import androidx.annotation.RecentlyNonNull;
import androidx.annotation.RecentlyNullable;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.a.b;
import defpackage.l83;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public abstract class b<R extends l83, A extends a.b> extends BasePendingResult<R> {
    public final a.c<A> p;
    public final com.google.android.gms.common.api.a<?> q;

    public b(@RecentlyNonNull com.google.android.gms.common.api.a<?> aVar, @RecentlyNonNull GoogleApiClient googleApiClient) {
        super((GoogleApiClient) zt2.k(googleApiClient, "GoogleApiClient must not be null"));
        zt2.k(aVar, "Api must not be null");
        this.p = (a.c<A>) aVar.c();
        this.q = aVar;
    }

    public abstract void q(@RecentlyNonNull A a) throws RemoteException;

    @RecentlyNullable
    public final com.google.android.gms.common.api.a<?> r() {
        return this.q;
    }

    @RecentlyNonNull
    public final a.c<A> s() {
        return this.p;
    }

    public void t(@RecentlyNonNull R r) {
    }

    public final void u(@RecentlyNonNull A a) throws DeadObjectException {
        try {
            q(a);
        } catch (DeadObjectException e) {
            v(e);
            throw e;
        } catch (RemoteException e2) {
            v(e2);
        }
    }

    public final void v(RemoteException remoteException) {
        w(new Status(8, remoteException.getLocalizedMessage(), (PendingIntent) null));
    }

    public final void w(@RecentlyNonNull Status status) {
        zt2.b(!status.L1(), "Failed result must not be success");
        R d = d(status);
        g(d);
        t(d);
    }
}
