package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.internal.a;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public final class h implements a.InterfaceC0109a {
    public final /* synthetic */ c a;

    public h(c cVar) {
        this.a = cVar;
    }

    @Override // com.google.android.gms.common.api.internal.a.InterfaceC0109a
    public final void a(boolean z) {
        this.a.q0.sendMessage(this.a.q0.obtainMessage(1, Boolean.valueOf(z)));
    }
}
