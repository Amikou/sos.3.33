package com.google.android.gms.common.api;

import android.text.TextUtils;
import androidx.annotation.RecentlyNonNull;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.a;
import java.util.ArrayList;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public class AvailabilityException extends Exception {
    private final rh<ze<?>, ConnectionResult> zaa;

    public AvailabilityException(@RecentlyNonNull rh<ze<?>, ConnectionResult> rhVar) {
        this.zaa = rhVar;
    }

    public ConnectionResult getConnectionResult(@RecentlyNonNull b<? extends a.d> bVar) {
        ze<? extends a.d> b = bVar.b();
        boolean z = this.zaa.get(b) != null;
        String a = b.a();
        StringBuilder sb = new StringBuilder(String.valueOf(a).length() + 58);
        sb.append("The given API (");
        sb.append(a);
        sb.append(") was not part of the availability request.");
        zt2.b(z, sb.toString());
        return (ConnectionResult) zt2.j(this.zaa.get(b));
    }

    @Override // java.lang.Throwable
    public String getMessage() {
        ArrayList arrayList = new ArrayList();
        boolean z = true;
        for (ze<?> zeVar : this.zaa.keySet()) {
            ConnectionResult connectionResult = (ConnectionResult) zt2.j(this.zaa.get(zeVar));
            if (connectionResult.M1()) {
                z = false;
            }
            String a = zeVar.a();
            String valueOf = String.valueOf(connectionResult);
            StringBuilder sb = new StringBuilder(String.valueOf(a).length() + 2 + valueOf.length());
            sb.append(a);
            sb.append(": ");
            sb.append(valueOf);
            arrayList.add(sb.toString());
        }
        StringBuilder sb2 = new StringBuilder();
        if (z) {
            sb2.append("None of the queried APIs are available. ");
        } else {
            sb2.append("Some of the queried APIs are unavailable. ");
        }
        sb2.append(TextUtils.join("; ", arrayList));
        return sb2.toString();
    }

    public ConnectionResult getConnectionResult(@RecentlyNonNull c<? extends a.d> cVar) {
        ze<? extends a.d> b = cVar.b();
        boolean z = this.zaa.get(b) != null;
        String a = b.a();
        StringBuilder sb = new StringBuilder(String.valueOf(a).length() + 58);
        sb.append("The given API (");
        sb.append(a);
        sb.append(") was not part of the availability request.");
        zt2.b(z, sb.toString());
        return (ConnectionResult) zt2.j(this.zaa.get(b));
    }
}
