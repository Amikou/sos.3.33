package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.internal.c;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public final class l implements Runnable {
    public final /* synthetic */ ConnectionResult a;
    public final /* synthetic */ c.a f0;

    public l(c.a aVar, ConnectionResult connectionResult) {
        this.f0 = aVar;
        this.a = connectionResult;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f0.onConnectionFailed(this.a);
    }
}
