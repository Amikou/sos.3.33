package com.google.android.gms.common.api.internal;

import java.util.Collections;
import java.util.Set;
import java.util.WeakHashMap;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public class e {
    public final Set<d<?>> a = Collections.newSetFromMap(new WeakHashMap());

    public final void a() {
        for (d<?> dVar : this.a) {
            dVar.a();
        }
        this.a.clear();
    }
}
