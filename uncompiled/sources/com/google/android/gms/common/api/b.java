package com.google.android.gms.common.api;

import android.accounts.Account;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.RecentlyNonNull;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.a.d;
import com.google.android.gms.common.api.internal.c;
import defpackage.kz;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.Set;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public class b<O extends a.d> implements c<O> {
    public final Context a;
    public final com.google.android.gms.common.api.a<O> b;
    public final O c;
    public final ze<O> d;
    public final Looper e;
    public final int f;
    public final GoogleApiClient g;
    public final com.google.android.gms.common.api.internal.c h;

    /* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
    /* loaded from: classes.dex */
    public static class a {
        @RecentlyNonNull
        public final nt3 a;
        @RecentlyNonNull
        public final Looper b;

        /* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
        /* renamed from: com.google.android.gms.common.api.b$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public static class C0108a {
            public nt3 a;
            public Looper b;

            @RecentlyNonNull
            public a a() {
                if (this.a == null) {
                    this.a = new ye();
                }
                if (this.b == null) {
                    this.b = Looper.getMainLooper();
                }
                return new a(this.a, this.b);
            }

            @RecentlyNonNull
            public C0108a b(@RecentlyNonNull nt3 nt3Var) {
                zt2.k(nt3Var, "StatusExceptionMapper must not be null.");
                this.a = nt3Var;
                return this;
            }
        }

        static {
            new C0108a().a();
        }

        public a(nt3 nt3Var, Account account, Looper looper) {
            this.a = nt3Var;
            this.b = looper;
        }
    }

    public b(@RecentlyNonNull Context context, @RecentlyNonNull com.google.android.gms.common.api.a<O> aVar, @RecentlyNonNull O o, @RecentlyNonNull a aVar2) {
        zt2.k(context, "Null context is not permitted.");
        zt2.k(aVar, "Api must not be null.");
        zt2.k(aVar2, "Settings must not be null; use Settings.DEFAULT_SETTINGS instead.");
        Context applicationContext = context.getApplicationContext();
        this.a = applicationContext;
        k(context);
        this.b = aVar;
        this.c = o;
        this.e = aVar2.b;
        this.d = ze.b(aVar, o);
        this.g = new s05(this);
        com.google.android.gms.common.api.internal.c c = com.google.android.gms.common.api.internal.c.c(applicationContext);
        this.h = c;
        this.f = c.g();
        c.d(this);
    }

    public static String k(Object obj) {
        if (jr2.k()) {
            try {
                return (String) Context.class.getMethod("getAttributionTag", new Class[0]).invoke(obj, new Object[0]);
            } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException unused) {
                return null;
            }
        }
        return null;
    }

    @Override // com.google.android.gms.common.api.c
    @RecentlyNonNull
    public ze<O> b() {
        return this.d;
    }

    @RecentlyNonNull
    public GoogleApiClient c() {
        return this.g;
    }

    @RecentlyNonNull
    public kz.a d() {
        Account account;
        Set<Scope> emptySet;
        GoogleSignInAccount H0;
        GoogleSignInAccount H02;
        kz.a aVar = new kz.a();
        O o = this.c;
        if ((o instanceof a.d.b) && (H02 = ((a.d.b) o).H0()) != null) {
            account = H02.getAccount();
        } else {
            O o2 = this.c;
            account = o2 instanceof a.d.InterfaceC0107a ? ((a.d.InterfaceC0107a) o2).getAccount() : null;
        }
        kz.a c = aVar.c(account);
        O o3 = this.c;
        if ((o3 instanceof a.d.b) && (H0 = ((a.d.b) o3).H0()) != null) {
            emptySet = H0.P1();
        } else {
            emptySet = Collections.emptySet();
        }
        return c.e(emptySet).d(this.a.getClass().getName()).b(this.a.getPackageName());
    }

    @RecentlyNonNull
    public <A extends a.b, T extends com.google.android.gms.common.api.internal.b<? extends l83, A>> T e(@RecentlyNonNull T t) {
        return (T) j(2, t);
    }

    @RecentlyNonNull
    public <A extends a.b, T extends com.google.android.gms.common.api.internal.b<? extends l83, A>> T f(@RecentlyNonNull T t) {
        return (T) j(1, t);
    }

    @RecentlyNonNull
    public Looper g() {
        return this.e;
    }

    @RecentlyNonNull
    public final int h() {
        return this.f;
    }

    public final a.f i(Looper looper, c.a<O> aVar) {
        return ((a.AbstractC0106a) zt2.j(this.b.b())).d(this.a, looper, d().a(), this.c, aVar, aVar);
    }

    public final <A extends a.b, T extends com.google.android.gms.common.api.internal.b<? extends l83, A>> T j(int i, T t) {
        t.n();
        this.h.e(this, i, t);
        return t;
    }

    public final e15 l(Context context, Handler handler) {
        return new e15(context, handler, d().a());
    }

    @Deprecated
    public b(@RecentlyNonNull Context context, @RecentlyNonNull com.google.android.gms.common.api.a<O> aVar, @RecentlyNonNull O o, @RecentlyNonNull nt3 nt3Var) {
        this(context, aVar, o, new a.C0108a().b(nt3Var).a());
    }
}
