package com.google.android.gms.common.api;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.RecentlyNonNull;
import androidx.annotation.RecentlyNullable;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* loaded from: classes.dex */
public final class Status extends AbstractSafeParcelable implements l83, ReflectedParcelable {
    @RecentlyNonNull
    public static final Parcelable.Creator<Status> CREATOR;
    @RecentlyNonNull
    public static final Status j0 = new Status(0);
    @RecentlyNonNull
    public static final Status k0;
    @RecentlyNonNull
    public static final Status l0;
    @RecentlyNonNull
    public static final Status m0;
    public final int a;
    public final int f0;
    public final String g0;
    public final PendingIntent h0;
    public final ConnectionResult i0;

    static {
        new Status(14);
        k0 = new Status(8);
        l0 = new Status(15);
        m0 = new Status(16);
        new Status(17);
        new Status(18);
        CREATOR = new o75();
    }

    public Status(int i, int i2, String str, PendingIntent pendingIntent) {
        this(i, i2, str, pendingIntent, null);
    }

    @RecentlyNullable
    public final ConnectionResult I1() {
        return this.i0;
    }

    @RecentlyNonNull
    public final int J1() {
        return this.f0;
    }

    @RecentlyNullable
    public final String K1() {
        return this.g0;
    }

    @RecentlyNonNull
    public final boolean L1() {
        return this.f0 <= 0;
    }

    @RecentlyNonNull
    public final String M1() {
        String str = this.g0;
        return str != null ? str : i30.a(this.f0);
    }

    @Override // defpackage.l83
    @RecentlyNonNull
    public final Status e() {
        return this;
    }

    @RecentlyNonNull
    public final boolean equals(Object obj) {
        if (obj instanceof Status) {
            Status status = (Status) obj;
            return this.a == status.a && this.f0 == status.f0 && pl2.a(this.g0, status.g0) && pl2.a(this.h0, status.h0) && pl2.a(this.i0, status.i0);
        }
        return false;
    }

    @RecentlyNonNull
    public final int hashCode() {
        return pl2.b(Integer.valueOf(this.a), Integer.valueOf(this.f0), this.g0, this.h0, this.i0);
    }

    @RecentlyNonNull
    public final String toString() {
        return pl2.c(this).a("statusCode", M1()).a("resolution", this.h0).toString();
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        int a = yb3.a(parcel);
        yb3.m(parcel, 1, J1());
        yb3.s(parcel, 2, K1(), false);
        yb3.r(parcel, 3, this.h0, i, false);
        yb3.r(parcel, 4, I1(), i, false);
        yb3.m(parcel, 1000, this.a);
        yb3.b(parcel, a);
    }

    public Status(int i, int i2, String str, PendingIntent pendingIntent, ConnectionResult connectionResult) {
        this.a = i;
        this.f0 = i2;
        this.g0 = str;
        this.h0 = pendingIntent;
        this.i0 = connectionResult;
    }

    public Status(@RecentlyNonNull int i) {
        this(i, (String) null);
    }

    public Status(@RecentlyNonNull int i, String str) {
        this(1, i, str, null);
    }

    public Status(@RecentlyNonNull int i, String str, PendingIntent pendingIntent) {
        this(1, i, str, pendingIntent);
    }

    public Status(@RecentlyNonNull ConnectionResult connectionResult, @RecentlyNonNull String str) {
        this(connectionResult, str, 17);
    }

    @Deprecated
    public Status(@RecentlyNonNull ConnectionResult connectionResult, @RecentlyNonNull String str, @RecentlyNonNull int i) {
        this(1, i, str, connectionResult.K1(), connectionResult);
    }
}
