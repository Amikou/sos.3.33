package com.google.android.gms.common.api;

import android.accounts.Account;
import android.content.Context;
import android.content.Intent;
import android.os.Looper;
import androidx.annotation.RecentlyNonNull;
import androidx.annotation.RecentlyNullable;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.Feature;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.a.d;
import com.google.android.gms.common.internal.b;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public final class a<O extends d> {
    public final AbstractC0106a<?, O> a;
    public final g<?> b;
    public final String c;

    /* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
    /* renamed from: com.google.android.gms.common.api.a$a  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static abstract class AbstractC0106a<T extends f, O> extends e<T, O> {
        @RecentlyNonNull
        public T c(@RecentlyNonNull Context context, @RecentlyNonNull Looper looper, @RecentlyNonNull kz kzVar, @RecentlyNonNull O o, @RecentlyNonNull u50 u50Var, @RecentlyNonNull jm2 jm2Var) {
            throw new UnsupportedOperationException("buildClient must be implemented");
        }

        @RecentlyNonNull
        @Deprecated
        public T d(@RecentlyNonNull Context context, @RecentlyNonNull Looper looper, @RecentlyNonNull kz kzVar, @RecentlyNonNull O o, @RecentlyNonNull GoogleApiClient.b bVar, @RecentlyNonNull GoogleApiClient.c cVar) {
            return c(context, looper, kzVar, o, bVar, cVar);
        }
    }

    /* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
    /* loaded from: classes.dex */
    public interface b {
    }

    /* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
    /* loaded from: classes.dex */
    public static class c<C extends b> {
    }

    /* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
    /* loaded from: classes.dex */
    public interface d {

        /* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
        /* renamed from: com.google.android.gms.common.api.a$d$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public interface InterfaceC0107a extends d {
            @RecentlyNonNull
            Account getAccount();
        }

        /* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
        /* loaded from: classes.dex */
        public interface b extends d {
            @RecentlyNonNull
            GoogleSignInAccount H0();
        }
    }

    /* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
    /* loaded from: classes.dex */
    public static abstract class e<T extends b, O> {
        @RecentlyNonNull
        public List<Scope> a(O o) {
            return Collections.emptyList();
        }

        @RecentlyNonNull
        public int b() {
            return Integer.MAX_VALUE;
        }
    }

    /* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
    /* loaded from: classes.dex */
    public interface f extends b {
        void b();

        @RecentlyNonNull
        boolean c();

        @RecentlyNonNull
        boolean e();

        Set<Scope> g();

        void h(com.google.android.gms.common.internal.d dVar, Set<Scope> set);

        void i(@RecentlyNonNull String str);

        @RecentlyNonNull
        boolean k();

        @RecentlyNonNull
        String l();

        void m(@RecentlyNonNull b.c cVar);

        void n(@RecentlyNonNull b.e eVar);

        void o(@RecentlyNonNull String str, FileDescriptor fileDescriptor, @RecentlyNonNull PrintWriter printWriter, String[] strArr);

        @RecentlyNonNull
        boolean p();

        @RecentlyNonNull
        int q();

        @RecentlyNonNull
        Feature[] r();

        @RecentlyNullable
        String s();

        @RecentlyNonNull
        Intent t();

        @RecentlyNonNull
        boolean u();
    }

    /* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
    /* loaded from: classes.dex */
    public static final class g<C extends f> extends c<C> {
    }

    /* JADX WARN: Multi-variable type inference failed */
    public <C extends f> a(@RecentlyNonNull String str, @RecentlyNonNull AbstractC0106a<C, O> abstractC0106a, @RecentlyNonNull g<C> gVar) {
        zt2.k(abstractC0106a, "Cannot construct an Api with a null ClientBuilder");
        zt2.k(gVar, "Cannot construct an Api with a null ClientKey");
        this.c = str;
        this.a = abstractC0106a;
        this.b = gVar;
    }

    @RecentlyNonNull
    public final e<?, O> a() {
        return this.a;
    }

    @RecentlyNonNull
    public final AbstractC0106a<?, O> b() {
        return this.a;
    }

    @RecentlyNonNull
    public final c<?> c() {
        return this.b;
    }

    @RecentlyNonNull
    public final String d() {
        return this.c;
    }
}
