package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.a;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public final class m implements Runnable {
    public final /* synthetic */ k a;

    public m(k kVar) {
        this.a = kVar;
    }

    @Override // java.lang.Runnable
    public final void run() {
        a.f fVar;
        a.f fVar2;
        fVar = this.a.a.b;
        fVar2 = this.a.a.b;
        fVar.i(fVar2.getClass().getName().concat(" disconnecting because it was signed out."));
    }
}
