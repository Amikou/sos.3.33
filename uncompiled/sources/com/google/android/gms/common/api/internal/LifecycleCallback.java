package com.google.android.gms.common.api.internal;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Keep;
import androidx.annotation.RecentlyNonNull;
import java.io.FileDescriptor;
import java.io.PrintWriter;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* loaded from: classes.dex */
public class LifecycleCallback {
    @RecentlyNonNull
    public final nz1 a;

    public LifecycleCallback(@RecentlyNonNull nz1 nz1Var) {
        this.a = nz1Var;
    }

    @RecentlyNonNull
    public static nz1 c(@RecentlyNonNull lz1 lz1Var) {
        throw null;
    }

    @Keep
    private static nz1 getChimeraLifecycleFragmentImpl(lz1 lz1Var) {
        throw new IllegalStateException("Method not available in SDK.");
    }

    public void a(@RecentlyNonNull String str, @RecentlyNonNull FileDescriptor fileDescriptor, @RecentlyNonNull PrintWriter printWriter, @RecentlyNonNull String[] strArr) {
    }

    @RecentlyNonNull
    public Activity b() {
        return this.a.c();
    }

    public void d(@RecentlyNonNull int i, @RecentlyNonNull int i2, @RecentlyNonNull Intent intent) {
    }

    public void e(Bundle bundle) {
    }

    public void f() {
    }

    public void g() {
    }

    public void h(@RecentlyNonNull Bundle bundle) {
    }

    public void i() {
    }

    public void j() {
    }
}
