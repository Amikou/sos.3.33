package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.Feature;
import com.google.android.gms.common.api.internal.c;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public abstract class g extends o {
    public g(int i) {
        super(i);
    }

    public abstract Feature[] g(c.a<?> aVar);

    public abstract boolean h(c.a<?> aVar);
}
