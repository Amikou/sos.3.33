package com.google.android.gms.common.api;

import android.accounts.Account;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import androidx.annotation.RecentlyNonNull;
import com.google.android.gms.common.api.a;
import defpackage.kz;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.locks.ReentrantLock;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
@Deprecated
/* loaded from: classes.dex */
public abstract class GoogleApiClient {
    public static final Set<GoogleApiClient> a = Collections.newSetFromMap(new WeakHashMap());

    /* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
    @Deprecated
    /* loaded from: classes.dex */
    public static final class a {
        public Account a;
        public int d;
        public View e;
        public String f;
        public String g;
        public final Context i;
        public lz1 k;
        public c m;
        public Looper n;
        public final Set<Scope> b = new HashSet();
        public final Set<Scope> c = new HashSet();
        public final Map<com.google.android.gms.common.api.a<?>, kz.b> h = new rh();
        public final Map<com.google.android.gms.common.api.a<?>, a.d> j = new rh();
        public int l = -1;
        public dh1 o = dh1.q();
        public a.AbstractC0106a<? extends p15, uo3> p = lz4.c;
        public final ArrayList<b> q = new ArrayList<>();
        public final ArrayList<c> r = new ArrayList<>();

        public a(@RecentlyNonNull Context context) {
            this.i = context;
            this.n = context.getMainLooper();
            this.f = context.getPackageName();
            this.g = context.getClass().getName();
        }

        @RecentlyNonNull
        public final a a(@RecentlyNonNull com.google.android.gms.common.api.a<Object> aVar) {
            zt2.k(aVar, "Api must not be null");
            this.j.put(aVar, null);
            List<Scope> a = ((a.e) zt2.k(aVar.a(), "Base client builder must not be null")).a(null);
            this.c.addAll(a);
            this.b.addAll(a);
            return this;
        }

        @RecentlyNonNull
        public final a b(@RecentlyNonNull b bVar) {
            zt2.k(bVar, "Listener must not be null");
            this.q.add(bVar);
            return this;
        }

        @RecentlyNonNull
        public final a c(@RecentlyNonNull c cVar) {
            zt2.k(cVar, "Listener must not be null");
            this.r.add(cVar);
            return this;
        }

        @RecentlyNonNull
        public final GoogleApiClient d() {
            zt2.b(!this.j.isEmpty(), "must call addApi() to add at least one API");
            kz e = e();
            com.google.android.gms.common.api.a<?> aVar = null;
            Map<com.google.android.gms.common.api.a<?>, kz.b> f = e.f();
            rh rhVar = new rh();
            rh rhVar2 = new rh();
            ArrayList arrayList = new ArrayList();
            boolean z = false;
            for (com.google.android.gms.common.api.a<?> aVar2 : this.j.keySet()) {
                a.d dVar = this.j.get(aVar2);
                boolean z2 = f.get(aVar2) != null;
                rhVar.put(aVar2, Boolean.valueOf(z2));
                r25 r25Var = new r25(aVar2, z2);
                arrayList.add(r25Var);
                a.AbstractC0106a abstractC0106a = (a.AbstractC0106a) zt2.j(aVar2.b());
                a.f d = abstractC0106a.d(this.i, this.n, e, dVar, r25Var, r25Var);
                rhVar2.put(aVar2.c(), d);
                if (abstractC0106a.b() == 1) {
                    z = dVar != null;
                }
                if (d.e()) {
                    if (aVar != null) {
                        String d2 = aVar2.d();
                        String d3 = aVar.d();
                        StringBuilder sb = new StringBuilder(String.valueOf(d2).length() + 21 + String.valueOf(d3).length());
                        sb.append(d2);
                        sb.append(" cannot be used with ");
                        sb.append(d3);
                        throw new IllegalStateException(sb.toString());
                    }
                    aVar = aVar2;
                }
            }
            if (aVar != null) {
                if (!z) {
                    zt2.o(this.a == null, "Must not set an account in GoogleApiClient.Builder when using %s. Set account in GoogleSignInOptions.Builder instead", aVar.d());
                    zt2.o(this.b.equals(this.c), "Must not set scopes in GoogleApiClient.Builder when using %s. Set account in GoogleSignInOptions.Builder instead.", aVar.d());
                } else {
                    String d4 = aVar.d();
                    StringBuilder sb2 = new StringBuilder(String.valueOf(d4).length() + 82);
                    sb2.append("With using ");
                    sb2.append(d4);
                    sb2.append(", GamesOptions can only be specified within GoogleSignInOptions.Builder");
                    throw new IllegalStateException(sb2.toString());
                }
            }
            d05 d05Var = new d05(this.i, new ReentrantLock(), this.n, e, this.o, this.p, rhVar, this.q, this.r, rhVar2, this.l, d05.m(rhVar2.values(), true), arrayList);
            synchronized (GoogleApiClient.a) {
                GoogleApiClient.a.add(d05Var);
            }
            if (this.l >= 0) {
                w15.p(this.k).r(this.l, d05Var, this.m);
            }
            return d05Var;
        }

        @RecentlyNonNull
        public final kz e() {
            uo3 uo3Var = uo3.a;
            Map<com.google.android.gms.common.api.a<?>, a.d> map = this.j;
            com.google.android.gms.common.api.a<uo3> aVar = lz4.e;
            if (map.containsKey(aVar)) {
                uo3Var = (uo3) this.j.get(aVar);
            }
            return new kz(this.a, this.b, this.h, this.d, this.e, this.f, this.g, uo3Var, false);
        }

        @RecentlyNonNull
        public final a f(@RecentlyNonNull Handler handler) {
            zt2.k(handler, "Handler must not be null");
            this.n = handler.getLooper();
            return this;
        }
    }

    /* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
    @Deprecated
    /* loaded from: classes.dex */
    public interface b extends u50 {
    }

    /* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
    @Deprecated
    /* loaded from: classes.dex */
    public interface c extends jm2 {
    }

    public void c(@RecentlyNonNull int i) {
        throw new UnsupportedOperationException();
    }

    public abstract void connect();

    public abstract void disconnect();

    public abstract void e(@RecentlyNonNull String str, @RecentlyNonNull FileDescriptor fileDescriptor, @RecentlyNonNull PrintWriter printWriter, @RecentlyNonNull String[] strArr);

    @RecentlyNonNull
    public <A extends a.b, T extends com.google.android.gms.common.api.internal.b<? extends l83, A>> T f(@RecentlyNonNull T t) {
        throw new UnsupportedOperationException();
    }

    @RecentlyNonNull
    public Looper g() {
        throw new UnsupportedOperationException();
    }

    @RecentlyNonNull
    public abstract boolean h();

    public abstract void i(@RecentlyNonNull c cVar);

    public abstract void j(@RecentlyNonNull c cVar);

    public void l(j15 j15Var) {
        throw new UnsupportedOperationException();
    }
}
