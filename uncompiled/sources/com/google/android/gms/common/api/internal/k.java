package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.internal.c;
import com.google.android.gms.common.internal.b;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public final class k implements b.e {
    public final /* synthetic */ c.a a;

    public k(c.a aVar) {
        this.a = aVar;
    }

    @Override // com.google.android.gms.common.internal.b.e
    public final void a() {
        c.this.q0.post(new m(this));
    }
}
