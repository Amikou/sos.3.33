package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.internal.c;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public final class n implements Runnable {
    public final /* synthetic */ ConnectionResult a;
    public final /* synthetic */ c.C0110c f0;

    public n(c.C0110c c0110c, ConnectionResult connectionResult) {
        this.f0 = c0110c;
        this.a = connectionResult;
    }

    @Override // java.lang.Runnable
    public final void run() {
        ze zeVar;
        a.f fVar;
        a.f fVar2;
        a.f fVar3;
        a.f fVar4;
        Map map = c.this.m0;
        zeVar = this.f0.b;
        c.a aVar = (c.a) map.get(zeVar);
        if (aVar == null) {
            return;
        }
        if (this.a.M1()) {
            this.f0.e = true;
            fVar = this.f0.a;
            if (fVar.u()) {
                this.f0.e();
                return;
            }
            try {
                fVar3 = this.f0.a;
                fVar4 = this.f0.a;
                fVar3.h(null, fVar4.g());
                return;
            } catch (SecurityException unused) {
                fVar2 = this.f0.a;
                fVar2.i("Failed to get service from broker.");
                aVar.onConnectionFailed(new ConnectionResult(10));
                return;
            }
        }
        aVar.onConnectionFailed(this.a);
    }
}
