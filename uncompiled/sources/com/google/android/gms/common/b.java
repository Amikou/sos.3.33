package com.google.android.gms.common;

import android.content.Context;
import android.os.RemoteException;
import android.os.StrictMode;
import com.google.android.gms.common.b;
import com.google.android.gms.common.internal.q;
import com.google.android.gms.dynamite.DynamiteModule;
import java.util.concurrent.Callable;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* loaded from: classes.dex */
public final class b {
    public static volatile q a;
    public static final Object b = new Object();
    public static Context c;

    public static final /* synthetic */ String a(boolean z, String str, vc5 vc5Var) throws Exception {
        boolean z2 = true;
        if (z || !d(str, vc5Var, true, false).a) {
            z2 = false;
        }
        return dx5.a(str, vc5Var, z, z2);
    }

    public static dx5 b(String str, vc5 vc5Var, boolean z, boolean z2) {
        StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        try {
            return d(str, vc5Var, z, z2);
        } finally {
            StrictMode.setThreadPolicy(allowThreadDiskReads);
        }
    }

    public static synchronized void c(Context context) {
        synchronized (b.class) {
            if (c != null || context == null) {
                return;
            }
            c = context.getApplicationContext();
        }
    }

    public static dx5 d(final String str, final vc5 vc5Var, final boolean z, boolean z2) {
        try {
            if (a == null) {
                zt2.j(c);
                synchronized (b) {
                    if (a == null) {
                        a = h36.F1(DynamiteModule.d(c, DynamiteModule.j, "com.google.android.gms.googlecertificates").c("com.google.android.gms.common.GoogleCertificatesImpl"));
                    }
                }
            }
            zt2.j(c);
            try {
                if (a.p0(new zzj(str, vc5Var, z, z2), nl2.H1(c.getPackageManager()))) {
                    return dx5.b();
                }
                return dx5.e(new Callable(z, str, vc5Var) { // from class: gf5
                    public final boolean a;
                    public final String b;
                    public final vc5 c;

                    {
                        this.a = z;
                        this.b = str;
                        this.c = vc5Var;
                    }

                    @Override // java.util.concurrent.Callable
                    public final Object call() {
                        return b.a(this.a, this.b, this.c);
                    }
                });
            } catch (RemoteException e) {
                return dx5.d("module call", e);
            }
        } catch (DynamiteModule.LoadingException e2) {
            String valueOf = String.valueOf(e2.getMessage());
            return dx5.d(valueOf.length() != 0 ? "module init: ".concat(valueOf) : new String("module init: "), e2);
        }
    }
}
