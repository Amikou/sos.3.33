package com.google.android.gms.common.stats;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import androidx.annotation.RecentlyNonNull;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
@Deprecated
/* loaded from: classes.dex */
public final class WakeLockEvent extends StatsEvent {
    @RecentlyNonNull
    public static final Parcelable.Creator<WakeLockEvent> CREATOR = new y35();
    public final int a;
    public final long f0;
    public int g0;
    public final String h0;
    public final String i0;
    public final String j0;
    public final int k0;
    public final List<String> l0;
    public final String m0;
    public final long n0;
    public int o0;
    public final String p0;
    public final float q0;
    public final long r0;
    public final boolean s0;
    public long t0 = -1;

    public WakeLockEvent(int i, long j, int i2, String str, int i3, List<String> list, String str2, long j2, int i4, String str3, String str4, float f, long j3, String str5, boolean z) {
        this.a = i;
        this.f0 = j;
        this.g0 = i2;
        this.h0 = str;
        this.i0 = str3;
        this.j0 = str5;
        this.k0 = i3;
        this.l0 = list;
        this.m0 = str2;
        this.n0 = j2;
        this.o0 = i4;
        this.p0 = str4;
        this.q0 = f;
        this.r0 = j3;
        this.s0 = z;
    }

    @Override // com.google.android.gms.common.stats.StatsEvent
    @RecentlyNonNull
    public final long I1() {
        return this.f0;
    }

    @Override // com.google.android.gms.common.stats.StatsEvent
    @RecentlyNonNull
    public final int J1() {
        return this.g0;
    }

    @Override // com.google.android.gms.common.stats.StatsEvent
    @RecentlyNonNull
    public final long K1() {
        return this.t0;
    }

    @Override // com.google.android.gms.common.stats.StatsEvent
    @RecentlyNonNull
    public final String L1() {
        List<String> list = this.l0;
        String str = this.h0;
        int i = this.k0;
        String join = list == null ? "" : TextUtils.join(",", list);
        int i2 = this.o0;
        String str2 = this.i0;
        if (str2 == null) {
            str2 = "";
        }
        String str3 = this.p0;
        if (str3 == null) {
            str3 = "";
        }
        float f = this.q0;
        String str4 = this.j0;
        String str5 = str4 != null ? str4 : "";
        boolean z = this.s0;
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 51 + String.valueOf(join).length() + String.valueOf(str2).length() + String.valueOf(str3).length() + String.valueOf(str5).length());
        sb.append("\t");
        sb.append(str);
        sb.append("\t");
        sb.append(i);
        sb.append("\t");
        sb.append(join);
        sb.append("\t");
        sb.append(i2);
        sb.append("\t");
        sb.append(str2);
        sb.append("\t");
        sb.append(str3);
        sb.append("\t");
        sb.append(f);
        sb.append("\t");
        sb.append(str5);
        sb.append("\t");
        sb.append(z);
        return sb.toString();
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        int a = yb3.a(parcel);
        yb3.m(parcel, 1, this.a);
        yb3.o(parcel, 2, I1());
        yb3.s(parcel, 4, this.h0, false);
        yb3.m(parcel, 5, this.k0);
        yb3.u(parcel, 6, this.l0, false);
        yb3.o(parcel, 8, this.n0);
        yb3.s(parcel, 10, this.i0, false);
        yb3.m(parcel, 11, J1());
        yb3.s(parcel, 12, this.m0, false);
        yb3.s(parcel, 13, this.p0, false);
        yb3.m(parcel, 14, this.o0);
        yb3.j(parcel, 15, this.q0);
        yb3.o(parcel, 16, this.r0);
        yb3.s(parcel, 17, this.j0, false);
        yb3.c(parcel, 18, this.s0);
        yb3.b(parcel, a);
    }
}
