package com.google.android.gms.common.stats;

import androidx.annotation.RecentlyNonNull;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
@Deprecated
/* loaded from: classes.dex */
public abstract class StatsEvent extends AbstractSafeParcelable implements ReflectedParcelable {
    @RecentlyNonNull
    public abstract long I1();

    @RecentlyNonNull
    public abstract int J1();

    @RecentlyNonNull
    public abstract long K1();

    @RecentlyNonNull
    public abstract String L1();

    @RecentlyNonNull
    public String toString() {
        long I1 = I1();
        int J1 = J1();
        long K1 = K1();
        String L1 = L1();
        StringBuilder sb = new StringBuilder(String.valueOf(L1).length() + 53);
        sb.append(I1);
        sb.append("\t");
        sb.append(J1);
        sb.append("\t");
        sb.append(K1);
        sb.append(L1);
        return sb.toString();
    }
}
