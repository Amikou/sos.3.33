package com.google.android.gms.common.data;

import android.database.CursorWindow;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.RecentlyNonNull;
import androidx.annotation.RecentlyNullable;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.HashMap;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
@KeepName
/* loaded from: classes.dex */
public final class DataHolder extends AbstractSafeParcelable implements Closeable {
    @RecentlyNonNull
    public static final Parcelable.Creator<DataHolder> CREATOR = new z05();
    public final int a;
    public final String[] f0;
    public Bundle g0;
    public final CursorWindow[] h0;
    public final int i0;
    public final Bundle j0;
    public int[] k0;
    public boolean l0 = false;
    public boolean m0 = true;

    static {
        new com.google.android.gms.common.data.a(new String[0], null);
    }

    public DataHolder(int i, String[] strArr, CursorWindow[] cursorWindowArr, int i2, Bundle bundle) {
        this.a = i;
        this.f0 = strArr;
        this.h0 = cursorWindowArr;
        this.i0 = i2;
        this.j0 = bundle;
    }

    @RecentlyNullable
    public final Bundle I1() {
        return this.j0;
    }

    @RecentlyNonNull
    public final int J1() {
        return this.i0;
    }

    public final void K1() {
        this.g0 = new Bundle();
        int i = 0;
        int i2 = 0;
        while (true) {
            String[] strArr = this.f0;
            if (i2 >= strArr.length) {
                break;
            }
            this.g0.putInt(strArr[i2], i2);
            i2++;
        }
        this.k0 = new int[this.h0.length];
        int i3 = 0;
        while (true) {
            CursorWindow[] cursorWindowArr = this.h0;
            if (i >= cursorWindowArr.length) {
                return;
            }
            this.k0[i] = i3;
            i3 += this.h0[i].getNumRows() - (i3 - cursorWindowArr[i].getStartPosition());
            i++;
        }
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public final void close() {
        synchronized (this) {
            if (!this.l0) {
                this.l0 = true;
                int i = 0;
                while (true) {
                    CursorWindow[] cursorWindowArr = this.h0;
                    if (i >= cursorWindowArr.length) {
                        break;
                    }
                    cursorWindowArr[i].close();
                    i++;
                }
            }
        }
    }

    public final void finalize() throws Throwable {
        try {
            if (this.m0 && this.h0.length > 0 && !isClosed()) {
                close();
                String obj = toString();
                StringBuilder sb = new StringBuilder(String.valueOf(obj).length() + 178);
                sb.append("Internal data leak within a DataBuffer object detected!  Be sure to explicitly call release() on all DataBuffer extending objects when you are done with them. (internal object: ");
                sb.append(obj);
                sb.append(")");
            }
        } finally {
            super.finalize();
        }
    }

    @RecentlyNonNull
    public final boolean isClosed() {
        boolean z;
        synchronized (this) {
            z = this.l0;
        }
        return z;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        int a2 = yb3.a(parcel);
        yb3.t(parcel, 1, this.f0, false);
        yb3.v(parcel, 2, this.h0, i, false);
        yb3.m(parcel, 3, J1());
        yb3.e(parcel, 4, I1(), false);
        yb3.m(parcel, 1000, this.a);
        yb3.b(parcel, a2);
        if ((i & 1) != 0) {
            close();
        }
    }

    /* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
    /* loaded from: classes.dex */
    public static class a {
        public a(String[] strArr, String str) {
            String[] strArr2 = (String[]) zt2.j(strArr);
            new ArrayList();
            new HashMap();
        }

        public /* synthetic */ a(String[] strArr, String str, com.google.android.gms.common.data.a aVar) {
            this(strArr, null);
        }
    }
}
