package com.google.android.gms.common;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import androidx.annotation.RecentlyNonNull;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.m;
import com.google.android.gms.common.internal.zay;
import com.google.android.gms.dynamic.RemoteCreator;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public final class SignInButton extends FrameLayout implements View.OnClickListener {
    public int a;
    public int f0;
    public View g0;
    public View.OnClickListener h0;

    public SignInButton(@RecentlyNonNull Context context) {
        this(context, null);
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(@RecentlyNonNull View view) {
        View.OnClickListener onClickListener = this.h0;
        if (onClickListener == null || view != this.g0) {
            return;
        }
        onClickListener.onClick(this);
    }

    public final void setColorScheme(@RecentlyNonNull int i) {
        setStyle(this.a, i);
    }

    @Override // android.view.View
    public final void setEnabled(@RecentlyNonNull boolean z) {
        super.setEnabled(z);
        this.g0.setEnabled(z);
    }

    @Override // android.view.View
    public final void setOnClickListener(View.OnClickListener onClickListener) {
        this.h0 = onClickListener;
        View view = this.g0;
        if (view != null) {
            view.setOnClickListener(this);
        }
    }

    @Deprecated
    public final void setScopes(@RecentlyNonNull Scope[] scopeArr) {
        setStyle(this.a, this.f0);
    }

    public final void setSize(@RecentlyNonNull int i) {
        setStyle(i, this.f0);
    }

    public final void setStyle(@RecentlyNonNull int i, @RecentlyNonNull int i2) {
        this.a = i;
        this.f0 = i2;
        Context context = getContext();
        View view = this.g0;
        if (view != null) {
            removeView(view);
        }
        try {
            this.g0 = m.c(context, this.a, this.f0);
        } catch (RemoteCreator.RemoteCreatorException unused) {
            int i3 = this.a;
            int i4 = this.f0;
            zay zayVar = new zay(context);
            zayVar.b(context.getResources(), i3, i4);
            this.g0 = zayVar;
        }
        addView(this.g0);
        this.g0.setEnabled(isEnabled());
        this.g0.setOnClickListener(this);
    }

    public SignInButton(@RecentlyNonNull Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public SignInButton(@RecentlyNonNull Context context, AttributeSet attributeSet, @RecentlyNonNull int i) {
        super(context, attributeSet, i);
        this.h0 = null;
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, n23.SignInButton, 0, 0);
        try {
            this.a = obtainStyledAttributes.getInt(n23.SignInButton_buttonSize, 0);
            this.f0 = obtainStyledAttributes.getInt(n23.SignInButton_colorScheme, 2);
            obtainStyledAttributes.recycle();
            setStyle(this.a, this.f0);
        } catch (Throwable th) {
            obtainStyledAttributes.recycle();
            throw th;
        }
    }

    @Deprecated
    public final void setStyle(@RecentlyNonNull int i, @RecentlyNonNull int i2, @RecentlyNonNull Scope[] scopeArr) {
        setStyle(i, i2);
    }
}
