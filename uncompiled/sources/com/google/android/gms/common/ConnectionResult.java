package com.google.android.gms.common;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.RecentlyNonNull;
import androidx.annotation.RecentlyNullable;
import com.fasterxml.jackson.databind.deser.std.ThrowableDeserializer;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* loaded from: classes.dex */
public final class ConnectionResult extends AbstractSafeParcelable {
    public final int a;
    public final int f0;
    public final PendingIntent g0;
    public final String h0;
    @RecentlyNonNull
    public static final ConnectionResult i0 = new ConnectionResult(0);
    @RecentlyNonNull
    public static final Parcelable.Creator<ConnectionResult> CREATOR = new t35();

    public ConnectionResult(int i, int i2, PendingIntent pendingIntent, String str) {
        this.a = i;
        this.f0 = i2;
        this.g0 = pendingIntent;
        this.h0 = str;
    }

    public static String N1(int i) {
        if (i != 99) {
            if (i != 1500) {
                switch (i) {
                    case -1:
                        return "UNKNOWN";
                    case 0:
                        return "SUCCESS";
                    case 1:
                        return "SERVICE_MISSING";
                    case 2:
                        return "SERVICE_VERSION_UPDATE_REQUIRED";
                    case 3:
                        return "SERVICE_DISABLED";
                    case 4:
                        return "SIGN_IN_REQUIRED";
                    case 5:
                        return "INVALID_ACCOUNT";
                    case 6:
                        return "RESOLUTION_REQUIRED";
                    case 7:
                        return "NETWORK_ERROR";
                    case 8:
                        return "INTERNAL_ERROR";
                    case 9:
                        return "SERVICE_INVALID";
                    case 10:
                        return "DEVELOPER_ERROR";
                    case 11:
                        return "LICENSE_CHECK_FAILED";
                    default:
                        switch (i) {
                            case 13:
                                return "CANCELED";
                            case 14:
                                return "TIMEOUT";
                            case 15:
                                return "INTERRUPTED";
                            case 16:
                                return "API_UNAVAILABLE";
                            case 17:
                                return "SIGN_IN_FAILED";
                            case 18:
                                return "SERVICE_UPDATING";
                            case 19:
                                return "SERVICE_MISSING_PERMISSION";
                            case 20:
                                return "RESTRICTED_PROFILE";
                            case 21:
                                return "API_VERSION_UPDATE_REQUIRED";
                            case 22:
                                return "RESOLUTION_ACTIVITY_NOT_FOUND";
                            default:
                                StringBuilder sb = new StringBuilder(31);
                                sb.append("UNKNOWN_ERROR_CODE(");
                                sb.append(i);
                                sb.append(")");
                                return sb.toString();
                        }
                }
            }
            return "DRIVE_EXTERNAL_STORAGE_REQUIRED";
        }
        return "UNFINISHED";
    }

    @RecentlyNonNull
    public final int I1() {
        return this.f0;
    }

    @RecentlyNullable
    public final String J1() {
        return this.h0;
    }

    @RecentlyNullable
    public final PendingIntent K1() {
        return this.g0;
    }

    @RecentlyNonNull
    public final boolean L1() {
        return (this.f0 == 0 || this.g0 == null) ? false : true;
    }

    @RecentlyNonNull
    public final boolean M1() {
        return this.f0 == 0;
    }

    @RecentlyNonNull
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof ConnectionResult) {
            ConnectionResult connectionResult = (ConnectionResult) obj;
            return this.f0 == connectionResult.f0 && pl2.a(this.g0, connectionResult.g0) && pl2.a(this.h0, connectionResult.h0);
        }
        return false;
    }

    @RecentlyNonNull
    public final int hashCode() {
        return pl2.b(Integer.valueOf(this.f0), this.g0, this.h0);
    }

    @RecentlyNonNull
    public final String toString() {
        return pl2.c(this).a("statusCode", N1(this.f0)).a("resolution", this.g0).a(ThrowableDeserializer.PROP_NAME_MESSAGE, this.h0).toString();
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        int a = yb3.a(parcel);
        yb3.m(parcel, 1, this.a);
        yb3.m(parcel, 2, I1());
        yb3.r(parcel, 3, K1(), i, false);
        yb3.s(parcel, 4, J1(), false);
        yb3.b(parcel, a);
    }

    public ConnectionResult(@RecentlyNonNull int i) {
        this(i, null, null);
    }

    public ConnectionResult(@RecentlyNonNull int i, PendingIntent pendingIntent) {
        this(i, pendingIntent, null);
    }

    public ConnectionResult(@RecentlyNonNull int i, PendingIntent pendingIntent, String str) {
        this(1, i, pendingIntent, str);
    }
}
