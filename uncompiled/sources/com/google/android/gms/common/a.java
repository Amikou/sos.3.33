package com.google.android.gms.common;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import androidx.annotation.RecentlyNonNull;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* loaded from: classes.dex */
public class a implements ServiceConnection {
    public boolean a = false;
    public final BlockingQueue<IBinder> f0 = new LinkedBlockingQueue();

    @RecentlyNonNull
    public IBinder a(@RecentlyNonNull long j, @RecentlyNonNull TimeUnit timeUnit) throws InterruptedException, TimeoutException {
        zt2.i("BlockingServiceConnection.getServiceWithTimeout() called on main thread");
        if (!this.a) {
            this.a = true;
            IBinder poll = this.f0.poll(j, timeUnit);
            if (poll != null) {
                return poll;
            }
            throw new TimeoutException("Timed out waiting for the service connection");
        }
        throw new IllegalStateException("Cannot call get on this connection more than once");
    }

    @Override // android.content.ServiceConnection
    public void onServiceConnected(@RecentlyNonNull ComponentName componentName, @RecentlyNonNull IBinder iBinder) {
        this.f0.add(iBinder);
    }

    @Override // android.content.ServiceConnection
    public void onServiceDisconnected(@RecentlyNonNull ComponentName componentName) {
    }
}
