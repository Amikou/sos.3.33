package com.google.android.gms.common;

import androidx.annotation.RecentlyNonNull;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* loaded from: classes.dex */
public final class GooglePlayServicesNotAvailableException extends Exception {
    @RecentlyNonNull
    public final int errorCode;

    public GooglePlayServicesNotAvailableException(@RecentlyNonNull int i) {
        this.errorCode = i;
    }
}
