package com.google.android.gms.common.server.response;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.server.response.FastJsonResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public final class zal extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zal> CREATOR = new h25();
    public final int a;
    public final HashMap<String, Map<String, FastJsonResponse.Field<?, ?>>> f0;
    public final String g0;

    public zal(int i, ArrayList<zak> arrayList, String str) {
        this.a = i;
        HashMap<String, Map<String, FastJsonResponse.Field<?, ?>>> hashMap = new HashMap<>();
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            zak zakVar = arrayList.get(i2);
            String str2 = zakVar.f0;
            HashMap hashMap2 = new HashMap();
            int size2 = ((ArrayList) zt2.j(zakVar.g0)).size();
            for (int i3 = 0; i3 < size2; i3++) {
                zan zanVar = zakVar.g0.get(i3);
                hashMap2.put(zanVar.f0, zanVar.g0);
            }
            hashMap.put(str2, hashMap2);
        }
        this.f0 = hashMap;
        this.g0 = (String) zt2.j(str);
        J1();
    }

    public final Map<String, FastJsonResponse.Field<?, ?>> I1(String str) {
        return this.f0.get(str);
    }

    public final void J1() {
        for (String str : this.f0.keySet()) {
            Map<String, FastJsonResponse.Field<?, ?>> map = this.f0.get(str);
            for (String str2 : map.keySet()) {
                map.get(str2).K1(this);
            }
        }
    }

    public final String K1() {
        return this.g0;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        for (String str : this.f0.keySet()) {
            sb.append(str);
            sb.append(":\n");
            Map<String, FastJsonResponse.Field<?, ?>> map = this.f0.get(str);
            for (String str2 : map.keySet()) {
                sb.append("  ");
                sb.append(str2);
                sb.append(": ");
                sb.append(map.get(str2));
            }
        }
        return sb.toString();
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int a = yb3.a(parcel);
        yb3.m(parcel, 1, this.a);
        ArrayList arrayList = new ArrayList();
        for (String str : this.f0.keySet()) {
            arrayList.add(new zak(str, this.f0.get(str)));
        }
        yb3.w(parcel, 2, arrayList, false);
        yb3.s(parcel, 3, this.g0, false);
        yb3.b(parcel, a);
    }
}
