package com.google.android.gms.common.server.response;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.server.response.FastJsonResponse;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public final class zan extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zan> CREATOR = new a();
    public final int a;
    public final String f0;
    public final FastJsonResponse.Field<?, ?> g0;

    public zan(int i, String str, FastJsonResponse.Field<?, ?> field) {
        this.a = i;
        this.f0 = str;
        this.g0 = field;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int a = yb3.a(parcel);
        yb3.m(parcel, 1, this.a);
        yb3.s(parcel, 2, this.f0, false);
        yb3.r(parcel, 3, this.g0, i, false);
        yb3.b(parcel, a);
    }

    public zan(String str, FastJsonResponse.Field<?, ?> field) {
        this.a = 1;
        this.f0 = str;
        this.g0 = field;
    }
}
