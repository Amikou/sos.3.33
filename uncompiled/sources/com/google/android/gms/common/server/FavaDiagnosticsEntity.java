package com.google.android.gms.common.server;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.RecentlyNonNull;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public class FavaDiagnosticsEntity extends AbstractSafeParcelable implements ReflectedParcelable {
    @RecentlyNonNull
    public static final Parcelable.Creator<FavaDiagnosticsEntity> CREATOR = new kz4();
    public final int a;
    public final String f0;
    public final int g0;

    public FavaDiagnosticsEntity(@RecentlyNonNull int i, @RecentlyNonNull String str, @RecentlyNonNull int i2) {
        this.a = i;
        this.f0 = str;
        this.g0 = i2;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        int a = yb3.a(parcel);
        yb3.m(parcel, 1, this.a);
        yb3.s(parcel, 2, this.f0, false);
        yb3.m(parcel, 3, this.g0);
        yb3.b(parcel, a);
    }
}
