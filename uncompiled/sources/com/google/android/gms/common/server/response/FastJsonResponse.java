package com.google.android.gms.common.server.response;

import android.os.Parcel;
import androidx.annotation.RecentlyNonNull;
import androidx.annotation.RecentlyNullable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.server.converter.zaa;
import defpackage.pl2;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public abstract class FastJsonResponse {

    /* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
    /* loaded from: classes.dex */
    public static class Field<I, O> extends AbstractSafeParcelable {
        public static final b CREATOR = new b();
        public final int a;
        @RecentlyNonNull
        public final int f0;
        @RecentlyNonNull
        public final boolean g0;
        @RecentlyNonNull
        public final int h0;
        @RecentlyNonNull
        public final boolean i0;
        @RecentlyNonNull
        public final String j0;
        @RecentlyNonNull
        public final int k0;
        @RecentlyNullable
        public final Class<? extends FastJsonResponse> l0;
        public final String m0;
        public zal n0;
        public a<I, O> o0;

        public Field(int i, int i2, boolean z, int i3, boolean z2, String str, int i4, String str2, zaa zaaVar) {
            this.a = i;
            this.f0 = i2;
            this.g0 = z;
            this.h0 = i3;
            this.i0 = z2;
            this.j0 = str;
            this.k0 = i4;
            if (str2 == null) {
                this.l0 = null;
                this.m0 = null;
            } else {
                this.l0 = SafeParcelResponse.class;
                this.m0 = str2;
            }
            if (zaaVar == null) {
                this.o0 = null;
            } else {
                this.o0 = (a<I, O>) zaaVar.J1();
            }
        }

        @RecentlyNonNull
        public int I1() {
            return this.k0;
        }

        public final void K1(zal zalVar) {
            this.n0 = zalVar;
        }

        @RecentlyNonNull
        public final I L1(@RecentlyNonNull O o) {
            zt2.j(this.o0);
            return this.o0.L0(o);
        }

        @RecentlyNonNull
        public final boolean M1() {
            return this.o0 != null;
        }

        @RecentlyNonNull
        public final Map<String, Field<?, ?>> N1() {
            zt2.j(this.m0);
            zt2.j(this.n0);
            return (Map) zt2.j(this.n0.I1(this.m0));
        }

        public final String O1() {
            String str = this.m0;
            if (str == null) {
                return null;
            }
            return str;
        }

        public final zaa P1() {
            a<I, O> aVar = this.o0;
            if (aVar == null) {
                return null;
            }
            return zaa.I1(aVar);
        }

        @RecentlyNonNull
        public String toString() {
            pl2.a a = pl2.c(this).a("versionCode", Integer.valueOf(this.a)).a("typeIn", Integer.valueOf(this.f0)).a("typeInArray", Boolean.valueOf(this.g0)).a("typeOut", Integer.valueOf(this.h0)).a("typeOutArray", Boolean.valueOf(this.i0)).a("outputFieldName", this.j0).a("safeParcelFieldId", Integer.valueOf(this.k0)).a("concreteTypeName", O1());
            Class<? extends FastJsonResponse> cls = this.l0;
            if (cls != null) {
                a.a("concreteType.class", cls.getCanonicalName());
            }
            a<I, O> aVar = this.o0;
            if (aVar != null) {
                a.a("converterName", aVar.getClass().getCanonicalName());
            }
            return a.toString();
        }

        @Override // android.os.Parcelable
        public void writeToParcel(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
            int a = yb3.a(parcel);
            yb3.m(parcel, 1, this.a);
            yb3.m(parcel, 2, this.f0);
            yb3.c(parcel, 3, this.g0);
            yb3.m(parcel, 4, this.h0);
            yb3.c(parcel, 5, this.i0);
            yb3.s(parcel, 6, this.j0, false);
            yb3.m(parcel, 7, I1());
            yb3.s(parcel, 8, O1(), false);
            yb3.r(parcel, 9, P1(), i, false);
            yb3.b(parcel, a);
        }
    }

    /* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
    /* loaded from: classes.dex */
    public interface a<I, O> {
        @RecentlyNonNull
        I L0(@RecentlyNonNull O o);
    }

    /* JADX WARN: Multi-variable type inference failed */
    @RecentlyNonNull
    public static <O, I> I f(@RecentlyNonNull Field<I, O> field, Object obj) {
        return field.o0 != null ? field.L1(obj) : obj;
    }

    public static void g(StringBuilder sb, Field field, Object obj) {
        int i = field.f0;
        if (i == 11) {
            Class<? extends FastJsonResponse> cls = field.l0;
            zt2.j(cls);
            sb.append(cls.cast(obj).toString());
        } else if (i == 7) {
            sb.append("\"");
            sb.append(iw1.a((String) obj));
            sb.append("\"");
        } else {
            sb.append(obj);
        }
    }

    @RecentlyNonNull
    public abstract Map<String, Field<?, ?>> a();

    @RecentlyNullable
    public Object b(@RecentlyNonNull Field field) {
        String str = field.j0;
        if (field.l0 != null) {
            zt2.o(c(str) == null, "Concrete field shouldn't be value object: %s", field.j0);
            try {
                char upperCase = Character.toUpperCase(str.charAt(0));
                String substring = str.substring(1);
                StringBuilder sb = new StringBuilder(String.valueOf(substring).length() + 4);
                sb.append("get");
                sb.append(upperCase);
                sb.append(substring);
                return getClass().getMethod(sb.toString(), new Class[0]).invoke(this, new Object[0]);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        return c(str);
    }

    @RecentlyNullable
    public abstract Object c(@RecentlyNonNull String str);

    @RecentlyNonNull
    public boolean d(@RecentlyNonNull Field field) {
        if (field.h0 == 11) {
            if (field.i0) {
                throw new UnsupportedOperationException("Concrete type arrays not supported");
            }
            throw new UnsupportedOperationException("Concrete types not supported");
        }
        return e(field.j0);
    }

    @RecentlyNonNull
    public abstract boolean e(@RecentlyNonNull String str);

    @RecentlyNonNull
    public String toString() {
        Map<String, Field<?, ?>> a2 = a();
        StringBuilder sb = new StringBuilder(100);
        for (String str : a2.keySet()) {
            Field<?, ?> field = a2.get(str);
            if (d(field)) {
                Object f = f(field, b(field));
                if (sb.length() == 0) {
                    sb.append("{");
                } else {
                    sb.append(",");
                }
                sb.append("\"");
                sb.append(str);
                sb.append("\":");
                if (f == null) {
                    sb.append("null");
                } else {
                    switch (field.h0) {
                        case 8:
                            sb.append("\"");
                            sb.append(mm.a((byte[]) f));
                            sb.append("\"");
                            continue;
                        case 9:
                            sb.append("\"");
                            sb.append(mm.b((byte[]) f));
                            sb.append("\"");
                            continue;
                        case 10:
                            w32.a(sb, (HashMap) f);
                            continue;
                        default:
                            if (field.g0) {
                                ArrayList arrayList = (ArrayList) f;
                                sb.append("[");
                                int size = arrayList.size();
                                for (int i = 0; i < size; i++) {
                                    if (i > 0) {
                                        sb.append(",");
                                    }
                                    Object obj = arrayList.get(i);
                                    if (obj != null) {
                                        g(sb, field, obj);
                                    }
                                }
                                sb.append("]");
                                break;
                            } else {
                                g(sb, field, f);
                                continue;
                            }
                    }
                }
            }
        }
        if (sb.length() > 0) {
            sb.append("}");
        } else {
            sb.append("{}");
        }
        return sb.toString();
    }
}
