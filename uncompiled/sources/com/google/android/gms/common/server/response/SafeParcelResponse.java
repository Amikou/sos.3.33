package com.google.android.gms.common.server.response;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.SparseArray;
import androidx.annotation.RecentlyNonNull;
import androidx.annotation.RecentlyNullable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.common.server.response.FastJsonResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public class SafeParcelResponse extends FastSafeParcelableJsonResponse {
    @RecentlyNonNull
    public static final Parcelable.Creator<SafeParcelResponse> CREATOR = new p25();
    public final int a;
    public final Parcel f0;
    public final int g0 = 2;
    public final zal h0;
    public final String i0;
    public int j0;
    public int k0;

    public SafeParcelResponse(int i, Parcel parcel, zal zalVar) {
        this.a = i;
        this.f0 = (Parcel) zt2.j(parcel);
        this.h0 = zalVar;
        if (zalVar == null) {
            this.i0 = null;
        } else {
            this.i0 = zalVar.K1();
        }
        this.j0 = 2;
    }

    public static void g(StringBuilder sb, FastJsonResponse.Field<?, ?> field, Object obj) {
        if (field.g0) {
            ArrayList arrayList = (ArrayList) obj;
            sb.append("[");
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                if (i != 0) {
                    sb.append(",");
                }
                i(sb, field.f0, arrayList.get(i));
            }
            sb.append("]");
            return;
        }
        i(sb, field.f0, obj);
    }

    public static void i(StringBuilder sb, int i, Object obj) {
        switch (i) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                sb.append(obj);
                return;
            case 7:
                sb.append("\"");
                sb.append(iw1.a(zt2.j(obj).toString()));
                sb.append("\"");
                return;
            case 8:
                sb.append("\"");
                sb.append(mm.a((byte[]) obj));
                sb.append("\"");
                return;
            case 9:
                sb.append("\"");
                sb.append(mm.b((byte[]) obj));
                sb.append("\"");
                return;
            case 10:
                w32.a(sb, (HashMap) zt2.j(obj));
                return;
            case 11:
                throw new IllegalArgumentException("Method does not accept concrete type.");
            default:
                StringBuilder sb2 = new StringBuilder(26);
                sb2.append("Unknown type = ");
                sb2.append(i);
                throw new IllegalArgumentException(sb2.toString());
        }
    }

    @Override // com.google.android.gms.common.server.response.FastJsonResponse
    @RecentlyNullable
    public Map<String, FastJsonResponse.Field<?, ?>> a() {
        zal zalVar = this.h0;
        if (zalVar == null) {
            return null;
        }
        return zalVar.I1((String) zt2.j(this.i0));
    }

    @Override // com.google.android.gms.common.server.response.FastSafeParcelableJsonResponse, com.google.android.gms.common.server.response.FastJsonResponse
    @RecentlyNonNull
    public Object c(@RecentlyNonNull String str) {
        throw new UnsupportedOperationException("Converting to JSON does not require this method.");
    }

    @Override // com.google.android.gms.common.server.response.FastSafeParcelableJsonResponse, com.google.android.gms.common.server.response.FastJsonResponse
    @RecentlyNonNull
    public boolean e(@RecentlyNonNull String str) {
        throw new UnsupportedOperationException("Converting to JSON does not require this method.");
    }

    /* JADX WARN: Code restructure failed: missing block: B:5:0x0005, code lost:
        if (r0 != 1) goto L6;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final android.os.Parcel h() {
        /*
            r2 = this;
            int r0 = r2.j0
            if (r0 == 0) goto L8
            r1 = 1
            if (r0 == r1) goto L10
            goto L1a
        L8:
            android.os.Parcel r0 = r2.f0
            int r0 = defpackage.yb3.a(r0)
            r2.k0 = r0
        L10:
            android.os.Parcel r0 = r2.f0
            int r1 = r2.k0
            defpackage.yb3.b(r0, r1)
            r0 = 2
            r2.j0 = r0
        L1a:
            android.os.Parcel r0 = r2.f0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.server.response.SafeParcelResponse.h():android.os.Parcel");
    }

    public final void j(StringBuilder sb, Map<String, FastJsonResponse.Field<?, ?>> map, Parcel parcel) {
        SparseArray sparseArray = new SparseArray();
        for (Map.Entry<String, FastJsonResponse.Field<?, ?>> entry : map.entrySet()) {
            sparseArray.put(entry.getValue().I1(), entry);
        }
        sb.append('{');
        int J = SafeParcelReader.J(parcel);
        boolean z = false;
        while (parcel.dataPosition() < J) {
            int C = SafeParcelReader.C(parcel);
            Map.Entry entry2 = (Map.Entry) sparseArray.get(SafeParcelReader.v(C));
            if (entry2 != null) {
                if (z) {
                    sb.append(",");
                }
                FastJsonResponse.Field field = (FastJsonResponse.Field) entry2.getValue();
                sb.append("\"");
                sb.append((String) entry2.getKey());
                sb.append("\":");
                if (field.M1()) {
                    switch (field.h0) {
                        case 0:
                            g(sb, field, FastJsonResponse.f(field, Integer.valueOf(SafeParcelReader.E(parcel, C))));
                            break;
                        case 1:
                            g(sb, field, FastJsonResponse.f(field, SafeParcelReader.c(parcel, C)));
                            break;
                        case 2:
                            g(sb, field, FastJsonResponse.f(field, Long.valueOf(SafeParcelReader.F(parcel, C))));
                            break;
                        case 3:
                            g(sb, field, FastJsonResponse.f(field, Float.valueOf(SafeParcelReader.A(parcel, C))));
                            break;
                        case 4:
                            g(sb, field, FastJsonResponse.f(field, Double.valueOf(SafeParcelReader.y(parcel, C))));
                            break;
                        case 5:
                            g(sb, field, FastJsonResponse.f(field, SafeParcelReader.a(parcel, C)));
                            break;
                        case 6:
                            g(sb, field, FastJsonResponse.f(field, Boolean.valueOf(SafeParcelReader.w(parcel, C))));
                            break;
                        case 7:
                            g(sb, field, FastJsonResponse.f(field, SafeParcelReader.p(parcel, C)));
                            break;
                        case 8:
                        case 9:
                            g(sb, field, FastJsonResponse.f(field, SafeParcelReader.g(parcel, C)));
                            break;
                        case 10:
                            Bundle f = SafeParcelReader.f(parcel, C);
                            HashMap hashMap = new HashMap();
                            for (String str : f.keySet()) {
                                hashMap.put(str, (String) zt2.j(f.getString(str)));
                            }
                            g(sb, field, FastJsonResponse.f(field, hashMap));
                            break;
                        case 11:
                            throw new IllegalArgumentException("Method does not accept concrete type.");
                        default:
                            int i = field.h0;
                            StringBuilder sb2 = new StringBuilder(36);
                            sb2.append("Unknown field out type = ");
                            sb2.append(i);
                            throw new IllegalArgumentException(sb2.toString());
                    }
                } else if (field.i0) {
                    sb.append("[");
                    switch (field.h0) {
                        case 0:
                            vh.e(sb, SafeParcelReader.k(parcel, C));
                            break;
                        case 1:
                            vh.g(sb, SafeParcelReader.d(parcel, C));
                            break;
                        case 2:
                            vh.f(sb, SafeParcelReader.l(parcel, C));
                            break;
                        case 3:
                            vh.d(sb, SafeParcelReader.j(parcel, C));
                            break;
                        case 4:
                            vh.c(sb, SafeParcelReader.i(parcel, C));
                            break;
                        case 5:
                            vh.g(sb, SafeParcelReader.b(parcel, C));
                            break;
                        case 6:
                            vh.h(sb, SafeParcelReader.e(parcel, C));
                            break;
                        case 7:
                            vh.i(sb, SafeParcelReader.q(parcel, C));
                            break;
                        case 8:
                        case 9:
                        case 10:
                            throw new UnsupportedOperationException("List of type BASE64, BASE64_URL_SAFE, or STRING_MAP is not supported");
                        case 11:
                            Parcel[] n = SafeParcelReader.n(parcel, C);
                            int length = n.length;
                            for (int i2 = 0; i2 < length; i2++) {
                                if (i2 > 0) {
                                    sb.append(",");
                                }
                                n[i2].setDataPosition(0);
                                j(sb, field.N1(), n[i2]);
                            }
                            break;
                        default:
                            throw new IllegalStateException("Unknown field type out.");
                    }
                    sb.append("]");
                } else {
                    switch (field.h0) {
                        case 0:
                            sb.append(SafeParcelReader.E(parcel, C));
                            break;
                        case 1:
                            sb.append(SafeParcelReader.c(parcel, C));
                            break;
                        case 2:
                            sb.append(SafeParcelReader.F(parcel, C));
                            break;
                        case 3:
                            sb.append(SafeParcelReader.A(parcel, C));
                            break;
                        case 4:
                            sb.append(SafeParcelReader.y(parcel, C));
                            break;
                        case 5:
                            sb.append(SafeParcelReader.a(parcel, C));
                            break;
                        case 6:
                            sb.append(SafeParcelReader.w(parcel, C));
                            break;
                        case 7:
                            String p = SafeParcelReader.p(parcel, C);
                            sb.append("\"");
                            sb.append(iw1.a(p));
                            sb.append("\"");
                            break;
                        case 8:
                            byte[] g = SafeParcelReader.g(parcel, C);
                            sb.append("\"");
                            sb.append(mm.a(g));
                            sb.append("\"");
                            break;
                        case 9:
                            byte[] g2 = SafeParcelReader.g(parcel, C);
                            sb.append("\"");
                            sb.append(mm.b(g2));
                            sb.append("\"");
                            break;
                        case 10:
                            Bundle f2 = SafeParcelReader.f(parcel, C);
                            Set<String> keySet = f2.keySet();
                            sb.append("{");
                            boolean z2 = true;
                            for (String str2 : keySet) {
                                if (!z2) {
                                    sb.append(",");
                                }
                                sb.append("\"");
                                sb.append(str2);
                                sb.append("\"");
                                sb.append(":");
                                sb.append("\"");
                                sb.append(iw1.a(f2.getString(str2)));
                                sb.append("\"");
                                z2 = false;
                            }
                            sb.append("}");
                            break;
                        case 11:
                            Parcel m = SafeParcelReader.m(parcel, C);
                            m.setDataPosition(0);
                            j(sb, field.N1(), m);
                            break;
                        default:
                            throw new IllegalStateException("Unknown field type out");
                    }
                }
                z = true;
            }
        }
        if (parcel.dataPosition() == J) {
            sb.append('}');
            return;
        }
        StringBuilder sb3 = new StringBuilder(37);
        sb3.append("Overread allowed size end=");
        sb3.append(J);
        throw new SafeParcelReader.ParseException(sb3.toString(), parcel);
    }

    @Override // com.google.android.gms.common.server.response.FastJsonResponse
    @RecentlyNonNull
    public String toString() {
        zt2.k(this.h0, "Cannot convert to JSON on client side.");
        Parcel h = h();
        h.setDataPosition(0);
        StringBuilder sb = new StringBuilder(100);
        j(sb, (Map) zt2.j(this.h0.I1((String) zt2.j(this.i0))), h);
        return sb.toString();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        zal zalVar;
        int a = yb3.a(parcel);
        yb3.m(parcel, 1, this.a);
        yb3.q(parcel, 2, h(), false);
        int i2 = this.g0;
        if (i2 == 0) {
            zalVar = null;
        } else if (i2 == 1) {
            zalVar = this.h0;
        } else if (i2 == 2) {
            zalVar = this.h0;
        } else {
            int i3 = this.g0;
            StringBuilder sb = new StringBuilder(34);
            sb.append("Invalid creation type: ");
            sb.append(i3);
            throw new IllegalStateException(sb.toString());
        }
        yb3.r(parcel, 3, zalVar, i, false);
        yb3.b(parcel, a);
    }
}
