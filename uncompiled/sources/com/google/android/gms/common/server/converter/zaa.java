package com.google.android.gms.common.server.converter;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.server.response.FastJsonResponse;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public final class zaa extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zaa> CREATOR = new n05();
    public final int a;
    public final StringToIntConverter f0;

    public zaa(int i, StringToIntConverter stringToIntConverter) {
        this.a = i;
        this.f0 = stringToIntConverter;
    }

    public static zaa I1(FastJsonResponse.a<?, ?> aVar) {
        if (aVar instanceof StringToIntConverter) {
            return new zaa((StringToIntConverter) aVar);
        }
        throw new IllegalArgumentException("Unsupported safe parcelable field converter class.");
    }

    public final FastJsonResponse.a<?, ?> J1() {
        StringToIntConverter stringToIntConverter = this.f0;
        if (stringToIntConverter != null) {
            return stringToIntConverter;
        }
        throw new IllegalStateException("There was no converter wrapped in this ConverterWrapper.");
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int a = yb3.a(parcel);
        yb3.m(parcel, 1, this.a);
        yb3.r(parcel, 2, this.f0, i, false);
        yb3.b(parcel, a);
    }

    public zaa(StringToIntConverter stringToIntConverter) {
        this.a = 1;
        this.f0 = stringToIntConverter;
    }
}
