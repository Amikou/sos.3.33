package com.google.android.gms.common.server.response;

import androidx.annotation.RecentlyNonNull;
import androidx.annotation.RecentlyNullable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.server.response.FastJsonResponse;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public abstract class FastSafeParcelableJsonResponse extends FastJsonResponse implements SafeParcelable {
    @Override // com.google.android.gms.common.server.response.FastJsonResponse
    @RecentlyNullable
    public Object c(@RecentlyNonNull String str) {
        return null;
    }

    @Override // android.os.Parcelable
    @RecentlyNonNull
    public final int describeContents() {
        return 0;
    }

    @Override // com.google.android.gms.common.server.response.FastJsonResponse
    @RecentlyNonNull
    public boolean e(@RecentlyNonNull String str) {
        return false;
    }

    @RecentlyNonNull
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (getClass().isInstance(obj)) {
            FastJsonResponse fastJsonResponse = (FastJsonResponse) obj;
            for (FastJsonResponse.Field<?, ?> field : a().values()) {
                if (d(field)) {
                    if (!fastJsonResponse.d(field) || !pl2.a(b(field), fastJsonResponse.b(field))) {
                        return false;
                    }
                } else if (fastJsonResponse.d(field)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    @RecentlyNonNull
    public int hashCode() {
        int i = 0;
        for (FastJsonResponse.Field<?, ?> field : a().values()) {
            if (d(field)) {
                i = (i * 31) + zt2.j(b(field)).hashCode();
            }
        }
        return i;
    }
}
