package com.google.android.gms.common.server.response;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.server.response.FastJsonResponse;
import java.util.ArrayList;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public final class zak extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zak> CREATOR = new m25();
    public final int a;
    public final String f0;
    public final ArrayList<zan> g0;

    public zak(int i, String str, ArrayList<zan> arrayList) {
        this.a = i;
        this.f0 = str;
        this.g0 = arrayList;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int a = yb3.a(parcel);
        yb3.m(parcel, 1, this.a);
        yb3.s(parcel, 2, this.f0, false);
        yb3.w(parcel, 3, this.g0, false);
        yb3.b(parcel, a);
    }

    public zak(String str, Map<String, FastJsonResponse.Field<?, ?>> map) {
        ArrayList<zan> arrayList;
        this.a = 1;
        this.f0 = str;
        if (map == null) {
            arrayList = null;
        } else {
            arrayList = new ArrayList<>();
            for (String str2 : map.keySet()) {
                arrayList.add(new zan(str2, map.get(str2)));
            }
        }
        this.g0 = arrayList;
    }
}
