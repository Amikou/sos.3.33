package com.google.android.gms.common.server.converter;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.SparseArray;
import androidx.annotation.RecentlyNonNull;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.server.response.FastJsonResponse;
import java.util.ArrayList;
import java.util.HashMap;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* loaded from: classes.dex */
public final class StringToIntConverter extends AbstractSafeParcelable implements FastJsonResponse.a<String, Integer> {
    @RecentlyNonNull
    public static final Parcelable.Creator<StringToIntConverter> CREATOR = new a();
    public final int a;
    public final HashMap<String, Integer> f0;
    public final SparseArray<String> g0;

    public StringToIntConverter(int i, ArrayList<zaa> arrayList) {
        this.a = i;
        this.f0 = new HashMap<>();
        this.g0 = new SparseArray<>();
        int size = arrayList.size();
        int i2 = 0;
        while (i2 < size) {
            zaa zaaVar = arrayList.get(i2);
            i2++;
            zaa zaaVar2 = zaaVar;
            I1(zaaVar2.f0, zaaVar2.g0);
        }
    }

    @RecentlyNonNull
    public final StringToIntConverter I1(@RecentlyNonNull String str, @RecentlyNonNull int i) {
        this.f0.put(str, Integer.valueOf(i));
        this.g0.put(i, str);
        return this;
    }

    @Override // com.google.android.gms.common.server.response.FastJsonResponse.a
    @RecentlyNonNull
    public final /* synthetic */ String L0(@RecentlyNonNull Integer num) {
        String str = this.g0.get(num.intValue());
        return (str == null && this.f0.containsKey("gms_unknown")) ? "gms_unknown" : str;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        int a = yb3.a(parcel);
        yb3.m(parcel, 1, this.a);
        ArrayList arrayList = new ArrayList();
        for (String str : this.f0.keySet()) {
            arrayList.add(new zaa(str, this.f0.get(str).intValue()));
        }
        yb3.w(parcel, 2, arrayList, false);
        yb3.b(parcel, a);
    }

    /* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
    /* loaded from: classes.dex */
    public static final class zaa extends AbstractSafeParcelable {
        public static final Parcelable.Creator<zaa> CREATOR = new b();
        public final int a;
        public final String f0;
        public final int g0;

        public zaa(int i, String str, int i2) {
            this.a = i;
            this.f0 = str;
            this.g0 = i2;
        }

        @Override // android.os.Parcelable
        public final void writeToParcel(Parcel parcel, int i) {
            int a = yb3.a(parcel);
            yb3.m(parcel, 1, this.a);
            yb3.s(parcel, 2, this.f0, false);
            yb3.m(parcel, 3, this.g0);
            yb3.b(parcel, a);
        }

        public zaa(String str, int i) {
            this.a = 1;
            this.f0 = str;
            this.g0 = i;
        }
    }

    public StringToIntConverter() {
        this.a = 1;
        this.f0 = new HashMap<>();
        this.g0 = new SparseArray<>();
    }
}
