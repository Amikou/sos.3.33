package com.google.android.gms.common;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.RecentlyNonNull;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* loaded from: classes.dex */
public class Feature extends AbstractSafeParcelable {
    @RecentlyNonNull
    public static final Parcelable.Creator<Feature> CREATOR = new n75();
    public final String a;
    @Deprecated
    public final int f0;
    public final long g0;

    public Feature(@RecentlyNonNull String str, @RecentlyNonNull int i, @RecentlyNonNull long j) {
        this.a = str;
        this.f0 = i;
        this.g0 = j;
    }

    @RecentlyNonNull
    public String I1() {
        return this.a;
    }

    @RecentlyNonNull
    public long J1() {
        long j = this.g0;
        return j == -1 ? this.f0 : j;
    }

    @RecentlyNonNull
    public boolean equals(Object obj) {
        if (obj instanceof Feature) {
            Feature feature = (Feature) obj;
            if (((I1() != null && I1().equals(feature.I1())) || (I1() == null && feature.I1() == null)) && J1() == feature.J1()) {
                return true;
            }
        }
        return false;
    }

    @RecentlyNonNull
    public int hashCode() {
        return pl2.b(I1(), Long.valueOf(J1()));
    }

    @RecentlyNonNull
    public String toString() {
        return pl2.c(this).a(PublicResolver.FUNC_NAME, I1()).a("version", Long.valueOf(J1())).toString();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        int a = yb3.a(parcel);
        yb3.s(parcel, 1, I1(), false);
        yb3.m(parcel, 2, this.f0);
        yb3.o(parcel, 3, J1());
        yb3.b(parcel, a);
    }
}
