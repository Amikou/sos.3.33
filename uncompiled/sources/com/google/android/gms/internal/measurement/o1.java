package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.o1;
import defpackage.zq5;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* loaded from: classes.dex */
public abstract class o1<MessageType extends zq5<MessageType, BuilderType>, BuilderType extends o1<MessageType, BuilderType>> implements y1 {
    @Override // com.google.android.gms.internal.measurement.y1
    public final /* bridge */ /* synthetic */ y1 H0(byte[] bArr) throws zzkn {
        return h(bArr, 0, bArr.length);
    }

    @Override // com.google.android.gms.internal.measurement.y1
    public final /* bridge */ /* synthetic */ y1 J0(byte[] bArr, qt5 qt5Var) throws zzkn {
        return j(bArr, 0, bArr.length, qt5Var);
    }

    public abstract BuilderType h(byte[] bArr, int i, int i2) throws zzkn;

    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.google.android.gms.internal.measurement.y1
    public final /* bridge */ /* synthetic */ y1 i0(z1 z1Var) {
        if (g().getClass().isInstance(z1Var)) {
            return k((zq5) z1Var);
        }
        throw new IllegalArgumentException("mergeFrom(MessageLite) can only merge messages of the same type.");
    }

    public abstract BuilderType j(byte[] bArr, int i, int i2, qt5 qt5Var) throws zzkn;

    public abstract BuilderType k(MessageType messagetype);
}
