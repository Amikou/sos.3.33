package com.google.android.gms.internal.measurement;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* loaded from: classes.dex */
public final class b {
    public a a;
    public a b;
    public final List<a> c;

    public b() {
        this.a = new a("", 0L, null);
        this.b = new a("", 0L, null);
        this.c = new ArrayList();
    }

    public final a a() {
        return this.a;
    }

    public final void b(a aVar) {
        this.a = aVar;
        this.b = aVar.clone();
        this.c.clear();
    }

    public final a c() {
        return this.b;
    }

    public final /* bridge */ /* synthetic */ Object clone() throws CloneNotSupportedException {
        b bVar = new b(this.a.clone());
        for (a aVar : this.c) {
            bVar.c.add(aVar.clone());
        }
        return bVar;
    }

    public final void d(a aVar) {
        this.b = aVar;
    }

    public final void e(String str, long j, Map<String, Object> map) {
        this.c.add(new a(str, j, map));
    }

    public final List<a> f() {
        return this.c;
    }

    public b(a aVar) {
        this.a = aVar;
        this.b = aVar.clone();
        this.c = new ArrayList();
    }
}
