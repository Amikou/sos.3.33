package com.google.android.gms.internal.measurement;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* loaded from: classes.dex */
public enum zzgs implements nv5 {
    UNKNOWN(0),
    STRING(1),
    NUMBER(2),
    BOOLEAN(3),
    STATEMENT(4);
    
    private final int zzg;

    static {
        new Object() { // from class: wl5
        };
    }

    zzgs(int i) {
        this.zzg = i;
    }

    public static zzgs zza(int i) {
        if (i != 0) {
            if (i != 1) {
                if (i != 2) {
                    if (i != 3) {
                        if (i != 4) {
                            return null;
                        }
                        return STATEMENT;
                    }
                    return BOOLEAN;
                }
                return NUMBER;
            }
            return STRING;
        }
        return UNKNOWN;
    }

    public static sv5 zzb() {
        return xl5.a;
    }

    @Override // java.lang.Enum
    public final String toString() {
        return "<" + zzgs.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.zzg + " name=" + name() + '>';
    }
}
