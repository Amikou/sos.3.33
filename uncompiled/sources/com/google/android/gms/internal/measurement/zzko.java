package com.google.android.gms.internal.measurement;

import com.github.mikephil.charting.utils.Utils;

/* JADX WARN: Enum visitor error
jadx.core.utils.exceptions.JadxRuntimeException: Init of enum zzb uses external variables
	at jadx.core.dex.visitors.EnumVisitor.createEnumFieldByConstructor(EnumVisitor.java:444)
	at jadx.core.dex.visitors.EnumVisitor.processEnumFieldByRegister(EnumVisitor.java:391)
	at jadx.core.dex.visitors.EnumVisitor.extractEnumFieldsFromFilledArray(EnumVisitor.java:320)
	at jadx.core.dex.visitors.EnumVisitor.extractEnumFieldsFromInsn(EnumVisitor.java:258)
	at jadx.core.dex.visitors.EnumVisitor.convertToEnum(EnumVisitor.java:151)
	at jadx.core.dex.visitors.EnumVisitor.visit(EnumVisitor.java:100)
 */
/* JADX WARN: Failed to restore enum class, 'enum' modifier and super class removed */
/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* loaded from: classes.dex */
public final class zzko {
    public static final /* synthetic */ zzko[] a;
    public static final zzko zza;
    public static final zzko zzb;
    public static final zzko zzc;
    public static final zzko zzd;
    public static final zzko zze;
    public static final zzko zzf;
    public static final zzko zzg;
    public static final zzko zzh;
    public static final zzko zzi;
    public static final zzko zzj;
    private final Class<?> zzk;
    private final Class<?> zzl;
    private final Object zzm;

    static {
        zzko zzkoVar = new zzko("VOID", 0, Void.class, Void.class, null);
        zza = zzkoVar;
        Class cls = Integer.TYPE;
        zzko zzkoVar2 = new zzko("INT", 1, cls, Integer.class, 0);
        zzb = zzkoVar2;
        zzko zzkoVar3 = new zzko("LONG", 2, Long.TYPE, Long.class, 0L);
        zzc = zzkoVar3;
        zzko zzkoVar4 = new zzko("FLOAT", 3, Float.TYPE, Float.class, Float.valueOf((float) Utils.FLOAT_EPSILON));
        zzd = zzkoVar4;
        zzko zzkoVar5 = new zzko("DOUBLE", 4, Double.TYPE, Double.class, Double.valueOf((double) Utils.DOUBLE_EPSILON));
        zze = zzkoVar5;
        zzko zzkoVar6 = new zzko("BOOLEAN", 5, Boolean.TYPE, Boolean.class, Boolean.FALSE);
        zzf = zzkoVar6;
        zzko zzkoVar7 = new zzko("STRING", 6, String.class, String.class, "");
        zzg = zzkoVar7;
        zzko zzkoVar8 = new zzko("BYTE_STRING", 7, zzjd.class, zzjd.class, zzjd.zzb);
        zzh = zzkoVar8;
        zzko zzkoVar9 = new zzko("ENUM", 8, cls, Integer.class, null);
        zzi = zzkoVar9;
        zzko zzkoVar10 = new zzko("MESSAGE", 9, Object.class, Object.class, null);
        zzj = zzkoVar10;
        a = new zzko[]{zzkoVar, zzkoVar2, zzkoVar3, zzkoVar4, zzkoVar5, zzkoVar6, zzkoVar7, zzkoVar8, zzkoVar9, zzkoVar10};
    }

    public zzko(String str, int i, Class cls, Class cls2, Object obj) {
        this.zzk = cls;
        this.zzl = cls2;
        this.zzm = obj;
    }

    public static zzko[] values() {
        return (zzko[]) a.clone();
    }

    public final Class<?> zza() {
        return this.zzl;
    }
}
