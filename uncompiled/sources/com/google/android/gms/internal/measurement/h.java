package com.google.android.gms.internal.measurement;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* loaded from: classes.dex */
public final class h extends c implements j {
    public h(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.measurement.api.internal.IAppMeasurementDynamiteService");
    }

    @Override // com.google.android.gms.internal.measurement.j
    public final void beginAdUnitExposure(String str, long j) throws RemoteException {
        Parcel F1 = F1();
        F1.writeString(str);
        F1.writeLong(j);
        G1(23, F1);
    }

    @Override // com.google.android.gms.internal.measurement.j
    public final void clearConditionalUserProperty(String str, String str2, Bundle bundle) throws RemoteException {
        Parcel F1 = F1();
        F1.writeString(str);
        F1.writeString(str2);
        a95.d(F1, bundle);
        G1(9, F1);
    }

    @Override // com.google.android.gms.internal.measurement.j
    public final void endAdUnitExposure(String str, long j) throws RemoteException {
        Parcel F1 = F1();
        F1.writeString(str);
        F1.writeLong(j);
        G1(24, F1);
    }

    @Override // com.google.android.gms.internal.measurement.j
    public final void generateEventId(m mVar) throws RemoteException {
        Parcel F1 = F1();
        a95.e(F1, mVar);
        G1(22, F1);
    }

    @Override // com.google.android.gms.internal.measurement.j
    public final void getCachedAppInstanceId(m mVar) throws RemoteException {
        Parcel F1 = F1();
        a95.e(F1, mVar);
        G1(19, F1);
    }

    @Override // com.google.android.gms.internal.measurement.j
    public final void getConditionalUserProperties(String str, String str2, m mVar) throws RemoteException {
        Parcel F1 = F1();
        F1.writeString(str);
        F1.writeString(str2);
        a95.e(F1, mVar);
        G1(10, F1);
    }

    @Override // com.google.android.gms.internal.measurement.j
    public final void getCurrentScreenClass(m mVar) throws RemoteException {
        Parcel F1 = F1();
        a95.e(F1, mVar);
        G1(17, F1);
    }

    @Override // com.google.android.gms.internal.measurement.j
    public final void getCurrentScreenName(m mVar) throws RemoteException {
        Parcel F1 = F1();
        a95.e(F1, mVar);
        G1(16, F1);
    }

    @Override // com.google.android.gms.internal.measurement.j
    public final void getGmpAppId(m mVar) throws RemoteException {
        Parcel F1 = F1();
        a95.e(F1, mVar);
        G1(21, F1);
    }

    @Override // com.google.android.gms.internal.measurement.j
    public final void getMaxUserProperties(String str, m mVar) throws RemoteException {
        Parcel F1 = F1();
        F1.writeString(str);
        a95.e(F1, mVar);
        G1(6, F1);
    }

    @Override // com.google.android.gms.internal.measurement.j
    public final void getUserProperties(String str, String str2, boolean z, m mVar) throws RemoteException {
        Parcel F1 = F1();
        F1.writeString(str);
        F1.writeString(str2);
        a95.b(F1, z);
        a95.e(F1, mVar);
        G1(5, F1);
    }

    @Override // com.google.android.gms.internal.measurement.j
    public final void initialize(lm1 lm1Var, zzcl zzclVar, long j) throws RemoteException {
        Parcel F1 = F1();
        a95.e(F1, lm1Var);
        a95.d(F1, zzclVar);
        F1.writeLong(j);
        G1(1, F1);
    }

    @Override // com.google.android.gms.internal.measurement.j
    public final void logEvent(String str, String str2, Bundle bundle, boolean z, boolean z2, long j) throws RemoteException {
        Parcel F1 = F1();
        F1.writeString(str);
        F1.writeString(str2);
        a95.d(F1, bundle);
        a95.b(F1, z);
        a95.b(F1, z2);
        F1.writeLong(j);
        G1(2, F1);
    }

    @Override // com.google.android.gms.internal.measurement.j
    public final void logHealthData(int i, String str, lm1 lm1Var, lm1 lm1Var2, lm1 lm1Var3) throws RemoteException {
        Parcel F1 = F1();
        F1.writeInt(5);
        F1.writeString(str);
        a95.e(F1, lm1Var);
        a95.e(F1, lm1Var2);
        a95.e(F1, lm1Var3);
        G1(33, F1);
    }

    @Override // com.google.android.gms.internal.measurement.j
    public final void onActivityCreated(lm1 lm1Var, Bundle bundle, long j) throws RemoteException {
        Parcel F1 = F1();
        a95.e(F1, lm1Var);
        a95.d(F1, bundle);
        F1.writeLong(j);
        G1(27, F1);
    }

    @Override // com.google.android.gms.internal.measurement.j
    public final void onActivityDestroyed(lm1 lm1Var, long j) throws RemoteException {
        Parcel F1 = F1();
        a95.e(F1, lm1Var);
        F1.writeLong(j);
        G1(28, F1);
    }

    @Override // com.google.android.gms.internal.measurement.j
    public final void onActivityPaused(lm1 lm1Var, long j) throws RemoteException {
        Parcel F1 = F1();
        a95.e(F1, lm1Var);
        F1.writeLong(j);
        G1(29, F1);
    }

    @Override // com.google.android.gms.internal.measurement.j
    public final void onActivityResumed(lm1 lm1Var, long j) throws RemoteException {
        Parcel F1 = F1();
        a95.e(F1, lm1Var);
        F1.writeLong(j);
        G1(30, F1);
    }

    @Override // com.google.android.gms.internal.measurement.j
    public final void onActivitySaveInstanceState(lm1 lm1Var, m mVar, long j) throws RemoteException {
        Parcel F1 = F1();
        a95.e(F1, lm1Var);
        a95.e(F1, mVar);
        F1.writeLong(j);
        G1(31, F1);
    }

    @Override // com.google.android.gms.internal.measurement.j
    public final void onActivityStarted(lm1 lm1Var, long j) throws RemoteException {
        Parcel F1 = F1();
        a95.e(F1, lm1Var);
        F1.writeLong(j);
        G1(25, F1);
    }

    @Override // com.google.android.gms.internal.measurement.j
    public final void onActivityStopped(lm1 lm1Var, long j) throws RemoteException {
        Parcel F1 = F1();
        a95.e(F1, lm1Var);
        F1.writeLong(j);
        G1(26, F1);
    }

    @Override // com.google.android.gms.internal.measurement.j
    public final void registerOnMeasurementEventListener(p pVar) throws RemoteException {
        Parcel F1 = F1();
        a95.e(F1, pVar);
        G1(35, F1);
    }

    @Override // com.google.android.gms.internal.measurement.j
    public final void setConditionalUserProperty(Bundle bundle, long j) throws RemoteException {
        Parcel F1 = F1();
        a95.d(F1, bundle);
        F1.writeLong(j);
        G1(8, F1);
    }

    @Override // com.google.android.gms.internal.measurement.j
    public final void setCurrentScreen(lm1 lm1Var, String str, String str2, long j) throws RemoteException {
        Parcel F1 = F1();
        a95.e(F1, lm1Var);
        F1.writeString(str);
        F1.writeString(str2);
        F1.writeLong(j);
        G1(15, F1);
    }

    @Override // com.google.android.gms.internal.measurement.j
    public final void setDataCollectionEnabled(boolean z) throws RemoteException {
        Parcel F1 = F1();
        a95.b(F1, z);
        G1(39, F1);
    }

    @Override // com.google.android.gms.internal.measurement.j
    public final void setUserProperty(String str, String str2, lm1 lm1Var, boolean z, long j) throws RemoteException {
        Parcel F1 = F1();
        F1.writeString(str);
        F1.writeString(str2);
        a95.e(F1, lm1Var);
        a95.b(F1, z);
        F1.writeLong(j);
        G1(4, F1);
    }
}
