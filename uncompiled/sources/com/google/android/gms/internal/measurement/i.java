package com.google.android.gms.internal.measurement;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import defpackage.lm1;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* loaded from: classes.dex */
public abstract class i extends d implements j {
    public i() {
        super("com.google.android.gms.measurement.api.internal.IAppMeasurementDynamiteService");
    }

    public static j asInterface(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.measurement.api.internal.IAppMeasurementDynamiteService");
        if (queryLocalInterface instanceof j) {
            return (j) queryLocalInterface;
        }
        return new h(iBinder);
    }

    @Override // com.google.android.gms.internal.measurement.d
    public final boolean b(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        m kVar;
        m mVar;
        m mVar2 = null;
        m mVar3 = null;
        m mVar4 = null;
        p pVar = null;
        p pVar2 = null;
        p pVar3 = null;
        m mVar5 = null;
        m mVar6 = null;
        m mVar7 = null;
        m mVar8 = null;
        m mVar9 = null;
        m mVar10 = null;
        kb5 kb5Var = null;
        m mVar11 = null;
        m mVar12 = null;
        m mVar13 = null;
        m mVar14 = null;
        switch (i) {
            case 1:
                initialize(lm1.a.F1(parcel.readStrongBinder()), (zzcl) a95.c(parcel, zzcl.CREATOR), parcel.readLong());
                break;
            case 2:
                logEvent(parcel.readString(), parcel.readString(), (Bundle) a95.c(parcel, Bundle.CREATOR), a95.a(parcel), a95.a(parcel), parcel.readLong());
                break;
            case 3:
                String readString = parcel.readString();
                String readString2 = parcel.readString();
                Bundle bundle = (Bundle) a95.c(parcel, Bundle.CREATOR);
                IBinder readStrongBinder = parcel.readStrongBinder();
                if (readStrongBinder == null) {
                    mVar = null;
                } else {
                    IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface instanceof m) {
                        kVar = (m) queryLocalInterface;
                    } else {
                        kVar = new k(readStrongBinder);
                    }
                    mVar = kVar;
                }
                logEventAndBundle(readString, readString2, bundle, mVar, parcel.readLong());
                break;
            case 4:
                setUserProperty(parcel.readString(), parcel.readString(), lm1.a.F1(parcel.readStrongBinder()), a95.a(parcel), parcel.readLong());
                break;
            case 5:
                String readString3 = parcel.readString();
                String readString4 = parcel.readString();
                boolean a = a95.a(parcel);
                IBinder readStrongBinder2 = parcel.readStrongBinder();
                if (readStrongBinder2 != null) {
                    IInterface queryLocalInterface2 = readStrongBinder2.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface2 instanceof m) {
                        mVar2 = (m) queryLocalInterface2;
                    } else {
                        mVar2 = new k(readStrongBinder2);
                    }
                }
                getUserProperties(readString3, readString4, a, mVar2);
                break;
            case 6:
                String readString5 = parcel.readString();
                IBinder readStrongBinder3 = parcel.readStrongBinder();
                if (readStrongBinder3 != null) {
                    IInterface queryLocalInterface3 = readStrongBinder3.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface3 instanceof m) {
                        mVar14 = (m) queryLocalInterface3;
                    } else {
                        mVar14 = new k(readStrongBinder3);
                    }
                }
                getMaxUserProperties(readString5, mVar14);
                break;
            case 7:
                setUserId(parcel.readString(), parcel.readLong());
                break;
            case 8:
                setConditionalUserProperty((Bundle) a95.c(parcel, Bundle.CREATOR), parcel.readLong());
                break;
            case 9:
                clearConditionalUserProperty(parcel.readString(), parcel.readString(), (Bundle) a95.c(parcel, Bundle.CREATOR));
                break;
            case 10:
                String readString6 = parcel.readString();
                String readString7 = parcel.readString();
                IBinder readStrongBinder4 = parcel.readStrongBinder();
                if (readStrongBinder4 != null) {
                    IInterface queryLocalInterface4 = readStrongBinder4.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface4 instanceof m) {
                        mVar13 = (m) queryLocalInterface4;
                    } else {
                        mVar13 = new k(readStrongBinder4);
                    }
                }
                getConditionalUserProperties(readString6, readString7, mVar13);
                break;
            case 11:
                setMeasurementEnabled(a95.a(parcel), parcel.readLong());
                break;
            case 12:
                resetAnalyticsData(parcel.readLong());
                break;
            case 13:
                setMinimumSessionDuration(parcel.readLong());
                break;
            case 14:
                setSessionTimeoutDuration(parcel.readLong());
                break;
            case 15:
                setCurrentScreen(lm1.a.F1(parcel.readStrongBinder()), parcel.readString(), parcel.readString(), parcel.readLong());
                break;
            case 16:
                IBinder readStrongBinder5 = parcel.readStrongBinder();
                if (readStrongBinder5 != null) {
                    IInterface queryLocalInterface5 = readStrongBinder5.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface5 instanceof m) {
                        mVar12 = (m) queryLocalInterface5;
                    } else {
                        mVar12 = new k(readStrongBinder5);
                    }
                }
                getCurrentScreenName(mVar12);
                break;
            case 17:
                IBinder readStrongBinder6 = parcel.readStrongBinder();
                if (readStrongBinder6 != null) {
                    IInterface queryLocalInterface6 = readStrongBinder6.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface6 instanceof m) {
                        mVar11 = (m) queryLocalInterface6;
                    } else {
                        mVar11 = new k(readStrongBinder6);
                    }
                }
                getCurrentScreenClass(mVar11);
                break;
            case 18:
                IBinder readStrongBinder7 = parcel.readStrongBinder();
                if (readStrongBinder7 != null) {
                    IInterface queryLocalInterface7 = readStrongBinder7.queryLocalInterface("com.google.android.gms.measurement.api.internal.IStringProvider");
                    if (queryLocalInterface7 instanceof kb5) {
                        kb5Var = (kb5) queryLocalInterface7;
                    } else {
                        kb5Var = new hb5(readStrongBinder7);
                    }
                }
                setInstanceIdProvider(kb5Var);
                break;
            case 19:
                IBinder readStrongBinder8 = parcel.readStrongBinder();
                if (readStrongBinder8 != null) {
                    IInterface queryLocalInterface8 = readStrongBinder8.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface8 instanceof m) {
                        mVar10 = (m) queryLocalInterface8;
                    } else {
                        mVar10 = new k(readStrongBinder8);
                    }
                }
                getCachedAppInstanceId(mVar10);
                break;
            case 20:
                IBinder readStrongBinder9 = parcel.readStrongBinder();
                if (readStrongBinder9 != null) {
                    IInterface queryLocalInterface9 = readStrongBinder9.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface9 instanceof m) {
                        mVar9 = (m) queryLocalInterface9;
                    } else {
                        mVar9 = new k(readStrongBinder9);
                    }
                }
                getAppInstanceId(mVar9);
                break;
            case 21:
                IBinder readStrongBinder10 = parcel.readStrongBinder();
                if (readStrongBinder10 != null) {
                    IInterface queryLocalInterface10 = readStrongBinder10.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface10 instanceof m) {
                        mVar8 = (m) queryLocalInterface10;
                    } else {
                        mVar8 = new k(readStrongBinder10);
                    }
                }
                getGmpAppId(mVar8);
                break;
            case 22:
                IBinder readStrongBinder11 = parcel.readStrongBinder();
                if (readStrongBinder11 != null) {
                    IInterface queryLocalInterface11 = readStrongBinder11.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface11 instanceof m) {
                        mVar7 = (m) queryLocalInterface11;
                    } else {
                        mVar7 = new k(readStrongBinder11);
                    }
                }
                generateEventId(mVar7);
                break;
            case 23:
                beginAdUnitExposure(parcel.readString(), parcel.readLong());
                break;
            case 24:
                endAdUnitExposure(parcel.readString(), parcel.readLong());
                break;
            case 25:
                onActivityStarted(lm1.a.F1(parcel.readStrongBinder()), parcel.readLong());
                break;
            case 26:
                onActivityStopped(lm1.a.F1(parcel.readStrongBinder()), parcel.readLong());
                break;
            case 27:
                onActivityCreated(lm1.a.F1(parcel.readStrongBinder()), (Bundle) a95.c(parcel, Bundle.CREATOR), parcel.readLong());
                break;
            case 28:
                onActivityDestroyed(lm1.a.F1(parcel.readStrongBinder()), parcel.readLong());
                break;
            case 29:
                onActivityPaused(lm1.a.F1(parcel.readStrongBinder()), parcel.readLong());
                break;
            case 30:
                onActivityResumed(lm1.a.F1(parcel.readStrongBinder()), parcel.readLong());
                break;
            case 31:
                lm1 F1 = lm1.a.F1(parcel.readStrongBinder());
                IBinder readStrongBinder12 = parcel.readStrongBinder();
                if (readStrongBinder12 != null) {
                    IInterface queryLocalInterface12 = readStrongBinder12.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface12 instanceof m) {
                        mVar6 = (m) queryLocalInterface12;
                    } else {
                        mVar6 = new k(readStrongBinder12);
                    }
                }
                onActivitySaveInstanceState(F1, mVar6, parcel.readLong());
                break;
            case 32:
                Bundle bundle2 = (Bundle) a95.c(parcel, Bundle.CREATOR);
                IBinder readStrongBinder13 = parcel.readStrongBinder();
                if (readStrongBinder13 != null) {
                    IInterface queryLocalInterface13 = readStrongBinder13.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface13 instanceof m) {
                        mVar5 = (m) queryLocalInterface13;
                    } else {
                        mVar5 = new k(readStrongBinder13);
                    }
                }
                performAction(bundle2, mVar5, parcel.readLong());
                break;
            case 33:
                logHealthData(parcel.readInt(), parcel.readString(), lm1.a.F1(parcel.readStrongBinder()), lm1.a.F1(parcel.readStrongBinder()), lm1.a.F1(parcel.readStrongBinder()));
                break;
            case 34:
                IBinder readStrongBinder14 = parcel.readStrongBinder();
                if (readStrongBinder14 != null) {
                    IInterface queryLocalInterface14 = readStrongBinder14.queryLocalInterface("com.google.android.gms.measurement.api.internal.IEventHandlerProxy");
                    if (queryLocalInterface14 instanceof p) {
                        pVar3 = (p) queryLocalInterface14;
                    } else {
                        pVar3 = new n(readStrongBinder14);
                    }
                }
                setEventInterceptor(pVar3);
                break;
            case 35:
                IBinder readStrongBinder15 = parcel.readStrongBinder();
                if (readStrongBinder15 != null) {
                    IInterface queryLocalInterface15 = readStrongBinder15.queryLocalInterface("com.google.android.gms.measurement.api.internal.IEventHandlerProxy");
                    if (queryLocalInterface15 instanceof p) {
                        pVar2 = (p) queryLocalInterface15;
                    } else {
                        pVar2 = new n(readStrongBinder15);
                    }
                }
                registerOnMeasurementEventListener(pVar2);
                break;
            case 36:
                IBinder readStrongBinder16 = parcel.readStrongBinder();
                if (readStrongBinder16 != null) {
                    IInterface queryLocalInterface16 = readStrongBinder16.queryLocalInterface("com.google.android.gms.measurement.api.internal.IEventHandlerProxy");
                    if (queryLocalInterface16 instanceof p) {
                        pVar = (p) queryLocalInterface16;
                    } else {
                        pVar = new n(readStrongBinder16);
                    }
                }
                unregisterOnMeasurementEventListener(pVar);
                break;
            case 37:
                initForTests(a95.f(parcel));
                break;
            case 38:
                IBinder readStrongBinder17 = parcel.readStrongBinder();
                if (readStrongBinder17 != null) {
                    IInterface queryLocalInterface17 = readStrongBinder17.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface17 instanceof m) {
                        mVar4 = (m) queryLocalInterface17;
                    } else {
                        mVar4 = new k(readStrongBinder17);
                    }
                }
                getTestFlag(mVar4, parcel.readInt());
                break;
            case 39:
                setDataCollectionEnabled(a95.a(parcel));
                break;
            case 40:
                IBinder readStrongBinder18 = parcel.readStrongBinder();
                if (readStrongBinder18 != null) {
                    IInterface queryLocalInterface18 = readStrongBinder18.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface18 instanceof m) {
                        mVar3 = (m) queryLocalInterface18;
                    } else {
                        mVar3 = new k(readStrongBinder18);
                    }
                }
                isDataCollectionEnabled(mVar3);
                break;
            case 41:
            default:
                return false;
            case 42:
                setDefaultEventParameters((Bundle) a95.c(parcel, Bundle.CREATOR));
                break;
            case 43:
                clearMeasurementEnabled(parcel.readLong());
                break;
            case 44:
                setConsent((Bundle) a95.c(parcel, Bundle.CREATOR), parcel.readLong());
                break;
            case 45:
                setConsentThirdParty((Bundle) a95.c(parcel, Bundle.CREATOR), parcel.readLong());
                break;
        }
        parcel2.writeNoException();
        return true;
    }
}
