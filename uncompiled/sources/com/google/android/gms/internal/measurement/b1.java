package com.google.android.gms.internal.measurement;

import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* loaded from: classes.dex */
public final class b1 extends x1<b1, tj5> implements xx5 {
    private static final b1 zzj;
    private int zza;
    private zv5<d1> zze = x1.o();
    private String zzf = "";
    private long zzg;
    private long zzh;
    private int zzi;

    static {
        b1 b1Var = new b1();
        zzj = b1Var;
        x1.u(b1.class, b1Var);
    }

    public static tj5 H() {
        return zzj.r();
    }

    public static /* synthetic */ void J(b1 b1Var, int i, d1 d1Var) {
        d1Var.getClass();
        b1Var.R();
        b1Var.zze.set(i, d1Var);
    }

    public static /* synthetic */ void K(b1 b1Var, d1 d1Var) {
        d1Var.getClass();
        b1Var.R();
        b1Var.zze.add(d1Var);
    }

    public static /* synthetic */ void L(b1 b1Var, Iterable iterable) {
        b1Var.R();
        zq5.j(iterable, b1Var.zze);
    }

    public static /* synthetic */ void N(b1 b1Var, int i) {
        b1Var.R();
        b1Var.zze.remove(i);
    }

    public static /* synthetic */ void O(b1 b1Var, String str) {
        str.getClass();
        b1Var.zza |= 1;
        b1Var.zzf = str;
    }

    public static /* synthetic */ void P(b1 b1Var, long j) {
        b1Var.zza |= 2;
        b1Var.zzg = j;
    }

    public static /* synthetic */ void Q(b1 b1Var, long j) {
        b1Var.zza |= 4;
        b1Var.zzh = j;
    }

    public final String A() {
        return this.zzf;
    }

    public final boolean B() {
        return (this.zza & 2) != 0;
    }

    public final long C() {
        return this.zzg;
    }

    public final boolean D() {
        return (this.zza & 4) != 0;
    }

    public final long E() {
        return this.zzh;
    }

    public final boolean F() {
        return (this.zza & 8) != 0;
    }

    public final int G() {
        return this.zzi;
    }

    public final void R() {
        zv5<d1> zv5Var = this.zze;
        if (zv5Var.zza()) {
            return;
        }
        this.zze = x1.p(zv5Var);
    }

    @Override // com.google.android.gms.internal.measurement.x1
    public final Object w(int i, Object obj, Object obj2) {
        int i2 = i - 1;
        if (i2 != 0) {
            if (i2 != 2) {
                if (i2 != 3) {
                    if (i2 != 4) {
                        if (i2 != 5) {
                            return null;
                        }
                        return zzj;
                    }
                    return new tj5(null);
                }
                return new b1();
            }
            return x1.v(zzj, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0001\u0000\u0001\u001b\u0002ဈ\u0000\u0003ဂ\u0001\u0004ဂ\u0002\u0005င\u0003", new Object[]{"zza", "zze", d1.class, "zzf", "zzg", "zzh", "zzi"});
        }
        return (byte) 1;
    }

    public final List<d1> x() {
        return this.zze;
    }

    public final int y() {
        return this.zze.size();
    }

    public final d1 z(int i) {
        return this.zze.get(i);
    }
}
