package com.google.android.gms.internal.measurement;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* loaded from: classes.dex */
public final class z0 extends x1<z0, jj5> implements xx5 {
    private static final z0 zzi;
    private int zza;
    private int zze;
    private h1 zzf;
    private h1 zzg;
    private boolean zzh;

    static {
        z0 z0Var = new z0();
        zzi = z0Var;
        x1.u(z0.class, z0Var);
    }

    public static jj5 E() {
        return zzi.r();
    }

    public static /* synthetic */ void G(z0 z0Var, int i) {
        z0Var.zza |= 1;
        z0Var.zze = i;
    }

    public static /* synthetic */ void H(z0 z0Var, h1 h1Var) {
        h1Var.getClass();
        z0Var.zzf = h1Var;
        z0Var.zza |= 2;
    }

    public static /* synthetic */ void I(z0 z0Var, h1 h1Var) {
        z0Var.zzg = h1Var;
        z0Var.zza |= 4;
    }

    public static /* synthetic */ void J(z0 z0Var, boolean z) {
        z0Var.zza |= 8;
        z0Var.zzh = z;
    }

    public final boolean A() {
        return (this.zza & 4) != 0;
    }

    public final h1 B() {
        h1 h1Var = this.zzg;
        return h1Var == null ? h1.I() : h1Var;
    }

    public final boolean C() {
        return (this.zza & 8) != 0;
    }

    public final boolean D() {
        return this.zzh;
    }

    @Override // com.google.android.gms.internal.measurement.x1
    public final Object w(int i, Object obj, Object obj2) {
        int i2 = i - 1;
        if (i2 != 0) {
            if (i2 != 2) {
                if (i2 != 3) {
                    if (i2 != 4) {
                        if (i2 != 5) {
                            return null;
                        }
                        return zzi;
                    }
                    return new jj5(null);
                }
                return new z0();
            }
            return x1.v(zzi, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0000\u0000\u0001င\u0000\u0002ဉ\u0001\u0003ဉ\u0002\u0004ဇ\u0003", new Object[]{"zza", "zze", "zzf", "zzg", "zzh"});
        }
        return (byte) 1;
    }

    public final boolean x() {
        return (this.zza & 1) != 0;
    }

    public final int y() {
        return this.zze;
    }

    public final h1 z() {
        h1 h1Var = this.zzf;
        return h1Var == null ? h1.I() : h1Var;
    }
}
