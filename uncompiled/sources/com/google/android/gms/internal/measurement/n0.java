package com.google.android.gms.internal.measurement;

import android.app.Activity;
import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@19.0.0 */
/* loaded from: classes.dex */
public final class n0 extends i0 {
    public final /* synthetic */ Activity i0;
    public final /* synthetic */ uf5 j0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public n0(uf5 uf5Var, Activity activity) {
        super(uf5Var.a, true);
        this.j0 = uf5Var;
        this.i0 = activity;
    }

    @Override // com.google.android.gms.internal.measurement.i0
    public final void a() throws RemoteException {
        j jVar;
        jVar = this.j0.a.h;
        ((j) zt2.j(jVar)).onActivityStopped(nl2.H1(this.i0), this.f0);
    }
}
