package com.google.android.gms.internal.measurement;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.RandomAccess;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* loaded from: classes.dex */
public final class d2 {
    public static final Class<?> a;
    public static final e2<?, ?> b;
    public static final e2<?, ?> c;
    public static final e2<?, ?> d;

    static {
        Class<?> cls;
        try {
            cls = Class.forName("com.google.protobuf.GeneratedMessage");
        } catch (Throwable unused) {
            cls = null;
        }
        a = cls;
        b = C(false);
        c = C(true);
        d = new g2();
    }

    public static void A(Class<?> cls) {
        Class<?> cls2;
        if (!x1.class.isAssignableFrom(cls) && (cls2 = a) != null && !cls2.isAssignableFrom(cls)) {
            throw new IllegalArgumentException("Message classes must extend GeneratedMessage or GeneratedMessageLite");
        }
    }

    public static void B(int i, List<?> list, u1 u1Var, c2 c2Var) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        for (int i2 = 0; i2 < list.size(); i2++) {
            u1Var.D(i, list.get(i2), c2Var);
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: BlockProcessor
        jadx.core.utils.exceptions.JadxRuntimeException: Unreachable block: B:5:0x0004
        	at jadx.core.dex.visitors.blocks.BlockProcessor.checkForUnreachableBlocks(BlockProcessor.java:81)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.processBlocksTree(BlockProcessor.java:47)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.visit(BlockProcessor.java:39)
        */
    public static com.google.android.gms.internal.measurement.e2<?, ?> C(boolean r6) {
        /*
            r0 = 0
            java.lang.Class<com.google.protobuf.j1> r1 = com.google.protobuf.j1.class
            goto L5
        L4:
            r1 = r0
        L5:
            if (r1 != 0) goto L8
            return r0
        L8:
            r2 = 1
            java.lang.Class[] r3 = new java.lang.Class[r2]     // Catch: java.lang.Throwable -> L23
            java.lang.Class r4 = java.lang.Boolean.TYPE     // Catch: java.lang.Throwable -> L23
            r5 = 0
            r3[r5] = r4     // Catch: java.lang.Throwable -> L23
            java.lang.reflect.Constructor r1 = r1.getConstructor(r3)     // Catch: java.lang.Throwable -> L23
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch: java.lang.Throwable -> L23
            java.lang.Boolean r6 = java.lang.Boolean.valueOf(r6)     // Catch: java.lang.Throwable -> L23
            r2[r5] = r6     // Catch: java.lang.Throwable -> L23
            java.lang.Object r6 = r1.newInstance(r2)     // Catch: java.lang.Throwable -> L23
            com.google.android.gms.internal.measurement.e2 r6 = (com.google.android.gms.internal.measurement.e2) r6     // Catch: java.lang.Throwable -> L23
            return r6
        L23:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.d2.C(boolean):com.google.android.gms.internal.measurement.e2");
    }

    public static int D(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof yw5) {
            yw5 yw5Var = (yw5) list;
            i = 0;
            while (i2 < size) {
                i += t1.B(yw5Var.z0(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += t1.B(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    public static int E(int i, List<Long> list, boolean z) {
        if (list.size() == 0) {
            return 0;
        }
        return D(list) + (list.size() * t1.y(i));
    }

    public static int F(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof yw5) {
            yw5 yw5Var = (yw5) list;
            i = 0;
            while (i2 < size) {
                i += t1.B(yw5Var.z0(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += t1.B(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    public static int G(int i, List<Long> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return F(list) + (size * t1.y(i));
    }

    public static int H(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof yw5) {
            yw5 yw5Var = (yw5) list;
            i = 0;
            while (i2 < size) {
                long z0 = yw5Var.z0(i2);
                i += t1.B((z0 >> 63) ^ (z0 + z0));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                long longValue = list.get(i2).longValue();
                i += t1.B((longValue >> 63) ^ (longValue + longValue));
                i2++;
            }
        }
        return i;
    }

    public static int I(int i, List<Long> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return H(list) + (size * t1.y(i));
    }

    public static int J(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof kv5) {
            kv5 kv5Var = (kv5) list;
            i = 0;
            while (i2 < size) {
                i += t1.z(kv5Var.k(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += t1.z(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    public static int K(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return J(list) + (size * t1.y(i));
    }

    public static int L(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof kv5) {
            kv5 kv5Var = (kv5) list;
            i = 0;
            while (i2 < size) {
                i += t1.z(kv5Var.k(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += t1.z(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    public static int M(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return L(list) + (size * t1.y(i));
    }

    public static int N(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof kv5) {
            kv5 kv5Var = (kv5) list;
            i = 0;
            while (i2 < size) {
                i += t1.A(kv5Var.k(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += t1.A(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    public static int O(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return N(list) + (size * t1.y(i));
    }

    public static int P(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof kv5) {
            kv5 kv5Var = (kv5) list;
            i = 0;
            while (i2 < size) {
                int k = kv5Var.k(i2);
                i += t1.A((k >> 31) ^ (k + k));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                int intValue = list.get(i2).intValue();
                i += t1.A((intValue >> 31) ^ (intValue + intValue));
                i2++;
            }
        }
        return i;
    }

    public static int Q(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return P(list) + (size * t1.y(i));
    }

    public static int R(List<?> list) {
        return list.size() * 4;
    }

    public static int S(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return size * (t1.A(i << 3) + 4);
    }

    public static int T(List<?> list) {
        return list.size() * 8;
    }

    public static int U(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return size * (t1.A(i << 3) + 8);
    }

    public static int V(List<?> list) {
        return list.size();
    }

    public static int W(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return size * (t1.A(i << 3) + 1);
    }

    public static int X(int i, List<?> list) {
        int C;
        int C2;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        int y = t1.y(i) * size;
        if (list instanceof nw5) {
            nw5 nw5Var = (nw5) list;
            while (i2 < size) {
                Object v1 = nw5Var.v1(i2);
                if (v1 instanceof zzjd) {
                    C2 = t1.a((zzjd) v1);
                } else {
                    C2 = t1.C((String) v1);
                }
                y += C2;
                i2++;
            }
        } else {
            while (i2 < size) {
                Object obj = list.get(i2);
                if (obj instanceof zzjd) {
                    C = t1.a((zzjd) obj);
                } else {
                    C = t1.C((String) obj);
                }
                y += C;
                i2++;
            }
        }
        return y;
    }

    public static int Y(int i, Object obj, c2 c2Var) {
        if (obj instanceof iw5) {
            int A = t1.A(i << 3);
            int a2 = ((iw5) obj).a();
            return A + t1.A(a2) + a2;
        }
        return t1.A(i << 3) + t1.b((z1) obj, c2Var);
    }

    public static int Z(int i, List<?> list, c2 c2Var) {
        int b2;
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int y = t1.y(i) * size;
        for (int i2 = 0; i2 < size; i2++) {
            Object obj = list.get(i2);
            if (obj instanceof iw5) {
                b2 = t1.D((iw5) obj);
            } else {
                b2 = t1.b((z1) obj, c2Var);
            }
            y += b2;
        }
        return y;
    }

    public static e2<?, ?> a() {
        return b;
    }

    public static int a0(int i, List<zzjd> list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int y = size * t1.y(i);
        for (int i2 = 0; i2 < list.size(); i2++) {
            y += t1.a(list.get(i2));
        }
        return y;
    }

    public static e2<?, ?> b() {
        return c;
    }

    public static int b0(int i, List<z1> list, c2 c2Var) {
        int size = list.size();
        if (size != 0) {
            int i2 = 0;
            for (int i3 = 0; i3 < size; i3++) {
                i2 += t1.e(i, list.get(i3), c2Var);
            }
            return i2;
        }
        return 0;
    }

    public static e2<?, ?> c() {
        return d;
    }

    public static boolean d(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    public static <T, FT extends cu5<FT>> void e(tt5<FT> tt5Var, T t, T t2) {
        tt5Var.b(t2);
        throw null;
    }

    public static <T, UT, UB> void f(e2<UT, UB> e2Var, T t, T t2) {
        e2Var.c(t, e2Var.f(e2Var.d(t), e2Var.d(t2)));
    }

    public static <UT, UB> UB g(int i, List<Integer> list, sv5 sv5Var, UB ub, e2<UT, UB> e2Var) {
        if (sv5Var == null) {
            return ub;
        }
        if (list instanceof RandomAccess) {
            int size = list.size();
            int i2 = 0;
            for (int i3 = 0; i3 < size; i3++) {
                int intValue = list.get(i3).intValue();
                if (sv5Var.d(intValue)) {
                    if (i3 != i2) {
                        list.set(i2, Integer.valueOf(intValue));
                    }
                    i2++;
                } else {
                    ub = (UB) h(i, intValue, ub, e2Var);
                }
            }
            if (i2 != size) {
                list.subList(i2, size).clear();
                return ub;
            }
        } else {
            Iterator<Integer> it = list.iterator();
            while (it.hasNext()) {
                int intValue2 = it.next().intValue();
                if (!sv5Var.d(intValue2)) {
                    ub = (UB) h(i, intValue2, ub, e2Var);
                    it.remove();
                }
            }
        }
        return ub;
    }

    public static <UT, UB> UB h(int i, int i2, UB ub, e2<UT, UB> e2Var) {
        if (ub == null) {
            ub = e2Var.b();
        }
        e2Var.a(ub, i, i2);
        return ub;
    }

    public static <T> void i(ox5 ox5Var, T t, T t2, long j) {
        l2.t(t, j, ox5.b(l2.s(t, j), l2.s(t2, j)));
    }

    public static void j(int i, List<Double> list, u1 u1Var, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        u1Var.b(i, list, z);
    }

    public static void k(int i, List<Float> list, u1 u1Var, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        u1Var.a(i, list, z);
    }

    public static void l(int i, List<Long> list, u1 u1Var, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        u1Var.I(i, list, z);
    }

    public static void m(int i, List<Long> list, u1 u1Var, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        u1Var.J(i, list, z);
    }

    public static void n(int i, List<Long> list, u1 u1Var, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        u1Var.k(i, list, z);
    }

    public static void o(int i, List<Long> list, u1 u1Var, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        u1Var.K(i, list, z);
    }

    public static void p(int i, List<Long> list, u1 u1Var, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        u1Var.i(i, list, z);
    }

    public static void q(int i, List<Integer> list, u1 u1Var, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        u1Var.G(i, list, z);
    }

    public static void r(int i, List<Integer> list, u1 u1Var, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        u1Var.g(i, list, z);
    }

    public static void s(int i, List<Integer> list, u1 u1Var, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        u1Var.j(i, list, z);
    }

    public static void t(int i, List<Integer> list, u1 u1Var, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        u1Var.H(i, list, z);
    }

    public static void u(int i, List<Integer> list, u1 u1Var, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        u1Var.h(i, list, z);
    }

    public static void v(int i, List<Integer> list, u1 u1Var, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        u1Var.c(i, list, z);
    }

    public static void w(int i, List<Boolean> list, u1 u1Var, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        u1Var.d(i, list, z);
    }

    public static void x(int i, List<String> list, u1 u1Var) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        u1Var.e(i, list);
    }

    public static void y(int i, List<zzjd> list, u1 u1Var) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        u1Var.f(i, list);
    }

    public static void z(int i, List<?> list, u1 u1Var, c2 c2Var) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        for (int i2 = 0; i2 < list.size(); i2++) {
            u1Var.C(i, list.get(i2), c2Var);
        }
    }
}
