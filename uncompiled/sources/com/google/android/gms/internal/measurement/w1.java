package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.w1;
import com.google.android.gms.internal.measurement.x1;
import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* loaded from: classes.dex */
public class w1<MessageType extends x1<MessageType, BuilderType>, BuilderType extends w1<MessageType, BuilderType>> extends o1<MessageType, BuilderType> {
    public final MessageType a;
    public MessageType f0;
    public boolean g0 = false;

    public w1(MessageType messagetype) {
        this.a = messagetype;
        this.f0 = (MessageType) messagetype.w(4, null, null);
    }

    public static final void l(MessageType messagetype, MessageType messagetype2) {
        jy5.a().b(messagetype.getClass()).h(messagetype, messagetype2);
    }

    @Override // defpackage.xx5
    public final /* bridge */ /* synthetic */ z1 g() {
        return this.a;
    }

    @Override // com.google.android.gms.internal.measurement.o1
    public final /* bridge */ /* synthetic */ o1 h(byte[] bArr, int i, int i2) throws zzkn {
        r(bArr, 0, i2, qt5.a());
        return this;
    }

    @Override // com.google.android.gms.internal.measurement.o1
    public final /* bridge */ /* synthetic */ o1 j(byte[] bArr, int i, int i2, qt5 qt5Var) throws zzkn {
        r(bArr, 0, i2, qt5Var);
        return this;
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.google.android.gms.internal.measurement.o1
    public final /* bridge */ /* synthetic */ o1 k(zq5 zq5Var) {
        p((x1) zq5Var);
        return this;
    }

    public final MessageType o() {
        MessageType q = q();
        boolean z = true;
        byte byteValue = ((Byte) q.w(1, null, null)).byteValue();
        if (byteValue != 1) {
            if (byteValue == 0) {
                z = false;
            } else {
                boolean e = jy5.a().b(q.getClass()).e(q);
                q.w(2, true != e ? null : q, null);
                z = e;
            }
        }
        if (z) {
            return q;
        }
        throw new zzmg(q);
    }

    public final BuilderType p(MessageType messagetype) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        l(this.f0, messagetype);
        return this;
    }

    public final BuilderType r(byte[] bArr, int i, int i2, qt5 qt5Var) throws zzkn {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        try {
            jy5.a().b(this.f0.getClass()).i(this.f0, bArr, 0, i2, new fr5(qt5Var));
            return this;
        } catch (zzkn e) {
            throw e;
        } catch (IOException e2) {
            throw new RuntimeException("Reading from byte array should not throw IOException.", e2);
        } catch (IndexOutOfBoundsException unused) {
            throw zzkn.zza();
        }
    }

    public void s() {
        MessageType messagetype = (MessageType) this.f0.w(4, null, null);
        l(messagetype, this.f0);
        this.f0 = messagetype;
    }

    /* renamed from: t */
    public final BuilderType clone() {
        BuilderType buildertype = (BuilderType) this.a.w(5, null, null);
        buildertype.p(q());
        return buildertype;
    }

    @Override // com.google.android.gms.internal.measurement.y1
    /* renamed from: u */
    public MessageType q() {
        if (this.g0) {
            return this.f0;
        }
        MessageType messagetype = this.f0;
        jy5.a().b(messagetype.getClass()).f(messagetype);
        this.g0 = true;
        return this.f0;
    }
}
