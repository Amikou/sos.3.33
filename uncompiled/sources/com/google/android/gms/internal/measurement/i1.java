package com.google.android.gms.internal.measurement;

import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* loaded from: classes.dex */
public final class i1 extends x1<i1, fl5> implements xx5 {
    private static final i1 zzg;
    private int zza;
    private int zze;
    private xv5 zzf = x1.m();

    static {
        i1 i1Var = new i1();
        zzg = i1Var;
        x1.u(i1.class, i1Var);
    }

    public static fl5 C() {
        return zzg.r();
    }

    public static /* synthetic */ void E(i1 i1Var, int i) {
        i1Var.zza |= 1;
        i1Var.zze = i;
    }

    public static /* synthetic */ void F(i1 i1Var, Iterable iterable) {
        xv5 xv5Var = i1Var.zzf;
        if (!xv5Var.zza()) {
            i1Var.zzf = x1.n(xv5Var);
        }
        zq5.j(iterable, i1Var.zzf);
    }

    public final int A() {
        return this.zzf.size();
    }

    public final long B(int i) {
        return this.zzf.z0(i);
    }

    @Override // com.google.android.gms.internal.measurement.x1
    public final Object w(int i, Object obj, Object obj2) {
        int i2 = i - 1;
        if (i2 != 0) {
            if (i2 != 2) {
                if (i2 != 3) {
                    if (i2 != 4) {
                        if (i2 != 5) {
                            return null;
                        }
                        return zzg;
                    }
                    return new fl5(null);
                }
                return new i1();
            }
            return x1.v(zzg, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0001\u0000\u0001င\u0000\u0002\u0014", new Object[]{"zza", "zze", "zzf"});
        }
        return (byte) 1;
    }

    public final boolean x() {
        return (this.zza & 1) != 0;
    }

    public final int y() {
        return this.zze;
    }

    public final List<Long> z() {
        return this.zzf;
    }
}
