package com.google.android.gms.internal.measurement;

import java.io.IOException;
import java.util.Arrays;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* loaded from: classes.dex */
public final class f2 {
    public static final f2 f = new f2(0, new int[0], new Object[0], false);
    public int a;
    public int[] b;
    public Object[] c;
    public int d;
    public boolean e;

    public f2() {
        this(0, new int[8], new Object[8], true);
    }

    public f2(int i, int[] iArr, Object[] objArr, boolean z) {
        this.d = -1;
        this.a = i;
        this.b = iArr;
        this.c = objArr;
        this.e = z;
    }

    public static f2 a() {
        return f;
    }

    public static f2 b() {
        return new f2(0, new int[8], new Object[8], true);
    }

    public static f2 c(f2 f2Var, f2 f2Var2) {
        int i = f2Var.a + f2Var2.a;
        int[] copyOf = Arrays.copyOf(f2Var.b, i);
        System.arraycopy(f2Var2.b, 0, copyOf, f2Var.a, f2Var2.a);
        Object[] copyOf2 = Arrays.copyOf(f2Var.c, i);
        System.arraycopy(f2Var2.c, 0, copyOf2, f2Var.a, f2Var2.a);
        return new f2(i, copyOf, copyOf2, true);
    }

    public final void d() {
        this.e = false;
    }

    public final int e() {
        int i = this.d;
        if (i == -1) {
            int i2 = 0;
            for (int i3 = 0; i3 < this.a; i3++) {
                int i4 = this.b[i3];
                int A = t1.A(8);
                int zzc = ((zzjd) this.c[i3]).zzc();
                i2 += A + A + t1.A(16) + t1.A(i4 >>> 3) + t1.A(24) + t1.A(zzc) + zzc;
            }
            this.d = i2;
            return i2;
        }
        return i;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && (obj instanceof f2)) {
            f2 f2Var = (f2) obj;
            int i = this.a;
            if (i == f2Var.a) {
                int[] iArr = this.b;
                int[] iArr2 = f2Var.b;
                int i2 = 0;
                while (true) {
                    if (i2 >= i) {
                        Object[] objArr = this.c;
                        Object[] objArr2 = f2Var.c;
                        int i3 = this.a;
                        for (int i4 = 0; i4 < i3; i4++) {
                            if (objArr[i4].equals(objArr2[i4])) {
                            }
                        }
                        return true;
                    } else if (iArr[i2] != iArr2[i2]) {
                        break;
                    } else {
                        i2++;
                    }
                }
            }
            return false;
        }
        return false;
    }

    public final int f() {
        int A;
        int B;
        int i;
        int i2 = this.d;
        if (i2 == -1) {
            int i3 = 0;
            for (int i4 = 0; i4 < this.a; i4++) {
                int i5 = this.b[i4];
                int i6 = i5 >>> 3;
                int i7 = i5 & 7;
                if (i7 != 0) {
                    if (i7 == 1) {
                        ((Long) this.c[i4]).longValue();
                        i = t1.A(i6 << 3) + 8;
                    } else if (i7 == 2) {
                        int A2 = t1.A(i6 << 3);
                        int zzc = ((zzjd) this.c[i4]).zzc();
                        i3 += A2 + t1.A(zzc) + zzc;
                    } else if (i7 == 3) {
                        int y = t1.y(i6);
                        A = y + y;
                        B = ((f2) this.c[i4]).f();
                    } else if (i7 == 5) {
                        ((Integer) this.c[i4]).intValue();
                        i = t1.A(i6 << 3) + 4;
                    } else {
                        throw new IllegalStateException(zzkn.zzd());
                    }
                    i3 += i;
                } else {
                    long longValue = ((Long) this.c[i4]).longValue();
                    A = t1.A(i6 << 3);
                    B = t1.B(longValue);
                }
                i = A + B;
                i3 += i;
            }
            this.d = i3;
            return i3;
        }
        return i2;
    }

    public final void g(StringBuilder sb, int i) {
        for (int i2 = 0; i2 < this.a; i2++) {
            zx5.b(sb, i, String.valueOf(this.b[i2] >>> 3), this.c[i2]);
        }
    }

    public final void h(int i, Object obj) {
        if (this.e) {
            int i2 = this.a;
            int[] iArr = this.b;
            if (i2 == iArr.length) {
                int i3 = i2 + (i2 < 4 ? 8 : i2 >> 1);
                this.b = Arrays.copyOf(iArr, i3);
                this.c = Arrays.copyOf(this.c, i3);
            }
            int[] iArr2 = this.b;
            int i4 = this.a;
            iArr2[i4] = i;
            this.c[i4] = obj;
            this.a = i4 + 1;
            return;
        }
        throw new UnsupportedOperationException();
    }

    public final int hashCode() {
        int i = this.a;
        int i2 = (i + 527) * 31;
        int[] iArr = this.b;
        int i3 = 17;
        int i4 = 17;
        for (int i5 = 0; i5 < i; i5++) {
            i4 = (i4 * 31) + iArr[i5];
        }
        int i6 = (i2 + i4) * 31;
        Object[] objArr = this.c;
        int i7 = this.a;
        for (int i8 = 0; i8 < i7; i8++) {
            i3 = (i3 * 31) + objArr[i8].hashCode();
        }
        return i6 + i3;
    }

    public final void i(u1 u1Var) throws IOException {
        if (this.a != 0) {
            for (int i = 0; i < this.a; i++) {
                int i2 = this.b[i];
                Object obj = this.c[i];
                int i3 = i2 >>> 3;
                int i4 = i2 & 7;
                if (i4 == 0) {
                    u1Var.n(i3, ((Long) obj).longValue());
                } else if (i4 == 1) {
                    u1Var.u(i3, ((Long) obj).longValue());
                } else if (i4 == 2) {
                    u1Var.y(i3, (zzjd) obj);
                } else if (i4 == 3) {
                    u1Var.E(i3);
                    ((f2) obj).i(u1Var);
                    u1Var.F(i3);
                } else if (i4 == 5) {
                    u1Var.v(i3, ((Integer) obj).intValue());
                } else {
                    throw new RuntimeException(zzkn.zzd());
                }
            }
        }
    }
}
