package com.google.android.gms.internal.measurement;

import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* loaded from: classes.dex */
public final class l1 extends x1<l1, ol5> implements xx5 {
    private static final l1 zzg;
    private int zza;
    private String zze = "";
    private zv5<n1> zzf = x1.o();

    static {
        l1 l1Var = new l1();
        zzg = l1Var;
        x1.u(l1.class, l1Var);
    }

    @Override // com.google.android.gms.internal.measurement.x1
    public final Object w(int i, Object obj, Object obj2) {
        int i2 = i - 1;
        if (i2 != 0) {
            if (i2 != 2) {
                if (i2 != 3) {
                    if (i2 != 4) {
                        if (i2 != 5) {
                            return null;
                        }
                        return zzg;
                    }
                    return new ol5(null);
                }
                return new l1();
            }
            return x1.v(zzg, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0001\u0000\u0001ဈ\u0000\u0002\u001b", new Object[]{"zza", "zze", "zzf", n1.class});
        }
        return (byte) 1;
    }

    public final String x() {
        return this.zze;
    }

    public final List<n1> y() {
        return this.zzf;
    }
}
