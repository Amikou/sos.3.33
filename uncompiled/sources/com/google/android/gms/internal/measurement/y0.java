package com.google.android.gms.internal.measurement;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* loaded from: classes.dex */
public final class y0 extends x1<y0, si5> implements xx5 {
    private static final y0 zzg;
    private int zza;
    private String zze = "";
    private String zzf = "";

    static {
        y0 y0Var = new y0();
        zzg = y0Var;
        x1.u(y0.class, y0Var);
    }

    @Override // com.google.android.gms.internal.measurement.x1
    public final Object w(int i, Object obj, Object obj2) {
        int i2 = i - 1;
        if (i2 != 0) {
            if (i2 != 2) {
                if (i2 != 3) {
                    if (i2 != 4) {
                        if (i2 != 5) {
                            return null;
                        }
                        return zzg;
                    }
                    return new si5(null);
                }
                return new y0();
            }
            return x1.v(zzg, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001ဈ\u0000\u0002ဈ\u0001", new Object[]{"zza", "zze", "zzf"});
        }
        return (byte) 1;
    }

    public final String x() {
        return this.zze;
    }

    public final String y() {
        return this.zzf;
    }
}
