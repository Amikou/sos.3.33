package com.google.android.gms.internal.measurement;

import android.os.Bundle;
import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@19.0.0 */
/* loaded from: classes.dex */
public final class g0 extends i0 {
    public final /* synthetic */ Long i0;
    public final /* synthetic */ String j0;
    public final /* synthetic */ String k0;
    public final /* synthetic */ Bundle l0;
    public final /* synthetic */ boolean m0;
    public final /* synthetic */ boolean n0;
    public final /* synthetic */ wf5 o0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public g0(wf5 wf5Var, Long l, String str, String str2, Bundle bundle, boolean z, boolean z2) {
        super(wf5Var, true);
        this.o0 = wf5Var;
        this.i0 = l;
        this.j0 = str;
        this.k0 = str2;
        this.l0 = bundle;
        this.m0 = z;
        this.n0 = z2;
    }

    @Override // com.google.android.gms.internal.measurement.i0
    public final void a() throws RemoteException {
        j jVar;
        Long l = this.i0;
        long longValue = l == null ? this.a : l.longValue();
        jVar = this.o0.h;
        ((j) zt2.j(jVar)).logEvent(this.j0, this.k0, this.l0, this.m0, this.n0, longValue);
    }
}
