package com.google.android.gms.internal.measurement;

import java.lang.reflect.Field;
import java.nio.Buffer;
import java.security.AccessController;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.web3j.abi.datatypes.Address;
import sun.misc.Unsafe;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* loaded from: classes.dex */
public final class l2 {
    public static final Unsafe a;
    public static final Class<?> b;
    public static final boolean c;
    public static final boolean d;
    public static final k2 e;
    public static final boolean f;
    public static final boolean g;
    public static final long h;
    public static final boolean i;

    /* JADX WARN: Removed duplicated region for block: B:33:0x012b  */
    /* JADX WARN: Removed duplicated region for block: B:39:0x013b  */
    static {
        /*
            Method dump skipped, instructions count: 319
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.l2.<clinit>():void");
    }

    public static /* synthetic */ boolean A(Object obj, long j) {
        return ((byte) ((e.k(obj, (-4) & j) >>> ((int) (((~j) & 3) << 3))) & 255)) != 0;
    }

    public static /* synthetic */ boolean B(Object obj, long j) {
        return ((byte) ((e.k(obj, (-4) & j) >>> ((int) ((j & 3) << 3))) & 255)) != 0;
    }

    public static int E(Class<?> cls) {
        if (g) {
            return e.i(cls);
        }
        return -1;
    }

    public static int a(Class<?> cls) {
        if (g) {
            return e.j(cls);
        }
        return -1;
    }

    public static Field b() {
        int i2 = p1.a;
        Field c2 = c(Buffer.class, "effectiveDirectAddress");
        if (c2 == null) {
            Field c3 = c(Buffer.class, Address.TYPE_NAME);
            if (c3 == null || c3.getType() != Long.TYPE) {
                return null;
            }
            return c3;
        }
        return c2;
    }

    public static Field c(Class<?> cls, String str) {
        try {
            return cls.getDeclaredField(str);
        } catch (Throwable unused) {
            return null;
        }
    }

    public static void d(Object obj, long j, byte b2) {
        long j2 = (-4) & j;
        k2 k2Var = e;
        int k = k2Var.k(obj, j2);
        int i2 = ((~((int) j)) & 3) << 3;
        k2Var.l(obj, j2, ((255 & b2) << i2) | (k & (~(255 << i2))));
    }

    public static void e(Object obj, long j, byte b2) {
        long j2 = (-4) & j;
        k2 k2Var = e;
        int i2 = (((int) j) & 3) << 3;
        k2Var.l(obj, j2, ((255 & b2) << i2) | (k2Var.k(obj, j2) & (~(255 << i2))));
    }

    public static boolean f() {
        return g;
    }

    public static boolean g() {
        return f;
    }

    public static <T> T h(Class<T> cls) {
        try {
            return (T) a.allocateInstance(cls);
        } catch (InstantiationException e2) {
            throw new IllegalStateException(e2);
        }
    }

    public static int i(Object obj, long j) {
        return e.k(obj, j);
    }

    public static void j(Object obj, long j, int i2) {
        e.l(obj, j, i2);
    }

    public static long k(Object obj, long j) {
        return e.m(obj, j);
    }

    public static void l(Object obj, long j, long j2) {
        e.n(obj, j, j2);
    }

    public static boolean m(Object obj, long j) {
        return e.b(obj, j);
    }

    public static void n(Object obj, long j, boolean z) {
        e.c(obj, j, z);
    }

    public static float o(Object obj, long j) {
        return e.d(obj, j);
    }

    public static void p(Object obj, long j, float f2) {
        e.e(obj, j, f2);
    }

    public static double q(Object obj, long j) {
        return e.f(obj, j);
    }

    public static void r(Object obj, long j, double d2) {
        e.g(obj, j, d2);
    }

    public static Object s(Object obj, long j) {
        return e.o(obj, j);
    }

    public static void t(Object obj, long j, Object obj2) {
        e.p(obj, j, obj2);
    }

    public static void u(byte[] bArr, long j, byte b2) {
        e.a(bArr, h + j, b2);
    }

    public static Unsafe v() {
        try {
            return (Unsafe) AccessController.doPrivileged(new h2());
        } catch (Throwable unused) {
            return null;
        }
    }

    public static boolean w(Class<?> cls) {
        int i2 = p1.a;
        try {
            Class<?> cls2 = b;
            Class<?> cls3 = Boolean.TYPE;
            cls2.getMethod("peekLong", cls, cls3);
            cls2.getMethod("pokeLong", cls, Long.TYPE, cls3);
            Class<?> cls4 = Integer.TYPE;
            cls2.getMethod("pokeInt", cls, cls4, cls3);
            cls2.getMethod("peekInt", cls, cls3);
            cls2.getMethod("pokeByte", cls, Byte.TYPE);
            cls2.getMethod("peekByte", cls);
            cls2.getMethod("pokeByteArray", cls, byte[].class, cls4, cls4);
            cls2.getMethod("peekByteArray", cls, byte[].class, cls4, cls4);
            return true;
        } catch (Throwable unused) {
            return false;
        }
    }

    public static /* synthetic */ void x(Throwable th) {
        Logger logger = Logger.getLogger(l2.class.getName());
        Level level = Level.WARNING;
        String valueOf = String.valueOf(th);
        StringBuilder sb = new StringBuilder(valueOf.length() + 71);
        sb.append("platform method missing - proto runtime falling back to safer methods: ");
        sb.append(valueOf);
        logger.logp(level, "com.google.protobuf.UnsafeUtil", "logMissingMethod", sb.toString());
    }
}
