package com.google.android.gms.internal.measurement;

import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* loaded from: classes.dex */
public final class v0 extends x1<v0, eh5> implements xx5 {
    private static final v0 zzi;
    private int zza;
    private int zze;
    private boolean zzg;
    private String zzf = "";
    private zv5<String> zzh = x1.o();

    static {
        v0 v0Var = new v0();
        zzi = v0Var;
        x1.u(v0.class, v0Var);
    }

    public static v0 F() {
        return zzi;
    }

    public final String A() {
        return this.zzf;
    }

    public final boolean B() {
        return (this.zza & 4) != 0;
    }

    public final boolean C() {
        return this.zzg;
    }

    public final List<String> D() {
        return this.zzh;
    }

    public final int E() {
        return this.zzh.size();
    }

    @Override // com.google.android.gms.internal.measurement.x1
    public final Object w(int i, Object obj, Object obj2) {
        int i2 = i - 1;
        if (i2 != 0) {
            if (i2 != 2) {
                if (i2 != 3) {
                    if (i2 != 4) {
                        if (i2 != 5) {
                            return null;
                        }
                        return zzi;
                    }
                    return new eh5(null);
                }
                return new v0();
            }
            return x1.v(zzi, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0001\u0000\u0001ဌ\u0000\u0002ဈ\u0001\u0003ဇ\u0002\u0004\u001a", new Object[]{"zza", "zze", zzew.zzb(), "zzf", "zzg", "zzh"});
        }
        return (byte) 1;
    }

    public final boolean x() {
        return (this.zza & 1) != 0;
    }

    public final zzew y() {
        zzew zza = zzew.zza(this.zze);
        return zza == null ? zzew.UNKNOWN_MATCH_TYPE : zza;
    }

    public final boolean z() {
        return (this.zza & 2) != 0;
    }
}
