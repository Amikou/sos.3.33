package com.google.android.gms.internal.measurement;

import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* loaded from: classes.dex */
public final class s1 extends t1 {
    public final byte[] d;
    public final int e;
    public int f;

    public s1(byte[] bArr, int i, int i2) {
        super(null);
        int length = bArr.length;
        if (((length - i2) | i2) >= 0) {
            this.d = bArr;
            this.f = 0;
            this.e = i2;
            return;
        }
        throw new IllegalArgumentException(String.format("Array range is invalid. Buffer.length=%d, offset=%d, length=%d", Integer.valueOf(length), 0, Integer.valueOf(i2)));
    }

    public final void E(byte[] bArr, int i, int i2) throws IOException {
        try {
            System.arraycopy(bArr, 0, this.d, this.f, i2);
            this.f += i2;
        } catch (IndexOutOfBoundsException e) {
            throw new zzjj(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.f), Integer.valueOf(this.e), Integer.valueOf(i2)), e);
        }
    }

    public final void F(String str) throws IOException {
        int i = this.f;
        try {
            int A = t1.A(str.length() * 3);
            int A2 = t1.A(str.length());
            if (A2 == A) {
                int i2 = i + A2;
                this.f = i2;
                int d = n2.d(str, this.d, i2, this.e - i2);
                this.f = i;
                r((d - i) - A2);
                this.f = d;
                return;
            }
            r(n2.c(str));
            byte[] bArr = this.d;
            int i3 = this.f;
            this.f = n2.d(str, bArr, i3, this.e - i3);
        } catch (zzmv e) {
            this.f = i;
            d(str, e);
        } catch (IndexOutOfBoundsException e2) {
            throw new zzjj(e2);
        }
    }

    @Override // com.google.android.gms.internal.measurement.t1
    public final void g(int i, int i2) throws IOException {
        r((i << 3) | i2);
    }

    @Override // com.google.android.gms.internal.measurement.t1
    public final void h(int i, int i2) throws IOException {
        r(i << 3);
        q(i2);
    }

    @Override // com.google.android.gms.internal.measurement.t1
    public final void i(int i, int i2) throws IOException {
        r(i << 3);
        r(i2);
    }

    @Override // com.google.android.gms.internal.measurement.t1
    public final void j(int i, int i2) throws IOException {
        r((i << 3) | 5);
        s(i2);
    }

    @Override // com.google.android.gms.internal.measurement.t1
    public final void k(int i, long j) throws IOException {
        r(i << 3);
        t(j);
    }

    @Override // com.google.android.gms.internal.measurement.t1
    public final void l(int i, long j) throws IOException {
        r((i << 3) | 1);
        u(j);
    }

    @Override // com.google.android.gms.internal.measurement.t1
    public final void m(int i, boolean z) throws IOException {
        r(i << 3);
        p(z ? (byte) 1 : (byte) 0);
    }

    @Override // com.google.android.gms.internal.measurement.t1
    public final void n(int i, String str) throws IOException {
        r((i << 3) | 2);
        F(str);
    }

    @Override // com.google.android.gms.internal.measurement.t1
    public final void o(int i, zzjd zzjdVar) throws IOException {
        r((i << 3) | 2);
        r(zzjdVar.zzc());
        zzjdVar.zzf(this);
    }

    @Override // com.google.android.gms.internal.measurement.t1
    public final void p(byte b) throws IOException {
        try {
            byte[] bArr = this.d;
            int i = this.f;
            this.f = i + 1;
            bArr[i] = b;
        } catch (IndexOutOfBoundsException e) {
            throw new zzjj(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.f), Integer.valueOf(this.e), 1), e);
        }
    }

    @Override // com.google.android.gms.internal.measurement.t1
    public final void q(int i) throws IOException {
        if (i >= 0) {
            r(i);
        } else {
            t(i);
        }
    }

    @Override // com.google.android.gms.internal.measurement.t1
    public final void r(int i) throws IOException {
        boolean z;
        z = t1.c;
        if (z) {
            int i2 = p1.a;
        }
        while ((i & (-128)) != 0) {
            try {
                byte[] bArr = this.d;
                int i3 = this.f;
                this.f = i3 + 1;
                bArr[i3] = (byte) ((i & 127) | 128);
                i >>>= 7;
            } catch (IndexOutOfBoundsException e) {
                throw new zzjj(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.f), Integer.valueOf(this.e), 1), e);
            }
        }
        byte[] bArr2 = this.d;
        int i4 = this.f;
        this.f = i4 + 1;
        bArr2[i4] = (byte) i;
    }

    @Override // com.google.android.gms.internal.measurement.t1
    public final void s(int i) throws IOException {
        try {
            byte[] bArr = this.d;
            int i2 = this.f;
            int i3 = i2 + 1;
            this.f = i3;
            bArr[i2] = (byte) (i & 255);
            int i4 = i3 + 1;
            this.f = i4;
            bArr[i3] = (byte) ((i >> 8) & 255);
            int i5 = i4 + 1;
            this.f = i5;
            bArr[i4] = (byte) ((i >> 16) & 255);
            this.f = i5 + 1;
            bArr[i5] = (byte) ((i >> 24) & 255);
        } catch (IndexOutOfBoundsException e) {
            throw new zzjj(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.f), Integer.valueOf(this.e), 1), e);
        }
    }

    @Override // com.google.android.gms.internal.measurement.t1
    public final void t(long j) throws IOException {
        boolean z;
        z = t1.c;
        if (z && this.e - this.f >= 10) {
            while ((j & (-128)) != 0) {
                byte[] bArr = this.d;
                int i = this.f;
                this.f = i + 1;
                l2.u(bArr, i, (byte) ((((int) j) & 127) | 128));
                j >>>= 7;
            }
            byte[] bArr2 = this.d;
            int i2 = this.f;
            this.f = i2 + 1;
            l2.u(bArr2, i2, (byte) j);
            return;
        }
        while ((j & (-128)) != 0) {
            try {
                byte[] bArr3 = this.d;
                int i3 = this.f;
                this.f = i3 + 1;
                bArr3[i3] = (byte) ((((int) j) & 127) | 128);
                j >>>= 7;
            } catch (IndexOutOfBoundsException e) {
                throw new zzjj(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.f), Integer.valueOf(this.e), 1), e);
            }
        }
        byte[] bArr4 = this.d;
        int i4 = this.f;
        this.f = i4 + 1;
        bArr4[i4] = (byte) j;
    }

    @Override // com.google.android.gms.internal.measurement.t1
    public final void u(long j) throws IOException {
        try {
            byte[] bArr = this.d;
            int i = this.f;
            int i2 = i + 1;
            this.f = i2;
            bArr[i] = (byte) (((int) j) & 255);
            int i3 = i2 + 1;
            this.f = i3;
            bArr[i2] = (byte) (((int) (j >> 8)) & 255);
            int i4 = i3 + 1;
            this.f = i4;
            bArr[i3] = (byte) (((int) (j >> 16)) & 255);
            int i5 = i4 + 1;
            this.f = i5;
            bArr[i4] = (byte) (((int) (j >> 24)) & 255);
            int i6 = i5 + 1;
            this.f = i6;
            bArr[i5] = (byte) (((int) (j >> 32)) & 255);
            int i7 = i6 + 1;
            this.f = i7;
            bArr[i6] = (byte) (((int) (j >> 40)) & 255);
            int i8 = i7 + 1;
            this.f = i8;
            bArr[i7] = (byte) (((int) (j >> 48)) & 255);
            this.f = i8 + 1;
            bArr[i8] = (byte) (((int) (j >> 56)) & 255);
        } catch (IndexOutOfBoundsException e) {
            throw new zzjj(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.f), Integer.valueOf(this.e), 1), e);
        }
    }

    @Override // com.google.android.gms.internal.measurement.t1
    public final void v(byte[] bArr, int i, int i2) throws IOException {
        E(bArr, 0, i2);
    }

    @Override // com.google.android.gms.internal.measurement.t1
    public final int w() {
        return this.e - this.f;
    }
}
