package com.google.android.gms.internal.measurement;

import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* loaded from: classes.dex */
public final class g2 extends e2<f2, f2> {
    @Override // com.google.android.gms.internal.measurement.e2
    public final /* bridge */ /* synthetic */ void a(f2 f2Var, int i, long j) {
        f2Var.h(i << 3, Long.valueOf(j));
    }

    @Override // com.google.android.gms.internal.measurement.e2
    public final /* bridge */ /* synthetic */ f2 b() {
        return f2.b();
    }

    @Override // com.google.android.gms.internal.measurement.e2
    public final /* bridge */ /* synthetic */ void c(Object obj, f2 f2Var) {
        ((x1) obj).zzc = f2Var;
    }

    @Override // com.google.android.gms.internal.measurement.e2
    public final /* bridge */ /* synthetic */ f2 d(Object obj) {
        return ((x1) obj).zzc;
    }

    @Override // com.google.android.gms.internal.measurement.e2
    public final void e(Object obj) {
        ((x1) obj).zzc.d();
    }

    @Override // com.google.android.gms.internal.measurement.e2
    public final /* bridge */ /* synthetic */ f2 f(f2 f2Var, f2 f2Var2) {
        f2 f2Var3 = f2Var2;
        return f2Var3.equals(f2.a()) ? f2Var : f2.c(f2Var, f2Var3);
    }

    @Override // com.google.android.gms.internal.measurement.e2
    public final /* bridge */ /* synthetic */ int g(f2 f2Var) {
        return f2Var.e();
    }

    @Override // com.google.android.gms.internal.measurement.e2
    public final /* bridge */ /* synthetic */ int h(f2 f2Var) {
        return f2Var.f();
    }

    @Override // com.google.android.gms.internal.measurement.e2
    public final /* bridge */ /* synthetic */ void i(f2 f2Var, u1 u1Var) throws IOException {
        f2Var.i(u1Var);
    }
}
