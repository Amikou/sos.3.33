package com.google.android.gms.internal.measurement;

import android.os.Bundle;
import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@19.0.0 */
/* loaded from: classes.dex */
public final class r extends i0 {
    public final /* synthetic */ String i0;
    public final /* synthetic */ String j0;
    public final /* synthetic */ Bundle k0;
    public final /* synthetic */ wf5 l0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public r(wf5 wf5Var, String str, String str2, Bundle bundle) {
        super(wf5Var, true);
        this.l0 = wf5Var;
        this.i0 = str;
        this.j0 = str2;
        this.k0 = bundle;
    }

    @Override // com.google.android.gms.internal.measurement.i0
    public final void a() throws RemoteException {
        j jVar;
        jVar = this.l0.h;
        ((j) zt2.j(jVar)).clearConditionalUserProperty(this.i0, this.j0, this.k0);
    }
}
