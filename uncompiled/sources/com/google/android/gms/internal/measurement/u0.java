package com.google.android.gms.internal.measurement;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* loaded from: classes.dex */
public final class u0 extends x1<u0, ah5> implements xx5 {
    private static final u0 zzk;
    private int zza;
    private int zze;
    private String zzf = "";
    private s0 zzg;
    private boolean zzh;
    private boolean zzi;
    private boolean zzj;

    static {
        u0 u0Var = new u0();
        zzk = u0Var;
        x1.u(u0.class, u0Var);
    }

    public static ah5 F() {
        return zzk.r();
    }

    public static /* synthetic */ void H(u0 u0Var, String str) {
        u0Var.zza |= 2;
        u0Var.zzf = str;
    }

    public final s0 A() {
        s0 s0Var = this.zzg;
        return s0Var == null ? s0.F() : s0Var;
    }

    public final boolean B() {
        return this.zzh;
    }

    public final boolean C() {
        return this.zzi;
    }

    public final boolean D() {
        return (this.zza & 32) != 0;
    }

    public final boolean E() {
        return this.zzj;
    }

    @Override // com.google.android.gms.internal.measurement.x1
    public final Object w(int i, Object obj, Object obj2) {
        int i2 = i - 1;
        if (i2 != 0) {
            if (i2 != 2) {
                if (i2 != 3) {
                    if (i2 != 4) {
                        if (i2 != 5) {
                            return null;
                        }
                        return zzk;
                    }
                    return new ah5(null);
                }
                return new u0();
            }
            return x1.v(zzk, "\u0001\u0006\u0000\u0001\u0001\u0006\u0006\u0000\u0000\u0000\u0001င\u0000\u0002ဈ\u0001\u0003ဉ\u0002\u0004ဇ\u0003\u0005ဇ\u0004\u0006ဇ\u0005", new Object[]{"zza", "zze", "zzf", "zzg", "zzh", "zzi", "zzj"});
        }
        return (byte) 1;
    }

    public final boolean x() {
        return (this.zza & 1) != 0;
    }

    public final int y() {
        return this.zze;
    }

    public final String z() {
        return this.zzf;
    }
}
