package com.google.android.gms.internal.measurement;

import com.github.mikephil.charting.utils.Utils;
import com.google.android.gms.internal.measurement.g;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* loaded from: classes.dex */
public final class g {
    public final th5 a;
    public wk5 b;
    public final b c;
    public final b66 d;

    public g() {
        th5 th5Var = new th5();
        this.a = th5Var;
        this.b = th5Var.b.c();
        this.c = new b();
        this.d = new b66();
        th5Var.d.a("internal.registerCallback", new Callable(this) { // from class: l35
            public final g a;

            {
                this.a = this;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return this.a.g();
            }
        });
        th5Var.d.a("internal.eventLogger", new Callable(this) { // from class: c75
            public final g a;

            {
                this.a = this;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return new vu5(this.a.c);
            }
        });
    }

    public final void a(String str, Callable<? extends c55> callable) {
        this.a.d.a(str, callable);
    }

    public final boolean b(a aVar) throws zzd {
        try {
            this.c.b(aVar);
            this.a.c.e("runtime.counter", new z45(Double.valueOf((double) Utils.DOUBLE_EPSILON)));
            this.d.b(this.b.c(), this.c);
            if (c()) {
                return true;
            }
            return d();
        } catch (Throwable th) {
            throw new zzd(th);
        }
    }

    public final boolean c() {
        return !this.c.c().equals(this.c.a());
    }

    public final boolean d() {
        return !this.c.f().isEmpty();
    }

    public final b e() {
        return this.c;
    }

    public final void f(m1 m1Var) throws zzd {
        c55 c55Var;
        try {
            this.b = this.a.b.c();
            if (!(this.a.a(this.b, (n1[]) m1Var.x().toArray(new n1[0])) instanceof v45)) {
                for (l1 l1Var : m1Var.y().x()) {
                    List<n1> y = l1Var.y();
                    String x = l1Var.x();
                    Iterator<n1> it = y.iterator();
                    while (it.hasNext()) {
                        z55 a = this.a.a(this.b, it.next());
                        if (a instanceof p55) {
                            wk5 wk5Var = this.b;
                            if (wk5Var.d(x)) {
                                z55 h = wk5Var.h(x);
                                if (!(h instanceof c55)) {
                                    String valueOf = String.valueOf(x);
                                    throw new IllegalStateException(valueOf.length() != 0 ? "Invalid function name: ".concat(valueOf) : new String("Invalid function name: "));
                                }
                                c55Var = (c55) h;
                            } else {
                                c55Var = null;
                            }
                            if (c55Var == null) {
                                String valueOf2 = String.valueOf(x);
                                throw new IllegalStateException(valueOf2.length() != 0 ? "Rule function is undefined: ".concat(valueOf2) : new String("Rule function is undefined: "));
                            }
                            c55Var.a(this.b, Collections.singletonList(a));
                        } else {
                            throw new IllegalArgumentException("Invalid rule definition");
                        }
                    }
                }
                return;
            }
            throw new IllegalStateException("Program loading failed");
        } catch (Throwable th) {
            throw new zzd(th);
        }
    }

    public final /* synthetic */ c55 g() throws Exception {
        return new a56(this.d);
    }
}
