package com.google.android.gms.internal.measurement;

import sun.misc.Unsafe;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* loaded from: classes.dex */
public final class i2 extends k2 {
    public i2(Unsafe unsafe) {
        super(unsafe);
    }

    @Override // com.google.android.gms.internal.measurement.k2
    public final void a(Object obj, long j, byte b) {
        if (l2.i) {
            l2.d(obj, j, b);
        } else {
            l2.e(obj, j, b);
        }
    }

    @Override // com.google.android.gms.internal.measurement.k2
    public final boolean b(Object obj, long j) {
        if (l2.i) {
            return l2.A(obj, j);
        }
        return l2.B(obj, j);
    }

    @Override // com.google.android.gms.internal.measurement.k2
    public final void c(Object obj, long j, boolean z) {
        if (l2.i) {
            l2.d(obj, j, r3 ? (byte) 1 : (byte) 0);
        } else {
            l2.e(obj, j, r3 ? (byte) 1 : (byte) 0);
        }
    }

    @Override // com.google.android.gms.internal.measurement.k2
    public final float d(Object obj, long j) {
        return Float.intBitsToFloat(k(obj, j));
    }

    @Override // com.google.android.gms.internal.measurement.k2
    public final void e(Object obj, long j, float f) {
        l(obj, j, Float.floatToIntBits(f));
    }

    @Override // com.google.android.gms.internal.measurement.k2
    public final double f(Object obj, long j) {
        return Double.longBitsToDouble(m(obj, j));
    }

    @Override // com.google.android.gms.internal.measurement.k2
    public final void g(Object obj, long j, double d) {
        n(obj, j, Double.doubleToLongBits(d));
    }
}
