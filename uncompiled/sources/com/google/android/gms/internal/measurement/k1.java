package com.google.android.gms.internal.measurement;

import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* loaded from: classes.dex */
public final class k1 extends x1<k1, ll5> implements xx5 {
    private static final k1 zze;
    private zv5<l1> zza = x1.o();

    static {
        k1 k1Var = new k1();
        zze = k1Var;
        x1.u(k1.class, k1Var);
    }

    public static k1 z() {
        return zze;
    }

    @Override // com.google.android.gms.internal.measurement.x1
    public final Object w(int i, Object obj, Object obj2) {
        int i2 = i - 1;
        if (i2 != 0) {
            if (i2 != 2) {
                if (i2 != 3) {
                    if (i2 != 4) {
                        if (i2 != 5) {
                            return null;
                        }
                        return zze;
                    }
                    return new ll5(null);
                }
                return new k1();
            }
            return x1.v(zze, "\u0001\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001\u001b", new Object[]{"zza", l1.class});
        }
        return (byte) 1;
    }

    public final List<l1> x() {
        return this.zza;
    }

    public final int y() {
        return this.zza.size();
    }
}
