package com.google.android.gms.internal.measurement;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* loaded from: classes.dex */
public final class n extends c implements p {
    public n(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.measurement.api.internal.IEventHandlerProxy");
    }

    @Override // com.google.android.gms.internal.measurement.p
    public final int c() throws RemoteException {
        Parcel b = b(2, F1());
        int readInt = b.readInt();
        b.recycle();
        return readInt;
    }

    @Override // com.google.android.gms.internal.measurement.p
    public final void y(String str, String str2, Bundle bundle, long j) throws RemoteException {
        Parcel F1 = F1();
        F1.writeString(str);
        F1.writeString(str2);
        a95.d(F1, bundle);
        F1.writeLong(j);
        G1(1, F1);
    }
}
