package com.google.android.gms.internal.measurement;

import android.app.Activity;
import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@19.0.0 */
/* loaded from: classes.dex */
public final class o0 extends i0 {
    public final /* synthetic */ Activity i0;
    public final /* synthetic */ aa5 j0;
    public final /* synthetic */ uf5 k0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public o0(uf5 uf5Var, Activity activity, aa5 aa5Var) {
        super(uf5Var.a, true);
        this.k0 = uf5Var;
        this.i0 = activity;
        this.j0 = aa5Var;
    }

    @Override // com.google.android.gms.internal.measurement.i0
    public final void a() throws RemoteException {
        j jVar;
        jVar = this.k0.a.h;
        ((j) zt2.j(jVar)).onActivitySaveInstanceState(nl2.H1(this.i0), this.j0, this.f0);
    }
}
