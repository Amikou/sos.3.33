package com.google.android.gms.internal.measurement;

import com.github.mikephil.charting.utils.Utils;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* loaded from: classes.dex */
public enum zzmy {
    INT(0),
    LONG(0L),
    FLOAT(Float.valueOf((float) Utils.FLOAT_EPSILON)),
    DOUBLE(Double.valueOf((double) Utils.DOUBLE_EPSILON)),
    BOOLEAN(Boolean.FALSE),
    STRING(""),
    BYTE_STRING(zzjd.zzb),
    ENUM(null),
    MESSAGE(null);
    
    private final Object zzj;

    zzmy(Object obj) {
        this.zzj = obj;
    }
}
