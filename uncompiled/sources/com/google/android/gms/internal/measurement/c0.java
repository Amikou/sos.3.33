package com.google.android.gms.internal.measurement;

import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@19.0.0 */
/* loaded from: classes.dex */
public final class c0 extends i0 {
    public final /* synthetic */ String i0;
    public final /* synthetic */ Object j0;
    public final /* synthetic */ wf5 k0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public c0(wf5 wf5Var, boolean z, int i, String str, Object obj, Object obj2, Object obj3) {
        super(wf5Var, false);
        this.k0 = wf5Var;
        this.i0 = str;
        this.j0 = obj;
    }

    @Override // com.google.android.gms.internal.measurement.i0
    public final void a() throws RemoteException {
        j jVar;
        jVar = this.k0.h;
        ((j) zt2.j(jVar)).logHealthData(5, this.i0, nl2.H1(this.j0), nl2.H1(null), nl2.H1(null));
    }
}
