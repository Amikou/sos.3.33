package com.google.android.gms.internal.measurement;

import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@19.0.0 */
/* loaded from: classes.dex */
public abstract class i0 implements Runnable {
    public final long a;
    public final long f0;
    public final boolean g0;
    public final /* synthetic */ wf5 h0;

    public i0(wf5 wf5Var, boolean z) {
        this.h0 = wf5Var;
        this.a = wf5Var.b.a();
        this.f0 = wf5Var.b.b();
        this.g0 = z;
    }

    public abstract void a() throws RemoteException;

    public void b() {
    }

    @Override // java.lang.Runnable
    public final void run() {
        boolean z;
        z = this.h0.g;
        if (z) {
            b();
            return;
        }
        try {
            a();
        } catch (Exception e) {
            this.h0.o(e, false, this.g0);
            b();
        }
    }
}
