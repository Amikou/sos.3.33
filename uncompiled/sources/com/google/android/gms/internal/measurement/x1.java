package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.w1;
import com.google.android.gms.internal.measurement.x1;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* loaded from: classes.dex */
public abstract class x1<MessageType extends x1<MessageType, BuilderType>, BuilderType extends w1<MessageType, BuilderType>> extends zq5<MessageType, BuilderType> {
    private static final Map<Object, x1<?, ?>> zza = new ConcurrentHashMap();
    public f2 zzc = f2.a();
    public int zzd = -1;

    public static Object k(Method method, Object obj, Object... objArr) {
        try {
            return method.invoke(obj, objArr);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Couldn't use Java reflection to implement protocol message reflection.", e);
        } catch (InvocationTargetException e2) {
            Throwable cause = e2.getCause();
            if (!(cause instanceof RuntimeException)) {
                if (cause instanceof Error) {
                    throw ((Error) cause);
                }
                throw new RuntimeException("Unexpected exception thrown by generated accessor method.", cause);
            }
            throw ((RuntimeException) cause);
        }
    }

    public static vv5 l() {
        return kv5.i();
    }

    public static xv5 m() {
        return yw5.i();
    }

    public static xv5 n(xv5 xv5Var) {
        int size = xv5Var.size();
        return xv5Var.C(size == 0 ? 10 : size + size);
    }

    public static <E> zv5<E> o() {
        return ly5.i();
    }

    public static <E> zv5<E> p(zv5<E> zv5Var) {
        int size = zv5Var.size();
        return zv5Var.Q(size == 0 ? 10 : size + size);
    }

    public static <T extends x1> T t(Class<T> cls) {
        Map<Object, x1<?, ?>> map = zza;
        x1<?, ?> x1Var = map.get(cls);
        if (x1Var == null) {
            try {
                Class.forName(cls.getName(), true, cls.getClassLoader());
                x1Var = map.get(cls);
            } catch (ClassNotFoundException e) {
                throw new IllegalStateException("Class initialization cannot fail.", e);
            }
        }
        if (x1Var == null) {
            x1Var = (x1) ((x1) l2.h(cls)).w(6, null, null);
            if (x1Var != null) {
                map.put(cls, x1Var);
            } else {
                throw new IllegalStateException();
            }
        }
        return x1Var;
    }

    public static <T extends x1> void u(Class<T> cls, T t) {
        zza.put(cls, t);
    }

    public static Object v(z1 z1Var, String str, Object[] objArr) {
        return new ny5(z1Var, str, objArr);
    }

    @Override // com.google.android.gms.internal.measurement.z1
    public final /* bridge */ /* synthetic */ y1 a() {
        return (w1) w(5, null, null);
    }

    @Override // com.google.android.gms.internal.measurement.z1
    public final /* bridge */ /* synthetic */ y1 b() {
        w1 w1Var = (w1) w(5, null, null);
        w1Var.p(this);
        return w1Var;
    }

    @Override // com.google.android.gms.internal.measurement.z1
    public final int e() {
        int i = this.zzd;
        if (i == -1) {
            int d = jy5.a().b(getClass()).d(this);
            this.zzd = d;
            return d;
        }
        return i;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass()) {
            return jy5.a().b(getClass()).c(this, (x1) obj);
        }
        return false;
    }

    @Override // com.google.android.gms.internal.measurement.z1
    public final void f(t1 t1Var) throws IOException {
        jy5.a().b(getClass()).g(this, u1.l(t1Var));
    }

    @Override // defpackage.xx5
    public final /* bridge */ /* synthetic */ z1 g() {
        return (x1) w(6, null, null);
    }

    @Override // defpackage.zq5
    public final int h() {
        return this.zzd;
    }

    public final int hashCode() {
        int i = this.zzb;
        if (i != 0) {
            return i;
        }
        int a = jy5.a().b(getClass()).a(this);
        this.zzb = a;
        return a;
    }

    @Override // defpackage.zq5
    public final void i(int i) {
        this.zzd = i;
    }

    public final <MessageType extends x1<MessageType, BuilderType>, BuilderType extends w1<MessageType, BuilderType>> BuilderType r() {
        return (BuilderType) w(5, null, null);
    }

    public final BuilderType s() {
        BuilderType buildertype = (BuilderType) w(5, null, null);
        buildertype.p(this);
        return buildertype;
    }

    public final String toString() {
        return zx5.a(this, super.toString());
    }

    public abstract Object w(int i, Object obj, Object obj2);
}
