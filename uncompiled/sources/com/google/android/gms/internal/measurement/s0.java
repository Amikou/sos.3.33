package com.google.android.gms.internal.measurement;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* loaded from: classes.dex */
public final class s0 extends x1<s0, ig5> implements xx5 {
    private static final s0 zzi;
    private int zza;
    private v0 zze;
    private t0 zzf;
    private boolean zzg;
    private String zzh = "";

    static {
        s0 s0Var = new s0();
        zzi = s0Var;
        x1.u(s0.class, s0Var);
    }

    public static s0 F() {
        return zzi;
    }

    public static /* synthetic */ void H(s0 s0Var, String str) {
        s0Var.zza |= 8;
        s0Var.zzh = str;
    }

    public final t0 A() {
        t0 t0Var = this.zzf;
        return t0Var == null ? t0.H() : t0Var;
    }

    public final boolean B() {
        return (this.zza & 4) != 0;
    }

    public final boolean C() {
        return this.zzg;
    }

    public final boolean D() {
        return (this.zza & 8) != 0;
    }

    public final String E() {
        return this.zzh;
    }

    @Override // com.google.android.gms.internal.measurement.x1
    public final Object w(int i, Object obj, Object obj2) {
        int i2 = i - 1;
        if (i2 != 0) {
            if (i2 != 2) {
                if (i2 != 3) {
                    if (i2 != 4) {
                        if (i2 != 5) {
                            return null;
                        }
                        return zzi;
                    }
                    return new ig5(null);
                }
                return new s0();
            }
            return x1.v(zzi, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဉ\u0001\u0003ဇ\u0002\u0004ဈ\u0003", new Object[]{"zza", "zze", "zzf", "zzg", "zzh"});
        }
        return (byte) 1;
    }

    public final boolean x() {
        return (this.zza & 1) != 0;
    }

    public final v0 y() {
        v0 v0Var = this.zze;
        return v0Var == null ? v0.F() : v0Var;
    }

    public final boolean z() {
        return (this.zza & 2) != 0;
    }
}
