package com.google.android.gms.internal.measurement;

import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* loaded from: classes.dex */
public final class n1 extends x1<n1, ul5> implements xx5 {
    private static final n1 zzk;
    private int zza;
    private int zze;
    private zv5<n1> zzf = x1.o();
    private String zzg = "";
    private String zzh = "";
    private boolean zzi;
    private double zzj;

    static {
        n1 n1Var = new n1();
        zzk = n1Var;
        x1.u(n1.class, n1Var);
    }

    public final boolean A() {
        return (this.zza & 4) != 0;
    }

    public final String B() {
        return this.zzh;
    }

    public final boolean C() {
        return (this.zza & 8) != 0;
    }

    public final boolean D() {
        return this.zzi;
    }

    public final boolean E() {
        return (this.zza & 16) != 0;
    }

    public final double F() {
        return this.zzj;
    }

    @Override // com.google.android.gms.internal.measurement.x1
    public final Object w(int i, Object obj, Object obj2) {
        int i2 = i - 1;
        if (i2 != 0) {
            if (i2 != 2) {
                if (i2 != 3) {
                    if (i2 != 4) {
                        if (i2 != 5) {
                            return null;
                        }
                        return zzk;
                    }
                    return new ul5(null);
                }
                return new n1();
            }
            return x1.v(zzk, "\u0001\u0006\u0000\u0001\u0001\u0006\u0006\u0000\u0001\u0000\u0001ဌ\u0000\u0002\u001b\u0003ဈ\u0001\u0004ဈ\u0002\u0005ဇ\u0003\u0006က\u0004", new Object[]{"zza", "zze", zzgs.zzb(), "zzf", n1.class, "zzg", "zzh", "zzi", "zzj"});
        }
        return (byte) 1;
    }

    public final zzgs x() {
        zzgs zza = zzgs.zza(this.zze);
        return zza == null ? zzgs.UNKNOWN : zza;
    }

    public final List<n1> y() {
        return this.zzf;
    }

    public final String z() {
        return this.zzg;
    }
}
