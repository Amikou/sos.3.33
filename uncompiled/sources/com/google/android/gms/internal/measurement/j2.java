package com.google.android.gms.internal.measurement;

import sun.misc.Unsafe;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* loaded from: classes.dex */
public final class j2 extends k2 {
    public j2(Unsafe unsafe) {
        super(unsafe);
    }

    @Override // com.google.android.gms.internal.measurement.k2
    public final void a(Object obj, long j, byte b) {
        if (l2.i) {
            l2.d(obj, j, b);
        } else {
            l2.e(obj, j, b);
        }
    }

    @Override // com.google.android.gms.internal.measurement.k2
    public final boolean b(Object obj, long j) {
        if (l2.i) {
            return l2.A(obj, j);
        }
        return l2.B(obj, j);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: InlineMethods
        jadx.core.utils.exceptions.JadxRuntimeException: Failed to process method for inline: com.google.android.gms.internal.measurement.l2.C(java.lang.Object, long, boolean):void
        	at jadx.core.dex.visitors.InlineMethods.processInvokeInsn(InlineMethods.java:76)
        	at jadx.core.dex.visitors.InlineMethods.visit(InlineMethods.java:51)
        Caused by: java.lang.ArrayIndexOutOfBoundsException: arraycopy: length -1 is negative
        	at java.base/java.lang.System.arraycopy(Native Method)
        	at java.base/java.util.ArrayList.shiftTailOverGap(ArrayList.java:746)
        	at java.base/java.util.ArrayList.removeIf(ArrayList.java:1691)
        	at java.base/java.util.ArrayList.removeIf(ArrayList.java:1660)
        	at jadx.core.dex.instructions.args.SSAVar.removeUse(SSAVar.java:130)
        	at jadx.core.dex.instructions.args.SSAVar.use(SSAVar.java:123)
        	at jadx.core.dex.nodes.InsnNode.rebindArgs(InsnNode.java:481)
        	at jadx.core.dex.instructions.mods.TernaryInsn.rebindArgs(TernaryInsn.java:92)
        	at jadx.core.dex.nodes.InsnNode.rebindArgs(InsnNode.java:484)
        	at jadx.core.utils.BlockUtils.replaceInsn(BlockUtils.java:1079)
        	at jadx.core.utils.BlockUtils.replaceInsn(BlockUtils.java:1088)
        	at jadx.core.dex.visitors.InlineMethods.inlineMethod(InlineMethods.java:115)
        	at jadx.core.dex.visitors.InlineMethods.processInvokeInsn(InlineMethods.java:74)
        	... 1 more
        */
    @Override // com.google.android.gms.internal.measurement.k2
    public final void c(java.lang.Object r2, long r3, boolean r5) {
        /*
            r1 = this;
            boolean r0 = com.google.android.gms.internal.measurement.l2.i
            if (r0 == 0) goto L8
            com.google.android.gms.internal.measurement.l2.C(r2, r3, r5)
            return
        L8:
            com.google.android.gms.internal.measurement.l2.D(r2, r3, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.j2.c(java.lang.Object, long, boolean):void");
    }

    @Override // com.google.android.gms.internal.measurement.k2
    public final float d(Object obj, long j) {
        return Float.intBitsToFloat(k(obj, j));
    }

    @Override // com.google.android.gms.internal.measurement.k2
    public final void e(Object obj, long j, float f) {
        l(obj, j, Float.floatToIntBits(f));
    }

    @Override // com.google.android.gms.internal.measurement.k2
    public final double f(Object obj, long j) {
        return Double.longBitsToDouble(m(obj, j));
    }

    @Override // com.google.android.gms.internal.measurement.k2
    public final void g(Object obj, long j, double d) {
        n(obj, j, Double.doubleToLongBits(d));
    }
}
