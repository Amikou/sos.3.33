package com.google.android.gms.internal.measurement;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* loaded from: classes.dex */
public final class c1 extends x1<c1, wj5> implements xx5 {
    private static final c1 zzg;
    private int zza;
    private String zze = "";
    private long zzf;

    static {
        c1 c1Var = new c1();
        zzg = c1Var;
        x1.u(c1.class, c1Var);
    }

    public static /* synthetic */ void A(c1 c1Var, long j) {
        c1Var.zza |= 2;
        c1Var.zzf = j;
    }

    public static wj5 x() {
        return zzg.r();
    }

    public static /* synthetic */ void z(c1 c1Var, String str) {
        str.getClass();
        c1Var.zza |= 1;
        c1Var.zze = str;
    }

    @Override // com.google.android.gms.internal.measurement.x1
    public final Object w(int i, Object obj, Object obj2) {
        int i2 = i - 1;
        if (i2 != 0) {
            if (i2 != 2) {
                if (i2 != 3) {
                    if (i2 != 4) {
                        if (i2 != 5) {
                            return null;
                        }
                        return zzg;
                    }
                    return new wj5(null);
                }
                return new c1();
            }
            return x1.v(zzg, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001ဈ\u0000\u0002ဂ\u0001", new Object[]{"zza", "zze", "zzf"});
        }
        return (byte) 1;
    }
}
