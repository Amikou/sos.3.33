package com.google.android.gms.internal.measurement;

import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* loaded from: classes.dex */
public final class e1 extends x1<e1, bk5> implements xx5 {
    private static final e1 zze;
    private zv5<f1> zza = x1.o();

    static {
        e1 e1Var = new e1();
        zze = e1Var;
        x1.u(e1.class, e1Var);
    }

    public static /* synthetic */ void B(e1 e1Var, f1 f1Var) {
        f1Var.getClass();
        zv5<f1> zv5Var = e1Var.zza;
        if (!zv5Var.zza()) {
            e1Var.zza = x1.p(zv5Var);
        }
        e1Var.zza.add(f1Var);
    }

    public static bk5 z() {
        return zze.r();
    }

    @Override // com.google.android.gms.internal.measurement.x1
    public final Object w(int i, Object obj, Object obj2) {
        int i2 = i - 1;
        if (i2 != 0) {
            if (i2 != 2) {
                if (i2 != 3) {
                    if (i2 != 4) {
                        if (i2 != 5) {
                            return null;
                        }
                        return zze;
                    }
                    return new bk5(null);
                }
                return new e1();
            }
            return x1.v(zze, "\u0001\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001\u001b", new Object[]{"zza", f1.class});
        }
        return (byte) 1;
    }

    public final List<f1> x() {
        return this.zza;
    }

    public final f1 y(int i) {
        return this.zza.get(0);
    }
}
