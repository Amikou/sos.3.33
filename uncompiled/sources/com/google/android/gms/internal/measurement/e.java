package com.google.android.gms.internal.measurement;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* loaded from: classes.dex */
public final class e extends c implements f {
    public e(IBinder iBinder) {
        super(iBinder, "com.google.android.finsky.externalreferrer.IGetInstallReferrerService");
    }

    @Override // com.google.android.gms.internal.measurement.f
    public final Bundle W(Bundle bundle) throws RemoteException {
        Parcel F1 = F1();
        a95.d(F1, bundle);
        Parcel b = b(1, F1);
        Bundle bundle2 = (Bundle) a95.c(b, Bundle.CREATOR);
        b.recycle();
        return bundle2;
    }
}
