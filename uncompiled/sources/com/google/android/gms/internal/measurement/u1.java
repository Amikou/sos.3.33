package com.google.android.gms.internal.measurement;

import java.io.IOException;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* loaded from: classes.dex */
public final class u1 {
    public final t1 a;

    public u1(t1 t1Var) {
        cw5.b(t1Var, "output");
        this.a = t1Var;
        t1Var.a = this;
    }

    public static u1 l(t1 t1Var) {
        u1 u1Var = t1Var.a;
        return u1Var != null ? u1Var : new u1(t1Var);
    }

    public final void A(int i, int i2) throws IOException {
        this.a.i(i, (i2 >> 31) ^ (i2 + i2));
    }

    public final void B(int i, long j) throws IOException {
        this.a.k(i, (j >> 63) ^ (j + j));
    }

    public final void C(int i, Object obj, c2 c2Var) throws IOException {
        z1 z1Var = (z1) obj;
        s1 s1Var = (s1) this.a;
        s1Var.r((i << 3) | 2);
        zq5 zq5Var = (zq5) z1Var;
        int h = zq5Var.h();
        if (h == -1) {
            h = c2Var.d(zq5Var);
            zq5Var.i(h);
        }
        s1Var.r(h);
        c2Var.g(z1Var, s1Var.a);
    }

    public final void D(int i, Object obj, c2 c2Var) throws IOException {
        t1 t1Var = this.a;
        t1Var.g(i, 3);
        c2Var.g((z1) obj, t1Var.a);
        t1Var.g(i, 4);
    }

    public final void E(int i) throws IOException {
        this.a.g(i, 3);
    }

    public final void F(int i) throws IOException {
        this.a.g(i, 4);
    }

    public final void G(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.g(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += t1.z(list.get(i4).intValue());
            }
            this.a.r(i3);
            while (i2 < list.size()) {
                this.a.q(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.h(i, list.get(i2).intValue());
            i2++;
        }
    }

    public final void H(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.g(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                list.get(i4).intValue();
                i3 += 4;
            }
            this.a.r(i3);
            while (i2 < list.size()) {
                this.a.s(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.j(i, list.get(i2).intValue());
            i2++;
        }
    }

    public final void I(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.g(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += t1.B(list.get(i4).longValue());
            }
            this.a.r(i3);
            while (i2 < list.size()) {
                this.a.t(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.k(i, list.get(i2).longValue());
            i2++;
        }
    }

    public final void J(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.g(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += t1.B(list.get(i4).longValue());
            }
            this.a.r(i3);
            while (i2 < list.size()) {
                this.a.t(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.k(i, list.get(i2).longValue());
            i2++;
        }
    }

    public final void K(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.g(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                list.get(i4).longValue();
                i3 += 8;
            }
            this.a.r(i3);
            while (i2 < list.size()) {
                this.a.u(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.l(i, list.get(i2).longValue());
            i2++;
        }
    }

    public final void a(int i, List<Float> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.g(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                list.get(i4).floatValue();
                i3 += 4;
            }
            this.a.r(i3);
            while (i2 < list.size()) {
                this.a.s(Float.floatToRawIntBits(list.get(i2).floatValue()));
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.j(i, Float.floatToRawIntBits(list.get(i2).floatValue()));
            i2++;
        }
    }

    public final void b(int i, List<Double> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.g(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                list.get(i4).doubleValue();
                i3 += 8;
            }
            this.a.r(i3);
            while (i2 < list.size()) {
                this.a.u(Double.doubleToRawLongBits(list.get(i2).doubleValue()));
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.l(i, Double.doubleToRawLongBits(list.get(i2).doubleValue()));
            i2++;
        }
    }

    public final void c(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.g(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += t1.z(list.get(i4).intValue());
            }
            this.a.r(i3);
            while (i2 < list.size()) {
                this.a.q(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.h(i, list.get(i2).intValue());
            i2++;
        }
    }

    public final void d(int i, List<Boolean> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.g(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                list.get(i4).booleanValue();
                i3++;
            }
            this.a.r(i3);
            while (i2 < list.size()) {
                this.a.p(list.get(i2).booleanValue() ? (byte) 1 : (byte) 0);
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.m(i, list.get(i2).booleanValue());
            i2++;
        }
    }

    public final void e(int i, List<String> list) throws IOException {
        int i2 = 0;
        if (list instanceof nw5) {
            nw5 nw5Var = (nw5) list;
            while (i2 < list.size()) {
                Object v1 = nw5Var.v1(i2);
                if (v1 instanceof String) {
                    this.a.n(i, (String) v1);
                } else {
                    this.a.o(i, (zzjd) v1);
                }
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.n(i, list.get(i2));
            i2++;
        }
    }

    public final void f(int i, List<zzjd> list) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            this.a.o(i, list.get(i2));
        }
    }

    public final void g(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.g(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += t1.A(list.get(i4).intValue());
            }
            this.a.r(i3);
            while (i2 < list.size()) {
                this.a.r(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.i(i, list.get(i2).intValue());
            i2++;
        }
    }

    public final void h(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.g(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                list.get(i4).intValue();
                i3 += 4;
            }
            this.a.r(i3);
            while (i2 < list.size()) {
                this.a.s(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.j(i, list.get(i2).intValue());
            i2++;
        }
    }

    public final void i(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.g(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                list.get(i4).longValue();
                i3 += 8;
            }
            this.a.r(i3);
            while (i2 < list.size()) {
                this.a.u(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.l(i, list.get(i2).longValue());
            i2++;
        }
    }

    public final void j(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.g(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                int intValue = list.get(i4).intValue();
                i3 += t1.A((intValue >> 31) ^ (intValue + intValue));
            }
            this.a.r(i3);
            while (i2 < list.size()) {
                t1 t1Var = this.a;
                int intValue2 = list.get(i2).intValue();
                t1Var.r((intValue2 >> 31) ^ (intValue2 + intValue2));
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            t1 t1Var2 = this.a;
            int intValue3 = list.get(i2).intValue();
            t1Var2.i(i, (intValue3 >> 31) ^ (intValue3 + intValue3));
            i2++;
        }
    }

    public final void k(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.g(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                long longValue = list.get(i4).longValue();
                i3 += t1.B((longValue >> 63) ^ (longValue + longValue));
            }
            this.a.r(i3);
            while (i2 < list.size()) {
                t1 t1Var = this.a;
                long longValue2 = list.get(i2).longValue();
                t1Var.t((longValue2 >> 63) ^ (longValue2 + longValue2));
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            t1 t1Var2 = this.a;
            long longValue3 = list.get(i2).longValue();
            t1Var2.k(i, (longValue3 >> 63) ^ (longValue3 + longValue3));
            i2++;
        }
    }

    public final void m(int i, int i2) throws IOException {
        this.a.j(i, i2);
    }

    public final void n(int i, long j) throws IOException {
        this.a.k(i, j);
    }

    public final void o(int i, long j) throws IOException {
        this.a.l(i, j);
    }

    public final void p(int i, float f) throws IOException {
        this.a.j(i, Float.floatToRawIntBits(f));
    }

    public final void q(int i, double d) throws IOException {
        this.a.l(i, Double.doubleToRawLongBits(d));
    }

    public final void r(int i, int i2) throws IOException {
        this.a.h(i, i2);
    }

    public final void s(int i, long j) throws IOException {
        this.a.k(i, j);
    }

    public final void t(int i, int i2) throws IOException {
        this.a.h(i, i2);
    }

    public final void u(int i, long j) throws IOException {
        this.a.l(i, j);
    }

    public final void v(int i, int i2) throws IOException {
        this.a.j(i, i2);
    }

    public final void w(int i, boolean z) throws IOException {
        this.a.m(i, z);
    }

    public final void x(int i, String str) throws IOException {
        this.a.n(i, str);
    }

    public final void y(int i, zzjd zzjdVar) throws IOException {
        this.a.o(i, zzjdVar);
    }

    public final void z(int i, int i2) throws IOException {
        this.a.i(i, i2);
    }
}
