package com.google.android.gms.internal.measurement;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* loaded from: classes.dex */
public final class g1 extends x1<g1, ik5> implements xx5 {
    private static final g1 zzg;
    private int zza;
    private int zze = 1;
    private zv5<c1> zzf = x1.o();

    static {
        g1 g1Var = new g1();
        zzg = g1Var;
        x1.u(g1.class, g1Var);
    }

    public static ik5 x() {
        return zzg.r();
    }

    public static /* synthetic */ void z(g1 g1Var, c1 c1Var) {
        c1Var.getClass();
        zv5<c1> zv5Var = g1Var.zzf;
        if (!zv5Var.zza()) {
            g1Var.zzf = x1.p(zv5Var);
        }
        g1Var.zzf.add(c1Var);
    }

    @Override // com.google.android.gms.internal.measurement.x1
    public final Object w(int i, Object obj, Object obj2) {
        int i2 = i - 1;
        if (i2 != 0) {
            if (i2 != 2) {
                if (i2 != 3) {
                    if (i2 != 4) {
                        if (i2 != 5) {
                            return null;
                        }
                        return zzg;
                    }
                    return new ik5(null);
                }
                return new g1();
            }
            return x1.v(zzg, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0001\u0000\u0001ဌ\u0000\u0002\u001b", new Object[]{"zza", "zze", zzga.zzb(), "zzf", c1.class});
        }
        return (byte) 1;
    }
}
