package com.google.android.gms.internal.measurement;

import android.app.Activity;
import android.os.Bundle;
import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@19.0.0 */
/* loaded from: classes.dex */
public final class j0 extends i0 {
    public final /* synthetic */ Bundle i0;
    public final /* synthetic */ Activity j0;
    public final /* synthetic */ uf5 k0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public j0(uf5 uf5Var, Bundle bundle, Activity activity) {
        super(uf5Var.a, true);
        this.k0 = uf5Var;
        this.i0 = bundle;
        this.j0 = activity;
    }

    @Override // com.google.android.gms.internal.measurement.i0
    public final void a() throws RemoteException {
        Bundle bundle;
        j jVar;
        if (this.i0 != null) {
            bundle = new Bundle();
            if (this.i0.containsKey("com.google.app_measurement.screen_service")) {
                Object obj = this.i0.get("com.google.app_measurement.screen_service");
                if (obj instanceof Bundle) {
                    bundle.putBundle("com.google.app_measurement.screen_service", (Bundle) obj);
                }
            }
        } else {
            bundle = null;
        }
        jVar = this.k0.a.h;
        ((j) zt2.j(jVar)).onActivityCreated(nl2.H1(this.j0), bundle, this.f0);
    }
}
