package com.google.android.gms.internal.measurement;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* loaded from: classes.dex */
public final class a1 extends x1<a1, rj5> implements xx5 {
    private static final a1 zzg;
    private int zza;
    private int zze;
    private long zzf;

    static {
        a1 a1Var = new a1();
        zzg = a1Var;
        x1.u(a1.class, a1Var);
    }

    public static rj5 B() {
        return zzg.r();
    }

    public static /* synthetic */ void D(a1 a1Var, int i) {
        a1Var.zza |= 1;
        a1Var.zze = i;
    }

    public static /* synthetic */ void E(a1 a1Var, long j) {
        a1Var.zza |= 2;
        a1Var.zzf = j;
    }

    public final long A() {
        return this.zzf;
    }

    @Override // com.google.android.gms.internal.measurement.x1
    public final Object w(int i, Object obj, Object obj2) {
        int i2 = i - 1;
        if (i2 != 0) {
            if (i2 != 2) {
                if (i2 != 3) {
                    if (i2 != 4) {
                        if (i2 != 5) {
                            return null;
                        }
                        return zzg;
                    }
                    return new rj5(null);
                }
                return new a1();
            }
            return x1.v(zzg, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001င\u0000\u0002ဂ\u0001", new Object[]{"zza", "zze", "zzf"});
        }
        return (byte) 1;
    }

    public final boolean x() {
        return (this.zza & 1) != 0;
    }

    public final int y() {
        return this.zze;
    }

    public final boolean z() {
        return (this.zza & 2) != 0;
    }
}
