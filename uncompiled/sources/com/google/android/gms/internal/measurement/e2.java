package com.google.android.gms.internal.measurement;

import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* loaded from: classes.dex */
public abstract class e2<T, B> {
    public abstract void a(B b, int i, long j);

    public abstract B b();

    public abstract void c(Object obj, T t);

    public abstract T d(Object obj);

    public abstract void e(Object obj);

    public abstract T f(T t, T t2);

    public abstract int g(T t);

    public abstract int h(T t);

    public abstract void i(T t, u1 u1Var) throws IOException;
}
