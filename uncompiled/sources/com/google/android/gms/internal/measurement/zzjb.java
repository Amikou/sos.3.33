package com.google.android.gms.internal.measurement;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Objects;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* loaded from: classes.dex */
public class zzjb extends zzja {
    public final byte[] zza;

    public zzjb(byte[] bArr) {
        Objects.requireNonNull(bArr);
        this.zza = bArr;
    }

    @Override // com.google.android.gms.internal.measurement.zzjd
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if ((obj instanceof zzjd) && zzc() == ((zzjd) obj).zzc()) {
            if (zzc() == 0) {
                return true;
            }
            if (obj instanceof zzjb) {
                zzjb zzjbVar = (zzjb) obj;
                int zzm = zzm();
                int zzm2 = zzjbVar.zzm();
                if (zzm == 0 || zzm2 == 0 || zzm == zzm2) {
                    int zzc = zzc();
                    if (zzc <= zzjbVar.zzc()) {
                        if (zzc <= zzjbVar.zzc()) {
                            byte[] bArr = this.zza;
                            byte[] bArr2 = zzjbVar.zza;
                            zzjbVar.zzd();
                            int i = 0;
                            int i2 = 0;
                            while (i < zzc) {
                                if (bArr[i] != bArr2[i2]) {
                                    return false;
                                }
                                i++;
                                i2++;
                            }
                            return true;
                        }
                        int zzc2 = zzjbVar.zzc();
                        StringBuilder sb = new StringBuilder(59);
                        sb.append("Ran off end of other: 0, ");
                        sb.append(zzc);
                        sb.append(", ");
                        sb.append(zzc2);
                        throw new IllegalArgumentException(sb.toString());
                    }
                    int zzc3 = zzc();
                    StringBuilder sb2 = new StringBuilder(40);
                    sb2.append("Length too large: ");
                    sb2.append(zzc);
                    sb2.append(zzc3);
                    throw new IllegalArgumentException(sb2.toString());
                }
                return false;
            }
            return obj.equals(this);
        }
        return false;
    }

    @Override // com.google.android.gms.internal.measurement.zzjd
    public byte zza(int i) {
        return this.zza[i];
    }

    @Override // com.google.android.gms.internal.measurement.zzjd
    public byte zzb(int i) {
        return this.zza[i];
    }

    @Override // com.google.android.gms.internal.measurement.zzjd
    public int zzc() {
        return this.zza.length;
    }

    public int zzd() {
        return 0;
    }

    @Override // com.google.android.gms.internal.measurement.zzjd
    public final zzjd zze(int i, int i2) {
        int zzn = zzjd.zzn(0, i2, zzc());
        return zzn == 0 ? zzjd.zzb : new zziy(this.zza, 0, zzn);
    }

    @Override // com.google.android.gms.internal.measurement.zzjd
    public final void zzf(mr5 mr5Var) throws IOException {
        ((s1) mr5Var).E(this.zza, 0, zzc());
    }

    @Override // com.google.android.gms.internal.measurement.zzjd
    public final String zzg(Charset charset) {
        return new String(this.zza, 0, zzc(), charset);
    }

    @Override // com.google.android.gms.internal.measurement.zzjd
    public final boolean zzh() {
        return n2.b(this.zza, 0, zzc());
    }

    @Override // com.google.android.gms.internal.measurement.zzjd
    public final int zzi(int i, int i2, int i3) {
        return cw5.h(i, this.zza, 0, i3);
    }
}
