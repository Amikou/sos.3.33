package com.google.android.gms.internal.measurement;

import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* loaded from: classes.dex */
public final class q1 {
    public static int a(byte[] bArr, int i, fr5 fr5Var) {
        int i2 = i + 1;
        byte b = bArr[i];
        if (b >= 0) {
            fr5Var.a = b;
            return i2;
        }
        return b(b, bArr, i2, fr5Var);
    }

    public static int b(int i, byte[] bArr, int i2, fr5 fr5Var) {
        int i3 = i & 127;
        int i4 = i2 + 1;
        byte b = bArr[i2];
        if (b >= 0) {
            fr5Var.a = i3 | (b << 7);
            return i4;
        }
        int i5 = i3 | ((b & Byte.MAX_VALUE) << 7);
        int i6 = i4 + 1;
        byte b2 = bArr[i4];
        if (b2 >= 0) {
            fr5Var.a = i5 | (b2 << 14);
            return i6;
        }
        int i7 = i5 | ((b2 & Byte.MAX_VALUE) << 14);
        int i8 = i6 + 1;
        byte b3 = bArr[i6];
        if (b3 >= 0) {
            fr5Var.a = i7 | (b3 << 21);
            return i8;
        }
        int i9 = i7 | ((b3 & Byte.MAX_VALUE) << 21);
        int i10 = i8 + 1;
        byte b4 = bArr[i8];
        if (b4 >= 0) {
            fr5Var.a = i9 | (b4 << 28);
            return i10;
        }
        int i11 = i9 | ((b4 & Byte.MAX_VALUE) << 28);
        while (true) {
            int i12 = i10 + 1;
            if (bArr[i10] >= 0) {
                fr5Var.a = i11;
                return i12;
            }
            i10 = i12;
        }
    }

    public static int c(byte[] bArr, int i, fr5 fr5Var) {
        byte b;
        int i2 = i + 1;
        long j = bArr[i];
        if (j >= 0) {
            fr5Var.b = j;
            return i2;
        }
        int i3 = i2 + 1;
        byte b2 = bArr[i2];
        long j2 = (j & 127) | ((b2 & Byte.MAX_VALUE) << 7);
        int i4 = 7;
        while (b2 < 0) {
            int i5 = i3 + 1;
            i4 += 7;
            j2 |= (b & Byte.MAX_VALUE) << i4;
            b2 = bArr[i3];
            i3 = i5;
        }
        fr5Var.b = j2;
        return i3;
    }

    public static int d(byte[] bArr, int i) {
        return ((bArr[i + 3] & 255) << 24) | (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << 16);
    }

    public static long e(byte[] bArr, int i) {
        return ((bArr[i + 7] & 255) << 56) | (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << 16) | ((bArr[i + 3] & 255) << 24) | ((bArr[i + 4] & 255) << 32) | ((bArr[i + 5] & 255) << 40) | ((bArr[i + 6] & 255) << 48);
    }

    public static int f(byte[] bArr, int i, fr5 fr5Var) throws zzkn {
        int a = a(bArr, i, fr5Var);
        int i2 = fr5Var.a;
        if (i2 >= 0) {
            if (i2 == 0) {
                fr5Var.c = "";
                return a;
            }
            fr5Var.c = new String(bArr, a, i2, cw5.a);
            return a + i2;
        }
        throw zzkn.zzb();
    }

    public static int g(byte[] bArr, int i, fr5 fr5Var) throws zzkn {
        int a = a(bArr, i, fr5Var);
        int i2 = fr5Var.a;
        if (i2 >= 0) {
            if (i2 == 0) {
                fr5Var.c = "";
                return a;
            }
            fr5Var.c = n2.e(bArr, a, i2);
            return a + i2;
        }
        throw zzkn.zzb();
    }

    public static int h(byte[] bArr, int i, fr5 fr5Var) throws zzkn {
        int a = a(bArr, i, fr5Var);
        int i2 = fr5Var.a;
        if (i2 >= 0) {
            if (i2 <= bArr.length - a) {
                if (i2 == 0) {
                    fr5Var.c = zzjd.zzb;
                    return a;
                }
                fr5Var.c = zzjd.zzj(bArr, a, i2);
                return a + i2;
            }
            throw zzkn.zza();
        }
        throw zzkn.zzb();
    }

    public static int i(c2 c2Var, byte[] bArr, int i, int i2, fr5 fr5Var) throws IOException {
        int i3 = i + 1;
        int i4 = bArr[i];
        if (i4 < 0) {
            i3 = b(i4, bArr, i3, fr5Var);
            i4 = fr5Var.a;
        }
        int i5 = i3;
        if (i4 >= 0 && i4 <= i2 - i5) {
            Object zza = c2Var.zza();
            int i6 = i4 + i5;
            c2Var.i(zza, bArr, i5, i6, fr5Var);
            c2Var.f(zza);
            fr5Var.c = zza;
            return i6;
        }
        throw zzkn.zza();
    }

    public static int j(c2 c2Var, byte[] bArr, int i, int i2, int i3, fr5 fr5Var) throws IOException {
        a2 a2Var = (a2) c2Var;
        Object zza = a2Var.zza();
        int D = a2Var.D(zza, bArr, i, i2, i3, fr5Var);
        a2Var.f(zza);
        fr5Var.c = zza;
        return D;
    }

    public static int k(int i, byte[] bArr, int i2, int i3, zv5<?> zv5Var, fr5 fr5Var) {
        kv5 kv5Var = (kv5) zv5Var;
        int a = a(bArr, i2, fr5Var);
        kv5Var.m(fr5Var.a);
        while (a < i3) {
            int a2 = a(bArr, a, fr5Var);
            if (i != fr5Var.a) {
                break;
            }
            a = a(bArr, a2, fr5Var);
            kv5Var.m(fr5Var.a);
        }
        return a;
    }

    public static int l(byte[] bArr, int i, zv5<?> zv5Var, fr5 fr5Var) throws IOException {
        kv5 kv5Var = (kv5) zv5Var;
        int a = a(bArr, i, fr5Var);
        int i2 = fr5Var.a + a;
        while (a < i2) {
            a = a(bArr, a, fr5Var);
            kv5Var.m(fr5Var.a);
        }
        if (a == i2) {
            return a;
        }
        throw zzkn.zza();
    }

    public static int m(c2<?> c2Var, int i, byte[] bArr, int i2, int i3, zv5<?> zv5Var, fr5 fr5Var) throws IOException {
        int i4 = i(c2Var, bArr, i2, i3, fr5Var);
        zv5Var.add(fr5Var.c);
        while (i4 < i3) {
            int a = a(bArr, i4, fr5Var);
            if (i != fr5Var.a) {
                break;
            }
            i4 = i(c2Var, bArr, a, i3, fr5Var);
            zv5Var.add(fr5Var.c);
        }
        return i4;
    }

    public static int n(int i, byte[] bArr, int i2, int i3, f2 f2Var, fr5 fr5Var) throws zzkn {
        if ((i >>> 3) != 0) {
            int i4 = i & 7;
            if (i4 == 0) {
                int c = c(bArr, i2, fr5Var);
                f2Var.h(i, Long.valueOf(fr5Var.b));
                return c;
            } else if (i4 == 1) {
                f2Var.h(i, Long.valueOf(e(bArr, i2)));
                return i2 + 8;
            } else if (i4 == 2) {
                int a = a(bArr, i2, fr5Var);
                int i5 = fr5Var.a;
                if (i5 >= 0) {
                    if (i5 <= bArr.length - a) {
                        if (i5 == 0) {
                            f2Var.h(i, zzjd.zzb);
                        } else {
                            f2Var.h(i, zzjd.zzj(bArr, a, i5));
                        }
                        return a + i5;
                    }
                    throw zzkn.zza();
                }
                throw zzkn.zzb();
            } else if (i4 != 3) {
                if (i4 == 5) {
                    f2Var.h(i, Integer.valueOf(d(bArr, i2)));
                    return i2 + 4;
                }
                throw zzkn.zzc();
            } else {
                int i6 = (i & (-8)) | 4;
                f2 b = f2.b();
                int i7 = 0;
                while (true) {
                    if (i2 >= i3) {
                        break;
                    }
                    int a2 = a(bArr, i2, fr5Var);
                    int i8 = fr5Var.a;
                    if (i8 == i6) {
                        i7 = i8;
                        i2 = a2;
                        break;
                    }
                    i7 = i8;
                    i2 = n(i8, bArr, a2, i3, b, fr5Var);
                }
                if (i2 <= i3 && i7 == i6) {
                    f2Var.h(i, b);
                    return i2;
                }
                throw zzkn.zze();
            }
        }
        throw zzkn.zzc();
    }
}
