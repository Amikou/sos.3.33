package com.google.android.gms.internal.measurement;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* loaded from: classes.dex */
public enum zzga implements nv5 {
    RADS(1),
    PROVISIONING(2);
    
    private final int zzd;

    static {
        new Object() { // from class: jk5
        };
    }

    zzga(int i) {
        this.zzd = i;
    }

    public static zzga zza(int i) {
        if (i != 1) {
            if (i != 2) {
                return null;
            }
            return PROVISIONING;
        }
        return RADS;
    }

    public static sv5 zzb() {
        return mk5.a;
    }

    @Override // java.lang.Enum
    public final String toString() {
        return "<" + zzga.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.zzd + " name=" + name() + '>';
    }
}
