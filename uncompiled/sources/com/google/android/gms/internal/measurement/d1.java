package com.google.android.gms.internal.measurement;

import com.github.mikephil.charting.utils.Utils;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* loaded from: classes.dex */
public final class d1 extends x1<d1, yj5> implements xx5 {
    private static final d1 zzk;
    private int zza;
    private long zzg;
    private float zzh;
    private double zzi;
    private String zze = "";
    private String zzf = "";
    private zv5<d1> zzj = x1.o();

    static {
        d1 d1Var = new d1();
        zzk = d1Var;
        x1.u(d1.class, d1Var);
    }

    public static yj5 J() {
        return zzk.r();
    }

    public static /* synthetic */ void L(d1 d1Var, String str) {
        str.getClass();
        d1Var.zza |= 1;
        d1Var.zze = str;
    }

    public static /* synthetic */ void M(d1 d1Var, String str) {
        str.getClass();
        d1Var.zza |= 2;
        d1Var.zzf = str;
    }

    public static /* synthetic */ void N(d1 d1Var) {
        d1Var.zza &= -3;
        d1Var.zzf = zzk.zzf;
    }

    public static /* synthetic */ void O(d1 d1Var, long j) {
        d1Var.zza |= 4;
        d1Var.zzg = j;
    }

    public static /* synthetic */ void P(d1 d1Var) {
        d1Var.zza &= -5;
        d1Var.zzg = 0L;
    }

    public static /* synthetic */ void Q(d1 d1Var, double d) {
        d1Var.zza |= 16;
        d1Var.zzi = d;
    }

    public static /* synthetic */ void R(d1 d1Var) {
        d1Var.zza &= -17;
        d1Var.zzi = Utils.DOUBLE_EPSILON;
    }

    public static /* synthetic */ void S(d1 d1Var, d1 d1Var2) {
        d1Var2.getClass();
        d1Var.V();
        d1Var.zzj.add(d1Var2);
    }

    public static /* synthetic */ void T(d1 d1Var, Iterable iterable) {
        d1Var.V();
        zq5.j(iterable, d1Var.zzj);
    }

    public final String A() {
        return this.zzf;
    }

    public final boolean B() {
        return (this.zza & 4) != 0;
    }

    public final long C() {
        return this.zzg;
    }

    public final boolean D() {
        return (this.zza & 8) != 0;
    }

    public final float E() {
        return this.zzh;
    }

    public final boolean F() {
        return (this.zza & 16) != 0;
    }

    public final double G() {
        return this.zzi;
    }

    public final List<d1> H() {
        return this.zzj;
    }

    public final int I() {
        return this.zzj.size();
    }

    public final void V() {
        zv5<d1> zv5Var = this.zzj;
        if (zv5Var.zza()) {
            return;
        }
        this.zzj = x1.p(zv5Var);
    }

    @Override // com.google.android.gms.internal.measurement.x1
    public final Object w(int i, Object obj, Object obj2) {
        int i2 = i - 1;
        if (i2 != 0) {
            if (i2 != 2) {
                if (i2 != 3) {
                    if (i2 != 4) {
                        if (i2 != 5) {
                            return null;
                        }
                        return zzk;
                    }
                    return new yj5(null);
                }
                return new d1();
            }
            return x1.v(zzk, "\u0001\u0006\u0000\u0001\u0001\u0006\u0006\u0000\u0001\u0000\u0001ဈ\u0000\u0002ဈ\u0001\u0003ဂ\u0002\u0004ခ\u0003\u0005က\u0004\u0006\u001b", new Object[]{"zza", "zze", "zzf", "zzg", "zzh", "zzi", "zzj", d1.class});
        }
        return (byte) 1;
    }

    public final boolean x() {
        return (this.zza & 1) != 0;
    }

    public final String y() {
        return this.zze;
    }

    public final boolean z() {
        return (this.zza & 2) != 0;
    }
}
