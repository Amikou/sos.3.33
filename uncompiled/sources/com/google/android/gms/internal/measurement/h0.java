package com.google.android.gms.internal.measurement;

import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@19.0.0 */
/* loaded from: classes.dex */
public final class h0 extends i0 {
    public final /* synthetic */ String i0;
    public final /* synthetic */ String j0;
    public final /* synthetic */ Object k0;
    public final /* synthetic */ boolean l0;
    public final /* synthetic */ wf5 m0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public h0(wf5 wf5Var, String str, String str2, Object obj, boolean z) {
        super(wf5Var, true);
        this.m0 = wf5Var;
        this.i0 = str;
        this.j0 = str2;
        this.k0 = obj;
        this.l0 = z;
    }

    @Override // com.google.android.gms.internal.measurement.i0
    public final void a() throws RemoteException {
        j jVar;
        jVar = this.m0.h;
        ((j) zt2.j(jVar)).setUserProperty(this.i0, this.j0, nl2.H1(this.k0), this.l0, this.a);
    }
}
