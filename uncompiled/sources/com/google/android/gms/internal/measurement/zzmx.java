package com.google.android.gms.internal.measurement;

/* JADX WARN: Enum visitor error
jadx.core.utils.exceptions.JadxRuntimeException: Init of enum zzc uses external variables
	at jadx.core.dex.visitors.EnumVisitor.createEnumFieldByConstructor(EnumVisitor.java:444)
	at jadx.core.dex.visitors.EnumVisitor.processEnumFieldByRegister(EnumVisitor.java:391)
	at jadx.core.dex.visitors.EnumVisitor.extractEnumFieldsFromFilledArray(EnumVisitor.java:320)
	at jadx.core.dex.visitors.EnumVisitor.extractEnumFieldsFromInsn(EnumVisitor.java:258)
	at jadx.core.dex.visitors.EnumVisitor.convertToEnum(EnumVisitor.java:151)
	at jadx.core.dex.visitors.EnumVisitor.visit(EnumVisitor.java:100)
 */
/* JADX WARN: Failed to restore enum class, 'enum' modifier and super class removed */
/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* loaded from: classes.dex */
public final class zzmx {
    public static final /* synthetic */ zzmx[] a;
    public static final zzmx zza;
    public static final zzmx zzb;
    public static final zzmx zzc;
    public static final zzmx zzd;
    public static final zzmx zze;
    public static final zzmx zzf;
    public static final zzmx zzg;
    public static final zzmx zzh;
    public static final zzmx zzi;
    public static final zzmx zzj;
    public static final zzmx zzk;
    public static final zzmx zzl;
    public static final zzmx zzm;
    public static final zzmx zzn;
    public static final zzmx zzo;
    public static final zzmx zzp;
    public static final zzmx zzq;
    public static final zzmx zzr;
    private final zzmy zzs;

    static {
        zzmx zzmxVar = new zzmx("DOUBLE", 0, zzmy.DOUBLE, 1);
        zza = zzmxVar;
        zzmx zzmxVar2 = new zzmx("FLOAT", 1, zzmy.FLOAT, 5);
        zzb = zzmxVar2;
        zzmy zzmyVar = zzmy.LONG;
        zzmx zzmxVar3 = new zzmx("INT64", 2, zzmyVar, 0);
        zzc = zzmxVar3;
        zzmx zzmxVar4 = new zzmx("UINT64", 3, zzmyVar, 0);
        zzd = zzmxVar4;
        zzmy zzmyVar2 = zzmy.INT;
        zzmx zzmxVar5 = new zzmx("INT32", 4, zzmyVar2, 0);
        zze = zzmxVar5;
        zzmx zzmxVar6 = new zzmx("FIXED64", 5, zzmyVar, 1);
        zzf = zzmxVar6;
        zzmx zzmxVar7 = new zzmx("FIXED32", 6, zzmyVar2, 5);
        zzg = zzmxVar7;
        zzmx zzmxVar8 = new zzmx("BOOL", 7, zzmy.BOOLEAN, 0);
        zzh = zzmxVar8;
        zzmx zzmxVar9 = new zzmx("STRING", 8, zzmy.STRING, 2);
        zzi = zzmxVar9;
        zzmy zzmyVar3 = zzmy.MESSAGE;
        zzmx zzmxVar10 = new zzmx("GROUP", 9, zzmyVar3, 3);
        zzj = zzmxVar10;
        zzmx zzmxVar11 = new zzmx("MESSAGE", 10, zzmyVar3, 2);
        zzk = zzmxVar11;
        zzmx zzmxVar12 = new zzmx("BYTES", 11, zzmy.BYTE_STRING, 2);
        zzl = zzmxVar12;
        zzmx zzmxVar13 = new zzmx("UINT32", 12, zzmyVar2, 0);
        zzm = zzmxVar13;
        zzmx zzmxVar14 = new zzmx("ENUM", 13, zzmy.ENUM, 0);
        zzn = zzmxVar14;
        zzmx zzmxVar15 = new zzmx("SFIXED32", 14, zzmyVar2, 5);
        zzo = zzmxVar15;
        zzmx zzmxVar16 = new zzmx("SFIXED64", 15, zzmyVar, 1);
        zzp = zzmxVar16;
        zzmx zzmxVar17 = new zzmx("SINT32", 16, zzmyVar2, 0);
        zzq = zzmxVar17;
        zzmx zzmxVar18 = new zzmx("SINT64", 17, zzmyVar, 0);
        zzr = zzmxVar18;
        a = new zzmx[]{zzmxVar, zzmxVar2, zzmxVar3, zzmxVar4, zzmxVar5, zzmxVar6, zzmxVar7, zzmxVar8, zzmxVar9, zzmxVar10, zzmxVar11, zzmxVar12, zzmxVar13, zzmxVar14, zzmxVar15, zzmxVar16, zzmxVar17, zzmxVar18};
    }

    public zzmx(String str, int i, zzmy zzmyVar, int i2) {
        this.zzs = zzmyVar;
    }

    public static zzmx[] values() {
        return (zzmx[]) a.clone();
    }

    public final zzmy zza() {
        return this.zzs;
    }
}
