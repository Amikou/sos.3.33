package com.google.android.gms.internal.measurement;

import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* loaded from: classes.dex */
public final class r0 extends x1<r0, dg5> implements xx5 {
    private static final r0 zzm;
    private int zza;
    private int zze;
    private String zzf = "";
    private zv5<s0> zzg = x1.o();
    private boolean zzh;
    private t0 zzi;
    private boolean zzj;
    private boolean zzk;
    private boolean zzl;

    static {
        r0 r0Var = new r0();
        zzm = r0Var;
        x1.u(r0.class, r0Var);
    }

    public static dg5 J() {
        return zzm.r();
    }

    public static /* synthetic */ void L(r0 r0Var, String str) {
        r0Var.zza |= 2;
        r0Var.zzf = str;
    }

    public static /* synthetic */ void M(r0 r0Var, int i, s0 s0Var) {
        s0Var.getClass();
        zv5<s0> zv5Var = r0Var.zzg;
        if (!zv5Var.zza()) {
            r0Var.zzg = x1.p(zv5Var);
        }
        r0Var.zzg.set(i, s0Var);
    }

    public final List<s0> A() {
        return this.zzg;
    }

    public final int B() {
        return this.zzg.size();
    }

    public final s0 C(int i) {
        return this.zzg.get(i);
    }

    public final boolean D() {
        return (this.zza & 8) != 0;
    }

    public final t0 E() {
        t0 t0Var = this.zzi;
        return t0Var == null ? t0.H() : t0Var;
    }

    public final boolean F() {
        return this.zzj;
    }

    public final boolean G() {
        return this.zzk;
    }

    public final boolean H() {
        return (this.zza & 64) != 0;
    }

    public final boolean I() {
        return this.zzl;
    }

    @Override // com.google.android.gms.internal.measurement.x1
    public final Object w(int i, Object obj, Object obj2) {
        int i2 = i - 1;
        if (i2 != 0) {
            if (i2 != 2) {
                if (i2 != 3) {
                    if (i2 != 4) {
                        if (i2 != 5) {
                            return null;
                        }
                        return zzm;
                    }
                    return new dg5(null);
                }
                return new r0();
            }
            return x1.v(zzm, "\u0001\b\u0000\u0001\u0001\b\b\u0000\u0001\u0000\u0001င\u0000\u0002ဈ\u0001\u0003\u001b\u0004ဇ\u0002\u0005ဉ\u0003\u0006ဇ\u0004\u0007ဇ\u0005\bဇ\u0006", new Object[]{"zza", "zze", "zzf", "zzg", s0.class, "zzh", "zzi", "zzj", "zzk", "zzl"});
        }
        return (byte) 1;
    }

    public final boolean x() {
        return (this.zza & 1) != 0;
    }

    public final int y() {
        return this.zze;
    }

    public final String z() {
        return this.zzf;
    }
}
