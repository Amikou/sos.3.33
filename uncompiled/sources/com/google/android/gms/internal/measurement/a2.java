package com.google.android.gms.internal.measurement;

import com.github.mikephil.charting.utils.Utils;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import sun.misc.Unsafe;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* loaded from: classes.dex */
public final class a2<T> implements c2<T> {
    public static final int[] o = new int[0];
    public static final Unsafe p = l2.v();
    public final int[] a;
    public final Object[] b;
    public final int c;
    public final int d;
    public final z1 e;
    public final boolean f;
    public final boolean g;
    public final int[] h;
    public final int i;
    public final int j;
    public final ww5 k;
    public final e2<?, ?> l;
    public final tt5<?> m;
    public final ox5 n;

    /* JADX WARN: Multi-variable type inference failed */
    public a2(int[] iArr, int[] iArr2, Object[] objArr, int i, int i2, z1 z1Var, boolean z, boolean z2, int[] iArr3, int i3, int i4, dy5 dy5Var, ww5 ww5Var, e2<?, ?> e2Var, tt5<?> tt5Var, ox5 ox5Var) {
        this.a = iArr;
        this.b = iArr2;
        this.c = objArr;
        this.d = i;
        this.g = z1Var;
        boolean z3 = false;
        if (e2Var != 0 && e2Var.a(i2)) {
            z3 = true;
        }
        this.f = z3;
        this.h = z2;
        this.i = iArr3;
        this.j = i3;
        this.k = dy5Var;
        this.l = ww5Var;
        this.m = e2Var;
        this.e = i2;
        this.n = tt5Var;
    }

    public static final void B(int i, Object obj, u1 u1Var) throws IOException {
        if (obj instanceof String) {
            u1Var.x(i, (String) obj);
        } else {
            u1Var.y(i, (zzjd) obj);
        }
    }

    public static f2 C(Object obj) {
        x1 x1Var = (x1) obj;
        f2 f2Var = x1Var.zzc;
        if (f2Var == f2.a()) {
            f2 b = f2.b();
            x1Var.zzc = b;
            return b;
        }
        return f2Var;
    }

    public static <T> a2<T> E(Class<T> cls, rx5 rx5Var, dy5 dy5Var, ww5 ww5Var, e2<?, ?> e2Var, tt5<?> tt5Var, ox5 ox5Var) {
        if (rx5Var instanceof ny5) {
            return F((ny5) rx5Var, dy5Var, ww5Var, e2Var, tt5Var, ox5Var);
        }
        iz5 iz5Var = (iz5) rx5Var;
        throw null;
    }

    /* JADX WARN: Removed duplicated region for block: B:123:0x025e  */
    /* JADX WARN: Removed duplicated region for block: B:124:0x0261  */
    /* JADX WARN: Removed duplicated region for block: B:127:0x0279  */
    /* JADX WARN: Removed duplicated region for block: B:128:0x027c  */
    /* JADX WARN: Removed duplicated region for block: B:162:0x032c  */
    /* JADX WARN: Removed duplicated region for block: B:180:0x0385  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static <T> com.google.android.gms.internal.measurement.a2<T> F(defpackage.ny5 r34, defpackage.dy5 r35, defpackage.ww5 r36, com.google.android.gms.internal.measurement.e2<?, ?> r37, defpackage.tt5<?> r38, defpackage.ox5 r39) {
        /*
            Method dump skipped, instructions count: 1016
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.a2.F(ny5, dy5, ww5, com.google.android.gms.internal.measurement.e2, tt5, ox5):com.google.android.gms.internal.measurement.a2");
    }

    public static Field G(Class<?> cls, String str) {
        try {
            return cls.getDeclaredField(str);
        } catch (NoSuchFieldException unused) {
            Field[] declaredFields = cls.getDeclaredFields();
            for (Field field : declaredFields) {
                if (str.equals(field.getName())) {
                    return field;
                }
            }
            String name = cls.getName();
            String arrays = Arrays.toString(declaredFields);
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 40 + name.length() + String.valueOf(arrays).length());
            sb.append("Field ");
            sb.append(str);
            sb.append(" for ");
            sb.append(name);
            sb.append(" not found. Known fields are ");
            sb.append(arrays);
            throw new RuntimeException(sb.toString());
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static boolean S(Object obj, int i, c2 c2Var) {
        return c2Var.e(l2.s(obj, i & 1048575));
    }

    public static int k(int i) {
        return (i >>> 20) & 255;
    }

    public static <T> double l(T t, long j) {
        return ((Double) l2.s(t, j)).doubleValue();
    }

    public static <T> float m(T t, long j) {
        return ((Float) l2.s(t, j)).floatValue();
    }

    public static <T> int n(T t, long j) {
        return ((Integer) l2.s(t, j)).intValue();
    }

    public static <T> long o(T t, long j) {
        return ((Long) l2.s(t, j)).longValue();
    }

    public static <T> boolean p(T t, long j) {
        return ((Boolean) l2.s(t, j)).booleanValue();
    }

    public final <K, V> void A(u1 u1Var, int i, Object obj, int i2) throws IOException {
        if (obj == null) {
            return;
        }
        nx5 nx5Var = (nx5) P(i2);
        throw null;
    }

    /* JADX WARN: Code restructure failed: missing block: B:144:0x0419, code lost:
        if (r6 == 1048575) goto L35;
     */
    /* JADX WARN: Code restructure failed: missing block: B:145:0x041b, code lost:
        r26.putInt(r12, r6, r5);
     */
    /* JADX WARN: Code restructure failed: missing block: B:146:0x0421, code lost:
        r3 = r9.i;
     */
    /* JADX WARN: Code restructure failed: missing block: B:148:0x0425, code lost:
        if (r3 >= r9.j) goto L49;
     */
    /* JADX WARN: Code restructure failed: missing block: B:149:0x0427, code lost:
        r4 = r9.h[r3];
        r5 = r9.a[r4];
        r5 = com.google.android.gms.internal.measurement.l2.s(r12, r9.b(r4) & 1048575);
     */
    /* JADX WARN: Code restructure failed: missing block: B:150:0x0439, code lost:
        if (r5 != null) goto L40;
     */
    /* JADX WARN: Code restructure failed: missing block: B:153:0x0440, code lost:
        if (r9.Q(r4) != null) goto L42;
     */
    /* JADX WARN: Code restructure failed: missing block: B:154:0x0442, code lost:
        r3 = r3 + 1;
     */
    /* JADX WARN: Code restructure failed: missing block: B:155:0x0445, code lost:
        r5 = (com.google.android.gms.internal.measurement.zzlc) r5;
        r0 = (defpackage.nx5) r9.P(r4);
     */
    /* JADX WARN: Code restructure failed: missing block: B:156:0x044d, code lost:
        throw null;
     */
    /* JADX WARN: Code restructure failed: missing block: B:157:0x044e, code lost:
        if (r7 != 0) goto L56;
     */
    /* JADX WARN: Code restructure failed: missing block: B:159:0x0452, code lost:
        if (r0 != r32) goto L53;
     */
    /* JADX WARN: Code restructure failed: missing block: B:162:0x0459, code lost:
        throw com.google.android.gms.internal.measurement.zzkn.zze();
     */
    /* JADX WARN: Code restructure failed: missing block: B:164:0x045c, code lost:
        if (r0 > r32) goto L59;
     */
    /* JADX WARN: Code restructure failed: missing block: B:165:0x045e, code lost:
        if (r1 != r7) goto L59;
     */
    /* JADX WARN: Code restructure failed: missing block: B:166:0x0460, code lost:
        return r0;
     */
    /* JADX WARN: Code restructure failed: missing block: B:168:0x0465, code lost:
        throw com.google.android.gms.internal.measurement.zzkn.zze();
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final int D(T r29, byte[] r30, int r31, int r32, int r33, defpackage.fr5 r34) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 1164
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.a2.D(java.lang.Object, byte[], int, int, int, fr5):int");
    }

    public final void H(T t, T t2, int i) {
        long b = b(i) & 1048575;
        if (s(t2, i)) {
            Object s = l2.s(t, b);
            Object s2 = l2.s(t2, b);
            if (s != null && s2 != null) {
                l2.t(t, b, cw5.i(s, s2));
                t(t, i);
            } else if (s2 != null) {
                l2.t(t, b, s2);
                t(t, i);
            }
        }
    }

    public final void I(T t, T t2, int i) {
        int b = b(i);
        int i2 = this.a[i];
        long j = b & 1048575;
        if (u(t2, i2, i)) {
            Object s = u(t, i2, i) ? l2.s(t, j) : null;
            Object s2 = l2.s(t2, j);
            if (s != null && s2 != null) {
                l2.t(t, j, cw5.i(s, s2));
                v(t, i2, i);
            } else if (s2 != null) {
                l2.t(t, j, s2);
                v(t, i2, i);
            }
        }
    }

    public final int J(T t) {
        int i;
        int A;
        int A2;
        int A3;
        int B;
        int A4;
        int z;
        int A5;
        int A6;
        int zzc;
        int A7;
        int Y;
        int y;
        int A8;
        int i2;
        Unsafe unsafe = p;
        int i3 = 1048575;
        int i4 = 0;
        int i5 = 0;
        for (int i6 = 0; i6 < this.a.length; i6 += 3) {
            int b = b(i6);
            int i7 = this.a[i6];
            int k = k(b);
            if (k <= 17) {
                int i8 = this.a[i6 + 2];
                int i9 = i8 & 1048575;
                i = 1 << (i8 >>> 20);
                if (i9 != i3) {
                    i5 = unsafe.getInt(t, i9);
                    i3 = i9;
                }
            } else {
                i = 0;
            }
            long j = b & 1048575;
            switch (k) {
                case 0:
                    if ((i5 & i) != 0) {
                        A = t1.A(i7 << 3);
                        Y = A + 8;
                        break;
                    } else {
                        continue;
                    }
                case 1:
                    if ((i5 & i) != 0) {
                        A2 = t1.A(i7 << 3);
                        Y = A2 + 4;
                        break;
                    } else {
                        continue;
                    }
                case 2:
                    if ((i5 & i) != 0) {
                        long j2 = unsafe.getLong(t, j);
                        A3 = t1.A(i7 << 3);
                        B = t1.B(j2);
                        Y = A3 + B;
                        break;
                    } else {
                        continue;
                    }
                case 3:
                    if ((i5 & i) != 0) {
                        long j3 = unsafe.getLong(t, j);
                        A3 = t1.A(i7 << 3);
                        B = t1.B(j3);
                        Y = A3 + B;
                        break;
                    } else {
                        continue;
                    }
                case 4:
                    if ((i5 & i) != 0) {
                        int i10 = unsafe.getInt(t, j);
                        A4 = t1.A(i7 << 3);
                        z = t1.z(i10);
                        i2 = A4 + z;
                        i4 += i2;
                    } else {
                        continue;
                    }
                case 5:
                    if ((i5 & i) != 0) {
                        A = t1.A(i7 << 3);
                        Y = A + 8;
                        break;
                    } else {
                        continue;
                    }
                case 6:
                    if ((i5 & i) != 0) {
                        A2 = t1.A(i7 << 3);
                        Y = A2 + 4;
                        break;
                    } else {
                        continue;
                    }
                case 7:
                    if ((i5 & i) != 0) {
                        A5 = t1.A(i7 << 3);
                        Y = A5 + 1;
                        break;
                    } else {
                        continue;
                    }
                case 8:
                    if ((i5 & i) != 0) {
                        Object object = unsafe.getObject(t, j);
                        if (object instanceof zzjd) {
                            A6 = t1.A(i7 << 3);
                            zzc = ((zzjd) object).zzc();
                            A7 = t1.A(zzc);
                            i2 = A6 + A7 + zzc;
                            i4 += i2;
                        } else {
                            A4 = t1.A(i7 << 3);
                            z = t1.C((String) object);
                            i2 = A4 + z;
                            i4 += i2;
                        }
                    } else {
                        continue;
                    }
                case 9:
                    if ((i5 & i) != 0) {
                        Y = d2.Y(i7, unsafe.getObject(t, j), O(i6));
                        break;
                    } else {
                        continue;
                    }
                case 10:
                    if ((i5 & i) != 0) {
                        A6 = t1.A(i7 << 3);
                        zzc = ((zzjd) unsafe.getObject(t, j)).zzc();
                        A7 = t1.A(zzc);
                        i2 = A6 + A7 + zzc;
                        i4 += i2;
                    } else {
                        continue;
                    }
                case 11:
                    if ((i5 & i) != 0) {
                        int i11 = unsafe.getInt(t, j);
                        A4 = t1.A(i7 << 3);
                        z = t1.A(i11);
                        i2 = A4 + z;
                        i4 += i2;
                    } else {
                        continue;
                    }
                case 12:
                    if ((i5 & i) != 0) {
                        int i12 = unsafe.getInt(t, j);
                        A4 = t1.A(i7 << 3);
                        z = t1.z(i12);
                        i2 = A4 + z;
                        i4 += i2;
                    } else {
                        continue;
                    }
                case 13:
                    if ((i5 & i) != 0) {
                        A2 = t1.A(i7 << 3);
                        Y = A2 + 4;
                        break;
                    } else {
                        continue;
                    }
                case 14:
                    if ((i5 & i) != 0) {
                        A = t1.A(i7 << 3);
                        Y = A + 8;
                        break;
                    } else {
                        continue;
                    }
                case 15:
                    if ((i5 & i) != 0) {
                        int i13 = unsafe.getInt(t, j);
                        A4 = t1.A(i7 << 3);
                        z = t1.A((i13 >> 31) ^ (i13 + i13));
                        i2 = A4 + z;
                        i4 += i2;
                    } else {
                        continue;
                    }
                case 16:
                    if ((i5 & i) != 0) {
                        long j4 = unsafe.getLong(t, j);
                        A4 = t1.A(i7 << 3);
                        z = t1.B((j4 >> 63) ^ (j4 + j4));
                        i2 = A4 + z;
                        i4 += i2;
                    } else {
                        continue;
                    }
                case 17:
                    if ((i5 & i) != 0) {
                        Y = t1.e(i7, (z1) unsafe.getObject(t, j), O(i6));
                        break;
                    } else {
                        continue;
                    }
                case 18:
                    Y = d2.U(i7, (List) unsafe.getObject(t, j), false);
                    break;
                case 19:
                    Y = d2.S(i7, (List) unsafe.getObject(t, j), false);
                    break;
                case 20:
                    Y = d2.E(i7, (List) unsafe.getObject(t, j), false);
                    break;
                case 21:
                    Y = d2.G(i7, (List) unsafe.getObject(t, j), false);
                    break;
                case 22:
                    Y = d2.M(i7, (List) unsafe.getObject(t, j), false);
                    break;
                case 23:
                    Y = d2.U(i7, (List) unsafe.getObject(t, j), false);
                    break;
                case 24:
                    Y = d2.S(i7, (List) unsafe.getObject(t, j), false);
                    break;
                case 25:
                    Y = d2.W(i7, (List) unsafe.getObject(t, j), false);
                    break;
                case 26:
                    Y = d2.X(i7, (List) unsafe.getObject(t, j));
                    break;
                case 27:
                    Y = d2.Z(i7, (List) unsafe.getObject(t, j), O(i6));
                    break;
                case 28:
                    Y = d2.a0(i7, (List) unsafe.getObject(t, j));
                    break;
                case 29:
                    Y = d2.O(i7, (List) unsafe.getObject(t, j), false);
                    break;
                case 30:
                    Y = d2.K(i7, (List) unsafe.getObject(t, j), false);
                    break;
                case 31:
                    Y = d2.S(i7, (List) unsafe.getObject(t, j), false);
                    break;
                case 32:
                    Y = d2.U(i7, (List) unsafe.getObject(t, j), false);
                    break;
                case 33:
                    Y = d2.Q(i7, (List) unsafe.getObject(t, j), false);
                    break;
                case 34:
                    Y = d2.I(i7, (List) unsafe.getObject(t, j), false);
                    break;
                case 35:
                    z = d2.T((List) unsafe.getObject(t, j));
                    if (z > 0) {
                        y = t1.y(i7);
                        A8 = t1.A(z);
                        A4 = y + A8;
                        i2 = A4 + z;
                        i4 += i2;
                    } else {
                        continue;
                    }
                case 36:
                    z = d2.R((List) unsafe.getObject(t, j));
                    if (z > 0) {
                        y = t1.y(i7);
                        A8 = t1.A(z);
                        A4 = y + A8;
                        i2 = A4 + z;
                        i4 += i2;
                    } else {
                        continue;
                    }
                case 37:
                    z = d2.D((List) unsafe.getObject(t, j));
                    if (z > 0) {
                        y = t1.y(i7);
                        A8 = t1.A(z);
                        A4 = y + A8;
                        i2 = A4 + z;
                        i4 += i2;
                    } else {
                        continue;
                    }
                case 38:
                    z = d2.F((List) unsafe.getObject(t, j));
                    if (z > 0) {
                        y = t1.y(i7);
                        A8 = t1.A(z);
                        A4 = y + A8;
                        i2 = A4 + z;
                        i4 += i2;
                    } else {
                        continue;
                    }
                case 39:
                    z = d2.L((List) unsafe.getObject(t, j));
                    if (z > 0) {
                        y = t1.y(i7);
                        A8 = t1.A(z);
                        A4 = y + A8;
                        i2 = A4 + z;
                        i4 += i2;
                    } else {
                        continue;
                    }
                case 40:
                    z = d2.T((List) unsafe.getObject(t, j));
                    if (z > 0) {
                        y = t1.y(i7);
                        A8 = t1.A(z);
                        A4 = y + A8;
                        i2 = A4 + z;
                        i4 += i2;
                    } else {
                        continue;
                    }
                case 41:
                    z = d2.R((List) unsafe.getObject(t, j));
                    if (z > 0) {
                        y = t1.y(i7);
                        A8 = t1.A(z);
                        A4 = y + A8;
                        i2 = A4 + z;
                        i4 += i2;
                    } else {
                        continue;
                    }
                case 42:
                    z = d2.V((List) unsafe.getObject(t, j));
                    if (z > 0) {
                        y = t1.y(i7);
                        A8 = t1.A(z);
                        A4 = y + A8;
                        i2 = A4 + z;
                        i4 += i2;
                    } else {
                        continue;
                    }
                case 43:
                    z = d2.N((List) unsafe.getObject(t, j));
                    if (z > 0) {
                        y = t1.y(i7);
                        A8 = t1.A(z);
                        A4 = y + A8;
                        i2 = A4 + z;
                        i4 += i2;
                    } else {
                        continue;
                    }
                case 44:
                    z = d2.J((List) unsafe.getObject(t, j));
                    if (z > 0) {
                        y = t1.y(i7);
                        A8 = t1.A(z);
                        A4 = y + A8;
                        i2 = A4 + z;
                        i4 += i2;
                    } else {
                        continue;
                    }
                case 45:
                    z = d2.R((List) unsafe.getObject(t, j));
                    if (z > 0) {
                        y = t1.y(i7);
                        A8 = t1.A(z);
                        A4 = y + A8;
                        i2 = A4 + z;
                        i4 += i2;
                    } else {
                        continue;
                    }
                case 46:
                    z = d2.T((List) unsafe.getObject(t, j));
                    if (z > 0) {
                        y = t1.y(i7);
                        A8 = t1.A(z);
                        A4 = y + A8;
                        i2 = A4 + z;
                        i4 += i2;
                    } else {
                        continue;
                    }
                case 47:
                    z = d2.P((List) unsafe.getObject(t, j));
                    if (z > 0) {
                        y = t1.y(i7);
                        A8 = t1.A(z);
                        A4 = y + A8;
                        i2 = A4 + z;
                        i4 += i2;
                    } else {
                        continue;
                    }
                case 48:
                    z = d2.H((List) unsafe.getObject(t, j));
                    if (z > 0) {
                        y = t1.y(i7);
                        A8 = t1.A(z);
                        A4 = y + A8;
                        i2 = A4 + z;
                        i4 += i2;
                    } else {
                        continue;
                    }
                case 49:
                    Y = d2.b0(i7, (List) unsafe.getObject(t, j), O(i6));
                    break;
                case 50:
                    ox5.a(i7, unsafe.getObject(t, j), P(i6));
                    continue;
                case 51:
                    if (u(t, i7, i6)) {
                        A = t1.A(i7 << 3);
                        Y = A + 8;
                        break;
                    } else {
                        continue;
                    }
                case 52:
                    if (u(t, i7, i6)) {
                        A2 = t1.A(i7 << 3);
                        Y = A2 + 4;
                        break;
                    } else {
                        continue;
                    }
                case 53:
                    if (u(t, i7, i6)) {
                        long o2 = o(t, j);
                        A3 = t1.A(i7 << 3);
                        B = t1.B(o2);
                        Y = A3 + B;
                        break;
                    } else {
                        continue;
                    }
                case 54:
                    if (u(t, i7, i6)) {
                        long o3 = o(t, j);
                        A3 = t1.A(i7 << 3);
                        B = t1.B(o3);
                        Y = A3 + B;
                        break;
                    } else {
                        continue;
                    }
                case 55:
                    if (u(t, i7, i6)) {
                        int n = n(t, j);
                        A4 = t1.A(i7 << 3);
                        z = t1.z(n);
                        i2 = A4 + z;
                        i4 += i2;
                    } else {
                        continue;
                    }
                case 56:
                    if (u(t, i7, i6)) {
                        A = t1.A(i7 << 3);
                        Y = A + 8;
                        break;
                    } else {
                        continue;
                    }
                case 57:
                    if (u(t, i7, i6)) {
                        A2 = t1.A(i7 << 3);
                        Y = A2 + 4;
                        break;
                    } else {
                        continue;
                    }
                case 58:
                    if (u(t, i7, i6)) {
                        A5 = t1.A(i7 << 3);
                        Y = A5 + 1;
                        break;
                    } else {
                        continue;
                    }
                case 59:
                    if (u(t, i7, i6)) {
                        Object object2 = unsafe.getObject(t, j);
                        if (object2 instanceof zzjd) {
                            A6 = t1.A(i7 << 3);
                            zzc = ((zzjd) object2).zzc();
                            A7 = t1.A(zzc);
                            i2 = A6 + A7 + zzc;
                            i4 += i2;
                        } else {
                            A4 = t1.A(i7 << 3);
                            z = t1.C((String) object2);
                            i2 = A4 + z;
                            i4 += i2;
                        }
                    } else {
                        continue;
                    }
                case 60:
                    if (u(t, i7, i6)) {
                        Y = d2.Y(i7, unsafe.getObject(t, j), O(i6));
                        break;
                    } else {
                        continue;
                    }
                case 61:
                    if (u(t, i7, i6)) {
                        A6 = t1.A(i7 << 3);
                        zzc = ((zzjd) unsafe.getObject(t, j)).zzc();
                        A7 = t1.A(zzc);
                        i2 = A6 + A7 + zzc;
                        i4 += i2;
                    } else {
                        continue;
                    }
                case 62:
                    if (u(t, i7, i6)) {
                        int n2 = n(t, j);
                        A4 = t1.A(i7 << 3);
                        z = t1.A(n2);
                        i2 = A4 + z;
                        i4 += i2;
                    } else {
                        continue;
                    }
                case 63:
                    if (u(t, i7, i6)) {
                        int n3 = n(t, j);
                        A4 = t1.A(i7 << 3);
                        z = t1.z(n3);
                        i2 = A4 + z;
                        i4 += i2;
                    } else {
                        continue;
                    }
                case 64:
                    if (u(t, i7, i6)) {
                        A2 = t1.A(i7 << 3);
                        Y = A2 + 4;
                        break;
                    } else {
                        continue;
                    }
                case 65:
                    if (u(t, i7, i6)) {
                        A = t1.A(i7 << 3);
                        Y = A + 8;
                        break;
                    } else {
                        continue;
                    }
                case 66:
                    if (u(t, i7, i6)) {
                        int n4 = n(t, j);
                        A4 = t1.A(i7 << 3);
                        z = t1.A((n4 >> 31) ^ (n4 + n4));
                        i2 = A4 + z;
                        i4 += i2;
                    } else {
                        continue;
                    }
                case 67:
                    if (u(t, i7, i6)) {
                        long o4 = o(t, j);
                        A4 = t1.A(i7 << 3);
                        z = t1.B((o4 >> 63) ^ (o4 + o4));
                        i2 = A4 + z;
                        i4 += i2;
                    } else {
                        continue;
                    }
                case 68:
                    if (u(t, i7, i6)) {
                        Y = t1.e(i7, (z1) unsafe.getObject(t, j), O(i6));
                        break;
                    } else {
                        continue;
                    }
                default:
            }
            i4 += Y;
        }
        e2<?, ?> e2Var = this.l;
        int h = i4 + e2Var.h(e2Var.d(t));
        if (this.f) {
            this.m.b(t);
            throw null;
        }
        return h;
    }

    public final int K(T t) {
        int A;
        int A2;
        int A3;
        int B;
        int A4;
        int z;
        int A5;
        int A6;
        int zzc;
        int A7;
        int Y;
        int y;
        int A8;
        int i;
        Unsafe unsafe = p;
        int i2 = 0;
        for (int i3 = 0; i3 < this.a.length; i3 += 3) {
            int b = b(i3);
            int k = k(b);
            int i4 = this.a[i3];
            long j = b & 1048575;
            if (k >= zzjv.zzJ.zza() && k <= zzjv.zzW.zza()) {
                int i5 = this.a[i3 + 2];
            }
            switch (k) {
                case 0:
                    if (s(t, i3)) {
                        A = t1.A(i4 << 3);
                        Y = A + 8;
                        i2 += Y;
                        break;
                    } else {
                        break;
                    }
                case 1:
                    if (s(t, i3)) {
                        A2 = t1.A(i4 << 3);
                        Y = A2 + 4;
                        i2 += Y;
                        break;
                    } else {
                        break;
                    }
                case 2:
                    if (s(t, i3)) {
                        long k2 = l2.k(t, j);
                        A3 = t1.A(i4 << 3);
                        B = t1.B(k2);
                        i2 += A3 + B;
                        break;
                    } else {
                        break;
                    }
                case 3:
                    if (s(t, i3)) {
                        long k3 = l2.k(t, j);
                        A3 = t1.A(i4 << 3);
                        B = t1.B(k3);
                        i2 += A3 + B;
                        break;
                    } else {
                        break;
                    }
                case 4:
                    if (s(t, i3)) {
                        int i6 = l2.i(t, j);
                        A4 = t1.A(i4 << 3);
                        z = t1.z(i6);
                        i = A4 + z;
                        i2 += i;
                        break;
                    } else {
                        break;
                    }
                case 5:
                    if (s(t, i3)) {
                        A = t1.A(i4 << 3);
                        Y = A + 8;
                        i2 += Y;
                        break;
                    } else {
                        break;
                    }
                case 6:
                    if (s(t, i3)) {
                        A2 = t1.A(i4 << 3);
                        Y = A2 + 4;
                        i2 += Y;
                        break;
                    } else {
                        break;
                    }
                case 7:
                    if (s(t, i3)) {
                        A5 = t1.A(i4 << 3);
                        Y = A5 + 1;
                        i2 += Y;
                        break;
                    } else {
                        break;
                    }
                case 8:
                    if (!s(t, i3)) {
                        break;
                    } else {
                        Object s = l2.s(t, j);
                        if (s instanceof zzjd) {
                            A6 = t1.A(i4 << 3);
                            zzc = ((zzjd) s).zzc();
                            A7 = t1.A(zzc);
                            i = A6 + A7 + zzc;
                            i2 += i;
                            break;
                        } else {
                            A4 = t1.A(i4 << 3);
                            z = t1.C((String) s);
                            i = A4 + z;
                            i2 += i;
                        }
                    }
                case 9:
                    if (s(t, i3)) {
                        Y = d2.Y(i4, l2.s(t, j), O(i3));
                        i2 += Y;
                        break;
                    } else {
                        break;
                    }
                case 10:
                    if (s(t, i3)) {
                        A6 = t1.A(i4 << 3);
                        zzc = ((zzjd) l2.s(t, j)).zzc();
                        A7 = t1.A(zzc);
                        i = A6 + A7 + zzc;
                        i2 += i;
                        break;
                    } else {
                        break;
                    }
                case 11:
                    if (s(t, i3)) {
                        int i7 = l2.i(t, j);
                        A4 = t1.A(i4 << 3);
                        z = t1.A(i7);
                        i = A4 + z;
                        i2 += i;
                        break;
                    } else {
                        break;
                    }
                case 12:
                    if (s(t, i3)) {
                        int i8 = l2.i(t, j);
                        A4 = t1.A(i4 << 3);
                        z = t1.z(i8);
                        i = A4 + z;
                        i2 += i;
                        break;
                    } else {
                        break;
                    }
                case 13:
                    if (s(t, i3)) {
                        A2 = t1.A(i4 << 3);
                        Y = A2 + 4;
                        i2 += Y;
                        break;
                    } else {
                        break;
                    }
                case 14:
                    if (s(t, i3)) {
                        A = t1.A(i4 << 3);
                        Y = A + 8;
                        i2 += Y;
                        break;
                    } else {
                        break;
                    }
                case 15:
                    if (s(t, i3)) {
                        int i9 = l2.i(t, j);
                        A4 = t1.A(i4 << 3);
                        z = t1.A((i9 >> 31) ^ (i9 + i9));
                        i = A4 + z;
                        i2 += i;
                        break;
                    } else {
                        break;
                    }
                case 16:
                    if (s(t, i3)) {
                        long k4 = l2.k(t, j);
                        A4 = t1.A(i4 << 3);
                        z = t1.B((k4 >> 63) ^ (k4 + k4));
                        i = A4 + z;
                        i2 += i;
                        break;
                    } else {
                        break;
                    }
                case 17:
                    if (s(t, i3)) {
                        Y = t1.e(i4, (z1) l2.s(t, j), O(i3));
                        i2 += Y;
                        break;
                    } else {
                        break;
                    }
                case 18:
                    Y = d2.U(i4, (List) l2.s(t, j), false);
                    i2 += Y;
                    break;
                case 19:
                    Y = d2.S(i4, (List) l2.s(t, j), false);
                    i2 += Y;
                    break;
                case 20:
                    Y = d2.E(i4, (List) l2.s(t, j), false);
                    i2 += Y;
                    break;
                case 21:
                    Y = d2.G(i4, (List) l2.s(t, j), false);
                    i2 += Y;
                    break;
                case 22:
                    Y = d2.M(i4, (List) l2.s(t, j), false);
                    i2 += Y;
                    break;
                case 23:
                    Y = d2.U(i4, (List) l2.s(t, j), false);
                    i2 += Y;
                    break;
                case 24:
                    Y = d2.S(i4, (List) l2.s(t, j), false);
                    i2 += Y;
                    break;
                case 25:
                    Y = d2.W(i4, (List) l2.s(t, j), false);
                    i2 += Y;
                    break;
                case 26:
                    Y = d2.X(i4, (List) l2.s(t, j));
                    i2 += Y;
                    break;
                case 27:
                    Y = d2.Z(i4, (List) l2.s(t, j), O(i3));
                    i2 += Y;
                    break;
                case 28:
                    Y = d2.a0(i4, (List) l2.s(t, j));
                    i2 += Y;
                    break;
                case 29:
                    Y = d2.O(i4, (List) l2.s(t, j), false);
                    i2 += Y;
                    break;
                case 30:
                    Y = d2.K(i4, (List) l2.s(t, j), false);
                    i2 += Y;
                    break;
                case 31:
                    Y = d2.S(i4, (List) l2.s(t, j), false);
                    i2 += Y;
                    break;
                case 32:
                    Y = d2.U(i4, (List) l2.s(t, j), false);
                    i2 += Y;
                    break;
                case 33:
                    Y = d2.Q(i4, (List) l2.s(t, j), false);
                    i2 += Y;
                    break;
                case 34:
                    Y = d2.I(i4, (List) l2.s(t, j), false);
                    i2 += Y;
                    break;
                case 35:
                    z = d2.T((List) unsafe.getObject(t, j));
                    if (z > 0) {
                        y = t1.y(i4);
                        A8 = t1.A(z);
                        A4 = y + A8;
                        i = A4 + z;
                        i2 += i;
                        break;
                    } else {
                        break;
                    }
                case 36:
                    z = d2.R((List) unsafe.getObject(t, j));
                    if (z > 0) {
                        y = t1.y(i4);
                        A8 = t1.A(z);
                        A4 = y + A8;
                        i = A4 + z;
                        i2 += i;
                        break;
                    } else {
                        break;
                    }
                case 37:
                    z = d2.D((List) unsafe.getObject(t, j));
                    if (z > 0) {
                        y = t1.y(i4);
                        A8 = t1.A(z);
                        A4 = y + A8;
                        i = A4 + z;
                        i2 += i;
                        break;
                    } else {
                        break;
                    }
                case 38:
                    z = d2.F((List) unsafe.getObject(t, j));
                    if (z > 0) {
                        y = t1.y(i4);
                        A8 = t1.A(z);
                        A4 = y + A8;
                        i = A4 + z;
                        i2 += i;
                        break;
                    } else {
                        break;
                    }
                case 39:
                    z = d2.L((List) unsafe.getObject(t, j));
                    if (z > 0) {
                        y = t1.y(i4);
                        A8 = t1.A(z);
                        A4 = y + A8;
                        i = A4 + z;
                        i2 += i;
                        break;
                    } else {
                        break;
                    }
                case 40:
                    z = d2.T((List) unsafe.getObject(t, j));
                    if (z > 0) {
                        y = t1.y(i4);
                        A8 = t1.A(z);
                        A4 = y + A8;
                        i = A4 + z;
                        i2 += i;
                        break;
                    } else {
                        break;
                    }
                case 41:
                    z = d2.R((List) unsafe.getObject(t, j));
                    if (z > 0) {
                        y = t1.y(i4);
                        A8 = t1.A(z);
                        A4 = y + A8;
                        i = A4 + z;
                        i2 += i;
                        break;
                    } else {
                        break;
                    }
                case 42:
                    z = d2.V((List) unsafe.getObject(t, j));
                    if (z > 0) {
                        y = t1.y(i4);
                        A8 = t1.A(z);
                        A4 = y + A8;
                        i = A4 + z;
                        i2 += i;
                        break;
                    } else {
                        break;
                    }
                case 43:
                    z = d2.N((List) unsafe.getObject(t, j));
                    if (z > 0) {
                        y = t1.y(i4);
                        A8 = t1.A(z);
                        A4 = y + A8;
                        i = A4 + z;
                        i2 += i;
                        break;
                    } else {
                        break;
                    }
                case 44:
                    z = d2.J((List) unsafe.getObject(t, j));
                    if (z > 0) {
                        y = t1.y(i4);
                        A8 = t1.A(z);
                        A4 = y + A8;
                        i = A4 + z;
                        i2 += i;
                        break;
                    } else {
                        break;
                    }
                case 45:
                    z = d2.R((List) unsafe.getObject(t, j));
                    if (z > 0) {
                        y = t1.y(i4);
                        A8 = t1.A(z);
                        A4 = y + A8;
                        i = A4 + z;
                        i2 += i;
                        break;
                    } else {
                        break;
                    }
                case 46:
                    z = d2.T((List) unsafe.getObject(t, j));
                    if (z > 0) {
                        y = t1.y(i4);
                        A8 = t1.A(z);
                        A4 = y + A8;
                        i = A4 + z;
                        i2 += i;
                        break;
                    } else {
                        break;
                    }
                case 47:
                    z = d2.P((List) unsafe.getObject(t, j));
                    if (z > 0) {
                        y = t1.y(i4);
                        A8 = t1.A(z);
                        A4 = y + A8;
                        i = A4 + z;
                        i2 += i;
                        break;
                    } else {
                        break;
                    }
                case 48:
                    z = d2.H((List) unsafe.getObject(t, j));
                    if (z > 0) {
                        y = t1.y(i4);
                        A8 = t1.A(z);
                        A4 = y + A8;
                        i = A4 + z;
                        i2 += i;
                        break;
                    } else {
                        break;
                    }
                case 49:
                    Y = d2.b0(i4, (List) l2.s(t, j), O(i3));
                    i2 += Y;
                    break;
                case 50:
                    ox5.a(i4, l2.s(t, j), P(i3));
                    break;
                case 51:
                    if (u(t, i4, i3)) {
                        A = t1.A(i4 << 3);
                        Y = A + 8;
                        i2 += Y;
                        break;
                    } else {
                        break;
                    }
                case 52:
                    if (u(t, i4, i3)) {
                        A2 = t1.A(i4 << 3);
                        Y = A2 + 4;
                        i2 += Y;
                        break;
                    } else {
                        break;
                    }
                case 53:
                    if (u(t, i4, i3)) {
                        long o2 = o(t, j);
                        A3 = t1.A(i4 << 3);
                        B = t1.B(o2);
                        i2 += A3 + B;
                        break;
                    } else {
                        break;
                    }
                case 54:
                    if (u(t, i4, i3)) {
                        long o3 = o(t, j);
                        A3 = t1.A(i4 << 3);
                        B = t1.B(o3);
                        i2 += A3 + B;
                        break;
                    } else {
                        break;
                    }
                case 55:
                    if (u(t, i4, i3)) {
                        int n = n(t, j);
                        A4 = t1.A(i4 << 3);
                        z = t1.z(n);
                        i = A4 + z;
                        i2 += i;
                        break;
                    } else {
                        break;
                    }
                case 56:
                    if (u(t, i4, i3)) {
                        A = t1.A(i4 << 3);
                        Y = A + 8;
                        i2 += Y;
                        break;
                    } else {
                        break;
                    }
                case 57:
                    if (u(t, i4, i3)) {
                        A2 = t1.A(i4 << 3);
                        Y = A2 + 4;
                        i2 += Y;
                        break;
                    } else {
                        break;
                    }
                case 58:
                    if (u(t, i4, i3)) {
                        A5 = t1.A(i4 << 3);
                        Y = A5 + 1;
                        i2 += Y;
                        break;
                    } else {
                        break;
                    }
                case 59:
                    if (!u(t, i4, i3)) {
                        break;
                    } else {
                        Object s2 = l2.s(t, j);
                        if (s2 instanceof zzjd) {
                            A6 = t1.A(i4 << 3);
                            zzc = ((zzjd) s2).zzc();
                            A7 = t1.A(zzc);
                            i = A6 + A7 + zzc;
                            i2 += i;
                            break;
                        } else {
                            A4 = t1.A(i4 << 3);
                            z = t1.C((String) s2);
                            i = A4 + z;
                            i2 += i;
                        }
                    }
                case 60:
                    if (u(t, i4, i3)) {
                        Y = d2.Y(i4, l2.s(t, j), O(i3));
                        i2 += Y;
                        break;
                    } else {
                        break;
                    }
                case 61:
                    if (u(t, i4, i3)) {
                        A6 = t1.A(i4 << 3);
                        zzc = ((zzjd) l2.s(t, j)).zzc();
                        A7 = t1.A(zzc);
                        i = A6 + A7 + zzc;
                        i2 += i;
                        break;
                    } else {
                        break;
                    }
                case 62:
                    if (u(t, i4, i3)) {
                        int n2 = n(t, j);
                        A4 = t1.A(i4 << 3);
                        z = t1.A(n2);
                        i = A4 + z;
                        i2 += i;
                        break;
                    } else {
                        break;
                    }
                case 63:
                    if (u(t, i4, i3)) {
                        int n3 = n(t, j);
                        A4 = t1.A(i4 << 3);
                        z = t1.z(n3);
                        i = A4 + z;
                        i2 += i;
                        break;
                    } else {
                        break;
                    }
                case 64:
                    if (u(t, i4, i3)) {
                        A2 = t1.A(i4 << 3);
                        Y = A2 + 4;
                        i2 += Y;
                        break;
                    } else {
                        break;
                    }
                case 65:
                    if (u(t, i4, i3)) {
                        A = t1.A(i4 << 3);
                        Y = A + 8;
                        i2 += Y;
                        break;
                    } else {
                        break;
                    }
                case 66:
                    if (u(t, i4, i3)) {
                        int n4 = n(t, j);
                        A4 = t1.A(i4 << 3);
                        z = t1.A((n4 >> 31) ^ (n4 + n4));
                        i = A4 + z;
                        i2 += i;
                        break;
                    } else {
                        break;
                    }
                case 67:
                    if (u(t, i4, i3)) {
                        long o4 = o(t, j);
                        A4 = t1.A(i4 << 3);
                        z = t1.B((o4 >> 63) ^ (o4 + o4));
                        i = A4 + z;
                        i2 += i;
                        break;
                    } else {
                        break;
                    }
                case 68:
                    if (u(t, i4, i3)) {
                        Y = t1.e(i4, (z1) l2.s(t, j), O(i3));
                        i2 += Y;
                        break;
                    } else {
                        break;
                    }
            }
        }
        e2<?, ?> e2Var = this.l;
        return i2 + e2Var.h(e2Var.d(t));
    }

    /* JADX WARN: Removed duplicated region for block: B:101:0x01cf  */
    /* JADX WARN: Removed duplicated region for block: B:121:0x021d  */
    /* JADX WARN: Removed duplicated region for block: B:68:0x0152  */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:119:0x021a -> B:120:0x021b). Please submit an issue!!! */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:66:0x014f -> B:67:0x0150). Please submit an issue!!! */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:99:0x01cc -> B:100:0x01cd). Please submit an issue!!! */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final int L(T r16, byte[] r17, int r18, int r19, int r20, int r21, int r22, int r23, long r24, int r26, long r27, defpackage.fr5 r29) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 1172
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.a2.L(java.lang.Object, byte[], int, int, int, int, int, int, long, int, long, fr5):int");
    }

    public final <K, V> int M(T t, byte[] bArr, int i, int i2, int i3, long j, fr5 fr5Var) throws IOException {
        Unsafe unsafe = p;
        Object P = P(i3);
        Object object = unsafe.getObject(t, j);
        if (!((zzlc) object).zze()) {
            zzlc<K, V> zzc = zzlc.zza().zzc();
            ox5.b(zzc, object);
            unsafe.putObject(t, j, zzc);
        }
        nx5 nx5Var = (nx5) P;
        throw null;
    }

    public final int N(T t, byte[] bArr, int i, int i2, int i3, int i4, int i5, int i6, int i7, long j, int i8, fr5 fr5Var) throws IOException {
        Unsafe unsafe = p;
        long j2 = this.a[i8 + 2] & 1048575;
        switch (i7) {
            case 51:
                if (i5 == 1) {
                    unsafe.putObject(t, j, Double.valueOf(Double.longBitsToDouble(q1.e(bArr, i))));
                    unsafe.putInt(t, j2, i4);
                    return i + 8;
                }
                break;
            case 52:
                if (i5 == 5) {
                    unsafe.putObject(t, j, Float.valueOf(Float.intBitsToFloat(q1.d(bArr, i))));
                    unsafe.putInt(t, j2, i4);
                    return i + 4;
                }
                break;
            case 53:
            case 54:
                if (i5 == 0) {
                    int c = q1.c(bArr, i, fr5Var);
                    unsafe.putObject(t, j, Long.valueOf(fr5Var.b));
                    unsafe.putInt(t, j2, i4);
                    return c;
                }
                break;
            case 55:
            case 62:
                if (i5 == 0) {
                    int a = q1.a(bArr, i, fr5Var);
                    unsafe.putObject(t, j, Integer.valueOf(fr5Var.a));
                    unsafe.putInt(t, j2, i4);
                    return a;
                }
                break;
            case 56:
            case 65:
                if (i5 == 1) {
                    unsafe.putObject(t, j, Long.valueOf(q1.e(bArr, i)));
                    unsafe.putInt(t, j2, i4);
                    return i + 8;
                }
                break;
            case 57:
            case 64:
                if (i5 == 5) {
                    unsafe.putObject(t, j, Integer.valueOf(q1.d(bArr, i)));
                    unsafe.putInt(t, j2, i4);
                    return i + 4;
                }
                break;
            case 58:
                if (i5 == 0) {
                    int c2 = q1.c(bArr, i, fr5Var);
                    unsafe.putObject(t, j, Boolean.valueOf(fr5Var.b != 0));
                    unsafe.putInt(t, j2, i4);
                    return c2;
                }
                break;
            case 59:
                if (i5 == 2) {
                    int a2 = q1.a(bArr, i, fr5Var);
                    int i9 = fr5Var.a;
                    if (i9 == 0) {
                        unsafe.putObject(t, j, "");
                    } else if ((i6 & 536870912) != 0 && !n2.b(bArr, a2, a2 + i9)) {
                        throw zzkn.zzf();
                    } else {
                        unsafe.putObject(t, j, new String(bArr, a2, i9, cw5.a));
                        a2 += i9;
                    }
                    unsafe.putInt(t, j2, i4);
                    return a2;
                }
                break;
            case 60:
                if (i5 == 2) {
                    int i10 = q1.i(O(i8), bArr, i, i2, fr5Var);
                    Object object = unsafe.getInt(t, j2) == i4 ? unsafe.getObject(t, j) : null;
                    if (object == null) {
                        unsafe.putObject(t, j, fr5Var.c);
                    } else {
                        unsafe.putObject(t, j, cw5.i(object, fr5Var.c));
                    }
                    unsafe.putInt(t, j2, i4);
                    return i10;
                }
                break;
            case 61:
                if (i5 == 2) {
                    int h = q1.h(bArr, i, fr5Var);
                    unsafe.putObject(t, j, fr5Var.c);
                    unsafe.putInt(t, j2, i4);
                    return h;
                }
                break;
            case 63:
                if (i5 == 0) {
                    int a3 = q1.a(bArr, i, fr5Var);
                    int i11 = fr5Var.a;
                    sv5 Q = Q(i8);
                    if (Q != null && !Q.d(i11)) {
                        C(t).h(i3, Long.valueOf(i11));
                    } else {
                        unsafe.putObject(t, j, Integer.valueOf(i11));
                        unsafe.putInt(t, j2, i4);
                    }
                    return a3;
                }
                break;
            case 66:
                if (i5 == 0) {
                    int a4 = q1.a(bArr, i, fr5Var);
                    unsafe.putObject(t, j, Integer.valueOf(ys5.a(fr5Var.a)));
                    unsafe.putInt(t, j2, i4);
                    return a4;
                }
                break;
            case 67:
                if (i5 == 0) {
                    int c3 = q1.c(bArr, i, fr5Var);
                    unsafe.putObject(t, j, Long.valueOf(ys5.b(fr5Var.b)));
                    unsafe.putInt(t, j2, i4);
                    return c3;
                }
                break;
            case 68:
                if (i5 == 3) {
                    int j3 = q1.j(O(i8), bArr, i, i2, (i3 & (-8)) | 4, fr5Var);
                    Object object2 = unsafe.getInt(t, j2) == i4 ? unsafe.getObject(t, j) : null;
                    if (object2 == null) {
                        unsafe.putObject(t, j, fr5Var.c);
                    } else {
                        unsafe.putObject(t, j, cw5.i(object2, fr5Var.c));
                    }
                    unsafe.putInt(t, j2, i4);
                    return j3;
                }
                break;
        }
        return i;
    }

    public final c2 O(int i) {
        int i2 = i / 3;
        int i3 = i2 + i2;
        c2 c2Var = (c2) this.b[i3];
        if (c2Var != null) {
            return c2Var;
        }
        c2<T> b = jy5.a().b((Class) this.b[i3 + 1]);
        this.b[i3] = b;
        return b;
    }

    public final Object P(int i) {
        int i2 = i / 3;
        return this.b[i2 + i2];
    }

    public final sv5 Q(int i) {
        int i2 = i / 3;
        return (sv5) this.b[i2 + i2 + 1];
    }

    /* JADX WARN: Code restructure failed: missing block: B:106:0x02a9, code lost:
        if (r0 != r15) goto L154;
     */
    /* JADX WARN: Code restructure failed: missing block: B:107:0x02ab, code lost:
        r15 = r30;
        r14 = r31;
        r12 = r32;
        r13 = r34;
        r11 = r35;
        r10 = r18;
        r2 = r19;
        r1 = r20;
        r6 = r24;
        r7 = r25;
     */
    /* JADX WARN: Code restructure failed: missing block: B:108:0x02c1, code lost:
        r2 = r0;
     */
    /* JADX WARN: Code restructure failed: missing block: B:114:0x02f4, code lost:
        if (r0 != r15) goto L154;
     */
    /* JADX WARN: Code restructure failed: missing block: B:119:0x0317, code lost:
        if (r0 != r15) goto L154;
     */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r3v10, types: [int] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final int R(T r31, byte[] r32, int r33, int r34, defpackage.fr5 r35) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 898
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.a2.R(java.lang.Object, byte[], int, int, fr5):int");
    }

    @Override // com.google.android.gms.internal.measurement.c2
    public final int a(T t) {
        int i;
        int e;
        int length = this.a.length;
        int i2 = 0;
        for (int i3 = 0; i3 < length; i3 += 3) {
            int b = b(i3);
            int i4 = this.a[i3];
            long j = 1048575 & b;
            int i5 = 37;
            switch (k(b)) {
                case 0:
                    i = i2 * 53;
                    e = cw5.e(Double.doubleToLongBits(l2.q(t, j)));
                    i2 = i + e;
                    break;
                case 1:
                    i = i2 * 53;
                    e = Float.floatToIntBits(l2.o(t, j));
                    i2 = i + e;
                    break;
                case 2:
                    i = i2 * 53;
                    e = cw5.e(l2.k(t, j));
                    i2 = i + e;
                    break;
                case 3:
                    i = i2 * 53;
                    e = cw5.e(l2.k(t, j));
                    i2 = i + e;
                    break;
                case 4:
                    i = i2 * 53;
                    e = l2.i(t, j);
                    i2 = i + e;
                    break;
                case 5:
                    i = i2 * 53;
                    e = cw5.e(l2.k(t, j));
                    i2 = i + e;
                    break;
                case 6:
                    i = i2 * 53;
                    e = l2.i(t, j);
                    i2 = i + e;
                    break;
                case 7:
                    i = i2 * 53;
                    e = cw5.f(l2.m(t, j));
                    i2 = i + e;
                    break;
                case 8:
                    i = i2 * 53;
                    e = ((String) l2.s(t, j)).hashCode();
                    i2 = i + e;
                    break;
                case 9:
                    Object s = l2.s(t, j);
                    if (s != null) {
                        i5 = s.hashCode();
                    }
                    i2 = (i2 * 53) + i5;
                    break;
                case 10:
                    i = i2 * 53;
                    e = l2.s(t, j).hashCode();
                    i2 = i + e;
                    break;
                case 11:
                    i = i2 * 53;
                    e = l2.i(t, j);
                    i2 = i + e;
                    break;
                case 12:
                    i = i2 * 53;
                    e = l2.i(t, j);
                    i2 = i + e;
                    break;
                case 13:
                    i = i2 * 53;
                    e = l2.i(t, j);
                    i2 = i + e;
                    break;
                case 14:
                    i = i2 * 53;
                    e = cw5.e(l2.k(t, j));
                    i2 = i + e;
                    break;
                case 15:
                    i = i2 * 53;
                    e = l2.i(t, j);
                    i2 = i + e;
                    break;
                case 16:
                    i = i2 * 53;
                    e = cw5.e(l2.k(t, j));
                    i2 = i + e;
                    break;
                case 17:
                    Object s2 = l2.s(t, j);
                    if (s2 != null) {
                        i5 = s2.hashCode();
                    }
                    i2 = (i2 * 53) + i5;
                    break;
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                case 48:
                case 49:
                    i = i2 * 53;
                    e = l2.s(t, j).hashCode();
                    i2 = i + e;
                    break;
                case 50:
                    i = i2 * 53;
                    e = l2.s(t, j).hashCode();
                    i2 = i + e;
                    break;
                case 51:
                    if (u(t, i4, i3)) {
                        i = i2 * 53;
                        e = cw5.e(Double.doubleToLongBits(l(t, j)));
                        i2 = i + e;
                        break;
                    } else {
                        break;
                    }
                case 52:
                    if (u(t, i4, i3)) {
                        i = i2 * 53;
                        e = Float.floatToIntBits(m(t, j));
                        i2 = i + e;
                        break;
                    } else {
                        break;
                    }
                case 53:
                    if (u(t, i4, i3)) {
                        i = i2 * 53;
                        e = cw5.e(o(t, j));
                        i2 = i + e;
                        break;
                    } else {
                        break;
                    }
                case 54:
                    if (u(t, i4, i3)) {
                        i = i2 * 53;
                        e = cw5.e(o(t, j));
                        i2 = i + e;
                        break;
                    } else {
                        break;
                    }
                case 55:
                    if (u(t, i4, i3)) {
                        i = i2 * 53;
                        e = n(t, j);
                        i2 = i + e;
                        break;
                    } else {
                        break;
                    }
                case 56:
                    if (u(t, i4, i3)) {
                        i = i2 * 53;
                        e = cw5.e(o(t, j));
                        i2 = i + e;
                        break;
                    } else {
                        break;
                    }
                case 57:
                    if (u(t, i4, i3)) {
                        i = i2 * 53;
                        e = n(t, j);
                        i2 = i + e;
                        break;
                    } else {
                        break;
                    }
                case 58:
                    if (u(t, i4, i3)) {
                        i = i2 * 53;
                        e = cw5.f(p(t, j));
                        i2 = i + e;
                        break;
                    } else {
                        break;
                    }
                case 59:
                    if (u(t, i4, i3)) {
                        i = i2 * 53;
                        e = ((String) l2.s(t, j)).hashCode();
                        i2 = i + e;
                        break;
                    } else {
                        break;
                    }
                case 60:
                    if (u(t, i4, i3)) {
                        i = i2 * 53;
                        e = l2.s(t, j).hashCode();
                        i2 = i + e;
                        break;
                    } else {
                        break;
                    }
                case 61:
                    if (u(t, i4, i3)) {
                        i = i2 * 53;
                        e = l2.s(t, j).hashCode();
                        i2 = i + e;
                        break;
                    } else {
                        break;
                    }
                case 62:
                    if (u(t, i4, i3)) {
                        i = i2 * 53;
                        e = n(t, j);
                        i2 = i + e;
                        break;
                    } else {
                        break;
                    }
                case 63:
                    if (u(t, i4, i3)) {
                        i = i2 * 53;
                        e = n(t, j);
                        i2 = i + e;
                        break;
                    } else {
                        break;
                    }
                case 64:
                    if (u(t, i4, i3)) {
                        i = i2 * 53;
                        e = n(t, j);
                        i2 = i + e;
                        break;
                    } else {
                        break;
                    }
                case 65:
                    if (u(t, i4, i3)) {
                        i = i2 * 53;
                        e = cw5.e(o(t, j));
                        i2 = i + e;
                        break;
                    } else {
                        break;
                    }
                case 66:
                    if (u(t, i4, i3)) {
                        i = i2 * 53;
                        e = n(t, j);
                        i2 = i + e;
                        break;
                    } else {
                        break;
                    }
                case 67:
                    if (u(t, i4, i3)) {
                        i = i2 * 53;
                        e = cw5.e(o(t, j));
                        i2 = i + e;
                        break;
                    } else {
                        break;
                    }
                case 68:
                    if (u(t, i4, i3)) {
                        i = i2 * 53;
                        e = l2.s(t, j).hashCode();
                        i2 = i + e;
                        break;
                    } else {
                        break;
                    }
            }
        }
        int hashCode = (i2 * 53) + this.l.d(t).hashCode();
        if (this.f) {
            this.m.b(t);
            throw null;
        }
        return hashCode;
    }

    public final int b(int i) {
        return this.a[i + 1];
    }

    @Override // com.google.android.gms.internal.measurement.c2
    public final boolean c(T t, T t2) {
        boolean d;
        int length = this.a.length;
        for (int i = 0; i < length; i += 3) {
            int b = b(i);
            long j = b & 1048575;
            switch (k(b)) {
                case 0:
                    if (q(t, t2, i) && Double.doubleToLongBits(l2.q(t, j)) == Double.doubleToLongBits(l2.q(t2, j))) {
                        continue;
                    }
                    return false;
                case 1:
                    if (q(t, t2, i) && Float.floatToIntBits(l2.o(t, j)) == Float.floatToIntBits(l2.o(t2, j))) {
                        continue;
                    }
                    return false;
                case 2:
                    if (q(t, t2, i) && l2.k(t, j) == l2.k(t2, j)) {
                        continue;
                    }
                    return false;
                case 3:
                    if (q(t, t2, i) && l2.k(t, j) == l2.k(t2, j)) {
                        continue;
                    }
                    return false;
                case 4:
                    if (q(t, t2, i) && l2.i(t, j) == l2.i(t2, j)) {
                        continue;
                    }
                    return false;
                case 5:
                    if (q(t, t2, i) && l2.k(t, j) == l2.k(t2, j)) {
                        continue;
                    }
                    return false;
                case 6:
                    if (q(t, t2, i) && l2.i(t, j) == l2.i(t2, j)) {
                        continue;
                    }
                    return false;
                case 7:
                    if (q(t, t2, i) && l2.m(t, j) == l2.m(t2, j)) {
                        continue;
                    }
                    return false;
                case 8:
                    if (q(t, t2, i) && d2.d(l2.s(t, j), l2.s(t2, j))) {
                        continue;
                    }
                    return false;
                case 9:
                    if (q(t, t2, i) && d2.d(l2.s(t, j), l2.s(t2, j))) {
                        continue;
                    }
                    return false;
                case 10:
                    if (q(t, t2, i) && d2.d(l2.s(t, j), l2.s(t2, j))) {
                        continue;
                    }
                    return false;
                case 11:
                    if (q(t, t2, i) && l2.i(t, j) == l2.i(t2, j)) {
                        continue;
                    }
                    return false;
                case 12:
                    if (q(t, t2, i) && l2.i(t, j) == l2.i(t2, j)) {
                        continue;
                    }
                    return false;
                case 13:
                    if (q(t, t2, i) && l2.i(t, j) == l2.i(t2, j)) {
                        continue;
                    }
                    return false;
                case 14:
                    if (q(t, t2, i) && l2.k(t, j) == l2.k(t2, j)) {
                        continue;
                    }
                    return false;
                case 15:
                    if (q(t, t2, i) && l2.i(t, j) == l2.i(t2, j)) {
                        continue;
                    }
                    return false;
                case 16:
                    if (q(t, t2, i) && l2.k(t, j) == l2.k(t2, j)) {
                        continue;
                    }
                    return false;
                case 17:
                    if (q(t, t2, i) && d2.d(l2.s(t, j), l2.s(t2, j))) {
                        continue;
                    }
                    return false;
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                case 48:
                case 49:
                    d = d2.d(l2.s(t, j), l2.s(t2, j));
                    break;
                case 50:
                    d = d2.d(l2.s(t, j), l2.s(t2, j));
                    break;
                case 51:
                case 52:
                case 53:
                case 54:
                case 55:
                case 56:
                case 57:
                case 58:
                case 59:
                case 60:
                case 61:
                case 62:
                case 63:
                case 64:
                case 65:
                case 66:
                case 67:
                case 68:
                    long j2 = j(i) & 1048575;
                    if (l2.i(t, j2) == l2.i(t2, j2) && d2.d(l2.s(t, j), l2.s(t2, j))) {
                        continue;
                    }
                    return false;
                default:
            }
            if (!d) {
                return false;
            }
        }
        if (this.l.d(t).equals(this.l.d(t2))) {
            if (this.f) {
                this.m.b(t);
                this.m.b(t2);
                throw null;
            }
            return true;
        }
        return false;
    }

    @Override // com.google.android.gms.internal.measurement.c2
    public final int d(T t) {
        return this.g ? K(t) : J(t);
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.google.android.gms.internal.measurement.c2
    public final boolean e(T t) {
        int i;
        int i2;
        int i3 = 1048575;
        int i4 = 0;
        int i5 = 0;
        while (i5 < this.i) {
            int i6 = this.h[i5];
            int i7 = this.a[i6];
            int b = b(i6);
            int i8 = this.a[i6 + 2];
            int i9 = i8 & 1048575;
            int i10 = 1 << (i8 >>> 20);
            if (i9 != i3) {
                if (i9 != 1048575) {
                    i4 = p.getInt(t, i9);
                }
                i2 = i4;
                i = i9;
            } else {
                i = i3;
                i2 = i4;
            }
            if ((268435456 & b) != 0 && !r(t, i6, i, i2, i10)) {
                return false;
            }
            int k = k(b);
            if (k != 9 && k != 17) {
                if (k != 27) {
                    if (k == 60 || k == 68) {
                        if (u(t, i7, i6) && !S(t, b, O(i6))) {
                            return false;
                        }
                    } else if (k != 49) {
                        if (k == 50 && !((zzlc) l2.s(t, b & 1048575)).isEmpty()) {
                            nx5 nx5Var = (nx5) P(i6);
                            throw null;
                        }
                    }
                }
                List list = (List) l2.s(t, b & 1048575);
                if (list.isEmpty()) {
                    continue;
                } else {
                    c2 O = O(i6);
                    for (int i11 = 0; i11 < list.size(); i11++) {
                        if (!O.e(list.get(i11))) {
                            return false;
                        }
                    }
                    continue;
                }
            } else if (r(t, i6, i, i2, i10) && !S(t, b, O(i6))) {
                return false;
            }
            i5++;
            i3 = i;
            i4 = i2;
        }
        if (this.f) {
            this.m.b(t);
            throw null;
        }
        return true;
    }

    @Override // com.google.android.gms.internal.measurement.c2
    public final void f(T t) {
        int i;
        int i2 = this.i;
        while (true) {
            i = this.j;
            if (i2 >= i) {
                break;
            }
            long b = b(this.h[i2]) & 1048575;
            Object s = l2.s(t, b);
            if (s != null) {
                ((zzlc) s).zzd();
                l2.t(t, b, s);
            }
            i2++;
        }
        int length = this.h.length;
        while (i < length) {
            this.k.a(t, this.h[i]);
            i++;
        }
        this.l.e(t);
        if (this.f) {
            this.m.c(t);
        }
    }

    @Override // com.google.android.gms.internal.measurement.c2
    public final void g(T t, u1 u1Var) throws IOException {
        if (!this.g) {
            z(t, u1Var);
        } else if (!this.f) {
            int length = this.a.length;
            for (int i = 0; i < length; i += 3) {
                int b = b(i);
                int i2 = this.a[i];
                switch (k(b)) {
                    case 0:
                        if (s(t, i)) {
                            u1Var.q(i2, l2.q(t, b & 1048575));
                            break;
                        } else {
                            break;
                        }
                    case 1:
                        if (s(t, i)) {
                            u1Var.p(i2, l2.o(t, b & 1048575));
                            break;
                        } else {
                            break;
                        }
                    case 2:
                        if (s(t, i)) {
                            u1Var.n(i2, l2.k(t, b & 1048575));
                            break;
                        } else {
                            break;
                        }
                    case 3:
                        if (s(t, i)) {
                            u1Var.s(i2, l2.k(t, b & 1048575));
                            break;
                        } else {
                            break;
                        }
                    case 4:
                        if (s(t, i)) {
                            u1Var.t(i2, l2.i(t, b & 1048575));
                            break;
                        } else {
                            break;
                        }
                    case 5:
                        if (s(t, i)) {
                            u1Var.u(i2, l2.k(t, b & 1048575));
                            break;
                        } else {
                            break;
                        }
                    case 6:
                        if (s(t, i)) {
                            u1Var.v(i2, l2.i(t, b & 1048575));
                            break;
                        } else {
                            break;
                        }
                    case 7:
                        if (s(t, i)) {
                            u1Var.w(i2, l2.m(t, b & 1048575));
                            break;
                        } else {
                            break;
                        }
                    case 8:
                        if (s(t, i)) {
                            B(i2, l2.s(t, b & 1048575), u1Var);
                            break;
                        } else {
                            break;
                        }
                    case 9:
                        if (s(t, i)) {
                            u1Var.C(i2, l2.s(t, b & 1048575), O(i));
                            break;
                        } else {
                            break;
                        }
                    case 10:
                        if (s(t, i)) {
                            u1Var.y(i2, (zzjd) l2.s(t, b & 1048575));
                            break;
                        } else {
                            break;
                        }
                    case 11:
                        if (s(t, i)) {
                            u1Var.z(i2, l2.i(t, b & 1048575));
                            break;
                        } else {
                            break;
                        }
                    case 12:
                        if (s(t, i)) {
                            u1Var.r(i2, l2.i(t, b & 1048575));
                            break;
                        } else {
                            break;
                        }
                    case 13:
                        if (s(t, i)) {
                            u1Var.m(i2, l2.i(t, b & 1048575));
                            break;
                        } else {
                            break;
                        }
                    case 14:
                        if (s(t, i)) {
                            u1Var.o(i2, l2.k(t, b & 1048575));
                            break;
                        } else {
                            break;
                        }
                    case 15:
                        if (s(t, i)) {
                            u1Var.A(i2, l2.i(t, b & 1048575));
                            break;
                        } else {
                            break;
                        }
                    case 16:
                        if (s(t, i)) {
                            u1Var.B(i2, l2.k(t, b & 1048575));
                            break;
                        } else {
                            break;
                        }
                    case 17:
                        if (s(t, i)) {
                            u1Var.D(i2, l2.s(t, b & 1048575), O(i));
                            break;
                        } else {
                            break;
                        }
                    case 18:
                        d2.j(this.a[i], (List) l2.s(t, b & 1048575), u1Var, false);
                        break;
                    case 19:
                        d2.k(this.a[i], (List) l2.s(t, b & 1048575), u1Var, false);
                        break;
                    case 20:
                        d2.l(this.a[i], (List) l2.s(t, b & 1048575), u1Var, false);
                        break;
                    case 21:
                        d2.m(this.a[i], (List) l2.s(t, b & 1048575), u1Var, false);
                        break;
                    case 22:
                        d2.q(this.a[i], (List) l2.s(t, b & 1048575), u1Var, false);
                        break;
                    case 23:
                        d2.o(this.a[i], (List) l2.s(t, b & 1048575), u1Var, false);
                        break;
                    case 24:
                        d2.t(this.a[i], (List) l2.s(t, b & 1048575), u1Var, false);
                        break;
                    case 25:
                        d2.w(this.a[i], (List) l2.s(t, b & 1048575), u1Var, false);
                        break;
                    case 26:
                        d2.x(this.a[i], (List) l2.s(t, b & 1048575), u1Var);
                        break;
                    case 27:
                        d2.z(this.a[i], (List) l2.s(t, b & 1048575), u1Var, O(i));
                        break;
                    case 28:
                        d2.y(this.a[i], (List) l2.s(t, b & 1048575), u1Var);
                        break;
                    case 29:
                        d2.r(this.a[i], (List) l2.s(t, b & 1048575), u1Var, false);
                        break;
                    case 30:
                        d2.v(this.a[i], (List) l2.s(t, b & 1048575), u1Var, false);
                        break;
                    case 31:
                        d2.u(this.a[i], (List) l2.s(t, b & 1048575), u1Var, false);
                        break;
                    case 32:
                        d2.p(this.a[i], (List) l2.s(t, b & 1048575), u1Var, false);
                        break;
                    case 33:
                        d2.s(this.a[i], (List) l2.s(t, b & 1048575), u1Var, false);
                        break;
                    case 34:
                        d2.n(this.a[i], (List) l2.s(t, b & 1048575), u1Var, false);
                        break;
                    case 35:
                        d2.j(this.a[i], (List) l2.s(t, b & 1048575), u1Var, true);
                        break;
                    case 36:
                        d2.k(this.a[i], (List) l2.s(t, b & 1048575), u1Var, true);
                        break;
                    case 37:
                        d2.l(this.a[i], (List) l2.s(t, b & 1048575), u1Var, true);
                        break;
                    case 38:
                        d2.m(this.a[i], (List) l2.s(t, b & 1048575), u1Var, true);
                        break;
                    case 39:
                        d2.q(this.a[i], (List) l2.s(t, b & 1048575), u1Var, true);
                        break;
                    case 40:
                        d2.o(this.a[i], (List) l2.s(t, b & 1048575), u1Var, true);
                        break;
                    case 41:
                        d2.t(this.a[i], (List) l2.s(t, b & 1048575), u1Var, true);
                        break;
                    case 42:
                        d2.w(this.a[i], (List) l2.s(t, b & 1048575), u1Var, true);
                        break;
                    case 43:
                        d2.r(this.a[i], (List) l2.s(t, b & 1048575), u1Var, true);
                        break;
                    case 44:
                        d2.v(this.a[i], (List) l2.s(t, b & 1048575), u1Var, true);
                        break;
                    case 45:
                        d2.u(this.a[i], (List) l2.s(t, b & 1048575), u1Var, true);
                        break;
                    case 46:
                        d2.p(this.a[i], (List) l2.s(t, b & 1048575), u1Var, true);
                        break;
                    case 47:
                        d2.s(this.a[i], (List) l2.s(t, b & 1048575), u1Var, true);
                        break;
                    case 48:
                        d2.n(this.a[i], (List) l2.s(t, b & 1048575), u1Var, true);
                        break;
                    case 49:
                        d2.B(this.a[i], (List) l2.s(t, b & 1048575), u1Var, O(i));
                        break;
                    case 50:
                        A(u1Var, i2, l2.s(t, b & 1048575), i);
                        break;
                    case 51:
                        if (u(t, i2, i)) {
                            u1Var.q(i2, l(t, b & 1048575));
                            break;
                        } else {
                            break;
                        }
                    case 52:
                        if (u(t, i2, i)) {
                            u1Var.p(i2, m(t, b & 1048575));
                            break;
                        } else {
                            break;
                        }
                    case 53:
                        if (u(t, i2, i)) {
                            u1Var.n(i2, o(t, b & 1048575));
                            break;
                        } else {
                            break;
                        }
                    case 54:
                        if (u(t, i2, i)) {
                            u1Var.s(i2, o(t, b & 1048575));
                            break;
                        } else {
                            break;
                        }
                    case 55:
                        if (u(t, i2, i)) {
                            u1Var.t(i2, n(t, b & 1048575));
                            break;
                        } else {
                            break;
                        }
                    case 56:
                        if (u(t, i2, i)) {
                            u1Var.u(i2, o(t, b & 1048575));
                            break;
                        } else {
                            break;
                        }
                    case 57:
                        if (u(t, i2, i)) {
                            u1Var.v(i2, n(t, b & 1048575));
                            break;
                        } else {
                            break;
                        }
                    case 58:
                        if (u(t, i2, i)) {
                            u1Var.w(i2, p(t, b & 1048575));
                            break;
                        } else {
                            break;
                        }
                    case 59:
                        if (u(t, i2, i)) {
                            B(i2, l2.s(t, b & 1048575), u1Var);
                            break;
                        } else {
                            break;
                        }
                    case 60:
                        if (u(t, i2, i)) {
                            u1Var.C(i2, l2.s(t, b & 1048575), O(i));
                            break;
                        } else {
                            break;
                        }
                    case 61:
                        if (u(t, i2, i)) {
                            u1Var.y(i2, (zzjd) l2.s(t, b & 1048575));
                            break;
                        } else {
                            break;
                        }
                    case 62:
                        if (u(t, i2, i)) {
                            u1Var.z(i2, n(t, b & 1048575));
                            break;
                        } else {
                            break;
                        }
                    case 63:
                        if (u(t, i2, i)) {
                            u1Var.r(i2, n(t, b & 1048575));
                            break;
                        } else {
                            break;
                        }
                    case 64:
                        if (u(t, i2, i)) {
                            u1Var.m(i2, n(t, b & 1048575));
                            break;
                        } else {
                            break;
                        }
                    case 65:
                        if (u(t, i2, i)) {
                            u1Var.o(i2, o(t, b & 1048575));
                            break;
                        } else {
                            break;
                        }
                    case 66:
                        if (u(t, i2, i)) {
                            u1Var.A(i2, n(t, b & 1048575));
                            break;
                        } else {
                            break;
                        }
                    case 67:
                        if (u(t, i2, i)) {
                            u1Var.B(i2, o(t, b & 1048575));
                            break;
                        } else {
                            break;
                        }
                    case 68:
                        if (u(t, i2, i)) {
                            u1Var.D(i2, l2.s(t, b & 1048575), O(i));
                            break;
                        } else {
                            break;
                        }
                }
            }
            e2<?, ?> e2Var = this.l;
            e2Var.i(e2Var.d(t), u1Var);
        } else {
            this.m.b(t);
            throw null;
        }
    }

    @Override // com.google.android.gms.internal.measurement.c2
    public final void h(T t, T t2) {
        Objects.requireNonNull(t2);
        for (int i = 0; i < this.a.length; i += 3) {
            int b = b(i);
            long j = 1048575 & b;
            int i2 = this.a[i];
            switch (k(b)) {
                case 0:
                    if (s(t2, i)) {
                        l2.r(t, j, l2.q(t2, j));
                        t(t, i);
                        break;
                    } else {
                        break;
                    }
                case 1:
                    if (s(t2, i)) {
                        l2.p(t, j, l2.o(t2, j));
                        t(t, i);
                        break;
                    } else {
                        break;
                    }
                case 2:
                    if (s(t2, i)) {
                        l2.l(t, j, l2.k(t2, j));
                        t(t, i);
                        break;
                    } else {
                        break;
                    }
                case 3:
                    if (s(t2, i)) {
                        l2.l(t, j, l2.k(t2, j));
                        t(t, i);
                        break;
                    } else {
                        break;
                    }
                case 4:
                    if (s(t2, i)) {
                        l2.j(t, j, l2.i(t2, j));
                        t(t, i);
                        break;
                    } else {
                        break;
                    }
                case 5:
                    if (s(t2, i)) {
                        l2.l(t, j, l2.k(t2, j));
                        t(t, i);
                        break;
                    } else {
                        break;
                    }
                case 6:
                    if (s(t2, i)) {
                        l2.j(t, j, l2.i(t2, j));
                        t(t, i);
                        break;
                    } else {
                        break;
                    }
                case 7:
                    if (s(t2, i)) {
                        l2.n(t, j, l2.m(t2, j));
                        t(t, i);
                        break;
                    } else {
                        break;
                    }
                case 8:
                    if (s(t2, i)) {
                        l2.t(t, j, l2.s(t2, j));
                        t(t, i);
                        break;
                    } else {
                        break;
                    }
                case 9:
                    H(t, t2, i);
                    break;
                case 10:
                    if (s(t2, i)) {
                        l2.t(t, j, l2.s(t2, j));
                        t(t, i);
                        break;
                    } else {
                        break;
                    }
                case 11:
                    if (s(t2, i)) {
                        l2.j(t, j, l2.i(t2, j));
                        t(t, i);
                        break;
                    } else {
                        break;
                    }
                case 12:
                    if (s(t2, i)) {
                        l2.j(t, j, l2.i(t2, j));
                        t(t, i);
                        break;
                    } else {
                        break;
                    }
                case 13:
                    if (s(t2, i)) {
                        l2.j(t, j, l2.i(t2, j));
                        t(t, i);
                        break;
                    } else {
                        break;
                    }
                case 14:
                    if (s(t2, i)) {
                        l2.l(t, j, l2.k(t2, j));
                        t(t, i);
                        break;
                    } else {
                        break;
                    }
                case 15:
                    if (s(t2, i)) {
                        l2.j(t, j, l2.i(t2, j));
                        t(t, i);
                        break;
                    } else {
                        break;
                    }
                case 16:
                    if (s(t2, i)) {
                        l2.l(t, j, l2.k(t2, j));
                        t(t, i);
                        break;
                    } else {
                        break;
                    }
                case 17:
                    H(t, t2, i);
                    break;
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                case 48:
                case 49:
                    this.k.b(t, t2, j);
                    break;
                case 50:
                    d2.i(this.n, t, t2, j);
                    break;
                case 51:
                case 52:
                case 53:
                case 54:
                case 55:
                case 56:
                case 57:
                case 58:
                case 59:
                    if (u(t2, i2, i)) {
                        l2.t(t, j, l2.s(t2, j));
                        v(t, i2, i);
                        break;
                    } else {
                        break;
                    }
                case 60:
                    I(t, t2, i);
                    break;
                case 61:
                case 62:
                case 63:
                case 64:
                case 65:
                case 66:
                case 67:
                    if (u(t2, i2, i)) {
                        l2.t(t, j, l2.s(t2, j));
                        v(t, i2, i);
                        break;
                    } else {
                        break;
                    }
                case 68:
                    I(t, t2, i);
                    break;
            }
        }
        d2.f(this.l, t, t2);
        if (this.f) {
            d2.e(this.m, t, t2);
        }
    }

    @Override // com.google.android.gms.internal.measurement.c2
    public final void i(T t, byte[] bArr, int i, int i2, fr5 fr5Var) throws IOException {
        if (this.g) {
            R(t, bArr, i, i2, fr5Var);
        } else {
            D(t, bArr, i, i2, 0, fr5Var);
        }
    }

    public final int j(int i) {
        return this.a[i + 2];
    }

    public final boolean q(T t, T t2, int i) {
        return s(t, i) == s(t2, i);
    }

    public final boolean r(T t, int i, int i2, int i3, int i4) {
        if (i2 == 1048575) {
            return s(t, i);
        }
        return (i3 & i4) != 0;
    }

    public final boolean s(T t, int i) {
        int j = j(i);
        long j2 = j & 1048575;
        if (j2 != 1048575) {
            return (l2.i(t, j2) & (1 << (j >>> 20))) != 0;
        }
        int b = b(i);
        long j3 = b & 1048575;
        switch (k(b)) {
            case 0:
                return l2.q(t, j3) != Utils.DOUBLE_EPSILON;
            case 1:
                return l2.o(t, j3) != Utils.FLOAT_EPSILON;
            case 2:
                return l2.k(t, j3) != 0;
            case 3:
                return l2.k(t, j3) != 0;
            case 4:
                return l2.i(t, j3) != 0;
            case 5:
                return l2.k(t, j3) != 0;
            case 6:
                return l2.i(t, j3) != 0;
            case 7:
                return l2.m(t, j3);
            case 8:
                Object s = l2.s(t, j3);
                if (s instanceof String) {
                    return !((String) s).isEmpty();
                } else if (s instanceof zzjd) {
                    return !zzjd.zzb.equals(s);
                } else {
                    throw new IllegalArgumentException();
                }
            case 9:
                return l2.s(t, j3) != null;
            case 10:
                return !zzjd.zzb.equals(l2.s(t, j3));
            case 11:
                return l2.i(t, j3) != 0;
            case 12:
                return l2.i(t, j3) != 0;
            case 13:
                return l2.i(t, j3) != 0;
            case 14:
                return l2.k(t, j3) != 0;
            case 15:
                return l2.i(t, j3) != 0;
            case 16:
                return l2.k(t, j3) != 0;
            case 17:
                return l2.s(t, j3) != null;
            default:
                throw new IllegalArgumentException();
        }
    }

    public final void t(T t, int i) {
        int j = j(i);
        long j2 = 1048575 & j;
        if (j2 == 1048575) {
            return;
        }
        l2.j(t, j2, (1 << (j >>> 20)) | l2.i(t, j2));
    }

    public final boolean u(T t, int i, int i2) {
        return l2.i(t, (long) (j(i2) & 1048575)) == i;
    }

    public final void v(T t, int i, int i2) {
        l2.j(t, j(i2) & 1048575, i);
    }

    public final int w(int i) {
        if (i < this.c || i > this.d) {
            return -1;
        }
        return y(i, 0);
    }

    public final int x(int i, int i2) {
        if (i < this.c || i > this.d) {
            return -1;
        }
        return y(i, i2);
    }

    public final int y(int i, int i2) {
        int length = (this.a.length / 3) - 1;
        while (i2 <= length) {
            int i3 = (length + i2) >>> 1;
            int i4 = i3 * 3;
            int i5 = this.a[i4];
            if (i == i5) {
                return i4;
            }
            if (i < i5) {
                length = i3 - 1;
            } else {
                i2 = i3 + 1;
            }
        }
        return -1;
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    public final void z(T t, u1 u1Var) throws IOException {
        int i;
        boolean z;
        if (!this.f) {
            int length = this.a.length;
            Unsafe unsafe = p;
            int i2 = 1048575;
            int i3 = 1048575;
            int i4 = 0;
            int i5 = 0;
            while (i4 < length) {
                int b = b(i4);
                int i6 = this.a[i4];
                int k = k(b);
                if (k <= 17) {
                    int i7 = this.a[i4 + 2];
                    int i8 = i7 & i2;
                    if (i8 != i3) {
                        i5 = unsafe.getInt(t, i8);
                        i3 = i8;
                    }
                    i = 1 << (i7 >>> 20);
                } else {
                    i = 0;
                }
                long j = b & i2;
                switch (k) {
                    case 0:
                        if ((i5 & i) == 0) {
                            break;
                        } else {
                            u1Var.q(i6, l2.q(t, j));
                            break;
                        }
                    case 1:
                        if ((i5 & i) == 0) {
                            break;
                        } else {
                            u1Var.p(i6, l2.o(t, j));
                            break;
                        }
                    case 2:
                        if ((i5 & i) == 0) {
                            break;
                        } else {
                            u1Var.n(i6, unsafe.getLong(t, j));
                            break;
                        }
                    case 3:
                        if ((i5 & i) == 0) {
                            break;
                        } else {
                            u1Var.s(i6, unsafe.getLong(t, j));
                            break;
                        }
                    case 4:
                        if ((i5 & i) == 0) {
                            break;
                        } else {
                            u1Var.t(i6, unsafe.getInt(t, j));
                            break;
                        }
                    case 5:
                        if ((i5 & i) == 0) {
                            break;
                        } else {
                            u1Var.u(i6, unsafe.getLong(t, j));
                            break;
                        }
                    case 6:
                        if ((i5 & i) == 0) {
                            break;
                        } else {
                            u1Var.v(i6, unsafe.getInt(t, j));
                            break;
                        }
                    case 7:
                        if ((i5 & i) == 0) {
                            break;
                        } else {
                            u1Var.w(i6, l2.m(t, j));
                            break;
                        }
                    case 8:
                        if ((i5 & i) == 0) {
                            break;
                        } else {
                            B(i6, unsafe.getObject(t, j), u1Var);
                            break;
                        }
                    case 9:
                        if ((i5 & i) == 0) {
                            break;
                        } else {
                            u1Var.C(i6, unsafe.getObject(t, j), O(i4));
                            break;
                        }
                    case 10:
                        if ((i5 & i) == 0) {
                            break;
                        } else {
                            u1Var.y(i6, (zzjd) unsafe.getObject(t, j));
                            break;
                        }
                    case 11:
                        if ((i5 & i) == 0) {
                            break;
                        } else {
                            u1Var.z(i6, unsafe.getInt(t, j));
                            break;
                        }
                    case 12:
                        if ((i5 & i) == 0) {
                            break;
                        } else {
                            u1Var.r(i6, unsafe.getInt(t, j));
                            break;
                        }
                    case 13:
                        if ((i5 & i) == 0) {
                            break;
                        } else {
                            u1Var.m(i6, unsafe.getInt(t, j));
                            break;
                        }
                    case 14:
                        if ((i5 & i) == 0) {
                            break;
                        } else {
                            u1Var.o(i6, unsafe.getLong(t, j));
                            break;
                        }
                    case 15:
                        if ((i5 & i) == 0) {
                            break;
                        } else {
                            u1Var.A(i6, unsafe.getInt(t, j));
                            break;
                        }
                    case 16:
                        if ((i5 & i) == 0) {
                            break;
                        } else {
                            u1Var.B(i6, unsafe.getLong(t, j));
                            break;
                        }
                    case 17:
                        if ((i5 & i) == 0) {
                            break;
                        } else {
                            u1Var.D(i6, unsafe.getObject(t, j), O(i4));
                            break;
                        }
                    case 18:
                        d2.j(this.a[i4], (List) unsafe.getObject(t, j), u1Var, false);
                        break;
                    case 19:
                        d2.k(this.a[i4], (List) unsafe.getObject(t, j), u1Var, false);
                        break;
                    case 20:
                        d2.l(this.a[i4], (List) unsafe.getObject(t, j), u1Var, false);
                        break;
                    case 21:
                        d2.m(this.a[i4], (List) unsafe.getObject(t, j), u1Var, false);
                        break;
                    case 22:
                        d2.q(this.a[i4], (List) unsafe.getObject(t, j), u1Var, false);
                        break;
                    case 23:
                        d2.o(this.a[i4], (List) unsafe.getObject(t, j), u1Var, false);
                        break;
                    case 24:
                        d2.t(this.a[i4], (List) unsafe.getObject(t, j), u1Var, false);
                        break;
                    case 25:
                        d2.w(this.a[i4], (List) unsafe.getObject(t, j), u1Var, false);
                        break;
                    case 26:
                        d2.x(this.a[i4], (List) unsafe.getObject(t, j), u1Var);
                        break;
                    case 27:
                        d2.z(this.a[i4], (List) unsafe.getObject(t, j), u1Var, O(i4));
                        break;
                    case 28:
                        d2.y(this.a[i4], (List) unsafe.getObject(t, j), u1Var);
                        break;
                    case 29:
                        z = false;
                        d2.r(this.a[i4], (List) unsafe.getObject(t, j), u1Var, false);
                        break;
                    case 30:
                        z = false;
                        d2.v(this.a[i4], (List) unsafe.getObject(t, j), u1Var, false);
                        break;
                    case 31:
                        z = false;
                        d2.u(this.a[i4], (List) unsafe.getObject(t, j), u1Var, false);
                        break;
                    case 32:
                        z = false;
                        d2.p(this.a[i4], (List) unsafe.getObject(t, j), u1Var, false);
                        break;
                    case 33:
                        z = false;
                        d2.s(this.a[i4], (List) unsafe.getObject(t, j), u1Var, false);
                        break;
                    case 34:
                        z = false;
                        d2.n(this.a[i4], (List) unsafe.getObject(t, j), u1Var, false);
                        break;
                    case 35:
                        d2.j(this.a[i4], (List) unsafe.getObject(t, j), u1Var, true);
                        break;
                    case 36:
                        d2.k(this.a[i4], (List) unsafe.getObject(t, j), u1Var, true);
                        break;
                    case 37:
                        d2.l(this.a[i4], (List) unsafe.getObject(t, j), u1Var, true);
                        break;
                    case 38:
                        d2.m(this.a[i4], (List) unsafe.getObject(t, j), u1Var, true);
                        break;
                    case 39:
                        d2.q(this.a[i4], (List) unsafe.getObject(t, j), u1Var, true);
                        break;
                    case 40:
                        d2.o(this.a[i4], (List) unsafe.getObject(t, j), u1Var, true);
                        break;
                    case 41:
                        d2.t(this.a[i4], (List) unsafe.getObject(t, j), u1Var, true);
                        break;
                    case 42:
                        d2.w(this.a[i4], (List) unsafe.getObject(t, j), u1Var, true);
                        break;
                    case 43:
                        d2.r(this.a[i4], (List) unsafe.getObject(t, j), u1Var, true);
                        break;
                    case 44:
                        d2.v(this.a[i4], (List) unsafe.getObject(t, j), u1Var, true);
                        break;
                    case 45:
                        d2.u(this.a[i4], (List) unsafe.getObject(t, j), u1Var, true);
                        break;
                    case 46:
                        d2.p(this.a[i4], (List) unsafe.getObject(t, j), u1Var, true);
                        break;
                    case 47:
                        d2.s(this.a[i4], (List) unsafe.getObject(t, j), u1Var, true);
                        break;
                    case 48:
                        d2.n(this.a[i4], (List) unsafe.getObject(t, j), u1Var, true);
                        break;
                    case 49:
                        d2.B(this.a[i4], (List) unsafe.getObject(t, j), u1Var, O(i4));
                        break;
                    case 50:
                        A(u1Var, i6, unsafe.getObject(t, j), i4);
                        break;
                    case 51:
                        if (u(t, i6, i4)) {
                            u1Var.q(i6, l(t, j));
                        }
                        break;
                    case 52:
                        if (u(t, i6, i4)) {
                            u1Var.p(i6, m(t, j));
                        }
                        break;
                    case 53:
                        if (u(t, i6, i4)) {
                            u1Var.n(i6, o(t, j));
                        }
                        break;
                    case 54:
                        if (u(t, i6, i4)) {
                            u1Var.s(i6, o(t, j));
                        }
                        break;
                    case 55:
                        if (u(t, i6, i4)) {
                            u1Var.t(i6, n(t, j));
                        }
                        break;
                    case 56:
                        if (u(t, i6, i4)) {
                            u1Var.u(i6, o(t, j));
                        }
                        break;
                    case 57:
                        if (u(t, i6, i4)) {
                            u1Var.v(i6, n(t, j));
                        }
                        break;
                    case 58:
                        if (u(t, i6, i4)) {
                            u1Var.w(i6, p(t, j));
                        }
                        break;
                    case 59:
                        if (u(t, i6, i4)) {
                            B(i6, unsafe.getObject(t, j), u1Var);
                        }
                        break;
                    case 60:
                        if (u(t, i6, i4)) {
                            u1Var.C(i6, unsafe.getObject(t, j), O(i4));
                        }
                        break;
                    case 61:
                        if (u(t, i6, i4)) {
                            u1Var.y(i6, (zzjd) unsafe.getObject(t, j));
                        }
                        break;
                    case 62:
                        if (u(t, i6, i4)) {
                            u1Var.z(i6, n(t, j));
                        }
                        break;
                    case 63:
                        if (u(t, i6, i4)) {
                            u1Var.r(i6, n(t, j));
                        }
                        break;
                    case 64:
                        if (u(t, i6, i4)) {
                            u1Var.m(i6, n(t, j));
                        }
                        break;
                    case 65:
                        if (u(t, i6, i4)) {
                            u1Var.o(i6, o(t, j));
                        }
                        break;
                    case 66:
                        if (u(t, i6, i4)) {
                            u1Var.A(i6, n(t, j));
                        }
                        break;
                    case 67:
                        if (u(t, i6, i4)) {
                            u1Var.B(i6, o(t, j));
                        }
                        break;
                    case 68:
                        if (u(t, i6, i4)) {
                            u1Var.D(i6, unsafe.getObject(t, j), O(i4));
                        }
                        break;
                }
                i4 += 3;
                i2 = 1048575;
            }
            e2<?, ?> e2Var = this.l;
            e2Var.i(e2Var.d(t), u1Var);
            return;
        }
        this.m.b(t);
        throw null;
    }

    @Override // com.google.android.gms.internal.measurement.c2
    public final T zza() {
        return (T) ((x1) this.e).w(4, null, null);
    }
}
