package com.google.android.gms.internal.measurement;

import android.os.Bundle;
import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@19.0.0 */
/* loaded from: classes.dex */
public final class q extends i0 {
    public final /* synthetic */ Bundle i0;
    public final /* synthetic */ wf5 j0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public q(wf5 wf5Var, Bundle bundle) {
        super(wf5Var, true);
        this.j0 = wf5Var;
        this.i0 = bundle;
    }

    @Override // com.google.android.gms.internal.measurement.i0
    public final void a() throws RemoteException {
        j jVar;
        jVar = this.j0.h;
        ((j) zt2.j(jVar)).setConditionalUserProperty(this.i0, this.a);
    }
}
