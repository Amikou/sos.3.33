package com.google.android.gms.internal.measurement;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* loaded from: classes.dex */
public final class w0 extends x1<w0, sh5> implements xx5 {
    private static final w0 zzi;
    private int zza;
    private String zze = "";
    private boolean zzf;
    private boolean zzg;
    private int zzh;

    static {
        w0 w0Var = new w0();
        zzi = w0Var;
        x1.u(w0.class, w0Var);
    }

    public static /* synthetic */ void D(w0 w0Var, String str) {
        str.getClass();
        w0Var.zza |= 1;
        w0Var.zze = str;
    }

    public final boolean A() {
        return (this.zza & 8) != 0;
    }

    public final int B() {
        return this.zzh;
    }

    @Override // com.google.android.gms.internal.measurement.x1
    public final Object w(int i, Object obj, Object obj2) {
        int i2 = i - 1;
        if (i2 != 0) {
            if (i2 != 2) {
                if (i2 != 3) {
                    if (i2 != 4) {
                        if (i2 != 5) {
                            return null;
                        }
                        return zzi;
                    }
                    return new sh5(null);
                }
                return new w0();
            }
            return x1.v(zzi, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0000\u0000\u0001ဈ\u0000\u0002ဇ\u0001\u0003ဇ\u0002\u0004င\u0003", new Object[]{"zza", "zze", "zzf", "zzg", "zzh"});
        }
        return (byte) 1;
    }

    public final String x() {
        return this.zze;
    }

    public final boolean y() {
        return this.zzf;
    }

    public final boolean z() {
        return this.zzg;
    }
}
