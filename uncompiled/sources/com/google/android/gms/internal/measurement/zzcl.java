package com.google.android.gms.internal.measurement;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* loaded from: classes.dex */
public final class zzcl extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzcl> CREATOR = new pb5();
    public final long a;
    public final long f0;
    public final boolean g0;
    public final String h0;
    public final String i0;
    public final String j0;
    public final Bundle k0;
    public final String l0;

    public zzcl(long j, long j2, boolean z, String str, String str2, String str3, Bundle bundle, String str4) {
        this.a = j;
        this.f0 = j2;
        this.g0 = z;
        this.h0 = str;
        this.i0 = str2;
        this.j0 = str3;
        this.k0 = bundle;
        this.l0 = str4;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int a = yb3.a(parcel);
        yb3.o(parcel, 1, this.a);
        yb3.o(parcel, 2, this.f0);
        yb3.c(parcel, 3, this.g0);
        yb3.s(parcel, 4, this.h0, false);
        yb3.s(parcel, 5, this.i0, false);
        yb3.s(parcel, 6, this.j0, false);
        yb3.e(parcel, 7, this.k0, false);
        yb3.s(parcel, 8, this.l0, false);
        yb3.b(parcel, a);
    }
}
