package com.google.android.gms.internal.measurement;

import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* loaded from: classes.dex */
public final class h1 extends x1<h1, cl5> implements xx5 {
    private static final h1 zzh;
    private xv5 zza = x1.m();
    private xv5 zze = x1.m();
    private zv5<a1> zzf = x1.o();
    private zv5<i1> zzg = x1.o();

    static {
        h1 h1Var = new h1();
        zzh = h1Var;
        x1.u(h1.class, h1Var);
    }

    public static cl5 H() {
        return zzh.r();
    }

    public static h1 I() {
        return zzh;
    }

    public static /* synthetic */ void K(h1 h1Var, Iterable iterable) {
        xv5 xv5Var = h1Var.zza;
        if (!xv5Var.zza()) {
            h1Var.zza = x1.n(xv5Var);
        }
        zq5.j(iterable, h1Var.zza);
    }

    public static /* synthetic */ void M(h1 h1Var, Iterable iterable) {
        xv5 xv5Var = h1Var.zze;
        if (!xv5Var.zza()) {
            h1Var.zze = x1.n(xv5Var);
        }
        zq5.j(iterable, h1Var.zze);
    }

    public static /* synthetic */ void O(h1 h1Var, Iterable iterable) {
        h1Var.S();
        zq5.j(iterable, h1Var.zzf);
    }

    public static /* synthetic */ void P(h1 h1Var, int i) {
        h1Var.S();
        h1Var.zzf.remove(i);
    }

    public static /* synthetic */ void Q(h1 h1Var, Iterable iterable) {
        h1Var.T();
        zq5.j(iterable, h1Var.zzg);
    }

    public static /* synthetic */ void R(h1 h1Var, int i) {
        h1Var.T();
        h1Var.zzg.remove(i);
    }

    public final int A() {
        return this.zze.size();
    }

    public final List<a1> B() {
        return this.zzf;
    }

    public final int C() {
        return this.zzf.size();
    }

    public final a1 D(int i) {
        return this.zzf.get(i);
    }

    public final List<i1> E() {
        return this.zzg;
    }

    public final int F() {
        return this.zzg.size();
    }

    public final i1 G(int i) {
        return this.zzg.get(i);
    }

    public final void S() {
        zv5<a1> zv5Var = this.zzf;
        if (zv5Var.zza()) {
            return;
        }
        this.zzf = x1.p(zv5Var);
    }

    public final void T() {
        zv5<i1> zv5Var = this.zzg;
        if (zv5Var.zza()) {
            return;
        }
        this.zzg = x1.p(zv5Var);
    }

    @Override // com.google.android.gms.internal.measurement.x1
    public final Object w(int i, Object obj, Object obj2) {
        int i2 = i - 1;
        if (i2 != 0) {
            if (i2 != 2) {
                if (i2 != 3) {
                    if (i2 != 4) {
                        if (i2 != 5) {
                            return null;
                        }
                        return zzh;
                    }
                    return new cl5(null);
                }
                return new h1();
            }
            return x1.v(zzh, "\u0001\u0004\u0000\u0000\u0001\u0004\u0004\u0000\u0004\u0000\u0001\u0015\u0002\u0015\u0003\u001b\u0004\u001b", new Object[]{"zza", "zze", "zzf", a1.class, "zzg", i1.class});
        }
        return (byte) 1;
    }

    public final List<Long> x() {
        return this.zza;
    }

    public final int y() {
        return this.zza.size();
    }

    public final List<Long> z() {
        return this.zze;
    }
}
