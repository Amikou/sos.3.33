package com.google.android.gms.internal.measurement;

import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* loaded from: classes.dex */
public final class q0 extends x1<q0, ag5> implements xx5 {
    private static final q0 zzj;
    private int zza;
    private int zze;
    private zv5<u0> zzf = x1.o();
    private zv5<r0> zzg = x1.o();
    private boolean zzh;
    private boolean zzi;

    static {
        q0 q0Var = new q0();
        zzj = q0Var;
        x1.u(q0.class, q0Var);
    }

    public static /* synthetic */ void G(q0 q0Var, int i, u0 u0Var) {
        u0Var.getClass();
        zv5<u0> zv5Var = q0Var.zzf;
        if (!zv5Var.zza()) {
            q0Var.zzf = x1.p(zv5Var);
        }
        q0Var.zzf.set(i, u0Var);
    }

    public static /* synthetic */ void H(q0 q0Var, int i, r0 r0Var) {
        r0Var.getClass();
        zv5<r0> zv5Var = q0Var.zzg;
        if (!zv5Var.zza()) {
            q0Var.zzg = x1.p(zv5Var);
        }
        q0Var.zzg.set(i, r0Var);
    }

    public final int A() {
        return this.zzf.size();
    }

    public final u0 B(int i) {
        return this.zzf.get(i);
    }

    public final List<r0> C() {
        return this.zzg;
    }

    public final int D() {
        return this.zzg.size();
    }

    public final r0 E(int i) {
        return this.zzg.get(i);
    }

    @Override // com.google.android.gms.internal.measurement.x1
    public final Object w(int i, Object obj, Object obj2) {
        int i2 = i - 1;
        if (i2 != 0) {
            if (i2 != 2) {
                if (i2 != 3) {
                    if (i2 != 4) {
                        if (i2 != 5) {
                            return null;
                        }
                        return zzj;
                    }
                    return new ag5(null);
                }
                return new q0();
            }
            return x1.v(zzj, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0002\u0000\u0001င\u0000\u0002\u001b\u0003\u001b\u0004ဇ\u0001\u0005ဇ\u0002", new Object[]{"zza", "zze", "zzf", u0.class, "zzg", r0.class, "zzh", "zzi"});
        }
        return (byte) 1;
    }

    public final boolean x() {
        return (this.zza & 1) != 0;
    }

    public final int y() {
        return this.zze;
    }

    public final List<u0> z() {
        return this.zzf;
    }
}
