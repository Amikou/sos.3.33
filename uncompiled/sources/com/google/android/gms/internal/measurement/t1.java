package com.google.android.gms.internal.measurement;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* loaded from: classes.dex */
public abstract class t1 extends mr5 {
    public static final Logger b = Logger.getLogger(t1.class.getName());
    public static final boolean c = l2.f();
    public u1 a;

    public t1() {
    }

    public /* synthetic */ t1(at5 at5Var) {
    }

    public static int A(int i) {
        if ((i & (-128)) == 0) {
            return 1;
        }
        if ((i & (-16384)) == 0) {
            return 2;
        }
        if (((-2097152) & i) == 0) {
            return 3;
        }
        return (i & (-268435456)) == 0 ? 4 : 5;
    }

    public static int B(long j) {
        int i;
        if (((-128) & j) == 0) {
            return 1;
        }
        if (j < 0) {
            return 10;
        }
        if (((-34359738368L) & j) != 0) {
            j >>>= 28;
            i = 6;
        } else {
            i = 2;
        }
        if (((-2097152) & j) != 0) {
            i += 2;
            j >>>= 14;
        }
        return (j & (-16384)) != 0 ? i + 1 : i;
    }

    public static int C(String str) {
        int length;
        try {
            length = n2.c(str);
        } catch (zzmv unused) {
            length = str.getBytes(cw5.a).length;
        }
        return A(length) + length;
    }

    public static int D(iw5 iw5Var) {
        int a = iw5Var.a();
        return A(a) + a;
    }

    public static int a(zzjd zzjdVar) {
        int zzc = zzjdVar.zzc();
        return A(zzc) + zzc;
    }

    public static int b(z1 z1Var, c2 c2Var) {
        zq5 zq5Var = (zq5) z1Var;
        int h = zq5Var.h();
        if (h == -1) {
            h = c2Var.d(zq5Var);
            zq5Var.i(h);
        }
        return A(h) + h;
    }

    @Deprecated
    public static int e(int i, z1 z1Var, c2 c2Var) {
        int A = A(i << 3);
        int i2 = A + A;
        zq5 zq5Var = (zq5) z1Var;
        int h = zq5Var.h();
        if (h == -1) {
            h = c2Var.d(zq5Var);
            zq5Var.i(h);
        }
        return i2 + h;
    }

    public static t1 x(byte[] bArr) {
        return new s1(bArr, 0, bArr.length);
    }

    public static int y(int i) {
        return A(i << 3);
    }

    public static int z(int i) {
        if (i >= 0) {
            return A(i);
        }
        return 10;
    }

    public final void c() {
        if (w() != 0) {
            throw new IllegalStateException("Did not write as much data as expected.");
        }
    }

    public final void d(String str, zzmv zzmvVar) throws IOException {
        b.logp(Level.WARNING, "com.google.protobuf.CodedOutputStream", "inefficientWriteStringNoTag", "Converting ill-formed UTF-16. Your Protocol Buffer will not round trip correctly!", (Throwable) zzmvVar);
        byte[] bytes = str.getBytes(cw5.a);
        try {
            int length = bytes.length;
            r(length);
            v(bytes, 0, length);
        } catch (zzjj e) {
            throw e;
        } catch (IndexOutOfBoundsException e2) {
            throw new zzjj(e2);
        }
    }

    public abstract void g(int i, int i2) throws IOException;

    public abstract void h(int i, int i2) throws IOException;

    public abstract void i(int i, int i2) throws IOException;

    public abstract void j(int i, int i2) throws IOException;

    public abstract void k(int i, long j) throws IOException;

    public abstract void l(int i, long j) throws IOException;

    public abstract void m(int i, boolean z) throws IOException;

    public abstract void n(int i, String str) throws IOException;

    public abstract void o(int i, zzjd zzjdVar) throws IOException;

    public abstract void p(byte b2) throws IOException;

    public abstract void q(int i) throws IOException;

    public abstract void r(int i) throws IOException;

    public abstract void s(int i) throws IOException;

    public abstract void t(long j) throws IOException;

    public abstract void u(long j) throws IOException;

    public abstract void v(byte[] bArr, int i, int i2) throws IOException;

    public abstract int w();
}
