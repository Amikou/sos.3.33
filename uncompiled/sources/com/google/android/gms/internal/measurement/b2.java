package com.google.android.gms.internal.measurement;

import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* loaded from: classes.dex */
public final class b2<T> implements c2<T> {
    public final z1 a;
    public final e2<?, ?> b;
    public final boolean c;
    public final tt5<?> d;

    public b2(e2<?, ?> e2Var, tt5<?> tt5Var, z1 z1Var) {
        this.b = e2Var;
        this.c = tt5Var.a(z1Var);
        this.d = tt5Var;
        this.a = z1Var;
    }

    public static <T> b2<T> b(e2<?, ?> e2Var, tt5<?> tt5Var, z1 z1Var) {
        return new b2<>(e2Var, tt5Var, z1Var);
    }

    @Override // com.google.android.gms.internal.measurement.c2
    public final int a(T t) {
        int hashCode = this.b.d(t).hashCode();
        if (this.c) {
            this.d.b(t);
            throw null;
        }
        return hashCode;
    }

    @Override // com.google.android.gms.internal.measurement.c2
    public final boolean c(T t, T t2) {
        if (this.b.d(t).equals(this.b.d(t2))) {
            if (this.c) {
                this.d.b(t);
                this.d.b(t2);
                throw null;
            }
            return true;
        }
        return false;
    }

    @Override // com.google.android.gms.internal.measurement.c2
    public final int d(T t) {
        e2<?, ?> e2Var = this.b;
        int g = e2Var.g(e2Var.d(t));
        if (this.c) {
            this.d.b(t);
            throw null;
        }
        return g;
    }

    @Override // com.google.android.gms.internal.measurement.c2
    public final boolean e(T t) {
        this.d.b(t);
        throw null;
    }

    @Override // com.google.android.gms.internal.measurement.c2
    public final void f(T t) {
        this.b.e(t);
        this.d.c(t);
    }

    @Override // com.google.android.gms.internal.measurement.c2
    public final void g(T t, u1 u1Var) throws IOException {
        this.d.b(t);
        throw null;
    }

    @Override // com.google.android.gms.internal.measurement.c2
    public final void h(T t, T t2) {
        d2.f(this.b, t, t2);
        if (this.c) {
            d2.e(this.d, t, t2);
        }
    }

    @Override // com.google.android.gms.internal.measurement.c2
    public final void i(T t, byte[] bArr, int i, int i2, fr5 fr5Var) throws IOException {
        x1 x1Var = (x1) t;
        if (x1Var.zzc == f2.a()) {
            x1Var.zzc = f2.b();
        }
        cv5 cv5Var = (cv5) t;
        throw null;
    }

    @Override // com.google.android.gms.internal.measurement.c2
    public final T zza() {
        return (T) this.a.a().q();
    }
}
