package com.google.android.gms.internal.measurement;

import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* loaded from: classes.dex */
public final class x0 extends x1<x0, li5> implements xx5 {
    private static final x0 zzn;
    private int zza;
    private long zze;
    private int zzg;
    private boolean zzl;
    private String zzf = "";
    private zv5<y0> zzh = x1.o();
    private zv5<w0> zzi = x1.o();
    private zv5<q0> zzj = x1.o();
    private String zzk = "";
    private zv5<m1> zzm = x1.o();

    static {
        x0 x0Var = new x0();
        zzn = x0Var;
        x1.u(x0.class, x0Var);
    }

    public static li5 I() {
        return zzn.r();
    }

    public static x0 J() {
        return zzn;
    }

    public static /* synthetic */ void L(x0 x0Var, int i, w0 w0Var) {
        w0Var.getClass();
        zv5<w0> zv5Var = x0Var.zzi;
        if (!zv5Var.zza()) {
            x0Var.zzi = x1.p(zv5Var);
        }
        x0Var.zzi.set(i, w0Var);
    }

    public final String A() {
        return this.zzf;
    }

    public final List<y0> B() {
        return this.zzh;
    }

    public final int C() {
        return this.zzi.size();
    }

    public final w0 D(int i) {
        return this.zzi.get(i);
    }

    public final List<q0> E() {
        return this.zzj;
    }

    public final boolean F() {
        return this.zzl;
    }

    public final List<m1> G() {
        return this.zzm;
    }

    public final int H() {
        return this.zzm.size();
    }

    @Override // com.google.android.gms.internal.measurement.x1
    public final Object w(int i, Object obj, Object obj2) {
        int i2 = i - 1;
        if (i2 != 0) {
            if (i2 != 2) {
                if (i2 != 3) {
                    if (i2 != 4) {
                        if (i2 != 5) {
                            return null;
                        }
                        return zzn;
                    }
                    return new li5(null);
                }
                return new x0();
            }
            return x1.v(zzn, "\u0001\t\u0000\u0001\u0001\t\t\u0000\u0004\u0000\u0001ဂ\u0000\u0002ဈ\u0001\u0003င\u0002\u0004\u001b\u0005\u001b\u0006\u001b\u0007ဈ\u0003\bဇ\u0004\t\u001b", new Object[]{"zza", "zze", "zzf", "zzg", "zzh", y0.class, "zzi", w0.class, "zzj", q0.class, "zzk", "zzl", "zzm", m1.class});
        }
        return (byte) 1;
    }

    public final boolean x() {
        return (this.zza & 1) != 0;
    }

    public final long y() {
        return this.zze;
    }

    public final boolean z() {
        return (this.zza & 2) != 0;
    }
}
