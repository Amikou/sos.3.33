package com.google.android.gms.internal.measurement;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* loaded from: classes.dex */
public interface j extends IInterface {
    void beginAdUnitExposure(String str, long j) throws RemoteException;

    void clearConditionalUserProperty(String str, String str2, Bundle bundle) throws RemoteException;

    void clearMeasurementEnabled(long j) throws RemoteException;

    void endAdUnitExposure(String str, long j) throws RemoteException;

    void generateEventId(m mVar) throws RemoteException;

    void getAppInstanceId(m mVar) throws RemoteException;

    void getCachedAppInstanceId(m mVar) throws RemoteException;

    void getConditionalUserProperties(String str, String str2, m mVar) throws RemoteException;

    void getCurrentScreenClass(m mVar) throws RemoteException;

    void getCurrentScreenName(m mVar) throws RemoteException;

    void getGmpAppId(m mVar) throws RemoteException;

    void getMaxUserProperties(String str, m mVar) throws RemoteException;

    void getTestFlag(m mVar, int i) throws RemoteException;

    void getUserProperties(String str, String str2, boolean z, m mVar) throws RemoteException;

    void initForTests(Map map) throws RemoteException;

    void initialize(lm1 lm1Var, zzcl zzclVar, long j) throws RemoteException;

    void isDataCollectionEnabled(m mVar) throws RemoteException;

    void logEvent(String str, String str2, Bundle bundle, boolean z, boolean z2, long j) throws RemoteException;

    void logEventAndBundle(String str, String str2, Bundle bundle, m mVar, long j) throws RemoteException;

    void logHealthData(int i, String str, lm1 lm1Var, lm1 lm1Var2, lm1 lm1Var3) throws RemoteException;

    void onActivityCreated(lm1 lm1Var, Bundle bundle, long j) throws RemoteException;

    void onActivityDestroyed(lm1 lm1Var, long j) throws RemoteException;

    void onActivityPaused(lm1 lm1Var, long j) throws RemoteException;

    void onActivityResumed(lm1 lm1Var, long j) throws RemoteException;

    void onActivitySaveInstanceState(lm1 lm1Var, m mVar, long j) throws RemoteException;

    void onActivityStarted(lm1 lm1Var, long j) throws RemoteException;

    void onActivityStopped(lm1 lm1Var, long j) throws RemoteException;

    void performAction(Bundle bundle, m mVar, long j) throws RemoteException;

    void registerOnMeasurementEventListener(p pVar) throws RemoteException;

    void resetAnalyticsData(long j) throws RemoteException;

    void setConditionalUserProperty(Bundle bundle, long j) throws RemoteException;

    void setConsent(Bundle bundle, long j) throws RemoteException;

    void setConsentThirdParty(Bundle bundle, long j) throws RemoteException;

    void setCurrentScreen(lm1 lm1Var, String str, String str2, long j) throws RemoteException;

    void setDataCollectionEnabled(boolean z) throws RemoteException;

    void setDefaultEventParameters(Bundle bundle) throws RemoteException;

    void setEventInterceptor(p pVar) throws RemoteException;

    void setInstanceIdProvider(kb5 kb5Var) throws RemoteException;

    void setMeasurementEnabled(boolean z, long j) throws RemoteException;

    void setMinimumSessionDuration(long j) throws RemoteException;

    void setSessionTimeoutDuration(long j) throws RemoteException;

    void setUserId(String str, long j) throws RemoteException;

    void setUserProperty(String str, String str2, lm1 lm1Var, boolean z, long j) throws RemoteException;

    void unregisterOnMeasurementEventListener(p pVar) throws RemoteException;
}
