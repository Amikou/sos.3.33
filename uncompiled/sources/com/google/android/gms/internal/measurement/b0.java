package com.google.android.gms.internal.measurement;

import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@19.0.0 */
/* loaded from: classes.dex */
public final class b0 extends i0 {
    public final /* synthetic */ String i0;
    public final /* synthetic */ String j0;
    public final /* synthetic */ boolean k0;
    public final /* synthetic */ aa5 l0;
    public final /* synthetic */ wf5 m0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public b0(wf5 wf5Var, String str, String str2, boolean z, aa5 aa5Var) {
        super(wf5Var, true);
        this.m0 = wf5Var;
        this.i0 = str;
        this.j0 = str2;
        this.k0 = z;
        this.l0 = aa5Var;
    }

    @Override // com.google.android.gms.internal.measurement.i0
    public final void a() throws RemoteException {
        j jVar;
        jVar = this.m0.h;
        ((j) zt2.j(jVar)).getUserProperties(this.i0, this.j0, this.k0, this.l0);
    }

    @Override // com.google.android.gms.internal.measurement.i0
    public final void b() {
        this.l0.K0(null);
    }
}
