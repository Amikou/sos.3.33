package com.google.android.gms.internal.measurement;

import com.github.mikephil.charting.utils.Utils;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* loaded from: classes.dex */
public final class j1 extends x1<j1, il5> implements xx5 {
    private static final j1 zzk;
    private int zza;
    private long zze;
    private String zzf = "";
    private String zzg = "";
    private long zzh;
    private float zzi;
    private double zzj;

    static {
        j1 j1Var = new j1();
        zzk = j1Var;
        x1.u(j1.class, j1Var);
    }

    public static il5 G() {
        return zzk.r();
    }

    public static /* synthetic */ void I(j1 j1Var, long j) {
        j1Var.zza |= 1;
        j1Var.zze = j;
    }

    public static /* synthetic */ void J(j1 j1Var, String str) {
        str.getClass();
        j1Var.zza |= 2;
        j1Var.zzf = str;
    }

    public static /* synthetic */ void K(j1 j1Var, String str) {
        str.getClass();
        j1Var.zza |= 4;
        j1Var.zzg = str;
    }

    public static /* synthetic */ void L(j1 j1Var) {
        j1Var.zza &= -5;
        j1Var.zzg = zzk.zzg;
    }

    public static /* synthetic */ void M(j1 j1Var, long j) {
        j1Var.zza |= 8;
        j1Var.zzh = j;
    }

    public static /* synthetic */ void N(j1 j1Var) {
        j1Var.zza &= -9;
        j1Var.zzh = 0L;
    }

    public static /* synthetic */ void O(j1 j1Var, double d) {
        j1Var.zza |= 32;
        j1Var.zzj = d;
    }

    public static /* synthetic */ void P(j1 j1Var) {
        j1Var.zza &= -33;
        j1Var.zzj = Utils.DOUBLE_EPSILON;
    }

    public final boolean A() {
        return (this.zza & 4) != 0;
    }

    public final String B() {
        return this.zzg;
    }

    public final boolean C() {
        return (this.zza & 8) != 0;
    }

    public final long D() {
        return this.zzh;
    }

    public final boolean E() {
        return (this.zza & 32) != 0;
    }

    public final double F() {
        return this.zzj;
    }

    @Override // com.google.android.gms.internal.measurement.x1
    public final Object w(int i, Object obj, Object obj2) {
        int i2 = i - 1;
        if (i2 != 0) {
            if (i2 != 2) {
                if (i2 != 3) {
                    if (i2 != 4) {
                        if (i2 != 5) {
                            return null;
                        }
                        return zzk;
                    }
                    return new il5(null);
                }
                return new j1();
            }
            return x1.v(zzk, "\u0001\u0006\u0000\u0001\u0001\u0006\u0006\u0000\u0000\u0000\u0001ဂ\u0000\u0002ဈ\u0001\u0003ဈ\u0002\u0004ဂ\u0003\u0005ခ\u0004\u0006က\u0005", new Object[]{"zza", "zze", "zzf", "zzg", "zzh", "zzi", "zzj"});
        }
        return (byte) 1;
    }

    public final boolean x() {
        return (this.zza & 1) != 0;
    }

    public final long y() {
        return this.zze;
    }

    public final String z() {
        return this.zzf;
    }
}
