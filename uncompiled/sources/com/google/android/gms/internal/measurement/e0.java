package com.google.android.gms.internal.measurement;

import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@19.0.0 */
/* loaded from: classes.dex */
public final class e0 extends i0 {
    public final /* synthetic */ boolean i0;
    public final /* synthetic */ wf5 j0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public e0(wf5 wf5Var, boolean z) {
        super(wf5Var, true);
        this.j0 = wf5Var;
        this.i0 = z;
    }

    @Override // com.google.android.gms.internal.measurement.i0
    public final void a() throws RemoteException {
        j jVar;
        jVar = this.j0.h;
        ((j) zt2.j(jVar)).setDataCollectionEnabled(this.i0);
    }
}
