package com.google.android.gms.internal.measurement;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* loaded from: classes.dex */
public final class r1 extends ys5 {
    public int a;
    public int b;
    public int c;

    public /* synthetic */ r1(byte[] bArr, int i, int i2, boolean z, ts5 ts5Var) {
        super(null);
        this.c = Integer.MAX_VALUE;
        this.a = 0;
    }

    public final int c(int i) throws zzkn {
        int i2 = this.c;
        this.c = 0;
        int i3 = this.a + this.b;
        this.a = i3;
        if (i3 > 0) {
            this.b = i3;
            this.a = 0;
        } else {
            this.b = 0;
        }
        return i2;
    }
}
