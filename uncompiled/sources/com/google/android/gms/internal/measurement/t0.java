package com.google.android.gms.internal.measurement;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* loaded from: classes.dex */
public final class t0 extends x1<t0, ng5> implements xx5 {
    private static final t0 zzj;
    private int zza;
    private int zze;
    private boolean zzf;
    private String zzg = "";
    private String zzh = "";
    private String zzi = "";

    static {
        t0 t0Var = new t0();
        zzj = t0Var;
        x1.u(t0.class, t0Var);
    }

    public static t0 H() {
        return zzj;
    }

    public final boolean A() {
        return this.zzf;
    }

    public final boolean B() {
        return (this.zza & 4) != 0;
    }

    public final String C() {
        return this.zzg;
    }

    public final boolean D() {
        return (this.zza & 8) != 0;
    }

    public final String E() {
        return this.zzh;
    }

    public final boolean F() {
        return (this.zza & 16) != 0;
    }

    public final String G() {
        return this.zzi;
    }

    @Override // com.google.android.gms.internal.measurement.x1
    public final Object w(int i, Object obj, Object obj2) {
        int i2 = i - 1;
        if (i2 != 0) {
            if (i2 != 2) {
                if (i2 != 3) {
                    if (i2 != 4) {
                        if (i2 != 5) {
                            return null;
                        }
                        return zzj;
                    }
                    return new ng5(null);
                }
                return new t0();
            }
            return x1.v(zzj, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0000\u0000\u0001ဌ\u0000\u0002ဇ\u0001\u0003ဈ\u0002\u0004ဈ\u0003\u0005ဈ\u0004", new Object[]{"zza", "zze", zzep.zzb(), "zzf", "zzg", "zzh", "zzi"});
        }
        return (byte) 1;
    }

    public final boolean x() {
        return (this.zza & 1) != 0;
    }

    public final zzep y() {
        zzep zza = zzep.zza(this.zze);
        return zza == null ? zzep.UNKNOWN_COMPARISON_TYPE : zza;
    }

    public final boolean z() {
        return (this.zza & 2) != 0;
    }
}
