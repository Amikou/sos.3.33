package com.google.android.gms.internal.measurement;

/* JADX WARN: Enum visitor error
jadx.core.utils.exceptions.JadxRuntimeException: Init of enum zza uses external variables
	at jadx.core.dex.visitors.EnumVisitor.createEnumFieldByConstructor(EnumVisitor.java:444)
	at jadx.core.dex.visitors.EnumVisitor.processEnumFieldByRegister(EnumVisitor.java:391)
	at jadx.core.dex.visitors.EnumVisitor.extractEnumFieldsFromFilledArray(EnumVisitor.java:320)
	at jadx.core.dex.visitors.EnumVisitor.extractEnumFieldsFromInsn(EnumVisitor.java:258)
	at jadx.core.dex.visitors.EnumVisitor.convertToEnum(EnumVisitor.java:151)
	at jadx.core.dex.visitors.EnumVisitor.visit(EnumVisitor.java:100)
 */
/* JADX WARN: Failed to restore enum class, 'enum' modifier and super class removed */
/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* loaded from: classes.dex */
public final class zzjv {
    public static final zzjv[] a;
    public static final /* synthetic */ zzjv[] f0;
    public static final zzjv zzA;
    public static final zzjv zzB;
    public static final zzjv zzC;
    public static final zzjv zzD;
    public static final zzjv zzE;
    public static final zzjv zzF;
    public static final zzjv zzG;
    public static final zzjv zzH;
    public static final zzjv zzI;
    public static final zzjv zzJ;
    public static final zzjv zzK;
    public static final zzjv zzL;
    public static final zzjv zzM;
    public static final zzjv zzN;
    public static final zzjv zzO;
    public static final zzjv zzP;
    public static final zzjv zzQ;
    public static final zzjv zzR;
    public static final zzjv zzS;
    public static final zzjv zzT;
    public static final zzjv zzU;
    public static final zzjv zzV;
    public static final zzjv zzW;
    public static final zzjv zzX;
    public static final zzjv zzY;
    public static final zzjv zza;
    public static final zzjv zzb;
    public static final zzjv zzc;
    public static final zzjv zzd;
    public static final zzjv zze;
    public static final zzjv zzf;
    public static final zzjv zzg;
    public static final zzjv zzh;
    public static final zzjv zzi;
    public static final zzjv zzj;
    public static final zzjv zzk;
    public static final zzjv zzl;
    public static final zzjv zzm;
    public static final zzjv zzn;
    public static final zzjv zzo;
    public static final zzjv zzp;
    public static final zzjv zzq;
    public static final zzjv zzr;
    public static final zzjv zzs;
    public static final zzjv zzt;
    public static final zzjv zzu;
    public static final zzjv zzv;
    public static final zzjv zzw;
    public static final zzjv zzx;
    public static final zzjv zzy;
    public static final zzjv zzz;
    private final zzko zzZ;
    private final int zzaa;
    private final Class<?> zzab;

    static {
        zzko zzkoVar = zzko.zze;
        zzjv zzjvVar = new zzjv("DOUBLE", 0, 0, 1, zzkoVar);
        zza = zzjvVar;
        zzko zzkoVar2 = zzko.zzd;
        zzjv zzjvVar2 = new zzjv("FLOAT", 1, 1, 1, zzkoVar2);
        zzb = zzjvVar2;
        zzko zzkoVar3 = zzko.zzc;
        zzjv zzjvVar3 = new zzjv("INT64", 2, 2, 1, zzkoVar3);
        zzc = zzjvVar3;
        zzjv zzjvVar4 = new zzjv("UINT64", 3, 3, 1, zzkoVar3);
        zzd = zzjvVar4;
        zzko zzkoVar4 = zzko.zzb;
        zzjv zzjvVar5 = new zzjv("INT32", 4, 4, 1, zzkoVar4);
        zze = zzjvVar5;
        zzjv zzjvVar6 = new zzjv("FIXED64", 5, 5, 1, zzkoVar3);
        zzf = zzjvVar6;
        zzjv zzjvVar7 = new zzjv("FIXED32", 6, 6, 1, zzkoVar4);
        zzg = zzjvVar7;
        zzko zzkoVar5 = zzko.zzf;
        zzjv zzjvVar8 = new zzjv("BOOL", 7, 7, 1, zzkoVar5);
        zzh = zzjvVar8;
        zzko zzkoVar6 = zzko.zzg;
        zzjv zzjvVar9 = new zzjv("STRING", 8, 8, 1, zzkoVar6);
        zzi = zzjvVar9;
        zzko zzkoVar7 = zzko.zzj;
        zzjv zzjvVar10 = new zzjv("MESSAGE", 9, 9, 1, zzkoVar7);
        zzj = zzjvVar10;
        zzko zzkoVar8 = zzko.zzh;
        zzjv zzjvVar11 = new zzjv("BYTES", 10, 10, 1, zzkoVar8);
        zzk = zzjvVar11;
        zzjv zzjvVar12 = new zzjv("UINT32", 11, 11, 1, zzkoVar4);
        zzl = zzjvVar12;
        zzko zzkoVar9 = zzko.zzi;
        zzjv zzjvVar13 = new zzjv("ENUM", 12, 12, 1, zzkoVar9);
        zzm = zzjvVar13;
        zzjv zzjvVar14 = new zzjv("SFIXED32", 13, 13, 1, zzkoVar4);
        zzn = zzjvVar14;
        zzjv zzjvVar15 = new zzjv("SFIXED64", 14, 14, 1, zzkoVar3);
        zzo = zzjvVar15;
        zzjv zzjvVar16 = new zzjv("SINT32", 15, 15, 1, zzkoVar4);
        zzp = zzjvVar16;
        zzjv zzjvVar17 = new zzjv("SINT64", 16, 16, 1, zzkoVar3);
        zzq = zzjvVar17;
        zzjv zzjvVar18 = new zzjv("GROUP", 17, 17, 1, zzkoVar7);
        zzr = zzjvVar18;
        zzjv zzjvVar19 = new zzjv("DOUBLE_LIST", 18, 18, 2, zzkoVar);
        zzs = zzjvVar19;
        zzjv zzjvVar20 = new zzjv("FLOAT_LIST", 19, 19, 2, zzkoVar2);
        zzt = zzjvVar20;
        zzjv zzjvVar21 = new zzjv("INT64_LIST", 20, 20, 2, zzkoVar3);
        zzu = zzjvVar21;
        zzjv zzjvVar22 = new zzjv("UINT64_LIST", 21, 21, 2, zzkoVar3);
        zzv = zzjvVar22;
        zzjv zzjvVar23 = new zzjv("INT32_LIST", 22, 22, 2, zzkoVar4);
        zzw = zzjvVar23;
        zzjv zzjvVar24 = new zzjv("FIXED64_LIST", 23, 23, 2, zzkoVar3);
        zzx = zzjvVar24;
        zzjv zzjvVar25 = new zzjv("FIXED32_LIST", 24, 24, 2, zzkoVar4);
        zzy = zzjvVar25;
        zzjv zzjvVar26 = new zzjv("BOOL_LIST", 25, 25, 2, zzkoVar5);
        zzz = zzjvVar26;
        zzjv zzjvVar27 = new zzjv("STRING_LIST", 26, 26, 2, zzkoVar6);
        zzA = zzjvVar27;
        zzjv zzjvVar28 = new zzjv("MESSAGE_LIST", 27, 27, 2, zzkoVar7);
        zzB = zzjvVar28;
        zzjv zzjvVar29 = new zzjv("BYTES_LIST", 28, 28, 2, zzkoVar8);
        zzC = zzjvVar29;
        zzjv zzjvVar30 = new zzjv("UINT32_LIST", 29, 29, 2, zzkoVar4);
        zzD = zzjvVar30;
        zzjv zzjvVar31 = new zzjv("ENUM_LIST", 30, 30, 2, zzkoVar9);
        zzE = zzjvVar31;
        zzjv zzjvVar32 = new zzjv("SFIXED32_LIST", 31, 31, 2, zzkoVar4);
        zzF = zzjvVar32;
        zzjv zzjvVar33 = new zzjv("SFIXED64_LIST", 32, 32, 2, zzkoVar3);
        zzG = zzjvVar33;
        zzjv zzjvVar34 = new zzjv("SINT32_LIST", 33, 33, 2, zzkoVar4);
        zzH = zzjvVar34;
        zzjv zzjvVar35 = new zzjv("SINT64_LIST", 34, 34, 2, zzkoVar3);
        zzI = zzjvVar35;
        zzjv zzjvVar36 = new zzjv("DOUBLE_LIST_PACKED", 35, 35, 3, zzkoVar);
        zzJ = zzjvVar36;
        zzjv zzjvVar37 = new zzjv("FLOAT_LIST_PACKED", 36, 36, 3, zzkoVar2);
        zzK = zzjvVar37;
        zzjv zzjvVar38 = new zzjv("INT64_LIST_PACKED", 37, 37, 3, zzkoVar3);
        zzL = zzjvVar38;
        zzjv zzjvVar39 = new zzjv("UINT64_LIST_PACKED", 38, 38, 3, zzkoVar3);
        zzM = zzjvVar39;
        zzjv zzjvVar40 = new zzjv("INT32_LIST_PACKED", 39, 39, 3, zzkoVar4);
        zzN = zzjvVar40;
        zzjv zzjvVar41 = new zzjv("FIXED64_LIST_PACKED", 40, 40, 3, zzkoVar3);
        zzO = zzjvVar41;
        zzjv zzjvVar42 = new zzjv("FIXED32_LIST_PACKED", 41, 41, 3, zzkoVar4);
        zzP = zzjvVar42;
        zzjv zzjvVar43 = new zzjv("BOOL_LIST_PACKED", 42, 42, 3, zzkoVar5);
        zzQ = zzjvVar43;
        zzjv zzjvVar44 = new zzjv("UINT32_LIST_PACKED", 43, 43, 3, zzkoVar4);
        zzR = zzjvVar44;
        zzjv zzjvVar45 = new zzjv("ENUM_LIST_PACKED", 44, 44, 3, zzkoVar9);
        zzS = zzjvVar45;
        zzjv zzjvVar46 = new zzjv("SFIXED32_LIST_PACKED", 45, 45, 3, zzkoVar4);
        zzT = zzjvVar46;
        zzjv zzjvVar47 = new zzjv("SFIXED64_LIST_PACKED", 46, 46, 3, zzkoVar3);
        zzU = zzjvVar47;
        zzjv zzjvVar48 = new zzjv("SINT32_LIST_PACKED", 47, 47, 3, zzkoVar4);
        zzV = zzjvVar48;
        zzjv zzjvVar49 = new zzjv("SINT64_LIST_PACKED", 48, 48, 3, zzkoVar3);
        zzW = zzjvVar49;
        zzjv zzjvVar50 = new zzjv("GROUP_LIST", 49, 49, 2, zzkoVar7);
        zzX = zzjvVar50;
        zzjv zzjvVar51 = new zzjv("MAP", 50, 50, 4, zzko.zza);
        zzY = zzjvVar51;
        f0 = new zzjv[]{zzjvVar, zzjvVar2, zzjvVar3, zzjvVar4, zzjvVar5, zzjvVar6, zzjvVar7, zzjvVar8, zzjvVar9, zzjvVar10, zzjvVar11, zzjvVar12, zzjvVar13, zzjvVar14, zzjvVar15, zzjvVar16, zzjvVar17, zzjvVar18, zzjvVar19, zzjvVar20, zzjvVar21, zzjvVar22, zzjvVar23, zzjvVar24, zzjvVar25, zzjvVar26, zzjvVar27, zzjvVar28, zzjvVar29, zzjvVar30, zzjvVar31, zzjvVar32, zzjvVar33, zzjvVar34, zzjvVar35, zzjvVar36, zzjvVar37, zzjvVar38, zzjvVar39, zzjvVar40, zzjvVar41, zzjvVar42, zzjvVar43, zzjvVar44, zzjvVar45, zzjvVar46, zzjvVar47, zzjvVar48, zzjvVar49, zzjvVar50, zzjvVar51};
        zzjv[] values = values();
        a = new zzjv[values.length];
        for (zzjv zzjvVar52 : values) {
            a[zzjvVar52.zzaa] = zzjvVar52;
        }
    }

    public zzjv(String str, int i, int i2, int i3, zzko zzkoVar) {
        this.zzaa = i2;
        this.zzZ = zzkoVar;
        zzko zzkoVar2 = zzko.zza;
        int i4 = i3 - 1;
        if (i4 == 1) {
            this.zzab = zzkoVar.zza();
        } else if (i4 != 3) {
            this.zzab = null;
        } else {
            this.zzab = zzkoVar.zza();
        }
        if (i3 == 1) {
            zzkoVar.ordinal();
        }
    }

    public static zzjv[] values() {
        return (zzjv[]) f0.clone();
    }

    public final int zza() {
        return this.zzaa;
    }
}
