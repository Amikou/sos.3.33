package com.google.android.gms.internal.measurement;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* loaded from: classes.dex */
public interface p extends IInterface {
    int c() throws RemoteException;

    void y(String str, String str2, Bundle bundle, long j) throws RemoteException;
}
