package com.google.android.gms.internal.measurement;

import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@19.0.0 */
/* loaded from: classes.dex */
public final class y extends i0 {
    public final /* synthetic */ aa5 i0;
    public final /* synthetic */ wf5 j0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public y(wf5 wf5Var, aa5 aa5Var) {
        super(wf5Var, true);
        this.j0 = wf5Var;
        this.i0 = aa5Var;
    }

    @Override // com.google.android.gms.internal.measurement.i0
    public final void a() throws RemoteException {
        j jVar;
        jVar = this.j0.h;
        ((j) zt2.j(jVar)).generateEventId(this.i0);
    }

    @Override // com.google.android.gms.internal.measurement.i0
    public final void b() {
        this.i0.K0(null);
    }
}
