package com.google.android.gms.internal.measurement;

import android.app.Activity;
import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@19.0.0 */
/* loaded from: classes.dex */
public final class t extends i0 {
    public final /* synthetic */ Activity i0;
    public final /* synthetic */ String j0;
    public final /* synthetic */ String k0;
    public final /* synthetic */ wf5 l0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public t(wf5 wf5Var, Activity activity, String str, String str2) {
        super(wf5Var, true);
        this.l0 = wf5Var;
        this.i0 = activity;
        this.j0 = str;
        this.k0 = str2;
    }

    @Override // com.google.android.gms.internal.measurement.i0
    public final void a() throws RemoteException {
        j jVar;
        jVar = this.l0.h;
        ((j) zt2.j(jVar)).setCurrentScreen(nl2.H1(this.i0), this.j0, this.k0, this.a);
    }
}
