package com.google.android.gms.internal.measurement;

import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* loaded from: classes.dex */
public interface z1 extends xx5 {
    y1 a();

    y1 b();

    zzjd d();

    int e();

    void f(t1 t1Var) throws IOException;
}
