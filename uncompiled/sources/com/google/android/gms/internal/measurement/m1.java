package com.google.android.gms.internal.measurement;

import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* loaded from: classes.dex */
public final class m1 extends x1<m1, rl5> implements xx5 {
    private static final m1 zzg;
    private int zza;
    private zv5<n1> zze = x1.o();
    private k1 zzf;

    static {
        m1 m1Var = new m1();
        zzg = m1Var;
        x1.u(m1.class, m1Var);
    }

    @Override // com.google.android.gms.internal.measurement.x1
    public final Object w(int i, Object obj, Object obj2) {
        int i2 = i - 1;
        if (i2 != 0) {
            if (i2 != 2) {
                if (i2 != 3) {
                    if (i2 != 4) {
                        if (i2 != 5) {
                            return null;
                        }
                        return zzg;
                    }
                    return new rl5(null);
                }
                return new m1();
            }
            return x1.v(zzg, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0001\u0000\u0001\u001b\u0002ဉ\u0000", new Object[]{"zza", "zze", n1.class, "zzf"});
        }
        return (byte) 1;
    }

    public final List<n1> x() {
        return this.zze;
    }

    public final k1 y() {
        k1 k1Var = this.zzf;
        return k1Var == null ? k1.z() : k1Var;
    }
}
