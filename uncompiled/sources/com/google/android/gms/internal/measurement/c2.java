package com.google.android.gms.internal.measurement;

import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* loaded from: classes.dex */
public interface c2<T> {
    int a(T t);

    boolean c(T t, T t2);

    int d(T t);

    boolean e(T t);

    void f(T t);

    void g(T t, u1 u1Var) throws IOException;

    void h(T t, T t2);

    void i(T t, byte[] bArr, int i, int i2, fr5 fr5Var) throws IOException;

    T zza();
}
