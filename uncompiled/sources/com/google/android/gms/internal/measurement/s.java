package com.google.android.gms.internal.measurement;

import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@19.0.0 */
/* loaded from: classes.dex */
public final class s extends i0 {
    public final /* synthetic */ String i0;
    public final /* synthetic */ String j0;
    public final /* synthetic */ aa5 k0;
    public final /* synthetic */ wf5 l0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public s(wf5 wf5Var, String str, String str2, aa5 aa5Var) {
        super(wf5Var, true);
        this.l0 = wf5Var;
        this.i0 = str;
        this.j0 = str2;
        this.k0 = aa5Var;
    }

    @Override // com.google.android.gms.internal.measurement.i0
    public final void a() throws RemoteException {
        j jVar;
        jVar = this.l0.h;
        ((j) zt2.j(jVar)).getConditionalUserProperties(this.i0, this.j0, this.k0);
    }

    @Override // com.google.android.gms.internal.measurement.i0
    public final void b() {
        this.k0.K0(null);
    }
}
