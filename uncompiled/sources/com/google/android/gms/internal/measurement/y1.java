package com.google.android.gms.internal.measurement;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* loaded from: classes.dex */
public interface y1 extends Cloneable, xx5 {
    y1 H0(byte[] bArr) throws zzkn;

    y1 J0(byte[] bArr, qt5 qt5Var) throws zzkn;

    y1 i0(z1 z1Var);

    z1 q();
}
