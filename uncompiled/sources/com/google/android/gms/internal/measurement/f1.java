package com.google.android.gms.internal.measurement;

import androidx.recyclerview.widget.RecyclerView;
import java.util.List;
import okhttp3.internal.http2.Http2;
import okhttp3.internal.http2.Http2Connection;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* loaded from: classes.dex */
public final class f1 extends x1<f1, dk5> implements xx5 {
    public static final /* synthetic */ int zza = 0;
    private static final f1 zzaa;
    private long zzB;
    private int zzC;
    private boolean zzF;
    private int zzI;
    private int zzJ;
    private int zzK;
    private long zzM;
    private long zzN;
    private int zzQ;
    private g1 zzS;
    private long zzU;
    private long zzV;
    private int zzY;
    private boolean zzZ;
    private int zze;
    private int zzf;
    private int zzg;
    private long zzj;
    private long zzk;
    private long zzl;
    private long zzm;
    private long zzn;
    private int zzs;
    private long zzw;
    private long zzx;
    private boolean zzz;
    private zv5<b1> zzh = x1.o();
    private zv5<j1> zzi = x1.o();
    private String zzo = "";
    private String zzp = "";
    private String zzq = "";
    private String zzr = "";
    private String zzt = "";
    private String zzu = "";
    private String zzv = "";
    private String zzy = "";
    private String zzA = "";
    private String zzD = "";
    private String zzE = "";
    private zv5<z0> zzG = x1.o();
    private String zzH = "";
    private String zzL = "";
    private String zzO = "";
    private String zzP = "";
    private String zzR = "";
    private vv5 zzT = x1.l();
    private String zzW = "";
    private String zzX = "";

    static {
        f1 f1Var = new f1();
        zzaa = f1Var;
        x1.u(f1.class, f1Var);
    }

    public static dk5 K0() {
        return zzaa.r();
    }

    public static /* synthetic */ void M0(f1 f1Var, int i) {
        f1Var.zze |= 1;
        f1Var.zzg = 1;
    }

    public static /* synthetic */ void N0(f1 f1Var, int i, b1 b1Var) {
        b1Var.getClass();
        f1Var.c1();
        f1Var.zzh.set(i, b1Var);
    }

    public static /* synthetic */ void O0(f1 f1Var, b1 b1Var) {
        b1Var.getClass();
        f1Var.c1();
        f1Var.zzh.add(b1Var);
    }

    public static /* synthetic */ void P0(f1 f1Var, Iterable iterable) {
        f1Var.c1();
        zq5.j(iterable, f1Var.zzh);
    }

    public static /* synthetic */ void R0(f1 f1Var, int i) {
        f1Var.c1();
        f1Var.zzh.remove(i);
    }

    public static /* synthetic */ void S0(f1 f1Var, int i, j1 j1Var) {
        j1Var.getClass();
        f1Var.d1();
        f1Var.zzi.set(i, j1Var);
    }

    public static /* synthetic */ void T0(f1 f1Var, j1 j1Var) {
        j1Var.getClass();
        f1Var.d1();
        f1Var.zzi.add(j1Var);
    }

    public static /* synthetic */ void U0(f1 f1Var, Iterable iterable) {
        f1Var.d1();
        zq5.j(iterable, f1Var.zzi);
    }

    public static /* synthetic */ void V0(f1 f1Var, int i) {
        f1Var.d1();
        f1Var.zzi.remove(i);
    }

    public static /* synthetic */ void W0(f1 f1Var, long j) {
        f1Var.zze |= 2;
        f1Var.zzj = j;
    }

    public static /* synthetic */ void X0(f1 f1Var, long j) {
        f1Var.zze |= 4;
        f1Var.zzk = j;
    }

    public static /* synthetic */ void Y(f1 f1Var, long j) {
        f1Var.zze |= 32;
        f1Var.zzn = j;
    }

    public static /* synthetic */ void Y0(f1 f1Var, long j) {
        f1Var.zze |= 8;
        f1Var.zzl = j;
    }

    public static /* synthetic */ void Z(f1 f1Var) {
        f1Var.zze &= -33;
        f1Var.zzn = 0L;
    }

    public static /* synthetic */ void Z0(f1 f1Var, long j) {
        f1Var.zze |= 16;
        f1Var.zzm = j;
    }

    public static /* synthetic */ void a0(f1 f1Var, String str) {
        f1Var.zze |= 64;
        f1Var.zzo = "android";
    }

    public static /* synthetic */ void a1(f1 f1Var) {
        f1Var.zze &= -17;
        f1Var.zzm = 0L;
    }

    public static /* synthetic */ void b0(f1 f1Var, String str) {
        str.getClass();
        f1Var.zze |= 128;
        f1Var.zzp = str;
    }

    public static /* synthetic */ void c0(f1 f1Var, String str) {
        str.getClass();
        f1Var.zze |= 256;
        f1Var.zzq = str;
    }

    public static /* synthetic */ void d0(f1 f1Var, String str) {
        str.getClass();
        f1Var.zze |= RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN;
        f1Var.zzr = str;
    }

    public static /* synthetic */ void e0(f1 f1Var, int i) {
        f1Var.zze |= RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE;
        f1Var.zzs = i;
    }

    public static /* synthetic */ void e1(f1 f1Var, String str) {
        str.getClass();
        f1Var.zze |= Http2Connection.OKHTTP_CLIENT_WINDOW_SIZE;
        f1Var.zzH = str;
    }

    public static /* synthetic */ void f0(f1 f1Var, String str) {
        str.getClass();
        f1Var.zze |= 2048;
        f1Var.zzt = str;
    }

    public static /* synthetic */ void f1(f1 f1Var, int i) {
        f1Var.zze |= 33554432;
        f1Var.zzI = i;
    }

    public static /* synthetic */ void g0(f1 f1Var, String str) {
        str.getClass();
        f1Var.zze |= 4096;
        f1Var.zzu = str;
    }

    public static /* synthetic */ void g1(f1 f1Var) {
        f1Var.zze &= -268435457;
        f1Var.zzL = zzaa.zzL;
    }

    public static /* synthetic */ void h0(f1 f1Var, String str) {
        str.getClass();
        f1Var.zze |= 8192;
        f1Var.zzv = str;
    }

    public static /* synthetic */ void h1(f1 f1Var, long j) {
        f1Var.zze |= 536870912;
        f1Var.zzM = j;
    }

    public static /* synthetic */ void i1(f1 f1Var, long j) {
        f1Var.zze |= 1073741824;
        f1Var.zzN = j;
    }

    public static /* synthetic */ void j0(f1 f1Var, long j) {
        f1Var.zze |= Http2.INITIAL_MAX_FRAME_SIZE;
        f1Var.zzw = j;
    }

    public static /* synthetic */ void j1(f1 f1Var) {
        f1Var.zze &= Integer.MAX_VALUE;
        f1Var.zzO = zzaa.zzO;
    }

    public static /* synthetic */ void k0(f1 f1Var, long j) {
        f1Var.zze |= 32768;
        f1Var.zzx = 42004L;
    }

    public static /* synthetic */ void k1(f1 f1Var, int i) {
        f1Var.zzf |= 2;
        f1Var.zzQ = i;
    }

    public static /* synthetic */ void l0(f1 f1Var, String str) {
        str.getClass();
        f1Var.zze |= 65536;
        f1Var.zzy = str;
    }

    public static /* synthetic */ void l1(f1 f1Var, String str) {
        str.getClass();
        f1Var.zzf |= 4;
        f1Var.zzR = str;
    }

    public static /* synthetic */ void m0(f1 f1Var) {
        f1Var.zze &= -65537;
        f1Var.zzy = zzaa.zzy;
    }

    public static /* synthetic */ void m1(f1 f1Var, g1 g1Var) {
        g1Var.getClass();
        f1Var.zzS = g1Var;
        f1Var.zzf |= 8;
    }

    public static /* synthetic */ void n0(f1 f1Var, boolean z) {
        f1Var.zze |= 131072;
        f1Var.zzz = z;
    }

    public static /* synthetic */ void n1(f1 f1Var, Iterable iterable) {
        vv5 vv5Var = f1Var.zzT;
        if (!vv5Var.zza()) {
            int size = vv5Var.size();
            f1Var.zzT = vv5Var.g0(size == 0 ? 10 : size + size);
        }
        zq5.j(iterable, f1Var.zzT);
    }

    public static /* synthetic */ void o0(f1 f1Var) {
        f1Var.zze &= -131073;
        f1Var.zzz = false;
    }

    public static /* synthetic */ void o1(f1 f1Var, long j) {
        f1Var.zzf |= 16;
        f1Var.zzU = j;
    }

    public static /* synthetic */ void p0(f1 f1Var, String str) {
        str.getClass();
        f1Var.zze |= 262144;
        f1Var.zzA = str;
    }

    public static /* synthetic */ void p1(f1 f1Var, long j) {
        f1Var.zzf |= 32;
        f1Var.zzV = j;
    }

    public static /* synthetic */ void q0(f1 f1Var) {
        f1Var.zze &= -262145;
        f1Var.zzA = zzaa.zzA;
    }

    public static /* synthetic */ void q1(f1 f1Var, String str) {
        str.getClass();
        f1Var.zzf |= 64;
        f1Var.zzW = str;
    }

    public static /* synthetic */ void r0(f1 f1Var, long j) {
        f1Var.zze |= 524288;
        f1Var.zzB = j;
    }

    public static /* synthetic */ void r1(f1 f1Var, String str) {
        str.getClass();
        f1Var.zzf |= 128;
        f1Var.zzX = str;
    }

    public static /* synthetic */ void s0(f1 f1Var, int i) {
        f1Var.zze |= 1048576;
        f1Var.zzC = i;
    }

    public static /* synthetic */ void t0(f1 f1Var, String str) {
        f1Var.zze |= 2097152;
        f1Var.zzD = str;
    }

    public static /* synthetic */ void u0(f1 f1Var) {
        f1Var.zze &= -2097153;
        f1Var.zzD = zzaa.zzD;
    }

    public static /* synthetic */ void v0(f1 f1Var, String str) {
        str.getClass();
        f1Var.zze |= 4194304;
        f1Var.zzE = str;
    }

    public static /* synthetic */ void w0(f1 f1Var, boolean z) {
        f1Var.zze |= 8388608;
        f1Var.zzF = z;
    }

    public static /* synthetic */ void x0(f1 f1Var, Iterable iterable) {
        zv5<z0> zv5Var = f1Var.zzG;
        if (!zv5Var.zza()) {
            f1Var.zzG = x1.p(zv5Var);
        }
        zq5.j(iterable, f1Var.zzG);
    }

    public final long A() {
        return this.zzw;
    }

    public final boolean A0() {
        return (this.zzf & 2) != 0;
    }

    public final boolean A1() {
        return (this.zze & 4) != 0;
    }

    public final boolean B() {
        return (this.zze & 32768) != 0;
    }

    public final int B0() {
        return this.zzQ;
    }

    public final long B1() {
        return this.zzk;
    }

    public final long C() {
        return this.zzx;
    }

    public final String C0() {
        return this.zzR;
    }

    public final boolean C1() {
        return (this.zze & 8) != 0;
    }

    public final String D() {
        return this.zzy;
    }

    public final boolean D0() {
        return (this.zzf & 16) != 0;
    }

    public final long D1() {
        return this.zzl;
    }

    public final boolean E() {
        return (this.zze & 131072) != 0;
    }

    public final long E0() {
        return this.zzU;
    }

    public final boolean E1() {
        return (this.zze & 16) != 0;
    }

    public final boolean F() {
        return this.zzz;
    }

    public final String F0() {
        return this.zzW;
    }

    public final long F1() {
        return this.zzm;
    }

    public final String G() {
        return this.zzA;
    }

    public final boolean G0() {
        return (this.zzf & 128) != 0;
    }

    public final boolean G1() {
        return (this.zze & 32) != 0;
    }

    public final boolean H() {
        return (this.zze & 524288) != 0;
    }

    public final long H1() {
        return this.zzn;
    }

    public final long I() {
        return this.zzB;
    }

    public final String I0() {
        return this.zzX;
    }

    public final String I1() {
        return this.zzo;
    }

    public final boolean J() {
        return (this.zze & 1048576) != 0;
    }

    public final String J1() {
        return this.zzp;
    }

    public final int K() {
        return this.zzC;
    }

    public final String K1() {
        return this.zzq;
    }

    public final String L() {
        return this.zzD;
    }

    public final String L1() {
        return this.zzr;
    }

    public final String M() {
        return this.zzE;
    }

    public final boolean M1() {
        return (this.zze & RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE) != 0;
    }

    public final boolean N() {
        return (this.zze & 8388608) != 0;
    }

    public final int N1() {
        return this.zzs;
    }

    public final boolean O() {
        return this.zzF;
    }

    public final String O1() {
        return this.zzt;
    }

    public final List<z0> P() {
        return this.zzG;
    }

    public final String Q() {
        return this.zzH;
    }

    public final boolean R() {
        return (this.zze & 33554432) != 0;
    }

    public final int S() {
        return this.zzI;
    }

    public final boolean T() {
        return (this.zze & 536870912) != 0;
    }

    public final long U() {
        return this.zzM;
    }

    public final boolean V() {
        return (this.zze & 1073741824) != 0;
    }

    public final long W() {
        return this.zzN;
    }

    public final boolean X() {
        return (this.zze & 1) != 0;
    }

    public final int b1() {
        return this.zzg;
    }

    public final void c1() {
        zv5<b1> zv5Var = this.zzh;
        if (zv5Var.zza()) {
            return;
        }
        this.zzh = x1.p(zv5Var);
    }

    public final void d1() {
        zv5<j1> zv5Var = this.zzi;
        if (zv5Var.zza()) {
            return;
        }
        this.zzi = x1.p(zv5Var);
    }

    public final List<b1> s1() {
        return this.zzh;
    }

    public final int t1() {
        return this.zzh.size();
    }

    public final b1 u1(int i) {
        return this.zzh.get(i);
    }

    public final List<j1> v1() {
        return this.zzi;
    }

    @Override // com.google.android.gms.internal.measurement.x1
    public final Object w(int i, Object obj, Object obj2) {
        int i2 = i - 1;
        if (i2 != 0) {
            if (i2 != 2) {
                if (i2 != 3) {
                    if (i2 != 4) {
                        if (i2 != 5) {
                            return null;
                        }
                        return zzaa;
                    }
                    return new dk5(null);
                }
                return new f1();
            }
            return x1.v(zzaa, "\u0001.\u0000\u0002\u00017.\u0000\u0004\u0000\u0001င\u0000\u0002\u001b\u0003\u001b\u0004ဂ\u0001\u0005ဂ\u0002\u0006ဂ\u0003\u0007ဂ\u0005\bဈ\u0006\tဈ\u0007\nဈ\b\u000bဈ\t\fင\n\rဈ\u000b\u000eဈ\f\u0010ဈ\r\u0011ဂ\u000e\u0012ဂ\u000f\u0013ဈ\u0010\u0014ဇ\u0011\u0015ဈ\u0012\u0016ဂ\u0013\u0017င\u0014\u0018ဈ\u0015\u0019ဈ\u0016\u001aဂ\u0004\u001cဇ\u0017\u001d\u001b\u001eဈ\u0018\u001fင\u0019 င\u001a!င\u001b\"ဈ\u001c#ဂ\u001d$ဂ\u001e%ဈ\u001f&ဈ 'င!)ဈ\",ဉ#-\u001d.ဂ$/ဂ%2ဈ&4ဈ'5ဌ(7ဇ)", new Object[]{"zze", "zzf", "zzg", "zzh", b1.class, "zzi", j1.class, "zzj", "zzk", "zzl", "zzn", "zzo", "zzp", "zzq", "zzr", "zzs", "zzt", "zzu", "zzv", "zzw", "zzx", "zzy", "zzz", "zzA", "zzB", "zzC", "zzD", "zzE", "zzm", "zzF", "zzG", z0.class, "zzH", "zzI", "zzJ", "zzK", "zzL", "zzM", "zzN", "zzO", "zzP", "zzQ", "zzR", "zzS", "zzT", "zzU", "zzV", "zzW", "zzX", "zzY", zzfi.zzb(), "zzZ"});
        }
        return (byte) 1;
    }

    public final int w1() {
        return this.zzi.size();
    }

    public final String x() {
        return this.zzu;
    }

    public final j1 x1(int i) {
        return this.zzi.get(i);
    }

    public final String y() {
        return this.zzv;
    }

    public final boolean y1() {
        return (this.zze & 2) != 0;
    }

    public final boolean z() {
        return (this.zze & Http2.INITIAL_MAX_FRAME_SIZE) != 0;
    }

    public final String z0() {
        return this.zzO;
    }

    public final long z1() {
        return this.zzj;
    }
}
