package com.google.android.gms.internal.measurement;

import java.util.HashMap;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* loaded from: classes.dex */
public final class a {
    public String a;
    public final long b;
    public final Map<String, Object> c;

    public a(String str, long j, Map<String, Object> map) {
        this.a = str;
        this.b = j;
        HashMap hashMap = new HashMap();
        this.c = hashMap;
        if (map != null) {
            hashMap.putAll(map);
        }
    }

    public final long a() {
        return this.b;
    }

    public final String b() {
        return this.a;
    }

    public final void c(String str) {
        this.a = str;
    }

    public final void d(String str, Object obj) {
        if (obj == null) {
            this.c.remove(str);
        } else {
            this.c.put(str, obj);
        }
    }

    public final Object e(String str) {
        if (this.c.containsKey(str)) {
            return this.c.get(str);
        }
        return null;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof a) {
            a aVar = (a) obj;
            if (this.b == aVar.b && this.a.equals(aVar.a)) {
                return this.c.equals(aVar.c);
            }
            return false;
        }
        return false;
    }

    public final Map<String, Object> f() {
        return this.c;
    }

    /* renamed from: g */
    public final a clone() {
        return new a(this.a, this.b, new HashMap(this.c));
    }

    public final int hashCode() {
        int hashCode = this.a.hashCode();
        long j = this.b;
        return (((hashCode * 31) + ((int) (j ^ (j >>> 32)))) * 31) + this.c.hashCode();
    }

    public final String toString() {
        String str = this.a;
        long j = this.b;
        String valueOf = String.valueOf(this.c);
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 55 + valueOf.length());
        sb.append("Event{name='");
        sb.append(str);
        sb.append("', timestamp=");
        sb.append(j);
        sb.append(", params=");
        sb.append(valueOf);
        sb.append('}');
        return sb.toString();
    }
}
