package com.google.android.gms.internal.clearcut;

import defpackage.ta5;
import java.io.IOException;
import java.util.Map;

/* loaded from: classes.dex */
public abstract class j<T extends ta5<T>> {
    public abstract int a(Map.Entry<?, ?> entry);

    public abstract l<T> b(Object obj);

    public abstract void c(a0 a0Var, Map.Entry<?, ?> entry) throws IOException;

    public abstract void d(Object obj, l<T> lVar);

    public abstract l<T> e(Object obj);

    public abstract void f(Object obj);

    public abstract boolean g(o oVar);
}
