package com.google.android.gms.internal.clearcut;

import com.google.android.gms.internal.clearcut.zzge$zzv$zzb;

/* loaded from: classes.dex */
public enum zzge$zzv$zzb implements jb5 {
    DEFAULT(0),
    UNMETERED_ONLY(1),
    UNMETERED_OR_DAILY(2),
    FAST_IF_RADIO_AWAKE(3),
    NEVER(4);
    
    public static final mb5<zzge$zzv$zzb> i0 = new mb5<zzge$zzv$zzb>() { // from class: zl5
        @Override // defpackage.mb5
        public final /* synthetic */ zzge$zzv$zzb h(int i) {
            return zzge$zzv$zzb.zzbc(i);
        }
    };
    private final int value;

    zzge$zzv$zzb(int i) {
        this.value = i;
    }

    public static zzge$zzv$zzb zzbc(int i) {
        if (i != 0) {
            if (i != 1) {
                if (i != 2) {
                    if (i != 3) {
                        if (i != 4) {
                            return null;
                        }
                        return NEVER;
                    }
                    return FAST_IF_RADIO_AWAKE;
                }
                return UNMETERED_OR_DAILY;
            }
            return UNMETERED_ONLY;
        }
        return DEFAULT;
    }

    public static mb5<zzge$zzv$zzb> zzd() {
        return i0;
    }

    @Override // defpackage.jb5
    public final int zzc() {
        return this.value;
    }
}
