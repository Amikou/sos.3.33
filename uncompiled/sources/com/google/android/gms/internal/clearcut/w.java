package com.google.android.gms.internal.clearcut;

import com.google.android.gms.internal.clearcut.m;
import java.io.IOException;
import java.util.Arrays;

/* loaded from: classes.dex */
public final class w {
    public static final w f = new w(0, new int[0], new Object[0], false);
    public int a;
    public int[] b;
    public Object[] c;
    public int d;
    public boolean e;

    public w() {
        this(0, new int[8], new Object[8], true);
    }

    public w(int i, int[] iArr, Object[] objArr, boolean z) {
        this.d = -1;
        this.a = i;
        this.b = iArr;
        this.c = objArr;
        this.e = z;
    }

    public static w a(w wVar, w wVar2) {
        int i = wVar.a + wVar2.a;
        int[] copyOf = Arrays.copyOf(wVar.b, i);
        System.arraycopy(wVar2.b, 0, copyOf, wVar.a, wVar2.a);
        Object[] copyOf2 = Arrays.copyOf(wVar.c, i);
        System.arraycopy(wVar2.c, 0, copyOf2, wVar.a, wVar2.a);
        return new w(i, copyOf, copyOf2, true);
    }

    public static void f(int i, Object obj, a0 a0Var) throws IOException {
        int i2 = i >>> 3;
        int i3 = i & 7;
        if (i3 == 0) {
            a0Var.K(i2, ((Long) obj).longValue());
        } else if (i3 == 1) {
            a0Var.g(i2, ((Long) obj).longValue());
        } else if (i3 == 2) {
            a0Var.M(i2, (zzbb) obj);
        } else if (i3 != 3) {
            if (i3 != 5) {
                throw new RuntimeException(zzco.zzbn());
            }
            a0Var.u(i2, ((Integer) obj).intValue());
        } else if (a0Var.Q() == m.e.k) {
            a0Var.O(i2);
            ((w) obj).g(a0Var);
            a0Var.J(i2);
        } else {
            a0Var.J(i2);
            ((w) obj).g(a0Var);
            a0Var.O(i2);
        }
    }

    public static w h() {
        return f;
    }

    public static w i() {
        return new w();
    }

    public final void b(a0 a0Var) throws IOException {
        if (a0Var.Q() == m.e.l) {
            for (int i = this.a - 1; i >= 0; i--) {
                a0Var.j(this.b[i] >>> 3, this.c[i]);
            }
            return;
        }
        for (int i2 = 0; i2 < this.a; i2++) {
            a0Var.j(this.b[i2] >>> 3, this.c[i2]);
        }
    }

    public final void c(StringBuilder sb, int i) {
        for (int i2 = 0; i2 < this.a; i2++) {
            p.c(sb, i, String.valueOf(this.b[i2] >>> 3), this.c[i2]);
        }
    }

    public final int d() {
        int d0;
        int i = this.d;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < this.a; i3++) {
            int i4 = this.b[i3];
            int i5 = i4 >>> 3;
            int i6 = i4 & 7;
            if (i6 == 0) {
                d0 = zzbn.d0(i5, ((Long) this.c[i3]).longValue());
            } else if (i6 == 1) {
                d0 = zzbn.k0(i5, ((Long) this.c[i3]).longValue());
            } else if (i6 == 2) {
                d0 = zzbn.N(i5, (zzbb) this.c[i3]);
            } else if (i6 == 3) {
                d0 = (zzbn.B0(i5) << 1) + ((w) this.c[i3]).d();
            } else if (i6 != 5) {
                throw new IllegalStateException(zzco.zzbn());
            } else {
                d0 = zzbn.t0(i5, ((Integer) this.c[i3]).intValue());
            }
            i2 += d0;
        }
        this.d = i2;
        return i2;
    }

    public final void e(int i, Object obj) {
        if (!this.e) {
            throw new UnsupportedOperationException();
        }
        int i2 = this.a;
        int[] iArr = this.b;
        if (i2 == iArr.length) {
            int i3 = i2 + (i2 < 4 ? 8 : i2 >> 1);
            this.b = Arrays.copyOf(iArr, i3);
            this.c = Arrays.copyOf(this.c, i3);
        }
        int[] iArr2 = this.b;
        int i4 = this.a;
        iArr2[i4] = i;
        this.c[i4] = obj;
        this.a = i4 + 1;
    }

    public final boolean equals(Object obj) {
        boolean z;
        boolean z2;
        if (this == obj) {
            return true;
        }
        if (obj != null && (obj instanceof w)) {
            w wVar = (w) obj;
            int i = this.a;
            if (i == wVar.a) {
                int[] iArr = this.b;
                int[] iArr2 = wVar.b;
                int i2 = 0;
                while (true) {
                    if (i2 >= i) {
                        z = true;
                        break;
                    } else if (iArr[i2] != iArr2[i2]) {
                        z = false;
                        break;
                    } else {
                        i2++;
                    }
                }
                if (z) {
                    Object[] objArr = this.c;
                    Object[] objArr2 = wVar.c;
                    int i3 = this.a;
                    int i4 = 0;
                    while (true) {
                        if (i4 >= i3) {
                            z2 = true;
                            break;
                        } else if (!objArr[i4].equals(objArr2[i4])) {
                            z2 = false;
                            break;
                        } else {
                            i4++;
                        }
                    }
                    if (z2) {
                        return true;
                    }
                }
            }
            return false;
        }
        return false;
    }

    public final void g(a0 a0Var) throws IOException {
        if (this.a == 0) {
            return;
        }
        if (a0Var.Q() == m.e.k) {
            for (int i = 0; i < this.a; i++) {
                f(this.b[i], this.c[i], a0Var);
            }
            return;
        }
        for (int i2 = this.a - 1; i2 >= 0; i2--) {
            f(this.b[i2], this.c[i2], a0Var);
        }
    }

    public final int hashCode() {
        int i = this.a;
        int i2 = (i + 527) * 31;
        int[] iArr = this.b;
        int i3 = 17;
        int i4 = 17;
        for (int i5 = 0; i5 < i; i5++) {
            i4 = (i4 * 31) + iArr[i5];
        }
        int i6 = (i2 + i4) * 31;
        Object[] objArr = this.c;
        int i7 = this.a;
        for (int i8 = 0; i8 < i7; i8++) {
            i3 = (i3 * 31) + objArr[i8].hashCode();
        }
        return i6 + i3;
    }

    public final int j() {
        int i = this.d;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < this.a; i3++) {
            i2 += zzbn.X(this.b[i3] >>> 3, (zzbb) this.c[i3]);
        }
        this.d = i2;
        return i2;
    }

    public final void k() {
        this.e = false;
    }
}
