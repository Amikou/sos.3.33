package com.google.android.gms.internal.clearcut;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes.dex */
public final class e0 implements Cloneable {
    public fk5<?, ?> a;
    public Object f0;
    public List<Object> g0 = new ArrayList();

    public final byte[] a() throws IOException {
        byte[] bArr = new byte[d()];
        b(b0.q(bArr));
        return bArr;
    }

    public final void b(b0 b0Var) throws IOException {
        if (this.f0 != null) {
            throw new NoSuchMethodError();
        }
        Iterator<Object> it = this.g0.iterator();
        if (it.hasNext()) {
            it.next();
            throw new NoSuchMethodError();
        }
    }

    public final int d() {
        if (this.f0 == null) {
            Iterator<Object> it = this.g0.iterator();
            if (it.hasNext()) {
                it.next();
                throw new NoSuchMethodError();
            }
            return 0;
        }
        throw new NoSuchMethodError();
    }

    /* renamed from: e */
    public final e0 clone() {
        Object clone;
        e0 e0Var = new e0();
        try {
            List<Object> list = this.g0;
            if (list == null) {
                e0Var.g0 = null;
            } else {
                e0Var.g0.addAll(list);
            }
            Object obj = this.f0;
            if (obj != null) {
                if (obj instanceof f0) {
                    clone = (f0) ((f0) obj).clone();
                } else if (obj instanceof byte[]) {
                    clone = ((byte[]) obj).clone();
                } else {
                    int i = 0;
                    if (obj instanceof byte[][]) {
                        byte[][] bArr = (byte[][]) obj;
                        byte[][] bArr2 = new byte[bArr.length];
                        e0Var.f0 = bArr2;
                        while (i < bArr.length) {
                            bArr2[i] = (byte[]) bArr[i].clone();
                            i++;
                        }
                    } else if (obj instanceof boolean[]) {
                        clone = ((boolean[]) obj).clone();
                    } else if (obj instanceof int[]) {
                        clone = ((int[]) obj).clone();
                    } else if (obj instanceof long[]) {
                        clone = ((long[]) obj).clone();
                    } else if (obj instanceof float[]) {
                        clone = ((float[]) obj).clone();
                    } else if (obj instanceof double[]) {
                        clone = ((double[]) obj).clone();
                    } else if (obj instanceof f0[]) {
                        f0[] f0VarArr = (f0[]) obj;
                        f0[] f0VarArr2 = new f0[f0VarArr.length];
                        e0Var.f0 = f0VarArr2;
                        while (i < f0VarArr.length) {
                            f0VarArr2[i] = (f0) f0VarArr[i].clone();
                            i++;
                        }
                    }
                }
                e0Var.f0 = clone;
            }
            return e0Var;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    public final boolean equals(Object obj) {
        List<Object> list;
        if (obj == this) {
            return true;
        }
        if (obj instanceof e0) {
            e0 e0Var = (e0) obj;
            if (this.f0 != null && e0Var.f0 != null) {
                if (this.a != e0Var.a) {
                    return false;
                }
                throw null;
            }
            List<Object> list2 = this.g0;
            if (list2 == null || (list = e0Var.g0) == null) {
                try {
                    return Arrays.equals(a(), e0Var.a());
                } catch (IOException e) {
                    throw new IllegalStateException(e);
                }
            }
            return list2.equals(list);
        }
        return false;
    }

    public final int hashCode() {
        try {
            return Arrays.hashCode(a()) + 527;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
