package com.google.android.gms.internal.clearcut;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.clearcut.zzc;
import com.google.android.gms.clearcut.zze;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;

/* loaded from: classes.dex */
public interface s0 extends IInterface {
    void A0(Status status, zze[] zzeVarArr) throws RemoteException;

    void G0(Status status, zzc zzcVar) throws RemoteException;

    void O0(Status status) throws RemoteException;

    void Y0(Status status) throws RemoteException;

    void r(Status status, long j) throws RemoteException;

    void s1(Status status) throws RemoteException;

    void u1(Status status, zzc zzcVar) throws RemoteException;

    void v(DataHolder dataHolder) throws RemoteException;

    void y1(Status status, long j) throws RemoteException;
}
