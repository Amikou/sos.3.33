package com.google.android.gms.internal.clearcut;

import java.io.IOException;

/* loaded from: classes.dex */
public final class x extends v<w, w> {
    public static void m(Object obj, w wVar) {
        ((m) obj).zzjp = wVar;
    }

    @Override // com.google.android.gms.internal.clearcut.v
    public final /* synthetic */ void a(w wVar, int i, long j) {
        wVar.e(i << 3, Long.valueOf(j));
    }

    @Override // com.google.android.gms.internal.clearcut.v
    public final /* synthetic */ void b(w wVar, int i, zzbb zzbbVar) {
        wVar.e((i << 3) | 2, zzbbVar);
    }

    @Override // com.google.android.gms.internal.clearcut.v
    public final /* synthetic */ void c(w wVar, a0 a0Var) throws IOException {
        wVar.g(a0Var);
    }

    @Override // com.google.android.gms.internal.clearcut.v
    public final void d(Object obj) {
        ((m) obj).zzjp.k();
    }

    @Override // com.google.android.gms.internal.clearcut.v
    public final /* synthetic */ void e(w wVar, a0 a0Var) throws IOException {
        wVar.b(a0Var);
    }

    @Override // com.google.android.gms.internal.clearcut.v
    public final /* synthetic */ w f() {
        return w.i();
    }

    @Override // com.google.android.gms.internal.clearcut.v
    public final /* synthetic */ void g(Object obj, w wVar) {
        m(obj, wVar);
    }

    @Override // com.google.android.gms.internal.clearcut.v
    public final /* synthetic */ void h(Object obj, w wVar) {
        m(obj, wVar);
    }

    @Override // com.google.android.gms.internal.clearcut.v
    public final /* synthetic */ w i(w wVar, w wVar2) {
        w wVar3 = wVar;
        w wVar4 = wVar2;
        return wVar4.equals(w.h()) ? wVar3 : w.a(wVar3, wVar4);
    }

    @Override // com.google.android.gms.internal.clearcut.v
    public final /* synthetic */ int j(w wVar) {
        return wVar.d();
    }

    @Override // com.google.android.gms.internal.clearcut.v
    public final /* synthetic */ w k(Object obj) {
        return ((m) obj).zzjp;
    }

    @Override // com.google.android.gms.internal.clearcut.v
    public final /* synthetic */ int l(w wVar) {
        return wVar.j();
    }
}
