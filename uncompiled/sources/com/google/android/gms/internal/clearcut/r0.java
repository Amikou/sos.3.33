package com.google.android.gms.internal.clearcut;

import java.io.IOException;

/* loaded from: classes.dex */
public final class r0 extends c0<r0> implements Cloneable {
    public static volatile r0[] i0;
    public String g0 = "";
    public String h0 = "";

    public r0() {
        this.f0 = null;
        this.a = -1;
    }

    public static r0[] h() {
        if (i0 == null) {
            synchronized (lk5.a) {
                if (i0 == null) {
                    i0 = new r0[0];
                }
            }
        }
        return i0;
    }

    @Override // com.google.android.gms.internal.clearcut.c0, com.google.android.gms.internal.clearcut.f0
    public final void a(b0 b0Var) throws IOException {
        String str = this.g0;
        if (str != null && !str.equals("")) {
            b0Var.c(1, this.g0);
        }
        String str2 = this.h0;
        if (str2 != null && !str2.equals("")) {
            b0Var.c(2, this.h0);
        }
        super.a(b0Var);
    }

    @Override // com.google.android.gms.internal.clearcut.c0, com.google.android.gms.internal.clearcut.f0
    public final int e() {
        int e = super.e();
        String str = this.g0;
        if (str != null && !str.equals("")) {
            e += b0.h(1, this.g0);
        }
        String str2 = this.h0;
        return (str2 == null || str2.equals("")) ? e : e + b0.h(2, this.h0);
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof r0) {
            r0 r0Var = (r0) obj;
            String str = this.g0;
            if (str == null) {
                if (r0Var.g0 != null) {
                    return false;
                }
            } else if (!str.equals(r0Var.g0)) {
                return false;
            }
            String str2 = this.h0;
            if (str2 == null) {
                if (r0Var.h0 != null) {
                    return false;
                }
            } else if (!str2.equals(r0Var.h0)) {
                return false;
            }
            d0 d0Var = this.f0;
            if (d0Var == null || d0Var.a()) {
                d0 d0Var2 = r0Var.f0;
                return d0Var2 == null || d0Var2.a();
            }
            return this.f0.equals(r0Var.f0);
        }
        return false;
    }

    @Override // com.google.android.gms.internal.clearcut.c0, com.google.android.gms.internal.clearcut.f0
    public final /* synthetic */ f0 f() throws CloneNotSupportedException {
        return (r0) clone();
    }

    @Override // com.google.android.gms.internal.clearcut.c0
    public final /* synthetic */ r0 g() throws CloneNotSupportedException {
        return (r0) clone();
    }

    public final int hashCode() {
        int hashCode = (r0.class.getName().hashCode() + 527) * 31;
        String str = this.g0;
        int i = 0;
        int hashCode2 = (hashCode + (str == null ? 0 : str.hashCode())) * 31;
        String str2 = this.h0;
        int hashCode3 = (hashCode2 + (str2 == null ? 0 : str2.hashCode())) * 31;
        d0 d0Var = this.f0;
        if (d0Var != null && !d0Var.a()) {
            i = this.f0.hashCode();
        }
        return hashCode3 + i;
    }

    @Override // com.google.android.gms.internal.clearcut.c0, com.google.android.gms.internal.clearcut.f0
    /* renamed from: j */
    public final r0 clone() {
        try {
            return (r0) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }
}
