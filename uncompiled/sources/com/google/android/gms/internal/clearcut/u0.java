package com.google.android.gms.internal.clearcut;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.clearcut.zze;

/* loaded from: classes.dex */
public interface u0 extends IInterface {
    void F(s0 s0Var, zze zzeVar) throws RemoteException;
}
