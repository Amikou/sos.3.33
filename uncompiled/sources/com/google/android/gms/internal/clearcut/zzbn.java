package com.google.android.gms.internal.clearcut;

import java.io.IOException;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

/* loaded from: classes.dex */
public abstract class zzbn extends f {
    public static final Logger b = Logger.getLogger(zzbn.class.getName());
    public static final boolean c = y.x();
    public i a;

    /* loaded from: classes.dex */
    public static class a extends zzbn {
        public final byte[] d;
        public final int e;
        public final int f;
        public int g;

        public a(byte[] bArr, int i, int i2) {
            super();
            Objects.requireNonNull(bArr, "buffer");
            int i3 = i + i2;
            if ((i | i2 | (bArr.length - i3)) < 0) {
                throw new IllegalArgumentException(String.format("Array range is invalid. Buffer.length=%d, offset=%d, length=%d", Integer.valueOf(bArr.length), Integer.valueOf(i), Integer.valueOf(i2)));
            }
            this.d = bArr;
            this.e = i;
            this.g = i;
            this.f = i3;
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void A0(int i) throws IOException {
            try {
                byte[] bArr = this.d;
                int i2 = this.g;
                int i3 = i2 + 1;
                this.g = i3;
                bArr[i2] = (byte) i;
                int i4 = i3 + 1;
                this.g = i4;
                bArr[i3] = (byte) (i >> 8);
                int i5 = i4 + 1;
                this.g = i5;
                bArr[i4] = (byte) (i >> 16);
                this.g = i5 + 1;
                bArr[i5] = i >> 24;
            } catch (IndexOutOfBoundsException e) {
                throw new zzc(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.g), Integer.valueOf(this.f), 1), e);
            }
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void G(int i, int i2) throws IOException {
            y0((i << 3) | i2);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void I(int i, zzbb zzbbVar) throws IOException {
            G(1, 3);
            b0(2, i);
            m(3, zzbbVar);
            G(1, 4);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void J(int i, o oVar) throws IOException {
            G(1, 3);
            b0(2, i);
            n(3, oVar);
            G(1, 4);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void K(int i, boolean z) throws IOException {
            G(i, 0);
            g(z ? (byte) 1 : (byte) 0);
        }

        public final int K0() {
            return this.g - this.e;
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void L(long j) throws IOException {
            if (zzbn.c && u() >= 10) {
                while ((j & (-128)) != 0) {
                    byte[] bArr = this.d;
                    int i = this.g;
                    this.g = i + 1;
                    y.k(bArr, i, (byte) ((((int) j) & 127) | 128));
                    j >>>= 7;
                }
                byte[] bArr2 = this.d;
                int i2 = this.g;
                this.g = i2 + 1;
                y.k(bArr2, i2, (byte) j);
                return;
            }
            while ((j & (-128)) != 0) {
                try {
                    byte[] bArr3 = this.d;
                    int i3 = this.g;
                    this.g = i3 + 1;
                    bArr3[i3] = (byte) ((((int) j) & 127) | 128);
                    j >>>= 7;
                } catch (IndexOutOfBoundsException e) {
                    throw new zzc(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.g), Integer.valueOf(this.f), 1), e);
                }
            }
            byte[] bArr4 = this.d;
            int i4 = this.g;
            this.g = i4 + 1;
            bArr4[i4] = (byte) j;
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void M(o oVar) throws IOException {
            y0(oVar.j());
            oVar.g(this);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void T(int i, int i2) throws IOException {
            G(i, 0);
            x0(i2);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void U(int i, long j) throws IOException {
            G(i, 1);
            c0(j);
        }

        @Override // com.google.android.gms.internal.clearcut.f
        public final void a(byte[] bArr, int i, int i2) throws IOException {
            c(bArr, i, i2);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public void b() {
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void b0(int i, int i2) throws IOException {
            G(i, 0);
            y0(i2);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void c(byte[] bArr, int i, int i2) throws IOException {
            try {
                System.arraycopy(bArr, i, this.d, this.g, i2);
                this.g += i2;
            } catch (IndexOutOfBoundsException e) {
                throw new zzc(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.g), Integer.valueOf(this.f), Integer.valueOf(i2)), e);
            }
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void c0(long j) throws IOException {
            try {
                byte[] bArr = this.d;
                int i = this.g;
                int i2 = i + 1;
                this.g = i2;
                bArr[i] = (byte) j;
                int i3 = i2 + 1;
                this.g = i3;
                bArr[i2] = (byte) (j >> 8);
                int i4 = i3 + 1;
                this.g = i4;
                bArr[i3] = (byte) (j >> 16);
                int i5 = i4 + 1;
                this.g = i5;
                bArr[i4] = (byte) (j >> 24);
                int i6 = i5 + 1;
                this.g = i6;
                bArr[i5] = (byte) (j >> 32);
                int i7 = i6 + 1;
                this.g = i7;
                bArr[i6] = (byte) (j >> 40);
                int i8 = i7 + 1;
                this.g = i8;
                bArr[i7] = (byte) (j >> 48);
                this.g = i8 + 1;
                bArr[i8] = (byte) (j >> 56);
            } catch (IndexOutOfBoundsException e) {
                throw new zzc(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.g), Integer.valueOf(this.f), 1), e);
            }
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void g(byte b) throws IOException {
            try {
                byte[] bArr = this.d;
                int i = this.g;
                this.g = i + 1;
                bArr[i] = b;
            } catch (IndexOutOfBoundsException e) {
                throw new zzc(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.g), Integer.valueOf(this.f), 1), e);
            }
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void i0(int i, int i2) throws IOException {
            G(i, 5);
            A0(i2);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void l(int i, long j) throws IOException {
            G(i, 0);
            L(j);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void m(int i, zzbb zzbbVar) throws IOException {
            G(i, 2);
            q(zzbbVar);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void m0(String str) throws IOException {
            int i = this.g;
            try {
                int D0 = zzbn.D0(str.length() * 3);
                int D02 = zzbn.D0(str.length());
                if (D02 != D0) {
                    y0(zi5.a(str));
                    this.g = zi5.b(str, this.d, this.g, u());
                    return;
                }
                int i2 = i + D02;
                this.g = i2;
                int b = zi5.b(str, this.d, i2, u());
                this.g = i;
                y0((b - i) - D02);
                this.g = b;
            } catch (zzfi e) {
                this.g = i;
                s(str, e);
            } catch (IndexOutOfBoundsException e2) {
                throw new zzc(e2);
            }
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void n(int i, o oVar) throws IOException {
            G(i, 2);
            M(oVar);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void o(int i, o oVar, t tVar) throws IOException {
            G(i, 2);
            com.google.android.gms.internal.clearcut.c cVar = (com.google.android.gms.internal.clearcut.c) oVar;
            int c = cVar.c();
            if (c == -1) {
                c = tVar.g(cVar);
                cVar.b(c);
            }
            y0(c);
            tVar.e(oVar, this.a);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void p(int i, String str) throws IOException {
            G(i, 2);
            m0(str);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void q(zzbb zzbbVar) throws IOException {
            y0(zzbbVar.size());
            zzbbVar.zza(this);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void r(o oVar, t tVar) throws IOException {
            com.google.android.gms.internal.clearcut.c cVar = (com.google.android.gms.internal.clearcut.c) oVar;
            int c = cVar.c();
            if (c == -1) {
                c = tVar.g(cVar);
                cVar.b(c);
            }
            y0(c);
            tVar.e(oVar, this.a);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final int u() {
            return this.f - this.g;
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void x0(int i) throws IOException {
            if (i >= 0) {
                y0(i);
            } else {
                L(i);
            }
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void y0(int i) throws IOException {
            if (zzbn.c && u() >= 10) {
                while ((i & (-128)) != 0) {
                    byte[] bArr = this.d;
                    int i2 = this.g;
                    this.g = i2 + 1;
                    y.k(bArr, i2, (byte) ((i & 127) | 128));
                    i >>>= 7;
                }
                byte[] bArr2 = this.d;
                int i3 = this.g;
                this.g = i3 + 1;
                y.k(bArr2, i3, (byte) i);
                return;
            }
            while ((i & (-128)) != 0) {
                try {
                    byte[] bArr3 = this.d;
                    int i4 = this.g;
                    this.g = i4 + 1;
                    bArr3[i4] = (byte) ((i & 127) | 128);
                    i >>>= 7;
                } catch (IndexOutOfBoundsException e) {
                    throw new zzc(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.g), Integer.valueOf(this.f), 1), e);
                }
            }
            byte[] bArr4 = this.d;
            int i5 = this.g;
            this.g = i5 + 1;
            bArr4[i5] = (byte) i;
        }
    }

    /* loaded from: classes.dex */
    public static final class b extends a {
        public final ByteBuffer h;
        public int i;

        public b(ByteBuffer byteBuffer) {
            super(byteBuffer.array(), byteBuffer.arrayOffset() + byteBuffer.position(), byteBuffer.remaining());
            this.h = byteBuffer;
            this.i = byteBuffer.position();
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn.a, com.google.android.gms.internal.clearcut.zzbn
        public final void b() {
            this.h.position(this.i + K0());
        }
    }

    /* loaded from: classes.dex */
    public static final class c extends zzbn {
        public final ByteBuffer d;
        public final ByteBuffer e;

        public c(ByteBuffer byteBuffer) {
            super();
            this.d = byteBuffer;
            this.e = byteBuffer.duplicate().order(ByteOrder.LITTLE_ENDIAN);
            byteBuffer.position();
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void A0(int i) throws IOException {
            try {
                this.e.putInt(i);
            } catch (BufferOverflowException e) {
                throw new zzc(e);
            }
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void G(int i, int i2) throws IOException {
            y0((i << 3) | i2);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void I(int i, zzbb zzbbVar) throws IOException {
            G(1, 3);
            b0(2, i);
            m(3, zzbbVar);
            G(1, 4);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void J(int i, o oVar) throws IOException {
            G(1, 3);
            b0(2, i);
            n(3, oVar);
            G(1, 4);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void K(int i, boolean z) throws IOException {
            G(i, 0);
            g(z ? (byte) 1 : (byte) 0);
        }

        public final void K0(String str) throws IOException {
            try {
                zi5.c(str, this.e);
            } catch (IndexOutOfBoundsException e) {
                throw new zzc(e);
            }
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void L(long j) throws IOException {
            while (((-128) & j) != 0) {
                try {
                    this.e.put((byte) ((((int) j) & 127) | 128));
                    j >>>= 7;
                } catch (BufferOverflowException e) {
                    throw new zzc(e);
                }
            }
            this.e.put((byte) j);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void M(o oVar) throws IOException {
            y0(oVar.j());
            oVar.g(this);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void T(int i, int i2) throws IOException {
            G(i, 0);
            x0(i2);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void U(int i, long j) throws IOException {
            G(i, 1);
            c0(j);
        }

        @Override // com.google.android.gms.internal.clearcut.f
        public final void a(byte[] bArr, int i, int i2) throws IOException {
            c(bArr, i, i2);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void b() {
            this.d.position(this.e.position());
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void b0(int i, int i2) throws IOException {
            G(i, 0);
            y0(i2);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void c(byte[] bArr, int i, int i2) throws IOException {
            try {
                this.e.put(bArr, i, i2);
            } catch (IndexOutOfBoundsException e) {
                throw new zzc(e);
            } catch (BufferOverflowException e2) {
                throw new zzc(e2);
            }
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void c0(long j) throws IOException {
            try {
                this.e.putLong(j);
            } catch (BufferOverflowException e) {
                throw new zzc(e);
            }
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void g(byte b) throws IOException {
            try {
                this.e.put(b);
            } catch (BufferOverflowException e) {
                throw new zzc(e);
            }
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void i0(int i, int i2) throws IOException {
            G(i, 5);
            A0(i2);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void l(int i, long j) throws IOException {
            G(i, 0);
            L(j);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void m(int i, zzbb zzbbVar) throws IOException {
            G(i, 2);
            q(zzbbVar);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void m0(String str) throws IOException {
            int position = this.e.position();
            try {
                int D0 = zzbn.D0(str.length() * 3);
                int D02 = zzbn.D0(str.length());
                if (D02 != D0) {
                    y0(zi5.a(str));
                    K0(str);
                    return;
                }
                int position2 = this.e.position() + D02;
                this.e.position(position2);
                K0(str);
                int position3 = this.e.position();
                this.e.position(position);
                y0(position3 - position2);
                this.e.position(position3);
            } catch (zzfi e) {
                this.e.position(position);
                s(str, e);
            } catch (IllegalArgumentException e2) {
                throw new zzc(e2);
            }
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void n(int i, o oVar) throws IOException {
            G(i, 2);
            M(oVar);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void o(int i, o oVar, t tVar) throws IOException {
            G(i, 2);
            r(oVar, tVar);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void p(int i, String str) throws IOException {
            G(i, 2);
            m0(str);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void q(zzbb zzbbVar) throws IOException {
            y0(zzbbVar.size());
            zzbbVar.zza(this);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void r(o oVar, t tVar) throws IOException {
            com.google.android.gms.internal.clearcut.c cVar = (com.google.android.gms.internal.clearcut.c) oVar;
            int c = cVar.c();
            if (c == -1) {
                c = tVar.g(cVar);
                cVar.b(c);
            }
            y0(c);
            tVar.e(oVar, this.a);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final int u() {
            return this.e.remaining();
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void x0(int i) throws IOException {
            if (i >= 0) {
                y0(i);
            } else {
                L(i);
            }
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void y0(int i) throws IOException {
            while ((i & (-128)) != 0) {
                try {
                    this.e.put((byte) ((i & 127) | 128));
                    i >>>= 7;
                } catch (BufferOverflowException e) {
                    throw new zzc(e);
                }
            }
            this.e.put((byte) i);
        }
    }

    /* loaded from: classes.dex */
    public static final class d extends zzbn {
        public final ByteBuffer d;
        public final ByteBuffer e;
        public final long f;
        public final long g;
        public final long h;
        public final long i;
        public long j;

        public d(ByteBuffer byteBuffer) {
            super();
            this.d = byteBuffer;
            this.e = byteBuffer.duplicate().order(ByteOrder.LITTLE_ENDIAN);
            long o = y.o(byteBuffer);
            this.f = o;
            long position = byteBuffer.position() + o;
            this.g = position;
            long limit = o + byteBuffer.limit();
            this.h = limit;
            this.i = limit - 10;
            this.j = position;
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void A0(int i) throws IOException {
            this.e.putInt((int) (this.j - this.f), i);
            this.j += 4;
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void G(int i, int i2) throws IOException {
            y0((i << 3) | i2);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void I(int i, zzbb zzbbVar) throws IOException {
            G(1, 3);
            b0(2, i);
            m(3, zzbbVar);
            G(1, 4);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void J(int i, o oVar) throws IOException {
            G(1, 3);
            b0(2, i);
            n(3, oVar);
            G(1, 4);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void K(int i, boolean z) throws IOException {
            G(i, 0);
            g(z ? (byte) 1 : (byte) 0);
        }

        public final void K0(long j) {
            this.e.position((int) (j - this.f));
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void L(long j) throws IOException {
            if (this.j <= this.i) {
                while ((j & (-128)) != 0) {
                    long j2 = this.j;
                    this.j = j2 + 1;
                    y.c(j2, (byte) ((((int) j) & 127) | 128));
                    j >>>= 7;
                }
                long j3 = this.j;
                this.j = 1 + j3;
                y.c(j3, (byte) j);
                return;
            }
            while (true) {
                long j4 = this.j;
                if (j4 >= this.h) {
                    throw new zzc(String.format("Pos: %d, limit: %d, len: %d", Long.valueOf(this.j), Long.valueOf(this.h), 1));
                }
                if ((j & (-128)) == 0) {
                    this.j = 1 + j4;
                    y.c(j4, (byte) j);
                    return;
                }
                this.j = j4 + 1;
                y.c(j4, (byte) ((((int) j) & 127) | 128));
                j >>>= 7;
            }
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void M(o oVar) throws IOException {
            y0(oVar.j());
            oVar.g(this);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void T(int i, int i2) throws IOException {
            G(i, 0);
            x0(i2);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void U(int i, long j) throws IOException {
            G(i, 1);
            c0(j);
        }

        @Override // com.google.android.gms.internal.clearcut.f
        public final void a(byte[] bArr, int i, int i2) throws IOException {
            c(bArr, i, i2);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void b() {
            this.d.position((int) (this.j - this.f));
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void b0(int i, int i2) throws IOException {
            G(i, 0);
            y0(i2);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void c(byte[] bArr, int i, int i2) throws IOException {
            if (bArr != null && i >= 0 && i2 >= 0 && bArr.length - i2 >= i) {
                long j = i2;
                long j2 = this.j;
                if (this.h - j >= j2) {
                    y.l(bArr, i, j2, j);
                    this.j += j;
                    return;
                }
            }
            Objects.requireNonNull(bArr, "value");
            throw new zzc(String.format("Pos: %d, limit: %d, len: %d", Long.valueOf(this.j), Long.valueOf(this.h), Integer.valueOf(i2)));
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void c0(long j) throws IOException {
            this.e.putLong((int) (this.j - this.f), j);
            this.j += 8;
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void g(byte b) throws IOException {
            long j = this.j;
            if (j >= this.h) {
                throw new zzc(String.format("Pos: %d, limit: %d, len: %d", Long.valueOf(this.j), Long.valueOf(this.h), 1));
            }
            this.j = 1 + j;
            y.c(j, b);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void i0(int i, int i2) throws IOException {
            G(i, 5);
            A0(i2);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void l(int i, long j) throws IOException {
            G(i, 0);
            L(j);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void m(int i, zzbb zzbbVar) throws IOException {
            G(i, 2);
            q(zzbbVar);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void m0(String str) throws IOException {
            long j = this.j;
            try {
                int D0 = zzbn.D0(str.length() * 3);
                int D02 = zzbn.D0(str.length());
                if (D02 != D0) {
                    int a = zi5.a(str);
                    y0(a);
                    K0(this.j);
                    zi5.c(str, this.e);
                    this.j += a;
                    return;
                }
                int i = ((int) (this.j - this.f)) + D02;
                this.e.position(i);
                zi5.c(str, this.e);
                int position = this.e.position() - i;
                y0(position);
                this.j += position;
            } catch (zzfi e) {
                this.j = j;
                K0(j);
                s(str, e);
            } catch (IllegalArgumentException e2) {
                throw new zzc(e2);
            } catch (IndexOutOfBoundsException e3) {
                throw new zzc(e3);
            }
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void n(int i, o oVar) throws IOException {
            G(i, 2);
            M(oVar);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void o(int i, o oVar, t tVar) throws IOException {
            G(i, 2);
            r(oVar, tVar);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void p(int i, String str) throws IOException {
            G(i, 2);
            m0(str);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void q(zzbb zzbbVar) throws IOException {
            y0(zzbbVar.size());
            zzbbVar.zza(this);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void r(o oVar, t tVar) throws IOException {
            com.google.android.gms.internal.clearcut.c cVar = (com.google.android.gms.internal.clearcut.c) oVar;
            int c = cVar.c();
            if (c == -1) {
                c = tVar.g(cVar);
                cVar.b(c);
            }
            y0(c);
            tVar.e(oVar, this.a);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final int u() {
            return (int) (this.h - this.j);
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void x0(int i) throws IOException {
            if (i >= 0) {
                y0(i);
            } else {
                L(i);
            }
        }

        @Override // com.google.android.gms.internal.clearcut.zzbn
        public final void y0(int i) throws IOException {
            long j;
            if (this.j <= this.i) {
                while (true) {
                    int i2 = i & (-128);
                    j = this.j;
                    if (i2 == 0) {
                        break;
                    }
                    this.j = j + 1;
                    y.c(j, (byte) ((i & 127) | 128));
                    i >>>= 7;
                }
            } else {
                while (true) {
                    j = this.j;
                    if (j >= this.h) {
                        throw new zzc(String.format("Pos: %d, limit: %d, len: %d", Long.valueOf(this.j), Long.valueOf(this.h), 1));
                    }
                    if ((i & (-128)) == 0) {
                        break;
                    }
                    this.j = j + 1;
                    y.c(j, (byte) ((i & 127) | 128));
                    i >>>= 7;
                }
            }
            this.j = 1 + j;
            y.c(j, (byte) i);
        }
    }

    /* loaded from: classes.dex */
    public static class zzc extends IOException {
        public zzc() {
            super("CodedOutputStream was writing to a flat byte array and ran out of space.");
        }

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public zzc(java.lang.String r3) {
            /*
                r2 = this;
                java.lang.String r0 = "CodedOutputStream was writing to a flat byte array and ran out of space.: "
                java.lang.String r3 = java.lang.String.valueOf(r3)
                int r1 = r3.length()
                if (r1 == 0) goto L11
                java.lang.String r3 = r0.concat(r3)
                goto L16
            L11:
                java.lang.String r3 = new java.lang.String
                r3.<init>(r0)
            L16:
                r2.<init>(r3)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.clearcut.zzbn.zzc.<init>(java.lang.String):void");
        }

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public zzc(java.lang.String r3, java.lang.Throwable r4) {
            /*
                r2 = this;
                java.lang.String r0 = "CodedOutputStream was writing to a flat byte array and ran out of space.: "
                java.lang.String r3 = java.lang.String.valueOf(r3)
                int r1 = r3.length()
                if (r1 == 0) goto L11
                java.lang.String r3 = r0.concat(r3)
                goto L16
            L11:
                java.lang.String r3 = new java.lang.String
                r3.<init>(r0)
            L16:
                r2.<init>(r3, r4)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.clearcut.zzbn.zzc.<init>(java.lang.String, java.lang.Throwable):void");
        }

        public zzc(Throwable th) {
            super("CodedOutputStream was writing to a flat byte array and ran out of space.", th);
        }
    }

    public zzbn() {
    }

    public static int A(int i, o oVar, t tVar) {
        return B0(i) + E(oVar, tVar);
    }

    public static int B(int i, String str) {
        return B0(i) + q0(str);
    }

    public static int B0(int i) {
        return D0(i << 3);
    }

    public static int C(int i, ec5 ec5Var) {
        return (B0(1) << 1) + n0(2, i) + d(3, ec5Var);
    }

    public static int C0(int i) {
        if (i >= 0) {
            return D0(i);
        }
        return 10;
    }

    public static int D(zzbb zzbbVar) {
        int size = zzbbVar.size();
        return D0(size) + size;
    }

    public static int D0(int i) {
        if ((i & (-128)) == 0) {
            return 1;
        }
        if ((i & (-16384)) == 0) {
            return 2;
        }
        if (((-2097152) & i) == 0) {
            return 3;
        }
        return (i & (-268435456)) == 0 ? 4 : 5;
    }

    public static int E(o oVar, t tVar) {
        com.google.android.gms.internal.clearcut.c cVar = (com.google.android.gms.internal.clearcut.c) oVar;
        int c2 = cVar.c();
        if (c2 == -1) {
            c2 = tVar.g(cVar);
            cVar.b(c2);
        }
        return D0(c2) + c2;
    }

    public static int E0(int i) {
        return D0(I0(i));
    }

    public static int F(boolean z) {
        return 1;
    }

    public static int F0(int i) {
        return 4;
    }

    public static int G0(int i) {
        return 4;
    }

    public static int H0(int i) {
        return C0(i);
    }

    public static int I0(int i) {
        return (i >> 31) ^ (i << 1);
    }

    @Deprecated
    public static int J0(int i) {
        return D0(i);
    }

    public static int N(int i, zzbb zzbbVar) {
        int B0 = B0(i);
        int size = zzbbVar.size();
        return B0 + D0(size) + size;
    }

    public static int O(int i, o oVar) {
        return B0(i) + R(oVar);
    }

    @Deprecated
    public static int P(int i, o oVar, t tVar) {
        int B0 = B0(i) << 1;
        com.google.android.gms.internal.clearcut.c cVar = (com.google.android.gms.internal.clearcut.c) oVar;
        int c2 = cVar.c();
        if (c2 == -1) {
            c2 = tVar.g(cVar);
            cVar.b(c2);
        }
        return B0 + c2;
    }

    public static int Q(int i, boolean z) {
        return B0(i) + 1;
    }

    public static int R(o oVar) {
        int j = oVar.j();
        return D0(j) + j;
    }

    public static zzbn S(byte[] bArr) {
        return new a(bArr, 0, bArr.length);
    }

    public static int W(int i, long j) {
        return B0(i) + h0(j);
    }

    public static int X(int i, zzbb zzbbVar) {
        return (B0(1) << 1) + n0(2, i) + N(3, zzbbVar);
    }

    public static int Y(int i, o oVar) {
        return (B0(1) << 1) + n0(2, i) + O(3, oVar);
    }

    @Deprecated
    public static int Z(o oVar) {
        return oVar.j();
    }

    public static int a0(byte[] bArr) {
        int length = bArr.length;
        return D0(length) + length;
    }

    public static int d(int i, ec5 ec5Var) {
        int B0 = B0(i);
        int a2 = ec5Var.a();
        return B0 + D0(a2) + a2;
    }

    public static int d0(int i, long j) {
        return B0(i) + h0(j);
    }

    public static int e(ec5 ec5Var) {
        int a2 = ec5Var.a();
        return D0(a2) + a2;
    }

    public static int e0(long j) {
        return h0(j);
    }

    public static zzbn f(ByteBuffer byteBuffer) {
        if (byteBuffer.hasArray()) {
            return new b(byteBuffer);
        }
        if (!byteBuffer.isDirect() || byteBuffer.isReadOnly()) {
            throw new IllegalArgumentException("ByteBuffer is read-only");
        }
        return y.y() ? new d(byteBuffer) : new c(byteBuffer);
    }

    public static int g0(int i, long j) {
        return B0(i) + h0(u0(j));
    }

    public static int h0(long j) {
        int i;
        if (((-128) & j) == 0) {
            return 1;
        }
        if (j < 0) {
            return 10;
        }
        if (((-34359738368L) & j) != 0) {
            i = 6;
            j >>>= 28;
        } else {
            i = 2;
        }
        if (((-2097152) & j) != 0) {
            i += 2;
            j >>>= 14;
        }
        return (j & (-16384)) != 0 ? i + 1 : i;
    }

    public static int j0(int i, int i2) {
        return B0(i) + C0(i2);
    }

    public static int k0(int i, long j) {
        return B0(i) + 8;
    }

    public static int l0(long j) {
        return h0(u0(j));
    }

    public static int n0(int i, int i2) {
        return B0(i) + D0(i2);
    }

    public static int o0(int i, long j) {
        return B0(i) + 8;
    }

    public static int p0(long j) {
        return 8;
    }

    public static int q0(String str) {
        int length;
        try {
            length = zi5.a(str);
        } catch (zzfi unused) {
            length = str.getBytes(gb5.a).length;
        }
        return D0(length) + length;
    }

    public static int r0(int i, int i2) {
        return B0(i) + D0(I0(i2));
    }

    public static int s0(long j) {
        return 8;
    }

    public static int t0(int i, int i2) {
        return B0(i) + 4;
    }

    public static long u0(long j) {
        return (j >> 63) ^ (j << 1);
    }

    public static int v0(int i, int i2) {
        return B0(i) + 4;
    }

    public static int w(double d2) {
        return 8;
    }

    public static int w0(int i, int i2) {
        return B0(i) + C0(i2);
    }

    public static int x(float f) {
        return 4;
    }

    public static int y(int i, double d2) {
        return B0(i) + 8;
    }

    public static int z(int i, float f) {
        return B0(i) + 4;
    }

    public abstract void A0(int i) throws IOException;

    public abstract void G(int i, int i2) throws IOException;

    public final void H(int i, long j) throws IOException {
        l(i, u0(j));
    }

    public abstract void I(int i, zzbb zzbbVar) throws IOException;

    public abstract void J(int i, o oVar) throws IOException;

    public abstract void K(int i, boolean z) throws IOException;

    public abstract void L(long j) throws IOException;

    public abstract void M(o oVar) throws IOException;

    public abstract void T(int i, int i2) throws IOException;

    public abstract void U(int i, long j) throws IOException;

    public final void V(long j) throws IOException {
        L(u0(j));
    }

    public abstract void b() throws IOException;

    public abstract void b0(int i, int i2) throws IOException;

    public abstract void c(byte[] bArr, int i, int i2) throws IOException;

    public abstract void c0(long j) throws IOException;

    public final void f0(int i, int i2) throws IOException {
        b0(i, I0(i2));
    }

    public abstract void g(byte b2) throws IOException;

    public final void h(double d2) throws IOException {
        c0(Double.doubleToRawLongBits(d2));
    }

    public final void i(float f) throws IOException {
        A0(Float.floatToRawIntBits(f));
    }

    public abstract void i0(int i, int i2) throws IOException;

    public final void j(int i, double d2) throws IOException {
        U(i, Double.doubleToRawLongBits(d2));
    }

    public final void k(int i, float f) throws IOException {
        i0(i, Float.floatToRawIntBits(f));
    }

    public abstract void l(int i, long j) throws IOException;

    public abstract void m(int i, zzbb zzbbVar) throws IOException;

    public abstract void m0(String str) throws IOException;

    public abstract void n(int i, o oVar) throws IOException;

    public abstract void o(int i, o oVar, t tVar) throws IOException;

    public abstract void p(int i, String str) throws IOException;

    public abstract void q(zzbb zzbbVar) throws IOException;

    public abstract void r(o oVar, t tVar) throws IOException;

    public final void s(String str, zzfi zzfiVar) throws IOException {
        b.logp(Level.WARNING, "com.google.protobuf.CodedOutputStream", "inefficientWriteStringNoTag", "Converting ill-formed UTF-16. Your Protocol Buffer will not round trip correctly!", (Throwable) zzfiVar);
        byte[] bytes = str.getBytes(gb5.a);
        try {
            y0(bytes.length);
            a(bytes, 0, bytes.length);
        } catch (zzc e) {
            throw e;
        } catch (IndexOutOfBoundsException e2) {
            throw new zzc(e2);
        }
    }

    public final void t(boolean z) throws IOException {
        g(z ? (byte) 1 : (byte) 0);
    }

    public abstract int u();

    public abstract void x0(int i) throws IOException;

    public abstract void y0(int i) throws IOException;

    public final void z0(int i) throws IOException {
        y0(I0(i));
    }
}
