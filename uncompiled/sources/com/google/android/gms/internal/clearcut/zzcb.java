package com.google.android.gms.internal.clearcut;

/* JADX WARN: Enum visitor error
jadx.core.utils.exceptions.JadxRuntimeException: Init of enum a uses external variables
	at jadx.core.dex.visitors.EnumVisitor.createEnumFieldByConstructor(EnumVisitor.java:444)
	at jadx.core.dex.visitors.EnumVisitor.processEnumFieldByRegister(EnumVisitor.java:391)
	at jadx.core.dex.visitors.EnumVisitor.extractEnumFieldsFromFilledArray(EnumVisitor.java:320)
	at jadx.core.dex.visitors.EnumVisitor.extractEnumFieldsFromInsn(EnumVisitor.java:258)
	at jadx.core.dex.visitors.EnumVisitor.convertToEnum(EnumVisitor.java:151)
	at jadx.core.dex.visitors.EnumVisitor.visit(EnumVisitor.java:100)
 */
/* JADX WARN: Failed to restore enum class, 'enum' modifier and super class removed */
/* loaded from: classes.dex */
public final class zzcb {
    public static final zzcb A0;
    public static final zzcb B0;
    public static final zzcb C0;
    public static final zzcb D0;
    public static final zzcb E0;
    public static final zzcb F0;
    public static final zzcb G0;
    public static final zzcb H0;
    public static final zzcb I0;
    public static final zzcb J0;
    public static final zzcb K0;
    public static final zzcb L0;
    public static final zzcb M0;
    public static final zzcb N0;
    public static final zzcb O0;
    public static final zzcb P0;
    public static final zzcb Q0;
    public static final zzcb R0;
    public static final zzcb[] S0;
    public static final /* synthetic */ zzcb[] T0;
    public static final zzcb a;
    public static final zzcb f0;
    public static final zzcb g0;
    public static final zzcb h0;
    public static final zzcb i0;
    public static final zzcb j0;
    public static final zzcb k0;
    public static final zzcb l0;
    public static final zzcb m0;
    public static final zzcb n0;
    public static final zzcb o0;
    public static final zzcb p0;
    public static final zzcb q0;
    public static final zzcb r0;
    public static final zzcb s0;
    public static final zzcb t0;
    public static final zzcb u0;
    public static final zzcb v0;
    public static final zzcb w0;
    public static final zzcb x0;
    public static final zzcb y0;
    public static final zzcb z0;
    public static final zzcb zzhh;
    public static final zzcb zzhk;
    public static final zzcb zzhp;
    public static final zzcb zzhq;
    public static final zzcb zzhz;
    public static final zzcb zzic;
    public static final zzcb zzih;
    public static final zzcb zziq;
    public static final zzcb zziu;
    public static final zzcb zziv;
    public static final zzcb zziw;
    private final int id;
    private final zzcq zzix;
    private final zzcd zziy;
    private final Class<?> zziz;
    private final boolean zzja;

    static {
        zzcd zzcdVar = zzcd.SCALAR;
        zzcq zzcqVar = zzcq.zzlb;
        zzcb zzcbVar = new zzcb("DOUBLE", 0, 0, zzcdVar, zzcqVar);
        a = zzcbVar;
        zzcq zzcqVar2 = zzcq.zzla;
        zzcb zzcbVar2 = new zzcb("FLOAT", 1, 1, zzcdVar, zzcqVar2);
        f0 = zzcbVar2;
        zzcq zzcqVar3 = zzcq.zzkz;
        zzcb zzcbVar3 = new zzcb("INT64", 2, 2, zzcdVar, zzcqVar3);
        g0 = zzcbVar3;
        zzcb zzcbVar4 = new zzcb("UINT64", 3, 3, zzcdVar, zzcqVar3);
        h0 = zzcbVar4;
        zzcq zzcqVar4 = zzcq.zzky;
        zzcb zzcbVar5 = new zzcb("INT32", 4, 4, zzcdVar, zzcqVar4);
        i0 = zzcbVar5;
        zzcb zzcbVar6 = new zzcb("FIXED64", 5, 5, zzcdVar, zzcqVar3);
        j0 = zzcbVar6;
        zzcb zzcbVar7 = new zzcb("FIXED32", 6, 6, zzcdVar, zzcqVar4);
        k0 = zzcbVar7;
        zzcq zzcqVar5 = zzcq.zzlc;
        zzcb zzcbVar8 = new zzcb("BOOL", 7, 7, zzcdVar, zzcqVar5);
        l0 = zzcbVar8;
        zzcq zzcqVar6 = zzcq.zzld;
        zzcb zzcbVar9 = new zzcb("STRING", 8, 8, zzcdVar, zzcqVar6);
        m0 = zzcbVar9;
        zzcq zzcqVar7 = zzcq.zzlg;
        zzcb zzcbVar10 = new zzcb("MESSAGE", 9, 9, zzcdVar, zzcqVar7);
        zzhh = zzcbVar10;
        zzcq zzcqVar8 = zzcq.zzle;
        zzcb zzcbVar11 = new zzcb("BYTES", 10, 10, zzcdVar, zzcqVar8);
        n0 = zzcbVar11;
        zzcb zzcbVar12 = new zzcb("UINT32", 11, 11, zzcdVar, zzcqVar4);
        o0 = zzcbVar12;
        zzcq zzcqVar9 = zzcq.zzlf;
        zzcb zzcbVar13 = new zzcb("ENUM", 12, 12, zzcdVar, zzcqVar9);
        zzhk = zzcbVar13;
        zzcb zzcbVar14 = new zzcb("SFIXED32", 13, 13, zzcdVar, zzcqVar4);
        p0 = zzcbVar14;
        zzcb zzcbVar15 = new zzcb("SFIXED64", 14, 14, zzcdVar, zzcqVar3);
        q0 = zzcbVar15;
        zzcb zzcbVar16 = new zzcb("SINT32", 15, 15, zzcdVar, zzcqVar4);
        r0 = zzcbVar16;
        zzcb zzcbVar17 = new zzcb("SINT64", 16, 16, zzcdVar, zzcqVar3);
        s0 = zzcbVar17;
        zzcb zzcbVar18 = new zzcb("GROUP", 17, 17, zzcdVar, zzcqVar7);
        zzhp = zzcbVar18;
        zzcd zzcdVar2 = zzcd.VECTOR;
        zzcb zzcbVar19 = new zzcb("DOUBLE_LIST", 18, 18, zzcdVar2, zzcqVar);
        zzhq = zzcbVar19;
        zzcb zzcbVar20 = new zzcb("FLOAT_LIST", 19, 19, zzcdVar2, zzcqVar2);
        t0 = zzcbVar20;
        zzcb zzcbVar21 = new zzcb("INT64_LIST", 20, 20, zzcdVar2, zzcqVar3);
        u0 = zzcbVar21;
        zzcb zzcbVar22 = new zzcb("UINT64_LIST", 21, 21, zzcdVar2, zzcqVar3);
        v0 = zzcbVar22;
        zzcb zzcbVar23 = new zzcb("INT32_LIST", 22, 22, zzcdVar2, zzcqVar4);
        w0 = zzcbVar23;
        zzcb zzcbVar24 = new zzcb("FIXED64_LIST", 23, 23, zzcdVar2, zzcqVar3);
        x0 = zzcbVar24;
        zzcb zzcbVar25 = new zzcb("FIXED32_LIST", 24, 24, zzcdVar2, zzcqVar4);
        y0 = zzcbVar25;
        zzcb zzcbVar26 = new zzcb("BOOL_LIST", 25, 25, zzcdVar2, zzcqVar5);
        z0 = zzcbVar26;
        zzcb zzcbVar27 = new zzcb("STRING_LIST", 26, 26, zzcdVar2, zzcqVar6);
        A0 = zzcbVar27;
        zzcb zzcbVar28 = new zzcb("MESSAGE_LIST", 27, 27, zzcdVar2, zzcqVar7);
        zzhz = zzcbVar28;
        zzcb zzcbVar29 = new zzcb("BYTES_LIST", 28, 28, zzcdVar2, zzcqVar8);
        B0 = zzcbVar29;
        zzcb zzcbVar30 = new zzcb("UINT32_LIST", 29, 29, zzcdVar2, zzcqVar4);
        C0 = zzcbVar30;
        zzcb zzcbVar31 = new zzcb("ENUM_LIST", 30, 30, zzcdVar2, zzcqVar9);
        zzic = zzcbVar31;
        zzcb zzcbVar32 = new zzcb("SFIXED32_LIST", 31, 31, zzcdVar2, zzcqVar4);
        D0 = zzcbVar32;
        zzcb zzcbVar33 = new zzcb("SFIXED64_LIST", 32, 32, zzcdVar2, zzcqVar3);
        E0 = zzcbVar33;
        zzcb zzcbVar34 = new zzcb("SINT32_LIST", 33, 33, zzcdVar2, zzcqVar4);
        F0 = zzcbVar34;
        zzcb zzcbVar35 = new zzcb("SINT64_LIST", 34, 34, zzcdVar2, zzcqVar3);
        G0 = zzcbVar35;
        zzcd zzcdVar3 = zzcd.PACKED_VECTOR;
        zzcb zzcbVar36 = new zzcb("DOUBLE_LIST_PACKED", 35, 35, zzcdVar3, zzcqVar);
        zzih = zzcbVar36;
        zzcb zzcbVar37 = new zzcb("FLOAT_LIST_PACKED", 36, 36, zzcdVar3, zzcqVar2);
        H0 = zzcbVar37;
        zzcb zzcbVar38 = new zzcb("INT64_LIST_PACKED", 37, 37, zzcdVar3, zzcqVar3);
        I0 = zzcbVar38;
        zzcb zzcbVar39 = new zzcb("UINT64_LIST_PACKED", 38, 38, zzcdVar3, zzcqVar3);
        J0 = zzcbVar39;
        zzcb zzcbVar40 = new zzcb("INT32_LIST_PACKED", 39, 39, zzcdVar3, zzcqVar4);
        K0 = zzcbVar40;
        zzcb zzcbVar41 = new zzcb("FIXED64_LIST_PACKED", 40, 40, zzcdVar3, zzcqVar3);
        L0 = zzcbVar41;
        zzcb zzcbVar42 = new zzcb("FIXED32_LIST_PACKED", 41, 41, zzcdVar3, zzcqVar4);
        M0 = zzcbVar42;
        zzcb zzcbVar43 = new zzcb("BOOL_LIST_PACKED", 42, 42, zzcdVar3, zzcqVar5);
        N0 = zzcbVar43;
        zzcb zzcbVar44 = new zzcb("UINT32_LIST_PACKED", 43, 43, zzcdVar3, zzcqVar4);
        O0 = zzcbVar44;
        zzcb zzcbVar45 = new zzcb("ENUM_LIST_PACKED", 44, 44, zzcdVar3, zzcqVar9);
        zziq = zzcbVar45;
        zzcb zzcbVar46 = new zzcb("SFIXED32_LIST_PACKED", 45, 45, zzcdVar3, zzcqVar4);
        P0 = zzcbVar46;
        zzcb zzcbVar47 = new zzcb("SFIXED64_LIST_PACKED", 46, 46, zzcdVar3, zzcqVar3);
        Q0 = zzcbVar47;
        zzcb zzcbVar48 = new zzcb("SINT32_LIST_PACKED", 47, 47, zzcdVar3, zzcqVar4);
        R0 = zzcbVar48;
        zzcb zzcbVar49 = new zzcb("SINT64_LIST_PACKED", 48, 48, zzcdVar3, zzcqVar3);
        zziu = zzcbVar49;
        zzcb zzcbVar50 = new zzcb("GROUP_LIST", 49, 49, zzcdVar2, zzcqVar7);
        zziv = zzcbVar50;
        zzcb zzcbVar51 = new zzcb("MAP", 50, 50, zzcd.MAP, zzcq.zzkx);
        zziw = zzcbVar51;
        T0 = new zzcb[]{zzcbVar, zzcbVar2, zzcbVar3, zzcbVar4, zzcbVar5, zzcbVar6, zzcbVar7, zzcbVar8, zzcbVar9, zzcbVar10, zzcbVar11, zzcbVar12, zzcbVar13, zzcbVar14, zzcbVar15, zzcbVar16, zzcbVar17, zzcbVar18, zzcbVar19, zzcbVar20, zzcbVar21, zzcbVar22, zzcbVar23, zzcbVar24, zzcbVar25, zzcbVar26, zzcbVar27, zzcbVar28, zzcbVar29, zzcbVar30, zzcbVar31, zzcbVar32, zzcbVar33, zzcbVar34, zzcbVar35, zzcbVar36, zzcbVar37, zzcbVar38, zzcbVar39, zzcbVar40, zzcbVar41, zzcbVar42, zzcbVar43, zzcbVar44, zzcbVar45, zzcbVar46, zzcbVar47, zzcbVar48, zzcbVar49, zzcbVar50, zzcbVar51};
        zzcb[] values = values();
        S0 = new zzcb[values.length];
        for (zzcb zzcbVar52 : values) {
            S0[zzcbVar52.id] = zzcbVar52;
        }
    }

    public zzcb(String str, int i, int i2, zzcd zzcdVar, zzcq zzcqVar) {
        int i3;
        this.id = i2;
        this.zziy = zzcdVar;
        this.zzix = zzcqVar;
        int i4 = wa5.a[zzcdVar.ordinal()];
        boolean z = true;
        this.zziz = (i4 == 1 || i4 == 2) ? zzcqVar.zzbq() : null;
        this.zzja = (zzcdVar != zzcd.SCALAR || (i3 = wa5.b[zzcqVar.ordinal()]) == 1 || i3 == 2 || i3 == 3) ? false : z;
    }

    public static zzcb[] values() {
        return (zzcb[]) T0.clone();
    }

    public final int id() {
        return this.id;
    }
}
