package com.google.android.gms.internal.clearcut;

import com.github.mikephil.charting.utils.Utils;
import com.google.android.gms.internal.clearcut.m;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import sun.misc.Unsafe;

/* loaded from: classes.dex */
public final class q<T> implements t<T> {
    public static final Unsafe r = y.z();
    public final int[] a;
    public final Object[] b;
    public final int c;
    public final int d;
    public final int e;
    public final o f;
    public final boolean g;
    public final boolean h;
    public final boolean i;
    public final int[] j;
    public final int[] k;
    public final int[] l;
    public final se5 m;
    public final lc5 n;
    public final v<?, ?> o;
    public final j<?> p;
    public final wd5 q;

    public q(int[] iArr, Object[] objArr, int i, int i2, int i3, o oVar, boolean z, boolean z2, int[] iArr2, int[] iArr3, int[] iArr4, se5 se5Var, lc5 lc5Var, v<?, ?> vVar, j<?> jVar, wd5 wd5Var) {
        this.a = iArr;
        this.b = objArr;
        this.c = i;
        this.d = i2;
        this.e = i3;
        boolean z3 = oVar instanceof m;
        this.h = z;
        this.g = jVar != null && jVar.g(oVar);
        this.i = false;
        this.j = iArr2;
        this.k = iArr3;
        this.l = iArr4;
        this.m = se5Var;
        this.n = lc5Var;
        this.o = vVar;
        this.p = jVar;
        this.f = oVar;
        this.q = wd5Var;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static boolean B(Object obj, int i, t tVar) {
        return tVar.i(y.M(obj, i & 1048575));
    }

    public static <E> List<E> N(Object obj, long j) {
        return (List) y.M(obj, j);
    }

    public static <T> double O(T t, long j) {
        return ((Double) y.M(t, j)).doubleValue();
    }

    public static <T> float P(T t, long j) {
        return ((Float) y.M(t, j)).floatValue();
    }

    public static <T> int Q(T t, long j) {
        return ((Integer) y.M(t, j)).intValue();
    }

    public static <T> long R(T t, long j) {
        return ((Long) y.M(t, j)).longValue();
    }

    public static <T> boolean S(T t, long j) {
        return ((Boolean) y.M(t, j)).booleanValue();
    }

    public static w T(Object obj) {
        m mVar = (m) obj;
        w wVar = mVar.zzjp;
        if (wVar == w.h()) {
            w i = w.i();
            mVar.zzjp = i;
            return i;
        }
        return wVar;
    }

    public static int j(int i, byte[] bArr, int i2, int i3, Object obj, v65 v65Var) throws IOException {
        return d.a(i, bArr, i2, i3, T(obj), v65Var);
    }

    public static int k(t<?> tVar, int i, byte[] bArr, int i2, int i3, rb5<?> rb5Var, v65 v65Var) throws IOException {
        int m = m(tVar, bArr, i2, i3, v65Var);
        while (true) {
            rb5Var.add(v65Var.c);
            if (m >= i3) {
                break;
            }
            int e = d.e(bArr, m, v65Var);
            if (i != v65Var.a) {
                break;
            }
            m = m(tVar, bArr, e, i3, v65Var);
        }
        return m;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static int l(t tVar, byte[] bArr, int i, int i2, int i3, v65 v65Var) throws IOException {
        q qVar = (q) tVar;
        Object c = qVar.c();
        int r2 = qVar.r(c, bArr, i, i2, i3, v65Var);
        qVar.a(c);
        v65Var.c = c;
        return r2;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static int m(t tVar, byte[] bArr, int i, int i2, v65 v65Var) throws IOException {
        int i3 = i + 1;
        int i4 = bArr[i];
        if (i4 < 0) {
            i3 = d.d(i4, bArr, i3, v65Var);
            i4 = v65Var.a;
        }
        int i5 = i3;
        if (i4 < 0 || i4 > i2 - i5) {
            throw zzco.zzbl();
        }
        Object c = tVar.c();
        int i6 = i4 + i5;
        tVar.h(c, bArr, i5, i6, v65Var);
        tVar.a(c);
        v65Var.c = c;
        return i6;
    }

    public static <UT, UB> int n(v<UT, UB> vVar, T t) {
        return vVar.j(vVar.k(t));
    }

    public static <T> q<T> s(Class<T> cls, ce5 ce5Var, se5 se5Var, lc5 lc5Var, v<?, ?> vVar, j<?> jVar, wd5 wd5Var) {
        int k;
        int i;
        int i2;
        int b;
        int i3;
        int i4;
        if (!(ce5Var instanceof tf5)) {
            ((dh5) ce5Var).a();
            throw new NoSuchMethodError();
        }
        tf5 tf5Var = (tf5) ce5Var;
        boolean z = tf5Var.a() == m.e.j;
        if (tf5Var.d() == 0) {
            k = 0;
            i = 0;
            i2 = 0;
        } else {
            int f = tf5Var.f();
            int g = tf5Var.g();
            k = tf5Var.k();
            i = f;
            i2 = g;
        }
        int[] iArr = new int[k << 2];
        Object[] objArr = new Object[k << 1];
        int[] iArr2 = tf5Var.h() > 0 ? new int[tf5Var.h()] : null;
        int[] iArr3 = tf5Var.i() > 0 ? new int[tf5Var.i()] : null;
        vf5 e = tf5Var.e();
        if (e.a()) {
            int g2 = e.g();
            int i5 = 0;
            int i6 = 0;
            int i7 = 0;
            while (true) {
                if (g2 >= tf5Var.l() || i5 >= ((g2 - i) << 2)) {
                    if (e.k()) {
                        b = (int) y.b(e.l());
                        i3 = (int) y.b(e.m());
                        i4 = 0;
                    } else {
                        b = (int) y.b(e.n());
                        if (e.o()) {
                            i3 = (int) y.b(e.p());
                            i4 = e.q();
                        } else {
                            i3 = 0;
                            i4 = 0;
                        }
                    }
                    iArr[i5] = e.g();
                    int i8 = i5 + 1;
                    iArr[i8] = (e.s() ? 536870912 : 0) | (e.r() ? 268435456 : 0) | (e.h() << 20) | b;
                    iArr[i5 + 2] = i3 | (i4 << 20);
                    if (e.v() != null) {
                        int i9 = (i5 / 4) << 1;
                        objArr[i9] = e.v();
                        if (e.t() != null) {
                            objArr[i9 + 1] = e.t();
                        } else if (e.u() != null) {
                            objArr[i9 + 1] = e.u();
                        }
                    } else if (e.t() != null) {
                        objArr[((i5 / 4) << 1) + 1] = e.t();
                    } else if (e.u() != null) {
                        objArr[((i5 / 4) << 1) + 1] = e.u();
                    }
                    int h = e.h();
                    if (h == zzcb.zziw.ordinal()) {
                        iArr2[i6] = i5;
                        i6++;
                    } else if (h >= 18 && h <= 49) {
                        iArr3[i7] = iArr[i8] & 1048575;
                        i7++;
                    }
                    if (!e.a()) {
                        break;
                    }
                    g2 = e.g();
                } else {
                    for (int i10 = 0; i10 < 4; i10++) {
                        iArr[i5 + i10] = -1;
                    }
                }
                i5 += 4;
            }
        }
        return new q<>(iArr, objArr, i, i2, tf5Var.l(), tf5Var.c(), z, false, tf5Var.j(), iArr2, iArr3, se5Var, lc5Var, vVar, jVar, wd5Var);
    }

    public static void u(int i, Object obj, a0 a0Var) throws IOException {
        if (obj instanceof String) {
            a0Var.n(i, (String) obj);
        } else {
            a0Var.M(i, (zzbb) obj);
        }
    }

    public static <UT, UB> void v(v<UT, UB> vVar, T t, a0 a0Var) throws IOException {
        vVar.c(vVar.k(t), a0Var);
    }

    public final boolean A(T t, int i, int i2, int i3) {
        return this.h ? y(t, i) : (i2 & i3) != 0;
    }

    public final t C(int i) {
        int i2 = (i / 4) << 1;
        t tVar = (t) this.b[i2];
        if (tVar != null) {
            return tVar;
        }
        t<T> b = rf5.a().b((Class) this.b[i2 + 1]);
        this.b[i2] = b;
        return b;
    }

    public final Object D(int i) {
        return this.b[(i / 4) << 1];
    }

    public final mb5<?> E(int i) {
        return (mb5) this.b[((i / 4) << 1) + 1];
    }

    public final int F(int i) {
        return this.a[i + 1];
    }

    public final int G(int i) {
        return this.a[i + 2];
    }

    public final int H(int i) {
        int i2 = this.c;
        if (i >= i2) {
            int i3 = this.e;
            if (i < i3) {
                int i4 = (i - i2) << 2;
                if (this.a[i4] == i) {
                    return i4;
                }
                return -1;
            } else if (i <= this.d) {
                int i5 = i3 - i2;
                int length = (this.a.length / 4) - 1;
                while (i5 <= length) {
                    int i6 = (length + i5) >>> 1;
                    int i7 = i6 << 2;
                    int i8 = this.a[i7];
                    if (i == i8) {
                        return i7;
                    }
                    if (i < i8) {
                        length = i6 - 1;
                    } else {
                        i5 = i6 + 1;
                    }
                }
            }
        }
        return -1;
    }

    public final void I(T t, int i) {
        if (this.h) {
            return;
        }
        int G = G(i);
        long j = G & 1048575;
        y.g(t, j, y.H(t, j) | (1 << (G >>> 20)));
    }

    public final void J(T t, int i, int i2) {
        y.g(t, G(i2) & 1048575, i);
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x002d  */
    /* JADX WARN: Removed duplicated region for block: B:172:0x0494  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void K(T r19, com.google.android.gms.internal.clearcut.a0 r20) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 1342
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.clearcut.q.K(java.lang.Object, com.google.android.gms.internal.clearcut.a0):void");
    }

    public final void L(T t, T t2, int i) {
        int F = F(i);
        int i2 = this.a[i];
        long j = F & 1048575;
        if (z(t2, i2, i)) {
            Object M = y.M(t, j);
            Object M2 = y.M(t2, j);
            if (M != null && M2 != null) {
                y.i(t, j, gb5.d(M, M2));
                J(t, i2, i);
            } else if (M2 != null) {
                y.i(t, j, M2);
                J(t, i2, i);
            }
        }
    }

    public final boolean M(T t, T t2, int i) {
        return y(t, i) == y(t2, i);
    }

    @Override // com.google.android.gms.internal.clearcut.t
    public final void a(T t) {
        int[] iArr = this.k;
        if (iArr != null) {
            for (int i : iArr) {
                long F = F(i) & 1048575;
                Object M = y.M(t, F);
                if (M != null) {
                    y.i(t, F, this.q.e(M));
                }
            }
        }
        int[] iArr2 = this.l;
        if (iArr2 != null) {
            for (int i2 : iArr2) {
                this.n.a(t, i2);
            }
        }
        this.o.d(t);
        if (this.g) {
            this.p.f(t);
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:102:0x01a0, code lost:
        if (com.google.android.gms.internal.clearcut.y.I(r10, r6) == com.google.android.gms.internal.clearcut.y.I(r11, r6)) goto L84;
     */
    /* JADX WARN: Code restructure failed: missing block: B:11:0x0038, code lost:
        if (com.google.android.gms.internal.clearcut.u.y(com.google.android.gms.internal.clearcut.y.M(r10, r6), com.google.android.gms.internal.clearcut.y.M(r11, r6)) != false) goto L84;
     */
    /* JADX WARN: Code restructure failed: missing block: B:17:0x005c, code lost:
        if (com.google.android.gms.internal.clearcut.u.y(com.google.android.gms.internal.clearcut.y.M(r10, r6), com.google.android.gms.internal.clearcut.y.M(r11, r6)) != false) goto L84;
     */
    /* JADX WARN: Code restructure failed: missing block: B:22:0x0070, code lost:
        if (com.google.android.gms.internal.clearcut.y.I(r10, r6) == com.google.android.gms.internal.clearcut.y.I(r11, r6)) goto L84;
     */
    /* JADX WARN: Code restructure failed: missing block: B:27:0x0082, code lost:
        if (com.google.android.gms.internal.clearcut.y.H(r10, r6) == com.google.android.gms.internal.clearcut.y.H(r11, r6)) goto L84;
     */
    /* JADX WARN: Code restructure failed: missing block: B:32:0x0096, code lost:
        if (com.google.android.gms.internal.clearcut.y.I(r10, r6) == com.google.android.gms.internal.clearcut.y.I(r11, r6)) goto L84;
     */
    /* JADX WARN: Code restructure failed: missing block: B:37:0x00a8, code lost:
        if (com.google.android.gms.internal.clearcut.y.H(r10, r6) == com.google.android.gms.internal.clearcut.y.H(r11, r6)) goto L84;
     */
    /* JADX WARN: Code restructure failed: missing block: B:42:0x00ba, code lost:
        if (com.google.android.gms.internal.clearcut.y.H(r10, r6) == com.google.android.gms.internal.clearcut.y.H(r11, r6)) goto L84;
     */
    /* JADX WARN: Code restructure failed: missing block: B:47:0x00cc, code lost:
        if (com.google.android.gms.internal.clearcut.y.H(r10, r6) == com.google.android.gms.internal.clearcut.y.H(r11, r6)) goto L84;
     */
    /* JADX WARN: Code restructure failed: missing block: B:52:0x00e2, code lost:
        if (com.google.android.gms.internal.clearcut.u.y(com.google.android.gms.internal.clearcut.y.M(r10, r6), com.google.android.gms.internal.clearcut.y.M(r11, r6)) != false) goto L84;
     */
    /* JADX WARN: Code restructure failed: missing block: B:57:0x00f8, code lost:
        if (com.google.android.gms.internal.clearcut.u.y(com.google.android.gms.internal.clearcut.y.M(r10, r6), com.google.android.gms.internal.clearcut.y.M(r11, r6)) != false) goto L84;
     */
    /* JADX WARN: Code restructure failed: missing block: B:62:0x010e, code lost:
        if (com.google.android.gms.internal.clearcut.u.y(com.google.android.gms.internal.clearcut.y.M(r10, r6), com.google.android.gms.internal.clearcut.y.M(r11, r6)) != false) goto L84;
     */
    /* JADX WARN: Code restructure failed: missing block: B:67:0x0120, code lost:
        if (com.google.android.gms.internal.clearcut.y.J(r10, r6) == com.google.android.gms.internal.clearcut.y.J(r11, r6)) goto L84;
     */
    /* JADX WARN: Code restructure failed: missing block: B:72:0x0132, code lost:
        if (com.google.android.gms.internal.clearcut.y.H(r10, r6) == com.google.android.gms.internal.clearcut.y.H(r11, r6)) goto L84;
     */
    /* JADX WARN: Code restructure failed: missing block: B:77:0x0145, code lost:
        if (com.google.android.gms.internal.clearcut.y.I(r10, r6) == com.google.android.gms.internal.clearcut.y.I(r11, r6)) goto L84;
     */
    /* JADX WARN: Code restructure failed: missing block: B:82:0x0156, code lost:
        if (com.google.android.gms.internal.clearcut.y.H(r10, r6) == com.google.android.gms.internal.clearcut.y.H(r11, r6)) goto L84;
     */
    /* JADX WARN: Code restructure failed: missing block: B:87:0x0169, code lost:
        if (com.google.android.gms.internal.clearcut.y.I(r10, r6) == com.google.android.gms.internal.clearcut.y.I(r11, r6)) goto L84;
     */
    /* JADX WARN: Code restructure failed: missing block: B:92:0x017c, code lost:
        if (com.google.android.gms.internal.clearcut.y.I(r10, r6) == com.google.android.gms.internal.clearcut.y.I(r11, r6)) goto L84;
     */
    /* JADX WARN: Code restructure failed: missing block: B:97:0x018d, code lost:
        if (com.google.android.gms.internal.clearcut.y.H(r10, r6) == com.google.android.gms.internal.clearcut.y.H(r11, r6)) goto L84;
     */
    @Override // com.google.android.gms.internal.clearcut.t
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final boolean b(T r10, T r11) {
        /*
            Method dump skipped, instructions count: 610
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.clearcut.q.b(java.lang.Object, java.lang.Object):boolean");
    }

    @Override // com.google.android.gms.internal.clearcut.t
    public final T c() {
        return (T) this.m.a(this.f);
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARN: Code restructure failed: missing block: B:62:0x00ce, code lost:
        if (r3 != null) goto L77;
     */
    /* JADX WARN: Code restructure failed: missing block: B:67:0x00e0, code lost:
        if (r3 != null) goto L77;
     */
    /* JADX WARN: Code restructure failed: missing block: B:68:0x00e2, code lost:
        r7 = r3.hashCode();
     */
    /* JADX WARN: Code restructure failed: missing block: B:69:0x00e6, code lost:
        r2 = (r2 * 53) + r7;
     */
    @Override // com.google.android.gms.internal.clearcut.t
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final int d(T r9) {
        /*
            Method dump skipped, instructions count: 476
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.clearcut.q.d(java.lang.Object):int");
    }

    /* JADX WARN: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARN: Removed duplicated region for block: B:180:0x04b9  */
    /* JADX WARN: Removed duplicated region for block: B:195:0x04f6  */
    /* JADX WARN: Removed duplicated region for block: B:363:0x0976  */
    @Override // com.google.android.gms.internal.clearcut.t
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void e(T r14, com.google.android.gms.internal.clearcut.a0 r15) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 2736
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.clearcut.q.e(java.lang.Object, com.google.android.gms.internal.clearcut.a0):void");
    }

    @Override // com.google.android.gms.internal.clearcut.t
    public final void f(T t, T t2) {
        Objects.requireNonNull(t2);
        for (int i = 0; i < this.a.length; i += 4) {
            int F = F(i);
            long j = 1048575 & F;
            int i2 = this.a[i];
            switch ((F & 267386880) >>> 20) {
                case 0:
                    if (y(t2, i)) {
                        y.e(t, j, y.L(t2, j));
                        I(t, i);
                        break;
                    } else {
                        break;
                    }
                case 1:
                    if (y(t2, i)) {
                        y.f(t, j, y.K(t2, j));
                        I(t, i);
                        break;
                    } else {
                        break;
                    }
                case 2:
                    if (!y(t2, i)) {
                        break;
                    }
                    y.h(t, j, y.I(t2, j));
                    I(t, i);
                    break;
                case 3:
                    if (!y(t2, i)) {
                        break;
                    }
                    y.h(t, j, y.I(t2, j));
                    I(t, i);
                    break;
                case 4:
                    if (!y(t2, i)) {
                        break;
                    }
                    y.g(t, j, y.H(t2, j));
                    I(t, i);
                    break;
                case 5:
                    if (!y(t2, i)) {
                        break;
                    }
                    y.h(t, j, y.I(t2, j));
                    I(t, i);
                    break;
                case 6:
                    if (!y(t2, i)) {
                        break;
                    }
                    y.g(t, j, y.H(t2, j));
                    I(t, i);
                    break;
                case 7:
                    if (y(t2, i)) {
                        y.j(t, j, y.J(t2, j));
                        I(t, i);
                        break;
                    } else {
                        break;
                    }
                case 8:
                    if (!y(t2, i)) {
                        break;
                    }
                    y.i(t, j, y.M(t2, j));
                    I(t, i);
                    break;
                case 9:
                case 17:
                    x(t, t2, i);
                    break;
                case 10:
                    if (!y(t2, i)) {
                        break;
                    }
                    y.i(t, j, y.M(t2, j));
                    I(t, i);
                    break;
                case 11:
                    if (!y(t2, i)) {
                        break;
                    }
                    y.g(t, j, y.H(t2, j));
                    I(t, i);
                    break;
                case 12:
                    if (!y(t2, i)) {
                        break;
                    }
                    y.g(t, j, y.H(t2, j));
                    I(t, i);
                    break;
                case 13:
                    if (!y(t2, i)) {
                        break;
                    }
                    y.g(t, j, y.H(t2, j));
                    I(t, i);
                    break;
                case 14:
                    if (!y(t2, i)) {
                        break;
                    }
                    y.h(t, j, y.I(t2, j));
                    I(t, i);
                    break;
                case 15:
                    if (!y(t2, i)) {
                        break;
                    }
                    y.g(t, j, y.H(t2, j));
                    I(t, i);
                    break;
                case 16:
                    if (!y(t2, i)) {
                        break;
                    }
                    y.h(t, j, y.I(t2, j));
                    I(t, i);
                    break;
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                case 48:
                case 49:
                    this.n.b(t, t2, j);
                    break;
                case 50:
                    u.i(this.q, t, t2, j);
                    break;
                case 51:
                case 52:
                case 53:
                case 54:
                case 55:
                case 56:
                case 57:
                case 58:
                case 59:
                    if (!z(t2, i2, i)) {
                        break;
                    }
                    y.i(t, j, y.M(t2, j));
                    J(t, i2, i);
                    break;
                case 60:
                case 68:
                    L(t, t2, i);
                    break;
                case 61:
                case 62:
                case 63:
                case 64:
                case 65:
                case 66:
                case 67:
                    if (!z(t2, i2, i)) {
                        break;
                    }
                    y.i(t, j, y.M(t2, j));
                    J(t, i2, i);
                    break;
            }
        }
        if (this.h) {
            return;
        }
        u.h(this.o, t, t2);
        if (this.g) {
            u.g(this.p, t, t2);
        }
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARN: Code restructure failed: missing block: B:101:0x0181, code lost:
        if (r19.i != false) goto L104;
     */
    /* JADX WARN: Code restructure failed: missing block: B:106:0x0193, code lost:
        if (r19.i != false) goto L104;
     */
    /* JADX WARN: Code restructure failed: missing block: B:111:0x01a5, code lost:
        if (r19.i != false) goto L104;
     */
    /* JADX WARN: Code restructure failed: missing block: B:116:0x01b6, code lost:
        if (r19.i != false) goto L104;
     */
    /* JADX WARN: Code restructure failed: missing block: B:121:0x01c7, code lost:
        if (r19.i != false) goto L104;
     */
    /* JADX WARN: Code restructure failed: missing block: B:126:0x01d8, code lost:
        if (r19.i != false) goto L104;
     */
    /* JADX WARN: Code restructure failed: missing block: B:131:0x01e9, code lost:
        if (r19.i != false) goto L104;
     */
    /* JADX WARN: Code restructure failed: missing block: B:136:0x01fa, code lost:
        if (r19.i != false) goto L104;
     */
    /* JADX WARN: Code restructure failed: missing block: B:141:0x020b, code lost:
        if (r19.i != false) goto L104;
     */
    /* JADX WARN: Code restructure failed: missing block: B:142:0x020d, code lost:
        r2.putInt(r20, r14, r5);
     */
    /* JADX WARN: Code restructure failed: missing block: B:143:0x0211, code lost:
        r3 = (com.google.android.gms.internal.clearcut.zzbn.B0(r3) + com.google.android.gms.internal.clearcut.zzbn.D0(r5)) + r5;
     */
    /* JADX WARN: Code restructure failed: missing block: B:193:0x0331, code lost:
        if ((r5 instanceof com.google.android.gms.internal.clearcut.zzbb) != false) goto L50;
     */
    /* JADX WARN: Code restructure failed: missing block: B:195:0x0334, code lost:
        r3 = com.google.android.gms.internal.clearcut.zzbn.B(r3, (java.lang.String) r5);
     */
    /* JADX WARN: Code restructure failed: missing block: B:248:0x0414, code lost:
        if (z(r20, r15, r5) != false) goto L266;
     */
    /* JADX WARN: Code restructure failed: missing block: B:257:0x0434, code lost:
        if (z(r20, r15, r5) != false) goto L278;
     */
    /* JADX WARN: Code restructure failed: missing block: B:260:0x043c, code lost:
        if (z(r20, r15, r5) != false) goto L281;
     */
    /* JADX WARN: Code restructure failed: missing block: B:269:0x045c, code lost:
        if (z(r20, r15, r5) != false) goto L293;
     */
    /* JADX WARN: Code restructure failed: missing block: B:272:0x0464, code lost:
        if (z(r20, r15, r5) != false) goto L297;
     */
    /* JADX WARN: Code restructure failed: missing block: B:277:0x0474, code lost:
        if ((r4 instanceof com.google.android.gms.internal.clearcut.zzbb) != false) goto L294;
     */
    /* JADX WARN: Code restructure failed: missing block: B:280:0x047c, code lost:
        if (z(r20, r15, r5) != false) goto L305;
     */
    /* JADX WARN: Code restructure failed: missing block: B:308:0x0514, code lost:
        if (r19.i != false) goto L334;
     */
    /* JADX WARN: Code restructure failed: missing block: B:313:0x0526, code lost:
        if (r19.i != false) goto L334;
     */
    /* JADX WARN: Code restructure failed: missing block: B:318:0x0538, code lost:
        if (r19.i != false) goto L334;
     */
    /* JADX WARN: Code restructure failed: missing block: B:323:0x054a, code lost:
        if (r19.i != false) goto L334;
     */
    /* JADX WARN: Code restructure failed: missing block: B:328:0x055c, code lost:
        if (r19.i != false) goto L334;
     */
    /* JADX WARN: Code restructure failed: missing block: B:333:0x056e, code lost:
        if (r19.i != false) goto L334;
     */
    /* JADX WARN: Code restructure failed: missing block: B:338:0x0580, code lost:
        if (r19.i != false) goto L334;
     */
    /* JADX WARN: Code restructure failed: missing block: B:343:0x0592, code lost:
        if (r19.i != false) goto L334;
     */
    /* JADX WARN: Code restructure failed: missing block: B:348:0x05a3, code lost:
        if (r19.i != false) goto L334;
     */
    /* JADX WARN: Code restructure failed: missing block: B:353:0x05b4, code lost:
        if (r19.i != false) goto L334;
     */
    /* JADX WARN: Code restructure failed: missing block: B:358:0x05c5, code lost:
        if (r19.i != false) goto L334;
     */
    /* JADX WARN: Code restructure failed: missing block: B:363:0x05d6, code lost:
        if (r19.i != false) goto L334;
     */
    /* JADX WARN: Code restructure failed: missing block: B:368:0x05e7, code lost:
        if (r19.i != false) goto L334;
     */
    /* JADX WARN: Code restructure failed: missing block: B:373:0x05f8, code lost:
        if (r19.i != false) goto L334;
     */
    /* JADX WARN: Code restructure failed: missing block: B:374:0x05fa, code lost:
        r2.putInt(r20, r9, r4);
     */
    /* JADX WARN: Code restructure failed: missing block: B:375:0x05fe, code lost:
        r9 = (com.google.android.gms.internal.clearcut.zzbn.B0(r15) + com.google.android.gms.internal.clearcut.zzbn.D0(r4)) + r4;
     */
    /* JADX WARN: Code restructure failed: missing block: B:395:0x06c4, code lost:
        if ((r12 & r18) != 0) goto L266;
     */
    /* JADX WARN: Code restructure failed: missing block: B:396:0x06c6, code lost:
        r4 = com.google.android.gms.internal.clearcut.zzbn.P(r15, (com.google.android.gms.internal.clearcut.o) r2.getObject(r20, r10), C(r5));
     */
    /* JADX WARN: Code restructure failed: missing block: B:406:0x06f1, code lost:
        if ((r12 & r18) != 0) goto L278;
     */
    /* JADX WARN: Code restructure failed: missing block: B:407:0x06f3, code lost:
        r4 = com.google.android.gms.internal.clearcut.zzbn.o0(r15, 0);
     */
    /* JADX WARN: Code restructure failed: missing block: B:409:0x06fc, code lost:
        if ((r12 & r18) != 0) goto L281;
     */
    /* JADX WARN: Code restructure failed: missing block: B:410:0x06fe, code lost:
        r9 = com.google.android.gms.internal.clearcut.zzbn.v0(r15, 0);
     */
    /* JADX WARN: Code restructure failed: missing block: B:421:0x0721, code lost:
        if ((r12 & r18) != 0) goto L293;
     */
    /* JADX WARN: Code restructure failed: missing block: B:422:0x0723, code lost:
        r4 = r2.getObject(r20, r10);
     */
    /* JADX WARN: Code restructure failed: missing block: B:423:0x0727, code lost:
        r4 = com.google.android.gms.internal.clearcut.zzbn.N(r15, (com.google.android.gms.internal.clearcut.zzbb) r4);
     */
    /* JADX WARN: Code restructure failed: missing block: B:425:0x0730, code lost:
        if ((r12 & r18) != 0) goto L297;
     */
    /* JADX WARN: Code restructure failed: missing block: B:426:0x0732, code lost:
        r4 = com.google.android.gms.internal.clearcut.u.n(r15, r2.getObject(r20, r10), C(r5));
     */
    /* JADX WARN: Code restructure failed: missing block: B:430:0x074a, code lost:
        if ((r4 instanceof com.google.android.gms.internal.clearcut.zzbb) != false) goto L294;
     */
    /* JADX WARN: Code restructure failed: missing block: B:432:0x074d, code lost:
        r4 = com.google.android.gms.internal.clearcut.zzbn.B(r15, (java.lang.String) r4);
     */
    /* JADX WARN: Code restructure failed: missing block: B:434:0x0757, code lost:
        if ((r12 & r18) != 0) goto L305;
     */
    /* JADX WARN: Code restructure failed: missing block: B:435:0x0759, code lost:
        r4 = com.google.android.gms.internal.clearcut.zzbn.Q(r15, true);
     */
    /* JADX WARN: Code restructure failed: missing block: B:45:0x00ab, code lost:
        if ((r5 instanceof com.google.android.gms.internal.clearcut.zzbb) != false) goto L50;
     */
    /* JADX WARN: Code restructure failed: missing block: B:76:0x0127, code lost:
        if (r19.i != false) goto L104;
     */
    /* JADX WARN: Code restructure failed: missing block: B:81:0x0139, code lost:
        if (r19.i != false) goto L104;
     */
    /* JADX WARN: Code restructure failed: missing block: B:86:0x014b, code lost:
        if (r19.i != false) goto L104;
     */
    /* JADX WARN: Code restructure failed: missing block: B:91:0x015d, code lost:
        if (r19.i != false) goto L104;
     */
    /* JADX WARN: Code restructure failed: missing block: B:96:0x016f, code lost:
        if (r19.i != false) goto L104;
     */
    @Override // com.google.android.gms.internal.clearcut.t
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final int g(T r20) {
        /*
            Method dump skipped, instructions count: 2306
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.clearcut.q.g(java.lang.Object):int");
    }

    /* JADX WARN: Code restructure failed: missing block: B:73:0x0164, code lost:
        if (r0 == r15) goto L38;
     */
    /* JADX WARN: Code restructure failed: missing block: B:79:0x0188, code lost:
        if (r0 == r15) goto L38;
     */
    /* JADX WARN: Code restructure failed: missing block: B:82:0x01a1, code lost:
        if (r0 == r15) goto L38;
     */
    /* JADX WARN: Code restructure failed: missing block: B:83:0x01a3, code lost:
        r2 = r0;
     */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v25, types: [int] */
    @Override // com.google.android.gms.internal.clearcut.t
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void h(T r23, byte[] r24, int r25, int r26, defpackage.v65 r27) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 518
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.clearcut.q.h(java.lang.Object, byte[], int, int, v65):void");
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.google.android.gms.internal.clearcut.t
    public final boolean i(T t) {
        int i;
        int i2;
        boolean z;
        int[] iArr = this.j;
        if (iArr != null && iArr.length != 0) {
            int i3 = -1;
            int length = iArr.length;
            int i4 = 0;
            for (int i5 = 0; i5 < length; i5 = i + 1) {
                int i6 = iArr[i5];
                int H = H(i6);
                int F = F(H);
                if (this.h) {
                    i = i5;
                    i2 = 0;
                } else {
                    int i7 = this.a[H + 2];
                    int i8 = i7 & 1048575;
                    i2 = 1 << (i7 >>> 20);
                    if (i8 != i3) {
                        i = i5;
                        i4 = r.getInt(t, i8);
                        i3 = i8;
                    } else {
                        i = i5;
                    }
                }
                if (((268435456 & F) != 0) && !A(t, H, i4, i2)) {
                    return false;
                }
                int i9 = (267386880 & F) >>> 20;
                if (i9 != 9 && i9 != 17) {
                    if (i9 != 27) {
                        if (i9 == 60 || i9 == 68) {
                            if (z(t, i6, H) && !B(t, F, C(H))) {
                                return false;
                            }
                        } else if (i9 != 49) {
                            if (i9 == 50 && !this.q.g(y.M(t, F & 1048575)).isEmpty()) {
                                this.q.a(D(H));
                                throw null;
                            }
                        }
                    }
                    List list = (List) y.M(t, F & 1048575);
                    if (!list.isEmpty()) {
                        t C = C(H);
                        for (int i10 = 0; i10 < list.size(); i10++) {
                            if (!C.i(list.get(i10))) {
                                z = false;
                                break;
                            }
                        }
                    }
                    z = true;
                    if (!z) {
                        return false;
                    }
                } else if (A(t, H, i4, i2) && !B(t, F, C(H))) {
                    return false;
                }
            }
            if (this.g && !this.p.b(t).d()) {
                return false;
            }
        }
        return true;
    }

    public final int o(T t, byte[] bArr, int i, int i2, int i3, int i4, int i5, int i6, int i7, long j, int i8, v65 v65Var) throws IOException {
        Object valueOf;
        Object valueOf2;
        int g;
        long j2;
        int i9;
        Object valueOf3;
        int i10;
        Unsafe unsafe = r;
        long j3 = this.a[i8 + 2] & 1048575;
        switch (i7) {
            case 51:
                if (i5 == 1) {
                    valueOf = Double.valueOf(d.l(bArr, i));
                    unsafe.putObject(t, j, valueOf);
                    g = i + 8;
                    unsafe.putInt(t, j3, i4);
                    return g;
                }
                return i;
            case 52:
                if (i5 == 5) {
                    valueOf2 = Float.valueOf(d.n(bArr, i));
                    unsafe.putObject(t, j, valueOf2);
                    g = i + 4;
                    unsafe.putInt(t, j3, i4);
                    return g;
                }
                return i;
            case 53:
            case 54:
                if (i5 == 0) {
                    g = d.g(bArr, i, v65Var);
                    j2 = v65Var.b;
                    valueOf3 = Long.valueOf(j2);
                    unsafe.putObject(t, j, valueOf3);
                    unsafe.putInt(t, j3, i4);
                    return g;
                }
                return i;
            case 55:
            case 62:
                if (i5 == 0) {
                    g = d.e(bArr, i, v65Var);
                    i9 = v65Var.a;
                    valueOf3 = Integer.valueOf(i9);
                    unsafe.putObject(t, j, valueOf3);
                    unsafe.putInt(t, j3, i4);
                    return g;
                }
                return i;
            case 56:
            case 65:
                if (i5 == 1) {
                    valueOf = Long.valueOf(d.k(bArr, i));
                    unsafe.putObject(t, j, valueOf);
                    g = i + 8;
                    unsafe.putInt(t, j3, i4);
                    return g;
                }
                return i;
            case 57:
            case 64:
                if (i5 == 5) {
                    valueOf2 = Integer.valueOf(d.h(bArr, i));
                    unsafe.putObject(t, j, valueOf2);
                    g = i + 4;
                    unsafe.putInt(t, j3, i4);
                    return g;
                }
                return i;
            case 58:
                if (i5 == 0) {
                    g = d.g(bArr, i, v65Var);
                    valueOf3 = Boolean.valueOf(v65Var.b != 0);
                    unsafe.putObject(t, j, valueOf3);
                    unsafe.putInt(t, j3, i4);
                    return g;
                }
                return i;
            case 59:
                if (i5 == 2) {
                    g = d.e(bArr, i, v65Var);
                    i10 = v65Var.a;
                    if (i10 == 0) {
                        valueOf3 = "";
                        unsafe.putObject(t, j, valueOf3);
                        unsafe.putInt(t, j3, i4);
                        return g;
                    } else if ((i6 & 536870912) == 0 || zi5.i(bArr, g, g + i10)) {
                        unsafe.putObject(t, j, new String(bArr, g, i10, gb5.a));
                        g += i10;
                        unsafe.putInt(t, j3, i4);
                        return g;
                    } else {
                        throw zzco.zzbp();
                    }
                }
                return i;
            case 60:
                if (i5 == 2) {
                    g = m(C(i8), bArr, i, i2, v65Var);
                    Object object = unsafe.getInt(t, j3) == i4 ? unsafe.getObject(t, j) : null;
                    valueOf3 = v65Var.c;
                    if (object != null) {
                        valueOf3 = gb5.d(object, valueOf3);
                    }
                    unsafe.putObject(t, j, valueOf3);
                    unsafe.putInt(t, j3, i4);
                    return g;
                }
                return i;
            case 61:
                if (i5 == 2) {
                    g = d.e(bArr, i, v65Var);
                    i10 = v65Var.a;
                    if (i10 == 0) {
                        valueOf3 = zzbb.zzfi;
                        unsafe.putObject(t, j, valueOf3);
                        unsafe.putInt(t, j3, i4);
                        return g;
                    }
                    unsafe.putObject(t, j, zzbb.zzb(bArr, g, i10));
                    g += i10;
                    unsafe.putInt(t, j3, i4);
                    return g;
                }
                return i;
            case 63:
                if (i5 == 0) {
                    int e = d.e(bArr, i, v65Var);
                    int i11 = v65Var.a;
                    mb5<?> E = E(i8);
                    if (E != null && E.h(i11) == null) {
                        T(t).e(i3, Long.valueOf(i11));
                        return e;
                    }
                    unsafe.putObject(t, j, Integer.valueOf(i11));
                    g = e;
                    unsafe.putInt(t, j3, i4);
                    return g;
                }
                return i;
            case 66:
                if (i5 == 0) {
                    g = d.e(bArr, i, v65Var);
                    i9 = g.e(v65Var.a);
                    valueOf3 = Integer.valueOf(i9);
                    unsafe.putObject(t, j, valueOf3);
                    unsafe.putInt(t, j3, i4);
                    return g;
                }
                return i;
            case 67:
                if (i5 == 0) {
                    g = d.g(bArr, i, v65Var);
                    j2 = g.a(v65Var.b);
                    valueOf3 = Long.valueOf(j2);
                    unsafe.putObject(t, j, valueOf3);
                    unsafe.putInt(t, j3, i4);
                    return g;
                }
                return i;
            case 68:
                if (i5 == 3) {
                    g = l(C(i8), bArr, i, i2, (i3 & (-8)) | 4, v65Var);
                    Object object2 = unsafe.getInt(t, j3) == i4 ? unsafe.getObject(t, j) : null;
                    valueOf3 = v65Var.c;
                    if (object2 != null) {
                        valueOf3 = gb5.d(object2, valueOf3);
                    }
                    unsafe.putObject(t, j, valueOf3);
                    unsafe.putInt(t, j3, i4);
                    return g;
                }
                return i;
            default:
                return i;
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:124:0x0233, code lost:
        if (r29.b != 0) goto L141;
     */
    /* JADX WARN: Code restructure failed: missing block: B:125:0x0235, code lost:
        r6 = true;
     */
    /* JADX WARN: Code restructure failed: missing block: B:126:0x0237, code lost:
        r6 = false;
     */
    /* JADX WARN: Code restructure failed: missing block: B:127:0x0238, code lost:
        r12.i(r6);
     */
    /* JADX WARN: Code restructure failed: missing block: B:128:0x023b, code lost:
        if (r4 >= r19) goto L254;
     */
    /* JADX WARN: Code restructure failed: missing block: B:129:0x023d, code lost:
        r6 = com.google.android.gms.internal.clearcut.d.e(r17, r4, r29);
     */
    /* JADX WARN: Code restructure failed: missing block: B:130:0x0243, code lost:
        if (r20 != r29.a) goto L254;
     */
    /* JADX WARN: Code restructure failed: missing block: B:131:0x0245, code lost:
        r4 = com.google.android.gms.internal.clearcut.d.g(r17, r6, r29);
     */
    /* JADX WARN: Code restructure failed: missing block: B:132:0x024d, code lost:
        if (r29.b == 0) goto L148;
     */
    /* JADX WARN: Code restructure failed: missing block: B:242:?, code lost:
        return r1;
     */
    /* JADX WARN: Code restructure failed: missing block: B:243:?, code lost:
        return r1;
     */
    /* JADX WARN: Code restructure failed: missing block: B:63:0x0137, code lost:
        if (r4 == 0) goto L74;
     */
    /* JADX WARN: Code restructure failed: missing block: B:64:0x0139, code lost:
        r12.add(com.google.android.gms.internal.clearcut.zzbb.zzfi);
     */
    /* JADX WARN: Code restructure failed: missing block: B:65:0x013f, code lost:
        r12.add(com.google.android.gms.internal.clearcut.zzbb.zzb(r17, r1, r4));
        r1 = r1 + r4;
     */
    /* JADX WARN: Code restructure failed: missing block: B:66:0x0147, code lost:
        if (r1 >= r19) goto L81;
     */
    /* JADX WARN: Code restructure failed: missing block: B:67:0x0149, code lost:
        r4 = com.google.android.gms.internal.clearcut.d.e(r17, r1, r29);
     */
    /* JADX WARN: Code restructure failed: missing block: B:68:0x014f, code lost:
        if (r20 != r29.a) goto L80;
     */
    /* JADX WARN: Code restructure failed: missing block: B:69:0x0151, code lost:
        r1 = com.google.android.gms.internal.clearcut.d.e(r17, r4, r29);
        r4 = r29.a;
     */
    /* JADX WARN: Code restructure failed: missing block: B:70:0x0157, code lost:
        if (r4 != 0) goto L82;
     */
    /* JADX WARN: Removed duplicated region for block: B:245:? A[RETURN, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:247:? A[RETURN, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:83:0x019a  */
    /* JADX WARN: Removed duplicated region for block: B:97:0x01d4  */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:100:0x01e2 -> B:91:0x01bb). Please submit an issue!!! */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:126:0x0237 -> B:127:0x0238). Please submit an issue!!! */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:132:0x024d -> B:125:0x0235). Please submit an issue!!! */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:65:0x013f -> B:66:0x0147). Please submit an issue!!! */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:70:0x0157 -> B:64:0x0139). Please submit an issue!!! */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:81:0x0194 -> B:82:0x0198). Please submit an issue!!! */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:86:0x01a8 -> B:79:0x0189). Please submit an issue!!! */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:95:0x01ce -> B:96:0x01d2). Please submit an issue!!! */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final int p(T r16, byte[] r17, int r18, int r19, int r20, int r21, int r22, int r23, long r24, int r26, long r27, defpackage.v65 r29) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 994
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.clearcut.q.p(java.lang.Object, byte[], int, int, int, int, int, int, long, int, long, v65):int");
    }

    public final <K, V> int q(T t, byte[] bArr, int i, int i2, int i3, int i4, long j, v65 v65Var) throws IOException {
        Unsafe unsafe = r;
        Object D = D(i3);
        Object object = unsafe.getObject(t, j);
        if (this.q.f(object)) {
            Object b = this.q.b(D);
            this.q.c(b, object);
            unsafe.putObject(t, j, b);
            object = b;
        }
        this.q.a(D);
        this.q.h(object);
        int e = d.e(bArr, i, v65Var);
        int i5 = v65Var.a;
        if (i5 < 0 || i5 > i2 - e) {
            throw zzco.zzbl();
        }
        throw null;
    }

    /* JADX WARN: Removed duplicated region for block: B:133:0x0372 A[ADDED_TO_REGION] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final int r(T r27, byte[] r28, int r29, int r30, int r31, defpackage.v65 r32) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 1072
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.clearcut.q.r(java.lang.Object, byte[], int, int, int, v65):int");
    }

    public final <K, V, UT, UB> UB t(int i, int i2, Map<K, V> map, mb5<?> mb5Var, UB ub, v<UT, UB> vVar) {
        sd5<?, ?> a = this.q.a(D(i));
        Iterator<Map.Entry<K, V>> it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<K, V> next = it.next();
            if (mb5Var.h(((Integer) next.getValue()).intValue()) == null) {
                if (ub == null) {
                    ub = vVar.f();
                }
                m85 zzk = zzbb.zzk(n.a(a, next.getKey(), next.getValue()));
                try {
                    n.b(zzk.b(), a, next.getKey(), next.getValue());
                    vVar.b(ub, i2, zzk.a());
                    it.remove();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        return ub;
    }

    public final <K, V> void w(a0 a0Var, int i, Object obj, int i2) throws IOException {
        if (obj != null) {
            a0Var.G(i, this.q.a(D(i2)), this.q.g(obj));
        }
    }

    public final void x(T t, T t2, int i) {
        long F = F(i) & 1048575;
        if (y(t2, i)) {
            Object M = y.M(t, F);
            Object M2 = y.M(t2, F);
            if (M != null && M2 != null) {
                y.i(t, F, gb5.d(M, M2));
                I(t, i);
            } else if (M2 != null) {
                y.i(t, F, M2);
                I(t, i);
            }
        }
    }

    public final boolean y(T t, int i) {
        if (!this.h) {
            int G = G(i);
            return (y.H(t, (long) (G & 1048575)) & (1 << (G >>> 20))) != 0;
        }
        int F = F(i);
        long j = F & 1048575;
        switch ((F & 267386880) >>> 20) {
            case 0:
                return y.L(t, j) != Utils.DOUBLE_EPSILON;
            case 1:
                return y.K(t, j) != Utils.FLOAT_EPSILON;
            case 2:
                return y.I(t, j) != 0;
            case 3:
                return y.I(t, j) != 0;
            case 4:
                return y.H(t, j) != 0;
            case 5:
                return y.I(t, j) != 0;
            case 6:
                return y.H(t, j) != 0;
            case 7:
                return y.J(t, j);
            case 8:
                Object M = y.M(t, j);
                if (M instanceof String) {
                    return !((String) M).isEmpty();
                } else if (M instanceof zzbb) {
                    return !zzbb.zzfi.equals(M);
                } else {
                    throw new IllegalArgumentException();
                }
            case 9:
                return y.M(t, j) != null;
            case 10:
                return !zzbb.zzfi.equals(y.M(t, j));
            case 11:
                return y.H(t, j) != 0;
            case 12:
                return y.H(t, j) != 0;
            case 13:
                return y.H(t, j) != 0;
            case 14:
                return y.I(t, j) != 0;
            case 15:
                return y.H(t, j) != 0;
            case 16:
                return y.I(t, j) != 0;
            case 17:
                return y.M(t, j) != null;
            default:
                throw new IllegalArgumentException();
        }
    }

    public final boolean z(T t, int i, int i2) {
        return y.H(t, (long) (G(i2) & 1048575)) == i;
    }
}
