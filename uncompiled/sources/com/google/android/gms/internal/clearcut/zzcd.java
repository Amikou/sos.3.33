package com.google.android.gms.internal.clearcut;

/* loaded from: classes.dex */
public enum zzcd {
    SCALAR(false),
    VECTOR(true),
    PACKED_VECTOR(true),
    MAP(false);
    
    private final boolean zzjk;

    zzcd(boolean z) {
        this.zzjk = z;
    }
}
