package com.google.android.gms.internal.clearcut;

import com.google.android.gms.internal.clearcut.m;

/* loaded from: classes.dex */
public final class h0 extends m<h0, a> implements je5 {
    private static volatile xe5<h0> zzbg;
    private static final h0 zztx;
    private int zzbb;
    private int zztu;
    private String zztv = "";
    private String zztw = "";

    /* loaded from: classes.dex */
    public static final class a extends m.a<h0, a> implements je5 {
        public a() {
            super(h0.zztx);
        }

        public /* synthetic */ a(i0 i0Var) {
            this();
        }
    }

    static {
        h0 h0Var = new h0();
        zztx = h0Var;
        m.n(h0.class, h0Var);
    }

    /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.clearcut.m$b, xe5<com.google.android.gms.internal.clearcut.h0>] */
    @Override // com.google.android.gms.internal.clearcut.m
    public final Object i(int i, Object obj, Object obj2) {
        xe5<h0> xe5Var;
        switch (i0.a[i - 1]) {
            case 1:
                return new h0();
            case 2:
                return new a(null);
            case 3:
                return m.k(zztx, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0004\u0000\u0000\u0000\u0001\u0004\u0000\u0002\b\u0001\u0003\b\u0002", new Object[]{"zzbb", "zztu", "zztv", "zztw"});
            case 4:
                return zztx;
            case 5:
                xe5<h0> xe5Var2 = zzbg;
                xe5<h0> xe5Var3 = xe5Var2;
                if (xe5Var2 == null) {
                    synchronized (h0.class) {
                        xe5<h0> xe5Var4 = zzbg;
                        xe5Var = xe5Var4;
                        if (xe5Var4 == null) {
                            ?? bVar = new m.b(zztx);
                            zzbg = bVar;
                            xe5Var = bVar;
                        }
                    }
                    xe5Var3 = xe5Var;
                }
                return xe5Var3;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
