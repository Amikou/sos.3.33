package com.google.android.gms.internal.clearcut;

import com.google.android.gms.internal.clearcut.m;
import java.io.IOException;
import java.util.Map;

/* loaded from: classes.dex */
public final class k extends j<m.d> {
    @Override // com.google.android.gms.internal.clearcut.j
    public final int a(Map.Entry<?, ?> entry) {
        return ((m.d) entry.getKey()).a;
    }

    @Override // com.google.android.gms.internal.clearcut.j
    public final l<m.d> b(Object obj) {
        return ((m.c) obj).zzjv;
    }

    @Override // com.google.android.gms.internal.clearcut.j
    public final void c(a0 a0Var, Map.Entry<?, ?> entry) throws IOException {
        m.d dVar = (m.d) entry.getKey();
        switch (u95.a[dVar.f0.ordinal()]) {
            case 1:
                a0Var.r(dVar.a, ((Double) entry.getValue()).doubleValue());
                return;
            case 2:
                a0Var.s(dVar.a, ((Float) entry.getValue()).floatValue());
                return;
            case 3:
                a0Var.K(dVar.a, ((Long) entry.getValue()).longValue());
                return;
            case 4:
                a0Var.q(dVar.a, ((Long) entry.getValue()).longValue());
                return;
            case 5:
                a0Var.f(dVar.a, ((Integer) entry.getValue()).intValue());
                return;
            case 6:
                a0Var.g(dVar.a, ((Long) entry.getValue()).longValue());
                return;
            case 7:
                a0Var.u(dVar.a, ((Integer) entry.getValue()).intValue());
                return;
            case 8:
                a0Var.H(dVar.a, ((Boolean) entry.getValue()).booleanValue());
                return;
            case 9:
                a0Var.A(dVar.a, ((Integer) entry.getValue()).intValue());
                return;
            case 10:
                a0Var.R(dVar.a, ((Integer) entry.getValue()).intValue());
                return;
            case 11:
                a0Var.E(dVar.a, ((Long) entry.getValue()).longValue());
                return;
            case 12:
                a0Var.C(dVar.a, ((Integer) entry.getValue()).intValue());
                return;
            case 13:
                a0Var.b(dVar.a, ((Long) entry.getValue()).longValue());
                return;
            case 14:
                a0Var.f(dVar.a, ((Integer) entry.getValue()).intValue());
                return;
            case 15:
                a0Var.M(dVar.a, (zzbb) entry.getValue());
                return;
            case 16:
                a0Var.n(dVar.a, (String) entry.getValue());
                return;
            case 17:
                a0Var.P(dVar.a, entry.getValue(), rf5.a().b(entry.getValue().getClass()));
                return;
            case 18:
                a0Var.N(dVar.a, entry.getValue(), rf5.a().b(entry.getValue().getClass()));
                return;
            default:
                return;
        }
    }

    @Override // com.google.android.gms.internal.clearcut.j
    public final void d(Object obj, l<m.d> lVar) {
        ((m.c) obj).zzjv = lVar;
    }

    @Override // com.google.android.gms.internal.clearcut.j
    public final l<m.d> e(Object obj) {
        l<m.d> b = b(obj);
        if (b.c()) {
            l<m.d> lVar = (l) b.clone();
            d(obj, lVar);
            return lVar;
        }
        return b;
    }

    @Override // com.google.android.gms.internal.clearcut.j
    public final void f(Object obj) {
        b(obj).t();
    }

    @Override // com.google.android.gms.internal.clearcut.j
    public final boolean g(o oVar) {
        return oVar instanceof m.c;
    }
}
