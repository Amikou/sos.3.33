package com.google.android.gms.internal.clearcut;

import com.google.android.gms.internal.clearcut.m;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/* loaded from: classes.dex */
public final class i implements a0 {
    public final zzbn a;

    public i(zzbn zzbnVar) {
        zzbn zzbnVar2 = (zzbn) gb5.e(zzbnVar, "output");
        this.a = zzbnVar2;
        zzbnVar2.a = this;
    }

    public static i d(zzbn zzbnVar) {
        i iVar = zzbnVar.a;
        return iVar != null ? iVar : new i(zzbnVar);
    }

    @Override // com.google.android.gms.internal.clearcut.a0
    public final void A(int i, int i2) throws IOException {
        this.a.b0(i, i2);
    }

    @Override // com.google.android.gms.internal.clearcut.a0
    public final void B(int i, List<Float> list, boolean z) throws IOException {
        int i2 = 0;
        if (!z) {
            while (i2 < list.size()) {
                this.a.k(i, list.get(i2).floatValue());
                i2++;
            }
            return;
        }
        this.a.G(i, 2);
        int i3 = 0;
        for (int i4 = 0; i4 < list.size(); i4++) {
            i3 += zzbn.x(list.get(i4).floatValue());
        }
        this.a.y0(i3);
        while (i2 < list.size()) {
            this.a.i(list.get(i2).floatValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.clearcut.a0
    public final void C(int i, int i2) throws IOException {
        this.a.f0(i, i2);
    }

    @Override // com.google.android.gms.internal.clearcut.a0
    public final void D(int i, List<String> list) throws IOException {
        int i2 = 0;
        if (!(list instanceof jc5)) {
            while (i2 < list.size()) {
                this.a.p(i, list.get(i2));
                i2++;
            }
            return;
        }
        jc5 jc5Var = (jc5) list;
        while (i2 < list.size()) {
            Object j = jc5Var.j(i2);
            if (j instanceof String) {
                this.a.p(i, (String) j);
            } else {
                this.a.m(i, (zzbb) j);
            }
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.clearcut.a0
    public final void E(int i, long j) throws IOException {
        this.a.U(i, j);
    }

    @Override // com.google.android.gms.internal.clearcut.a0
    public final void F(int i, List<?> list, t tVar) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            P(i, list.get(i2), tVar);
        }
    }

    @Override // com.google.android.gms.internal.clearcut.a0
    public final <K, V> void G(int i, sd5<K, V> sd5Var, Map<K, V> map) throws IOException {
        for (Map.Entry<K, V> entry : map.entrySet()) {
            this.a.G(i, 2);
            this.a.y0(n.a(sd5Var, entry.getKey(), entry.getValue()));
            n.b(this.a, sd5Var, entry.getKey(), entry.getValue());
        }
    }

    @Override // com.google.android.gms.internal.clearcut.a0
    public final void H(int i, boolean z) throws IOException {
        this.a.K(i, z);
    }

    @Override // com.google.android.gms.internal.clearcut.a0
    public final void I(int i, List<?> list, t tVar) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            N(i, list.get(i2), tVar);
        }
    }

    @Override // com.google.android.gms.internal.clearcut.a0
    public final void J(int i) throws IOException {
        this.a.G(i, 4);
    }

    @Override // com.google.android.gms.internal.clearcut.a0
    public final void K(int i, long j) throws IOException {
        this.a.l(i, j);
    }

    @Override // com.google.android.gms.internal.clearcut.a0
    public final void L(int i, int i2) throws IOException {
        this.a.T(i, i2);
    }

    @Override // com.google.android.gms.internal.clearcut.a0
    public final void M(int i, zzbb zzbbVar) throws IOException {
        this.a.m(i, zzbbVar);
    }

    @Override // com.google.android.gms.internal.clearcut.a0
    public final void N(int i, Object obj, t tVar) throws IOException {
        this.a.o(i, (o) obj, tVar);
    }

    @Override // com.google.android.gms.internal.clearcut.a0
    public final void O(int i) throws IOException {
        this.a.G(i, 3);
    }

    @Override // com.google.android.gms.internal.clearcut.a0
    public final void P(int i, Object obj, t tVar) throws IOException {
        zzbn zzbnVar = this.a;
        zzbnVar.G(i, 3);
        tVar.e((o) obj, zzbnVar.a);
        zzbnVar.G(i, 4);
    }

    @Override // com.google.android.gms.internal.clearcut.a0
    public final int Q() {
        return m.e.k;
    }

    @Override // com.google.android.gms.internal.clearcut.a0
    public final void R(int i, int i2) throws IOException {
        this.a.i0(i, i2);
    }

    @Override // com.google.android.gms.internal.clearcut.a0
    public final void a(int i, List<Boolean> list, boolean z) throws IOException {
        int i2 = 0;
        if (!z) {
            while (i2 < list.size()) {
                this.a.K(i, list.get(i2).booleanValue());
                i2++;
            }
            return;
        }
        this.a.G(i, 2);
        int i3 = 0;
        for (int i4 = 0; i4 < list.size(); i4++) {
            i3 += zzbn.F(list.get(i4).booleanValue());
        }
        this.a.y0(i3);
        while (i2 < list.size()) {
            this.a.t(list.get(i2).booleanValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.clearcut.a0
    public final void b(int i, long j) throws IOException {
        this.a.H(i, j);
    }

    @Override // com.google.android.gms.internal.clearcut.a0
    public final void c(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (!z) {
            while (i2 < list.size()) {
                this.a.T(i, list.get(i2).intValue());
                i2++;
            }
            return;
        }
        this.a.G(i, 2);
        int i3 = 0;
        for (int i4 = 0; i4 < list.size(); i4++) {
            i3 += zzbn.C0(list.get(i4).intValue());
        }
        this.a.y0(i3);
        while (i2 < list.size()) {
            this.a.x0(list.get(i2).intValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.clearcut.a0
    public final void e(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (!z) {
            while (i2 < list.size()) {
                this.a.T(i, list.get(i2).intValue());
                i2++;
            }
            return;
        }
        this.a.G(i, 2);
        int i3 = 0;
        for (int i4 = 0; i4 < list.size(); i4++) {
            i3 += zzbn.H0(list.get(i4).intValue());
        }
        this.a.y0(i3);
        while (i2 < list.size()) {
            this.a.x0(list.get(i2).intValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.clearcut.a0
    public final void f(int i, int i2) throws IOException {
        this.a.T(i, i2);
    }

    @Override // com.google.android.gms.internal.clearcut.a0
    public final void g(int i, long j) throws IOException {
        this.a.U(i, j);
    }

    @Override // com.google.android.gms.internal.clearcut.a0
    public final void j(int i, Object obj) throws IOException {
        if (obj instanceof zzbb) {
            this.a.I(i, (zzbb) obj);
        } else {
            this.a.J(i, (o) obj);
        }
    }

    @Override // com.google.android.gms.internal.clearcut.a0
    public final void k(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (!z) {
            while (i2 < list.size()) {
                this.a.l(i, list.get(i2).longValue());
                i2++;
            }
            return;
        }
        this.a.G(i, 2);
        int i3 = 0;
        for (int i4 = 0; i4 < list.size(); i4++) {
            i3 += zzbn.e0(list.get(i4).longValue());
        }
        this.a.y0(i3);
        while (i2 < list.size()) {
            this.a.L(list.get(i2).longValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.clearcut.a0
    public final void l(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (!z) {
            while (i2 < list.size()) {
                this.a.U(i, list.get(i2).longValue());
                i2++;
            }
            return;
        }
        this.a.G(i, 2);
        int i3 = 0;
        for (int i4 = 0; i4 < list.size(); i4++) {
            i3 += zzbn.s0(list.get(i4).longValue());
        }
        this.a.y0(i3);
        while (i2 < list.size()) {
            this.a.c0(list.get(i2).longValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.clearcut.a0
    public final void m(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (!z) {
            while (i2 < list.size()) {
                this.a.i0(i, list.get(i2).intValue());
                i2++;
            }
            return;
        }
        this.a.G(i, 2);
        int i3 = 0;
        for (int i4 = 0; i4 < list.size(); i4++) {
            i3 += zzbn.F0(list.get(i4).intValue());
        }
        this.a.y0(i3);
        while (i2 < list.size()) {
            this.a.A0(list.get(i2).intValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.clearcut.a0
    public final void n(int i, String str) throws IOException {
        this.a.p(i, str);
    }

    @Override // com.google.android.gms.internal.clearcut.a0
    public final void o(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (!z) {
            while (i2 < list.size()) {
                this.a.b0(i, list.get(i2).intValue());
                i2++;
            }
            return;
        }
        this.a.G(i, 2);
        int i3 = 0;
        for (int i4 = 0; i4 < list.size(); i4++) {
            i3 += zzbn.D0(list.get(i4).intValue());
        }
        this.a.y0(i3);
        while (i2 < list.size()) {
            this.a.y0(list.get(i2).intValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.clearcut.a0
    public final void p(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (!z) {
            while (i2 < list.size()) {
                this.a.i0(i, list.get(i2).intValue());
                i2++;
            }
            return;
        }
        this.a.G(i, 2);
        int i3 = 0;
        for (int i4 = 0; i4 < list.size(); i4++) {
            i3 += zzbn.G0(list.get(i4).intValue());
        }
        this.a.y0(i3);
        while (i2 < list.size()) {
            this.a.A0(list.get(i2).intValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.clearcut.a0
    public final void q(int i, long j) throws IOException {
        this.a.l(i, j);
    }

    @Override // com.google.android.gms.internal.clearcut.a0
    public final void r(int i, double d) throws IOException {
        this.a.j(i, d);
    }

    @Override // com.google.android.gms.internal.clearcut.a0
    public final void s(int i, float f) throws IOException {
        this.a.k(i, f);
    }

    @Override // com.google.android.gms.internal.clearcut.a0
    public final void t(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (!z) {
            while (i2 < list.size()) {
                this.a.U(i, list.get(i2).longValue());
                i2++;
            }
            return;
        }
        this.a.G(i, 2);
        int i3 = 0;
        for (int i4 = 0; i4 < list.size(); i4++) {
            i3 += zzbn.p0(list.get(i4).longValue());
        }
        this.a.y0(i3);
        while (i2 < list.size()) {
            this.a.c0(list.get(i2).longValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.clearcut.a0
    public final void u(int i, int i2) throws IOException {
        this.a.i0(i, i2);
    }

    @Override // com.google.android.gms.internal.clearcut.a0
    public final void v(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (!z) {
            while (i2 < list.size()) {
                this.a.H(i, list.get(i2).longValue());
                i2++;
            }
            return;
        }
        this.a.G(i, 2);
        int i3 = 0;
        for (int i4 = 0; i4 < list.size(); i4++) {
            i3 += zzbn.l0(list.get(i4).longValue());
        }
        this.a.y0(i3);
        while (i2 < list.size()) {
            this.a.V(list.get(i2).longValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.clearcut.a0
    public final void w(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (!z) {
            while (i2 < list.size()) {
                this.a.l(i, list.get(i2).longValue());
                i2++;
            }
            return;
        }
        this.a.G(i, 2);
        int i3 = 0;
        for (int i4 = 0; i4 < list.size(); i4++) {
            i3 += zzbn.h0(list.get(i4).longValue());
        }
        this.a.y0(i3);
        while (i2 < list.size()) {
            this.a.L(list.get(i2).longValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.clearcut.a0
    public final void x(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (!z) {
            while (i2 < list.size()) {
                this.a.f0(i, list.get(i2).intValue());
                i2++;
            }
            return;
        }
        this.a.G(i, 2);
        int i3 = 0;
        for (int i4 = 0; i4 < list.size(); i4++) {
            i3 += zzbn.E0(list.get(i4).intValue());
        }
        this.a.y0(i3);
        while (i2 < list.size()) {
            this.a.z0(list.get(i2).intValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.clearcut.a0
    public final void y(int i, List<Double> list, boolean z) throws IOException {
        int i2 = 0;
        if (!z) {
            while (i2 < list.size()) {
                this.a.j(i, list.get(i2).doubleValue());
                i2++;
            }
            return;
        }
        this.a.G(i, 2);
        int i3 = 0;
        for (int i4 = 0; i4 < list.size(); i4++) {
            i3 += zzbn.w(list.get(i4).doubleValue());
        }
        this.a.y0(i3);
        while (i2 < list.size()) {
            this.a.h(list.get(i2).doubleValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.clearcut.a0
    public final void z(int i, List<zzbb> list) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            this.a.m(i, list.get(i2));
        }
    }
}
