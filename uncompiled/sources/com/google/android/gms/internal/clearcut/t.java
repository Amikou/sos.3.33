package com.google.android.gms.internal.clearcut;

import java.io.IOException;

/* loaded from: classes.dex */
public interface t<T> {
    void a(T t);

    boolean b(T t, T t2);

    T c();

    int d(T t);

    void e(T t, a0 a0Var) throws IOException;

    void f(T t, T t2);

    int g(T t);

    void h(T t, byte[] bArr, int i, int i2, v65 v65Var) throws IOException;

    boolean i(T t);
}
