package com.google.android.gms.internal.clearcut;

import java.io.IOException;
import java.util.Arrays;

/* loaded from: classes.dex */
public final class q0 extends c0<q0> implements Cloneable {
    public long g0 = 0;
    public long h0 = 0;
    public String i0 = "";
    public int j0 = 0;
    public String k0 = "";
    public r0[] l0 = r0.h();
    public byte[] m0;
    public h0 n0;
    public byte[] o0;
    public String p0;
    public String q0;
    public n0 r0;
    public String s0;
    public long t0;
    public o0 u0;
    public byte[] v0;
    public String w0;
    public int[] x0;
    public zzge$zzs y0;
    public boolean z0;

    public q0() {
        byte[] bArr = al5.e;
        this.m0 = bArr;
        this.n0 = null;
        this.o0 = bArr;
        this.p0 = "";
        this.q0 = "";
        this.r0 = null;
        this.s0 = "";
        this.t0 = org.web3j.ens.a.DEFAULT_SYNC_THRESHOLD;
        this.u0 = null;
        this.v0 = bArr;
        this.w0 = "";
        this.x0 = al5.a;
        this.y0 = null;
        this.z0 = false;
        this.f0 = null;
        this.a = -1;
    }

    @Override // com.google.android.gms.internal.clearcut.c0, com.google.android.gms.internal.clearcut.f0
    public final void a(b0 b0Var) throws IOException {
        long j = this.g0;
        if (j != 0) {
            b0Var.u(1, j);
        }
        String str = this.i0;
        if (str != null && !str.equals("")) {
            b0Var.c(2, this.i0);
        }
        r0[] r0VarArr = this.l0;
        int i = 0;
        if (r0VarArr != null && r0VarArr.length > 0) {
            int i2 = 0;
            while (true) {
                r0[] r0VarArr2 = this.l0;
                if (i2 >= r0VarArr2.length) {
                    break;
                }
                r0 r0Var = r0VarArr2[i2];
                if (r0Var != null) {
                    b0Var.b(3, r0Var);
                }
                i2++;
            }
        }
        byte[] bArr = this.m0;
        byte[] bArr2 = al5.e;
        if (!Arrays.equals(bArr, bArr2)) {
            b0Var.d(4, this.m0);
        }
        if (!Arrays.equals(this.o0, bArr2)) {
            b0Var.d(6, this.o0);
        }
        n0 n0Var = this.r0;
        if (n0Var != null) {
            b0Var.b(7, n0Var);
        }
        String str2 = this.p0;
        if (str2 != null && !str2.equals("")) {
            b0Var.c(8, this.p0);
        }
        h0 h0Var = this.n0;
        if (h0Var != null) {
            b0Var.o(9, h0Var);
        }
        int i3 = this.j0;
        if (i3 != 0) {
            b0Var.l(11, i3);
        }
        String str3 = this.q0;
        if (str3 != null && !str3.equals("")) {
            b0Var.c(13, this.q0);
        }
        String str4 = this.s0;
        if (str4 != null && !str4.equals("")) {
            b0Var.c(14, this.s0);
        }
        long j2 = this.t0;
        if (j2 != org.web3j.ens.a.DEFAULT_SYNC_THRESHOLD) {
            b0Var.j(15, 0);
            b0Var.w(b0.v(j2));
        }
        o0 o0Var = this.u0;
        if (o0Var != null) {
            b0Var.b(16, o0Var);
        }
        long j3 = this.h0;
        if (j3 != 0) {
            b0Var.u(17, j3);
        }
        if (!Arrays.equals(this.v0, bArr2)) {
            b0Var.d(18, this.v0);
        }
        int[] iArr = this.x0;
        if (iArr != null && iArr.length > 0) {
            while (true) {
                int[] iArr2 = this.x0;
                if (i >= iArr2.length) {
                    break;
                }
                b0Var.l(20, iArr2[i]);
                i++;
            }
        }
        zzge$zzs zzge_zzs = this.y0;
        if (zzge_zzs != null) {
            b0Var.o(23, zzge_zzs);
        }
        String str5 = this.w0;
        if (str5 != null && !str5.equals("")) {
            b0Var.c(24, this.w0);
        }
        boolean z = this.z0;
        if (z) {
            b0Var.k(25, z);
        }
        String str6 = this.k0;
        if (str6 != null && !str6.equals("")) {
            b0Var.c(26, this.k0);
        }
        super.a(b0Var);
    }

    @Override // com.google.android.gms.internal.clearcut.c0, com.google.android.gms.internal.clearcut.f0
    public final int e() {
        int[] iArr;
        int e = super.e();
        long j = this.g0;
        if (j != 0) {
            e += b0.m(1, j);
        }
        String str = this.i0;
        if (str != null && !str.equals("")) {
            e += b0.h(2, this.i0);
        }
        r0[] r0VarArr = this.l0;
        int i = 0;
        if (r0VarArr != null && r0VarArr.length > 0) {
            int i2 = 0;
            while (true) {
                r0[] r0VarArr2 = this.l0;
                if (i2 >= r0VarArr2.length) {
                    break;
                }
                r0 r0Var = r0VarArr2[i2];
                if (r0Var != null) {
                    e += b0.g(3, r0Var);
                }
                i2++;
            }
        }
        byte[] bArr = this.m0;
        byte[] bArr2 = al5.e;
        if (!Arrays.equals(bArr, bArr2)) {
            e += b0.i(4, this.m0);
        }
        if (!Arrays.equals(this.o0, bArr2)) {
            e += b0.i(6, this.o0);
        }
        n0 n0Var = this.r0;
        if (n0Var != null) {
            e += b0.g(7, n0Var);
        }
        String str2 = this.p0;
        if (str2 != null && !str2.equals("")) {
            e += b0.h(8, this.p0);
        }
        h0 h0Var = this.n0;
        if (h0Var != null) {
            e += zzbn.O(9, h0Var);
        }
        int i3 = this.j0;
        if (i3 != 0) {
            e += b0.y(11) + b0.z(i3);
        }
        String str3 = this.q0;
        if (str3 != null && !str3.equals("")) {
            e += b0.h(13, this.q0);
        }
        String str4 = this.s0;
        if (str4 != null && !str4.equals("")) {
            e += b0.h(14, this.s0);
        }
        long j2 = this.t0;
        if (j2 != org.web3j.ens.a.DEFAULT_SYNC_THRESHOLD) {
            e += b0.y(15) + b0.x(b0.v(j2));
        }
        o0 o0Var = this.u0;
        if (o0Var != null) {
            e += b0.g(16, o0Var);
        }
        long j3 = this.h0;
        if (j3 != 0) {
            e += b0.m(17, j3);
        }
        if (!Arrays.equals(this.v0, bArr2)) {
            e += b0.i(18, this.v0);
        }
        int[] iArr2 = this.x0;
        if (iArr2 != null && iArr2.length > 0) {
            int i4 = 0;
            while (true) {
                iArr = this.x0;
                if (i >= iArr.length) {
                    break;
                }
                i4 += b0.z(iArr[i]);
                i++;
            }
            e = e + i4 + (iArr.length * 2);
        }
        zzge$zzs zzge_zzs = this.y0;
        if (zzge_zzs != null) {
            e += zzbn.O(23, zzge_zzs);
        }
        String str5 = this.w0;
        if (str5 != null && !str5.equals("")) {
            e += b0.h(24, this.w0);
        }
        if (this.z0) {
            e += b0.y(25) + 1;
        }
        String str6 = this.k0;
        return (str6 == null || str6.equals("")) ? e : e + b0.h(26, this.k0);
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof q0) {
            q0 q0Var = (q0) obj;
            if (this.g0 == q0Var.g0 && this.h0 == q0Var.h0) {
                String str = this.i0;
                if (str == null) {
                    if (q0Var.i0 != null) {
                        return false;
                    }
                } else if (!str.equals(q0Var.i0)) {
                    return false;
                }
                if (this.j0 != q0Var.j0) {
                    return false;
                }
                String str2 = this.k0;
                if (str2 == null) {
                    if (q0Var.k0 != null) {
                        return false;
                    }
                } else if (!str2.equals(q0Var.k0)) {
                    return false;
                }
                if (lk5.c(this.l0, q0Var.l0) && Arrays.equals(this.m0, q0Var.m0)) {
                    h0 h0Var = this.n0;
                    if (h0Var == null) {
                        if (q0Var.n0 != null) {
                            return false;
                        }
                    } else if (!h0Var.equals(q0Var.n0)) {
                        return false;
                    }
                    if (Arrays.equals(this.o0, q0Var.o0)) {
                        String str3 = this.p0;
                        if (str3 == null) {
                            if (q0Var.p0 != null) {
                                return false;
                            }
                        } else if (!str3.equals(q0Var.p0)) {
                            return false;
                        }
                        String str4 = this.q0;
                        if (str4 == null) {
                            if (q0Var.q0 != null) {
                                return false;
                            }
                        } else if (!str4.equals(q0Var.q0)) {
                            return false;
                        }
                        n0 n0Var = this.r0;
                        if (n0Var == null) {
                            if (q0Var.r0 != null) {
                                return false;
                            }
                        } else if (!n0Var.equals(q0Var.r0)) {
                            return false;
                        }
                        String str5 = this.s0;
                        if (str5 == null) {
                            if (q0Var.s0 != null) {
                                return false;
                            }
                        } else if (!str5.equals(q0Var.s0)) {
                            return false;
                        }
                        if (this.t0 != q0Var.t0) {
                            return false;
                        }
                        o0 o0Var = this.u0;
                        if (o0Var == null) {
                            if (q0Var.u0 != null) {
                                return false;
                            }
                        } else if (!o0Var.equals(q0Var.u0)) {
                            return false;
                        }
                        if (Arrays.equals(this.v0, q0Var.v0)) {
                            String str6 = this.w0;
                            if (str6 == null) {
                                if (q0Var.w0 != null) {
                                    return false;
                                }
                            } else if (!str6.equals(q0Var.w0)) {
                                return false;
                            }
                            if (lk5.a(this.x0, q0Var.x0)) {
                                zzge$zzs zzge_zzs = this.y0;
                                if (zzge_zzs == null) {
                                    if (q0Var.y0 != null) {
                                        return false;
                                    }
                                } else if (!zzge_zzs.equals(q0Var.y0)) {
                                    return false;
                                }
                                if (this.z0 != q0Var.z0) {
                                    return false;
                                }
                                d0 d0Var = this.f0;
                                if (d0Var == null || d0Var.a()) {
                                    d0 d0Var2 = q0Var.f0;
                                    return d0Var2 == null || d0Var2.a();
                                }
                                return this.f0.equals(q0Var.f0);
                            }
                            return false;
                        }
                        return false;
                    }
                    return false;
                }
                return false;
            }
            return false;
        }
        return false;
    }

    @Override // com.google.android.gms.internal.clearcut.c0, com.google.android.gms.internal.clearcut.f0
    public final /* synthetic */ f0 f() throws CloneNotSupportedException {
        return (q0) clone();
    }

    @Override // com.google.android.gms.internal.clearcut.c0
    public final /* synthetic */ q0 g() throws CloneNotSupportedException {
        return (q0) clone();
    }

    @Override // com.google.android.gms.internal.clearcut.c0, com.google.android.gms.internal.clearcut.f0
    /* renamed from: h */
    public final q0 clone() {
        try {
            q0 q0Var = (q0) super.clone();
            r0[] r0VarArr = this.l0;
            if (r0VarArr != null && r0VarArr.length > 0) {
                q0Var.l0 = new r0[r0VarArr.length];
                int i = 0;
                while (true) {
                    r0[] r0VarArr2 = this.l0;
                    if (i >= r0VarArr2.length) {
                        break;
                    }
                    if (r0VarArr2[i] != null) {
                        q0Var.l0[i] = (r0) r0VarArr2[i].clone();
                    }
                    i++;
                }
            }
            h0 h0Var = this.n0;
            if (h0Var != null) {
                q0Var.n0 = h0Var;
            }
            n0 n0Var = this.r0;
            if (n0Var != null) {
                q0Var.r0 = (n0) n0Var.clone();
            }
            o0 o0Var = this.u0;
            if (o0Var != null) {
                q0Var.u0 = (o0) o0Var.clone();
            }
            int[] iArr = this.x0;
            if (iArr != null && iArr.length > 0) {
                q0Var.x0 = (int[]) iArr.clone();
            }
            zzge$zzs zzge_zzs = this.y0;
            if (zzge_zzs != null) {
                q0Var.y0 = zzge_zzs;
            }
            return q0Var;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    public final int hashCode() {
        long j = this.g0;
        long j2 = this.h0;
        int hashCode = (((((q0.class.getName().hashCode() + 527) * 31) + ((int) (j ^ (j >>> 32)))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31 * 31;
        String str = this.i0;
        int i = 0;
        int hashCode2 = (((hashCode + (str == null ? 0 : str.hashCode())) * 31) + this.j0) * 31;
        String str2 = this.k0;
        int hashCode3 = ((((((hashCode2 + (str2 == null ? 0 : str2.hashCode())) * 31 * 31) + 1237) * 31) + lk5.f(this.l0)) * 31) + Arrays.hashCode(this.m0);
        h0 h0Var = this.n0;
        int hashCode4 = ((((hashCode3 * 31) + (h0Var == null ? 0 : h0Var.hashCode())) * 31) + Arrays.hashCode(this.o0)) * 31;
        String str3 = this.p0;
        int hashCode5 = (hashCode4 + (str3 == null ? 0 : str3.hashCode())) * 31;
        String str4 = this.q0;
        int hashCode6 = hashCode5 + (str4 == null ? 0 : str4.hashCode());
        n0 n0Var = this.r0;
        int hashCode7 = ((hashCode6 * 31) + (n0Var == null ? 0 : n0Var.hashCode())) * 31;
        String str5 = this.s0;
        int hashCode8 = str5 == null ? 0 : str5.hashCode();
        long j3 = this.t0;
        o0 o0Var = this.u0;
        int hashCode9 = (((((((hashCode7 + hashCode8) * 31) + ((int) (j3 ^ (j3 >>> 32)))) * 31) + (o0Var == null ? 0 : o0Var.hashCode())) * 31) + Arrays.hashCode(this.v0)) * 31;
        String str6 = this.w0;
        int hashCode10 = str6 == null ? 0 : str6.hashCode();
        zzge$zzs zzge_zzs = this.y0;
        int d = (((((((hashCode9 + hashCode10) * 31 * 31) + lk5.d(this.x0)) * 31 * 31) + (zzge_zzs == null ? 0 : zzge_zzs.hashCode())) * 31) + (this.z0 ? 1231 : 1237)) * 31;
        d0 d0Var = this.f0;
        if (d0Var != null && !d0Var.a()) {
            i = this.f0.hashCode();
        }
        return d + i;
    }
}
