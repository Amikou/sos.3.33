package com.google.android.gms.internal.clearcut;

/* loaded from: classes.dex */
public final class h extends g {
    public int a;
    public int b;
    public int c;
    public int d;
    public int e;

    public h(byte[] bArr, int i, int i2, boolean z) {
        super();
        this.e = Integer.MAX_VALUE;
        this.a = i2 + i;
        this.c = i;
        this.d = i;
    }

    @Override // com.google.android.gms.internal.clearcut.g
    public final int c() {
        return this.c - this.d;
    }

    @Override // com.google.android.gms.internal.clearcut.g
    public final int d(int i) throws zzco {
        if (i >= 0) {
            int c = i + c();
            int i2 = this.e;
            if (c <= i2) {
                this.e = c;
                int i3 = this.a + this.b;
                this.a = i3;
                int i4 = i3 - this.d;
                if (i4 > c) {
                    int i5 = i4 - c;
                    this.b = i5;
                    this.a = i3 - i5;
                } else {
                    this.b = 0;
                }
                return i2;
            }
            throw zzco.zzbl();
        }
        throw new zzco("CodedInputStream encountered an embedded string or message which claimed to have negative size.");
    }
}
