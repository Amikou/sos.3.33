package com.google.android.gms.internal.clearcut;

import com.google.protobuf.j1;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.RandomAccess;

/* loaded from: classes.dex */
public final class u {
    public static final Class<?> a = C();
    public static final v<?, ?> b = w(false);
    public static final v<?, ?> c = w(true);
    public static final v<?, ?> d = new x();

    public static v<?, ?> A() {
        return c;
    }

    public static v<?, ?> B() {
        return d;
    }

    public static Class<?> C() {
        try {
            return Class.forName("com.google.protobuf.GeneratedMessage");
        } catch (Throwable unused) {
            return null;
        }
    }

    public static Class<?> D() {
        return j1.class;
    }

    public static int E(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof eb5) {
            eb5 eb5Var = (eb5) list;
            i = 0;
            while (i2 < size) {
                i += zzbn.C0(eb5Var.getInt(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += zzbn.C0(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    public static void F(int i, List<Long> list, a0 a0Var, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        a0Var.v(i, list, z);
    }

    public static int G(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof eb5) {
            eb5 eb5Var = (eb5) list;
            i = 0;
            while (i2 < size) {
                i += zzbn.D0(eb5Var.getInt(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += zzbn.D0(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    public static void H(int i, List<Long> list, a0 a0Var, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        a0Var.t(i, list, z);
    }

    public static void I(Class<?> cls) {
        Class<?> cls2;
        if (!m.class.isAssignableFrom(cls) && (cls2 = a) != null && !cls2.isAssignableFrom(cls)) {
            throw new IllegalArgumentException("Message classes must extend GeneratedMessage or GeneratedMessageLite");
        }
    }

    public static int J(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof eb5) {
            eb5 eb5Var = (eb5) list;
            i = 0;
            while (i2 < size) {
                i += zzbn.E0(eb5Var.getInt(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += zzbn.E0(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    public static void K(int i, List<Long> list, a0 a0Var, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        a0Var.l(i, list, z);
    }

    public static int L(List<?> list) {
        return list.size() << 2;
    }

    public static void M(int i, List<Integer> list, a0 a0Var, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        a0Var.c(i, list, z);
    }

    public static int N(List<?> list) {
        return list.size() << 3;
    }

    public static void O(int i, List<Integer> list, a0 a0Var, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        a0Var.o(i, list, z);
    }

    public static int P(List<?> list) {
        return list.size();
    }

    public static void Q(int i, List<Integer> list, a0 a0Var, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        a0Var.x(i, list, z);
    }

    public static void R(int i, List<Integer> list, a0 a0Var, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        a0Var.m(i, list, z);
    }

    public static void S(int i, List<Integer> list, a0 a0Var, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        a0Var.p(i, list, z);
    }

    public static void T(int i, List<Integer> list, a0 a0Var, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        a0Var.e(i, list, z);
    }

    public static void U(int i, List<Boolean> list, a0 a0Var, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        a0Var.a(i, list, z);
    }

    public static int V(int i, List<Long> list, boolean z) {
        if (list.size() == 0) {
            return 0;
        }
        return a(list) + (list.size() * zzbn.B0(i));
    }

    public static int W(int i, List<Long> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return j(list) + (size * zzbn.B0(i));
    }

    public static int X(int i, List<Long> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return q(list) + (size * zzbn.B0(i));
    }

    public static int Y(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return v(list) + (size * zzbn.B0(i));
    }

    public static int Z(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return E(list) + (size * zzbn.B0(i));
    }

    public static int a(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof jd5) {
            jd5 jd5Var = (jd5) list;
            i = 0;
            while (i2 < size) {
                i += zzbn.e0(jd5Var.i(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += zzbn.e0(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    public static int a0(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return G(list) + (size * zzbn.B0(i));
    }

    public static <UT, UB> UB b(int i, int i2, UB ub, v<UT, UB> vVar) {
        if (ub == null) {
            ub = vVar.f();
        }
        vVar.a(ub, i, i2);
        return ub;
    }

    public static int b0(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return J(list) + (size * zzbn.B0(i));
    }

    public static <UT, UB> UB c(int i, List<Integer> list, mb5<?> mb5Var, UB ub, v<UT, UB> vVar) {
        if (mb5Var == null) {
            return ub;
        }
        if (list instanceof RandomAccess) {
            int size = list.size();
            int i2 = 0;
            for (int i3 = 0; i3 < size; i3++) {
                int intValue = list.get(i3).intValue();
                if (mb5Var.h(intValue) != null) {
                    if (i3 != i2) {
                        list.set(i2, Integer.valueOf(intValue));
                    }
                    i2++;
                } else {
                    ub = (UB) b(i, intValue, ub, vVar);
                }
            }
            if (i2 != size) {
                list.subList(i2, size).clear();
            }
        } else {
            Iterator<Integer> it = list.iterator();
            while (it.hasNext()) {
                int intValue2 = it.next().intValue();
                if (mb5Var.h(intValue2) == null) {
                    ub = (UB) b(i, intValue2, ub, vVar);
                    it.remove();
                }
            }
        }
        return ub;
    }

    public static int c0(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return size * zzbn.t0(i, 0);
    }

    public static void d(int i, List<String> list, a0 a0Var) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        a0Var.D(i, list);
    }

    public static int d0(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return size * zzbn.k0(i, 0L);
    }

    public static void e(int i, List<?> list, a0 a0Var, t tVar) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        a0Var.I(i, list, tVar);
    }

    public static int e0(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return size * zzbn.Q(i, true);
    }

    public static void f(int i, List<Double> list, a0 a0Var, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        a0Var.y(i, list, z);
    }

    public static <T, FT extends ta5<FT>> void g(j<FT> jVar, T t, T t2) {
        l<FT> b2 = jVar.b(t2);
        if (b2.b()) {
            return;
        }
        jVar.e(t).h(b2);
    }

    public static <T, UT, UB> void h(v<UT, UB> vVar, T t, T t2) {
        vVar.g(t, vVar.i(vVar.k(t), vVar.k(t2)));
    }

    public static <T> void i(wd5 wd5Var, T t, T t2, long j) {
        y.i(t, j, wd5Var.c(y.M(t, j), y.M(t2, j)));
    }

    public static int j(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof jd5) {
            jd5 jd5Var = (jd5) list;
            i = 0;
            while (i2 < size) {
                i += zzbn.h0(jd5Var.i(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += zzbn.h0(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    public static void k(int i, List<zzbb> list, a0 a0Var) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        a0Var.z(i, list);
    }

    public static void l(int i, List<?> list, a0 a0Var, t tVar) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        a0Var.F(i, list, tVar);
    }

    public static void m(int i, List<Float> list, a0 a0Var, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        a0Var.B(i, list, z);
    }

    public static int n(int i, Object obj, t tVar) {
        return obj instanceof ec5 ? zzbn.d(i, (ec5) obj) : zzbn.A(i, (o) obj, tVar);
    }

    public static int o(int i, List<?> list) {
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        int B0 = zzbn.B0(i) * size;
        if (list instanceof jc5) {
            jc5 jc5Var = (jc5) list;
            while (i2 < size) {
                Object j = jc5Var.j(i2);
                B0 += j instanceof zzbb ? zzbn.D((zzbb) j) : zzbn.q0((String) j);
                i2++;
            }
        } else {
            while (i2 < size) {
                Object obj = list.get(i2);
                B0 += obj instanceof zzbb ? zzbn.D((zzbb) obj) : zzbn.q0((String) obj);
                i2++;
            }
        }
        return B0;
    }

    public static int p(int i, List<?> list, t tVar) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int B0 = zzbn.B0(i) * size;
        for (int i2 = 0; i2 < size; i2++) {
            Object obj = list.get(i2);
            B0 += obj instanceof ec5 ? zzbn.e((ec5) obj) : zzbn.E((o) obj, tVar);
        }
        return B0;
    }

    public static int q(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof jd5) {
            jd5 jd5Var = (jd5) list;
            i = 0;
            while (i2 < size) {
                i += zzbn.l0(jd5Var.i(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += zzbn.l0(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    public static void r(int i, List<Long> list, a0 a0Var, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        a0Var.k(i, list, z);
    }

    public static boolean s(int i, int i2, int i3) {
        if (i2 < 40) {
            return true;
        }
        long j = i3;
        return ((((long) i2) - ((long) i)) + 1) + 9 <= ((2 * j) + 3) + ((j + 3) * 3);
    }

    public static int t(int i, List<zzbb> list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int B0 = size * zzbn.B0(i);
        for (int i2 = 0; i2 < list.size(); i2++) {
            B0 += zzbn.D(list.get(i2));
        }
        return B0;
    }

    public static int u(int i, List<o> list, t tVar) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < size; i3++) {
            i2 += zzbn.P(i, list.get(i3), tVar);
        }
        return i2;
    }

    public static int v(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof eb5) {
            eb5 eb5Var = (eb5) list;
            i = 0;
            while (i2 < size) {
                i += zzbn.H0(eb5Var.getInt(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += zzbn.H0(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    public static v<?, ?> w(boolean z) {
        try {
            Class<?> D = D();
            if (D == null) {
                return null;
            }
            return (v) D.getConstructor(Boolean.TYPE).newInstance(Boolean.valueOf(z));
        } catch (Throwable unused) {
            return null;
        }
    }

    public static void x(int i, List<Long> list, a0 a0Var, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        a0Var.w(i, list, z);
    }

    public static boolean y(Object obj, Object obj2) {
        if (obj != obj2) {
            return obj != null && obj.equals(obj2);
        }
        return true;
    }

    public static v<?, ?> z() {
        return b;
    }
}
