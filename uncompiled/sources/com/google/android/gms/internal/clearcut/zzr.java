package com.google.android.gms.internal.clearcut;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

/* loaded from: classes.dex */
public final class zzr extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzr> CREATOR = new n46();
    public final String a;
    public final int f0;
    public final int g0;
    public final String h0;
    public final String i0;
    public final boolean j0;
    public final String k0;
    public final boolean l0;
    public final int m0;

    public zzr(String str, int i, int i2, String str2, String str3, String str4, boolean z, zzge$zzv$zzb zzge_zzv_zzb) {
        this.a = (String) zt2.j(str);
        this.f0 = i;
        this.g0 = i2;
        this.k0 = str2;
        this.h0 = str3;
        this.i0 = str4;
        this.j0 = !z;
        this.l0 = z;
        this.m0 = zzge_zzv_zzb.zzc();
    }

    public zzr(String str, int i, int i2, String str2, String str3, boolean z, String str4, boolean z2, int i3) {
        this.a = str;
        this.f0 = i;
        this.g0 = i2;
        this.h0 = str2;
        this.i0 = str3;
        this.j0 = z;
        this.k0 = str4;
        this.l0 = z2;
        this.m0 = i3;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof zzr) {
            zzr zzrVar = (zzr) obj;
            if (pl2.a(this.a, zzrVar.a) && this.f0 == zzrVar.f0 && this.g0 == zzrVar.g0 && pl2.a(this.k0, zzrVar.k0) && pl2.a(this.h0, zzrVar.h0) && pl2.a(this.i0, zzrVar.i0) && this.j0 == zzrVar.j0 && this.l0 == zzrVar.l0 && this.m0 == zzrVar.m0) {
                return true;
            }
        }
        return false;
    }

    public final int hashCode() {
        return pl2.b(this.a, Integer.valueOf(this.f0), Integer.valueOf(this.g0), this.k0, this.h0, this.i0, Boolean.valueOf(this.j0), Boolean.valueOf(this.l0), Integer.valueOf(this.m0));
    }

    public final String toString() {
        return "PlayLoggerContext[package=" + this.a + ",packageVersionCode=" + this.f0 + ",logSource=" + this.g0 + ",logSourceName=" + this.k0 + ",uploadAccount=" + this.h0 + ",loggingId=" + this.i0 + ",logAndroidId=" + this.j0 + ",isAnonymous=" + this.l0 + ",qosTier=" + this.m0 + "]";
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int a = yb3.a(parcel);
        yb3.s(parcel, 2, this.a, false);
        yb3.m(parcel, 3, this.f0);
        yb3.m(parcel, 4, this.g0);
        yb3.s(parcel, 5, this.h0, false);
        yb3.s(parcel, 6, this.i0, false);
        yb3.c(parcel, 7, this.j0);
        yb3.s(parcel, 8, this.k0, false);
        yb3.c(parcel, 9, this.l0);
        yb3.m(parcel, 10, this.m0);
        yb3.b(parcel, a);
    }
}
