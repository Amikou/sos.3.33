package com.google.android.gms.internal.clearcut;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.clearcut.zze;

/* loaded from: classes.dex */
public final class v0 extends a implements u0 {
    public v0(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.clearcut.internal.IClearcutLoggerService");
    }

    @Override // com.google.android.gms.internal.clearcut.u0
    public final void F(s0 s0Var, zze zzeVar) throws RemoteException {
        Parcel b = b();
        la5.b(b, s0Var);
        la5.c(b, zzeVar);
        F1(1, b);
    }
}
