package com.google.android.gms.internal.clearcut;

import java.io.IOException;

/* loaded from: classes.dex */
public final class d {
    public static int a(int i, byte[] bArr, int i2, int i3, w wVar, v65 v65Var) throws IOException {
        if ((i >>> 3) != 0) {
            int i4 = i & 7;
            if (i4 == 0) {
                int g = g(bArr, i2, v65Var);
                wVar.e(i, Long.valueOf(v65Var.b));
                return g;
            } else if (i4 == 1) {
                wVar.e(i, Long.valueOf(k(bArr, i2)));
                return i2 + 8;
            } else if (i4 == 2) {
                int e = e(bArr, i2, v65Var);
                int i5 = v65Var.a;
                wVar.e(i, i5 == 0 ? zzbb.zzfi : zzbb.zzb(bArr, e, i5));
                return e + i5;
            } else if (i4 != 3) {
                if (i4 == 5) {
                    wVar.e(i, Integer.valueOf(h(bArr, i2)));
                    return i2 + 4;
                }
                throw zzco.zzbm();
            } else {
                w i6 = w.i();
                int i7 = (i & (-8)) | 4;
                int i8 = 0;
                while (true) {
                    if (i2 >= i3) {
                        break;
                    }
                    int e2 = e(bArr, i2, v65Var);
                    int i9 = v65Var.a;
                    i8 = i9;
                    if (i9 == i7) {
                        i2 = e2;
                        break;
                    }
                    int a = a(i8, bArr, e2, i3, i6, v65Var);
                    i8 = i9;
                    i2 = a;
                }
                if (i2 > i3 || i8 != i7) {
                    throw zzco.zzbo();
                }
                wVar.e(i, i6);
                return i2;
            }
        }
        throw zzco.zzbm();
    }

    public static int b(int i, byte[] bArr, int i2, int i3, v65 v65Var) throws zzco {
        if ((i >>> 3) != 0) {
            int i4 = i & 7;
            if (i4 != 0) {
                if (i4 != 1) {
                    if (i4 != 2) {
                        if (i4 != 3) {
                            if (i4 == 5) {
                                return i2 + 4;
                            }
                            throw zzco.zzbm();
                        }
                        int i5 = (i & (-8)) | 4;
                        int i6 = 0;
                        while (i2 < i3) {
                            i2 = e(bArr, i2, v65Var);
                            i6 = v65Var.a;
                            if (i6 == i5) {
                                break;
                            }
                            i2 = b(i6, bArr, i2, i3, v65Var);
                        }
                        if (i2 > i3 || i6 != i5) {
                            throw zzco.zzbo();
                        }
                        return i2;
                    }
                    return e(bArr, i2, v65Var) + v65Var.a;
                }
                return i2 + 8;
            }
            return g(bArr, i2, v65Var);
        }
        throw zzco.zzbm();
    }

    public static int c(int i, byte[] bArr, int i2, int i3, rb5<?> rb5Var, v65 v65Var) {
        eb5 eb5Var = (eb5) rb5Var;
        int e = e(bArr, i2, v65Var);
        while (true) {
            eb5Var.i(v65Var.a);
            if (e >= i3) {
                break;
            }
            int e2 = e(bArr, e, v65Var);
            if (i != v65Var.a) {
                break;
            }
            e = e(bArr, e2, v65Var);
        }
        return e;
    }

    public static int d(int i, byte[] bArr, int i2, v65 v65Var) {
        int i3;
        int i4;
        int i5 = i & 127;
        int i6 = i2 + 1;
        byte b = bArr[i2];
        if (b < 0) {
            int i7 = i5 | ((b & Byte.MAX_VALUE) << 7);
            int i8 = i6 + 1;
            byte b2 = bArr[i6];
            if (b2 >= 0) {
                i3 = b2 << 14;
            } else {
                i5 = i7 | ((b2 & Byte.MAX_VALUE) << 14);
                i6 = i8 + 1;
                byte b3 = bArr[i8];
                if (b3 >= 0) {
                    i4 = b3 << 21;
                } else {
                    i7 = i5 | ((b3 & Byte.MAX_VALUE) << 21);
                    i8 = i6 + 1;
                    byte b4 = bArr[i6];
                    if (b4 >= 0) {
                        i3 = b4 << 28;
                    } else {
                        int i9 = i7 | ((b4 & Byte.MAX_VALUE) << 28);
                        while (true) {
                            int i10 = i8 + 1;
                            if (bArr[i8] >= 0) {
                                v65Var.a = i9;
                                return i10;
                            }
                            i8 = i10;
                        }
                    }
                }
            }
            v65Var.a = i7 | i3;
            return i8;
        }
        i4 = b << 7;
        v65Var.a = i5 | i4;
        return i6;
    }

    public static int e(byte[] bArr, int i, v65 v65Var) {
        int i2 = i + 1;
        byte b = bArr[i];
        if (b >= 0) {
            v65Var.a = b;
            return i2;
        }
        return d(b, bArr, i2, v65Var);
    }

    public static int f(byte[] bArr, int i, rb5<?> rb5Var, v65 v65Var) throws IOException {
        eb5 eb5Var = (eb5) rb5Var;
        int e = e(bArr, i, v65Var);
        int i2 = v65Var.a + e;
        while (e < i2) {
            e = e(bArr, e, v65Var);
            eb5Var.i(v65Var.a);
        }
        if (e == i2) {
            return e;
        }
        throw zzco.zzbl();
    }

    public static int g(byte[] bArr, int i, v65 v65Var) {
        byte b;
        int i2 = i + 1;
        long j = bArr[i];
        if (j >= 0) {
            v65Var.b = j;
            return i2;
        }
        int i3 = i2 + 1;
        byte b2 = bArr[i2];
        long j2 = (j & 127) | ((b2 & Byte.MAX_VALUE) << 7);
        int i4 = 7;
        while (b2 < 0) {
            int i5 = i3 + 1;
            i4 += 7;
            j2 |= (b & Byte.MAX_VALUE) << i4;
            b2 = bArr[i3];
            i3 = i5;
        }
        v65Var.b = j2;
        return i3;
    }

    public static int h(byte[] bArr, int i) {
        return ((bArr[i + 3] & 255) << 24) | (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << 16);
    }

    public static int i(byte[] bArr, int i, v65 v65Var) {
        int e = e(bArr, i, v65Var);
        int i2 = v65Var.a;
        if (i2 == 0) {
            v65Var.c = "";
            return e;
        }
        v65Var.c = new String(bArr, e, i2, gb5.a);
        return e + i2;
    }

    public static int j(byte[] bArr, int i, v65 v65Var) throws IOException {
        int e = e(bArr, i, v65Var);
        int i2 = v65Var.a;
        if (i2 == 0) {
            v65Var.c = "";
            return e;
        }
        int i3 = e + i2;
        if (zi5.i(bArr, e, i3)) {
            v65Var.c = new String(bArr, e, i2, gb5.a);
            return i3;
        }
        throw zzco.zzbp();
    }

    public static long k(byte[] bArr, int i) {
        return ((bArr[i + 7] & 255) << 56) | (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << 16) | ((bArr[i + 3] & 255) << 24) | ((bArr[i + 4] & 255) << 32) | ((bArr[i + 5] & 255) << 40) | ((bArr[i + 6] & 255) << 48);
    }

    public static double l(byte[] bArr, int i) {
        return Double.longBitsToDouble(k(bArr, i));
    }

    public static int m(byte[] bArr, int i, v65 v65Var) {
        int e = e(bArr, i, v65Var);
        int i2 = v65Var.a;
        if (i2 == 0) {
            v65Var.c = zzbb.zzfi;
            return e;
        }
        v65Var.c = zzbb.zzb(bArr, e, i2);
        return e + i2;
    }

    public static float n(byte[] bArr, int i) {
        return Float.intBitsToFloat(h(bArr, i));
    }
}
