package com.google.android.gms.internal.clearcut;

import java.io.IOException;

/* loaded from: classes.dex */
public interface o extends je5 {
    zzbb e();

    he5 f();

    void g(zzbn zzbnVar) throws IOException;

    he5 h();

    int j();
}
