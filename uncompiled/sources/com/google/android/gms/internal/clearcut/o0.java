package com.google.android.gms.internal.clearcut;

import java.io.IOException;
import java.util.Arrays;

/* loaded from: classes.dex */
public final class o0 extends c0<o0> implements Cloneable {
    public byte[] g0 = al5.e;
    public String h0 = "";
    public byte[][] i0 = al5.d;

    public o0() {
        this.f0 = null;
        this.a = -1;
    }

    @Override // com.google.android.gms.internal.clearcut.c0, com.google.android.gms.internal.clearcut.f0
    public final void a(b0 b0Var) throws IOException {
        if (!Arrays.equals(this.g0, al5.e)) {
            b0Var.d(1, this.g0);
        }
        byte[][] bArr = this.i0;
        if (bArr != null && bArr.length > 0) {
            int i = 0;
            while (true) {
                byte[][] bArr2 = this.i0;
                if (i >= bArr2.length) {
                    break;
                }
                byte[] bArr3 = bArr2[i];
                if (bArr3 != null) {
                    b0Var.d(2, bArr3);
                }
                i++;
            }
        }
        String str = this.h0;
        if (str != null && !str.equals("")) {
            b0Var.c(4, this.h0);
        }
        super.a(b0Var);
    }

    @Override // com.google.android.gms.internal.clearcut.c0, com.google.android.gms.internal.clearcut.f0
    public final int e() {
        int e = super.e();
        if (!Arrays.equals(this.g0, al5.e)) {
            e += b0.i(1, this.g0);
        }
        byte[][] bArr = this.i0;
        if (bArr != null && bArr.length > 0) {
            int i = 0;
            int i2 = 0;
            int i3 = 0;
            while (true) {
                byte[][] bArr2 = this.i0;
                if (i >= bArr2.length) {
                    break;
                }
                byte[] bArr3 = bArr2[i];
                if (bArr3 != null) {
                    i3++;
                    i2 += b0.s(bArr3);
                }
                i++;
            }
            e = e + i2 + (i3 * 1);
        }
        String str = this.h0;
        return (str == null || str.equals("")) ? e : e + b0.h(4, this.h0);
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof o0) {
            o0 o0Var = (o0) obj;
            if (Arrays.equals(this.g0, o0Var.g0)) {
                String str = this.h0;
                if (str == null) {
                    if (o0Var.h0 != null) {
                        return false;
                    }
                } else if (!str.equals(o0Var.h0)) {
                    return false;
                }
                if (lk5.i(this.i0, o0Var.i0)) {
                    d0 d0Var = this.f0;
                    if (d0Var == null || d0Var.a()) {
                        d0 d0Var2 = o0Var.f0;
                        return d0Var2 == null || d0Var2.a();
                    }
                    return this.f0.equals(o0Var.f0);
                }
                return false;
            }
            return false;
        }
        return false;
    }

    @Override // com.google.android.gms.internal.clearcut.c0, com.google.android.gms.internal.clearcut.f0
    public final /* synthetic */ f0 f() throws CloneNotSupportedException {
        return (o0) clone();
    }

    @Override // com.google.android.gms.internal.clearcut.c0
    public final /* synthetic */ o0 g() throws CloneNotSupportedException {
        return (o0) clone();
    }

    @Override // com.google.android.gms.internal.clearcut.c0, com.google.android.gms.internal.clearcut.f0
    /* renamed from: h */
    public final o0 clone() {
        try {
            o0 o0Var = (o0) super.clone();
            byte[][] bArr = this.i0;
            if (bArr != null && bArr.length > 0) {
                o0Var.i0 = (byte[][]) bArr.clone();
            }
            return o0Var;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    public final int hashCode() {
        int hashCode = (((o0.class.getName().hashCode() + 527) * 31) + Arrays.hashCode(this.g0)) * 31;
        String str = this.h0;
        int i = 0;
        int hashCode2 = (((((hashCode + (str == null ? 0 : str.hashCode())) * 31) + lk5.g(this.i0)) * 31) + 1237) * 31;
        d0 d0Var = this.f0;
        if (d0Var != null && !d0Var.a()) {
            i = this.f0.hashCode();
        }
        return hashCode2 + i;
    }
}
