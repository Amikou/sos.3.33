package com.google.android.gms.internal.clearcut;

import java.io.IOException;

/* loaded from: classes.dex */
public abstract class v<T, B> {
    public abstract void a(B b, int i, long j);

    public abstract void b(B b, int i, zzbb zzbbVar);

    public abstract void c(T t, a0 a0Var) throws IOException;

    public abstract void d(Object obj);

    public abstract void e(T t, a0 a0Var) throws IOException;

    public abstract B f();

    public abstract void g(Object obj, T t);

    public abstract void h(Object obj, B b);

    public abstract T i(T t, T t2);

    public abstract int j(T t);

    public abstract T k(Object obj);

    public abstract int l(T t);
}
