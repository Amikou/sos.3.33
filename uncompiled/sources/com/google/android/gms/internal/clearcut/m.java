package com.google.android.gms.internal.clearcut;

import com.google.android.gms.internal.clearcut.m;
import com.google.android.gms.internal.clearcut.m.a;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* loaded from: classes.dex */
public abstract class m<MessageType extends m<MessageType, BuilderType>, BuilderType extends a<MessageType, BuilderType>> extends com.google.android.gms.internal.clearcut.c<MessageType, BuilderType> {
    private static Map<Object, m<?, ?>> zzjr = new ConcurrentHashMap();
    public w zzjp = w.h();
    private int zzjq = -1;

    /* loaded from: classes.dex */
    public static abstract class a<MessageType extends m<MessageType, BuilderType>, BuilderType extends a<MessageType, BuilderType>> extends h65<MessageType, BuilderType> {
        public final MessageType a;
        public MessageType f0;
        public boolean g0 = false;

        public a(MessageType messagetype) {
            this.a = messagetype;
            this.f0 = (MessageType) messagetype.i(e.d, null, null);
        }

        public static void k(MessageType messagetype, MessageType messagetype2) {
            rf5.a().d(messagetype).f(messagetype, messagetype2);
        }

        @Override // defpackage.je5
        public final /* synthetic */ o a() {
            return this.a;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public /* synthetic */ Object clone() throws CloneNotSupportedException {
            a aVar = (a) this.a.i(e.e, null, null);
            aVar.b((m) w());
            return aVar;
        }

        @Override // defpackage.h65
        /* renamed from: d */
        public final BuilderType b(MessageType messagetype) {
            l();
            k(this.f0, messagetype);
            return this;
        }

        public void l() {
            if (this.g0) {
                MessageType messagetype = (MessageType) this.f0.i(e.d, null, null);
                k(messagetype, this.f0);
                this.f0 = messagetype;
                this.g0 = false;
            }
        }

        @Override // defpackage.he5
        public final /* synthetic */ o m() {
            m mVar = (m) w();
            byte byteValue = ((Byte) mVar.i(e.a, null, null)).byteValue();
            boolean z = true;
            if (byteValue != 1) {
                if (byteValue == 0) {
                    z = false;
                } else {
                    z = rf5.a().d(mVar).i(mVar);
                    mVar.i(e.b, z ? mVar : null, null);
                }
            }
            if (z) {
                return mVar;
            }
            throw new zzew(mVar);
        }

        @Override // defpackage.he5
        /* renamed from: o */
        public MessageType w() {
            if (this.g0) {
                return this.f0;
            }
            MessageType messagetype = this.f0;
            rf5.a().d(messagetype).a(messagetype);
            this.g0 = true;
            return this.f0;
        }

        public final MessageType p() {
            MessageType messagetype = (MessageType) w();
            byte byteValue = ((Byte) messagetype.i(e.a, null, null)).byteValue();
            boolean z = true;
            if (byteValue != 1) {
                if (byteValue == 0) {
                    z = false;
                } else {
                    z = rf5.a().d(messagetype).i(messagetype);
                    messagetype.i(e.b, z ? messagetype : null, null);
                }
            }
            if (z) {
                return messagetype;
            }
            throw new zzew(messagetype);
        }
    }

    /* loaded from: classes.dex */
    public static class b<T extends m<T, ?>> extends k65<T> {
        public b(T t) {
        }
    }

    /* loaded from: classes.dex */
    public static abstract class c<MessageType extends c<MessageType, BuilderType>, BuilderType> extends m<MessageType, BuilderType> implements je5 {
        public l<d> zzjv = l.k();
    }

    /* loaded from: classes.dex */
    public static final class d implements ta5<d> {
        public final int a;
        public final zzfl f0;

        /* JADX WARN: Multi-variable type inference failed */
        @Override // defpackage.ta5
        public final he5 F(he5 he5Var, o oVar) {
            return ((a) he5Var).b((m) oVar);
        }

        @Override // defpackage.ta5
        public final qe5 H1(qe5 qe5Var, qe5 qe5Var2) {
            throw new UnsupportedOperationException();
        }

        @Override // defpackage.ta5
        public final boolean J0() {
            return false;
        }

        @Override // java.lang.Comparable
        public final /* synthetic */ int compareTo(Object obj) {
            return this.a - ((d) obj).a;
        }

        @Override // defpackage.ta5
        public final boolean m() {
            return false;
        }

        @Override // defpackage.ta5
        public final zzfq q() {
            return this.f0.zzek();
        }

        @Override // defpackage.ta5
        public final zzfl w() {
            return this.f0;
        }

        @Override // defpackage.ta5
        public final int zzc() {
            return this.a;
        }
    }

    /* loaded from: classes.dex */
    public enum e {
        public static final int a = 1;
        public static final int b = 2;
        public static final int c = 3;
        public static final int d = 4;
        public static final int e = 5;
        public static final int f = 6;
        public static final int g = 7;
        public static final /* synthetic */ int[] h = {1, 2, 3, 4, 5, 6, 7};
        public static final int i = 1;
        public static final int j = 2;
        public static final int k = 1;
        public static final int l = 2;

        public static int[] a() {
            return (int[]) h.clone();
        }
    }

    public static <T extends m<T, ?>> T d(T t, byte[] bArr) throws zzco {
        T t2 = (T) t.i(e.d, null, null);
        try {
            rf5.a().d(t2).h(t2, bArr, 0, bArr.length, new v65());
            rf5.a().d(t2).a(t2);
            if (t2.zzex == 0) {
                return t2;
            }
            throw new RuntimeException();
        } catch (IOException e2) {
            if (e2.getCause() instanceof zzco) {
                throw ((zzco) e2.getCause());
            }
            throw new zzco(e2.getMessage()).zzg(t2);
        } catch (IndexOutOfBoundsException unused) {
            throw zzco.zzbl().zzg(t2);
        }
    }

    public static Object k(o oVar, String str, Object[] objArr) {
        return new tf5(oVar, str, objArr);
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    public static Object l(Method method, Object obj, Object... objArr) {
        try {
            return method.invoke(obj, objArr);
        } catch (IllegalAccessException e2) {
            throw new RuntimeException("Couldn't use Java reflection to implement protocol message reflection.", e2);
        } catch (InvocationTargetException e3) {
            Throwable cause = e3.getCause();
            if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            }
            if (cause instanceof Error) {
                throw ((Error) cause);
            }
            throw new RuntimeException("Unexpected exception thrown by generated accessor method.", cause);
        }
    }

    public static <T extends m<?, ?>> void n(Class<T> cls, T t) {
        zzjr.put(cls, t);
    }

    public static <T extends m<T, ?>> T o(T t, byte[] bArr) throws zzco {
        T t2 = (T) d(t, bArr);
        if (t2 != null) {
            byte byteValue = ((Byte) t2.i(e.a, null, null)).byteValue();
            boolean z = true;
            if (byteValue != 1) {
                if (byteValue == 0) {
                    z = false;
                } else {
                    z = rf5.a().d(t2).i(t2);
                    t2.i(e.b, z ? t2 : null, null);
                }
            }
            if (!z) {
                throw new zzco(new zzew(t2).getMessage()).zzg(t2);
            }
        }
        return t2;
    }

    public static <E> rb5<E> p() {
        return sf5.i();
    }

    public static <T extends m<?, ?>> T q(Class<T> cls) {
        T t = (T) zzjr.get(cls);
        if (t == null) {
            try {
                Class.forName(cls.getName(), true, cls.getClassLoader());
                t = (T) zzjr.get(cls);
            } catch (ClassNotFoundException e2) {
                throw new IllegalStateException("Class initialization cannot fail.", e2);
            }
        }
        if (t == null) {
            String name = cls.getName();
            throw new IllegalStateException(name.length() != 0 ? "Unable to get default instance for: ".concat(name) : new String("Unable to get default instance for: "));
        }
        return t;
    }

    @Override // defpackage.je5
    public final /* synthetic */ o a() {
        return (m) i(e.f, null, null);
    }

    @Override // com.google.android.gms.internal.clearcut.c
    public final void b(int i) {
        this.zzjq = i;
    }

    @Override // com.google.android.gms.internal.clearcut.c
    public final int c() {
        return this.zzjq;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (((m) i(e.f, null, null)).getClass().isInstance(obj)) {
            return rf5.a().d(this).b(this, (m) obj);
        }
        return false;
    }

    @Override // com.google.android.gms.internal.clearcut.o
    public final /* synthetic */ he5 f() {
        a aVar = (a) i(e.e, null, null);
        aVar.b(this);
        return aVar;
    }

    @Override // com.google.android.gms.internal.clearcut.o
    public final void g(zzbn zzbnVar) throws IOException {
        rf5.a().b(getClass()).e(this, i.d(zzbnVar));
    }

    @Override // com.google.android.gms.internal.clearcut.o
    public final /* synthetic */ he5 h() {
        return (a) i(e.e, null, null);
    }

    public int hashCode() {
        int i = this.zzex;
        if (i != 0) {
            return i;
        }
        int d2 = rf5.a().d(this).d(this);
        this.zzex = d2;
        return d2;
    }

    public abstract Object i(int i, Object obj, Object obj2);

    @Override // defpackage.je5
    public final boolean isInitialized() {
        byte byteValue = ((Byte) i(e.a, null, null)).byteValue();
        if (byteValue == 1) {
            return true;
        }
        if (byteValue == 0) {
            return false;
        }
        boolean i = rf5.a().d(this).i(this);
        i(e.b, i ? this : null, null);
        return i;
    }

    @Override // com.google.android.gms.internal.clearcut.o
    public final int j() {
        if (this.zzjq == -1) {
            this.zzjq = rf5.a().d(this).g(this);
        }
        return this.zzjq;
    }

    public String toString() {
        return p.a(this, super.toString());
    }
}
