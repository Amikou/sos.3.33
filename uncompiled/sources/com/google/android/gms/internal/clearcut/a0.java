package com.google.android.gms.internal.clearcut;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/* loaded from: classes.dex */
public interface a0 {
    void A(int i, int i2) throws IOException;

    void B(int i, List<Float> list, boolean z) throws IOException;

    void C(int i, int i2) throws IOException;

    void D(int i, List<String> list) throws IOException;

    void E(int i, long j) throws IOException;

    @Deprecated
    void F(int i, List<?> list, t tVar) throws IOException;

    <K, V> void G(int i, sd5<K, V> sd5Var, Map<K, V> map) throws IOException;

    void H(int i, boolean z) throws IOException;

    void I(int i, List<?> list, t tVar) throws IOException;

    @Deprecated
    void J(int i) throws IOException;

    void K(int i, long j) throws IOException;

    void L(int i, int i2) throws IOException;

    void M(int i, zzbb zzbbVar) throws IOException;

    void N(int i, Object obj, t tVar) throws IOException;

    @Deprecated
    void O(int i) throws IOException;

    @Deprecated
    void P(int i, Object obj, t tVar) throws IOException;

    int Q();

    void R(int i, int i2) throws IOException;

    void a(int i, List<Boolean> list, boolean z) throws IOException;

    void b(int i, long j) throws IOException;

    void c(int i, List<Integer> list, boolean z) throws IOException;

    void e(int i, List<Integer> list, boolean z) throws IOException;

    void f(int i, int i2) throws IOException;

    void g(int i, long j) throws IOException;

    void j(int i, Object obj) throws IOException;

    void k(int i, List<Long> list, boolean z) throws IOException;

    void l(int i, List<Long> list, boolean z) throws IOException;

    void m(int i, List<Integer> list, boolean z) throws IOException;

    void n(int i, String str) throws IOException;

    void o(int i, List<Integer> list, boolean z) throws IOException;

    void p(int i, List<Integer> list, boolean z) throws IOException;

    void q(int i, long j) throws IOException;

    void r(int i, double d) throws IOException;

    void s(int i, float f) throws IOException;

    void t(int i, List<Long> list, boolean z) throws IOException;

    void u(int i, int i2) throws IOException;

    void v(int i, List<Long> list, boolean z) throws IOException;

    void w(int i, List<Long> list, boolean z) throws IOException;

    void x(int i, List<Integer> list, boolean z) throws IOException;

    void y(int i, List<Double> list, boolean z) throws IOException;

    void z(int i, List<zzbb> list) throws IOException;
}
