package com.google.android.gms.internal.clearcut;

import com.google.android.gms.internal.clearcut.c;
import defpackage.h65;
import java.io.IOException;

/* loaded from: classes.dex */
public abstract class c<MessageType extends c<MessageType, BuilderType>, BuilderType extends h65<MessageType, BuilderType>> implements o {
    private static boolean zzey = false;
    public int zzex = 0;

    public void b(int i) {
        throw new UnsupportedOperationException();
    }

    public int c() {
        throw new UnsupportedOperationException();
    }

    @Override // com.google.android.gms.internal.clearcut.o
    public final zzbb e() {
        try {
            m85 zzk = zzbb.zzk(j());
            g(zzk.b());
            return zzk.a();
        } catch (IOException e) {
            String name = getClass().getName();
            StringBuilder sb = new StringBuilder(name.length() + 62 + "ByteString".length());
            sb.append("Serializing ");
            sb.append(name);
            sb.append(" to a ");
            sb.append("ByteString");
            sb.append(" threw an IOException (should never happen).");
            throw new RuntimeException(sb.toString(), e);
        }
    }
}
