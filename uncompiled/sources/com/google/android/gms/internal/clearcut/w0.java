package com.google.android.gms.internal.clearcut;

import android.content.Context;
import com.google.android.gms.clearcut.a;
import com.google.android.gms.clearcut.zze;
import com.google.android.gms.internal.clearcut.l0;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/* loaded from: classes.dex */
public final class w0 implements a.b {
    public static final Charset b = Charset.forName("UTF-8");
    public static final w55 c;
    public static final w55 d;
    public static final ConcurrentHashMap<String, r45<l0>> e;
    public static final HashMap<String, r45<String>> f;
    public static Boolean g;
    public static Long h;
    public static final r45<Boolean> i;
    public final Context a;

    static {
        w55 i2 = new w55(sq2.a("com.google.android.gms.clearcut.public")).g("gms:playlog:service:samplingrules_").i("LogSamplingRules__");
        c = i2;
        d = new w55(sq2.a("com.google.android.gms.clearcut.public")).g("gms:playlog:service:sampling_").i("LogSampling__");
        e = new ConcurrentHashMap<>();
        f = new HashMap<>();
        g = null;
        h = null;
        i = i2.f("enable_log_sampling_rules", false);
    }

    public w0(Context context) {
        this.a = context;
        if (context != null) {
            r45.b(context);
        }
    }

    public static long b(String str, long j) {
        if (str == null || str.isEmpty()) {
            return uu5.c(ByteBuffer.allocate(8).putLong(j).array());
        }
        byte[] bytes = str.getBytes(b);
        ByteBuffer allocate = ByteBuffer.allocate(bytes.length + 8);
        allocate.put(bytes);
        allocate.putLong(j);
        return uu5.c(allocate.array());
    }

    public static l0.b c(String str) {
        String str2;
        if (str == null) {
            return null;
        }
        int indexOf = str.indexOf(44);
        int i2 = 0;
        if (indexOf >= 0) {
            i2 = indexOf + 1;
            str2 = str.substring(0, indexOf);
        } else {
            str2 = "";
        }
        int indexOf2 = str.indexOf(47, i2);
        if (indexOf2 <= 0) {
            if (str.length() != 0) {
                "Failed to parse the rule: ".concat(str);
            }
            return null;
        }
        try {
            long parseLong = Long.parseLong(str.substring(i2, indexOf2));
            long parseLong2 = Long.parseLong(str.substring(indexOf2 + 1));
            if (parseLong < 0 || parseLong2 < 0) {
                StringBuilder sb = new StringBuilder(72);
                sb.append("negative values not supported: ");
                sb.append(parseLong);
                sb.append("/");
                sb.append(parseLong2);
                return null;
            }
            return l0.b.B().r(str2).s(parseLong).t(parseLong2).p();
        } catch (NumberFormatException unused) {
            if (str.length() != 0) {
                "parseLong() failed while parsing: ".concat(str);
            }
            return null;
        }
    }

    public static boolean d(long j, long j2, long j3) {
        if (j2 < 0 || j3 <= 0) {
            return true;
        }
        return ((j > 0L ? 1 : (j == 0L ? 0 : -1)) >= 0 ? j % j3 : (((Long.MAX_VALUE % j3) + 1) + ((j & Long.MAX_VALUE) % j3)) % j3) < j2;
    }

    public static boolean e(Context context) {
        if (g == null) {
            g = Boolean.valueOf(kr4.a(context).a("com.google.android.providers.gsf.permission.READ_GSERVICES") == 0);
        }
        return g.booleanValue();
    }

    public static long f(Context context) {
        if (h == null) {
            if (context == null) {
                return 0L;
            }
            h = Long.valueOf(e(context) ? v56.a(context.getContentResolver(), "android_id", 0L) : 0L);
        }
        return h.longValue();
    }

    @Override // com.google.android.gms.clearcut.a.b
    public final boolean a(zze zzeVar) {
        List<l0.b> r;
        r45<l0> putIfAbsent;
        zzr zzrVar = zzeVar.a;
        String str = zzrVar.k0;
        int i2 = zzrVar.g0;
        q0 q0Var = zzeVar.m0;
        int i3 = q0Var != null ? q0Var.j0 : 0;
        String str2 = null;
        if (!i.a().booleanValue()) {
            if (str == null || str.isEmpty()) {
                str = i2 >= 0 ? String.valueOf(i2) : null;
            }
            if (str != null) {
                Context context = this.a;
                if (context != null && e(context)) {
                    HashMap<String, r45<String>> hashMap = f;
                    r45<String> r45Var = hashMap.get(str);
                    if (r45Var == null) {
                        r45Var = d.c(str, null);
                        hashMap.put(str, r45Var);
                    }
                    str2 = r45Var.a();
                }
                l0.b c2 = c(str2);
                if (c2 != null) {
                    return d(b(c2.x(), f(this.a)), c2.y(), c2.A());
                }
                return true;
            }
            return true;
        }
        if (str == null || str.isEmpty()) {
            str = i2 >= 0 ? String.valueOf(i2) : null;
        }
        if (str != null) {
            if (this.a == null) {
                r = Collections.emptyList();
            } else {
                ConcurrentHashMap<String, r45<l0>> concurrentHashMap = e;
                r45<l0> r45Var2 = concurrentHashMap.get(str);
                if (r45Var2 == null && (putIfAbsent = concurrentHashMap.putIfAbsent(str, (r45Var2 = c.b(str, l0.s(), i36.a)))) != null) {
                    r45Var2 = putIfAbsent;
                }
                r = r45Var2.a().r();
            }
            for (l0.b bVar : r) {
                if (!bVar.v() || bVar.r() == 0 || bVar.r() == i3) {
                    if (!d(b(bVar.x(), f(this.a)), bVar.y(), bVar.A())) {
                        return false;
                    }
                }
            }
            return true;
        }
        return true;
    }
}
