package com.google.android.gms.internal.clearcut;

/* loaded from: classes.dex */
public final class d0 implements Cloneable {
    public int[] a;
    public e0[] f0;
    public int g0;

    static {
        new e0();
    }

    public d0() {
        this(10);
    }

    public d0(int i) {
        int i2 = i << 2;
        int i3 = 4;
        while (true) {
            if (i3 >= 32) {
                break;
            }
            int i4 = (1 << i3) - 12;
            if (i2 <= i4) {
                i2 = i4;
                break;
            }
            i3++;
        }
        int i5 = i2 / 4;
        this.a = new int[i5];
        this.f0 = new e0[i5];
        this.g0 = 0;
    }

    public final boolean a() {
        return this.g0 == 0;
    }

    public final int b() {
        return this.g0;
    }

    public final /* synthetic */ Object clone() throws CloneNotSupportedException {
        int i = this.g0;
        d0 d0Var = new d0(i);
        System.arraycopy(this.a, 0, d0Var.a, 0, i);
        for (int i2 = 0; i2 < i; i2++) {
            e0[] e0VarArr = this.f0;
            if (e0VarArr[i2] != null) {
                d0Var.f0[i2] = (e0) e0VarArr[i2].clone();
            }
        }
        d0Var.g0 = i;
        return d0Var;
    }

    public final e0 d(int i) {
        return this.f0[i];
    }

    public final boolean equals(Object obj) {
        boolean z;
        boolean z2;
        if (obj == this) {
            return true;
        }
        if (obj instanceof d0) {
            d0 d0Var = (d0) obj;
            int i = this.g0;
            if (i != d0Var.g0) {
                return false;
            }
            int[] iArr = this.a;
            int[] iArr2 = d0Var.a;
            int i2 = 0;
            while (true) {
                if (i2 >= i) {
                    z = true;
                    break;
                } else if (iArr[i2] != iArr2[i2]) {
                    z = false;
                    break;
                } else {
                    i2++;
                }
            }
            if (z) {
                e0[] e0VarArr = this.f0;
                e0[] e0VarArr2 = d0Var.f0;
                int i3 = this.g0;
                int i4 = 0;
                while (true) {
                    if (i4 >= i3) {
                        z2 = true;
                        break;
                    } else if (!e0VarArr[i4].equals(e0VarArr2[i4])) {
                        z2 = false;
                        break;
                    } else {
                        i4++;
                    }
                }
                if (z2) {
                    return true;
                }
            }
            return false;
        }
        return false;
    }

    public final int hashCode() {
        int i = 17;
        for (int i2 = 0; i2 < this.g0; i2++) {
            i = (((i * 31) + this.a[i2]) * 31) + this.f0[i2].hashCode();
        }
        return i;
    }
}
