package com.google.android.gms.internal.clearcut;

import com.google.android.gms.internal.clearcut.m;
import java.util.List;
import java.util.Objects;

/* loaded from: classes.dex */
public final class l0 extends m<l0, a> implements je5 {
    private static volatile xe5<l0> zzbg;
    private static final l0 zzbir;
    private rb5<b> zzbiq = m.p();

    /* loaded from: classes.dex */
    public static final class a extends m.a<l0, a> implements je5 {
        public a() {
            super(l0.zzbir);
        }

        public /* synthetic */ a(m0 m0Var) {
            this();
        }
    }

    /* loaded from: classes.dex */
    public static final class b extends m<b, a> implements je5 {
        private static volatile xe5<b> zzbg;
        private static final b zzbiv;
        private int zzbb;
        private String zzbis = "";
        private long zzbit;
        private long zzbiu;
        private int zzya;

        /* loaded from: classes.dex */
        public static final class a extends m.a<b, a> implements je5 {
            public a() {
                super(b.zzbiv);
            }

            public /* synthetic */ a(m0 m0Var) {
                this();
            }

            public final a r(String str) {
                l();
                ((b) this.f0).D(str);
                return this;
            }

            public final a s(long j) {
                l();
                ((b) this.f0).E(j);
                return this;
            }

            public final a t(long j) {
                l();
                ((b) this.f0).F(j);
                return this;
            }
        }

        static {
            b bVar = new b();
            zzbiv = bVar;
            m.n(b.class, bVar);
        }

        public static a B() {
            return (a) ((m.a) zzbiv.i(m.e.e, null, null));
        }

        public final long A() {
            return this.zzbiu;
        }

        public final void D(String str) {
            Objects.requireNonNull(str);
            this.zzbb |= 2;
            this.zzbis = str;
        }

        public final void E(long j) {
            this.zzbb |= 4;
            this.zzbit = j;
        }

        public final void F(long j) {
            this.zzbb |= 8;
            this.zzbiu = j;
        }

        /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.clearcut.m$b, xe5<com.google.android.gms.internal.clearcut.l0$b>] */
        @Override // com.google.android.gms.internal.clearcut.m
        public final Object i(int i, Object obj, Object obj2) {
            xe5<b> xe5Var;
            switch (m0.a[i - 1]) {
                case 1:
                    return new b();
                case 2:
                    return new a(null);
                case 3:
                    return m.k(zzbiv, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0005\u0000\u0000\u0000\u0001\u0004\u0000\u0002\b\u0001\u0003\u0002\u0002\u0004\u0002\u0003", new Object[]{"zzbb", "zzya", "zzbis", "zzbit", "zzbiu"});
                case 4:
                    return zzbiv;
                case 5:
                    xe5<b> xe5Var2 = zzbg;
                    xe5<b> xe5Var3 = xe5Var2;
                    if (xe5Var2 == null) {
                        synchronized (b.class) {
                            xe5<b> xe5Var4 = zzbg;
                            xe5Var = xe5Var4;
                            if (xe5Var4 == null) {
                                ?? bVar = new m.b(zzbiv);
                                zzbg = bVar;
                                xe5Var = bVar;
                            }
                        }
                        xe5Var3 = xe5Var;
                    }
                    return xe5Var3;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        public final int r() {
            return this.zzya;
        }

        public final boolean v() {
            return (this.zzbb & 1) == 1;
        }

        public final String x() {
            return this.zzbis;
        }

        public final long y() {
            return this.zzbit;
        }
    }

    static {
        l0 l0Var = new l0();
        zzbir = l0Var;
        m.n(l0.class, l0Var);
    }

    public static l0 s() {
        return zzbir;
    }

    public static l0 u(byte[] bArr) throws zzco {
        return (l0) m.o(zzbir, bArr);
    }

    /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.clearcut.m$b, xe5<com.google.android.gms.internal.clearcut.l0>] */
    @Override // com.google.android.gms.internal.clearcut.m
    public final Object i(int i, Object obj, Object obj2) {
        xe5<l0> xe5Var;
        switch (m0.a[i - 1]) {
            case 1:
                return new l0();
            case 2:
                return new a(null);
            case 3:
                return m.k(zzbir, "\u0001\u0001\u0000\u0000\u0001\u0001\u0001\u0002\u0000\u0001\u0000\u0001\u001b", new Object[]{"zzbiq", b.class});
            case 4:
                return zzbir;
            case 5:
                xe5<l0> xe5Var2 = zzbg;
                xe5<l0> xe5Var3 = xe5Var2;
                if (xe5Var2 == null) {
                    synchronized (l0.class) {
                        xe5<l0> xe5Var4 = zzbg;
                        xe5Var = xe5Var4;
                        if (xe5Var4 == null) {
                            ?? bVar = new m.b(zzbir);
                            zzbg = bVar;
                            xe5Var = bVar;
                        }
                    }
                    xe5Var3 = xe5Var;
                }
                return xe5Var3;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public final List<b> r() {
        return this.zzbiq;
    }
}
