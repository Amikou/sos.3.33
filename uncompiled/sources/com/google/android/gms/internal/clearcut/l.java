package com.google.android.gms.internal.clearcut;

import defpackage.ta5;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* loaded from: classes.dex */
public final class l<FieldDescriptorType extends ta5<FieldDescriptorType>> {
    public static final l d = new l(true);
    public boolean b;
    public boolean c = false;
    public final fg5<FieldDescriptorType, Object> a = fg5.f(16);

    public l() {
    }

    public l(boolean z) {
        t();
    }

    public static int f(zzfl zzflVar, int i, Object obj) {
        int B0 = zzbn.B0(i);
        if (zzflVar == zzfl.zzql) {
            gb5.i((o) obj);
            B0 <<= 1;
        }
        return B0 + n(zzflVar, obj);
    }

    /* JADX WARN: Code restructure failed: missing block: B:10:0x001e, code lost:
        r0 = false;
     */
    /* JADX WARN: Code restructure failed: missing block: B:14:0x0026, code lost:
        if ((r3 instanceof defpackage.jb5) == false) goto L7;
     */
    /* JADX WARN: Code restructure failed: missing block: B:19:0x002f, code lost:
        if ((r3 instanceof byte[]) == false) goto L7;
     */
    /* JADX WARN: Code restructure failed: missing block: B:8:0x001b, code lost:
        if ((r3 instanceof defpackage.wb5) == false) goto L7;
     */
    /* JADX WARN: Removed duplicated region for block: B:29:0x0046 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:30:0x0047  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static void i(com.google.android.gms.internal.clearcut.zzfl r2, java.lang.Object r3) {
        /*
            defpackage.gb5.a(r3)
            int[] r0 = defpackage.ca5.a
            com.google.android.gms.internal.clearcut.zzfq r2 = r2.zzek()
            int r2 = r2.ordinal()
            r2 = r0[r2]
            r0 = 1
            r1 = 0
            switch(r2) {
                case 1: goto L41;
                case 2: goto L3e;
                case 3: goto L3b;
                case 4: goto L38;
                case 5: goto L35;
                case 6: goto L32;
                case 7: goto L29;
                case 8: goto L20;
                case 9: goto L15;
                default: goto L14;
            }
        L14:
            goto L44
        L15:
            boolean r2 = r3 instanceof com.google.android.gms.internal.clearcut.o
            if (r2 != 0) goto L43
            boolean r2 = r3 instanceof defpackage.wb5
            if (r2 == 0) goto L1e
            goto L43
        L1e:
            r0 = r1
            goto L43
        L20:
            boolean r2 = r3 instanceof java.lang.Integer
            if (r2 != 0) goto L43
            boolean r2 = r3 instanceof defpackage.jb5
            if (r2 == 0) goto L1e
            goto L43
        L29:
            boolean r2 = r3 instanceof com.google.android.gms.internal.clearcut.zzbb
            if (r2 != 0) goto L43
            boolean r2 = r3 instanceof byte[]
            if (r2 == 0) goto L1e
            goto L43
        L32:
            boolean r0 = r3 instanceof java.lang.String
            goto L43
        L35:
            boolean r0 = r3 instanceof java.lang.Boolean
            goto L43
        L38:
            boolean r0 = r3 instanceof java.lang.Double
            goto L43
        L3b:
            boolean r0 = r3 instanceof java.lang.Float
            goto L43
        L3e:
            boolean r0 = r3 instanceof java.lang.Long
            goto L43
        L41:
            boolean r0 = r3 instanceof java.lang.Integer
        L43:
            r1 = r0
        L44:
            if (r1 == 0) goto L47
            return
        L47:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.String r3 = "Wrong object type used with protocol message reflection."
            r2.<init>(r3)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.clearcut.l.i(com.google.android.gms.internal.clearcut.zzfl, java.lang.Object):void");
    }

    public static <T extends ta5<T>> l<T> k() {
        return d;
    }

    public static int n(zzfl zzflVar, Object obj) {
        switch (ca5.b[zzflVar.ordinal()]) {
            case 1:
                return zzbn.w(((Double) obj).doubleValue());
            case 2:
                return zzbn.x(((Float) obj).floatValue());
            case 3:
                return zzbn.e0(((Long) obj).longValue());
            case 4:
                return zzbn.h0(((Long) obj).longValue());
            case 5:
                return zzbn.C0(((Integer) obj).intValue());
            case 6:
                return zzbn.p0(((Long) obj).longValue());
            case 7:
                return zzbn.F0(((Integer) obj).intValue());
            case 8:
                return zzbn.F(((Boolean) obj).booleanValue());
            case 9:
                return zzbn.Z((o) obj);
            case 10:
                return obj instanceof wb5 ? zzbn.e((wb5) obj) : zzbn.R((o) obj);
            case 11:
                return obj instanceof zzbb ? zzbn.D((zzbb) obj) : zzbn.q0((String) obj);
            case 12:
                return obj instanceof zzbb ? zzbn.D((zzbb) obj) : zzbn.a0((byte[]) obj);
            case 13:
                return zzbn.D0(((Integer) obj).intValue());
            case 14:
                return zzbn.G0(((Integer) obj).intValue());
            case 15:
                return zzbn.s0(((Long) obj).longValue());
            case 16:
                return zzbn.E0(((Integer) obj).intValue());
            case 17:
                return zzbn.l0(((Long) obj).longValue());
            case 18:
                return obj instanceof jb5 ? zzbn.H0(((jb5) obj).zzc()) : zzbn.H0(((Integer) obj).intValue());
            default:
                throw new RuntimeException("There is no way to get here, but the compiler thinks otherwise.");
        }
    }

    public static int o(ta5<?> ta5Var, Object obj) {
        zzfl w = ta5Var.w();
        int zzc = ta5Var.zzc();
        if (ta5Var.J0()) {
            int i = 0;
            List<Object> list = (List) obj;
            if (ta5Var.m()) {
                for (Object obj2 : list) {
                    i += n(w, obj2);
                }
                return zzbn.B0(zzc) + i + zzbn.J0(i);
            }
            for (Object obj3 : list) {
                i += f(w, zzc, obj3);
            }
            return i;
        }
        return f(w, zzc, obj);
    }

    public static boolean p(Map.Entry<FieldDescriptorType, Object> entry) {
        FieldDescriptorType key = entry.getKey();
        if (key.q() == zzfq.MESSAGE) {
            boolean J0 = key.J0();
            Object value = entry.getValue();
            if (J0) {
                for (o oVar : (List) value) {
                    if (!oVar.isInitialized()) {
                        return false;
                    }
                }
            } else if (!(value instanceof o)) {
                if (value instanceof wb5) {
                    return true;
                }
                throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
            } else if (!((o) value).isInitialized()) {
                return false;
            }
        }
        return true;
    }

    public static int r(Map.Entry<FieldDescriptorType, Object> entry) {
        FieldDescriptorType key = entry.getKey();
        Object value = entry.getValue();
        if (key.q() != zzfq.MESSAGE || key.J0() || key.m()) {
            return o(key, value);
        }
        boolean z = value instanceof wb5;
        int zzc = entry.getKey().zzc();
        return z ? zzbn.C(zzc, (wb5) value) : zzbn.Y(zzc, (o) value);
    }

    public static Object s(Object obj) {
        if (obj instanceof qe5) {
            return ((qe5) obj).y0();
        }
        if (obj instanceof byte[]) {
            byte[] bArr = (byte[]) obj;
            byte[] bArr2 = new byte[bArr.length];
            System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
            return bArr2;
        }
        return obj;
    }

    public final Iterator<Map.Entry<FieldDescriptorType, Object>> a() {
        return this.c ? new cc5(this.a.n().iterator()) : this.a.n().iterator();
    }

    public final boolean b() {
        return this.a.isEmpty();
    }

    public final boolean c() {
        return this.b;
    }

    public final /* synthetic */ Object clone() throws CloneNotSupportedException {
        l lVar = new l();
        for (int i = 0; i < this.a.l(); i++) {
            Map.Entry<FieldDescriptorType, Object> g = this.a.g(i);
            lVar.j(g.getKey(), g.getValue());
        }
        for (Map.Entry<FieldDescriptorType, Object> entry : this.a.m()) {
            lVar.j(entry.getKey(), entry.getValue());
        }
        lVar.c = this.c;
        return lVar;
    }

    public final boolean d() {
        for (int i = 0; i < this.a.l(); i++) {
            if (!p(this.a.g(i))) {
                return false;
            }
        }
        for (Map.Entry<FieldDescriptorType, Object> entry : this.a.m()) {
            if (!p(entry)) {
                return false;
            }
        }
        return true;
    }

    public final Iterator<Map.Entry<FieldDescriptorType, Object>> e() {
        return this.c ? new cc5(this.a.entrySet().iterator()) : this.a.entrySet().iterator();
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof l) {
            return this.a.equals(((l) obj).a);
        }
        return false;
    }

    public final Object g(FieldDescriptorType fielddescriptortype) {
        Object obj = this.a.get(fielddescriptortype);
        return obj instanceof wb5 ? wb5.e() : obj;
    }

    public final void h(l<FieldDescriptorType> lVar) {
        for (int i = 0; i < lVar.a.l(); i++) {
            q(lVar.a.g(i));
        }
        for (Map.Entry<FieldDescriptorType, Object> entry : lVar.a.m()) {
            q(entry);
        }
    }

    public final int hashCode() {
        return this.a.hashCode();
    }

    public final void j(FieldDescriptorType fielddescriptortype, Object obj) {
        if (!fielddescriptortype.J0()) {
            i(fielddescriptortype.w(), obj);
        } else if (!(obj instanceof List)) {
            throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
        } else {
            ArrayList arrayList = new ArrayList();
            arrayList.addAll((List) obj);
            int size = arrayList.size();
            int i = 0;
            while (i < size) {
                Object obj2 = arrayList.get(i);
                i++;
                i(fielddescriptortype.w(), obj2);
            }
            obj = arrayList;
        }
        if (obj instanceof wb5) {
            this.c = true;
        }
        this.a.put(fielddescriptortype, obj);
    }

    public final int l() {
        int i = 0;
        for (int i2 = 0; i2 < this.a.l(); i2++) {
            Map.Entry<FieldDescriptorType, Object> g = this.a.g(i2);
            i += o(g.getKey(), g.getValue());
        }
        for (Map.Entry<FieldDescriptorType, Object> entry : this.a.m()) {
            i += o(entry.getKey(), entry.getValue());
        }
        return i;
    }

    public final int m() {
        int i = 0;
        for (int i2 = 0; i2 < this.a.l(); i2++) {
            i += r(this.a.g(i2));
        }
        for (Map.Entry<FieldDescriptorType, Object> entry : this.a.m()) {
            i += r(entry);
        }
        return i;
    }

    public final void q(Map.Entry<FieldDescriptorType, Object> entry) {
        FieldDescriptorType key = entry.getKey();
        Object value = entry.getValue();
        if (value instanceof wb5) {
            value = wb5.e();
        }
        if (key.J0()) {
            Object g = g(key);
            if (g == null) {
                g = new ArrayList();
            }
            for (Object obj : (List) value) {
                ((List) g).add(s(obj));
            }
            this.a.put(key, g);
        } else if (key.q() != zzfq.MESSAGE) {
            this.a.put(key, s(value));
        } else {
            Object g2 = g(key);
            if (g2 == null) {
                this.a.put(key, s(value));
            } else {
                this.a.put(key, g2 instanceof qe5 ? key.H1((qe5) g2, (qe5) value) : key.F(((o) g2).f(), (o) value).m());
            }
        }
    }

    public final void t() {
        if (this.b) {
            return;
        }
        this.a.q();
        this.b = true;
    }
}
