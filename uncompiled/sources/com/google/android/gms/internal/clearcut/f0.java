package com.google.android.gms.internal.clearcut;

import java.io.IOException;

/* loaded from: classes.dex */
public class f0 {
    public volatile int a = -1;

    public static final void b(f0 f0Var, byte[] bArr, int i, int i2) {
        try {
            b0 t = b0.t(bArr, 0, i2);
            f0Var.a(t);
            t.p();
        } catch (IOException e) {
            throw new RuntimeException("Serializing to a byte array threw an IOException (should never happen).", e);
        }
    }

    public void a(b0 b0Var) throws IOException {
    }

    public final int d() {
        int e = e();
        this.a = e;
        return e;
    }

    public int e() {
        return 0;
    }

    @Override // 
    /* renamed from: f */
    public f0 clone() throws CloneNotSupportedException {
        return (f0) super.clone();
    }

    public String toString() {
        return g0.a(this);
    }
}
