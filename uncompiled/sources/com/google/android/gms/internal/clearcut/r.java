package com.google.android.gms.internal.clearcut;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

/* loaded from: classes.dex */
public final class r<T> implements t<T> {
    public final o a;
    public final v<?, ?> b;
    public final boolean c;
    public final j<?> d;

    public r(v<?, ?> vVar, j<?> jVar, o oVar) {
        this.b = vVar;
        this.c = jVar.g(oVar);
        this.d = jVar;
        this.a = oVar;
    }

    public static <T> r<T> j(v<?, ?> vVar, j<?> jVar, o oVar) {
        return new r<>(vVar, jVar, oVar);
    }

    @Override // com.google.android.gms.internal.clearcut.t
    public final void a(T t) {
        this.b.d(t);
        this.d.f(t);
    }

    @Override // com.google.android.gms.internal.clearcut.t
    public final boolean b(T t, T t2) {
        if (this.b.k(t).equals(this.b.k(t2))) {
            if (this.c) {
                return this.d.b(t).equals(this.d.b(t2));
            }
            return true;
        }
        return false;
    }

    @Override // com.google.android.gms.internal.clearcut.t
    public final T c() {
        return (T) this.a.h().w();
    }

    @Override // com.google.android.gms.internal.clearcut.t
    public final int d(T t) {
        int hashCode = this.b.k(t).hashCode();
        return this.c ? (hashCode * 53) + this.d.b(t).hashCode() : hashCode;
    }

    @Override // com.google.android.gms.internal.clearcut.t
    public final void e(T t, a0 a0Var) throws IOException {
        Iterator<Map.Entry<?, Object>> e = this.d.b(t).e();
        while (e.hasNext()) {
            Map.Entry<?, Object> next = e.next();
            ta5 ta5Var = (ta5) next.getKey();
            if (ta5Var.q() != zzfq.MESSAGE || ta5Var.J0() || ta5Var.m()) {
                throw new IllegalStateException("Found invalid MessageSet item.");
            }
            a0Var.j(ta5Var.zzc(), next instanceof ac5 ? ((ac5) next).a().d() : next.getValue());
        }
        v<?, ?> vVar = this.b;
        vVar.e(vVar.k(t), a0Var);
    }

    @Override // com.google.android.gms.internal.clearcut.t
    public final void f(T t, T t2) {
        u.h(this.b, t, t2);
        if (this.c) {
            u.g(this.d, t, t2);
        }
    }

    @Override // com.google.android.gms.internal.clearcut.t
    public final int g(T t) {
        v<?, ?> vVar = this.b;
        int l = vVar.l(vVar.k(t)) + 0;
        return this.c ? l + this.d.b(t).m() : l;
    }

    /* JADX WARN: Removed duplicated region for block: B:26:0x005c  */
    /* JADX WARN: Removed duplicated region for block: B:49:0x0061 A[EDGE_INSN: B:49:0x0061->B:27:0x0061 ?: BREAK  , SYNTHETIC] */
    @Override // com.google.android.gms.internal.clearcut.t
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void h(T r8, byte[] r9, int r10, int r11, defpackage.v65 r12) throws java.io.IOException {
        /*
            r7 = this;
            com.google.android.gms.internal.clearcut.m r8 = (com.google.android.gms.internal.clearcut.m) r8
            com.google.android.gms.internal.clearcut.w r0 = r8.zzjp
            com.google.android.gms.internal.clearcut.w r1 = com.google.android.gms.internal.clearcut.w.h()
            if (r0 != r1) goto L10
            com.google.android.gms.internal.clearcut.w r0 = com.google.android.gms.internal.clearcut.w.i()
            r8.zzjp = r0
        L10:
            r8 = r0
        L11:
            if (r10 >= r11) goto L6b
            int r2 = com.google.android.gms.internal.clearcut.d.e(r9, r10, r12)
            int r0 = r12.a
            r10 = 11
            r1 = 2
            if (r0 == r10) goto L30
            r10 = r0 & 7
            if (r10 != r1) goto L2b
            r1 = r9
            r3 = r11
            r4 = r8
            r5 = r12
            int r10 = com.google.android.gms.internal.clearcut.d.a(r0, r1, r2, r3, r4, r5)
            goto L11
        L2b:
            int r10 = com.google.android.gms.internal.clearcut.d.b(r0, r9, r2, r11, r12)
            goto L11
        L30:
            r10 = 0
            r0 = 0
        L32:
            if (r2 >= r11) goto L61
            int r2 = com.google.android.gms.internal.clearcut.d.e(r9, r2, r12)
            int r3 = r12.a
            int r4 = r3 >>> 3
            r5 = r3 & 7
            if (r4 == r1) goto L4f
            r6 = 3
            if (r4 == r6) goto L44
            goto L58
        L44:
            if (r5 != r1) goto L58
            int r2 = com.google.android.gms.internal.clearcut.d.m(r9, r2, r12)
            java.lang.Object r0 = r12.c
            com.google.android.gms.internal.clearcut.zzbb r0 = (com.google.android.gms.internal.clearcut.zzbb) r0
            goto L32
        L4f:
            if (r5 != 0) goto L58
            int r2 = com.google.android.gms.internal.clearcut.d.e(r9, r2, r12)
            int r10 = r12.a
            goto L32
        L58:
            r4 = 12
            if (r3 == r4) goto L61
            int r2 = com.google.android.gms.internal.clearcut.d.b(r3, r9, r2, r11, r12)
            goto L32
        L61:
            if (r0 == 0) goto L69
            int r10 = r10 << 3
            r10 = r10 | r1
            r8.e(r10, r0)
        L69:
            r10 = r2
            goto L11
        L6b:
            if (r10 != r11) goto L6e
            return
        L6e:
            com.google.android.gms.internal.clearcut.zzco r8 = com.google.android.gms.internal.clearcut.zzco.zzbo()
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.clearcut.r.h(java.lang.Object, byte[], int, int, v65):void");
    }

    @Override // com.google.android.gms.internal.clearcut.t
    public final boolean i(T t) {
        return this.d.b(t).d();
    }
}
