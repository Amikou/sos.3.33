package com.google.android.gms.internal.clearcut;

import java.io.IOException;

/* loaded from: classes.dex */
public interface b<T> {
    T a(byte[] bArr) throws IOException;
}
