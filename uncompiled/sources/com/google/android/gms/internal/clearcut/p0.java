package com.google.android.gms.internal.clearcut;

import android.os.RemoteException;
import com.google.android.gms.clearcut.a;
import com.google.android.gms.clearcut.zze;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;

/* loaded from: classes.dex */
public final class p0 extends com.google.android.gms.common.api.internal.b<Status, as5> {
    public final zze r;

    public p0(zze zzeVar, GoogleApiClient googleApiClient) {
        super(com.google.android.gms.clearcut.a.o, googleApiClient);
        this.r = zzeVar;
    }

    @Override // com.google.android.gms.common.api.internal.BasePendingResult
    public final /* synthetic */ l83 d(Status status) {
        return status;
    }

    @Override // com.google.android.gms.common.api.internal.b
    public final /* synthetic */ void q(as5 as5Var) throws RemoteException {
        as5 as5Var2 = as5Var;
        op5 op5Var = new op5(this);
        try {
            zze zzeVar = this.r;
            a.c cVar = zzeVar.n0;
            if (cVar != null) {
                q0 q0Var = zzeVar.m0;
                if (q0Var.o0.length == 0) {
                    q0Var.o0 = cVar.zza();
                }
            }
            a.c cVar2 = zzeVar.o0;
            if (cVar2 != null) {
                q0 q0Var2 = zzeVar.m0;
                if (q0Var2.v0.length == 0) {
                    q0Var2.v0 = cVar2.zza();
                }
            }
            q0 q0Var3 = zzeVar.m0;
            int d = q0Var3.d();
            byte[] bArr = new byte[d];
            f0.b(q0Var3, bArr, 0, d);
            zzeVar.f0 = bArr;
            ((u0) as5Var2.G()).F(op5Var, this.r);
        } catch (RuntimeException unused) {
            w(new Status(10, "MessageProducer"));
        }
    }
}
