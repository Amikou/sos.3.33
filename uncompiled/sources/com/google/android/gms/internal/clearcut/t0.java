package com.google.android.gms.internal.clearcut;

import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.clearcut.zzc;
import com.google.android.gms.clearcut.zze;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;

/* loaded from: classes.dex */
public abstract class t0 extends e implements s0 {
    public t0() {
        super("com.google.android.gms.clearcut.internal.IClearcutLoggerCallbacks");
    }

    @Override // com.google.android.gms.internal.clearcut.e
    public final boolean b(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        switch (i) {
            case 1:
                O0((Status) la5.a(parcel, Status.CREATOR));
                return true;
            case 2:
                Y0((Status) la5.a(parcel, Status.CREATOR));
                return true;
            case 3:
                r((Status) la5.a(parcel, Status.CREATOR), parcel.readLong());
                return true;
            case 4:
                s1((Status) la5.a(parcel, Status.CREATOR));
                return true;
            case 5:
                y1((Status) la5.a(parcel, Status.CREATOR), parcel.readLong());
                return true;
            case 6:
                A0((Status) la5.a(parcel, Status.CREATOR), (zze[]) parcel.createTypedArray(zze.CREATOR));
                return true;
            case 7:
                v((DataHolder) la5.a(parcel, DataHolder.CREATOR));
                return true;
            case 8:
                G0((Status) la5.a(parcel, Status.CREATOR), (zzc) la5.a(parcel, zzc.CREATOR));
                return true;
            case 9:
                u1((Status) la5.a(parcel, Status.CREATOR), (zzc) la5.a(parcel, zzc.CREATOR));
                return true;
            default:
                return false;
        }
    }
}
