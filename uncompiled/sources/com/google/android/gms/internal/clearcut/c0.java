package com.google.android.gms.internal.clearcut;

import com.google.android.gms.internal.clearcut.c0;
import java.io.IOException;

/* loaded from: classes.dex */
public class c0<M extends c0<M>> extends f0 {
    public d0 f0;

    @Override // com.google.android.gms.internal.clearcut.f0
    public void a(b0 b0Var) throws IOException {
        if (this.f0 == null) {
            return;
        }
        for (int i = 0; i < this.f0.b(); i++) {
            this.f0.d(i).b(b0Var);
        }
    }

    @Override // com.google.android.gms.internal.clearcut.f0
    public int e() {
        if (this.f0 != null) {
            for (int i = 0; i < this.f0.b(); i++) {
                this.f0.d(i).d();
            }
        }
        return 0;
    }

    @Override // com.google.android.gms.internal.clearcut.f0
    public /* synthetic */ f0 f() throws CloneNotSupportedException {
        return (c0) clone();
    }

    @Override // com.google.android.gms.internal.clearcut.f0
    /* renamed from: g */
    public M clone() throws CloneNotSupportedException {
        M m = (M) super.clone();
        lk5.h(this, m);
        return m;
    }
}
