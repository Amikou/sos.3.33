package com.google.android.gms.internal.clearcut;

/* loaded from: classes.dex */
public abstract class g {
    public g() {
    }

    public static long a(long j) {
        return (-(j & 1)) ^ (j >>> 1);
    }

    public static g b(byte[] bArr, int i, int i2, boolean z) {
        h hVar = new h(bArr, 0, i2, false);
        try {
            hVar.d(i2);
            return hVar;
        } catch (zzco e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static int e(int i) {
        return (-(i & 1)) ^ (i >>> 1);
    }

    public abstract int c();

    public abstract int d(int i) throws zzco;
}
