package com.google.android.gms.internal.clearcut;

import java.io.IOException;

/* loaded from: classes.dex */
public final class n0 extends c0<n0> implements Cloneable {
    public String[] g0;
    public String[] h0;
    public int[] i0;
    public long[] j0;
    public long[] k0;

    public n0() {
        String[] strArr = al5.c;
        this.g0 = strArr;
        this.h0 = strArr;
        this.i0 = al5.a;
        long[] jArr = al5.b;
        this.j0 = jArr;
        this.k0 = jArr;
        this.f0 = null;
        this.a = -1;
    }

    @Override // com.google.android.gms.internal.clearcut.c0, com.google.android.gms.internal.clearcut.f0
    public final void a(b0 b0Var) throws IOException {
        String[] strArr = this.g0;
        int i = 0;
        if (strArr != null && strArr.length > 0) {
            int i2 = 0;
            while (true) {
                String[] strArr2 = this.g0;
                if (i2 >= strArr2.length) {
                    break;
                }
                String str = strArr2[i2];
                if (str != null) {
                    b0Var.c(1, str);
                }
                i2++;
            }
        }
        String[] strArr3 = this.h0;
        if (strArr3 != null && strArr3.length > 0) {
            int i3 = 0;
            while (true) {
                String[] strArr4 = this.h0;
                if (i3 >= strArr4.length) {
                    break;
                }
                String str2 = strArr4[i3];
                if (str2 != null) {
                    b0Var.c(2, str2);
                }
                i3++;
            }
        }
        int[] iArr = this.i0;
        if (iArr != null && iArr.length > 0) {
            int i4 = 0;
            while (true) {
                int[] iArr2 = this.i0;
                if (i4 >= iArr2.length) {
                    break;
                }
                b0Var.l(3, iArr2[i4]);
                i4++;
            }
        }
        long[] jArr = this.j0;
        if (jArr != null && jArr.length > 0) {
            int i5 = 0;
            while (true) {
                long[] jArr2 = this.j0;
                if (i5 >= jArr2.length) {
                    break;
                }
                b0Var.u(4, jArr2[i5]);
                i5++;
            }
        }
        long[] jArr3 = this.k0;
        if (jArr3 != null && jArr3.length > 0) {
            while (true) {
                long[] jArr4 = this.k0;
                if (i >= jArr4.length) {
                    break;
                }
                b0Var.u(5, jArr4[i]);
                i++;
            }
        }
        super.a(b0Var);
    }

    @Override // com.google.android.gms.internal.clearcut.c0, com.google.android.gms.internal.clearcut.f0
    public final int e() {
        long[] jArr;
        int[] iArr;
        int e = super.e();
        String[] strArr = this.g0;
        int i = 0;
        if (strArr != null && strArr.length > 0) {
            int i2 = 0;
            int i3 = 0;
            int i4 = 0;
            while (true) {
                String[] strArr2 = this.g0;
                if (i2 >= strArr2.length) {
                    break;
                }
                String str = strArr2[i2];
                if (str != null) {
                    i4++;
                    i3 += b0.r(str);
                }
                i2++;
            }
            e = e + i3 + (i4 * 1);
        }
        String[] strArr3 = this.h0;
        if (strArr3 != null && strArr3.length > 0) {
            int i5 = 0;
            int i6 = 0;
            int i7 = 0;
            while (true) {
                String[] strArr4 = this.h0;
                if (i5 >= strArr4.length) {
                    break;
                }
                String str2 = strArr4[i5];
                if (str2 != null) {
                    i7++;
                    i6 += b0.r(str2);
                }
                i5++;
            }
            e = e + i6 + (i7 * 1);
        }
        int[] iArr2 = this.i0;
        if (iArr2 != null && iArr2.length > 0) {
            int i8 = 0;
            int i9 = 0;
            while (true) {
                iArr = this.i0;
                if (i8 >= iArr.length) {
                    break;
                }
                i9 += b0.z(iArr[i8]);
                i8++;
            }
            e = e + i9 + (iArr.length * 1);
        }
        long[] jArr2 = this.j0;
        if (jArr2 != null && jArr2.length > 0) {
            int i10 = 0;
            int i11 = 0;
            while (true) {
                jArr = this.j0;
                if (i10 >= jArr.length) {
                    break;
                }
                i11 += b0.x(jArr[i10]);
                i10++;
            }
            e = e + i11 + (jArr.length * 1);
        }
        long[] jArr3 = this.k0;
        if (jArr3 == null || jArr3.length <= 0) {
            return e;
        }
        int i12 = 0;
        while (true) {
            long[] jArr4 = this.k0;
            if (i >= jArr4.length) {
                return e + i12 + (jArr4.length * 1);
            }
            i12 += b0.x(jArr4[i]);
            i++;
        }
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof n0) {
            n0 n0Var = (n0) obj;
            if (lk5.c(this.g0, n0Var.g0) && lk5.c(this.h0, n0Var.h0) && lk5.a(this.i0, n0Var.i0) && lk5.b(this.j0, n0Var.j0) && lk5.b(this.k0, n0Var.k0)) {
                d0 d0Var = this.f0;
                if (d0Var == null || d0Var.a()) {
                    d0 d0Var2 = n0Var.f0;
                    return d0Var2 == null || d0Var2.a();
                }
                return this.f0.equals(n0Var.f0);
            }
            return false;
        }
        return false;
    }

    @Override // com.google.android.gms.internal.clearcut.c0, com.google.android.gms.internal.clearcut.f0
    public final /* synthetic */ f0 f() throws CloneNotSupportedException {
        return (n0) clone();
    }

    @Override // com.google.android.gms.internal.clearcut.c0
    public final /* synthetic */ n0 g() throws CloneNotSupportedException {
        return (n0) clone();
    }

    @Override // com.google.android.gms.internal.clearcut.c0, com.google.android.gms.internal.clearcut.f0
    /* renamed from: h */
    public final n0 clone() {
        try {
            n0 n0Var = (n0) super.clone();
            String[] strArr = this.g0;
            if (strArr != null && strArr.length > 0) {
                n0Var.g0 = (String[]) strArr.clone();
            }
            String[] strArr2 = this.h0;
            if (strArr2 != null && strArr2.length > 0) {
                n0Var.h0 = (String[]) strArr2.clone();
            }
            int[] iArr = this.i0;
            if (iArr != null && iArr.length > 0) {
                n0Var.i0 = (int[]) iArr.clone();
            }
            long[] jArr = this.j0;
            if (jArr != null && jArr.length > 0) {
                n0Var.j0 = (long[]) jArr.clone();
            }
            long[] jArr2 = this.k0;
            if (jArr2 != null && jArr2.length > 0) {
                n0Var.k0 = (long[]) jArr2.clone();
            }
            return n0Var;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    public final int hashCode() {
        int hashCode = (((((((((((n0.class.getName().hashCode() + 527) * 31) + lk5.f(this.g0)) * 31) + lk5.f(this.h0)) * 31) + lk5.d(this.i0)) * 31) + lk5.e(this.j0)) * 31) + lk5.e(this.k0)) * 31;
        d0 d0Var = this.f0;
        return hashCode + ((d0Var == null || d0Var.a()) ? 0 : this.f0.hashCode());
    }
}
