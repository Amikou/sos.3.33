package com.google.android.gms.internal.firebase_messaging;

import defpackage.h31;
import java.io.IOException;

/* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
/* loaded from: classes.dex */
public final class e implements hl2<g82> {
    public static final e a = new e();
    public static final h31 b;

    static {
        h31.b a2 = h31.a("messagingClientEvent");
        e56 e56Var = new e56();
        e56Var.a(1);
        b = a2.b(e56Var.b()).a();
    }

    @Override // com.google.firebase.encoders.b
    public final /* bridge */ /* synthetic */ void a(Object obj, com.google.firebase.encoders.c cVar) throws IOException {
        cVar.a(b, ((g82) obj).a());
    }
}
