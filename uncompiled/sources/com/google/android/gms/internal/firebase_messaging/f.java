package com.google.android.gms.internal.firebase_messaging;

import java.io.IOException;

/* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
/* loaded from: classes.dex */
public final class f implements hl2<nf5> {
    public static final f a = new f();
    public static final h31 b = h31.d("messagingClientEventExtension");

    @Override // com.google.firebase.encoders.b
    public final /* bridge */ /* synthetic */ void a(Object obj, com.google.firebase.encoders.c cVar) throws IOException {
        cVar.a(b, ((nf5) obj).b());
    }
}
