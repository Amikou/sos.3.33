package com.google.android.gms.internal.firebase_messaging;

import com.google.firebase.messaging.reporting.MessagingClientEvent;
import defpackage.h31;
import java.io.IOException;
import org.web3j.ens.contracts.generated.ENS;

/* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
/* loaded from: classes.dex */
public final class a implements hl2<MessagingClientEvent> {
    public static final a a = new a();
    public static final h31 b;
    public static final h31 c;
    public static final h31 d;
    public static final h31 e;
    public static final h31 f;
    public static final h31 g;
    public static final h31 h;
    public static final h31 i;
    public static final h31 j;
    public static final h31 k;
    public static final h31 l;
    public static final h31 m;
    public static final h31 n;
    public static final h31 o;
    public static final h31 p;

    static {
        h31.b a2 = h31.a("projectNumber");
        e56 e56Var = new e56();
        e56Var.a(1);
        b = a2.b(e56Var.b()).a();
        h31.b a3 = h31.a("messageId");
        e56 e56Var2 = new e56();
        e56Var2.a(2);
        c = a3.b(e56Var2.b()).a();
        h31.b a4 = h31.a("instanceId");
        e56 e56Var3 = new e56();
        e56Var3.a(3);
        d = a4.b(e56Var3.b()).a();
        h31.b a5 = h31.a("messageType");
        e56 e56Var4 = new e56();
        e56Var4.a(4);
        e = a5.b(e56Var4.b()).a();
        h31.b a6 = h31.a("sdkPlatform");
        e56 e56Var5 = new e56();
        e56Var5.a(5);
        f = a6.b(e56Var5.b()).a();
        h31.b a7 = h31.a("packageName");
        e56 e56Var6 = new e56();
        e56Var6.a(6);
        g = a7.b(e56Var6.b()).a();
        h31.b a8 = h31.a("collapseKey");
        e56 e56Var7 = new e56();
        e56Var7.a(7);
        h = a8.b(e56Var7.b()).a();
        h31.b a9 = h31.a("priority");
        e56 e56Var8 = new e56();
        e56Var8.a(8);
        i = a9.b(e56Var8.b()).a();
        h31.b a10 = h31.a(ENS.FUNC_TTL);
        e56 e56Var9 = new e56();
        e56Var9.a(9);
        j = a10.b(e56Var9.b()).a();
        h31.b a11 = h31.a("topic");
        e56 e56Var10 = new e56();
        e56Var10.a(10);
        k = a11.b(e56Var10.b()).a();
        h31.b a12 = h31.a("bulkId");
        e56 e56Var11 = new e56();
        e56Var11.a(11);
        l = a12.b(e56Var11.b()).a();
        h31.b a13 = h31.a("event");
        e56 e56Var12 = new e56();
        e56Var12.a(12);
        m = a13.b(e56Var12.b()).a();
        h31.b a14 = h31.a("analyticsLabel");
        e56 e56Var13 = new e56();
        e56Var13.a(13);
        n = a14.b(e56Var13.b()).a();
        h31.b a15 = h31.a("campaignId");
        e56 e56Var14 = new e56();
        e56Var14.a(14);
        o = a15.b(e56Var14.b()).a();
        h31.b a16 = h31.a("composerLabel");
        e56 e56Var15 = new e56();
        e56Var15.a(15);
        p = a16.b(e56Var15.b()).a();
    }

    @Override // com.google.firebase.encoders.b
    public final /* bridge */ /* synthetic */ void a(Object obj, com.google.firebase.encoders.c cVar) throws IOException {
        MessagingClientEvent messagingClientEvent = (MessagingClientEvent) obj;
        com.google.firebase.encoders.c cVar2 = cVar;
        cVar2.f(b, messagingClientEvent.l());
        cVar2.a(c, messagingClientEvent.h());
        cVar2.a(d, messagingClientEvent.g());
        cVar2.a(e, messagingClientEvent.i());
        cVar2.a(f, messagingClientEvent.m());
        cVar2.a(g, messagingClientEvent.j());
        cVar2.a(h, messagingClientEvent.d());
        cVar2.e(i, messagingClientEvent.k());
        cVar2.e(j, messagingClientEvent.o());
        cVar2.a(k, messagingClientEvent.n());
        cVar2.f(l, messagingClientEvent.b());
        cVar2.a(m, messagingClientEvent.f());
        cVar2.a(n, messagingClientEvent.a());
        cVar2.f(o, messagingClientEvent.c());
        cVar2.a(p, messagingClientEvent.e());
    }
}
