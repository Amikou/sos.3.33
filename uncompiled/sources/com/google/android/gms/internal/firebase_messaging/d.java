package com.google.android.gms.internal.firebase_messaging;

import com.google.firebase.encoders.EncodingException;
import java.io.IOException;

/* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
/* loaded from: classes.dex */
public final class d implements com.google.firebase.encoders.d {
    public boolean a = false;
    public boolean b = false;
    public h31 c;
    public final b d;

    public d(b bVar) {
        this.d = bVar;
    }

    public final void a(h31 h31Var, boolean z) {
        this.a = false;
        this.c = h31Var;
        this.b = z;
    }

    @Override // com.google.firebase.encoders.d
    public final com.google.firebase.encoders.d b(String str) throws IOException {
        d();
        this.d.b(this.c, str, this.b);
        return this;
    }

    @Override // com.google.firebase.encoders.d
    public final com.google.firebase.encoders.d c(boolean z) throws IOException {
        d();
        this.d.h(this.c, z ? 1 : 0, this.b);
        return this;
    }

    public final void d() {
        if (this.a) {
            throw new EncodingException("Cannot encode a second value in the ValueEncoderContext");
        }
        this.a = true;
    }
}
