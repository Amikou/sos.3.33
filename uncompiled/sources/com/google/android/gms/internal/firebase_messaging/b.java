package com.google.android.gms.internal.firebase_messaging;

import com.github.mikephil.charting.utils.Utils;
import com.google.firebase.encoders.EncodingException;
import defpackage.h31;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.Map;

/* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
/* loaded from: classes.dex */
public final class b implements com.google.firebase.encoders.c {
    public static final Charset f = Charset.forName("UTF-8");
    public static final h31 g;
    public static final h31 h;
    public static final hl2<Map.Entry<Object, Object>> i;
    public OutputStream a;
    public final Map<Class<?>, hl2<?>> b;
    public final Map<Class<?>, zg4<?>> c;
    public final hl2<Object> d;
    public final d e = new d(this);

    static {
        h31.b a = h31.a("key");
        e56 e56Var = new e56();
        e56Var.a(1);
        g = a.b(e56Var.b()).a();
        h31.b a2 = h31.a("value");
        e56 e56Var2 = new e56();
        e56Var2.a(2);
        h = a2.b(e56Var2.b()).a();
        i = c45.a;
    }

    public b(OutputStream outputStream, Map<Class<?>, hl2<?>> map, Map<Class<?>, zg4<?>> map2, hl2<Object> hl2Var) {
        this.a = outputStream;
        this.b = map;
        this.c = map2;
        this.d = hl2Var;
    }

    public static final /* synthetic */ void k(Map.Entry entry, com.google.firebase.encoders.c cVar) throws IOException {
        cVar.a(g, entry.getKey());
        cVar.a(h, entry.getValue());
    }

    public static ByteBuffer o(int i2) {
        return ByteBuffer.allocate(i2).order(ByteOrder.LITTLE_ENDIAN);
    }

    public static int p(h31 h31Var) {
        i iVar = (i) h31Var.c(i.class);
        if (iVar != null) {
            return iVar.zza();
        }
        throw new EncodingException("Field has no @Protobuf config");
    }

    public static i q(h31 h31Var) {
        i iVar = (i) h31Var.c(i.class);
        if (iVar != null) {
            return iVar;
        }
        throw new EncodingException("Field has no @Protobuf config");
    }

    @Override // com.google.firebase.encoders.c
    public final com.google.firebase.encoders.c a(h31 h31Var, Object obj) throws IOException {
        b(h31Var, obj, true);
        return this;
    }

    public final com.google.firebase.encoders.c b(h31 h31Var, Object obj, boolean z) throws IOException {
        if (obj == null) {
            return this;
        }
        if (obj instanceof CharSequence) {
            CharSequence charSequence = (CharSequence) obj;
            if (z && charSequence.length() == 0) {
                return this;
            }
            r((p(h31Var) << 3) | 2);
            byte[] bytes = charSequence.toString().getBytes(f);
            r(bytes.length);
            this.a.write(bytes);
            return this;
        } else if (obj instanceof Collection) {
            for (Object obj2 : (Collection) obj) {
                b(h31Var, obj2, false);
            }
            return this;
        } else if (obj instanceof Map) {
            for (Map.Entry entry : ((Map) obj).entrySet()) {
                l(i, h31Var, entry, false);
            }
            return this;
        } else if (obj instanceof Double) {
            c(h31Var, ((Double) obj).doubleValue(), z);
            return this;
        } else if (obj instanceof Float) {
            g(h31Var, ((Float) obj).floatValue(), z);
            return this;
        } else if (obj instanceof Number) {
            i(h31Var, ((Number) obj).longValue(), z);
            return this;
        } else if (obj instanceof Boolean) {
            h(h31Var, ((Boolean) obj).booleanValue() ? 1 : 0, z);
            return this;
        } else if (obj instanceof byte[]) {
            byte[] bArr = (byte[]) obj;
            if (z && bArr.length == 0) {
                return this;
            }
            r((p(h31Var) << 3) | 2);
            r(bArr.length);
            this.a.write(bArr);
            return this;
        } else {
            hl2<?> hl2Var = this.b.get(obj.getClass());
            if (hl2Var != null) {
                l(hl2Var, h31Var, obj, z);
                return this;
            }
            zg4<?> zg4Var = this.c.get(obj.getClass());
            if (zg4Var != null) {
                n(zg4Var, h31Var, obj, z);
                return this;
            } else if (obj instanceof q56) {
                h(h31Var, ((q56) obj).getNumber(), true);
                return this;
            } else if (obj instanceof Enum) {
                h(h31Var, ((Enum) obj).ordinal(), true);
                return this;
            } else {
                l(this.d, h31Var, obj, z);
                return this;
            }
        }
    }

    public final com.google.firebase.encoders.c c(h31 h31Var, double d, boolean z) throws IOException {
        if (z && d == Utils.DOUBLE_EPSILON) {
            return this;
        }
        r((p(h31Var) << 3) | 1);
        this.a.write(o(8).putDouble(d).array());
        return this;
    }

    @Override // com.google.firebase.encoders.c
    public final /* bridge */ /* synthetic */ com.google.firebase.encoders.c d(h31 h31Var, boolean z) throws IOException {
        h(h31Var, z ? 1 : 0, true);
        return this;
    }

    @Override // com.google.firebase.encoders.c
    public final /* bridge */ /* synthetic */ com.google.firebase.encoders.c e(h31 h31Var, int i2) throws IOException {
        h(h31Var, i2, true);
        return this;
    }

    @Override // com.google.firebase.encoders.c
    public final /* bridge */ /* synthetic */ com.google.firebase.encoders.c f(h31 h31Var, long j) throws IOException {
        i(h31Var, j, true);
        return this;
    }

    public final com.google.firebase.encoders.c g(h31 h31Var, float f2, boolean z) throws IOException {
        if (z && f2 == Utils.FLOAT_EPSILON) {
            return this;
        }
        r((p(h31Var) << 3) | 5);
        this.a.write(o(4).putFloat(f2).array());
        return this;
    }

    public final b h(h31 h31Var, int i2, boolean z) throws IOException {
        if (z && i2 == 0) {
            return this;
        }
        i q = q(h31Var);
        zzy zzyVar = zzy.DEFAULT;
        int ordinal = q.zzb().ordinal();
        if (ordinal == 0) {
            r(q.zza() << 3);
            r(i2);
        } else if (ordinal == 1) {
            r(q.zza() << 3);
            r((i2 + i2) ^ (i2 >> 31));
        } else if (ordinal == 2) {
            r((q.zza() << 3) | 5);
            this.a.write(o(4).putInt(i2).array());
        }
        return this;
    }

    public final b i(h31 h31Var, long j, boolean z) throws IOException {
        if (z && j == 0) {
            return this;
        }
        i q = q(h31Var);
        zzy zzyVar = zzy.DEFAULT;
        int ordinal = q.zzb().ordinal();
        if (ordinal == 0) {
            r(q.zza() << 3);
            s(j);
        } else if (ordinal == 1) {
            r(q.zza() << 3);
            s((j >> 63) ^ (j + j));
        } else if (ordinal == 2) {
            r((q.zza() << 3) | 1);
            this.a.write(o(8).putLong(j).array());
        }
        return this;
    }

    public final b j(Object obj) throws IOException {
        if (obj == null) {
            return this;
        }
        hl2<?> hl2Var = this.b.get(obj.getClass());
        if (hl2Var != null) {
            hl2Var.a(obj, this);
            return this;
        }
        String valueOf = String.valueOf(obj.getClass());
        StringBuilder sb = new StringBuilder(valueOf.length() + 15);
        sb.append("No encoder for ");
        sb.append(valueOf);
        throw new EncodingException(sb.toString());
    }

    public final <T> b l(hl2<T> hl2Var, h31 h31Var, T t, boolean z) throws IOException {
        long m = m(hl2Var, t);
        if (z && m == 0) {
            return this;
        }
        r((p(h31Var) << 3) | 2);
        s(m);
        hl2Var.a(t, this);
        return this;
    }

    public final <T> long m(hl2<T> hl2Var, T t) throws IOException {
        l56 l56Var = new l56();
        try {
            OutputStream outputStream = this.a;
            this.a = l56Var;
            hl2Var.a(t, this);
            this.a = outputStream;
            long a = l56Var.a();
            l56Var.close();
            return a;
        } catch (Throwable th) {
            try {
                l56Var.close();
            } catch (Throwable th2) {
                t46.a(th, th2);
            }
            throw th;
        }
    }

    public final <T> b n(zg4<T> zg4Var, h31 h31Var, T t, boolean z) throws IOException {
        this.e.a(h31Var, z);
        zg4Var.a(t, this.e);
        return this;
    }

    public final void r(int i2) throws IOException {
        while ((i2 & (-128)) != 0) {
            this.a.write((i2 & 127) | 128);
            i2 >>>= 7;
        }
        this.a.write(i2 & 127);
    }

    public final void s(long j) throws IOException {
        while (((-128) & j) != 0) {
            this.a.write((((int) j) & 127) | 128);
            j >>>= 7;
        }
        this.a.write(((int) j) & 127);
    }
}
