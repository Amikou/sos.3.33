package com.google.android.gms.internal.firebase_messaging;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

/* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
/* loaded from: classes.dex */
public final class c {
    public final Map<Class<?>, hl2<?>> a;
    public final Map<Class<?>, zg4<?>> b;
    public final hl2<Object> c;

    public c(Map<Class<?>, hl2<?>> map, Map<Class<?>, zg4<?>> map2, hl2<Object> hl2Var) {
        this.a = map;
        this.b = map2;
        this.c = hl2Var;
    }

    public final void a(Object obj, OutputStream outputStream) throws IOException {
        new b(outputStream, this.a, this.b, this.c).j(obj);
    }
}
