package com.google.android.gms.internal.vision;

import java.io.IOException;
import java.util.Arrays;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public final class x0 {
    public static final x0 f = new x0(0, new int[0], new Object[0], false);
    public int a;
    public int[] b;
    public Object[] c;
    public int d;
    public boolean e;

    public x0() {
        this(0, new int[8], new Object[8], true);
    }

    public static x0 a() {
        return f;
    }

    public static x0 b(x0 x0Var, x0 x0Var2) {
        int i = x0Var.a + x0Var2.a;
        int[] copyOf = Arrays.copyOf(x0Var.b, i);
        System.arraycopy(x0Var2.b, 0, copyOf, x0Var.a, x0Var2.a);
        Object[] copyOf2 = Arrays.copyOf(x0Var.c, i);
        System.arraycopy(x0Var2.c, 0, copyOf2, x0Var.a, x0Var2.a);
        return new x0(i, copyOf, copyOf2, true);
    }

    public static void d(int i, Object obj, g1 g1Var) throws IOException {
        int i2 = i >>> 3;
        int i3 = i & 7;
        if (i3 == 0) {
            g1Var.q(i2, ((Long) obj).longValue());
        } else if (i3 == 1) {
            g1Var.J(i2, ((Long) obj).longValue());
        } else if (i3 == 2) {
            g1Var.F(i2, (zzht) obj);
        } else if (i3 != 3) {
            if (i3 == 5) {
                g1Var.A(i2, ((Integer) obj).intValue());
                return;
            }
            throw new RuntimeException(zzjk.zzf());
        } else if (g1Var.zza() == oz5.a) {
            g1Var.d(i2);
            ((x0) obj).h(g1Var);
            g1Var.h(i2);
        } else {
            g1Var.h(i2);
            ((x0) obj).h(g1Var);
            g1Var.d(i2);
        }
    }

    public static x0 g() {
        return new x0();
    }

    public final void c(int i, Object obj) {
        if (this.e) {
            int i2 = this.a;
            int[] iArr = this.b;
            if (i2 == iArr.length) {
                int i3 = i2 + (i2 < 4 ? 8 : i2 >> 1);
                this.b = Arrays.copyOf(iArr, i3);
                this.c = Arrays.copyOf(this.c, i3);
            }
            int[] iArr2 = this.b;
            int i4 = this.a;
            iArr2[i4] = i;
            this.c[i4] = obj;
            this.a = i4 + 1;
            return;
        }
        throw new UnsupportedOperationException();
    }

    public final void e(g1 g1Var) throws IOException {
        if (g1Var.zza() == oz5.b) {
            for (int i = this.a - 1; i >= 0; i--) {
                g1Var.j(this.b[i] >>> 3, this.c[i]);
            }
            return;
        }
        for (int i2 = 0; i2 < this.a; i2++) {
            g1Var.j(this.b[i2] >>> 3, this.c[i2]);
        }
    }

    public final boolean equals(Object obj) {
        boolean z;
        boolean z2;
        if (this == obj) {
            return true;
        }
        if (obj != null && (obj instanceof x0)) {
            x0 x0Var = (x0) obj;
            int i = this.a;
            if (i == x0Var.a) {
                int[] iArr = this.b;
                int[] iArr2 = x0Var.b;
                int i2 = 0;
                while (true) {
                    if (i2 >= i) {
                        z = true;
                        break;
                    } else if (iArr[i2] != iArr2[i2]) {
                        z = false;
                        break;
                    } else {
                        i2++;
                    }
                }
                if (z) {
                    Object[] objArr = this.c;
                    Object[] objArr2 = x0Var.c;
                    int i3 = this.a;
                    int i4 = 0;
                    while (true) {
                        if (i4 >= i3) {
                            z2 = true;
                            break;
                        } else if (!objArr[i4].equals(objArr2[i4])) {
                            z2 = false;
                            break;
                        } else {
                            i4++;
                        }
                    }
                    if (z2) {
                        return true;
                    }
                }
            }
            return false;
        }
        return false;
    }

    public final void f(StringBuilder sb, int i) {
        for (int i2 = 0; i2 < this.a; i2++) {
            p0.d(sb, i, String.valueOf(this.b[i2] >>> 3), this.c[i2]);
        }
    }

    public final void h(g1 g1Var) throws IOException {
        if (this.a == 0) {
            return;
        }
        if (g1Var.zza() == oz5.a) {
            for (int i = 0; i < this.a; i++) {
                d(this.b[i], this.c[i], g1Var);
            }
            return;
        }
        for (int i2 = this.a - 1; i2 >= 0; i2--) {
            d(this.b[i2], this.c[i2], g1Var);
        }
    }

    public final int hashCode() {
        int i = this.a;
        int i2 = (i + 527) * 31;
        int[] iArr = this.b;
        int i3 = 17;
        int i4 = 17;
        for (int i5 = 0; i5 < i; i5++) {
            i4 = (i4 * 31) + iArr[i5];
        }
        int i6 = (i2 + i4) * 31;
        Object[] objArr = this.c;
        int i7 = this.a;
        for (int i8 = 0; i8 < i7; i8++) {
            i3 = (i3 * 31) + objArr[i8].hashCode();
        }
        return i6 + i3;
    }

    public final void i() {
        this.e = false;
    }

    public final int j() {
        int i = this.d;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < this.a; i3++) {
            i2 += zzii.c0(this.b[i3] >>> 3, (zzht) this.c[i3]);
        }
        this.d = i2;
        return i2;
    }

    public final int k() {
        int h0;
        int i = this.d;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < this.a; i3++) {
            int i4 = this.b[i3];
            int i5 = i4 >>> 3;
            int i6 = i4 & 7;
            if (i6 == 0) {
                h0 = zzii.h0(i5, ((Long) this.c[i3]).longValue());
            } else if (i6 == 1) {
                h0 = zzii.q0(i5, ((Long) this.c[i3]).longValue());
            } else if (i6 == 2) {
                h0 = zzii.T(i5, (zzht) this.c[i3]);
            } else if (i6 == 3) {
                h0 = (zzii.g0(i5) << 1) + ((x0) this.c[i3]).k();
            } else if (i6 == 5) {
                h0 = zzii.x0(i5, ((Integer) this.c[i3]).intValue());
            } else {
                throw new IllegalStateException(zzjk.zzf());
            }
            i2 += h0;
        }
        this.d = i2;
        return i2;
    }

    public x0(int i, int[] iArr, Object[] objArr, boolean z) {
        this.d = -1;
        this.a = i;
        this.b = iArr;
        this.c = objArr;
        this.e = z;
    }
}
