package com.google.android.gms.internal.vision;

import android.os.IInterface;
import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-vision@@20.1.3 */
/* loaded from: classes.dex */
public interface h1 extends IInterface {
    s0 g0(lm1 lm1Var, zzk zzkVar) throws RemoteException;
}
