package com.google.android.gms.internal.vision;

import android.content.Context;
import android.content.Intent;
import android.os.RemoteException;
import com.google.android.gms.dynamite.DynamiteModule;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public abstract class k1<T> {
    public final Context a;
    public final String c;
    public final String d;
    public final String e;
    public T h;
    public final Object b = new Object();
    public boolean f = false;
    public boolean g = false;

    public k1(Context context, String str, String str2) {
        this.a = context;
        this.c = str;
        String valueOf = String.valueOf(str2);
        this.d = valueOf.length() != 0 ? "com.google.android.gms.vision.dynamite.".concat(valueOf) : new String("com.google.android.gms.vision.dynamite.");
        this.e = str2;
    }

    public abstract T a(DynamiteModule dynamiteModule, Context context) throws RemoteException, DynamiteModule.LoadingException;

    public abstract void b() throws RemoteException;

    public final boolean c() {
        return e() != null;
    }

    public final void d() {
        synchronized (this.b) {
            if (this.h == null) {
                return;
            }
            try {
                b();
            } catch (RemoteException unused) {
            }
        }
    }

    public final T e() {
        synchronized (this.b) {
            T t = this.h;
            if (t != null) {
                return t;
            }
            DynamiteModule dynamiteModule = null;
            try {
                dynamiteModule = DynamiteModule.d(this.a, DynamiteModule.k, this.d);
            } catch (DynamiteModule.LoadingException unused) {
                String format = String.format("%s.%s", "com.google.android.gms.vision", this.e);
                zx1.a("Cannot load thick client module, fall back to load optional module %s", format);
                try {
                    dynamiteModule = DynamiteModule.d(this.a, DynamiteModule.i, format);
                } catch (DynamiteModule.LoadingException e) {
                    zx1.c(e, "Error loading optional module %s", format);
                    if (!this.f) {
                        zx1.a("Broadcasting download intent for dependency %s", this.e);
                        String str = this.e;
                        Intent intent = new Intent();
                        intent.setClassName("com.google.android.gms", "com.google.android.gms.vision.DependencyBroadcastReceiverProxy");
                        intent.putExtra("com.google.android.gms.vision.DEPENDENCIES", str);
                        intent.setAction("com.google.android.gms.vision.DEPENDENCY");
                        this.a.sendBroadcast(intent);
                        this.f = true;
                    }
                }
            }
            if (dynamiteModule != null) {
                try {
                    this.h = a(dynamiteModule, this.a);
                } catch (RemoteException | DynamiteModule.LoadingException unused2) {
                }
            }
            boolean z = this.g;
            if (!z && this.h == null) {
                this.g = true;
            } else if (z) {
                T t2 = this.h;
            }
            return this.h;
        }
    }
}
