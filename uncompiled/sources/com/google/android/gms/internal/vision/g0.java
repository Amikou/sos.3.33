package com.google.android.gms.internal.vision;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public final class g0 implements g1 {
    public final zzii a;

    public g0(zzii zziiVar) {
        zzii zziiVar2 = (zzii) vs5.f(zziiVar, "output");
        this.a = zziiVar2;
        zziiVar2.a = this;
    }

    public static g0 O(zzii zziiVar) {
        g0 g0Var = zziiVar.a;
        return g0Var != null ? g0Var : new g0(zziiVar);
    }

    @Override // com.google.android.gms.internal.vision.g1
    public final void A(int i, int i2) throws IOException {
        this.a.j0(i, i2);
    }

    @Override // com.google.android.gms.internal.vision.g1
    public final void B(int i, List<Float> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.m(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzii.A(list.get(i4).floatValue());
            }
            this.a.O(i3);
            while (i2 < list.size()) {
                this.a.i(list.get(i2).floatValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.l(i, list.get(i2).floatValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.vision.g1
    public final void C(int i, int i2) throws IOException {
        this.a.X(i, i2);
    }

    @Override // com.google.android.gms.internal.vision.g1
    public final void D(int i, List<String> list) throws IOException {
        int i2 = 0;
        if (list instanceof gu5) {
            gu5 gu5Var = (gu5) list;
            while (i2 < list.size()) {
                Object h = gu5Var.h(i2);
                if (h instanceof String) {
                    this.a.r(i, (String) h);
                } else {
                    this.a.o(i, (zzht) h);
                }
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.r(i, list.get(i2));
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.vision.g1
    public final void E(int i, boolean z) throws IOException {
        this.a.s(i, z);
    }

    @Override // com.google.android.gms.internal.vision.g1
    public final void F(int i, zzht zzhtVar) throws IOException {
        this.a.o(i, zzhtVar);
    }

    @Override // com.google.android.gms.internal.vision.g1
    public final void G(int i, int i2) throws IOException {
        this.a.P(i, i2);
    }

    @Override // com.google.android.gms.internal.vision.g1
    public final void H(int i, List<?> list, t0 t0Var) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            M(i, list.get(i2), t0Var);
        }
    }

    @Override // com.google.android.gms.internal.vision.g1
    public final void I(int i, Object obj, t0 t0Var) throws IOException {
        this.a.q(i, (n0) obj, t0Var);
    }

    @Override // com.google.android.gms.internal.vision.g1
    public final void J(int i, long j) throws IOException {
        this.a.Y(i, j);
    }

    @Override // com.google.android.gms.internal.vision.g1
    public final <K, V> void K(int i, lv5<K, V> lv5Var, Map<K, V> map) throws IOException {
        for (Map.Entry<K, V> entry : map.entrySet()) {
            this.a.m(i, 2);
            this.a.O(m0.a(lv5Var, entry.getKey(), entry.getValue()));
            m0.b(this.a, lv5Var, entry.getKey(), entry.getValue());
        }
    }

    @Override // com.google.android.gms.internal.vision.g1
    public final void L(int i, long j) throws IOException {
        this.a.Q(i, j);
    }

    @Override // com.google.android.gms.internal.vision.g1
    public final void M(int i, Object obj, t0 t0Var) throws IOException {
        zzii zziiVar = this.a;
        zziiVar.m(i, 3);
        t0Var.i((n0) obj, zziiVar.a);
        zziiVar.m(i, 4);
    }

    @Override // com.google.android.gms.internal.vision.g1
    public final void N(int i, List<?> list, t0 t0Var) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            I(i, list.get(i2), t0Var);
        }
    }

    @Override // com.google.android.gms.internal.vision.g1
    public final void a(int i, List<Boolean> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.m(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzii.L(list.get(i4).booleanValue());
            }
            this.a.O(i3);
            while (i2 < list.size()) {
                this.a.y(list.get(i2).booleanValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.s(i, list.get(i2).booleanValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.vision.g1
    public final void b(int i, long j) throws IOException {
        this.a.Y(i, j);
    }

    @Override // com.google.android.gms.internal.vision.g1
    public final void c(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.m(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzii.k0(list.get(i4).intValue());
            }
            this.a.O(i3);
            while (i2 < list.size()) {
                this.a.j(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.P(i, list.get(i2).intValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.vision.g1
    public final void d(int i) throws IOException {
        this.a.m(i, 3);
    }

    @Override // com.google.android.gms.internal.vision.g1
    public final void e(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.m(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzii.B0(list.get(i4).intValue());
            }
            this.a.O(i3);
            while (i2 < list.size()) {
                this.a.j(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.P(i, list.get(i2).intValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.vision.g1
    public final void f(int i, int i2) throws IOException {
        this.a.P(i, i2);
    }

    @Override // com.google.android.gms.internal.vision.g1
    public final void g(int i, long j) throws IOException {
        this.a.n(i, j);
    }

    @Override // com.google.android.gms.internal.vision.g1
    public final void h(int i) throws IOException {
        this.a.m(i, 4);
    }

    @Override // com.google.android.gms.internal.vision.g1
    public final void i(int i, int i2) throws IOException {
        this.a.j0(i, i2);
    }

    @Override // com.google.android.gms.internal.vision.g1
    public final void j(int i, Object obj) throws IOException {
        if (obj instanceof zzht) {
            this.a.R(i, (zzht) obj);
        } else {
            this.a.p(i, (n0) obj);
        }
    }

    @Override // com.google.android.gms.internal.vision.g1
    public final void k(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.m(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzii.d0(list.get(i4).longValue());
            }
            this.a.O(i3);
            while (i2 < list.size()) {
                this.a.t(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.n(i, list.get(i2).longValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.vision.g1
    public final void l(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.m(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzii.v0(list.get(i4).longValue());
            }
            this.a.O(i3);
            while (i2 < list.size()) {
                this.a.Z(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.Y(i, list.get(i2).longValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.vision.g1
    public final void m(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.m(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzii.w0(list.get(i4).intValue());
            }
            this.a.O(i3);
            while (i2 < list.size()) {
                this.a.e0(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.j0(i, list.get(i2).intValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.vision.g1
    public final void n(int i, String str) throws IOException {
        this.a.r(i, str);
    }

    @Override // com.google.android.gms.internal.vision.g1
    public final void o(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.m(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzii.o0(list.get(i4).intValue());
            }
            this.a.O(i3);
            while (i2 < list.size()) {
                this.a.O(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.X(i, list.get(i2).intValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.vision.g1
    public final void p(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.m(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzii.z0(list.get(i4).intValue());
            }
            this.a.O(i3);
            while (i2 < list.size()) {
                this.a.e0(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.j0(i, list.get(i2).intValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.vision.g1
    public final void q(int i, long j) throws IOException {
        this.a.n(i, j);
    }

    @Override // com.google.android.gms.internal.vision.g1
    public final void r(int i, double d) throws IOException {
        this.a.k(i, d);
    }

    @Override // com.google.android.gms.internal.vision.g1
    public final void s(int i, float f) throws IOException {
        this.a.l(i, f);
    }

    @Override // com.google.android.gms.internal.vision.g1
    public final void t(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.m(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzii.r0(list.get(i4).longValue());
            }
            this.a.O(i3);
            while (i2 < list.size()) {
                this.a.Z(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.Y(i, list.get(i2).longValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.vision.g1
    public final void u(int i, int i2) throws IOException {
        this.a.f0(i, i2);
    }

    @Override // com.google.android.gms.internal.vision.g1
    public final void v(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.m(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzii.n0(list.get(i4).longValue());
            }
            this.a.O(i3);
            while (i2 < list.size()) {
                this.a.S(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.Q(i, list.get(i2).longValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.vision.g1
    public final void w(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.m(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzii.i0(list.get(i4).longValue());
            }
            this.a.O(i3);
            while (i2 < list.size()) {
                this.a.t(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.n(i, list.get(i2).longValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.vision.g1
    public final void x(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.m(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzii.s0(list.get(i4).intValue());
            }
            this.a.O(i3);
            while (i2 < list.size()) {
                this.a.W(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.f0(i, list.get(i2).intValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.vision.g1
    public final void y(int i, List<Double> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.m(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzii.z(list.get(i4).doubleValue());
            }
            this.a.O(i3);
            while (i2 < list.size()) {
                this.a.h(list.get(i2).doubleValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.k(i, list.get(i2).doubleValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.vision.g1
    public final void z(int i, List<zzht> list) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            this.a.o(i, list.get(i2));
        }
    }

    @Override // com.google.android.gms.internal.vision.g1
    public final int zza() {
        return oz5.a;
    }
}
