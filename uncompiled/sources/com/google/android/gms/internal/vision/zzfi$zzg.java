package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.vision.l0;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public final class zzfi$zzg extends l0<zzfi$zzg, a> implements ew5 {
    private static final zzfi$zzg zzj;
    private static volatile xw5<zzfi$zzg> zzk;
    private int zzc;
    private int zzd;
    private int zze;
    private int zzf;
    private boolean zzg;
    private boolean zzh;
    private float zzi;

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* loaded from: classes.dex */
    public static final class a extends l0.b<zzfi$zzg, a> implements ew5 {
        public a() {
            super(zzfi$zzg.zzj);
        }

        public /* synthetic */ a(m mVar) {
            this();
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* loaded from: classes.dex */
    public enum zzb implements rs5 {
        CLASSIFICATION_UNKNOWN(0),
        CLASSIFICATION_NONE(1),
        CLASSIFICATION_ALL(2);
        
        private final int zze;

        static {
            new r();
        }

        zzb(int i) {
            this.zze = i;
        }

        public static ws5 zzb() {
            return s.a;
        }

        @Override // java.lang.Enum
        public final String toString() {
            return "<" + zzb.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.zze + " name=" + name() + '>';
        }

        @Override // defpackage.rs5
        public final int zza() {
            return this.zze;
        }

        public static zzb zza(int i) {
            if (i != 0) {
                if (i != 1) {
                    if (i != 2) {
                        return null;
                    }
                    return CLASSIFICATION_ALL;
                }
                return CLASSIFICATION_NONE;
            }
            return CLASSIFICATION_UNKNOWN;
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* loaded from: classes.dex */
    public enum zzc implements rs5 {
        LANDMARK_UNKNOWN(0),
        LANDMARK_NONE(1),
        LANDMARK_ALL(2),
        LANDMARK_CONTOUR(3);
        
        private final int zzf;

        static {
            new u();
        }

        zzc(int i) {
            this.zzf = i;
        }

        public static ws5 zzb() {
            return t.a;
        }

        @Override // java.lang.Enum
        public final String toString() {
            return "<" + zzc.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.zzf + " name=" + name() + '>';
        }

        @Override // defpackage.rs5
        public final int zza() {
            return this.zzf;
        }

        public static zzc zza(int i) {
            if (i != 0) {
                if (i != 1) {
                    if (i != 2) {
                        if (i != 3) {
                            return null;
                        }
                        return LANDMARK_CONTOUR;
                    }
                    return LANDMARK_ALL;
                }
                return LANDMARK_NONE;
            }
            return LANDMARK_UNKNOWN;
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* loaded from: classes.dex */
    public enum zzd implements rs5 {
        MODE_UNKNOWN(0),
        MODE_ACCURATE(1),
        MODE_FAST(2),
        MODE_SELFIE(3);
        
        private final int zzf;

        static {
            new v();
        }

        zzd(int i) {
            this.zzf = i;
        }

        public static ws5 zzb() {
            return w.a;
        }

        @Override // java.lang.Enum
        public final String toString() {
            return "<" + zzd.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.zzf + " name=" + name() + '>';
        }

        @Override // defpackage.rs5
        public final int zza() {
            return this.zzf;
        }

        public static zzd zza(int i) {
            if (i != 0) {
                if (i != 1) {
                    if (i != 2) {
                        if (i != 3) {
                            return null;
                        }
                        return MODE_SELFIE;
                    }
                    return MODE_FAST;
                }
                return MODE_ACCURATE;
            }
            return MODE_UNKNOWN;
        }
    }

    static {
        zzfi$zzg zzfi_zzg = new zzfi$zzg();
        zzj = zzfi_zzg;
        l0.s(zzfi$zzg.class, zzfi_zzg);
    }

    /* JADX WARN: Type inference failed for: r2v14, types: [xw5<com.google.android.gms.internal.vision.zzfi$zzg>, com.google.android.gms.internal.vision.l0$a] */
    @Override // com.google.android.gms.internal.vision.l0
    public final Object o(int i, Object obj, Object obj2) {
        xw5<zzfi$zzg> xw5Var;
        switch (m.a[i - 1]) {
            case 1:
                return new zzfi$zzg();
            case 2:
                return new a(null);
            case 3:
                return l0.p(zzj, "\u0001\u0006\u0000\u0001\u0001\u0006\u0006\u0000\u0000\u0000\u0001ဌ\u0000\u0002ဌ\u0001\u0003ဌ\u0002\u0004ဇ\u0003\u0005ဇ\u0004\u0006ခ\u0005", new Object[]{"zzc", "zzd", zzd.zzb(), "zze", zzc.zzb(), "zzf", zzb.zzb(), "zzg", "zzh", "zzi"});
            case 4:
                return zzj;
            case 5:
                xw5<zzfi$zzg> xw5Var2 = zzk;
                xw5<zzfi$zzg> xw5Var3 = xw5Var2;
                if (xw5Var2 == null) {
                    synchronized (zzfi$zzg.class) {
                        xw5<zzfi$zzg> xw5Var4 = zzk;
                        xw5Var = xw5Var4;
                        if (xw5Var4 == null) {
                            ?? aVar = new l0.a(zzj);
                            zzk = aVar;
                            xw5Var = aVar;
                        }
                    }
                    xw5Var3 = xw5Var;
                }
                return xw5Var3;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
