package com.google.android.gms.internal.vision;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

/* compiled from: com.google.android.gms:play-services-vision@@20.1.3 */
/* loaded from: classes.dex */
public final class zzao extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzao> CREATOR = new b65();
    public final zzal[] a;
    public final zzab f0;
    public final zzab g0;
    public final String h0;
    public final float i0;
    public final String j0;
    public final boolean k0;

    public zzao(zzal[] zzalVarArr, zzab zzabVar, zzab zzabVar2, String str, float f, String str2, boolean z) {
        this.a = zzalVarArr;
        this.f0 = zzabVar;
        this.g0 = zzabVar2;
        this.h0 = str;
        this.i0 = f;
        this.j0 = str2;
        this.k0 = z;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int a = yb3.a(parcel);
        yb3.v(parcel, 2, this.a, i, false);
        yb3.r(parcel, 3, this.f0, i, false);
        yb3.r(parcel, 4, this.g0, i, false);
        yb3.s(parcel, 5, this.h0, false);
        yb3.j(parcel, 6, this.i0);
        yb3.s(parcel, 7, this.j0, false);
        yb3.c(parcel, 8, this.k0);
        yb3.b(parcel, a);
    }
}
