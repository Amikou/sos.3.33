package com.google.android.gms.internal.vision;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public final class q0<T> implements t0<T> {
    public final n0 a;
    public final v0<?, ?> b;
    public final boolean c;
    public final j0<?> d;

    public q0(v0<?, ?> v0Var, j0<?> j0Var, n0 n0Var) {
        this.b = v0Var;
        this.c = j0Var.e(n0Var);
        this.d = j0Var;
        this.a = n0Var;
    }

    public static <T> q0<T> d(v0<?, ?> v0Var, j0<?> j0Var, n0 n0Var) {
        return new q0<>(v0Var, j0Var, n0Var);
    }

    @Override // com.google.android.gms.internal.vision.t0
    public final void a(T t) {
        this.b.j(t);
        this.d.g(t);
    }

    @Override // com.google.android.gms.internal.vision.t0
    public final int b(T t) {
        int hashCode = this.b.f(t).hashCode();
        return this.c ? (hashCode * 53) + this.d.b(t).hashCode() : hashCode;
    }

    @Override // com.google.android.gms.internal.vision.t0
    public final void c(T t, T t2) {
        u0.n(this.b, t, t2);
        if (this.c) {
            u0.m(this.d, t, t2);
        }
    }

    @Override // com.google.android.gms.internal.vision.t0
    public final boolean e(T t) {
        return this.d.b(t).r();
    }

    @Override // com.google.android.gms.internal.vision.t0
    public final int f(T t) {
        v0<?, ?> v0Var = this.b;
        int k = v0Var.k(v0Var.f(t)) + 0;
        return this.c ? k + this.d.b(t).s() : k;
    }

    @Override // com.google.android.gms.internal.vision.t0
    public final boolean g(T t, T t2) {
        if (this.b.f(t).equals(this.b.f(t2))) {
            if (this.c) {
                return this.d.b(t).equals(this.d.b(t2));
            }
            return true;
        }
        return false;
    }

    /* JADX WARN: Removed duplicated region for block: B:32:0x00b9  */
    /* JADX WARN: Removed duplicated region for block: B:57:0x00be A[EDGE_INSN: B:57:0x00be->B:33:0x00be ?: BREAK  , SYNTHETIC] */
    @Override // com.google.android.gms.internal.vision.t0
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void h(T r11, byte[] r12, int r13, int r14, defpackage.go5 r15) throws java.io.IOException {
        /*
            r10 = this;
            r0 = r11
            com.google.android.gms.internal.vision.l0 r0 = (com.google.android.gms.internal.vision.l0) r0
            com.google.android.gms.internal.vision.x0 r1 = r0.zzb
            com.google.android.gms.internal.vision.x0 r2 = com.google.android.gms.internal.vision.x0.a()
            if (r1 != r2) goto L11
            com.google.android.gms.internal.vision.x0 r1 = com.google.android.gms.internal.vision.x0.g()
            r0.zzb = r1
        L11:
            com.google.android.gms.internal.vision.l0$c r11 = (com.google.android.gms.internal.vision.l0.c) r11
            com.google.android.gms.internal.vision.k0 r11 = r11.x()
            r0 = 0
            r2 = r0
        L19:
            if (r13 >= r14) goto Lc9
            int r4 = com.google.android.gms.internal.vision.c0.i(r12, r13, r15)
            int r13 = r15.a
            r3 = 11
            r5 = 2
            if (r13 == r3) goto L65
            r3 = r13 & 7
            if (r3 != r5) goto L60
            com.google.android.gms.internal.vision.j0<?> r2 = r10.d
            com.google.android.gms.internal.vision.h0 r3 = r15.d
            com.google.android.gms.internal.vision.n0 r5 = r10.a
            int r6 = r13 >>> 3
            java.lang.Object r2 = r2.c(r3, r5, r6)
            r8 = r2
            com.google.android.gms.internal.vision.l0$d r8 = (com.google.android.gms.internal.vision.l0.d) r8
            if (r8 == 0) goto L55
            zw5 r13 = defpackage.zw5.c()
            com.google.android.gms.internal.vision.n0 r2 = r8.a
            java.lang.Class r2 = r2.getClass()
            com.google.android.gms.internal.vision.t0 r13 = r13.a(r2)
            int r13 = com.google.android.gms.internal.vision.c0.g(r13, r12, r4, r14, r15)
            com.google.android.gms.internal.vision.l0$e r2 = r8.b
            java.lang.Object r3 = r15.c
            r11.g(r2, r3)
            goto L5e
        L55:
            r2 = r13
            r3 = r12
            r5 = r14
            r6 = r1
            r7 = r15
            int r13 = com.google.android.gms.internal.vision.c0.a(r2, r3, r4, r5, r6, r7)
        L5e:
            r2 = r8
            goto L19
        L60:
            int r13 = com.google.android.gms.internal.vision.c0.b(r13, r12, r4, r14, r15)
            goto L19
        L65:
            r13 = 0
            r3 = r0
        L67:
            if (r4 >= r14) goto Lbe
            int r4 = com.google.android.gms.internal.vision.c0.i(r12, r4, r15)
            int r6 = r15.a
            int r7 = r6 >>> 3
            r8 = r6 & 7
            if (r7 == r5) goto La0
            r9 = 3
            if (r7 == r9) goto L79
            goto Lb5
        L79:
            if (r2 == 0) goto L95
            zw5 r6 = defpackage.zw5.c()
            com.google.android.gms.internal.vision.n0 r7 = r2.a
            java.lang.Class r7 = r7.getClass()
            com.google.android.gms.internal.vision.t0 r6 = r6.a(r7)
            int r4 = com.google.android.gms.internal.vision.c0.g(r6, r12, r4, r14, r15)
            com.google.android.gms.internal.vision.l0$e r6 = r2.b
            java.lang.Object r7 = r15.c
            r11.g(r6, r7)
            goto L67
        L95:
            if (r8 != r5) goto Lb5
            int r4 = com.google.android.gms.internal.vision.c0.q(r12, r4, r15)
            java.lang.Object r3 = r15.c
            com.google.android.gms.internal.vision.zzht r3 = (com.google.android.gms.internal.vision.zzht) r3
            goto L67
        La0:
            if (r8 != 0) goto Lb5
            int r4 = com.google.android.gms.internal.vision.c0.i(r12, r4, r15)
            int r13 = r15.a
            com.google.android.gms.internal.vision.j0<?> r2 = r10.d
            com.google.android.gms.internal.vision.h0 r6 = r15.d
            com.google.android.gms.internal.vision.n0 r7 = r10.a
            java.lang.Object r2 = r2.c(r6, r7, r13)
            com.google.android.gms.internal.vision.l0$d r2 = (com.google.android.gms.internal.vision.l0.d) r2
            goto L67
        Lb5:
            r7 = 12
            if (r6 == r7) goto Lbe
            int r4 = com.google.android.gms.internal.vision.c0.b(r6, r12, r4, r14, r15)
            goto L67
        Lbe:
            if (r3 == 0) goto Lc6
            int r13 = r13 << 3
            r13 = r13 | r5
            r1.c(r13, r3)
        Lc6:
            r13 = r4
            goto L19
        Lc9:
            if (r13 != r14) goto Lcc
            return
        Lcc:
            com.google.android.gms.internal.vision.zzjk r11 = com.google.android.gms.internal.vision.zzjk.zzg()
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.vision.q0.h(java.lang.Object, byte[], int, int, go5):void");
    }

    @Override // com.google.android.gms.internal.vision.t0
    public final void i(T t, g1 g1Var) throws IOException {
        Iterator<Map.Entry<?, Object>> o = this.d.b(t).o();
        while (o.hasNext()) {
            Map.Entry<?, Object> next = o.next();
            pr5 pr5Var = (pr5) next.getKey();
            if (pr5Var.zzc() == zzmo.MESSAGE && !pr5Var.b() && !pr5Var.c()) {
                if (next instanceof ut5) {
                    g1Var.j(pr5Var.zza(), ((ut5) next).a().d());
                } else {
                    g1Var.j(pr5Var.zza(), next.getValue());
                }
            } else {
                throw new IllegalStateException("Found invalid MessageSet item.");
            }
        }
        v0<?, ?> v0Var = this.b;
        v0Var.g(v0Var.f(t), g1Var);
    }

    @Override // com.google.android.gms.internal.vision.t0
    public final T zza() {
        return (T) this.a.j().c();
    }
}
