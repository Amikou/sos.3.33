package com.google.android.gms.internal.vision;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.RandomAccess;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public final class u0 {
    public static final Class<?> a = F();
    public static final v0<?, ?> b = g(false);
    public static final v0<?, ?> c = g(true);
    public static final v0<?, ?> d = new w0();

    public static int A(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof nu5) {
            nu5 nu5Var = (nu5) list;
            i = 0;
            while (i2 < size) {
                i += zzii.n0(nu5Var.k(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += zzii.n0(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    public static v0<?, ?> B() {
        return d;
    }

    public static void C(int i, List<Long> list, g1 g1Var, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        g1Var.k(i, list, z);
    }

    public static int D(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return E(list) + (size * zzii.g0(i));
    }

    public static int E(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof qs5) {
            qs5 qs5Var = (qs5) list;
            i = 0;
            while (i2 < size) {
                i += zzii.B0(qs5Var.i(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += zzii.B0(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    public static Class<?> F() {
        try {
            return Class.forName("com.google.protobuf.GeneratedMessage");
        } catch (Throwable unused) {
            return null;
        }
    }

    public static void G(int i, List<Long> list, g1 g1Var, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        g1Var.w(i, list, z);
    }

    public static int H(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return I(list) + (size * zzii.g0(i));
    }

    public static int I(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof qs5) {
            qs5 qs5Var = (qs5) list;
            i = 0;
            while (i2 < size) {
                i += zzii.k0(qs5Var.i(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += zzii.k0(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    public static Class<?> J() {
        return com.google.protobuf.j1.class;
    }

    public static void K(int i, List<Long> list, g1 g1Var, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        g1Var.v(i, list, z);
    }

    public static int L(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return M(list) + (size * zzii.g0(i));
    }

    public static int M(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof qs5) {
            qs5 qs5Var = (qs5) list;
            i = 0;
            while (i2 < size) {
                i += zzii.o0(qs5Var.i(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += zzii.o0(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    public static void N(int i, List<Long> list, g1 g1Var, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        g1Var.t(i, list, z);
    }

    public static int O(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return P(list) + (size * zzii.g0(i));
    }

    public static int P(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof qs5) {
            qs5 qs5Var = (qs5) list;
            i = 0;
            while (i2 < size) {
                i += zzii.s0(qs5Var.i(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += zzii.s0(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    public static void Q(int i, List<Long> list, g1 g1Var, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        g1Var.l(i, list, z);
    }

    public static int R(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return size * zzii.x0(i, 0);
    }

    public static int S(List<?> list) {
        return list.size() << 2;
    }

    public static void T(int i, List<Integer> list, g1 g1Var, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        g1Var.c(i, list, z);
    }

    public static int U(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return size * zzii.q0(i, 0L);
    }

    public static int V(List<?> list) {
        return list.size() << 3;
    }

    public static void W(int i, List<Integer> list, g1 g1Var, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        g1Var.o(i, list, z);
    }

    public static int X(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return size * zzii.H(i, true);
    }

    public static int Y(List<?> list) {
        return list.size();
    }

    public static void Z(int i, List<Integer> list, g1 g1Var, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        g1Var.x(i, list, z);
    }

    public static int a(int i, Object obj, t0 t0Var) {
        if (obj instanceof au5) {
            return zzii.c(i, (au5) obj);
        }
        return zzii.E(i, (n0) obj, t0Var);
    }

    public static void a0(int i, List<Integer> list, g1 g1Var, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        g1Var.m(i, list, z);
    }

    public static int b(int i, List<?> list) {
        int K;
        int K2;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        int g0 = zzii.g0(i) * size;
        if (list instanceof gu5) {
            gu5 gu5Var = (gu5) list;
            while (i2 < size) {
                Object h = gu5Var.h(i2);
                if (h instanceof zzht) {
                    K2 = zzii.I((zzht) h);
                } else {
                    K2 = zzii.K((String) h);
                }
                g0 += K2;
                i2++;
            }
        } else {
            while (i2 < size) {
                Object obj = list.get(i2);
                if (obj instanceof zzht) {
                    K = zzii.I((zzht) obj);
                } else {
                    K = zzii.K((String) obj);
                }
                g0 += K;
                i2++;
            }
        }
        return g0;
    }

    public static void b0(int i, List<Integer> list, g1 g1Var, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        g1Var.p(i, list, z);
    }

    public static int c(int i, List<?> list, t0 t0Var) {
        int d2;
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int g0 = zzii.g0(i) * size;
        for (int i2 = 0; i2 < size; i2++) {
            Object obj = list.get(i2);
            if (obj instanceof au5) {
                d2 = zzii.e((au5) obj);
            } else {
                d2 = zzii.d((n0) obj, t0Var);
            }
            g0 += d2;
        }
        return g0;
    }

    public static void c0(int i, List<Integer> list, g1 g1Var, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        g1Var.e(i, list, z);
    }

    public static int d(int i, List<Long> list, boolean z) {
        if (list.size() == 0) {
            return 0;
        }
        return e(list) + (list.size() * zzii.g0(i));
    }

    public static void d0(int i, List<Boolean> list, g1 g1Var, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        g1Var.a(i, list, z);
    }

    public static int e(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof nu5) {
            nu5 nu5Var = (nu5) list;
            i = 0;
            while (i2 < size) {
                i += zzii.d0(nu5Var.k(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += zzii.d0(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    public static v0<?, ?> f() {
        return b;
    }

    public static v0<?, ?> g(boolean z) {
        try {
            Class<?> J = J();
            if (J == null) {
                return null;
            }
            return (v0) J.getConstructor(Boolean.TYPE).newInstance(Boolean.valueOf(z));
        } catch (Throwable unused) {
            return null;
        }
    }

    public static <UT, UB> UB h(int i, int i2, UB ub, v0<UT, UB> v0Var) {
        if (ub == null) {
            ub = v0Var.a();
        }
        v0Var.b(ub, i, i2);
        return ub;
    }

    public static <UT, UB> UB i(int i, List<Integer> list, ws5 ws5Var, UB ub, v0<UT, UB> v0Var) {
        if (ws5Var == null) {
            return ub;
        }
        if (list instanceof RandomAccess) {
            int size = list.size();
            int i2 = 0;
            for (int i3 = 0; i3 < size; i3++) {
                int intValue = list.get(i3).intValue();
                if (ws5Var.d(intValue)) {
                    if (i3 != i2) {
                        list.set(i2, Integer.valueOf(intValue));
                    }
                    i2++;
                } else {
                    ub = (UB) h(i, intValue, ub, v0Var);
                }
            }
            if (i2 != size) {
                list.subList(i2, size).clear();
            }
        } else {
            Iterator<Integer> it = list.iterator();
            while (it.hasNext()) {
                int intValue2 = it.next().intValue();
                if (!ws5Var.d(intValue2)) {
                    ub = (UB) h(i, intValue2, ub, v0Var);
                    it.remove();
                }
            }
        }
        return ub;
    }

    public static void j(int i, List<String> list, g1 g1Var) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        g1Var.D(i, list);
    }

    public static void k(int i, List<?> list, g1 g1Var, t0 t0Var) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        g1Var.N(i, list, t0Var);
    }

    public static void l(int i, List<Double> list, g1 g1Var, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        g1Var.y(i, list, z);
    }

    public static <T, FT extends pr5<FT>> void m(j0<FT> j0Var, T t, T t2) {
        k0<FT> b2 = j0Var.b(t2);
        if (b2.a.isEmpty()) {
            return;
        }
        j0Var.f(t).f(b2);
    }

    public static <T, UT, UB> void n(v0<UT, UB> v0Var, T t, T t2) {
        v0Var.e(t, v0Var.i(v0Var.f(t), v0Var.f(t2)));
    }

    public static void o(Class<?> cls) {
        Class<?> cls2;
        if (!l0.class.isAssignableFrom(cls) && (cls2 = a) != null && !cls2.isAssignableFrom(cls)) {
            throw new IllegalArgumentException("Message classes must extend GeneratedMessage or GeneratedMessageLite");
        }
    }

    public static <T> void p(qv5 qv5Var, T t, T t2, long j) {
        z0.j(t, j, qv5Var.g(z0.F(t, j), z0.F(t2, j)));
    }

    public static boolean q(Object obj, Object obj2) {
        if (obj != obj2) {
            return obj != null && obj.equals(obj2);
        }
        return true;
    }

    public static int r(int i, List<zzht> list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int g0 = size * zzii.g0(i);
        for (int i2 = 0; i2 < list.size(); i2++) {
            g0 += zzii.I(list.get(i2));
        }
        return g0;
    }

    public static int s(int i, List<n0> list, t0 t0Var) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < size; i3++) {
            i2 += zzii.U(i, list.get(i3), t0Var);
        }
        return i2;
    }

    public static int t(int i, List<Long> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return u(list) + (size * zzii.g0(i));
    }

    public static int u(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof nu5) {
            nu5 nu5Var = (nu5) list;
            i = 0;
            while (i2 < size) {
                i += zzii.i0(nu5Var.k(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += zzii.i0(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    public static v0<?, ?> v() {
        return c;
    }

    public static void w(int i, List<zzht> list, g1 g1Var) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        g1Var.z(i, list);
    }

    public static void x(int i, List<?> list, g1 g1Var, t0 t0Var) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        g1Var.H(i, list, t0Var);
    }

    public static void y(int i, List<Float> list, g1 g1Var, boolean z) throws IOException {
        if (list == null || list.isEmpty()) {
            return;
        }
        g1Var.B(i, list, z);
    }

    public static int z(int i, List<Long> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return A(list) + (size * zzii.g0(i));
    }
}
