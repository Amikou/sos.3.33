package com.google.android.gms.internal.vision;

import java.lang.reflect.Field;
import java.nio.Buffer;
import java.nio.ByteOrder;
import java.security.AccessController;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.web3j.abi.datatypes.Address;
import sun.misc.Unsafe;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public final class z0 {
    public static final Unsafe a;
    public static final Class<?> b;
    public static final boolean c;
    public static final boolean d;
    public static final d e;
    public static final boolean f;
    public static final boolean g;
    public static final long h;
    public static final boolean i;

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* loaded from: classes.dex */
    public static final class a extends d {
        public a(Unsafe unsafe) {
            super(unsafe);
        }

        @Override // com.google.android.gms.internal.vision.z0.d
        public final byte a(Object obj, long j) {
            return z0.i ? z0.L(obj, j) : z0.M(obj, j);
        }

        @Override // com.google.android.gms.internal.vision.z0.d
        public final void b(Object obj, long j, byte b) {
            if (z0.i) {
                z0.u(obj, j, b);
            } else {
                z0.y(obj, j, b);
            }
        }

        @Override // com.google.android.gms.internal.vision.z0.d
        public final void c(Object obj, long j, double d) {
            f(obj, j, Double.doubleToLongBits(d));
        }

        @Override // com.google.android.gms.internal.vision.z0.d
        public final void d(Object obj, long j, float f) {
            e(obj, j, Float.floatToIntBits(f));
        }

        @Override // com.google.android.gms.internal.vision.z0.d
        public final void g(Object obj, long j, boolean z) {
            if (z0.i) {
                z0.z(obj, j, z);
            } else {
                z0.D(obj, j, z);
            }
        }

        @Override // com.google.android.gms.internal.vision.z0.d
        public final boolean h(Object obj, long j) {
            return z0.i ? z0.N(obj, j) : z0.O(obj, j);
        }

        @Override // com.google.android.gms.internal.vision.z0.d
        public final float i(Object obj, long j) {
            return Float.intBitsToFloat(k(obj, j));
        }

        @Override // com.google.android.gms.internal.vision.z0.d
        public final double j(Object obj, long j) {
            return Double.longBitsToDouble(l(obj, j));
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* loaded from: classes.dex */
    public static final class b extends d {
        public b(Unsafe unsafe) {
            super(unsafe);
        }

        @Override // com.google.android.gms.internal.vision.z0.d
        public final byte a(Object obj, long j) {
            return this.a.getByte(obj, j);
        }

        @Override // com.google.android.gms.internal.vision.z0.d
        public final void b(Object obj, long j, byte b) {
            this.a.putByte(obj, j, b);
        }

        @Override // com.google.android.gms.internal.vision.z0.d
        public final void c(Object obj, long j, double d) {
            this.a.putDouble(obj, j, d);
        }

        @Override // com.google.android.gms.internal.vision.z0.d
        public final void d(Object obj, long j, float f) {
            this.a.putFloat(obj, j, f);
        }

        @Override // com.google.android.gms.internal.vision.z0.d
        public final void g(Object obj, long j, boolean z) {
            this.a.putBoolean(obj, j, z);
        }

        @Override // com.google.android.gms.internal.vision.z0.d
        public final boolean h(Object obj, long j) {
            return this.a.getBoolean(obj, j);
        }

        @Override // com.google.android.gms.internal.vision.z0.d
        public final float i(Object obj, long j) {
            return this.a.getFloat(obj, j);
        }

        @Override // com.google.android.gms.internal.vision.z0.d
        public final double j(Object obj, long j) {
            return this.a.getDouble(obj, j);
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* loaded from: classes.dex */
    public static final class c extends d {
        public c(Unsafe unsafe) {
            super(unsafe);
        }

        @Override // com.google.android.gms.internal.vision.z0.d
        public final byte a(Object obj, long j) {
            return z0.i ? z0.L(obj, j) : z0.M(obj, j);
        }

        @Override // com.google.android.gms.internal.vision.z0.d
        public final void b(Object obj, long j, byte b) {
            if (z0.i) {
                z0.u(obj, j, b);
            } else {
                z0.y(obj, j, b);
            }
        }

        @Override // com.google.android.gms.internal.vision.z0.d
        public final void c(Object obj, long j, double d) {
            f(obj, j, Double.doubleToLongBits(d));
        }

        @Override // com.google.android.gms.internal.vision.z0.d
        public final void d(Object obj, long j, float f) {
            e(obj, j, Float.floatToIntBits(f));
        }

        @Override // com.google.android.gms.internal.vision.z0.d
        public final void g(Object obj, long j, boolean z) {
            if (z0.i) {
                z0.z(obj, j, z);
            } else {
                z0.D(obj, j, z);
            }
        }

        @Override // com.google.android.gms.internal.vision.z0.d
        public final boolean h(Object obj, long j) {
            return z0.i ? z0.N(obj, j) : z0.O(obj, j);
        }

        @Override // com.google.android.gms.internal.vision.z0.d
        public final float i(Object obj, long j) {
            return Float.intBitsToFloat(k(obj, j));
        }

        @Override // com.google.android.gms.internal.vision.z0.d
        public final double j(Object obj, long j) {
            return Double.longBitsToDouble(l(obj, j));
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* loaded from: classes.dex */
    public static abstract class d {
        public Unsafe a;

        public d(Unsafe unsafe) {
            this.a = unsafe;
        }

        public abstract byte a(Object obj, long j);

        public abstract void b(Object obj, long j, byte b);

        public abstract void c(Object obj, long j, double d);

        public abstract void d(Object obj, long j, float f);

        public final void e(Object obj, long j, int i) {
            this.a.putInt(obj, j, i);
        }

        public final void f(Object obj, long j, long j2) {
            this.a.putLong(obj, j, j2);
        }

        public abstract void g(Object obj, long j, boolean z);

        public abstract boolean h(Object obj, long j);

        public abstract float i(Object obj, long j);

        public abstract double j(Object obj, long j);

        public final int k(Object obj, long j) {
            return this.a.getInt(obj, j);
        }

        public final long l(Object obj, long j) {
            return this.a.getLong(obj, j);
        }
    }

    static {
        Unsafe t = t();
        a = t;
        b = tn5.c();
        boolean B = B(Long.TYPE);
        c = B;
        boolean B2 = B(Integer.TYPE);
        d = B2;
        d dVar = null;
        if (t != null) {
            if (!tn5.b()) {
                dVar = new b(t);
            } else if (B) {
                dVar = new c(t);
            } else if (B2) {
                dVar = new a(t);
            }
        }
        e = dVar;
        f = E();
        g = A();
        h = n(byte[].class);
        n(boolean[].class);
        s(boolean[].class);
        n(int[].class);
        s(int[].class);
        n(long[].class);
        s(long[].class);
        n(float[].class);
        s(float[].class);
        n(double[].class);
        s(double[].class);
        n(Object[].class);
        s(Object[].class);
        Field G = G();
        if (G != null && dVar != null) {
            dVar.a.objectFieldOffset(G);
        }
        i = ByteOrder.nativeOrder() == ByteOrder.BIG_ENDIAN;
    }

    public static boolean A() {
        Unsafe unsafe = a;
        if (unsafe == null) {
            return false;
        }
        try {
            Class<?> cls = unsafe.getClass();
            cls.getMethod("objectFieldOffset", Field.class);
            cls.getMethod("arrayBaseOffset", Class.class);
            cls.getMethod("arrayIndexScale", Class.class);
            Class<?> cls2 = Long.TYPE;
            cls.getMethod("getInt", Object.class, cls2);
            cls.getMethod("putInt", Object.class, cls2, Integer.TYPE);
            cls.getMethod("getLong", Object.class, cls2);
            cls.getMethod("putLong", Object.class, cls2, cls2);
            cls.getMethod("getObject", Object.class, cls2);
            cls.getMethod("putObject", Object.class, cls2, Object.class);
            if (tn5.b()) {
                return true;
            }
            cls.getMethod("getByte", Object.class, cls2);
            cls.getMethod("putByte", Object.class, cls2, Byte.TYPE);
            cls.getMethod("getBoolean", Object.class, cls2);
            cls.getMethod("putBoolean", Object.class, cls2, Boolean.TYPE);
            cls.getMethod("getFloat", Object.class, cls2);
            cls.getMethod("putFloat", Object.class, cls2, Float.TYPE);
            cls.getMethod("getDouble", Object.class, cls2);
            cls.getMethod("putDouble", Object.class, cls2, Double.TYPE);
            return true;
        } catch (Throwable th) {
            Logger logger = Logger.getLogger(z0.class.getName());
            Level level = Level.WARNING;
            String valueOf = String.valueOf(th);
            StringBuilder sb = new StringBuilder(valueOf.length() + 71);
            sb.append("platform method missing - proto runtime falling back to safer methods: ");
            sb.append(valueOf);
            logger.logp(level, "com.google.protobuf.UnsafeUtil", "supportsUnsafeArrayOperations", sb.toString());
            return false;
        }
    }

    public static boolean B(Class<?> cls) {
        if (tn5.b()) {
            try {
                Class<?> cls2 = b;
                Class<?> cls3 = Boolean.TYPE;
                cls2.getMethod("peekLong", cls, cls3);
                cls2.getMethod("pokeLong", cls, Long.TYPE, cls3);
                Class<?> cls4 = Integer.TYPE;
                cls2.getMethod("pokeInt", cls, cls4, cls3);
                cls2.getMethod("peekInt", cls, cls3);
                cls2.getMethod("pokeByte", cls, Byte.TYPE);
                cls2.getMethod("peekByte", cls);
                cls2.getMethod("pokeByteArray", cls, byte[].class, cls4, cls4);
                cls2.getMethod("peekByteArray", cls, byte[].class, cls4, cls4);
                return true;
            } catch (Throwable unused) {
                return false;
            }
        }
        return false;
    }

    public static double C(Object obj, long j) {
        return e.j(obj, j);
    }

    public static void D(Object obj, long j, boolean z) {
        y(obj, j, z ? (byte) 1 : (byte) 0);
    }

    public static boolean E() {
        Unsafe unsafe = a;
        if (unsafe == null) {
            return false;
        }
        try {
            Class<?> cls = unsafe.getClass();
            cls.getMethod("objectFieldOffset", Field.class);
            Class<?> cls2 = Long.TYPE;
            cls.getMethod("getLong", Object.class, cls2);
            if (G() == null) {
                return false;
            }
            if (tn5.b()) {
                return true;
            }
            cls.getMethod("getByte", cls2);
            cls.getMethod("putByte", cls2, Byte.TYPE);
            cls.getMethod("getInt", cls2);
            cls.getMethod("putInt", cls2, Integer.TYPE);
            cls.getMethod("getLong", cls2);
            cls.getMethod("putLong", cls2, cls2);
            cls.getMethod("copyMemory", cls2, cls2, cls2);
            cls.getMethod("copyMemory", Object.class, cls2, Object.class, cls2, cls2);
            return true;
        } catch (Throwable th) {
            Logger logger = Logger.getLogger(z0.class.getName());
            Level level = Level.WARNING;
            String valueOf = String.valueOf(th);
            StringBuilder sb = new StringBuilder(valueOf.length() + 71);
            sb.append("platform method missing - proto runtime falling back to safer methods: ");
            sb.append(valueOf);
            logger.logp(level, "com.google.protobuf.UnsafeUtil", "supportsUnsafeByteBufferOperations", sb.toString());
            return false;
        }
    }

    public static Object F(Object obj, long j) {
        return e.a.getObject(obj, j);
    }

    public static Field G() {
        Field d2;
        if (!tn5.b() || (d2 = d(Buffer.class, "effectiveDirectAddress")) == null) {
            Field d3 = d(Buffer.class, Address.TYPE_NAME);
            if (d3 == null || d3.getType() != Long.TYPE) {
                return null;
            }
            return d3;
        }
        return d2;
    }

    public static byte L(Object obj, long j) {
        return (byte) (b(obj, (-4) & j) >>> ((int) (((~j) & 3) << 3)));
    }

    public static byte M(Object obj, long j) {
        return (byte) (b(obj, (-4) & j) >>> ((int) ((j & 3) << 3)));
    }

    public static boolean N(Object obj, long j) {
        return L(obj, j) != 0;
    }

    public static boolean O(Object obj, long j) {
        return M(obj, j) != 0;
    }

    public static byte a(byte[] bArr, long j) {
        return e.a(bArr, h + j);
    }

    public static int b(Object obj, long j) {
        return e.k(obj, j);
    }

    public static <T> T c(Class<T> cls) {
        try {
            return (T) a.allocateInstance(cls);
        } catch (InstantiationException e2) {
            throw new IllegalStateException(e2);
        }
    }

    public static Field d(Class<?> cls, String str) {
        try {
            return cls.getDeclaredField(str);
        } catch (Throwable unused) {
            return null;
        }
    }

    public static void f(Object obj, long j, double d2) {
        e.c(obj, j, d2);
    }

    public static void g(Object obj, long j, float f2) {
        e.d(obj, j, f2);
    }

    public static void h(Object obj, long j, int i2) {
        e.e(obj, j, i2);
    }

    public static void i(Object obj, long j, long j2) {
        e.f(obj, j, j2);
    }

    public static void j(Object obj, long j, Object obj2) {
        e.a.putObject(obj, j, obj2);
    }

    public static void k(Object obj, long j, boolean z) {
        e.g(obj, j, z);
    }

    public static void l(byte[] bArr, long j, byte b2) {
        e.b(bArr, h + j, b2);
    }

    public static boolean m() {
        return g;
    }

    public static int n(Class<?> cls) {
        if (g) {
            return e.a.arrayBaseOffset(cls);
        }
        return -1;
    }

    public static long o(Object obj, long j) {
        return e.l(obj, j);
    }

    public static boolean r() {
        return f;
    }

    public static int s(Class<?> cls) {
        if (g) {
            return e.a.arrayIndexScale(cls);
        }
        return -1;
    }

    public static Unsafe t() {
        try {
            return (Unsafe) AccessController.doPrivileged(new a1());
        } catch (Throwable unused) {
            return null;
        }
    }

    public static void u(Object obj, long j, byte b2) {
        long j2 = (-4) & j;
        int b3 = b(obj, j2);
        int i2 = ((~((int) j)) & 3) << 3;
        h(obj, j2, ((255 & b2) << i2) | (b3 & (~(255 << i2))));
    }

    public static boolean w(Object obj, long j) {
        return e.h(obj, j);
    }

    public static float x(Object obj, long j) {
        return e.i(obj, j);
    }

    public static void y(Object obj, long j, byte b2) {
        long j2 = (-4) & j;
        int i2 = (((int) j) & 3) << 3;
        h(obj, j2, ((255 & b2) << i2) | (b(obj, j2) & (~(255 << i2))));
    }

    public static void z(Object obj, long j, boolean z) {
        u(obj, j, z ? (byte) 1 : (byte) 0);
    }
}
