package com.google.android.gms.internal.vision;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public final class zzfz extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzfz> CREATOR = new kk5();
    public final int a;
    public final int f0;
    public final int g0;
    public final int h0;
    public final long i0;

    public zzfz(int i, int i2, int i3, int i4, long j) {
        this.a = i;
        this.f0 = i2;
        this.g0 = i3;
        this.h0 = i4;
        this.i0 = j;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int a = yb3.a(parcel);
        yb3.m(parcel, 1, this.a);
        yb3.m(parcel, 2, this.f0);
        yb3.m(parcel, 3, this.g0);
        yb3.m(parcel, 4, this.h0);
        yb3.o(parcel, 5, this.i0);
        yb3.b(parcel, a);
    }
}
