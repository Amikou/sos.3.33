package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.vision.l0;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public final class g extends l0<g, a> implements ew5 {
    private static final g zzg;
    private static volatile xw5<g> zzh;
    private int zzc;
    private zzfi$zzj zzd;
    private i zze;
    private gt5<zzfi$zzf> zzf = l0.w();

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* loaded from: classes.dex */
    public static final class a extends l0.b<g, a> implements ew5 {
        public a() {
            super(g.zzg);
        }

        public final a u(zzfi$zzj zzfi_zzj) {
            if (this.g0) {
                r();
                this.g0 = false;
            }
            ((g) this.f0).A(zzfi_zzj);
            return this;
        }

        public final a v(Iterable<? extends zzfi$zzf> iterable) {
            if (this.g0) {
                r();
                this.g0 = false;
            }
            ((g) this.f0).B(iterable);
            return this;
        }

        public /* synthetic */ a(m mVar) {
            this();
        }
    }

    static {
        g gVar = new g();
        zzg = gVar;
        l0.s(g.class, gVar);
    }

    public static a x() {
        return zzg.u();
    }

    public final void A(zzfi$zzj zzfi_zzj) {
        zzfi_zzj.getClass();
        this.zzd = zzfi_zzj;
        this.zzc |= 1;
    }

    public final void B(Iterable<? extends zzfi$zzf> iterable) {
        D();
        a0.b(iterable, this.zzf);
    }

    public final void D() {
        gt5<zzfi$zzf> gt5Var = this.zzf;
        if (gt5Var.zza()) {
            return;
        }
        this.zzf = l0.r(gt5Var);
    }

    /* JADX WARN: Type inference failed for: r2v14, types: [xw5<com.google.android.gms.internal.vision.g>, com.google.android.gms.internal.vision.l0$a] */
    @Override // com.google.android.gms.internal.vision.l0
    public final Object o(int i, Object obj, Object obj2) {
        xw5<g> xw5Var;
        switch (m.a[i - 1]) {
            case 1:
                return new g();
            case 2:
                return new a(null);
            case 3:
                return l0.p(zzg, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0001\u0000\u0001ဉ\u0000\u0002ဉ\u0001\u0003\u001b", new Object[]{"zzc", "zzd", "zze", "zzf", zzfi$zzf.class});
            case 4:
                return zzg;
            case 5:
                xw5<g> xw5Var2 = zzh;
                xw5<g> xw5Var3 = xw5Var2;
                if (xw5Var2 == null) {
                    synchronized (g.class) {
                        xw5<g> xw5Var4 = zzh;
                        xw5Var = xw5Var4;
                        if (xw5Var4 == null) {
                            ?? aVar = new l0.a(zzg);
                            zzh = aVar;
                            xw5Var = aVar;
                        }
                    }
                    xw5Var3 = xw5Var;
                }
                return xw5Var3;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
