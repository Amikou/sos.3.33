package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.vision.zzfi$zzj;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public final class x implements ws5 {
    public static final ws5 a = new x();

    @Override // defpackage.ws5
    public final boolean d(int i) {
        return zzfi$zzj.zza.zza(i) != null;
    }
}
