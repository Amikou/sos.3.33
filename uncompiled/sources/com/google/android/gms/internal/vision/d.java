package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.vision.l0;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public final class d extends l0<d, a> implements ew5 {
    private static final d zzg;
    private static volatile xw5<d> zzh;
    private int zzc;
    private int zzd;
    private int zze;
    private String zzf = "";

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* loaded from: classes.dex */
    public static final class a extends l0.b<d, a> implements ew5 {
        public a() {
            super(d.zzg);
        }

        public /* synthetic */ a(m mVar) {
            this();
        }
    }

    static {
        d dVar = new d();
        zzg = dVar;
        l0.s(d.class, dVar);
    }

    /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.vision.l0$a, xw5<com.google.android.gms.internal.vision.d>] */
    @Override // com.google.android.gms.internal.vision.l0
    public final Object o(int i, Object obj, Object obj2) {
        xw5<d> xw5Var;
        switch (m.a[i - 1]) {
            case 1:
                return new d();
            case 2:
                return new a(null);
            case 3:
                return l0.p(zzg, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001ဌ\u0000\u0002ဌ\u0001\u0003ဈ\u0002", new Object[]{"zzc", "zzd", zzgz.zzb(), "zze", zzha.zzb(), "zzf"});
            case 4:
                return zzg;
            case 5:
                xw5<d> xw5Var2 = zzh;
                xw5<d> xw5Var3 = xw5Var2;
                if (xw5Var2 == null) {
                    synchronized (d.class) {
                        xw5<d> xw5Var4 = zzh;
                        xw5Var = xw5Var4;
                        if (xw5Var4 == null) {
                            ?? aVar = new l0.a(zzg);
                            zzh = aVar;
                            xw5Var = aVar;
                        }
                    }
                    xw5Var3 = xw5Var;
                }
                return xw5Var3;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
