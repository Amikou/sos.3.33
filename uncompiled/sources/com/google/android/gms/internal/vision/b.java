package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.vision.l0;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public final class b extends l0<b, a> implements ew5 {
    private static final b zzf;
    private static volatile xw5<b> zzg;
    private int zzc;
    private String zzd = "";
    private String zze = "";

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* loaded from: classes.dex */
    public static final class a extends l0.b<b, a> implements ew5 {
        public a() {
            super(b.zzf);
        }

        public final a u(String str) {
            if (this.g0) {
                r();
                this.g0 = false;
            }
            ((b) this.f0).z(str);
            return this;
        }

        public final a v(String str) {
            if (this.g0) {
                r();
                this.g0 = false;
            }
            ((b) this.f0).C(str);
            return this;
        }

        public /* synthetic */ a(m mVar) {
            this();
        }
    }

    static {
        b bVar = new b();
        zzf = bVar;
        l0.s(b.class, bVar);
    }

    public static a x() {
        return zzf.u();
    }

    public final void C(String str) {
        str.getClass();
        this.zzc |= 2;
        this.zze = str;
    }

    /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.vision.l0$a, xw5<com.google.android.gms.internal.vision.b>] */
    @Override // com.google.android.gms.internal.vision.l0
    public final Object o(int i, Object obj, Object obj2) {
        xw5<b> xw5Var;
        switch (m.a[i - 1]) {
            case 1:
                return new b();
            case 2:
                return new a(null);
            case 3:
                return l0.p(zzf, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001ဈ\u0000\u0002ဈ\u0001", new Object[]{"zzc", "zzd", "zze"});
            case 4:
                return zzf;
            case 5:
                xw5<b> xw5Var2 = zzg;
                xw5<b> xw5Var3 = xw5Var2;
                if (xw5Var2 == null) {
                    synchronized (b.class) {
                        xw5<b> xw5Var4 = zzg;
                        xw5Var = xw5Var4;
                        if (xw5Var4 == null) {
                            ?? aVar = new l0.a(zzf);
                            zzg = aVar;
                            xw5Var = aVar;
                        }
                    }
                    xw5Var3 = xw5Var;
                }
                return xw5Var3;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public final void z(String str) {
        str.getClass();
        this.zzc |= 1;
        this.zzd = str;
    }
}
