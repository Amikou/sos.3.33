package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.vision.l0;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public final class zzfi$zzj extends l0<zzfi$zzj, a> implements ew5 {
    private static final zzfi$zzj zzi;
    private static volatile xw5<zzfi$zzj> zzj;
    private int zzc;
    private int zzd;
    private long zze;
    private long zzf;
    private long zzg;
    private long zzh;

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* loaded from: classes.dex */
    public static final class a extends l0.b<zzfi$zzj, a> implements ew5 {
        public a() {
            super(zzfi$zzj.zzi);
        }

        public final a u(long j) {
            if (this.g0) {
                r();
                this.g0 = false;
            }
            ((zzfi$zzj) this.f0).y(j);
            return this;
        }

        public final a v(long j) {
            if (this.g0) {
                r();
                this.g0 = false;
            }
            ((zzfi$zzj) this.f0).B(j);
            return this;
        }

        public final a x(long j) {
            if (this.g0) {
                r();
                this.g0 = false;
            }
            ((zzfi$zzj) this.f0).D(j);
            return this;
        }

        public final a y(long j) {
            if (this.g0) {
                r();
                this.g0 = false;
            }
            ((zzfi$zzj) this.f0).F(j);
            return this;
        }

        public /* synthetic */ a(m mVar) {
            this();
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* loaded from: classes.dex */
    public enum zza implements rs5 {
        FORMAT_UNKNOWN(0),
        FORMAT_LUMINANCE(1),
        FORMAT_RGB8(2),
        FORMAT_MONOCHROME(3);
        
        private final int zzf;

        static {
            new y();
        }

        zza(int i) {
            this.zzf = i;
        }

        public static ws5 zzb() {
            return x.a;
        }

        @Override // java.lang.Enum
        public final String toString() {
            return "<" + zza.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.zzf + " name=" + name() + '>';
        }

        @Override // defpackage.rs5
        public final int zza() {
            return this.zzf;
        }

        public static zza zza(int i) {
            if (i != 0) {
                if (i != 1) {
                    if (i != 2) {
                        if (i != 3) {
                            return null;
                        }
                        return FORMAT_MONOCHROME;
                    }
                    return FORMAT_RGB8;
                }
                return FORMAT_LUMINANCE;
            }
            return FORMAT_UNKNOWN;
        }
    }

    static {
        zzfi$zzj zzfi_zzj = new zzfi$zzj();
        zzi = zzfi_zzj;
        l0.s(zzfi$zzj.class, zzfi_zzj);
    }

    public static a x() {
        return zzi.u();
    }

    public final void B(long j) {
        this.zzc |= 4;
        this.zzf = j;
    }

    public final void D(long j) {
        this.zzc |= 8;
        this.zzg = j;
    }

    public final void F(long j) {
        this.zzc |= 16;
        this.zzh = j;
    }

    /* JADX WARN: Type inference failed for: r2v14, types: [xw5<com.google.android.gms.internal.vision.zzfi$zzj>, com.google.android.gms.internal.vision.l0$a] */
    @Override // com.google.android.gms.internal.vision.l0
    public final Object o(int i, Object obj, Object obj2) {
        xw5<zzfi$zzj> xw5Var;
        switch (m.a[i - 1]) {
            case 1:
                return new zzfi$zzj();
            case 2:
                return new a(null);
            case 3:
                return l0.p(zzi, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0000\u0000\u0001ဌ\u0000\u0002ဂ\u0001\u0003ဂ\u0002\u0004ဂ\u0004\u0005ဂ\u0003", new Object[]{"zzc", "zzd", zza.zzb(), "zze", "zzf", "zzh", "zzg"});
            case 4:
                return zzi;
            case 5:
                xw5<zzfi$zzj> xw5Var2 = zzj;
                xw5<zzfi$zzj> xw5Var3 = xw5Var2;
                if (xw5Var2 == null) {
                    synchronized (zzfi$zzj.class) {
                        xw5<zzfi$zzj> xw5Var4 = zzj;
                        xw5Var = xw5Var4;
                        if (xw5Var4 == null) {
                            ?? aVar = new l0.a(zzi);
                            zzj = aVar;
                            xw5Var = aVar;
                        }
                    }
                    xw5Var3 = xw5Var;
                }
                return xw5Var3;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public final void y(long j) {
        this.zzc |= 2;
        this.zze = j;
    }
}
