package com.google.android.gms.internal.vision;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public final class f0 extends e0 {
    public int a;
    public int b;
    public int c;
    public int d;
    public int e;

    public f0(byte[] bArr, int i, int i2, boolean z) {
        super();
        this.e = Integer.MAX_VALUE;
        this.a = i2 + i;
        this.c = i;
        this.d = i;
    }

    @Override // com.google.android.gms.internal.vision.e0
    public final int c(int i) throws zzjk {
        if (i >= 0) {
            int e = i + e();
            int i2 = this.e;
            if (e <= i2) {
                this.e = e;
                f();
                return i2;
            }
            throw zzjk.zza();
        }
        throw zzjk.zzb();
    }

    @Override // com.google.android.gms.internal.vision.e0
    public final int e() {
        return this.c - this.d;
    }

    public final void f() {
        int i = this.a + this.b;
        this.a = i;
        int i2 = i - this.d;
        int i3 = this.e;
        if (i2 > i3) {
            int i4 = i2 - i3;
            this.b = i4;
            this.a = i - i4;
            return;
        }
        this.b = 0;
    }
}
