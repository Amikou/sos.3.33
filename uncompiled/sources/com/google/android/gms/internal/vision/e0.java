package com.google.android.gms.internal.vision;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public abstract class e0 {
    public e0() {
    }

    public static long a(long j) {
        return (-(j & 1)) ^ (j >>> 1);
    }

    public static e0 b(byte[] bArr, int i, int i2, boolean z) {
        f0 f0Var = new f0(bArr, i2);
        try {
            f0Var.c(i2);
            return f0Var;
        } catch (zzjk e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static int d(int i) {
        return (-(i & 1)) ^ (i >>> 1);
    }

    public abstract int c(int i) throws zzjk;

    public abstract int e();
}
