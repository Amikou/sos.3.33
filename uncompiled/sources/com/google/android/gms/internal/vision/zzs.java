package com.google.android.gms.internal.vision;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public final class zzs extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzs> CREATOR = new g56();
    public int a;
    public int f0;
    public int g0;
    public long h0;
    public int i0;

    public zzs() {
    }

    public static zzs I1(yb1 yb1Var) {
        zzs zzsVar = new zzs();
        zzsVar.a = yb1Var.c().f();
        zzsVar.f0 = yb1Var.c().b();
        zzsVar.i0 = yb1Var.c().d();
        zzsVar.g0 = yb1Var.c().c();
        zzsVar.h0 = yb1Var.c().e();
        return zzsVar;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int a = yb3.a(parcel);
        yb3.m(parcel, 2, this.a);
        yb3.m(parcel, 3, this.f0);
        yb3.m(parcel, 4, this.g0);
        yb3.o(parcel, 5, this.h0);
        yb3.m(parcel, 6, this.i0);
        yb3.b(parcel, a);
    }

    public zzs(int i, int i2, int i3, long j, int i4) {
        this.a = i;
        this.f0 = i2;
        this.g0 = i3;
        this.h0 = j;
        this.i0 = i4;
    }
}
