package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.vision.l0;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public final class i0 extends j0<l0.e> {
    @Override // com.google.android.gms.internal.vision.j0
    public final int a(Map.Entry<?, ?> entry) {
        return ((l0.e) entry.getKey()).a;
    }

    @Override // com.google.android.gms.internal.vision.j0
    public final k0<l0.e> b(Object obj) {
        return ((l0.c) obj).zzc;
    }

    @Override // com.google.android.gms.internal.vision.j0
    public final Object c(h0 h0Var, n0 n0Var, int i) {
        return h0Var.a(n0Var, i);
    }

    @Override // com.google.android.gms.internal.vision.j0
    public final void d(g1 g1Var, Map.Entry<?, ?> entry) throws IOException {
        l0.e eVar = (l0.e) entry.getKey();
        if (eVar.g0) {
            switch (hr5.a[eVar.f0.ordinal()]) {
                case 1:
                    u0.l(eVar.a, (List) entry.getValue(), g1Var, false);
                    return;
                case 2:
                    u0.y(eVar.a, (List) entry.getValue(), g1Var, false);
                    return;
                case 3:
                    u0.C(eVar.a, (List) entry.getValue(), g1Var, false);
                    return;
                case 4:
                    u0.G(eVar.a, (List) entry.getValue(), g1Var, false);
                    return;
                case 5:
                    u0.T(eVar.a, (List) entry.getValue(), g1Var, false);
                    return;
                case 6:
                    u0.N(eVar.a, (List) entry.getValue(), g1Var, false);
                    return;
                case 7:
                    u0.a0(eVar.a, (List) entry.getValue(), g1Var, false);
                    return;
                case 8:
                    u0.d0(eVar.a, (List) entry.getValue(), g1Var, false);
                    return;
                case 9:
                    u0.W(eVar.a, (List) entry.getValue(), g1Var, false);
                    return;
                case 10:
                    u0.b0(eVar.a, (List) entry.getValue(), g1Var, false);
                    return;
                case 11:
                    u0.Q(eVar.a, (List) entry.getValue(), g1Var, false);
                    return;
                case 12:
                    u0.Z(eVar.a, (List) entry.getValue(), g1Var, false);
                    return;
                case 13:
                    u0.K(eVar.a, (List) entry.getValue(), g1Var, false);
                    return;
                case 14:
                    u0.T(eVar.a, (List) entry.getValue(), g1Var, false);
                    return;
                case 15:
                    u0.w(eVar.a, (List) entry.getValue(), g1Var);
                    return;
                case 16:
                    u0.j(eVar.a, (List) entry.getValue(), g1Var);
                    return;
                case 17:
                    List list = (List) entry.getValue();
                    if (list == null || list.isEmpty()) {
                        return;
                    }
                    u0.x(eVar.a, (List) entry.getValue(), g1Var, zw5.c().a(list.get(0).getClass()));
                    return;
                case 18:
                    List list2 = (List) entry.getValue();
                    if (list2 == null || list2.isEmpty()) {
                        return;
                    }
                    u0.k(eVar.a, (List) entry.getValue(), g1Var, zw5.c().a(list2.get(0).getClass()));
                    return;
                default:
                    return;
            }
        }
        switch (hr5.a[eVar.f0.ordinal()]) {
            case 1:
                g1Var.r(eVar.a, ((Double) entry.getValue()).doubleValue());
                return;
            case 2:
                g1Var.s(eVar.a, ((Float) entry.getValue()).floatValue());
                return;
            case 3:
                g1Var.q(eVar.a, ((Long) entry.getValue()).longValue());
                return;
            case 4:
                g1Var.g(eVar.a, ((Long) entry.getValue()).longValue());
                return;
            case 5:
                g1Var.f(eVar.a, ((Integer) entry.getValue()).intValue());
                return;
            case 6:
                g1Var.J(eVar.a, ((Long) entry.getValue()).longValue());
                return;
            case 7:
                g1Var.A(eVar.a, ((Integer) entry.getValue()).intValue());
                return;
            case 8:
                g1Var.E(eVar.a, ((Boolean) entry.getValue()).booleanValue());
                return;
            case 9:
                g1Var.C(eVar.a, ((Integer) entry.getValue()).intValue());
                return;
            case 10:
                g1Var.i(eVar.a, ((Integer) entry.getValue()).intValue());
                return;
            case 11:
                g1Var.b(eVar.a, ((Long) entry.getValue()).longValue());
                return;
            case 12:
                g1Var.u(eVar.a, ((Integer) entry.getValue()).intValue());
                return;
            case 13:
                g1Var.L(eVar.a, ((Long) entry.getValue()).longValue());
                return;
            case 14:
                g1Var.f(eVar.a, ((Integer) entry.getValue()).intValue());
                return;
            case 15:
                g1Var.F(eVar.a, (zzht) entry.getValue());
                return;
            case 16:
                g1Var.n(eVar.a, (String) entry.getValue());
                return;
            case 17:
                g1Var.M(eVar.a, entry.getValue(), zw5.c().a(entry.getValue().getClass()));
                return;
            case 18:
                g1Var.I(eVar.a, entry.getValue(), zw5.c().a(entry.getValue().getClass()));
                return;
            default:
                return;
        }
    }

    @Override // com.google.android.gms.internal.vision.j0
    public final boolean e(n0 n0Var) {
        return n0Var instanceof l0.c;
    }

    @Override // com.google.android.gms.internal.vision.j0
    public final k0<l0.e> f(Object obj) {
        return ((l0.c) obj).x();
    }

    @Override // com.google.android.gms.internal.vision.j0
    public final void g(Object obj) {
        b(obj).i();
    }
}
