package com.google.android.gms.internal.vision;

import defpackage.pr5;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public final class k0<T extends pr5<T>> {
    public static final k0 d = new k0(true);
    public final ux5<T, Object> a;
    public boolean b;
    public boolean c;

    public k0() {
        this.a = ux5.d(16);
    }

    public static int a(zzml zzmlVar, int i, Object obj) {
        int g0 = zzii.g0(i);
        if (zzmlVar == zzml.zzj) {
            vs5.g((n0) obj);
            g0 <<= 1;
        }
        return g0 + b(zzmlVar, obj);
    }

    public static int b(zzml zzmlVar, Object obj) {
        switch (ir5.b[zzmlVar.ordinal()]) {
            case 1:
                return zzii.z(((Double) obj).doubleValue());
            case 2:
                return zzii.A(((Float) obj).floatValue());
            case 3:
                return zzii.d0(((Long) obj).longValue());
            case 4:
                return zzii.i0(((Long) obj).longValue());
            case 5:
                return zzii.k0(((Integer) obj).intValue());
            case 6:
                return zzii.r0(((Long) obj).longValue());
            case 7:
                return zzii.w0(((Integer) obj).intValue());
            case 8:
                return zzii.L(((Boolean) obj).booleanValue());
            case 9:
                return zzii.V((n0) obj);
            case 10:
                if (obj instanceof ot5) {
                    return zzii.e((ot5) obj);
                }
                return zzii.J((n0) obj);
            case 11:
                if (obj instanceof zzht) {
                    return zzii.I((zzht) obj);
                }
                return zzii.K((String) obj);
            case 12:
                if (obj instanceof zzht) {
                    return zzii.I((zzht) obj);
                }
                return zzii.M((byte[]) obj);
            case 13:
                return zzii.o0(((Integer) obj).intValue());
            case 14:
                return zzii.z0(((Integer) obj).intValue());
            case 15:
                return zzii.v0(((Long) obj).longValue());
            case 16:
                return zzii.s0(((Integer) obj).intValue());
            case 17:
                return zzii.n0(((Long) obj).longValue());
            case 18:
                if (obj instanceof rs5) {
                    return zzii.B0(((rs5) obj).zza());
                }
                return zzii.B0(((Integer) obj).intValue());
            default:
                throw new RuntimeException("There is no way to get here, but the compiler thinks otherwise.");
        }
    }

    public static <T extends pr5<T>> k0<T> c() {
        return d;
    }

    public static Object d(Object obj) {
        if (obj instanceof ow5) {
            return ((ow5) obj).zza();
        }
        if (obj instanceof byte[]) {
            byte[] bArr = (byte[]) obj;
            byte[] bArr2 = new byte[bArr.length];
            System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
            return bArr2;
        }
        return obj;
    }

    public static <T extends pr5<T>> boolean h(Map.Entry<T, Object> entry) {
        T key = entry.getKey();
        if (key.zzc() == zzmo.MESSAGE) {
            if (key.b()) {
                for (n0 n0Var : (List) entry.getValue()) {
                    if (!n0Var.a()) {
                        return false;
                    }
                }
            } else {
                Object value = entry.getValue();
                if (value instanceof n0) {
                    if (!((n0) value).a()) {
                        return false;
                    }
                } else if (value instanceof ot5) {
                    return true;
                } else {
                    throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
                }
            }
        }
        return true;
    }

    public static int l(Map.Entry<T, Object> entry) {
        T key = entry.getKey();
        Object value = entry.getValue();
        if (key.zzc() == zzmo.MESSAGE && !key.b() && !key.c()) {
            if (value instanceof ot5) {
                return zzii.G(entry.getKey().zza(), (ot5) value);
            }
            return zzii.D(entry.getKey().zza(), (n0) value);
        }
        return m(key, value);
    }

    public static int m(pr5<?> pr5Var, Object obj) {
        zzml zzb = pr5Var.zzb();
        int zza = pr5Var.zza();
        if (pr5Var.b()) {
            int i = 0;
            if (pr5Var.c()) {
                for (Object obj2 : (List) obj) {
                    i += b(zzb, obj2);
                }
                return zzii.g0(zza) + i + zzii.D0(i);
            }
            for (Object obj3 : (List) obj) {
                i += a(zzb, zza, obj3);
            }
            return i;
        }
        return a(zzb, zza, obj);
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARN: Code restructure failed: missing block: B:13:0x0029, code lost:
        if ((r6 instanceof defpackage.rs5) == false) goto L3;
     */
    /* JADX WARN: Code restructure failed: missing block: B:18:0x0032, code lost:
        if ((r6 instanceof byte[]) == false) goto L3;
     */
    /* JADX WARN: Code restructure failed: missing block: B:8:0x0020, code lost:
        if ((r6 instanceof defpackage.ot5) == false) goto L3;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static void p(T r5, java.lang.Object r6) {
        /*
            com.google.android.gms.internal.vision.zzml r0 = r5.zzb()
            defpackage.vs5.d(r6)
            int[] r1 = defpackage.ir5.a
            com.google.android.gms.internal.vision.zzmo r0 = r0.zza()
            int r0 = r0.ordinal()
            r0 = r1[r0]
            r1 = 1
            r2 = 0
            switch(r0) {
                case 1: goto L45;
                case 2: goto L42;
                case 3: goto L3f;
                case 4: goto L3c;
                case 5: goto L39;
                case 6: goto L36;
                case 7: goto L2c;
                case 8: goto L23;
                case 9: goto L1a;
                default: goto L18;
            }
        L18:
            r0 = r2
            goto L47
        L1a:
            boolean r0 = r6 instanceof com.google.android.gms.internal.vision.n0
            if (r0 != 0) goto L34
            boolean r0 = r6 instanceof defpackage.ot5
            if (r0 == 0) goto L18
            goto L34
        L23:
            boolean r0 = r6 instanceof java.lang.Integer
            if (r0 != 0) goto L34
            boolean r0 = r6 instanceof defpackage.rs5
            if (r0 == 0) goto L18
            goto L34
        L2c:
            boolean r0 = r6 instanceof com.google.android.gms.internal.vision.zzht
            if (r0 != 0) goto L34
            boolean r0 = r6 instanceof byte[]
            if (r0 == 0) goto L18
        L34:
            r0 = r1
            goto L47
        L36:
            boolean r0 = r6 instanceof java.lang.String
            goto L47
        L39:
            boolean r0 = r6 instanceof java.lang.Boolean
            goto L47
        L3c:
            boolean r0 = r6 instanceof java.lang.Double
            goto L47
        L3f:
            boolean r0 = r6 instanceof java.lang.Float
            goto L47
        L42:
            boolean r0 = r6 instanceof java.lang.Long
            goto L47
        L45:
            boolean r0 = r6 instanceof java.lang.Integer
        L47:
            if (r0 == 0) goto L4a
            return
        L4a:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r3 = 3
            java.lang.Object[] r3 = new java.lang.Object[r3]
            int r4 = r5.zza()
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
            r3[r2] = r4
            com.google.android.gms.internal.vision.zzml r5 = r5.zzb()
            com.google.android.gms.internal.vision.zzmo r5 = r5.zza()
            r3[r1] = r5
            r5 = 2
            java.lang.Class r6 = r6.getClass()
            java.lang.String r6 = r6.getName()
            r3[r5] = r6
            java.lang.String r5 = "Wrong object type used with protocol message reflection.\nField number: %d, field java type: %s, value type: %s\n"
            java.lang.String r5 = java.lang.String.format(r5, r3)
            r0.<init>(r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.vision.k0.p(pr5, java.lang.Object):void");
    }

    public final /* synthetic */ Object clone() throws CloneNotSupportedException {
        k0 k0Var = new k0();
        for (int i = 0; i < this.a.j(); i++) {
            Map.Entry<T, Object> h = this.a.h(i);
            k0Var.g(h.getKey(), h.getValue());
        }
        for (Map.Entry<T, Object> entry : this.a.m()) {
            k0Var.g(entry.getKey(), entry.getValue());
        }
        k0Var.c = this.c;
        return k0Var;
    }

    public final Object e(T t) {
        Object obj = this.a.get(t);
        if (obj instanceof ot5) {
            ot5 ot5Var = (ot5) obj;
            return ot5.e();
        }
        return obj;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof k0) {
            return this.a.equals(((k0) obj).a);
        }
        return false;
    }

    public final void f(k0<T> k0Var) {
        for (int i = 0; i < k0Var.a.j(); i++) {
            j(k0Var.a.h(i));
        }
        for (Map.Entry<T, Object> entry : k0Var.a.m()) {
            j(entry);
        }
    }

    public final void g(T t, Object obj) {
        if (t.b()) {
            if (obj instanceof List) {
                ArrayList arrayList = new ArrayList();
                arrayList.addAll((List) obj);
                int size = arrayList.size();
                int i = 0;
                while (i < size) {
                    Object obj2 = arrayList.get(i);
                    i++;
                    p(t, obj2);
                }
                obj = arrayList;
            } else {
                throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
            }
        } else {
            p(t, obj);
        }
        if (obj instanceof ot5) {
            this.c = true;
        }
        this.a.put(t, obj);
    }

    public final int hashCode() {
        return this.a.hashCode();
    }

    public final void i() {
        if (this.b) {
            return;
        }
        this.a.e();
        this.b = true;
    }

    public final void j(Map.Entry<T, Object> entry) {
        n0 i;
        T key = entry.getKey();
        Object value = entry.getValue();
        if (value instanceof ot5) {
            ot5 ot5Var = (ot5) value;
            value = ot5.e();
        }
        if (key.b()) {
            Object e = e(key);
            if (e == null) {
                e = new ArrayList();
            }
            for (Object obj : (List) value) {
                ((List) e).add(d(obj));
            }
            this.a.put(key, e);
        } else if (key.zzc() == zzmo.MESSAGE) {
            Object e2 = e(key);
            if (e2 == null) {
                this.a.put(key, d(value));
                return;
            }
            if (e2 instanceof ow5) {
                i = key.c1((ow5) e2, (ow5) value);
            } else {
                i = key.F0(((n0) e2).d(), (n0) value).i();
            }
            this.a.put(key, i);
        } else {
            this.a.put(key, d(value));
        }
    }

    public final void k(T t, Object obj) {
        List list;
        if (t.b()) {
            p(t, obj);
            Object e = e(t);
            if (e == null) {
                list = new ArrayList();
                this.a.put(t, list);
            } else {
                list = (List) e;
            }
            list.add(obj);
            return;
        }
        throw new IllegalArgumentException("addRepeatedField() can only be called on repeated fields.");
    }

    public final boolean n() {
        return this.b;
    }

    public final Iterator<Map.Entry<T, Object>> o() {
        if (this.c) {
            return new rt5(this.a.entrySet().iterator());
        }
        return this.a.entrySet().iterator();
    }

    public final Iterator<Map.Entry<T, Object>> q() {
        if (this.c) {
            return new rt5(this.a.o().iterator());
        }
        return this.a.o().iterator();
    }

    public final boolean r() {
        for (int i = 0; i < this.a.j(); i++) {
            if (!h(this.a.h(i))) {
                return false;
            }
        }
        for (Map.Entry<T, Object> entry : this.a.m()) {
            if (!h(entry)) {
                return false;
            }
        }
        return true;
    }

    public final int s() {
        int i = 0;
        for (int i2 = 0; i2 < this.a.j(); i2++) {
            i += l(this.a.h(i2));
        }
        for (Map.Entry<T, Object> entry : this.a.m()) {
            i += l(entry);
        }
        return i;
    }

    public k0(boolean z) {
        this(ux5.d(0));
        i();
    }

    public k0(ux5<T, Object> ux5Var) {
        this.a = ux5Var;
        i();
    }
}
