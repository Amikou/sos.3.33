package com.google.android.gms.internal.vision;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamite.DynamiteModule;
import com.google.android.gms.vision.barcode.Barcode;
import java.nio.ByteBuffer;

/* compiled from: com.google.android.gms:play-services-vision@@20.1.3 */
/* loaded from: classes.dex */
public final class y0 extends k1<s0> {
    public final zzk i;

    public y0(Context context, zzk zzkVar) {
        super(context, "BarcodeNativeHandle", "barcode");
        this.i = zzkVar;
        e();
    }

    @Override // com.google.android.gms.internal.vision.k1
    public final /* synthetic */ s0 a(DynamiteModule dynamiteModule, Context context) throws RemoteException, DynamiteModule.LoadingException {
        h1 j1Var;
        IBinder c = dynamiteModule.c("com.google.android.gms.vision.barcode.ChimeraNativeBarcodeDetectorCreator");
        if (c == null) {
            j1Var = null;
        } else {
            IInterface queryLocalInterface = c.queryLocalInterface("com.google.android.gms.vision.barcode.internal.client.INativeBarcodeDetectorCreator");
            if (queryLocalInterface instanceof h1) {
                j1Var = (h1) queryLocalInterface;
            } else {
                j1Var = new j1(c);
            }
        }
        if (j1Var == null) {
            return null;
        }
        return j1Var.g0(nl2.H1(context), (zzk) zt2.j(this.i));
    }

    @Override // com.google.android.gms.internal.vision.k1
    public final void b() throws RemoteException {
        if (c()) {
            ((s0) zt2.j(e())).zza();
        }
    }

    public final Barcode[] f(Bitmap bitmap, zzs zzsVar) {
        if (c()) {
            try {
                return ((s0) zt2.j(e())).Z(nl2.H1(bitmap), zzsVar);
            } catch (RemoteException unused) {
                return new Barcode[0];
            }
        }
        return new Barcode[0];
    }

    public final Barcode[] g(ByteBuffer byteBuffer, zzs zzsVar) {
        if (c()) {
            try {
                return ((s0) zt2.j(e())).h(nl2.H1(byteBuffer), zzsVar);
            } catch (RemoteException unused) {
                return new Barcode[0];
            }
        }
        return new Barcode[0];
    }
}
