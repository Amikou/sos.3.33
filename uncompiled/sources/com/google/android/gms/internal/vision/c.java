package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.vision.l0;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public final class c extends l0<c, a> implements ew5 {
    private static final ct5<Integer, zzgz> zzd = new pj5();
    private static final c zze;
    private static volatile xw5<c> zzf;
    private et5 zzc = l0.v();

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* loaded from: classes.dex */
    public static final class a extends l0.b<c, a> implements ew5 {
        public a() {
            super(c.zze);
        }

        public /* synthetic */ a(m mVar) {
            this();
        }
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [ct5<java.lang.Integer, com.google.android.gms.internal.vision.zzgz>, pj5] */
    static {
        c cVar = new c();
        zze = cVar;
        l0.s(c.class, cVar);
    }

    /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.vision.l0$a, xw5<com.google.android.gms.internal.vision.c>] */
    @Override // com.google.android.gms.internal.vision.l0
    public final Object o(int i, Object obj, Object obj2) {
        xw5<c> xw5Var;
        switch (m.a[i - 1]) {
            case 1:
                return new c();
            case 2:
                return new a(null);
            case 3:
                return l0.p(zze, "\u0001\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001\u001e", new Object[]{"zzc", zzgz.zzb()});
            case 4:
                return zze;
            case 5:
                xw5<c> xw5Var2 = zzf;
                xw5<c> xw5Var3 = xw5Var2;
                if (xw5Var2 == null) {
                    synchronized (c.class) {
                        xw5<c> xw5Var4 = zzf;
                        xw5Var = xw5Var4;
                        if (xw5Var4 == null) {
                            ?? aVar = new l0.a(zze);
                            zzf = aVar;
                            xw5Var = aVar;
                        }
                    }
                    xw5Var3 = xw5Var;
                }
                return xw5Var3;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
