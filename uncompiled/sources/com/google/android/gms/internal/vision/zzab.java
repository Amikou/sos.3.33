package com.google.android.gms.internal.vision;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

/* compiled from: com.google.android.gms:play-services-vision@@20.1.3 */
/* loaded from: classes.dex */
public final class zzab extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzab> CREATOR = new d45();
    public final int a;
    public final int f0;
    public final int g0;
    public final int h0;
    public final float i0;

    public zzab(int i, int i2, int i3, int i4, float f) {
        this.a = i;
        this.f0 = i2;
        this.g0 = i3;
        this.h0 = i4;
        this.i0 = f;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int a = yb3.a(parcel);
        yb3.m(parcel, 2, this.a);
        yb3.m(parcel, 3, this.f0);
        yb3.m(parcel, 4, this.g0);
        yb3.m(parcel, 5, this.h0);
        yb3.j(parcel, 6, this.i0);
        yb3.b(parcel, a);
    }
}
