package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.vision.l0;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public class h0 {
    public static volatile h0 b;
    public static volatile h0 c;
    public static final h0 d = new h0(true);
    public final Map<a, l0.d<?, ?>> a;

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* loaded from: classes.dex */
    public static final class a {
        public final Object a;
        public final int b;

        public a(Object obj, int i) {
            this.a = obj;
            this.b = i;
        }

        public final boolean equals(Object obj) {
            if (obj instanceof a) {
                a aVar = (a) obj;
                return this.a == aVar.a && this.b == aVar.b;
            }
            return false;
        }

        public final int hashCode() {
            return (System.identityHashCode(this.a) * 65535) + this.b;
        }
    }

    public h0() {
        this.a = new HashMap();
    }

    public static h0 b() {
        h0 h0Var = b;
        if (h0Var == null) {
            synchronized (h0.class) {
                h0Var = b;
                if (h0Var == null) {
                    h0Var = d;
                    b = h0Var;
                }
            }
        }
        return h0Var;
    }

    public static h0 c() {
        h0 h0Var = c;
        if (h0Var != null) {
            return h0Var;
        }
        synchronized (h0.class) {
            h0 h0Var2 = c;
            if (h0Var2 != null) {
                return h0Var2;
            }
            h0 b2 = wr5.b(h0.class);
            c = b2;
            return b2;
        }
    }

    public final <ContainingType extends n0> l0.d<ContainingType, ?> a(ContainingType containingtype, int i) {
        return (l0.d<ContainingType, ?>) this.a.get(new a(containingtype, i));
    }

    public h0(boolean z) {
        this.a = Collections.emptyMap();
    }
}
