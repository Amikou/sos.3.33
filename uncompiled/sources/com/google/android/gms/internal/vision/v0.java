package com.google.android.gms.internal.vision;

import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public abstract class v0<T, B> {
    public abstract B a();

    public abstract void b(B b, int i, long j);

    public abstract void c(B b, int i, zzht zzhtVar);

    public abstract void d(T t, g1 g1Var) throws IOException;

    public abstract void e(Object obj, T t);

    public abstract T f(Object obj);

    public abstract void g(T t, g1 g1Var) throws IOException;

    public abstract void h(Object obj, B b);

    public abstract T i(T t, T t2);

    public abstract void j(Object obj);

    public abstract int k(T t);

    public abstract int l(T t);
}
