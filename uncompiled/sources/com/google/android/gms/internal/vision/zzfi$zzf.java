package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.vision.l0;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public final class zzfi$zzf extends l0<zzfi$zzf, a> implements ew5 {
    private static final zzfi$zzf zzl;
    private static volatile xw5<zzfi$zzf> zzm;
    private int zzc;
    private int zzg;
    private long zzi;
    private long zzj;
    private String zzd = "";
    private String zze = "";
    private gt5<String> zzf = l0.w();
    private String zzh = "";
    private gt5<k> zzk = l0.w();

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* loaded from: classes.dex */
    public static final class a extends l0.b<zzfi$zzf, a> implements ew5 {
        public a() {
            super(zzfi$zzf.zzl);
        }

        public final a u(long j) {
            if (this.g0) {
                r();
                this.g0 = false;
            }
            ((zzfi$zzf) this.f0).y(j);
            return this;
        }

        public final a v(Iterable<? extends k> iterable) {
            if (this.g0) {
                r();
                this.g0 = false;
            }
            ((zzfi$zzf) this.f0).C(iterable);
            return this;
        }

        public final a x(String str) {
            if (this.g0) {
                r();
                this.g0 = false;
            }
            ((zzfi$zzf) this.f0).D(str);
            return this;
        }

        public final a y(long j) {
            if (this.g0) {
                r();
                this.g0 = false;
            }
            ((zzfi$zzf) this.f0).F(j);
            return this;
        }

        public /* synthetic */ a(m mVar) {
            this();
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* loaded from: classes.dex */
    public enum zza implements rs5 {
        RESULT_UNKNOWN(0),
        RESULT_SUCCESS(1),
        RESULT_FAIL(2),
        RESULT_SKIPPED(3);
        
        private final int zzf;

        static {
            new q();
        }

        zza(int i) {
            this.zzf = i;
        }

        public static ws5 zzb() {
            return p.a;
        }

        @Override // java.lang.Enum
        public final String toString() {
            return "<" + zza.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.zzf + " name=" + name() + '>';
        }

        @Override // defpackage.rs5
        public final int zza() {
            return this.zzf;
        }

        public static zza zza(int i) {
            if (i != 0) {
                if (i != 1) {
                    if (i != 2) {
                        if (i != 3) {
                            return null;
                        }
                        return RESULT_SKIPPED;
                    }
                    return RESULT_FAIL;
                }
                return RESULT_SUCCESS;
            }
            return RESULT_UNKNOWN;
        }
    }

    static {
        zzfi$zzf zzfi_zzf = new zzfi$zzf();
        zzl = zzfi_zzf;
        l0.s(zzfi$zzf.class, zzfi_zzf);
    }

    public static a x() {
        return zzl.u();
    }

    public final void C(Iterable<? extends k> iterable) {
        gt5<k> gt5Var = this.zzk;
        if (!gt5Var.zza()) {
            this.zzk = l0.r(gt5Var);
        }
        a0.b(iterable, this.zzk);
    }

    public final void D(String str) {
        str.getClass();
        this.zzc |= 1;
        this.zzd = str;
    }

    public final void F(long j) {
        this.zzc |= 32;
        this.zzj = j;
    }

    /* JADX WARN: Type inference failed for: r2v14, types: [xw5<com.google.android.gms.internal.vision.zzfi$zzf>, com.google.android.gms.internal.vision.l0$a] */
    @Override // com.google.android.gms.internal.vision.l0
    public final Object o(int i, Object obj, Object obj2) {
        xw5<zzfi$zzf> xw5Var;
        switch (m.a[i - 1]) {
            case 1:
                return new zzfi$zzf();
            case 2:
                return new a(null);
            case 3:
                return l0.p(zzl, "\u0001\b\u0000\u0001\u0001\b\b\u0000\u0002\u0000\u0001ဈ\u0000\u0002ဈ\u0001\u0003\u001a\u0004ဌ\u0002\u0005ဈ\u0003\u0006ဂ\u0004\u0007ဂ\u0005\b\u001b", new Object[]{"zzc", "zzd", "zze", "zzf", "zzg", zza.zzb(), "zzh", "zzi", "zzj", "zzk", k.class});
            case 4:
                return zzl;
            case 5:
                xw5<zzfi$zzf> xw5Var2 = zzm;
                xw5<zzfi$zzf> xw5Var3 = xw5Var2;
                if (xw5Var2 == null) {
                    synchronized (zzfi$zzf.class) {
                        xw5<zzfi$zzf> xw5Var4 = zzm;
                        xw5Var = xw5Var4;
                        if (xw5Var4 == null) {
                            ?? aVar = new l0.a(zzl);
                            zzm = aVar;
                            xw5Var = aVar;
                        }
                    }
                    xw5Var3 = xw5Var;
                }
                return xw5Var3;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public final void y(long j) {
        this.zzc |= 16;
        this.zzi = j;
    }
}
