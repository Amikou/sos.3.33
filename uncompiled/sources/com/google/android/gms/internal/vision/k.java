package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.vision.l0;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public final class k extends l0<k, a> implements ew5 {
    private static final k zzh;
    private static volatile xw5<k> zzi;
    private int zzc;
    private e zzd;
    private int zze;
    private f zzf;
    private d zzg;

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* loaded from: classes.dex */
    public static final class a extends l0.b<k, a> implements ew5 {
        public a() {
            super(k.zzh);
        }

        public /* synthetic */ a(m mVar) {
            this();
        }
    }

    static {
        k kVar = new k();
        zzh = kVar;
        l0.s(k.class, kVar);
    }

    /* JADX WARN: Type inference failed for: r2v14, types: [xw5<com.google.android.gms.internal.vision.k>, com.google.android.gms.internal.vision.l0$a] */
    @Override // com.google.android.gms.internal.vision.l0
    public final Object o(int i, Object obj, Object obj2) {
        xw5<k> xw5Var;
        switch (m.a[i - 1]) {
            case 1:
                return new k();
            case 2:
                return new a(null);
            case 3:
                return l0.p(zzh, "\u0001\u0004\u0000\u0001\u0001\u0011\u0004\u0000\u0000\u0000\u0001ဉ\u0000\u0002င\u0001\u0010ဉ\u0002\u0011ဉ\u0003", new Object[]{"zzc", "zzd", "zze", "zzf", "zzg"});
            case 4:
                return zzh;
            case 5:
                xw5<k> xw5Var2 = zzi;
                xw5<k> xw5Var3 = xw5Var2;
                if (xw5Var2 == null) {
                    synchronized (k.class) {
                        xw5<k> xw5Var4 = zzi;
                        xw5Var = xw5Var4;
                        if (xw5Var4 == null) {
                            ?? aVar = new l0.a(zzh);
                            zzi = aVar;
                            xw5Var = aVar;
                        }
                    }
                    xw5Var3 = xw5Var;
                }
                return xw5Var3;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
