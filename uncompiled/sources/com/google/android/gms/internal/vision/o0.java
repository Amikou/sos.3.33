package com.google.android.gms.internal.vision;

import com.github.mikephil.charting.utils.Utils;
import com.google.android.gms.internal.vision.l0;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import sun.misc.Unsafe;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public final class o0<T> implements t0<T> {
    public static final int[] p = new int[0];
    public static final Unsafe q = z0.t();
    public final int[] a;
    public final Object[] b;
    public final int c;
    public final int d;
    public final n0 e;
    public final boolean f;
    public final boolean g;
    public final int[] h;
    public final int i;
    public final int j;
    public final lw5 k;
    public final eu5 l;
    public final v0<?, ?> m;
    public final j0<?> n;
    public final qv5 o;

    public o0(int[] iArr, Object[] objArr, int i, int i2, n0 n0Var, boolean z, boolean z2, int[] iArr2, int i3, int i4, lw5 lw5Var, eu5 eu5Var, v0<?, ?> v0Var, j0<?> j0Var, qv5 qv5Var) {
        this.a = iArr;
        this.b = objArr;
        this.c = i;
        this.d = i2;
        boolean z3 = n0Var instanceof l0;
        this.g = z;
        this.f = j0Var != null && j0Var.e(n0Var);
        this.h = iArr2;
        this.i = i3;
        this.j = i4;
        this.k = lw5Var;
        this.l = eu5Var;
        this.m = v0Var;
        this.n = j0Var;
        this.e = n0Var;
        this.o = qv5Var;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static boolean B(Object obj, int i, t0 t0Var) {
        return t0Var.e(z0.F(obj, i & 1048575));
    }

    public static <T> double C(T t, long j) {
        return ((Double) z0.F(t, j)).doubleValue();
    }

    public static <T> float J(T t, long j) {
        return ((Float) z0.F(t, j)).floatValue();
    }

    public static <T> int N(T t, long j) {
        return ((Integer) z0.F(t, j)).intValue();
    }

    public static <T> long P(T t, long j) {
        return ((Long) z0.F(t, j)).longValue();
    }

    public static x0 Q(Object obj) {
        l0 l0Var = (l0) obj;
        x0 x0Var = l0Var.zzb;
        if (x0Var == x0.a()) {
            x0 g = x0.g();
            l0Var.zzb = g;
            return g;
        }
        return x0Var;
    }

    public static <T> boolean R(T t, long j) {
        return ((Boolean) z0.F(t, j)).booleanValue();
    }

    public static <UT, UB> int j(v0<UT, UB> v0Var, T t) {
        return v0Var.l(v0Var.f(t));
    }

    /* JADX WARN: Removed duplicated region for block: B:163:0x0338  */
    /* JADX WARN: Removed duplicated region for block: B:180:0x0397  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static <T> com.google.android.gms.internal.vision.o0<T> o(java.lang.Class<T> r33, defpackage.tv5 r34, defpackage.lw5 r35, defpackage.eu5 r36, com.google.android.gms.internal.vision.v0<?, ?> r37, com.google.android.gms.internal.vision.j0<?> r38, defpackage.qv5 r39) {
        /*
            Method dump skipped, instructions count: 1045
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.vision.o0.o(java.lang.Class, tv5, lw5, eu5, com.google.android.gms.internal.vision.v0, com.google.android.gms.internal.vision.j0, qv5):com.google.android.gms.internal.vision.o0");
    }

    public static Field s(Class<?> cls, String str) {
        try {
            return cls.getDeclaredField(str);
        } catch (NoSuchFieldException unused) {
            Field[] declaredFields = cls.getDeclaredFields();
            for (Field field : declaredFields) {
                if (str.equals(field.getName())) {
                    return field;
                }
            }
            String name = cls.getName();
            String arrays = Arrays.toString(declaredFields);
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 40 + name.length() + String.valueOf(arrays).length());
            sb.append("Field ");
            sb.append(str);
            sb.append(" for ");
            sb.append(name);
            sb.append(" not found. Known fields are ");
            sb.append(arrays);
            throw new RuntimeException(sb.toString());
        }
    }

    public static List<?> t(Object obj, long j) {
        return (List) z0.F(obj, j);
    }

    public static void u(int i, Object obj, g1 g1Var) throws IOException {
        if (obj instanceof String) {
            g1Var.n(i, (String) obj);
        } else {
            g1Var.F(i, (zzht) obj);
        }
    }

    public static <UT, UB> void v(v0<UT, UB> v0Var, T t, g1 g1Var) throws IOException {
        v0Var.d(v0Var.f(t), g1Var);
    }

    public final boolean A(T t, int i, int i2, int i3, int i4) {
        if (i2 == 1048575) {
            return y(t, i);
        }
        return (i3 & i4) != 0;
    }

    public final int D(int i, int i2) {
        int length = (this.a.length / 3) - 1;
        while (i2 <= length) {
            int i3 = (length + i2) >>> 1;
            int i4 = i3 * 3;
            int i5 = this.a[i4];
            if (i == i5) {
                return i4;
            }
            if (i < i5) {
                length = i3 - 1;
            } else {
                i2 = i3 + 1;
            }
        }
        return -1;
    }

    public final Object E(int i) {
        return this.b[(i / 3) << 1];
    }

    public final void F(T t, int i) {
        int O = O(i);
        long j = 1048575 & O;
        if (j == 1048575) {
            return;
        }
        z0.h(t, j, (1 << (O >>> 20)) | z0.b(t, j));
    }

    public final void G(T t, int i, int i2) {
        z0.h(t, O(i2) & 1048575, i);
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARN: Removed duplicated region for block: B:10:0x0031  */
    /* JADX WARN: Removed duplicated region for block: B:171:0x048b  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void H(T r18, com.google.android.gms.internal.vision.g1 r19) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 1332
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.vision.o0.H(java.lang.Object, com.google.android.gms.internal.vision.g1):void");
    }

    public final void I(T t, T t2, int i) {
        int M = M(i);
        int i2 = this.a[i];
        long j = M & 1048575;
        if (z(t2, i2, i)) {
            Object F = z(t, i2, i) ? z0.F(t, j) : null;
            Object F2 = z0.F(t2, j);
            if (F != null && F2 != null) {
                z0.j(t, j, vs5.e(F, F2));
                G(t, i2, i);
            } else if (F2 != null) {
                z0.j(t, j, F2);
                G(t, i2, i);
            }
        }
    }

    public final ws5 K(int i) {
        return (ws5) this.b[((i / 3) << 1) + 1];
    }

    public final boolean L(T t, T t2, int i) {
        return y(t, i) == y(t2, i);
    }

    public final int M(int i) {
        return this.a[i + 1];
    }

    public final int O(int i) {
        return this.a[i + 2];
    }

    public final int S(int i) {
        if (i < this.c || i > this.d) {
            return -1;
        }
        return D(i, 0);
    }

    @Override // com.google.android.gms.internal.vision.t0
    public final void a(T t) {
        int i;
        int i2 = this.i;
        while (true) {
            i = this.j;
            if (i2 >= i) {
                break;
            }
            long M = M(this.h[i2]) & 1048575;
            Object F = z0.F(t, M);
            if (F != null) {
                z0.j(t, M, this.o.d(F));
            }
            i2++;
        }
        int length = this.h.length;
        while (i < length) {
            this.l.d(t, this.h[i]);
            i++;
        }
        this.m.j(t);
        if (this.f) {
            this.n.g(t);
        }
    }

    @Override // com.google.android.gms.internal.vision.t0
    public final int b(T t) {
        int i;
        int b;
        int length = this.a.length;
        int i2 = 0;
        for (int i3 = 0; i3 < length; i3 += 3) {
            int M = M(i3);
            int i4 = this.a[i3];
            long j = 1048575 & M;
            int i5 = 37;
            switch ((M & 267386880) >>> 20) {
                case 0:
                    i = i2 * 53;
                    b = vs5.b(Double.doubleToLongBits(z0.C(t, j)));
                    i2 = i + b;
                    break;
                case 1:
                    i = i2 * 53;
                    b = Float.floatToIntBits(z0.x(t, j));
                    i2 = i + b;
                    break;
                case 2:
                    i = i2 * 53;
                    b = vs5.b(z0.o(t, j));
                    i2 = i + b;
                    break;
                case 3:
                    i = i2 * 53;
                    b = vs5.b(z0.o(t, j));
                    i2 = i + b;
                    break;
                case 4:
                    i = i2 * 53;
                    b = z0.b(t, j);
                    i2 = i + b;
                    break;
                case 5:
                    i = i2 * 53;
                    b = vs5.b(z0.o(t, j));
                    i2 = i + b;
                    break;
                case 6:
                    i = i2 * 53;
                    b = z0.b(t, j);
                    i2 = i + b;
                    break;
                case 7:
                    i = i2 * 53;
                    b = vs5.c(z0.w(t, j));
                    i2 = i + b;
                    break;
                case 8:
                    i = i2 * 53;
                    b = ((String) z0.F(t, j)).hashCode();
                    i2 = i + b;
                    break;
                case 9:
                    Object F = z0.F(t, j);
                    if (F != null) {
                        i5 = F.hashCode();
                    }
                    i2 = (i2 * 53) + i5;
                    break;
                case 10:
                    i = i2 * 53;
                    b = z0.F(t, j).hashCode();
                    i2 = i + b;
                    break;
                case 11:
                    i = i2 * 53;
                    b = z0.b(t, j);
                    i2 = i + b;
                    break;
                case 12:
                    i = i2 * 53;
                    b = z0.b(t, j);
                    i2 = i + b;
                    break;
                case 13:
                    i = i2 * 53;
                    b = z0.b(t, j);
                    i2 = i + b;
                    break;
                case 14:
                    i = i2 * 53;
                    b = vs5.b(z0.o(t, j));
                    i2 = i + b;
                    break;
                case 15:
                    i = i2 * 53;
                    b = z0.b(t, j);
                    i2 = i + b;
                    break;
                case 16:
                    i = i2 * 53;
                    b = vs5.b(z0.o(t, j));
                    i2 = i + b;
                    break;
                case 17:
                    Object F2 = z0.F(t, j);
                    if (F2 != null) {
                        i5 = F2.hashCode();
                    }
                    i2 = (i2 * 53) + i5;
                    break;
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                case 48:
                case 49:
                    i = i2 * 53;
                    b = z0.F(t, j).hashCode();
                    i2 = i + b;
                    break;
                case 50:
                    i = i2 * 53;
                    b = z0.F(t, j).hashCode();
                    i2 = i + b;
                    break;
                case 51:
                    if (z(t, i4, i3)) {
                        i = i2 * 53;
                        b = vs5.b(Double.doubleToLongBits(C(t, j)));
                        i2 = i + b;
                        break;
                    } else {
                        break;
                    }
                case 52:
                    if (z(t, i4, i3)) {
                        i = i2 * 53;
                        b = Float.floatToIntBits(J(t, j));
                        i2 = i + b;
                        break;
                    } else {
                        break;
                    }
                case 53:
                    if (z(t, i4, i3)) {
                        i = i2 * 53;
                        b = vs5.b(P(t, j));
                        i2 = i + b;
                        break;
                    } else {
                        break;
                    }
                case 54:
                    if (z(t, i4, i3)) {
                        i = i2 * 53;
                        b = vs5.b(P(t, j));
                        i2 = i + b;
                        break;
                    } else {
                        break;
                    }
                case 55:
                    if (z(t, i4, i3)) {
                        i = i2 * 53;
                        b = N(t, j);
                        i2 = i + b;
                        break;
                    } else {
                        break;
                    }
                case 56:
                    if (z(t, i4, i3)) {
                        i = i2 * 53;
                        b = vs5.b(P(t, j));
                        i2 = i + b;
                        break;
                    } else {
                        break;
                    }
                case 57:
                    if (z(t, i4, i3)) {
                        i = i2 * 53;
                        b = N(t, j);
                        i2 = i + b;
                        break;
                    } else {
                        break;
                    }
                case 58:
                    if (z(t, i4, i3)) {
                        i = i2 * 53;
                        b = vs5.c(R(t, j));
                        i2 = i + b;
                        break;
                    } else {
                        break;
                    }
                case 59:
                    if (z(t, i4, i3)) {
                        i = i2 * 53;
                        b = ((String) z0.F(t, j)).hashCode();
                        i2 = i + b;
                        break;
                    } else {
                        break;
                    }
                case 60:
                    if (z(t, i4, i3)) {
                        i = i2 * 53;
                        b = z0.F(t, j).hashCode();
                        i2 = i + b;
                        break;
                    } else {
                        break;
                    }
                case 61:
                    if (z(t, i4, i3)) {
                        i = i2 * 53;
                        b = z0.F(t, j).hashCode();
                        i2 = i + b;
                        break;
                    } else {
                        break;
                    }
                case 62:
                    if (z(t, i4, i3)) {
                        i = i2 * 53;
                        b = N(t, j);
                        i2 = i + b;
                        break;
                    } else {
                        break;
                    }
                case 63:
                    if (z(t, i4, i3)) {
                        i = i2 * 53;
                        b = N(t, j);
                        i2 = i + b;
                        break;
                    } else {
                        break;
                    }
                case 64:
                    if (z(t, i4, i3)) {
                        i = i2 * 53;
                        b = N(t, j);
                        i2 = i + b;
                        break;
                    } else {
                        break;
                    }
                case 65:
                    if (z(t, i4, i3)) {
                        i = i2 * 53;
                        b = vs5.b(P(t, j));
                        i2 = i + b;
                        break;
                    } else {
                        break;
                    }
                case 66:
                    if (z(t, i4, i3)) {
                        i = i2 * 53;
                        b = N(t, j);
                        i2 = i + b;
                        break;
                    } else {
                        break;
                    }
                case 67:
                    if (z(t, i4, i3)) {
                        i = i2 * 53;
                        b = vs5.b(P(t, j));
                        i2 = i + b;
                        break;
                    } else {
                        break;
                    }
                case 68:
                    if (z(t, i4, i3)) {
                        i = i2 * 53;
                        b = z0.F(t, j).hashCode();
                        i2 = i + b;
                        break;
                    } else {
                        break;
                    }
            }
        }
        int hashCode = (i2 * 53) + this.m.f(t).hashCode();
        return this.f ? (hashCode * 53) + this.n.b(t).hashCode() : hashCode;
    }

    @Override // com.google.android.gms.internal.vision.t0
    public final void c(T t, T t2) {
        Objects.requireNonNull(t2);
        for (int i = 0; i < this.a.length; i += 3) {
            int M = M(i);
            long j = 1048575 & M;
            int i2 = this.a[i];
            switch ((M & 267386880) >>> 20) {
                case 0:
                    if (y(t2, i)) {
                        z0.f(t, j, z0.C(t2, j));
                        F(t, i);
                        break;
                    } else {
                        break;
                    }
                case 1:
                    if (y(t2, i)) {
                        z0.g(t, j, z0.x(t2, j));
                        F(t, i);
                        break;
                    } else {
                        break;
                    }
                case 2:
                    if (y(t2, i)) {
                        z0.i(t, j, z0.o(t2, j));
                        F(t, i);
                        break;
                    } else {
                        break;
                    }
                case 3:
                    if (y(t2, i)) {
                        z0.i(t, j, z0.o(t2, j));
                        F(t, i);
                        break;
                    } else {
                        break;
                    }
                case 4:
                    if (y(t2, i)) {
                        z0.h(t, j, z0.b(t2, j));
                        F(t, i);
                        break;
                    } else {
                        break;
                    }
                case 5:
                    if (y(t2, i)) {
                        z0.i(t, j, z0.o(t2, j));
                        F(t, i);
                        break;
                    } else {
                        break;
                    }
                case 6:
                    if (y(t2, i)) {
                        z0.h(t, j, z0.b(t2, j));
                        F(t, i);
                        break;
                    } else {
                        break;
                    }
                case 7:
                    if (y(t2, i)) {
                        z0.k(t, j, z0.w(t2, j));
                        F(t, i);
                        break;
                    } else {
                        break;
                    }
                case 8:
                    if (y(t2, i)) {
                        z0.j(t, j, z0.F(t2, j));
                        F(t, i);
                        break;
                    } else {
                        break;
                    }
                case 9:
                    x(t, t2, i);
                    break;
                case 10:
                    if (y(t2, i)) {
                        z0.j(t, j, z0.F(t2, j));
                        F(t, i);
                        break;
                    } else {
                        break;
                    }
                case 11:
                    if (y(t2, i)) {
                        z0.h(t, j, z0.b(t2, j));
                        F(t, i);
                        break;
                    } else {
                        break;
                    }
                case 12:
                    if (y(t2, i)) {
                        z0.h(t, j, z0.b(t2, j));
                        F(t, i);
                        break;
                    } else {
                        break;
                    }
                case 13:
                    if (y(t2, i)) {
                        z0.h(t, j, z0.b(t2, j));
                        F(t, i);
                        break;
                    } else {
                        break;
                    }
                case 14:
                    if (y(t2, i)) {
                        z0.i(t, j, z0.o(t2, j));
                        F(t, i);
                        break;
                    } else {
                        break;
                    }
                case 15:
                    if (y(t2, i)) {
                        z0.h(t, j, z0.b(t2, j));
                        F(t, i);
                        break;
                    } else {
                        break;
                    }
                case 16:
                    if (y(t2, i)) {
                        z0.i(t, j, z0.o(t2, j));
                        F(t, i);
                        break;
                    } else {
                        break;
                    }
                case 17:
                    x(t, t2, i);
                    break;
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                case 48:
                case 49:
                    this.l.b(t, t2, j);
                    break;
                case 50:
                    u0.p(this.o, t, t2, j);
                    break;
                case 51:
                case 52:
                case 53:
                case 54:
                case 55:
                case 56:
                case 57:
                case 58:
                case 59:
                    if (z(t2, i2, i)) {
                        z0.j(t, j, z0.F(t2, j));
                        G(t, i2, i);
                        break;
                    } else {
                        break;
                    }
                case 60:
                    I(t, t2, i);
                    break;
                case 61:
                case 62:
                case 63:
                case 64:
                case 65:
                case 66:
                case 67:
                    if (z(t2, i2, i)) {
                        z0.j(t, j, z0.F(t2, j));
                        G(t, i2, i);
                        break;
                    } else {
                        break;
                    }
                case 68:
                    I(t, t2, i);
                    break;
            }
        }
        u0.n(this.m, t, t2);
        if (this.f) {
            u0.m(this.n, t, t2);
        }
    }

    public final int d(int i, int i2) {
        if (i < this.c || i > this.d) {
            return -1;
        }
        return D(i, i2);
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.google.android.gms.internal.vision.t0
    public final boolean e(T t) {
        int i;
        int i2;
        int i3 = 1048575;
        int i4 = 0;
        int i5 = 0;
        while (true) {
            boolean z = true;
            if (i5 >= this.i) {
                return !this.f || this.n.b(t).r();
            }
            int i6 = this.h[i5];
            int i7 = this.a[i6];
            int M = M(i6);
            int i8 = this.a[i6 + 2];
            int i9 = i8 & 1048575;
            int i10 = 1 << (i8 >>> 20);
            if (i9 != i3) {
                if (i9 != 1048575) {
                    i4 = q.getInt(t, i9);
                }
                i2 = i4;
                i = i9;
            } else {
                i = i3;
                i2 = i4;
            }
            if (((268435456 & M) != 0) && !A(t, i6, i, i2, i10)) {
                return false;
            }
            int i11 = (267386880 & M) >>> 20;
            if (i11 != 9 && i11 != 17) {
                if (i11 != 27) {
                    if (i11 == 60 || i11 == 68) {
                        if (z(t, i7, i6) && !B(t, M, p(i6))) {
                            return false;
                        }
                    } else if (i11 != 49) {
                        if (i11 == 50 && !this.o.a(z0.F(t, M & 1048575)).isEmpty()) {
                            this.o.f(E(i6));
                            throw null;
                        }
                    }
                }
                List list = (List) z0.F(t, M & 1048575);
                if (!list.isEmpty()) {
                    t0 p2 = p(i6);
                    int i12 = 0;
                    while (true) {
                        if (i12 >= list.size()) {
                            break;
                        } else if (!p2.e(list.get(i12))) {
                            z = false;
                            break;
                        } else {
                            i12++;
                        }
                    }
                }
                if (!z) {
                    return false;
                }
            } else if (A(t, i6, i, i2, i10) && !B(t, M, p(i6))) {
                return false;
            }
            i5++;
            i3 = i;
            i4 = i2;
        }
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // com.google.android.gms.internal.vision.t0
    public final int f(T t) {
        int i;
        int i2;
        int i3;
        long j;
        int b0;
        int F;
        int A0;
        int R;
        int V;
        int g0;
        int o0;
        int B;
        int V2;
        int g02;
        int o02;
        int i4 = 267386880;
        int i5 = 1048575;
        int i6 = 1;
        int i7 = 0;
        if (this.g) {
            Unsafe unsafe = q;
            int i8 = 0;
            int i9 = 0;
            while (i8 < this.a.length) {
                int M = M(i8);
                int i10 = (M & i4) >>> 20;
                int i11 = this.a[i8];
                long j2 = M & 1048575;
                if (i10 >= zziv.zza.zza() && i10 <= zziv.zzb.zza()) {
                    int i12 = this.a[i8 + 2];
                }
                switch (i10) {
                    case 0:
                        if (y(t, i8)) {
                            B = zzii.B(i11, Utils.DOUBLE_EPSILON);
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 1:
                        if (y(t, i8)) {
                            B = zzii.C(i11, Utils.FLOAT_EPSILON);
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 2:
                        if (y(t, i8)) {
                            B = zzii.b0(i11, z0.o(t, j2));
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 3:
                        if (y(t, i8)) {
                            B = zzii.h0(i11, z0.o(t, j2));
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 4:
                        if (y(t, i8)) {
                            B = zzii.l0(i11, z0.b(t, j2));
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 5:
                        if (y(t, i8)) {
                            B = zzii.q0(i11, 0L);
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 6:
                        if (y(t, i8)) {
                            B = zzii.x0(i11, 0);
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 7:
                        if (y(t, i8)) {
                            B = zzii.H(i11, true);
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 8:
                        if (y(t, i8)) {
                            Object F2 = z0.F(t, j2);
                            if (F2 instanceof zzht) {
                                B = zzii.T(i11, (zzht) F2);
                                break;
                            } else {
                                B = zzii.F(i11, (String) F2);
                                break;
                            }
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 9:
                        if (y(t, i8)) {
                            B = u0.a(i11, z0.F(t, j2), p(i8));
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 10:
                        if (y(t, i8)) {
                            B = zzii.T(i11, (zzht) z0.F(t, j2));
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 11:
                        if (y(t, i8)) {
                            B = zzii.p0(i11, z0.b(t, j2));
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 12:
                        if (y(t, i8)) {
                            B = zzii.C0(i11, z0.b(t, j2));
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 13:
                        if (y(t, i8)) {
                            B = zzii.A0(i11, 0);
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 14:
                        if (y(t, i8)) {
                            B = zzii.u0(i11, 0L);
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 15:
                        if (y(t, i8)) {
                            B = zzii.t0(i11, z0.b(t, j2));
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 16:
                        if (y(t, i8)) {
                            B = zzii.m0(i11, z0.o(t, j2));
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 17:
                        if (y(t, i8)) {
                            B = zzii.U(i11, (n0) z0.F(t, j2), p(i8));
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 18:
                        B = u0.U(i11, t(t, j2), false);
                        break;
                    case 19:
                        B = u0.R(i11, t(t, j2), false);
                        break;
                    case 20:
                        B = u0.d(i11, t(t, j2), false);
                        break;
                    case 21:
                        B = u0.t(i11, t(t, j2), false);
                        break;
                    case 22:
                        B = u0.H(i11, t(t, j2), false);
                        break;
                    case 23:
                        B = u0.U(i11, t(t, j2), false);
                        break;
                    case 24:
                        B = u0.R(i11, t(t, j2), false);
                        break;
                    case 25:
                        B = u0.X(i11, t(t, j2), false);
                        break;
                    case 26:
                        B = u0.b(i11, t(t, j2));
                        break;
                    case 27:
                        B = u0.c(i11, t(t, j2), p(i8));
                        break;
                    case 28:
                        B = u0.r(i11, t(t, j2));
                        break;
                    case 29:
                        B = u0.L(i11, t(t, j2), false);
                        break;
                    case 30:
                        B = u0.D(i11, t(t, j2), false);
                        break;
                    case 31:
                        B = u0.R(i11, t(t, j2), false);
                        break;
                    case 32:
                        B = u0.U(i11, t(t, j2), false);
                        break;
                    case 33:
                        B = u0.O(i11, t(t, j2), false);
                        break;
                    case 34:
                        B = u0.z(i11, t(t, j2), false);
                        break;
                    case 35:
                        V2 = u0.V((List) unsafe.getObject(t, j2));
                        if (V2 > 0) {
                            g02 = zzii.g0(i11);
                            o02 = zzii.o0(V2);
                            B = g02 + o02 + V2;
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 36:
                        V2 = u0.S((List) unsafe.getObject(t, j2));
                        if (V2 > 0) {
                            g02 = zzii.g0(i11);
                            o02 = zzii.o0(V2);
                            B = g02 + o02 + V2;
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 37:
                        V2 = u0.e((List) unsafe.getObject(t, j2));
                        if (V2 > 0) {
                            g02 = zzii.g0(i11);
                            o02 = zzii.o0(V2);
                            B = g02 + o02 + V2;
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 38:
                        V2 = u0.u((List) unsafe.getObject(t, j2));
                        if (V2 > 0) {
                            g02 = zzii.g0(i11);
                            o02 = zzii.o0(V2);
                            B = g02 + o02 + V2;
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 39:
                        V2 = u0.I((List) unsafe.getObject(t, j2));
                        if (V2 > 0) {
                            g02 = zzii.g0(i11);
                            o02 = zzii.o0(V2);
                            B = g02 + o02 + V2;
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 40:
                        V2 = u0.V((List) unsafe.getObject(t, j2));
                        if (V2 > 0) {
                            g02 = zzii.g0(i11);
                            o02 = zzii.o0(V2);
                            B = g02 + o02 + V2;
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 41:
                        V2 = u0.S((List) unsafe.getObject(t, j2));
                        if (V2 > 0) {
                            g02 = zzii.g0(i11);
                            o02 = zzii.o0(V2);
                            B = g02 + o02 + V2;
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 42:
                        V2 = u0.Y((List) unsafe.getObject(t, j2));
                        if (V2 > 0) {
                            g02 = zzii.g0(i11);
                            o02 = zzii.o0(V2);
                            B = g02 + o02 + V2;
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 43:
                        V2 = u0.M((List) unsafe.getObject(t, j2));
                        if (V2 > 0) {
                            g02 = zzii.g0(i11);
                            o02 = zzii.o0(V2);
                            B = g02 + o02 + V2;
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 44:
                        V2 = u0.E((List) unsafe.getObject(t, j2));
                        if (V2 > 0) {
                            g02 = zzii.g0(i11);
                            o02 = zzii.o0(V2);
                            B = g02 + o02 + V2;
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 45:
                        V2 = u0.S((List) unsafe.getObject(t, j2));
                        if (V2 > 0) {
                            g02 = zzii.g0(i11);
                            o02 = zzii.o0(V2);
                            B = g02 + o02 + V2;
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 46:
                        V2 = u0.V((List) unsafe.getObject(t, j2));
                        if (V2 > 0) {
                            g02 = zzii.g0(i11);
                            o02 = zzii.o0(V2);
                            B = g02 + o02 + V2;
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 47:
                        V2 = u0.P((List) unsafe.getObject(t, j2));
                        if (V2 > 0) {
                            g02 = zzii.g0(i11);
                            o02 = zzii.o0(V2);
                            B = g02 + o02 + V2;
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 48:
                        V2 = u0.A((List) unsafe.getObject(t, j2));
                        if (V2 > 0) {
                            g02 = zzii.g0(i11);
                            o02 = zzii.o0(V2);
                            B = g02 + o02 + V2;
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 49:
                        B = u0.s(i11, t(t, j2), p(i8));
                        break;
                    case 50:
                        B = this.o.h(i11, z0.F(t, j2), E(i8));
                        break;
                    case 51:
                        if (z(t, i11, i8)) {
                            B = zzii.B(i11, Utils.DOUBLE_EPSILON);
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 52:
                        if (z(t, i11, i8)) {
                            B = zzii.C(i11, Utils.FLOAT_EPSILON);
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 53:
                        if (z(t, i11, i8)) {
                            B = zzii.b0(i11, P(t, j2));
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 54:
                        if (z(t, i11, i8)) {
                            B = zzii.h0(i11, P(t, j2));
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 55:
                        if (z(t, i11, i8)) {
                            B = zzii.l0(i11, N(t, j2));
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 56:
                        if (z(t, i11, i8)) {
                            B = zzii.q0(i11, 0L);
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 57:
                        if (z(t, i11, i8)) {
                            B = zzii.x0(i11, 0);
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 58:
                        if (z(t, i11, i8)) {
                            B = zzii.H(i11, true);
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 59:
                        if (z(t, i11, i8)) {
                            Object F3 = z0.F(t, j2);
                            if (F3 instanceof zzht) {
                                B = zzii.T(i11, (zzht) F3);
                                break;
                            } else {
                                B = zzii.F(i11, (String) F3);
                                break;
                            }
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 60:
                        if (z(t, i11, i8)) {
                            B = u0.a(i11, z0.F(t, j2), p(i8));
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 61:
                        if (z(t, i11, i8)) {
                            B = zzii.T(i11, (zzht) z0.F(t, j2));
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 62:
                        if (z(t, i11, i8)) {
                            B = zzii.p0(i11, N(t, j2));
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 63:
                        if (z(t, i11, i8)) {
                            B = zzii.C0(i11, N(t, j2));
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 64:
                        if (z(t, i11, i8)) {
                            B = zzii.A0(i11, 0);
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 65:
                        if (z(t, i11, i8)) {
                            B = zzii.u0(i11, 0L);
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 66:
                        if (z(t, i11, i8)) {
                            B = zzii.t0(i11, N(t, j2));
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 67:
                        if (z(t, i11, i8)) {
                            B = zzii.m0(i11, P(t, j2));
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    case 68:
                        if (z(t, i11, i8)) {
                            B = zzii.U(i11, (n0) z0.F(t, j2), p(i8));
                            break;
                        } else {
                            continue;
                            i8 += 3;
                            i4 = 267386880;
                        }
                    default:
                        i8 += 3;
                        i4 = 267386880;
                }
                i9 += B;
                i8 += 3;
                i4 = 267386880;
            }
            return i9 + j(this.m, t);
        }
        Unsafe unsafe2 = q;
        int i13 = 1048575;
        int i14 = 0;
        int i15 = 0;
        int i16 = 0;
        while (i14 < this.a.length) {
            int M2 = M(i14);
            int[] iArr = this.a;
            int i17 = iArr[i14];
            int i18 = (M2 & 267386880) >>> 20;
            if (i18 <= 17) {
                int i19 = iArr[i14 + 2];
                int i20 = i19 & i5;
                i = i6 << (i19 >>> 20);
                if (i20 != i13) {
                    i16 = unsafe2.getInt(t, i20);
                    i13 = i20;
                }
            } else {
                i = 0;
            }
            long j3 = M2 & i5;
            switch (i18) {
                case 0:
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    if ((i16 & i) != 0) {
                        i15 += zzii.B(i17, Utils.DOUBLE_EPSILON);
                        continue;
                        i14 += 3;
                        i6 = i2;
                        i7 = i3;
                        i5 = 1048575;
                    }
                    break;
                case 1:
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    if ((i16 & i) != 0) {
                        i15 += zzii.C(i17, Utils.FLOAT_EPSILON);
                        break;
                    }
                    break;
                case 2:
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    if ((i & i16) != 0) {
                        b0 = zzii.b0(i17, unsafe2.getLong(t, j3));
                        i15 += b0;
                        break;
                    }
                    break;
                case 3:
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    if ((i & i16) != 0) {
                        b0 = zzii.h0(i17, unsafe2.getLong(t, j3));
                        i15 += b0;
                        break;
                    }
                    break;
                case 4:
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    if ((i & i16) != 0) {
                        b0 = zzii.l0(i17, unsafe2.getInt(t, j3));
                        i15 += b0;
                        break;
                    }
                    break;
                case 5:
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    if ((i16 & i) != 0) {
                        b0 = zzii.q0(i17, 0L);
                        i15 += b0;
                        break;
                    }
                    break;
                case 6:
                    i2 = 1;
                    i3 = 0;
                    if ((i16 & i) != 0) {
                        i15 += zzii.x0(i17, 0);
                    }
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 7:
                    if ((i16 & i) != 0) {
                        i2 = 1;
                        i15 += zzii.H(i17, true);
                        i3 = 0;
                        j = 0;
                        i14 += 3;
                        i6 = i2;
                        i7 = i3;
                        i5 = 1048575;
                    }
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 8:
                    if ((i16 & i) != 0) {
                        Object object = unsafe2.getObject(t, j3);
                        if (object instanceof zzht) {
                            F = zzii.T(i17, (zzht) object);
                        } else {
                            F = zzii.F(i17, (String) object);
                        }
                        i15 += F;
                    }
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 9:
                    if ((i16 & i) != 0) {
                        F = u0.a(i17, unsafe2.getObject(t, j3), p(i14));
                        i15 += F;
                    }
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 10:
                    if ((i16 & i) != 0) {
                        F = zzii.T(i17, (zzht) unsafe2.getObject(t, j3));
                        i15 += F;
                    }
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 11:
                    if ((i16 & i) != 0) {
                        F = zzii.p0(i17, unsafe2.getInt(t, j3));
                        i15 += F;
                    }
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 12:
                    if ((i16 & i) != 0) {
                        F = zzii.C0(i17, unsafe2.getInt(t, j3));
                        i15 += F;
                    }
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 13:
                    if ((i16 & i) != 0) {
                        A0 = zzii.A0(i17, 0);
                        i15 += A0;
                    }
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 14:
                    if ((i16 & i) != 0) {
                        F = zzii.u0(i17, 0L);
                        i15 += F;
                    }
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 15:
                    if ((i16 & i) != 0) {
                        F = zzii.t0(i17, unsafe2.getInt(t, j3));
                        i15 += F;
                    }
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 16:
                    if ((i16 & i) != 0) {
                        F = zzii.m0(i17, unsafe2.getLong(t, j3));
                        i15 += F;
                    }
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 17:
                    if ((i16 & i) != 0) {
                        F = zzii.U(i17, (n0) unsafe2.getObject(t, j3), p(i14));
                        i15 += F;
                    }
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 18:
                    F = u0.U(i17, (List) unsafe2.getObject(t, j3), false);
                    i15 += F;
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 19:
                    i3 = 0;
                    R = u0.R(i17, (List) unsafe2.getObject(t, j3), false);
                    i15 += R;
                    i2 = 1;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 20:
                    i3 = 0;
                    R = u0.d(i17, (List) unsafe2.getObject(t, j3), false);
                    i15 += R;
                    i2 = 1;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 21:
                    i3 = 0;
                    R = u0.t(i17, (List) unsafe2.getObject(t, j3), false);
                    i15 += R;
                    i2 = 1;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 22:
                    i3 = 0;
                    R = u0.H(i17, (List) unsafe2.getObject(t, j3), false);
                    i15 += R;
                    i2 = 1;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 23:
                    i3 = 0;
                    R = u0.U(i17, (List) unsafe2.getObject(t, j3), false);
                    i15 += R;
                    i2 = 1;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 24:
                    i3 = 0;
                    R = u0.R(i17, (List) unsafe2.getObject(t, j3), false);
                    i15 += R;
                    i2 = 1;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 25:
                    i3 = 0;
                    R = u0.X(i17, (List) unsafe2.getObject(t, j3), false);
                    i15 += R;
                    i2 = 1;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 26:
                    F = u0.b(i17, (List) unsafe2.getObject(t, j3));
                    i15 += F;
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 27:
                    F = u0.c(i17, (List) unsafe2.getObject(t, j3), p(i14));
                    i15 += F;
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 28:
                    F = u0.r(i17, (List) unsafe2.getObject(t, j3));
                    i15 += F;
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 29:
                    F = u0.L(i17, (List) unsafe2.getObject(t, j3), false);
                    i15 += F;
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 30:
                    i3 = 0;
                    R = u0.D(i17, (List) unsafe2.getObject(t, j3), false);
                    i15 += R;
                    i2 = 1;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 31:
                    i3 = 0;
                    R = u0.R(i17, (List) unsafe2.getObject(t, j3), false);
                    i15 += R;
                    i2 = 1;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 32:
                    i3 = 0;
                    R = u0.U(i17, (List) unsafe2.getObject(t, j3), false);
                    i15 += R;
                    i2 = 1;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 33:
                    i3 = 0;
                    R = u0.O(i17, (List) unsafe2.getObject(t, j3), false);
                    i15 += R;
                    i2 = 1;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 34:
                    i3 = 0;
                    R = u0.z(i17, (List) unsafe2.getObject(t, j3), false);
                    i15 += R;
                    i2 = 1;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 35:
                    V = u0.V((List) unsafe2.getObject(t, j3));
                    if (V > 0) {
                        g0 = zzii.g0(i17);
                        o0 = zzii.o0(V);
                        A0 = g0 + o0 + V;
                        i15 += A0;
                    }
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 36:
                    V = u0.S((List) unsafe2.getObject(t, j3));
                    if (V > 0) {
                        g0 = zzii.g0(i17);
                        o0 = zzii.o0(V);
                        A0 = g0 + o0 + V;
                        i15 += A0;
                    }
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 37:
                    V = u0.e((List) unsafe2.getObject(t, j3));
                    if (V > 0) {
                        g0 = zzii.g0(i17);
                        o0 = zzii.o0(V);
                        A0 = g0 + o0 + V;
                        i15 += A0;
                    }
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 38:
                    V = u0.u((List) unsafe2.getObject(t, j3));
                    if (V > 0) {
                        g0 = zzii.g0(i17);
                        o0 = zzii.o0(V);
                        A0 = g0 + o0 + V;
                        i15 += A0;
                    }
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 39:
                    V = u0.I((List) unsafe2.getObject(t, j3));
                    if (V > 0) {
                        g0 = zzii.g0(i17);
                        o0 = zzii.o0(V);
                        A0 = g0 + o0 + V;
                        i15 += A0;
                    }
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 40:
                    V = u0.V((List) unsafe2.getObject(t, j3));
                    if (V > 0) {
                        g0 = zzii.g0(i17);
                        o0 = zzii.o0(V);
                        A0 = g0 + o0 + V;
                        i15 += A0;
                    }
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 41:
                    V = u0.S((List) unsafe2.getObject(t, j3));
                    if (V > 0) {
                        g0 = zzii.g0(i17);
                        o0 = zzii.o0(V);
                        A0 = g0 + o0 + V;
                        i15 += A0;
                    }
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 42:
                    V = u0.Y((List) unsafe2.getObject(t, j3));
                    if (V > 0) {
                        g0 = zzii.g0(i17);
                        o0 = zzii.o0(V);
                        A0 = g0 + o0 + V;
                        i15 += A0;
                    }
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 43:
                    V = u0.M((List) unsafe2.getObject(t, j3));
                    if (V > 0) {
                        g0 = zzii.g0(i17);
                        o0 = zzii.o0(V);
                        A0 = g0 + o0 + V;
                        i15 += A0;
                    }
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 44:
                    V = u0.E((List) unsafe2.getObject(t, j3));
                    if (V > 0) {
                        g0 = zzii.g0(i17);
                        o0 = zzii.o0(V);
                        A0 = g0 + o0 + V;
                        i15 += A0;
                    }
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 45:
                    V = u0.S((List) unsafe2.getObject(t, j3));
                    if (V > 0) {
                        g0 = zzii.g0(i17);
                        o0 = zzii.o0(V);
                        A0 = g0 + o0 + V;
                        i15 += A0;
                    }
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 46:
                    V = u0.V((List) unsafe2.getObject(t, j3));
                    if (V > 0) {
                        g0 = zzii.g0(i17);
                        o0 = zzii.o0(V);
                        A0 = g0 + o0 + V;
                        i15 += A0;
                    }
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 47:
                    V = u0.P((List) unsafe2.getObject(t, j3));
                    if (V > 0) {
                        g0 = zzii.g0(i17);
                        o0 = zzii.o0(V);
                        A0 = g0 + o0 + V;
                        i15 += A0;
                    }
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 48:
                    V = u0.A((List) unsafe2.getObject(t, j3));
                    if (V > 0) {
                        g0 = zzii.g0(i17);
                        o0 = zzii.o0(V);
                        A0 = g0 + o0 + V;
                        i15 += A0;
                    }
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 49:
                    F = u0.s(i17, (List) unsafe2.getObject(t, j3), p(i14));
                    i15 += F;
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 50:
                    F = this.o.h(i17, unsafe2.getObject(t, j3), E(i14));
                    i15 += F;
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 51:
                    if (z(t, i17, i14)) {
                        F = zzii.B(i17, Utils.DOUBLE_EPSILON);
                        i15 += F;
                    }
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 52:
                    if (z(t, i17, i14)) {
                        A0 = zzii.C(i17, Utils.FLOAT_EPSILON);
                        i15 += A0;
                    }
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 53:
                    if (z(t, i17, i14)) {
                        F = zzii.b0(i17, P(t, j3));
                        i15 += F;
                    }
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 54:
                    if (z(t, i17, i14)) {
                        F = zzii.h0(i17, P(t, j3));
                        i15 += F;
                    }
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 55:
                    if (z(t, i17, i14)) {
                        F = zzii.l0(i17, N(t, j3));
                        i15 += F;
                    }
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 56:
                    if (z(t, i17, i14)) {
                        F = zzii.q0(i17, 0L);
                        i15 += F;
                    }
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 57:
                    if (z(t, i17, i14)) {
                        A0 = zzii.x0(i17, 0);
                        i15 += A0;
                    }
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 58:
                    if (z(t, i17, i14)) {
                        A0 = zzii.H(i17, true);
                        i15 += A0;
                    }
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 59:
                    if (z(t, i17, i14)) {
                        Object object2 = unsafe2.getObject(t, j3);
                        if (object2 instanceof zzht) {
                            F = zzii.T(i17, (zzht) object2);
                        } else {
                            F = zzii.F(i17, (String) object2);
                        }
                        i15 += F;
                    }
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 60:
                    if (z(t, i17, i14)) {
                        F = u0.a(i17, unsafe2.getObject(t, j3), p(i14));
                        i15 += F;
                    }
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 61:
                    if (z(t, i17, i14)) {
                        F = zzii.T(i17, (zzht) unsafe2.getObject(t, j3));
                        i15 += F;
                    }
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 62:
                    if (z(t, i17, i14)) {
                        F = zzii.p0(i17, N(t, j3));
                        i15 += F;
                    }
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 63:
                    if (z(t, i17, i14)) {
                        F = zzii.C0(i17, N(t, j3));
                        i15 += F;
                    }
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 64:
                    if (z(t, i17, i14)) {
                        A0 = zzii.A0(i17, 0);
                        i15 += A0;
                    }
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 65:
                    if (z(t, i17, i14)) {
                        F = zzii.u0(i17, 0L);
                        i15 += F;
                    }
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 66:
                    if (z(t, i17, i14)) {
                        F = zzii.t0(i17, N(t, j3));
                        i15 += F;
                    }
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 67:
                    if (z(t, i17, i14)) {
                        F = zzii.m0(i17, P(t, j3));
                        i15 += F;
                    }
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                case 68:
                    if (z(t, i17, i14)) {
                        F = zzii.U(i17, (n0) unsafe2.getObject(t, j3), p(i14));
                        i15 += F;
                    }
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
                default:
                    i2 = 1;
                    i3 = 0;
                    j = 0;
                    i14 += 3;
                    i6 = i2;
                    i7 = i3;
                    i5 = 1048575;
            }
            i14 += 3;
            i6 = i2;
            i7 = i3;
            i5 = 1048575;
        }
        int i21 = i7;
        int j4 = i15 + j(this.m, t);
        if (this.f) {
            k0<?> b = this.n.b(t);
            for (int i22 = i21; i22 < b.a.j(); i22++) {
                Map.Entry<?, Object> h = b.a.h(i22);
                i21 += k0.m((pr5) h.getKey(), h.getValue());
            }
            for (Map.Entry<?, Object> entry : b.a.m()) {
                i21 += k0.m((pr5) entry.getKey(), entry.getValue());
            }
            return j4 + i21;
        }
        return j4;
    }

    /* JADX WARN: Code restructure failed: missing block: B:103:0x01bf, code lost:
        if (java.lang.Double.doubleToLongBits(com.google.android.gms.internal.vision.z0.C(r10, r6)) == java.lang.Double.doubleToLongBits(com.google.android.gms.internal.vision.z0.C(r11, r6))) goto L85;
     */
    /* JADX WARN: Code restructure failed: missing block: B:11:0x0038, code lost:
        if (com.google.android.gms.internal.vision.u0.q(com.google.android.gms.internal.vision.z0.F(r10, r6), com.google.android.gms.internal.vision.z0.F(r11, r6)) != false) goto L85;
     */
    /* JADX WARN: Code restructure failed: missing block: B:18:0x006a, code lost:
        if (com.google.android.gms.internal.vision.u0.q(com.google.android.gms.internal.vision.z0.F(r10, r6), com.google.android.gms.internal.vision.z0.F(r11, r6)) != false) goto L85;
     */
    /* JADX WARN: Code restructure failed: missing block: B:23:0x007e, code lost:
        if (com.google.android.gms.internal.vision.z0.o(r10, r6) == com.google.android.gms.internal.vision.z0.o(r11, r6)) goto L85;
     */
    /* JADX WARN: Code restructure failed: missing block: B:28:0x0090, code lost:
        if (com.google.android.gms.internal.vision.z0.b(r10, r6) == com.google.android.gms.internal.vision.z0.b(r11, r6)) goto L85;
     */
    /* JADX WARN: Code restructure failed: missing block: B:33:0x00a4, code lost:
        if (com.google.android.gms.internal.vision.z0.o(r10, r6) == com.google.android.gms.internal.vision.z0.o(r11, r6)) goto L85;
     */
    /* JADX WARN: Code restructure failed: missing block: B:38:0x00b6, code lost:
        if (com.google.android.gms.internal.vision.z0.b(r10, r6) == com.google.android.gms.internal.vision.z0.b(r11, r6)) goto L85;
     */
    /* JADX WARN: Code restructure failed: missing block: B:43:0x00c8, code lost:
        if (com.google.android.gms.internal.vision.z0.b(r10, r6) == com.google.android.gms.internal.vision.z0.b(r11, r6)) goto L85;
     */
    /* JADX WARN: Code restructure failed: missing block: B:48:0x00da, code lost:
        if (com.google.android.gms.internal.vision.z0.b(r10, r6) == com.google.android.gms.internal.vision.z0.b(r11, r6)) goto L85;
     */
    /* JADX WARN: Code restructure failed: missing block: B:53:0x00f0, code lost:
        if (com.google.android.gms.internal.vision.u0.q(com.google.android.gms.internal.vision.z0.F(r10, r6), com.google.android.gms.internal.vision.z0.F(r11, r6)) != false) goto L85;
     */
    /* JADX WARN: Code restructure failed: missing block: B:58:0x0106, code lost:
        if (com.google.android.gms.internal.vision.u0.q(com.google.android.gms.internal.vision.z0.F(r10, r6), com.google.android.gms.internal.vision.z0.F(r11, r6)) != false) goto L85;
     */
    /* JADX WARN: Code restructure failed: missing block: B:63:0x011c, code lost:
        if (com.google.android.gms.internal.vision.u0.q(com.google.android.gms.internal.vision.z0.F(r10, r6), com.google.android.gms.internal.vision.z0.F(r11, r6)) != false) goto L85;
     */
    /* JADX WARN: Code restructure failed: missing block: B:68:0x012e, code lost:
        if (com.google.android.gms.internal.vision.z0.w(r10, r6) == com.google.android.gms.internal.vision.z0.w(r11, r6)) goto L85;
     */
    /* JADX WARN: Code restructure failed: missing block: B:73:0x0140, code lost:
        if (com.google.android.gms.internal.vision.z0.b(r10, r6) == com.google.android.gms.internal.vision.z0.b(r11, r6)) goto L85;
     */
    /* JADX WARN: Code restructure failed: missing block: B:78:0x0154, code lost:
        if (com.google.android.gms.internal.vision.z0.o(r10, r6) == com.google.android.gms.internal.vision.z0.o(r11, r6)) goto L85;
     */
    /* JADX WARN: Code restructure failed: missing block: B:83:0x0165, code lost:
        if (com.google.android.gms.internal.vision.z0.b(r10, r6) == com.google.android.gms.internal.vision.z0.b(r11, r6)) goto L85;
     */
    /* JADX WARN: Code restructure failed: missing block: B:88:0x0178, code lost:
        if (com.google.android.gms.internal.vision.z0.o(r10, r6) == com.google.android.gms.internal.vision.z0.o(r11, r6)) goto L85;
     */
    /* JADX WARN: Code restructure failed: missing block: B:93:0x018b, code lost:
        if (com.google.android.gms.internal.vision.z0.o(r10, r6) == com.google.android.gms.internal.vision.z0.o(r11, r6)) goto L85;
     */
    /* JADX WARN: Code restructure failed: missing block: B:98:0x01a4, code lost:
        if (java.lang.Float.floatToIntBits(com.google.android.gms.internal.vision.z0.x(r10, r6)) == java.lang.Float.floatToIntBits(com.google.android.gms.internal.vision.z0.x(r11, r6))) goto L85;
     */
    @Override // com.google.android.gms.internal.vision.t0
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final boolean g(T r10, T r11) {
        /*
            Method dump skipped, instructions count: 640
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.vision.o0.g(java.lang.Object, java.lang.Object):boolean");
    }

    /* JADX WARN: Code restructure failed: missing block: B:101:0x02d1, code lost:
        if (r0 == r5) goto L136;
     */
    /* JADX WARN: Code restructure failed: missing block: B:103:0x02d5, code lost:
        r15 = r30;
        r14 = r31;
        r12 = r32;
        r13 = r34;
        r11 = r35;
        r2 = r18;
        r10 = r20;
        r1 = r25;
        r6 = r27;
        r7 = r28;
     */
    /* JADX WARN: Code restructure failed: missing block: B:109:0x031a, code lost:
        if (r0 == r15) goto L136;
     */
    /* JADX WARN: Code restructure failed: missing block: B:114:0x033d, code lost:
        if (r0 == r15) goto L136;
     */
    /* JADX WARN: Code restructure failed: missing block: B:115:0x033f, code lost:
        r2 = r0;
     */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r3v11, types: [int] */
    @Override // com.google.android.gms.internal.vision.t0
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void h(T r31, byte[] r32, int r33, int r34, defpackage.go5 r35) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 956
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.vision.o0.h(java.lang.Object, byte[], int, int, go5):void");
    }

    /* JADX WARN: Removed duplicated region for block: B:12:0x003b  */
    /* JADX WARN: Removed duplicated region for block: B:166:0x0513  */
    /* JADX WARN: Removed duplicated region for block: B:181:0x0552  */
    /* JADX WARN: Removed duplicated region for block: B:335:0x0a2a  */
    @Override // com.google.android.gms.internal.vision.t0
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void i(T r14, com.google.android.gms.internal.vision.g1 r15) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 2916
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.vision.o0.i(java.lang.Object, com.google.android.gms.internal.vision.g1):void");
    }

    public final int k(T t, byte[] bArr, int i, int i2, int i3, int i4, int i5, int i6, int i7, long j, int i8, go5 go5Var) throws IOException {
        int k;
        Unsafe unsafe = q;
        long j2 = this.a[i8 + 2] & 1048575;
        switch (i7) {
            case 51:
                if (i5 == 1) {
                    unsafe.putObject(t, j, Double.valueOf(c0.m(bArr, i)));
                    k = i + 8;
                    unsafe.putInt(t, j2, i4);
                    return k;
                }
                return i;
            case 52:
                if (i5 == 5) {
                    unsafe.putObject(t, j, Float.valueOf(c0.o(bArr, i)));
                    k = i + 4;
                    unsafe.putInt(t, j2, i4);
                    return k;
                }
                return i;
            case 53:
            case 54:
                if (i5 == 0) {
                    k = c0.k(bArr, i, go5Var);
                    unsafe.putObject(t, j, Long.valueOf(go5Var.b));
                    unsafe.putInt(t, j2, i4);
                    return k;
                }
                return i;
            case 55:
            case 62:
                if (i5 == 0) {
                    k = c0.i(bArr, i, go5Var);
                    unsafe.putObject(t, j, Integer.valueOf(go5Var.a));
                    unsafe.putInt(t, j2, i4);
                    return k;
                }
                return i;
            case 56:
            case 65:
                if (i5 == 1) {
                    unsafe.putObject(t, j, Long.valueOf(c0.l(bArr, i)));
                    k = i + 8;
                    unsafe.putInt(t, j2, i4);
                    return k;
                }
                return i;
            case 57:
            case 64:
                if (i5 == 5) {
                    unsafe.putObject(t, j, Integer.valueOf(c0.h(bArr, i)));
                    k = i + 4;
                    unsafe.putInt(t, j2, i4);
                    return k;
                }
                return i;
            case 58:
                if (i5 == 0) {
                    k = c0.k(bArr, i, go5Var);
                    unsafe.putObject(t, j, Boolean.valueOf(go5Var.b != 0));
                    unsafe.putInt(t, j2, i4);
                    return k;
                }
                return i;
            case 59:
                if (i5 == 2) {
                    int i9 = c0.i(bArr, i, go5Var);
                    int i10 = go5Var.a;
                    if (i10 == 0) {
                        unsafe.putObject(t, j, "");
                    } else if ((i6 & 536870912) != 0 && !b1.g(bArr, i9, i9 + i10)) {
                        throw zzjk.zzh();
                    } else {
                        unsafe.putObject(t, j, new String(bArr, i9, i10, vs5.a));
                        i9 += i10;
                    }
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                return i;
            case 60:
                if (i5 == 2) {
                    int g = c0.g(p(i8), bArr, i, i2, go5Var);
                    Object object = unsafe.getInt(t, j2) == i4 ? unsafe.getObject(t, j) : null;
                    if (object == null) {
                        unsafe.putObject(t, j, go5Var.c);
                    } else {
                        unsafe.putObject(t, j, vs5.e(object, go5Var.c));
                    }
                    unsafe.putInt(t, j2, i4);
                    return g;
                }
                return i;
            case 61:
                if (i5 == 2) {
                    k = c0.q(bArr, i, go5Var);
                    unsafe.putObject(t, j, go5Var.c);
                    unsafe.putInt(t, j2, i4);
                    return k;
                }
                return i;
            case 63:
                if (i5 == 0) {
                    int i11 = c0.i(bArr, i, go5Var);
                    int i12 = go5Var.a;
                    ws5 K = K(i8);
                    if (K != null && !K.d(i12)) {
                        Q(t).c(i3, Long.valueOf(i12));
                        return i11;
                    }
                    unsafe.putObject(t, j, Integer.valueOf(i12));
                    k = i11;
                    unsafe.putInt(t, j2, i4);
                    return k;
                }
                return i;
            case 66:
                if (i5 == 0) {
                    k = c0.i(bArr, i, go5Var);
                    unsafe.putObject(t, j, Integer.valueOf(e0.d(go5Var.a)));
                    unsafe.putInt(t, j2, i4);
                    return k;
                }
                return i;
            case 67:
                if (i5 == 0) {
                    k = c0.k(bArr, i, go5Var);
                    unsafe.putObject(t, j, Long.valueOf(e0.a(go5Var.b)));
                    unsafe.putInt(t, j2, i4);
                    return k;
                }
                return i;
            case 68:
                if (i5 == 3) {
                    k = c0.f(p(i8), bArr, i, i2, (i3 & (-8)) | 4, go5Var);
                    Object object2 = unsafe.getInt(t, j2) == i4 ? unsafe.getObject(t, j) : null;
                    if (object2 == null) {
                        unsafe.putObject(t, j, go5Var.c);
                    } else {
                        unsafe.putObject(t, j, vs5.e(object2, go5Var.c));
                    }
                    unsafe.putInt(t, j2, i4);
                    return k;
                }
                return i;
            default:
                return i;
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:117:0x0236  */
    /* JADX WARN: Removed duplicated region for block: B:68:0x016e  */
    /* JADX WARN: Removed duplicated region for block: B:97:0x01e8  */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:115:0x0233 -> B:116:0x0234). Please submit an issue!!! */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:66:0x016b -> B:67:0x016c). Please submit an issue!!! */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:95:0x01e5 -> B:96:0x01e6). Please submit an issue!!! */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final int l(T r16, byte[] r17, int r18, int r19, int r20, int r21, int r22, int r23, long r24, int r26, long r27, defpackage.go5 r29) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 1126
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.vision.o0.l(java.lang.Object, byte[], int, int, int, int, int, int, long, int, long, go5):int");
    }

    public final <K, V> int m(T t, byte[] bArr, int i, int i2, int i3, long j, go5 go5Var) throws IOException {
        Unsafe unsafe = q;
        Object E = E(i3);
        Object object = unsafe.getObject(t, j);
        if (this.o.e(object)) {
            Object c = this.o.c(E);
            this.o.g(c, object);
            unsafe.putObject(t, j, c);
            object = c;
        }
        this.o.f(E);
        this.o.b(object);
        int i4 = c0.i(bArr, i, go5Var);
        int i5 = go5Var.a;
        if (i5 >= 0 && i5 <= i2 - i4) {
            throw null;
        }
        throw zzjk.zza();
    }

    public final int n(T t, byte[] bArr, int i, int i2, int i3, go5 go5Var) throws IOException {
        Unsafe unsafe;
        int i4;
        int i5;
        T t2;
        o0<T> o0Var;
        int i6;
        int i7;
        byte b;
        int S;
        int i8;
        int i9;
        int i10;
        int i11;
        int i12;
        int i13;
        boolean z;
        T t3;
        byte[] bArr2;
        go5 go5Var2;
        int i14;
        int i15;
        Object obj;
        Object e;
        long j;
        int i16;
        int i17;
        int i18;
        int i19;
        boolean z2;
        int k;
        int q2;
        T t4;
        int i20;
        int i21;
        o0<T> o0Var2 = this;
        T t5 = t;
        byte[] bArr3 = bArr;
        int i22 = i2;
        int i23 = i3;
        go5 go5Var3 = go5Var;
        Unsafe unsafe2 = q;
        int i24 = i;
        int i25 = 0;
        int i26 = 0;
        int i27 = 0;
        int i28 = -1;
        int i29 = 1048575;
        while (true) {
            Object obj2 = null;
            if (i24 < i22) {
                int i30 = i24 + 1;
                byte b2 = bArr3[i24];
                if (b2 < 0) {
                    int d = c0.d(b2, bArr3, i30, go5Var3);
                    b = go5Var3.a;
                    i30 = d;
                } else {
                    b = b2;
                }
                int i31 = b >>> 3;
                int i32 = b & 7;
                if (i31 > i28) {
                    S = o0Var2.d(i31, i25 / 3);
                } else {
                    S = o0Var2.S(i31);
                }
                int i33 = S;
                if (i33 == -1) {
                    i8 = i31;
                    i9 = i30;
                    i10 = b;
                    i11 = i27;
                    unsafe = unsafe2;
                    i12 = i23;
                    i13 = 0;
                    z = true;
                } else {
                    int[] iArr = o0Var2.a;
                    int i34 = iArr[i33 + 1];
                    int i35 = (i34 & 267386880) >>> 20;
                    int i36 = b;
                    long j2 = i34 & 1048575;
                    if (i35 <= 17) {
                        int i37 = iArr[i33 + 2];
                        int i38 = 1 << (i37 >>> 20);
                        int i39 = i37 & 1048575;
                        if (i39 != i29) {
                            if (i29 != 1048575) {
                                long j3 = i29;
                                t4 = t;
                                j = j2;
                                unsafe2.putInt(t4, j3, i27);
                            } else {
                                t4 = t;
                                j = j2;
                            }
                            i27 = unsafe2.getInt(t4, i39);
                            t5 = t4;
                        } else {
                            t5 = t;
                            j = j2;
                            i39 = i29;
                        }
                        int i40 = i27;
                        switch (i35) {
                            case 0:
                                i16 = i31;
                                i17 = i33;
                                i11 = i40;
                                i18 = i39;
                                i19 = i36;
                                long j4 = j;
                                z2 = true;
                                if (i32 == 1) {
                                    z0.f(t5, j4, c0.m(bArr3, i30));
                                    i24 = i30 + 8;
                                    i27 = i11 | i38;
                                    i29 = i18;
                                    i26 = i19;
                                    i25 = i17;
                                    i28 = i16;
                                    i22 = i2;
                                    i23 = i3;
                                    break;
                                } else {
                                    i29 = i18;
                                    i12 = i3;
                                    z = z2;
                                    i9 = i30;
                                    i10 = i19;
                                    unsafe = unsafe2;
                                    i13 = i17;
                                    i8 = i16;
                                    break;
                                }
                            case 1:
                                i16 = i31;
                                i17 = i33;
                                i11 = i40;
                                i18 = i39;
                                i19 = i36;
                                long j5 = j;
                                if (i32 == 5) {
                                    z0.g(t5, j5, c0.o(bArr3, i30));
                                    i24 = i30 + 4;
                                    i27 = i11 | i38;
                                    i29 = i18;
                                    i26 = i19;
                                    i25 = i17;
                                    i28 = i16;
                                    i22 = i2;
                                    i23 = i3;
                                    break;
                                } else {
                                    z2 = true;
                                    i29 = i18;
                                    i12 = i3;
                                    z = z2;
                                    i9 = i30;
                                    i10 = i19;
                                    unsafe = unsafe2;
                                    i13 = i17;
                                    i8 = i16;
                                    break;
                                }
                            case 2:
                            case 3:
                                i16 = i31;
                                i17 = i33;
                                i11 = i40;
                                i18 = i39;
                                i19 = i36;
                                long j6 = j;
                                if (i32 == 0) {
                                    k = c0.k(bArr3, i30, go5Var3);
                                    unsafe2.putLong(t, j6, go5Var3.b);
                                    i27 = i11 | i38;
                                    i29 = i18;
                                    i24 = k;
                                    i26 = i19;
                                    i25 = i17;
                                    i28 = i16;
                                    i22 = i2;
                                    i23 = i3;
                                    break;
                                } else {
                                    z2 = true;
                                    i29 = i18;
                                    i12 = i3;
                                    z = z2;
                                    i9 = i30;
                                    i10 = i19;
                                    unsafe = unsafe2;
                                    i13 = i17;
                                    i8 = i16;
                                    break;
                                }
                            case 4:
                            case 11:
                                i16 = i31;
                                i17 = i33;
                                i11 = i40;
                                i18 = i39;
                                i19 = i36;
                                long j7 = j;
                                if (i32 == 0) {
                                    i24 = c0.i(bArr3, i30, go5Var3);
                                    unsafe2.putInt(t5, j7, go5Var3.a);
                                    i27 = i11 | i38;
                                    i29 = i18;
                                    i26 = i19;
                                    i25 = i17;
                                    i28 = i16;
                                    i22 = i2;
                                    i23 = i3;
                                    break;
                                } else {
                                    z2 = true;
                                    i29 = i18;
                                    i12 = i3;
                                    z = z2;
                                    i9 = i30;
                                    i10 = i19;
                                    unsafe = unsafe2;
                                    i13 = i17;
                                    i8 = i16;
                                    break;
                                }
                            case 5:
                            case 14:
                                i16 = i31;
                                i17 = i33;
                                i11 = i40;
                                i18 = i39;
                                i19 = i36;
                                long j8 = j;
                                if (i32 == 1) {
                                    unsafe2.putLong(t, j8, c0.l(bArr3, i30));
                                    i24 = i30 + 8;
                                    i27 = i11 | i38;
                                    i29 = i18;
                                    i26 = i19;
                                    i25 = i17;
                                    i28 = i16;
                                    i22 = i2;
                                    i23 = i3;
                                    break;
                                } else {
                                    z2 = true;
                                    i29 = i18;
                                    i12 = i3;
                                    z = z2;
                                    i9 = i30;
                                    i10 = i19;
                                    unsafe = unsafe2;
                                    i13 = i17;
                                    i8 = i16;
                                    break;
                                }
                            case 6:
                            case 13:
                                i16 = i31;
                                i17 = i33;
                                i11 = i40;
                                i18 = i39;
                                i19 = i36;
                                long j9 = j;
                                if (i32 == 5) {
                                    unsafe2.putInt(t5, j9, c0.h(bArr3, i30));
                                    i24 = i30 + 4;
                                    i27 = i11 | i38;
                                    i29 = i18;
                                    i26 = i19;
                                    i25 = i17;
                                    i28 = i16;
                                    i22 = i2;
                                    i23 = i3;
                                    break;
                                } else {
                                    z2 = true;
                                    i29 = i18;
                                    i12 = i3;
                                    z = z2;
                                    i9 = i30;
                                    i10 = i19;
                                    unsafe = unsafe2;
                                    i13 = i17;
                                    i8 = i16;
                                    break;
                                }
                            case 7:
                                i16 = i31;
                                i17 = i33;
                                i11 = i40;
                                i18 = i39;
                                i19 = i36;
                                long j10 = j;
                                if (i32 == 0) {
                                    i24 = c0.k(bArr3, i30, go5Var3);
                                    z0.k(t5, j10, go5Var3.b != 0);
                                    i27 = i11 | i38;
                                    i29 = i18;
                                    i26 = i19;
                                    i25 = i17;
                                    i28 = i16;
                                    i22 = i2;
                                    i23 = i3;
                                    break;
                                } else {
                                    z2 = true;
                                    i29 = i18;
                                    i12 = i3;
                                    z = z2;
                                    i9 = i30;
                                    i10 = i19;
                                    unsafe = unsafe2;
                                    i13 = i17;
                                    i8 = i16;
                                    break;
                                }
                            case 8:
                                i16 = i31;
                                i17 = i33;
                                i11 = i40;
                                i18 = i39;
                                i19 = i36;
                                long j11 = j;
                                if (i32 == 2) {
                                    if ((536870912 & i34) == 0) {
                                        i24 = c0.n(bArr3, i30, go5Var3);
                                    } else {
                                        i24 = c0.p(bArr3, i30, go5Var3);
                                    }
                                    unsafe2.putObject(t5, j11, go5Var3.c);
                                    i27 = i11 | i38;
                                    i29 = i18;
                                    i26 = i19;
                                    i25 = i17;
                                    i28 = i16;
                                    i22 = i2;
                                    i23 = i3;
                                    break;
                                } else {
                                    z2 = true;
                                    i29 = i18;
                                    i12 = i3;
                                    z = z2;
                                    i9 = i30;
                                    i10 = i19;
                                    unsafe = unsafe2;
                                    i13 = i17;
                                    i8 = i16;
                                    break;
                                }
                            case 9:
                                i16 = i31;
                                i17 = i33;
                                i18 = i39;
                                i19 = i36;
                                long j12 = j;
                                if (i32 == 2) {
                                    int g = c0.g(o0Var2.p(i17), bArr3, i30, i2, go5Var3);
                                    if ((i40 & i38) == 0) {
                                        unsafe2.putObject(t5, j12, go5Var3.c);
                                    } else {
                                        unsafe2.putObject(t5, j12, vs5.e(unsafe2.getObject(t5, j12), go5Var3.c));
                                    }
                                    int i41 = i40 | i38;
                                    i29 = i18;
                                    i26 = i19;
                                    i28 = i16;
                                    i22 = i2;
                                    i27 = i41;
                                    i24 = g;
                                    i25 = i17;
                                    i23 = i3;
                                    break;
                                } else {
                                    i11 = i40;
                                    z2 = true;
                                    i29 = i18;
                                    i12 = i3;
                                    z = z2;
                                    i9 = i30;
                                    i10 = i19;
                                    unsafe = unsafe2;
                                    i13 = i17;
                                    i8 = i16;
                                    break;
                                }
                            case 10:
                                i16 = i31;
                                i17 = i33;
                                i18 = i39;
                                i19 = i36;
                                long j13 = j;
                                if (i32 == 2) {
                                    q2 = c0.q(bArr3, i30, go5Var3);
                                    unsafe2.putObject(t5, j13, go5Var3.c);
                                    i27 = i40 | i38;
                                    i29 = i18;
                                    i24 = q2;
                                    i26 = i19;
                                    i25 = i17;
                                    i28 = i16;
                                    i22 = i2;
                                    i23 = i3;
                                    break;
                                } else {
                                    i11 = i40;
                                    z2 = true;
                                    i29 = i18;
                                    i12 = i3;
                                    z = z2;
                                    i9 = i30;
                                    i10 = i19;
                                    unsafe = unsafe2;
                                    i13 = i17;
                                    i8 = i16;
                                    break;
                                }
                            case 12:
                                i16 = i31;
                                i17 = i33;
                                i18 = i39;
                                i19 = i36;
                                long j14 = j;
                                if (i32 == 0) {
                                    q2 = c0.i(bArr3, i30, go5Var3);
                                    int i42 = go5Var3.a;
                                    ws5 K = o0Var2.K(i17);
                                    if (K != null && !K.d(i42)) {
                                        Q(t).c(i19, Long.valueOf(i42));
                                        i24 = q2;
                                        i27 = i40;
                                        i26 = i19;
                                        i25 = i17;
                                        i28 = i16;
                                        i29 = i18;
                                        i22 = i2;
                                        i23 = i3;
                                    } else {
                                        unsafe2.putInt(t5, j14, i42);
                                        i27 = i40 | i38;
                                        i29 = i18;
                                        i24 = q2;
                                        i26 = i19;
                                        i25 = i17;
                                        i28 = i16;
                                        i22 = i2;
                                        i23 = i3;
                                        break;
                                    }
                                } else {
                                    i11 = i40;
                                    z2 = true;
                                    i29 = i18;
                                    i12 = i3;
                                    z = z2;
                                    i9 = i30;
                                    i10 = i19;
                                    unsafe = unsafe2;
                                    i13 = i17;
                                    i8 = i16;
                                    break;
                                }
                                break;
                            case 15:
                                i16 = i31;
                                i17 = i33;
                                i18 = i39;
                                i19 = i36;
                                long j15 = j;
                                if (i32 == 0) {
                                    q2 = c0.i(bArr3, i30, go5Var3);
                                    unsafe2.putInt(t5, j15, e0.d(go5Var3.a));
                                    i27 = i40 | i38;
                                    i29 = i18;
                                    i24 = q2;
                                    i26 = i19;
                                    i25 = i17;
                                    i28 = i16;
                                    i22 = i2;
                                    i23 = i3;
                                    break;
                                } else {
                                    i11 = i40;
                                    z2 = true;
                                    i29 = i18;
                                    i12 = i3;
                                    z = z2;
                                    i9 = i30;
                                    i10 = i19;
                                    unsafe = unsafe2;
                                    i13 = i17;
                                    i8 = i16;
                                    break;
                                }
                            case 16:
                                i16 = i31;
                                i17 = i33;
                                long j16 = j;
                                if (i32 == 0) {
                                    k = c0.k(bArr3, i30, go5Var3);
                                    i18 = i39;
                                    i19 = i36;
                                    unsafe2.putLong(t, j16, e0.a(go5Var3.b));
                                    i27 = i40 | i38;
                                    i29 = i18;
                                    i24 = k;
                                    i26 = i19;
                                    i25 = i17;
                                    i28 = i16;
                                    i22 = i2;
                                    i23 = i3;
                                    break;
                                } else {
                                    i18 = i39;
                                    i19 = i36;
                                    i11 = i40;
                                    z2 = true;
                                    i29 = i18;
                                    i12 = i3;
                                    z = z2;
                                    i9 = i30;
                                    i10 = i19;
                                    unsafe = unsafe2;
                                    i13 = i17;
                                    i8 = i16;
                                    break;
                                }
                            case 17:
                                if (i32 == 3) {
                                    i16 = i31;
                                    i17 = i33;
                                    i24 = c0.f(o0Var2.p(i33), bArr, i30, i2, (i31 << 3) | 4, go5Var);
                                    if ((i40 & i38) == 0) {
                                        unsafe2.putObject(t5, j, go5Var3.c);
                                    } else {
                                        long j17 = j;
                                        unsafe2.putObject(t5, j17, vs5.e(unsafe2.getObject(t5, j17), go5Var3.c));
                                    }
                                    i27 = i40 | i38;
                                    i26 = i36;
                                    i29 = i39;
                                    i25 = i17;
                                    i28 = i16;
                                    i22 = i2;
                                    i23 = i3;
                                    break;
                                } else {
                                    i16 = i31;
                                    i17 = i33;
                                    i11 = i40;
                                    i18 = i39;
                                    i19 = i36;
                                    z2 = true;
                                    i29 = i18;
                                    i12 = i3;
                                    z = z2;
                                    i9 = i30;
                                    i10 = i19;
                                    unsafe = unsafe2;
                                    i13 = i17;
                                    i8 = i16;
                                    break;
                                }
                            default:
                                i16 = i31;
                                i17 = i33;
                                i11 = i40;
                                i18 = i39;
                                i19 = i36;
                                z2 = true;
                                i29 = i18;
                                i12 = i3;
                                z = z2;
                                i9 = i30;
                                i10 = i19;
                                unsafe = unsafe2;
                                i13 = i17;
                                i8 = i16;
                                break;
                        }
                    } else {
                        i11 = i27;
                        int i43 = i29;
                        t5 = t;
                        if (i35 != 27) {
                            i13 = i33;
                            if (i35 <= 49) {
                                int i44 = i30;
                                i21 = i36;
                                z = true;
                                unsafe = unsafe2;
                                i12 = i3;
                                i8 = i31;
                                i24 = l(t, bArr, i30, i2, i36, i31, i32, i13, i34, i35, j2, go5Var);
                                if (i24 == i44) {
                                    i9 = i24;
                                } else {
                                    t5 = t;
                                    bArr3 = bArr;
                                    i22 = i2;
                                    go5Var3 = go5Var;
                                    i23 = i12;
                                    i26 = i21;
                                    i29 = i43;
                                    i27 = i11;
                                    i25 = i13;
                                    i28 = i8;
                                    unsafe2 = unsafe;
                                    o0Var2 = this;
                                }
                            } else {
                                i12 = i3;
                                i20 = i30;
                                i21 = i36;
                                unsafe = unsafe2;
                                i8 = i31;
                                z = true;
                                if (i35 != 50) {
                                    i24 = k(t, bArr, i20, i2, i21, i8, i32, i34, i35, j2, i13, go5Var);
                                    if (i24 != i20) {
                                        t5 = t;
                                        bArr3 = bArr;
                                        i22 = i2;
                                        go5Var3 = go5Var;
                                        i26 = i21;
                                        i23 = i12;
                                        i29 = i43;
                                        i27 = i11;
                                        i25 = i13;
                                        i28 = i8;
                                        unsafe2 = unsafe;
                                        o0Var2 = this;
                                    }
                                } else if (i32 == 2) {
                                    i24 = m(t, bArr, i20, i2, i13, j2, go5Var);
                                    if (i24 != i20) {
                                        t5 = t;
                                        bArr3 = bArr;
                                        i22 = i2;
                                        go5Var3 = go5Var;
                                        i23 = i12;
                                        i26 = i21;
                                        i29 = i43;
                                        i27 = i11;
                                        i25 = i13;
                                        i28 = i8;
                                        unsafe2 = unsafe;
                                        o0Var2 = this;
                                    }
                                } else {
                                    i9 = i20;
                                }
                                i9 = i24;
                            }
                        } else if (i32 == 2) {
                            gt5 gt5Var = (gt5) unsafe2.getObject(t5, j2);
                            if (!gt5Var.zza()) {
                                int size = gt5Var.size();
                                gt5Var = gt5Var.d(size == 0 ? 10 : size << 1);
                                unsafe2.putObject(t5, j2, gt5Var);
                            }
                            i24 = c0.e(o0Var2.p(i33), i36, bArr, i30, i2, gt5Var, go5Var);
                            i23 = i3;
                            i26 = i36;
                            i28 = i31;
                            i29 = i43;
                            i27 = i11;
                            i25 = i33;
                            i22 = i2;
                        } else {
                            i13 = i33;
                            i12 = i3;
                            i20 = i30;
                            i21 = i36;
                            unsafe = unsafe2;
                            i8 = i31;
                            z = true;
                            i9 = i20;
                        }
                        i10 = i21;
                        i29 = i43;
                    }
                }
                if (i10 != i12 || i12 == 0) {
                    int i45 = i12;
                    if (this.f) {
                        go5Var2 = go5Var;
                        if (go5Var2.d != h0.b()) {
                            int i46 = i8;
                            l0.d a = go5Var2.d.a(this.e, i46);
                            if (a == null) {
                                i24 = c0.a(i10, bArr, i9, i2, Q(t), go5Var);
                                t3 = t;
                                i14 = i29;
                                i8 = i46;
                                bArr2 = bArr;
                                i15 = i2;
                            } else {
                                t3 = t;
                                l0.c cVar = (l0.c) t3;
                                cVar.x();
                                k0<l0.e> k0Var = cVar.zzc;
                                l0.e eVar = a.b;
                                boolean z3 = eVar.g0;
                                zzml zzmlVar = eVar.f0;
                                if (zzmlVar != zzml.zzn) {
                                    int[] iArr2 = zn5.a;
                                    switch (iArr2[zzmlVar.ordinal()]) {
                                        case 1:
                                            i14 = i29;
                                            i8 = i46;
                                            bArr2 = bArr;
                                            i15 = i2;
                                            obj2 = Double.valueOf(c0.m(bArr2, i9));
                                            i9 += 8;
                                            obj = obj2;
                                            break;
                                        case 2:
                                            i14 = i29;
                                            i8 = i46;
                                            bArr2 = bArr;
                                            i15 = i2;
                                            obj2 = Float.valueOf(c0.o(bArr2, i9));
                                            i9 += 4;
                                            obj = obj2;
                                            break;
                                        case 3:
                                        case 4:
                                            i14 = i29;
                                            i8 = i46;
                                            bArr2 = bArr;
                                            i15 = i2;
                                            i9 = c0.k(bArr2, i9, go5Var2);
                                            obj2 = Long.valueOf(go5Var2.b);
                                            obj = obj2;
                                            break;
                                        case 5:
                                        case 6:
                                            i14 = i29;
                                            i8 = i46;
                                            bArr2 = bArr;
                                            i15 = i2;
                                            i9 = c0.i(bArr2, i9, go5Var2);
                                            obj2 = Integer.valueOf(go5Var2.a);
                                            obj = obj2;
                                            break;
                                        case 7:
                                        case 8:
                                            i14 = i29;
                                            i8 = i46;
                                            bArr2 = bArr;
                                            i15 = i2;
                                            obj2 = Long.valueOf(c0.l(bArr2, i9));
                                            i9 += 8;
                                            obj = obj2;
                                            break;
                                        case 9:
                                        case 10:
                                            i14 = i29;
                                            i8 = i46;
                                            bArr2 = bArr;
                                            i15 = i2;
                                            obj2 = Integer.valueOf(c0.h(bArr2, i9));
                                            i9 += 4;
                                            obj = obj2;
                                            break;
                                        case 11:
                                            i14 = i29;
                                            i8 = i46;
                                            bArr2 = bArr;
                                            i15 = i2;
                                            i9 = c0.k(bArr2, i9, go5Var2);
                                            if (go5Var2.b == 0) {
                                                z = false;
                                            }
                                            obj2 = Boolean.valueOf(z);
                                            obj = obj2;
                                            break;
                                        case 12:
                                            i14 = i29;
                                            i8 = i46;
                                            bArr2 = bArr;
                                            i15 = i2;
                                            i9 = c0.i(bArr2, i9, go5Var2);
                                            obj2 = Integer.valueOf(e0.d(go5Var2.a));
                                            obj = obj2;
                                            break;
                                        case 13:
                                            i14 = i29;
                                            i8 = i46;
                                            bArr2 = bArr;
                                            i15 = i2;
                                            i9 = c0.k(bArr2, i9, go5Var2);
                                            obj2 = Long.valueOf(e0.a(go5Var2.b));
                                            obj = obj2;
                                            break;
                                        case 14:
                                            throw new IllegalStateException("Shouldn't reach here.");
                                        case 15:
                                            i14 = i29;
                                            i8 = i46;
                                            bArr2 = bArr;
                                            i15 = i2;
                                            i9 = c0.q(bArr2, i9, go5Var2);
                                            obj = go5Var2.c;
                                            break;
                                        case 16:
                                            i14 = i29;
                                            i8 = i46;
                                            bArr2 = bArr;
                                            i15 = i2;
                                            i9 = c0.n(bArr2, i9, go5Var2);
                                            obj = go5Var2.c;
                                            break;
                                        case 17:
                                            int i47 = (i46 << 3) | 4;
                                            i14 = i29;
                                            i15 = i2;
                                            i8 = i46;
                                            bArr2 = bArr;
                                            i9 = c0.f(zw5.c().a(a.a.getClass()), bArr, i9, i2, i47, go5Var);
                                            obj = go5Var2.c;
                                            break;
                                        case 18:
                                            i9 = c0.g(zw5.c().a(a.a.getClass()), bArr, i9, i2, go5Var2);
                                            obj = go5Var2.c;
                                            i14 = i29;
                                            i8 = i46;
                                            i15 = i2;
                                            bArr2 = bArr;
                                            break;
                                        default:
                                            i14 = i29;
                                            i8 = i46;
                                            bArr2 = bArr;
                                            i15 = i2;
                                            obj = obj2;
                                            break;
                                    }
                                    l0.e eVar2 = a.b;
                                    if (eVar2.g0) {
                                        k0Var.k(eVar2, obj);
                                    } else {
                                        int i48 = iArr2[eVar2.f0.ordinal()];
                                        if ((i48 == 17 || i48 == 18) && (e = k0Var.e(a.b)) != null) {
                                            obj = vs5.e(e, obj);
                                        }
                                        k0Var.g(a.b, obj);
                                    }
                                    i24 = i9;
                                } else {
                                    c0.i(bArr, i9, go5Var2);
                                    throw null;
                                }
                            }
                            i26 = i10;
                            o0Var2 = this;
                            bArr3 = bArr2;
                            t5 = t3;
                            i27 = i11;
                            i25 = i13;
                            i28 = i8;
                            i22 = i15;
                            i23 = i45;
                            go5Var3 = go5Var2;
                            unsafe2 = unsafe;
                            i29 = i14;
                        } else {
                            t3 = t;
                            bArr2 = bArr;
                        }
                    } else {
                        t3 = t;
                        bArr2 = bArr;
                        go5Var2 = go5Var;
                    }
                    i14 = i29;
                    i15 = i2;
                    i24 = c0.a(i10, bArr, i9, i2, Q(t), go5Var);
                    i26 = i10;
                    o0Var2 = this;
                    bArr3 = bArr2;
                    t5 = t3;
                    i27 = i11;
                    i25 = i13;
                    i28 = i8;
                    i22 = i15;
                    i23 = i45;
                    go5Var3 = go5Var2;
                    unsafe2 = unsafe;
                    i29 = i14;
                } else {
                    o0Var = this;
                    t2 = t;
                    i24 = i9;
                    i6 = i29;
                    i26 = i10;
                    i4 = i12;
                    i27 = i11;
                    i7 = 1048575;
                    i5 = i2;
                }
            } else {
                int i49 = i29;
                unsafe = unsafe2;
                i4 = i23;
                i5 = i22;
                t2 = t5;
                o0Var = o0Var2;
                i6 = i49;
                i7 = 1048575;
            }
        }
        if (i6 != i7) {
            unsafe.putInt(t2, i6, i27);
        }
        x0 x0Var = null;
        for (int i50 = o0Var.i; i50 < o0Var.j; i50++) {
            x0Var = (x0) o0Var.r(t2, o0Var.h[i50], x0Var, o0Var.m);
        }
        if (x0Var != null) {
            o0Var.m.h(t2, x0Var);
        }
        if (i4 == 0) {
            if (i24 != i5) {
                throw zzjk.zzg();
            }
        } else if (i24 > i5 || i26 != i4) {
            throw zzjk.zzg();
        }
        return i24;
    }

    public final t0 p(int i) {
        int i2 = (i / 3) << 1;
        t0 t0Var = (t0) this.b[i2];
        if (t0Var != null) {
            return t0Var;
        }
        t0<T> a = zw5.c().a((Class) this.b[i2 + 1]);
        this.b[i2] = a;
        return a;
    }

    public final <K, V, UT, UB> UB q(int i, int i2, Map<K, V> map, ws5 ws5Var, UB ub, v0<UT, UB> v0Var) {
        lv5<?, ?> f = this.o.f(E(i));
        Iterator<Map.Entry<K, V>> it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<K, V> next = it.next();
            if (!ws5Var.d(((Integer) next.getValue()).intValue())) {
                if (ub == null) {
                    ub = v0Var.a();
                }
                wp5 zzc = zzht.zzc(m0.a(f, next.getKey(), next.getValue()));
                try {
                    m0.b(zzc.b(), f, next.getKey(), next.getValue());
                    v0Var.c(ub, i2, zzc.a());
                    it.remove();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        return ub;
    }

    public final <UT, UB> UB r(Object obj, int i, UB ub, v0<UT, UB> v0Var) {
        ws5 K;
        int i2 = this.a[i];
        Object F = z0.F(obj, M(i) & 1048575);
        return (F == null || (K = K(i)) == null) ? ub : (UB) q(i, i2, this.o.b(F), K, ub, v0Var);
    }

    public final <K, V> void w(g1 g1Var, int i, Object obj, int i2) throws IOException {
        if (obj != null) {
            g1Var.K(i, this.o.f(E(i2)), this.o.a(obj));
        }
    }

    public final void x(T t, T t2, int i) {
        long M = M(i) & 1048575;
        if (y(t2, i)) {
            Object F = z0.F(t, M);
            Object F2 = z0.F(t2, M);
            if (F != null && F2 != null) {
                z0.j(t, M, vs5.e(F, F2));
                F(t, i);
            } else if (F2 != null) {
                z0.j(t, M, F2);
                F(t, i);
            }
        }
    }

    public final boolean y(T t, int i) {
        int O = O(i);
        long j = O & 1048575;
        if (j != 1048575) {
            return (z0.b(t, j) & (1 << (O >>> 20))) != 0;
        }
        int M = M(i);
        long j2 = M & 1048575;
        switch ((M & 267386880) >>> 20) {
            case 0:
                return z0.C(t, j2) != Utils.DOUBLE_EPSILON;
            case 1:
                return z0.x(t, j2) != Utils.FLOAT_EPSILON;
            case 2:
                return z0.o(t, j2) != 0;
            case 3:
                return z0.o(t, j2) != 0;
            case 4:
                return z0.b(t, j2) != 0;
            case 5:
                return z0.o(t, j2) != 0;
            case 6:
                return z0.b(t, j2) != 0;
            case 7:
                return z0.w(t, j2);
            case 8:
                Object F = z0.F(t, j2);
                if (F instanceof String) {
                    return !((String) F).isEmpty();
                } else if (F instanceof zzht) {
                    return !zzht.zza.equals(F);
                } else {
                    throw new IllegalArgumentException();
                }
            case 9:
                return z0.F(t, j2) != null;
            case 10:
                return !zzht.zza.equals(z0.F(t, j2));
            case 11:
                return z0.b(t, j2) != 0;
            case 12:
                return z0.b(t, j2) != 0;
            case 13:
                return z0.b(t, j2) != 0;
            case 14:
                return z0.o(t, j2) != 0;
            case 15:
                return z0.b(t, j2) != 0;
            case 16:
                return z0.o(t, j2) != 0;
            case 17:
                return z0.F(t, j2) != null;
            default:
                throw new IllegalArgumentException();
        }
    }

    public final boolean z(T t, int i, int i2) {
        return z0.b(t, (long) (O(i2) & 1048575)) == i;
    }

    @Override // com.google.android.gms.internal.vision.t0
    public final T zza() {
        return (T) this.k.b(this.e);
    }
}
