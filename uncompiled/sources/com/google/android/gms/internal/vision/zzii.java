package com.google.android.gms.internal.vision;

import java.io.IOException;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public abstract class zzii extends d0 {
    public static final Logger b = Logger.getLogger(zzii.class.getName());
    public static final boolean c = z0.m();
    public g0 a;

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* loaded from: classes.dex */
    public static class a extends zzii {
        public final byte[] d;
        public final int e;
        public int f;

        public a(byte[] bArr, int i, int i2) {
            super();
            Objects.requireNonNull(bArr, "buffer");
            if ((i2 | 0 | (bArr.length - i2)) >= 0) {
                this.d = bArr;
                this.f = 0;
                this.e = i2;
                return;
            }
            throw new IllegalArgumentException(String.format("Array range is invalid. Buffer.length=%d, offset=%d, length=%d", Integer.valueOf(bArr.length), 0, Integer.valueOf(i2)));
        }

        public final void F0(byte[] bArr, int i, int i2) throws IOException {
            try {
                System.arraycopy(bArr, i, this.d, this.f, i2);
                this.f += i2;
            } catch (IndexOutOfBoundsException e) {
                throw new zzb(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.f), Integer.valueOf(this.e), Integer.valueOf(i2)), e);
            }
        }

        @Override // com.google.android.gms.internal.vision.zzii
        public final void O(int i) throws IOException {
            if (!zzii.c || tn5.b() || b() < 5) {
                while ((i & (-128)) != 0) {
                    try {
                        byte[] bArr = this.d;
                        int i2 = this.f;
                        this.f = i2 + 1;
                        bArr[i2] = (byte) ((i & 127) | 128);
                        i >>>= 7;
                    } catch (IndexOutOfBoundsException e) {
                        throw new zzb(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.f), Integer.valueOf(this.e), 1), e);
                    }
                }
                byte[] bArr2 = this.d;
                int i3 = this.f;
                this.f = i3 + 1;
                bArr2[i3] = (byte) i;
            } else if ((i & (-128)) == 0) {
                byte[] bArr3 = this.d;
                int i4 = this.f;
                this.f = i4 + 1;
                z0.l(bArr3, i4, (byte) i);
            } else {
                byte[] bArr4 = this.d;
                int i5 = this.f;
                this.f = i5 + 1;
                z0.l(bArr4, i5, (byte) (i | 128));
                int i6 = i >>> 7;
                if ((i6 & (-128)) == 0) {
                    byte[] bArr5 = this.d;
                    int i7 = this.f;
                    this.f = i7 + 1;
                    z0.l(bArr5, i7, (byte) i6);
                    return;
                }
                byte[] bArr6 = this.d;
                int i8 = this.f;
                this.f = i8 + 1;
                z0.l(bArr6, i8, (byte) (i6 | 128));
                int i9 = i6 >>> 7;
                if ((i9 & (-128)) == 0) {
                    byte[] bArr7 = this.d;
                    int i10 = this.f;
                    this.f = i10 + 1;
                    z0.l(bArr7, i10, (byte) i9);
                    return;
                }
                byte[] bArr8 = this.d;
                int i11 = this.f;
                this.f = i11 + 1;
                z0.l(bArr8, i11, (byte) (i9 | 128));
                int i12 = i9 >>> 7;
                if ((i12 & (-128)) == 0) {
                    byte[] bArr9 = this.d;
                    int i13 = this.f;
                    this.f = i13 + 1;
                    z0.l(bArr9, i13, (byte) i12);
                    return;
                }
                byte[] bArr10 = this.d;
                int i14 = this.f;
                this.f = i14 + 1;
                z0.l(bArr10, i14, (byte) (i12 | 128));
                byte[] bArr11 = this.d;
                int i15 = this.f;
                this.f = i15 + 1;
                z0.l(bArr11, i15, (byte) (i12 >>> 7));
            }
        }

        @Override // com.google.android.gms.internal.vision.zzii
        public final void P(int i, int i2) throws IOException {
            m(i, 0);
            j(i2);
        }

        @Override // com.google.android.gms.internal.vision.zzii
        public final void R(int i, zzht zzhtVar) throws IOException {
            m(1, 3);
            X(2, i);
            o(3, zzhtVar);
            m(1, 4);
        }

        @Override // com.google.android.gms.internal.vision.zzii
        public final void X(int i, int i2) throws IOException {
            m(i, 0);
            O(i2);
        }

        @Override // com.google.android.gms.internal.vision.zzii
        public final void Y(int i, long j) throws IOException {
            m(i, 1);
            Z(j);
        }

        @Override // com.google.android.gms.internal.vision.zzii
        public final void Z(long j) throws IOException {
            try {
                byte[] bArr = this.d;
                int i = this.f;
                int i2 = i + 1;
                this.f = i2;
                bArr[i] = (byte) j;
                int i3 = i2 + 1;
                this.f = i3;
                bArr[i2] = (byte) (j >> 8);
                int i4 = i3 + 1;
                this.f = i4;
                bArr[i3] = (byte) (j >> 16);
                int i5 = i4 + 1;
                this.f = i5;
                bArr[i4] = (byte) (j >> 24);
                int i6 = i5 + 1;
                this.f = i6;
                bArr[i5] = (byte) (j >> 32);
                int i7 = i6 + 1;
                this.f = i7;
                bArr[i6] = (byte) (j >> 40);
                int i8 = i7 + 1;
                this.f = i8;
                bArr[i7] = (byte) (j >> 48);
                this.f = i8 + 1;
                bArr[i8] = (byte) (j >> 56);
            } catch (IndexOutOfBoundsException e) {
                throw new zzb(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.f), Integer.valueOf(this.e), 1), e);
            }
        }

        @Override // com.google.android.gms.internal.vision.d0
        public final void a(byte[] bArr, int i, int i2) throws IOException {
            F0(bArr, i, i2);
        }

        @Override // com.google.android.gms.internal.vision.zzii
        public final int b() {
            return this.e - this.f;
        }

        @Override // com.google.android.gms.internal.vision.zzii
        public final void e0(int i) throws IOException {
            try {
                byte[] bArr = this.d;
                int i2 = this.f;
                int i3 = i2 + 1;
                this.f = i3;
                bArr[i2] = (byte) i;
                int i4 = i3 + 1;
                this.f = i4;
                bArr[i3] = (byte) (i >> 8);
                int i5 = i4 + 1;
                this.f = i5;
                bArr[i4] = (byte) (i >> 16);
                this.f = i5 + 1;
                bArr[i5] = (byte) (i >>> 24);
            } catch (IndexOutOfBoundsException e) {
                throw new zzb(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.f), Integer.valueOf(this.e), 1), e);
            }
        }

        @Override // com.google.android.gms.internal.vision.zzii
        public final void g(byte b) throws IOException {
            try {
                byte[] bArr = this.d;
                int i = this.f;
                this.f = i + 1;
                bArr[i] = b;
            } catch (IndexOutOfBoundsException e) {
                throw new zzb(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.f), Integer.valueOf(this.e), 1), e);
            }
        }

        @Override // com.google.android.gms.internal.vision.zzii
        public final void j(int i) throws IOException {
            if (i >= 0) {
                O(i);
            } else {
                t(i);
            }
        }

        @Override // com.google.android.gms.internal.vision.zzii
        public final void j0(int i, int i2) throws IOException {
            m(i, 5);
            e0(i2);
        }

        @Override // com.google.android.gms.internal.vision.zzii
        public final void m(int i, int i2) throws IOException {
            O((i << 3) | i2);
        }

        @Override // com.google.android.gms.internal.vision.zzii
        public final void n(int i, long j) throws IOException {
            m(i, 0);
            t(j);
        }

        @Override // com.google.android.gms.internal.vision.zzii
        public final void o(int i, zzht zzhtVar) throws IOException {
            m(i, 2);
            u(zzhtVar);
        }

        @Override // com.google.android.gms.internal.vision.zzii
        public final void p(int i, n0 n0Var) throws IOException {
            m(1, 3);
            X(2, i);
            m(3, 2);
            v(n0Var);
            m(1, 4);
        }

        @Override // com.google.android.gms.internal.vision.zzii
        public final void q(int i, n0 n0Var, t0 t0Var) throws IOException {
            m(i, 2);
            a0 a0Var = (a0) n0Var;
            int m = a0Var.m();
            if (m == -1) {
                m = t0Var.f(a0Var);
                a0Var.f(m);
            }
            O(m);
            t0Var.i(n0Var, this.a);
        }

        @Override // com.google.android.gms.internal.vision.zzii
        public final void r(int i, String str) throws IOException {
            m(i, 2);
            w(str);
        }

        @Override // com.google.android.gms.internal.vision.zzii
        public final void s(int i, boolean z) throws IOException {
            m(i, 0);
            g(z ? (byte) 1 : (byte) 0);
        }

        @Override // com.google.android.gms.internal.vision.zzii
        public final void t(long j) throws IOException {
            if (zzii.c && b() >= 10) {
                while ((j & (-128)) != 0) {
                    byte[] bArr = this.d;
                    int i = this.f;
                    this.f = i + 1;
                    z0.l(bArr, i, (byte) ((((int) j) & 127) | 128));
                    j >>>= 7;
                }
                byte[] bArr2 = this.d;
                int i2 = this.f;
                this.f = i2 + 1;
                z0.l(bArr2, i2, (byte) j);
                return;
            }
            while ((j & (-128)) != 0) {
                try {
                    byte[] bArr3 = this.d;
                    int i3 = this.f;
                    this.f = i3 + 1;
                    bArr3[i3] = (byte) ((((int) j) & 127) | 128);
                    j >>>= 7;
                } catch (IndexOutOfBoundsException e) {
                    throw new zzb(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.f), Integer.valueOf(this.e), 1), e);
                }
            }
            byte[] bArr4 = this.d;
            int i4 = this.f;
            this.f = i4 + 1;
            bArr4[i4] = (byte) j;
        }

        @Override // com.google.android.gms.internal.vision.zzii
        public final void u(zzht zzhtVar) throws IOException {
            O(zzhtVar.zza());
            zzhtVar.zza(this);
        }

        @Override // com.google.android.gms.internal.vision.zzii
        public final void v(n0 n0Var) throws IOException {
            O(n0Var.k());
            n0Var.l(this);
        }

        @Override // com.google.android.gms.internal.vision.zzii
        public final void w(String str) throws IOException {
            int i = this.f;
            try {
                int o0 = zzii.o0(str.length() * 3);
                int o02 = zzii.o0(str.length());
                if (o02 == o0) {
                    int i2 = i + o02;
                    this.f = i2;
                    int e = b1.e(str, this.d, i2, b());
                    this.f = i;
                    O((e - i) - o02);
                    this.f = e;
                    return;
                }
                O(b1.d(str));
                this.f = b1.e(str, this.d, this.f, b());
            } catch (zzmg e2) {
                this.f = i;
                x(str, e2);
            } catch (IndexOutOfBoundsException e3) {
                throw new zzb(e3);
            }
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* loaded from: classes.dex */
    public static class zzb extends IOException {
        public zzb() {
            super("CodedOutputStream was writing to a flat byte array and ran out of space.");
        }

        public zzb(Throwable th) {
            super("CodedOutputStream was writing to a flat byte array and ran out of space.", th);
        }

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public zzb(java.lang.String r3, java.lang.Throwable r4) {
            /*
                r2 = this;
                java.lang.String r0 = "CodedOutputStream was writing to a flat byte array and ran out of space.: "
                java.lang.String r3 = java.lang.String.valueOf(r3)
                int r1 = r3.length()
                if (r1 == 0) goto L11
                java.lang.String r3 = r0.concat(r3)
                goto L16
            L11:
                java.lang.String r3 = new java.lang.String
                r3.<init>(r0)
            L16:
                r2.<init>(r3, r4)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.vision.zzii.zzb.<init>(java.lang.String, java.lang.Throwable):void");
        }
    }

    public zzii() {
    }

    public static int A(float f) {
        return 4;
    }

    public static int A0(int i, int i2) {
        return o0(i << 3) + 4;
    }

    public static int B(int i, double d) {
        return o0(i << 3) + 8;
    }

    public static int B0(int i) {
        return k0(i);
    }

    public static int C(int i, float f) {
        return o0(i << 3) + 4;
    }

    public static int C0(int i, int i2) {
        return o0(i << 3) + k0(i2);
    }

    public static int D(int i, n0 n0Var) {
        return (o0(8) << 1) + p0(2, i) + o0(24) + J(n0Var);
    }

    @Deprecated
    public static int D0(int i) {
        return o0(i);
    }

    public static int E(int i, n0 n0Var, t0 t0Var) {
        return o0(i << 3) + d(n0Var, t0Var);
    }

    public static int E0(int i) {
        return (i >> 31) ^ (i << 1);
    }

    public static int F(int i, String str) {
        return o0(i << 3) + K(str);
    }

    public static int G(int i, au5 au5Var) {
        return (o0(8) << 1) + p0(2, i) + c(3, au5Var);
    }

    public static int H(int i, boolean z) {
        return o0(i << 3) + 1;
    }

    public static int I(zzht zzhtVar) {
        int zza = zzhtVar.zza();
        return o0(zza) + zza;
    }

    public static int J(n0 n0Var) {
        int k = n0Var.k();
        return o0(k) + k;
    }

    public static int K(String str) {
        int length;
        try {
            length = b1.d(str);
        } catch (zzmg unused) {
            length = str.getBytes(vs5.a).length;
        }
        return o0(length) + length;
    }

    public static int L(boolean z) {
        return 1;
    }

    public static int M(byte[] bArr) {
        int length = bArr.length;
        return o0(length) + length;
    }

    public static int T(int i, zzht zzhtVar) {
        int o0 = o0(i << 3);
        int zza = zzhtVar.zza();
        return o0 + o0(zza) + zza;
    }

    @Deprecated
    public static int U(int i, n0 n0Var, t0 t0Var) {
        int o0 = o0(i << 3) << 1;
        a0 a0Var = (a0) n0Var;
        int m = a0Var.m();
        if (m == -1) {
            m = t0Var.f(a0Var);
            a0Var.f(m);
        }
        return o0 + m;
    }

    @Deprecated
    public static int V(n0 n0Var) {
        return n0Var.k();
    }

    public static int b0(int i, long j) {
        return o0(i << 3) + i0(j);
    }

    public static int c(int i, au5 au5Var) {
        int o0 = o0(i << 3);
        int b2 = au5Var.b();
        return o0 + o0(b2) + b2;
    }

    public static int c0(int i, zzht zzhtVar) {
        return (o0(8) << 1) + p0(2, i) + T(3, zzhtVar);
    }

    public static int d(n0 n0Var, t0 t0Var) {
        a0 a0Var = (a0) n0Var;
        int m = a0Var.m();
        if (m == -1) {
            m = t0Var.f(a0Var);
            a0Var.f(m);
        }
        return o0(m) + m;
    }

    public static int d0(long j) {
        return i0(j);
    }

    public static int e(au5 au5Var) {
        int b2 = au5Var.b();
        return o0(b2) + b2;
    }

    public static zzii f(byte[] bArr) {
        return new a(bArr, 0, bArr.length);
    }

    public static int g0(int i) {
        return o0(i << 3);
    }

    public static int h0(int i, long j) {
        return o0(i << 3) + i0(j);
    }

    public static int i0(long j) {
        int i;
        if (((-128) & j) == 0) {
            return 1;
        }
        if (j < 0) {
            return 10;
        }
        if (((-34359738368L) & j) != 0) {
            i = 6;
            j >>>= 28;
        } else {
            i = 2;
        }
        if (((-2097152) & j) != 0) {
            i += 2;
            j >>>= 14;
        }
        return (j & (-16384)) != 0 ? i + 1 : i;
    }

    public static int k0(int i) {
        if (i >= 0) {
            return o0(i);
        }
        return 10;
    }

    public static int l0(int i, int i2) {
        return o0(i << 3) + k0(i2);
    }

    public static int m0(int i, long j) {
        return o0(i << 3) + i0(y0(j));
    }

    public static int n0(long j) {
        return i0(y0(j));
    }

    public static int o0(int i) {
        if ((i & (-128)) == 0) {
            return 1;
        }
        if ((i & (-16384)) == 0) {
            return 2;
        }
        if (((-2097152) & i) == 0) {
            return 3;
        }
        return (i & (-268435456)) == 0 ? 4 : 5;
    }

    public static int p0(int i, int i2) {
        return o0(i << 3) + o0(i2);
    }

    public static int q0(int i, long j) {
        return o0(i << 3) + 8;
    }

    public static int r0(long j) {
        return 8;
    }

    public static int s0(int i) {
        return o0(E0(i));
    }

    public static int t0(int i, int i2) {
        return o0(i << 3) + o0(E0(i2));
    }

    public static int u0(int i, long j) {
        return o0(i << 3) + 8;
    }

    public static int v0(long j) {
        return 8;
    }

    public static int w0(int i) {
        return 4;
    }

    public static int x0(int i, int i2) {
        return o0(i << 3) + 4;
    }

    public static long y0(long j) {
        return (j >> 63) ^ (j << 1);
    }

    public static int z(double d) {
        return 8;
    }

    public static int z0(int i) {
        return 4;
    }

    public final void N() {
        if (b() != 0) {
            throw new IllegalStateException("Did not write as much data as expected.");
        }
    }

    public abstract void O(int i) throws IOException;

    public abstract void P(int i, int i2) throws IOException;

    public final void Q(int i, long j) throws IOException {
        n(i, y0(j));
    }

    public abstract void R(int i, zzht zzhtVar) throws IOException;

    public final void S(long j) throws IOException {
        t(y0(j));
    }

    public final void W(int i) throws IOException {
        O(E0(i));
    }

    public abstract void X(int i, int i2) throws IOException;

    public abstract void Y(int i, long j) throws IOException;

    public abstract void Z(long j) throws IOException;

    public abstract int b();

    public abstract void e0(int i) throws IOException;

    public final void f0(int i, int i2) throws IOException {
        X(i, E0(i2));
    }

    public abstract void g(byte b2) throws IOException;

    public final void h(double d) throws IOException {
        Z(Double.doubleToRawLongBits(d));
    }

    public final void i(float f) throws IOException {
        e0(Float.floatToRawIntBits(f));
    }

    public abstract void j(int i) throws IOException;

    public abstract void j0(int i, int i2) throws IOException;

    public final void k(int i, double d) throws IOException {
        Y(i, Double.doubleToRawLongBits(d));
    }

    public final void l(int i, float f) throws IOException {
        j0(i, Float.floatToRawIntBits(f));
    }

    public abstract void m(int i, int i2) throws IOException;

    public abstract void n(int i, long j) throws IOException;

    public abstract void o(int i, zzht zzhtVar) throws IOException;

    public abstract void p(int i, n0 n0Var) throws IOException;

    public abstract void q(int i, n0 n0Var, t0 t0Var) throws IOException;

    public abstract void r(int i, String str) throws IOException;

    public abstract void s(int i, boolean z) throws IOException;

    public abstract void t(long j) throws IOException;

    public abstract void u(zzht zzhtVar) throws IOException;

    public abstract void v(n0 n0Var) throws IOException;

    public abstract void w(String str) throws IOException;

    public final void x(String str, zzmg zzmgVar) throws IOException {
        b.logp(Level.WARNING, "com.google.protobuf.CodedOutputStream", "inefficientWriteStringNoTag", "Converting ill-formed UTF-16. Your Protocol Buffer will not round trip correctly!", (Throwable) zzmgVar);
        byte[] bytes = str.getBytes(vs5.a);
        try {
            O(bytes.length);
            a(bytes, 0, bytes.length);
        } catch (zzb e) {
            throw e;
        } catch (IndexOutOfBoundsException e2) {
            throw new zzb(e2);
        }
    }

    public final void y(boolean z) throws IOException {
        g(z ? (byte) 1 : (byte) 0);
    }
}
