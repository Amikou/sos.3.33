package com.google.android.gms.internal.vision;

import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public final class c0 {
    public static int a(int i, byte[] bArr, int i2, int i3, x0 x0Var, go5 go5Var) throws zzjk {
        if ((i >>> 3) != 0) {
            int i4 = i & 7;
            if (i4 == 0) {
                int k = k(bArr, i2, go5Var);
                x0Var.c(i, Long.valueOf(go5Var.b));
                return k;
            } else if (i4 == 1) {
                x0Var.c(i, Long.valueOf(l(bArr, i2)));
                return i2 + 8;
            } else if (i4 == 2) {
                int i5 = i(bArr, i2, go5Var);
                int i6 = go5Var.a;
                if (i6 >= 0) {
                    if (i6 <= bArr.length - i5) {
                        if (i6 == 0) {
                            x0Var.c(i, zzht.zza);
                        } else {
                            x0Var.c(i, zzht.zza(bArr, i5, i6));
                        }
                        return i5 + i6;
                    }
                    throw zzjk.zza();
                }
                throw zzjk.zzb();
            } else if (i4 != 3) {
                if (i4 == 5) {
                    x0Var.c(i, Integer.valueOf(h(bArr, i2)));
                    return i2 + 4;
                }
                throw zzjk.zzd();
            } else {
                x0 g = x0.g();
                int i7 = (i & (-8)) | 4;
                int i8 = 0;
                while (true) {
                    if (i2 >= i3) {
                        break;
                    }
                    int i9 = i(bArr, i2, go5Var);
                    int i10 = go5Var.a;
                    i8 = i10;
                    if (i10 == i7) {
                        i2 = i9;
                        break;
                    }
                    int a = a(i8, bArr, i9, i3, g, go5Var);
                    i8 = i10;
                    i2 = a;
                }
                if (i2 <= i3 && i8 == i7) {
                    x0Var.c(i, g);
                    return i2;
                }
                throw zzjk.zzg();
            }
        }
        throw zzjk.zzd();
    }

    public static int b(int i, byte[] bArr, int i2, int i3, go5 go5Var) throws zzjk {
        if ((i >>> 3) != 0) {
            int i4 = i & 7;
            if (i4 != 0) {
                if (i4 != 1) {
                    if (i4 != 2) {
                        if (i4 != 3) {
                            if (i4 == 5) {
                                return i2 + 4;
                            }
                            throw zzjk.zzd();
                        }
                        int i5 = (i & (-8)) | 4;
                        int i6 = 0;
                        while (i2 < i3) {
                            i2 = i(bArr, i2, go5Var);
                            i6 = go5Var.a;
                            if (i6 == i5) {
                                break;
                            }
                            i2 = b(i6, bArr, i2, i3, go5Var);
                        }
                        if (i2 > i3 || i6 != i5) {
                            throw zzjk.zzg();
                        }
                        return i2;
                    }
                    return i(bArr, i2, go5Var) + go5Var.a;
                }
                return i2 + 8;
            }
            return k(bArr, i2, go5Var);
        }
        throw zzjk.zzd();
    }

    public static int c(int i, byte[] bArr, int i2, int i3, gt5<?> gt5Var, go5 go5Var) {
        qs5 qs5Var = (qs5) gt5Var;
        int i4 = i(bArr, i2, go5Var);
        qs5Var.k(go5Var.a);
        while (i4 < i3) {
            int i5 = i(bArr, i4, go5Var);
            if (i != go5Var.a) {
                break;
            }
            i4 = i(bArr, i5, go5Var);
            qs5Var.k(go5Var.a);
        }
        return i4;
    }

    public static int d(int i, byte[] bArr, int i2, go5 go5Var) {
        int i3 = i & 127;
        int i4 = i2 + 1;
        byte b = bArr[i2];
        if (b >= 0) {
            go5Var.a = i3 | (b << 7);
            return i4;
        }
        int i5 = i3 | ((b & Byte.MAX_VALUE) << 7);
        int i6 = i4 + 1;
        byte b2 = bArr[i4];
        if (b2 >= 0) {
            go5Var.a = i5 | (b2 << 14);
            return i6;
        }
        int i7 = i5 | ((b2 & Byte.MAX_VALUE) << 14);
        int i8 = i6 + 1;
        byte b3 = bArr[i6];
        if (b3 >= 0) {
            go5Var.a = i7 | (b3 << 21);
            return i8;
        }
        int i9 = i7 | ((b3 & Byte.MAX_VALUE) << 21);
        int i10 = i8 + 1;
        byte b4 = bArr[i8];
        if (b4 >= 0) {
            go5Var.a = i9 | (b4 << 28);
            return i10;
        }
        int i11 = i9 | ((b4 & Byte.MAX_VALUE) << 28);
        while (true) {
            int i12 = i10 + 1;
            if (bArr[i10] >= 0) {
                go5Var.a = i11;
                return i12;
            }
            i10 = i12;
        }
    }

    public static int e(t0<?> t0Var, int i, byte[] bArr, int i2, int i3, gt5<?> gt5Var, go5 go5Var) throws IOException {
        int g = g(t0Var, bArr, i2, i3, go5Var);
        gt5Var.add(go5Var.c);
        while (g < i3) {
            int i4 = i(bArr, g, go5Var);
            if (i != go5Var.a) {
                break;
            }
            g = g(t0Var, bArr, i4, i3, go5Var);
            gt5Var.add(go5Var.c);
        }
        return g;
    }

    public static int f(t0 t0Var, byte[] bArr, int i, int i2, int i3, go5 go5Var) throws IOException {
        o0 o0Var = (o0) t0Var;
        Object zza = o0Var.zza();
        int n = o0Var.n(zza, bArr, i, i2, i3, go5Var);
        o0Var.a(zza);
        go5Var.c = zza;
        return n;
    }

    public static int g(t0 t0Var, byte[] bArr, int i, int i2, go5 go5Var) throws IOException {
        int i3 = i + 1;
        int i4 = bArr[i];
        if (i4 < 0) {
            i3 = d(i4, bArr, i3, go5Var);
            i4 = go5Var.a;
        }
        int i5 = i3;
        if (i4 >= 0 && i4 <= i2 - i5) {
            Object zza = t0Var.zza();
            int i6 = i4 + i5;
            t0Var.h(zza, bArr, i5, i6, go5Var);
            t0Var.a(zza);
            go5Var.c = zza;
            return i6;
        }
        throw zzjk.zza();
    }

    public static int h(byte[] bArr, int i) {
        return ((bArr[i + 3] & 255) << 24) | (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << 16);
    }

    public static int i(byte[] bArr, int i, go5 go5Var) {
        int i2 = i + 1;
        byte b = bArr[i];
        if (b >= 0) {
            go5Var.a = b;
            return i2;
        }
        return d(b, bArr, i2, go5Var);
    }

    public static int j(byte[] bArr, int i, gt5<?> gt5Var, go5 go5Var) throws IOException {
        qs5 qs5Var = (qs5) gt5Var;
        int i2 = i(bArr, i, go5Var);
        int i3 = go5Var.a + i2;
        while (i2 < i3) {
            i2 = i(bArr, i2, go5Var);
            qs5Var.k(go5Var.a);
        }
        if (i2 == i3) {
            return i2;
        }
        throw zzjk.zza();
    }

    public static int k(byte[] bArr, int i, go5 go5Var) {
        byte b;
        int i2 = i + 1;
        long j = bArr[i];
        if (j >= 0) {
            go5Var.b = j;
            return i2;
        }
        int i3 = i2 + 1;
        byte b2 = bArr[i2];
        long j2 = (j & 127) | ((b2 & Byte.MAX_VALUE) << 7);
        int i4 = 7;
        while (b2 < 0) {
            int i5 = i3 + 1;
            i4 += 7;
            j2 |= (b & Byte.MAX_VALUE) << i4;
            b2 = bArr[i3];
            i3 = i5;
        }
        go5Var.b = j2;
        return i3;
    }

    public static long l(byte[] bArr, int i) {
        return ((bArr[i + 7] & 255) << 56) | (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << 16) | ((bArr[i + 3] & 255) << 24) | ((bArr[i + 4] & 255) << 32) | ((bArr[i + 5] & 255) << 40) | ((bArr[i + 6] & 255) << 48);
    }

    public static double m(byte[] bArr, int i) {
        return Double.longBitsToDouble(l(bArr, i));
    }

    public static int n(byte[] bArr, int i, go5 go5Var) throws zzjk {
        int i2 = i(bArr, i, go5Var);
        int i3 = go5Var.a;
        if (i3 >= 0) {
            if (i3 == 0) {
                go5Var.c = "";
                return i2;
            }
            go5Var.c = new String(bArr, i2, i3, vs5.a);
            return i2 + i3;
        }
        throw zzjk.zzb();
    }

    public static float o(byte[] bArr, int i) {
        return Float.intBitsToFloat(h(bArr, i));
    }

    public static int p(byte[] bArr, int i, go5 go5Var) throws zzjk {
        int i2 = i(bArr, i, go5Var);
        int i3 = go5Var.a;
        if (i3 >= 0) {
            if (i3 == 0) {
                go5Var.c = "";
                return i2;
            }
            go5Var.c = b1.k(bArr, i2, i3);
            return i2 + i3;
        }
        throw zzjk.zzb();
    }

    public static int q(byte[] bArr, int i, go5 go5Var) throws zzjk {
        int i2 = i(bArr, i, go5Var);
        int i3 = go5Var.a;
        if (i3 >= 0) {
            if (i3 <= bArr.length - i2) {
                if (i3 == 0) {
                    go5Var.c = zzht.zza;
                    return i2;
                }
                go5Var.c = zzht.zza(bArr, i2, i3);
                return i2 + i3;
            }
            throw zzjk.zza();
        }
        throw zzjk.zzb();
    }
}
