package com.google.android.gms.internal.vision;

import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public interface t0<T> {
    void a(T t);

    int b(T t);

    void c(T t, T t2);

    boolean e(T t);

    int f(T t);

    boolean g(T t, T t2);

    void h(T t, byte[] bArr, int i, int i2, go5 go5Var) throws IOException;

    void i(T t, g1 g1Var) throws IOException;

    T zza();
}
