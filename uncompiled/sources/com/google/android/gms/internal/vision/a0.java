package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.vision.a0;
import com.google.android.gms.internal.vision.z;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public abstract class a0<MessageType extends a0<MessageType, BuilderType>, BuilderType extends z<MessageType, BuilderType>> implements n0 {
    public int zza = 0;

    public static <T> void b(Iterable<T> iterable, List<? super T> list) {
        vs5.d(iterable);
        if (iterable instanceof gu5) {
            List<?> b = ((gu5) iterable).b();
            gu5 gu5Var = (gu5) list;
            int size = list.size();
            for (Object obj : b) {
                if (obj == null) {
                    StringBuilder sb = new StringBuilder(37);
                    sb.append("Element at index ");
                    sb.append(gu5Var.size() - size);
                    sb.append(" is null.");
                    String sb2 = sb.toString();
                    for (int size2 = gu5Var.size() - 1; size2 >= size; size2--) {
                        gu5Var.remove(size2);
                    }
                    throw new NullPointerException(sb2);
                } else if (obj instanceof zzht) {
                    gu5Var.f1((zzht) obj);
                } else {
                    gu5Var.add((String) obj);
                }
            }
        } else if (iterable instanceof vw5) {
            list.addAll((Collection) iterable);
        } else {
            if ((list instanceof ArrayList) && (iterable instanceof Collection)) {
                ((ArrayList) list).ensureCapacity(list.size() + ((Collection) iterable).size());
            }
            int size3 = list.size();
            for (T t : iterable) {
                if (t == null) {
                    StringBuilder sb3 = new StringBuilder(37);
                    sb3.append("Element at index ");
                    sb3.append(list.size() - size3);
                    sb3.append(" is null.");
                    String sb4 = sb3.toString();
                    for (int size4 = list.size() - 1; size4 >= size3; size4--) {
                        list.remove(size4);
                    }
                    throw new NullPointerException(sb4);
                }
                list.add(t);
            }
        }
    }

    public void f(int i) {
        throw new UnsupportedOperationException();
    }

    public final byte[] g() {
        try {
            byte[] bArr = new byte[k()];
            zzii f = zzii.f(bArr);
            l(f);
            f.N();
            return bArr;
        } catch (IOException e) {
            String name = getClass().getName();
            StringBuilder sb = new StringBuilder(name.length() + 62 + "byte array".length());
            sb.append("Serializing ");
            sb.append(name);
            sb.append(" to a ");
            sb.append("byte array");
            sb.append(" threw an IOException (should never happen).");
            throw new RuntimeException(sb.toString(), e);
        }
    }

    @Override // com.google.android.gms.internal.vision.n0
    public final zzht h() {
        try {
            wp5 zzc = zzht.zzc(k());
            l(zzc.b());
            return zzc.a();
        } catch (IOException e) {
            String name = getClass().getName();
            StringBuilder sb = new StringBuilder(name.length() + 62 + "ByteString".length());
            sb.append("Serializing ");
            sb.append(name);
            sb.append(" to a ");
            sb.append("ByteString");
            sb.append(" threw an IOException (should never happen).");
            throw new RuntimeException(sb.toString(), e);
        }
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    public int m() {
        throw new UnsupportedOperationException();
    }
}
