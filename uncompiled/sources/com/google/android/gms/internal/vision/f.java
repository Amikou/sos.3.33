package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.vision.l0;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public final class f extends l0<f, a> implements ew5 {
    private static final f zzj;
    private static volatile xw5<f> zzk;
    private int zzc;
    private float zzd;
    private float zze;
    private float zzf;
    private float zzg;
    private float zzh;
    private float zzi;

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* loaded from: classes.dex */
    public static final class a extends l0.b<f, a> implements ew5 {
        public a() {
            super(f.zzj);
        }

        public /* synthetic */ a(m mVar) {
            this();
        }
    }

    static {
        f fVar = new f();
        zzj = fVar;
        l0.s(f.class, fVar);
    }

    /* JADX WARN: Type inference failed for: r2v14, types: [xw5<com.google.android.gms.internal.vision.f>, com.google.android.gms.internal.vision.l0$a] */
    @Override // com.google.android.gms.internal.vision.l0
    public final Object o(int i, Object obj, Object obj2) {
        xw5<f> xw5Var;
        switch (m.a[i - 1]) {
            case 1:
                return new f();
            case 2:
                return new a(null);
            case 3:
                return l0.p(zzj, "\u0001\u0006\u0000\u0001\u0001\u0006\u0006\u0000\u0000\u0000\u0001ခ\u0000\u0002ခ\u0001\u0003ခ\u0002\u0004ခ\u0003\u0005ခ\u0004\u0006ခ\u0005", new Object[]{"zzc", "zzd", "zze", "zzf", "zzg", "zzh", "zzi"});
            case 4:
                return zzj;
            case 5:
                xw5<f> xw5Var2 = zzk;
                xw5<f> xw5Var3 = xw5Var2;
                if (xw5Var2 == null) {
                    synchronized (f.class) {
                        xw5<f> xw5Var4 = zzk;
                        xw5Var = xw5Var4;
                        if (xw5Var4 == null) {
                            ?? aVar = new l0.a(zzj);
                            zzk = aVar;
                            xw5Var = aVar;
                        }
                    }
                    xw5Var3 = xw5Var;
                }
                return xw5Var3;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
