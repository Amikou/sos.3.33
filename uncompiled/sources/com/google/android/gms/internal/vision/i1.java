package com.google.android.gms.internal.vision;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.vision.barcode.Barcode;

/* compiled from: com.google.android.gms:play-services-vision@@20.1.3 */
/* loaded from: classes.dex */
public final class i1 extends a implements s0 {
    public i1(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.vision.barcode.internal.client.INativeBarcodeDetector");
    }

    @Override // com.google.android.gms.internal.vision.s0
    public final Barcode[] Z(lm1 lm1Var, zzs zzsVar) throws RemoteException {
        Parcel b = b();
        oc5.a(b, lm1Var);
        oc5.b(b, zzsVar);
        Parcel F1 = F1(2, b);
        Barcode[] barcodeArr = (Barcode[]) F1.createTypedArray(Barcode.CREATOR);
        F1.recycle();
        return barcodeArr;
    }

    @Override // com.google.android.gms.internal.vision.s0
    public final Barcode[] h(lm1 lm1Var, zzs zzsVar) throws RemoteException {
        Parcel b = b();
        oc5.a(b, lm1Var);
        oc5.b(b, zzsVar);
        Parcel F1 = F1(1, b);
        Barcode[] barcodeArr = (Barcode[]) F1.createTypedArray(Barcode.CREATOR);
        F1.recycle();
        return barcodeArr;
    }

    @Override // com.google.android.gms.internal.vision.s0
    public final void zza() throws RemoteException {
        G1(3, b());
    }
}
