package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.vision.l0;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public final class zzfi$zze extends l0<zzfi$zze, a> implements ew5 {
    private static final zzfi$zze zzl;
    private static volatile xw5<zzfi$zze> zzm;
    private int zzc;
    private boolean zze;
    private int zzf;
    private long zzg;
    private long zzh;
    private long zzi;
    private boolean zzk;
    private String zzd = "";
    private String zzj = "";

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* loaded from: classes.dex */
    public static final class a extends l0.b<zzfi$zze, a> implements ew5 {
        public a() {
            super(zzfi$zze.zzl);
        }

        public /* synthetic */ a(m mVar) {
            this();
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* loaded from: classes.dex */
    public enum zzb implements rs5 {
        REASON_UNKNOWN(0),
        REASON_MISSING(1),
        REASON_UPGRADE(2),
        REASON_INVALID(3);
        
        private final int zzf;

        static {
            new n();
        }

        zzb(int i) {
            this.zzf = i;
        }

        public static ws5 zzb() {
            return o.a;
        }

        @Override // java.lang.Enum
        public final String toString() {
            return "<" + zzb.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.zzf + " name=" + name() + '>';
        }

        @Override // defpackage.rs5
        public final int zza() {
            return this.zzf;
        }

        public static zzb zza(int i) {
            if (i != 0) {
                if (i != 1) {
                    if (i != 2) {
                        if (i != 3) {
                            return null;
                        }
                        return REASON_INVALID;
                    }
                    return REASON_UPGRADE;
                }
                return REASON_MISSING;
            }
            return REASON_UNKNOWN;
        }
    }

    static {
        zzfi$zze zzfi_zze = new zzfi$zze();
        zzl = zzfi_zze;
        l0.s(zzfi$zze.class, zzfi_zze);
    }

    /* JADX WARN: Type inference failed for: r2v14, types: [xw5<com.google.android.gms.internal.vision.zzfi$zze>, com.google.android.gms.internal.vision.l0$a] */
    @Override // com.google.android.gms.internal.vision.l0
    public final Object o(int i, Object obj, Object obj2) {
        xw5<zzfi$zze> xw5Var;
        switch (m.a[i - 1]) {
            case 1:
                return new zzfi$zze();
            case 2:
                return new a(null);
            case 3:
                return l0.p(zzl, "\u0001\b\u0000\u0001\u0001\b\b\u0000\u0000\u0000\u0001ဈ\u0000\u0002ဇ\u0001\u0003ဌ\u0002\u0004ဂ\u0003\u0005ဂ\u0004\u0006ဂ\u0005\u0007ဈ\u0006\bဇ\u0007", new Object[]{"zzc", "zzd", "zze", "zzf", zzb.zzb(), "zzg", "zzh", "zzi", "zzj", "zzk"});
            case 4:
                return zzl;
            case 5:
                xw5<zzfi$zze> xw5Var2 = zzm;
                xw5<zzfi$zze> xw5Var3 = xw5Var2;
                if (xw5Var2 == null) {
                    synchronized (zzfi$zze.class) {
                        xw5<zzfi$zze> xw5Var4 = zzm;
                        xw5Var = xw5Var4;
                        if (xw5Var4 == null) {
                            ?? aVar = new l0.a(zzl);
                            zzm = aVar;
                            xw5Var = aVar;
                        }
                    }
                    xw5Var3 = xw5Var;
                }
                return xw5Var3;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
