package com.google.android.gms.internal.vision;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public abstract class b0 implements ow5 {
    @Override // defpackage.ow5
    /* renamed from: zza */
    public final ow5 clone() {
        throw new UnsupportedOperationException("clone() should be implemented by subclasses.");
    }
}
