package com.google.android.gms.internal.vision;

/* JADX WARN: Enum visitor error
jadx.core.utils.exceptions.JadxRuntimeException: Init of enum a uses external variables
	at jadx.core.dex.visitors.EnumVisitor.createEnumFieldByConstructor(EnumVisitor.java:444)
	at jadx.core.dex.visitors.EnumVisitor.processEnumFieldByRegister(EnumVisitor.java:391)
	at jadx.core.dex.visitors.EnumVisitor.extractEnumFieldsFromFilledArray(EnumVisitor.java:320)
	at jadx.core.dex.visitors.EnumVisitor.extractEnumFieldsFromInsn(EnumVisitor.java:258)
	at jadx.core.dex.visitors.EnumVisitor.convertToEnum(EnumVisitor.java:151)
	at jadx.core.dex.visitors.EnumVisitor.visit(EnumVisitor.java:100)
 */
/* JADX WARN: Failed to restore enum class, 'enum' modifier and super class removed */
/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public final class zziv {
    public static final zziv A0;
    public static final zziv B0;
    public static final zziv C0;
    public static final zziv D0;
    public static final zziv E0;
    public static final zziv F0;
    public static final zziv G0;
    public static final zziv H0;
    public static final zziv I0;
    public static final zziv J0;
    public static final zziv K0;
    public static final zziv L0;
    public static final zziv M0;
    public static final zziv N0;
    public static final zziv O0;
    public static final zziv P0;
    public static final zziv Q0;
    public static final zziv R0;
    public static final zziv S0;
    public static final zziv T0;
    public static final zziv U0;
    public static final zziv V0;
    public static final zziv W0;
    public static final zziv X0;
    public static final zziv Y0;
    public static final zziv Z0;
    public static final zziv a;
    public static final zziv a1;
    public static final zziv[] b1;
    public static final /* synthetic */ zziv[] c1;
    public static final zziv f0;
    public static final zziv g0;
    public static final zziv h0;
    public static final zziv i0;
    public static final zziv j0;
    public static final zziv k0;
    public static final zziv l0;
    public static final zziv m0;
    public static final zziv n0;
    public static final zziv o0;
    public static final zziv p0;
    public static final zziv q0;
    public static final zziv r0;
    public static final zziv s0;
    public static final zziv t0;
    public static final zziv u0;
    public static final zziv v0;
    public static final zziv w0;
    public static final zziv x0;
    public static final zziv y0;
    public static final zziv z0;
    public static final zziv zza;
    public static final zziv zzb;
    private final zzjm zzaz;
    private final int zzba;
    private final zzix zzbb;
    private final Class<?> zzbc;
    private final boolean zzbd;

    static {
        zzix zzixVar = zzix.SCALAR;
        zzjm zzjmVar = zzjm.zze;
        zziv zzivVar = new zziv("DOUBLE", 0, 0, zzixVar, zzjmVar);
        a = zzivVar;
        zzjm zzjmVar2 = zzjm.zzd;
        zziv zzivVar2 = new zziv("FLOAT", 1, 1, zzixVar, zzjmVar2);
        f0 = zzivVar2;
        zzjm zzjmVar3 = zzjm.zzc;
        zziv zzivVar3 = new zziv("INT64", 2, 2, zzixVar, zzjmVar3);
        g0 = zzivVar3;
        zziv zzivVar4 = new zziv("UINT64", 3, 3, zzixVar, zzjmVar3);
        h0 = zzivVar4;
        zzjm zzjmVar4 = zzjm.zzb;
        zziv zzivVar5 = new zziv("INT32", 4, 4, zzixVar, zzjmVar4);
        i0 = zzivVar5;
        zziv zzivVar6 = new zziv("FIXED64", 5, 5, zzixVar, zzjmVar3);
        j0 = zzivVar6;
        zziv zzivVar7 = new zziv("FIXED32", 6, 6, zzixVar, zzjmVar4);
        k0 = zzivVar7;
        zzjm zzjmVar5 = zzjm.zzf;
        zziv zzivVar8 = new zziv("BOOL", 7, 7, zzixVar, zzjmVar5);
        l0 = zzivVar8;
        zzjm zzjmVar6 = zzjm.zzg;
        zziv zzivVar9 = new zziv("STRING", 8, 8, zzixVar, zzjmVar6);
        m0 = zzivVar9;
        zzjm zzjmVar7 = zzjm.zzj;
        zziv zzivVar10 = new zziv("MESSAGE", 9, 9, zzixVar, zzjmVar7);
        n0 = zzivVar10;
        zzjm zzjmVar8 = zzjm.zzh;
        zziv zzivVar11 = new zziv("BYTES", 10, 10, zzixVar, zzjmVar8);
        o0 = zzivVar11;
        zziv zzivVar12 = new zziv("UINT32", 11, 11, zzixVar, zzjmVar4);
        p0 = zzivVar12;
        zzjm zzjmVar9 = zzjm.zzi;
        zziv zzivVar13 = new zziv("ENUM", 12, 12, zzixVar, zzjmVar9);
        q0 = zzivVar13;
        zziv zzivVar14 = new zziv("SFIXED32", 13, 13, zzixVar, zzjmVar4);
        r0 = zzivVar14;
        zziv zzivVar15 = new zziv("SFIXED64", 14, 14, zzixVar, zzjmVar3);
        s0 = zzivVar15;
        zziv zzivVar16 = new zziv("SINT32", 15, 15, zzixVar, zzjmVar4);
        t0 = zzivVar16;
        zziv zzivVar17 = new zziv("SINT64", 16, 16, zzixVar, zzjmVar3);
        u0 = zzivVar17;
        zziv zzivVar18 = new zziv("GROUP", 17, 17, zzixVar, zzjmVar7);
        v0 = zzivVar18;
        zzix zzixVar2 = zzix.VECTOR;
        zziv zzivVar19 = new zziv("DOUBLE_LIST", 18, 18, zzixVar2, zzjmVar);
        w0 = zzivVar19;
        zziv zzivVar20 = new zziv("FLOAT_LIST", 19, 19, zzixVar2, zzjmVar2);
        x0 = zzivVar20;
        zziv zzivVar21 = new zziv("INT64_LIST", 20, 20, zzixVar2, zzjmVar3);
        y0 = zzivVar21;
        zziv zzivVar22 = new zziv("UINT64_LIST", 21, 21, zzixVar2, zzjmVar3);
        z0 = zzivVar22;
        zziv zzivVar23 = new zziv("INT32_LIST", 22, 22, zzixVar2, zzjmVar4);
        A0 = zzivVar23;
        zziv zzivVar24 = new zziv("FIXED64_LIST", 23, 23, zzixVar2, zzjmVar3);
        B0 = zzivVar24;
        zziv zzivVar25 = new zziv("FIXED32_LIST", 24, 24, zzixVar2, zzjmVar4);
        C0 = zzivVar25;
        zziv zzivVar26 = new zziv("BOOL_LIST", 25, 25, zzixVar2, zzjmVar5);
        D0 = zzivVar26;
        zziv zzivVar27 = new zziv("STRING_LIST", 26, 26, zzixVar2, zzjmVar6);
        E0 = zzivVar27;
        zziv zzivVar28 = new zziv("MESSAGE_LIST", 27, 27, zzixVar2, zzjmVar7);
        F0 = zzivVar28;
        zziv zzivVar29 = new zziv("BYTES_LIST", 28, 28, zzixVar2, zzjmVar8);
        G0 = zzivVar29;
        zziv zzivVar30 = new zziv("UINT32_LIST", 29, 29, zzixVar2, zzjmVar4);
        H0 = zzivVar30;
        zziv zzivVar31 = new zziv("ENUM_LIST", 30, 30, zzixVar2, zzjmVar9);
        I0 = zzivVar31;
        zziv zzivVar32 = new zziv("SFIXED32_LIST", 31, 31, zzixVar2, zzjmVar4);
        J0 = zzivVar32;
        zziv zzivVar33 = new zziv("SFIXED64_LIST", 32, 32, zzixVar2, zzjmVar3);
        K0 = zzivVar33;
        zziv zzivVar34 = new zziv("SINT32_LIST", 33, 33, zzixVar2, zzjmVar4);
        L0 = zzivVar34;
        zziv zzivVar35 = new zziv("SINT64_LIST", 34, 34, zzixVar2, zzjmVar3);
        M0 = zzivVar35;
        zzix zzixVar3 = zzix.PACKED_VECTOR;
        zziv zzivVar36 = new zziv("DOUBLE_LIST_PACKED", 35, 35, zzixVar3, zzjmVar);
        zza = zzivVar36;
        zziv zzivVar37 = new zziv("FLOAT_LIST_PACKED", 36, 36, zzixVar3, zzjmVar2);
        N0 = zzivVar37;
        zziv zzivVar38 = new zziv("INT64_LIST_PACKED", 37, 37, zzixVar3, zzjmVar3);
        O0 = zzivVar38;
        zziv zzivVar39 = new zziv("UINT64_LIST_PACKED", 38, 38, zzixVar3, zzjmVar3);
        P0 = zzivVar39;
        zziv zzivVar40 = new zziv("INT32_LIST_PACKED", 39, 39, zzixVar3, zzjmVar4);
        Q0 = zzivVar40;
        zziv zzivVar41 = new zziv("FIXED64_LIST_PACKED", 40, 40, zzixVar3, zzjmVar3);
        R0 = zzivVar41;
        zziv zzivVar42 = new zziv("FIXED32_LIST_PACKED", 41, 41, zzixVar3, zzjmVar4);
        S0 = zzivVar42;
        zziv zzivVar43 = new zziv("BOOL_LIST_PACKED", 42, 42, zzixVar3, zzjmVar5);
        T0 = zzivVar43;
        zziv zzivVar44 = new zziv("UINT32_LIST_PACKED", 43, 43, zzixVar3, zzjmVar4);
        U0 = zzivVar44;
        zziv zzivVar45 = new zziv("ENUM_LIST_PACKED", 44, 44, zzixVar3, zzjmVar9);
        V0 = zzivVar45;
        zziv zzivVar46 = new zziv("SFIXED32_LIST_PACKED", 45, 45, zzixVar3, zzjmVar4);
        W0 = zzivVar46;
        zziv zzivVar47 = new zziv("SFIXED64_LIST_PACKED", 46, 46, zzixVar3, zzjmVar3);
        X0 = zzivVar47;
        zziv zzivVar48 = new zziv("SINT32_LIST_PACKED", 47, 47, zzixVar3, zzjmVar4);
        Y0 = zzivVar48;
        zziv zzivVar49 = new zziv("SINT64_LIST_PACKED", 48, 48, zzixVar3, zzjmVar3);
        zzb = zzivVar49;
        zziv zzivVar50 = new zziv("GROUP_LIST", 49, 49, zzixVar2, zzjmVar7);
        Z0 = zzivVar50;
        zziv zzivVar51 = new zziv("MAP", 50, 50, zzix.MAP, zzjm.zza);
        a1 = zzivVar51;
        c1 = new zziv[]{zzivVar, zzivVar2, zzivVar3, zzivVar4, zzivVar5, zzivVar6, zzivVar7, zzivVar8, zzivVar9, zzivVar10, zzivVar11, zzivVar12, zzivVar13, zzivVar14, zzivVar15, zzivVar16, zzivVar17, zzivVar18, zzivVar19, zzivVar20, zzivVar21, zzivVar22, zzivVar23, zzivVar24, zzivVar25, zzivVar26, zzivVar27, zzivVar28, zzivVar29, zzivVar30, zzivVar31, zzivVar32, zzivVar33, zzivVar34, zzivVar35, zzivVar36, zzivVar37, zzivVar38, zzivVar39, zzivVar40, zzivVar41, zzivVar42, zzivVar43, zzivVar44, zzivVar45, zzivVar46, zzivVar47, zzivVar48, zzivVar49, zzivVar50, zzivVar51};
        zziv[] values = values();
        b1 = new zziv[values.length];
        for (zziv zzivVar52 : values) {
            b1[zzivVar52.zzba] = zzivVar52;
        }
    }

    public zziv(String str, int i, int i2, zzix zzixVar, zzjm zzjmVar) {
        int i3;
        this.zzba = i2;
        this.zzbb = zzixVar;
        this.zzaz = zzjmVar;
        int i4 = vr5.a[zzixVar.ordinal()];
        boolean z = true;
        if (i4 == 1) {
            this.zzbc = zzjmVar.zza();
        } else if (i4 != 2) {
            this.zzbc = null;
        } else {
            this.zzbc = zzjmVar.zza();
        }
        this.zzbd = (zzixVar != zzix.SCALAR || (i3 = vr5.b[zzjmVar.ordinal()]) == 1 || i3 == 2 || i3 == 3) ? false : z;
    }

    public static zziv[] values() {
        return (zziv[]) c1.clone();
    }

    public final int zza() {
        return this.zzba;
    }
}
