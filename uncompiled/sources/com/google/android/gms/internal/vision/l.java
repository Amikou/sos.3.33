package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.vision.l0;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public final class l extends l0<l, a> implements ew5 {
    private static final l zzi;
    private static volatile xw5<l> zzj;
    private int zzc;
    private zzfi$zze zzd;
    private h zze;
    private g zzf;
    private int zzg;
    private boolean zzh;

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* loaded from: classes.dex */
    public static final class a extends l0.b<l, a> implements ew5 {
        public a() {
            super(l.zzi);
        }

        public final a u(g gVar) {
            if (this.g0) {
                r();
                this.g0 = false;
            }
            ((l) this.f0).y(gVar);
            return this;
        }

        public /* synthetic */ a(m mVar) {
            this();
        }
    }

    static {
        l lVar = new l();
        zzi = lVar;
        l0.s(l.class, lVar);
    }

    public static a x() {
        return zzi.u();
    }

    /* JADX WARN: Type inference failed for: r2v14, types: [xw5<com.google.android.gms.internal.vision.l>, com.google.android.gms.internal.vision.l0$a] */
    @Override // com.google.android.gms.internal.vision.l0
    public final Object o(int i, Object obj, Object obj2) {
        xw5<l> xw5Var;
        switch (m.a[i - 1]) {
            case 1:
                return new l();
            case 2:
                return new a(null);
            case 3:
                return l0.p(zzi, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဉ\u0001\u0003ဉ\u0002\u0004င\u0003\u0005ဇ\u0004", new Object[]{"zzc", "zzd", "zze", "zzf", "zzg", "zzh"});
            case 4:
                return zzi;
            case 5:
                xw5<l> xw5Var2 = zzj;
                xw5<l> xw5Var3 = xw5Var2;
                if (xw5Var2 == null) {
                    synchronized (l.class) {
                        xw5<l> xw5Var4 = zzj;
                        xw5Var = xw5Var4;
                        if (xw5Var4 == null) {
                            ?? aVar = new l0.a(zzi);
                            zzj = aVar;
                            xw5Var = aVar;
                        }
                    }
                    xw5Var3 = xw5Var;
                }
                return xw5Var3;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public final void y(g gVar) {
        gVar.getClass();
        this.zzf = gVar;
        this.zzc |= 4;
    }
}
