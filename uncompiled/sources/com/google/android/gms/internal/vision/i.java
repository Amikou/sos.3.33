package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.vision.l0;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public final class i extends l0<i, a> implements ew5 {
    private static final i zzf;
    private static volatile xw5<i> zzg;
    private int zzc;
    private long zzd;
    private long zze;

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* loaded from: classes.dex */
    public static final class a extends l0.b<i, a> implements ew5 {
        public a() {
            super(i.zzf);
        }

        public /* synthetic */ a(m mVar) {
            this();
        }
    }

    static {
        i iVar = new i();
        zzf = iVar;
        l0.s(i.class, iVar);
    }

    /* JADX WARN: Type inference failed for: r2v14, types: [xw5<com.google.android.gms.internal.vision.i>, com.google.android.gms.internal.vision.l0$a] */
    @Override // com.google.android.gms.internal.vision.l0
    public final Object o(int i, Object obj, Object obj2) {
        xw5<i> xw5Var;
        switch (m.a[i - 1]) {
            case 1:
                return new i();
            case 2:
                return new a(null);
            case 3:
                return l0.p(zzf, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001ဂ\u0000\u0002ဂ\u0001", new Object[]{"zzc", "zzd", "zze"});
            case 4:
                return zzf;
            case 5:
                xw5<i> xw5Var2 = zzg;
                xw5<i> xw5Var3 = xw5Var2;
                if (xw5Var2 == null) {
                    synchronized (i.class) {
                        xw5<i> xw5Var4 = zzg;
                        xw5Var = xw5Var4;
                        if (xw5Var4 == null) {
                            ?? aVar = new l0.a(zzf);
                            zzg = aVar;
                            xw5Var = aVar;
                        }
                    }
                    xw5Var3 = xw5Var;
                }
                return xw5Var3;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
