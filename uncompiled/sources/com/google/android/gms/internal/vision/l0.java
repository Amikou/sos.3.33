package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.vision.l0;
import com.google.android.gms.internal.vision.l0.b;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public abstract class l0<MessageType extends l0<MessageType, BuilderType>, BuilderType extends b<MessageType, BuilderType>> extends a0<MessageType, BuilderType> {
    private static Map<Object, l0<?, ?>> zzd = new ConcurrentHashMap();
    public x0 zzb = x0.a();
    private int zzc = -1;

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* loaded from: classes.dex */
    public static class a<T extends l0<T, ?>> extends on5<T> {
        public a(T t) {
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* loaded from: classes.dex */
    public static abstract class b<MessageType extends l0<MessageType, BuilderType>, BuilderType extends b<MessageType, BuilderType>> extends z<MessageType, BuilderType> {
        public final MessageType a;
        public MessageType f0;
        public boolean g0 = false;

        public b(MessageType messagetype) {
            this.a = messagetype;
            this.f0 = (MessageType) messagetype.o(f.d, null, null);
        }

        public static void o(MessageType messagetype, MessageType messagetype2) {
            zw5.c().b(messagetype).c(messagetype, messagetype2);
        }

        /* JADX WARN: Multi-variable type inference failed */
        public /* synthetic */ Object clone() throws CloneNotSupportedException {
            b bVar = (b) this.a.o(f.e, null, null);
            bVar.b((l0) c());
            return bVar;
        }

        @Override // defpackage.ew5
        public final /* synthetic */ n0 e() {
            return this.a;
        }

        @Override // com.google.android.gms.internal.vision.z
        public final /* synthetic */ z f(byte[] bArr, int i, int i2, h0 h0Var) throws zzjk {
            return p(bArr, 0, i2, h0Var);
        }

        @Override // com.google.android.gms.internal.vision.z
        /* renamed from: g */
        public final BuilderType b(MessageType messagetype) {
            if (this.g0) {
                r();
                this.g0 = false;
            }
            o(this.f0, messagetype);
            return this;
        }

        public final BuilderType p(byte[] bArr, int i, int i2, h0 h0Var) throws zzjk {
            if (this.g0) {
                r();
                this.g0 = false;
            }
            try {
                zw5.c().b(this.f0).h(this.f0, bArr, 0, i2, new go5(h0Var));
                return this;
            } catch (zzjk e) {
                throw e;
            } catch (IOException e2) {
                throw new RuntimeException("Reading from byte array should not throw IOException.", e2);
            } catch (IndexOutOfBoundsException unused) {
                throw zzjk.zza();
            }
        }

        public void r() {
            MessageType messagetype = (MessageType) this.f0.o(f.d, null, null);
            o(messagetype, this.f0);
            this.f0 = messagetype;
        }

        @Override // defpackage.gw5
        /* renamed from: s */
        public MessageType c() {
            if (this.g0) {
                return this.f0;
            }
            MessageType messagetype = this.f0;
            zw5.c().b(messagetype).a(messagetype);
            this.g0 = true;
            return this.f0;
        }

        @Override // defpackage.gw5
        /* renamed from: t */
        public final MessageType i() {
            MessageType messagetype = (MessageType) c();
            if (messagetype.a()) {
                return messagetype;
            }
            throw new zzlv(messagetype);
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* loaded from: classes.dex */
    public static abstract class c<MessageType extends c<MessageType, BuilderType>, BuilderType> extends l0<MessageType, BuilderType> implements ew5 {
        public k0<e> zzc = k0.c();

        public final k0<e> x() {
            if (this.zzc.n()) {
                this.zzc = (k0) this.zzc.clone();
            }
            return this.zzc;
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* loaded from: classes.dex */
    public static class d<ContainingType extends n0, Type> extends uq5<ContainingType, Type> {
        public final n0 a;
        public final e b;
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* loaded from: classes.dex */
    public static final class e implements pr5<e> {
        public final int a;
        public final zzml f0;
        public final boolean g0;

        /* JADX WARN: Multi-variable type inference failed */
        @Override // defpackage.pr5
        public final gw5 F0(gw5 gw5Var, n0 n0Var) {
            return ((b) gw5Var).b((l0) n0Var);
        }

        @Override // defpackage.pr5
        public final boolean b() {
            return this.g0;
        }

        @Override // defpackage.pr5
        public final boolean c() {
            return false;
        }

        @Override // defpackage.pr5
        public final ow5 c1(ow5 ow5Var, ow5 ow5Var2) {
            throw new UnsupportedOperationException();
        }

        @Override // java.lang.Comparable
        public final /* synthetic */ int compareTo(Object obj) {
            return this.a - ((e) obj).a;
        }

        @Override // defpackage.pr5
        public final int zza() {
            return this.a;
        }

        @Override // defpackage.pr5
        public final zzml zzb() {
            return this.f0;
        }

        @Override // defpackage.pr5
        public final zzmo zzc() {
            return this.f0.zza();
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* loaded from: classes.dex */
    public enum f {
        public static final int a = 1;
        public static final int b = 2;
        public static final int c = 3;
        public static final int d = 4;
        public static final int e = 5;
        public static final int f = 6;
        public static final int g = 7;
        public static final /* synthetic */ int[] h = {1, 2, 3, 4, 5, 6, 7};

        public static int[] a() {
            return (int[]) h.clone();
        }
    }

    public static <T extends l0<?, ?>> T n(Class<T> cls) {
        l0<?, ?> l0Var = zzd.get(cls);
        if (l0Var == null) {
            try {
                Class.forName(cls.getName(), true, cls.getClassLoader());
                l0Var = zzd.get(cls);
            } catch (ClassNotFoundException e2) {
                throw new IllegalStateException("Class initialization cannot fail.", e2);
            }
        }
        if (l0Var == null) {
            l0Var = (T) ((l0) z0.c(cls)).o(f.f, null, null);
            if (l0Var != null) {
                zzd.put(cls, l0Var);
            } else {
                throw new IllegalStateException();
            }
        }
        return (T) l0Var;
    }

    public static Object p(n0 n0Var, String str, Object[] objArr) {
        return new kx5(n0Var, str, objArr);
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    public static Object q(Method method, Object obj, Object... objArr) {
        try {
            return method.invoke(obj, objArr);
        } catch (IllegalAccessException e2) {
            throw new RuntimeException("Couldn't use Java reflection to implement protocol message reflection.", e2);
        } catch (InvocationTargetException e3) {
            Throwable cause = e3.getCause();
            if (!(cause instanceof RuntimeException)) {
                if (cause instanceof Error) {
                    throw ((Error) cause);
                }
                throw new RuntimeException("Unexpected exception thrown by generated accessor method.", cause);
            }
            throw ((RuntimeException) cause);
        }
    }

    public static <E> gt5<E> r(gt5<E> gt5Var) {
        int size = gt5Var.size();
        return gt5Var.d(size == 0 ? 10 : size << 1);
    }

    public static <T extends l0<?, ?>> void s(Class<T> cls, T t) {
        zzd.put(cls, t);
    }

    public static final <T extends l0<T, ?>> boolean t(T t, boolean z) {
        byte byteValue = ((Byte) t.o(f.a, null, null)).byteValue();
        if (byteValue == 1) {
            return true;
        }
        if (byteValue == 0) {
            return false;
        }
        boolean e2 = zw5.c().b(t).e(t);
        if (z) {
            t.o(f.b, e2 ? t : null, null);
        }
        return e2;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [qs5, et5] */
    public static et5 v() {
        return qs5.m();
    }

    public static <E> gt5<E> w() {
        return mx5.m();
    }

    @Override // defpackage.ew5
    public final boolean a() {
        return t(this, true);
    }

    @Override // com.google.android.gms.internal.vision.n0
    public final /* synthetic */ gw5 d() {
        b bVar = (b) o(f.e, null, null);
        bVar.b(this);
        return bVar;
    }

    @Override // defpackage.ew5
    public final /* synthetic */ n0 e() {
        return (l0) o(f.f, null, null);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass()) {
            return zw5.c().b(this).g(this, (l0) obj);
        }
        return false;
    }

    @Override // com.google.android.gms.internal.vision.a0
    public final void f(int i) {
        this.zzc = i;
    }

    public int hashCode() {
        int i = this.zza;
        if (i != 0) {
            return i;
        }
        int b2 = zw5.c().b(this).b(this);
        this.zza = b2;
        return b2;
    }

    @Override // com.google.android.gms.internal.vision.n0
    public final /* synthetic */ gw5 j() {
        return (b) o(f.e, null, null);
    }

    @Override // com.google.android.gms.internal.vision.n0
    public final int k() {
        if (this.zzc == -1) {
            this.zzc = zw5.c().b(this).f(this);
        }
        return this.zzc;
    }

    @Override // com.google.android.gms.internal.vision.n0
    public final void l(zzii zziiVar) throws IOException {
        zw5.c().b(this).i(this, g0.O(zziiVar));
    }

    @Override // com.google.android.gms.internal.vision.a0
    final int m() {
        return this.zzc;
    }

    public abstract Object o(int i, Object obj, Object obj2);

    public String toString() {
        return p0.a(this, super.toString());
    }

    public final <MessageType extends l0<MessageType, BuilderType>, BuilderType extends b<MessageType, BuilderType>> BuilderType u() {
        return (BuilderType) o(f.e, null, null);
    }
}
