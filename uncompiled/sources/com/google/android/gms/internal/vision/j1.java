package com.google.android.gms.internal.vision;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-vision@@20.1.3 */
/* loaded from: classes.dex */
public final class j1 extends a implements h1 {
    public j1(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.vision.barcode.internal.client.INativeBarcodeDetectorCreator");
    }

    @Override // com.google.android.gms.internal.vision.h1
    public final s0 g0(lm1 lm1Var, zzk zzkVar) throws RemoteException {
        s0 i1Var;
        Parcel b = b();
        oc5.a(b, lm1Var);
        oc5.b(b, zzkVar);
        Parcel F1 = F1(1, b);
        IBinder readStrongBinder = F1.readStrongBinder();
        if (readStrongBinder == null) {
            i1Var = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.vision.barcode.internal.client.INativeBarcodeDetector");
            if (queryLocalInterface instanceof s0) {
                i1Var = (s0) queryLocalInterface;
            } else {
                i1Var = new i1(readStrongBinder);
            }
        }
        F1.recycle();
        return i1Var;
    }
}
