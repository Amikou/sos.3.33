package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.vision.l0;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public final class h extends l0<h, a> implements ew5 {
    private static final h zzj;
    private static volatile xw5<h> zzk;
    private int zzc;
    private long zze;
    private b zzf;
    private zzfi$zzg zzh;
    private c zzi;
    private String zzd = "";
    private String zzg = "";

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* loaded from: classes.dex */
    public static final class a extends l0.b<h, a> implements ew5 {
        public a() {
            super(h.zzj);
        }

        public /* synthetic */ a(m mVar) {
            this();
        }
    }

    static {
        h hVar = new h();
        zzj = hVar;
        l0.s(h.class, hVar);
    }

    /* JADX WARN: Type inference failed for: r2v14, types: [xw5<com.google.android.gms.internal.vision.h>, com.google.android.gms.internal.vision.l0$a] */
    @Override // com.google.android.gms.internal.vision.l0
    public final Object o(int i, Object obj, Object obj2) {
        xw5<h> xw5Var;
        switch (m.a[i - 1]) {
            case 1:
                return new h();
            case 2:
                return new a(null);
            case 3:
                return l0.p(zzj, "\u0001\u0006\u0000\u0001\u0001\u0011\u0006\u0000\u0000\u0000\u0001ဈ\u0000\u0002ဂ\u0001\u0003ဉ\u0002\u0006ဈ\u0003\u0010ဉ\u0004\u0011ဉ\u0005", new Object[]{"zzc", "zzd", "zze", "zzf", "zzg", "zzh", "zzi"});
            case 4:
                return zzj;
            case 5:
                xw5<h> xw5Var2 = zzk;
                xw5<h> xw5Var3 = xw5Var2;
                if (xw5Var2 == null) {
                    synchronized (h.class) {
                        xw5<h> xw5Var4 = zzk;
                        xw5Var = xw5Var4;
                        if (xw5Var4 == null) {
                            ?? aVar = new l0.a(zzj);
                            zzk = aVar;
                            xw5Var = aVar;
                        }
                    }
                    xw5Var3 = xw5Var;
                }
                return xw5Var3;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
