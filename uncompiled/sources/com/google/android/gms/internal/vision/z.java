package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.vision.a0;
import com.google.android.gms.internal.vision.z;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public abstract class z<MessageType extends a0<MessageType, BuilderType>, BuilderType extends z<MessageType, BuilderType>> implements gw5 {
    /* JADX WARN: Multi-variable type inference failed */
    @Override // defpackage.gw5
    public final /* synthetic */ gw5 F0(n0 n0Var) {
        if (e().getClass().isInstance(n0Var)) {
            return b((a0) n0Var);
        }
        throw new IllegalArgumentException("mergeFrom(MessageLite) can only merge messages of the same type.");
    }

    public abstract BuilderType b(MessageType messagetype);

    public abstract BuilderType f(byte[] bArr, int i, int i2, h0 h0Var) throws zzjk;
}
