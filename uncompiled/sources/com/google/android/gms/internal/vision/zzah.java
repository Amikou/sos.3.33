package com.google.android.gms.internal.vision;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

/* compiled from: com.google.android.gms:play-services-vision@@20.1.3 */
/* loaded from: classes.dex */
public final class zzah extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzah> CREATOR = new w45();
    public final zzao[] a;
    public final zzab f0;
    public final zzab g0;
    public final zzab h0;
    public final String i0;
    public final float j0;
    public final String k0;
    public final int l0;
    public final boolean m0;
    public final int n0;
    public final int o0;

    public zzah(zzao[] zzaoVarArr, zzab zzabVar, zzab zzabVar2, zzab zzabVar3, String str, float f, String str2, int i, boolean z, int i2, int i3) {
        this.a = zzaoVarArr;
        this.f0 = zzabVar;
        this.g0 = zzabVar2;
        this.h0 = zzabVar3;
        this.i0 = str;
        this.j0 = f;
        this.k0 = str2;
        this.l0 = i;
        this.m0 = z;
        this.n0 = i2;
        this.o0 = i3;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int a = yb3.a(parcel);
        yb3.v(parcel, 2, this.a, i, false);
        yb3.r(parcel, 3, this.f0, i, false);
        yb3.r(parcel, 4, this.g0, i, false);
        yb3.r(parcel, 5, this.h0, i, false);
        yb3.s(parcel, 6, this.i0, false);
        yb3.j(parcel, 7, this.j0);
        yb3.s(parcel, 8, this.k0, false);
        yb3.m(parcel, 9, this.l0);
        yb3.c(parcel, 10, this.m0);
        yb3.m(parcel, 11, this.n0);
        yb3.m(parcel, 12, this.o0);
        yb3.b(parcel, a);
    }
}
