package com.google.android.gms.internal.vision;

import defpackage.pr5;
import java.io.IOException;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public abstract class j0<T extends pr5<T>> {
    public abstract int a(Map.Entry<?, ?> entry);

    public abstract k0<T> b(Object obj);

    public abstract Object c(h0 h0Var, n0 n0Var, int i);

    public abstract void d(g1 g1Var, Map.Entry<?, ?> entry) throws IOException;

    public abstract boolean e(n0 n0Var);

    public abstract k0<T> f(Object obj);

    public abstract void g(Object obj);
}
