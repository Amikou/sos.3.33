package com.google.android.gms.internal.vision;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public final class d1 {
    public static void f(byte b, byte b2, byte b3, byte b4, char[] cArr, int i) throws zzjk {
        if (!o(b2) && (((b << 28) + (b2 + 112)) >> 30) == 0 && !o(b3) && !o(b4)) {
            int i2 = ((b & 7) << 18) | ((b2 & 63) << 12) | ((b3 & 63) << 6) | (b4 & 63);
            cArr[i] = (char) ((i2 >>> 10) + 55232);
            cArr[i + 1] = (char) ((i2 & 1023) + 56320);
            return;
        }
        throw zzjk.zzh();
    }

    public static void g(byte b, byte b2, byte b3, char[] cArr, int i) throws zzjk {
        if (!o(b2) && ((b != -32 || b2 >= -96) && ((b != -19 || b2 < -96) && !o(b3)))) {
            cArr[i] = (char) (((b & 15) << 12) | ((b2 & 63) << 6) | (b3 & 63));
            return;
        }
        throw zzjk.zzh();
    }

    public static void h(byte b, byte b2, char[] cArr, int i) throws zzjk {
        if (b >= -62 && !o(b2)) {
            cArr[i] = (char) (((b & 31) << 6) | (b2 & 63));
            return;
        }
        throw zzjk.zzh();
    }

    public static void i(byte b, char[] cArr, int i) {
        cArr[i] = (char) b;
    }

    public static boolean l(byte b) {
        return b >= 0;
    }

    public static boolean m(byte b) {
        return b < -32;
    }

    public static boolean n(byte b) {
        return b < -16;
    }

    public static boolean o(byte b) {
        return b > -65;
    }
}
