package com.google.android.gms.internal.vision;

import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public abstract class d0 {
    public abstract void a(byte[] bArr, int i, int i2) throws IOException;
}
