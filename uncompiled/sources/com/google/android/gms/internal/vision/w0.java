package com.google.android.gms.internal.vision;

import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public final class w0 extends v0<x0, x0> {
    public static void m(Object obj, x0 x0Var) {
        ((l0) obj).zzb = x0Var;
    }

    @Override // com.google.android.gms.internal.vision.v0
    public final /* synthetic */ x0 a() {
        return x0.g();
    }

    @Override // com.google.android.gms.internal.vision.v0
    public final /* synthetic */ void b(x0 x0Var, int i, long j) {
        x0Var.c(i << 3, Long.valueOf(j));
    }

    @Override // com.google.android.gms.internal.vision.v0
    public final /* synthetic */ void c(x0 x0Var, int i, zzht zzhtVar) {
        x0Var.c((i << 3) | 2, zzhtVar);
    }

    @Override // com.google.android.gms.internal.vision.v0
    public final /* synthetic */ void d(x0 x0Var, g1 g1Var) throws IOException {
        x0Var.h(g1Var);
    }

    @Override // com.google.android.gms.internal.vision.v0
    public final /* bridge */ /* synthetic */ void e(Object obj, x0 x0Var) {
        m(obj, x0Var);
    }

    @Override // com.google.android.gms.internal.vision.v0
    public final /* synthetic */ x0 f(Object obj) {
        return ((l0) obj).zzb;
    }

    @Override // com.google.android.gms.internal.vision.v0
    public final /* synthetic */ void g(x0 x0Var, g1 g1Var) throws IOException {
        x0Var.e(g1Var);
    }

    @Override // com.google.android.gms.internal.vision.v0
    public final /* synthetic */ void h(Object obj, x0 x0Var) {
        m(obj, x0Var);
    }

    @Override // com.google.android.gms.internal.vision.v0
    public final /* synthetic */ x0 i(x0 x0Var, x0 x0Var2) {
        x0 x0Var3 = x0Var;
        x0 x0Var4 = x0Var2;
        return x0Var4.equals(x0.a()) ? x0Var3 : x0.b(x0Var3, x0Var4);
    }

    @Override // com.google.android.gms.internal.vision.v0
    public final void j(Object obj) {
        ((l0) obj).zzb.i();
    }

    @Override // com.google.android.gms.internal.vision.v0
    public final /* synthetic */ int k(x0 x0Var) {
        return x0Var.j();
    }

    @Override // com.google.android.gms.internal.vision.v0
    public final /* synthetic */ int l(x0 x0Var) {
        return x0Var.k();
    }
}
