package com.google.android.gms.internal.vision;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public abstract class c1 {
    public abstract int a(int i, byte[] bArr, int i2, int i3);

    public abstract int b(CharSequence charSequence, byte[] bArr, int i, int i2);

    public final boolean c(byte[] bArr, int i, int i2) {
        return a(0, bArr, i, i2) == 0;
    }

    public abstract String d(byte[] bArr, int i, int i2) throws zzjk;
}
