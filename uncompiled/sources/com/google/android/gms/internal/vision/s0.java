package com.google.android.gms.internal.vision;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.vision.barcode.Barcode;

/* compiled from: com.google.android.gms:play-services-vision@@20.1.3 */
/* loaded from: classes.dex */
public interface s0 extends IInterface {
    Barcode[] Z(lm1 lm1Var, zzs zzsVar) throws RemoteException;

    Barcode[] h(lm1 lm1Var, zzs zzsVar) throws RemoteException;

    void zza() throws RemoteException;
}
