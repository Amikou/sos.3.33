package com.google.android.gms.internal.vision;

import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public interface n0 extends ew5 {
    gw5 d();

    zzht h();

    gw5 j();

    int k();

    void l(zzii zziiVar) throws IOException;
}
