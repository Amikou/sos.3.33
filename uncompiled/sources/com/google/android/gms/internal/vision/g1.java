package com.google.android.gms.internal.vision;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public interface g1 {
    void A(int i, int i2) throws IOException;

    void B(int i, List<Float> list, boolean z) throws IOException;

    void C(int i, int i2) throws IOException;

    void D(int i, List<String> list) throws IOException;

    void E(int i, boolean z) throws IOException;

    void F(int i, zzht zzhtVar) throws IOException;

    void G(int i, int i2) throws IOException;

    @Deprecated
    void H(int i, List<?> list, t0 t0Var) throws IOException;

    void I(int i, Object obj, t0 t0Var) throws IOException;

    void J(int i, long j) throws IOException;

    <K, V> void K(int i, lv5<K, V> lv5Var, Map<K, V> map) throws IOException;

    void L(int i, long j) throws IOException;

    @Deprecated
    void M(int i, Object obj, t0 t0Var) throws IOException;

    void N(int i, List<?> list, t0 t0Var) throws IOException;

    void a(int i, List<Boolean> list, boolean z) throws IOException;

    void b(int i, long j) throws IOException;

    void c(int i, List<Integer> list, boolean z) throws IOException;

    @Deprecated
    void d(int i) throws IOException;

    void e(int i, List<Integer> list, boolean z) throws IOException;

    void f(int i, int i2) throws IOException;

    void g(int i, long j) throws IOException;

    @Deprecated
    void h(int i) throws IOException;

    void i(int i, int i2) throws IOException;

    void j(int i, Object obj) throws IOException;

    void k(int i, List<Long> list, boolean z) throws IOException;

    void l(int i, List<Long> list, boolean z) throws IOException;

    void m(int i, List<Integer> list, boolean z) throws IOException;

    void n(int i, String str) throws IOException;

    void o(int i, List<Integer> list, boolean z) throws IOException;

    void p(int i, List<Integer> list, boolean z) throws IOException;

    void q(int i, long j) throws IOException;

    void r(int i, double d) throws IOException;

    void s(int i, float f) throws IOException;

    void t(int i, List<Long> list, boolean z) throws IOException;

    void u(int i, int i2) throws IOException;

    void v(int i, List<Long> list, boolean z) throws IOException;

    void w(int i, List<Long> list, boolean z) throws IOException;

    void x(int i, List<Integer> list, boolean z) throws IOException;

    void y(int i, List<Double> list, boolean z) throws IOException;

    void z(int i, List<zzht> list) throws IOException;

    int zza();
}
