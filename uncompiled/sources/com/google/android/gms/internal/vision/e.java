package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.vision.l0;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public final class e extends l0<e, a> implements ew5 {
    private static final e zzd;
    private static volatile xw5<e> zze;
    private gt5<j> zzc = l0.w();

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* loaded from: classes.dex */
    public static final class a extends l0.b<e, a> implements ew5 {
        public a() {
            super(e.zzd);
        }

        public /* synthetic */ a(m mVar) {
            this();
        }
    }

    static {
        e eVar = new e();
        zzd = eVar;
        l0.s(e.class, eVar);
    }

    /* JADX WARN: Type inference failed for: r2v14, types: [xw5<com.google.android.gms.internal.vision.e>, com.google.android.gms.internal.vision.l0$a] */
    @Override // com.google.android.gms.internal.vision.l0
    public final Object o(int i, Object obj, Object obj2) {
        xw5<e> xw5Var;
        switch (m.a[i - 1]) {
            case 1:
                return new e();
            case 2:
                return new a(null);
            case 3:
                return l0.p(zzd, "\u0001\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001\u001b", new Object[]{"zzc", j.class});
            case 4:
                return zzd;
            case 5:
                xw5<e> xw5Var2 = zze;
                xw5<e> xw5Var3 = xw5Var2;
                if (xw5Var2 == null) {
                    synchronized (e.class) {
                        xw5<e> xw5Var4 = zze;
                        xw5Var = xw5Var4;
                        if (xw5Var4 == null) {
                            ?? aVar = new l0.a(zzd);
                            zze = aVar;
                            xw5Var = aVar;
                        }
                    }
                    xw5Var3 = xw5Var;
                }
                return xw5Var3;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
