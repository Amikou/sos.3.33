package com.google.android.gms.internal.vision;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

/* compiled from: com.google.android.gms:play-services-vision@@20.1.3 */
/* loaded from: classes.dex */
public final class zzk extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzk> CREATOR = new es5();
    public int a;
    public boolean f0;

    public zzk() {
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int a = yb3.a(parcel);
        yb3.m(parcel, 2, this.a);
        yb3.c(parcel, 3, this.f0);
        yb3.b(parcel, a);
    }

    public zzk(int i, boolean z) {
        this.a = i;
        this.f0 = z;
    }
}
