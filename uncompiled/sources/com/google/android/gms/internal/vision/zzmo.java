package com.google.android.gms.internal.vision;

import com.github.mikephil.charting.utils.Utils;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public enum zzmo {
    INT(0),
    LONG(0L),
    FLOAT(Float.valueOf((float) Utils.FLOAT_EPSILON)),
    DOUBLE(Double.valueOf((double) Utils.DOUBLE_EPSILON)),
    BOOLEAN(Boolean.FALSE),
    STRING(""),
    BYTE_STRING(zzht.zza),
    ENUM(null),
    MESSAGE(null);
    
    private final Object zzj;

    zzmo(Object obj) {
        this.zzj = obj;
    }
}
