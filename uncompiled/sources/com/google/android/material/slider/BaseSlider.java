package com.google.android.material.slider;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityManager;
import android.widget.SeekBar;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.material.slider.BaseSlider;
import defpackage.b6;
import defpackage.sn;
import defpackage.tn;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/* JADX INFO: Access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public abstract class BaseSlider<S extends BaseSlider<S, L, T>, L extends sn<S>, T extends tn<S>> extends View {
    public static final String d1 = BaseSlider.class.getSimpleName();
    public static final int e1 = y13.Widget_MaterialComponents_Slider;
    public int A0;
    public int B0;
    public int C0;
    public int D0;
    public int E0;
    public float F0;
    public MotionEvent G0;
    public by1 H0;
    public boolean I0;
    public float J0;
    public float K0;
    public ArrayList<Float> L0;
    public int M0;
    public int N0;
    public float O0;
    public float[] P0;
    public boolean Q0;
    public int R0;
    public boolean S0;
    public boolean T0;
    public boolean U0;
    public ColorStateList V0;
    public ColorStateList W0;
    public ColorStateList X0;
    public ColorStateList Y0;
    public ColorStateList Z0;
    public final Paint a;
    public final o42 a1;
    public float b1;
    public int c1;
    public final Paint f0;
    public final Paint g0;
    public final Paint h0;
    public final Paint i0;
    public final Paint j0;
    public final e k0;
    public final AccessibilityManager l0;
    public BaseSlider<S, L, T>.d m0;
    public final f n0;
    public final List<l74> o0;
    public final List<L> p0;
    public final List<T> q0;
    public boolean r0;
    public ValueAnimator s0;
    public ValueAnimator t0;
    public final int u0;
    public int v0;
    public int w0;
    public int x0;
    public int y0;
    public int z0;

    /* loaded from: classes2.dex */
    public static class SliderState extends View.BaseSavedState {
        public static final Parcelable.Creator<SliderState> CREATOR = new a();
        public float a;
        public float f0;
        public ArrayList<Float> g0;
        public float h0;
        public boolean i0;

        /* loaded from: classes2.dex */
        public static class a implements Parcelable.Creator<SliderState> {
            @Override // android.os.Parcelable.Creator
            /* renamed from: a */
            public SliderState createFromParcel(Parcel parcel) {
                return new SliderState(parcel, null);
            }

            @Override // android.os.Parcelable.Creator
            /* renamed from: b */
            public SliderState[] newArray(int i) {
                return new SliderState[i];
            }
        }

        public /* synthetic */ SliderState(Parcel parcel, a aVar) {
            this(parcel);
        }

        @Override // android.view.View.BaseSavedState, android.view.AbsSavedState, android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeFloat(this.a);
            parcel.writeFloat(this.f0);
            parcel.writeList(this.g0);
            parcel.writeFloat(this.h0);
            parcel.writeBooleanArray(new boolean[]{this.i0});
        }

        public SliderState(Parcelable parcelable) {
            super(parcelable);
        }

        public SliderState(Parcel parcel) {
            super(parcel);
            this.a = parcel.readFloat();
            this.f0 = parcel.readFloat();
            ArrayList<Float> arrayList = new ArrayList<>();
            this.g0 = arrayList;
            parcel.readList(arrayList, Float.class.getClassLoader());
            this.h0 = parcel.readFloat();
            this.i0 = parcel.createBooleanArray()[0];
        }
    }

    /* loaded from: classes2.dex */
    public class a implements f {
        public final /* synthetic */ AttributeSet a;
        public final /* synthetic */ int b;

        public a(AttributeSet attributeSet, int i) {
            this.a = attributeSet;
            this.b = i;
        }

        @Override // com.google.android.material.slider.BaseSlider.f
        public l74 a() {
            TypedArray h = a54.h(BaseSlider.this.getContext(), this.a, o23.Slider, this.b, BaseSlider.e1, new int[0]);
            l74 S = BaseSlider.S(BaseSlider.this.getContext(), h);
            h.recycle();
            return S;
        }
    }

    /* loaded from: classes2.dex */
    public class b implements ValueAnimator.AnimatorUpdateListener {
        public b() {
        }

        @Override // android.animation.ValueAnimator.AnimatorUpdateListener
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            float floatValue = ((Float) valueAnimator.getAnimatedValue()).floatValue();
            for (l74 l74Var : BaseSlider.this.o0) {
                l74Var.B0(floatValue);
            }
            ei4.j0(BaseSlider.this);
        }
    }

    /* loaded from: classes2.dex */
    public class c extends AnimatorListenerAdapter {
        public c() {
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            super.onAnimationEnd(animator);
            for (l74 l74Var : BaseSlider.this.o0) {
                mk4.e(BaseSlider.this).b(l74Var);
            }
        }
    }

    /* loaded from: classes2.dex */
    public static class e extends c11 {
        public final BaseSlider<?, ?, ?> q;
        public Rect r;

        public e(BaseSlider<?, ?, ?> baseSlider) {
            super(baseSlider);
            this.r = new Rect();
            this.q = baseSlider;
        }

        @Override // defpackage.c11
        public int B(float f, float f2) {
            for (int i = 0; i < this.q.getValues().size(); i++) {
                this.q.d0(i, this.r);
                if (this.r.contains((int) f, (int) f2)) {
                    return i;
                }
            }
            return -1;
        }

        @Override // defpackage.c11
        public void C(List<Integer> list) {
            for (int i = 0; i < this.q.getValues().size(); i++) {
                list.add(Integer.valueOf(i));
            }
        }

        @Override // defpackage.c11
        public boolean L(int i, int i2, Bundle bundle) {
            if (this.q.isEnabled()) {
                if (i2 == 4096 || i2 == 8192) {
                    float l = this.q.l(20);
                    if (i2 == 8192) {
                        l = -l;
                    }
                    if (this.q.G()) {
                        l = -l;
                    }
                    if (this.q.b0(i, x42.a(this.q.getValues().get(i).floatValue() + l, this.q.getValueFrom(), this.q.getValueTo()))) {
                        this.q.e0();
                        this.q.postInvalidate();
                        E(i);
                        return true;
                    }
                    return false;
                }
                if (i2 == 16908349 && bundle != null && bundle.containsKey("android.view.accessibility.action.ARGUMENT_PROGRESS_VALUE")) {
                    if (this.q.b0(i, bundle.getFloat("android.view.accessibility.action.ARGUMENT_PROGRESS_VALUE"))) {
                        this.q.e0();
                        this.q.postInvalidate();
                        E(i);
                        return true;
                    }
                }
                return false;
            }
            return false;
        }

        @Override // defpackage.c11
        public void P(int i, b6 b6Var) {
            b6Var.b(b6.a.o);
            List<Float> values = this.q.getValues();
            float floatValue = values.get(i).floatValue();
            float valueFrom = this.q.getValueFrom();
            float valueTo = this.q.getValueTo();
            if (this.q.isEnabled()) {
                if (floatValue > valueFrom) {
                    b6Var.a(8192);
                }
                if (floatValue < valueTo) {
                    b6Var.a(4096);
                }
            }
            b6Var.v0(b6.d.a(1, valueFrom, valueTo, floatValue));
            b6Var.c0(SeekBar.class.getName());
            StringBuilder sb = new StringBuilder();
            if (this.q.getContentDescription() != null) {
                sb.append(this.q.getContentDescription());
                sb.append(",");
            }
            if (values.size() > 1) {
                sb.append(Y(i));
                sb.append(this.q.z(floatValue));
            }
            b6Var.g0(sb.toString());
            this.q.d0(i, this.r);
            b6Var.X(this.r);
        }

        public final String Y(int i) {
            if (i == this.q.getValues().size() - 1) {
                return this.q.getContext().getString(r13.material_slider_range_end);
            }
            return i == 0 ? this.q.getContext().getString(r13.material_slider_range_start) : "";
        }
    }

    /* loaded from: classes2.dex */
    public interface f {
        l74 a();
    }

    public BaseSlider(Context context) {
        this(context, null);
    }

    public static float A(ValueAnimator valueAnimator, float f2) {
        if (valueAnimator == null || !valueAnimator.isRunning()) {
            return f2;
        }
        float floatValue = ((Float) valueAnimator.getAnimatedValue()).floatValue();
        valueAnimator.cancel();
        return floatValue;
    }

    public static l74 S(Context context, TypedArray typedArray) {
        return l74.u0(context, null, 0, typedArray.getResourceId(o23.Slider_labelStyle, y13.Widget_MaterialComponents_Tooltip));
    }

    public static int U(float[] fArr, float f2) {
        return Math.round(f2 * ((fArr.length / 2) - 1));
    }

    private float[] getActiveRange() {
        float floatValue = ((Float) Collections.max(getValues())).floatValue();
        float floatValue2 = ((Float) Collections.min(getValues())).floatValue();
        if (this.L0.size() == 1) {
            floatValue2 = this.J0;
        }
        float O = O(floatValue2);
        float O2 = O(floatValue);
        return G() ? new float[]{O2, O} : new float[]{O, O2};
    }

    private float getValueOfTouchPosition() {
        double a0 = a0(this.b1);
        if (G()) {
            a0 = 1.0d - a0;
        }
        float f2 = this.K0;
        float f3 = this.J0;
        return (float) ((a0 * (f2 - f3)) + f3);
    }

    private float getValueOfTouchPositionAbsolute() {
        float f2 = this.b1;
        if (G()) {
            f2 = 1.0f - f2;
        }
        float f3 = this.K0;
        float f4 = this.J0;
        return (f2 * (f3 - f4)) + f4;
    }

    private void setValuesInternal(ArrayList<Float> arrayList) {
        if (!arrayList.isEmpty()) {
            Collections.sort(arrayList);
            if (this.L0.size() == arrayList.size() && this.L0.equals(arrayList)) {
                return;
            }
            this.L0 = arrayList;
            this.U0 = true;
            this.N0 = 0;
            e0();
            o();
            s();
            postInvalidate();
            return;
        }
        throw new IllegalArgumentException("At least one value must be set");
    }

    public final float B(int i, float f2) {
        float f3 = this.O0;
        float f4 = Utils.FLOAT_EPSILON;
        if (f3 == Utils.FLOAT_EPSILON) {
            f4 = getMinSeparation();
        }
        if (this.c1 == 0) {
            f4 = q(f4);
        }
        if (G()) {
            f4 = -f4;
        }
        int i2 = i + 1;
        int i3 = i - 1;
        return x42.a(f2, i3 < 0 ? this.J0 : this.L0.get(i3).floatValue() + f4, i2 >= this.L0.size() ? this.K0 : this.L0.get(i2).floatValue() - f4);
    }

    public final int C(ColorStateList colorStateList) {
        return colorStateList.getColorForState(getDrawableState(), colorStateList.getDefaultColor());
    }

    public boolean D() {
        return this.H0 != null;
    }

    public final void E() {
        this.a.setStrokeWidth(this.z0);
        this.f0.setStrokeWidth(this.z0);
        this.i0.setStrokeWidth(this.z0 / 2.0f);
        this.j0.setStrokeWidth(this.z0 / 2.0f);
    }

    public final boolean F() {
        ViewParent parent = getParent();
        while (true) {
            boolean z = false;
            if (!(parent instanceof ViewGroup)) {
                return false;
            }
            ViewGroup viewGroup = (ViewGroup) parent;
            if (viewGroup.canScrollVertically(1) || viewGroup.canScrollVertically(-1)) {
                z = true;
            }
            if (z && viewGroup.shouldDelayChildPressedState()) {
                return true;
            }
            parent = parent.getParent();
        }
    }

    public final boolean G() {
        return ei4.E(this) == 1;
    }

    public final void H(Resources resources) {
        this.x0 = resources.getDimensionPixelSize(jz2.mtrl_slider_widget_height);
        int dimensionPixelOffset = resources.getDimensionPixelOffset(jz2.mtrl_slider_track_side_padding);
        this.v0 = dimensionPixelOffset;
        this.A0 = dimensionPixelOffset;
        this.w0 = resources.getDimensionPixelSize(jz2.mtrl_slider_thumb_radius);
        this.B0 = resources.getDimensionPixelOffset(jz2.mtrl_slider_track_top);
        this.E0 = resources.getDimensionPixelSize(jz2.mtrl_slider_label_padding);
    }

    public final void I() {
        if (this.O0 <= Utils.FLOAT_EPSILON) {
            return;
        }
        g0();
        int min = Math.min((int) (((this.K0 - this.J0) / this.O0) + 1.0f), (this.R0 / (this.z0 * 2)) + 1);
        float[] fArr = this.P0;
        if (fArr == null || fArr.length != min * 2) {
            this.P0 = new float[min * 2];
        }
        float f2 = this.R0 / (min - 1);
        for (int i = 0; i < min * 2; i += 2) {
            float[] fArr2 = this.P0;
            fArr2[i] = this.A0 + ((i / 2) * f2);
            fArr2[i + 1] = m();
        }
    }

    public final void J(Canvas canvas, int i, int i2) {
        if (Y()) {
            int O = (int) (this.A0 + (O(this.L0.get(this.N0).floatValue()) * i));
            if (Build.VERSION.SDK_INT < 28) {
                int i3 = this.D0;
                canvas.clipRect(O - i3, i2 - i3, O + i3, i3 + i2, Region.Op.UNION);
            }
            canvas.drawCircle(O, i2, this.D0, this.h0);
        }
    }

    public final void K(Canvas canvas) {
        if (!this.Q0 || this.O0 <= Utils.FLOAT_EPSILON) {
            return;
        }
        float[] activeRange = getActiveRange();
        int U = U(this.P0, activeRange[0]);
        int U2 = U(this.P0, activeRange[1]);
        int i = U * 2;
        canvas.drawPoints(this.P0, 0, i, this.i0);
        int i2 = U2 * 2;
        canvas.drawPoints(this.P0, i, i2 - i, this.j0);
        float[] fArr = this.P0;
        canvas.drawPoints(fArr, i2, fArr.length - i2, this.i0);
    }

    public final void L() {
        this.A0 = this.v0 + Math.max(this.C0 - this.w0, 0);
        if (ei4.W(this)) {
            f0(getWidth());
        }
    }

    public final boolean M(int i) {
        int i2 = this.N0;
        int c2 = (int) x42.c(i2 + i, 0L, this.L0.size() - 1);
        this.N0 = c2;
        if (c2 == i2) {
            return false;
        }
        if (this.M0 != -1) {
            this.M0 = c2;
        }
        e0();
        postInvalidate();
        return true;
    }

    public final boolean N(int i) {
        if (G()) {
            i = i == Integer.MIN_VALUE ? Integer.MAX_VALUE : -i;
        }
        return M(i);
    }

    public final float O(float f2) {
        float f3 = this.J0;
        float f4 = (f2 - f3) / (this.K0 - f3);
        return G() ? 1.0f - f4 : f4;
    }

    public final Boolean P(int i, KeyEvent keyEvent) {
        if (i != 61) {
            if (i != 66) {
                if (i != 81) {
                    if (i == 69) {
                        M(-1);
                        return Boolean.TRUE;
                    } else if (i != 70) {
                        switch (i) {
                            case 21:
                                N(-1);
                                return Boolean.TRUE;
                            case 22:
                                N(1);
                                return Boolean.TRUE;
                            case 23:
                                break;
                            default:
                                return null;
                        }
                    }
                }
                M(1);
                return Boolean.TRUE;
            }
            this.M0 = this.N0;
            postInvalidate();
            return Boolean.TRUE;
        } else if (keyEvent.hasNoModifiers()) {
            return Boolean.valueOf(M(1));
        } else {
            if (keyEvent.isShiftPressed()) {
                return Boolean.valueOf(M(-1));
            }
            return Boolean.FALSE;
        }
    }

    public final void Q() {
        for (T t : this.q0) {
            t.a(this);
        }
    }

    public final void R() {
        for (T t : this.q0) {
            t.b(this);
        }
    }

    public boolean T() {
        if (this.M0 != -1) {
            return true;
        }
        float valueOfTouchPositionAbsolute = getValueOfTouchPositionAbsolute();
        float m0 = m0(valueOfTouchPositionAbsolute);
        this.M0 = 0;
        float abs = Math.abs(this.L0.get(0).floatValue() - valueOfTouchPositionAbsolute);
        for (int i = 1; i < this.L0.size(); i++) {
            float abs2 = Math.abs(this.L0.get(i).floatValue() - valueOfTouchPositionAbsolute);
            float m02 = m0(this.L0.get(i).floatValue());
            if (Float.compare(abs2, abs) > 1) {
                break;
            }
            boolean z = !G() ? m02 - m0 >= Utils.FLOAT_EPSILON : m02 - m0 <= Utils.FLOAT_EPSILON;
            if (Float.compare(abs2, abs) < 0) {
                this.M0 = i;
            } else {
                if (Float.compare(abs2, abs) != 0) {
                    continue;
                } else if (Math.abs(m02 - m0) < this.u0) {
                    this.M0 = -1;
                    return false;
                } else if (z) {
                    this.M0 = i;
                }
            }
            abs = abs2;
        }
        return this.M0 != -1;
    }

    public final void V(Context context, AttributeSet attributeSet, int i) {
        TypedArray h = a54.h(context, attributeSet, o23.Slider, i, e1, new int[0]);
        this.J0 = h.getFloat(o23.Slider_android_valueFrom, Utils.FLOAT_EPSILON);
        this.K0 = h.getFloat(o23.Slider_android_valueTo, 1.0f);
        setValues(Float.valueOf(this.J0));
        this.O0 = h.getFloat(o23.Slider_android_stepSize, Utils.FLOAT_EPSILON);
        int i2 = o23.Slider_trackColor;
        boolean hasValue = h.hasValue(i2);
        int i3 = hasValue ? i2 : o23.Slider_trackColorInactive;
        if (!hasValue) {
            i2 = o23.Slider_trackColorActive;
        }
        ColorStateList b2 = n42.b(context, h, i3);
        if (b2 == null) {
            b2 = mf.c(context, ty2.material_slider_inactive_track_color);
        }
        setTrackInactiveTintList(b2);
        ColorStateList b3 = n42.b(context, h, i2);
        if (b3 == null) {
            b3 = mf.c(context, ty2.material_slider_active_track_color);
        }
        setTrackActiveTintList(b3);
        this.a1.a0(n42.b(context, h, o23.Slider_thumbColor));
        int i4 = o23.Slider_thumbStrokeColor;
        if (h.hasValue(i4)) {
            setThumbStrokeColor(n42.b(context, h, i4));
        }
        setThumbStrokeWidth(h.getDimension(o23.Slider_thumbStrokeWidth, Utils.FLOAT_EPSILON));
        ColorStateList b4 = n42.b(context, h, o23.Slider_haloColor);
        if (b4 == null) {
            b4 = mf.c(context, ty2.material_slider_halo_color);
        }
        setHaloTintList(b4);
        this.Q0 = h.getBoolean(o23.Slider_tickVisible, true);
        int i5 = o23.Slider_tickColor;
        boolean hasValue2 = h.hasValue(i5);
        int i6 = hasValue2 ? i5 : o23.Slider_tickColorInactive;
        if (!hasValue2) {
            i5 = o23.Slider_tickColorActive;
        }
        ColorStateList b5 = n42.b(context, h, i6);
        if (b5 == null) {
            b5 = mf.c(context, ty2.material_slider_inactive_tick_marks_color);
        }
        setTickInactiveTintList(b5);
        ColorStateList b6 = n42.b(context, h, i5);
        if (b6 == null) {
            b6 = mf.c(context, ty2.material_slider_active_tick_marks_color);
        }
        setTickActiveTintList(b6);
        setThumbRadius(h.getDimensionPixelSize(o23.Slider_thumbRadius, 0));
        setHaloRadius(h.getDimensionPixelSize(o23.Slider_haloRadius, 0));
        setThumbElevation(h.getDimension(o23.Slider_thumbElevation, Utils.FLOAT_EPSILON));
        setTrackHeight(h.getDimensionPixelSize(o23.Slider_trackHeight, 0));
        this.y0 = h.getInt(o23.Slider_labelBehavior, 0);
        if (!h.getBoolean(o23.Slider_android_enabled, true)) {
            setEnabled(false);
        }
        h.recycle();
    }

    public final void W(int i) {
        BaseSlider<S, L, T>.d dVar = this.m0;
        if (dVar == null) {
            this.m0 = new d(this, null);
        } else {
            removeCallbacks(dVar);
        }
        this.m0.a(i);
        postDelayed(this.m0, 200L);
    }

    public final void X(l74 l74Var, float f2) {
        l74Var.C0(z(f2));
        int O = (this.A0 + ((int) (O(f2) * this.R0))) - (l74Var.getIntrinsicWidth() / 2);
        int m = m() - (this.E0 + this.C0);
        l74Var.setBounds(O, m - l74Var.getIntrinsicHeight(), l74Var.getIntrinsicWidth() + O, m);
        Rect rect = new Rect(l74Var.getBounds());
        mm0.c(mk4.d(this), this, rect);
        l74Var.setBounds(rect);
        mk4.e(this).a(l74Var);
    }

    public final boolean Y() {
        return this.S0 || Build.VERSION.SDK_INT < 21 || !(getBackground() instanceof RippleDrawable);
    }

    public final boolean Z(float f2) {
        return b0(this.M0, f2);
    }

    public final double a0(float f2) {
        float f3 = this.O0;
        if (f3 > Utils.FLOAT_EPSILON) {
            int i = (int) ((this.K0 - this.J0) / f3);
            return Math.round(f2 * i) / i;
        }
        return f2;
    }

    public final boolean b0(int i, float f2) {
        if (Math.abs(f2 - this.L0.get(i).floatValue()) < 1.0E-4d) {
            return false;
        }
        this.L0.set(i, Float.valueOf(B(i, f2)));
        this.N0 = i;
        r(i);
        return true;
    }

    public final boolean c0() {
        return Z(getValueOfTouchPosition());
    }

    public void d0(int i, Rect rect) {
        int O = this.A0 + ((int) (O(getValues().get(i).floatValue()) * this.R0));
        int m = m();
        int i2 = this.C0;
        rect.set(O - i2, m - i2, O + i2, m + i2);
    }

    @Override // android.view.View
    public boolean dispatchHoverEvent(MotionEvent motionEvent) {
        return this.k0.v(motionEvent) || super.dispatchHoverEvent(motionEvent);
    }

    @Override // android.view.View
    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return super.dispatchKeyEvent(keyEvent);
    }

    @Override // android.view.View
    public void drawableStateChanged() {
        super.drawableStateChanged();
        this.a.setColor(C(this.Z0));
        this.f0.setColor(C(this.Y0));
        this.i0.setColor(C(this.X0));
        this.j0.setColor(C(this.W0));
        for (l74 l74Var : this.o0) {
            if (l74Var.isStateful()) {
                l74Var.setState(getDrawableState());
            }
        }
        if (this.a1.isStateful()) {
            this.a1.setState(getDrawableState());
        }
        this.h0.setColor(C(this.V0));
        this.h0.setAlpha(63);
    }

    public final void e0() {
        if (Y() || getMeasuredWidth() <= 0) {
            return;
        }
        Drawable background = getBackground();
        if (background instanceof RippleDrawable) {
            int O = (int) ((O(this.L0.get(this.N0).floatValue()) * this.R0) + this.A0);
            int m = m();
            int i = this.D0;
            androidx.core.graphics.drawable.a.l(background, O - i, m - i, O + i, m + i);
        }
    }

    public final void f0(int i) {
        this.R0 = Math.max(i - (this.A0 * 2), 0);
        I();
    }

    public final void g0() {
        if (this.U0) {
            i0();
            j0();
            h0();
            k0();
            n0();
            this.U0 = false;
        }
    }

    @Override // android.view.View
    public CharSequence getAccessibilityClassName() {
        return SeekBar.class.getName();
    }

    public final int getAccessibilityFocusedVirtualViewId() {
        return this.k0.x();
    }

    public int getActiveThumbIndex() {
        return this.M0;
    }

    public int getFocusedThumbIndex() {
        return this.N0;
    }

    public int getHaloRadius() {
        return this.D0;
    }

    public ColorStateList getHaloTintList() {
        return this.V0;
    }

    public int getLabelBehavior() {
        return this.y0;
    }

    public float getMinSeparation() {
        return Utils.FLOAT_EPSILON;
    }

    public float getStepSize() {
        return this.O0;
    }

    public float getThumbElevation() {
        return this.a1.w();
    }

    public int getThumbRadius() {
        return this.C0;
    }

    public ColorStateList getThumbStrokeColor() {
        return this.a1.E();
    }

    public float getThumbStrokeWidth() {
        return this.a1.G();
    }

    public ColorStateList getThumbTintList() {
        return this.a1.x();
    }

    public ColorStateList getTickActiveTintList() {
        return this.W0;
    }

    public ColorStateList getTickInactiveTintList() {
        return this.X0;
    }

    public ColorStateList getTickTintList() {
        if (this.X0.equals(this.W0)) {
            return this.W0;
        }
        throw new IllegalStateException("The inactive and active ticks are different colors. Use the getTickColorInactive() and getTickColorActive() methods instead.");
    }

    public ColorStateList getTrackActiveTintList() {
        return this.Y0;
    }

    public int getTrackHeight() {
        return this.z0;
    }

    public ColorStateList getTrackInactiveTintList() {
        return this.Z0;
    }

    public int getTrackSidePadding() {
        return this.A0;
    }

    public ColorStateList getTrackTintList() {
        if (this.Z0.equals(this.Y0)) {
            return this.Y0;
        }
        throw new IllegalStateException("The inactive and active parts of the track are different colors. Use the getInactiveTrackColor() and getActiveTrackColor() methods instead.");
    }

    public int getTrackWidth() {
        return this.R0;
    }

    public float getValueFrom() {
        return this.J0;
    }

    public float getValueTo() {
        return this.K0;
    }

    public List<Float> getValues() {
        return new ArrayList(this.L0);
    }

    public void h(L l) {
        this.p0.add(l);
    }

    public final void h0() {
        if (this.O0 > Utils.FLOAT_EPSILON && !l0(this.K0)) {
            throw new IllegalStateException(String.format("The stepSize(%s) must be 0, or a factor of the valueFrom(%s)-valueTo(%s) range", Float.toString(this.O0), Float.toString(this.J0), Float.toString(this.K0)));
        }
    }

    public final void i(l74 l74Var) {
        l74Var.A0(mk4.d(this));
    }

    public final void i0() {
        if (this.J0 >= this.K0) {
            throw new IllegalStateException(String.format("valueFrom(%s) must be smaller than valueTo(%s)", Float.toString(this.J0), Float.toString(this.K0)));
        }
    }

    public final Float j(int i) {
        float l = this.T0 ? l(20) : k();
        if (i == 21) {
            if (!G()) {
                l = -l;
            }
            return Float.valueOf(l);
        } else if (i == 22) {
            if (G()) {
                l = -l;
            }
            return Float.valueOf(l);
        } else if (i != 69) {
            if (i == 70 || i == 81) {
                return Float.valueOf(l);
            }
            return null;
        } else {
            return Float.valueOf(-l);
        }
    }

    public final void j0() {
        if (this.K0 <= this.J0) {
            throw new IllegalStateException(String.format("valueTo(%s) must be greater than valueFrom(%s)", Float.toString(this.K0), Float.toString(this.J0)));
        }
    }

    public final float k() {
        float f2 = this.O0;
        if (f2 == Utils.FLOAT_EPSILON) {
            return 1.0f;
        }
        return f2;
    }

    public final void k0() {
        Iterator<Float> it = this.L0.iterator();
        while (it.hasNext()) {
            Float next = it.next();
            if (next.floatValue() >= this.J0 && next.floatValue() <= this.K0) {
                if (this.O0 > Utils.FLOAT_EPSILON && !l0(next.floatValue())) {
                    throw new IllegalStateException(String.format("Value(%s) must be equal to valueFrom(%s) plus a multiple of stepSize(%s) when using stepSize(%s)", Float.toString(next.floatValue()), Float.toString(this.J0), Float.toString(this.O0), Float.toString(this.O0)));
                }
            } else {
                throw new IllegalStateException(String.format("Slider value(%s) must be greater or equal to valueFrom(%s), and lower or equal to valueTo(%s)", Float.toString(next.floatValue()), Float.toString(this.J0), Float.toString(this.K0)));
            }
        }
    }

    public final float l(int i) {
        float f2;
        float f3;
        float k = k();
        return (this.K0 - this.J0) / k <= i ? k : Math.round(f2 / f3) * k;
    }

    public final boolean l0(float f2) {
        double doubleValue = new BigDecimal(Float.toString(f2)).subtract(new BigDecimal(Float.toString(this.J0))).divide(new BigDecimal(Float.toString(this.O0)), MathContext.DECIMAL64).doubleValue();
        return Math.abs(((double) Math.round(doubleValue)) - doubleValue) < 1.0E-4d;
    }

    public final int m() {
        return this.B0 + (this.y0 == 1 ? this.o0.get(0).getIntrinsicHeight() : 0);
    }

    public final float m0(float f2) {
        return (O(f2) * this.R0) + this.A0;
    }

    public final ValueAnimator n(boolean z) {
        float f2 = Utils.FLOAT_EPSILON;
        float A = A(z ? this.t0 : this.s0, z ? 0.0f : 1.0f);
        if (z) {
            f2 = 1.0f;
        }
        ValueAnimator ofFloat = ValueAnimator.ofFloat(A, f2);
        ofFloat.setDuration(z ? 83L : 117L);
        ofFloat.setInterpolator(z ? ne.e : ne.c);
        ofFloat.addUpdateListener(new b());
        return ofFloat;
    }

    public final void n0() {
        float f2 = this.O0;
        if (f2 == Utils.FLOAT_EPSILON) {
            return;
        }
        if (((int) f2) != f2) {
            String.format("Floating point value used for %s(%s). Using floats can have rounding errors which may result in incorrect values. Instead, consider using integers with a custom LabelFormatter to display the  value correctly.", "stepSize", Float.valueOf(f2));
        }
        float f3 = this.J0;
        if (((int) f3) != f3) {
            String.format("Floating point value used for %s(%s). Using floats can have rounding errors which may result in incorrect values. Instead, consider using integers with a custom LabelFormatter to display the  value correctly.", "valueFrom", Float.valueOf(f3));
        }
        float f4 = this.K0;
        if (((int) f4) != f4) {
            String.format("Floating point value used for %s(%s). Using floats can have rounding errors which may result in incorrect values. Instead, consider using integers with a custom LabelFormatter to display the  value correctly.", "valueTo", Float.valueOf(f4));
        }
    }

    public final void o() {
        if (this.o0.size() > this.L0.size()) {
            List<l74> subList = this.o0.subList(this.L0.size(), this.o0.size());
            for (l74 l74Var : subList) {
                if (ei4.V(this)) {
                    p(l74Var);
                }
            }
            subList.clear();
        }
        while (this.o0.size() < this.L0.size()) {
            l74 a2 = this.n0.a();
            this.o0.add(a2);
            if (ei4.V(this)) {
                i(a2);
            }
        }
        int i = this.o0.size() == 1 ? 0 : 1;
        for (l74 l74Var2 : this.o0) {
            l74Var2.m0(i);
        }
    }

    @Override // android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        for (l74 l74Var : this.o0) {
            i(l74Var);
        }
    }

    @Override // android.view.View
    public void onDetachedFromWindow() {
        BaseSlider<S, L, T>.d dVar = this.m0;
        if (dVar != null) {
            removeCallbacks(dVar);
        }
        this.r0 = false;
        for (l74 l74Var : this.o0) {
            p(l74Var);
        }
        super.onDetachedFromWindow();
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        if (this.U0) {
            g0();
            I();
        }
        super.onDraw(canvas);
        int m = m();
        u(canvas, this.R0, m);
        if (((Float) Collections.max(getValues())).floatValue() > this.J0) {
            t(canvas, this.R0, m);
        }
        K(canvas);
        if ((this.I0 || isFocused()) && isEnabled()) {
            J(canvas, this.R0, m);
            if (this.M0 != -1) {
                w();
            }
        }
        v(canvas, this.R0, m);
    }

    @Override // android.view.View
    public void onFocusChanged(boolean z, int i, Rect rect) {
        super.onFocusChanged(z, i, rect);
        if (!z) {
            this.M0 = -1;
            x();
            this.k0.o(this.N0);
            return;
        }
        y(i);
        this.k0.V(this.N0);
    }

    @Override // android.view.View, android.view.KeyEvent.Callback
    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (!isEnabled()) {
            return super.onKeyDown(i, keyEvent);
        }
        if (this.L0.size() == 1) {
            this.M0 = 0;
        }
        if (this.M0 == -1) {
            Boolean P = P(i, keyEvent);
            return P != null ? P.booleanValue() : super.onKeyDown(i, keyEvent);
        }
        this.T0 |= keyEvent.isLongPress();
        Float j = j(i);
        if (j != null) {
            if (Z(this.L0.get(this.M0).floatValue() + j.floatValue())) {
                e0();
                postInvalidate();
            }
            return true;
        }
        if (i != 23) {
            if (i == 61) {
                if (keyEvent.hasNoModifiers()) {
                    return M(1);
                }
                if (keyEvent.isShiftPressed()) {
                    return M(-1);
                }
                return false;
            } else if (i != 66) {
                return super.onKeyDown(i, keyEvent);
            }
        }
        this.M0 = -1;
        x();
        postInvalidate();
        return true;
    }

    @Override // android.view.View, android.view.KeyEvent.Callback
    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        this.T0 = false;
        return super.onKeyUp(i, keyEvent);
    }

    @Override // android.view.View
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, View.MeasureSpec.makeMeasureSpec(this.x0 + (this.y0 == 1 ? this.o0.get(0).getIntrinsicHeight() : 0), 1073741824));
    }

    @Override // android.view.View
    public void onRestoreInstanceState(Parcelable parcelable) {
        SliderState sliderState = (SliderState) parcelable;
        super.onRestoreInstanceState(sliderState.getSuperState());
        this.J0 = sliderState.a;
        this.K0 = sliderState.f0;
        setValuesInternal(sliderState.g0);
        this.O0 = sliderState.h0;
        if (sliderState.i0) {
            requestFocus();
        }
        s();
    }

    @Override // android.view.View
    public Parcelable onSaveInstanceState() {
        SliderState sliderState = new SliderState(super.onSaveInstanceState());
        sliderState.a = this.J0;
        sliderState.f0 = this.K0;
        sliderState.g0 = new ArrayList<>(this.L0);
        sliderState.h0 = this.O0;
        sliderState.i0 = hasFocus();
        return sliderState;
    }

    @Override // android.view.View
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        f0(i);
        e0();
    }

    @Override // android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (isEnabled()) {
            float x = motionEvent.getX();
            float f2 = (x - this.A0) / this.R0;
            this.b1 = f2;
            float max = Math.max((float) Utils.FLOAT_EPSILON, f2);
            this.b1 = max;
            this.b1 = Math.min(1.0f, max);
            int actionMasked = motionEvent.getActionMasked();
            if (actionMasked == 0) {
                this.F0 = x;
                if (!F()) {
                    getParent().requestDisallowInterceptTouchEvent(true);
                    if (T()) {
                        requestFocus();
                        this.I0 = true;
                        c0();
                        e0();
                        invalidate();
                        Q();
                    }
                }
            } else if (actionMasked == 1) {
                this.I0 = false;
                MotionEvent motionEvent2 = this.G0;
                if (motionEvent2 != null && motionEvent2.getActionMasked() == 0 && Math.abs(this.G0.getX() - motionEvent.getX()) <= this.u0 && Math.abs(this.G0.getY() - motionEvent.getY()) <= this.u0 && T()) {
                    Q();
                }
                if (this.M0 != -1) {
                    c0();
                    this.M0 = -1;
                    R();
                }
                x();
                invalidate();
            } else if (actionMasked == 2) {
                if (!this.I0) {
                    if (F() && Math.abs(x - this.F0) < this.u0) {
                        return false;
                    }
                    getParent().requestDisallowInterceptTouchEvent(true);
                    Q();
                }
                if (T()) {
                    this.I0 = true;
                    c0();
                    e0();
                    invalidate();
                }
            }
            setPressed(this.I0);
            this.G0 = MotionEvent.obtain(motionEvent);
            return true;
        }
        return false;
    }

    public final void p(l74 l74Var) {
        pj4 e2 = mk4.e(this);
        if (e2 != null) {
            e2.b(l74Var);
            l74Var.w0(mk4.d(this));
        }
    }

    public final float q(float f2) {
        if (f2 == Utils.FLOAT_EPSILON) {
            return Utils.FLOAT_EPSILON;
        }
        float f3 = (f2 - this.A0) / this.R0;
        float f4 = this.J0;
        return (f3 * (f4 - this.K0)) + f4;
    }

    public final void r(int i) {
        for (L l : this.p0) {
            l.a(this, this.L0.get(i).floatValue(), true);
        }
        AccessibilityManager accessibilityManager = this.l0;
        if (accessibilityManager == null || !accessibilityManager.isEnabled()) {
            return;
        }
        W(i);
    }

    public final void s() {
        for (L l : this.p0) {
            Iterator<Float> it = this.L0.iterator();
            while (it.hasNext()) {
                l.a(this, it.next().floatValue(), false);
            }
        }
    }

    public void setActiveThumbIndex(int i) {
        this.M0 = i;
    }

    @Override // android.view.View
    public void setEnabled(boolean z) {
        super.setEnabled(z);
        setLayerType(z ? 0 : 2, null);
    }

    public void setFocusedThumbIndex(int i) {
        if (i >= 0 && i < this.L0.size()) {
            this.N0 = i;
            this.k0.V(i);
            postInvalidate();
            return;
        }
        throw new IllegalArgumentException("index out of range");
    }

    public void setHaloRadius(int i) {
        if (i == this.D0) {
            return;
        }
        this.D0 = i;
        Drawable background = getBackground();
        if (!Y() && (background instanceof RippleDrawable)) {
            cr0.a((RippleDrawable) background, this.D0);
        } else {
            postInvalidate();
        }
    }

    public void setHaloRadiusResource(int i) {
        setHaloRadius(getResources().getDimensionPixelSize(i));
    }

    public void setHaloTintList(ColorStateList colorStateList) {
        if (colorStateList.equals(this.V0)) {
            return;
        }
        this.V0 = colorStateList;
        Drawable background = getBackground();
        if (!Y() && (background instanceof RippleDrawable)) {
            ((RippleDrawable) background).setColor(colorStateList);
            return;
        }
        this.h0.setColor(C(colorStateList));
        this.h0.setAlpha(63);
        invalidate();
    }

    public void setLabelBehavior(int i) {
        if (this.y0 != i) {
            this.y0 = i;
            requestLayout();
        }
    }

    public void setLabelFormatter(by1 by1Var) {
        this.H0 = by1Var;
    }

    public void setSeparationUnit(int i) {
        this.c1 = i;
    }

    public void setStepSize(float f2) {
        if (f2 >= Utils.FLOAT_EPSILON) {
            if (this.O0 != f2) {
                this.O0 = f2;
                this.U0 = true;
                postInvalidate();
                return;
            }
            return;
        }
        throw new IllegalArgumentException(String.format("The stepSize(%s) must be 0, or a factor of the valueFrom(%s)-valueTo(%s) range", Float.toString(f2), Float.toString(this.J0), Float.toString(this.K0)));
    }

    public void setThumbElevation(float f2) {
        this.a1.Z(f2);
    }

    public void setThumbElevationResource(int i) {
        setThumbElevation(getResources().getDimension(i));
    }

    public void setThumbRadius(int i) {
        if (i == this.C0) {
            return;
        }
        this.C0 = i;
        L();
        this.a1.setShapeAppearanceModel(pn3.a().q(0, this.C0).m());
        o42 o42Var = this.a1;
        int i2 = this.C0;
        o42Var.setBounds(0, 0, i2 * 2, i2 * 2);
        postInvalidate();
    }

    public void setThumbRadiusResource(int i) {
        setThumbRadius(getResources().getDimensionPixelSize(i));
    }

    public void setThumbStrokeColor(ColorStateList colorStateList) {
        this.a1.l0(colorStateList);
        postInvalidate();
    }

    public void setThumbStrokeColorResource(int i) {
        if (i != 0) {
            setThumbStrokeColor(mf.c(getContext(), i));
        }
    }

    public void setThumbStrokeWidth(float f2) {
        this.a1.m0(f2);
        postInvalidate();
    }

    public void setThumbStrokeWidthResource(int i) {
        if (i != 0) {
            setThumbStrokeWidth(getResources().getDimension(i));
        }
    }

    public void setThumbTintList(ColorStateList colorStateList) {
        if (colorStateList.equals(this.a1.x())) {
            return;
        }
        this.a1.a0(colorStateList);
        invalidate();
    }

    public void setTickActiveTintList(ColorStateList colorStateList) {
        if (colorStateList.equals(this.W0)) {
            return;
        }
        this.W0 = colorStateList;
        this.j0.setColor(C(colorStateList));
        invalidate();
    }

    public void setTickInactiveTintList(ColorStateList colorStateList) {
        if (colorStateList.equals(this.X0)) {
            return;
        }
        this.X0 = colorStateList;
        this.i0.setColor(C(colorStateList));
        invalidate();
    }

    public void setTickTintList(ColorStateList colorStateList) {
        setTickInactiveTintList(colorStateList);
        setTickActiveTintList(colorStateList);
    }

    public void setTickVisible(boolean z) {
        if (this.Q0 != z) {
            this.Q0 = z;
            postInvalidate();
        }
    }

    public void setTrackActiveTintList(ColorStateList colorStateList) {
        if (colorStateList.equals(this.Y0)) {
            return;
        }
        this.Y0 = colorStateList;
        this.f0.setColor(C(colorStateList));
        invalidate();
    }

    public void setTrackHeight(int i) {
        if (this.z0 != i) {
            this.z0 = i;
            E();
            postInvalidate();
        }
    }

    public void setTrackInactiveTintList(ColorStateList colorStateList) {
        if (colorStateList.equals(this.Z0)) {
            return;
        }
        this.Z0 = colorStateList;
        this.a.setColor(C(colorStateList));
        invalidate();
    }

    public void setTrackTintList(ColorStateList colorStateList) {
        setTrackInactiveTintList(colorStateList);
        setTrackActiveTintList(colorStateList);
    }

    public void setValueFrom(float f2) {
        this.J0 = f2;
        this.U0 = true;
        postInvalidate();
    }

    public void setValueTo(float f2) {
        this.K0 = f2;
        this.U0 = true;
        postInvalidate();
    }

    public void setValues(Float... fArr) {
        ArrayList<Float> arrayList = new ArrayList<>();
        Collections.addAll(arrayList, fArr);
        setValuesInternal(arrayList);
    }

    public final void t(Canvas canvas, int i, int i2) {
        float[] activeRange = getActiveRange();
        int i3 = this.A0;
        float f2 = i;
        float f3 = i2;
        canvas.drawLine(i3 + (activeRange[0] * f2), f3, i3 + (activeRange[1] * f2), f3, this.f0);
    }

    public final void u(Canvas canvas, int i, int i2) {
        int i3;
        float[] activeRange = getActiveRange();
        float f2 = i;
        float f3 = this.A0 + (activeRange[1] * f2);
        if (f3 < i3 + i) {
            float f4 = i2;
            canvas.drawLine(f3, f4, i3 + i, f4, this.a);
        }
        int i4 = this.A0;
        float f5 = i4 + (activeRange[0] * f2);
        if (f5 > i4) {
            float f6 = i2;
            canvas.drawLine(i4, f6, f5, f6, this.a);
        }
    }

    public final void v(Canvas canvas, int i, int i2) {
        if (!isEnabled()) {
            Iterator<Float> it = this.L0.iterator();
            while (it.hasNext()) {
                canvas.drawCircle(this.A0 + (O(it.next().floatValue()) * i), i2, this.C0, this.g0);
            }
        }
        Iterator<Float> it2 = this.L0.iterator();
        while (it2.hasNext()) {
            canvas.save();
            int O = this.A0 + ((int) (O(it2.next().floatValue()) * i));
            int i3 = this.C0;
            canvas.translate(O - i3, i2 - i3);
            this.a1.draw(canvas);
            canvas.restore();
        }
    }

    public final void w() {
        if (this.y0 == 2) {
            return;
        }
        if (!this.r0) {
            this.r0 = true;
            ValueAnimator n = n(true);
            this.s0 = n;
            this.t0 = null;
            n.start();
        }
        Iterator<l74> it = this.o0.iterator();
        for (int i = 0; i < this.L0.size() && it.hasNext(); i++) {
            if (i != this.N0) {
                X(it.next(), this.L0.get(i).floatValue());
            }
        }
        if (it.hasNext()) {
            X(it.next(), this.L0.get(this.N0).floatValue());
            return;
        }
        throw new IllegalStateException(String.format("Not enough labels(%d) to display all the values(%d)", Integer.valueOf(this.o0.size()), Integer.valueOf(this.L0.size())));
    }

    public final void x() {
        if (this.r0) {
            this.r0 = false;
            ValueAnimator n = n(false);
            this.t0 = n;
            this.s0 = null;
            n.addListener(new c());
            this.t0.start();
        }
    }

    public final void y(int i) {
        if (i == 1) {
            M(Integer.MAX_VALUE);
        } else if (i == 2) {
            M(Integer.MIN_VALUE);
        } else if (i == 17) {
            N(Integer.MAX_VALUE);
        } else if (i != 66) {
        } else {
            N(Integer.MIN_VALUE);
        }
    }

    public final String z(float f2) {
        if (D()) {
            return this.H0.a(f2);
        }
        return String.format(((float) ((int) f2)) == f2 ? "%.0f" : "%.2f", Float.valueOf(f2));
    }

    /* loaded from: classes2.dex */
    public class d implements Runnable {
        public int a;

        public d() {
            this.a = -1;
        }

        public void a(int i) {
            this.a = i;
        }

        @Override // java.lang.Runnable
        public void run() {
            BaseSlider.this.k0.W(this.a, 4);
        }

        public /* synthetic */ d(BaseSlider baseSlider, a aVar) {
            this();
        }
    }

    public BaseSlider(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, gy2.sliderStyle);
    }

    public BaseSlider(Context context, AttributeSet attributeSet, int i) {
        super(r42.c(context, attributeSet, i, e1), attributeSet, i);
        this.o0 = new ArrayList();
        this.p0 = new ArrayList();
        this.q0 = new ArrayList();
        this.r0 = false;
        this.I0 = false;
        this.L0 = new ArrayList<>();
        this.M0 = -1;
        this.N0 = -1;
        this.O0 = Utils.FLOAT_EPSILON;
        this.Q0 = true;
        this.T0 = false;
        o42 o42Var = new o42();
        this.a1 = o42Var;
        this.c1 = 0;
        Context context2 = getContext();
        Paint paint = new Paint();
        this.a = paint;
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeCap(Paint.Cap.ROUND);
        Paint paint2 = new Paint();
        this.f0 = paint2;
        paint2.setStyle(Paint.Style.STROKE);
        paint2.setStrokeCap(Paint.Cap.ROUND);
        Paint paint3 = new Paint(1);
        this.g0 = paint3;
        paint3.setStyle(Paint.Style.FILL);
        paint3.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        Paint paint4 = new Paint(1);
        this.h0 = paint4;
        paint4.setStyle(Paint.Style.FILL);
        Paint paint5 = new Paint();
        this.i0 = paint5;
        paint5.setStyle(Paint.Style.STROKE);
        paint5.setStrokeCap(Paint.Cap.ROUND);
        Paint paint6 = new Paint();
        this.j0 = paint6;
        paint6.setStyle(Paint.Style.STROKE);
        paint6.setStrokeCap(Paint.Cap.ROUND);
        H(context2.getResources());
        this.n0 = new a(attributeSet, i);
        V(context2, attributeSet, i);
        setFocusable(true);
        setClickable(true);
        o42Var.i0(2);
        this.u0 = ViewConfiguration.get(context2).getScaledTouchSlop();
        e eVar = new e(this);
        this.k0 = eVar;
        ei4.t0(this, eVar);
        this.l0 = (AccessibilityManager) getContext().getSystemService("accessibility");
    }

    public void setValues(List<Float> list) {
        setValuesInternal(new ArrayList<>(list));
    }
}
