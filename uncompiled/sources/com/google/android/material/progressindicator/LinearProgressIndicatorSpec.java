package com.google.android.material.progressindicator;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;

/* loaded from: classes2.dex */
public final class LinearProgressIndicatorSpec extends wn {
    public int g;
    public int h;
    public boolean i;

    public LinearProgressIndicatorSpec(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, gy2.linearProgressIndicatorStyle);
    }

    @Override // defpackage.wn
    public void e() {
        if (this.g == 0) {
            if (this.b <= 0) {
                if (this.c.length < 3) {
                    throw new IllegalArgumentException("Contiguous indeterminate animation must be used with 3 or more indicator colors.");
                }
                return;
            }
            throw new IllegalArgumentException("Rounded corners are not supported in contiguous indeterminate animation.");
        }
    }

    public LinearProgressIndicatorSpec(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, LinearProgressIndicator.s0);
    }

    public LinearProgressIndicatorSpec(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        TypedArray h = a54.h(context, attributeSet, o23.LinearProgressIndicator, gy2.linearProgressIndicatorStyle, LinearProgressIndicator.s0, new int[0]);
        this.g = h.getInt(o23.LinearProgressIndicator_indeterminateAnimationType, 1);
        this.h = h.getInt(o23.LinearProgressIndicator_indicatorDirectionLinear, 0);
        h.recycle();
        e();
        this.i = this.h == 1;
    }
}
