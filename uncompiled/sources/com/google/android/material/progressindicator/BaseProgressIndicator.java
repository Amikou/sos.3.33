package com.google.android.material.progressindicator;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewParent;
import android.widget.ProgressBar;
import com.github.mikephil.charting.utils.Utils;
import defpackage.wn;
import java.util.Arrays;

/* loaded from: classes2.dex */
public abstract class BaseProgressIndicator<S extends wn> extends ProgressBar {
    public static final int r0 = y13.Widget_MaterialComponents_ProgressIndicator;
    public S a;
    public int f0;
    public boolean g0;
    public boolean h0;
    public final int i0;
    public long j0;
    public pe k0;
    public boolean l0;
    public int m0;
    public final Runnable n0;
    public final Runnable o0;
    public final hd p0;
    public final hd q0;

    /* loaded from: classes2.dex */
    public class a implements Runnable {
        public a() {
        }

        @Override // java.lang.Runnable
        public void run() {
            BaseProgressIndicator.this.k();
        }
    }

    /* loaded from: classes2.dex */
    public class b implements Runnable {
        public b() {
        }

        @Override // java.lang.Runnable
        public void run() {
            BaseProgressIndicator.this.j();
            BaseProgressIndicator.this.j0 = -1L;
        }
    }

    /* loaded from: classes2.dex */
    public class c extends hd {
        public c() {
        }

        @Override // defpackage.hd
        public void onAnimationEnd(Drawable drawable) {
            BaseProgressIndicator.this.setIndeterminate(false);
            BaseProgressIndicator.this.setProgressCompat(0, false);
            BaseProgressIndicator baseProgressIndicator = BaseProgressIndicator.this;
            baseProgressIndicator.setProgressCompat(baseProgressIndicator.f0, BaseProgressIndicator.this.g0);
        }
    }

    /* loaded from: classes2.dex */
    public class d extends hd {
        public d() {
        }

        @Override // defpackage.hd
        public void onAnimationEnd(Drawable drawable) {
            super.onAnimationEnd(drawable);
            if (BaseProgressIndicator.this.l0) {
                return;
            }
            BaseProgressIndicator baseProgressIndicator = BaseProgressIndicator.this;
            baseProgressIndicator.setVisibility(baseProgressIndicator.m0);
        }
    }

    public BaseProgressIndicator(Context context, AttributeSet attributeSet, int i, int i2) {
        super(r42.c(context, attributeSet, i, r0), attributeSet, i);
        this.l0 = false;
        this.m0 = 4;
        this.n0 = new a();
        this.o0 = new b();
        this.p0 = new c();
        this.q0 = new d();
        Context context2 = getContext();
        this.a = i(context2, attributeSet);
        TypedArray h = a54.h(context2, attributeSet, o23.BaseProgressIndicator, i, i2, new int[0]);
        h.getInt(o23.BaseProgressIndicator_showDelay, -1);
        this.i0 = Math.min(h.getInt(o23.BaseProgressIndicator_minHideDelay, -1), 1000);
        h.recycle();
        this.k0 = new pe();
        this.h0 = true;
    }

    private nr0<S> getCurrentDrawingDelegate() {
        if (isIndeterminate()) {
            if (getIndeterminateDrawable() == null) {
                return null;
            }
            return getIndeterminateDrawable().v();
        } else if (getProgressDrawable() == null) {
            return null;
        } else {
            return getProgressDrawable().w();
        }
    }

    @Override // android.widget.ProgressBar
    public Drawable getCurrentDrawable() {
        return isIndeterminate() ? getIndeterminateDrawable() : getProgressDrawable();
    }

    public int getHideAnimationBehavior() {
        return this.a.f;
    }

    public int[] getIndicatorColor() {
        return this.a.c;
    }

    public int getShowAnimationBehavior() {
        return this.a.e;
    }

    public int getTrackColor() {
        return this.a.d;
    }

    public int getTrackCornerRadius() {
        return this.a.b;
    }

    public int getTrackThickness() {
        return this.a.a;
    }

    public void h(boolean z) {
        if (this.h0) {
            ((er0) getCurrentDrawable()).p(p(), false, z);
        }
    }

    public abstract S i(Context context, AttributeSet attributeSet);

    @Override // android.view.View
    public void invalidate() {
        super.invalidate();
        if (getCurrentDrawable() != null) {
            getCurrentDrawable().invalidateSelf();
        }
    }

    public final void j() {
        ((er0) getCurrentDrawable()).p(false, false, true);
        if (m()) {
            setVisibility(4);
        }
    }

    public final void k() {
        if (this.i0 > 0) {
            SystemClock.uptimeMillis();
        }
        setVisibility(0);
    }

    public boolean l() {
        View view = this;
        while (view.getVisibility() == 0) {
            ViewParent parent = view.getParent();
            if (parent == null) {
                return getWindowVisibility() == 0;
            } else if (!(parent instanceof View)) {
                return true;
            } else {
                view = (View) parent;
            }
        }
        return false;
    }

    public final boolean m() {
        return (getProgressDrawable() == null || !getProgressDrawable().isVisible()) && (getIndeterminateDrawable() == null || !getIndeterminateDrawable().isVisible());
    }

    public final void n() {
        if (getProgressDrawable() != null && getIndeterminateDrawable() != null) {
            getIndeterminateDrawable().u().d(this.p0);
        }
        if (getProgressDrawable() != null) {
            getProgressDrawable().l(this.q0);
        }
        if (getIndeterminateDrawable() != null) {
            getIndeterminateDrawable().l(this.q0);
        }
    }

    public final void o() {
        if (getIndeterminateDrawable() != null) {
            getIndeterminateDrawable().r(this.q0);
            getIndeterminateDrawable().u().h();
        }
        if (getProgressDrawable() != null) {
            getProgressDrawable().r(this.q0);
        }
    }

    @Override // android.widget.ProgressBar, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        n();
        if (p()) {
            k();
        }
    }

    @Override // android.widget.ProgressBar, android.view.View
    public void onDetachedFromWindow() {
        removeCallbacks(this.o0);
        removeCallbacks(this.n0);
        ((er0) getCurrentDrawable()).h();
        o();
        super.onDetachedFromWindow();
    }

    @Override // android.widget.ProgressBar, android.view.View
    public synchronized void onDraw(Canvas canvas) {
        int save = canvas.save();
        if (getPaddingLeft() != 0 || getPaddingTop() != 0) {
            canvas.translate(getPaddingLeft(), getPaddingTop());
        }
        if (getPaddingRight() != 0 || getPaddingBottom() != 0) {
            canvas.clipRect(0, 0, getWidth() - (getPaddingLeft() + getPaddingRight()), getHeight() - (getPaddingTop() + getPaddingBottom()));
        }
        getCurrentDrawable().draw(canvas);
        canvas.restoreToCount(save);
    }

    @Override // android.widget.ProgressBar, android.view.View
    public synchronized void onMeasure(int i, int i2) {
        int paddingLeft;
        int paddingTop;
        super.onMeasure(i, i2);
        nr0<S> currentDrawingDelegate = getCurrentDrawingDelegate();
        if (currentDrawingDelegate == null) {
            return;
        }
        int e = currentDrawingDelegate.e();
        int d2 = currentDrawingDelegate.d();
        if (e < 0) {
            paddingLeft = getMeasuredWidth();
        } else {
            paddingLeft = e + getPaddingLeft() + getPaddingRight();
        }
        if (d2 < 0) {
            paddingTop = getMeasuredHeight();
        } else {
            paddingTop = d2 + getPaddingTop() + getPaddingBottom();
        }
        setMeasuredDimension(paddingLeft, paddingTop);
    }

    @Override // android.view.View
    public void onVisibilityChanged(View view, int i) {
        super.onVisibilityChanged(view, i);
        h(i == 0);
    }

    @Override // android.view.View
    public void onWindowVisibilityChanged(int i) {
        super.onWindowVisibilityChanged(i);
        h(false);
    }

    public boolean p() {
        return ei4.V(this) && getWindowVisibility() == 0 && l();
    }

    public void setAnimatorDurationScaleProvider(pe peVar) {
        this.k0 = peVar;
        if (getProgressDrawable() != null) {
            getProgressDrawable().g0 = peVar;
        }
        if (getIndeterminateDrawable() != null) {
            getIndeterminateDrawable().g0 = peVar;
        }
    }

    public void setHideAnimationBehavior(int i) {
        this.a.f = i;
        invalidate();
    }

    @Override // android.widget.ProgressBar
    public synchronized void setIndeterminate(boolean z) {
        if (z == isIndeterminate()) {
            return;
        }
        if (p() && z) {
            throw new IllegalStateException("Cannot switch to indeterminate mode while the progress indicator is visible.");
        }
        er0 er0Var = (er0) getCurrentDrawable();
        if (er0Var != null) {
            er0Var.h();
        }
        super.setIndeterminate(z);
        er0 er0Var2 = (er0) getCurrentDrawable();
        if (er0Var2 != null) {
            er0Var2.p(p(), false, false);
        }
        this.l0 = false;
    }

    @Override // android.widget.ProgressBar
    public void setIndeterminateDrawable(Drawable drawable) {
        if (drawable == null) {
            super.setIndeterminateDrawable(null);
        } else if (drawable instanceof iq1) {
            ((er0) drawable).h();
            super.setIndeterminateDrawable(drawable);
        } else {
            throw new IllegalArgumentException("Cannot set framework drawable as indeterminate drawable.");
        }
    }

    public void setIndicatorColor(int... iArr) {
        if (iArr.length == 0) {
            iArr = new int[]{l42.b(getContext(), gy2.colorPrimary, -1)};
        }
        if (Arrays.equals(getIndicatorColor(), iArr)) {
            return;
        }
        this.a.c = iArr;
        getIndeterminateDrawable().u().c();
        invalidate();
    }

    @Override // android.widget.ProgressBar
    public synchronized void setProgress(int i) {
        if (isIndeterminate()) {
            return;
        }
        setProgressCompat(i, false);
    }

    public void setProgressCompat(int i, boolean z) {
        if (isIndeterminate()) {
            if (getProgressDrawable() != null) {
                this.f0 = i;
                this.g0 = z;
                this.l0 = true;
                if (getIndeterminateDrawable().isVisible() && this.k0.a(getContext().getContentResolver()) != Utils.FLOAT_EPSILON) {
                    getIndeterminateDrawable().u().f();
                    return;
                } else {
                    this.p0.onAnimationEnd(getIndeterminateDrawable());
                    return;
                }
            }
            return;
        }
        super.setProgress(i);
        if (getProgressDrawable() == null || z) {
            return;
        }
        getProgressDrawable().jumpToCurrentState();
    }

    @Override // android.widget.ProgressBar
    public void setProgressDrawable(Drawable drawable) {
        if (drawable == null) {
            super.setProgressDrawable(null);
        } else if (drawable instanceof sm0) {
            sm0 sm0Var = (sm0) drawable;
            sm0Var.h();
            super.setProgressDrawable(sm0Var);
            sm0Var.A(getProgress() / getMax());
        } else {
            throw new IllegalArgumentException("Cannot set framework drawable as progress drawable.");
        }
    }

    public void setShowAnimationBehavior(int i) {
        this.a.e = i;
        invalidate();
    }

    public void setTrackColor(int i) {
        S s = this.a;
        if (s.d != i) {
            s.d = i;
            invalidate();
        }
    }

    public void setTrackCornerRadius(int i) {
        S s = this.a;
        if (s.b != i) {
            s.b = Math.min(i, s.a / 2);
        }
    }

    public void setTrackThickness(int i) {
        S s = this.a;
        if (s.a != i) {
            s.a = i;
            requestLayout();
        }
    }

    public void setVisibilityAfterHide(int i) {
        if (i != 0 && i != 4 && i != 8) {
            throw new IllegalArgumentException("The component's visibility must be one of VISIBLE, INVISIBLE, and GONE defined in View.");
        }
        this.m0 = i;
    }

    @Override // android.widget.ProgressBar
    public iq1<S> getIndeterminateDrawable() {
        return (iq1) super.getIndeterminateDrawable();
    }

    @Override // android.widget.ProgressBar
    public sm0<S> getProgressDrawable() {
        return (sm0) super.getProgressDrawable();
    }
}
