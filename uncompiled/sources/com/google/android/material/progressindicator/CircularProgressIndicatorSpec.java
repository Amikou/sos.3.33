package com.google.android.material.progressindicator;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;

/* loaded from: classes2.dex */
public final class CircularProgressIndicatorSpec extends wn {
    public int g;
    public int h;
    public int i;

    public CircularProgressIndicatorSpec(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, gy2.circularProgressIndicatorStyle);
    }

    @Override // defpackage.wn
    public void e() {
    }

    public CircularProgressIndicatorSpec(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, CircularProgressIndicator.s0);
    }

    public CircularProgressIndicatorSpec(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        int dimensionPixelSize = context.getResources().getDimensionPixelSize(jz2.mtrl_progress_circular_size_medium);
        int dimensionPixelSize2 = context.getResources().getDimensionPixelSize(jz2.mtrl_progress_circular_inset_medium);
        TypedArray h = a54.h(context, attributeSet, o23.CircularProgressIndicator, i, i2, new int[0]);
        this.g = Math.max(n42.c(context, h, o23.CircularProgressIndicator_indicatorSize, dimensionPixelSize), this.a * 2);
        this.h = n42.c(context, h, o23.CircularProgressIndicator_indicatorInset, dimensionPixelSize2);
        this.i = h.getInt(o23.CircularProgressIndicator_indicatorDirectionCircular, 0);
        h.recycle();
        e();
    }
}
