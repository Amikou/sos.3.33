package com.google.android.material.progressindicator;

import android.content.Context;
import android.util.AttributeSet;

/* loaded from: classes2.dex */
public final class CircularProgressIndicator extends BaseProgressIndicator<CircularProgressIndicatorSpec> {
    public static final int s0 = y13.Widget_MaterialComponents_CircularProgressIndicator;

    public CircularProgressIndicator(Context context) {
        this(context, null);
    }

    public int getIndicatorDirection() {
        return ((CircularProgressIndicatorSpec) this.a).i;
    }

    public int getIndicatorInset() {
        return ((CircularProgressIndicatorSpec) this.a).h;
    }

    public int getIndicatorSize() {
        return ((CircularProgressIndicatorSpec) this.a).g;
    }

    @Override // com.google.android.material.progressindicator.BaseProgressIndicator
    /* renamed from: q */
    public CircularProgressIndicatorSpec i(Context context, AttributeSet attributeSet) {
        return new CircularProgressIndicatorSpec(context, attributeSet);
    }

    public final void r() {
        setIndeterminateDrawable(iq1.s(getContext(), (CircularProgressIndicatorSpec) this.a));
        setProgressDrawable(sm0.u(getContext(), (CircularProgressIndicatorSpec) this.a));
    }

    public void setIndicatorDirection(int i) {
        ((CircularProgressIndicatorSpec) this.a).i = i;
        invalidate();
    }

    public void setIndicatorInset(int i) {
        S s = this.a;
        if (((CircularProgressIndicatorSpec) s).h != i) {
            ((CircularProgressIndicatorSpec) s).h = i;
            invalidate();
        }
    }

    public void setIndicatorSize(int i) {
        int max = Math.max(i, getTrackThickness() * 2);
        S s = this.a;
        if (((CircularProgressIndicatorSpec) s).g != max) {
            ((CircularProgressIndicatorSpec) s).g = max;
            ((CircularProgressIndicatorSpec) s).e();
            invalidate();
        }
    }

    @Override // com.google.android.material.progressindicator.BaseProgressIndicator
    public void setTrackThickness(int i) {
        super.setTrackThickness(i);
        ((CircularProgressIndicatorSpec) this.a).e();
    }

    public CircularProgressIndicator(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, gy2.circularProgressIndicatorStyle);
    }

    public CircularProgressIndicator(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i, s0);
        r();
    }
}
