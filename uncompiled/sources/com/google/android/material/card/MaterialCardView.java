package com.google.android.material.card;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.Checkable;
import android.widget.FrameLayout;
import androidx.cardview.widget.CardView;

/* loaded from: classes2.dex */
public class MaterialCardView extends CardView implements Checkable, sn3 {
    public static final int[] s0 = {16842911};
    public static final int[] t0 = {16842912};
    public static final int[] u0 = {gy2.state_dragged};
    public static final int v0 = y13.Widget_MaterialComponents_CardView;
    public final k42 n0;
    public boolean o0;
    public boolean p0;
    public boolean q0;
    public a r0;

    /* loaded from: classes2.dex */
    public interface a {
        void a(MaterialCardView materialCardView, boolean z);
    }

    public MaterialCardView(Context context) {
        this(context, null);
    }

    private RectF getBoundsAsRectF() {
        RectF rectF = new RectF();
        rectF.set(this.n0.k().getBounds());
        return rectF;
    }

    @Override // androidx.cardview.widget.CardView
    public ColorStateList getCardBackgroundColor() {
        return this.n0.l();
    }

    public ColorStateList getCardForegroundColor() {
        return this.n0.m();
    }

    public float getCardViewRadius() {
        return super.getRadius();
    }

    public Drawable getCheckedIcon() {
        return this.n0.n();
    }

    public int getCheckedIconMargin() {
        return this.n0.o();
    }

    public int getCheckedIconSize() {
        return this.n0.p();
    }

    public ColorStateList getCheckedIconTint() {
        return this.n0.q();
    }

    @Override // androidx.cardview.widget.CardView
    public int getContentPaddingBottom() {
        return this.n0.A().bottom;
    }

    @Override // androidx.cardview.widget.CardView
    public int getContentPaddingLeft() {
        return this.n0.A().left;
    }

    @Override // androidx.cardview.widget.CardView
    public int getContentPaddingRight() {
        return this.n0.A().right;
    }

    @Override // androidx.cardview.widget.CardView
    public int getContentPaddingTop() {
        return this.n0.A().top;
    }

    public float getProgress() {
        return this.n0.u();
    }

    @Override // androidx.cardview.widget.CardView
    public float getRadius() {
        return this.n0.s();
    }

    public ColorStateList getRippleColor() {
        return this.n0.v();
    }

    public pn3 getShapeAppearanceModel() {
        return this.n0.w();
    }

    @Deprecated
    public int getStrokeColor() {
        return this.n0.x();
    }

    public ColorStateList getStrokeColorStateList() {
        return this.n0.y();
    }

    public int getStrokeWidth() {
        return this.n0.z();
    }

    public final void i() {
        if (Build.VERSION.SDK_INT > 26) {
            this.n0.j();
        }
    }

    @Override // android.widget.Checkable
    public boolean isChecked() {
        return this.p0;
    }

    public boolean j() {
        k42 k42Var = this.n0;
        return k42Var != null && k42Var.D();
    }

    public boolean k() {
        return this.q0;
    }

    public void l(int i, int i2, int i3, int i4) {
        super.setContentPadding(i, i2, i3, i4);
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        p42.f(this, this.n0.k());
    }

    @Override // android.view.ViewGroup, android.view.View
    public int[] onCreateDrawableState(int i) {
        int[] onCreateDrawableState = super.onCreateDrawableState(i + 3);
        if (j()) {
            FrameLayout.mergeDrawableStates(onCreateDrawableState, s0);
        }
        if (isChecked()) {
            FrameLayout.mergeDrawableStates(onCreateDrawableState, t0);
        }
        if (k()) {
            FrameLayout.mergeDrawableStates(onCreateDrawableState, u0);
        }
        return onCreateDrawableState;
    }

    @Override // android.view.View
    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        accessibilityEvent.setClassName("androidx.cardview.widget.CardView");
        accessibilityEvent.setChecked(isChecked());
    }

    @Override // android.view.View
    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        accessibilityNodeInfo.setClassName("androidx.cardview.widget.CardView");
        accessibilityNodeInfo.setCheckable(j());
        accessibilityNodeInfo.setClickable(isClickable());
        accessibilityNodeInfo.setChecked(isChecked());
    }

    @Override // androidx.cardview.widget.CardView, android.widget.FrameLayout, android.view.View
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        this.n0.F(getMeasuredWidth(), getMeasuredHeight());
    }

    @Override // android.view.View
    public void setBackground(Drawable drawable) {
        setBackgroundDrawable(drawable);
    }

    @Override // android.view.View
    public void setBackgroundDrawable(Drawable drawable) {
        if (this.o0) {
            if (!this.n0.C()) {
                this.n0.G(true);
            }
            super.setBackgroundDrawable(drawable);
        }
    }

    public void setBackgroundInternal(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
    }

    @Override // androidx.cardview.widget.CardView
    public void setCardBackgroundColor(int i) {
        this.n0.H(ColorStateList.valueOf(i));
    }

    @Override // androidx.cardview.widget.CardView
    public void setCardElevation(float f) {
        super.setCardElevation(f);
        this.n0.Z();
    }

    public void setCardForegroundColor(ColorStateList colorStateList) {
        this.n0.I(colorStateList);
    }

    public void setCheckable(boolean z) {
        this.n0.J(z);
    }

    @Override // android.widget.Checkable
    public void setChecked(boolean z) {
        if (this.p0 != z) {
            toggle();
        }
    }

    public void setCheckedIcon(Drawable drawable) {
        this.n0.K(drawable);
    }

    public void setCheckedIconMargin(int i) {
        this.n0.L(i);
    }

    public void setCheckedIconMarginResource(int i) {
        if (i != -1) {
            this.n0.L(getResources().getDimensionPixelSize(i));
        }
    }

    public void setCheckedIconResource(int i) {
        this.n0.K(mf.d(getContext(), i));
    }

    public void setCheckedIconSize(int i) {
        this.n0.M(i);
    }

    public void setCheckedIconSizeResource(int i) {
        if (i != 0) {
            this.n0.M(getResources().getDimensionPixelSize(i));
        }
    }

    public void setCheckedIconTint(ColorStateList colorStateList) {
        this.n0.N(colorStateList);
    }

    @Override // android.view.View
    public void setClickable(boolean z) {
        super.setClickable(z);
        k42 k42Var = this.n0;
        if (k42Var != null) {
            k42Var.X();
        }
    }

    @Override // androidx.cardview.widget.CardView
    public void setContentPadding(int i, int i2, int i3, int i4) {
        this.n0.U(i, i2, i3, i4);
    }

    public void setDragged(boolean z) {
        if (this.q0 != z) {
            this.q0 = z;
            refreshDrawableState();
            i();
            invalidate();
        }
    }

    @Override // androidx.cardview.widget.CardView
    public void setMaxCardElevation(float f) {
        super.setMaxCardElevation(f);
        this.n0.b0();
    }

    public void setOnCheckedChangeListener(a aVar) {
        this.r0 = aVar;
    }

    @Override // androidx.cardview.widget.CardView
    public void setPreventCornerOverlap(boolean z) {
        super.setPreventCornerOverlap(z);
        this.n0.b0();
        this.n0.Y();
    }

    public void setProgress(float f) {
        this.n0.P(f);
    }

    @Override // androidx.cardview.widget.CardView
    public void setRadius(float f) {
        super.setRadius(f);
        this.n0.O(f);
    }

    public void setRippleColor(ColorStateList colorStateList) {
        this.n0.Q(colorStateList);
    }

    public void setRippleColorResource(int i) {
        this.n0.Q(mf.c(getContext(), i));
    }

    @Override // defpackage.sn3
    public void setShapeAppearanceModel(pn3 pn3Var) {
        if (Build.VERSION.SDK_INT >= 21) {
            setClipToOutline(pn3Var.u(getBoundsAsRectF()));
        }
        this.n0.R(pn3Var);
    }

    public void setStrokeColor(int i) {
        this.n0.S(ColorStateList.valueOf(i));
    }

    public void setStrokeWidth(int i) {
        this.n0.T(i);
    }

    @Override // androidx.cardview.widget.CardView
    public void setUseCompatPadding(boolean z) {
        super.setUseCompatPadding(z);
        this.n0.b0();
        this.n0.Y();
    }

    @Override // android.widget.Checkable
    public void toggle() {
        if (j() && isEnabled()) {
            this.p0 = !this.p0;
            refreshDrawableState();
            i();
            a aVar = this.r0;
            if (aVar != null) {
                aVar.a(this, this.p0);
            }
        }
    }

    public MaterialCardView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, gy2.materialCardViewStyle);
    }

    @Override // androidx.cardview.widget.CardView
    public void setCardBackgroundColor(ColorStateList colorStateList) {
        this.n0.H(colorStateList);
    }

    public void setStrokeColor(ColorStateList colorStateList) {
        this.n0.S(colorStateList);
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public MaterialCardView(android.content.Context r8, android.util.AttributeSet r9, int r10) {
        /*
            r7 = this;
            int r6 = com.google.android.material.card.MaterialCardView.v0
            android.content.Context r8 = defpackage.r42.c(r8, r9, r10, r6)
            r7.<init>(r8, r9, r10)
            r8 = 0
            r7.p0 = r8
            r7.q0 = r8
            r0 = 1
            r7.o0 = r0
            android.content.Context r0 = r7.getContext()
            int[] r2 = defpackage.o23.MaterialCardView
            int[] r5 = new int[r8]
            r1 = r9
            r3 = r10
            r4 = r6
            android.content.res.TypedArray r8 = defpackage.a54.h(r0, r1, r2, r3, r4, r5)
            k42 r0 = new k42
            r0.<init>(r7, r9, r10, r6)
            r7.n0 = r0
            android.content.res.ColorStateList r9 = super.getCardBackgroundColor()
            r0.H(r9)
            int r9 = super.getContentPaddingLeft()
            int r10 = super.getContentPaddingTop()
            int r1 = super.getContentPaddingRight()
            int r2 = super.getContentPaddingBottom()
            r0.U(r9, r10, r1, r2)
            r0.E(r8)
            r8.recycle()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.card.MaterialCardView.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }
}
