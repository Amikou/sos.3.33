package com.google.android.material.circularreveal;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.View;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.material.circularreveal.c;

/* compiled from: CircularRevealHelper.java */
/* loaded from: classes2.dex */
public class b {
    public static final int j;
    public final a a;
    public final View b;
    public final Path c;
    public final Paint d;
    public final Paint e;
    public c.e f;
    public Drawable g;
    public boolean h;
    public boolean i;

    /* compiled from: CircularRevealHelper.java */
    /* loaded from: classes2.dex */
    public interface a {
        void c(Canvas canvas);

        boolean d();
    }

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 21) {
            j = 2;
        } else if (i >= 18) {
            j = 1;
        } else {
            j = 0;
        }
    }

    public b(a aVar) {
        this.a = aVar;
        View view = (View) aVar;
        this.b = view;
        view.setWillNotDraw(false);
        this.c = new Path();
        this.d = new Paint(7);
        Paint paint = new Paint(1);
        this.e = paint;
        paint.setColor(0);
    }

    public void a() {
        if (j == 0) {
            this.h = true;
            this.i = false;
            this.b.buildDrawingCache();
            Bitmap drawingCache = this.b.getDrawingCache();
            if (drawingCache == null && this.b.getWidth() != 0 && this.b.getHeight() != 0) {
                drawingCache = Bitmap.createBitmap(this.b.getWidth(), this.b.getHeight(), Bitmap.Config.ARGB_8888);
                this.b.draw(new Canvas(drawingCache));
            }
            if (drawingCache != null) {
                Paint paint = this.d;
                Shader.TileMode tileMode = Shader.TileMode.CLAMP;
                paint.setShader(new BitmapShader(drawingCache, tileMode, tileMode));
            }
            this.h = false;
            this.i = true;
        }
    }

    public void b() {
        if (j == 0) {
            this.i = false;
            this.b.destroyDrawingCache();
            this.d.setShader(null);
            this.b.invalidate();
        }
    }

    public void c(Canvas canvas) {
        if (n()) {
            int i = j;
            if (i == 0) {
                c.e eVar = this.f;
                canvas.drawCircle(eVar.a, eVar.b, eVar.c, this.d);
                if (p()) {
                    c.e eVar2 = this.f;
                    canvas.drawCircle(eVar2.a, eVar2.b, eVar2.c, this.e);
                }
            } else if (i == 1) {
                int save = canvas.save();
                canvas.clipPath(this.c);
                this.a.c(canvas);
                if (p()) {
                    canvas.drawRect(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, this.b.getWidth(), this.b.getHeight(), this.e);
                }
                canvas.restoreToCount(save);
            } else if (i == 2) {
                this.a.c(canvas);
                if (p()) {
                    canvas.drawRect(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, this.b.getWidth(), this.b.getHeight(), this.e);
                }
            } else {
                throw new IllegalStateException("Unsupported strategy " + i);
            }
        } else {
            this.a.c(canvas);
            if (p()) {
                canvas.drawRect(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, this.b.getWidth(), this.b.getHeight(), this.e);
            }
        }
        d(canvas);
    }

    public final void d(Canvas canvas) {
        if (o()) {
            Rect bounds = this.g.getBounds();
            float width = this.f.a - (bounds.width() / 2.0f);
            float height = this.f.b - (bounds.height() / 2.0f);
            canvas.translate(width, height);
            this.g.draw(canvas);
            canvas.translate(-width, -height);
        }
    }

    public Drawable e() {
        return this.g;
    }

    public int f() {
        return this.e.getColor();
    }

    public final float g(c.e eVar) {
        return w42.b(eVar.a, eVar.b, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, this.b.getWidth(), this.b.getHeight());
    }

    public c.e h() {
        c.e eVar = this.f;
        if (eVar == null) {
            return null;
        }
        c.e eVar2 = new c.e(eVar);
        if (eVar2.a()) {
            eVar2.c = g(eVar2);
        }
        return eVar2;
    }

    public final void i() {
        if (j == 1) {
            this.c.rewind();
            c.e eVar = this.f;
            if (eVar != null) {
                this.c.addCircle(eVar.a, eVar.b, eVar.c, Path.Direction.CW);
            }
        }
        this.b.invalidate();
    }

    public boolean j() {
        return this.a.d() && !n();
    }

    public void k(Drawable drawable) {
        this.g = drawable;
        this.b.invalidate();
    }

    public void l(int i) {
        this.e.setColor(i);
        this.b.invalidate();
    }

    public void m(c.e eVar) {
        if (eVar == null) {
            this.f = null;
        } else {
            c.e eVar2 = this.f;
            if (eVar2 == null) {
                this.f = new c.e(eVar);
            } else {
                eVar2.c(eVar);
            }
            if (w42.c(eVar.c, g(eVar), 1.0E-4f)) {
                this.f.c = Float.MAX_VALUE;
            }
        }
        i();
    }

    public final boolean n() {
        c.e eVar = this.f;
        boolean z = eVar == null || eVar.a();
        return j == 0 ? !z && this.i : !z;
    }

    public final boolean o() {
        return (this.h || this.g == null || this.f == null) ? false : true;
    }

    public final boolean p() {
        return (this.h || Color.alpha(this.e.getColor()) == 0) ? false : true;
    }
}
