package com.google.android.material.chip;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Outline;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.PointerIcon;
import android.view.View;
import android.view.ViewOutlineProvider;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.media3.common.PlaybackException;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.material.chip.a;
import defpackage.b6;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

/* loaded from: classes2.dex */
public class Chip extends AppCompatCheckBox implements a.InterfaceC0116a, sn3 {
    public com.google.android.material.chip.a h0;
    public InsetDrawable i0;
    public RippleDrawable j0;
    public View.OnClickListener k0;
    public CompoundButton.OnCheckedChangeListener l0;
    public boolean m0;
    public boolean n0;
    public boolean o0;
    public boolean p0;
    public boolean q0;
    public int r0;
    public int s0;
    public final c t0;
    public final Rect u0;
    public final RectF v0;
    public final f44 w0;
    public static final int x0 = y13.Widget_MaterialComponents_Chip_Action;
    public static final Rect y0 = new Rect();
    public static final int[] z0 = {16842913};
    public static final int[] A0 = {16842911};

    /* loaded from: classes2.dex */
    public class a extends f44 {
        public a() {
        }

        @Override // defpackage.f44
        public void a(int i) {
        }

        @Override // defpackage.f44
        public void b(Typeface typeface, boolean z) {
            Chip chip = Chip.this;
            chip.setText(chip.h0.R2() ? Chip.this.h0.n1() : Chip.this.getText());
            Chip.this.requestLayout();
            Chip.this.invalidate();
        }
    }

    /* loaded from: classes2.dex */
    public class b extends ViewOutlineProvider {
        public b() {
        }

        @Override // android.view.ViewOutlineProvider
        @TargetApi(21)
        public void getOutline(View view, Outline outline) {
            if (Chip.this.h0 != null) {
                Chip.this.h0.getOutline(outline);
            } else {
                outline.setAlpha(Utils.FLOAT_EPSILON);
            }
        }
    }

    /* loaded from: classes2.dex */
    public class c extends c11 {
        public c(Chip chip) {
            super(chip);
        }

        @Override // defpackage.c11
        public int B(float f, float f2) {
            return (Chip.this.n() && Chip.this.getCloseIconTouchBounds().contains(f, f2)) ? 1 : 0;
        }

        @Override // defpackage.c11
        public void C(List<Integer> list) {
            list.add(0);
            if (Chip.this.n() && Chip.this.s() && Chip.this.k0 != null) {
                list.add(1);
            }
        }

        @Override // defpackage.c11
        public boolean L(int i, int i2, Bundle bundle) {
            if (i2 == 16) {
                if (i == 0) {
                    return Chip.this.performClick();
                }
                if (i == 1) {
                    return Chip.this.t();
                }
                return false;
            }
            return false;
        }

        @Override // defpackage.c11
        public void O(b6 b6Var) {
            b6Var.a0(Chip.this.r());
            b6Var.d0(Chip.this.isClickable());
            if (!Chip.this.r() && !Chip.this.isClickable()) {
                b6Var.c0("android.view.View");
            } else {
                b6Var.c0(Chip.this.r() ? "android.widget.CompoundButton" : "android.widget.Button");
            }
            CharSequence text = Chip.this.getText();
            if (Build.VERSION.SDK_INT >= 23) {
                b6Var.E0(text);
            } else {
                b6Var.g0(text);
            }
        }

        @Override // defpackage.c11
        public void P(int i, b6 b6Var) {
            if (i == 1) {
                CharSequence closeIconContentDescription = Chip.this.getCloseIconContentDescription();
                if (closeIconContentDescription != null) {
                    b6Var.g0(closeIconContentDescription);
                } else {
                    CharSequence text = Chip.this.getText();
                    Context context = Chip.this.getContext();
                    int i2 = r13.mtrl_chip_close_icon_content_description;
                    Object[] objArr = new Object[1];
                    objArr[0] = TextUtils.isEmpty(text) ? "" : text;
                    b6Var.g0(context.getString(i2, objArr).trim());
                }
                b6Var.X(Chip.this.getCloseIconTouchBoundsInt());
                b6Var.b(b6.a.g);
                b6Var.i0(Chip.this.isEnabled());
                return;
            }
            b6Var.g0("");
            b6Var.X(Chip.y0);
        }

        @Override // defpackage.c11
        public void Q(int i, boolean z) {
            if (i == 1) {
                Chip.this.p0 = z;
                Chip.this.refreshDrawableState();
            }
        }
    }

    public Chip(Context context) {
        this(context, null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public RectF getCloseIconTouchBounds() {
        this.v0.setEmpty();
        if (n() && this.k0 != null) {
            this.h0.e1(this.v0);
        }
        return this.v0;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public Rect getCloseIconTouchBoundsInt() {
        RectF closeIconTouchBounds = getCloseIconTouchBounds();
        this.u0.set((int) closeIconTouchBounds.left, (int) closeIconTouchBounds.top, (int) closeIconTouchBounds.right, (int) closeIconTouchBounds.bottom);
        return this.u0;
    }

    private d44 getTextAppearance() {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            return aVar.o1();
        }
        return null;
    }

    private void setCloseIconHovered(boolean z) {
        if (this.o0 != z) {
            this.o0 = z;
            refreshDrawableState();
        }
    }

    private void setCloseIconPressed(boolean z) {
        if (this.n0 != z) {
            this.n0 = z;
            refreshDrawableState();
        }
    }

    public final void A() {
        com.google.android.material.chip.a aVar;
        if (TextUtils.isEmpty(getText()) || (aVar = this.h0) == null) {
            return;
        }
        int P0 = (int) (aVar.P0() + this.h0.p1() + this.h0.w0());
        int U0 = (int) (this.h0.U0() + this.h0.q1() + this.h0.s0());
        if (this.i0 != null) {
            Rect rect = new Rect();
            this.i0.getPadding(rect);
            U0 += rect.left;
            P0 += rect.right;
        }
        ei4.H0(this, U0, getPaddingTop(), P0, getPaddingBottom());
    }

    public final void B() {
        TextPaint paint = getPaint();
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            paint.drawableState = aVar.getState();
        }
        d44 textAppearance = getTextAppearance();
        if (textAppearance != null) {
            textAppearance.j(getContext(), paint, this.w0);
        }
    }

    public final void C(AttributeSet attributeSet) {
        if (attributeSet == null) {
            return;
        }
        attributeSet.getAttributeValue("http://schemas.android.com/apk/res/android", "background");
        if (attributeSet.getAttributeValue("http://schemas.android.com/apk/res/android", "drawableLeft") == null) {
            if (attributeSet.getAttributeValue("http://schemas.android.com/apk/res/android", "drawableStart") == null) {
                if (attributeSet.getAttributeValue("http://schemas.android.com/apk/res/android", "drawableEnd") == null) {
                    if (attributeSet.getAttributeValue("http://schemas.android.com/apk/res/android", "drawableRight") == null) {
                        if (attributeSet.getAttributeBooleanValue("http://schemas.android.com/apk/res/android", "singleLine", true) && attributeSet.getAttributeIntValue("http://schemas.android.com/apk/res/android", "lines", 1) == 1 && attributeSet.getAttributeIntValue("http://schemas.android.com/apk/res/android", "minLines", 1) == 1 && attributeSet.getAttributeIntValue("http://schemas.android.com/apk/res/android", "maxLines", 1) == 1) {
                            attributeSet.getAttributeIntValue("http://schemas.android.com/apk/res/android", "gravity", 8388627);
                            return;
                        }
                        throw new UnsupportedOperationException("Chip does not support multi-line text");
                    }
                    throw new UnsupportedOperationException("Please set end drawable using R.attr#closeIcon.");
                }
                throw new UnsupportedOperationException("Please set end drawable using R.attr#closeIcon.");
            }
            throw new UnsupportedOperationException("Please set start drawable using R.attr#chipIcon.");
        }
        throw new UnsupportedOperationException("Please set left drawable using R.attr#chipIcon.");
    }

    @Override // com.google.android.material.chip.a.InterfaceC0116a
    public void a() {
        k(this.s0);
        requestLayout();
        if (Build.VERSION.SDK_INT >= 21) {
            invalidateOutline();
        }
    }

    @Override // android.view.View
    public boolean dispatchHoverEvent(MotionEvent motionEvent) {
        return m(motionEvent) || this.t0.v(motionEvent) || super.dispatchHoverEvent(motionEvent);
    }

    @Override // android.view.View
    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (!this.t0.w(keyEvent) || this.t0.A() == Integer.MIN_VALUE) {
            return super.dispatchKeyEvent(keyEvent);
        }
        return true;
    }

    @Override // androidx.appcompat.widget.AppCompatCheckBox, android.widget.CompoundButton, android.widget.TextView, android.view.View
    public void drawableStateChanged() {
        super.drawableStateChanged();
        com.google.android.material.chip.a aVar = this.h0;
        if ((aVar == null || !aVar.v1()) ? false : this.h0.r2(j())) {
            invalidate();
        }
    }

    public Drawable getBackgroundDrawable() {
        InsetDrawable insetDrawable = this.i0;
        return insetDrawable == null ? this.h0 : insetDrawable;
    }

    public Drawable getCheckedIcon() {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            return aVar.L0();
        }
        return null;
    }

    public ColorStateList getCheckedIconTint() {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            return aVar.M0();
        }
        return null;
    }

    public ColorStateList getChipBackgroundColor() {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            return aVar.N0();
        }
        return null;
    }

    public float getChipCornerRadius() {
        com.google.android.material.chip.a aVar = this.h0;
        return aVar != null ? Math.max((float) Utils.FLOAT_EPSILON, aVar.O0()) : Utils.FLOAT_EPSILON;
    }

    public Drawable getChipDrawable() {
        return this.h0;
    }

    public float getChipEndPadding() {
        com.google.android.material.chip.a aVar = this.h0;
        return aVar != null ? aVar.P0() : Utils.FLOAT_EPSILON;
    }

    public Drawable getChipIcon() {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            return aVar.Q0();
        }
        return null;
    }

    public float getChipIconSize() {
        com.google.android.material.chip.a aVar = this.h0;
        return aVar != null ? aVar.R0() : Utils.FLOAT_EPSILON;
    }

    public ColorStateList getChipIconTint() {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            return aVar.S0();
        }
        return null;
    }

    public float getChipMinHeight() {
        com.google.android.material.chip.a aVar = this.h0;
        return aVar != null ? aVar.T0() : Utils.FLOAT_EPSILON;
    }

    public float getChipStartPadding() {
        com.google.android.material.chip.a aVar = this.h0;
        return aVar != null ? aVar.U0() : Utils.FLOAT_EPSILON;
    }

    public ColorStateList getChipStrokeColor() {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            return aVar.V0();
        }
        return null;
    }

    public float getChipStrokeWidth() {
        com.google.android.material.chip.a aVar = this.h0;
        return aVar != null ? aVar.W0() : Utils.FLOAT_EPSILON;
    }

    @Deprecated
    public CharSequence getChipText() {
        return getText();
    }

    public Drawable getCloseIcon() {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            return aVar.X0();
        }
        return null;
    }

    public CharSequence getCloseIconContentDescription() {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            return aVar.Y0();
        }
        return null;
    }

    public float getCloseIconEndPadding() {
        com.google.android.material.chip.a aVar = this.h0;
        return aVar != null ? aVar.Z0() : Utils.FLOAT_EPSILON;
    }

    public float getCloseIconSize() {
        com.google.android.material.chip.a aVar = this.h0;
        return aVar != null ? aVar.a1() : Utils.FLOAT_EPSILON;
    }

    public float getCloseIconStartPadding() {
        com.google.android.material.chip.a aVar = this.h0;
        return aVar != null ? aVar.b1() : Utils.FLOAT_EPSILON;
    }

    public ColorStateList getCloseIconTint() {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            return aVar.d1();
        }
        return null;
    }

    @Override // android.widget.TextView
    public TextUtils.TruncateAt getEllipsize() {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            return aVar.h1();
        }
        return null;
    }

    @Override // android.widget.TextView, android.view.View
    public void getFocusedRect(Rect rect) {
        if (this.t0.A() != 1 && this.t0.x() != 1) {
            super.getFocusedRect(rect);
        } else {
            rect.set(getCloseIconTouchBoundsInt());
        }
    }

    public z92 getHideMotionSpec() {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            return aVar.i1();
        }
        return null;
    }

    public float getIconEndPadding() {
        com.google.android.material.chip.a aVar = this.h0;
        return aVar != null ? aVar.j1() : Utils.FLOAT_EPSILON;
    }

    public float getIconStartPadding() {
        com.google.android.material.chip.a aVar = this.h0;
        return aVar != null ? aVar.k1() : Utils.FLOAT_EPSILON;
    }

    public ColorStateList getRippleColor() {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            return aVar.l1();
        }
        return null;
    }

    public pn3 getShapeAppearanceModel() {
        return this.h0.D();
    }

    public z92 getShowMotionSpec() {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            return aVar.m1();
        }
        return null;
    }

    public float getTextEndPadding() {
        com.google.android.material.chip.a aVar = this.h0;
        return aVar != null ? aVar.p1() : Utils.FLOAT_EPSILON;
    }

    public float getTextStartPadding() {
        com.google.android.material.chip.a aVar = this.h0;
        return aVar != null ? aVar.q1() : Utils.FLOAT_EPSILON;
    }

    public final void i(com.google.android.material.chip.a aVar) {
        aVar.v2(this);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [int, boolean] */
    public final int[] j() {
        ?? isEnabled = isEnabled();
        int i = isEnabled;
        if (this.p0) {
            i = isEnabled + 1;
        }
        int i2 = i;
        if (this.o0) {
            i2 = i + 1;
        }
        int i3 = i2;
        if (this.n0) {
            i3 = i2 + 1;
        }
        int i4 = i3;
        if (isChecked()) {
            i4 = i3 + 1;
        }
        int[] iArr = new int[i4];
        int i5 = 0;
        if (isEnabled()) {
            iArr[0] = 16842910;
            i5 = 1;
        }
        if (this.p0) {
            iArr[i5] = 16842908;
            i5++;
        }
        if (this.o0) {
            iArr[i5] = 16843623;
            i5++;
        }
        if (this.n0) {
            iArr[i5] = 16842919;
            i5++;
        }
        if (isChecked()) {
            iArr[i5] = 16842913;
        }
        return iArr;
    }

    public boolean k(int i) {
        this.s0 = i;
        if (!v()) {
            if (this.i0 != null) {
                u();
            } else {
                y();
            }
            return false;
        }
        int max = Math.max(0, i - this.h0.getIntrinsicHeight());
        int max2 = Math.max(0, i - this.h0.getIntrinsicWidth());
        if (max2 <= 0 && max <= 0) {
            if (this.i0 != null) {
                u();
            } else {
                y();
            }
            return false;
        }
        int i2 = max2 > 0 ? max2 / 2 : 0;
        int i3 = max > 0 ? max / 2 : 0;
        if (this.i0 != null) {
            Rect rect = new Rect();
            this.i0.getPadding(rect);
            if (rect.top == i3 && rect.bottom == i3 && rect.left == i2 && rect.right == i2) {
                y();
                return true;
            }
        }
        if (Build.VERSION.SDK_INT >= 16) {
            if (getMinHeight() != i) {
                setMinHeight(i);
            }
            if (getMinWidth() != i) {
                setMinWidth(i);
            }
        } else {
            setMinHeight(i);
            setMinWidth(i);
        }
        q(i2, i3, i2, i3);
        y();
        return true;
    }

    public final void l() {
        if (getBackgroundDrawable() == this.i0 && this.h0.getCallback() == null) {
            this.h0.setCallback(this.i0);
        }
    }

    @SuppressLint({"PrivateApi"})
    public final boolean m(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 10) {
            try {
                Field declaredField = c11.class.getDeclaredField("m");
                declaredField.setAccessible(true);
                if (((Integer) declaredField.get(this.t0)).intValue() != Integer.MIN_VALUE) {
                    Method declaredMethod = c11.class.getDeclaredMethod("X", Integer.TYPE);
                    declaredMethod.setAccessible(true);
                    declaredMethod.invoke(this.t0, Integer.MIN_VALUE);
                    return true;
                }
            } catch (IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException unused) {
            }
        }
        return false;
    }

    public final boolean n() {
        com.google.android.material.chip.a aVar = this.h0;
        return (aVar == null || aVar.X0() == null) ? false : true;
    }

    public final void o(Context context, AttributeSet attributeSet, int i) {
        TypedArray h = a54.h(context, attributeSet, o23.Chip, i, x0, new int[0]);
        this.q0 = h.getBoolean(o23.Chip_ensureMinTouchTargetSize, false);
        this.s0 = (int) Math.ceil(h.getDimension(o23.Chip_chipMinTouchTargetSize, (float) Math.ceil(mk4.c(getContext(), 48))));
        h.recycle();
    }

    @Override // android.widget.TextView, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        p42.f(this, this.h0);
    }

    @Override // android.widget.CompoundButton, android.widget.TextView, android.view.View
    public int[] onCreateDrawableState(int i) {
        int[] onCreateDrawableState = super.onCreateDrawableState(i + 2);
        if (isChecked()) {
            CheckBox.mergeDrawableStates(onCreateDrawableState, z0);
        }
        if (r()) {
            CheckBox.mergeDrawableStates(onCreateDrawableState, A0);
        }
        return onCreateDrawableState;
    }

    @Override // android.widget.TextView, android.view.View
    public void onFocusChanged(boolean z, int i, Rect rect) {
        super.onFocusChanged(z, i, rect);
        this.t0.K(z, i, rect);
    }

    @Override // android.view.View
    public boolean onHoverEvent(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 7) {
            setCloseIconHovered(getCloseIconTouchBounds().contains(motionEvent.getX(), motionEvent.getY()));
        } else if (actionMasked == 10) {
            setCloseIconHovered(false);
        }
        return super.onHoverEvent(motionEvent);
    }

    @Override // android.view.View
    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        if (!r() && !isClickable()) {
            accessibilityNodeInfo.setClassName("android.view.View");
        } else {
            accessibilityNodeInfo.setClassName(r() ? "android.widget.CompoundButton" : "android.widget.Button");
        }
        accessibilityNodeInfo.setCheckable(r());
        accessibilityNodeInfo.setClickable(isClickable());
        if (getParent() instanceof ChipGroup) {
            ChipGroup chipGroup = (ChipGroup) getParent();
            b6.I0(accessibilityNodeInfo).f0(b6.c.a(chipGroup.b(this), 1, chipGroup.c() ? chipGroup.o(this) : -1, 1, false, isChecked()));
        }
    }

    @Override // android.widget.Button, android.widget.TextView, android.view.View
    @TargetApi(24)
    public PointerIcon onResolvePointerIcon(MotionEvent motionEvent, int i) {
        if (getCloseIconTouchBounds().contains(motionEvent.getX(), motionEvent.getY()) && isEnabled()) {
            return PointerIcon.getSystemIcon(getContext(), PlaybackException.ERROR_CODE_BEHIND_LIVE_WINDOW);
        }
        return null;
    }

    @Override // android.widget.TextView, android.view.View
    @TargetApi(17)
    public void onRtlPropertiesChanged(int i) {
        super.onRtlPropertiesChanged(i);
        if (this.r0 != i) {
            this.r0 = i;
            A();
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:8:0x001e, code lost:
        if (r0 != 3) goto L17;
     */
    @Override // android.widget.TextView, android.view.View
    @android.annotation.SuppressLint({"ClickableViewAccessibility"})
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public boolean onTouchEvent(android.view.MotionEvent r6) {
        /*
            r5 = this;
            int r0 = r6.getActionMasked()
            android.graphics.RectF r1 = r5.getCloseIconTouchBounds()
            float r2 = r6.getX()
            float r3 = r6.getY()
            boolean r1 = r1.contains(r2, r3)
            r2 = 0
            r3 = 1
            if (r0 == 0) goto L39
            if (r0 == r3) goto L2b
            r4 = 2
            if (r0 == r4) goto L21
            r1 = 3
            if (r0 == r1) goto L34
            goto L40
        L21:
            boolean r0 = r5.n0
            if (r0 == 0) goto L40
            if (r1 != 0) goto L3e
            r5.setCloseIconPressed(r2)
            goto L3e
        L2b:
            boolean r0 = r5.n0
            if (r0 == 0) goto L34
            r5.t()
            r0 = r3
            goto L35
        L34:
            r0 = r2
        L35:
            r5.setCloseIconPressed(r2)
            goto L41
        L39:
            if (r1 == 0) goto L40
            r5.setCloseIconPressed(r3)
        L3e:
            r0 = r3
            goto L41
        L40:
            r0 = r2
        L41:
            if (r0 != 0) goto L49
            boolean r6 = super.onTouchEvent(r6)
            if (r6 == 0) goto L4a
        L49:
            r2 = r3
        L4a:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.chip.Chip.onTouchEvent(android.view.MotionEvent):boolean");
    }

    public final void p() {
        if (Build.VERSION.SDK_INT >= 21) {
            setOutlineProvider(new b());
        }
    }

    public final void q(int i, int i2, int i3, int i4) {
        this.i0 = new InsetDrawable((Drawable) this.h0, i, i2, i3, i4);
    }

    public boolean r() {
        com.google.android.material.chip.a aVar = this.h0;
        return aVar != null && aVar.u1();
    }

    public boolean s() {
        com.google.android.material.chip.a aVar = this.h0;
        return aVar != null && aVar.w1();
    }

    @Override // android.view.View
    public void setBackground(Drawable drawable) {
        if (drawable == getBackgroundDrawable() || drawable == this.j0) {
            super.setBackground(drawable);
        }
    }

    @Override // android.view.View
    public void setBackgroundColor(int i) {
    }

    @Override // androidx.appcompat.widget.AppCompatCheckBox, android.view.View
    public void setBackgroundDrawable(Drawable drawable) {
        if (drawable == getBackgroundDrawable() || drawable == this.j0) {
            super.setBackgroundDrawable(drawable);
        }
    }

    @Override // androidx.appcompat.widget.AppCompatCheckBox, android.view.View
    public void setBackgroundResource(int i) {
    }

    @Override // android.view.View
    public void setBackgroundTintList(ColorStateList colorStateList) {
    }

    @Override // android.view.View
    public void setBackgroundTintMode(PorterDuff.Mode mode) {
    }

    public void setCheckable(boolean z) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.D1(z);
        }
    }

    public void setCheckableResource(int i) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.E1(i);
        }
    }

    @Override // android.widget.CompoundButton, android.widget.Checkable
    public void setChecked(boolean z) {
        CompoundButton.OnCheckedChangeListener onCheckedChangeListener;
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar == null) {
            this.m0 = z;
        } else if (aVar.u1()) {
            boolean isChecked = isChecked();
            super.setChecked(z);
            if (isChecked == z || (onCheckedChangeListener = this.l0) == null) {
                return;
            }
            onCheckedChangeListener.onCheckedChanged(this, z);
        }
    }

    public void setCheckedIcon(Drawable drawable) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.F1(drawable);
        }
    }

    @Deprecated
    public void setCheckedIconEnabled(boolean z) {
        setCheckedIconVisible(z);
    }

    @Deprecated
    public void setCheckedIconEnabledResource(int i) {
        setCheckedIconVisible(i);
    }

    public void setCheckedIconResource(int i) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.G1(i);
        }
    }

    public void setCheckedIconTint(ColorStateList colorStateList) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.H1(colorStateList);
        }
    }

    public void setCheckedIconTintResource(int i) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.I1(i);
        }
    }

    public void setCheckedIconVisible(int i) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.J1(i);
        }
    }

    public void setChipBackgroundColor(ColorStateList colorStateList) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.L1(colorStateList);
        }
    }

    public void setChipBackgroundColorResource(int i) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.M1(i);
        }
    }

    @Deprecated
    public void setChipCornerRadius(float f) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.N1(f);
        }
    }

    @Deprecated
    public void setChipCornerRadiusResource(int i) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.O1(i);
        }
    }

    public void setChipDrawable(com.google.android.material.chip.a aVar) {
        com.google.android.material.chip.a aVar2 = this.h0;
        if (aVar2 != aVar) {
            w(aVar2);
            this.h0 = aVar;
            aVar.G2(false);
            i(this.h0);
            k(this.s0);
        }
    }

    public void setChipEndPadding(float f) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.P1(f);
        }
    }

    public void setChipEndPaddingResource(int i) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.Q1(i);
        }
    }

    public void setChipIcon(Drawable drawable) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.R1(drawable);
        }
    }

    @Deprecated
    public void setChipIconEnabled(boolean z) {
        setChipIconVisible(z);
    }

    @Deprecated
    public void setChipIconEnabledResource(int i) {
        setChipIconVisible(i);
    }

    public void setChipIconResource(int i) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.S1(i);
        }
    }

    public void setChipIconSize(float f) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.T1(f);
        }
    }

    public void setChipIconSizeResource(int i) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.U1(i);
        }
    }

    public void setChipIconTint(ColorStateList colorStateList) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.V1(colorStateList);
        }
    }

    public void setChipIconTintResource(int i) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.W1(i);
        }
    }

    public void setChipIconVisible(int i) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.X1(i);
        }
    }

    public void setChipMinHeight(float f) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.Z1(f);
        }
    }

    public void setChipMinHeightResource(int i) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.a2(i);
        }
    }

    public void setChipStartPadding(float f) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.b2(f);
        }
    }

    public void setChipStartPaddingResource(int i) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.c2(i);
        }
    }

    public void setChipStrokeColor(ColorStateList colorStateList) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.d2(colorStateList);
        }
    }

    public void setChipStrokeColorResource(int i) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.e2(i);
        }
    }

    public void setChipStrokeWidth(float f) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.f2(f);
        }
    }

    public void setChipStrokeWidthResource(int i) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.g2(i);
        }
    }

    @Deprecated
    public void setChipText(CharSequence charSequence) {
        setText(charSequence);
    }

    @Deprecated
    public void setChipTextResource(int i) {
        setText(getResources().getString(i));
    }

    public void setCloseIcon(Drawable drawable) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.i2(drawable);
        }
        x();
    }

    public void setCloseIconContentDescription(CharSequence charSequence) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.j2(charSequence);
        }
    }

    @Deprecated
    public void setCloseIconEnabled(boolean z) {
        setCloseIconVisible(z);
    }

    @Deprecated
    public void setCloseIconEnabledResource(int i) {
        setCloseIconVisible(i);
    }

    public void setCloseIconEndPadding(float f) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.k2(f);
        }
    }

    public void setCloseIconEndPaddingResource(int i) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.l2(i);
        }
    }

    public void setCloseIconResource(int i) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.m2(i);
        }
        x();
    }

    public void setCloseIconSize(float f) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.n2(f);
        }
    }

    public void setCloseIconSizeResource(int i) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.o2(i);
        }
    }

    public void setCloseIconStartPadding(float f) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.p2(f);
        }
    }

    public void setCloseIconStartPaddingResource(int i) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.q2(i);
        }
    }

    public void setCloseIconTint(ColorStateList colorStateList) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.s2(colorStateList);
        }
    }

    public void setCloseIconTintResource(int i) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.t2(i);
        }
    }

    public void setCloseIconVisible(int i) {
        setCloseIconVisible(getResources().getBoolean(i));
    }

    @Override // android.widget.TextView
    public void setCompoundDrawables(Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
        if (drawable != null) {
            throw new UnsupportedOperationException("Please set start drawable using R.attr#chipIcon.");
        }
        if (drawable3 == null) {
            super.setCompoundDrawables(drawable, drawable2, drawable3, drawable4);
            return;
        }
        throw new UnsupportedOperationException("Please set end drawable using R.attr#closeIcon.");
    }

    @Override // android.widget.TextView
    public void setCompoundDrawablesRelative(Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
        if (drawable != null) {
            throw new UnsupportedOperationException("Please set start drawable using R.attr#chipIcon.");
        }
        if (drawable3 == null) {
            super.setCompoundDrawablesRelative(drawable, drawable2, drawable3, drawable4);
            return;
        }
        throw new UnsupportedOperationException("Please set end drawable using R.attr#closeIcon.");
    }

    @Override // android.widget.TextView
    public void setCompoundDrawablesRelativeWithIntrinsicBounds(int i, int i2, int i3, int i4) {
        if (i != 0) {
            throw new UnsupportedOperationException("Please set start drawable using R.attr#chipIcon.");
        }
        if (i3 == 0) {
            super.setCompoundDrawablesRelativeWithIntrinsicBounds(i, i2, i3, i4);
            return;
        }
        throw new UnsupportedOperationException("Please set end drawable using R.attr#closeIcon.");
    }

    @Override // android.widget.TextView
    public void setCompoundDrawablesWithIntrinsicBounds(int i, int i2, int i3, int i4) {
        if (i != 0) {
            throw new UnsupportedOperationException("Please set start drawable using R.attr#chipIcon.");
        }
        if (i3 == 0) {
            super.setCompoundDrawablesWithIntrinsicBounds(i, i2, i3, i4);
            return;
        }
        throw new UnsupportedOperationException("Please set end drawable using R.attr#closeIcon.");
    }

    @Override // android.view.View
    public void setElevation(float f) {
        super.setElevation(f);
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.Z(f);
        }
    }

    @Override // android.widget.TextView
    public void setEllipsize(TextUtils.TruncateAt truncateAt) {
        if (this.h0 == null) {
            return;
        }
        if (truncateAt != TextUtils.TruncateAt.MARQUEE) {
            super.setEllipsize(truncateAt);
            com.google.android.material.chip.a aVar = this.h0;
            if (aVar != null) {
                aVar.w2(truncateAt);
                return;
            }
            return;
        }
        throw new UnsupportedOperationException("Text within a chip are not allowed to scroll.");
    }

    public void setEnsureMinTouchTargetSize(boolean z) {
        this.q0 = z;
        k(this.s0);
    }

    @Override // android.widget.TextView
    public void setGravity(int i) {
        if (i != 8388627) {
            return;
        }
        super.setGravity(i);
    }

    public void setHideMotionSpec(z92 z92Var) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.x2(z92Var);
        }
    }

    public void setHideMotionSpecResource(int i) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.y2(i);
        }
    }

    public void setIconEndPadding(float f) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.z2(f);
        }
    }

    public void setIconEndPaddingResource(int i) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.A2(i);
        }
    }

    public void setIconStartPadding(float f) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.B2(f);
        }
    }

    public void setIconStartPaddingResource(int i) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.C2(i);
        }
    }

    @Override // android.view.View
    public void setLayoutDirection(int i) {
        if (this.h0 != null && Build.VERSION.SDK_INT >= 17) {
            super.setLayoutDirection(i);
        }
    }

    @Override // android.widget.TextView
    public void setLines(int i) {
        if (i <= 1) {
            super.setLines(i);
            return;
        }
        throw new UnsupportedOperationException("Chip does not support multi-line text");
    }

    @Override // android.widget.TextView
    public void setMaxLines(int i) {
        if (i <= 1) {
            super.setMaxLines(i);
            return;
        }
        throw new UnsupportedOperationException("Chip does not support multi-line text");
    }

    @Override // android.widget.TextView
    public void setMaxWidth(int i) {
        super.setMaxWidth(i);
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.D2(i);
        }
    }

    @Override // android.widget.TextView
    public void setMinLines(int i) {
        if (i <= 1) {
            super.setMinLines(i);
            return;
        }
        throw new UnsupportedOperationException("Chip does not support multi-line text");
    }

    public void setOnCheckedChangeListenerInternal(CompoundButton.OnCheckedChangeListener onCheckedChangeListener) {
        this.l0 = onCheckedChangeListener;
    }

    public void setOnCloseIconClickListener(View.OnClickListener onClickListener) {
        this.k0 = onClickListener;
        x();
    }

    public void setRippleColor(ColorStateList colorStateList) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.E2(colorStateList);
        }
        if (this.h0.s1()) {
            return;
        }
        z();
    }

    public void setRippleColorResource(int i) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.F2(i);
            if (this.h0.s1()) {
                return;
            }
            z();
        }
    }

    @Override // defpackage.sn3
    public void setShapeAppearanceModel(pn3 pn3Var) {
        this.h0.setShapeAppearanceModel(pn3Var);
    }

    public void setShowMotionSpec(z92 z92Var) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.H2(z92Var);
        }
    }

    public void setShowMotionSpecResource(int i) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.I2(i);
        }
    }

    @Override // android.widget.TextView
    public void setSingleLine(boolean z) {
        if (z) {
            super.setSingleLine(z);
            return;
        }
        throw new UnsupportedOperationException("Chip does not support multi-line text");
    }

    @Override // android.widget.TextView
    public void setText(CharSequence charSequence, TextView.BufferType bufferType) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar == null) {
            return;
        }
        if (charSequence == null) {
            charSequence = "";
        }
        super.setText(aVar.R2() ? null : charSequence, bufferType);
        com.google.android.material.chip.a aVar2 = this.h0;
        if (aVar2 != null) {
            aVar2.J2(charSequence);
        }
    }

    public void setTextAppearance(d44 d44Var) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.K2(d44Var);
        }
        B();
    }

    public void setTextAppearanceResource(int i) {
        setTextAppearance(getContext(), i);
    }

    public void setTextEndPadding(float f) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.M2(f);
        }
    }

    public void setTextEndPaddingResource(int i) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.N2(i);
        }
    }

    public void setTextStartPadding(float f) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.O2(f);
        }
    }

    public void setTextStartPaddingResource(int i) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.P2(i);
        }
    }

    public boolean t() {
        boolean z = false;
        playSoundEffect(0);
        View.OnClickListener onClickListener = this.k0;
        if (onClickListener != null) {
            onClickListener.onClick(this);
            z = true;
        }
        this.t0.W(1, 1);
        return z;
    }

    public final void u() {
        if (this.i0 != null) {
            this.i0 = null;
            setMinWidth(0);
            setMinHeight((int) getChipMinHeight());
            y();
        }
    }

    public boolean v() {
        return this.q0;
    }

    public final void w(com.google.android.material.chip.a aVar) {
        if (aVar != null) {
            aVar.v2(null);
        }
    }

    public final void x() {
        if (n() && s() && this.k0 != null) {
            ei4.t0(this, this.t0);
        } else {
            ei4.t0(this, null);
        }
    }

    public final void y() {
        if (b93.a) {
            z();
            return;
        }
        this.h0.Q2(true);
        ei4.w0(this, getBackgroundDrawable());
        A();
        l();
    }

    public final void z() {
        this.j0 = new RippleDrawable(b93.d(this.h0.l1()), getBackgroundDrawable(), null);
        this.h0.Q2(false);
        ei4.w0(this, this.j0);
        A();
    }

    public Chip(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, gy2.chipStyle);
    }

    public void setCloseIconVisible(boolean z) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.u2(z);
        }
        x();
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public Chip(android.content.Context r8, android.util.AttributeSet r9, int r10) {
        /*
            r7 = this;
            int r4 = com.google.android.material.chip.Chip.x0
            android.content.Context r8 = defpackage.r42.c(r8, r9, r10, r4)
            r7.<init>(r8, r9, r10)
            android.graphics.Rect r8 = new android.graphics.Rect
            r8.<init>()
            r7.u0 = r8
            android.graphics.RectF r8 = new android.graphics.RectF
            r8.<init>()
            r7.v0 = r8
            com.google.android.material.chip.Chip$a r8 = new com.google.android.material.chip.Chip$a
            r8.<init>()
            r7.w0 = r8
            android.content.Context r8 = r7.getContext()
            r7.C(r9)
            com.google.android.material.chip.a r6 = com.google.android.material.chip.a.B0(r8, r9, r10, r4)
            r7.o(r8, r9, r10)
            r7.setChipDrawable(r6)
            float r0 = defpackage.ei4.y(r7)
            r6.Z(r0)
            int[] r2 = defpackage.o23.Chip
            r0 = 0
            int[] r5 = new int[r0]
            r0 = r8
            r1 = r9
            r3 = r10
            android.content.res.TypedArray r9 = defpackage.a54.h(r0, r1, r2, r3, r4, r5)
            int r10 = android.os.Build.VERSION.SDK_INT
            r0 = 23
            if (r10 >= r0) goto L51
            int r10 = defpackage.o23.Chip_android_textColor
            android.content.res.ColorStateList r8 = defpackage.n42.b(r8, r9, r10)
            r7.setTextColor(r8)
        L51:
            int r8 = defpackage.o23.Chip_shapeAppearance
            boolean r8 = r9.hasValue(r8)
            r9.recycle()
            com.google.android.material.chip.Chip$c r9 = new com.google.android.material.chip.Chip$c
            r9.<init>(r7)
            r7.t0 = r9
            r7.x()
            if (r8 != 0) goto L69
            r7.p()
        L69:
            boolean r8 = r7.m0
            r7.setChecked(r8)
            java.lang.CharSequence r8 = r6.n1()
            r7.setText(r8)
            android.text.TextUtils$TruncateAt r8 = r6.h1()
            r7.setEllipsize(r8)
            r7.B()
            com.google.android.material.chip.a r8 = r7.h0
            boolean r8 = r8.R2()
            if (r8 != 0) goto L8e
            r8 = 1
            r7.setLines(r8)
            r7.setHorizontallyScrolling(r8)
        L8e:
            r8 = 8388627(0x800013, float:1.175497E-38)
            r7.setGravity(r8)
            r7.A()
            boolean r8 = r7.v()
            if (r8 == 0) goto La2
            int r8 = r7.s0
            r7.setMinHeight(r8)
        La2:
            int r8 = defpackage.ei4.E(r7)
            r7.r0 = r8
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.chip.Chip.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }

    public void setCheckedIconVisible(boolean z) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.K1(z);
        }
    }

    public void setChipIconVisible(boolean z) {
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.Y1(z);
        }
    }

    @Override // android.widget.TextView
    public void setCompoundDrawablesRelativeWithIntrinsicBounds(Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
        if (drawable != null) {
            throw new UnsupportedOperationException("Please set start drawable using R.attr#chipIcon.");
        }
        if (drawable3 == null) {
            super.setCompoundDrawablesRelativeWithIntrinsicBounds(drawable, drawable2, drawable3, drawable4);
            return;
        }
        throw new UnsupportedOperationException("Please set end drawable using R.attr#closeIcon.");
    }

    @Override // android.widget.TextView
    public void setCompoundDrawablesWithIntrinsicBounds(Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
        if (drawable != null) {
            throw new UnsupportedOperationException("Please set left drawable using R.attr#chipIcon.");
        }
        if (drawable3 == null) {
            super.setCompoundDrawablesWithIntrinsicBounds(drawable, drawable2, drawable3, drawable4);
            return;
        }
        throw new UnsupportedOperationException("Please set right drawable using R.attr#closeIcon.");
    }

    @Override // android.widget.TextView
    public void setTextAppearance(Context context, int i) {
        super.setTextAppearance(context, i);
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.L2(i);
        }
        B();
    }

    @Override // android.widget.TextView
    public void setTextAppearance(int i) {
        super.setTextAppearance(i);
        com.google.android.material.chip.a aVar = this.h0;
        if (aVar != null) {
            aVar.L2(i);
        }
        B();
    }
}
