package com.google.android.material.chip;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Outline;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.RippleDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.text.TextUtils;
import android.util.AttributeSet;
import com.github.mikephil.charting.utils.Utils;
import defpackage.i44;
import java.lang.ref.WeakReference;
import java.util.Arrays;

/* compiled from: ChipDrawable.java */
/* loaded from: classes2.dex */
public class a extends o42 implements Drawable.Callback, i44.b {
    public static final int[] M1 = {16842910};
    public static final ShapeDrawable N1 = new ShapeDrawable(new OvalShape());
    public ColorFilter A1;
    public PorterDuffColorFilter B1;
    public ColorStateList C0;
    public ColorStateList C1;
    public ColorStateList D0;
    public PorterDuff.Mode D1;
    public float E0;
    public int[] E1;
    public float F0;
    public boolean F1;
    public ColorStateList G0;
    public ColorStateList G1;
    public float H0;
    public WeakReference<InterfaceC0116a> H1;
    public ColorStateList I0;
    public TextUtils.TruncateAt I1;
    public CharSequence J0;
    public boolean J1;
    public boolean K0;
    public int K1;
    public Drawable L0;
    public boolean L1;
    public ColorStateList M0;
    public float N0;
    public boolean O0;
    public boolean P0;
    public Drawable Q0;
    public Drawable R0;
    public ColorStateList S0;
    public float T0;
    public CharSequence U0;
    public boolean V0;
    public boolean W0;
    public Drawable X0;
    public ColorStateList Y0;
    public z92 Z0;
    public z92 a1;
    public float b1;
    public float c1;
    public float d1;
    public float e1;
    public float f1;
    public float g1;
    public float h1;
    public float i1;
    public final Context j1;
    public final Paint k1;
    public final Paint l1;
    public final Paint.FontMetrics m1;
    public final RectF n1;
    public final PointF o1;
    public final Path p1;
    public final i44 q1;
    public int r1;
    public int s1;
    public int t1;
    public int u1;
    public int v1;
    public int w1;
    public boolean x1;
    public int y1;
    public int z1;

    /* compiled from: ChipDrawable.java */
    /* renamed from: com.google.android.material.chip.a$a  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public interface InterfaceC0116a {
        void a();
    }

    public a(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        this.F0 = -1.0f;
        this.k1 = new Paint(1);
        this.m1 = new Paint.FontMetrics();
        this.n1 = new RectF();
        this.o1 = new PointF();
        this.p1 = new Path();
        this.z1 = 255;
        this.D1 = PorterDuff.Mode.SRC_IN;
        this.H1 = new WeakReference<>(null);
        P(context);
        this.j1 = context;
        i44 i44Var = new i44(this);
        this.q1 = i44Var;
        this.J0 = "";
        i44Var.e().density = context.getResources().getDisplayMetrics().density;
        this.l1 = null;
        int[] iArr = M1;
        setState(iArr);
        r2(iArr);
        this.J1 = true;
        if (b93.a) {
            N1.setTint(-1);
        }
    }

    public static a B0(Context context, AttributeSet attributeSet, int i, int i2) {
        a aVar = new a(context, attributeSet, i, i2);
        aVar.A1(attributeSet, i, i2);
        return aVar;
    }

    public static boolean t1(int[] iArr, int i) {
        if (iArr == null) {
            return false;
        }
        for (int i2 : iArr) {
            if (i2 == i) {
                return true;
            }
        }
        return false;
    }

    public static boolean x1(d44 d44Var) {
        ColorStateList colorStateList;
        return (d44Var == null || (colorStateList = d44Var.a) == null || !colorStateList.isStateful()) ? false : true;
    }

    public static boolean y1(ColorStateList colorStateList) {
        return colorStateList != null && colorStateList.isStateful();
    }

    public static boolean z1(Drawable drawable) {
        return drawable != null && drawable.isStateful();
    }

    public final boolean A0() {
        return this.W0 && this.X0 != null && this.V0;
    }

    public final void A1(AttributeSet attributeSet, int i, int i2) {
        TypedArray h = a54.h(this.j1, attributeSet, o23.Chip, i, i2, new int[0]);
        this.L1 = h.hasValue(o23.Chip_shapeAppearance);
        h2(n42.b(this.j1, h, o23.Chip_chipSurfaceColor));
        L1(n42.b(this.j1, h, o23.Chip_chipBackgroundColor));
        Z1(h.getDimension(o23.Chip_chipMinHeight, Utils.FLOAT_EPSILON));
        int i3 = o23.Chip_chipCornerRadius;
        if (h.hasValue(i3)) {
            N1(h.getDimension(i3, Utils.FLOAT_EPSILON));
        }
        d2(n42.b(this.j1, h, o23.Chip_chipStrokeColor));
        f2(h.getDimension(o23.Chip_chipStrokeWidth, Utils.FLOAT_EPSILON));
        E2(n42.b(this.j1, h, o23.Chip_rippleColor));
        J2(h.getText(o23.Chip_android_text));
        d44 f = n42.f(this.j1, h, o23.Chip_android_textAppearance);
        f.k = h.getDimension(o23.Chip_android_textSize, f.k);
        K2(f);
        int i4 = h.getInt(o23.Chip_android_ellipsize, 0);
        if (i4 == 1) {
            w2(TextUtils.TruncateAt.START);
        } else if (i4 == 2) {
            w2(TextUtils.TruncateAt.MIDDLE);
        } else if (i4 == 3) {
            w2(TextUtils.TruncateAt.END);
        }
        Y1(h.getBoolean(o23.Chip_chipIconVisible, false));
        if (attributeSet != null && attributeSet.getAttributeValue("http://schemas.android.com/apk/res-auto", "chipIconEnabled") != null && attributeSet.getAttributeValue("http://schemas.android.com/apk/res-auto", "chipIconVisible") == null) {
            Y1(h.getBoolean(o23.Chip_chipIconEnabled, false));
        }
        R1(n42.d(this.j1, h, o23.Chip_chipIcon));
        int i5 = o23.Chip_chipIconTint;
        if (h.hasValue(i5)) {
            V1(n42.b(this.j1, h, i5));
        }
        T1(h.getDimension(o23.Chip_chipIconSize, -1.0f));
        u2(h.getBoolean(o23.Chip_closeIconVisible, false));
        if (attributeSet != null && attributeSet.getAttributeValue("http://schemas.android.com/apk/res-auto", "closeIconEnabled") != null && attributeSet.getAttributeValue("http://schemas.android.com/apk/res-auto", "closeIconVisible") == null) {
            u2(h.getBoolean(o23.Chip_closeIconEnabled, false));
        }
        i2(n42.d(this.j1, h, o23.Chip_closeIcon));
        s2(n42.b(this.j1, h, o23.Chip_closeIconTint));
        n2(h.getDimension(o23.Chip_closeIconSize, Utils.FLOAT_EPSILON));
        D1(h.getBoolean(o23.Chip_android_checkable, false));
        K1(h.getBoolean(o23.Chip_checkedIconVisible, false));
        if (attributeSet != null && attributeSet.getAttributeValue("http://schemas.android.com/apk/res-auto", "checkedIconEnabled") != null && attributeSet.getAttributeValue("http://schemas.android.com/apk/res-auto", "checkedIconVisible") == null) {
            K1(h.getBoolean(o23.Chip_checkedIconEnabled, false));
        }
        F1(n42.d(this.j1, h, o23.Chip_checkedIcon));
        int i6 = o23.Chip_checkedIconTint;
        if (h.hasValue(i6)) {
            H1(n42.b(this.j1, h, i6));
        }
        H2(z92.c(this.j1, h, o23.Chip_showMotionSpec));
        x2(z92.c(this.j1, h, o23.Chip_hideMotionSpec));
        b2(h.getDimension(o23.Chip_chipStartPadding, Utils.FLOAT_EPSILON));
        B2(h.getDimension(o23.Chip_iconStartPadding, Utils.FLOAT_EPSILON));
        z2(h.getDimension(o23.Chip_iconEndPadding, Utils.FLOAT_EPSILON));
        O2(h.getDimension(o23.Chip_textStartPadding, Utils.FLOAT_EPSILON));
        M2(h.getDimension(o23.Chip_textEndPadding, Utils.FLOAT_EPSILON));
        p2(h.getDimension(o23.Chip_closeIconStartPadding, Utils.FLOAT_EPSILON));
        k2(h.getDimension(o23.Chip_closeIconEndPadding, Utils.FLOAT_EPSILON));
        P1(h.getDimension(o23.Chip_chipEndPadding, Utils.FLOAT_EPSILON));
        D2(h.getDimensionPixelSize(o23.Chip_android_maxWidth, Integer.MAX_VALUE));
        h.recycle();
    }

    public void A2(int i) {
        z2(this.j1.getResources().getDimension(i));
    }

    public void B1() {
        InterfaceC0116a interfaceC0116a = this.H1.get();
        if (interfaceC0116a != null) {
            interfaceC0116a.a();
        }
    }

    public void B2(float f) {
        if (this.c1 != f) {
            float s0 = s0();
            this.c1 = f;
            float s02 = s0();
            invalidateSelf();
            if (s0 != s02) {
                B1();
            }
        }
    }

    public final void C0(Canvas canvas, Rect rect) {
        if (S2()) {
            r0(rect, this.n1);
            RectF rectF = this.n1;
            float f = rectF.left;
            float f2 = rectF.top;
            canvas.translate(f, f2);
            this.X0.setBounds(0, 0, (int) this.n1.width(), (int) this.n1.height());
            this.X0.draw(canvas);
            canvas.translate(-f, -f2);
        }
    }

    public final boolean C1(int[] iArr, int[] iArr2) {
        boolean z;
        boolean onStateChange = super.onStateChange(iArr);
        ColorStateList colorStateList = this.C0;
        int l = l(colorStateList != null ? colorStateList.getColorForState(iArr, this.r1) : 0);
        boolean z2 = true;
        if (this.r1 != l) {
            this.r1 = l;
            onStateChange = true;
        }
        ColorStateList colorStateList2 = this.D0;
        int l2 = l(colorStateList2 != null ? colorStateList2.getColorForState(iArr, this.s1) : 0);
        if (this.s1 != l2) {
            this.s1 = l2;
            onStateChange = true;
        }
        int g = l42.g(l, l2);
        if ((this.t1 != g) | (x() == null)) {
            this.t1 = g;
            a0(ColorStateList.valueOf(g));
            onStateChange = true;
        }
        ColorStateList colorStateList3 = this.G0;
        int colorForState = colorStateList3 != null ? colorStateList3.getColorForState(iArr, this.u1) : 0;
        if (this.u1 != colorForState) {
            this.u1 = colorForState;
            onStateChange = true;
        }
        int colorForState2 = (this.G1 == null || !b93.e(iArr)) ? 0 : this.G1.getColorForState(iArr, this.v1);
        if (this.v1 != colorForState2) {
            this.v1 = colorForState2;
            if (this.F1) {
                onStateChange = true;
            }
        }
        int colorForState3 = (this.q1.d() == null || this.q1.d().a == null) ? 0 : this.q1.d().a.getColorForState(iArr, this.w1);
        if (this.w1 != colorForState3) {
            this.w1 = colorForState3;
            onStateChange = true;
        }
        boolean z3 = t1(getState(), 16842912) && this.V0;
        if (this.x1 == z3 || this.X0 == null) {
            z = false;
        } else {
            float s0 = s0();
            this.x1 = z3;
            if (s0 != s0()) {
                onStateChange = true;
                z = true;
            } else {
                z = false;
                onStateChange = true;
            }
        }
        ColorStateList colorStateList4 = this.C1;
        int colorForState4 = colorStateList4 != null ? colorStateList4.getColorForState(iArr, this.y1) : 0;
        if (this.y1 != colorForState4) {
            this.y1 = colorForState4;
            this.B1 = cr0.b(this, this.C1, this.D1);
        } else {
            z2 = onStateChange;
        }
        if (z1(this.L0)) {
            z2 |= this.L0.setState(iArr);
        }
        if (z1(this.X0)) {
            z2 |= this.X0.setState(iArr);
        }
        if (z1(this.Q0)) {
            int[] iArr3 = new int[iArr.length + iArr2.length];
            System.arraycopy(iArr, 0, iArr3, 0, iArr.length);
            System.arraycopy(iArr2, 0, iArr3, iArr.length, iArr2.length);
            z2 |= this.Q0.setState(iArr3);
        }
        if (b93.a && z1(this.R0)) {
            z2 |= this.R0.setState(iArr2);
        }
        if (z2) {
            invalidateSelf();
        }
        if (z) {
            B1();
        }
        return z2;
    }

    public void C2(int i) {
        B2(this.j1.getResources().getDimension(i));
    }

    public final void D0(Canvas canvas, Rect rect) {
        if (this.L1) {
            return;
        }
        this.k1.setColor(this.s1);
        this.k1.setStyle(Paint.Style.FILL);
        this.k1.setColorFilter(r1());
        this.n1.set(rect);
        canvas.drawRoundRect(this.n1, O0(), O0(), this.k1);
    }

    public void D1(boolean z) {
        if (this.V0 != z) {
            this.V0 = z;
            float s0 = s0();
            if (!z && this.x1) {
                this.x1 = false;
            }
            float s02 = s0();
            invalidateSelf();
            if (s0 != s02) {
                B1();
            }
        }
    }

    public void D2(int i) {
        this.K1 = i;
    }

    public final void E0(Canvas canvas, Rect rect) {
        if (T2()) {
            r0(rect, this.n1);
            RectF rectF = this.n1;
            float f = rectF.left;
            float f2 = rectF.top;
            canvas.translate(f, f2);
            this.L0.setBounds(0, 0, (int) this.n1.width(), (int) this.n1.height());
            this.L0.draw(canvas);
            canvas.translate(-f, -f2);
        }
    }

    public void E1(int i) {
        D1(this.j1.getResources().getBoolean(i));
    }

    public void E2(ColorStateList colorStateList) {
        if (this.I0 != colorStateList) {
            this.I0 = colorStateList;
            W2();
            onStateChange(getState());
        }
    }

    public final void F0(Canvas canvas, Rect rect) {
        if (this.H0 <= Utils.FLOAT_EPSILON || this.L1) {
            return;
        }
        this.k1.setColor(this.u1);
        this.k1.setStyle(Paint.Style.STROKE);
        if (!this.L1) {
            this.k1.setColorFilter(r1());
        }
        RectF rectF = this.n1;
        float f = this.H0;
        rectF.set(rect.left + (f / 2.0f), rect.top + (f / 2.0f), rect.right - (f / 2.0f), rect.bottom - (f / 2.0f));
        float f2 = this.F0 - (this.H0 / 2.0f);
        canvas.drawRoundRect(this.n1, f2, f2, this.k1);
    }

    public void F1(Drawable drawable) {
        if (this.X0 != drawable) {
            float s0 = s0();
            this.X0 = drawable;
            float s02 = s0();
            V2(this.X0);
            q0(this.X0);
            invalidateSelf();
            if (s0 != s02) {
                B1();
            }
        }
    }

    public void F2(int i) {
        E2(mf.c(this.j1, i));
    }

    public final void G0(Canvas canvas, Rect rect) {
        if (this.L1) {
            return;
        }
        this.k1.setColor(this.r1);
        this.k1.setStyle(Paint.Style.FILL);
        this.n1.set(rect);
        canvas.drawRoundRect(this.n1, O0(), O0(), this.k1);
    }

    public void G1(int i) {
        F1(mf.d(this.j1, i));
    }

    public void G2(boolean z) {
        this.J1 = z;
    }

    public final void H0(Canvas canvas, Rect rect) {
        if (U2()) {
            u0(rect, this.n1);
            RectF rectF = this.n1;
            float f = rectF.left;
            float f2 = rectF.top;
            canvas.translate(f, f2);
            this.Q0.setBounds(0, 0, (int) this.n1.width(), (int) this.n1.height());
            if (b93.a) {
                this.R0.setBounds(this.Q0.getBounds());
                this.R0.jumpToCurrentState();
                this.R0.draw(canvas);
            } else {
                this.Q0.draw(canvas);
            }
            canvas.translate(-f, -f2);
        }
    }

    public void H1(ColorStateList colorStateList) {
        if (this.Y0 != colorStateList) {
            this.Y0 = colorStateList;
            if (A0()) {
                androidx.core.graphics.drawable.a.o(this.X0, colorStateList);
            }
            onStateChange(getState());
        }
    }

    public void H2(z92 z92Var) {
        this.Z0 = z92Var;
    }

    public final void I0(Canvas canvas, Rect rect) {
        this.k1.setColor(this.v1);
        this.k1.setStyle(Paint.Style.FILL);
        this.n1.set(rect);
        if (!this.L1) {
            canvas.drawRoundRect(this.n1, O0(), O0(), this.k1);
            return;
        }
        h(new RectF(rect), this.p1);
        super.q(canvas, this.k1, this.p1, u());
    }

    public void I1(int i) {
        H1(mf.c(this.j1, i));
    }

    public void I2(int i) {
        H2(z92.d(this.j1, i));
    }

    public final void J0(Canvas canvas, Rect rect) {
        Paint paint = this.l1;
        if (paint != null) {
            paint.setColor(z20.j(-16777216, 127));
            canvas.drawRect(rect, this.l1);
            if (T2() || S2()) {
                r0(rect, this.n1);
                canvas.drawRect(this.n1, this.l1);
            }
            if (this.J0 != null) {
                canvas.drawLine(rect.left, rect.exactCenterY(), rect.right, rect.exactCenterY(), this.l1);
            }
            if (U2()) {
                u0(rect, this.n1);
                canvas.drawRect(this.n1, this.l1);
            }
            this.l1.setColor(z20.j(-65536, 127));
            t0(rect, this.n1);
            canvas.drawRect(this.n1, this.l1);
            this.l1.setColor(z20.j(-16711936, 127));
            v0(rect, this.n1);
            canvas.drawRect(this.n1, this.l1);
        }
    }

    public void J1(int i) {
        K1(this.j1.getResources().getBoolean(i));
    }

    public void J2(CharSequence charSequence) {
        if (charSequence == null) {
            charSequence = "";
        }
        if (TextUtils.equals(this.J0, charSequence)) {
            return;
        }
        this.J0 = charSequence;
        this.q1.i(true);
        invalidateSelf();
        B1();
    }

    public final void K0(Canvas canvas, Rect rect) {
        if (this.J0 != null) {
            Paint.Align z0 = z0(rect, this.o1);
            x0(rect, this.n1);
            if (this.q1.d() != null) {
                this.q1.e().drawableState = getState();
                this.q1.j(this.j1);
            }
            this.q1.e().setTextAlign(z0);
            int i = 0;
            boolean z = Math.round(this.q1.f(n1().toString())) > Math.round(this.n1.width());
            if (z) {
                i = canvas.save();
                canvas.clipRect(this.n1);
            }
            CharSequence charSequence = this.J0;
            if (z && this.I1 != null) {
                charSequence = TextUtils.ellipsize(charSequence, this.q1.e(), this.n1.width(), this.I1);
            }
            CharSequence charSequence2 = charSequence;
            int length = charSequence2.length();
            PointF pointF = this.o1;
            canvas.drawText(charSequence2, 0, length, pointF.x, pointF.y, this.q1.e());
            if (z) {
                canvas.restoreToCount(i);
            }
        }
    }

    public void K1(boolean z) {
        if (this.W0 != z) {
            boolean S2 = S2();
            this.W0 = z;
            boolean S22 = S2();
            if (S2 != S22) {
                if (S22) {
                    q0(this.X0);
                } else {
                    V2(this.X0);
                }
                invalidateSelf();
                B1();
            }
        }
    }

    public void K2(d44 d44Var) {
        this.q1.h(d44Var, this.j1);
    }

    public Drawable L0() {
        return this.X0;
    }

    public void L1(ColorStateList colorStateList) {
        if (this.D0 != colorStateList) {
            this.D0 = colorStateList;
            onStateChange(getState());
        }
    }

    public void L2(int i) {
        K2(new d44(this.j1, i));
    }

    public ColorStateList M0() {
        return this.Y0;
    }

    public void M1(int i) {
        L1(mf.c(this.j1, i));
    }

    public void M2(float f) {
        if (this.f1 != f) {
            this.f1 = f;
            invalidateSelf();
            B1();
        }
    }

    public ColorStateList N0() {
        return this.D0;
    }

    @Deprecated
    public void N1(float f) {
        if (this.F0 != f) {
            this.F0 = f;
            setShapeAppearanceModel(D().w(f));
        }
    }

    public void N2(int i) {
        M2(this.j1.getResources().getDimension(i));
    }

    public float O0() {
        return this.L1 ? I() : this.F0;
    }

    @Deprecated
    public void O1(int i) {
        N1(this.j1.getResources().getDimension(i));
    }

    public void O2(float f) {
        if (this.e1 != f) {
            this.e1 = f;
            invalidateSelf();
            B1();
        }
    }

    public float P0() {
        return this.i1;
    }

    public void P1(float f) {
        if (this.i1 != f) {
            this.i1 = f;
            invalidateSelf();
            B1();
        }
    }

    public void P2(int i) {
        O2(this.j1.getResources().getDimension(i));
    }

    public Drawable Q0() {
        Drawable drawable = this.L0;
        if (drawable != null) {
            return androidx.core.graphics.drawable.a.q(drawable);
        }
        return null;
    }

    public void Q1(int i) {
        P1(this.j1.getResources().getDimension(i));
    }

    public void Q2(boolean z) {
        if (this.F1 != z) {
            this.F1 = z;
            W2();
            onStateChange(getState());
        }
    }

    public float R0() {
        return this.N0;
    }

    public void R1(Drawable drawable) {
        Drawable Q0 = Q0();
        if (Q0 != drawable) {
            float s0 = s0();
            this.L0 = drawable != null ? androidx.core.graphics.drawable.a.r(drawable).mutate() : null;
            float s02 = s0();
            V2(Q0);
            if (T2()) {
                q0(this.L0);
            }
            invalidateSelf();
            if (s0 != s02) {
                B1();
            }
        }
    }

    public boolean R2() {
        return this.J1;
    }

    public ColorStateList S0() {
        return this.M0;
    }

    public void S1(int i) {
        R1(mf.d(this.j1, i));
    }

    public final boolean S2() {
        return this.W0 && this.X0 != null && this.x1;
    }

    public float T0() {
        return this.E0;
    }

    public void T1(float f) {
        if (this.N0 != f) {
            float s0 = s0();
            this.N0 = f;
            float s02 = s0();
            invalidateSelf();
            if (s0 != s02) {
                B1();
            }
        }
    }

    public final boolean T2() {
        return this.K0 && this.L0 != null;
    }

    public float U0() {
        return this.b1;
    }

    public void U1(int i) {
        T1(this.j1.getResources().getDimension(i));
    }

    public final boolean U2() {
        return this.P0 && this.Q0 != null;
    }

    public ColorStateList V0() {
        return this.G0;
    }

    public void V1(ColorStateList colorStateList) {
        this.O0 = true;
        if (this.M0 != colorStateList) {
            this.M0 = colorStateList;
            if (T2()) {
                androidx.core.graphics.drawable.a.o(this.L0, colorStateList);
            }
            onStateChange(getState());
        }
    }

    public final void V2(Drawable drawable) {
        if (drawable != null) {
            drawable.setCallback(null);
        }
    }

    public float W0() {
        return this.H0;
    }

    public void W1(int i) {
        V1(mf.c(this.j1, i));
    }

    public final void W2() {
        this.G1 = this.F1 ? b93.d(this.I0) : null;
    }

    public Drawable X0() {
        Drawable drawable = this.Q0;
        if (drawable != null) {
            return androidx.core.graphics.drawable.a.q(drawable);
        }
        return null;
    }

    public void X1(int i) {
        Y1(this.j1.getResources().getBoolean(i));
    }

    @TargetApi(21)
    public final void X2() {
        this.R0 = new RippleDrawable(b93.d(l1()), this.Q0, N1);
    }

    public CharSequence Y0() {
        return this.U0;
    }

    public void Y1(boolean z) {
        if (this.K0 != z) {
            boolean T2 = T2();
            this.K0 = z;
            boolean T22 = T2();
            if (T2 != T22) {
                if (T22) {
                    q0(this.L0);
                } else {
                    V2(this.L0);
                }
                invalidateSelf();
                B1();
            }
        }
    }

    public float Z0() {
        return this.h1;
    }

    public void Z1(float f) {
        if (this.E0 != f) {
            this.E0 = f;
            invalidateSelf();
            B1();
        }
    }

    @Override // defpackage.i44.b
    public void a() {
        B1();
        invalidateSelf();
    }

    public float a1() {
        return this.T0;
    }

    public void a2(int i) {
        Z1(this.j1.getResources().getDimension(i));
    }

    public float b1() {
        return this.g1;
    }

    public void b2(float f) {
        if (this.b1 != f) {
            this.b1 = f;
            invalidateSelf();
            B1();
        }
    }

    public int[] c1() {
        return this.E1;
    }

    public void c2(int i) {
        b2(this.j1.getResources().getDimension(i));
    }

    public ColorStateList d1() {
        return this.S0;
    }

    public void d2(ColorStateList colorStateList) {
        if (this.G0 != colorStateList) {
            this.G0 = colorStateList;
            if (this.L1) {
                l0(colorStateList);
            }
            onStateChange(getState());
        }
    }

    @Override // defpackage.o42, android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        Rect bounds = getBounds();
        if (bounds.isEmpty() || getAlpha() == 0) {
            return;
        }
        int i = this.z1;
        int a = i < 255 ? xv.a(canvas, bounds.left, bounds.top, bounds.right, bounds.bottom, i) : 0;
        G0(canvas, bounds);
        D0(canvas, bounds);
        if (this.L1) {
            super.draw(canvas);
        }
        F0(canvas, bounds);
        I0(canvas, bounds);
        E0(canvas, bounds);
        C0(canvas, bounds);
        if (this.J1) {
            K0(canvas, bounds);
        }
        H0(canvas, bounds);
        J0(canvas, bounds);
        if (this.z1 < 255) {
            canvas.restoreToCount(a);
        }
    }

    public void e1(RectF rectF) {
        v0(getBounds(), rectF);
    }

    public void e2(int i) {
        d2(mf.c(this.j1, i));
    }

    public final float f1() {
        Drawable drawable = this.x1 ? this.X0 : this.L0;
        float f = this.N0;
        if (f <= Utils.FLOAT_EPSILON && drawable != null) {
            f = (float) Math.ceil(mk4.c(this.j1, 24));
            if (drawable.getIntrinsicHeight() <= f) {
                return drawable.getIntrinsicHeight();
            }
        }
        return f;
    }

    public void f2(float f) {
        if (this.H0 != f) {
            this.H0 = f;
            this.k1.setStrokeWidth(f);
            if (this.L1) {
                super.m0(f);
            }
            invalidateSelf();
        }
    }

    public final float g1() {
        Drawable drawable = this.x1 ? this.X0 : this.L0;
        float f = this.N0;
        return (f > Utils.FLOAT_EPSILON || drawable == null) ? f : drawable.getIntrinsicWidth();
    }

    public void g2(int i) {
        f2(this.j1.getResources().getDimension(i));
    }

    @Override // android.graphics.drawable.Drawable
    public int getAlpha() {
        return this.z1;
    }

    @Override // android.graphics.drawable.Drawable
    public ColorFilter getColorFilter() {
        return this.A1;
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicHeight() {
        return (int) this.E0;
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicWidth() {
        return Math.min(Math.round(this.b1 + s0() + this.e1 + this.q1.f(n1().toString()) + this.f1 + w0() + this.i1), this.K1);
    }

    @Override // defpackage.o42, android.graphics.drawable.Drawable
    public int getOpacity() {
        return -3;
    }

    @Override // defpackage.o42, android.graphics.drawable.Drawable
    @TargetApi(21)
    public void getOutline(Outline outline) {
        if (this.L1) {
            super.getOutline(outline);
            return;
        }
        Rect bounds = getBounds();
        if (!bounds.isEmpty()) {
            outline.setRoundRect(bounds, this.F0);
        } else {
            outline.setRoundRect(0, 0, getIntrinsicWidth(), getIntrinsicHeight(), this.F0);
        }
        outline.setAlpha(getAlpha() / 255.0f);
    }

    public TextUtils.TruncateAt h1() {
        return this.I1;
    }

    public final void h2(ColorStateList colorStateList) {
        if (this.C0 != colorStateList) {
            this.C0 = colorStateList;
            onStateChange(getState());
        }
    }

    public z92 i1() {
        return this.a1;
    }

    public void i2(Drawable drawable) {
        Drawable X0 = X0();
        if (X0 != drawable) {
            float w0 = w0();
            this.Q0 = drawable != null ? androidx.core.graphics.drawable.a.r(drawable).mutate() : null;
            if (b93.a) {
                X2();
            }
            float w02 = w0();
            V2(X0);
            if (U2()) {
                q0(this.Q0);
            }
            invalidateSelf();
            if (w0 != w02) {
                B1();
            }
        }
    }

    @Override // android.graphics.drawable.Drawable.Callback
    public void invalidateDrawable(Drawable drawable) {
        Drawable.Callback callback = getCallback();
        if (callback != null) {
            callback.invalidateDrawable(this);
        }
    }

    @Override // defpackage.o42, android.graphics.drawable.Drawable
    public boolean isStateful() {
        return y1(this.C0) || y1(this.D0) || y1(this.G0) || (this.F1 && y1(this.G1)) || x1(this.q1.d()) || A0() || z1(this.L0) || z1(this.X0) || y1(this.C1);
    }

    public float j1() {
        return this.d1;
    }

    public void j2(CharSequence charSequence) {
        if (this.U0 != charSequence) {
            this.U0 = gp.c().h(charSequence);
            invalidateSelf();
        }
    }

    public float k1() {
        return this.c1;
    }

    public void k2(float f) {
        if (this.h1 != f) {
            this.h1 = f;
            invalidateSelf();
            if (U2()) {
                B1();
            }
        }
    }

    public ColorStateList l1() {
        return this.I0;
    }

    public void l2(int i) {
        k2(this.j1.getResources().getDimension(i));
    }

    public z92 m1() {
        return this.Z0;
    }

    public void m2(int i) {
        i2(mf.d(this.j1, i));
    }

    public CharSequence n1() {
        return this.J0;
    }

    public void n2(float f) {
        if (this.T0 != f) {
            this.T0 = f;
            invalidateSelf();
            if (U2()) {
                B1();
            }
        }
    }

    public d44 o1() {
        return this.q1.d();
    }

    public void o2(int i) {
        n2(this.j1.getResources().getDimension(i));
    }

    @Override // android.graphics.drawable.Drawable
    public boolean onLayoutDirectionChanged(int i) {
        boolean onLayoutDirectionChanged = super.onLayoutDirectionChanged(i);
        if (T2()) {
            onLayoutDirectionChanged |= androidx.core.graphics.drawable.a.m(this.L0, i);
        }
        if (S2()) {
            onLayoutDirectionChanged |= androidx.core.graphics.drawable.a.m(this.X0, i);
        }
        if (U2()) {
            onLayoutDirectionChanged |= androidx.core.graphics.drawable.a.m(this.Q0, i);
        }
        if (onLayoutDirectionChanged) {
            invalidateSelf();
            return true;
        }
        return true;
    }

    @Override // android.graphics.drawable.Drawable
    public boolean onLevelChange(int i) {
        boolean onLevelChange = super.onLevelChange(i);
        if (T2()) {
            onLevelChange |= this.L0.setLevel(i);
        }
        if (S2()) {
            onLevelChange |= this.X0.setLevel(i);
        }
        if (U2()) {
            onLevelChange |= this.Q0.setLevel(i);
        }
        if (onLevelChange) {
            invalidateSelf();
        }
        return onLevelChange;
    }

    @Override // defpackage.o42, android.graphics.drawable.Drawable, defpackage.i44.b
    public boolean onStateChange(int[] iArr) {
        if (this.L1) {
            super.onStateChange(iArr);
        }
        return C1(iArr, c1());
    }

    public float p1() {
        return this.f1;
    }

    public void p2(float f) {
        if (this.g1 != f) {
            this.g1 = f;
            invalidateSelf();
            if (U2()) {
                B1();
            }
        }
    }

    public final void q0(Drawable drawable) {
        if (drawable == null) {
            return;
        }
        drawable.setCallback(this);
        androidx.core.graphics.drawable.a.m(drawable, androidx.core.graphics.drawable.a.f(this));
        drawable.setLevel(getLevel());
        drawable.setVisible(isVisible(), false);
        if (drawable == this.Q0) {
            if (drawable.isStateful()) {
                drawable.setState(c1());
            }
            androidx.core.graphics.drawable.a.o(drawable, this.S0);
            return;
        }
        if (drawable.isStateful()) {
            drawable.setState(getState());
        }
        Drawable drawable2 = this.L0;
        if (drawable == drawable2 && this.O0) {
            androidx.core.graphics.drawable.a.o(drawable2, this.M0);
        }
    }

    public float q1() {
        return this.e1;
    }

    public void q2(int i) {
        p2(this.j1.getResources().getDimension(i));
    }

    public final void r0(Rect rect, RectF rectF) {
        rectF.setEmpty();
        if (T2() || S2()) {
            float f = this.b1 + this.c1;
            float g1 = g1();
            if (androidx.core.graphics.drawable.a.f(this) == 0) {
                float f2 = rect.left + f;
                rectF.left = f2;
                rectF.right = f2 + g1;
            } else {
                float f3 = rect.right - f;
                rectF.right = f3;
                rectF.left = f3 - g1;
            }
            float f1 = f1();
            float exactCenterY = rect.exactCenterY() - (f1 / 2.0f);
            rectF.top = exactCenterY;
            rectF.bottom = exactCenterY + f1;
        }
    }

    public final ColorFilter r1() {
        ColorFilter colorFilter = this.A1;
        return colorFilter != null ? colorFilter : this.B1;
    }

    public boolean r2(int[] iArr) {
        if (Arrays.equals(this.E1, iArr)) {
            return false;
        }
        this.E1 = iArr;
        if (U2()) {
            return C1(getState(), iArr);
        }
        return false;
    }

    public float s0() {
        return (T2() || S2()) ? this.c1 + g1() + this.d1 : Utils.FLOAT_EPSILON;
    }

    public boolean s1() {
        return this.F1;
    }

    public void s2(ColorStateList colorStateList) {
        if (this.S0 != colorStateList) {
            this.S0 = colorStateList;
            if (U2()) {
                androidx.core.graphics.drawable.a.o(this.Q0, colorStateList);
            }
            onStateChange(getState());
        }
    }

    @Override // android.graphics.drawable.Drawable.Callback
    public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
        Drawable.Callback callback = getCallback();
        if (callback != null) {
            callback.scheduleDrawable(this, runnable, j);
        }
    }

    @Override // defpackage.o42, android.graphics.drawable.Drawable
    public void setAlpha(int i) {
        if (this.z1 != i) {
            this.z1 = i;
            invalidateSelf();
        }
    }

    @Override // defpackage.o42, android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
        if (this.A1 != colorFilter) {
            this.A1 = colorFilter;
            invalidateSelf();
        }
    }

    @Override // defpackage.o42, android.graphics.drawable.Drawable, defpackage.i64
    public void setTintList(ColorStateList colorStateList) {
        if (this.C1 != colorStateList) {
            this.C1 = colorStateList;
            onStateChange(getState());
        }
    }

    @Override // defpackage.o42, android.graphics.drawable.Drawable, defpackage.i64
    public void setTintMode(PorterDuff.Mode mode) {
        if (this.D1 != mode) {
            this.D1 = mode;
            this.B1 = cr0.b(this, this.C1, mode);
            invalidateSelf();
        }
    }

    @Override // android.graphics.drawable.Drawable
    public boolean setVisible(boolean z, boolean z2) {
        boolean visible = super.setVisible(z, z2);
        if (T2()) {
            visible |= this.L0.setVisible(z, z2);
        }
        if (S2()) {
            visible |= this.X0.setVisible(z, z2);
        }
        if (U2()) {
            visible |= this.Q0.setVisible(z, z2);
        }
        if (visible) {
            invalidateSelf();
        }
        return visible;
    }

    public final void t0(Rect rect, RectF rectF) {
        rectF.set(rect);
        if (U2()) {
            float f = this.i1 + this.h1 + this.T0 + this.g1 + this.f1;
            if (androidx.core.graphics.drawable.a.f(this) == 0) {
                rectF.right = rect.right - f;
            } else {
                rectF.left = rect.left + f;
            }
        }
    }

    public void t2(int i) {
        s2(mf.c(this.j1, i));
    }

    public final void u0(Rect rect, RectF rectF) {
        rectF.setEmpty();
        if (U2()) {
            float f = this.i1 + this.h1;
            if (androidx.core.graphics.drawable.a.f(this) == 0) {
                float f2 = rect.right - f;
                rectF.right = f2;
                rectF.left = f2 - this.T0;
            } else {
                float f3 = rect.left + f;
                rectF.left = f3;
                rectF.right = f3 + this.T0;
            }
            float exactCenterY = rect.exactCenterY();
            float f4 = this.T0;
            float f5 = exactCenterY - (f4 / 2.0f);
            rectF.top = f5;
            rectF.bottom = f5 + f4;
        }
    }

    public boolean u1() {
        return this.V0;
    }

    public void u2(boolean z) {
        if (this.P0 != z) {
            boolean U2 = U2();
            this.P0 = z;
            boolean U22 = U2();
            if (U2 != U22) {
                if (U22) {
                    q0(this.Q0);
                } else {
                    V2(this.Q0);
                }
                invalidateSelf();
                B1();
            }
        }
    }

    @Override // android.graphics.drawable.Drawable.Callback
    public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        Drawable.Callback callback = getCallback();
        if (callback != null) {
            callback.unscheduleDrawable(this, runnable);
        }
    }

    public final void v0(Rect rect, RectF rectF) {
        rectF.setEmpty();
        if (U2()) {
            float f = this.i1 + this.h1 + this.T0 + this.g1 + this.f1;
            if (androidx.core.graphics.drawable.a.f(this) == 0) {
                float f2 = rect.right;
                rectF.right = f2;
                rectF.left = f2 - f;
            } else {
                int i = rect.left;
                rectF.left = i;
                rectF.right = i + f;
            }
            rectF.top = rect.top;
            rectF.bottom = rect.bottom;
        }
    }

    public boolean v1() {
        return z1(this.Q0);
    }

    public void v2(InterfaceC0116a interfaceC0116a) {
        this.H1 = new WeakReference<>(interfaceC0116a);
    }

    public float w0() {
        return U2() ? this.g1 + this.T0 + this.h1 : Utils.FLOAT_EPSILON;
    }

    public boolean w1() {
        return this.P0;
    }

    public void w2(TextUtils.TruncateAt truncateAt) {
        this.I1 = truncateAt;
    }

    public final void x0(Rect rect, RectF rectF) {
        rectF.setEmpty();
        if (this.J0 != null) {
            float s0 = this.b1 + s0() + this.e1;
            float w0 = this.i1 + w0() + this.f1;
            if (androidx.core.graphics.drawable.a.f(this) == 0) {
                rectF.left = rect.left + s0;
                rectF.right = rect.right - w0;
            } else {
                rectF.left = rect.left + w0;
                rectF.right = rect.right - s0;
            }
            rectF.top = rect.top;
            rectF.bottom = rect.bottom;
        }
    }

    public void x2(z92 z92Var) {
        this.a1 = z92Var;
    }

    public final float y0() {
        this.q1.e().getFontMetrics(this.m1);
        Paint.FontMetrics fontMetrics = this.m1;
        return (fontMetrics.descent + fontMetrics.ascent) / 2.0f;
    }

    public void y2(int i) {
        x2(z92.d(this.j1, i));
    }

    public Paint.Align z0(Rect rect, PointF pointF) {
        pointF.set(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON);
        Paint.Align align = Paint.Align.LEFT;
        if (this.J0 != null) {
            float s0 = this.b1 + s0() + this.e1;
            if (androidx.core.graphics.drawable.a.f(this) == 0) {
                pointF.x = rect.left + s0;
                align = Paint.Align.LEFT;
            } else {
                pointF.x = rect.right - s0;
                align = Paint.Align.RIGHT;
            }
            pointF.y = rect.centerY() - y0();
        }
        return align;
    }

    public void z2(float f) {
        if (this.d1 != f) {
            float s0 = s0();
            this.d1 = f;
            float s02 = s0();
            invalidateSelf();
            if (s0 != s02) {
                B1();
            }
        }
    }
}
