package com.google.android.material.bottomnavigation;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import com.google.android.material.navigation.NavigationBarMenuView;
import com.google.android.material.navigation.NavigationBarView;

/* loaded from: classes2.dex */
public class BottomNavigationView extends NavigationBarView {

    @Deprecated
    /* loaded from: classes2.dex */
    public interface a extends NavigationBarView.c {
    }

    @Deprecated
    /* loaded from: classes2.dex */
    public interface b extends NavigationBarView.d {
    }

    public BottomNavigationView(Context context) {
        this(context, null);
    }

    @Override // com.google.android.material.navigation.NavigationBarView
    public NavigationBarMenuView e(Context context) {
        return new BottomNavigationMenuView(context);
    }

    public final void g(Context context) {
        View view = new View(context);
        view.setBackgroundColor(m70.d(context, ty2.design_bottom_navigation_shadow_color));
        view.setLayoutParams(new FrameLayout.LayoutParams(-1, getResources().getDimensionPixelSize(jz2.design_bottom_navigation_shadow_height)));
        addView(view);
    }

    @Override // com.google.android.material.navigation.NavigationBarView
    public int getMaxItemCount() {
        return 5;
    }

    public final boolean h() {
        return Build.VERSION.SDK_INT < 21 && !(getBackground() instanceof o42);
    }

    public void setItemHorizontalTranslationEnabled(boolean z) {
        BottomNavigationMenuView bottomNavigationMenuView = (BottomNavigationMenuView) getMenuView();
        if (bottomNavigationMenuView.l() != z) {
            bottomNavigationMenuView.setItemHorizontalTranslationEnabled(z);
            getPresenter().d(false);
        }
    }

    @Deprecated
    public void setOnNavigationItemReselectedListener(a aVar) {
        setOnItemReselectedListener(aVar);
    }

    @Deprecated
    public void setOnNavigationItemSelectedListener(b bVar) {
        setOnItemSelectedListener(bVar);
    }

    public BottomNavigationView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, gy2.bottomNavigationStyle);
    }

    public BottomNavigationView(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, y13.Widget_Design_BottomNavigationView);
    }

    public BottomNavigationView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        Context context2 = getContext();
        l64 i3 = a54.i(context2, attributeSet, o23.BottomNavigationView, i, i2, new int[0]);
        setItemHorizontalTranslationEnabled(i3.a(o23.BottomNavigationView_itemHorizontalTranslationEnabled, true));
        i3.w();
        if (h()) {
            g(context2);
        }
    }
}
