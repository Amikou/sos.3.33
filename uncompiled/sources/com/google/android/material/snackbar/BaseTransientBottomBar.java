package com.google.android.material.snackbar;

import android.accessibilityservice.AccessibilityServiceInfo;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import android.view.WindowInsets;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityManager;
import android.widget.FrameLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.material.behavior.SwipeDismissBehavior;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.a;
import java.util.List;

/* loaded from: classes2.dex */
public abstract class BaseTransientBottomBar<B extends BaseTransientBottomBar<B>> {
    public static final Handler t;
    public static final boolean u;
    public static final int[] v;
    public static final String w;
    public final ViewGroup a;
    public final Context b;
    public final SnackbarBaseLayout c;
    public final k70 d;
    public int e;
    public boolean f;
    public View g;
    public boolean h = false;
    public final Runnable i;
    public Rect j;
    public int k;
    public int l;
    public int m;
    public int n;
    public int o;
    public List<s<B>> p;
    public Behavior q;
    public final AccessibilityManager r;
    public a.b s;

    /* loaded from: classes2.dex */
    public static class Behavior extends SwipeDismissBehavior<View> {
        public final t k = new t(this);

        @Override // com.google.android.material.behavior.SwipeDismissBehavior
        public boolean a(View view) {
            return this.k.a(view);
        }

        public final void l(BaseTransientBottomBar<?> baseTransientBottomBar) {
            this.k.c(baseTransientBottomBar);
        }

        @Override // com.google.android.material.behavior.SwipeDismissBehavior, androidx.coordinatorlayout.widget.CoordinatorLayout.Behavior
        public boolean onInterceptTouchEvent(CoordinatorLayout coordinatorLayout, View view, MotionEvent motionEvent) {
            this.k.b(coordinatorLayout, view, motionEvent);
            return super.onInterceptTouchEvent(coordinatorLayout, view, motionEvent);
        }
    }

    /* loaded from: classes2.dex */
    public static class SnackbarBaseLayout extends FrameLayout {
        public static final View.OnTouchListener l0 = new a();
        public v a;
        public u f0;
        public int g0;
        public final float h0;
        public final float i0;
        public ColorStateList j0;
        public PorterDuff.Mode k0;

        /* loaded from: classes2.dex */
        public static class a implements View.OnTouchListener {
            @Override // android.view.View.OnTouchListener
            @SuppressLint({"ClickableViewAccessibility"})
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        }

        public SnackbarBaseLayout(Context context) {
            this(context, null);
        }

        public final Drawable a() {
            float dimension = getResources().getDimension(jz2.mtrl_snackbar_background_corner_radius);
            GradientDrawable gradientDrawable = new GradientDrawable();
            gradientDrawable.setShape(0);
            gradientDrawable.setCornerRadius(dimension);
            gradientDrawable.setColor(l42.i(this, gy2.colorSurface, gy2.colorOnSurface, getBackgroundOverlayColorAlpha()));
            if (this.j0 != null) {
                Drawable r = androidx.core.graphics.drawable.a.r(gradientDrawable);
                androidx.core.graphics.drawable.a.o(r, this.j0);
                return r;
            }
            return androidx.core.graphics.drawable.a.r(gradientDrawable);
        }

        public float getActionTextColorAlpha() {
            return this.i0;
        }

        public int getAnimationMode() {
            return this.g0;
        }

        public float getBackgroundOverlayColorAlpha() {
            return this.h0;
        }

        @Override // android.view.ViewGroup, android.view.View
        public void onAttachedToWindow() {
            super.onAttachedToWindow();
            u uVar = this.f0;
            if (uVar != null) {
                uVar.onViewAttachedToWindow(this);
            }
            ei4.q0(this);
        }

        @Override // android.view.ViewGroup, android.view.View
        public void onDetachedFromWindow() {
            super.onDetachedFromWindow();
            u uVar = this.f0;
            if (uVar != null) {
                uVar.onViewDetachedFromWindow(this);
            }
        }

        @Override // android.widget.FrameLayout, android.view.ViewGroup, android.view.View
        public void onLayout(boolean z, int i, int i2, int i3, int i4) {
            super.onLayout(z, i, i2, i3, i4);
            v vVar = this.a;
            if (vVar != null) {
                vVar.a(this, i, i2, i3, i4);
            }
        }

        public void setAnimationMode(int i) {
            this.g0 = i;
        }

        @Override // android.view.View
        public void setBackground(Drawable drawable) {
            setBackgroundDrawable(drawable);
        }

        @Override // android.view.View
        public void setBackgroundDrawable(Drawable drawable) {
            if (drawable != null && this.j0 != null) {
                drawable = androidx.core.graphics.drawable.a.r(drawable.mutate());
                androidx.core.graphics.drawable.a.o(drawable, this.j0);
                androidx.core.graphics.drawable.a.p(drawable, this.k0);
            }
            super.setBackgroundDrawable(drawable);
        }

        @Override // android.view.View
        public void setBackgroundTintList(ColorStateList colorStateList) {
            this.j0 = colorStateList;
            if (getBackground() != null) {
                Drawable r = androidx.core.graphics.drawable.a.r(getBackground().mutate());
                androidx.core.graphics.drawable.a.o(r, colorStateList);
                androidx.core.graphics.drawable.a.p(r, this.k0);
                if (r != getBackground()) {
                    super.setBackgroundDrawable(r);
                }
            }
        }

        @Override // android.view.View
        public void setBackgroundTintMode(PorterDuff.Mode mode) {
            this.k0 = mode;
            if (getBackground() != null) {
                Drawable r = androidx.core.graphics.drawable.a.r(getBackground().mutate());
                androidx.core.graphics.drawable.a.p(r, mode);
                if (r != getBackground()) {
                    super.setBackgroundDrawable(r);
                }
            }
        }

        public void setOnAttachStateChangeListener(u uVar) {
            this.f0 = uVar;
        }

        @Override // android.view.View
        public void setOnClickListener(View.OnClickListener onClickListener) {
            setOnTouchListener(onClickListener != null ? null : l0);
            super.setOnClickListener(onClickListener);
        }

        public void setOnLayoutChangeListener(v vVar) {
            this.a = vVar;
        }

        public SnackbarBaseLayout(Context context, AttributeSet attributeSet) {
            super(r42.c(context, attributeSet, 0, 0), attributeSet);
            Context context2 = getContext();
            TypedArray obtainStyledAttributes = context2.obtainStyledAttributes(attributeSet, o23.SnackbarLayout);
            int i = o23.SnackbarLayout_elevation;
            if (obtainStyledAttributes.hasValue(i)) {
                ei4.A0(this, obtainStyledAttributes.getDimensionPixelSize(i, 0));
            }
            this.g0 = obtainStyledAttributes.getInt(o23.SnackbarLayout_animationMode, 0);
            this.h0 = obtainStyledAttributes.getFloat(o23.SnackbarLayout_backgroundOverlayColorAlpha, 1.0f);
            setBackgroundTintList(n42.b(context2, obtainStyledAttributes, o23.SnackbarLayout_backgroundTint));
            setBackgroundTintMode(mk4.i(obtainStyledAttributes.getInt(o23.SnackbarLayout_backgroundTintMode, -1), PorterDuff.Mode.SRC_IN));
            this.i0 = obtainStyledAttributes.getFloat(o23.SnackbarLayout_actionTextColorAlpha, 1.0f);
            obtainStyledAttributes.recycle();
            setOnTouchListener(l0);
            setFocusable(true);
            if (getBackground() == null) {
                ei4.w0(this, a());
            }
        }
    }

    /* loaded from: classes2.dex */
    public class a implements Runnable {
        public a() {
        }

        @Override // java.lang.Runnable
        public void run() {
            SnackbarBaseLayout snackbarBaseLayout = BaseTransientBottomBar.this.c;
            if (snackbarBaseLayout == null) {
                return;
            }
            if (snackbarBaseLayout.getParent() != null) {
                BaseTransientBottomBar.this.c.setVisibility(0);
            }
            if (BaseTransientBottomBar.this.c.getAnimationMode() == 1) {
                BaseTransientBottomBar.this.T();
            } else {
                BaseTransientBottomBar.this.V();
            }
        }
    }

    /* loaded from: classes2.dex */
    public class b extends AnimatorListenerAdapter {
        public b() {
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            BaseTransientBottomBar.this.L();
        }
    }

    /* loaded from: classes2.dex */
    public class c extends AnimatorListenerAdapter {
        public final /* synthetic */ int a;

        public c(int i) {
            this.a = i;
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            BaseTransientBottomBar.this.K(this.a);
        }
    }

    /* loaded from: classes2.dex */
    public class d implements ValueAnimator.AnimatorUpdateListener {
        public d() {
        }

        @Override // android.animation.ValueAnimator.AnimatorUpdateListener
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            BaseTransientBottomBar.this.c.setAlpha(((Float) valueAnimator.getAnimatedValue()).floatValue());
        }
    }

    /* loaded from: classes2.dex */
    public class e implements ValueAnimator.AnimatorUpdateListener {
        public e() {
        }

        @Override // android.animation.ValueAnimator.AnimatorUpdateListener
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            float floatValue = ((Float) valueAnimator.getAnimatedValue()).floatValue();
            BaseTransientBottomBar.this.c.setScaleX(floatValue);
            BaseTransientBottomBar.this.c.setScaleY(floatValue);
        }
    }

    /* loaded from: classes2.dex */
    public class f extends AnimatorListenerAdapter {
        public f() {
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            BaseTransientBottomBar.this.L();
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationStart(Animator animator) {
            BaseTransientBottomBar.this.d.a(70, 180);
        }
    }

    /* loaded from: classes2.dex */
    public class g implements ValueAnimator.AnimatorUpdateListener {
        public int a;
        public final /* synthetic */ int f0;

        public g(int i) {
            this.f0 = i;
            this.a = i;
        }

        @Override // android.animation.ValueAnimator.AnimatorUpdateListener
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            int intValue = ((Integer) valueAnimator.getAnimatedValue()).intValue();
            if (BaseTransientBottomBar.u) {
                ei4.d0(BaseTransientBottomBar.this.c, intValue - this.a);
            } else {
                BaseTransientBottomBar.this.c.setTranslationY(intValue);
            }
            this.a = intValue;
        }
    }

    /* loaded from: classes2.dex */
    public class h extends AnimatorListenerAdapter {
        public final /* synthetic */ int a;

        public h(int i) {
            this.a = i;
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            BaseTransientBottomBar.this.K(this.a);
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationStart(Animator animator) {
            BaseTransientBottomBar.this.d.b(0, 180);
        }
    }

    /* loaded from: classes2.dex */
    public class i implements ValueAnimator.AnimatorUpdateListener {
        public int a = 0;

        public i() {
        }

        @Override // android.animation.ValueAnimator.AnimatorUpdateListener
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            int intValue = ((Integer) valueAnimator.getAnimatedValue()).intValue();
            if (BaseTransientBottomBar.u) {
                ei4.d0(BaseTransientBottomBar.this.c, intValue - this.a);
            } else {
                BaseTransientBottomBar.this.c.setTranslationY(intValue);
            }
            this.a = intValue;
        }
    }

    /* loaded from: classes2.dex */
    public static class j implements Handler.Callback {
        @Override // android.os.Handler.Callback
        public boolean handleMessage(Message message) {
            int i = message.what;
            if (i == 0) {
                ((BaseTransientBottomBar) message.obj).R();
                return true;
            } else if (i != 1) {
                return false;
            } else {
                ((BaseTransientBottomBar) message.obj).H(message.arg1);
                return true;
            }
        }
    }

    /* loaded from: classes2.dex */
    public class k implements ViewTreeObserver.OnGlobalLayoutListener {
        public k() {
        }

        @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
        public void onGlobalLayout() {
            if (BaseTransientBottomBar.this.h) {
                BaseTransientBottomBar baseTransientBottomBar = BaseTransientBottomBar.this;
                baseTransientBottomBar.o = baseTransientBottomBar.u();
                BaseTransientBottomBar.this.X();
            }
        }
    }

    /* loaded from: classes2.dex */
    public class l implements Runnable {
        public l() {
        }

        @Override // java.lang.Runnable
        public void run() {
            int C;
            BaseTransientBottomBar baseTransientBottomBar = BaseTransientBottomBar.this;
            if (baseTransientBottomBar.c == null || baseTransientBottomBar.b == null || (C = (BaseTransientBottomBar.this.C() - BaseTransientBottomBar.this.F()) + ((int) BaseTransientBottomBar.this.c.getTranslationY())) >= BaseTransientBottomBar.this.n) {
                return;
            }
            ViewGroup.LayoutParams layoutParams = BaseTransientBottomBar.this.c.getLayoutParams();
            if (!(layoutParams instanceof ViewGroup.MarginLayoutParams)) {
                String unused = BaseTransientBottomBar.w;
                return;
            }
            ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin += BaseTransientBottomBar.this.n - C;
            BaseTransientBottomBar.this.c.requestLayout();
        }
    }

    /* loaded from: classes2.dex */
    public class m implements em2 {
        public m() {
        }

        @Override // defpackage.em2
        public jp4 a(View view, jp4 jp4Var) {
            BaseTransientBottomBar.this.k = jp4Var.j();
            BaseTransientBottomBar.this.l = jp4Var.k();
            BaseTransientBottomBar.this.m = jp4Var.l();
            BaseTransientBottomBar.this.X();
            return jp4Var;
        }
    }

    /* loaded from: classes2.dex */
    public class n extends z5 {
        public n() {
        }

        @Override // defpackage.z5
        public void g(View view, b6 b6Var) {
            super.g(view, b6Var);
            b6Var.a(1048576);
            b6Var.h0(true);
        }

        @Override // defpackage.z5
        public boolean j(View view, int i, Bundle bundle) {
            if (i == 1048576) {
                BaseTransientBottomBar.this.v();
                return true;
            }
            return super.j(view, i, bundle);
        }
    }

    /* loaded from: classes2.dex */
    public class o implements a.b {
        public o() {
        }

        @Override // com.google.android.material.snackbar.a.b
        public void a(int i) {
            Handler handler = BaseTransientBottomBar.t;
            handler.sendMessage(handler.obtainMessage(1, i, 0, BaseTransientBottomBar.this));
        }

        @Override // com.google.android.material.snackbar.a.b
        public void b() {
            Handler handler = BaseTransientBottomBar.t;
            handler.sendMessage(handler.obtainMessage(0, BaseTransientBottomBar.this));
        }
    }

    /* loaded from: classes2.dex */
    public class p implements u {

        /* loaded from: classes2.dex */
        public class a implements Runnable {
            public a() {
            }

            @Override // java.lang.Runnable
            public void run() {
                BaseTransientBottomBar.this.K(3);
            }
        }

        public p() {
        }

        @Override // com.google.android.material.snackbar.BaseTransientBottomBar.u
        public void onViewAttachedToWindow(View view) {
            WindowInsets rootWindowInsets;
            if (Build.VERSION.SDK_INT < 29 || (rootWindowInsets = BaseTransientBottomBar.this.c.getRootWindowInsets()) == null) {
                return;
            }
            BaseTransientBottomBar.this.n = rootWindowInsets.getMandatorySystemGestureInsets().bottom;
            BaseTransientBottomBar.this.X();
        }

        @Override // com.google.android.material.snackbar.BaseTransientBottomBar.u
        public void onViewDetachedFromWindow(View view) {
            if (BaseTransientBottomBar.this.I()) {
                BaseTransientBottomBar.t.post(new a());
            }
        }
    }

    /* loaded from: classes2.dex */
    public class q implements v {
        public q() {
        }

        @Override // com.google.android.material.snackbar.BaseTransientBottomBar.v
        public void a(View view, int i, int i2, int i3, int i4) {
            BaseTransientBottomBar.this.c.setOnLayoutChangeListener(null);
            BaseTransientBottomBar.this.S();
        }
    }

    /* loaded from: classes2.dex */
    public class r implements SwipeDismissBehavior.c {
        public r() {
        }

        @Override // com.google.android.material.behavior.SwipeDismissBehavior.c
        public void a(View view) {
            if (view.getParent() != null) {
                view.setVisibility(8);
            }
            BaseTransientBottomBar.this.w(0);
        }

        @Override // com.google.android.material.behavior.SwipeDismissBehavior.c
        public void b(int i) {
            if (i == 0) {
                com.google.android.material.snackbar.a.c().k(BaseTransientBottomBar.this.s);
            } else if (i == 1 || i == 2) {
                com.google.android.material.snackbar.a.c().j(BaseTransientBottomBar.this.s);
            }
        }
    }

    /* loaded from: classes2.dex */
    public static abstract class s<B> {
        public void a(B b, int i) {
        }

        public void b(B b) {
        }
    }

    /* loaded from: classes2.dex */
    public static class t {
        public a.b a;

        public t(SwipeDismissBehavior<?> swipeDismissBehavior) {
            swipeDismissBehavior.h(0.1f);
            swipeDismissBehavior.f(0.6f);
            swipeDismissBehavior.i(0);
        }

        public boolean a(View view) {
            return view instanceof SnackbarBaseLayout;
        }

        public void b(CoordinatorLayout coordinatorLayout, View view, MotionEvent motionEvent) {
            int actionMasked = motionEvent.getActionMasked();
            if (actionMasked == 0) {
                if (coordinatorLayout.isPointInChildBounds(view, (int) motionEvent.getX(), (int) motionEvent.getY())) {
                    com.google.android.material.snackbar.a.c().j(this.a);
                }
            } else if (actionMasked == 1 || actionMasked == 3) {
                com.google.android.material.snackbar.a.c().k(this.a);
            }
        }

        public void c(BaseTransientBottomBar<?> baseTransientBottomBar) {
            this.a = baseTransientBottomBar.s;
        }
    }

    /* loaded from: classes2.dex */
    public interface u {
        void onViewAttachedToWindow(View view);

        void onViewDetachedFromWindow(View view);
    }

    /* loaded from: classes2.dex */
    public interface v {
        void a(View view, int i, int i2, int i3, int i4);
    }

    static {
        int i2 = Build.VERSION.SDK_INT;
        u = i2 >= 16 && i2 <= 19;
        v = new int[]{gy2.snackbarStyle};
        w = BaseTransientBottomBar.class.getSimpleName();
        t = new Handler(Looper.getMainLooper(), new j());
    }

    public BaseTransientBottomBar(Context context, ViewGroup viewGroup, View view, k70 k70Var) {
        new k();
        this.i = new l();
        this.s = new o();
        if (viewGroup == null) {
            throw new IllegalArgumentException("Transient bottom bar must have non-null parent");
        }
        if (view == null) {
            throw new IllegalArgumentException("Transient bottom bar must have non-null content");
        }
        if (k70Var != null) {
            this.a = viewGroup;
            this.d = k70Var;
            this.b = context;
            a54.a(context);
            SnackbarBaseLayout snackbarBaseLayout = (SnackbarBaseLayout) LayoutInflater.from(context).inflate(D(), viewGroup, false);
            this.c = snackbarBaseLayout;
            if (view instanceof SnackbarContentLayout) {
                ((SnackbarContentLayout) view).c(snackbarBaseLayout.getActionTextColorAlpha());
            }
            snackbarBaseLayout.addView(view);
            ViewGroup.LayoutParams layoutParams = snackbarBaseLayout.getLayoutParams();
            if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
                ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
                this.j = new Rect(marginLayoutParams.leftMargin, marginLayoutParams.topMargin, marginLayoutParams.rightMargin, marginLayoutParams.bottomMargin);
            }
            ei4.v0(snackbarBaseLayout, 1);
            ei4.D0(snackbarBaseLayout, 1);
            ei4.B0(snackbarBaseLayout, true);
            ei4.G0(snackbarBaseLayout, new m());
            ei4.t0(snackbarBaseLayout, new n());
            this.r = (AccessibilityManager) context.getSystemService("accessibility");
            return;
        }
        throw new IllegalArgumentException("Transient bottom bar must have non-null callback");
    }

    public SwipeDismissBehavior<? extends View> A() {
        return new Behavior();
    }

    public final ValueAnimator B(float... fArr) {
        ValueAnimator ofFloat = ValueAnimator.ofFloat(fArr);
        ofFloat.setInterpolator(ne.d);
        ofFloat.addUpdateListener(new e());
        return ofFloat;
    }

    public final int C() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) this.b.getSystemService("window")).getDefaultDisplay().getRealMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }

    public int D() {
        return G() ? x03.mtrl_layout_snackbar : x03.design_layout_snackbar;
    }

    public final int E() {
        int height = this.c.getHeight();
        ViewGroup.LayoutParams layoutParams = this.c.getLayoutParams();
        return layoutParams instanceof ViewGroup.MarginLayoutParams ? height + ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin : height;
    }

    public final int F() {
        int[] iArr = new int[2];
        this.c.getLocationOnScreen(iArr);
        return iArr[1] + this.c.getHeight();
    }

    public boolean G() {
        TypedArray obtainStyledAttributes = this.b.obtainStyledAttributes(v);
        int resourceId = obtainStyledAttributes.getResourceId(0, -1);
        obtainStyledAttributes.recycle();
        return resourceId != -1;
    }

    public final void H(int i2) {
        if (O() && this.c.getVisibility() == 0) {
            t(i2);
        } else {
            K(i2);
        }
    }

    public boolean I() {
        return com.google.android.material.snackbar.a.c().e(this.s);
    }

    public final boolean J() {
        ViewGroup.LayoutParams layoutParams = this.c.getLayoutParams();
        return (layoutParams instanceof CoordinatorLayout.e) && (((CoordinatorLayout.e) layoutParams).f() instanceof SwipeDismissBehavior);
    }

    public void K(int i2) {
        com.google.android.material.snackbar.a.c().h(this.s);
        List<s<B>> list = this.p;
        if (list != null) {
            for (int size = list.size() - 1; size >= 0; size--) {
                this.p.get(size).a(this, i2);
            }
        }
        ViewParent parent = this.c.getParent();
        if (parent instanceof ViewGroup) {
            ((ViewGroup) parent).removeView(this.c);
        }
    }

    public void L() {
        com.google.android.material.snackbar.a.c().i(this.s);
        List<s<B>> list = this.p;
        if (list != null) {
            for (int size = list.size() - 1; size >= 0; size--) {
                this.p.get(size).b(this);
            }
        }
    }

    public B M(int i2) {
        this.e = i2;
        return this;
    }

    public final void N(CoordinatorLayout.e eVar) {
        SwipeDismissBehavior<? extends View> swipeDismissBehavior = this.q;
        if (swipeDismissBehavior == null) {
            swipeDismissBehavior = A();
        }
        if (swipeDismissBehavior instanceof Behavior) {
            ((Behavior) swipeDismissBehavior).l(this);
        }
        swipeDismissBehavior.g(new r());
        eVar.o(swipeDismissBehavior);
        if (this.g == null) {
            eVar.g = 80;
        }
    }

    public boolean O() {
        AccessibilityManager accessibilityManager = this.r;
        if (accessibilityManager == null) {
            return true;
        }
        List<AccessibilityServiceInfo> enabledAccessibilityServiceList = accessibilityManager.getEnabledAccessibilityServiceList(1);
        return enabledAccessibilityServiceList != null && enabledAccessibilityServiceList.isEmpty();
    }

    public final boolean P() {
        return this.n > 0 && !this.f && J();
    }

    public void Q() {
        com.google.android.material.snackbar.a.c().m(z(), this.s);
    }

    public final void R() {
        this.c.setOnAttachStateChangeListener(new p());
        if (this.c.getParent() == null) {
            ViewGroup.LayoutParams layoutParams = this.c.getLayoutParams();
            if (layoutParams instanceof CoordinatorLayout.e) {
                N((CoordinatorLayout.e) layoutParams);
            }
            this.o = u();
            X();
            this.c.setVisibility(4);
            this.a.addView(this.c);
        }
        if (ei4.W(this.c)) {
            S();
        } else {
            this.c.setOnLayoutChangeListener(new q());
        }
    }

    public final void S() {
        if (O()) {
            s();
            return;
        }
        if (this.c.getParent() != null) {
            this.c.setVisibility(0);
        }
        L();
    }

    public final void T() {
        ValueAnimator x = x(Utils.FLOAT_EPSILON, 1.0f);
        ValueAnimator B = B(0.8f, 1.0f);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(x, B);
        animatorSet.setDuration(150L);
        animatorSet.addListener(new b());
        animatorSet.start();
    }

    public final void U(int i2) {
        ValueAnimator x = x(1.0f, Utils.FLOAT_EPSILON);
        x.setDuration(75L);
        x.addListener(new c(i2));
        x.start();
    }

    public final void V() {
        int E = E();
        if (u) {
            ei4.d0(this.c, E);
        } else {
            this.c.setTranslationY(E);
        }
        ValueAnimator valueAnimator = new ValueAnimator();
        valueAnimator.setIntValues(E, 0);
        valueAnimator.setInterpolator(ne.b);
        valueAnimator.setDuration(250L);
        valueAnimator.addListener(new f());
        valueAnimator.addUpdateListener(new g(E));
        valueAnimator.start();
    }

    public final void W(int i2) {
        ValueAnimator valueAnimator = new ValueAnimator();
        valueAnimator.setIntValues(0, E());
        valueAnimator.setInterpolator(ne.b);
        valueAnimator.setDuration(250L);
        valueAnimator.addListener(new h(i2));
        valueAnimator.addUpdateListener(new i());
        valueAnimator.start();
    }

    public final void X() {
        Rect rect;
        ViewGroup.LayoutParams layoutParams = this.c.getLayoutParams();
        if (!(layoutParams instanceof ViewGroup.MarginLayoutParams) || (rect = this.j) == null) {
            return;
        }
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
        marginLayoutParams.bottomMargin = rect.bottom + (this.g != null ? this.o : this.k);
        marginLayoutParams.leftMargin = rect.left + this.l;
        marginLayoutParams.rightMargin = rect.right + this.m;
        this.c.requestLayout();
        if (Build.VERSION.SDK_INT < 29 || !P()) {
            return;
        }
        this.c.removeCallbacks(this.i);
        this.c.post(this.i);
    }

    public void s() {
        this.c.post(new a());
    }

    public final void t(int i2) {
        if (this.c.getAnimationMode() == 1) {
            U(i2);
        } else {
            W(i2);
        }
    }

    public final int u() {
        View view = this.g;
        if (view == null) {
            return 0;
        }
        int[] iArr = new int[2];
        view.getLocationOnScreen(iArr);
        int i2 = iArr[1];
        int[] iArr2 = new int[2];
        this.a.getLocationOnScreen(iArr2);
        return (iArr2[1] + this.a.getHeight()) - i2;
    }

    public void v() {
        w(3);
    }

    public void w(int i2) {
        com.google.android.material.snackbar.a.c().b(this.s, i2);
    }

    public final ValueAnimator x(float... fArr) {
        ValueAnimator ofFloat = ValueAnimator.ofFloat(fArr);
        ofFloat.setInterpolator(ne.a);
        ofFloat.addUpdateListener(new d());
        return ofFloat;
    }

    public Context y() {
        return this.b;
    }

    public int z() {
        return this.e;
    }
}
