package com.google.android.material.imageview;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.Outline;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewOutlineProvider;
import androidx.appcompat.widget.AppCompatImageView;
import com.github.mikephil.charting.utils.Utils;

/* loaded from: classes2.dex */
public class ShapeableImageView extends AppCompatImageView implements sn3 {
    public static final int w0 = y13.Widget_MaterialComponents_ShapeableImageView;
    public final qn3 a;
    public final RectF f0;
    public final RectF g0;
    public final Paint h0;
    public final Paint i0;
    public final Path j0;
    public ColorStateList k0;
    public o42 l0;
    public pn3 m0;
    public float n0;
    public Path o0;
    public int p0;
    public int q0;
    public int r0;
    public int s0;
    public int t0;
    public int u0;
    public boolean v0;

    @TargetApi(21)
    /* loaded from: classes2.dex */
    public class a extends ViewOutlineProvider {
        public final Rect a = new Rect();

        public a() {
        }

        @Override // android.view.ViewOutlineProvider
        public void getOutline(View view, Outline outline) {
            if (ShapeableImageView.this.m0 == null) {
                return;
            }
            if (ShapeableImageView.this.l0 == null) {
                ShapeableImageView.this.l0 = new o42(ShapeableImageView.this.m0);
            }
            ShapeableImageView.this.f0.round(this.a);
            ShapeableImageView.this.l0.setBounds(this.a);
            ShapeableImageView.this.l0.getOutline(outline);
        }
    }

    public ShapeableImageView(Context context) {
        this(context, null, 0);
    }

    public final void g(Canvas canvas) {
        if (this.k0 == null) {
            return;
        }
        this.h0.setStrokeWidth(this.n0);
        int colorForState = this.k0.getColorForState(getDrawableState(), this.k0.getDefaultColor());
        if (this.n0 <= Utils.FLOAT_EPSILON || colorForState == 0) {
            return;
        }
        this.h0.setColor(colorForState);
        canvas.drawPath(this.j0, this.h0);
    }

    public int getContentPaddingBottom() {
        return this.s0;
    }

    public final int getContentPaddingEnd() {
        int i = this.u0;
        return i != Integer.MIN_VALUE ? i : i() ? this.p0 : this.r0;
    }

    public int getContentPaddingLeft() {
        int i;
        int i2;
        if (h()) {
            if (i() && (i2 = this.u0) != Integer.MIN_VALUE) {
                return i2;
            }
            if (!i() && (i = this.t0) != Integer.MIN_VALUE) {
                return i;
            }
        }
        return this.p0;
    }

    public int getContentPaddingRight() {
        int i;
        int i2;
        if (h()) {
            if (i() && (i2 = this.t0) != Integer.MIN_VALUE) {
                return i2;
            }
            if (!i() && (i = this.u0) != Integer.MIN_VALUE) {
                return i;
            }
        }
        return this.r0;
    }

    public final int getContentPaddingStart() {
        int i = this.t0;
        return i != Integer.MIN_VALUE ? i : i() ? this.r0 : this.p0;
    }

    public int getContentPaddingTop() {
        return this.q0;
    }

    @Override // android.view.View
    public int getPaddingBottom() {
        return super.getPaddingBottom() - getContentPaddingBottom();
    }

    @Override // android.view.View
    public int getPaddingEnd() {
        return super.getPaddingEnd() - getContentPaddingEnd();
    }

    @Override // android.view.View
    public int getPaddingLeft() {
        return super.getPaddingLeft() - getContentPaddingLeft();
    }

    @Override // android.view.View
    public int getPaddingRight() {
        return super.getPaddingRight() - getContentPaddingRight();
    }

    @Override // android.view.View
    public int getPaddingStart() {
        return super.getPaddingStart() - getContentPaddingStart();
    }

    @Override // android.view.View
    public int getPaddingTop() {
        return super.getPaddingTop() - getContentPaddingTop();
    }

    public pn3 getShapeAppearanceModel() {
        return this.m0;
    }

    public ColorStateList getStrokeColor() {
        return this.k0;
    }

    public float getStrokeWidth() {
        return this.n0;
    }

    public final boolean h() {
        return (this.t0 == Integer.MIN_VALUE && this.u0 == Integer.MIN_VALUE) ? false : true;
    }

    public final boolean i() {
        return Build.VERSION.SDK_INT >= 17 && getLayoutDirection() == 1;
    }

    public final void j(int i, int i2) {
        this.f0.set(getPaddingLeft(), getPaddingTop(), i - getPaddingRight(), i2 - getPaddingBottom());
        this.a.e(this.m0, 1.0f, this.f0, this.j0);
        this.o0.rewind();
        this.o0.addPath(this.j0);
        this.g0.set(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, i, i2);
        this.o0.addRect(this.g0, Path.Direction.CCW);
    }

    @Override // android.widget.ImageView, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        setLayerType(2, null);
    }

    @Override // android.widget.ImageView, android.view.View
    public void onDetachedFromWindow() {
        setLayerType(0, null);
        super.onDetachedFromWindow();
    }

    @Override // android.widget.ImageView, android.view.View
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawPath(this.o0, this.i0);
        g(canvas);
    }

    @Override // android.widget.ImageView, android.view.View
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (this.v0) {
            return;
        }
        int i3 = Build.VERSION.SDK_INT;
        if (i3 <= 19 || isLayoutDirectionResolved()) {
            this.v0 = true;
            if (i3 >= 21 && (isPaddingRelative() || h())) {
                setPaddingRelative(super.getPaddingStart(), super.getPaddingTop(), super.getPaddingEnd(), super.getPaddingBottom());
            } else {
                setPadding(super.getPaddingLeft(), super.getPaddingTop(), super.getPaddingRight(), super.getPaddingBottom());
            }
        }
    }

    @Override // android.view.View
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        j(i, i2);
    }

    public void setContentPadding(int i, int i2, int i3, int i4) {
        this.t0 = Integer.MIN_VALUE;
        this.u0 = Integer.MIN_VALUE;
        super.setPadding((super.getPaddingLeft() - this.p0) + i, (super.getPaddingTop() - this.q0) + i2, (super.getPaddingRight() - this.r0) + i3, (super.getPaddingBottom() - this.s0) + i4);
        this.p0 = i;
        this.q0 = i2;
        this.r0 = i3;
        this.s0 = i4;
    }

    public void setContentPaddingRelative(int i, int i2, int i3, int i4) {
        super.setPaddingRelative((super.getPaddingStart() - getContentPaddingStart()) + i, (super.getPaddingTop() - this.q0) + i2, (super.getPaddingEnd() - getContentPaddingEnd()) + i3, (super.getPaddingBottom() - this.s0) + i4);
        this.p0 = i() ? i3 : i;
        this.q0 = i2;
        if (!i()) {
            i = i3;
        }
        this.r0 = i;
        this.s0 = i4;
    }

    @Override // android.view.View
    public void setPadding(int i, int i2, int i3, int i4) {
        super.setPadding(i + getContentPaddingLeft(), i2 + getContentPaddingTop(), i3 + getContentPaddingRight(), i4 + getContentPaddingBottom());
    }

    @Override // android.view.View
    public void setPaddingRelative(int i, int i2, int i3, int i4) {
        super.setPaddingRelative(i + getContentPaddingStart(), i2 + getContentPaddingTop(), i3 + getContentPaddingEnd(), i4 + getContentPaddingBottom());
    }

    @Override // defpackage.sn3
    public void setShapeAppearanceModel(pn3 pn3Var) {
        this.m0 = pn3Var;
        o42 o42Var = this.l0;
        if (o42Var != null) {
            o42Var.setShapeAppearanceModel(pn3Var);
        }
        j(getWidth(), getHeight());
        invalidate();
        if (Build.VERSION.SDK_INT >= 21) {
            invalidateOutline();
        }
    }

    public void setStrokeColor(ColorStateList colorStateList) {
        this.k0 = colorStateList;
        invalidate();
    }

    public void setStrokeColorResource(int i) {
        setStrokeColor(mf.c(getContext(), i));
    }

    public void setStrokeWidth(float f) {
        if (this.n0 != f) {
            this.n0 = f;
            invalidate();
        }
    }

    public void setStrokeWidthResource(int i) {
        setStrokeWidth(getResources().getDimensionPixelSize(i));
    }

    public ShapeableImageView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public ShapeableImageView(android.content.Context r7, android.util.AttributeSet r8, int r9) {
        /*
            r6 = this;
            int r0 = com.google.android.material.imageview.ShapeableImageView.w0
            android.content.Context r7 = defpackage.r42.c(r7, r8, r9, r0)
            r6.<init>(r7, r8, r9)
            qn3 r7 = defpackage.qn3.k()
            r6.a = r7
            android.graphics.Path r7 = new android.graphics.Path
            r7.<init>()
            r6.j0 = r7
            r7 = 0
            r6.v0 = r7
            android.content.Context r1 = r6.getContext()
            android.graphics.Paint r2 = new android.graphics.Paint
            r2.<init>()
            r6.i0 = r2
            r3 = 1
            r2.setAntiAlias(r3)
            r4 = -1
            r2.setColor(r4)
            android.graphics.PorterDuffXfermode r4 = new android.graphics.PorterDuffXfermode
            android.graphics.PorterDuff$Mode r5 = android.graphics.PorterDuff.Mode.DST_OUT
            r4.<init>(r5)
            r2.setXfermode(r4)
            android.graphics.RectF r2 = new android.graphics.RectF
            r2.<init>()
            r6.f0 = r2
            android.graphics.RectF r2 = new android.graphics.RectF
            r2.<init>()
            r6.g0 = r2
            android.graphics.Path r2 = new android.graphics.Path
            r2.<init>()
            r6.o0 = r2
            int[] r2 = defpackage.o23.ShapeableImageView
            android.content.res.TypedArray r2 = r1.obtainStyledAttributes(r8, r2, r9, r0)
            int r4 = defpackage.o23.ShapeableImageView_strokeColor
            android.content.res.ColorStateList r4 = defpackage.n42.b(r1, r2, r4)
            r6.k0 = r4
            int r4 = defpackage.o23.ShapeableImageView_strokeWidth
            int r4 = r2.getDimensionPixelSize(r4, r7)
            float r4 = (float) r4
            r6.n0 = r4
            int r4 = defpackage.o23.ShapeableImageView_contentPadding
            int r7 = r2.getDimensionPixelSize(r4, r7)
            r6.p0 = r7
            r6.q0 = r7
            r6.r0 = r7
            r6.s0 = r7
            int r4 = defpackage.o23.ShapeableImageView_contentPaddingLeft
            int r4 = r2.getDimensionPixelSize(r4, r7)
            r6.p0 = r4
            int r4 = defpackage.o23.ShapeableImageView_contentPaddingTop
            int r4 = r2.getDimensionPixelSize(r4, r7)
            r6.q0 = r4
            int r4 = defpackage.o23.ShapeableImageView_contentPaddingRight
            int r4 = r2.getDimensionPixelSize(r4, r7)
            r6.r0 = r4
            int r4 = defpackage.o23.ShapeableImageView_contentPaddingBottom
            int r7 = r2.getDimensionPixelSize(r4, r7)
            r6.s0 = r7
            int r7 = defpackage.o23.ShapeableImageView_contentPaddingStart
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            int r7 = r2.getDimensionPixelSize(r7, r4)
            r6.t0 = r7
            int r7 = defpackage.o23.ShapeableImageView_contentPaddingEnd
            int r7 = r2.getDimensionPixelSize(r7, r4)
            r6.u0 = r7
            r2.recycle()
            android.graphics.Paint r7 = new android.graphics.Paint
            r7.<init>()
            r6.h0 = r7
            android.graphics.Paint$Style r2 = android.graphics.Paint.Style.STROKE
            r7.setStyle(r2)
            r7.setAntiAlias(r3)
            pn3$b r7 = defpackage.pn3.e(r1, r8, r9, r0)
            pn3 r7 = r7.m()
            r6.m0 = r7
            int r7 = android.os.Build.VERSION.SDK_INT
            r8 = 21
            if (r7 < r8) goto Lcc
            com.google.android.material.imageview.ShapeableImageView$a r7 = new com.google.android.material.imageview.ShapeableImageView$a
            r7.<init>()
            r6.setOutlineProvider(r7)
        Lcc:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.imageview.ShapeableImageView.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }
}
