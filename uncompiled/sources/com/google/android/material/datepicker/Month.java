package com.google.android.material.datepicker;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;

/* JADX INFO: Access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public final class Month implements Comparable<Month>, Parcelable {
    public static final Parcelable.Creator<Month> CREATOR = new a();
    public final Calendar a;
    public final int f0;
    public final int g0;
    public final int h0;
    public final int i0;
    public final long j0;
    public String k0;

    /* loaded from: classes2.dex */
    public static class a implements Parcelable.Creator<Month> {
        @Override // android.os.Parcelable.Creator
        /* renamed from: a */
        public Month createFromParcel(Parcel parcel) {
            return Month.d(parcel.readInt(), parcel.readInt());
        }

        @Override // android.os.Parcelable.Creator
        /* renamed from: b */
        public Month[] newArray(int i) {
            return new Month[i];
        }
    }

    public Month(Calendar calendar) {
        calendar.set(5, 1);
        Calendar f = hg4.f(calendar);
        this.a = f;
        this.f0 = f.get(2);
        this.g0 = f.get(1);
        this.h0 = f.getMaximum(7);
        this.i0 = f.getActualMaximum(5);
        this.j0 = f.getTimeInMillis();
    }

    public static Month d(int i, int i2) {
        Calendar q = hg4.q();
        q.set(1, i);
        q.set(2, i2);
        return new Month(q);
    }

    public static Month e(long j) {
        Calendar q = hg4.q();
        q.setTimeInMillis(j);
        return new Month(q);
    }

    public static Month f() {
        return new Month(hg4.o());
    }

    @Override // java.lang.Comparable
    /* renamed from: a */
    public int compareTo(Month month) {
        return this.a.compareTo(month.a);
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof Month) {
            Month month = (Month) obj;
            return this.f0 == month.f0 && this.g0 == month.g0;
        }
        return false;
    }

    public int g() {
        int firstDayOfWeek = this.a.get(7) - this.a.getFirstDayOfWeek();
        return firstDayOfWeek < 0 ? firstDayOfWeek + this.h0 : firstDayOfWeek;
    }

    public long h(int i) {
        Calendar f = hg4.f(this.a);
        f.set(5, i);
        return f.getTimeInMillis();
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(this.f0), Integer.valueOf(this.g0)});
    }

    public int j(long j) {
        Calendar f = hg4.f(this.a);
        f.setTimeInMillis(j);
        return f.get(5);
    }

    public String k(Context context) {
        if (this.k0 == null) {
            this.k0 = qe0.i(context, this.a.getTimeInMillis());
        }
        return this.k0;
    }

    public long l() {
        return this.a.getTimeInMillis();
    }

    public Month o(int i) {
        Calendar f = hg4.f(this.a);
        f.add(2, i);
        return new Month(f);
    }

    public int p(Month month) {
        if (this.a instanceof GregorianCalendar) {
            return ((month.g0 - this.g0) * 12) + (month.f0 - this.f0);
        }
        throw new IllegalArgumentException("Only Gregorian calendars are supported.");
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.g0);
        parcel.writeInt(this.f0);
    }
}
