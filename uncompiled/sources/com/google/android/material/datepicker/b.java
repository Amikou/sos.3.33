package com.google.android.material.datepicker;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.fragment.app.j;
import com.google.android.material.datepicker.CalendarConstraints;
import com.google.android.material.internal.CheckableImageButton;
import java.util.Iterator;
import java.util.LinkedHashSet;

/* compiled from: MaterialDatePicker.java */
/* loaded from: classes2.dex */
public final class b<S> extends sn0 {
    public static final Object L0 = "CONFIRM_BUTTON_TAG";
    public static final Object M0 = "CANCEL_BUTTON_TAG";
    public static final Object N0 = "TOGGLE_BUTTON_TAG";
    public uq2<S> A0;
    public CalendarConstraints B0;
    public MaterialCalendar<S> C0;
    public int D0;
    public CharSequence E0;
    public boolean F0;
    public int G0;
    public TextView H0;
    public CheckableImageButton I0;
    public o42 J0;
    public Button K0;
    public final LinkedHashSet<m42<? super S>> u0 = new LinkedHashSet<>();
    public final LinkedHashSet<View.OnClickListener> v0 = new LinkedHashSet<>();
    public final LinkedHashSet<DialogInterface.OnCancelListener> w0 = new LinkedHashSet<>();
    public final LinkedHashSet<DialogInterface.OnDismissListener> x0 = new LinkedHashSet<>();
    public int y0;
    public DateSelector<S> z0;

    /* compiled from: MaterialDatePicker.java */
    /* loaded from: classes2.dex */
    public class a implements View.OnClickListener {
        public a() {
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            Iterator it = b.this.u0.iterator();
            while (it.hasNext()) {
                ((m42) it.next()).a(b.this.H());
            }
            b.this.h();
        }
    }

    /* compiled from: MaterialDatePicker.java */
    /* renamed from: com.google.android.material.datepicker.b$b  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public class View$OnClickListenerC0120b implements View.OnClickListener {
        public View$OnClickListenerC0120b() {
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            Iterator it = b.this.v0.iterator();
            while (it.hasNext()) {
                ((View.OnClickListener) it.next()).onClick(view);
            }
            b.this.h();
        }
    }

    /* compiled from: MaterialDatePicker.java */
    /* loaded from: classes2.dex */
    public class c extends sm2<S> {
        public c() {
        }

        @Override // defpackage.sm2
        public void a() {
            b.this.K0.setEnabled(false);
        }

        @Override // defpackage.sm2
        public void b(S s) {
            b.this.O();
            b.this.K0.setEnabled(b.this.z0.Z0());
        }
    }

    /* compiled from: MaterialDatePicker.java */
    /* loaded from: classes2.dex */
    public class d implements View.OnClickListener {
        public d() {
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            b.this.K0.setEnabled(b.this.z0.Z0());
            b.this.I0.toggle();
            b bVar = b.this;
            bVar.P(bVar.I0);
            b.this.N();
        }
    }

    public static Drawable D(Context context) {
        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.addState(new int[]{16842912}, mf.d(context, qz2.material_ic_calendar_black_24dp));
        stateListDrawable.addState(new int[0], mf.d(context, qz2.material_ic_edit_black_24dp));
        return stateListDrawable;
    }

    public static int E(Context context) {
        Resources resources = context.getResources();
        int dimensionPixelSize = resources.getDimensionPixelSize(jz2.mtrl_calendar_navigation_height) + resources.getDimensionPixelOffset(jz2.mtrl_calendar_navigation_top_padding) + resources.getDimensionPixelOffset(jz2.mtrl_calendar_navigation_bottom_padding);
        int dimensionPixelSize2 = resources.getDimensionPixelSize(jz2.mtrl_calendar_days_of_week_height);
        int i = com.google.android.material.datepicker.c.j0;
        return dimensionPixelSize + dimensionPixelSize2 + (resources.getDimensionPixelSize(jz2.mtrl_calendar_day_height) * i) + ((i - 1) * resources.getDimensionPixelOffset(jz2.mtrl_calendar_month_vertical_padding)) + resources.getDimensionPixelOffset(jz2.mtrl_calendar_bottom_padding);
    }

    public static int G(Context context) {
        Resources resources = context.getResources();
        int dimensionPixelOffset = resources.getDimensionPixelOffset(jz2.mtrl_calendar_content_padding);
        int i = Month.f().h0;
        return (dimensionPixelOffset * 2) + (resources.getDimensionPixelSize(jz2.mtrl_calendar_day_width) * i) + ((i - 1) * resources.getDimensionPixelOffset(jz2.mtrl_calendar_month_horizontal_padding));
    }

    public static boolean K(Context context) {
        return M(context, 16843277);
    }

    public static boolean L(Context context) {
        return M(context, gy2.nestedScrollable);
    }

    public static boolean M(Context context, int i) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(i42.c(context, gy2.materialCalendarStyle, MaterialCalendar.class.getCanonicalName()), new int[]{i});
        boolean z = obtainStyledAttributes.getBoolean(0, false);
        obtainStyledAttributes.recycle();
        return z;
    }

    public String F() {
        return this.z0.N(getContext());
    }

    public final S H() {
        return this.z0.o1();
    }

    public final int I(Context context) {
        int i = this.y0;
        return i != 0 ? i : this.z0.U0(context);
    }

    public final void J(Context context) {
        this.I0.setTag(N0);
        this.I0.setImageDrawable(D(context));
        this.I0.setChecked(this.G0 != 0);
        ei4.t0(this.I0, null);
        P(this.I0);
        this.I0.setOnClickListener(new d());
    }

    public final void N() {
        int I = I(requireContext());
        this.C0 = MaterialCalendar.v(this.z0, I, this.B0);
        this.A0 = this.I0.isChecked() ? q42.g(this.z0, I, this.B0) : this.C0;
        O();
        j n = getChildFragmentManager().n();
        n.s(b03.mtrl_calendar_frame, this.A0);
        n.l();
        this.A0.e(new c());
    }

    public final void O() {
        String F = F();
        this.H0.setContentDescription(String.format(getString(r13.mtrl_picker_announce_current_selection), F));
        this.H0.setText(F);
    }

    public final void P(CheckableImageButton checkableImageButton) {
        String string;
        if (this.I0.isChecked()) {
            string = checkableImageButton.getContext().getString(r13.mtrl_picker_toggle_to_calendar_input_mode);
        } else {
            string = checkableImageButton.getContext().getString(r13.mtrl_picker_toggle_to_text_input_mode);
        }
        this.I0.setContentDescription(string);
    }

    @Override // defpackage.sn0
    public final Dialog m(Bundle bundle) {
        Dialog dialog = new Dialog(requireContext(), I(requireContext()));
        Context context = dialog.getContext();
        this.F0 = K(context);
        int c2 = i42.c(context, gy2.colorSurface, b.class.getCanonicalName());
        o42 o42Var = new o42(context, null, gy2.materialCalendarStyle, y13.Widget_MaterialComponents_MaterialCalendar);
        this.J0 = o42Var;
        o42Var.P(context);
        this.J0.a0(ColorStateList.valueOf(c2));
        this.J0.Z(ei4.y(dialog.getWindow().getDecorView()));
        return dialog;
    }

    @Override // defpackage.sn0, android.content.DialogInterface.OnCancelListener
    public final void onCancel(DialogInterface dialogInterface) {
        Iterator<DialogInterface.OnCancelListener> it = this.w0.iterator();
        while (it.hasNext()) {
            it.next().onCancel(dialogInterface);
        }
        super.onCancel(dialogInterface);
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (bundle == null) {
            bundle = getArguments();
        }
        this.y0 = bundle.getInt("OVERRIDE_THEME_RES_ID");
        this.z0 = (DateSelector) bundle.getParcelable("DATE_SELECTOR_KEY");
        this.B0 = (CalendarConstraints) bundle.getParcelable("CALENDAR_CONSTRAINTS_KEY");
        this.D0 = bundle.getInt("TITLE_TEXT_RES_ID_KEY");
        this.E0 = bundle.getCharSequence("TITLE_TEXT_KEY");
        this.G0 = bundle.getInt("INPUT_MODE_KEY");
    }

    @Override // androidx.fragment.app.Fragment
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(this.F0 ? x03.mtrl_picker_fullscreen : x03.mtrl_picker_dialog, viewGroup);
        Context context = inflate.getContext();
        if (this.F0) {
            inflate.findViewById(b03.mtrl_calendar_frame).setLayoutParams(new LinearLayout.LayoutParams(G(context), -2));
        } else {
            View findViewById = inflate.findViewById(b03.mtrl_calendar_main_pane);
            View findViewById2 = inflate.findViewById(b03.mtrl_calendar_frame);
            findViewById.setLayoutParams(new LinearLayout.LayoutParams(G(context), -1));
            findViewById2.setMinimumHeight(E(requireContext()));
        }
        TextView textView = (TextView) inflate.findViewById(b03.mtrl_picker_header_selection_text);
        this.H0 = textView;
        ei4.v0(textView, 1);
        this.I0 = (CheckableImageButton) inflate.findViewById(b03.mtrl_picker_header_toggle);
        TextView textView2 = (TextView) inflate.findViewById(b03.mtrl_picker_title_text);
        CharSequence charSequence = this.E0;
        if (charSequence != null) {
            textView2.setText(charSequence);
        } else {
            textView2.setText(this.D0);
        }
        J(context);
        this.K0 = (Button) inflate.findViewById(b03.confirm_button);
        if (this.z0.Z0()) {
            this.K0.setEnabled(true);
        } else {
            this.K0.setEnabled(false);
        }
        this.K0.setTag(L0);
        this.K0.setOnClickListener(new a());
        Button button = (Button) inflate.findViewById(b03.cancel_button);
        button.setTag(M0);
        button.setOnClickListener(new View$OnClickListenerC0120b());
        return inflate;
    }

    @Override // defpackage.sn0, android.content.DialogInterface.OnDismissListener
    public final void onDismiss(DialogInterface dialogInterface) {
        Iterator<DialogInterface.OnDismissListener> it = this.x0.iterator();
        while (it.hasNext()) {
            it.next().onDismiss(dialogInterface);
        }
        ViewGroup viewGroup = (ViewGroup) getView();
        if (viewGroup != null) {
            viewGroup.removeAllViews();
        }
        super.onDismiss(dialogInterface);
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public final void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt("OVERRIDE_THEME_RES_ID", this.y0);
        bundle.putParcelable("DATE_SELECTOR_KEY", this.z0);
        CalendarConstraints.b bVar = new CalendarConstraints.b(this.B0);
        if (this.C0.r() != null) {
            bVar.b(this.C0.r().j0);
        }
        bundle.putParcelable("CALENDAR_CONSTRAINTS_KEY", bVar.a());
        bundle.putInt("TITLE_TEXT_RES_ID_KEY", this.D0);
        bundle.putCharSequence("TITLE_TEXT_KEY", this.E0);
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onStart() {
        super.onStart();
        Window window = q().getWindow();
        if (this.F0) {
            window.setLayout(-1, -1);
            window.setBackgroundDrawable(this.J0);
        } else {
            window.setLayout(-2, -2);
            int dimensionPixelOffset = getResources().getDimensionPixelOffset(jz2.mtrl_calendar_dialog_background_inset);
            Rect rect = new Rect(dimensionPixelOffset, dimensionPixelOffset, dimensionPixelOffset, dimensionPixelOffset);
            window.setBackgroundDrawable(new InsetDrawable((Drawable) this.J0, dimensionPixelOffset, dimensionPixelOffset, dimensionPixelOffset, dimensionPixelOffset));
            window.getDecorView().setOnTouchListener(new br1(q(), rect));
        }
        N();
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onStop() {
        this.A0.f();
        super.onStop();
    }
}
