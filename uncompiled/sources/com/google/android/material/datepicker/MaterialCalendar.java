package com.google.android.material.datepicker;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Build;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ListAdapter;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.r;
import com.google.android.material.button.MaterialButton;
import java.util.Calendar;
import java.util.Iterator;

/* loaded from: classes2.dex */
public final class MaterialCalendar<S> extends uq2<S> {
    public static final Object p0 = "MONTHS_VIEW_GROUP_TAG";
    public static final Object q0 = "NAVIGATION_PREV_TAG";
    public static final Object r0 = "NAVIGATION_NEXT_TAG";
    public static final Object s0 = "SELECTOR_TOGGLE_TAG";
    public int f0;
    public DateSelector<S> g0;
    public CalendarConstraints h0;
    public Month i0;
    public CalendarSelector j0;
    public tu k0;
    public RecyclerView l0;
    public RecyclerView m0;
    public View n0;
    public View o0;

    /* loaded from: classes2.dex */
    public enum CalendarSelector {
        DAY,
        YEAR
    }

    /* loaded from: classes2.dex */
    public class a implements Runnable {
        public final /* synthetic */ int a;

        public a(int i) {
            this.a = i;
        }

        @Override // java.lang.Runnable
        public void run() {
            MaterialCalendar.this.m0.w1(this.a);
        }
    }

    /* loaded from: classes2.dex */
    public class b extends z5 {
        public b(MaterialCalendar materialCalendar) {
        }

        @Override // defpackage.z5
        public void g(View view, b6 b6Var) {
            super.g(view, b6Var);
            b6Var.e0(null);
        }
    }

    /* loaded from: classes2.dex */
    public class c extends vq3 {
        public final /* synthetic */ int M0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public c(Context context, int i, boolean z, int i2) {
            super(context, i, z);
            this.M0 = i2;
        }

        @Override // androidx.recyclerview.widget.LinearLayoutManager
        public void V1(RecyclerView.x xVar, int[] iArr) {
            if (this.M0 == 0) {
                iArr[0] = MaterialCalendar.this.m0.getWidth();
                iArr[1] = MaterialCalendar.this.m0.getWidth();
                return;
            }
            iArr[0] = MaterialCalendar.this.m0.getHeight();
            iArr[1] = MaterialCalendar.this.m0.getHeight();
        }
    }

    /* loaded from: classes2.dex */
    public class d implements k {
        public d() {
        }

        @Override // com.google.android.material.datepicker.MaterialCalendar.k
        public void a(long j) {
            if (MaterialCalendar.this.h0.f().W0(j)) {
                MaterialCalendar.this.g0.z1(j);
                Iterator<sm2<S>> it = MaterialCalendar.this.a.iterator();
                while (it.hasNext()) {
                    it.next().b((S) MaterialCalendar.this.g0.o1());
                }
                MaterialCalendar.this.m0.getAdapter().notifyDataSetChanged();
                if (MaterialCalendar.this.l0 != null) {
                    MaterialCalendar.this.l0.getAdapter().notifyDataSetChanged();
                }
            }
        }
    }

    /* loaded from: classes2.dex */
    public class e extends RecyclerView.n {
        public final Calendar a = hg4.q();
        public final Calendar b = hg4.q();

        public e() {
        }

        @Override // androidx.recyclerview.widget.RecyclerView.n
        public void onDraw(Canvas canvas, RecyclerView recyclerView, RecyclerView.x xVar) {
            int width;
            if ((recyclerView.getAdapter() instanceof com.google.android.material.datepicker.e) && (recyclerView.getLayoutManager() instanceof GridLayoutManager)) {
                com.google.android.material.datepicker.e eVar = (com.google.android.material.datepicker.e) recyclerView.getAdapter();
                GridLayoutManager gridLayoutManager = (GridLayoutManager) recyclerView.getLayoutManager();
                for (jp2<Long, Long> jp2Var : MaterialCalendar.this.g0.R()) {
                    Long l = jp2Var.a;
                    if (l != null && jp2Var.b != null) {
                        this.a.setTimeInMillis(l.longValue());
                        this.b.setTimeInMillis(jp2Var.b.longValue());
                        int c = eVar.c(this.a.get(1));
                        int c2 = eVar.c(this.b.get(1));
                        View N = gridLayoutManager.N(c);
                        View N2 = gridLayoutManager.N(c2);
                        int e3 = c / gridLayoutManager.e3();
                        int e32 = c2 / gridLayoutManager.e3();
                        int i = e3;
                        while (i <= e32) {
                            View N3 = gridLayoutManager.N(gridLayoutManager.e3() * i);
                            if (N3 != null) {
                                int top = N3.getTop() + MaterialCalendar.this.k0.d.c();
                                int bottom = N3.getBottom() - MaterialCalendar.this.k0.d.b();
                                int left = i == e3 ? N.getLeft() + (N.getWidth() / 2) : 0;
                                if (i == e32) {
                                    width = N2.getLeft() + (N2.getWidth() / 2);
                                } else {
                                    width = recyclerView.getWidth();
                                }
                                canvas.drawRect(left, top, width, bottom, MaterialCalendar.this.k0.h);
                            }
                            i++;
                        }
                    }
                }
            }
        }
    }

    /* loaded from: classes2.dex */
    public class f extends z5 {
        public f() {
        }

        @Override // defpackage.z5
        public void g(View view, b6 b6Var) {
            String string;
            super.g(view, b6Var);
            if (MaterialCalendar.this.o0.getVisibility() == 0) {
                string = MaterialCalendar.this.getString(r13.mtrl_picker_toggle_to_year_selection);
            } else {
                string = MaterialCalendar.this.getString(r13.mtrl_picker_toggle_to_day_selection);
            }
            b6Var.n0(string);
        }
    }

    /* loaded from: classes2.dex */
    public class g extends RecyclerView.r {
        public final /* synthetic */ com.google.android.material.datepicker.d a;
        public final /* synthetic */ MaterialButton b;

        public g(com.google.android.material.datepicker.d dVar, MaterialButton materialButton) {
            this.a = dVar;
            this.b = materialButton;
        }

        @Override // androidx.recyclerview.widget.RecyclerView.r
        public void onScrollStateChanged(RecyclerView recyclerView, int i) {
            if (i == 0) {
                CharSequence text = this.b.getText();
                if (Build.VERSION.SDK_INT >= 16) {
                    recyclerView.announceForAccessibility(text);
                } else {
                    recyclerView.sendAccessibilityEvent(2048);
                }
            }
        }

        @Override // androidx.recyclerview.widget.RecyclerView.r
        public void onScrolled(RecyclerView recyclerView, int i, int i2) {
            int k2;
            if (i < 0) {
                k2 = MaterialCalendar.this.u().h2();
            } else {
                k2 = MaterialCalendar.this.u().k2();
            }
            MaterialCalendar.this.i0 = this.a.b(k2);
            this.b.setText(this.a.c(k2));
        }
    }

    /* loaded from: classes2.dex */
    public class h implements View.OnClickListener {
        public h() {
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            MaterialCalendar.this.z();
        }
    }

    /* loaded from: classes2.dex */
    public class i implements View.OnClickListener {
        public final /* synthetic */ com.google.android.material.datepicker.d a;

        public i(com.google.android.material.datepicker.d dVar) {
            this.a = dVar;
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            int h2 = MaterialCalendar.this.u().h2() + 1;
            if (h2 < MaterialCalendar.this.m0.getAdapter().getItemCount()) {
                MaterialCalendar.this.x(this.a.b(h2));
            }
        }
    }

    /* loaded from: classes2.dex */
    public class j implements View.OnClickListener {
        public final /* synthetic */ com.google.android.material.datepicker.d a;

        public j(com.google.android.material.datepicker.d dVar) {
            this.a = dVar;
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            int k2 = MaterialCalendar.this.u().k2() - 1;
            if (k2 >= 0) {
                MaterialCalendar.this.x(this.a.b(k2));
            }
        }
    }

    /* loaded from: classes2.dex */
    public interface k {
        void a(long j);
    }

    public static int t(Context context) {
        return context.getResources().getDimensionPixelSize(jz2.mtrl_calendar_day_height);
    }

    public static <T> MaterialCalendar<T> v(DateSelector<T> dateSelector, int i2, CalendarConstraints calendarConstraints) {
        MaterialCalendar<T> materialCalendar = new MaterialCalendar<>();
        Bundle bundle = new Bundle();
        bundle.putInt("THEME_RES_ID_KEY", i2);
        bundle.putParcelable("GRID_SELECTOR_KEY", dateSelector);
        bundle.putParcelable("CALENDAR_CONSTRAINTS_KEY", calendarConstraints);
        bundle.putParcelable("CURRENT_MONTH_KEY", calendarConstraints.i());
        materialCalendar.setArguments(bundle);
        return materialCalendar;
    }

    @Override // defpackage.uq2
    public boolean e(sm2<S> sm2Var) {
        return super.e(sm2Var);
    }

    public final void n(View view, com.google.android.material.datepicker.d dVar) {
        MaterialButton materialButton = (MaterialButton) view.findViewById(b03.month_navigation_fragment_toggle);
        materialButton.setTag(s0);
        ei4.t0(materialButton, new f());
        MaterialButton materialButton2 = (MaterialButton) view.findViewById(b03.month_navigation_previous);
        materialButton2.setTag(q0);
        MaterialButton materialButton3 = (MaterialButton) view.findViewById(b03.month_navigation_next);
        materialButton3.setTag(r0);
        this.n0 = view.findViewById(b03.mtrl_calendar_year_selector_frame);
        this.o0 = view.findViewById(b03.mtrl_calendar_day_selector_frame);
        y(CalendarSelector.DAY);
        materialButton.setText(this.i0.k(view.getContext()));
        this.m0.l(new g(dVar, materialButton));
        materialButton.setOnClickListener(new h());
        materialButton3.setOnClickListener(new i(dVar));
        materialButton2.setOnClickListener(new j(dVar));
    }

    public final RecyclerView.n o() {
        return new e();
    }

    @Override // androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (bundle == null) {
            bundle = getArguments();
        }
        this.f0 = bundle.getInt("THEME_RES_ID_KEY");
        this.g0 = (DateSelector) bundle.getParcelable("GRID_SELECTOR_KEY");
        this.h0 = (CalendarConstraints) bundle.getParcelable("CALENDAR_CONSTRAINTS_KEY");
        this.i0 = (Month) bundle.getParcelable("CURRENT_MONTH_KEY");
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        int i2;
        int i3;
        ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(getContext(), this.f0);
        this.k0 = new tu(contextThemeWrapper);
        LayoutInflater cloneInContext = layoutInflater.cloneInContext(contextThemeWrapper);
        Month j2 = this.h0.j();
        if (com.google.android.material.datepicker.b.K(contextThemeWrapper)) {
            i2 = x03.mtrl_calendar_vertical;
            i3 = 1;
        } else {
            i2 = x03.mtrl_calendar_horizontal;
            i3 = 0;
        }
        View inflate = cloneInContext.inflate(i2, viewGroup, false);
        GridView gridView = (GridView) inflate.findViewById(b03.mtrl_calendar_days_of_week);
        ei4.t0(gridView, new b(this));
        gridView.setAdapter((ListAdapter) new se0());
        gridView.setNumColumns(j2.h0);
        gridView.setEnabled(false);
        this.m0 = (RecyclerView) inflate.findViewById(b03.mtrl_calendar_months);
        this.m0.setLayoutManager(new c(getContext(), i3, false, i3));
        this.m0.setTag(p0);
        com.google.android.material.datepicker.d dVar = new com.google.android.material.datepicker.d(contextThemeWrapper, this.g0, this.h0, new d());
        this.m0.setAdapter(dVar);
        int integer = contextThemeWrapper.getResources().getInteger(r03.mtrl_calendar_year_selector_span);
        RecyclerView recyclerView = (RecyclerView) inflate.findViewById(b03.mtrl_calendar_year_selector_frame);
        this.l0 = recyclerView;
        if (recyclerView != null) {
            recyclerView.setHasFixedSize(true);
            this.l0.setLayoutManager(new GridLayoutManager((Context) contextThemeWrapper, integer, 1, false));
            this.l0.setAdapter(new com.google.android.material.datepicker.e(this));
            this.l0.h(o());
        }
        if (inflate.findViewById(b03.month_navigation_fragment_toggle) != null) {
            n(inflate, dVar);
        }
        if (!com.google.android.material.datepicker.b.K(contextThemeWrapper)) {
            new r().b(this.m0);
        }
        this.m0.o1(dVar.d(this.i0));
        return inflate;
    }

    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt("THEME_RES_ID_KEY", this.f0);
        bundle.putParcelable("GRID_SELECTOR_KEY", this.g0);
        bundle.putParcelable("CALENDAR_CONSTRAINTS_KEY", this.h0);
        bundle.putParcelable("CURRENT_MONTH_KEY", this.i0);
    }

    public CalendarConstraints p() {
        return this.h0;
    }

    public tu q() {
        return this.k0;
    }

    public Month r() {
        return this.i0;
    }

    public DateSelector<S> s() {
        return this.g0;
    }

    public LinearLayoutManager u() {
        return (LinearLayoutManager) this.m0.getLayoutManager();
    }

    public final void w(int i2) {
        this.m0.post(new a(i2));
    }

    public void x(Month month) {
        com.google.android.material.datepicker.d dVar = (com.google.android.material.datepicker.d) this.m0.getAdapter();
        int d2 = dVar.d(month);
        int d3 = d2 - dVar.d(this.i0);
        boolean z = Math.abs(d3) > 3;
        boolean z2 = d3 > 0;
        this.i0 = month;
        if (z && z2) {
            this.m0.o1(d2 - 3);
            w(d2);
        } else if (z) {
            this.m0.o1(d2 + 3);
            w(d2);
        } else {
            w(d2);
        }
    }

    public void y(CalendarSelector calendarSelector) {
        this.j0 = calendarSelector;
        if (calendarSelector == CalendarSelector.YEAR) {
            this.l0.getLayoutManager().G1(((com.google.android.material.datepicker.e) this.l0.getAdapter()).c(this.i0.g0));
            this.n0.setVisibility(0);
            this.o0.setVisibility(8);
        } else if (calendarSelector == CalendarSelector.DAY) {
            this.n0.setVisibility(8);
            this.o0.setVisibility(0);
            x(this.i0);
        }
    }

    public void z() {
        CalendarSelector calendarSelector = this.j0;
        CalendarSelector calendarSelector2 = CalendarSelector.YEAR;
        if (calendarSelector == calendarSelector2) {
            y(CalendarSelector.DAY);
        } else if (calendarSelector == CalendarSelector.DAY) {
            y(calendarSelector2);
        }
    }
}
