package com.google.android.material.datepicker;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.datepicker.MaterialCalendar;

/* compiled from: MonthsPagerAdapter.java */
/* loaded from: classes2.dex */
public class d extends RecyclerView.Adapter<b> {
    public final Context a;
    public final CalendarConstraints b;
    public final DateSelector<?> c;
    public final MaterialCalendar.k d;
    public final int e;

    /* compiled from: MonthsPagerAdapter.java */
    /* loaded from: classes2.dex */
    public class a implements AdapterView.OnItemClickListener {
        public final /* synthetic */ MaterialCalendarGridView a;

        public a(MaterialCalendarGridView materialCalendarGridView) {
            this.a = materialCalendarGridView;
        }

        @Override // android.widget.AdapterView.OnItemClickListener
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            if (this.a.getAdapter2().n(i)) {
                d.this.d.a(this.a.getAdapter2().getItem(i).longValue());
            }
        }
    }

    /* compiled from: MonthsPagerAdapter.java */
    /* loaded from: classes2.dex */
    public static class b extends RecyclerView.a0 {
        public final TextView a;
        public final MaterialCalendarGridView b;

        public b(LinearLayout linearLayout, boolean z) {
            super(linearLayout);
            TextView textView = (TextView) linearLayout.findViewById(b03.month_title);
            this.a = textView;
            ei4.u0(textView, true);
            this.b = (MaterialCalendarGridView) linearLayout.findViewById(b03.month_grid);
            if (z) {
                return;
            }
            textView.setVisibility(8);
        }
    }

    public d(Context context, DateSelector<?> dateSelector, CalendarConstraints calendarConstraints, MaterialCalendar.k kVar) {
        Month j = calendarConstraints.j();
        Month g = calendarConstraints.g();
        Month i = calendarConstraints.i();
        if (j.compareTo(i) <= 0) {
            if (i.compareTo(g) <= 0) {
                int t = c.j0 * MaterialCalendar.t(context);
                int t2 = com.google.android.material.datepicker.b.K(context) ? MaterialCalendar.t(context) : 0;
                this.a = context;
                this.e = t + t2;
                this.b = calendarConstraints;
                this.c = dateSelector;
                this.d = kVar;
                setHasStableIds(true);
                return;
            }
            throw new IllegalArgumentException("currentPage cannot be after lastPage");
        }
        throw new IllegalArgumentException("firstPage cannot be after currentPage");
    }

    public Month b(int i) {
        return this.b.j().o(i);
    }

    public CharSequence c(int i) {
        return b(i).k(this.a);
    }

    public int d(Month month) {
        return this.b.j().p(month);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: e */
    public void onBindViewHolder(b bVar, int i) {
        Month o = this.b.j().o(i);
        bVar.a.setText(o.k(bVar.itemView.getContext()));
        MaterialCalendarGridView materialCalendarGridView = (MaterialCalendarGridView) bVar.b.findViewById(b03.month_grid);
        if (materialCalendarGridView.getAdapter2() != null && o.equals(materialCalendarGridView.getAdapter2().a)) {
            materialCalendarGridView.invalidate();
            materialCalendarGridView.getAdapter2().m(materialCalendarGridView);
        } else {
            c cVar = new c(o, this.c, this.b);
            materialCalendarGridView.setNumColumns(o.h0);
            materialCalendarGridView.setAdapter((ListAdapter) cVar);
        }
        materialCalendarGridView.setOnItemClickListener(new a(materialCalendarGridView));
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: f */
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(viewGroup.getContext()).inflate(x03.mtrl_calendar_month_labeled, viewGroup, false);
        if (com.google.android.material.datepicker.b.K(viewGroup.getContext())) {
            linearLayout.setLayoutParams(new RecyclerView.LayoutParams(-1, this.e));
            return new b(linearLayout, true);
        }
        return new b(linearLayout, false);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.b.h();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public long getItemId(int i) {
        return this.b.j().o(i).l();
    }
}
