package com.google.android.material.datepicker;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import com.google.android.material.textfield.TextInputLayout;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

/* compiled from: DateFormatTextWatcher.java */
/* loaded from: classes2.dex */
public abstract class a extends w44 {
    public final TextInputLayout a;
    public final DateFormat f0;
    public final CalendarConstraints g0;
    public final String h0;
    public final Runnable i0;
    public Runnable j0;

    /* compiled from: DateFormatTextWatcher.java */
    /* renamed from: com.google.android.material.datepicker.a$a  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public class RunnableC0119a implements Runnable {
        public final /* synthetic */ String a;

        public RunnableC0119a(String str) {
            this.a = str;
        }

        @Override // java.lang.Runnable
        public void run() {
            TextInputLayout textInputLayout = a.this.a;
            DateFormat dateFormat = a.this.f0;
            Context context = textInputLayout.getContext();
            String string = context.getString(r13.mtrl_picker_invalid_format);
            String format = String.format(context.getString(r13.mtrl_picker_invalid_format_use), this.a);
            String format2 = String.format(context.getString(r13.mtrl_picker_invalid_format_example), dateFormat.format(new Date(hg4.o().getTimeInMillis())));
            textInputLayout.setError(string + "\n" + format + "\n" + format2);
            a.this.e();
        }
    }

    /* compiled from: DateFormatTextWatcher.java */
    /* loaded from: classes2.dex */
    public class b implements Runnable {
        public final /* synthetic */ long a;

        public b(long j) {
            this.a = j;
        }

        @Override // java.lang.Runnable
        public void run() {
            a.this.a.setError(String.format(a.this.h0, qe0.c(this.a)));
            a.this.e();
        }
    }

    public a(String str, DateFormat dateFormat, TextInputLayout textInputLayout, CalendarConstraints calendarConstraints) {
        this.f0 = dateFormat;
        this.a = textInputLayout;
        this.g0 = calendarConstraints;
        this.h0 = textInputLayout.getContext().getString(r13.mtrl_picker_out_of_range);
        this.i0 = new RunnableC0119a(str);
    }

    public final Runnable d(long j) {
        return new b(j);
    }

    public abstract void e();

    public abstract void f(Long l);

    public void g(View view, Runnable runnable) {
        view.postDelayed(runnable, 1000L);
    }

    @Override // defpackage.w44, android.text.TextWatcher
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        this.a.removeCallbacks(this.i0);
        this.a.removeCallbacks(this.j0);
        this.a.setError(null);
        f(null);
        if (TextUtils.isEmpty(charSequence)) {
            return;
        }
        try {
            Date parse = this.f0.parse(charSequence.toString());
            this.a.setError(null);
            long time = parse.getTime();
            if (this.g0.f().W0(time) && this.g0.l(time)) {
                f(Long.valueOf(parse.getTime()));
                return;
            }
            Runnable d = d(time);
            this.j0 = d;
            g(this.a, d);
        } catch (ParseException unused) {
            g(this.a, this.i0);
        }
    }
}
