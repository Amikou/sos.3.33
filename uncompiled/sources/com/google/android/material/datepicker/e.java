package com.google.android.material.datepicker;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.datepicker.MaterialCalendar;
import java.util.Calendar;
import java.util.Locale;

/* compiled from: YearGridAdapter.java */
/* loaded from: classes2.dex */
public class e extends RecyclerView.Adapter<b> {
    public final MaterialCalendar<?> a;

    /* compiled from: YearGridAdapter.java */
    /* loaded from: classes2.dex */
    public class a implements View.OnClickListener {
        public final /* synthetic */ int a;

        public a(int i) {
            this.a = i;
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            e.this.a.x(e.this.a.p().e(Month.d(this.a, e.this.a.r().f0)));
            e.this.a.y(MaterialCalendar.CalendarSelector.DAY);
        }
    }

    /* compiled from: YearGridAdapter.java */
    /* loaded from: classes2.dex */
    public static class b extends RecyclerView.a0 {
        public final TextView a;

        public b(TextView textView) {
            super(textView);
            this.a = textView;
        }
    }

    public e(MaterialCalendar<?> materialCalendar) {
        this.a = materialCalendar;
    }

    public final View.OnClickListener b(int i) {
        return new a(i);
    }

    public int c(int i) {
        return i - this.a.p().j().g0;
    }

    public int d(int i) {
        return this.a.p().j().g0 + i;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: e */
    public void onBindViewHolder(b bVar, int i) {
        int d = d(i);
        String string = bVar.a.getContext().getString(r13.mtrl_picker_navigate_to_year_description);
        bVar.a.setText(String.format(Locale.getDefault(), "%d", Integer.valueOf(d)));
        bVar.a.setContentDescription(String.format(string, Integer.valueOf(d)));
        tu q = this.a.q();
        Calendar o = hg4.o();
        su suVar = o.get(1) == d ? q.f : q.d;
        for (Long l : this.a.s().k1()) {
            o.setTimeInMillis(l.longValue());
            if (o.get(1) == d) {
                suVar = q.e;
            }
        }
        suVar.d(bVar.a);
        bVar.a.setOnClickListener(b(d));
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: f */
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new b((TextView) LayoutInflater.from(viewGroup.getContext()).inflate(x03.mtrl_calendar_year, viewGroup, false));
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.a.p().k();
    }
}
