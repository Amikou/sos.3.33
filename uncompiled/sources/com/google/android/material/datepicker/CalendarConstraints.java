package com.google.android.material.datepicker;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;

/* loaded from: classes2.dex */
public final class CalendarConstraints implements Parcelable {
    public static final Parcelable.Creator<CalendarConstraints> CREATOR = new a();
    public final Month a;
    public final Month f0;
    public final DateValidator g0;
    public Month h0;
    public final int i0;
    public final int j0;

    /* loaded from: classes2.dex */
    public interface DateValidator extends Parcelable {
        boolean W0(long j);
    }

    /* loaded from: classes2.dex */
    public static class a implements Parcelable.Creator<CalendarConstraints> {
        @Override // android.os.Parcelable.Creator
        /* renamed from: a */
        public CalendarConstraints createFromParcel(Parcel parcel) {
            return new CalendarConstraints((Month) parcel.readParcelable(Month.class.getClassLoader()), (Month) parcel.readParcelable(Month.class.getClassLoader()), (DateValidator) parcel.readParcelable(DateValidator.class.getClassLoader()), (Month) parcel.readParcelable(Month.class.getClassLoader()), null);
        }

        @Override // android.os.Parcelable.Creator
        /* renamed from: b */
        public CalendarConstraints[] newArray(int i) {
            return new CalendarConstraints[i];
        }
    }

    /* loaded from: classes2.dex */
    public static final class b {
        public static final long e = hg4.a(Month.d(1900, 0).j0);
        public static final long f = hg4.a(Month.d(2100, 11).j0);
        public long a;
        public long b;
        public Long c;
        public DateValidator d;

        public b(CalendarConstraints calendarConstraints) {
            this.a = e;
            this.b = f;
            this.d = DateValidatorPointForward.a(Long.MIN_VALUE);
            this.a = calendarConstraints.a.j0;
            this.b = calendarConstraints.f0.j0;
            this.c = Long.valueOf(calendarConstraints.h0.j0);
            this.d = calendarConstraints.g0;
        }

        public CalendarConstraints a() {
            Bundle bundle = new Bundle();
            bundle.putParcelable("DEEP_COPY_VALIDATOR_KEY", this.d);
            Month e2 = Month.e(this.a);
            Month e3 = Month.e(this.b);
            DateValidator dateValidator = (DateValidator) bundle.getParcelable("DEEP_COPY_VALIDATOR_KEY");
            Long l = this.c;
            return new CalendarConstraints(e2, e3, dateValidator, l == null ? null : Month.e(l.longValue()), null);
        }

        public b b(long j) {
            this.c = Long.valueOf(j);
            return this;
        }
    }

    public /* synthetic */ CalendarConstraints(Month month, Month month2, DateValidator dateValidator, Month month3, a aVar) {
        this(month, month2, dateValidator, month3);
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public Month e(Month month) {
        if (month.compareTo(this.a) < 0) {
            return this.a;
        }
        return month.compareTo(this.f0) > 0 ? this.f0 : month;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof CalendarConstraints) {
            CalendarConstraints calendarConstraints = (CalendarConstraints) obj;
            return this.a.equals(calendarConstraints.a) && this.f0.equals(calendarConstraints.f0) && sl2.a(this.h0, calendarConstraints.h0) && this.g0.equals(calendarConstraints.g0);
        }
        return false;
    }

    public DateValidator f() {
        return this.g0;
    }

    public Month g() {
        return this.f0;
    }

    public int h() {
        return this.j0;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.a, this.f0, this.h0, this.g0});
    }

    public Month i() {
        return this.h0;
    }

    public Month j() {
        return this.a;
    }

    public int k() {
        return this.i0;
    }

    public boolean l(long j) {
        if (this.a.h(1) <= j) {
            Month month = this.f0;
            if (j <= month.h(month.i0)) {
                return true;
            }
        }
        return false;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.a, 0);
        parcel.writeParcelable(this.f0, 0);
        parcel.writeParcelable(this.h0, 0);
        parcel.writeParcelable(this.g0, 0);
    }

    public CalendarConstraints(Month month, Month month2, DateValidator dateValidator, Month month3) {
        this.a = month;
        this.f0 = month2;
        this.h0 = month3;
        this.g0 = dateValidator;
        if (month3 != null && month.compareTo(month3) > 0) {
            throw new IllegalArgumentException("start Month cannot be after current Month");
        }
        if (month3 != null && month3.compareTo(month2) > 0) {
            throw new IllegalArgumentException("current Month cannot be after end Month");
        }
        this.j0 = month.p(month2) + 1;
        this.i0 = (month2.g0 - month.g0) + 1;
    }
}
