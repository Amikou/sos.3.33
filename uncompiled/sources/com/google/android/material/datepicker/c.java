package com.google.android.material.datepicker;

import android.content.Context;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.Collection;
import java.util.Iterator;

/* compiled from: MonthAdapter.java */
/* loaded from: classes2.dex */
public class c extends BaseAdapter {
    public static final int j0 = hg4.q().getMaximum(4);
    public final Month a;
    public final DateSelector<?> f0;
    public Collection<Long> g0;
    public tu h0;
    public final CalendarConstraints i0;

    public c(Month month, DateSelector<?> dateSelector, CalendarConstraints calendarConstraints) {
        this.a = month;
        this.f0 = dateSelector;
        this.i0 = calendarConstraints;
        this.g0 = dateSelector.k1();
    }

    public int a(int i) {
        return b() + (i - 1);
    }

    public int b() {
        return this.a.g();
    }

    @Override // android.widget.Adapter
    /* renamed from: c */
    public Long getItem(int i) {
        if (i < this.a.g() || i > i()) {
            return null;
        }
        return Long.valueOf(this.a.h(j(i)));
    }

    /* JADX WARN: Removed duplicated region for block: B:18:0x0083 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:19:0x0084  */
    @Override // android.widget.Adapter
    /* renamed from: d */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public android.widget.TextView getView(int r6, android.view.View r7, android.view.ViewGroup r8) {
        /*
            r5 = this;
            android.content.Context r0 = r8.getContext()
            r5.e(r0)
            r0 = r7
            android.widget.TextView r0 = (android.widget.TextView) r0
            r1 = 0
            if (r7 != 0) goto L1e
            android.content.Context r7 = r8.getContext()
            android.view.LayoutInflater r7 = android.view.LayoutInflater.from(r7)
            int r0 = defpackage.x03.mtrl_calendar_day
            android.view.View r7 = r7.inflate(r0, r8, r1)
            r0 = r7
            android.widget.TextView r0 = (android.widget.TextView) r0
        L1e:
            int r7 = r5.b()
            int r7 = r6 - r7
            if (r7 < 0) goto L75
            com.google.android.material.datepicker.Month r8 = r5.a
            int r2 = r8.i0
            if (r7 < r2) goto L2d
            goto L75
        L2d:
            r2 = 1
            int r7 = r7 + r2
            r0.setTag(r8)
            android.content.res.Resources r8 = r0.getResources()
            android.content.res.Configuration r8 = r8.getConfiguration()
            java.util.Locale r8 = r8.locale
            java.lang.Object[] r3 = new java.lang.Object[r2]
            java.lang.Integer r4 = java.lang.Integer.valueOf(r7)
            r3[r1] = r4
            java.lang.String r4 = "%d"
            java.lang.String r8 = java.lang.String.format(r8, r4, r3)
            r0.setText(r8)
            com.google.android.material.datepicker.Month r8 = r5.a
            long r7 = r8.h(r7)
            com.google.android.material.datepicker.Month r3 = r5.a
            int r3 = r3.g0
            com.google.android.material.datepicker.Month r4 = com.google.android.material.datepicker.Month.f()
            int r4 = r4.g0
            if (r3 != r4) goto L67
            java.lang.String r7 = defpackage.qe0.g(r7)
            r0.setContentDescription(r7)
            goto L6e
        L67:
            java.lang.String r7 = defpackage.qe0.l(r7)
            r0.setContentDescription(r7)
        L6e:
            r0.setVisibility(r1)
            r0.setEnabled(r2)
            goto L7d
        L75:
            r7 = 8
            r0.setVisibility(r7)
            r0.setEnabled(r1)
        L7d:
            java.lang.Long r6 = r5.getItem(r6)
            if (r6 != 0) goto L84
            return r0
        L84:
            long r6 = r6.longValue()
            r5.k(r0, r6)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.datepicker.c.getView(int, android.view.View, android.view.ViewGroup):android.widget.TextView");
    }

    public final void e(Context context) {
        if (this.h0 == null) {
            this.h0 = new tu(context);
        }
    }

    public boolean f(int i) {
        return i % this.a.h0 == 0;
    }

    public boolean g(int i) {
        return (i + 1) % this.a.h0 == 0;
    }

    @Override // android.widget.Adapter
    public int getCount() {
        return this.a.i0 + b();
    }

    @Override // android.widget.Adapter
    public long getItemId(int i) {
        return i / this.a.h0;
    }

    public final boolean h(long j) {
        Iterator<Long> it = this.f0.k1().iterator();
        while (it.hasNext()) {
            if (hg4.a(j) == hg4.a(it.next().longValue())) {
                return true;
            }
        }
        return false;
    }

    @Override // android.widget.BaseAdapter, android.widget.Adapter
    public boolean hasStableIds() {
        return true;
    }

    public int i() {
        return (this.a.g() + this.a.i0) - 1;
    }

    public int j(int i) {
        return (i - this.a.g()) + 1;
    }

    public final void k(TextView textView, long j) {
        su suVar;
        if (textView == null) {
            return;
        }
        if (this.i0.f().W0(j)) {
            textView.setEnabled(true);
            if (h(j)) {
                suVar = this.h0.b;
            } else if (hg4.o().getTimeInMillis() == j) {
                suVar = this.h0.c;
            } else {
                suVar = this.h0.a;
            }
        } else {
            textView.setEnabled(false);
            suVar = this.h0.g;
        }
        suVar.d(textView);
    }

    public final void l(MaterialCalendarGridView materialCalendarGridView, long j) {
        if (Month.e(j).equals(this.a)) {
            k((TextView) materialCalendarGridView.getChildAt(materialCalendarGridView.getAdapter2().a(this.a.j(j)) - materialCalendarGridView.getFirstVisiblePosition()), j);
        }
    }

    public void m(MaterialCalendarGridView materialCalendarGridView) {
        for (Long l : this.g0) {
            l(materialCalendarGridView, l.longValue());
        }
        DateSelector<?> dateSelector = this.f0;
        if (dateSelector != null) {
            for (Long l2 : dateSelector.k1()) {
                l(materialCalendarGridView, l2.longValue());
            }
            this.g0 = this.f0.k1();
        }
    }

    public boolean n(int i) {
        return i >= b() && i <= i();
    }
}
