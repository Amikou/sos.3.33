package com.google.android.material.datepicker;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import com.google.android.material.textfield.TextInputLayout;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;

/* loaded from: classes2.dex */
public class SingleDateSelector implements DateSelector<Long> {
    public static final Parcelable.Creator<SingleDateSelector> CREATOR = new b();
    public Long a;

    /* loaded from: classes2.dex */
    public class a extends com.google.android.material.datepicker.a {
        public final /* synthetic */ sm2 k0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(String str, DateFormat dateFormat, TextInputLayout textInputLayout, CalendarConstraints calendarConstraints, sm2 sm2Var) {
            super(str, dateFormat, textInputLayout, calendarConstraints);
            this.k0 = sm2Var;
        }

        @Override // com.google.android.material.datepicker.a
        public void e() {
            this.k0.a();
        }

        @Override // com.google.android.material.datepicker.a
        public void f(Long l) {
            if (l == null) {
                SingleDateSelector.this.c();
            } else {
                SingleDateSelector.this.z1(l.longValue());
            }
            this.k0.b(SingleDateSelector.this.o1());
        }
    }

    /* loaded from: classes2.dex */
    public static class b implements Parcelable.Creator<SingleDateSelector> {
        @Override // android.os.Parcelable.Creator
        /* renamed from: a */
        public SingleDateSelector createFromParcel(Parcel parcel) {
            SingleDateSelector singleDateSelector = new SingleDateSelector();
            singleDateSelector.a = (Long) parcel.readValue(Long.class.getClassLoader());
            return singleDateSelector;
        }

        @Override // android.os.Parcelable.Creator
        /* renamed from: b */
        public SingleDateSelector[] newArray(int i) {
            return new SingleDateSelector[i];
        }
    }

    @Override // com.google.android.material.datepicker.DateSelector
    public String N(Context context) {
        Resources resources = context.getResources();
        Long l = this.a;
        if (l == null) {
            return resources.getString(r13.mtrl_picker_date_header_unselected);
        }
        return resources.getString(r13.mtrl_picker_date_header_selected, qe0.j(l.longValue()));
    }

    @Override // com.google.android.material.datepicker.DateSelector
    public Collection<jp2<Long, Long>> R() {
        return new ArrayList();
    }

    @Override // com.google.android.material.datepicker.DateSelector
    public View S(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle, CalendarConstraints calendarConstraints, sm2<Long> sm2Var) {
        View inflate = layoutInflater.inflate(x03.mtrl_picker_text_input_date, viewGroup, false);
        TextInputLayout textInputLayout = (TextInputLayout) inflate.findViewById(b03.mtrl_picker_text_input_date);
        EditText editText = textInputLayout.getEditText();
        if (s32.a()) {
            editText.setInputType(17);
        }
        SimpleDateFormat k = hg4.k();
        String l = hg4.l(inflate.getResources(), k);
        textInputLayout.setPlaceholderText(l);
        Long l2 = this.a;
        if (l2 != null) {
            editText.setText(k.format(l2));
        }
        editText.addTextChangedListener(new a(l, k, textInputLayout, calendarConstraints, sm2Var));
        mk4.k(editText);
        return inflate;
    }

    @Override // com.google.android.material.datepicker.DateSelector
    public int U0(Context context) {
        return i42.c(context, gy2.materialCalendarTheme, com.google.android.material.datepicker.b.class.getCanonicalName());
    }

    @Override // com.google.android.material.datepicker.DateSelector
    public boolean Z0() {
        return this.a != null;
    }

    public final void c() {
        this.a = null;
    }

    @Override // com.google.android.material.datepicker.DateSelector
    /* renamed from: d */
    public Long o1() {
        return this.a;
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // com.google.android.material.datepicker.DateSelector
    public Collection<Long> k1() {
        ArrayList arrayList = new ArrayList();
        Long l = this.a;
        if (l != null) {
            arrayList.add(l);
        }
        return arrayList;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeValue(this.a);
    }

    @Override // com.google.android.material.datepicker.DateSelector
    public void z1(long j) {
        this.a = Long.valueOf(j);
    }
}
