package com.google.android.material.datepicker;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import com.google.android.material.textfield.TextInputLayout;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;

/* loaded from: classes2.dex */
public class RangeDateSelector implements DateSelector<jp2<Long, Long>> {
    public static final Parcelable.Creator<RangeDateSelector> CREATOR = new c();
    public String a;
    public Long f0 = null;
    public Long g0 = null;
    public Long h0 = null;
    public Long i0 = null;

    /* loaded from: classes2.dex */
    public class a extends com.google.android.material.datepicker.a {
        public final /* synthetic */ TextInputLayout k0;
        public final /* synthetic */ TextInputLayout l0;
        public final /* synthetic */ sm2 m0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(String str, DateFormat dateFormat, TextInputLayout textInputLayout, CalendarConstraints calendarConstraints, TextInputLayout textInputLayout2, TextInputLayout textInputLayout3, sm2 sm2Var) {
            super(str, dateFormat, textInputLayout, calendarConstraints);
            this.k0 = textInputLayout2;
            this.l0 = textInputLayout3;
            this.m0 = sm2Var;
        }

        @Override // com.google.android.material.datepicker.a
        public void e() {
            RangeDateSelector.this.h0 = null;
            RangeDateSelector.this.j(this.k0, this.l0, this.m0);
        }

        @Override // com.google.android.material.datepicker.a
        public void f(Long l) {
            RangeDateSelector.this.h0 = l;
            RangeDateSelector.this.j(this.k0, this.l0, this.m0);
        }
    }

    /* loaded from: classes2.dex */
    public class b extends com.google.android.material.datepicker.a {
        public final /* synthetic */ TextInputLayout k0;
        public final /* synthetic */ TextInputLayout l0;
        public final /* synthetic */ sm2 m0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(String str, DateFormat dateFormat, TextInputLayout textInputLayout, CalendarConstraints calendarConstraints, TextInputLayout textInputLayout2, TextInputLayout textInputLayout3, sm2 sm2Var) {
            super(str, dateFormat, textInputLayout, calendarConstraints);
            this.k0 = textInputLayout2;
            this.l0 = textInputLayout3;
            this.m0 = sm2Var;
        }

        @Override // com.google.android.material.datepicker.a
        public void e() {
            RangeDateSelector.this.i0 = null;
            RangeDateSelector.this.j(this.k0, this.l0, this.m0);
        }

        @Override // com.google.android.material.datepicker.a
        public void f(Long l) {
            RangeDateSelector.this.i0 = l;
            RangeDateSelector.this.j(this.k0, this.l0, this.m0);
        }
    }

    /* loaded from: classes2.dex */
    public static class c implements Parcelable.Creator<RangeDateSelector> {
        @Override // android.os.Parcelable.Creator
        /* renamed from: a */
        public RangeDateSelector createFromParcel(Parcel parcel) {
            RangeDateSelector rangeDateSelector = new RangeDateSelector();
            rangeDateSelector.f0 = (Long) parcel.readValue(Long.class.getClassLoader());
            rangeDateSelector.g0 = (Long) parcel.readValue(Long.class.getClassLoader());
            return rangeDateSelector;
        }

        @Override // android.os.Parcelable.Creator
        /* renamed from: b */
        public RangeDateSelector[] newArray(int i) {
            return new RangeDateSelector[i];
        }
    }

    @Override // com.google.android.material.datepicker.DateSelector
    public String N(Context context) {
        Resources resources = context.getResources();
        Long l = this.f0;
        if (l == null && this.g0 == null) {
            return resources.getString(r13.mtrl_picker_range_header_unselected);
        }
        Long l2 = this.g0;
        if (l2 == null) {
            return resources.getString(r13.mtrl_picker_range_header_only_start_selected, qe0.c(l.longValue()));
        }
        if (l == null) {
            return resources.getString(r13.mtrl_picker_range_header_only_end_selected, qe0.c(l2.longValue()));
        }
        jp2<String, String> a2 = qe0.a(l, l2);
        return resources.getString(r13.mtrl_picker_range_header_selected, a2.a, a2.b);
    }

    @Override // com.google.android.material.datepicker.DateSelector
    public Collection<jp2<Long, Long>> R() {
        if (this.f0 != null && this.g0 != null) {
            ArrayList arrayList = new ArrayList();
            arrayList.add(new jp2(this.f0, this.g0));
            return arrayList;
        }
        return new ArrayList();
    }

    @Override // com.google.android.material.datepicker.DateSelector
    public View S(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle, CalendarConstraints calendarConstraints, sm2<jp2<Long, Long>> sm2Var) {
        View inflate = layoutInflater.inflate(x03.mtrl_picker_text_input_date_range, viewGroup, false);
        TextInputLayout textInputLayout = (TextInputLayout) inflate.findViewById(b03.mtrl_picker_text_input_range_start);
        TextInputLayout textInputLayout2 = (TextInputLayout) inflate.findViewById(b03.mtrl_picker_text_input_range_end);
        EditText editText = textInputLayout.getEditText();
        EditText editText2 = textInputLayout2.getEditText();
        if (s32.a()) {
            editText.setInputType(17);
            editText2.setInputType(17);
        }
        this.a = inflate.getResources().getString(r13.mtrl_picker_invalid_range);
        SimpleDateFormat k = hg4.k();
        Long l = this.f0;
        if (l != null) {
            editText.setText(k.format(l));
            this.h0 = this.f0;
        }
        Long l2 = this.g0;
        if (l2 != null) {
            editText2.setText(k.format(l2));
            this.i0 = this.g0;
        }
        String l3 = hg4.l(inflate.getResources(), k);
        textInputLayout.setPlaceholderText(l3);
        textInputLayout2.setPlaceholderText(l3);
        editText.addTextChangedListener(new a(l3, k, textInputLayout, calendarConstraints, textInputLayout, textInputLayout2, sm2Var));
        editText2.addTextChangedListener(new b(l3, k, textInputLayout2, calendarConstraints, textInputLayout, textInputLayout2, sm2Var));
        mk4.k(editText);
        return inflate;
    }

    @Override // com.google.android.material.datepicker.DateSelector
    public int U0(Context context) {
        Resources resources = context.getResources();
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        return i42.c(context, Math.min(displayMetrics.widthPixels, displayMetrics.heightPixels) > resources.getDimensionPixelSize(jz2.mtrl_calendar_maximum_default_fullscreen_minor_axis) ? gy2.materialCalendarTheme : gy2.materialCalendarFullscreenTheme, com.google.android.material.datepicker.b.class.getCanonicalName());
    }

    @Override // com.google.android.material.datepicker.DateSelector
    public boolean Z0() {
        Long l = this.f0;
        return (l == null || this.g0 == null || !h(l.longValue(), this.g0.longValue())) ? false : true;
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public final void f(TextInputLayout textInputLayout, TextInputLayout textInputLayout2) {
        if (textInputLayout.getError() != null && this.a.contentEquals(textInputLayout.getError())) {
            textInputLayout.setError(null);
        }
        if (textInputLayout2.getError() == null || !MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR.contentEquals(textInputLayout2.getError())) {
            return;
        }
        textInputLayout2.setError(null);
    }

    @Override // com.google.android.material.datepicker.DateSelector
    /* renamed from: g */
    public jp2<Long, Long> o1() {
        return new jp2<>(this.f0, this.g0);
    }

    public final boolean h(long j, long j2) {
        return j <= j2;
    }

    public final void i(TextInputLayout textInputLayout, TextInputLayout textInputLayout2) {
        textInputLayout.setError(this.a);
        textInputLayout2.setError(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
    }

    public final void j(TextInputLayout textInputLayout, TextInputLayout textInputLayout2, sm2<jp2<Long, Long>> sm2Var) {
        Long l = this.h0;
        if (l != null && this.i0 != null) {
            if (h(l.longValue(), this.i0.longValue())) {
                this.f0 = this.h0;
                this.g0 = this.i0;
                sm2Var.b(o1());
                return;
            }
            i(textInputLayout, textInputLayout2);
            sm2Var.a();
            return;
        }
        f(textInputLayout, textInputLayout2);
        sm2Var.a();
    }

    @Override // com.google.android.material.datepicker.DateSelector
    public Collection<Long> k1() {
        ArrayList arrayList = new ArrayList();
        Long l = this.f0;
        if (l != null) {
            arrayList.add(l);
        }
        Long l2 = this.g0;
        if (l2 != null) {
            arrayList.add(l2);
        }
        return arrayList;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeValue(this.f0);
        parcel.writeValue(this.g0);
    }

    @Override // com.google.android.material.datepicker.DateSelector
    public void z1(long j) {
        Long l = this.f0;
        if (l == null) {
            this.f0 = Long.valueOf(j);
        } else if (this.g0 == null && h(l.longValue(), j)) {
            this.g0 = Long.valueOf(j);
        } else {
            this.g0 = null;
            this.f0 = Long.valueOf(j);
        }
    }
}
