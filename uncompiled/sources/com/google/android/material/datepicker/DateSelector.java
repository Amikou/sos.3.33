package com.google.android.material.datepicker;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.Collection;

/* loaded from: classes2.dex */
public interface DateSelector<S> extends Parcelable {
    String N(Context context);

    Collection<jp2<Long, Long>> R();

    View S(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle, CalendarConstraints calendarConstraints, sm2<S> sm2Var);

    int U0(Context context);

    boolean Z0();

    Collection<Long> k1();

    S o1();

    void z1(long j);
}
