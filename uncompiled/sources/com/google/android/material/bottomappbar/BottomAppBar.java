package com.google.android.material.bottomappbar;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.ActionMenuView;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.customview.view.AbsSavedState;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.material.behavior.HideBottomViewOnScrollBehavior;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import defpackage.mk4;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes2.dex */
public class BottomAppBar extends Toolbar implements CoordinatorLayout.b {
    public static final int o1 = y13.Widget_MaterialComponents_BottomAppBar;
    public final int T0;
    public final o42 U0;
    public Animator V0;
    public Animator W0;
    public int X0;
    public int Y0;
    public boolean Z0;
    public final boolean a1;
    public final boolean b1;
    public final boolean c1;
    public int d1;
    public ArrayList<j> e1;
    public int f1;
    public boolean g1;
    public boolean h1;
    public Behavior i1;
    public int j1;
    public int k1;
    public int l1;
    public AnimatorListenerAdapter m1;
    public ab4<FloatingActionButton> n1;

    /* loaded from: classes2.dex */
    public static class SavedState extends AbsSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new a();
        public int g0;
        public boolean h0;

        /* loaded from: classes2.dex */
        public static class a implements Parcelable.ClassLoaderCreator<SavedState> {
            @Override // android.os.Parcelable.Creator
            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel, null);
            }

            @Override // android.os.Parcelable.ClassLoaderCreator
            /* renamed from: b */
            public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new SavedState(parcel, classLoader);
            }

            @Override // android.os.Parcelable.Creator
            /* renamed from: c */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        @Override // androidx.customview.view.AbsSavedState, android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.g0);
            parcel.writeInt(this.h0 ? 1 : 0);
        }

        public SavedState(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            this.g0 = parcel.readInt();
            this.h0 = parcel.readInt() != 0;
        }
    }

    /* loaded from: classes2.dex */
    public class a extends AnimatorListenerAdapter {
        public a() {
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationStart(Animator animator) {
            if (BottomAppBar.this.g1) {
                return;
            }
            BottomAppBar bottomAppBar = BottomAppBar.this;
            bottomAppBar.D0(bottomAppBar.X0, BottomAppBar.this.h1);
        }
    }

    /* loaded from: classes2.dex */
    public class b implements ab4<FloatingActionButton> {
        public b() {
        }

        @Override // defpackage.ab4
        /* renamed from: c */
        public void a(FloatingActionButton floatingActionButton) {
            BottomAppBar.this.U0.b0(floatingActionButton.getVisibility() == 0 ? floatingActionButton.getScaleY() : Utils.FLOAT_EPSILON);
        }

        @Override // defpackage.ab4
        /* renamed from: d */
        public void b(FloatingActionButton floatingActionButton) {
            float translationX = floatingActionButton.getTranslationX();
            if (BottomAppBar.this.getTopEdgeTreatment().j() != translationX) {
                BottomAppBar.this.getTopEdgeTreatment().s(translationX);
                BottomAppBar.this.U0.invalidateSelf();
            }
            float f = Utils.FLOAT_EPSILON;
            float max = Math.max((float) Utils.FLOAT_EPSILON, -floatingActionButton.getTranslationY());
            if (BottomAppBar.this.getTopEdgeTreatment().d() != max) {
                BottomAppBar.this.getTopEdgeTreatment().k(max);
                BottomAppBar.this.U0.invalidateSelf();
            }
            o42 o42Var = BottomAppBar.this.U0;
            if (floatingActionButton.getVisibility() == 0) {
                f = floatingActionButton.getScaleY();
            }
            o42Var.b0(f);
        }
    }

    /* loaded from: classes2.dex */
    public class c implements mk4.e {
        public c() {
        }

        @Override // defpackage.mk4.e
        public jp4 a(View view, jp4 jp4Var, mk4.f fVar) {
            boolean z;
            if (BottomAppBar.this.a1) {
                BottomAppBar.this.j1 = jp4Var.j();
            }
            boolean z2 = false;
            if (BottomAppBar.this.b1) {
                z = BottomAppBar.this.l1 != jp4Var.k();
                BottomAppBar.this.l1 = jp4Var.k();
            } else {
                z = false;
            }
            if (BottomAppBar.this.c1) {
                boolean z3 = BottomAppBar.this.k1 != jp4Var.l();
                BottomAppBar.this.k1 = jp4Var.l();
                z2 = z3;
            }
            if (z || z2) {
                BottomAppBar.this.s0();
                BottomAppBar.this.H0();
                BottomAppBar.this.G0();
            }
            return jp4Var;
        }
    }

    /* loaded from: classes2.dex */
    public class d extends AnimatorListenerAdapter {
        public d() {
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            BottomAppBar.this.w0();
            BottomAppBar.this.V0 = null;
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationStart(Animator animator) {
            BottomAppBar.this.x0();
        }
    }

    /* loaded from: classes2.dex */
    public class e extends FloatingActionButton.b {
        public final /* synthetic */ int a;

        /* loaded from: classes2.dex */
        public class a extends FloatingActionButton.b {
            public a() {
            }

            @Override // com.google.android.material.floatingactionbutton.FloatingActionButton.b
            public void b(FloatingActionButton floatingActionButton) {
                BottomAppBar.this.w0();
            }
        }

        public e(int i) {
            this.a = i;
        }

        @Override // com.google.android.material.floatingactionbutton.FloatingActionButton.b
        public void a(FloatingActionButton floatingActionButton) {
            floatingActionButton.setTranslationX(BottomAppBar.this.B0(this.a));
            floatingActionButton.u(new a());
        }
    }

    /* loaded from: classes2.dex */
    public class f extends AnimatorListenerAdapter {
        public f() {
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            BottomAppBar.this.w0();
            BottomAppBar.this.g1 = false;
            BottomAppBar.this.W0 = null;
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationStart(Animator animator) {
            BottomAppBar.this.x0();
        }
    }

    /* loaded from: classes2.dex */
    public class g extends AnimatorListenerAdapter {
        public boolean a;
        public final /* synthetic */ ActionMenuView f0;
        public final /* synthetic */ int g0;
        public final /* synthetic */ boolean h0;

        public g(ActionMenuView actionMenuView, int i, boolean z) {
            this.f0 = actionMenuView;
            this.g0 = i;
            this.h0 = z;
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationCancel(Animator animator) {
            this.a = true;
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            if (this.a) {
                return;
            }
            boolean z = BottomAppBar.this.f1 != 0;
            BottomAppBar bottomAppBar = BottomAppBar.this;
            bottomAppBar.F0(bottomAppBar.f1);
            BottomAppBar.this.K0(this.f0, this.g0, this.h0, z);
        }
    }

    /* loaded from: classes2.dex */
    public class h implements Runnable {
        public final /* synthetic */ ActionMenuView a;
        public final /* synthetic */ int f0;
        public final /* synthetic */ boolean g0;

        public h(ActionMenuView actionMenuView, int i, boolean z) {
            this.a = actionMenuView;
            this.f0 = i;
            this.g0 = z;
        }

        @Override // java.lang.Runnable
        public void run() {
            ActionMenuView actionMenuView = this.a;
            actionMenuView.setTranslationX(BottomAppBar.this.A0(actionMenuView, this.f0, this.g0));
        }
    }

    /* loaded from: classes2.dex */
    public class i extends AnimatorListenerAdapter {
        public i() {
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationStart(Animator animator) {
            BottomAppBar.this.m1.onAnimationStart(animator);
            FloatingActionButton y0 = BottomAppBar.this.y0();
            if (y0 != null) {
                y0.setTranslationX(BottomAppBar.this.getFabTranslationX());
            }
        }
    }

    /* loaded from: classes2.dex */
    public interface j {
        void a(BottomAppBar bottomAppBar);

        void b(BottomAppBar bottomAppBar);
    }

    public BottomAppBar(Context context) {
        this(context, null, 0);
    }

    private ActionMenuView getActionMenuView() {
        for (int i2 = 0; i2 < getChildCount(); i2++) {
            View childAt = getChildAt(i2);
            if (childAt instanceof ActionMenuView) {
                return (ActionMenuView) childAt;
            }
        }
        return null;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public int getBottomInset() {
        return this.j1;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public float getFabTranslationX() {
        return B0(this.X0);
    }

    private float getFabTranslationY() {
        return -getTopEdgeTreatment().d();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public int getLeftInset() {
        return this.l1;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public int getRightInset() {
        return this.k1;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public er getTopEdgeTreatment() {
        return (er) this.U0.D().p();
    }

    public int A0(ActionMenuView actionMenuView, int i2, boolean z) {
        if (i2 == 1 && z) {
            boolean h2 = mk4.h(this);
            int measuredWidth = h2 ? getMeasuredWidth() : 0;
            for (int i3 = 0; i3 < getChildCount(); i3++) {
                View childAt = getChildAt(i3);
                if ((childAt.getLayoutParams() instanceof Toolbar.LayoutParams) && (((Toolbar.LayoutParams) childAt.getLayoutParams()).a & 8388615) == 8388611) {
                    if (h2) {
                        measuredWidth = Math.min(measuredWidth, childAt.getLeft());
                    } else {
                        measuredWidth = Math.max(measuredWidth, childAt.getRight());
                    }
                }
            }
            return measuredWidth - ((h2 ? actionMenuView.getRight() : actionMenuView.getLeft()) + (h2 ? this.k1 : -this.l1));
        }
        return 0;
    }

    public final float B0(int i2) {
        boolean h2 = mk4.h(this);
        if (i2 == 1) {
            return ((getMeasuredWidth() / 2) - (this.T0 + (h2 ? this.l1 : this.k1))) * (h2 ? -1 : 1);
        }
        return Utils.FLOAT_EPSILON;
    }

    public final boolean C0() {
        FloatingActionButton y0 = y0();
        return y0 != null && y0.p();
    }

    public final void D0(int i2, boolean z) {
        if (!ei4.W(this)) {
            this.g1 = false;
            F0(this.f1);
            return;
        }
        Animator animator = this.W0;
        if (animator != null) {
            animator.cancel();
        }
        ArrayList arrayList = new ArrayList();
        if (!C0()) {
            i2 = 0;
            z = false;
        }
        v0(i2, z, arrayList);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(arrayList);
        this.W0 = animatorSet;
        animatorSet.addListener(new f());
        this.W0.start();
    }

    public final void E0(int i2) {
        if (this.X0 == i2 || !ei4.W(this)) {
            return;
        }
        Animator animator = this.V0;
        if (animator != null) {
            animator.cancel();
        }
        ArrayList arrayList = new ArrayList();
        if (this.Y0 == 1) {
            u0(i2, arrayList);
        } else {
            t0(i2, arrayList);
        }
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(arrayList);
        this.V0 = animatorSet;
        animatorSet.addListener(new d());
        this.V0.start();
    }

    public void F0(int i2) {
        if (i2 != 0) {
            this.f1 = 0;
            getMenu().clear();
            x(i2);
        }
    }

    public final void G0() {
        ActionMenuView actionMenuView = getActionMenuView();
        if (actionMenuView == null || this.W0 != null) {
            return;
        }
        actionMenuView.setAlpha(1.0f);
        if (!C0()) {
            J0(actionMenuView, 0, false);
        } else {
            J0(actionMenuView, this.X0, this.h1);
        }
    }

    public final void H0() {
        getTopEdgeTreatment().s(getFabTranslationX());
        View z0 = z0();
        this.U0.b0((this.h1 && C0()) ? 1.0f : Utils.FLOAT_EPSILON);
        if (z0 != null) {
            z0.setTranslationY(getFabTranslationY());
            z0.setTranslationX(getFabTranslationX());
        }
    }

    public boolean I0(int i2) {
        float f2 = i2;
        if (f2 != getTopEdgeTreatment().h()) {
            getTopEdgeTreatment().r(f2);
            this.U0.invalidateSelf();
            return true;
        }
        return false;
    }

    public final void J0(ActionMenuView actionMenuView, int i2, boolean z) {
        K0(actionMenuView, i2, z, false);
    }

    public final void K0(ActionMenuView actionMenuView, int i2, boolean z, boolean z2) {
        h hVar = new h(actionMenuView, i2, z);
        if (z2) {
            actionMenuView.post(hVar);
        } else {
            hVar.run();
        }
    }

    public ColorStateList getBackgroundTint() {
        return this.U0.H();
    }

    public float getCradleVerticalOffset() {
        return getTopEdgeTreatment().d();
    }

    public int getFabAlignmentMode() {
        return this.X0;
    }

    public int getFabAnimationMode() {
        return this.Y0;
    }

    public float getFabCradleMargin() {
        return getTopEdgeTreatment().f();
    }

    public float getFabCradleRoundedCornerRadius() {
        return getTopEdgeTreatment().g();
    }

    public boolean getHideOnScroll() {
        return this.Z0;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        p42.f(this, this.U0);
        if (getParent() instanceof ViewGroup) {
            ((ViewGroup) getParent()).setClipChildren(false);
        }
    }

    @Override // androidx.appcompat.widget.Toolbar, android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        super.onLayout(z, i2, i3, i4, i5);
        if (z) {
            s0();
            H0();
        }
        G0();
    }

    @Override // androidx.appcompat.widget.Toolbar, android.view.View
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.a());
        this.X0 = savedState.g0;
        this.h1 = savedState.h0;
    }

    @Override // androidx.appcompat.widget.Toolbar, android.view.View
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.g0 = this.X0;
        savedState.h0 = this.h1;
        return savedState;
    }

    public final void r0(FloatingActionButton floatingActionButton) {
        floatingActionButton.e(this.m1);
        floatingActionButton.f(new i());
        floatingActionButton.g(this.n1);
    }

    public final void s0() {
        Animator animator = this.W0;
        if (animator != null) {
            animator.cancel();
        }
        Animator animator2 = this.V0;
        if (animator2 != null) {
            animator2.cancel();
        }
    }

    public void setBackgroundTint(ColorStateList colorStateList) {
        androidx.core.graphics.drawable.a.o(this.U0, colorStateList);
    }

    public void setCradleVerticalOffset(float f2) {
        if (f2 != getCradleVerticalOffset()) {
            getTopEdgeTreatment().k(f2);
            this.U0.invalidateSelf();
            H0();
        }
    }

    @Override // android.view.View
    public void setElevation(float f2) {
        this.U0.Z(f2);
        getBehavior().c(this, this.U0.C() - this.U0.B());
    }

    public void setFabAlignmentMode(int i2) {
        setFabAlignmentModeAndReplaceMenu(i2, 0);
    }

    public void setFabAlignmentModeAndReplaceMenu(int i2, int i3) {
        this.f1 = i3;
        this.g1 = true;
        D0(i2, this.h1);
        E0(i2);
        this.X0 = i2;
    }

    public void setFabAnimationMode(int i2) {
        this.Y0 = i2;
    }

    public void setFabCornerSize(float f2) {
        if (f2 != getTopEdgeTreatment().e()) {
            getTopEdgeTreatment().l(f2);
            this.U0.invalidateSelf();
        }
    }

    public void setFabCradleMargin(float f2) {
        if (f2 != getFabCradleMargin()) {
            getTopEdgeTreatment().o(f2);
            this.U0.invalidateSelf();
        }
    }

    public void setFabCradleRoundedCornerRadius(float f2) {
        if (f2 != getFabCradleRoundedCornerRadius()) {
            getTopEdgeTreatment().p(f2);
            this.U0.invalidateSelf();
        }
    }

    public void setHideOnScroll(boolean z) {
        this.Z0 = z;
    }

    @Override // androidx.appcompat.widget.Toolbar
    public void setSubtitle(CharSequence charSequence) {
    }

    @Override // androidx.appcompat.widget.Toolbar
    public void setTitle(CharSequence charSequence) {
    }

    public void t0(int i2, List<Animator> list) {
        FloatingActionButton y0 = y0();
        if (y0 == null || y0.o()) {
            return;
        }
        x0();
        y0.m(new e(i2));
    }

    public final void u0(int i2, List<Animator> list) {
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(y0(), "translationX", B0(i2));
        ofFloat.setDuration(300L);
        list.add(ofFloat);
    }

    public final void v0(int i2, boolean z, List<Animator> list) {
        ActionMenuView actionMenuView = getActionMenuView();
        if (actionMenuView == null) {
            return;
        }
        Animator ofFloat = ObjectAnimator.ofFloat(actionMenuView, "alpha", 1.0f);
        if (Math.abs(actionMenuView.getTranslationX() - A0(actionMenuView, i2, z)) > 1.0f) {
            ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(actionMenuView, "alpha", Utils.FLOAT_EPSILON);
            ofFloat2.addListener(new g(actionMenuView, i2, z));
            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.setDuration(150L);
            animatorSet.playSequentially(ofFloat2, ofFloat);
            list.add(animatorSet);
        } else if (actionMenuView.getAlpha() < 1.0f) {
            list.add(ofFloat);
        }
    }

    public final void w0() {
        ArrayList<j> arrayList;
        int i2 = this.d1 - 1;
        this.d1 = i2;
        if (i2 != 0 || (arrayList = this.e1) == null) {
            return;
        }
        Iterator<j> it = arrayList.iterator();
        while (it.hasNext()) {
            it.next().b(this);
        }
    }

    public final void x0() {
        ArrayList<j> arrayList;
        int i2 = this.d1;
        this.d1 = i2 + 1;
        if (i2 != 0 || (arrayList = this.e1) == null) {
            return;
        }
        Iterator<j> it = arrayList.iterator();
        while (it.hasNext()) {
            it.next().a(this);
        }
    }

    public final FloatingActionButton y0() {
        View z0 = z0();
        if (z0 instanceof FloatingActionButton) {
            return (FloatingActionButton) z0;
        }
        return null;
    }

    /* JADX WARN: Removed duplicated region for block: B:8:0x001e  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final android.view.View z0() {
        /*
            r4 = this;
            android.view.ViewParent r0 = r4.getParent()
            boolean r0 = r0 instanceof androidx.coordinatorlayout.widget.CoordinatorLayout
            r1 = 0
            if (r0 != 0) goto La
            return r1
        La:
            android.view.ViewParent r0 = r4.getParent()
            androidx.coordinatorlayout.widget.CoordinatorLayout r0 = (androidx.coordinatorlayout.widget.CoordinatorLayout) r0
            java.util.List r0 = r0.getDependents(r4)
            java.util.Iterator r0 = r0.iterator()
        L18:
            boolean r2 = r0.hasNext()
            if (r2 == 0) goto L2d
            java.lang.Object r2 = r0.next()
            android.view.View r2 = (android.view.View) r2
            boolean r3 = r2 instanceof com.google.android.material.floatingactionbutton.FloatingActionButton
            if (r3 != 0) goto L2c
            boolean r3 = r2 instanceof com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton
            if (r3 == 0) goto L18
        L2c:
            return r2
        L2d:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.bottomappbar.BottomAppBar.z0():android.view.View");
    }

    public BottomAppBar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, gy2.bottomAppBarStyle);
    }

    @Override // androidx.coordinatorlayout.widget.CoordinatorLayout.b
    public Behavior getBehavior() {
        if (this.i1 == null) {
            this.i1 = new Behavior();
        }
        return this.i1;
    }

    /* loaded from: classes2.dex */
    public static class Behavior extends HideBottomViewOnScrollBehavior<BottomAppBar> {
        public final Rect e;
        public WeakReference<BottomAppBar> f;
        public int g;
        public final View.OnLayoutChangeListener h;

        /* loaded from: classes2.dex */
        public class a implements View.OnLayoutChangeListener {
            public a() {
            }

            @Override // android.view.View.OnLayoutChangeListener
            public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                BottomAppBar bottomAppBar = (BottomAppBar) Behavior.this.f.get();
                if (bottomAppBar != null && (view instanceof FloatingActionButton)) {
                    FloatingActionButton floatingActionButton = (FloatingActionButton) view;
                    floatingActionButton.j(Behavior.this.e);
                    int height = Behavior.this.e.height();
                    bottomAppBar.I0(height);
                    bottomAppBar.setFabCornerSize(floatingActionButton.getShapeAppearanceModel().r().a(new RectF(Behavior.this.e)));
                    CoordinatorLayout.e eVar = (CoordinatorLayout.e) view.getLayoutParams();
                    if (Behavior.this.g == 0) {
                        ((ViewGroup.MarginLayoutParams) eVar).bottomMargin = bottomAppBar.getBottomInset() + (bottomAppBar.getResources().getDimensionPixelOffset(jz2.mtrl_bottomappbar_fab_bottom_margin) - ((floatingActionButton.getMeasuredHeight() - height) / 2));
                        ((ViewGroup.MarginLayoutParams) eVar).leftMargin = bottomAppBar.getLeftInset();
                        ((ViewGroup.MarginLayoutParams) eVar).rightMargin = bottomAppBar.getRightInset();
                        if (mk4.h(floatingActionButton)) {
                            ((ViewGroup.MarginLayoutParams) eVar).leftMargin += bottomAppBar.T0;
                            return;
                        } else {
                            ((ViewGroup.MarginLayoutParams) eVar).rightMargin += bottomAppBar.T0;
                            return;
                        }
                    }
                    return;
                }
                view.removeOnLayoutChangeListener(this);
            }
        }

        public Behavior() {
            this.h = new a();
            this.e = new Rect();
        }

        @Override // com.google.android.material.behavior.HideBottomViewOnScrollBehavior, androidx.coordinatorlayout.widget.CoordinatorLayout.Behavior
        /* renamed from: i */
        public boolean onLayoutChild(CoordinatorLayout coordinatorLayout, BottomAppBar bottomAppBar, int i) {
            this.f = new WeakReference<>(bottomAppBar);
            View z0 = bottomAppBar.z0();
            if (z0 != null && !ei4.W(z0)) {
                CoordinatorLayout.e eVar = (CoordinatorLayout.e) z0.getLayoutParams();
                eVar.d = 49;
                this.g = ((ViewGroup.MarginLayoutParams) eVar).bottomMargin;
                if (z0 instanceof FloatingActionButton) {
                    FloatingActionButton floatingActionButton = (FloatingActionButton) z0;
                    floatingActionButton.addOnLayoutChangeListener(this.h);
                    bottomAppBar.r0(floatingActionButton);
                }
                bottomAppBar.H0();
            }
            coordinatorLayout.onLayoutChild(bottomAppBar, i);
            return super.onLayoutChild(coordinatorLayout, bottomAppBar, i);
        }

        @Override // com.google.android.material.behavior.HideBottomViewOnScrollBehavior, androidx.coordinatorlayout.widget.CoordinatorLayout.Behavior
        /* renamed from: j */
        public boolean onStartNestedScroll(CoordinatorLayout coordinatorLayout, BottomAppBar bottomAppBar, View view, View view2, int i, int i2) {
            return bottomAppBar.getHideOnScroll() && super.onStartNestedScroll(coordinatorLayout, bottomAppBar, view, view2, i, i2);
        }

        public Behavior(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            this.h = new a();
            this.e = new Rect();
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public BottomAppBar(android.content.Context r11, android.util.AttributeSet r12, int r13) {
        /*
            r10 = this;
            int r6 = com.google.android.material.bottomappbar.BottomAppBar.o1
            android.content.Context r11 = defpackage.r42.c(r11, r12, r13, r6)
            r10.<init>(r11, r12, r13)
            o42 r11 = new o42
            r11.<init>()
            r10.U0 = r11
            r7 = 0
            r10.d1 = r7
            r10.f1 = r7
            r10.g1 = r7
            r0 = 1
            r10.h1 = r0
            com.google.android.material.bottomappbar.BottomAppBar$a r0 = new com.google.android.material.bottomappbar.BottomAppBar$a
            r0.<init>()
            r10.m1 = r0
            com.google.android.material.bottomappbar.BottomAppBar$b r0 = new com.google.android.material.bottomappbar.BottomAppBar$b
            r0.<init>()
            r10.n1 = r0
            android.content.Context r8 = r10.getContext()
            int[] r2 = defpackage.o23.BottomAppBar
            int[] r5 = new int[r7]
            r0 = r8
            r1 = r12
            r3 = r13
            r4 = r6
            android.content.res.TypedArray r0 = defpackage.a54.h(r0, r1, r2, r3, r4, r5)
            int r1 = defpackage.o23.BottomAppBar_backgroundTint
            android.content.res.ColorStateList r1 = defpackage.n42.b(r8, r0, r1)
            int r2 = defpackage.o23.BottomAppBar_elevation
            int r2 = r0.getDimensionPixelSize(r2, r7)
            int r3 = defpackage.o23.BottomAppBar_fabCradleMargin
            int r3 = r0.getDimensionPixelOffset(r3, r7)
            float r3 = (float) r3
            int r4 = defpackage.o23.BottomAppBar_fabCradleRoundedCornerRadius
            int r4 = r0.getDimensionPixelOffset(r4, r7)
            float r4 = (float) r4
            int r5 = defpackage.o23.BottomAppBar_fabCradleVerticalOffset
            int r5 = r0.getDimensionPixelOffset(r5, r7)
            float r5 = (float) r5
            int r9 = defpackage.o23.BottomAppBar_fabAlignmentMode
            int r9 = r0.getInt(r9, r7)
            r10.X0 = r9
            int r9 = defpackage.o23.BottomAppBar_fabAnimationMode
            int r9 = r0.getInt(r9, r7)
            r10.Y0 = r9
            int r9 = defpackage.o23.BottomAppBar_hideOnScroll
            boolean r9 = r0.getBoolean(r9, r7)
            r10.Z0 = r9
            int r9 = defpackage.o23.BottomAppBar_paddingBottomSystemWindowInsets
            boolean r9 = r0.getBoolean(r9, r7)
            r10.a1 = r9
            int r9 = defpackage.o23.BottomAppBar_paddingLeftSystemWindowInsets
            boolean r9 = r0.getBoolean(r9, r7)
            r10.b1 = r9
            int r9 = defpackage.o23.BottomAppBar_paddingRightSystemWindowInsets
            boolean r7 = r0.getBoolean(r9, r7)
            r10.c1 = r7
            r0.recycle()
            android.content.res.Resources r0 = r10.getResources()
            int r7 = defpackage.jz2.mtrl_bottomappbar_fabOffsetEndMode
            int r0 = r0.getDimensionPixelOffset(r7)
            r10.T0 = r0
            er r0 = new er
            r0.<init>(r3, r4, r5)
            pn3$b r3 = defpackage.pn3.a()
            pn3$b r0 = r3.B(r0)
            pn3 r0 = r0.m()
            r11.setShapeAppearanceModel(r0)
            r0 = 2
            r11.i0(r0)
            android.graphics.Paint$Style r0 = android.graphics.Paint.Style.FILL
            r11.d0(r0)
            r11.P(r8)
            float r0 = (float) r2
            r10.setElevation(r0)
            androidx.core.graphics.drawable.a.o(r11, r1)
            defpackage.ei4.w0(r10, r11)
            com.google.android.material.bottomappbar.BottomAppBar$c r11 = new com.google.android.material.bottomappbar.BottomAppBar$c
            r11.<init>()
            defpackage.mk4.b(r10, r12, r13, r6, r11)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.bottomappbar.BottomAppBar.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }
}
