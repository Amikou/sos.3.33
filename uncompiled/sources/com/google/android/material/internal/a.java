package com.google.android.material.internal;

import android.animation.TimeInterpolator;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.Build;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.TextUtils;
import android.view.View;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.material.internal.StaticLayoutBuilderCompat;
import defpackage.lv;

/* compiled from: CollapsingTextHelper.java */
/* loaded from: classes2.dex */
public final class a {
    public static final boolean j0;
    public static final Paint k0;
    public lv A;
    public CharSequence B;
    public CharSequence C;
    public boolean D;
    public boolean F;
    public Bitmap G;
    public Paint H;
    public float I;
    public float J;
    public int[] K;
    public boolean L;
    public final TextPaint M;
    public final TextPaint N;
    public TimeInterpolator O;
    public TimeInterpolator P;
    public float Q;
    public float R;
    public float S;
    public ColorStateList T;
    public float U;
    public float V;
    public float W;
    public ColorStateList X;
    public float Y;
    public float Z;
    public final View a;
    public StaticLayout a0;
    public boolean b;
    public float b0;
    public float c;
    public float c0;
    public boolean d;
    public float d0;
    public float e;
    public CharSequence e0;
    public float f;
    public int g;
    public final Rect h;
    public final Rect i;
    public final RectF j;
    public ColorStateList o;
    public ColorStateList p;
    public float q;
    public float r;
    public float s;
    public float t;
    public float u;
    public float v;
    public Typeface w;
    public Typeface x;
    public Typeface y;
    public lv z;
    public int k = 16;
    public int l = 16;
    public float m = 15.0f;
    public float n = 15.0f;
    public boolean E = true;
    public int f0 = 1;
    public float g0 = Utils.FLOAT_EPSILON;
    public float h0 = 1.0f;
    public int i0 = StaticLayoutBuilderCompat.n;

    /* compiled from: CollapsingTextHelper.java */
    /* renamed from: com.google.android.material.internal.a$a  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public class C0122a implements lv.a {
        public C0122a() {
        }

        @Override // defpackage.lv.a
        public void a(Typeface typeface) {
            a.this.d0(typeface);
        }
    }

    /* compiled from: CollapsingTextHelper.java */
    /* loaded from: classes2.dex */
    public class b implements lv.a {
        public b() {
        }

        @Override // defpackage.lv.a
        public void a(Typeface typeface) {
            a.this.n0(typeface);
        }
    }

    static {
        j0 = Build.VERSION.SDK_INT < 18;
        k0 = null;
    }

    public a(View view) {
        this.a = view;
        TextPaint textPaint = new TextPaint(129);
        this.M = textPaint;
        this.N = new TextPaint(textPaint);
        this.i = new Rect();
        this.h = new Rect();
        this.j = new RectF();
        this.f = f();
    }

    public static boolean O(float f, float f2) {
        return Math.abs(f - f2) < 0.001f;
    }

    public static float S(float f, float f2, float f3, TimeInterpolator timeInterpolator) {
        if (timeInterpolator != null) {
            f3 = timeInterpolator.getInterpolation(f3);
        }
        return ne.a(f, f2, f3);
    }

    public static boolean W(Rect rect, int i, int i2, int i3, int i4) {
        return rect.left == i && rect.top == i2 && rect.right == i3 && rect.bottom == i4;
    }

    public static int a(int i, int i2, float f) {
        float f2 = 1.0f - f;
        return Color.argb((int) ((Color.alpha(i) * f2) + (Color.alpha(i2) * f)), (int) ((Color.red(i) * f2) + (Color.red(i2) * f)), (int) ((Color.green(i) * f2) + (Color.green(i2) * f)), (int) ((Color.blue(i) * f2) + (Color.blue(i2) * f)));
    }

    public int A() {
        return this.k;
    }

    public void A0(CharSequence charSequence) {
        if (charSequence == null || !TextUtils.equals(this.B, charSequence)) {
            this.B = charSequence;
            this.C = null;
            k();
            U();
        }
    }

    public float B() {
        M(this.N);
        return -this.N.ascent();
    }

    public void B0(TimeInterpolator timeInterpolator) {
        this.P = timeInterpolator;
        U();
    }

    public Typeface C() {
        Typeface typeface = this.x;
        return typeface != null ? typeface : Typeface.DEFAULT;
    }

    public void C0(Typeface typeface) {
        boolean e0 = e0(typeface);
        boolean o0 = o0(typeface);
        if (e0 || o0) {
            U();
        }
    }

    public float D() {
        return this.c;
    }

    public final boolean D0() {
        return this.f0 > 1 && (!this.D || this.d) && !this.F;
    }

    public float E() {
        return this.f;
    }

    public int F() {
        return this.i0;
    }

    public int G() {
        StaticLayout staticLayout = this.a0;
        if (staticLayout != null) {
            return staticLayout.getLineCount();
        }
        return 0;
    }

    public float H() {
        return this.a0.getSpacingAdd();
    }

    public float I() {
        return this.a0.getSpacingMultiplier();
    }

    public int J() {
        return this.f0;
    }

    public CharSequence K() {
        return this.B;
    }

    public final void L(TextPaint textPaint) {
        textPaint.setTextSize(this.n);
        textPaint.setTypeface(this.w);
        if (Build.VERSION.SDK_INT >= 21) {
            textPaint.setLetterSpacing(this.Y);
        }
    }

    public final void M(TextPaint textPaint) {
        textPaint.setTextSize(this.m);
        textPaint.setTypeface(this.x);
        if (Build.VERSION.SDK_INT >= 21) {
            textPaint.setLetterSpacing(this.Z);
        }
    }

    public final void N(float f) {
        if (this.d) {
            this.j.set(f < this.f ? this.h : this.i);
            return;
        }
        this.j.left = S(this.h.left, this.i.left, f, this.O);
        this.j.top = S(this.q, this.r, f, this.O);
        this.j.right = S(this.h.right, this.i.right, f, this.O);
        this.j.bottom = S(this.h.bottom, this.i.bottom, f, this.O);
    }

    public final boolean P() {
        return ei4.E(this.a) == 1;
    }

    public final boolean Q() {
        ColorStateList colorStateList;
        ColorStateList colorStateList2 = this.p;
        return (colorStateList2 != null && colorStateList2.isStateful()) || ((colorStateList = this.o) != null && colorStateList.isStateful());
    }

    public final boolean R(CharSequence charSequence, boolean z) {
        return (z ? h44.d : h44.c).a(charSequence, 0, charSequence.length());
    }

    public void T() {
        this.b = this.i.width() > 0 && this.i.height() > 0 && this.h.width() > 0 && this.h.height() > 0;
    }

    public void U() {
        V(false);
    }

    public void V(boolean z) {
        if ((this.a.getHeight() <= 0 || this.a.getWidth() <= 0) && !z) {
            return;
        }
        b(z);
        d();
    }

    public void X(int i, int i2, int i3, int i4) {
        if (W(this.i, i, i2, i3, i4)) {
            return;
        }
        this.i.set(i, i2, i3, i4);
        this.L = true;
        T();
    }

    public void Y(Rect rect) {
        X(rect.left, rect.top, rect.right, rect.bottom);
    }

    public void Z(int i) {
        d44 d44Var = new d44(this.a.getContext(), i);
        ColorStateList colorStateList = d44Var.a;
        if (colorStateList != null) {
            this.p = colorStateList;
        }
        float f = d44Var.k;
        if (f != Utils.FLOAT_EPSILON) {
            this.n = f;
        }
        ColorStateList colorStateList2 = d44Var.b;
        if (colorStateList2 != null) {
            this.T = colorStateList2;
        }
        this.R = d44Var.f;
        this.S = d44Var.g;
        this.Q = d44Var.h;
        this.Y = d44Var.j;
        lv lvVar = this.A;
        if (lvVar != null) {
            lvVar.c();
        }
        this.A = new lv(new C0122a(), d44Var.e());
        d44Var.g(this.a.getContext(), this.A);
        U();
    }

    public final void a0(float f) {
        this.b0 = f;
        ei4.j0(this.a);
    }

    public final void b(boolean z) {
        StaticLayout staticLayout;
        StaticLayout staticLayout2;
        float f = this.J;
        j(this.n, z);
        CharSequence charSequence = this.C;
        if (charSequence != null && (staticLayout2 = this.a0) != null) {
            this.e0 = TextUtils.ellipsize(charSequence, this.M, staticLayout2.getWidth(), TextUtils.TruncateAt.END);
        }
        CharSequence charSequence2 = this.e0;
        float f2 = Utils.FLOAT_EPSILON;
        float measureText = charSequence2 != null ? this.M.measureText(charSequence2, 0, charSequence2.length()) : 0.0f;
        int b2 = ri1.b(this.l, this.D ? 1 : 0);
        int i = b2 & 112;
        if (i == 48) {
            this.r = this.i.top;
        } else if (i != 80) {
            this.r = this.i.centerY() - ((this.M.descent() - this.M.ascent()) / 2.0f);
        } else {
            this.r = this.i.bottom + this.M.ascent();
        }
        int i2 = b2 & 8388615;
        if (i2 == 1) {
            this.t = this.i.centerX() - (measureText / 2.0f);
        } else if (i2 != 5) {
            this.t = this.i.left;
        } else {
            this.t = this.i.right - measureText;
        }
        j(this.m, z);
        float height = this.a0 != null ? staticLayout.getHeight() : 0.0f;
        CharSequence charSequence3 = this.C;
        float measureText2 = charSequence3 != null ? this.M.measureText(charSequence3, 0, charSequence3.length()) : 0.0f;
        StaticLayout staticLayout3 = this.a0;
        if (staticLayout3 != null && this.f0 > 1) {
            measureText2 = staticLayout3.getWidth();
        }
        StaticLayout staticLayout4 = this.a0;
        if (staticLayout4 != null) {
            f2 = this.f0 > 1 ? staticLayout4.getLineStart(0) : staticLayout4.getLineLeft(0);
        }
        this.d0 = f2;
        int b3 = ri1.b(this.k, this.D ? 1 : 0);
        int i3 = b3 & 112;
        if (i3 == 48) {
            this.q = this.h.top;
        } else if (i3 != 80) {
            this.q = this.h.centerY() - (height / 2.0f);
        } else {
            this.q = (this.h.bottom - height) + this.M.descent();
        }
        int i4 = b3 & 8388615;
        if (i4 == 1) {
            this.s = this.h.centerX() - (measureText2 / 2.0f);
        } else if (i4 != 5) {
            this.s = this.h.left;
        } else {
            this.s = this.h.right - measureText2;
        }
        k();
        t0(f);
    }

    public void b0(ColorStateList colorStateList) {
        if (this.p != colorStateList) {
            this.p = colorStateList;
            U();
        }
    }

    public float c() {
        if (this.B == null) {
            return Utils.FLOAT_EPSILON;
        }
        L(this.N);
        TextPaint textPaint = this.N;
        CharSequence charSequence = this.B;
        return textPaint.measureText(charSequence, 0, charSequence.length());
    }

    public void c0(int i) {
        if (this.l != i) {
            this.l = i;
            U();
        }
    }

    public final void d() {
        h(this.c);
    }

    public void d0(Typeface typeface) {
        if (e0(typeface)) {
            U();
        }
    }

    public final float e(float f) {
        float f2 = this.f;
        if (f <= f2) {
            return ne.b(1.0f, Utils.FLOAT_EPSILON, this.e, f2, f);
        }
        return ne.b(Utils.FLOAT_EPSILON, 1.0f, f2, 1.0f, f);
    }

    public final boolean e0(Typeface typeface) {
        lv lvVar = this.A;
        if (lvVar != null) {
            lvVar.c();
        }
        if (this.w != typeface) {
            this.w = typeface;
            return true;
        }
        return false;
    }

    public final float f() {
        float f = this.e;
        return f + ((1.0f - f) * 0.5f);
    }

    public void f0(int i) {
        this.g = i;
    }

    public final boolean g(CharSequence charSequence) {
        boolean P = P();
        return this.E ? R(charSequence, P) : P;
    }

    public void g0(int i, int i2, int i3, int i4) {
        if (W(this.h, i, i2, i3, i4)) {
            return;
        }
        this.h.set(i, i2, i3, i4);
        this.L = true;
        T();
    }

    public final void h(float f) {
        float f2;
        N(f);
        if (this.d) {
            if (f < this.f) {
                this.u = this.s;
                this.v = this.q;
                t0(this.m);
                f2 = 0.0f;
            } else {
                this.u = this.t;
                this.v = this.r - Math.max(0, this.g);
                t0(this.n);
                f2 = 1.0f;
            }
        } else {
            this.u = S(this.s, this.t, f, this.O);
            this.v = S(this.q, this.r, f, this.O);
            t0(S(this.m, this.n, f, this.P));
            f2 = f;
        }
        TimeInterpolator timeInterpolator = ne.b;
        a0(1.0f - S(Utils.FLOAT_EPSILON, 1.0f, 1.0f - f, timeInterpolator));
        j0(S(1.0f, Utils.FLOAT_EPSILON, f, timeInterpolator));
        if (this.p != this.o) {
            this.M.setColor(a(y(), w(), f2));
        } else {
            this.M.setColor(w());
        }
        if (Build.VERSION.SDK_INT >= 21) {
            float f3 = this.Y;
            float f4 = this.Z;
            if (f3 != f4) {
                this.M.setLetterSpacing(S(f4, f3, f, timeInterpolator));
            } else {
                this.M.setLetterSpacing(f3);
            }
        }
        this.M.setShadowLayer(S(this.U, this.Q, f, null), S(this.V, this.R, f, null), S(this.W, this.S, f, null), a(x(this.X), x(this.T), f));
        if (this.d) {
            this.M.setAlpha((int) (e(f) * 255.0f));
        }
        ei4.j0(this.a);
    }

    public void h0(Rect rect) {
        g0(rect.left, rect.top, rect.right, rect.bottom);
    }

    public final void i(float f) {
        j(f, false);
    }

    public void i0(int i) {
        d44 d44Var = new d44(this.a.getContext(), i);
        ColorStateList colorStateList = d44Var.a;
        if (colorStateList != null) {
            this.o = colorStateList;
        }
        float f = d44Var.k;
        if (f != Utils.FLOAT_EPSILON) {
            this.m = f;
        }
        ColorStateList colorStateList2 = d44Var.b;
        if (colorStateList2 != null) {
            this.X = colorStateList2;
        }
        this.V = d44Var.f;
        this.W = d44Var.g;
        this.U = d44Var.h;
        this.Z = d44Var.j;
        lv lvVar = this.z;
        if (lvVar != null) {
            lvVar.c();
        }
        this.z = new lv(new b(), d44Var.e());
        d44Var.g(this.a.getContext(), this.z);
        U();
    }

    public final void j(float f, boolean z) {
        boolean z2;
        float f2;
        boolean z3;
        if (this.B == null) {
            return;
        }
        float width = this.i.width();
        float width2 = this.h.width();
        if (O(f, this.n)) {
            f2 = this.n;
            this.I = 1.0f;
            Typeface typeface = this.y;
            Typeface typeface2 = this.w;
            if (typeface != typeface2) {
                this.y = typeface2;
                z3 = true;
            } else {
                z3 = false;
            }
        } else {
            float f3 = this.m;
            Typeface typeface3 = this.y;
            Typeface typeface4 = this.x;
            if (typeface3 != typeface4) {
                this.y = typeface4;
                z2 = true;
            } else {
                z2 = false;
            }
            if (O(f, f3)) {
                this.I = 1.0f;
            } else {
                this.I = f / this.m;
            }
            float f4 = this.n / this.m;
            width = (!z && width2 * f4 > width) ? Math.min(width / f4, width2) : width2;
            f2 = f3;
            z3 = z2;
        }
        if (width > Utils.FLOAT_EPSILON) {
            z3 = this.J != f2 || this.L || z3;
            this.J = f2;
            this.L = false;
        }
        if (this.C == null || z3) {
            this.M.setTextSize(this.J);
            this.M.setTypeface(this.y);
            this.M.setLinearText(this.I != 1.0f);
            this.D = g(this.B);
            StaticLayout l = l(D0() ? this.f0 : 1, width, this.D);
            this.a0 = l;
            this.C = l.getText();
        }
    }

    public final void j0(float f) {
        this.c0 = f;
        ei4.j0(this.a);
    }

    public final void k() {
        Bitmap bitmap = this.G;
        if (bitmap != null) {
            bitmap.recycle();
            this.G = null;
        }
    }

    public void k0(ColorStateList colorStateList) {
        if (this.o != colorStateList) {
            this.o = colorStateList;
            U();
        }
    }

    public final StaticLayout l(int i, float f, boolean z) {
        StaticLayout staticLayout;
        try {
            staticLayout = StaticLayoutBuilderCompat.c(this.B, this.M, (int) f).e(TextUtils.TruncateAt.END).h(z).d(Layout.Alignment.ALIGN_NORMAL).g(false).j(i).i(this.g0, this.h0).f(this.i0).a();
        } catch (StaticLayoutBuilderCompat.StaticLayoutBuilderCompatException e) {
            e.getCause().getMessage();
            staticLayout = null;
        }
        return (StaticLayout) du2.e(staticLayout);
    }

    public void l0(int i) {
        if (this.k != i) {
            this.k = i;
            U();
        }
    }

    public void m(Canvas canvas) {
        int save = canvas.save();
        if (this.C == null || !this.b) {
            return;
        }
        boolean z = true;
        float lineStart = (this.u + (this.f0 > 1 ? this.a0.getLineStart(0) : this.a0.getLineLeft(0))) - (this.d0 * 2.0f);
        this.M.setTextSize(this.J);
        float f = this.u;
        float f2 = this.v;
        if (!this.F || this.G == null) {
            z = false;
        }
        float f3 = this.I;
        if (f3 != 1.0f && !this.d) {
            canvas.scale(f3, f3, f, f2);
        }
        if (z) {
            canvas.drawBitmap(this.G, f, f2, this.H);
            canvas.restoreToCount(save);
            return;
        }
        if (D0() && (!this.d || this.c > this.f)) {
            n(canvas, lineStart, f2);
        } else {
            canvas.translate(f, f2);
            this.a0.draw(canvas);
        }
        canvas.restoreToCount(save);
    }

    public void m0(float f) {
        if (this.m != f) {
            this.m = f;
            U();
        }
    }

    public final void n(Canvas canvas, float f, float f2) {
        int alpha = this.M.getAlpha();
        canvas.translate(f, f2);
        float f3 = alpha;
        this.M.setAlpha((int) (this.c0 * f3));
        this.a0.draw(canvas);
        this.M.setAlpha((int) (this.b0 * f3));
        int lineBaseline = this.a0.getLineBaseline(0);
        CharSequence charSequence = this.e0;
        float f4 = lineBaseline;
        canvas.drawText(charSequence, 0, charSequence.length(), Utils.FLOAT_EPSILON, f4, this.M);
        if (this.d) {
            return;
        }
        String trim = this.e0.toString().trim();
        if (trim.endsWith("…")) {
            trim = trim.substring(0, trim.length() - 1);
        }
        String str = trim;
        this.M.setAlpha(alpha);
        canvas.drawText(str, 0, Math.min(this.a0.getLineEnd(0), str.length()), Utils.FLOAT_EPSILON, f4, (Paint) this.M);
    }

    public void n0(Typeface typeface) {
        if (o0(typeface)) {
            U();
        }
    }

    public final void o() {
        if (this.G != null || this.h.isEmpty() || TextUtils.isEmpty(this.C)) {
            return;
        }
        h(Utils.FLOAT_EPSILON);
        int width = this.a0.getWidth();
        int height = this.a0.getHeight();
        if (width <= 0 || height <= 0) {
            return;
        }
        this.G = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        this.a0.draw(new Canvas(this.G));
        if (this.H == null) {
            this.H = new Paint(3);
        }
    }

    public final boolean o0(Typeface typeface) {
        lv lvVar = this.z;
        if (lvVar != null) {
            lvVar.c();
        }
        if (this.x != typeface) {
            this.x = typeface;
            return true;
        }
        return false;
    }

    public void p(RectF rectF, int i, int i2) {
        this.D = g(this.B);
        rectF.left = t(i, i2);
        rectF.top = this.i.top;
        rectF.right = u(rectF, i, i2);
        rectF.bottom = this.i.top + s();
    }

    public void p0(float f) {
        float a = x42.a(f, Utils.FLOAT_EPSILON, 1.0f);
        if (a != this.c) {
            this.c = a;
            d();
        }
    }

    public ColorStateList q() {
        return this.p;
    }

    public void q0(boolean z) {
        this.d = z;
    }

    public int r() {
        return this.l;
    }

    public void r0(float f) {
        this.e = f;
        this.f = f();
    }

    public float s() {
        L(this.N);
        return -this.N.ascent();
    }

    public void s0(int i) {
        this.i0 = i;
    }

    public final float t(int i, int i2) {
        if (i2 == 17 || (i2 & 7) == 1) {
            return (i / 2.0f) - (c() / 2.0f);
        }
        return ((i2 & 8388613) == 8388613 || (i2 & 5) == 5) ? this.D ? this.i.left : this.i.right - c() : this.D ? this.i.right - c() : this.i.left;
    }

    public final void t0(float f) {
        i(f);
        boolean z = j0 && this.I != 1.0f;
        this.F = z;
        if (z) {
            o();
        }
        ei4.j0(this.a);
    }

    public final float u(RectF rectF, int i, int i2) {
        if (i2 == 17 || (i2 & 7) == 1) {
            return (i / 2.0f) + (c() / 2.0f);
        }
        return ((i2 & 8388613) == 8388613 || (i2 & 5) == 5) ? this.D ? rectF.left + c() : this.i.right : this.D ? this.i.right : rectF.left + c();
    }

    public void u0(float f) {
        this.g0 = f;
    }

    public Typeface v() {
        Typeface typeface = this.w;
        return typeface != null ? typeface : Typeface.DEFAULT;
    }

    public void v0(float f) {
        this.h0 = f;
    }

    public int w() {
        return x(this.p);
    }

    public void w0(int i) {
        if (i != this.f0) {
            this.f0 = i;
            k();
            U();
        }
    }

    public final int x(ColorStateList colorStateList) {
        if (colorStateList == null) {
            return 0;
        }
        int[] iArr = this.K;
        if (iArr != null) {
            return colorStateList.getColorForState(iArr, 0);
        }
        return colorStateList.getDefaultColor();
    }

    public void x0(TimeInterpolator timeInterpolator) {
        this.O = timeInterpolator;
        U();
    }

    public final int y() {
        return x(this.o);
    }

    public void y0(boolean z) {
        this.E = z;
    }

    public float z() {
        M(this.N);
        return (-this.N.ascent()) + this.N.descent();
    }

    public final boolean z0(int[] iArr) {
        this.K = iArr;
        if (Q()) {
            U();
            return true;
        }
        return false;
    }
}
