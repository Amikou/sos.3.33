package com.google.android.material.internal;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

/* loaded from: classes2.dex */
public class ScrimInsetsFrameLayout extends FrameLayout {
    public Drawable a;
    public Rect f0;
    public Rect g0;
    public boolean h0;
    public boolean i0;

    /* loaded from: classes2.dex */
    public class a implements em2 {
        public a() {
        }

        @Override // defpackage.em2
        public jp4 a(View view, jp4 jp4Var) {
            ScrimInsetsFrameLayout scrimInsetsFrameLayout = ScrimInsetsFrameLayout.this;
            if (scrimInsetsFrameLayout.f0 == null) {
                scrimInsetsFrameLayout.f0 = new Rect();
            }
            ScrimInsetsFrameLayout.this.f0.set(jp4Var.k(), jp4Var.m(), jp4Var.l(), jp4Var.j());
            ScrimInsetsFrameLayout.this.a(jp4Var);
            ScrimInsetsFrameLayout.this.setWillNotDraw(!jp4Var.n() || ScrimInsetsFrameLayout.this.a == null);
            ei4.j0(ScrimInsetsFrameLayout.this);
            return jp4Var.c();
        }
    }

    public ScrimInsetsFrameLayout(Context context) {
        this(context, null);
    }

    public void a(jp4 jp4Var) {
    }

    @Override // android.view.View
    public void draw(Canvas canvas) {
        super.draw(canvas);
        int width = getWidth();
        int height = getHeight();
        if (this.f0 == null || this.a == null) {
            return;
        }
        int save = canvas.save();
        canvas.translate(getScrollX(), getScrollY());
        if (this.h0) {
            this.g0.set(0, 0, width, this.f0.top);
            this.a.setBounds(this.g0);
            this.a.draw(canvas);
        }
        if (this.i0) {
            this.g0.set(0, height - this.f0.bottom, width, height);
            this.a.setBounds(this.g0);
            this.a.draw(canvas);
        }
        Rect rect = this.g0;
        Rect rect2 = this.f0;
        rect.set(0, rect2.top, rect2.left, height - rect2.bottom);
        this.a.setBounds(this.g0);
        this.a.draw(canvas);
        Rect rect3 = this.g0;
        Rect rect4 = this.f0;
        rect3.set(width - rect4.right, rect4.top, width, height - rect4.bottom);
        this.a.setBounds(this.g0);
        this.a.draw(canvas);
        canvas.restoreToCount(save);
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Drawable drawable = this.a;
        if (drawable != null) {
            drawable.setCallback(this);
        }
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        Drawable drawable = this.a;
        if (drawable != null) {
            drawable.setCallback(null);
        }
    }

    public void setDrawBottomInsetForeground(boolean z) {
        this.i0 = z;
    }

    public void setDrawTopInsetForeground(boolean z) {
        this.h0 = z;
    }

    public void setScrimInsetForeground(Drawable drawable) {
        this.a = drawable;
    }

    public ScrimInsetsFrameLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ScrimInsetsFrameLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.g0 = new Rect();
        this.h0 = true;
        this.i0 = true;
        TypedArray h = a54.h(context, attributeSet, o23.ScrimInsetsFrameLayout, i, y13.Widget_Design_ScrimInsetsFrameLayout, new int[0]);
        this.a = h.getDrawable(o23.ScrimInsetsFrameLayout_insetForeground);
        h.recycle();
        setWillNotDraw(true);
        ei4.G0(this, new a());
    }
}
