package com.google.android.material.internal;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.CheckedTextView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import androidx.appcompat.view.menu.g;
import androidx.appcompat.view.menu.j;
import androidx.appcompat.widget.LinearLayoutCompat;

/* loaded from: classes2.dex */
public class NavigationMenuItemView extends ForegroundLinearLayout implements j.a {
    public static final int[] u0 = {16842912};
    public int k0;
    public boolean l0;
    public boolean m0;
    public final CheckedTextView n0;
    public FrameLayout o0;
    public g p0;
    public ColorStateList q0;
    public boolean r0;
    public Drawable s0;
    public final z5 t0;

    /* loaded from: classes2.dex */
    public class a extends z5 {
        public a() {
        }

        @Override // defpackage.z5
        public void g(View view, b6 b6Var) {
            super.g(view, b6Var);
            b6Var.a0(NavigationMenuItemView.this.m0);
        }
    }

    public NavigationMenuItemView(Context context) {
        this(context, null);
    }

    private void setActionView(View view) {
        if (view != null) {
            if (this.o0 == null) {
                this.o0 = (FrameLayout) ((ViewStub) findViewById(b03.design_menu_item_action_area_stub)).inflate();
            }
            this.o0.removeAllViews();
            this.o0.addView(view);
        }
    }

    @Override // androidx.appcompat.view.menu.j.a
    public boolean c() {
        return false;
    }

    @Override // androidx.appcompat.view.menu.j.a
    public void d(g gVar, int i) {
        this.p0 = gVar;
        if (gVar.getItemId() > 0) {
            setId(gVar.getItemId());
        }
        setVisibility(gVar.isVisible() ? 0 : 8);
        if (getBackground() == null) {
            ei4.w0(this, f());
        }
        setCheckable(gVar.isCheckable());
        setChecked(gVar.isChecked());
        setEnabled(gVar.isEnabled());
        setTitle(gVar.getTitle());
        setIcon(gVar.getIcon());
        setActionView(gVar.getActionView());
        setContentDescription(gVar.getContentDescription());
        j74.a(this, gVar.getTooltipText());
        e();
    }

    public final void e() {
        if (h()) {
            this.n0.setVisibility(8);
            FrameLayout frameLayout = this.o0;
            if (frameLayout != null) {
                LinearLayoutCompat.LayoutParams layoutParams = (LinearLayoutCompat.LayoutParams) frameLayout.getLayoutParams();
                ((LinearLayout.LayoutParams) layoutParams).width = -1;
                this.o0.setLayoutParams(layoutParams);
                return;
            }
            return;
        }
        this.n0.setVisibility(0);
        FrameLayout frameLayout2 = this.o0;
        if (frameLayout2 != null) {
            LinearLayoutCompat.LayoutParams layoutParams2 = (LinearLayoutCompat.LayoutParams) frameLayout2.getLayoutParams();
            ((LinearLayout.LayoutParams) layoutParams2).width = -2;
            this.o0.setLayoutParams(layoutParams2);
        }
    }

    public final StateListDrawable f() {
        TypedValue typedValue = new TypedValue();
        if (getContext().getTheme().resolveAttribute(jy2.colorControlHighlight, typedValue, true)) {
            StateListDrawable stateListDrawable = new StateListDrawable();
            stateListDrawable.addState(u0, new ColorDrawable(typedValue.data));
            stateListDrawable.addState(ViewGroup.EMPTY_STATE_SET, new ColorDrawable(0));
            return stateListDrawable;
        }
        return null;
    }

    public void g() {
        FrameLayout frameLayout = this.o0;
        if (frameLayout != null) {
            frameLayout.removeAllViews();
        }
        this.n0.setCompoundDrawables(null, null, null, null);
    }

    @Override // androidx.appcompat.view.menu.j.a
    public g getItemData() {
        return this.p0;
    }

    public final boolean h() {
        return this.p0.getTitle() == null && this.p0.getIcon() == null && this.p0.getActionView() != null;
    }

    @Override // android.view.ViewGroup, android.view.View
    public int[] onCreateDrawableState(int i) {
        int[] onCreateDrawableState = super.onCreateDrawableState(i + 1);
        g gVar = this.p0;
        if (gVar != null && gVar.isCheckable() && this.p0.isChecked()) {
            ViewGroup.mergeDrawableStates(onCreateDrawableState, u0);
        }
        return onCreateDrawableState;
    }

    public void setCheckable(boolean z) {
        refreshDrawableState();
        if (this.m0 != z) {
            this.m0 = z;
            this.t0.l(this.n0, 2048);
        }
    }

    public void setChecked(boolean z) {
        refreshDrawableState();
        this.n0.setChecked(z);
    }

    public void setHorizontalPadding(int i) {
        setPadding(i, 0, i, 0);
    }

    public void setIcon(Drawable drawable) {
        if (drawable != null) {
            if (this.r0) {
                Drawable.ConstantState constantState = drawable.getConstantState();
                if (constantState != null) {
                    drawable = constantState.newDrawable();
                }
                drawable = androidx.core.graphics.drawable.a.r(drawable).mutate();
                androidx.core.graphics.drawable.a.o(drawable, this.q0);
            }
            int i = this.k0;
            drawable.setBounds(0, 0, i, i);
        } else if (this.l0) {
            if (this.s0 == null) {
                Drawable f = g83.f(getResources(), qz2.navigation_empty_icon, getContext().getTheme());
                this.s0 = f;
                if (f != null) {
                    int i2 = this.k0;
                    f.setBounds(0, 0, i2, i2);
                }
            }
            drawable = this.s0;
        }
        t44.l(this.n0, drawable, null, null, null);
    }

    public void setIconPadding(int i) {
        this.n0.setCompoundDrawablePadding(i);
    }

    public void setIconSize(int i) {
        this.k0 = i;
    }

    public void setIconTintList(ColorStateList colorStateList) {
        this.q0 = colorStateList;
        this.r0 = colorStateList != null;
        g gVar = this.p0;
        if (gVar != null) {
            setIcon(gVar.getIcon());
        }
    }

    public void setMaxLines(int i) {
        this.n0.setMaxLines(i);
    }

    public void setNeedsEmptyIcon(boolean z) {
        this.l0 = z;
    }

    public void setShortcut(boolean z, char c) {
    }

    public void setTextAppearance(int i) {
        t44.q(this.n0, i);
    }

    public void setTextColor(ColorStateList colorStateList) {
        this.n0.setTextColor(colorStateList);
    }

    public void setTitle(CharSequence charSequence) {
        this.n0.setText(charSequence);
    }

    public NavigationMenuItemView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public NavigationMenuItemView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a aVar = new a();
        this.t0 = aVar;
        setOrientation(0);
        LayoutInflater.from(context).inflate(x03.design_navigation_menu_item, (ViewGroup) this, true);
        setIconSize(context.getResources().getDimensionPixelSize(jz2.design_navigation_icon_size));
        CheckedTextView checkedTextView = (CheckedTextView) findViewById(b03.design_menu_item_text);
        this.n0 = checkedTextView;
        checkedTextView.setDuplicateParentStateEnabled(true);
        ei4.t0(checkedTextView, aVar);
    }
}
