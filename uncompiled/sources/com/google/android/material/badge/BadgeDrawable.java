package com.google.android.material.badge;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import defpackage.i44;
import java.lang.ref.WeakReference;
import java.text.NumberFormat;
import org.slf4j.Marker;

/* loaded from: classes2.dex */
public class BadgeDrawable extends Drawable implements i44.b {
    public static final int u0 = y13.Widget_MaterialComponents_Badge;
    public static final int v0 = gy2.badgeStyle;
    public final WeakReference<Context> a;
    public final o42 f0;
    public final i44 g0;
    public final Rect h0;
    public final float i0;
    public final float j0;
    public final float k0;
    public final SavedState l0;
    public float m0;
    public float n0;
    public int o0;
    public float p0;
    public float q0;
    public float r0;
    public WeakReference<View> s0;
    public WeakReference<FrameLayout> t0;

    /* loaded from: classes2.dex */
    public class a implements Runnable {
        public final /* synthetic */ View a;
        public final /* synthetic */ FrameLayout f0;

        public a(View view, FrameLayout frameLayout) {
            this.a = view;
            this.f0 = frameLayout;
        }

        @Override // java.lang.Runnable
        public void run() {
            BadgeDrawable.this.F(this.a, this.f0);
        }
    }

    public BadgeDrawable(Context context) {
        this.a = new WeakReference<>(context);
        a54.c(context);
        Resources resources = context.getResources();
        this.h0 = new Rect();
        this.f0 = new o42();
        this.i0 = resources.getDimensionPixelSize(jz2.mtrl_badge_radius);
        this.k0 = resources.getDimensionPixelSize(jz2.mtrl_badge_long_text_horizontal_padding);
        this.j0 = resources.getDimensionPixelSize(jz2.mtrl_badge_with_text_radius);
        i44 i44Var = new i44(this);
        this.g0 = i44Var;
        i44Var.e().setTextAlign(Paint.Align.CENTER);
        this.l0 = new SavedState(context);
        A(y13.TextAppearance_MaterialComponents_Badge);
    }

    public static void E(View view) {
        ViewGroup viewGroup = (ViewGroup) view.getParent();
        viewGroup.setClipChildren(false);
        viewGroup.setClipToPadding(false);
    }

    public static BadgeDrawable c(Context context) {
        return d(context, null, v0, u0);
    }

    public static BadgeDrawable d(Context context, AttributeSet attributeSet, int i, int i2) {
        BadgeDrawable badgeDrawable = new BadgeDrawable(context);
        badgeDrawable.o(context, attributeSet, i, i2);
        return badgeDrawable;
    }

    public static BadgeDrawable e(Context context, SavedState savedState) {
        BadgeDrawable badgeDrawable = new BadgeDrawable(context);
        badgeDrawable.q(savedState);
        return badgeDrawable;
    }

    public static int p(Context context, TypedArray typedArray, int i) {
        return n42.b(context, typedArray, i).getDefaultColor();
    }

    public final void A(int i) {
        Context context = this.a.get();
        if (context == null) {
            return;
        }
        z(new d44(context, i));
    }

    public void B(int i) {
        this.l0.p0 = i;
        G();
    }

    public void C(boolean z) {
        setVisible(z, false);
        this.l0.n0 = z;
        if (!com.google.android.material.badge.a.a || i() == null || z) {
            return;
        }
        ((ViewGroup) i().getParent()).invalidate();
    }

    public final void D(View view) {
        ViewGroup viewGroup = (ViewGroup) view.getParent();
        if (viewGroup == null || viewGroup.getId() != b03.mtrl_anchor_parent) {
            WeakReference<FrameLayout> weakReference = this.t0;
            if (weakReference == null || weakReference.get() != viewGroup) {
                E(view);
                FrameLayout frameLayout = new FrameLayout(view.getContext());
                frameLayout.setId(b03.mtrl_anchor_parent);
                frameLayout.setClipChildren(false);
                frameLayout.setClipToPadding(false);
                frameLayout.setLayoutParams(view.getLayoutParams());
                frameLayout.setMinimumWidth(view.getWidth());
                frameLayout.setMinimumHeight(view.getHeight());
                int indexOfChild = viewGroup.indexOfChild(view);
                viewGroup.removeViewAt(indexOfChild);
                view.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
                frameLayout.addView(view);
                viewGroup.addView(frameLayout, indexOfChild);
                this.t0 = new WeakReference<>(frameLayout);
                frameLayout.post(new a(view, frameLayout));
            }
        }
    }

    public void F(View view, FrameLayout frameLayout) {
        this.s0 = new WeakReference<>(view);
        boolean z = com.google.android.material.badge.a.a;
        if (z && frameLayout == null) {
            D(view);
        } else {
            this.t0 = new WeakReference<>(frameLayout);
        }
        if (!z) {
            E(view);
        }
        G();
        invalidateSelf();
    }

    public final void G() {
        Context context = this.a.get();
        WeakReference<View> weakReference = this.s0;
        View view = weakReference != null ? weakReference.get() : null;
        if (context == null || view == null) {
            return;
        }
        Rect rect = new Rect();
        rect.set(this.h0);
        Rect rect2 = new Rect();
        view.getDrawingRect(rect2);
        WeakReference<FrameLayout> weakReference2 = this.t0;
        FrameLayout frameLayout = weakReference2 != null ? weakReference2.get() : null;
        if (frameLayout != null || com.google.android.material.badge.a.a) {
            if (frameLayout == null) {
                frameLayout = (ViewGroup) view.getParent();
            }
            frameLayout.offsetDescendantRectToMyCoords(view, rect2);
        }
        b(context, rect2, view);
        com.google.android.material.badge.a.f(this.h0, this.m0, this.n0, this.q0, this.r0);
        this.f0.X(this.p0);
        if (rect.equals(this.h0)) {
            return;
        }
        this.f0.setBounds(this.h0);
    }

    public final void H() {
        this.o0 = ((int) Math.pow(10.0d, k() - 1.0d)) - 1;
    }

    @Override // defpackage.i44.b
    public void a() {
        invalidateSelf();
    }

    public final void b(Context context, Rect rect, View view) {
        int i = this.l0.p0 + this.l0.r0;
        int i2 = this.l0.m0;
        if (i2 != 8388691 && i2 != 8388693) {
            this.n0 = rect.top + i;
        } else {
            this.n0 = rect.bottom - i;
        }
        if (l() <= 9) {
            float f = !n() ? this.i0 : this.j0;
            this.p0 = f;
            this.r0 = f;
            this.q0 = f;
        } else {
            float f2 = this.j0;
            this.p0 = f2;
            this.r0 = f2;
            this.q0 = (this.g0.f(g()) / 2.0f) + this.k0;
        }
        int dimensionPixelSize = context.getResources().getDimensionPixelSize(n() ? jz2.mtrl_badge_text_horizontal_edge_offset : jz2.mtrl_badge_horizontal_edge_offset);
        int i3 = this.l0.o0 + this.l0.q0;
        int i4 = this.l0.m0;
        if (i4 != 8388659 && i4 != 8388691) {
            this.m0 = ei4.E(view) == 0 ? ((rect.right + this.q0) - dimensionPixelSize) - i3 : (rect.left - this.q0) + dimensionPixelSize + i3;
        } else {
            this.m0 = ei4.E(view) == 0 ? (rect.left - this.q0) + dimensionPixelSize + i3 : ((rect.right + this.q0) - dimensionPixelSize) - i3;
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        if (getBounds().isEmpty() || getAlpha() == 0 || !isVisible()) {
            return;
        }
        this.f0.draw(canvas);
        if (n()) {
            f(canvas);
        }
    }

    public final void f(Canvas canvas) {
        Rect rect = new Rect();
        String g = g();
        this.g0.e().getTextBounds(g, 0, g.length(), rect);
        canvas.drawText(g, this.m0, this.n0 + (rect.height() / 2), this.g0.e());
    }

    public final String g() {
        if (l() <= this.o0) {
            return NumberFormat.getInstance().format(l());
        }
        Context context = this.a.get();
        return context == null ? "" : context.getString(r13.mtrl_exceed_max_badge_number_suffix, Integer.valueOf(this.o0), Marker.ANY_NON_NULL_MARKER);
    }

    @Override // android.graphics.drawable.Drawable
    public int getAlpha() {
        return this.l0.g0;
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicHeight() {
        return this.h0.height();
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicWidth() {
        return this.h0.width();
    }

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        return -3;
    }

    public CharSequence h() {
        Context context;
        if (isVisible()) {
            if (n()) {
                if (this.l0.k0 <= 0 || (context = this.a.get()) == null) {
                    return null;
                }
                if (l() <= this.o0) {
                    return context.getResources().getQuantityString(this.l0.k0, l(), Integer.valueOf(l()));
                }
                return context.getString(this.l0.l0, Integer.valueOf(this.o0));
            }
            return this.l0.j0;
        }
        return null;
    }

    public FrameLayout i() {
        WeakReference<FrameLayout> weakReference = this.t0;
        if (weakReference != null) {
            return weakReference.get();
        }
        return null;
    }

    @Override // android.graphics.drawable.Drawable
    public boolean isStateful() {
        return false;
    }

    public int j() {
        return this.l0.o0;
    }

    public int k() {
        return this.l0.i0;
    }

    public int l() {
        if (n()) {
            return this.l0.h0;
        }
        return 0;
    }

    public SavedState m() {
        return this.l0;
    }

    public boolean n() {
        return this.l0.h0 != -1;
    }

    public final void o(Context context, AttributeSet attributeSet, int i, int i2) {
        TypedArray h = a54.h(context, attributeSet, o23.Badge, i, i2, new int[0]);
        x(h.getInt(o23.Badge_maxCharacterCount, 4));
        int i3 = o23.Badge_number;
        if (h.hasValue(i3)) {
            y(h.getInt(i3, 0));
        }
        t(p(context, h, o23.Badge_backgroundColor));
        int i4 = o23.Badge_badgeTextColor;
        if (h.hasValue(i4)) {
            v(p(context, h, i4));
        }
        u(h.getInt(o23.Badge_badgeGravity, 8388661));
        w(h.getDimensionPixelOffset(o23.Badge_horizontalOffset, 0));
        B(h.getDimensionPixelOffset(o23.Badge_verticalOffset, 0));
        h.recycle();
    }

    @Override // android.graphics.drawable.Drawable, defpackage.i44.b
    public boolean onStateChange(int[] iArr) {
        return super.onStateChange(iArr);
    }

    public final void q(SavedState savedState) {
        x(savedState.i0);
        if (savedState.h0 != -1) {
            y(savedState.h0);
        }
        t(savedState.a);
        v(savedState.f0);
        u(savedState.m0);
        w(savedState.o0);
        B(savedState.p0);
        r(savedState.q0);
        s(savedState.r0);
        C(savedState.n0);
    }

    public void r(int i) {
        this.l0.q0 = i;
        G();
    }

    public void s(int i) {
        this.l0.r0 = i;
        G();
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
        this.l0.g0 = i;
        this.g0.e().setAlpha(i);
        invalidateSelf();
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
    }

    public void t(int i) {
        this.l0.a = i;
        ColorStateList valueOf = ColorStateList.valueOf(i);
        if (this.f0.x() != valueOf) {
            this.f0.a0(valueOf);
            invalidateSelf();
        }
    }

    public void u(int i) {
        if (this.l0.m0 != i) {
            this.l0.m0 = i;
            WeakReference<View> weakReference = this.s0;
            if (weakReference == null || weakReference.get() == null) {
                return;
            }
            View view = this.s0.get();
            WeakReference<FrameLayout> weakReference2 = this.t0;
            F(view, weakReference2 != null ? weakReference2.get() : null);
        }
    }

    public void v(int i) {
        this.l0.f0 = i;
        if (this.g0.e().getColor() != i) {
            this.g0.e().setColor(i);
            invalidateSelf();
        }
    }

    public void w(int i) {
        this.l0.o0 = i;
        G();
    }

    public void x(int i) {
        if (this.l0.i0 != i) {
            this.l0.i0 = i;
            H();
            this.g0.i(true);
            G();
            invalidateSelf();
        }
    }

    public void y(int i) {
        int max = Math.max(0, i);
        if (this.l0.h0 != max) {
            this.l0.h0 = max;
            this.g0.i(true);
            G();
            invalidateSelf();
        }
    }

    public final void z(d44 d44Var) {
        Context context;
        if (this.g0.d() == d44Var || (context = this.a.get()) == null) {
            return;
        }
        this.g0.h(d44Var, context);
        G();
    }

    /* loaded from: classes2.dex */
    public static final class SavedState implements Parcelable {
        public static final Parcelable.Creator<SavedState> CREATOR = new a();
        public int a;
        public int f0;
        public int g0;
        public int h0;
        public int i0;
        public CharSequence j0;
        public int k0;
        public int l0;
        public int m0;
        public boolean n0;
        public int o0;
        public int p0;
        public int q0;
        public int r0;

        /* loaded from: classes2.dex */
        public static class a implements Parcelable.Creator<SavedState> {
            @Override // android.os.Parcelable.Creator
            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            @Override // android.os.Parcelable.Creator
            /* renamed from: b */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        }

        public SavedState(Context context) {
            this.g0 = 255;
            this.h0 = -1;
            this.f0 = new d44(context, y13.TextAppearance_MaterialComponents_Badge).a.getDefaultColor();
            this.j0 = context.getString(r13.mtrl_badge_numberless_content_description);
            this.k0 = g13.mtrl_badge_content_description;
            this.l0 = r13.mtrl_exceed_max_badge_number_content_description;
            this.n0 = true;
        }

        @Override // android.os.Parcelable
        public int describeContents() {
            return 0;
        }

        @Override // android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.a);
            parcel.writeInt(this.f0);
            parcel.writeInt(this.g0);
            parcel.writeInt(this.h0);
            parcel.writeInt(this.i0);
            parcel.writeString(this.j0.toString());
            parcel.writeInt(this.k0);
            parcel.writeInt(this.m0);
            parcel.writeInt(this.o0);
            parcel.writeInt(this.p0);
            parcel.writeInt(this.q0);
            parcel.writeInt(this.r0);
            parcel.writeInt(this.n0 ? 1 : 0);
        }

        public SavedState(Parcel parcel) {
            this.g0 = 255;
            this.h0 = -1;
            this.a = parcel.readInt();
            this.f0 = parcel.readInt();
            this.g0 = parcel.readInt();
            this.h0 = parcel.readInt();
            this.i0 = parcel.readInt();
            this.j0 = parcel.readString();
            this.k0 = parcel.readInt();
            this.m0 = parcel.readInt();
            this.o0 = parcel.readInt();
            this.p0 = parcel.readInt();
            this.q0 = parcel.readInt();
            this.r0 = parcel.readInt();
            this.n0 = parcel.readInt() != 0;
        }
    }
}
