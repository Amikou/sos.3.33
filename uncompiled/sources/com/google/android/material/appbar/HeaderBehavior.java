package com.google.android.material.appbar;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.OverScroller;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

/* JADX INFO: Access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public abstract class HeaderBehavior<V extends View> extends ViewOffsetBehavior<V> {
    public Runnable a;
    public OverScroller b;
    public boolean c;
    public int d;
    public int e;
    public int f;
    public VelocityTracker g;

    /* loaded from: classes2.dex */
    public class a implements Runnable {
        public final CoordinatorLayout a;
        public final V f0;

        public a(CoordinatorLayout coordinatorLayout, V v) {
            this.a = coordinatorLayout;
            this.f0 = v;
        }

        @Override // java.lang.Runnable
        public void run() {
            OverScroller overScroller;
            if (this.f0 == null || (overScroller = HeaderBehavior.this.b) == null) {
                return;
            }
            if (overScroller.computeScrollOffset()) {
                HeaderBehavior headerBehavior = HeaderBehavior.this;
                headerBehavior.i(this.a, this.f0, headerBehavior.b.getCurrY());
                ei4.l0(this.f0, this);
                return;
            }
            HeaderBehavior.this.g(this.a, this.f0);
        }
    }

    public HeaderBehavior() {
        this.d = -1;
        this.f = -1;
    }

    public boolean a(V v) {
        return false;
    }

    public final void b() {
        if (this.g == null) {
            this.g = VelocityTracker.obtain();
        }
    }

    public final boolean c(CoordinatorLayout coordinatorLayout, V v, int i, int i2, float f) {
        Runnable runnable = this.a;
        if (runnable != null) {
            v.removeCallbacks(runnable);
            this.a = null;
        }
        if (this.b == null) {
            this.b = new OverScroller(v.getContext());
        }
        this.b.fling(0, getTopAndBottomOffset(), 0, Math.round(f), 0, 0, i, i2);
        if (this.b.computeScrollOffset()) {
            a aVar = new a(coordinatorLayout, v);
            this.a = aVar;
            ei4.l0(v, aVar);
            return true;
        }
        g(coordinatorLayout, v);
        return false;
    }

    public int d(V v) {
        return -v.getHeight();
    }

    public int e(V v) {
        return v.getHeight();
    }

    public int f() {
        return getTopAndBottomOffset();
    }

    public void g(CoordinatorLayout coordinatorLayout, V v) {
    }

    public final int h(CoordinatorLayout coordinatorLayout, V v, int i, int i2, int i3) {
        return j(coordinatorLayout, v, f() - i, i2, i3);
    }

    public int i(CoordinatorLayout coordinatorLayout, V v, int i) {
        return j(coordinatorLayout, v, i, Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    public int j(CoordinatorLayout coordinatorLayout, V v, int i, int i2, int i3) {
        int b;
        int topAndBottomOffset = getTopAndBottomOffset();
        if (i2 == 0 || topAndBottomOffset < i2 || topAndBottomOffset > i3 || topAndBottomOffset == (b = x42.b(i, i2, i3))) {
            return 0;
        }
        setTopAndBottomOffset(b);
        return topAndBottomOffset - b;
    }

    @Override // androidx.coordinatorlayout.widget.CoordinatorLayout.Behavior
    public boolean onInterceptTouchEvent(CoordinatorLayout coordinatorLayout, V v, MotionEvent motionEvent) {
        int findPointerIndex;
        if (this.f < 0) {
            this.f = ViewConfiguration.get(coordinatorLayout.getContext()).getScaledTouchSlop();
        }
        if (motionEvent.getActionMasked() == 2 && this.c) {
            int i = this.d;
            if (i == -1 || (findPointerIndex = motionEvent.findPointerIndex(i)) == -1) {
                return false;
            }
            int y = (int) motionEvent.getY(findPointerIndex);
            if (Math.abs(y - this.e) > this.f) {
                this.e = y;
                return true;
            }
        }
        if (motionEvent.getActionMasked() == 0) {
            this.d = -1;
            int x = (int) motionEvent.getX();
            int y2 = (int) motionEvent.getY();
            boolean z = a(v) && coordinatorLayout.isPointInChildBounds(v, x, y2);
            this.c = z;
            if (z) {
                this.e = y2;
                this.d = motionEvent.getPointerId(0);
                b();
                OverScroller overScroller = this.b;
                if (overScroller != null && !overScroller.isFinished()) {
                    this.b.abortAnimation();
                    return true;
                }
            }
        }
        VelocityTracker velocityTracker = this.g;
        if (velocityTracker != null) {
            velocityTracker.addMovement(motionEvent);
        }
        return false;
    }

    /* JADX WARN: Removed duplicated region for block: B:27:0x007b  */
    /* JADX WARN: Removed duplicated region for block: B:30:0x0085  */
    /* JADX WARN: Removed duplicated region for block: B:33:0x008c A[ADDED_TO_REGION] */
    /* JADX WARN: Removed duplicated region for block: B:37:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    @Override // androidx.coordinatorlayout.widget.CoordinatorLayout.Behavior
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public boolean onTouchEvent(androidx.coordinatorlayout.widget.CoordinatorLayout r12, V r13, android.view.MotionEvent r14) {
        /*
            r11 = this;
            int r0 = r14.getActionMasked()
            r1 = -1
            r2 = 1
            r3 = 0
            if (r0 == r2) goto L4e
            r4 = 2
            if (r0 == r4) goto L2d
            r12 = 3
            if (r0 == r12) goto L72
            r12 = 6
            if (r0 == r12) goto L13
            goto L4c
        L13:
            int r12 = r14.getActionIndex()
            if (r12 != 0) goto L1b
            r12 = r2
            goto L1c
        L1b:
            r12 = r3
        L1c:
            int r13 = r14.getPointerId(r12)
            r11.d = r13
            float r12 = r14.getY(r12)
            r13 = 1056964608(0x3f000000, float:0.5)
            float r12 = r12 + r13
            int r12 = (int) r12
            r11.e = r12
            goto L4c
        L2d:
            int r0 = r11.d
            int r0 = r14.findPointerIndex(r0)
            if (r0 != r1) goto L36
            return r3
        L36:
            float r0 = r14.getY(r0)
            int r0 = (int) r0
            int r1 = r11.e
            int r7 = r1 - r0
            r11.e = r0
            int r8 = r11.d(r13)
            r9 = 0
            r4 = r11
            r5 = r12
            r6 = r13
            r4.h(r5, r6, r7, r8, r9)
        L4c:
            r12 = r3
            goto L81
        L4e:
            android.view.VelocityTracker r0 = r11.g
            if (r0 == 0) goto L72
            r0.addMovement(r14)
            android.view.VelocityTracker r0 = r11.g
            r4 = 1000(0x3e8, float:1.401E-42)
            r0.computeCurrentVelocity(r4)
            android.view.VelocityTracker r0 = r11.g
            int r4 = r11.d
            float r10 = r0.getYVelocity(r4)
            int r0 = r11.e(r13)
            int r8 = -r0
            r9 = 0
            r5 = r11
            r6 = r12
            r7 = r13
            r5.c(r6, r7, r8, r9, r10)
            r12 = r2
            goto L73
        L72:
            r12 = r3
        L73:
            r11.c = r3
            r11.d = r1
            android.view.VelocityTracker r13 = r11.g
            if (r13 == 0) goto L81
            r13.recycle()
            r13 = 0
            r11.g = r13
        L81:
            android.view.VelocityTracker r13 = r11.g
            if (r13 == 0) goto L88
            r13.addMovement(r14)
        L88:
            boolean r13 = r11.c
            if (r13 != 0) goto L90
            if (r12 == 0) goto L8f
            goto L90
        L8f:
            r2 = r3
        L90:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.appbar.HeaderBehavior.onTouchEvent(androidx.coordinatorlayout.widget.CoordinatorLayout, android.view.View, android.view.MotionEvent):boolean");
    }

    public HeaderBehavior(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.d = -1;
        this.f = -1;
    }
}
