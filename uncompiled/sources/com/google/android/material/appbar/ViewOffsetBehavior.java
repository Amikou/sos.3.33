package com.google.android.material.appbar;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

/* loaded from: classes2.dex */
class ViewOffsetBehavior<V extends View> extends CoordinatorLayout.Behavior<V> {
    private int tempLeftRightOffset;
    private int tempTopBottomOffset;
    private jj4 viewOffsetHelper;

    public ViewOffsetBehavior() {
        this.tempTopBottomOffset = 0;
        this.tempLeftRightOffset = 0;
    }

    public int getLeftAndRightOffset() {
        jj4 jj4Var = this.viewOffsetHelper;
        if (jj4Var != null) {
            return jj4Var.c();
        }
        return 0;
    }

    public int getTopAndBottomOffset() {
        jj4 jj4Var = this.viewOffsetHelper;
        if (jj4Var != null) {
            return jj4Var.d();
        }
        return 0;
    }

    public boolean isHorizontalOffsetEnabled() {
        jj4 jj4Var = this.viewOffsetHelper;
        return jj4Var != null && jj4Var.e();
    }

    public boolean isVerticalOffsetEnabled() {
        jj4 jj4Var = this.viewOffsetHelper;
        return jj4Var != null && jj4Var.f();
    }

    public void layoutChild(CoordinatorLayout coordinatorLayout, V v, int i) {
        coordinatorLayout.onLayoutChild(v, i);
    }

    @Override // androidx.coordinatorlayout.widget.CoordinatorLayout.Behavior
    public boolean onLayoutChild(CoordinatorLayout coordinatorLayout, V v, int i) {
        layoutChild(coordinatorLayout, v, i);
        if (this.viewOffsetHelper == null) {
            this.viewOffsetHelper = new jj4(v);
        }
        this.viewOffsetHelper.g();
        this.viewOffsetHelper.a();
        int i2 = this.tempTopBottomOffset;
        if (i2 != 0) {
            this.viewOffsetHelper.j(i2);
            this.tempTopBottomOffset = 0;
        }
        int i3 = this.tempLeftRightOffset;
        if (i3 != 0) {
            this.viewOffsetHelper.i(i3);
            this.tempLeftRightOffset = 0;
            return true;
        }
        return true;
    }

    public void setHorizontalOffsetEnabled(boolean z) {
        jj4 jj4Var = this.viewOffsetHelper;
        if (jj4Var != null) {
            jj4Var.h(z);
        }
    }

    public boolean setLeftAndRightOffset(int i) {
        jj4 jj4Var = this.viewOffsetHelper;
        if (jj4Var != null) {
            return jj4Var.i(i);
        }
        this.tempLeftRightOffset = i;
        return false;
    }

    public boolean setTopAndBottomOffset(int i) {
        jj4 jj4Var = this.viewOffsetHelper;
        if (jj4Var != null) {
            return jj4Var.j(i);
        }
        this.tempTopBottomOffset = i;
        return false;
    }

    public void setVerticalOffsetEnabled(boolean z) {
        jj4 jj4Var = this.viewOffsetHelper;
        if (jj4Var != null) {
            jj4Var.k(z);
        }
    }

    public ViewOffsetBehavior(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.tempTopBottomOffset = 0;
        this.tempLeftRightOffset = 0;
    }
}
