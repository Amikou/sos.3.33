package com.google.android.material.appbar;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;
import androidx.appcompat.widget.Toolbar;
import com.google.android.material.appbar.AppBarLayout;

/* loaded from: classes2.dex */
public class CollapsingToolbarLayout extends FrameLayout {
    public static final int H0 = y13.Widget_Design_CollapsingToolbar;
    public int A0;
    public int B0;
    public jp4 C0;
    public int D0;
    public boolean E0;
    public int F0;
    public boolean G0;
    public boolean a;
    public int f0;
    public ViewGroup g0;
    public View h0;
    public View i0;
    public int j0;
    public int k0;
    public int l0;
    public int m0;
    public final Rect n0;
    public final com.google.android.material.internal.a o0;
    public final lu0 p0;
    public boolean q0;
    public boolean r0;
    public Drawable s0;
    public Drawable t0;
    public int u0;
    public boolean v0;
    public ValueAnimator w0;
    public long x0;
    public int y0;
    public AppBarLayout.d z0;

    /* loaded from: classes2.dex */
    public class a implements em2 {
        public a() {
        }

        @Override // defpackage.em2
        public jp4 a(View view, jp4 jp4Var) {
            return CollapsingToolbarLayout.this.n(jp4Var);
        }
    }

    /* loaded from: classes2.dex */
    public class b implements ValueAnimator.AnimatorUpdateListener {
        public b() {
        }

        @Override // android.animation.ValueAnimator.AnimatorUpdateListener
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            CollapsingToolbarLayout.this.setScrimAlpha(((Integer) valueAnimator.getAnimatedValue()).intValue());
        }
    }

    /* loaded from: classes2.dex */
    public class c implements AppBarLayout.d {
        public c() {
        }

        @Override // com.google.android.material.appbar.AppBarLayout.c
        public void a(AppBarLayout appBarLayout, int i) {
            int height;
            CollapsingToolbarLayout collapsingToolbarLayout = CollapsingToolbarLayout.this;
            collapsingToolbarLayout.A0 = i;
            jp4 jp4Var = collapsingToolbarLayout.C0;
            int m = jp4Var != null ? jp4Var.m() : 0;
            int childCount = CollapsingToolbarLayout.this.getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                View childAt = CollapsingToolbarLayout.this.getChildAt(i2);
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                jj4 j = CollapsingToolbarLayout.j(childAt);
                int i3 = layoutParams.a;
                if (i3 == 1) {
                    j.j(x42.b(-i, 0, CollapsingToolbarLayout.this.h(childAt)));
                } else if (i3 == 2) {
                    j.j(Math.round((-i) * layoutParams.b));
                }
            }
            CollapsingToolbarLayout.this.t();
            CollapsingToolbarLayout collapsingToolbarLayout2 = CollapsingToolbarLayout.this;
            if (collapsingToolbarLayout2.t0 != null && m > 0) {
                ei4.j0(collapsingToolbarLayout2);
            }
            int height2 = (CollapsingToolbarLayout.this.getHeight() - ei4.F(CollapsingToolbarLayout.this)) - m;
            float f = height2;
            CollapsingToolbarLayout.this.o0.r0(Math.min(1.0f, (height - CollapsingToolbarLayout.this.getScrimVisibleHeightTrigger()) / f));
            CollapsingToolbarLayout collapsingToolbarLayout3 = CollapsingToolbarLayout.this;
            collapsingToolbarLayout3.o0.f0(collapsingToolbarLayout3.A0 + height2);
            CollapsingToolbarLayout.this.o0.p0(Math.abs(i) / f);
        }
    }

    public CollapsingToolbarLayout(Context context) {
        this(context, null);
    }

    public static int g(View view) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
            return view.getMeasuredHeight() + marginLayoutParams.topMargin + marginLayoutParams.bottomMargin;
        }
        return view.getMeasuredHeight();
    }

    public static CharSequence i(View view) {
        if (view instanceof Toolbar) {
            return ((Toolbar) view).getTitle();
        }
        if (Build.VERSION.SDK_INT < 21 || !(view instanceof android.widget.Toolbar)) {
            return null;
        }
        return ((android.widget.Toolbar) view).getTitle();
    }

    public static jj4 j(View view) {
        int i = b03.view_offset_helper;
        jj4 jj4Var = (jj4) view.getTag(i);
        if (jj4Var == null) {
            jj4 jj4Var2 = new jj4(view);
            view.setTag(i, jj4Var2);
            return jj4Var2;
        }
        return jj4Var;
    }

    public static boolean l(View view) {
        return (view instanceof Toolbar) || (Build.VERSION.SDK_INT >= 21 && (view instanceof android.widget.Toolbar));
    }

    public final void a(int i) {
        c();
        ValueAnimator valueAnimator = this.w0;
        if (valueAnimator == null) {
            ValueAnimator valueAnimator2 = new ValueAnimator();
            this.w0 = valueAnimator2;
            valueAnimator2.setDuration(this.x0);
            this.w0.setInterpolator(i > this.u0 ? ne.c : ne.d);
            this.w0.addUpdateListener(new b());
        } else if (valueAnimator.isRunning()) {
            this.w0.cancel();
        }
        this.w0.setIntValues(this.u0, i);
        this.w0.start();
    }

    public final void b(AppBarLayout appBarLayout) {
        if (k()) {
            appBarLayout.setLiftOnScroll(false);
        }
    }

    public final void c() {
        if (this.a) {
            ViewGroup viewGroup = null;
            this.g0 = null;
            this.h0 = null;
            int i = this.f0;
            if (i != -1) {
                ViewGroup viewGroup2 = (ViewGroup) findViewById(i);
                this.g0 = viewGroup2;
                if (viewGroup2 != null) {
                    this.h0 = d(viewGroup2);
                }
            }
            if (this.g0 == null) {
                int childCount = getChildCount();
                int i2 = 0;
                while (true) {
                    if (i2 >= childCount) {
                        break;
                    }
                    View childAt = getChildAt(i2);
                    if (l(childAt)) {
                        viewGroup = (ViewGroup) childAt;
                        break;
                    }
                    i2++;
                }
                this.g0 = viewGroup;
            }
            s();
            this.a = false;
        }
    }

    @Override // android.widget.FrameLayout, android.view.ViewGroup
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams;
    }

    public final View d(View view) {
        for (ViewParent parent = view.getParent(); parent != this && parent != null; parent = parent.getParent()) {
            if (parent instanceof View) {
                view = (View) parent;
            }
        }
        return view;
    }

    @Override // android.view.View
    public void draw(Canvas canvas) {
        Drawable drawable;
        super.draw(canvas);
        c();
        if (this.g0 == null && (drawable = this.s0) != null && this.u0 > 0) {
            drawable.mutate().setAlpha(this.u0);
            this.s0.draw(canvas);
        }
        if (this.q0 && this.r0) {
            if (this.g0 != null && this.s0 != null && this.u0 > 0 && k() && this.o0.D() < this.o0.E()) {
                int save = canvas.save();
                canvas.clipRect(this.s0.getBounds(), Region.Op.DIFFERENCE);
                this.o0.m(canvas);
                canvas.restoreToCount(save);
            } else {
                this.o0.m(canvas);
            }
        }
        if (this.t0 == null || this.u0 <= 0) {
            return;
        }
        jp4 jp4Var = this.C0;
        int m = jp4Var != null ? jp4Var.m() : 0;
        if (m > 0) {
            this.t0.setBounds(0, -this.A0, getWidth(), m - this.A0);
            this.t0.mutate().setAlpha(this.u0);
            this.t0.draw(canvas);
        }
    }

    @Override // android.view.ViewGroup
    public boolean drawChild(Canvas canvas, View view, long j) {
        boolean z;
        if (this.s0 == null || this.u0 <= 0 || !m(view)) {
            z = false;
        } else {
            r(this.s0, view, getWidth(), getHeight());
            this.s0.mutate().setAlpha(this.u0);
            this.s0.draw(canvas);
            z = true;
        }
        return super.drawChild(canvas, view, j) || z;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void drawableStateChanged() {
        super.drawableStateChanged();
        int[] drawableState = getDrawableState();
        Drawable drawable = this.t0;
        boolean z = false;
        if (drawable != null && drawable.isStateful()) {
            z = false | drawable.setState(drawableState);
        }
        Drawable drawable2 = this.s0;
        if (drawable2 != null && drawable2.isStateful()) {
            z |= drawable2.setState(drawableState);
        }
        com.google.android.material.internal.a aVar = this.o0;
        if (aVar != null) {
            z |= aVar.z0(drawableState);
        }
        if (z) {
            invalidate();
        }
    }

    @Override // android.widget.FrameLayout, android.view.ViewGroup
    /* renamed from: e */
    public LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(-1, -1);
    }

    @Override // android.widget.FrameLayout, android.view.ViewGroup
    /* renamed from: f */
    public FrameLayout.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return new LayoutParams(layoutParams);
    }

    public int getCollapsedTitleGravity() {
        return this.o0.r();
    }

    public Typeface getCollapsedTitleTypeface() {
        return this.o0.v();
    }

    public Drawable getContentScrim() {
        return this.s0;
    }

    public int getExpandedTitleGravity() {
        return this.o0.A();
    }

    public int getExpandedTitleMarginBottom() {
        return this.m0;
    }

    public int getExpandedTitleMarginEnd() {
        return this.l0;
    }

    public int getExpandedTitleMarginStart() {
        return this.j0;
    }

    public int getExpandedTitleMarginTop() {
        return this.k0;
    }

    public Typeface getExpandedTitleTypeface() {
        return this.o0.C();
    }

    public int getHyphenationFrequency() {
        return this.o0.F();
    }

    public int getLineCount() {
        return this.o0.G();
    }

    public float getLineSpacingAdd() {
        return this.o0.H();
    }

    public float getLineSpacingMultiplier() {
        return this.o0.I();
    }

    public int getMaxLines() {
        return this.o0.J();
    }

    public int getScrimAlpha() {
        return this.u0;
    }

    public long getScrimAnimationDuration() {
        return this.x0;
    }

    public int getScrimVisibleHeightTrigger() {
        int i = this.y0;
        if (i >= 0) {
            return i + this.D0 + this.F0;
        }
        jp4 jp4Var = this.C0;
        int m = jp4Var != null ? jp4Var.m() : 0;
        int F = ei4.F(this);
        if (F > 0) {
            return Math.min((F * 2) + m, getHeight());
        }
        return getHeight() / 3;
    }

    public Drawable getStatusBarScrim() {
        return this.t0;
    }

    public CharSequence getTitle() {
        if (this.q0) {
            return this.o0.K();
        }
        return null;
    }

    public int getTitleCollapseMode() {
        return this.B0;
    }

    public final int h(View view) {
        return ((getHeight() - j(view).b()) - view.getHeight()) - ((FrameLayout.LayoutParams) ((LayoutParams) view.getLayoutParams())).bottomMargin;
    }

    public final boolean k() {
        return this.B0 == 1;
    }

    public final boolean m(View view) {
        View view2 = this.h0;
        if (view2 == null || view2 == this) {
            if (view == this.g0) {
                return true;
            }
        } else if (view == view2) {
            return true;
        }
        return false;
    }

    public jp4 n(jp4 jp4Var) {
        jp4 jp4Var2 = ei4.B(this) ? jp4Var : null;
        if (!sl2.a(this.C0, jp4Var2)) {
            this.C0 = jp4Var2;
            requestLayout();
        }
        return jp4Var.c();
    }

    public final void o(boolean z) {
        int i;
        int i2;
        int i3;
        View view = this.h0;
        if (view == null) {
            view = this.g0;
        }
        int h = h(view);
        mm0.a(this, this.i0, this.n0);
        ViewGroup viewGroup = this.g0;
        int i4 = 0;
        if (viewGroup instanceof Toolbar) {
            Toolbar toolbar = (Toolbar) viewGroup;
            i4 = toolbar.getTitleMarginStart();
            i2 = toolbar.getTitleMarginEnd();
            i3 = toolbar.getTitleMarginTop();
            i = toolbar.getTitleMarginBottom();
        } else if (Build.VERSION.SDK_INT < 24 || !(viewGroup instanceof android.widget.Toolbar)) {
            i = 0;
            i2 = 0;
            i3 = 0;
        } else {
            android.widget.Toolbar toolbar2 = (android.widget.Toolbar) viewGroup;
            i4 = toolbar2.getTitleMarginStart();
            i2 = toolbar2.getTitleMarginEnd();
            i3 = toolbar2.getTitleMarginTop();
            i = toolbar2.getTitleMarginBottom();
        }
        com.google.android.material.internal.a aVar = this.o0;
        Rect rect = this.n0;
        int i5 = rect.left + (z ? i2 : i4);
        int i6 = rect.top + h + i3;
        int i7 = rect.right;
        if (!z) {
            i4 = i2;
        }
        aVar.X(i5, i6, i7 - i4, (rect.bottom + h) - i);
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        ViewParent parent = getParent();
        if (parent instanceof AppBarLayout) {
            AppBarLayout appBarLayout = (AppBarLayout) parent;
            b(appBarLayout);
            ei4.B0(this, ei4.B(appBarLayout));
            if (this.z0 == null) {
                this.z0 = new c();
            }
            appBarLayout.b(this.z0);
            ei4.q0(this);
        }
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        ViewParent parent = getParent();
        AppBarLayout.d dVar = this.z0;
        if (dVar != null && (parent instanceof AppBarLayout)) {
            ((AppBarLayout) parent).p(dVar);
        }
        super.onDetachedFromWindow();
    }

    @Override // android.widget.FrameLayout, android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        jp4 jp4Var = this.C0;
        if (jp4Var != null) {
            int m = jp4Var.m();
            int childCount = getChildCount();
            for (int i5 = 0; i5 < childCount; i5++) {
                View childAt = getChildAt(i5);
                if (!ei4.B(childAt) && childAt.getTop() < m) {
                    ei4.d0(childAt, m);
                }
            }
        }
        int childCount2 = getChildCount();
        for (int i6 = 0; i6 < childCount2; i6++) {
            j(getChildAt(i6)).g();
        }
        u(i, i2, i3, i4, false);
        v();
        t();
        int childCount3 = getChildCount();
        for (int i7 = 0; i7 < childCount3; i7++) {
            j(getChildAt(i7)).a();
        }
    }

    @Override // android.widget.FrameLayout, android.view.View
    public void onMeasure(int i, int i2) {
        c();
        super.onMeasure(i, i2);
        int mode = View.MeasureSpec.getMode(i2);
        jp4 jp4Var = this.C0;
        int m = jp4Var != null ? jp4Var.m() : 0;
        if ((mode == 0 || this.E0) && m > 0) {
            this.D0 = m;
            super.onMeasure(i, View.MeasureSpec.makeMeasureSpec(getMeasuredHeight() + m, 1073741824));
        }
        if (this.G0 && this.o0.J() > 1) {
            v();
            u(0, 0, getMeasuredWidth(), getMeasuredHeight(), true);
            int G = this.o0.G();
            if (G > 1) {
                this.F0 = Math.round(this.o0.z()) * (G - 1);
                super.onMeasure(i, View.MeasureSpec.makeMeasureSpec(getMeasuredHeight() + this.F0, 1073741824));
            }
        }
        ViewGroup viewGroup = this.g0;
        if (viewGroup != null) {
            View view = this.h0;
            if (view != null && view != this) {
                setMinimumHeight(g(view));
            } else {
                setMinimumHeight(g(viewGroup));
            }
        }
    }

    @Override // android.view.View
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        Drawable drawable = this.s0;
        if (drawable != null) {
            q(drawable, i, i2);
        }
    }

    public final void p() {
        setContentDescription(getTitle());
    }

    public final void q(Drawable drawable, int i, int i2) {
        r(drawable, this.g0, i, i2);
    }

    public final void r(Drawable drawable, View view, int i, int i2) {
        if (k() && view != null && this.q0) {
            i2 = view.getBottom();
        }
        drawable.setBounds(0, 0, i, i2);
    }

    public final void s() {
        View view;
        if (!this.q0 && (view = this.i0) != null) {
            ViewParent parent = view.getParent();
            if (parent instanceof ViewGroup) {
                ((ViewGroup) parent).removeView(this.i0);
            }
        }
        if (!this.q0 || this.g0 == null) {
            return;
        }
        if (this.i0 == null) {
            this.i0 = new View(getContext());
        }
        if (this.i0.getParent() == null) {
            this.g0.addView(this.i0, -1, -1);
        }
    }

    public void setCollapsedTitleGravity(int i) {
        this.o0.c0(i);
    }

    public void setCollapsedTitleTextAppearance(int i) {
        this.o0.Z(i);
    }

    public void setCollapsedTitleTextColor(int i) {
        setCollapsedTitleTextColor(ColorStateList.valueOf(i));
    }

    public void setCollapsedTitleTypeface(Typeface typeface) {
        this.o0.d0(typeface);
    }

    public void setContentScrim(Drawable drawable) {
        Drawable drawable2 = this.s0;
        if (drawable2 != drawable) {
            if (drawable2 != null) {
                drawable2.setCallback(null);
            }
            Drawable mutate = drawable != null ? drawable.mutate() : null;
            this.s0 = mutate;
            if (mutate != null) {
                q(mutate, getWidth(), getHeight());
                this.s0.setCallback(this);
                this.s0.setAlpha(this.u0);
            }
            ei4.j0(this);
        }
    }

    public void setContentScrimColor(int i) {
        setContentScrim(new ColorDrawable(i));
    }

    public void setContentScrimResource(int i) {
        setContentScrim(m70.f(getContext(), i));
    }

    public void setExpandedTitleColor(int i) {
        setExpandedTitleTextColor(ColorStateList.valueOf(i));
    }

    public void setExpandedTitleGravity(int i) {
        this.o0.l0(i);
    }

    public void setExpandedTitleMargin(int i, int i2, int i3, int i4) {
        this.j0 = i;
        this.k0 = i2;
        this.l0 = i3;
        this.m0 = i4;
        requestLayout();
    }

    public void setExpandedTitleMarginBottom(int i) {
        this.m0 = i;
        requestLayout();
    }

    public void setExpandedTitleMarginEnd(int i) {
        this.l0 = i;
        requestLayout();
    }

    public void setExpandedTitleMarginStart(int i) {
        this.j0 = i;
        requestLayout();
    }

    public void setExpandedTitleMarginTop(int i) {
        this.k0 = i;
        requestLayout();
    }

    public void setExpandedTitleTextAppearance(int i) {
        this.o0.i0(i);
    }

    public void setExpandedTitleTextColor(ColorStateList colorStateList) {
        this.o0.k0(colorStateList);
    }

    public void setExpandedTitleTypeface(Typeface typeface) {
        this.o0.n0(typeface);
    }

    public void setExtraMultilineHeightEnabled(boolean z) {
        this.G0 = z;
    }

    public void setForceApplySystemWindowInsetTop(boolean z) {
        this.E0 = z;
    }

    public void setHyphenationFrequency(int i) {
        this.o0.s0(i);
    }

    public void setLineSpacingAdd(float f) {
        this.o0.u0(f);
    }

    public void setLineSpacingMultiplier(float f) {
        this.o0.v0(f);
    }

    public void setMaxLines(int i) {
        this.o0.w0(i);
    }

    public void setRtlTextDirectionHeuristicsEnabled(boolean z) {
        this.o0.y0(z);
    }

    public void setScrimAlpha(int i) {
        ViewGroup viewGroup;
        if (i != this.u0) {
            if (this.s0 != null && (viewGroup = this.g0) != null) {
                ei4.j0(viewGroup);
            }
            this.u0 = i;
            ei4.j0(this);
        }
    }

    public void setScrimAnimationDuration(long j) {
        this.x0 = j;
    }

    public void setScrimVisibleHeightTrigger(int i) {
        if (this.y0 != i) {
            this.y0 = i;
            t();
        }
    }

    public void setScrimsShown(boolean z) {
        setScrimsShown(z, ei4.W(this) && !isInEditMode());
    }

    public void setStatusBarScrim(Drawable drawable) {
        Drawable drawable2 = this.t0;
        if (drawable2 != drawable) {
            if (drawable2 != null) {
                drawable2.setCallback(null);
            }
            Drawable mutate = drawable != null ? drawable.mutate() : null;
            this.t0 = mutate;
            if (mutate != null) {
                if (mutate.isStateful()) {
                    this.t0.setState(getDrawableState());
                }
                androidx.core.graphics.drawable.a.m(this.t0, ei4.E(this));
                this.t0.setVisible(getVisibility() == 0, false);
                this.t0.setCallback(this);
                this.t0.setAlpha(this.u0);
            }
            ei4.j0(this);
        }
    }

    public void setStatusBarScrimColor(int i) {
        setStatusBarScrim(new ColorDrawable(i));
    }

    public void setStatusBarScrimResource(int i) {
        setStatusBarScrim(m70.f(getContext(), i));
    }

    public void setTitle(CharSequence charSequence) {
        this.o0.A0(charSequence);
        p();
    }

    public void setTitleCollapseMode(int i) {
        this.B0 = i;
        boolean k = k();
        this.o0.q0(k);
        ViewParent parent = getParent();
        if (parent instanceof AppBarLayout) {
            b((AppBarLayout) parent);
        }
        if (k && this.s0 == null) {
            setContentScrimColor(this.p0.d(getResources().getDimension(jz2.design_appbar_elevation)));
        }
    }

    public void setTitleEnabled(boolean z) {
        if (z != this.q0) {
            this.q0 = z;
            p();
            s();
            requestLayout();
        }
    }

    @Override // android.view.View
    public void setVisibility(int i) {
        super.setVisibility(i);
        boolean z = i == 0;
        Drawable drawable = this.t0;
        if (drawable != null && drawable.isVisible() != z) {
            this.t0.setVisible(z, false);
        }
        Drawable drawable2 = this.s0;
        if (drawable2 == null || drawable2.isVisible() == z) {
            return;
        }
        this.s0.setVisible(z, false);
    }

    public final void t() {
        if (this.s0 == null && this.t0 == null) {
            return;
        }
        setScrimsShown(getHeight() + this.A0 < getScrimVisibleHeightTrigger());
    }

    public final void u(int i, int i2, int i3, int i4, boolean z) {
        View view;
        if (!this.q0 || (view = this.i0) == null) {
            return;
        }
        boolean z2 = ei4.V(view) && this.i0.getVisibility() == 0;
        this.r0 = z2;
        if (z2 || z) {
            boolean z3 = ei4.E(this) == 1;
            o(z3);
            this.o0.g0(z3 ? this.l0 : this.j0, this.n0.top + this.k0, (i3 - i) - (z3 ? this.j0 : this.l0), (i4 - i2) - this.m0);
            this.o0.V(z);
        }
    }

    public final void v() {
        if (this.g0 != null && this.q0 && TextUtils.isEmpty(this.o0.K())) {
            setTitle(i(this.g0));
        }
    }

    @Override // android.view.View
    public boolean verifyDrawable(Drawable drawable) {
        return super.verifyDrawable(drawable) || drawable == this.s0 || drawable == this.t0;
    }

    public CollapsingToolbarLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, gy2.collapsingToolbarLayoutStyle);
    }

    public void setCollapsedTitleTextColor(ColorStateList colorStateList) {
        this.o0.b0(colorStateList);
    }

    public void setScrimsShown(boolean z, boolean z2) {
        if (this.v0 != z) {
            if (z2) {
                a(z ? 255 : 0);
            } else {
                setScrimAlpha(z ? 255 : 0);
            }
            this.v0 = z;
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public CollapsingToolbarLayout(android.content.Context r10, android.util.AttributeSet r11, int r12) {
        /*
            Method dump skipped, instructions count: 314
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.appbar.CollapsingToolbarLayout.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }

    @Override // android.widget.FrameLayout, android.view.ViewGroup
    public FrameLayout.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(getContext(), attributeSet);
    }

    /* loaded from: classes2.dex */
    public static class LayoutParams extends FrameLayout.LayoutParams {
        public int a;
        public float b;

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            this.a = 0;
            this.b = 0.5f;
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, o23.CollapsingToolbarLayout_Layout);
            this.a = obtainStyledAttributes.getInt(o23.CollapsingToolbarLayout_Layout_layout_collapseMode, 0);
            a(obtainStyledAttributes.getFloat(o23.CollapsingToolbarLayout_Layout_layout_collapseParallaxMultiplier, 0.5f));
            obtainStyledAttributes.recycle();
        }

        public void a(float f) {
            this.b = f;
        }

        public LayoutParams(int i, int i2) {
            super(i, i2);
            this.a = 0;
            this.b = 0.5f;
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
            this.a = 0;
            this.b = 0.5f;
        }
    }
}
