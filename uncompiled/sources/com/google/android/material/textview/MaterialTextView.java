package com.google.android.material.textview;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatTextView;

/* loaded from: classes2.dex */
public class MaterialTextView extends AppCompatTextView {
    public MaterialTextView(Context context) {
        this(context, null);
    }

    public static boolean f(Context context) {
        return i42.b(context, gy2.textAppearanceLineHeightEnabled, true);
    }

    public static int g(Resources.Theme theme, AttributeSet attributeSet, int i, int i2) {
        TypedArray obtainStyledAttributes = theme.obtainStyledAttributes(attributeSet, o23.MaterialTextView, i, i2);
        int resourceId = obtainStyledAttributes.getResourceId(o23.MaterialTextView_android_textAppearance, -1);
        obtainStyledAttributes.recycle();
        return resourceId;
    }

    public static int h(Context context, TypedArray typedArray, int... iArr) {
        int i = -1;
        for (int i2 = 0; i2 < iArr.length && i < 0; i2++) {
            i = n42.c(context, typedArray, iArr[i2], -1);
        }
        return i;
    }

    public static boolean i(Context context, Resources.Theme theme, AttributeSet attributeSet, int i, int i2) {
        TypedArray obtainStyledAttributes = theme.obtainStyledAttributes(attributeSet, o23.MaterialTextView, i, i2);
        int h = h(context, obtainStyledAttributes, o23.MaterialTextView_android_lineHeight, o23.MaterialTextView_lineHeight);
        obtainStyledAttributes.recycle();
        return h != -1;
    }

    public final void e(Resources.Theme theme, int i) {
        TypedArray obtainStyledAttributes = theme.obtainStyledAttributes(i, o23.MaterialTextAppearance);
        int h = h(getContext(), obtainStyledAttributes, o23.MaterialTextAppearance_android_lineHeight, o23.MaterialTextAppearance_lineHeight);
        obtainStyledAttributes.recycle();
        if (h >= 0) {
            setLineHeight(h);
        }
    }

    @Override // androidx.appcompat.widget.AppCompatTextView, android.widget.TextView
    public void setTextAppearance(Context context, int i) {
        super.setTextAppearance(context, i);
        if (f(context)) {
            e(context.getTheme(), i);
        }
    }

    public MaterialTextView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 16842884);
    }

    public MaterialTextView(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, 0);
    }

    public MaterialTextView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(r42.c(context, attributeSet, i, i2), attributeSet, i);
        int g;
        Context context2 = getContext();
        if (f(context2)) {
            Resources.Theme theme = context2.getTheme();
            if (i(context2, theme, attributeSet, i, i2) || (g = g(theme, attributeSet, i, i2)) == -1) {
                return;
            }
            e(theme, g);
        }
    }
}
