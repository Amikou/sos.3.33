package com.google.android.material.behavior;

import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.github.mikephil.charting.utils.Utils;
import defpackage.b6;
import defpackage.e6;
import defpackage.ji4;

/* loaded from: classes2.dex */
public class SwipeDismissBehavior<V extends View> extends CoordinatorLayout.Behavior<V> {
    public ji4 a;
    public c b;
    public boolean c;
    public boolean e;
    public float d = Utils.FLOAT_EPSILON;
    public int f = 2;
    public float g = 0.5f;
    public float h = Utils.FLOAT_EPSILON;
    public float i = 0.5f;
    public final ji4.c j = new a();

    /* loaded from: classes2.dex */
    public class a extends ji4.c {
        public int a;
        public int b = -1;

        public a() {
        }

        @Override // defpackage.ji4.c
        public int a(View view, int i, int i2) {
            int width;
            int width2;
            int width3;
            boolean z = ei4.E(view) == 1;
            int i3 = SwipeDismissBehavior.this.f;
            if (i3 == 0) {
                if (z) {
                    width = this.a - view.getWidth();
                    width2 = this.a;
                } else {
                    width = this.a;
                    width3 = view.getWidth();
                    width2 = width3 + width;
                }
            } else if (i3 != 1) {
                width = this.a - view.getWidth();
                width2 = view.getWidth() + this.a;
            } else if (z) {
                width = this.a;
                width3 = view.getWidth();
                width2 = width3 + width;
            } else {
                width = this.a - view.getWidth();
                width2 = this.a;
            }
            return SwipeDismissBehavior.c(width, i, width2);
        }

        @Override // defpackage.ji4.c
        public int b(View view, int i, int i2) {
            return view.getTop();
        }

        @Override // defpackage.ji4.c
        public int d(View view) {
            return view.getWidth();
        }

        @Override // defpackage.ji4.c
        public void i(View view, int i) {
            this.b = i;
            this.a = view.getLeft();
            ViewParent parent = view.getParent();
            if (parent != null) {
                parent.requestDisallowInterceptTouchEvent(true);
            }
        }

        @Override // defpackage.ji4.c
        public void j(int i) {
            c cVar = SwipeDismissBehavior.this.b;
            if (cVar != null) {
                cVar.b(i);
            }
        }

        @Override // defpackage.ji4.c
        public void k(View view, int i, int i2, int i3, int i4) {
            float width = this.a + (view.getWidth() * SwipeDismissBehavior.this.h);
            float width2 = this.a + (view.getWidth() * SwipeDismissBehavior.this.i);
            float f = i;
            if (f <= width) {
                view.setAlpha(1.0f);
            } else if (f >= width2) {
                view.setAlpha(Utils.FLOAT_EPSILON);
            } else {
                view.setAlpha(SwipeDismissBehavior.b(Utils.FLOAT_EPSILON, 1.0f - SwipeDismissBehavior.e(width, width2, f), 1.0f));
            }
        }

        @Override // defpackage.ji4.c
        public void l(View view, float f, float f2) {
            int i;
            boolean z;
            c cVar;
            this.b = -1;
            int width = view.getWidth();
            if (n(view, f)) {
                int left = view.getLeft();
                int i2 = this.a;
                i = left < i2 ? i2 - width : i2 + width;
                z = true;
            } else {
                i = this.a;
                z = false;
            }
            if (SwipeDismissBehavior.this.a.P(i, view.getTop())) {
                ei4.l0(view, new d(view, z));
            } else if (!z || (cVar = SwipeDismissBehavior.this.b) == null) {
            } else {
                cVar.a(view);
            }
        }

        @Override // defpackage.ji4.c
        public boolean m(View view, int i) {
            int i2 = this.b;
            return (i2 == -1 || i2 == i) && SwipeDismissBehavior.this.a(view);
        }

        public final boolean n(View view, float f) {
            int i = (f > Utils.FLOAT_EPSILON ? 1 : (f == Utils.FLOAT_EPSILON ? 0 : -1));
            if (i == 0) {
                return Math.abs(view.getLeft() - this.a) >= Math.round(((float) view.getWidth()) * SwipeDismissBehavior.this.g);
            }
            boolean z = ei4.E(view) == 1;
            int i2 = SwipeDismissBehavior.this.f;
            if (i2 == 2) {
                return true;
            }
            if (i2 == 0) {
                if (z) {
                    if (f >= Utils.FLOAT_EPSILON) {
                        return false;
                    }
                } else if (i <= 0) {
                    return false;
                }
                return true;
            } else if (i2 == 1) {
                if (z) {
                    if (i <= 0) {
                        return false;
                    }
                } else if (f >= Utils.FLOAT_EPSILON) {
                    return false;
                }
                return true;
            } else {
                return false;
            }
        }
    }

    /* loaded from: classes2.dex */
    public class b implements e6 {
        public b() {
        }

        @Override // defpackage.e6
        public boolean a(View view, e6.a aVar) {
            boolean z = false;
            if (SwipeDismissBehavior.this.a(view)) {
                boolean z2 = ei4.E(view) == 1;
                int i = SwipeDismissBehavior.this.f;
                if ((i == 0 && z2) || (i == 1 && !z2)) {
                    z = true;
                }
                int width = view.getWidth();
                if (z) {
                    width = -width;
                }
                ei4.c0(view, width);
                view.setAlpha(Utils.FLOAT_EPSILON);
                c cVar = SwipeDismissBehavior.this.b;
                if (cVar != null) {
                    cVar.a(view);
                }
                return true;
            }
            return false;
        }
    }

    /* loaded from: classes2.dex */
    public interface c {
        void a(View view);

        void b(int i);
    }

    /* loaded from: classes2.dex */
    public class d implements Runnable {
        public final View a;
        public final boolean f0;

        public d(View view, boolean z) {
            this.a = view;
            this.f0 = z;
        }

        @Override // java.lang.Runnable
        public void run() {
            c cVar;
            ji4 ji4Var = SwipeDismissBehavior.this.a;
            if (ji4Var != null && ji4Var.n(true)) {
                ei4.l0(this.a, this);
            } else if (!this.f0 || (cVar = SwipeDismissBehavior.this.b) == null) {
            } else {
                cVar.a(this.a);
            }
        }
    }

    public static float b(float f, float f2, float f3) {
        return Math.min(Math.max(f, f2), f3);
    }

    public static int c(int i, int i2, int i3) {
        return Math.min(Math.max(i, i2), i3);
    }

    public static float e(float f, float f2, float f3) {
        return (f3 - f) / (f2 - f);
    }

    public boolean a(View view) {
        return true;
    }

    public final void d(ViewGroup viewGroup) {
        ji4 p;
        if (this.a == null) {
            if (this.e) {
                p = ji4.o(viewGroup, this.d, this.j);
            } else {
                p = ji4.p(viewGroup, this.j);
            }
            this.a = p;
        }
    }

    public void f(float f) {
        this.i = b(Utils.FLOAT_EPSILON, f, 1.0f);
    }

    public void g(c cVar) {
        this.b = cVar;
    }

    public void h(float f) {
        this.h = b(Utils.FLOAT_EPSILON, f, 1.0f);
    }

    public void i(int i) {
        this.f = i;
    }

    public final void j(View view) {
        ei4.n0(view, 1048576);
        if (a(view)) {
            ei4.p0(view, b6.a.l, null, new b());
        }
    }

    @Override // androidx.coordinatorlayout.widget.CoordinatorLayout.Behavior
    public boolean onInterceptTouchEvent(CoordinatorLayout coordinatorLayout, V v, MotionEvent motionEvent) {
        boolean z = this.c;
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 0) {
            z = coordinatorLayout.isPointInChildBounds(v, (int) motionEvent.getX(), (int) motionEvent.getY());
            this.c = z;
        } else if (actionMasked == 1 || actionMasked == 3) {
            this.c = false;
        }
        if (z) {
            d(coordinatorLayout);
            return this.a.Q(motionEvent);
        }
        return false;
    }

    @Override // androidx.coordinatorlayout.widget.CoordinatorLayout.Behavior
    public boolean onLayoutChild(CoordinatorLayout coordinatorLayout, V v, int i) {
        boolean onLayoutChild = super.onLayoutChild(coordinatorLayout, v, i);
        if (ei4.C(v) == 0) {
            ei4.D0(v, 1);
            j(v);
        }
        return onLayoutChild;
    }

    @Override // androidx.coordinatorlayout.widget.CoordinatorLayout.Behavior
    public boolean onTouchEvent(CoordinatorLayout coordinatorLayout, V v, MotionEvent motionEvent) {
        ji4 ji4Var = this.a;
        if (ji4Var != null) {
            ji4Var.G(motionEvent);
            return true;
        }
        return false;
    }
}
