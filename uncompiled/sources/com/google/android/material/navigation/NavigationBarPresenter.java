package com.google.android.material.navigation;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.appcompat.view.menu.e;
import androidx.appcompat.view.menu.g;
import androidx.appcompat.view.menu.i;
import androidx.appcompat.view.menu.l;
import com.google.android.material.badge.a;
import com.google.android.material.internal.ParcelableSparseArray;

/* loaded from: classes2.dex */
public class NavigationBarPresenter implements i {
    public e a;
    public NavigationBarMenuView f0;
    public boolean g0 = false;
    public int h0;

    /* loaded from: classes2.dex */
    public static class SavedState implements Parcelable {
        public static final Parcelable.Creator<SavedState> CREATOR = new a();
        public int a;
        public ParcelableSparseArray f0;

        /* loaded from: classes2.dex */
        public static class a implements Parcelable.Creator<SavedState> {
            @Override // android.os.Parcelable.Creator
            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            @Override // android.os.Parcelable.Creator
            /* renamed from: b */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        }

        public SavedState() {
        }

        @Override // android.os.Parcelable
        public int describeContents() {
            return 0;
        }

        @Override // android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.a);
            parcel.writeParcelable(this.f0, 0);
        }

        public SavedState(Parcel parcel) {
            this.a = parcel.readInt();
            this.f0 = (ParcelableSparseArray) parcel.readParcelable(getClass().getClassLoader());
        }
    }

    public void a(int i) {
        this.h0 = i;
    }

    public void b(NavigationBarMenuView navigationBarMenuView) {
        this.f0 = navigationBarMenuView;
    }

    @Override // androidx.appcompat.view.menu.i
    public void c(e eVar, boolean z) {
    }

    @Override // androidx.appcompat.view.menu.i
    public void d(boolean z) {
        if (this.g0) {
            return;
        }
        if (z) {
            this.f0.d();
        } else {
            this.f0.k();
        }
    }

    @Override // androidx.appcompat.view.menu.i
    public boolean e() {
        return false;
    }

    @Override // androidx.appcompat.view.menu.i
    public boolean f(e eVar, g gVar) {
        return false;
    }

    @Override // androidx.appcompat.view.menu.i
    public boolean g(e eVar, g gVar) {
        return false;
    }

    @Override // androidx.appcompat.view.menu.i
    public int getId() {
        return this.h0;
    }

    @Override // androidx.appcompat.view.menu.i
    public void i(Context context, e eVar) {
        this.a = eVar;
        this.f0.b(eVar);
    }

    @Override // androidx.appcompat.view.menu.i
    public void j(Parcelable parcelable) {
        if (parcelable instanceof SavedState) {
            SavedState savedState = (SavedState) parcelable;
            this.f0.j(savedState.a);
            this.f0.setBadgeDrawables(a.b(this.f0.getContext(), savedState.f0));
        }
    }

    public void k(boolean z) {
        this.g0 = z;
    }

    @Override // androidx.appcompat.view.menu.i
    public boolean l(l lVar) {
        return false;
    }

    @Override // androidx.appcompat.view.menu.i
    public Parcelable m() {
        SavedState savedState = new SavedState();
        savedState.a = this.f0.getSelectedItemId();
        savedState.f0 = a.c(this.f0.getBadgeDrawables());
        return savedState;
    }
}
