package com.google.android.material.navigation;

import android.app.Activity;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import androidx.appcompat.view.menu.e;
import androidx.appcompat.view.menu.g;
import androidx.customview.view.AbsSavedState;
import com.google.android.material.internal.ScrimInsetsFrameLayout;

/* loaded from: classes2.dex */
public class NavigationView extends ScrimInsetsFrameLayout {
    public static final int[] q0 = {16842912};
    public static final int[] r0 = {-16842910};
    public static final int s0 = y13.Widget_Design_NavigationView;
    public final ke2 j0;
    public final le2 k0;
    public c l0;
    public final int m0;
    public final int[] n0;
    public MenuInflater o0;
    public ViewTreeObserver.OnGlobalLayoutListener p0;

    /* loaded from: classes2.dex */
    public class a implements e.a {
        public a() {
        }

        @Override // androidx.appcompat.view.menu.e.a
        public boolean a(e eVar, MenuItem menuItem) {
            c cVar = NavigationView.this.l0;
            return cVar != null && cVar.a(menuItem);
        }

        @Override // androidx.appcompat.view.menu.e.a
        public void b(e eVar) {
        }
    }

    /* loaded from: classes2.dex */
    public class b implements ViewTreeObserver.OnGlobalLayoutListener {
        public b() {
        }

        @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
        public void onGlobalLayout() {
            NavigationView navigationView = NavigationView.this;
            navigationView.getLocationOnScreen(navigationView.n0);
            boolean z = true;
            boolean z2 = NavigationView.this.n0[1] == 0;
            NavigationView.this.k0.x(z2);
            NavigationView.this.setDrawTopInsetForeground(z2);
            Activity a = p70.a(NavigationView.this.getContext());
            if (a == null || Build.VERSION.SDK_INT < 21) {
                return;
            }
            boolean z3 = a.findViewById(16908290).getHeight() == NavigationView.this.getHeight();
            boolean z4 = Color.alpha(a.getWindow().getNavigationBarColor()) != 0;
            NavigationView navigationView2 = NavigationView.this;
            if (!z3 || !z4) {
                z = false;
            }
            navigationView2.setDrawBottomInsetForeground(z);
        }
    }

    /* loaded from: classes2.dex */
    public interface c {
        boolean a(MenuItem menuItem);
    }

    public NavigationView(Context context) {
        this(context, null);
    }

    private MenuInflater getMenuInflater() {
        if (this.o0 == null) {
            this.o0 = new kw3(getContext());
        }
        return this.o0;
    }

    @Override // com.google.android.material.internal.ScrimInsetsFrameLayout
    public void a(jp4 jp4Var) {
        this.k0.k(jp4Var);
    }

    public final ColorStateList d(int i) {
        TypedValue typedValue = new TypedValue();
        if (getContext().getTheme().resolveAttribute(i, typedValue, true)) {
            ColorStateList c2 = mf.c(getContext(), typedValue.resourceId);
            if (getContext().getTheme().resolveAttribute(jy2.colorPrimary, typedValue, true)) {
                int i2 = typedValue.data;
                int defaultColor = c2.getDefaultColor();
                int[] iArr = r0;
                return new ColorStateList(new int[][]{iArr, q0, FrameLayout.EMPTY_STATE_SET}, new int[]{c2.getColorForState(iArr, defaultColor), i2, defaultColor});
            }
            return null;
        }
        return null;
    }

    public final Drawable e(l64 l64Var) {
        o42 o42Var = new o42(pn3.b(getContext(), l64Var.n(o23.NavigationView_itemShapeAppearance, 0), l64Var.n(o23.NavigationView_itemShapeAppearanceOverlay, 0)).m());
        o42Var.a0(n42.a(getContext(), l64Var, o23.NavigationView_itemShapeFillColor));
        return new InsetDrawable((Drawable) o42Var, l64Var.f(o23.NavigationView_itemShapeInsetStart, 0), l64Var.f(o23.NavigationView_itemShapeInsetTop, 0), l64Var.f(o23.NavigationView_itemShapeInsetEnd, 0), l64Var.f(o23.NavigationView_itemShapeInsetBottom, 0));
    }

    public final boolean f(l64 l64Var) {
        return l64Var.s(o23.NavigationView_itemShapeAppearance) || l64Var.s(o23.NavigationView_itemShapeAppearanceOverlay);
    }

    public View g(int i) {
        return this.k0.w(i);
    }

    public MenuItem getCheckedItem() {
        return this.k0.n();
    }

    public int getHeaderCount() {
        return this.k0.o();
    }

    public Drawable getItemBackground() {
        return this.k0.p();
    }

    public int getItemHorizontalPadding() {
        return this.k0.q();
    }

    public int getItemIconPadding() {
        return this.k0.r();
    }

    public ColorStateList getItemIconTintList() {
        return this.k0.u();
    }

    public int getItemMaxLines() {
        return this.k0.s();
    }

    public ColorStateList getItemTextColor() {
        return this.k0.t();
    }

    public Menu getMenu() {
        return this.j0;
    }

    public void h(int i) {
        this.k0.J(true);
        getMenuInflater().inflate(i, this.j0);
        this.k0.J(false);
        this.k0.d(false);
    }

    public final void i() {
        this.p0 = new b();
        getViewTreeObserver().addOnGlobalLayoutListener(this.p0);
    }

    @Override // com.google.android.material.internal.ScrimInsetsFrameLayout, android.view.ViewGroup, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        p42.e(this);
    }

    @Override // com.google.android.material.internal.ScrimInsetsFrameLayout, android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (Build.VERSION.SDK_INT < 16) {
            getViewTreeObserver().removeGlobalOnLayoutListener(this.p0);
        } else {
            getViewTreeObserver().removeOnGlobalLayoutListener(this.p0);
        }
    }

    @Override // android.widget.FrameLayout, android.view.View
    public void onMeasure(int i, int i2) {
        int mode = View.MeasureSpec.getMode(i);
        if (mode == Integer.MIN_VALUE) {
            i = View.MeasureSpec.makeMeasureSpec(Math.min(View.MeasureSpec.getSize(i), this.m0), 1073741824);
        } else if (mode == 0) {
            i = View.MeasureSpec.makeMeasureSpec(this.m0, 1073741824);
        }
        super.onMeasure(i, i2);
    }

    @Override // android.view.View
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.a());
        this.j0.S(savedState.g0);
    }

    @Override // android.view.View
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        Bundle bundle = new Bundle();
        savedState.g0 = bundle;
        this.j0.U(bundle);
        return savedState;
    }

    public void setCheckedItem(int i) {
        MenuItem findItem = this.j0.findItem(i);
        if (findItem != null) {
            this.k0.y((g) findItem);
        }
    }

    @Override // android.view.View
    public void setElevation(float f) {
        if (Build.VERSION.SDK_INT >= 21) {
            super.setElevation(f);
        }
        p42.d(this, f);
    }

    public void setItemBackground(Drawable drawable) {
        this.k0.A(drawable);
    }

    public void setItemBackgroundResource(int i) {
        setItemBackground(m70.f(getContext(), i));
    }

    public void setItemHorizontalPadding(int i) {
        this.k0.B(i);
    }

    public void setItemHorizontalPaddingResource(int i) {
        this.k0.B(getResources().getDimensionPixelSize(i));
    }

    public void setItemIconPadding(int i) {
        this.k0.C(i);
    }

    public void setItemIconPaddingResource(int i) {
        this.k0.C(getResources().getDimensionPixelSize(i));
    }

    public void setItemIconSize(int i) {
        this.k0.D(i);
    }

    public void setItemIconTintList(ColorStateList colorStateList) {
        this.k0.E(colorStateList);
    }

    public void setItemMaxLines(int i) {
        this.k0.F(i);
    }

    public void setItemTextAppearance(int i) {
        this.k0.G(i);
    }

    public void setItemTextColor(ColorStateList colorStateList) {
        this.k0.H(colorStateList);
    }

    public void setNavigationItemSelectedListener(c cVar) {
        this.l0 = cVar;
    }

    @Override // android.view.View
    public void setOverScrollMode(int i) {
        super.setOverScrollMode(i);
        le2 le2Var = this.k0;
        if (le2Var != null) {
            le2Var.I(i);
        }
    }

    /* loaded from: classes2.dex */
    public static class SavedState extends AbsSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new a();
        public Bundle g0;

        /* loaded from: classes2.dex */
        public static class a implements Parcelable.ClassLoaderCreator<SavedState> {
            @Override // android.os.Parcelable.Creator
            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel, null);
            }

            @Override // android.os.Parcelable.ClassLoaderCreator
            /* renamed from: b */
            public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new SavedState(parcel, classLoader);
            }

            @Override // android.os.Parcelable.Creator
            /* renamed from: c */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        }

        public SavedState(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            this.g0 = parcel.readBundle(classLoader);
        }

        @Override // androidx.customview.view.AbsSavedState, android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeBundle(this.g0);
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }
    }

    public NavigationView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, gy2.navigationViewStyle);
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public NavigationView(android.content.Context r11, android.util.AttributeSet r12, int r13) {
        /*
            Method dump skipped, instructions count: 358
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.navigation.NavigationView.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }

    public void setCheckedItem(MenuItem menuItem) {
        MenuItem findItem = this.j0.findItem(menuItem.getItemId());
        if (findItem != null) {
            this.k0.y((g) findItem);
            return;
        }
        throw new IllegalArgumentException("Called setCheckedItem(MenuItem) with an item that is not in the current menu.");
    }
}
