package com.google.android.material.navigation;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.view.menu.g;
import androidx.appcompat.view.menu.j;
import androidx.media3.common.PlaybackException;
import com.google.android.material.badge.BadgeDrawable;
import defpackage.b6;

/* loaded from: classes2.dex */
public abstract class NavigationBarItemView extends FrameLayout implements j.a {
    public static final int[] u0 = {16842912};
    public final int a;
    public float f0;
    public float g0;
    public float h0;
    public int i0;
    public boolean j0;
    public ImageView k0;
    public final ViewGroup l0;
    public final TextView m0;
    public final TextView n0;
    public int o0;
    public g p0;
    public ColorStateList q0;
    public Drawable r0;
    public Drawable s0;
    public BadgeDrawable t0;

    /* loaded from: classes2.dex */
    public class a implements View.OnLayoutChangeListener {
        public a() {
        }

        @Override // android.view.View.OnLayoutChangeListener
        public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
            if (NavigationBarItemView.this.k0.getVisibility() == 0) {
                NavigationBarItemView navigationBarItemView = NavigationBarItemView.this;
                navigationBarItemView.m(navigationBarItemView.k0);
            }
        }
    }

    public NavigationBarItemView(Context context) {
        super(context);
        this.o0 = -1;
        LayoutInflater.from(context).inflate(getItemLayoutResId(), (ViewGroup) this, true);
        this.k0 = (ImageView) findViewById(b03.navigation_bar_item_icon_view);
        ViewGroup viewGroup = (ViewGroup) findViewById(b03.navigation_bar_item_labels_group);
        this.l0 = viewGroup;
        TextView textView = (TextView) findViewById(b03.navigation_bar_item_small_label_view);
        this.m0 = textView;
        TextView textView2 = (TextView) findViewById(b03.navigation_bar_item_large_label_view);
        this.n0 = textView2;
        setBackgroundResource(getItemBackgroundResId());
        this.a = getResources().getDimensionPixelSize(getItemDefaultMarginResId());
        viewGroup.setTag(b03.mtrl_view_tag_bottom_padding, Integer.valueOf(viewGroup.getPaddingBottom()));
        ei4.D0(textView, 2);
        ei4.D0(textView2, 2);
        setFocusable(true);
        e(textView.getTextSize(), textView2.getTextSize());
        ImageView imageView = this.k0;
        if (imageView != null) {
            imageView.addOnLayoutChangeListener(new a());
        }
    }

    private int getItemVisiblePosition() {
        ViewGroup viewGroup = (ViewGroup) getParent();
        int indexOfChild = viewGroup.indexOfChild(this);
        int i = 0;
        for (int i2 = 0; i2 < indexOfChild; i2++) {
            View childAt = viewGroup.getChildAt(i2);
            if ((childAt instanceof NavigationBarItemView) && childAt.getVisibility() == 0) {
                i++;
            }
        }
        return i;
    }

    private int getSuggestedIconHeight() {
        BadgeDrawable badgeDrawable = this.t0;
        int minimumHeight = badgeDrawable != null ? badgeDrawable.getMinimumHeight() / 2 : 0;
        return Math.max(minimumHeight, ((FrameLayout.LayoutParams) this.k0.getLayoutParams()).topMargin) + this.k0.getMeasuredWidth() + minimumHeight;
    }

    private int getSuggestedIconWidth() {
        BadgeDrawable badgeDrawable = this.t0;
        int minimumWidth = badgeDrawable == null ? 0 : badgeDrawable.getMinimumWidth() - this.t0.j();
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) this.k0.getLayoutParams();
        return Math.max(minimumWidth, layoutParams.leftMargin) + this.k0.getMeasuredWidth() + Math.max(minimumWidth, layoutParams.rightMargin);
    }

    public static void i(View view, int i, int i2) {
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) view.getLayoutParams();
        layoutParams.topMargin = i;
        layoutParams.gravity = i2;
        view.setLayoutParams(layoutParams);
    }

    public static void j(View view, float f, float f2, int i) {
        view.setScaleX(f);
        view.setScaleY(f2);
        view.setVisibility(i);
    }

    public static void n(View view, int i) {
        view.setPadding(view.getPaddingLeft(), view.getPaddingTop(), view.getPaddingRight(), i);
    }

    @Override // androidx.appcompat.view.menu.j.a
    public boolean c() {
        return false;
    }

    @Override // androidx.appcompat.view.menu.j.a
    public void d(g gVar, int i) {
        CharSequence title;
        this.p0 = gVar;
        setCheckable(gVar.isCheckable());
        setChecked(gVar.isChecked());
        setEnabled(gVar.isEnabled());
        setIcon(gVar.getIcon());
        setTitle(gVar.getTitle());
        setId(gVar.getItemId());
        if (!TextUtils.isEmpty(gVar.getContentDescription())) {
            setContentDescription(gVar.getContentDescription());
        }
        if (!TextUtils.isEmpty(gVar.getTooltipText())) {
            title = gVar.getTooltipText();
        } else {
            title = gVar.getTitle();
        }
        int i2 = Build.VERSION.SDK_INT;
        if (i2 < 21 || i2 > 23) {
            j74.a(this, title);
        }
        setVisibility(gVar.isVisible() ? 0 : 8);
    }

    public final void e(float f, float f2) {
        this.f0 = f - f2;
        this.g0 = (f2 * 1.0f) / f;
        this.h0 = (f * 1.0f) / f2;
    }

    public final FrameLayout f(View view) {
        ImageView imageView = this.k0;
        if (view == imageView && com.google.android.material.badge.a.a) {
            return (FrameLayout) imageView.getParent();
        }
        return null;
    }

    public final boolean g() {
        return this.t0 != null;
    }

    public BadgeDrawable getBadge() {
        return this.t0;
    }

    public int getItemBackgroundResId() {
        return qz2.mtrl_navigation_bar_item_background;
    }

    @Override // androidx.appcompat.view.menu.j.a
    public g getItemData() {
        return this.p0;
    }

    public int getItemDefaultMarginResId() {
        return jz2.mtrl_navigation_bar_item_default_margin;
    }

    public abstract int getItemLayoutResId();

    public int getItemPosition() {
        return this.o0;
    }

    @Override // android.view.View
    public int getSuggestedMinimumHeight() {
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) this.l0.getLayoutParams();
        return getSuggestedIconHeight() + layoutParams.topMargin + this.l0.getMeasuredHeight() + layoutParams.bottomMargin;
    }

    @Override // android.view.View
    public int getSuggestedMinimumWidth() {
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) this.l0.getLayoutParams();
        return Math.max(getSuggestedIconWidth(), layoutParams.leftMargin + this.l0.getMeasuredWidth() + layoutParams.rightMargin);
    }

    public void h() {
        l(this.k0);
    }

    public final void k(View view) {
        if (g() && view != null) {
            setClipChildren(false);
            setClipToPadding(false);
            com.google.android.material.badge.a.a(this.t0, view, f(view));
        }
    }

    public final void l(View view) {
        if (g()) {
            if (view != null) {
                setClipChildren(true);
                setClipToPadding(true);
                com.google.android.material.badge.a.d(this.t0, view);
            }
            this.t0 = null;
        }
    }

    public final void m(View view) {
        if (g()) {
            com.google.android.material.badge.a.e(this.t0, view, f(view));
        }
    }

    @Override // android.view.ViewGroup, android.view.View
    public int[] onCreateDrawableState(int i) {
        int[] onCreateDrawableState = super.onCreateDrawableState(i + 1);
        g gVar = this.p0;
        if (gVar != null && gVar.isCheckable() && this.p0.isChecked()) {
            FrameLayout.mergeDrawableStates(onCreateDrawableState, u0);
        }
        return onCreateDrawableState;
    }

    @Override // android.view.View
    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        BadgeDrawable badgeDrawable = this.t0;
        if (badgeDrawable != null && badgeDrawable.isVisible()) {
            CharSequence title = this.p0.getTitle();
            if (!TextUtils.isEmpty(this.p0.getContentDescription())) {
                title = this.p0.getContentDescription();
            }
            accessibilityNodeInfo.setContentDescription(((Object) title) + ", " + ((Object) this.t0.h()));
        }
        b6 I0 = b6.I0(accessibilityNodeInfo);
        I0.f0(b6.c.a(0, 1, getItemVisiblePosition(), 1, false, isSelected()));
        if (isSelected()) {
            I0.d0(false);
            I0.T(b6.a.g);
        }
        I0.w0(getResources().getString(r13.item_view_role_description));
    }

    public void setBadge(BadgeDrawable badgeDrawable) {
        this.t0 = badgeDrawable;
        ImageView imageView = this.k0;
        if (imageView != null) {
            k(imageView);
        }
    }

    public void setCheckable(boolean z) {
        refreshDrawableState();
    }

    public void setChecked(boolean z) {
        TextView textView = this.n0;
        textView.setPivotX(textView.getWidth() / 2);
        TextView textView2 = this.n0;
        textView2.setPivotY(textView2.getBaseline());
        TextView textView3 = this.m0;
        textView3.setPivotX(textView3.getWidth() / 2);
        TextView textView4 = this.m0;
        textView4.setPivotY(textView4.getBaseline());
        int i = this.i0;
        if (i != -1) {
            if (i == 0) {
                if (z) {
                    i(this.k0, this.a, 49);
                    ViewGroup viewGroup = this.l0;
                    n(viewGroup, ((Integer) viewGroup.getTag(b03.mtrl_view_tag_bottom_padding)).intValue());
                    this.n0.setVisibility(0);
                } else {
                    i(this.k0, this.a, 17);
                    n(this.l0, 0);
                    this.n0.setVisibility(4);
                }
                this.m0.setVisibility(4);
            } else if (i == 1) {
                ViewGroup viewGroup2 = this.l0;
                n(viewGroup2, ((Integer) viewGroup2.getTag(b03.mtrl_view_tag_bottom_padding)).intValue());
                if (z) {
                    i(this.k0, (int) (this.a + this.f0), 49);
                    j(this.n0, 1.0f, 1.0f, 0);
                    TextView textView5 = this.m0;
                    float f = this.g0;
                    j(textView5, f, f, 4);
                } else {
                    i(this.k0, this.a, 49);
                    TextView textView6 = this.n0;
                    float f2 = this.h0;
                    j(textView6, f2, f2, 4);
                    j(this.m0, 1.0f, 1.0f, 0);
                }
            } else if (i == 2) {
                i(this.k0, this.a, 17);
                this.n0.setVisibility(8);
                this.m0.setVisibility(8);
            }
        } else if (this.j0) {
            if (z) {
                i(this.k0, this.a, 49);
                ViewGroup viewGroup3 = this.l0;
                n(viewGroup3, ((Integer) viewGroup3.getTag(b03.mtrl_view_tag_bottom_padding)).intValue());
                this.n0.setVisibility(0);
            } else {
                i(this.k0, this.a, 17);
                n(this.l0, 0);
                this.n0.setVisibility(4);
            }
            this.m0.setVisibility(4);
        } else {
            ViewGroup viewGroup4 = this.l0;
            n(viewGroup4, ((Integer) viewGroup4.getTag(b03.mtrl_view_tag_bottom_padding)).intValue());
            if (z) {
                i(this.k0, (int) (this.a + this.f0), 49);
                j(this.n0, 1.0f, 1.0f, 0);
                TextView textView7 = this.m0;
                float f3 = this.g0;
                j(textView7, f3, f3, 4);
            } else {
                i(this.k0, this.a, 49);
                TextView textView8 = this.n0;
                float f4 = this.h0;
                j(textView8, f4, f4, 4);
                j(this.m0, 1.0f, 1.0f, 0);
            }
        }
        refreshDrawableState();
        setSelected(z);
    }

    @Override // android.view.View
    public void setEnabled(boolean z) {
        super.setEnabled(z);
        this.m0.setEnabled(z);
        this.n0.setEnabled(z);
        this.k0.setEnabled(z);
        if (z) {
            ei4.I0(this, ms2.b(getContext(), PlaybackException.ERROR_CODE_BEHIND_LIVE_WINDOW));
        } else {
            ei4.I0(this, null);
        }
    }

    public void setIcon(Drawable drawable) {
        if (drawable == this.r0) {
            return;
        }
        this.r0 = drawable;
        if (drawable != null) {
            Drawable.ConstantState constantState = drawable.getConstantState();
            if (constantState != null) {
                drawable = constantState.newDrawable();
            }
            drawable = androidx.core.graphics.drawable.a.r(drawable).mutate();
            this.s0 = drawable;
            ColorStateList colorStateList = this.q0;
            if (colorStateList != null) {
                androidx.core.graphics.drawable.a.o(drawable, colorStateList);
            }
        }
        this.k0.setImageDrawable(drawable);
    }

    public void setIconSize(int i) {
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) this.k0.getLayoutParams();
        layoutParams.width = i;
        layoutParams.height = i;
        this.k0.setLayoutParams(layoutParams);
    }

    public void setIconTintList(ColorStateList colorStateList) {
        Drawable drawable;
        this.q0 = colorStateList;
        if (this.p0 == null || (drawable = this.s0) == null) {
            return;
        }
        androidx.core.graphics.drawable.a.o(drawable, colorStateList);
        this.s0.invalidateSelf();
    }

    public void setItemBackground(int i) {
        setItemBackground(i == 0 ? null : m70.f(getContext(), i));
    }

    public void setItemPosition(int i) {
        this.o0 = i;
    }

    public void setLabelVisibilityMode(int i) {
        if (this.i0 != i) {
            this.i0 = i;
            g gVar = this.p0;
            if (gVar != null) {
                setChecked(gVar.isChecked());
            }
        }
    }

    public void setShifting(boolean z) {
        if (this.j0 != z) {
            this.j0 = z;
            g gVar = this.p0;
            if (gVar != null) {
                setChecked(gVar.isChecked());
            }
        }
    }

    public void setShortcut(boolean z, char c) {
    }

    public void setTextAppearanceActive(int i) {
        t44.q(this.n0, i);
        e(this.m0.getTextSize(), this.n0.getTextSize());
    }

    public void setTextAppearanceInactive(int i) {
        t44.q(this.m0, i);
        e(this.m0.getTextSize(), this.n0.getTextSize());
    }

    public void setTextColor(ColorStateList colorStateList) {
        if (colorStateList != null) {
            this.m0.setTextColor(colorStateList);
            this.n0.setTextColor(colorStateList);
        }
    }

    public void setTitle(CharSequence charSequence) {
        this.m0.setText(charSequence);
        this.n0.setText(charSequence);
        g gVar = this.p0;
        if (gVar == null || TextUtils.isEmpty(gVar.getContentDescription())) {
            setContentDescription(charSequence);
        }
        g gVar2 = this.p0;
        if (gVar2 != null && !TextUtils.isEmpty(gVar2.getTooltipText())) {
            charSequence = this.p0.getTooltipText();
        }
        int i = Build.VERSION.SDK_INT;
        if (i < 21 || i > 23) {
            j74.a(this, charSequence);
        }
    }

    public void setItemBackground(Drawable drawable) {
        if (drawable != null && drawable.getConstantState() != null) {
            drawable = drawable.getConstantState().newDrawable().mutate();
        }
        ei4.w0(this, drawable);
    }
}
