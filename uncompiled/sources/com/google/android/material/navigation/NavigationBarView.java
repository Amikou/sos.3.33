package com.google.android.material.navigation;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import androidx.appcompat.view.menu.e;
import androidx.appcompat.view.menu.j;
import androidx.customview.view.AbsSavedState;
import defpackage.mk4;

/* loaded from: classes2.dex */
public abstract class NavigationBarView extends FrameLayout {
    public final ie2 a;
    public final NavigationBarMenuView f0;
    public final NavigationBarPresenter g0;
    public ColorStateList h0;
    public MenuInflater i0;
    public d j0;
    public c k0;

    /* loaded from: classes2.dex */
    public static class SavedState extends AbsSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new a();
        public Bundle g0;

        /* loaded from: classes2.dex */
        public static class a implements Parcelable.ClassLoaderCreator<SavedState> {
            @Override // android.os.Parcelable.Creator
            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel, null);
            }

            @Override // android.os.Parcelable.ClassLoaderCreator
            /* renamed from: b */
            public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new SavedState(parcel, classLoader);
            }

            @Override // android.os.Parcelable.Creator
            /* renamed from: c */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public final void b(Parcel parcel, ClassLoader classLoader) {
            this.g0 = parcel.readBundle(classLoader);
        }

        @Override // androidx.customview.view.AbsSavedState, android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeBundle(this.g0);
        }

        public SavedState(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            b(parcel, classLoader == null ? getClass().getClassLoader() : classLoader);
        }
    }

    /* loaded from: classes2.dex */
    public class a implements e.a {
        public a() {
        }

        @Override // androidx.appcompat.view.menu.e.a
        public boolean a(e eVar, MenuItem menuItem) {
            if (NavigationBarView.this.k0 == null || menuItem.getItemId() != NavigationBarView.this.getSelectedItemId()) {
                return (NavigationBarView.this.j0 == null || NavigationBarView.this.j0.a(menuItem)) ? false : true;
            }
            NavigationBarView.this.k0.a(menuItem);
            return true;
        }

        @Override // androidx.appcompat.view.menu.e.a
        public void b(e eVar) {
        }
    }

    /* loaded from: classes2.dex */
    public class b implements mk4.e {
        public b(NavigationBarView navigationBarView) {
        }

        @Override // defpackage.mk4.e
        public jp4 a(View view, jp4 jp4Var, mk4.f fVar) {
            fVar.d += jp4Var.j();
            boolean z = ei4.E(view) == 1;
            int k = jp4Var.k();
            int l = jp4Var.l();
            fVar.a += z ? l : k;
            int i = fVar.c;
            if (!z) {
                k = l;
            }
            fVar.c = i + k;
            fVar.a(view);
            return jp4Var;
        }
    }

    /* loaded from: classes2.dex */
    public interface c {
        void a(MenuItem menuItem);
    }

    /* loaded from: classes2.dex */
    public interface d {
        boolean a(MenuItem menuItem);
    }

    public NavigationBarView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(r42.c(context, attributeSet, i, i2), attributeSet, i);
        NavigationBarPresenter navigationBarPresenter = new NavigationBarPresenter();
        this.g0 = navigationBarPresenter;
        Context context2 = getContext();
        int[] iArr = o23.NavigationBarView;
        int i3 = o23.NavigationBarView_itemTextAppearanceInactive;
        int i4 = o23.NavigationBarView_itemTextAppearanceActive;
        l64 i5 = a54.i(context2, attributeSet, iArr, i, i2, i3, i4);
        ie2 ie2Var = new ie2(context2, getClass(), getMaxItemCount());
        this.a = ie2Var;
        NavigationBarMenuView e = e(context2);
        this.f0 = e;
        navigationBarPresenter.b(e);
        navigationBarPresenter.a(1);
        e.setPresenter(navigationBarPresenter);
        ie2Var.b(navigationBarPresenter);
        navigationBarPresenter.i(getContext(), ie2Var);
        int i6 = o23.NavigationBarView_itemIconTint;
        if (i5.s(i6)) {
            e.setIconTintList(i5.c(i6));
        } else {
            e.setIconTintList(e.e(16842808));
        }
        setItemIconSize(i5.f(o23.NavigationBarView_itemIconSize, getResources().getDimensionPixelSize(jz2.mtrl_navigation_bar_item_default_icon_size)));
        if (i5.s(i3)) {
            setItemTextAppearanceInactive(i5.n(i3, 0));
        }
        if (i5.s(i4)) {
            setItemTextAppearanceActive(i5.n(i4, 0));
        }
        int i7 = o23.NavigationBarView_itemTextColor;
        if (i5.s(i7)) {
            setItemTextColor(i5.c(i7));
        }
        if (getBackground() == null || (getBackground() instanceof ColorDrawable)) {
            ei4.w0(this, d(context2));
        }
        int i8 = o23.NavigationBarView_elevation;
        if (i5.s(i8)) {
            setElevation(i5.f(i8, 0));
        }
        androidx.core.graphics.drawable.a.o(getBackground().mutate(), n42.a(context2, i5, o23.NavigationBarView_backgroundTint));
        setLabelVisibilityMode(i5.l(o23.NavigationBarView_labelVisibilityMode, -1));
        int n = i5.n(o23.NavigationBarView_itemBackground, 0);
        if (n != 0) {
            e.setItemBackgroundRes(n);
        } else {
            setItemRippleColor(n42.a(context2, i5, o23.NavigationBarView_itemRippleColor));
        }
        int i9 = o23.NavigationBarView_menu;
        if (i5.s(i9)) {
            f(i5.n(i9, 0));
        }
        i5.w();
        addView(e);
        ie2Var.V(new a());
        c();
    }

    private MenuInflater getMenuInflater() {
        if (this.i0 == null) {
            this.i0 = new kw3(getContext());
        }
        return this.i0;
    }

    public final void c() {
        mk4.a(this, new b(this));
    }

    public final o42 d(Context context) {
        o42 o42Var = new o42();
        Drawable background = getBackground();
        if (background instanceof ColorDrawable) {
            o42Var.a0(ColorStateList.valueOf(((ColorDrawable) background).getColor()));
        }
        o42Var.P(context);
        return o42Var;
    }

    public abstract NavigationBarMenuView e(Context context);

    public void f(int i) {
        this.g0.k(true);
        getMenuInflater().inflate(i, this.a);
        this.g0.k(false);
        this.g0.d(true);
    }

    public Drawable getItemBackground() {
        return this.f0.getItemBackground();
    }

    @Deprecated
    public int getItemBackgroundResource() {
        return this.f0.getItemBackgroundRes();
    }

    public int getItemIconSize() {
        return this.f0.getItemIconSize();
    }

    public ColorStateList getItemIconTintList() {
        return this.f0.getIconTintList();
    }

    public ColorStateList getItemRippleColor() {
        return this.h0;
    }

    public int getItemTextAppearanceActive() {
        return this.f0.getItemTextAppearanceActive();
    }

    public int getItemTextAppearanceInactive() {
        return this.f0.getItemTextAppearanceInactive();
    }

    public ColorStateList getItemTextColor() {
        return this.f0.getItemTextColor();
    }

    public int getLabelVisibilityMode() {
        return this.f0.getLabelVisibilityMode();
    }

    public abstract int getMaxItemCount();

    public Menu getMenu() {
        return this.a;
    }

    public j getMenuView() {
        return this.f0;
    }

    public NavigationBarPresenter getPresenter() {
        return this.g0;
    }

    public int getSelectedItemId() {
        return this.f0.getSelectedItemId();
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        p42.e(this);
    }

    @Override // android.view.View
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.a());
        this.a.S(savedState.g0);
    }

    @Override // android.view.View
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        Bundle bundle = new Bundle();
        savedState.g0 = bundle;
        this.a.U(bundle);
        return savedState;
    }

    @Override // android.view.View
    public void setElevation(float f) {
        if (Build.VERSION.SDK_INT >= 21) {
            super.setElevation(f);
        }
        p42.d(this, f);
    }

    public void setItemBackground(Drawable drawable) {
        this.f0.setItemBackground(drawable);
        this.h0 = null;
    }

    public void setItemBackgroundResource(int i) {
        this.f0.setItemBackgroundRes(i);
        this.h0 = null;
    }

    public void setItemIconSize(int i) {
        this.f0.setItemIconSize(i);
    }

    public void setItemIconSizeRes(int i) {
        setItemIconSize(getResources().getDimensionPixelSize(i));
    }

    public void setItemIconTintList(ColorStateList colorStateList) {
        this.f0.setIconTintList(colorStateList);
    }

    public void setItemOnTouchListener(int i, View.OnTouchListener onTouchListener) {
        this.f0.setItemOnTouchListener(i, onTouchListener);
    }

    public void setItemRippleColor(ColorStateList colorStateList) {
        if (this.h0 == colorStateList) {
            if (colorStateList != null || this.f0.getItemBackground() == null) {
                return;
            }
            this.f0.setItemBackground(null);
            return;
        }
        this.h0 = colorStateList;
        if (colorStateList == null) {
            this.f0.setItemBackground(null);
            return;
        }
        ColorStateList a2 = b93.a(colorStateList);
        if (Build.VERSION.SDK_INT >= 21) {
            this.f0.setItemBackground(new RippleDrawable(a2, null, null));
            return;
        }
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setCornerRadius(1.0E-5f);
        Drawable r = androidx.core.graphics.drawable.a.r(gradientDrawable);
        androidx.core.graphics.drawable.a.o(r, a2);
        this.f0.setItemBackground(r);
    }

    public void setItemTextAppearanceActive(int i) {
        this.f0.setItemTextAppearanceActive(i);
    }

    public void setItemTextAppearanceInactive(int i) {
        this.f0.setItemTextAppearanceInactive(i);
    }

    public void setItemTextColor(ColorStateList colorStateList) {
        this.f0.setItemTextColor(colorStateList);
    }

    public void setLabelVisibilityMode(int i) {
        if (this.f0.getLabelVisibilityMode() != i) {
            this.f0.setLabelVisibilityMode(i);
            this.g0.d(false);
        }
    }

    public void setOnItemReselectedListener(c cVar) {
        this.k0 = cVar;
    }

    public void setOnItemSelectedListener(d dVar) {
        this.j0 = dVar;
    }

    public void setSelectedItemId(int i) {
        MenuItem findItem = this.a.findItem(i);
        if (findItem == null || this.a.O(findItem, this.g0, 0)) {
            return;
        }
        findItem.setChecked(true);
    }
}
