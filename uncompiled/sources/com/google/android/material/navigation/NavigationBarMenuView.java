package com.google.android.material.navigation;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityNodeInfo;
import androidx.appcompat.view.menu.e;
import androidx.appcompat.view.menu.g;
import androidx.appcompat.view.menu.j;
import androidx.transition.AutoTransition;
import androidx.transition.TransitionSet;
import androidx.transition.d;
import com.google.android.material.badge.BadgeDrawable;
import defpackage.b6;
import java.util.HashSet;

/* loaded from: classes2.dex */
public abstract class NavigationBarMenuView extends ViewGroup implements j {
    public static final int[] x0 = {16842912};
    public static final int[] y0 = {-16842910};
    public final TransitionSet a;
    public final View.OnClickListener f0;
    public final et2<NavigationBarItemView> g0;
    public final SparseArray<View.OnTouchListener> h0;
    public int i0;
    public NavigationBarItemView[] j0;
    public int k0;
    public int l0;
    public ColorStateList m0;
    public int n0;
    public ColorStateList o0;
    public final ColorStateList p0;
    public int q0;
    public int r0;
    public Drawable s0;
    public int t0;
    public SparseArray<BadgeDrawable> u0;
    public NavigationBarPresenter v0;
    public e w0;

    /* loaded from: classes2.dex */
    public class a implements View.OnClickListener {
        public a() {
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            g itemData = ((NavigationBarItemView) view).getItemData();
            if (NavigationBarMenuView.this.w0.O(itemData, NavigationBarMenuView.this.v0, 0)) {
                return;
            }
            itemData.setChecked(true);
        }
    }

    public NavigationBarMenuView(Context context) {
        super(context);
        this.g0 = new it2(5);
        this.h0 = new SparseArray<>(5);
        this.k0 = 0;
        this.l0 = 0;
        this.u0 = new SparseArray<>(5);
        this.p0 = e(16842808);
        AutoTransition autoTransition = new AutoTransition();
        this.a = autoTransition;
        autoTransition.D0(0);
        autoTransition.h0(115L);
        autoTransition.k0(new f21());
        autoTransition.u0(new p44());
        this.f0 = new a();
        ei4.D0(this, 1);
    }

    private NavigationBarItemView getNewItem() {
        NavigationBarItemView b = this.g0.b();
        return b == null ? f(getContext()) : b;
    }

    private void setBadgeIfNeeded(NavigationBarItemView navigationBarItemView) {
        BadgeDrawable badgeDrawable;
        int id = navigationBarItemView.getId();
        if (h(id) && (badgeDrawable = this.u0.get(id)) != null) {
            navigationBarItemView.setBadge(badgeDrawable);
        }
    }

    @Override // androidx.appcompat.view.menu.j
    public void b(e eVar) {
        this.w0 = eVar;
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public void d() {
        removeAllViews();
        NavigationBarItemView[] navigationBarItemViewArr = this.j0;
        if (navigationBarItemViewArr != null) {
            for (NavigationBarItemView navigationBarItemView : navigationBarItemViewArr) {
                if (navigationBarItemView != null) {
                    this.g0.a(navigationBarItemView);
                    navigationBarItemView.h();
                }
            }
        }
        if (this.w0.size() == 0) {
            this.k0 = 0;
            this.l0 = 0;
            this.j0 = null;
            return;
        }
        i();
        this.j0 = new NavigationBarItemView[this.w0.size()];
        boolean g = g(this.i0, this.w0.G().size());
        for (int i = 0; i < this.w0.size(); i++) {
            this.v0.k(true);
            this.w0.getItem(i).setCheckable(true);
            this.v0.k(false);
            NavigationBarItemView newItem = getNewItem();
            this.j0[i] = newItem;
            newItem.setIconTintList(this.m0);
            newItem.setIconSize(this.n0);
            newItem.setTextColor(this.p0);
            newItem.setTextAppearanceInactive(this.q0);
            newItem.setTextAppearanceActive(this.r0);
            newItem.setTextColor(this.o0);
            Drawable drawable = this.s0;
            if (drawable != null) {
                newItem.setItemBackground(drawable);
            } else {
                newItem.setItemBackground(this.t0);
            }
            newItem.setShifting(g);
            newItem.setLabelVisibilityMode(this.i0);
            g gVar = (g) this.w0.getItem(i);
            newItem.d(gVar, 0);
            newItem.setItemPosition(i);
            int itemId = gVar.getItemId();
            newItem.setOnTouchListener(this.h0.get(itemId));
            newItem.setOnClickListener(this.f0);
            int i2 = this.k0;
            if (i2 != 0 && itemId == i2) {
                this.l0 = i;
            }
            setBadgeIfNeeded(newItem);
            addView(newItem);
        }
        int min = Math.min(this.w0.size() - 1, this.l0);
        this.l0 = min;
        this.w0.getItem(min).setChecked(true);
    }

    public ColorStateList e(int i) {
        TypedValue typedValue = new TypedValue();
        if (getContext().getTheme().resolveAttribute(i, typedValue, true)) {
            ColorStateList c = mf.c(getContext(), typedValue.resourceId);
            if (getContext().getTheme().resolveAttribute(jy2.colorPrimary, typedValue, true)) {
                int i2 = typedValue.data;
                int defaultColor = c.getDefaultColor();
                int[] iArr = y0;
                return new ColorStateList(new int[][]{iArr, x0, ViewGroup.EMPTY_STATE_SET}, new int[]{c.getColorForState(iArr, defaultColor), i2, defaultColor});
            }
            return null;
        }
        return null;
    }

    public abstract NavigationBarItemView f(Context context);

    public boolean g(int i, int i2) {
        if (i == -1) {
            if (i2 > 3) {
                return true;
            }
        } else if (i == 0) {
            return true;
        }
        return false;
    }

    public SparseArray<BadgeDrawable> getBadgeDrawables() {
        return this.u0;
    }

    public ColorStateList getIconTintList() {
        return this.m0;
    }

    public Drawable getItemBackground() {
        NavigationBarItemView[] navigationBarItemViewArr = this.j0;
        if (navigationBarItemViewArr != null && navigationBarItemViewArr.length > 0) {
            return navigationBarItemViewArr[0].getBackground();
        }
        return this.s0;
    }

    @Deprecated
    public int getItemBackgroundRes() {
        return this.t0;
    }

    public int getItemIconSize() {
        return this.n0;
    }

    public int getItemTextAppearanceActive() {
        return this.r0;
    }

    public int getItemTextAppearanceInactive() {
        return this.q0;
    }

    public ColorStateList getItemTextColor() {
        return this.o0;
    }

    public int getLabelVisibilityMode() {
        return this.i0;
    }

    public e getMenu() {
        return this.w0;
    }

    public int getSelectedItemId() {
        return this.k0;
    }

    public int getSelectedItemPosition() {
        return this.l0;
    }

    public int getWindowAnimations() {
        return 0;
    }

    public final boolean h(int i) {
        return i != -1;
    }

    public final void i() {
        HashSet hashSet = new HashSet();
        for (int i = 0; i < this.w0.size(); i++) {
            hashSet.add(Integer.valueOf(this.w0.getItem(i).getItemId()));
        }
        for (int i2 = 0; i2 < this.u0.size(); i2++) {
            int keyAt = this.u0.keyAt(i2);
            if (!hashSet.contains(Integer.valueOf(keyAt))) {
                this.u0.delete(keyAt);
            }
        }
    }

    public void j(int i) {
        int size = this.w0.size();
        for (int i2 = 0; i2 < size; i2++) {
            MenuItem item = this.w0.getItem(i2);
            if (i == item.getItemId()) {
                this.k0 = i;
                this.l0 = i2;
                item.setChecked(true);
                return;
            }
        }
    }

    public void k() {
        e eVar = this.w0;
        if (eVar == null || this.j0 == null) {
            return;
        }
        int size = eVar.size();
        if (size != this.j0.length) {
            d();
            return;
        }
        int i = this.k0;
        for (int i2 = 0; i2 < size; i2++) {
            MenuItem item = this.w0.getItem(i2);
            if (item.isChecked()) {
                this.k0 = item.getItemId();
                this.l0 = i2;
            }
        }
        if (i != this.k0) {
            d.a(this, this.a);
        }
        boolean g = g(this.i0, this.w0.G().size());
        for (int i3 = 0; i3 < size; i3++) {
            this.v0.k(true);
            this.j0[i3].setLabelVisibilityMode(this.i0);
            this.j0[i3].setShifting(g);
            this.j0[i3].d((g) this.w0.getItem(i3), 0);
            this.v0.k(false);
        }
    }

    @Override // android.view.View
    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        b6.I0(accessibilityNodeInfo).e0(b6.b.b(1, this.w0.G().size(), false, 1));
    }

    public void setBadgeDrawables(SparseArray<BadgeDrawable> sparseArray) {
        this.u0 = sparseArray;
        NavigationBarItemView[] navigationBarItemViewArr = this.j0;
        if (navigationBarItemViewArr != null) {
            for (NavigationBarItemView navigationBarItemView : navigationBarItemViewArr) {
                navigationBarItemView.setBadge(sparseArray.get(navigationBarItemView.getId()));
            }
        }
    }

    public void setIconTintList(ColorStateList colorStateList) {
        this.m0 = colorStateList;
        NavigationBarItemView[] navigationBarItemViewArr = this.j0;
        if (navigationBarItemViewArr != null) {
            for (NavigationBarItemView navigationBarItemView : navigationBarItemViewArr) {
                navigationBarItemView.setIconTintList(colorStateList);
            }
        }
    }

    public void setItemBackground(Drawable drawable) {
        this.s0 = drawable;
        NavigationBarItemView[] navigationBarItemViewArr = this.j0;
        if (navigationBarItemViewArr != null) {
            for (NavigationBarItemView navigationBarItemView : navigationBarItemViewArr) {
                navigationBarItemView.setItemBackground(drawable);
            }
        }
    }

    public void setItemBackgroundRes(int i) {
        this.t0 = i;
        NavigationBarItemView[] navigationBarItemViewArr = this.j0;
        if (navigationBarItemViewArr != null) {
            for (NavigationBarItemView navigationBarItemView : navigationBarItemViewArr) {
                navigationBarItemView.setItemBackground(i);
            }
        }
    }

    public void setItemIconSize(int i) {
        this.n0 = i;
        NavigationBarItemView[] navigationBarItemViewArr = this.j0;
        if (navigationBarItemViewArr != null) {
            for (NavigationBarItemView navigationBarItemView : navigationBarItemViewArr) {
                navigationBarItemView.setIconSize(i);
            }
        }
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public void setItemOnTouchListener(int i, View.OnTouchListener onTouchListener) {
        if (onTouchListener == null) {
            this.h0.remove(i);
        } else {
            this.h0.put(i, onTouchListener);
        }
        NavigationBarItemView[] navigationBarItemViewArr = this.j0;
        if (navigationBarItemViewArr != null) {
            for (NavigationBarItemView navigationBarItemView : navigationBarItemViewArr) {
                if (navigationBarItemView.getItemData().getItemId() == i) {
                    navigationBarItemView.setOnTouchListener(onTouchListener);
                }
            }
        }
    }

    public void setItemTextAppearanceActive(int i) {
        this.r0 = i;
        NavigationBarItemView[] navigationBarItemViewArr = this.j0;
        if (navigationBarItemViewArr != null) {
            for (NavigationBarItemView navigationBarItemView : navigationBarItemViewArr) {
                navigationBarItemView.setTextAppearanceActive(i);
                ColorStateList colorStateList = this.o0;
                if (colorStateList != null) {
                    navigationBarItemView.setTextColor(colorStateList);
                }
            }
        }
    }

    public void setItemTextAppearanceInactive(int i) {
        this.q0 = i;
        NavigationBarItemView[] navigationBarItemViewArr = this.j0;
        if (navigationBarItemViewArr != null) {
            for (NavigationBarItemView navigationBarItemView : navigationBarItemViewArr) {
                navigationBarItemView.setTextAppearanceInactive(i);
                ColorStateList colorStateList = this.o0;
                if (colorStateList != null) {
                    navigationBarItemView.setTextColor(colorStateList);
                }
            }
        }
    }

    public void setItemTextColor(ColorStateList colorStateList) {
        this.o0 = colorStateList;
        NavigationBarItemView[] navigationBarItemViewArr = this.j0;
        if (navigationBarItemViewArr != null) {
            for (NavigationBarItemView navigationBarItemView : navigationBarItemViewArr) {
                navigationBarItemView.setTextColor(colorStateList);
            }
        }
    }

    public void setLabelVisibilityMode(int i) {
        this.i0 = i;
    }

    public void setPresenter(NavigationBarPresenter navigationBarPresenter) {
        this.v0 = navigationBarPresenter;
    }
}
