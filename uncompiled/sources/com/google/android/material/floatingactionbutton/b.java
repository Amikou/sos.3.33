package com.google.android.material.floatingactionbutton;

import android.animation.Animator;
import android.animation.AnimatorSet;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import java.util.List;

/* compiled from: MotionStrategy.java */
/* loaded from: classes2.dex */
public interface b {
    void a();

    int b();

    void c();

    z92 d();

    boolean e();

    void f();

    AnimatorSet g();

    List<Animator.AnimatorListener> h();

    void i(ExtendedFloatingActionButton.j jVar);

    void j(z92 z92Var);

    void onAnimationStart(Animator animator);
}
