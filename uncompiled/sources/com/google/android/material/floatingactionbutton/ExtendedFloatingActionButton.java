package com.google.android.material.floatingactionbutton;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Property;
import android.view.View;
import android.view.ViewGroup;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.button.MaterialButton;
import java.util.List;

/* loaded from: classes2.dex */
public class ExtendedFloatingActionButton extends MaterialButton implements CoordinatorLayout.b {
    public static final int K0 = y13.Widget_MaterialComponents_ExtendedFloatingActionButton_Icon;
    public static final Property<View, Float> L0 = new d(Float.class, "width");
    public static final Property<View, Float> M0 = new e(Float.class, "height");
    public static final Property<View, Float> N0 = new f(Float.class, "paddingStart");
    public static final Property<View, Float> O0 = new g(Float.class, "paddingEnd");
    public final com.google.android.material.floatingactionbutton.b A0;
    public final com.google.android.material.floatingactionbutton.b B0;
    public final int C0;
    public int D0;
    public int E0;
    public final CoordinatorLayout.Behavior<ExtendedFloatingActionButton> F0;
    public boolean G0;
    public boolean H0;
    public boolean I0;
    public ColorStateList J0;
    public int w0;
    public final se x0;
    public final com.google.android.material.floatingactionbutton.b y0;
    public final com.google.android.material.floatingactionbutton.b z0;

    /* loaded from: classes2.dex */
    public class a implements l {
        public a() {
        }

        @Override // com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton.l
        public ViewGroup.LayoutParams a() {
            return new ViewGroup.LayoutParams(-2, -2);
        }

        @Override // com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton.l
        public int getHeight() {
            return ExtendedFloatingActionButton.this.getMeasuredHeight();
        }

        @Override // com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton.l
        public int getPaddingEnd() {
            return ExtendedFloatingActionButton.this.E0;
        }

        @Override // com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton.l
        public int getPaddingStart() {
            return ExtendedFloatingActionButton.this.D0;
        }

        @Override // com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton.l
        public int getWidth() {
            return (ExtendedFloatingActionButton.this.getMeasuredWidth() - (ExtendedFloatingActionButton.this.getCollapsedPadding() * 2)) + ExtendedFloatingActionButton.this.D0 + ExtendedFloatingActionButton.this.E0;
        }
    }

    /* loaded from: classes2.dex */
    public class b implements l {
        public b() {
        }

        @Override // com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton.l
        public ViewGroup.LayoutParams a() {
            return new ViewGroup.LayoutParams(getWidth(), getHeight());
        }

        @Override // com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton.l
        public int getHeight() {
            return ExtendedFloatingActionButton.this.getCollapsedSize();
        }

        @Override // com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton.l
        public int getPaddingEnd() {
            return ExtendedFloatingActionButton.this.getCollapsedPadding();
        }

        @Override // com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton.l
        public int getPaddingStart() {
            return ExtendedFloatingActionButton.this.getCollapsedPadding();
        }

        @Override // com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton.l
        public int getWidth() {
            return ExtendedFloatingActionButton.this.getCollapsedSize();
        }
    }

    /* loaded from: classes2.dex */
    public class c extends AnimatorListenerAdapter {
        public boolean a;
        public final /* synthetic */ com.google.android.material.floatingactionbutton.b f0;
        public final /* synthetic */ j g0;

        public c(ExtendedFloatingActionButton extendedFloatingActionButton, com.google.android.material.floatingactionbutton.b bVar, j jVar) {
            this.f0 = bVar;
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationCancel(Animator animator) {
            this.a = true;
            this.f0.a();
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            this.f0.f();
            if (this.a) {
                return;
            }
            this.f0.i(this.g0);
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationStart(Animator animator) {
            this.f0.onAnimationStart(animator);
            this.a = false;
        }
    }

    /* loaded from: classes2.dex */
    public static class d extends Property<View, Float> {
        public d(Class cls, String str) {
            super(cls, str);
        }

        @Override // android.util.Property
        /* renamed from: a */
        public Float get(View view) {
            return Float.valueOf(view.getLayoutParams().width);
        }

        @Override // android.util.Property
        /* renamed from: b */
        public void set(View view, Float f) {
            view.getLayoutParams().width = f.intValue();
            view.requestLayout();
        }
    }

    /* loaded from: classes2.dex */
    public static class e extends Property<View, Float> {
        public e(Class cls, String str) {
            super(cls, str);
        }

        @Override // android.util.Property
        /* renamed from: a */
        public Float get(View view) {
            return Float.valueOf(view.getLayoutParams().height);
        }

        @Override // android.util.Property
        /* renamed from: b */
        public void set(View view, Float f) {
            view.getLayoutParams().height = f.intValue();
            view.requestLayout();
        }
    }

    /* loaded from: classes2.dex */
    public static class f extends Property<View, Float> {
        public f(Class cls, String str) {
            super(cls, str);
        }

        @Override // android.util.Property
        /* renamed from: a */
        public Float get(View view) {
            return Float.valueOf(ei4.J(view));
        }

        @Override // android.util.Property
        /* renamed from: b */
        public void set(View view, Float f) {
            ei4.H0(view, f.intValue(), view.getPaddingTop(), ei4.I(view), view.getPaddingBottom());
        }
    }

    /* loaded from: classes2.dex */
    public static class g extends Property<View, Float> {
        public g(Class cls, String str) {
            super(cls, str);
        }

        @Override // android.util.Property
        /* renamed from: a */
        public Float get(View view) {
            return Float.valueOf(ei4.I(view));
        }

        @Override // android.util.Property
        /* renamed from: b */
        public void set(View view, Float f) {
            ei4.H0(view, ei4.J(view), view.getPaddingTop(), f.intValue(), view.getPaddingBottom());
        }
    }

    /* loaded from: classes2.dex */
    public class h extends pn {
        public final l g;
        public final boolean h;

        public h(se seVar, l lVar, boolean z) {
            super(ExtendedFloatingActionButton.this, seVar);
            this.g = lVar;
            this.h = z;
        }

        @Override // com.google.android.material.floatingactionbutton.b
        public int b() {
            return this.h ? vx2.mtrl_extended_fab_change_size_expand_motion_spec : vx2.mtrl_extended_fab_change_size_collapse_motion_spec;
        }

        @Override // com.google.android.material.floatingactionbutton.b
        public void c() {
            ExtendedFloatingActionButton.this.G0 = this.h;
            ViewGroup.LayoutParams layoutParams = ExtendedFloatingActionButton.this.getLayoutParams();
            if (layoutParams == null) {
                return;
            }
            layoutParams.width = this.g.a().width;
            layoutParams.height = this.g.a().height;
            ei4.H0(ExtendedFloatingActionButton.this, this.g.getPaddingStart(), ExtendedFloatingActionButton.this.getPaddingTop(), this.g.getPaddingEnd(), ExtendedFloatingActionButton.this.getPaddingBottom());
            ExtendedFloatingActionButton.this.requestLayout();
        }

        @Override // com.google.android.material.floatingactionbutton.b
        public boolean e() {
            return this.h == ExtendedFloatingActionButton.this.G0 || ExtendedFloatingActionButton.this.getIcon() == null || TextUtils.isEmpty(ExtendedFloatingActionButton.this.getText());
        }

        @Override // defpackage.pn, com.google.android.material.floatingactionbutton.b
        public void f() {
            super.f();
            ExtendedFloatingActionButton.this.H0 = false;
            ExtendedFloatingActionButton.this.setHorizontallyScrolling(false);
            ViewGroup.LayoutParams layoutParams = ExtendedFloatingActionButton.this.getLayoutParams();
            if (layoutParams == null) {
                return;
            }
            layoutParams.width = this.g.a().width;
            layoutParams.height = this.g.a().height;
        }

        @Override // defpackage.pn, com.google.android.material.floatingactionbutton.b
        public AnimatorSet g() {
            z92 m = m();
            if (m.j("width")) {
                PropertyValuesHolder[] g = m.g("width");
                g[0].setFloatValues(ExtendedFloatingActionButton.this.getWidth(), this.g.getWidth());
                m.l("width", g);
            }
            if (m.j("height")) {
                PropertyValuesHolder[] g2 = m.g("height");
                g2[0].setFloatValues(ExtendedFloatingActionButton.this.getHeight(), this.g.getHeight());
                m.l("height", g2);
            }
            if (m.j("paddingStart")) {
                PropertyValuesHolder[] g3 = m.g("paddingStart");
                g3[0].setFloatValues(ei4.J(ExtendedFloatingActionButton.this), this.g.getPaddingStart());
                m.l("paddingStart", g3);
            }
            if (m.j("paddingEnd")) {
                PropertyValuesHolder[] g4 = m.g("paddingEnd");
                g4[0].setFloatValues(ei4.I(ExtendedFloatingActionButton.this), this.g.getPaddingEnd());
                m.l("paddingEnd", g4);
            }
            if (m.j("labelOpacity")) {
                PropertyValuesHolder[] g5 = m.g("labelOpacity");
                boolean z = this.h;
                float f = Utils.FLOAT_EPSILON;
                float f2 = z ? 0.0f : 1.0f;
                if (z) {
                    f = 1.0f;
                }
                g5[0].setFloatValues(f2, f);
                m.l("labelOpacity", g5);
            }
            return super.l(m);
        }

        @Override // com.google.android.material.floatingactionbutton.b
        public void i(j jVar) {
        }

        @Override // defpackage.pn, com.google.android.material.floatingactionbutton.b
        public void onAnimationStart(Animator animator) {
            super.onAnimationStart(animator);
            ExtendedFloatingActionButton.this.G0 = this.h;
            ExtendedFloatingActionButton.this.H0 = true;
            ExtendedFloatingActionButton.this.setHorizontallyScrolling(true);
        }
    }

    /* loaded from: classes2.dex */
    public class i extends pn {
        public boolean g;

        public i(se seVar) {
            super(ExtendedFloatingActionButton.this, seVar);
        }

        @Override // defpackage.pn, com.google.android.material.floatingactionbutton.b
        public void a() {
            super.a();
            this.g = true;
        }

        @Override // com.google.android.material.floatingactionbutton.b
        public int b() {
            return vx2.mtrl_extended_fab_hide_motion_spec;
        }

        @Override // com.google.android.material.floatingactionbutton.b
        public void c() {
            ExtendedFloatingActionButton.this.setVisibility(8);
        }

        @Override // com.google.android.material.floatingactionbutton.b
        public boolean e() {
            return ExtendedFloatingActionButton.this.y();
        }

        @Override // defpackage.pn, com.google.android.material.floatingactionbutton.b
        public void f() {
            super.f();
            ExtendedFloatingActionButton.this.w0 = 0;
            if (this.g) {
                return;
            }
            ExtendedFloatingActionButton.this.setVisibility(8);
        }

        @Override // com.google.android.material.floatingactionbutton.b
        public void i(j jVar) {
        }

        @Override // defpackage.pn, com.google.android.material.floatingactionbutton.b
        public void onAnimationStart(Animator animator) {
            super.onAnimationStart(animator);
            this.g = false;
            ExtendedFloatingActionButton.this.setVisibility(0);
            ExtendedFloatingActionButton.this.w0 = 1;
        }
    }

    /* loaded from: classes2.dex */
    public static abstract class j {
    }

    /* loaded from: classes2.dex */
    public class k extends pn {
        public k(se seVar) {
            super(ExtendedFloatingActionButton.this, seVar);
        }

        @Override // com.google.android.material.floatingactionbutton.b
        public int b() {
            return vx2.mtrl_extended_fab_show_motion_spec;
        }

        @Override // com.google.android.material.floatingactionbutton.b
        public void c() {
            ExtendedFloatingActionButton.this.setVisibility(0);
            ExtendedFloatingActionButton.this.setAlpha(1.0f);
            ExtendedFloatingActionButton.this.setScaleY(1.0f);
            ExtendedFloatingActionButton.this.setScaleX(1.0f);
        }

        @Override // com.google.android.material.floatingactionbutton.b
        public boolean e() {
            return ExtendedFloatingActionButton.this.z();
        }

        @Override // defpackage.pn, com.google.android.material.floatingactionbutton.b
        public void f() {
            super.f();
            ExtendedFloatingActionButton.this.w0 = 0;
        }

        @Override // com.google.android.material.floatingactionbutton.b
        public void i(j jVar) {
        }

        @Override // defpackage.pn, com.google.android.material.floatingactionbutton.b
        public void onAnimationStart(Animator animator) {
            super.onAnimationStart(animator);
            ExtendedFloatingActionButton.this.setVisibility(0);
            ExtendedFloatingActionButton.this.w0 = 2;
        }
    }

    /* loaded from: classes2.dex */
    public interface l {
        ViewGroup.LayoutParams a();

        int getHeight();

        int getPaddingEnd();

        int getPaddingStart();

        int getWidth();
    }

    public ExtendedFloatingActionButton(Context context) {
        this(context, null);
    }

    public final void A(com.google.android.material.floatingactionbutton.b bVar, j jVar) {
        if (bVar.e()) {
            return;
        }
        if (!C()) {
            bVar.c();
            bVar.i(jVar);
            return;
        }
        measure(0, 0);
        AnimatorSet g2 = bVar.g();
        g2.addListener(new c(this, bVar, jVar));
        for (Animator.AnimatorListener animatorListener : bVar.h()) {
            g2.addListener(animatorListener);
        }
        g2.start();
    }

    public final void B() {
        this.J0 = getTextColors();
    }

    public final boolean C() {
        return (ei4.W(this) || (!z() && this.I0)) && !isInEditMode();
    }

    public void D(ColorStateList colorStateList) {
        super.setTextColor(colorStateList);
    }

    @Override // androidx.coordinatorlayout.widget.CoordinatorLayout.b
    public CoordinatorLayout.Behavior<ExtendedFloatingActionButton> getBehavior() {
        return this.F0;
    }

    public int getCollapsedPadding() {
        return (getCollapsedSize() - getIconSize()) / 2;
    }

    public int getCollapsedSize() {
        int i2 = this.C0;
        return i2 < 0 ? (Math.min(ei4.J(this), ei4.I(this)) * 2) + getIconSize() : i2;
    }

    public z92 getExtendMotionSpec() {
        return this.z0.d();
    }

    public z92 getHideMotionSpec() {
        return this.B0.d();
    }

    public z92 getShowMotionSpec() {
        return this.A0.d();
    }

    public z92 getShrinkMotionSpec() {
        return this.y0.d();
    }

    @Override // com.google.android.material.button.MaterialButton, android.widget.TextView, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.G0 && TextUtils.isEmpty(getText()) && getIcon() != null) {
            this.G0 = false;
            this.y0.c();
        }
    }

    public void setAnimateShowBeforeLayout(boolean z) {
        this.I0 = z;
    }

    public void setExtendMotionSpec(z92 z92Var) {
        this.z0.j(z92Var);
    }

    public void setExtendMotionSpecResource(int i2) {
        setExtendMotionSpec(z92.d(getContext(), i2));
    }

    public void setExtended(boolean z) {
        if (this.G0 == z) {
            return;
        }
        com.google.android.material.floatingactionbutton.b bVar = z ? this.z0 : this.y0;
        if (bVar.e()) {
            return;
        }
        bVar.c();
    }

    public void setHideMotionSpec(z92 z92Var) {
        this.B0.j(z92Var);
    }

    public void setHideMotionSpecResource(int i2) {
        setHideMotionSpec(z92.d(getContext(), i2));
    }

    @Override // android.widget.TextView, android.view.View
    public void setPadding(int i2, int i3, int i4, int i5) {
        super.setPadding(i2, i3, i4, i5);
        if (!this.G0 || this.H0) {
            return;
        }
        this.D0 = ei4.J(this);
        this.E0 = ei4.I(this);
    }

    @Override // android.widget.TextView, android.view.View
    public void setPaddingRelative(int i2, int i3, int i4, int i5) {
        super.setPaddingRelative(i2, i3, i4, i5);
        if (!this.G0 || this.H0) {
            return;
        }
        this.D0 = i2;
        this.E0 = i4;
    }

    public void setShowMotionSpec(z92 z92Var) {
        this.A0.j(z92Var);
    }

    public void setShowMotionSpecResource(int i2) {
        setShowMotionSpec(z92.d(getContext(), i2));
    }

    public void setShrinkMotionSpec(z92 z92Var) {
        this.y0.j(z92Var);
    }

    public void setShrinkMotionSpecResource(int i2) {
        setShrinkMotionSpec(z92.d(getContext(), i2));
    }

    @Override // android.widget.TextView
    public void setTextColor(int i2) {
        super.setTextColor(i2);
        B();
    }

    public final boolean y() {
        return getVisibility() == 0 ? this.w0 == 1 : this.w0 != 2;
    }

    public final boolean z() {
        return getVisibility() != 0 ? this.w0 == 2 : this.w0 != 1;
    }

    public ExtendedFloatingActionButton(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, gy2.extendedFloatingActionButtonStyle);
    }

    /* loaded from: classes2.dex */
    public static class ExtendedFloatingActionButtonBehavior<T extends ExtendedFloatingActionButton> extends CoordinatorLayout.Behavior<T> {
        public Rect a;
        public j b;
        public j c;
        public boolean d;
        public boolean e;

        public ExtendedFloatingActionButtonBehavior() {
            this.d = false;
            this.e = true;
        }

        public static boolean c(View view) {
            ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
            if (layoutParams instanceof CoordinatorLayout.e) {
                return ((CoordinatorLayout.e) layoutParams).f() instanceof BottomSheetBehavior;
            }
            return false;
        }

        public void a(ExtendedFloatingActionButton extendedFloatingActionButton) {
            boolean z = this.e;
            extendedFloatingActionButton.A(z ? extendedFloatingActionButton.z0 : extendedFloatingActionButton.A0, z ? this.c : this.b);
        }

        @Override // androidx.coordinatorlayout.widget.CoordinatorLayout.Behavior
        /* renamed from: b */
        public boolean getInsetDodgeRect(CoordinatorLayout coordinatorLayout, ExtendedFloatingActionButton extendedFloatingActionButton, Rect rect) {
            return super.getInsetDodgeRect(coordinatorLayout, extendedFloatingActionButton, rect);
        }

        @Override // androidx.coordinatorlayout.widget.CoordinatorLayout.Behavior
        /* renamed from: d */
        public boolean onDependentViewChanged(CoordinatorLayout coordinatorLayout, ExtendedFloatingActionButton extendedFloatingActionButton, View view) {
            if (view instanceof AppBarLayout) {
                h(coordinatorLayout, (AppBarLayout) view, extendedFloatingActionButton);
                return false;
            } else if (c(view)) {
                i(view, extendedFloatingActionButton);
                return false;
            } else {
                return false;
            }
        }

        @Override // androidx.coordinatorlayout.widget.CoordinatorLayout.Behavior
        /* renamed from: e */
        public boolean onLayoutChild(CoordinatorLayout coordinatorLayout, ExtendedFloatingActionButton extendedFloatingActionButton, int i) {
            List<View> dependencies = coordinatorLayout.getDependencies(extendedFloatingActionButton);
            int size = dependencies.size();
            for (int i2 = 0; i2 < size; i2++) {
                View view = dependencies.get(i2);
                if (view instanceof AppBarLayout) {
                    if (h(coordinatorLayout, (AppBarLayout) view, extendedFloatingActionButton)) {
                        break;
                    }
                } else {
                    if (c(view) && i(view, extendedFloatingActionButton)) {
                        break;
                    }
                }
            }
            coordinatorLayout.onLayoutChild(extendedFloatingActionButton, i);
            return true;
        }

        public final boolean f(View view, ExtendedFloatingActionButton extendedFloatingActionButton) {
            return (this.d || this.e) && ((CoordinatorLayout.e) extendedFloatingActionButton.getLayoutParams()).e() == view.getId();
        }

        public void g(ExtendedFloatingActionButton extendedFloatingActionButton) {
            boolean z = this.e;
            extendedFloatingActionButton.A(z ? extendedFloatingActionButton.y0 : extendedFloatingActionButton.B0, z ? this.c : this.b);
        }

        public final boolean h(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, ExtendedFloatingActionButton extendedFloatingActionButton) {
            if (f(appBarLayout, extendedFloatingActionButton)) {
                if (this.a == null) {
                    this.a = new Rect();
                }
                Rect rect = this.a;
                mm0.a(coordinatorLayout, appBarLayout, rect);
                if (rect.bottom <= appBarLayout.getMinimumHeightForVisibleOverlappingContent()) {
                    g(extendedFloatingActionButton);
                    return true;
                }
                a(extendedFloatingActionButton);
                return true;
            }
            return false;
        }

        public final boolean i(View view, ExtendedFloatingActionButton extendedFloatingActionButton) {
            if (f(view, extendedFloatingActionButton)) {
                if (view.getTop() < (extendedFloatingActionButton.getHeight() / 2) + ((ViewGroup.MarginLayoutParams) ((CoordinatorLayout.e) extendedFloatingActionButton.getLayoutParams())).topMargin) {
                    g(extendedFloatingActionButton);
                    return true;
                }
                a(extendedFloatingActionButton);
                return true;
            }
            return false;
        }

        @Override // androidx.coordinatorlayout.widget.CoordinatorLayout.Behavior
        public void onAttachedToLayoutParams(CoordinatorLayout.e eVar) {
            if (eVar.h == 0) {
                eVar.h = 80;
            }
        }

        public ExtendedFloatingActionButtonBehavior(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, o23.ExtendedFloatingActionButton_Behavior_Layout);
            this.d = obtainStyledAttributes.getBoolean(o23.ExtendedFloatingActionButton_Behavior_Layout_behavior_autoHide, false);
            this.e = obtainStyledAttributes.getBoolean(o23.ExtendedFloatingActionButton_Behavior_Layout_behavior_autoShrink, true);
            obtainStyledAttributes.recycle();
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public ExtendedFloatingActionButton(android.content.Context r17, android.util.AttributeSet r18, int r19) {
        /*
            r16 = this;
            r0 = r16
            r7 = r18
            r8 = r19
            int r9 = com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton.K0
            r1 = r17
            android.content.Context r1 = defpackage.r42.c(r1, r7, r8, r9)
            r0.<init>(r1, r7, r8)
            r10 = 0
            r0.w0 = r10
            se r1 = new se
            r1.<init>()
            r0.x0 = r1
            com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton$k r11 = new com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton$k
            r11.<init>(r1)
            r0.A0 = r11
            com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton$i r12 = new com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton$i
            r12.<init>(r1)
            r0.B0 = r12
            r13 = 1
            r0.G0 = r13
            r0.H0 = r10
            r0.I0 = r10
            android.content.Context r14 = r16.getContext()
            com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton$ExtendedFloatingActionButtonBehavior r1 = new com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton$ExtendedFloatingActionButtonBehavior
            r1.<init>(r14, r7)
            r0.F0 = r1
            int[] r3 = defpackage.o23.ExtendedFloatingActionButton
            int[] r6 = new int[r10]
            r1 = r14
            r2 = r18
            r4 = r19
            r5 = r9
            android.content.res.TypedArray r1 = defpackage.a54.h(r1, r2, r3, r4, r5, r6)
            int r2 = defpackage.o23.ExtendedFloatingActionButton_showMotionSpec
            z92 r2 = defpackage.z92.c(r14, r1, r2)
            int r3 = defpackage.o23.ExtendedFloatingActionButton_hideMotionSpec
            z92 r3 = defpackage.z92.c(r14, r1, r3)
            int r4 = defpackage.o23.ExtendedFloatingActionButton_extendMotionSpec
            z92 r4 = defpackage.z92.c(r14, r1, r4)
            int r5 = defpackage.o23.ExtendedFloatingActionButton_shrinkMotionSpec
            z92 r5 = defpackage.z92.c(r14, r1, r5)
            int r6 = defpackage.o23.ExtendedFloatingActionButton_collapsedSize
            r15 = -1
            int r6 = r1.getDimensionPixelSize(r6, r15)
            r0.C0 = r6
            int r6 = defpackage.ei4.J(r16)
            r0.D0 = r6
            int r6 = defpackage.ei4.I(r16)
            r0.E0 = r6
            se r6 = new se
            r6.<init>()
            com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton$h r15 = new com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton$h
            com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton$a r10 = new com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton$a
            r10.<init>()
            r15.<init>(r6, r10, r13)
            r0.z0 = r15
            com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton$h r10 = new com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton$h
            com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton$b r13 = new com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton$b
            r13.<init>()
            r7 = 0
            r10.<init>(r6, r13, r7)
            r0.y0 = r10
            r11.j(r2)
            r12.j(r3)
            r15.j(r4)
            r10.j(r5)
            r1.recycle()
            v80 r1 = defpackage.pn3.m
            r2 = r18
            pn3$b r1 = defpackage.pn3.g(r14, r2, r8, r9, r1)
            pn3 r1 = r1.m()
            r0.setShapeAppearanceModel(r1)
            r16.B()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }

    @Override // android.widget.TextView
    public void setTextColor(ColorStateList colorStateList) {
        super.setTextColor(colorStateList);
        B();
    }
}
