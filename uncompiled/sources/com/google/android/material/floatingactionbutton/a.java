package com.google.android.material.floatingactionbutton;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.FloatEvaluator;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.content.res.ColorStateList;
import android.graphics.Matrix;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.view.View;
import android.view.ViewTreeObserver;
import com.github.mikephil.charting.utils.Utils;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: FloatingActionButtonImpl.java */
/* loaded from: classes2.dex */
public class a {
    public static final TimeInterpolator F = ne.c;
    public static final int[] G = {16842919, 16842910};
    public static final int[] H = {16843623, 16842908, 16842910};
    public static final int[] I = {16842908, 16842910};
    public static final int[] J = {16843623, 16842910};
    public static final int[] K = {16842910};
    public static final int[] L = new int[0];
    public ViewTreeObserver.OnPreDrawListener E;
    public pn3 a;
    public o42 b;
    public Drawable c;
    public dr d;
    public Drawable e;
    public boolean f;
    public float h;
    public float i;
    public float j;
    public int k;
    public final zs3 l;
    public z92 m;
    public z92 n;
    public Animator o;
    public z92 p;
    public z92 q;
    public float r;
    public int t;
    public ArrayList<Animator.AnimatorListener> v;
    public ArrayList<Animator.AnimatorListener> w;
    public ArrayList<i> x;
    public final FloatingActionButton y;
    public final on3 z;
    public boolean g = true;
    public float s = 1.0f;
    public int u = 0;
    public final Rect A = new Rect();
    public final RectF B = new RectF();
    public final RectF C = new RectF();
    public final Matrix D = new Matrix();

    /* compiled from: FloatingActionButtonImpl.java */
    /* renamed from: com.google.android.material.floatingactionbutton.a$a  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public class C0121a extends AnimatorListenerAdapter {
        public boolean a;
        public final /* synthetic */ boolean f0;
        public final /* synthetic */ j g0;

        public C0121a(boolean z, j jVar) {
            this.f0 = z;
            this.g0 = jVar;
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationCancel(Animator animator) {
            this.a = true;
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            a.this.u = 0;
            a.this.o = null;
            if (this.a) {
                return;
            }
            FloatingActionButton floatingActionButton = a.this.y;
            boolean z = this.f0;
            floatingActionButton.b(z ? 8 : 4, z);
            j jVar = this.g0;
            if (jVar != null) {
                jVar.b();
            }
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationStart(Animator animator) {
            a.this.y.b(0, this.f0);
            a.this.u = 1;
            a.this.o = animator;
            this.a = false;
        }
    }

    /* compiled from: FloatingActionButtonImpl.java */
    /* loaded from: classes2.dex */
    public class b extends AnimatorListenerAdapter {
        public final /* synthetic */ boolean a;
        public final /* synthetic */ j f0;

        public b(boolean z, j jVar) {
            this.a = z;
            this.f0 = jVar;
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            a.this.u = 0;
            a.this.o = null;
            j jVar = this.f0;
            if (jVar != null) {
                jVar.a();
            }
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationStart(Animator animator) {
            a.this.y.b(0, this.a);
            a.this.u = 2;
            a.this.o = animator;
        }
    }

    /* compiled from: FloatingActionButtonImpl.java */
    /* loaded from: classes2.dex */
    public class c extends a52 {
        public c() {
        }

        @Override // android.animation.TypeEvaluator
        /* renamed from: a */
        public Matrix evaluate(float f, Matrix matrix, Matrix matrix2) {
            a.this.s = f;
            return super.a(f, matrix, matrix2);
        }
    }

    /* compiled from: FloatingActionButtonImpl.java */
    /* loaded from: classes2.dex */
    public class d implements TypeEvaluator<Float> {
        public FloatEvaluator a = new FloatEvaluator();

        public d(a aVar) {
        }

        @Override // android.animation.TypeEvaluator
        /* renamed from: a */
        public Float evaluate(float f, Float f2, Float f3) {
            float floatValue = this.a.evaluate(f, (Number) f2, (Number) f3).floatValue();
            if (floatValue < 0.1f) {
                floatValue = Utils.FLOAT_EPSILON;
            }
            return Float.valueOf(floatValue);
        }
    }

    /* compiled from: FloatingActionButtonImpl.java */
    /* loaded from: classes2.dex */
    public class e implements ViewTreeObserver.OnPreDrawListener {
        public e() {
        }

        @Override // android.view.ViewTreeObserver.OnPreDrawListener
        public boolean onPreDraw() {
            a.this.H();
            return true;
        }
    }

    /* compiled from: FloatingActionButtonImpl.java */
    /* loaded from: classes2.dex */
    public class f extends l {
        public f(a aVar) {
            super(aVar, null);
        }

        @Override // com.google.android.material.floatingactionbutton.a.l
        public float a() {
            return Utils.FLOAT_EPSILON;
        }
    }

    /* compiled from: FloatingActionButtonImpl.java */
    /* loaded from: classes2.dex */
    public class g extends l {
        public g() {
            super(a.this, null);
        }

        @Override // com.google.android.material.floatingactionbutton.a.l
        public float a() {
            a aVar = a.this;
            return aVar.h + aVar.i;
        }
    }

    /* compiled from: FloatingActionButtonImpl.java */
    /* loaded from: classes2.dex */
    public class h extends l {
        public h() {
            super(a.this, null);
        }

        @Override // com.google.android.material.floatingactionbutton.a.l
        public float a() {
            a aVar = a.this;
            return aVar.h + aVar.j;
        }
    }

    /* compiled from: FloatingActionButtonImpl.java */
    /* loaded from: classes2.dex */
    public interface i {
        void a();

        void b();
    }

    /* compiled from: FloatingActionButtonImpl.java */
    /* loaded from: classes2.dex */
    public interface j {
        void a();

        void b();
    }

    /* compiled from: FloatingActionButtonImpl.java */
    /* loaded from: classes2.dex */
    public class k extends l {
        public k() {
            super(a.this, null);
        }

        @Override // com.google.android.material.floatingactionbutton.a.l
        public float a() {
            return a.this.h;
        }
    }

    /* compiled from: FloatingActionButtonImpl.java */
    /* loaded from: classes2.dex */
    public abstract class l extends AnimatorListenerAdapter implements ValueAnimator.AnimatorUpdateListener {
        public boolean a;
        public float f0;
        public float g0;

        public l() {
        }

        public abstract float a();

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            a.this.g0((int) this.g0);
            this.a = false;
        }

        @Override // android.animation.ValueAnimator.AnimatorUpdateListener
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            if (!this.a) {
                o42 o42Var = a.this.b;
                this.f0 = o42Var == null ? Utils.FLOAT_EPSILON : o42Var.w();
                this.g0 = a();
                this.a = true;
            }
            a aVar = a.this;
            float f = this.f0;
            aVar.g0((int) (f + ((this.g0 - f) * valueAnimator.getAnimatedFraction())));
        }

        public /* synthetic */ l(a aVar, C0121a c0121a) {
            this();
        }
    }

    public a(FloatingActionButton floatingActionButton, on3 on3Var) {
        this.y = floatingActionButton;
        this.z = on3Var;
        zs3 zs3Var = new zs3();
        this.l = zs3Var;
        zs3Var.a(G, i(new h()));
        zs3Var.a(H, i(new g()));
        zs3Var.a(I, i(new g()));
        zs3Var.a(J, i(new g()));
        zs3Var.a(K, i(new k()));
        zs3Var.a(L, i(new f(this)));
        this.r = floatingActionButton.getRotation();
    }

    public void A() {
        this.l.c();
    }

    public void B() {
        o42 o42Var = this.b;
        if (o42Var != null) {
            p42.f(this.y, o42Var);
        }
        if (K()) {
            this.y.getViewTreeObserver().addOnPreDrawListener(r());
        }
    }

    public void C() {
    }

    public void D() {
        ViewTreeObserver viewTreeObserver = this.y.getViewTreeObserver();
        ViewTreeObserver.OnPreDrawListener onPreDrawListener = this.E;
        if (onPreDrawListener != null) {
            viewTreeObserver.removeOnPreDrawListener(onPreDrawListener);
            this.E = null;
        }
    }

    public void E(int[] iArr) {
        this.l.d(iArr);
    }

    public void F(float f2, float f3, float f4) {
        f0();
        g0(f2);
    }

    public void G(Rect rect) {
        du2.f(this.e, "Didn't initialize content background");
        if (Z()) {
            this.z.c(new InsetDrawable(this.e, rect.left, rect.top, rect.right, rect.bottom));
            return;
        }
        this.z.c(this.e);
    }

    public void H() {
        float rotation = this.y.getRotation();
        if (this.r != rotation) {
            this.r = rotation;
            d0();
        }
    }

    public void I() {
        ArrayList<i> arrayList = this.x;
        if (arrayList != null) {
            Iterator<i> it = arrayList.iterator();
            while (it.hasNext()) {
                it.next().b();
            }
        }
    }

    public void J() {
        ArrayList<i> arrayList = this.x;
        if (arrayList != null) {
            Iterator<i> it = arrayList.iterator();
            while (it.hasNext()) {
                it.next().a();
            }
        }
    }

    public boolean K() {
        return true;
    }

    public void L(ColorStateList colorStateList) {
        o42 o42Var = this.b;
        if (o42Var != null) {
            o42Var.setTintList(colorStateList);
        }
        dr drVar = this.d;
        if (drVar != null) {
            drVar.c(colorStateList);
        }
    }

    public void M(PorterDuff.Mode mode) {
        o42 o42Var = this.b;
        if (o42Var != null) {
            o42Var.setTintMode(mode);
        }
    }

    public final void N(float f2) {
        if (this.h != f2) {
            this.h = f2;
            F(f2, this.i, this.j);
        }
    }

    public void O(boolean z) {
        this.f = z;
    }

    public final void P(z92 z92Var) {
        this.q = z92Var;
    }

    public final void Q(float f2) {
        if (this.i != f2) {
            this.i = f2;
            F(this.h, f2, this.j);
        }
    }

    public final void R(float f2) {
        this.s = f2;
        Matrix matrix = this.D;
        g(f2, matrix);
        this.y.setImageMatrix(matrix);
    }

    public final void S(int i2) {
        if (this.t != i2) {
            this.t = i2;
            e0();
        }
    }

    public void T(int i2) {
        this.k = i2;
    }

    public final void U(float f2) {
        if (this.j != f2) {
            this.j = f2;
            F(this.h, this.i, f2);
        }
    }

    public void V(ColorStateList colorStateList) {
        Drawable drawable = this.c;
        if (drawable != null) {
            androidx.core.graphics.drawable.a.o(drawable, b93.d(colorStateList));
        }
    }

    public void W(boolean z) {
        this.g = z;
        f0();
    }

    public final void X(pn3 pn3Var) {
        this.a = pn3Var;
        o42 o42Var = this.b;
        if (o42Var != null) {
            o42Var.setShapeAppearanceModel(pn3Var);
        }
        Drawable drawable = this.c;
        if (drawable instanceof sn3) {
            ((sn3) drawable).setShapeAppearanceModel(pn3Var);
        }
        dr drVar = this.d;
        if (drVar != null) {
            drVar.f(pn3Var);
        }
    }

    public final void Y(z92 z92Var) {
        this.p = z92Var;
    }

    public boolean Z() {
        return true;
    }

    public final boolean a0() {
        return ei4.W(this.y) && !this.y.isInEditMode();
    }

    public final boolean b0() {
        return !this.f || this.y.getSizeDimension() >= this.k;
    }

    public void c0(j jVar, boolean z) {
        if (z()) {
            return;
        }
        Animator animator = this.o;
        if (animator != null) {
            animator.cancel();
        }
        if (a0()) {
            if (this.y.getVisibility() != 0) {
                this.y.setAlpha(Utils.FLOAT_EPSILON);
                this.y.setScaleY(Utils.FLOAT_EPSILON);
                this.y.setScaleX(Utils.FLOAT_EPSILON);
                R(Utils.FLOAT_EPSILON);
            }
            z92 z92Var = this.p;
            if (z92Var == null) {
                z92Var = m();
            }
            AnimatorSet h2 = h(z92Var, 1.0f, 1.0f, 1.0f);
            h2.addListener(new b(z, jVar));
            ArrayList<Animator.AnimatorListener> arrayList = this.v;
            if (arrayList != null) {
                Iterator<Animator.AnimatorListener> it = arrayList.iterator();
                while (it.hasNext()) {
                    h2.addListener(it.next());
                }
            }
            h2.start();
            return;
        }
        this.y.b(0, z);
        this.y.setAlpha(1.0f);
        this.y.setScaleY(1.0f);
        this.y.setScaleX(1.0f);
        R(1.0f);
        if (jVar != null) {
            jVar.a();
        }
    }

    public void d(Animator.AnimatorListener animatorListener) {
        if (this.w == null) {
            this.w = new ArrayList<>();
        }
        this.w.add(animatorListener);
    }

    public void d0() {
        if (Build.VERSION.SDK_INT == 19) {
            if (this.r % 90.0f != Utils.FLOAT_EPSILON) {
                if (this.y.getLayerType() != 1) {
                    this.y.setLayerType(1, null);
                }
            } else if (this.y.getLayerType() != 0) {
                this.y.setLayerType(0, null);
            }
        }
        o42 o42Var = this.b;
        if (o42Var != null) {
            o42Var.h0((int) this.r);
        }
    }

    public void e(Animator.AnimatorListener animatorListener) {
        if (this.v == null) {
            this.v = new ArrayList<>();
        }
        this.v.add(animatorListener);
    }

    public final void e0() {
        R(this.s);
    }

    public void f(i iVar) {
        if (this.x == null) {
            this.x = new ArrayList<>();
        }
        this.x.add(iVar);
    }

    public final void f0() {
        Rect rect = this.A;
        s(rect);
        G(rect);
        this.z.a(rect.left, rect.top, rect.right, rect.bottom);
    }

    public final void g(float f2, Matrix matrix) {
        matrix.reset();
        Drawable drawable = this.y.getDrawable();
        if (drawable == null || this.t == 0) {
            return;
        }
        RectF rectF = this.B;
        RectF rectF2 = this.C;
        rectF.set(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        int i2 = this.t;
        rectF2.set(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, i2, i2);
        matrix.setRectToRect(rectF, rectF2, Matrix.ScaleToFit.CENTER);
        int i3 = this.t;
        matrix.postScale(f2, f2, i3 / 2.0f, i3 / 2.0f);
    }

    public void g0(float f2) {
        o42 o42Var = this.b;
        if (o42Var != null) {
            o42Var.Z(f2);
        }
    }

    public final AnimatorSet h(z92 z92Var, float f2, float f3, float f4) {
        ArrayList arrayList = new ArrayList();
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this.y, View.ALPHA, f2);
        z92Var.h("opacity").a(ofFloat);
        arrayList.add(ofFloat);
        ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(this.y, View.SCALE_X, f3);
        z92Var.h("scale").a(ofFloat2);
        h0(ofFloat2);
        arrayList.add(ofFloat2);
        ObjectAnimator ofFloat3 = ObjectAnimator.ofFloat(this.y, View.SCALE_Y, f3);
        z92Var.h("scale").a(ofFloat3);
        h0(ofFloat3);
        arrayList.add(ofFloat3);
        g(f4, this.D);
        ObjectAnimator ofObject = ObjectAnimator.ofObject(this.y, new do1(), new c(), new Matrix(this.D));
        z92Var.h("iconScale").a(ofObject);
        arrayList.add(ofObject);
        AnimatorSet animatorSet = new AnimatorSet();
        re.a(animatorSet, arrayList);
        return animatorSet;
    }

    public final void h0(ObjectAnimator objectAnimator) {
        if (Build.VERSION.SDK_INT != 26) {
            return;
        }
        objectAnimator.setEvaluator(new d(this));
    }

    public final ValueAnimator i(l lVar) {
        ValueAnimator valueAnimator = new ValueAnimator();
        valueAnimator.setInterpolator(F);
        valueAnimator.setDuration(100L);
        valueAnimator.addListener(lVar);
        valueAnimator.addUpdateListener(lVar);
        valueAnimator.setFloatValues(Utils.FLOAT_EPSILON, 1.0f);
        return valueAnimator;
    }

    public o42 j() {
        return new o42((pn3) du2.e(this.a));
    }

    public final Drawable k() {
        return this.e;
    }

    public final z92 l() {
        if (this.n == null) {
            this.n = z92.d(this.y.getContext(), vx2.design_fab_hide_motion_spec);
        }
        return (z92) du2.e(this.n);
    }

    public final z92 m() {
        if (this.m == null) {
            this.m = z92.d(this.y.getContext(), vx2.design_fab_show_motion_spec);
        }
        return (z92) du2.e(this.m);
    }

    public float n() {
        return this.h;
    }

    public boolean o() {
        return this.f;
    }

    public final z92 p() {
        return this.q;
    }

    public float q() {
        return this.i;
    }

    public final ViewTreeObserver.OnPreDrawListener r() {
        if (this.E == null) {
            this.E = new e();
        }
        return this.E;
    }

    public void s(Rect rect) {
        int sizeDimension = this.f ? (this.k - this.y.getSizeDimension()) / 2 : 0;
        float n = this.g ? n() + this.j : Utils.FLOAT_EPSILON;
        int max = Math.max(sizeDimension, (int) Math.ceil(n));
        int max2 = Math.max(sizeDimension, (int) Math.ceil(n * 1.5f));
        rect.set(max, max2, max, max2);
    }

    public float t() {
        return this.j;
    }

    public final pn3 u() {
        return this.a;
    }

    public final z92 v() {
        return this.p;
    }

    public void w(j jVar, boolean z) {
        if (y()) {
            return;
        }
        Animator animator = this.o;
        if (animator != null) {
            animator.cancel();
        }
        if (a0()) {
            z92 z92Var = this.q;
            if (z92Var == null) {
                z92Var = l();
            }
            AnimatorSet h2 = h(z92Var, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON);
            h2.addListener(new C0121a(z, jVar));
            ArrayList<Animator.AnimatorListener> arrayList = this.w;
            if (arrayList != null) {
                Iterator<Animator.AnimatorListener> it = arrayList.iterator();
                while (it.hasNext()) {
                    h2.addListener(it.next());
                }
            }
            h2.start();
            return;
        }
        this.y.b(z ? 8 : 4, z);
        if (jVar != null) {
            jVar.b();
        }
    }

    public void x(ColorStateList colorStateList, PorterDuff.Mode mode, ColorStateList colorStateList2, int i2) {
        o42 j2 = j();
        this.b = j2;
        j2.setTintList(colorStateList);
        if (mode != null) {
            this.b.setTintMode(mode);
        }
        this.b.g0(-12303292);
        this.b.P(this.y.getContext());
        a93 a93Var = new a93(this.b.D());
        a93Var.setTintList(b93.d(colorStateList2));
        this.c = a93Var;
        this.e = new LayerDrawable(new Drawable[]{(Drawable) du2.e(this.b), a93Var});
    }

    public boolean y() {
        return this.y.getVisibility() == 0 ? this.u == 1 : this.u != 2;
    }

    public boolean z() {
        return this.y.getVisibility() != 0 ? this.u == 2 : this.u != 1;
    }
}
