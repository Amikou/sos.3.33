package com.google.android.material.checkbox;

import android.content.Context;
import android.content.res.ColorStateList;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatCheckBox;

/* loaded from: classes2.dex */
public class MaterialCheckBox extends AppCompatCheckBox {
    public static final int j0 = y13.Widget_MaterialComponents_CompoundButton_CheckBox;
    public static final int[][] k0 = {new int[]{16842910, 16842912}, new int[]{16842910, -16842912}, new int[]{-16842910, 16842912}, new int[]{-16842910, -16842912}};
    public ColorStateList h0;
    public boolean i0;

    public MaterialCheckBox(Context context) {
        this(context, null);
    }

    private ColorStateList getMaterialThemeColorsTintList() {
        if (this.h0 == null) {
            int[][] iArr = k0;
            int[] iArr2 = new int[iArr.length];
            int d = l42.d(this, gy2.colorControlActivated);
            int d2 = l42.d(this, gy2.colorSurface);
            int d3 = l42.d(this, gy2.colorOnSurface);
            iArr2[0] = l42.h(d2, d, 1.0f);
            iArr2[1] = l42.h(d2, d3, 0.54f);
            iArr2[2] = l42.h(d2, d3, 0.38f);
            iArr2[3] = l42.h(d2, d3, 0.38f);
            this.h0 = new ColorStateList(iArr, iArr2);
        }
        return this.h0;
    }

    @Override // android.widget.TextView, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.i0 && r40.b(this) == null) {
            setUseMaterialThemeColors(true);
        }
    }

    public void setUseMaterialThemeColors(boolean z) {
        this.i0 = z;
        if (z) {
            r40.c(this, getMaterialThemeColorsTintList());
        } else {
            r40.c(this, null);
        }
    }

    public MaterialCheckBox(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, gy2.checkboxStyle);
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public MaterialCheckBox(android.content.Context r8, android.util.AttributeSet r9, int r10) {
        /*
            r7 = this;
            int r4 = com.google.android.material.checkbox.MaterialCheckBox.j0
            android.content.Context r8 = defpackage.r42.c(r8, r9, r10, r4)
            r7.<init>(r8, r9, r10)
            android.content.Context r8 = r7.getContext()
            int[] r2 = defpackage.o23.MaterialCheckBox
            r6 = 0
            int[] r5 = new int[r6]
            r0 = r8
            r1 = r9
            r3 = r10
            android.content.res.TypedArray r9 = defpackage.a54.h(r0, r1, r2, r3, r4, r5)
            int r10 = defpackage.o23.MaterialCheckBox_buttonTint
            boolean r0 = r9.hasValue(r10)
            if (r0 == 0) goto L28
            android.content.res.ColorStateList r8 = defpackage.n42.b(r8, r9, r10)
            defpackage.r40.c(r7, r8)
        L28:
            int r8 = defpackage.o23.MaterialCheckBox_useMaterialThemeColors
            boolean r8 = r9.getBoolean(r8, r6)
            r7.i0 = r8
            r9.recycle()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.checkbox.MaterialCheckBox.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }
}
