package com.google.android.material.tabs;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.database.DataSetObserver;
import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.text.Layout;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.media3.common.PlaybackException;
import androidx.viewpager.widget.ViewPager;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.material.badge.BadgeDrawable;
import defpackage.b6;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;

@ViewPager.e
/* loaded from: classes2.dex */
public class TabLayout extends HorizontalScrollView {
    public static final int U0 = y13.Widget_Design_TabLayout;
    public static final et2<g> V0 = new it2(16);
    public int A0;
    public int B0;
    public int C0;
    public int D0;
    public boolean E0;
    public boolean F0;
    public int G0;
    public boolean H0;
    public com.google.android.material.tabs.a I0;
    public c J0;
    public final ArrayList<c> K0;
    public c L0;
    public ValueAnimator M0;
    public ViewPager N0;
    public dp2 O0;
    public DataSetObserver P0;
    public h Q0;
    public b R0;
    public boolean S0;
    public final et2<TabView> T0;
    public final ArrayList<g> a;
    public g f0;
    public final f g0;
    public int h0;
    public int i0;
    public int j0;
    public int k0;
    public int l0;
    public ColorStateList m0;
    public ColorStateList n0;
    public ColorStateList o0;
    public Drawable p0;
    public int q0;
    public PorterDuff.Mode r0;
    public float s0;
    public float t0;
    public final int u0;
    public int v0;
    public final int w0;
    public final int x0;
    public final int y0;
    public int z0;

    /* loaded from: classes2.dex */
    public final class TabView extends LinearLayout {
        public g a;
        public TextView f0;
        public ImageView g0;
        public View h0;
        public BadgeDrawable i0;
        public View j0;
        public TextView k0;
        public ImageView l0;
        public Drawable m0;
        public int n0;

        /* loaded from: classes2.dex */
        public class a implements View.OnLayoutChangeListener {
            public final /* synthetic */ View a;

            public a(View view) {
                this.a = view;
            }

            @Override // android.view.View.OnLayoutChangeListener
            public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                if (this.a.getVisibility() == 0) {
                    TabView.this.s(this.a);
                }
            }
        }

        public TabView(Context context) {
            super(context);
            this.n0 = 2;
            u(context);
            ei4.H0(this, TabLayout.this.h0, TabLayout.this.i0, TabLayout.this.j0, TabLayout.this.k0);
            setGravity(17);
            setOrientation(!TabLayout.this.E0 ? 1 : 0);
            setClickable(true);
            ei4.I0(this, ms2.b(getContext(), PlaybackException.ERROR_CODE_BEHIND_LIVE_WINDOW));
        }

        private BadgeDrawable getBadge() {
            return this.i0;
        }

        private BadgeDrawable getOrCreateBadge() {
            if (this.i0 == null) {
                this.i0 = BadgeDrawable.c(getContext());
            }
            r();
            BadgeDrawable badgeDrawable = this.i0;
            if (badgeDrawable != null) {
                return badgeDrawable;
            }
            throw new IllegalStateException("Unable to create badge");
        }

        @Override // android.view.ViewGroup, android.view.View
        public void drawableStateChanged() {
            super.drawableStateChanged();
            int[] drawableState = getDrawableState();
            Drawable drawable = this.m0;
            boolean z = false;
            if (drawable != null && drawable.isStateful()) {
                z = false | this.m0.setState(drawableState);
            }
            if (z) {
                invalidate();
                TabLayout.this.invalidate();
            }
        }

        public final void f(View view) {
            if (view == null) {
                return;
            }
            view.addOnLayoutChangeListener(new a(view));
        }

        public final float g(Layout layout, int i, float f) {
            return layout.getLineWidth(i) * (f / layout.getPaint().getTextSize());
        }

        public int getContentHeight() {
            View[] viewArr = {this.f0, this.g0, this.j0};
            int i = 0;
            int i2 = 0;
            boolean z = false;
            for (int i3 = 0; i3 < 3; i3++) {
                View view = viewArr[i3];
                if (view != null && view.getVisibility() == 0) {
                    i2 = z ? Math.min(i2, view.getTop()) : view.getTop();
                    i = z ? Math.max(i, view.getBottom()) : view.getBottom();
                    z = true;
                }
            }
            return i - i2;
        }

        public int getContentWidth() {
            View[] viewArr = {this.f0, this.g0, this.j0};
            int i = 0;
            int i2 = 0;
            boolean z = false;
            for (int i3 = 0; i3 < 3; i3++) {
                View view = viewArr[i3];
                if (view != null && view.getVisibility() == 0) {
                    i2 = z ? Math.min(i2, view.getLeft()) : view.getLeft();
                    i = z ? Math.max(i, view.getRight()) : view.getRight();
                    z = true;
                }
            }
            return i - i2;
        }

        public g getTab() {
            return this.a;
        }

        public final void h(boolean z) {
            setClipChildren(z);
            setClipToPadding(z);
            ViewGroup viewGroup = (ViewGroup) getParent();
            if (viewGroup != null) {
                viewGroup.setClipChildren(z);
                viewGroup.setClipToPadding(z);
            }
        }

        public final FrameLayout i() {
            FrameLayout frameLayout = new FrameLayout(getContext());
            frameLayout.setLayoutParams(new FrameLayout.LayoutParams(-2, -2));
            return frameLayout;
        }

        public final void j(Canvas canvas) {
            Drawable drawable = this.m0;
            if (drawable != null) {
                drawable.setBounds(getLeft(), getTop(), getRight(), getBottom());
                this.m0.draw(canvas);
            }
        }

        public final FrameLayout k(View view) {
            if ((view == this.g0 || view == this.f0) && com.google.android.material.badge.a.a) {
                return (FrameLayout) view.getParent();
            }
            return null;
        }

        public final boolean l() {
            return this.i0 != null;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public final void m() {
            FrameLayout frameLayout;
            if (com.google.android.material.badge.a.a) {
                frameLayout = i();
                addView(frameLayout, 0);
            } else {
                frameLayout = this;
            }
            ImageView imageView = (ImageView) LayoutInflater.from(getContext()).inflate(x03.design_layout_tab_icon, (ViewGroup) frameLayout, false);
            this.g0 = imageView;
            frameLayout.addView(imageView, 0);
        }

        /* JADX WARN: Multi-variable type inference failed */
        public final void n() {
            FrameLayout frameLayout;
            if (com.google.android.material.badge.a.a) {
                frameLayout = i();
                addView(frameLayout);
            } else {
                frameLayout = this;
            }
            TextView textView = (TextView) LayoutInflater.from(getContext()).inflate(x03.design_layout_tab_text, (ViewGroup) frameLayout, false);
            this.f0 = textView;
            frameLayout.addView(textView);
        }

        public void o() {
            setTab(null);
            setSelected(false);
        }

        @Override // android.view.View
        public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
            super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
            BadgeDrawable badgeDrawable = this.i0;
            if (badgeDrawable != null && badgeDrawable.isVisible()) {
                CharSequence contentDescription = getContentDescription();
                accessibilityNodeInfo.setContentDescription(((Object) contentDescription) + ", " + ((Object) this.i0.h()));
            }
            b6 I0 = b6.I0(accessibilityNodeInfo);
            I0.f0(b6.c.a(0, 1, this.a.g(), 1, false, isSelected()));
            if (isSelected()) {
                I0.d0(false);
                I0.T(b6.a.g);
            }
            I0.w0(getResources().getString(r13.item_view_role_description));
        }

        @Override // android.widget.LinearLayout, android.view.View
        public void onMeasure(int i, int i2) {
            Layout layout;
            int size = View.MeasureSpec.getSize(i);
            int mode = View.MeasureSpec.getMode(i);
            int tabMaxWidth = TabLayout.this.getTabMaxWidth();
            if (tabMaxWidth > 0 && (mode == 0 || size > tabMaxWidth)) {
                i = View.MeasureSpec.makeMeasureSpec(TabLayout.this.v0, Integer.MIN_VALUE);
            }
            super.onMeasure(i, i2);
            if (this.f0 != null) {
                float f = TabLayout.this.s0;
                int i3 = this.n0;
                ImageView imageView = this.g0;
                boolean z = true;
                if (imageView == null || imageView.getVisibility() != 0) {
                    TextView textView = this.f0;
                    if (textView != null && textView.getLineCount() > 1) {
                        f = TabLayout.this.t0;
                    }
                } else {
                    i3 = 1;
                }
                float textSize = this.f0.getTextSize();
                int lineCount = this.f0.getLineCount();
                int d = t44.d(this.f0);
                int i4 = (f > textSize ? 1 : (f == textSize ? 0 : -1));
                if (i4 != 0 || (d >= 0 && i3 != d)) {
                    if (TabLayout.this.D0 == 1 && i4 > 0 && lineCount == 1 && ((layout = this.f0.getLayout()) == null || g(layout, 0, f) > (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight())) {
                        z = false;
                    }
                    if (z) {
                        this.f0.setTextSize(0, f);
                        this.f0.setMaxLines(i3);
                        super.onMeasure(i, i2);
                    }
                }
            }
        }

        public final void p(View view) {
            if (l() && view != null) {
                h(false);
                com.google.android.material.badge.a.a(this.i0, view, k(view));
                this.h0 = view;
            }
        }

        @Override // android.view.View
        public boolean performClick() {
            boolean performClick = super.performClick();
            if (this.a != null) {
                if (!performClick) {
                    playSoundEffect(0);
                }
                this.a.l();
                return true;
            }
            return performClick;
        }

        public final void q() {
            if (l()) {
                h(true);
                View view = this.h0;
                if (view != null) {
                    com.google.android.material.badge.a.d(this.i0, view);
                    this.h0 = null;
                }
            }
        }

        public final void r() {
            g gVar;
            g gVar2;
            if (l()) {
                if (this.j0 != null) {
                    q();
                } else if (this.g0 != null && (gVar2 = this.a) != null && gVar2.f() != null) {
                    View view = this.h0;
                    ImageView imageView = this.g0;
                    if (view != imageView) {
                        q();
                        p(this.g0);
                        return;
                    }
                    s(imageView);
                } else if (this.f0 != null && (gVar = this.a) != null && gVar.h() == 1) {
                    View view2 = this.h0;
                    TextView textView = this.f0;
                    if (view2 != textView) {
                        q();
                        p(this.f0);
                        return;
                    }
                    s(textView);
                } else {
                    q();
                }
            }
        }

        public final void s(View view) {
            if (l() && view == this.h0) {
                com.google.android.material.badge.a.e(this.i0, view, k(view));
            }
        }

        @Override // android.view.View
        public void setSelected(boolean z) {
            boolean z2 = isSelected() != z;
            super.setSelected(z);
            if (z2 && z && Build.VERSION.SDK_INT < 16) {
                sendAccessibilityEvent(4);
            }
            TextView textView = this.f0;
            if (textView != null) {
                textView.setSelected(z);
            }
            ImageView imageView = this.g0;
            if (imageView != null) {
                imageView.setSelected(z);
            }
            View view = this.j0;
            if (view != null) {
                view.setSelected(z);
            }
        }

        public void setTab(g gVar) {
            if (gVar != this.a) {
                this.a = gVar;
                t();
            }
        }

        public final void t() {
            g gVar = this.a;
            Drawable drawable = null;
            View e = gVar != null ? gVar.e() : null;
            if (e != null) {
                ViewParent parent = e.getParent();
                if (parent != this) {
                    if (parent != null) {
                        ((ViewGroup) parent).removeView(e);
                    }
                    addView(e);
                }
                this.j0 = e;
                TextView textView = this.f0;
                if (textView != null) {
                    textView.setVisibility(8);
                }
                ImageView imageView = this.g0;
                if (imageView != null) {
                    imageView.setVisibility(8);
                    this.g0.setImageDrawable(null);
                }
                TextView textView2 = (TextView) e.findViewById(16908308);
                this.k0 = textView2;
                if (textView2 != null) {
                    this.n0 = t44.d(textView2);
                }
                this.l0 = (ImageView) e.findViewById(16908294);
            } else {
                View view = this.j0;
                if (view != null) {
                    removeView(view);
                    this.j0 = null;
                }
                this.k0 = null;
                this.l0 = null;
            }
            if (this.j0 == null) {
                if (this.g0 == null) {
                    m();
                }
                if (gVar != null && gVar.f() != null) {
                    drawable = androidx.core.graphics.drawable.a.r(gVar.f()).mutate();
                }
                if (drawable != null) {
                    androidx.core.graphics.drawable.a.o(drawable, TabLayout.this.n0);
                    PorterDuff.Mode mode = TabLayout.this.r0;
                    if (mode != null) {
                        androidx.core.graphics.drawable.a.p(drawable, mode);
                    }
                }
                if (this.f0 == null) {
                    n();
                    this.n0 = t44.d(this.f0);
                }
                t44.q(this.f0, TabLayout.this.l0);
                ColorStateList colorStateList = TabLayout.this.m0;
                if (colorStateList != null) {
                    this.f0.setTextColor(colorStateList);
                }
                w(this.f0, this.g0);
                r();
                f(this.g0);
                f(this.f0);
            } else {
                TextView textView3 = this.k0;
                if (textView3 != null || this.l0 != null) {
                    w(textView3, this.l0);
                }
            }
            if (gVar != null && !TextUtils.isEmpty(gVar.c)) {
                setContentDescription(gVar.c);
            }
            setSelected(gVar != null && gVar.j());
        }

        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r0v3, types: [android.graphics.drawable.RippleDrawable] */
        /* JADX WARN: Type inference failed for: r2v3, types: [android.graphics.drawable.LayerDrawable] */
        public final void u(Context context) {
            int i = TabLayout.this.u0;
            if (i != 0) {
                Drawable d = mf.d(context, i);
                this.m0 = d;
                if (d != null && d.isStateful()) {
                    this.m0.setState(getDrawableState());
                }
            } else {
                this.m0 = null;
            }
            GradientDrawable gradientDrawable = new GradientDrawable();
            gradientDrawable.setColor(0);
            if (TabLayout.this.o0 != null) {
                GradientDrawable gradientDrawable2 = new GradientDrawable();
                gradientDrawable2.setCornerRadius(1.0E-5f);
                gradientDrawable2.setColor(-1);
                ColorStateList a2 = b93.a(TabLayout.this.o0);
                if (Build.VERSION.SDK_INT >= 21) {
                    boolean z = TabLayout.this.H0;
                    if (z) {
                        gradientDrawable = null;
                    }
                    gradientDrawable = new RippleDrawable(a2, gradientDrawable, z ? null : gradientDrawable2);
                } else {
                    Drawable r = androidx.core.graphics.drawable.a.r(gradientDrawable2);
                    androidx.core.graphics.drawable.a.o(r, a2);
                    gradientDrawable = new LayerDrawable(new Drawable[]{gradientDrawable, r});
                }
            }
            ei4.w0(this, gradientDrawable);
            TabLayout.this.invalidate();
        }

        public final void v() {
            setOrientation(!TabLayout.this.E0 ? 1 : 0);
            TextView textView = this.k0;
            if (textView == null && this.l0 == null) {
                w(this.f0, this.g0);
            } else {
                w(textView, this.l0);
            }
        }

        public final void w(TextView textView, ImageView imageView) {
            g gVar = this.a;
            Drawable mutate = (gVar == null || gVar.f() == null) ? null : androidx.core.graphics.drawable.a.r(this.a.f()).mutate();
            g gVar2 = this.a;
            CharSequence i = gVar2 != null ? gVar2.i() : null;
            if (imageView != null) {
                if (mutate != null) {
                    imageView.setImageDrawable(mutate);
                    imageView.setVisibility(0);
                    setVisibility(0);
                } else {
                    imageView.setVisibility(8);
                    imageView.setImageDrawable(null);
                }
            }
            boolean z = !TextUtils.isEmpty(i);
            if (textView != null) {
                if (z) {
                    textView.setText(i);
                    if (this.a.f == 1) {
                        textView.setVisibility(0);
                    } else {
                        textView.setVisibility(8);
                    }
                    setVisibility(0);
                } else {
                    textView.setVisibility(8);
                    textView.setText((CharSequence) null);
                }
            }
            if (imageView != null) {
                ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) imageView.getLayoutParams();
                int c = (z && imageView.getVisibility() == 0) ? (int) mk4.c(getContext(), 8) : 0;
                if (TabLayout.this.E0) {
                    if (c != b42.a(marginLayoutParams)) {
                        b42.c(marginLayoutParams, c);
                        marginLayoutParams.bottomMargin = 0;
                        imageView.setLayoutParams(marginLayoutParams);
                        imageView.requestLayout();
                    }
                } else if (c != marginLayoutParams.bottomMargin) {
                    marginLayoutParams.bottomMargin = c;
                    b42.c(marginLayoutParams, 0);
                    imageView.setLayoutParams(marginLayoutParams);
                    imageView.requestLayout();
                }
            }
            g gVar3 = this.a;
            CharSequence charSequence = gVar3 != null ? gVar3.c : null;
            int i2 = Build.VERSION.SDK_INT;
            if (i2 < 21 || i2 > 23) {
                if (!z) {
                    i = charSequence;
                }
                j74.a(this, i);
            }
        }
    }

    /* loaded from: classes2.dex */
    public class a implements ValueAnimator.AnimatorUpdateListener {
        public a() {
        }

        @Override // android.animation.ValueAnimator.AnimatorUpdateListener
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            TabLayout.this.scrollTo(((Integer) valueAnimator.getAnimatedValue()).intValue(), 0);
        }
    }

    /* loaded from: classes2.dex */
    public class b implements ViewPager.h {
        public boolean a;

        public b() {
        }

        public void a(boolean z) {
            this.a = z;
        }

        @Override // androidx.viewpager.widget.ViewPager.h
        public void b(ViewPager viewPager, dp2 dp2Var, dp2 dp2Var2) {
            TabLayout tabLayout = TabLayout.this;
            if (tabLayout.N0 == viewPager) {
                tabLayout.G(dp2Var2, this.a);
            }
        }
    }

    @Deprecated
    /* loaded from: classes2.dex */
    public interface c<T extends g> {
        void a(T t);

        void b(T t);

        void c(T t);
    }

    /* loaded from: classes2.dex */
    public interface d extends c<g> {
    }

    /* loaded from: classes2.dex */
    public class e extends DataSetObserver {
        public e() {
        }

        @Override // android.database.DataSetObserver
        public void onChanged() {
            TabLayout.this.z();
        }

        @Override // android.database.DataSetObserver
        public void onInvalidated() {
            TabLayout.this.z();
        }
    }

    /* loaded from: classes2.dex */
    public class f extends LinearLayout {
        public ValueAnimator a;
        public int f0;
        public float g0;
        public int h0;

        /* loaded from: classes2.dex */
        public class a implements ValueAnimator.AnimatorUpdateListener {
            public final /* synthetic */ View a;
            public final /* synthetic */ View f0;

            public a(View view, View view2) {
                this.a = view;
                this.f0 = view2;
            }

            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                f.this.g(this.a, this.f0, valueAnimator.getAnimatedFraction());
            }
        }

        /* loaded from: classes2.dex */
        public class b extends AnimatorListenerAdapter {
            public final /* synthetic */ int a;

            public b(int i) {
                this.a = i;
            }

            @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
            public void onAnimationEnd(Animator animator) {
                f.this.f0 = this.a;
            }

            @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
            public void onAnimationStart(Animator animator) {
                f.this.f0 = this.a;
            }
        }

        public f(Context context) {
            super(context);
            this.f0 = -1;
            this.h0 = -1;
            setWillNotDraw(false);
        }

        public void b(int i, int i2) {
            ValueAnimator valueAnimator = this.a;
            if (valueAnimator != null && valueAnimator.isRunning()) {
                this.a.cancel();
            }
            h(true, i, i2);
        }

        public boolean c() {
            int childCount = getChildCount();
            for (int i = 0; i < childCount; i++) {
                if (getChildAt(i).getWidth() <= 0) {
                    return true;
                }
            }
            return false;
        }

        public final void d() {
            View childAt = getChildAt(this.f0);
            com.google.android.material.tabs.a aVar = TabLayout.this.I0;
            TabLayout tabLayout = TabLayout.this;
            aVar.d(tabLayout, childAt, tabLayout.p0);
        }

        @Override // android.view.View
        public void draw(Canvas canvas) {
            int height = TabLayout.this.p0.getBounds().height();
            if (height < 0) {
                height = TabLayout.this.p0.getIntrinsicHeight();
            }
            int i = TabLayout.this.C0;
            int i2 = 0;
            if (i == 0) {
                i2 = getHeight() - height;
                height = getHeight();
            } else if (i == 1) {
                i2 = (getHeight() - height) / 2;
                height = (getHeight() + height) / 2;
            } else if (i != 2) {
                height = i != 3 ? 0 : getHeight();
            }
            if (TabLayout.this.p0.getBounds().width() > 0) {
                Rect bounds = TabLayout.this.p0.getBounds();
                TabLayout.this.p0.setBounds(bounds.left, i2, bounds.right, height);
                TabLayout tabLayout = TabLayout.this;
                Drawable drawable = tabLayout.p0;
                if (tabLayout.q0 != 0) {
                    drawable = androidx.core.graphics.drawable.a.r(drawable);
                    if (Build.VERSION.SDK_INT == 21) {
                        drawable.setColorFilter(TabLayout.this.q0, PorterDuff.Mode.SRC_IN);
                    } else {
                        androidx.core.graphics.drawable.a.n(drawable, TabLayout.this.q0);
                    }
                }
                drawable.draw(canvas);
            }
            super.draw(canvas);
        }

        public void e(int i, float f) {
            ValueAnimator valueAnimator = this.a;
            if (valueAnimator != null && valueAnimator.isRunning()) {
                this.a.cancel();
            }
            this.f0 = i;
            this.g0 = f;
            g(getChildAt(i), getChildAt(this.f0 + 1), this.g0);
        }

        public void f(int i) {
            Rect bounds = TabLayout.this.p0.getBounds();
            TabLayout.this.p0.setBounds(bounds.left, 0, bounds.right, i);
            requestLayout();
        }

        public final void g(View view, View view2, float f) {
            if (view != null && view.getWidth() > 0) {
                com.google.android.material.tabs.a aVar = TabLayout.this.I0;
                TabLayout tabLayout = TabLayout.this;
                aVar.c(tabLayout, view, view2, f, tabLayout.p0);
            } else {
                Drawable drawable = TabLayout.this.p0;
                drawable.setBounds(-1, drawable.getBounds().top, -1, TabLayout.this.p0.getBounds().bottom);
            }
            ei4.j0(this);
        }

        public final void h(boolean z, int i, int i2) {
            View childAt = getChildAt(this.f0);
            View childAt2 = getChildAt(i);
            if (childAt2 == null) {
                d();
                return;
            }
            a aVar = new a(childAt, childAt2);
            if (z) {
                ValueAnimator valueAnimator = new ValueAnimator();
                this.a = valueAnimator;
                valueAnimator.setInterpolator(ne.b);
                valueAnimator.setDuration(i2);
                valueAnimator.setFloatValues(Utils.FLOAT_EPSILON, 1.0f);
                valueAnimator.addUpdateListener(aVar);
                valueAnimator.addListener(new b(i));
                valueAnimator.start();
                return;
            }
            this.a.removeAllUpdateListeners();
            this.a.addUpdateListener(aVar);
        }

        @Override // android.widget.LinearLayout, android.view.ViewGroup, android.view.View
        public void onLayout(boolean z, int i, int i2, int i3, int i4) {
            super.onLayout(z, i, i2, i3, i4);
            ValueAnimator valueAnimator = this.a;
            if (valueAnimator != null && valueAnimator.isRunning()) {
                h(false, this.f0, -1);
            } else {
                d();
            }
        }

        @Override // android.widget.LinearLayout, android.view.View
        public void onMeasure(int i, int i2) {
            super.onMeasure(i, i2);
            if (View.MeasureSpec.getMode(i) != 1073741824) {
                return;
            }
            TabLayout tabLayout = TabLayout.this;
            boolean z = true;
            if (tabLayout.A0 == 1 || tabLayout.D0 == 2) {
                int childCount = getChildCount();
                int i3 = 0;
                for (int i4 = 0; i4 < childCount; i4++) {
                    View childAt = getChildAt(i4);
                    if (childAt.getVisibility() == 0) {
                        i3 = Math.max(i3, childAt.getMeasuredWidth());
                    }
                }
                if (i3 <= 0) {
                    return;
                }
                if (i3 * childCount <= getMeasuredWidth() - (((int) mk4.c(getContext(), 16)) * 2)) {
                    boolean z2 = false;
                    for (int i5 = 0; i5 < childCount; i5++) {
                        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) getChildAt(i5).getLayoutParams();
                        if (layoutParams.width != i3 || layoutParams.weight != Utils.FLOAT_EPSILON) {
                            layoutParams.width = i3;
                            layoutParams.weight = Utils.FLOAT_EPSILON;
                            z2 = true;
                        }
                    }
                    z = z2;
                } else {
                    TabLayout tabLayout2 = TabLayout.this;
                    tabLayout2.A0 = 0;
                    tabLayout2.K(false);
                }
                if (z) {
                    super.onMeasure(i, i2);
                }
            }
        }

        @Override // android.widget.LinearLayout, android.view.View
        public void onRtlPropertiesChanged(int i) {
            super.onRtlPropertiesChanged(i);
            if (Build.VERSION.SDK_INT >= 23 || this.h0 == i) {
                return;
            }
            requestLayout();
            this.h0 = i;
        }
    }

    /* loaded from: classes2.dex */
    public static class g {
        public Drawable a;
        public CharSequence b;
        public CharSequence c;
        public View e;
        public TabLayout g;
        public TabView h;
        public int d = -1;
        public int f = 1;
        public int i = -1;

        public View e() {
            return this.e;
        }

        public Drawable f() {
            return this.a;
        }

        public int g() {
            return this.d;
        }

        public int h() {
            return this.f;
        }

        public CharSequence i() {
            return this.b;
        }

        public boolean j() {
            TabLayout tabLayout = this.g;
            if (tabLayout != null) {
                return tabLayout.getSelectedTabPosition() == this.d;
            }
            throw new IllegalArgumentException("Tab not attached to a TabLayout");
        }

        public void k() {
            this.g = null;
            this.h = null;
            this.a = null;
            this.i = -1;
            this.b = null;
            this.c = null;
            this.d = -1;
            this.e = null;
        }

        public void l() {
            TabLayout tabLayout = this.g;
            if (tabLayout != null) {
                tabLayout.E(this);
                return;
            }
            throw new IllegalArgumentException("Tab not attached to a TabLayout");
        }

        public g m(CharSequence charSequence) {
            this.c = charSequence;
            s();
            return this;
        }

        public g n(int i) {
            return o(LayoutInflater.from(this.h.getContext()).inflate(i, (ViewGroup) this.h, false));
        }

        public g o(View view) {
            this.e = view;
            s();
            return this;
        }

        public g p(Drawable drawable) {
            this.a = drawable;
            TabLayout tabLayout = this.g;
            if (tabLayout.A0 == 1 || tabLayout.D0 == 2) {
                tabLayout.K(true);
            }
            s();
            if (com.google.android.material.badge.a.a && this.h.l() && this.h.i0.isVisible()) {
                this.h.invalidate();
            }
            return this;
        }

        public void q(int i) {
            this.d = i;
        }

        public g r(CharSequence charSequence) {
            if (TextUtils.isEmpty(this.c) && !TextUtils.isEmpty(charSequence)) {
                this.h.setContentDescription(charSequence);
            }
            this.b = charSequence;
            s();
            return this;
        }

        public void s() {
            TabView tabView = this.h;
            if (tabView != null) {
                tabView.t();
            }
        }
    }

    /* loaded from: classes2.dex */
    public static class h implements ViewPager.i {
        public final WeakReference<TabLayout> a;
        public int f0;
        public int g0;

        public h(TabLayout tabLayout) {
            this.a = new WeakReference<>(tabLayout);
        }

        @Override // androidx.viewpager.widget.ViewPager.i
        public void a(int i, float f, int i2) {
            TabLayout tabLayout = this.a.get();
            if (tabLayout != null) {
                int i3 = this.g0;
                boolean z = false;
                boolean z2 = i3 != 2 || this.f0 == 1;
                if (i3 != 2 || this.f0 != 0) {
                    z = true;
                }
                tabLayout.setScrollPosition(i, f, z2, z);
            }
        }

        public void b() {
            this.g0 = 0;
            this.f0 = 0;
        }

        @Override // androidx.viewpager.widget.ViewPager.i
        public void d(int i) {
            this.f0 = this.g0;
            this.g0 = i;
        }

        @Override // androidx.viewpager.widget.ViewPager.i
        public void e(int i) {
            TabLayout tabLayout = this.a.get();
            if (tabLayout == null || tabLayout.getSelectedTabPosition() == i || i >= tabLayout.getTabCount()) {
                return;
            }
            int i2 = this.g0;
            tabLayout.F(tabLayout.w(i), i2 == 0 || (i2 == 2 && this.f0 == 0));
        }
    }

    /* loaded from: classes2.dex */
    public static class i implements d {
        public final ViewPager a;

        public i(ViewPager viewPager) {
            this.a = viewPager;
        }

        @Override // com.google.android.material.tabs.TabLayout.c
        public void a(g gVar) {
        }

        @Override // com.google.android.material.tabs.TabLayout.c
        public void b(g gVar) {
            this.a.setCurrentItem(gVar.g());
        }

        @Override // com.google.android.material.tabs.TabLayout.c
        public void c(g gVar) {
        }
    }

    public TabLayout(Context context) {
        this(context, null);
    }

    private int getDefaultHeight() {
        int size = this.a.size();
        boolean z = false;
        int i2 = 0;
        while (true) {
            if (i2 < size) {
                g gVar = this.a.get(i2);
                if (gVar != null && gVar.f() != null && !TextUtils.isEmpty(gVar.i())) {
                    z = true;
                    break;
                }
                i2++;
            } else {
                break;
            }
        }
        return (!z || this.E0) ? 48 : 72;
    }

    private int getTabMinWidth() {
        int i2 = this.w0;
        if (i2 != -1) {
            return i2;
        }
        int i3 = this.D0;
        if (i3 == 0 || i3 == 2) {
            return this.y0;
        }
        return 0;
    }

    private int getTabScrollRange() {
        return Math.max(0, ((this.g0.getWidth() - getWidth()) - getPaddingLeft()) - getPaddingRight());
    }

    public static ColorStateList o(int i2, int i3) {
        return new ColorStateList(new int[][]{HorizontalScrollView.SELECTED_STATE_SET, HorizontalScrollView.EMPTY_STATE_SET}, new int[]{i3, i2});
    }

    private void setSelectedTabView(int i2) {
        int childCount = this.g0.getChildCount();
        if (i2 < childCount) {
            int i3 = 0;
            while (i3 < childCount) {
                View childAt = this.g0.getChildAt(i3);
                boolean z = true;
                childAt.setSelected(i3 == i2);
                if (i3 != i2) {
                    z = false;
                }
                childAt.setActivated(z);
                i3++;
            }
        }
    }

    public boolean A(g gVar) {
        return V0.a(gVar);
    }

    public void B() {
        for (int childCount = this.g0.getChildCount() - 1; childCount >= 0; childCount--) {
            D(childCount);
        }
        Iterator<g> it = this.a.iterator();
        while (it.hasNext()) {
            g next = it.next();
            it.remove();
            next.k();
            A(next);
        }
        this.f0 = null;
    }

    @Deprecated
    public void C(c cVar) {
        this.K0.remove(cVar);
    }

    public final void D(int i2) {
        TabView tabView = (TabView) this.g0.getChildAt(i2);
        this.g0.removeViewAt(i2);
        if (tabView != null) {
            tabView.o();
            this.T0.a(tabView);
        }
        requestLayout();
    }

    public void E(g gVar) {
        F(gVar, true);
    }

    public void F(g gVar, boolean z) {
        g gVar2 = this.f0;
        if (gVar2 == gVar) {
            if (gVar2 != null) {
                s(gVar);
                j(gVar.g());
                return;
            }
            return;
        }
        int g2 = gVar != null ? gVar.g() : -1;
        if (z) {
            if ((gVar2 == null || gVar2.g() == -1) && g2 != -1) {
                setScrollPosition(g2, Utils.FLOAT_EPSILON, true);
            } else {
                j(g2);
            }
            if (g2 != -1) {
                setSelectedTabView(g2);
            }
        }
        this.f0 = gVar;
        if (gVar2 != null) {
            u(gVar2);
        }
        if (gVar != null) {
            t(gVar);
        }
    }

    public void G(dp2 dp2Var, boolean z) {
        DataSetObserver dataSetObserver;
        dp2 dp2Var2 = this.O0;
        if (dp2Var2 != null && (dataSetObserver = this.P0) != null) {
            dp2Var2.t(dataSetObserver);
        }
        this.O0 = dp2Var;
        if (z && dp2Var != null) {
            if (this.P0 == null) {
                this.P0 = new e();
            }
            dp2Var.l(this.P0);
        }
        z();
    }

    public final void H(ViewPager viewPager, boolean z, boolean z2) {
        ViewPager viewPager2 = this.N0;
        if (viewPager2 != null) {
            h hVar = this.Q0;
            if (hVar != null) {
                viewPager2.M(hVar);
            }
            b bVar = this.R0;
            if (bVar != null) {
                this.N0.L(bVar);
            }
        }
        c cVar = this.L0;
        if (cVar != null) {
            C(cVar);
            this.L0 = null;
        }
        if (viewPager != null) {
            this.N0 = viewPager;
            if (this.Q0 == null) {
                this.Q0 = new h(this);
            }
            this.Q0.b();
            viewPager.c(this.Q0);
            i iVar = new i(viewPager);
            this.L0 = iVar;
            c(iVar);
            dp2 adapter = viewPager.getAdapter();
            if (adapter != null) {
                G(adapter, z);
            }
            if (this.R0 == null) {
                this.R0 = new b();
            }
            this.R0.a(z);
            viewPager.b(this.R0);
            setScrollPosition(viewPager.getCurrentItem(), Utils.FLOAT_EPSILON, true);
        } else {
            this.N0 = null;
            G(null, false);
        }
        this.S0 = z2;
    }

    public final void I() {
        int size = this.a.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.a.get(i2).s();
        }
    }

    public final void J(LinearLayout.LayoutParams layoutParams) {
        if (this.D0 == 1 && this.A0 == 0) {
            layoutParams.width = 0;
            layoutParams.weight = 1.0f;
            return;
        }
        layoutParams.width = -2;
        layoutParams.weight = Utils.FLOAT_EPSILON;
    }

    public void K(boolean z) {
        for (int i2 = 0; i2 < this.g0.getChildCount(); i2++) {
            View childAt = this.g0.getChildAt(i2);
            childAt.setMinimumWidth(getTabMinWidth());
            J((LinearLayout.LayoutParams) childAt.getLayoutParams());
            if (z) {
                childAt.requestLayout();
            }
        }
    }

    @Override // android.widget.HorizontalScrollView, android.view.ViewGroup
    public void addView(View view) {
        i(view);
    }

    @Deprecated
    public void c(c cVar) {
        if (this.K0.contains(cVar)) {
            return;
        }
        this.K0.add(cVar);
    }

    public void d(g gVar) {
        f(gVar, this.a.isEmpty());
    }

    public void e(g gVar, int i2, boolean z) {
        if (gVar.g == this) {
            n(gVar, i2);
            h(gVar);
            if (z) {
                gVar.l();
                return;
            }
            return;
        }
        throw new IllegalArgumentException("Tab belongs to a different TabLayout.");
    }

    public void f(g gVar, boolean z) {
        e(gVar, this.a.size(), z);
    }

    public final void g(TabItem tabItem) {
        g y = y();
        CharSequence charSequence = tabItem.a;
        if (charSequence != null) {
            y.r(charSequence);
        }
        Drawable drawable = tabItem.f0;
        if (drawable != null) {
            y.p(drawable);
        }
        int i2 = tabItem.g0;
        if (i2 != 0) {
            y.n(i2);
        }
        if (!TextUtils.isEmpty(tabItem.getContentDescription())) {
            y.m(tabItem.getContentDescription());
        }
        d(y);
    }

    public int getSelectedTabPosition() {
        g gVar = this.f0;
        if (gVar != null) {
            return gVar.g();
        }
        return -1;
    }

    public int getTabCount() {
        return this.a.size();
    }

    public int getTabGravity() {
        return this.A0;
    }

    public ColorStateList getTabIconTint() {
        return this.n0;
    }

    public int getTabIndicatorAnimationMode() {
        return this.G0;
    }

    public int getTabIndicatorGravity() {
        return this.C0;
    }

    public int getTabMaxWidth() {
        return this.v0;
    }

    public int getTabMode() {
        return this.D0;
    }

    public ColorStateList getTabRippleColor() {
        return this.o0;
    }

    public Drawable getTabSelectedIndicator() {
        return this.p0;
    }

    public ColorStateList getTabTextColors() {
        return this.m0;
    }

    public final void h(g gVar) {
        TabView tabView = gVar.h;
        tabView.setSelected(false);
        tabView.setActivated(false);
        this.g0.addView(tabView, gVar.g(), p());
    }

    public final void i(View view) {
        if (view instanceof TabItem) {
            g((TabItem) view);
            return;
        }
        throw new IllegalArgumentException("Only TabItem instances can be added to TabLayout");
    }

    public final void j(int i2) {
        if (i2 == -1) {
            return;
        }
        if (getWindowToken() != null && ei4.W(this) && !this.g0.c()) {
            int scrollX = getScrollX();
            int m = m(i2, Utils.FLOAT_EPSILON);
            if (scrollX != m) {
                v();
                this.M0.setIntValues(scrollX, m);
                this.M0.start();
            }
            this.g0.b(i2, this.B0);
            return;
        }
        setScrollPosition(i2, Utils.FLOAT_EPSILON, true);
    }

    public final void k(int i2) {
        if (i2 != 0) {
            if (i2 == 1) {
                this.g0.setGravity(1);
                return;
            } else if (i2 != 2) {
                return;
            }
        }
        this.g0.setGravity(8388611);
    }

    public final void l() {
        int i2 = this.D0;
        ei4.H0(this.g0, (i2 == 0 || i2 == 2) ? Math.max(0, this.z0 - this.h0) : 0, 0, 0, 0);
        int i3 = this.D0;
        if (i3 == 0) {
            k(this.A0);
        } else if (i3 == 1 || i3 == 2) {
            this.g0.setGravity(1);
        }
        K(true);
    }

    public final int m(int i2, float f2) {
        int i3 = this.D0;
        if (i3 == 0 || i3 == 2) {
            View childAt = this.g0.getChildAt(i2);
            int i4 = i2 + 1;
            View childAt2 = i4 < this.g0.getChildCount() ? this.g0.getChildAt(i4) : null;
            int width = childAt != null ? childAt.getWidth() : 0;
            int width2 = childAt2 != null ? childAt2.getWidth() : 0;
            int left = (childAt.getLeft() + (width / 2)) - (getWidth() / 2);
            int i5 = (int) ((width + width2) * 0.5f * f2);
            return ei4.E(this) == 0 ? left + i5 : left - i5;
        }
        return 0;
    }

    public final void n(g gVar, int i2) {
        gVar.q(i2);
        this.a.add(i2, gVar);
        int size = this.a.size();
        while (true) {
            i2++;
            if (i2 >= size) {
                return;
            }
            this.a.get(i2).q(i2);
        }
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        p42.e(this);
        if (this.N0 == null) {
            ViewParent parent = getParent();
            if (parent instanceof ViewPager) {
                H((ViewPager) parent, true, true);
            }
        }
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.S0) {
            setupWithViewPager(null);
            this.S0 = false;
        }
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        for (int i2 = 0; i2 < this.g0.getChildCount(); i2++) {
            View childAt = this.g0.getChildAt(i2);
            if (childAt instanceof TabView) {
                ((TabView) childAt).j(canvas);
            }
        }
        super.onDraw(canvas);
    }

    @Override // android.view.View
    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        b6.I0(accessibilityNodeInfo).e0(b6.b.b(1, getTabCount(), false, 1));
    }

    /* JADX WARN: Code restructure failed: missing block: B:25:0x0073, code lost:
        if (r0 != 2) goto L18;
     */
    /* JADX WARN: Code restructure failed: missing block: B:28:0x007e, code lost:
        if (r7.getMeasuredWidth() != getMeasuredWidth()) goto L25;
     */
    /* JADX WARN: Code restructure failed: missing block: B:29:0x0080, code lost:
        r4 = true;
     */
    /* JADX WARN: Code restructure failed: missing block: B:31:0x008a, code lost:
        if (r7.getMeasuredWidth() < getMeasuredWidth()) goto L25;
     */
    @Override // android.widget.HorizontalScrollView, android.widget.FrameLayout, android.view.View
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void onMeasure(int r7, int r8) {
        /*
            r6 = this;
            android.content.Context r0 = r6.getContext()
            int r1 = r6.getDefaultHeight()
            float r0 = defpackage.mk4.c(r0, r1)
            int r0 = java.lang.Math.round(r0)
            int r1 = android.view.View.MeasureSpec.getMode(r8)
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = 1073741824(0x40000000, float:2.0)
            r4 = 0
            r5 = 1
            if (r1 == r2) goto L2e
            if (r1 == 0) goto L1f
            goto L41
        L1f:
            int r8 = r6.getPaddingTop()
            int r0 = r0 + r8
            int r8 = r6.getPaddingBottom()
            int r0 = r0 + r8
            int r8 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r3)
            goto L41
        L2e:
            int r1 = r6.getChildCount()
            if (r1 != r5) goto L41
            int r1 = android.view.View.MeasureSpec.getSize(r8)
            if (r1 < r0) goto L41
            android.view.View r1 = r6.getChildAt(r4)
            r1.setMinimumHeight(r0)
        L41:
            int r0 = android.view.View.MeasureSpec.getSize(r7)
            int r1 = android.view.View.MeasureSpec.getMode(r7)
            if (r1 == 0) goto L5f
            int r1 = r6.x0
            if (r1 <= 0) goto L50
            goto L5d
        L50:
            float r0 = (float) r0
            android.content.Context r1 = r6.getContext()
            r2 = 56
            float r1 = defpackage.mk4.c(r1, r2)
            float r0 = r0 - r1
            int r1 = (int) r0
        L5d:
            r6.v0 = r1
        L5f:
            super.onMeasure(r7, r8)
            int r7 = r6.getChildCount()
            if (r7 != r5) goto Lad
            android.view.View r7 = r6.getChildAt(r4)
            int r0 = r6.D0
            if (r0 == 0) goto L82
            if (r0 == r5) goto L76
            r1 = 2
            if (r0 == r1) goto L82
            goto L8d
        L76:
            int r0 = r7.getMeasuredWidth()
            int r1 = r6.getMeasuredWidth()
            if (r0 == r1) goto L8d
        L80:
            r4 = r5
            goto L8d
        L82:
            int r0 = r7.getMeasuredWidth()
            int r1 = r6.getMeasuredWidth()
            if (r0 >= r1) goto L8d
            goto L80
        L8d:
            if (r4 == 0) goto Lad
            int r0 = r6.getPaddingTop()
            int r1 = r6.getPaddingBottom()
            int r0 = r0 + r1
            android.view.ViewGroup$LayoutParams r1 = r7.getLayoutParams()
            int r1 = r1.height
            int r8 = android.widget.HorizontalScrollView.getChildMeasureSpec(r8, r0, r1)
            int r0 = r6.getMeasuredWidth()
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r3)
            r7.measure(r0, r8)
        Lad:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.tabs.TabLayout.onMeasure(int, int):void");
    }

    public final LinearLayout.LayoutParams p() {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -1);
        J(layoutParams);
        return layoutParams;
    }

    public g q() {
        g b2 = V0.b();
        return b2 == null ? new g() : b2;
    }

    public final TabView r(g gVar) {
        et2<TabView> et2Var = this.T0;
        TabView b2 = et2Var != null ? et2Var.b() : null;
        if (b2 == null) {
            b2 = new TabView(getContext());
        }
        b2.setTab(gVar);
        b2.setFocusable(true);
        b2.setMinimumWidth(getTabMinWidth());
        if (TextUtils.isEmpty(gVar.c)) {
            b2.setContentDescription(gVar.b);
        } else {
            b2.setContentDescription(gVar.c);
        }
        return b2;
    }

    public final void s(g gVar) {
        for (int size = this.K0.size() - 1; size >= 0; size--) {
            this.K0.get(size).a(gVar);
        }
    }

    @Override // android.view.View
    public void setElevation(float f2) {
        super.setElevation(f2);
        p42.d(this, f2);
    }

    public void setInlineLabel(boolean z) {
        if (this.E0 != z) {
            this.E0 = z;
            for (int i2 = 0; i2 < this.g0.getChildCount(); i2++) {
                View childAt = this.g0.getChildAt(i2);
                if (childAt instanceof TabView) {
                    ((TabView) childAt).v();
                }
            }
            l();
        }
    }

    public void setInlineLabelResource(int i2) {
        setInlineLabel(getResources().getBoolean(i2));
    }

    @Deprecated
    public void setOnTabSelectedListener(d dVar) {
        setOnTabSelectedListener((c) dVar);
    }

    public void setScrollAnimatorListener(Animator.AnimatorListener animatorListener) {
        v();
        this.M0.addListener(animatorListener);
    }

    public void setScrollPosition(int i2, float f2, boolean z) {
        setScrollPosition(i2, f2, z, true);
    }

    public void setSelectedTabIndicator(Drawable drawable) {
        if (this.p0 != drawable) {
            if (drawable == null) {
                drawable = new GradientDrawable();
            }
            this.p0 = drawable;
        }
    }

    public void setSelectedTabIndicatorColor(int i2) {
        this.q0 = i2;
    }

    public void setSelectedTabIndicatorGravity(int i2) {
        if (this.C0 != i2) {
            this.C0 = i2;
            ei4.j0(this.g0);
        }
    }

    @Deprecated
    public void setSelectedTabIndicatorHeight(int i2) {
        this.g0.f(i2);
    }

    public void setTabGravity(int i2) {
        if (this.A0 != i2) {
            this.A0 = i2;
            l();
        }
    }

    public void setTabIconTint(ColorStateList colorStateList) {
        if (this.n0 != colorStateList) {
            this.n0 = colorStateList;
            I();
        }
    }

    public void setTabIconTintResource(int i2) {
        setTabIconTint(mf.c(getContext(), i2));
    }

    public void setTabIndicatorAnimationMode(int i2) {
        this.G0 = i2;
        if (i2 == 0) {
            this.I0 = new com.google.android.material.tabs.a();
        } else if (i2 == 1) {
            this.I0 = new iu0();
        } else {
            throw new IllegalArgumentException(i2 + " is not a valid TabIndicatorAnimationMode");
        }
    }

    public void setTabIndicatorFullWidth(boolean z) {
        this.F0 = z;
        ei4.j0(this.g0);
    }

    public void setTabMode(int i2) {
        if (i2 != this.D0) {
            this.D0 = i2;
            l();
        }
    }

    public void setTabRippleColor(ColorStateList colorStateList) {
        if (this.o0 != colorStateList) {
            this.o0 = colorStateList;
            for (int i2 = 0; i2 < this.g0.getChildCount(); i2++) {
                View childAt = this.g0.getChildAt(i2);
                if (childAt instanceof TabView) {
                    ((TabView) childAt).u(getContext());
                }
            }
        }
    }

    public void setTabRippleColorResource(int i2) {
        setTabRippleColor(mf.c(getContext(), i2));
    }

    public void setTabTextColors(ColorStateList colorStateList) {
        if (this.m0 != colorStateList) {
            this.m0 = colorStateList;
            I();
        }
    }

    @Deprecated
    public void setTabsFromPagerAdapter(dp2 dp2Var) {
        G(dp2Var, false);
    }

    public void setUnboundedRipple(boolean z) {
        if (this.H0 != z) {
            this.H0 = z;
            for (int i2 = 0; i2 < this.g0.getChildCount(); i2++) {
                View childAt = this.g0.getChildAt(i2);
                if (childAt instanceof TabView) {
                    ((TabView) childAt).u(getContext());
                }
            }
        }
    }

    public void setUnboundedRippleResource(int i2) {
        setUnboundedRipple(getResources().getBoolean(i2));
    }

    public void setupWithViewPager(ViewPager viewPager) {
        setupWithViewPager(viewPager, true);
    }

    @Override // android.widget.HorizontalScrollView, android.widget.FrameLayout, android.view.ViewGroup
    public boolean shouldDelayChildPressedState() {
        return getTabScrollRange() > 0;
    }

    public final void t(g gVar) {
        for (int size = this.K0.size() - 1; size >= 0; size--) {
            this.K0.get(size).b(gVar);
        }
    }

    public final void u(g gVar) {
        for (int size = this.K0.size() - 1; size >= 0; size--) {
            this.K0.get(size).c(gVar);
        }
    }

    public final void v() {
        if (this.M0 == null) {
            ValueAnimator valueAnimator = new ValueAnimator();
            this.M0 = valueAnimator;
            valueAnimator.setInterpolator(ne.b);
            this.M0.setDuration(this.B0);
            this.M0.addUpdateListener(new a());
        }
    }

    public g w(int i2) {
        if (i2 < 0 || i2 >= getTabCount()) {
            return null;
        }
        return this.a.get(i2);
    }

    public boolean x() {
        return this.F0;
    }

    public g y() {
        g q = q();
        q.g = this;
        q.h = r(q);
        if (q.i != -1) {
            q.h.setId(q.i);
        }
        return q;
    }

    public void z() {
        int currentItem;
        B();
        dp2 dp2Var = this.O0;
        if (dp2Var != null) {
            int e2 = dp2Var.e();
            for (int i2 = 0; i2 < e2; i2++) {
                f(y().r(this.O0.g(i2)), false);
            }
            ViewPager viewPager = this.N0;
            if (viewPager == null || e2 <= 0 || (currentItem = viewPager.getCurrentItem()) == getSelectedTabPosition() || currentItem >= getTabCount()) {
                return;
            }
            E(w(currentItem));
        }
    }

    public TabLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, gy2.tabStyle);
    }

    @Override // android.widget.HorizontalScrollView, android.view.ViewGroup
    public void addView(View view, int i2) {
        i(view);
    }

    @Override // android.widget.FrameLayout, android.view.ViewGroup
    public FrameLayout.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return generateDefaultLayoutParams();
    }

    @Deprecated
    public void setOnTabSelectedListener(c cVar) {
        c cVar2 = this.J0;
        if (cVar2 != null) {
            C(cVar2);
        }
        this.J0 = cVar;
        if (cVar != null) {
            c(cVar);
        }
    }

    public void setScrollPosition(int i2, float f2, boolean z, boolean z2) {
        int round = Math.round(i2 + f2);
        if (round < 0 || round >= this.g0.getChildCount()) {
            return;
        }
        if (z2) {
            this.g0.e(i2, f2);
        }
        ValueAnimator valueAnimator = this.M0;
        if (valueAnimator != null && valueAnimator.isRunning()) {
            this.M0.cancel();
        }
        scrollTo(m(i2, f2), 0);
        if (z) {
            setSelectedTabView(round);
        }
    }

    public void setupWithViewPager(ViewPager viewPager, boolean z) {
        H(viewPager, z, false);
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public TabLayout(android.content.Context r12, android.util.AttributeSet r13, int r14) {
        /*
            Method dump skipped, instructions count: 444
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.tabs.TabLayout.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }

    @Override // android.widget.HorizontalScrollView, android.view.ViewGroup, android.view.ViewManager
    public void addView(View view, ViewGroup.LayoutParams layoutParams) {
        i(view);
    }

    public void setSelectedTabIndicator(int i2) {
        if (i2 != 0) {
            setSelectedTabIndicator(mf.d(getContext(), i2));
        } else {
            setSelectedTabIndicator((Drawable) null);
        }
    }

    @Override // android.widget.HorizontalScrollView, android.view.ViewGroup
    public void addView(View view, int i2, ViewGroup.LayoutParams layoutParams) {
        i(view);
    }

    public void setTabTextColors(int i2, int i3) {
        setTabTextColors(o(i2, i3));
    }
}
