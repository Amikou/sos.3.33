package com.google.android.material.tabs;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;

/* loaded from: classes2.dex */
public class TabItem extends View {
    public final CharSequence a;
    public final Drawable f0;
    public final int g0;

    public TabItem(Context context) {
        this(context, null);
    }

    public TabItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        l64 u = l64.u(context, attributeSet, o23.TabItem);
        this.a = u.p(o23.TabItem_android_text);
        this.f0 = u.g(o23.TabItem_android_icon);
        this.g0 = u.n(o23.TabItem_android_layout, 0);
        u.w();
    }
}
