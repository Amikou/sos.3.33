package com.google.android.material.navigationrail;

import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;
import com.google.android.material.navigation.NavigationBarItemView;
import com.google.android.material.navigation.NavigationBarMenuView;

/* loaded from: classes2.dex */
public class NavigationRailMenuView extends NavigationBarMenuView {
    public final FrameLayout.LayoutParams z0;

    public NavigationRailMenuView(Context context) {
        super(context);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -2);
        this.z0 = layoutParams;
        layoutParams.gravity = 49;
        setLayoutParams(layoutParams);
    }

    @Override // com.google.android.material.navigation.NavigationBarMenuView
    public NavigationBarItemView f(Context context) {
        return new me2(context);
    }

    public int getMenuGravity() {
        return this.z0.gravity;
    }

    public boolean l() {
        return (this.z0.gravity & 112) == 48;
    }

    public final int m(int i, int i2, int i3) {
        return View.MeasureSpec.makeMeasureSpec(Math.min(View.MeasureSpec.getSize(i), i2 / Math.max(1, i3)), 0);
    }

    public final int n(View view, int i, int i2) {
        if (view.getVisibility() != 8) {
            view.measure(i, i2);
            return view.getMeasuredHeight();
        }
        return 0;
    }

    public final int o(int i, int i2, int i3, View view) {
        int makeMeasureSpec;
        m(i, i2, i3);
        if (view == null) {
            makeMeasureSpec = m(i, i2, i3);
        } else {
            makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(view.getMeasuredHeight(), 0);
        }
        int childCount = getChildCount();
        int i4 = 0;
        for (int i5 = 0; i5 < childCount; i5++) {
            View childAt = getChildAt(i5);
            if (childAt != view) {
                i4 += n(childAt, i, makeMeasureSpec);
            }
        }
        return i4;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int childCount = getChildCount();
        int i5 = i3 - i;
        int i6 = 0;
        for (int i7 = 0; i7 < childCount; i7++) {
            View childAt = getChildAt(i7);
            if (childAt.getVisibility() != 8) {
                int measuredHeight = childAt.getMeasuredHeight() + i6;
                childAt.layout(0, i6, i5, measuredHeight);
                i6 = measuredHeight;
            }
        }
    }

    @Override // android.view.View
    public void onMeasure(int i, int i2) {
        int o;
        int size = View.MeasureSpec.getSize(i2);
        int size2 = getMenu().G().size();
        if (size2 > 1 && g(getLabelVisibilityMode(), size2)) {
            o = p(i, size, size2);
        } else {
            o = o(i, size, size2, null);
        }
        setMeasuredDimension(View.resolveSizeAndState(View.MeasureSpec.getSize(i), i, 0), View.resolveSizeAndState(o, i2, 0));
    }

    public final int p(int i, int i2, int i3) {
        int i4;
        View childAt = getChildAt(getSelectedItemPosition());
        if (childAt != null) {
            i4 = n(childAt, i, m(i, i2, i3));
            i2 -= i4;
            i3--;
        } else {
            i4 = 0;
        }
        return i4 + o(i, i2, i3, childAt);
    }

    public void setMenuGravity(int i) {
        FrameLayout.LayoutParams layoutParams = this.z0;
        if (layoutParams.gravity != i) {
            layoutParams.gravity = i;
            setLayoutParams(layoutParams);
        }
    }
}
