package com.google.android.material.navigationrail;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.google.android.material.navigation.NavigationBarView;

/* loaded from: classes2.dex */
public class NavigationRailView extends NavigationBarView {
    public final int l0;
    public View m0;

    public NavigationRailView(Context context) {
        this(context, null);
    }

    private NavigationRailMenuView getNavigationRailMenuView() {
        return (NavigationRailMenuView) getMenuView();
    }

    public void g(int i) {
        h(LayoutInflater.from(getContext()).inflate(i, (ViewGroup) this, false));
    }

    public View getHeaderView() {
        return this.m0;
    }

    @Override // com.google.android.material.navigation.NavigationBarView
    public int getMaxItemCount() {
        return 7;
    }

    public int getMenuGravity() {
        return getNavigationRailMenuView().getMenuGravity();
    }

    public void h(View view) {
        l();
        this.m0 = view;
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2);
        layoutParams.gravity = 49;
        layoutParams.topMargin = this.l0;
        addView(view, 0, layoutParams);
    }

    @Override // com.google.android.material.navigation.NavigationBarView
    /* renamed from: i */
    public NavigationRailMenuView e(Context context) {
        return new NavigationRailMenuView(context);
    }

    public final boolean j() {
        View view = this.m0;
        return (view == null || view.getVisibility() == 8) ? false : true;
    }

    public final int k(int i) {
        int suggestedMinimumWidth = getSuggestedMinimumWidth();
        if (View.MeasureSpec.getMode(i) == 1073741824 || suggestedMinimumWidth <= 0) {
            return i;
        }
        return View.MeasureSpec.makeMeasureSpec(Math.min(View.MeasureSpec.getSize(i), suggestedMinimumWidth + getPaddingLeft() + getPaddingRight()), 1073741824);
    }

    public void l() {
        View view = this.m0;
        if (view != null) {
            removeView(view);
            this.m0 = null;
        }
    }

    @Override // android.widget.FrameLayout, android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        NavigationRailMenuView navigationRailMenuView = getNavigationRailMenuView();
        int i5 = 0;
        if (j()) {
            int bottom = this.m0.getBottom() + this.l0;
            int top = navigationRailMenuView.getTop();
            if (top < bottom) {
                i5 = bottom - top;
            }
        } else if (navigationRailMenuView.l()) {
            i5 = this.l0;
        }
        if (i5 > 0) {
            navigationRailMenuView.layout(navigationRailMenuView.getLeft(), navigationRailMenuView.getTop() + i5, navigationRailMenuView.getRight(), navigationRailMenuView.getBottom() + i5);
        }
    }

    @Override // android.widget.FrameLayout, android.view.View
    public void onMeasure(int i, int i2) {
        int k = k(i);
        super.onMeasure(k, i2);
        if (j()) {
            measureChild(getNavigationRailMenuView(), k, View.MeasureSpec.makeMeasureSpec((getMeasuredHeight() - this.m0.getMeasuredHeight()) - this.l0, Integer.MIN_VALUE));
        }
    }

    public void setMenuGravity(int i) {
        getNavigationRailMenuView().setMenuGravity(i);
    }

    public NavigationRailView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, gy2.navigationRailStyle);
    }

    public NavigationRailView(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, y13.Widget_MaterialComponents_NavigationRailView);
    }

    public NavigationRailView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        this.l0 = getResources().getDimensionPixelSize(jz2.mtrl_navigation_rail_margin);
        l64 i3 = a54.i(getContext(), attributeSet, o23.NavigationRailView, i, i2, new int[0]);
        int n = i3.n(o23.NavigationRailView_headerLayout, 0);
        if (n != 0) {
            g(n);
        }
        setMenuGravity(i3.k(o23.NavigationRailView_menuGravity, 49));
        i3.w();
    }
}
