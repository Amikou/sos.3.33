package com.google.android.material.timepicker;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.github.mikephil.charting.utils.Utils;

/* loaded from: classes2.dex */
class RadialViewGroup extends ConstraintLayout {
    public o42 A0;
    public final Runnable y0;
    public int z0;

    /* loaded from: classes2.dex */
    public class a implements Runnable {
        public a() {
        }

        @Override // java.lang.Runnable
        public void run() {
            RadialViewGroup.this.y();
        }
    }

    public RadialViewGroup(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public static boolean x(View view) {
        return "skip".equals(view.getTag());
    }

    @Override // android.view.ViewGroup
    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        super.addView(view, i, layoutParams);
        if (view.getId() == -1) {
            view.setId(ei4.m());
        }
        z();
    }

    @Override // android.view.View
    public void onFinishInflate() {
        super.onFinishInflate();
        y();
    }

    @Override // androidx.constraintlayout.widget.ConstraintLayout, android.view.ViewGroup
    public void onViewRemoved(View view) {
        super.onViewRemoved(view);
        z();
    }

    @Override // android.view.View
    public void setBackgroundColor(int i) {
        this.A0.a0(ColorStateList.valueOf(i));
    }

    public final Drawable u() {
        o42 o42Var = new o42();
        this.A0 = o42Var;
        o42Var.Y(new j63(0.5f));
        this.A0.a0(ColorStateList.valueOf(-1));
        return this.A0;
    }

    public int v() {
        return this.z0;
    }

    public void w(int i) {
        this.z0 = i;
        y();
    }

    public void y() {
        int childCount = getChildCount();
        int i = 1;
        for (int i2 = 0; i2 < childCount; i2++) {
            if (x(getChildAt(i2))) {
                i++;
            }
        }
        androidx.constraintlayout.widget.a aVar = new androidx.constraintlayout.widget.a();
        aVar.p(this);
        float f = Utils.FLOAT_EPSILON;
        for (int i3 = 0; i3 < childCount; i3++) {
            View childAt = getChildAt(i3);
            int id = childAt.getId();
            int i4 = b03.circle_center;
            if (id != i4 && !x(childAt)) {
                aVar.s(childAt.getId(), i4, this.z0, f);
                f += 360.0f / (childCount - i);
            }
        }
        aVar.i(this);
    }

    public final void z() {
        Handler handler = getHandler();
        if (handler != null) {
            handler.removeCallbacks(this.y0);
            handler.post(this.y0);
        }
    }

    public RadialViewGroup(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        LayoutInflater.from(context).inflate(x03.material_radial_view_group, this);
        ei4.w0(this, u());
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, o23.RadialViewGroup, i, 0);
        this.z0 = obtainStyledAttributes.getDimensionPixelSize(o23.RadialViewGroup_materialCircleRadius, 0);
        this.y0 = new a();
        obtainStyledAttributes.recycle();
    }
}
