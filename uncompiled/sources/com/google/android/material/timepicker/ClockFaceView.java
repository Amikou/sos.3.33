package com.google.android.material.timepicker;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.RadialGradient;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.TextView;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.material.timepicker.ClockHandView;
import defpackage.b6;
import java.util.Arrays;

/* loaded from: classes2.dex */
class ClockFaceView extends RadialViewGroup implements ClockHandView.d {
    public final ClockHandView B0;
    public final Rect C0;
    public final RectF D0;
    public final SparseArray<TextView> E0;
    public final z5 F0;
    public final int[] G0;
    public final float[] H0;
    public final int I0;
    public final int J0;
    public final int K0;
    public final int L0;
    public String[] M0;
    public float N0;
    public final ColorStateList O0;

    /* loaded from: classes2.dex */
    public class a implements ViewTreeObserver.OnPreDrawListener {
        public a() {
        }

        @Override // android.view.ViewTreeObserver.OnPreDrawListener
        public boolean onPreDraw() {
            if (ClockFaceView.this.isShown()) {
                ClockFaceView.this.getViewTreeObserver().removeOnPreDrawListener(this);
                ClockFaceView.this.w(((ClockFaceView.this.getHeight() / 2) - ClockFaceView.this.B0.g()) - ClockFaceView.this.I0);
                return true;
            }
            return true;
        }
    }

    /* loaded from: classes2.dex */
    public class b extends z5 {
        public b() {
        }

        @Override // defpackage.z5
        public void g(View view, b6 b6Var) {
            super.g(view, b6Var);
            int intValue = ((Integer) view.getTag(b03.material_value_index)).intValue();
            if (intValue > 0) {
                b6Var.F0((View) ClockFaceView.this.E0.get(intValue - 1));
            }
            b6Var.f0(b6.c.a(0, 1, intValue, 1, false, view.isSelected()));
        }
    }

    public ClockFaceView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, gy2.materialClockStyle);
    }

    public static float F(float f, float f2, float f3) {
        return Math.max(Math.max(f, f2), f3);
    }

    public final void D() {
        RectF d = this.B0.d();
        for (int i = 0; i < this.E0.size(); i++) {
            TextView textView = this.E0.get(i);
            if (textView != null) {
                textView.getDrawingRect(this.C0);
                this.C0.offset(textView.getPaddingLeft(), textView.getPaddingTop());
                offsetDescendantRectToMyCoords(textView, this.C0);
                this.D0.set(this.C0);
                textView.getPaint().setShader(E(d, this.D0));
                textView.invalidate();
            }
        }
    }

    public final RadialGradient E(RectF rectF, RectF rectF2) {
        if (RectF.intersects(rectF, rectF2)) {
            return new RadialGradient(rectF.centerX() - this.D0.left, rectF.centerY() - this.D0.top, rectF.width() * 0.5f, this.G0, this.H0, Shader.TileMode.CLAMP);
        }
        return null;
    }

    public void G(String[] strArr, int i) {
        this.M0 = strArr;
        H(i);
    }

    public final void H(int i) {
        LayoutInflater from = LayoutInflater.from(getContext());
        int size = this.E0.size();
        for (int i2 = 0; i2 < Math.max(this.M0.length, size); i2++) {
            TextView textView = this.E0.get(i2);
            if (i2 >= this.M0.length) {
                removeView(textView);
                this.E0.remove(i2);
            } else {
                if (textView == null) {
                    textView = (TextView) from.inflate(x03.material_clockface_textview, (ViewGroup) this, false);
                    this.E0.put(i2, textView);
                    addView(textView);
                }
                textView.setVisibility(0);
                textView.setText(this.M0[i2]);
                textView.setTag(b03.material_value_index, Integer.valueOf(i2));
                ei4.t0(textView, this.F0);
                textView.setTextColor(this.O0);
                if (i != 0) {
                    textView.setContentDescription(getResources().getString(i, this.M0[i2]));
                }
            }
        }
    }

    @Override // com.google.android.material.timepicker.ClockHandView.d
    public void a(float f, boolean z) {
        if (Math.abs(this.N0 - f) > 0.001f) {
            this.N0 = f;
            D();
        }
    }

    @Override // android.view.View
    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        b6.I0(accessibilityNodeInfo).e0(b6.b.b(1, this.M0.length, false, 1));
    }

    @Override // androidx.constraintlayout.widget.ConstraintLayout, android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        D();
    }

    @Override // androidx.constraintlayout.widget.ConstraintLayout, android.view.View
    public void onMeasure(int i, int i2) {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        int F = (int) (this.L0 / F(this.J0 / displayMetrics.heightPixels, this.K0 / displayMetrics.widthPixels, 1.0f));
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(F, 1073741824);
        setMeasuredDimension(F, F);
        super.onMeasure(makeMeasureSpec, makeMeasureSpec);
    }

    @Override // com.google.android.material.timepicker.RadialViewGroup
    public void w(int i) {
        if (i != v()) {
            super.w(i);
            this.B0.j(v());
        }
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public ClockFaceView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.C0 = new Rect();
        this.D0 = new RectF();
        this.E0 = new SparseArray<>();
        this.H0 = new float[]{Utils.FLOAT_EPSILON, 0.9f, 1.0f};
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, o23.ClockFaceView, i, y13.Widget_MaterialComponents_TimePicker_Clock);
        Resources resources = getResources();
        ColorStateList b2 = n42.b(context, obtainStyledAttributes, o23.ClockFaceView_clockNumberTextColor);
        this.O0 = b2;
        LayoutInflater.from(context).inflate(x03.material_clockface_view, (ViewGroup) this, true);
        ClockHandView clockHandView = (ClockHandView) findViewById(b03.material_clock_hand);
        this.B0 = clockHandView;
        this.I0 = resources.getDimensionPixelSize(jz2.material_clock_hand_padding);
        int colorForState = b2.getColorForState(new int[]{16842913}, b2.getDefaultColor());
        this.G0 = new int[]{colorForState, colorForState, b2.getDefaultColor()};
        clockHandView.b(this);
        int defaultColor = mf.c(context, ty2.material_timepicker_clockface).getDefaultColor();
        ColorStateList b3 = n42.b(context, obtainStyledAttributes, o23.ClockFaceView_clockFaceBackgroundColor);
        setBackgroundColor(b3 != null ? b3.getDefaultColor() : defaultColor);
        getViewTreeObserver().addOnPreDrawListener(new a());
        setFocusable(true);
        obtainStyledAttributes.recycle();
        this.F0 = new b();
        String[] strArr = new String[12];
        Arrays.fill(strArr, "");
        G(strArr, 0);
        this.J0 = resources.getDimensionPixelSize(jz2.material_time_picker_minimum_screen_height);
        this.K0 = resources.getDimensionPixelSize(jz2.material_time_picker_minimum_screen_width);
        this.L0 = resources.getDimensionPixelSize(jz2.material_clock_size);
    }
}
