package com.google.android.material.timepicker;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import com.github.mikephil.charting.utils.Utils;
import java.util.ArrayList;
import java.util.List;

/* JADX INFO: Access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public class ClockHandView extends View {
    public ValueAnimator a;
    public boolean f0;
    public float g0;
    public float h0;
    public boolean i0;
    public int j0;
    public final List<d> k0;
    public final int l0;
    public final float m0;
    public final Paint n0;
    public final RectF o0;
    public final int p0;
    public float q0;
    public boolean r0;
    public c s0;
    public double t0;
    public int u0;

    /* loaded from: classes2.dex */
    public class a implements ValueAnimator.AnimatorUpdateListener {
        public a() {
        }

        @Override // android.animation.ValueAnimator.AnimatorUpdateListener
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            ClockHandView.this.m(((Float) valueAnimator.getAnimatedValue()).floatValue(), true);
        }
    }

    /* loaded from: classes2.dex */
    public class b extends AnimatorListenerAdapter {
        public b(ClockHandView clockHandView) {
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationCancel(Animator animator) {
            animator.end();
        }
    }

    /* loaded from: classes2.dex */
    public interface c {
        void a(float f, boolean z);
    }

    /* loaded from: classes2.dex */
    public interface d {
        void a(float f, boolean z);
    }

    public ClockHandView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, gy2.materialClockStyle);
    }

    public void b(d dVar) {
        this.k0.add(dVar);
    }

    public final void c(Canvas canvas) {
        int width;
        int height = getHeight() / 2;
        float width2 = getWidth() / 2;
        float f = height;
        this.n0.setStrokeWidth(Utils.FLOAT_EPSILON);
        canvas.drawCircle((this.u0 * ((float) Math.cos(this.t0))) + width2, (this.u0 * ((float) Math.sin(this.t0))) + f, this.l0, this.n0);
        double sin = Math.sin(this.t0);
        double cos = Math.cos(this.t0);
        this.n0.setStrokeWidth(this.p0);
        canvas.drawLine(width2, f, width + ((int) (cos * r6)), height + ((int) (r6 * sin)), this.n0);
        canvas.drawCircle(width2, f, this.m0, this.n0);
    }

    public RectF d() {
        return this.o0;
    }

    public final int e(float f, float f2) {
        int degrees = ((int) Math.toDegrees(Math.atan2(f2 - (getHeight() / 2), f - (getWidth() / 2)))) + 90;
        return degrees < 0 ? degrees + 360 : degrees;
    }

    public float f() {
        return this.q0;
    }

    public int g() {
        return this.l0;
    }

    public final Pair<Float, Float> h(float f) {
        float f2 = f();
        if (Math.abs(f2 - f) > 180.0f) {
            if (f2 > 180.0f && f < 180.0f) {
                f += 360.0f;
            }
            if (f2 < 180.0f && f > 180.0f) {
                f2 += 360.0f;
            }
        }
        return new Pair<>(Float.valueOf(f2), Float.valueOf(f));
    }

    public final boolean i(float f, float f2, boolean z, boolean z2, boolean z3) {
        float e = e(f, f2);
        boolean z4 = false;
        boolean z5 = f() != e;
        if (z2 && z5) {
            return true;
        }
        if (z5 || z) {
            if (z3 && this.f0) {
                z4 = true;
            }
            l(e, z4);
            return true;
        }
        return false;
    }

    public void j(int i) {
        this.u0 = i;
        invalidate();
    }

    public void k(float f) {
        l(f, false);
    }

    public void l(float f, boolean z) {
        ValueAnimator valueAnimator = this.a;
        if (valueAnimator != null) {
            valueAnimator.cancel();
        }
        if (!z) {
            m(f, false);
            return;
        }
        Pair<Float, Float> h = h(f);
        ValueAnimator ofFloat = ValueAnimator.ofFloat(((Float) h.first).floatValue(), ((Float) h.second).floatValue());
        this.a = ofFloat;
        ofFloat.setDuration(200L);
        this.a.addUpdateListener(new a());
        this.a.addListener(new b(this));
        this.a.start();
    }

    public final void m(float f, boolean z) {
        float f2 = f % 360.0f;
        this.q0 = f2;
        this.t0 = Math.toRadians(f2 - 90.0f);
        float width = (getWidth() / 2) + (this.u0 * ((float) Math.cos(this.t0)));
        float height = (getHeight() / 2) + (this.u0 * ((float) Math.sin(this.t0)));
        RectF rectF = this.o0;
        int i = this.l0;
        rectF.set(width - i, height - i, width + i, height + i);
        for (d dVar : this.k0) {
            dVar.a(f2, z);
        }
        invalidate();
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        c(canvas);
    }

    @Override // android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        k(f());
    }

    @Override // android.view.View
    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean z;
        boolean z2;
        boolean z3;
        c cVar;
        int actionMasked = motionEvent.getActionMasked();
        float x = motionEvent.getX();
        float y = motionEvent.getY();
        if (actionMasked == 0) {
            this.g0 = x;
            this.h0 = y;
            this.i0 = true;
            this.r0 = false;
            z = false;
            z2 = false;
            z3 = true;
        } else if (actionMasked == 1 || actionMasked == 2) {
            int i = (int) (x - this.g0);
            int i2 = (int) (y - this.h0);
            this.i0 = (i * i) + (i2 * i2) > this.j0;
            boolean z4 = this.r0;
            z = actionMasked == 1;
            z3 = false;
            z2 = z4;
        } else {
            z = false;
            z2 = false;
            z3 = false;
        }
        boolean i3 = i(x, y, z2, z3, z) | this.r0;
        this.r0 = i3;
        if (i3 && z && (cVar = this.s0) != null) {
            cVar.a(e(x, y), this.i0);
        }
        return true;
    }

    public ClockHandView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.k0 = new ArrayList();
        Paint paint = new Paint();
        this.n0 = paint;
        this.o0 = new RectF();
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, o23.ClockHandView, i, y13.Widget_MaterialComponents_TimePicker_Clock);
        this.u0 = obtainStyledAttributes.getDimensionPixelSize(o23.ClockHandView_materialCircleRadius, 0);
        this.l0 = obtainStyledAttributes.getDimensionPixelSize(o23.ClockHandView_selectorSize, 0);
        Resources resources = getResources();
        this.p0 = resources.getDimensionPixelSize(jz2.material_clock_hand_stroke_width);
        this.m0 = resources.getDimensionPixelSize(jz2.material_clock_hand_center_dot_radius);
        int color = obtainStyledAttributes.getColor(o23.ClockHandView_clockHandColor, 0);
        paint.setAntiAlias(true);
        paint.setColor(color);
        k(Utils.FLOAT_EPSILON);
        this.j0 = ViewConfiguration.get(context).getScaledTouchSlop();
        ei4.D0(this, 2);
        obtainStyledAttributes.recycle();
    }
}
