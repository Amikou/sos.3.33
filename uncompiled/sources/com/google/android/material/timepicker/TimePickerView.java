package com.google.android.material.timepicker;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Checkable;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButtonToggleGroup;
import com.google.android.material.chip.Chip;

/* loaded from: classes2.dex */
class TimePickerView extends ConstraintLayout {
    public final MaterialButtonToggleGroup A0;
    public final View.OnClickListener B0;
    public f C0;
    public g D0;
    public e E0;
    public final Chip y0;
    public final Chip z0;

    /* loaded from: classes2.dex */
    public class a implements View.OnClickListener {
        public a() {
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            if (TimePickerView.this.D0 != null) {
                TimePickerView.this.D0.a(((Integer) view.getTag(b03.selection_type)).intValue());
            }
        }
    }

    /* loaded from: classes2.dex */
    public class b implements MaterialButtonToggleGroup.e {
        public b() {
        }

        @Override // com.google.android.material.button.MaterialButtonToggleGroup.e
        public void a(MaterialButtonToggleGroup materialButtonToggleGroup, int i, boolean z) {
            int i2 = i == b03.material_clock_period_pm_button ? 1 : 0;
            if (TimePickerView.this.C0 == null || !z) {
                return;
            }
            TimePickerView.this.C0.a(i2);
        }
    }

    /* loaded from: classes2.dex */
    public class c extends GestureDetector.SimpleOnGestureListener {
        public c() {
        }

        @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnDoubleTapListener
        public boolean onDoubleTap(MotionEvent motionEvent) {
            boolean onDoubleTap = super.onDoubleTap(motionEvent);
            if (TimePickerView.this.E0 != null) {
                TimePickerView.this.E0.a();
            }
            return onDoubleTap;
        }
    }

    /* loaded from: classes2.dex */
    public class d implements View.OnTouchListener {
        public final /* synthetic */ GestureDetector a;

        public d(TimePickerView timePickerView, GestureDetector gestureDetector) {
            this.a = gestureDetector;
        }

        @Override // android.view.View.OnTouchListener
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (((Checkable) view).isChecked()) {
                return this.a.onTouchEvent(motionEvent);
            }
            return false;
        }
    }

    /* loaded from: classes2.dex */
    public interface e {
        void a();
    }

    /* loaded from: classes2.dex */
    public interface f {
        void a(int i);
    }

    /* loaded from: classes2.dex */
    public interface g {
        void a(int i);
    }

    public TimePickerView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        z();
    }

    @Override // android.view.View
    public void onVisibilityChanged(View view, int i) {
        super.onVisibilityChanged(view, i);
        if (view == this && i == 0) {
            z();
        }
    }

    public final void x() {
        Chip chip = this.y0;
        int i = b03.selection_type;
        chip.setTag(i, 12);
        this.z0.setTag(i, 10);
        this.y0.setOnClickListener(this.B0);
        this.z0.setOnClickListener(this.B0);
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public final void y() {
        d dVar = new d(this, new GestureDetector(getContext(), new c()));
        this.y0.setOnTouchListener(dVar);
        this.z0.setOnTouchListener(dVar);
    }

    public final void z() {
        if (this.A0.getVisibility() == 0) {
            androidx.constraintlayout.widget.a aVar = new androidx.constraintlayout.widget.a();
            aVar.p(this);
            aVar.n(b03.material_clock_display, ei4.E(this) == 0 ? 2 : 1);
            aVar.i(this);
        }
    }

    public TimePickerView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.B0 = new a();
        LayoutInflater.from(context).inflate(x03.material_timepicker, this);
        ClockFaceView clockFaceView = (ClockFaceView) findViewById(b03.material_clock_face);
        MaterialButtonToggleGroup materialButtonToggleGroup = (MaterialButtonToggleGroup) findViewById(b03.material_clock_period_toggle);
        this.A0 = materialButtonToggleGroup;
        materialButtonToggleGroup.g(new b());
        this.y0 = (Chip) findViewById(b03.material_minute_tv);
        this.z0 = (Chip) findViewById(b03.material_hour_tv);
        ClockHandView clockHandView = (ClockHandView) findViewById(b03.material_clock_hand);
        y();
        x();
    }
}
