package com.google.android.material.bottomsheet;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.customview.view.AbsSavedState;
import com.github.mikephil.charting.utils.Utils;
import defpackage.b6;
import defpackage.e6;
import defpackage.ji4;
import defpackage.mk4;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/* loaded from: classes2.dex */
public class BottomSheetBehavior<V extends View> extends CoordinatorLayout.Behavior<V> {
    public static final int Y = y13.Widget_Design_BottomSheet_Modal;
    public float A;
    public int B;
    public float C;
    public boolean D;
    public boolean E;
    public boolean F;
    public int G;
    public ji4 H;
    public boolean I;
    public int J;
    public boolean K;
    public int L;
    public int M;
    public int N;
    public WeakReference<V> O;
    public WeakReference<View> P;
    public final ArrayList<g> Q;
    public VelocityTracker R;
    public int S;
    public int T;
    public boolean U;
    public Map<View, Integer> V;
    public int W;
    public final ji4.c X;
    public int a;
    public boolean b;
    public boolean c;
    public float d;
    public int e;
    public boolean f;
    public int g;
    public int h;
    public boolean i;
    public o42 j;
    public int k;
    public int l;
    public boolean m;
    public boolean n;
    public boolean o;
    public boolean p;
    public boolean q;
    public int r;
    public int s;
    public pn3 t;
    public boolean u;
    public BottomSheetBehavior<V>.h v;
    public ValueAnimator w;
    public int x;
    public int y;
    public int z;

    /* loaded from: classes2.dex */
    public class a implements Runnable {
        public final /* synthetic */ View a;
        public final /* synthetic */ ViewGroup.LayoutParams f0;

        public a(BottomSheetBehavior bottomSheetBehavior, View view, ViewGroup.LayoutParams layoutParams) {
            this.a = view;
            this.f0 = layoutParams;
        }

        @Override // java.lang.Runnable
        public void run() {
            this.a.setLayoutParams(this.f0);
        }
    }

    /* loaded from: classes2.dex */
    public class b implements Runnable {
        public final /* synthetic */ View a;
        public final /* synthetic */ int f0;

        public b(View view, int i) {
            this.a = view;
            this.f0 = i;
        }

        @Override // java.lang.Runnable
        public void run() {
            BottomSheetBehavior.this.Y(this.a, this.f0);
        }
    }

    /* loaded from: classes2.dex */
    public class c implements ValueAnimator.AnimatorUpdateListener {
        public c() {
        }

        @Override // android.animation.ValueAnimator.AnimatorUpdateListener
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            float floatValue = ((Float) valueAnimator.getAnimatedValue()).floatValue();
            if (BottomSheetBehavior.this.j != null) {
                BottomSheetBehavior.this.j.b0(floatValue);
            }
        }
    }

    /* loaded from: classes2.dex */
    public class d implements mk4.e {
        public final /* synthetic */ boolean a;

        public d(boolean z) {
            this.a = z;
        }

        @Override // defpackage.mk4.e
        public jp4 a(View view, jp4 jp4Var, mk4.f fVar) {
            BottomSheetBehavior.this.s = jp4Var.m();
            boolean h = mk4.h(view);
            int paddingBottom = view.getPaddingBottom();
            int paddingLeft = view.getPaddingLeft();
            int paddingRight = view.getPaddingRight();
            if (BottomSheetBehavior.this.n) {
                BottomSheetBehavior.this.r = jp4Var.j();
                paddingBottom = fVar.d + BottomSheetBehavior.this.r;
            }
            if (BottomSheetBehavior.this.o) {
                paddingLeft = (h ? fVar.c : fVar.a) + jp4Var.k();
            }
            if (BottomSheetBehavior.this.p) {
                paddingRight = (h ? fVar.a : fVar.c) + jp4Var.l();
            }
            view.setPadding(paddingLeft, view.getPaddingTop(), paddingRight, paddingBottom);
            if (this.a) {
                BottomSheetBehavior.this.l = jp4Var.g().d;
            }
            if (BottomSheetBehavior.this.n || this.a) {
                BottomSheetBehavior.this.f0(false);
            }
            return jp4Var;
        }
    }

    /* loaded from: classes2.dex */
    public class e extends ji4.c {
        public e() {
        }

        @Override // defpackage.ji4.c
        public int a(View view, int i, int i2) {
            return view.getLeft();
        }

        @Override // defpackage.ji4.c
        public int b(View view, int i, int i2) {
            int z = BottomSheetBehavior.this.z();
            BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.this;
            return x42.b(i, z, bottomSheetBehavior.D ? bottomSheetBehavior.N : bottomSheetBehavior.B);
        }

        @Override // defpackage.ji4.c
        public int e(View view) {
            BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.this;
            if (bottomSheetBehavior.D) {
                return bottomSheetBehavior.N;
            }
            return bottomSheetBehavior.B;
        }

        @Override // defpackage.ji4.c
        public void j(int i) {
            if (i == 1 && BottomSheetBehavior.this.F) {
                BottomSheetBehavior.this.W(1);
            }
        }

        @Override // defpackage.ji4.c
        public void k(View view, int i, int i2, int i3, int i4) {
            BottomSheetBehavior.this.w(i2);
        }

        @Override // defpackage.ji4.c
        public void l(View view, float f, float f2) {
            int i;
            int i2 = 4;
            if (f2 < Utils.FLOAT_EPSILON) {
                if (BottomSheetBehavior.this.b) {
                    i = BottomSheetBehavior.this.y;
                } else {
                    int top = view.getTop();
                    BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.this;
                    int i3 = bottomSheetBehavior.z;
                    if (top > i3) {
                        i = i3;
                        i2 = 6;
                    } else {
                        i = bottomSheetBehavior.z();
                    }
                }
                i2 = 3;
            } else {
                BottomSheetBehavior bottomSheetBehavior2 = BottomSheetBehavior.this;
                if (bottomSheetBehavior2.D && bottomSheetBehavior2.a0(view, f2)) {
                    if ((Math.abs(f) >= Math.abs(f2) || f2 <= 500.0f) && !n(view)) {
                        if (BottomSheetBehavior.this.b) {
                            i = BottomSheetBehavior.this.y;
                        } else if (Math.abs(view.getTop() - BottomSheetBehavior.this.z()) < Math.abs(view.getTop() - BottomSheetBehavior.this.z)) {
                            i = BottomSheetBehavior.this.z();
                        } else {
                            i = BottomSheetBehavior.this.z;
                            i2 = 6;
                        }
                        i2 = 3;
                    } else {
                        i = BottomSheetBehavior.this.N;
                        i2 = 5;
                    }
                } else if (f2 != Utils.FLOAT_EPSILON && Math.abs(f) <= Math.abs(f2)) {
                    if (BottomSheetBehavior.this.b) {
                        i = BottomSheetBehavior.this.B;
                    } else {
                        int top2 = view.getTop();
                        if (Math.abs(top2 - BottomSheetBehavior.this.z) < Math.abs(top2 - BottomSheetBehavior.this.B)) {
                            i = BottomSheetBehavior.this.z;
                            i2 = 6;
                        } else {
                            i = BottomSheetBehavior.this.B;
                        }
                    }
                } else {
                    int top3 = view.getTop();
                    if (BottomSheetBehavior.this.b) {
                        if (Math.abs(top3 - BottomSheetBehavior.this.y) < Math.abs(top3 - BottomSheetBehavior.this.B)) {
                            i = BottomSheetBehavior.this.y;
                            i2 = 3;
                        } else {
                            i = BottomSheetBehavior.this.B;
                        }
                    } else {
                        BottomSheetBehavior bottomSheetBehavior3 = BottomSheetBehavior.this;
                        int i4 = bottomSheetBehavior3.z;
                        if (top3 < i4) {
                            if (top3 < Math.abs(top3 - bottomSheetBehavior3.B)) {
                                i = BottomSheetBehavior.this.z();
                                i2 = 3;
                            } else {
                                i = BottomSheetBehavior.this.z;
                            }
                        } else if (Math.abs(top3 - i4) < Math.abs(top3 - BottomSheetBehavior.this.B)) {
                            i = BottomSheetBehavior.this.z;
                        } else {
                            i = BottomSheetBehavior.this.B;
                        }
                        i2 = 6;
                    }
                }
            }
            BottomSheetBehavior.this.b0(view, i2, i, true);
        }

        @Override // defpackage.ji4.c
        public boolean m(View view, int i) {
            BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.this;
            int i2 = bottomSheetBehavior.G;
            if (i2 == 1 || bottomSheetBehavior.U) {
                return false;
            }
            if (i2 == 3 && bottomSheetBehavior.S == i) {
                WeakReference<View> weakReference = bottomSheetBehavior.P;
                View view2 = weakReference != null ? weakReference.get() : null;
                if (view2 != null && view2.canScrollVertically(-1)) {
                    return false;
                }
            }
            WeakReference<V> weakReference2 = BottomSheetBehavior.this.O;
            return weakReference2 != null && weakReference2.get() == view;
        }

        public final boolean n(View view) {
            int top = view.getTop();
            BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.this;
            return top > (bottomSheetBehavior.N + bottomSheetBehavior.z()) / 2;
        }
    }

    /* loaded from: classes2.dex */
    public class f implements e6 {
        public final /* synthetic */ int a;

        public f(int i) {
            this.a = i;
        }

        @Override // defpackage.e6
        public boolean a(View view, e6.a aVar) {
            BottomSheetBehavior.this.V(this.a);
            return true;
        }
    }

    /* loaded from: classes2.dex */
    public static abstract class g {
        public abstract void a(View view, float f);

        public abstract void b(View view, int i);
    }

    /* loaded from: classes2.dex */
    public class h implements Runnable {
        public final View a;
        public boolean f0;
        public int g0;

        public h(View view, int i) {
            this.a = view;
            this.g0 = i;
        }

        @Override // java.lang.Runnable
        public void run() {
            ji4 ji4Var = BottomSheetBehavior.this.H;
            if (ji4Var != null && ji4Var.n(true)) {
                ei4.l0(this.a, this);
            } else {
                BottomSheetBehavior.this.W(this.g0);
            }
            this.f0 = false;
        }
    }

    public BottomSheetBehavior() {
        this.a = 0;
        this.b = true;
        this.c = false;
        this.k = -1;
        this.v = null;
        this.A = 0.5f;
        this.C = -1.0f;
        this.F = true;
        this.G = 4;
        this.Q = new ArrayList<>();
        this.W = -1;
        this.X = new e();
    }

    public static <V extends View> BottomSheetBehavior<V> y(V v) {
        ViewGroup.LayoutParams layoutParams = v.getLayoutParams();
        if (layoutParams instanceof CoordinatorLayout.e) {
            CoordinatorLayout.Behavior f2 = ((CoordinatorLayout.e) layoutParams).f();
            if (f2 instanceof BottomSheetBehavior) {
                return (BottomSheetBehavior) f2;
            }
            throw new IllegalArgumentException("The view is not associated with BottomSheetBehavior");
        }
        throw new IllegalArgumentException("The view is not a child of CoordinatorLayout");
    }

    public o42 A() {
        return this.j;
    }

    public int B() {
        if (this.f) {
            return -1;
        }
        return this.e;
    }

    public int C() {
        return this.G;
    }

    public final float D() {
        VelocityTracker velocityTracker = this.R;
        if (velocityTracker == null) {
            return Utils.FLOAT_EPSILON;
        }
        velocityTracker.computeCurrentVelocity(1000, this.d);
        return this.R.getYVelocity(this.S);
    }

    public boolean E() {
        return this.m;
    }

    public boolean F() {
        return this.D;
    }

    public void G(g gVar) {
        this.Q.remove(gVar);
    }

    public final void H(V v, b6.a aVar, int i) {
        ei4.p0(v, aVar, null, s(i));
    }

    public final void I() {
        this.S = -1;
        VelocityTracker velocityTracker = this.R;
        if (velocityTracker != null) {
            velocityTracker.recycle();
            this.R = null;
        }
    }

    public final void J(SavedState savedState) {
        int i = this.a;
        if (i == 0) {
            return;
        }
        if (i == -1 || (i & 1) == 1) {
            this.e = savedState.h0;
        }
        if (i == -1 || (i & 2) == 2) {
            this.b = savedState.i0;
        }
        if (i == -1 || (i & 4) == 4) {
            this.D = savedState.j0;
        }
        if (i == -1 || (i & 8) == 8) {
            this.E = savedState.k0;
        }
    }

    public void K(boolean z) {
        this.F = z;
    }

    public void L(int i) {
        if (i >= 0) {
            this.x = i;
            return;
        }
        throw new IllegalArgumentException("offset must be greater than or equal to 0");
    }

    public void M(boolean z) {
        if (this.b == z) {
            return;
        }
        this.b = z;
        if (this.O != null) {
            p();
        }
        W((this.b && this.G == 6) ? 3 : this.G);
        c0();
    }

    public void N(boolean z) {
        this.m = z;
    }

    public void O(float f2) {
        if (f2 > Utils.FLOAT_EPSILON && f2 < 1.0f) {
            this.A = f2;
            if (this.O != null) {
                q();
                return;
            }
            return;
        }
        throw new IllegalArgumentException("ratio must be a float value between 0 and 1");
    }

    public void P(boolean z) {
        if (this.D != z) {
            this.D = z;
            if (!z && this.G == 5) {
                V(4);
            }
            c0();
        }
    }

    public void Q(int i) {
        this.k = i;
    }

    public void R(int i) {
        S(i, false);
    }

    public final void S(int i, boolean z) {
        boolean z2 = true;
        if (i == -1) {
            if (!this.f) {
                this.f = true;
            }
            z2 = false;
        } else {
            if (this.f || this.e != i) {
                this.f = false;
                this.e = Math.max(0, i);
            }
            z2 = false;
        }
        if (z2) {
            f0(z);
        }
    }

    public void T(int i) {
        this.a = i;
    }

    public void U(boolean z) {
        this.E = z;
    }

    public void V(int i) {
        if (i == this.G) {
            return;
        }
        if (this.O == null) {
            if (i == 4 || i == 3 || i == 6 || (this.D && i == 5)) {
                this.G = i;
                return;
            }
            return;
        }
        Z(i);
    }

    public void W(int i) {
        V v;
        if (this.G == i) {
            return;
        }
        this.G = i;
        WeakReference<V> weakReference = this.O;
        if (weakReference == null || (v = weakReference.get()) == null) {
            return;
        }
        if (i == 3) {
            e0(true);
        } else if (i == 6 || i == 5 || i == 4) {
            e0(false);
        }
        d0(i);
        for (int i2 = 0; i2 < this.Q.size(); i2++) {
            this.Q.get(i2).b(v, i);
        }
        c0();
    }

    public final void X(View view) {
        boolean z = (Build.VERSION.SDK_INT < 29 || E() || this.f) ? false : true;
        if (this.n || this.o || this.p || z) {
            mk4.a(view, new d(z));
        }
    }

    public void Y(View view, int i) {
        int i2;
        int i3;
        if (i == 4) {
            i2 = this.B;
        } else if (i == 6) {
            int i4 = this.z;
            if (!this.b || i4 > (i3 = this.y)) {
                i2 = i4;
            } else {
                i = 3;
                i2 = i3;
            }
        } else if (i == 3) {
            i2 = z();
        } else if (this.D && i == 5) {
            i2 = this.N;
        } else {
            throw new IllegalArgumentException("Illegal state argument: " + i);
        }
        b0(view, i, i2, false);
    }

    public final void Z(int i) {
        V v = this.O.get();
        if (v == null) {
            return;
        }
        ViewParent parent = v.getParent();
        if (parent != null && parent.isLayoutRequested() && ei4.V(v)) {
            v.post(new b(v, i));
        } else {
            Y(v, i);
        }
    }

    public boolean a0(View view, float f2) {
        if (this.E) {
            return true;
        }
        if (view.getTop() < this.B) {
            return false;
        }
        return Math.abs((((float) view.getTop()) + (f2 * 0.1f)) - ((float) this.B)) / ((float) r()) > 0.5f;
    }

    public void b0(View view, int i, int i2, boolean z) {
        ji4 ji4Var = this.H;
        if (ji4Var != null && (!z ? !ji4Var.R(view, view.getLeft(), i2) : !ji4Var.P(view.getLeft(), i2))) {
            W(2);
            d0(i);
            if (this.v == null) {
                this.v = new h(view, i);
            }
            if (!this.v.f0) {
                BottomSheetBehavior<V>.h hVar = this.v;
                hVar.g0 = i;
                ei4.l0(view, hVar);
                this.v.f0 = true;
                return;
            }
            this.v.g0 = i;
            return;
        }
        W(i);
    }

    public final void c0() {
        V v;
        WeakReference<V> weakReference = this.O;
        if (weakReference == null || (v = weakReference.get()) == null) {
            return;
        }
        ei4.n0(v, 524288);
        ei4.n0(v, 262144);
        ei4.n0(v, 1048576);
        int i = this.W;
        if (i != -1) {
            ei4.n0(v, i);
        }
        if (!this.b && this.G != 6) {
            this.W = n(v, r13.bottomsheet_action_expand_halfway, 6);
        }
        if (this.D && this.G != 5) {
            H(v, b6.a.l, 5);
        }
        int i2 = this.G;
        if (i2 == 3) {
            H(v, b6.a.k, this.b ? 4 : 6);
        } else if (i2 == 4) {
            H(v, b6.a.j, this.b ? 3 : 6);
        } else if (i2 != 6) {
        } else {
            H(v, b6.a.k, 4);
            H(v, b6.a.j, 3);
        }
    }

    public final void d0(int i) {
        ValueAnimator valueAnimator;
        if (i == 2) {
            return;
        }
        boolean z = i == 3;
        if (this.u != z) {
            this.u = z;
            if (this.j == null || (valueAnimator = this.w) == null) {
                return;
            }
            if (valueAnimator.isRunning()) {
                this.w.reverse();
                return;
            }
            float f2 = z ? Utils.FLOAT_EPSILON : 1.0f;
            this.w.setFloatValues(1.0f - f2, f2);
            this.w.start();
        }
    }

    public final void e0(boolean z) {
        Map<View, Integer> map;
        WeakReference<V> weakReference = this.O;
        if (weakReference == null) {
            return;
        }
        ViewParent parent = weakReference.get().getParent();
        if (parent instanceof CoordinatorLayout) {
            CoordinatorLayout coordinatorLayout = (CoordinatorLayout) parent;
            int childCount = coordinatorLayout.getChildCount();
            if (Build.VERSION.SDK_INT >= 16 && z) {
                if (this.V != null) {
                    return;
                }
                this.V = new HashMap(childCount);
            }
            for (int i = 0; i < childCount; i++) {
                View childAt = coordinatorLayout.getChildAt(i);
                if (childAt != this.O.get()) {
                    if (z) {
                        if (Build.VERSION.SDK_INT >= 16) {
                            this.V.put(childAt, Integer.valueOf(childAt.getImportantForAccessibility()));
                        }
                        if (this.c) {
                            ei4.D0(childAt, 4);
                        }
                    } else if (this.c && (map = this.V) != null && map.containsKey(childAt)) {
                        ei4.D0(childAt, this.V.get(childAt).intValue());
                    }
                }
            }
            if (!z) {
                this.V = null;
            } else if (this.c) {
                this.O.get().sendAccessibilityEvent(8);
            }
        }
    }

    public final void f0(boolean z) {
        V v;
        if (this.O != null) {
            p();
            if (this.G != 4 || (v = this.O.get()) == null) {
                return;
            }
            if (z) {
                Z(this.G);
            } else {
                v.requestLayout();
            }
        }
    }

    public final int n(V v, int i, int i2) {
        return ei4.c(v, v.getResources().getString(i), s(i2));
    }

    public void o(g gVar) {
        if (this.Q.contains(gVar)) {
            return;
        }
        this.Q.add(gVar);
    }

    @Override // androidx.coordinatorlayout.widget.CoordinatorLayout.Behavior
    public void onAttachedToLayoutParams(CoordinatorLayout.e eVar) {
        super.onAttachedToLayoutParams(eVar);
        this.O = null;
        this.H = null;
    }

    @Override // androidx.coordinatorlayout.widget.CoordinatorLayout.Behavior
    public void onDetachedFromLayoutParams() {
        super.onDetachedFromLayoutParams();
        this.O = null;
        this.H = null;
    }

    @Override // androidx.coordinatorlayout.widget.CoordinatorLayout.Behavior
    public boolean onInterceptTouchEvent(CoordinatorLayout coordinatorLayout, V v, MotionEvent motionEvent) {
        ji4 ji4Var;
        if (v.isShown() && this.F) {
            int actionMasked = motionEvent.getActionMasked();
            if (actionMasked == 0) {
                I();
            }
            if (this.R == null) {
                this.R = VelocityTracker.obtain();
            }
            this.R.addMovement(motionEvent);
            if (actionMasked == 0) {
                int x = (int) motionEvent.getX();
                this.T = (int) motionEvent.getY();
                if (this.G != 2) {
                    WeakReference<View> weakReference = this.P;
                    View view = weakReference != null ? weakReference.get() : null;
                    if (view != null && coordinatorLayout.isPointInChildBounds(view, x, this.T)) {
                        this.S = motionEvent.getPointerId(motionEvent.getActionIndex());
                        this.U = true;
                    }
                }
                this.I = this.S == -1 && !coordinatorLayout.isPointInChildBounds(v, x, this.T);
            } else if (actionMasked == 1 || actionMasked == 3) {
                this.U = false;
                this.S = -1;
                if (this.I) {
                    this.I = false;
                    return false;
                }
            }
            if (this.I || (ji4Var = this.H) == null || !ji4Var.Q(motionEvent)) {
                WeakReference<View> weakReference2 = this.P;
                View view2 = weakReference2 != null ? weakReference2.get() : null;
                return (actionMasked != 2 || view2 == null || this.I || this.G == 1 || coordinatorLayout.isPointInChildBounds(view2, (int) motionEvent.getX(), (int) motionEvent.getY()) || this.H == null || Math.abs(((float) this.T) - motionEvent.getY()) <= ((float) this.H.A())) ? false : true;
            }
            return true;
        }
        this.I = true;
        return false;
    }

    @Override // androidx.coordinatorlayout.widget.CoordinatorLayout.Behavior
    public boolean onLayoutChild(CoordinatorLayout coordinatorLayout, V v, int i) {
        o42 o42Var;
        if (ei4.B(coordinatorLayout) && !ei4.B(v)) {
            v.setFitsSystemWindows(true);
        }
        if (this.O == null) {
            this.g = coordinatorLayout.getResources().getDimensionPixelSize(jz2.design_bottom_sheet_peek_height_min);
            X(v);
            this.O = new WeakReference<>(v);
            if (this.i && (o42Var = this.j) != null) {
                ei4.w0(v, o42Var);
            }
            o42 o42Var2 = this.j;
            if (o42Var2 != null) {
                float f2 = this.C;
                if (f2 == -1.0f) {
                    f2 = ei4.y(v);
                }
                o42Var2.Z(f2);
                boolean z = this.G == 3;
                this.u = z;
                this.j.b0(z ? Utils.FLOAT_EPSILON : 1.0f);
            }
            c0();
            if (ei4.C(v) == 0) {
                ei4.D0(v, 1);
            }
            int measuredWidth = v.getMeasuredWidth();
            int i2 = this.k;
            if (measuredWidth > i2 && i2 != -1) {
                ViewGroup.LayoutParams layoutParams = v.getLayoutParams();
                layoutParams.width = this.k;
                v.post(new a(this, v, layoutParams));
            }
        }
        if (this.H == null) {
            this.H = ji4.p(coordinatorLayout, this.X);
        }
        int top = v.getTop();
        coordinatorLayout.onLayoutChild(v, i);
        this.M = coordinatorLayout.getWidth();
        this.N = coordinatorLayout.getHeight();
        int height = v.getHeight();
        this.L = height;
        int i3 = this.N;
        int i4 = i3 - height;
        int i5 = this.s;
        if (i4 < i5) {
            if (this.q) {
                this.L = i3;
            } else {
                this.L = i3 - i5;
            }
        }
        this.y = Math.max(0, i3 - this.L);
        q();
        p();
        int i6 = this.G;
        if (i6 == 3) {
            ei4.d0(v, z());
        } else if (i6 == 6) {
            ei4.d0(v, this.z);
        } else if (this.D && i6 == 5) {
            ei4.d0(v, this.N);
        } else if (i6 == 4) {
            ei4.d0(v, this.B);
        } else if (i6 == 1 || i6 == 2) {
            ei4.d0(v, top - v.getTop());
        }
        this.P = new WeakReference<>(x(v));
        return true;
    }

    @Override // androidx.coordinatorlayout.widget.CoordinatorLayout.Behavior
    public boolean onNestedPreFling(CoordinatorLayout coordinatorLayout, V v, View view, float f2, float f3) {
        WeakReference<View> weakReference = this.P;
        if (weakReference == null || view != weakReference.get()) {
            return false;
        }
        return this.G != 3 || super.onNestedPreFling(coordinatorLayout, v, view, f2, f3);
    }

    @Override // androidx.coordinatorlayout.widget.CoordinatorLayout.Behavior
    public void onNestedPreScroll(CoordinatorLayout coordinatorLayout, V v, View view, int i, int i2, int[] iArr, int i3) {
        if (i3 == 1) {
            return;
        }
        WeakReference<View> weakReference = this.P;
        if (view != (weakReference != null ? weakReference.get() : null)) {
            return;
        }
        int top = v.getTop();
        int i4 = top - i2;
        if (i2 > 0) {
            if (i4 < z()) {
                iArr[1] = top - z();
                ei4.d0(v, -iArr[1]);
                W(3);
            } else if (!this.F) {
                return;
            } else {
                iArr[1] = i2;
                ei4.d0(v, -i2);
                W(1);
            }
        } else if (i2 < 0 && !view.canScrollVertically(-1)) {
            int i5 = this.B;
            if (i4 > i5 && !this.D) {
                iArr[1] = top - i5;
                ei4.d0(v, -iArr[1]);
                W(4);
            } else if (!this.F) {
                return;
            } else {
                iArr[1] = i2;
                ei4.d0(v, -i2);
                W(1);
            }
        }
        w(v.getTop());
        this.J = i2;
        this.K = true;
    }

    @Override // androidx.coordinatorlayout.widget.CoordinatorLayout.Behavior
    public void onNestedScroll(CoordinatorLayout coordinatorLayout, V v, View view, int i, int i2, int i3, int i4, int i5, int[] iArr) {
    }

    @Override // androidx.coordinatorlayout.widget.CoordinatorLayout.Behavior
    public void onRestoreInstanceState(CoordinatorLayout coordinatorLayout, V v, Parcelable parcelable) {
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(coordinatorLayout, v, savedState.a());
        J(savedState);
        int i = savedState.g0;
        if (i != 1 && i != 2) {
            this.G = i;
        } else {
            this.G = 4;
        }
    }

    @Override // androidx.coordinatorlayout.widget.CoordinatorLayout.Behavior
    public Parcelable onSaveInstanceState(CoordinatorLayout coordinatorLayout, V v) {
        return new SavedState(super.onSaveInstanceState(coordinatorLayout, v), (BottomSheetBehavior<?>) this);
    }

    @Override // androidx.coordinatorlayout.widget.CoordinatorLayout.Behavior
    public boolean onStartNestedScroll(CoordinatorLayout coordinatorLayout, V v, View view, View view2, int i, int i2) {
        this.J = 0;
        this.K = false;
        return (i & 2) != 0;
    }

    @Override // androidx.coordinatorlayout.widget.CoordinatorLayout.Behavior
    public void onStopNestedScroll(CoordinatorLayout coordinatorLayout, V v, View view, int i) {
        int i2;
        int i3 = 3;
        if (v.getTop() == z()) {
            W(3);
            return;
        }
        WeakReference<View> weakReference = this.P;
        if (weakReference != null && view == weakReference.get() && this.K) {
            if (this.J > 0) {
                if (this.b) {
                    i2 = this.y;
                } else {
                    int top = v.getTop();
                    int i4 = this.z;
                    if (top > i4) {
                        i2 = i4;
                        i3 = 6;
                    } else {
                        i2 = z();
                    }
                }
            } else if (this.D && a0(v, D())) {
                i2 = this.N;
                i3 = 5;
            } else if (this.J == 0) {
                int top2 = v.getTop();
                if (this.b) {
                    if (Math.abs(top2 - this.y) < Math.abs(top2 - this.B)) {
                        i2 = this.y;
                    } else {
                        i2 = this.B;
                        i3 = 4;
                    }
                } else {
                    int i5 = this.z;
                    if (top2 < i5) {
                        if (top2 < Math.abs(top2 - this.B)) {
                            i2 = z();
                        } else {
                            i2 = this.z;
                        }
                    } else if (Math.abs(top2 - i5) < Math.abs(top2 - this.B)) {
                        i2 = this.z;
                    } else {
                        i2 = this.B;
                        i3 = 4;
                    }
                    i3 = 6;
                }
            } else {
                if (this.b) {
                    i2 = this.B;
                } else {
                    int top3 = v.getTop();
                    if (Math.abs(top3 - this.z) < Math.abs(top3 - this.B)) {
                        i2 = this.z;
                        i3 = 6;
                    } else {
                        i2 = this.B;
                    }
                }
                i3 = 4;
            }
            b0(v, i3, i2, false);
            this.K = false;
        }
    }

    @Override // androidx.coordinatorlayout.widget.CoordinatorLayout.Behavior
    public boolean onTouchEvent(CoordinatorLayout coordinatorLayout, V v, MotionEvent motionEvent) {
        if (v.isShown()) {
            int actionMasked = motionEvent.getActionMasked();
            if (this.G == 1 && actionMasked == 0) {
                return true;
            }
            ji4 ji4Var = this.H;
            if (ji4Var != null) {
                ji4Var.G(motionEvent);
            }
            if (actionMasked == 0) {
                I();
            }
            if (this.R == null) {
                this.R = VelocityTracker.obtain();
            }
            this.R.addMovement(motionEvent);
            if (this.H != null && actionMasked == 2 && !this.I && Math.abs(this.T - motionEvent.getY()) > this.H.A()) {
                this.H.c(v, motionEvent.getPointerId(motionEvent.getActionIndex()));
            }
            return !this.I;
        }
        return false;
    }

    public final void p() {
        int r = r();
        if (this.b) {
            this.B = Math.max(this.N - r, this.y);
        } else {
            this.B = this.N - r;
        }
    }

    public final void q() {
        this.z = (int) (this.N * (1.0f - this.A));
    }

    public final int r() {
        int i;
        if (this.f) {
            return Math.min(Math.max(this.g, this.N - ((this.M * 9) / 16)), this.L) + this.r;
        }
        if (!this.m && !this.n && (i = this.l) > 0) {
            return Math.max(this.e, i + this.h);
        }
        return this.e + this.r;
    }

    public final e6 s(int i) {
        return new f(i);
    }

    public final void t(Context context, AttributeSet attributeSet, boolean z) {
        u(context, attributeSet, z, null);
    }

    public final void u(Context context, AttributeSet attributeSet, boolean z, ColorStateList colorStateList) {
        if (this.i) {
            this.t = pn3.e(context, attributeSet, gy2.bottomSheetStyle, Y).m();
            o42 o42Var = new o42(this.t);
            this.j = o42Var;
            o42Var.P(context);
            if (z && colorStateList != null) {
                this.j.a0(colorStateList);
                return;
            }
            TypedValue typedValue = new TypedValue();
            context.getTheme().resolveAttribute(16842801, typedValue, true);
            this.j.setTint(typedValue.data);
        }
    }

    public final void v() {
        ValueAnimator ofFloat = ValueAnimator.ofFloat(Utils.FLOAT_EPSILON, 1.0f);
        this.w = ofFloat;
        ofFloat.setDuration(500L);
        this.w.addUpdateListener(new c());
    }

    public void w(int i) {
        float f2;
        float f3;
        V v = this.O.get();
        if (v == null || this.Q.isEmpty()) {
            return;
        }
        int i2 = this.B;
        if (i <= i2 && i2 != z()) {
            int i3 = this.B;
            f2 = i3 - i;
            f3 = i3 - z();
        } else {
            int i4 = this.B;
            f2 = i4 - i;
            f3 = this.N - i4;
        }
        float f4 = f2 / f3;
        for (int i5 = 0; i5 < this.Q.size(); i5++) {
            this.Q.get(i5).a(v, f4);
        }
    }

    public View x(View view) {
        if (ei4.X(view)) {
            return view;
        }
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            int childCount = viewGroup.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View x = x(viewGroup.getChildAt(i));
                if (x != null) {
                    return x;
                }
            }
            return null;
        }
        return null;
    }

    public int z() {
        if (this.b) {
            return this.y;
        }
        return Math.max(this.x, this.q ? 0 : this.s);
    }

    /* loaded from: classes2.dex */
    public static class SavedState extends AbsSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new a();
        public final int g0;
        public int h0;
        public boolean i0;
        public boolean j0;
        public boolean k0;

        /* loaded from: classes2.dex */
        public static class a implements Parcelable.ClassLoaderCreator<SavedState> {
            @Override // android.os.Parcelable.Creator
            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel, (ClassLoader) null);
            }

            @Override // android.os.Parcelable.ClassLoaderCreator
            /* renamed from: b */
            public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new SavedState(parcel, classLoader);
            }

            @Override // android.os.Parcelable.Creator
            /* renamed from: c */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        }

        public SavedState(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            this.g0 = parcel.readInt();
            this.h0 = parcel.readInt();
            this.i0 = parcel.readInt() == 1;
            this.j0 = parcel.readInt() == 1;
            this.k0 = parcel.readInt() == 1;
        }

        @Override // androidx.customview.view.AbsSavedState, android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.g0);
            parcel.writeInt(this.h0);
            parcel.writeInt(this.i0 ? 1 : 0);
            parcel.writeInt(this.j0 ? 1 : 0);
            parcel.writeInt(this.k0 ? 1 : 0);
        }

        public SavedState(Parcelable parcelable, BottomSheetBehavior<?> bottomSheetBehavior) {
            super(parcelable);
            this.g0 = bottomSheetBehavior.G;
            this.h0 = bottomSheetBehavior.e;
            this.i0 = bottomSheetBehavior.b;
            this.j0 = bottomSheetBehavior.D;
            this.k0 = bottomSheetBehavior.E;
        }
    }

    public BottomSheetBehavior(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        int i;
        this.a = 0;
        this.b = true;
        this.c = false;
        this.k = -1;
        this.v = null;
        this.A = 0.5f;
        this.C = -1.0f;
        this.F = true;
        this.G = 4;
        this.Q = new ArrayList<>();
        this.W = -1;
        this.X = new e();
        this.h = context.getResources().getDimensionPixelSize(jz2.mtrl_min_touch_target_size);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, o23.BottomSheetBehavior_Layout);
        this.i = obtainStyledAttributes.hasValue(o23.BottomSheetBehavior_Layout_shapeAppearance);
        int i2 = o23.BottomSheetBehavior_Layout_backgroundTint;
        boolean hasValue = obtainStyledAttributes.hasValue(i2);
        if (hasValue) {
            u(context, attributeSet, hasValue, n42.b(context, obtainStyledAttributes, i2));
        } else {
            t(context, attributeSet, hasValue);
        }
        v();
        if (Build.VERSION.SDK_INT >= 21) {
            this.C = obtainStyledAttributes.getDimension(o23.BottomSheetBehavior_Layout_android_elevation, -1.0f);
        }
        int i3 = o23.BottomSheetBehavior_Layout_android_maxWidth;
        if (obtainStyledAttributes.hasValue(i3)) {
            Q(obtainStyledAttributes.getDimensionPixelSize(i3, -1));
        }
        int i4 = o23.BottomSheetBehavior_Layout_behavior_peekHeight;
        TypedValue peekValue = obtainStyledAttributes.peekValue(i4);
        if (peekValue != null && (i = peekValue.data) == -1) {
            R(i);
        } else {
            R(obtainStyledAttributes.getDimensionPixelSize(i4, -1));
        }
        P(obtainStyledAttributes.getBoolean(o23.BottomSheetBehavior_Layout_behavior_hideable, false));
        N(obtainStyledAttributes.getBoolean(o23.BottomSheetBehavior_Layout_gestureInsetBottomIgnored, false));
        M(obtainStyledAttributes.getBoolean(o23.BottomSheetBehavior_Layout_behavior_fitToContents, true));
        U(obtainStyledAttributes.getBoolean(o23.BottomSheetBehavior_Layout_behavior_skipCollapsed, false));
        K(obtainStyledAttributes.getBoolean(o23.BottomSheetBehavior_Layout_behavior_draggable, true));
        T(obtainStyledAttributes.getInt(o23.BottomSheetBehavior_Layout_behavior_saveFlags, 0));
        O(obtainStyledAttributes.getFloat(o23.BottomSheetBehavior_Layout_behavior_halfExpandedRatio, 0.5f));
        int i5 = o23.BottomSheetBehavior_Layout_behavior_expandedOffset;
        TypedValue peekValue2 = obtainStyledAttributes.peekValue(i5);
        if (peekValue2 != null && peekValue2.type == 16) {
            L(peekValue2.data);
        } else {
            L(obtainStyledAttributes.getDimensionPixelOffset(i5, 0));
        }
        this.n = obtainStyledAttributes.getBoolean(o23.BottomSheetBehavior_Layout_paddingBottomSystemWindowInsets, false);
        this.o = obtainStyledAttributes.getBoolean(o23.BottomSheetBehavior_Layout_paddingLeftSystemWindowInsets, false);
        this.p = obtainStyledAttributes.getBoolean(o23.BottomSheetBehavior_Layout_paddingRightSystemWindowInsets, false);
        this.q = obtainStyledAttributes.getBoolean(o23.BottomSheetBehavior_Layout_paddingTopSystemWindowInsets, true);
        obtainStyledAttributes.recycle();
        this.d = ViewConfiguration.get(context).getScaledMaximumFlingVelocity();
    }
}
