package com.google.android.material.bottomsheet;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

/* compiled from: BottomSheetDialogFragment.java */
/* loaded from: classes2.dex */
public class b extends ff {
    public boolean u0;

    /* compiled from: BottomSheetDialogFragment.java */
    /* renamed from: com.google.android.material.bottomsheet.b$b  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public class C0115b extends BottomSheetBehavior.g {
        public C0115b() {
        }

        @Override // com.google.android.material.bottomsheet.BottomSheetBehavior.g
        public void a(View view, float f) {
        }

        @Override // com.google.android.material.bottomsheet.BottomSheetBehavior.g
        public void b(View view, int i) {
            if (i == 5) {
                b.this.w();
            }
        }
    }

    @Override // defpackage.sn0
    public void h() {
        if (y(false)) {
            return;
        }
        super.h();
    }

    @Override // defpackage.sn0
    public void i() {
        if (y(true)) {
            return;
        }
        super.i();
    }

    @Override // defpackage.ff, defpackage.sn0
    public Dialog m(Bundle bundle) {
        return new com.google.android.material.bottomsheet.a(getContext(), l());
    }

    public final void w() {
        if (this.u0) {
            super.i();
        } else {
            super.h();
        }
    }

    public final void x(BottomSheetBehavior<?> bottomSheetBehavior, boolean z) {
        this.u0 = z;
        if (bottomSheetBehavior.C() == 5) {
            w();
            return;
        }
        if (k() instanceof com.google.android.material.bottomsheet.a) {
            ((com.google.android.material.bottomsheet.a) k()).removeDefaultCallback();
        }
        bottomSheetBehavior.o(new C0115b());
        bottomSheetBehavior.V(5);
    }

    public final boolean y(boolean z) {
        Dialog k = k();
        if (k instanceof com.google.android.material.bottomsheet.a) {
            com.google.android.material.bottomsheet.a aVar = (com.google.android.material.bottomsheet.a) k;
            BottomSheetBehavior<FrameLayout> behavior = aVar.getBehavior();
            if (behavior.F() && aVar.getDismissWithAnimation()) {
                x(behavior, z);
                return true;
            }
            return false;
        }
        return false;
    }
}
