package com.google.android.material.button;

import android.content.Context;
import android.graphics.Canvas;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.LinearLayout;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.material.button.MaterialButton;
import defpackage.b6;
import defpackage.pn3;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.TreeMap;

/* loaded from: classes2.dex */
public class MaterialButtonToggleGroup extends LinearLayout {
    public static final String o0 = MaterialButtonToggleGroup.class.getSimpleName();
    public static final int p0 = y13.Widget_MaterialComponents_MaterialButtonToggleGroup;
    public final List<d> a;
    public final c f0;
    public final f g0;
    public final LinkedHashSet<e> h0;
    public final Comparator<MaterialButton> i0;
    public Integer[] j0;
    public boolean k0;
    public boolean l0;
    public boolean m0;
    public int n0;

    /* loaded from: classes2.dex */
    public class a implements Comparator<MaterialButton> {
        public a() {
        }

        @Override // java.util.Comparator
        /* renamed from: a */
        public int compare(MaterialButton materialButton, MaterialButton materialButton2) {
            int compareTo = Boolean.valueOf(materialButton.isChecked()).compareTo(Boolean.valueOf(materialButton2.isChecked()));
            if (compareTo != 0) {
                return compareTo;
            }
            int compareTo2 = Boolean.valueOf(materialButton.isPressed()).compareTo(Boolean.valueOf(materialButton2.isPressed()));
            return compareTo2 != 0 ? compareTo2 : Integer.valueOf(MaterialButtonToggleGroup.this.indexOfChild(materialButton)).compareTo(Integer.valueOf(MaterialButtonToggleGroup.this.indexOfChild(materialButton2)));
        }
    }

    /* loaded from: classes2.dex */
    public class b extends z5 {
        public b() {
        }

        @Override // defpackage.z5
        public void g(View view, b6 b6Var) {
            super.g(view, b6Var);
            b6Var.f0(b6.c.a(0, 1, MaterialButtonToggleGroup.this.n(view), 1, false, ((MaterialButton) view).isChecked()));
        }
    }

    /* loaded from: classes2.dex */
    public class c implements MaterialButton.a {
        public c() {
        }

        @Override // com.google.android.material.button.MaterialButton.a
        public void a(MaterialButton materialButton, boolean z) {
            if (MaterialButtonToggleGroup.this.k0) {
                return;
            }
            if (MaterialButtonToggleGroup.this.l0) {
                MaterialButtonToggleGroup.this.n0 = z ? materialButton.getId() : -1;
            }
            if (MaterialButtonToggleGroup.this.u(materialButton.getId(), z)) {
                MaterialButtonToggleGroup.this.l(materialButton.getId(), materialButton.isChecked());
            }
            MaterialButtonToggleGroup.this.invalidate();
        }

        public /* synthetic */ c(MaterialButtonToggleGroup materialButtonToggleGroup, a aVar) {
            this();
        }
    }

    /* loaded from: classes2.dex */
    public static class d {
        public static final v80 e = new o4(Utils.FLOAT_EPSILON);
        public v80 a;
        public v80 b;
        public v80 c;
        public v80 d;

        public d(v80 v80Var, v80 v80Var2, v80 v80Var3, v80 v80Var4) {
            this.a = v80Var;
            this.b = v80Var3;
            this.c = v80Var4;
            this.d = v80Var2;
        }

        public static d a(d dVar) {
            v80 v80Var = e;
            return new d(v80Var, dVar.d, v80Var, dVar.c);
        }

        public static d b(d dVar, View view) {
            return mk4.h(view) ? c(dVar) : d(dVar);
        }

        public static d c(d dVar) {
            v80 v80Var = dVar.a;
            v80 v80Var2 = dVar.d;
            v80 v80Var3 = e;
            return new d(v80Var, v80Var2, v80Var3, v80Var3);
        }

        public static d d(d dVar) {
            v80 v80Var = e;
            return new d(v80Var, v80Var, dVar.b, dVar.c);
        }

        public static d e(d dVar, View view) {
            return mk4.h(view) ? d(dVar) : c(dVar);
        }

        public static d f(d dVar) {
            v80 v80Var = dVar.a;
            v80 v80Var2 = e;
            return new d(v80Var, v80Var2, dVar.b, v80Var2);
        }
    }

    /* loaded from: classes2.dex */
    public interface e {
        void a(MaterialButtonToggleGroup materialButtonToggleGroup, int i, boolean z);
    }

    /* loaded from: classes2.dex */
    public class f implements MaterialButton.b {
        public f() {
        }

        @Override // com.google.android.material.button.MaterialButton.b
        public void a(MaterialButton materialButton, boolean z) {
            MaterialButtonToggleGroup.this.invalidate();
        }

        public /* synthetic */ f(MaterialButtonToggleGroup materialButtonToggleGroup, a aVar) {
            this();
        }
    }

    public MaterialButtonToggleGroup(Context context) {
        this(context, null);
    }

    private int getFirstVisibleChildIndex() {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            if (p(i)) {
                return i;
            }
        }
        return -1;
    }

    private int getLastVisibleChildIndex() {
        for (int childCount = getChildCount() - 1; childCount >= 0; childCount--) {
            if (p(childCount)) {
                return childCount;
            }
        }
        return -1;
    }

    private int getVisibleButtonCount() {
        int i = 0;
        for (int i2 = 0; i2 < getChildCount(); i2++) {
            if ((getChildAt(i2) instanceof MaterialButton) && p(i2)) {
                i++;
            }
        }
        return i;
    }

    private void setCheckedId(int i) {
        this.n0 = i;
        l(i, true);
    }

    private void setGeneratedIdIfNeeded(MaterialButton materialButton) {
        if (materialButton.getId() == -1) {
            materialButton.setId(ei4.m());
        }
    }

    private void setupButtonChild(MaterialButton materialButton) {
        materialButton.setMaxLines(1);
        materialButton.setEllipsize(TextUtils.TruncateAt.END);
        materialButton.setCheckable(true);
        materialButton.a(this.f0);
        materialButton.setOnPressedChangeListenerInternal(this.g0);
        materialButton.setShouldDrawSurfaceColorStroke(true);
    }

    public static void t(pn3.b bVar, d dVar) {
        if (dVar == null) {
            bVar.o(Utils.FLOAT_EPSILON);
        } else {
            bVar.F(dVar.a).w(dVar.d).J(dVar.b).A(dVar.c);
        }
    }

    @Override // android.view.ViewGroup
    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        if (view instanceof MaterialButton) {
            super.addView(view, i, layoutParams);
            MaterialButton materialButton = (MaterialButton) view;
            setGeneratedIdIfNeeded(materialButton);
            setupButtonChild(materialButton);
            if (materialButton.isChecked()) {
                u(materialButton.getId(), true);
                setCheckedId(materialButton.getId());
            }
            pn3 shapeAppearanceModel = materialButton.getShapeAppearanceModel();
            this.a.add(new d(shapeAppearanceModel.r(), shapeAppearanceModel.j(), shapeAppearanceModel.t(), shapeAppearanceModel.l()));
            ei4.t0(materialButton, new b());
        }
    }

    @Override // android.view.ViewGroup, android.view.View
    public void dispatchDraw(Canvas canvas) {
        v();
        super.dispatchDraw(canvas);
    }

    public void g(e eVar) {
        this.h0.add(eVar);
    }

    @Override // android.widget.LinearLayout, android.view.ViewGroup, android.view.View
    public CharSequence getAccessibilityClassName() {
        return MaterialButtonToggleGroup.class.getName();
    }

    public int getCheckedButtonId() {
        if (this.l0) {
            return this.n0;
        }
        return -1;
    }

    public List<Integer> getCheckedButtonIds() {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < getChildCount(); i++) {
            MaterialButton m = m(i);
            if (m.isChecked()) {
                arrayList.add(Integer.valueOf(m.getId()));
            }
        }
        return arrayList;
    }

    @Override // android.view.ViewGroup
    public int getChildDrawingOrder(int i, int i2) {
        Integer[] numArr = this.j0;
        return (numArr == null || i2 >= numArr.length) ? i2 : numArr[i2].intValue();
    }

    public final void h() {
        int firstVisibleChildIndex = getFirstVisibleChildIndex();
        if (firstVisibleChildIndex == -1) {
            return;
        }
        for (int i = firstVisibleChildIndex + 1; i < getChildCount(); i++) {
            MaterialButton m = m(i);
            int min = Math.min(m.getStrokeWidth(), m(i - 1).getStrokeWidth());
            LinearLayout.LayoutParams i2 = i(m);
            if (getOrientation() == 0) {
                b42.c(i2, 0);
                b42.d(i2, -min);
                i2.topMargin = 0;
            } else {
                i2.bottomMargin = 0;
                i2.topMargin = -min;
                b42.d(i2, 0);
            }
            m.setLayoutParams(i2);
        }
        r(firstVisibleChildIndex);
    }

    public final LinearLayout.LayoutParams i(View view) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams instanceof LinearLayout.LayoutParams) {
            return (LinearLayout.LayoutParams) layoutParams;
        }
        return new LinearLayout.LayoutParams(layoutParams.width, layoutParams.height);
    }

    public final void j(int i, boolean z) {
        MaterialButton materialButton = (MaterialButton) findViewById(i);
        if (materialButton != null) {
            materialButton.setChecked(z);
        }
    }

    public void k() {
        this.k0 = true;
        for (int i = 0; i < getChildCount(); i++) {
            MaterialButton m = m(i);
            m.setChecked(false);
            l(m.getId(), false);
        }
        this.k0 = false;
        setCheckedId(-1);
    }

    public final void l(int i, boolean z) {
        Iterator<e> it = this.h0.iterator();
        while (it.hasNext()) {
            it.next().a(this, i, z);
        }
    }

    public final MaterialButton m(int i) {
        return (MaterialButton) getChildAt(i);
    }

    public final int n(View view) {
        if (view instanceof MaterialButton) {
            int i = 0;
            for (int i2 = 0; i2 < getChildCount(); i2++) {
                if (getChildAt(i2) == view) {
                    return i;
                }
                if ((getChildAt(i2) instanceof MaterialButton) && p(i2)) {
                    i++;
                }
            }
            return -1;
        }
        return -1;
    }

    public final d o(int i, int i2, int i3) {
        d dVar = this.a.get(i);
        if (i2 == i3) {
            return dVar;
        }
        boolean z = getOrientation() == 0;
        if (i == i2) {
            return z ? d.e(dVar, this) : d.f(dVar);
        } else if (i == i3) {
            return z ? d.b(dVar, this) : d.a(dVar);
        } else {
            return null;
        }
    }

    @Override // android.view.View
    public void onFinishInflate() {
        super.onFinishInflate();
        int i = this.n0;
        if (i != -1) {
            j(i, true);
        }
    }

    @Override // android.view.View
    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        b6.I0(accessibilityNodeInfo).e0(b6.b.b(1, getVisibleButtonCount(), false, q() ? 1 : 2));
    }

    @Override // android.widget.LinearLayout, android.view.View
    public void onMeasure(int i, int i2) {
        w();
        h();
        super.onMeasure(i, i2);
    }

    @Override // android.view.ViewGroup
    public void onViewRemoved(View view) {
        super.onViewRemoved(view);
        if (view instanceof MaterialButton) {
            MaterialButton materialButton = (MaterialButton) view;
            materialButton.h(this.f0);
            materialButton.setOnPressedChangeListenerInternal(null);
        }
        int indexOfChild = indexOfChild(view);
        if (indexOfChild >= 0) {
            this.a.remove(indexOfChild);
        }
        w();
        h();
    }

    public final boolean p(int i) {
        return getChildAt(i).getVisibility() != 8;
    }

    public boolean q() {
        return this.l0;
    }

    public final void r(int i) {
        if (getChildCount() == 0 || i == -1) {
            return;
        }
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) m(i).getLayoutParams();
        if (getOrientation() == 1) {
            layoutParams.topMargin = 0;
            layoutParams.bottomMargin = 0;
            return;
        }
        b42.c(layoutParams, 0);
        b42.d(layoutParams, 0);
        layoutParams.leftMargin = 0;
        layoutParams.rightMargin = 0;
    }

    public final void s(int i, boolean z) {
        View findViewById = findViewById(i);
        if (findViewById instanceof MaterialButton) {
            this.k0 = true;
            ((MaterialButton) findViewById).setChecked(z);
            this.k0 = false;
        }
    }

    public void setSelectionRequired(boolean z) {
        this.m0 = z;
    }

    public void setSingleSelection(boolean z) {
        if (this.l0 != z) {
            this.l0 = z;
            k();
        }
    }

    public final boolean u(int i, boolean z) {
        List<Integer> checkedButtonIds = getCheckedButtonIds();
        if (this.m0 && checkedButtonIds.isEmpty()) {
            s(i, true);
            this.n0 = i;
            return false;
        }
        if (z && this.l0) {
            checkedButtonIds.remove(Integer.valueOf(i));
            for (Integer num : checkedButtonIds) {
                int intValue = num.intValue();
                s(intValue, false);
                l(intValue, false);
            }
        }
        return true;
    }

    public final void v() {
        TreeMap treeMap = new TreeMap(this.i0);
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            treeMap.put(m(i), Integer.valueOf(i));
        }
        this.j0 = (Integer[]) treeMap.values().toArray(new Integer[0]);
    }

    public void w() {
        int childCount = getChildCount();
        int firstVisibleChildIndex = getFirstVisibleChildIndex();
        int lastVisibleChildIndex = getLastVisibleChildIndex();
        for (int i = 0; i < childCount; i++) {
            MaterialButton m = m(i);
            if (m.getVisibility() != 8) {
                pn3.b v = m.getShapeAppearanceModel().v();
                t(v, o(i, firstVisibleChildIndex, lastVisibleChildIndex));
                m.setShapeAppearanceModel(v.m());
            }
        }
    }

    public MaterialButtonToggleGroup(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, gy2.materialButtonToggleGroupStyle);
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public MaterialButtonToggleGroup(android.content.Context r7, android.util.AttributeSet r8, int r9) {
        /*
            r6 = this;
            int r4 = com.google.android.material.button.MaterialButtonToggleGroup.p0
            android.content.Context r7 = defpackage.r42.c(r7, r8, r9, r4)
            r6.<init>(r7, r8, r9)
            java.util.ArrayList r7 = new java.util.ArrayList
            r7.<init>()
            r6.a = r7
            com.google.android.material.button.MaterialButtonToggleGroup$c r7 = new com.google.android.material.button.MaterialButtonToggleGroup$c
            r0 = 0
            r7.<init>(r6, r0)
            r6.f0 = r7
            com.google.android.material.button.MaterialButtonToggleGroup$f r7 = new com.google.android.material.button.MaterialButtonToggleGroup$f
            r7.<init>(r6, r0)
            r6.g0 = r7
            java.util.LinkedHashSet r7 = new java.util.LinkedHashSet
            r7.<init>()
            r6.h0 = r7
            com.google.android.material.button.MaterialButtonToggleGroup$a r7 = new com.google.android.material.button.MaterialButtonToggleGroup$a
            r7.<init>()
            r6.i0 = r7
            r7 = 0
            r6.k0 = r7
            android.content.Context r0 = r6.getContext()
            int[] r2 = defpackage.o23.MaterialButtonToggleGroup
            int[] r5 = new int[r7]
            r1 = r8
            r3 = r9
            android.content.res.TypedArray r8 = defpackage.a54.h(r0, r1, r2, r3, r4, r5)
            int r9 = defpackage.o23.MaterialButtonToggleGroup_singleSelection
            boolean r9 = r8.getBoolean(r9, r7)
            r6.setSingleSelection(r9)
            int r9 = defpackage.o23.MaterialButtonToggleGroup_checkedButton
            r0 = -1
            int r9 = r8.getResourceId(r9, r0)
            r6.n0 = r9
            int r9 = defpackage.o23.MaterialButtonToggleGroup_selectionRequired
            boolean r7 = r8.getBoolean(r9, r7)
            r6.m0 = r7
            r7 = 1
            r6.setChildrenDrawingOrderEnabled(r7)
            r8.recycle()
            defpackage.ei4.D0(r6, r7)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.button.MaterialButtonToggleGroup.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }

    public void setSingleSelection(int i) {
        setSingleSelection(getResources().getBoolean(i));
    }
}
