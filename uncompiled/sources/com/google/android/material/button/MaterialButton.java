package com.google.android.material.button;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.Button;
import android.widget.Checkable;
import android.widget.CompoundButton;
import androidx.appcompat.widget.AppCompatButton;
import androidx.customview.view.AbsSavedState;
import java.util.Iterator;
import java.util.LinkedHashSet;

/* loaded from: classes2.dex */
public class MaterialButton extends AppCompatButton implements Checkable, sn3 {
    public static final int[] t0 = {16842911};
    public static final int[] u0 = {16842912};
    public static final int v0 = y13.Widget_MaterialComponents_Button;
    public final j42 g0;
    public final LinkedHashSet<a> h0;
    public b i0;
    public PorterDuff.Mode j0;
    public ColorStateList k0;
    public Drawable l0;
    public int m0;
    public int n0;
    public int o0;
    public int p0;
    public boolean q0;
    public boolean r0;
    public int s0;

    /* loaded from: classes2.dex */
    public static class SavedState extends AbsSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new a();
        public boolean g0;

        /* loaded from: classes2.dex */
        public static class a implements Parcelable.ClassLoaderCreator<SavedState> {
            @Override // android.os.Parcelable.Creator
            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel, null);
            }

            @Override // android.os.Parcelable.ClassLoaderCreator
            /* renamed from: b */
            public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new SavedState(parcel, classLoader);
            }

            @Override // android.os.Parcelable.Creator
            /* renamed from: c */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public final void b(Parcel parcel) {
            this.g0 = parcel.readInt() == 1;
        }

        @Override // androidx.customview.view.AbsSavedState, android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.g0 ? 1 : 0);
        }

        public SavedState(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            if (classLoader == null) {
                getClass().getClassLoader();
            }
            b(parcel);
        }
    }

    /* loaded from: classes2.dex */
    public interface a {
        void a(MaterialButton materialButton, boolean z);
    }

    /* loaded from: classes2.dex */
    public interface b {
        void a(MaterialButton materialButton, boolean z);
    }

    public MaterialButton(Context context) {
        this(context, null);
    }

    private boolean f() {
        return ei4.E(this) == 1;
    }

    private String getA11yClassName() {
        return (b() ? CompoundButton.class : Button.class).getName();
    }

    private int getTextHeight() {
        TextPaint paint = getPaint();
        String charSequence = getText().toString();
        if (getTransformationMethod() != null) {
            charSequence = getTransformationMethod().getTransformation(charSequence, this).toString();
        }
        Rect rect = new Rect();
        paint.getTextBounds(charSequence, 0, charSequence.length(), rect);
        return Math.min(rect.height(), getLayout().getHeight());
    }

    private int getTextWidth() {
        TextPaint paint = getPaint();
        String charSequence = getText().toString();
        if (getTransformationMethod() != null) {
            charSequence = getTransformationMethod().getTransformation(charSequence, this).toString();
        }
        return Math.min((int) paint.measureText(charSequence), getLayout().getEllipsizedWidth());
    }

    public void a(a aVar) {
        this.h0.add(aVar);
    }

    public boolean b() {
        j42 j42Var = this.g0;
        return j42Var != null && j42Var.p();
    }

    public final boolean c() {
        int i = this.s0;
        return i == 3 || i == 4;
    }

    public final boolean d() {
        int i = this.s0;
        return i == 1 || i == 2;
    }

    public final boolean e() {
        int i = this.s0;
        return i == 16 || i == 32;
    }

    public final boolean g() {
        j42 j42Var = this.g0;
        return (j42Var == null || j42Var.o()) ? false : true;
    }

    @Override // android.view.View
    public ColorStateList getBackgroundTintList() {
        return getSupportBackgroundTintList();
    }

    @Override // android.view.View
    public PorterDuff.Mode getBackgroundTintMode() {
        return getSupportBackgroundTintMode();
    }

    public int getCornerRadius() {
        if (g()) {
            return this.g0.b();
        }
        return 0;
    }

    public Drawable getIcon() {
        return this.l0;
    }

    public int getIconGravity() {
        return this.s0;
    }

    public int getIconPadding() {
        return this.p0;
    }

    public int getIconSize() {
        return this.m0;
    }

    public ColorStateList getIconTint() {
        return this.k0;
    }

    public PorterDuff.Mode getIconTintMode() {
        return this.j0;
    }

    public int getInsetBottom() {
        return this.g0.c();
    }

    public int getInsetTop() {
        return this.g0.d();
    }

    public ColorStateList getRippleColor() {
        if (g()) {
            return this.g0.h();
        }
        return null;
    }

    public pn3 getShapeAppearanceModel() {
        if (g()) {
            return this.g0.i();
        }
        throw new IllegalStateException("Attempted to get ShapeAppearanceModel from a MaterialButton which has an overwritten background.");
    }

    public ColorStateList getStrokeColor() {
        if (g()) {
            return this.g0.j();
        }
        return null;
    }

    public int getStrokeWidth() {
        if (g()) {
            return this.g0.k();
        }
        return 0;
    }

    @Override // androidx.appcompat.widget.AppCompatButton, defpackage.m64
    public ColorStateList getSupportBackgroundTintList() {
        if (g()) {
            return this.g0.l();
        }
        return super.getSupportBackgroundTintList();
    }

    @Override // androidx.appcompat.widget.AppCompatButton, defpackage.m64
    public PorterDuff.Mode getSupportBackgroundTintMode() {
        if (g()) {
            return this.g0.m();
        }
        return super.getSupportBackgroundTintMode();
    }

    public void h(a aVar) {
        this.h0.remove(aVar);
    }

    public final void i() {
        if (d()) {
            t44.l(this, this.l0, null, null, null);
        } else if (c()) {
            t44.l(this, null, null, this.l0, null);
        } else if (e()) {
            t44.l(this, null, this.l0, null, null);
        }
    }

    @Override // android.widget.Checkable
    public boolean isChecked() {
        return this.q0;
    }

    public final void j(boolean z) {
        Drawable drawable = this.l0;
        if (drawable != null) {
            Drawable mutate = androidx.core.graphics.drawable.a.r(drawable).mutate();
            this.l0 = mutate;
            androidx.core.graphics.drawable.a.o(mutate, this.k0);
            PorterDuff.Mode mode = this.j0;
            if (mode != null) {
                androidx.core.graphics.drawable.a.p(this.l0, mode);
            }
            int i = this.m0;
            if (i == 0) {
                i = this.l0.getIntrinsicWidth();
            }
            int i2 = this.m0;
            if (i2 == 0) {
                i2 = this.l0.getIntrinsicHeight();
            }
            Drawable drawable2 = this.l0;
            int i3 = this.n0;
            int i4 = this.o0;
            drawable2.setBounds(i3, i4, i + i3, i2 + i4);
        }
        if (z) {
            i();
            return;
        }
        Drawable[] a2 = t44.a(this);
        boolean z2 = false;
        Drawable drawable3 = a2[0];
        Drawable drawable4 = a2[1];
        Drawable drawable5 = a2[2];
        if ((d() && drawable3 != this.l0) || ((c() && drawable5 != this.l0) || (e() && drawable4 != this.l0))) {
            z2 = true;
        }
        if (z2) {
            i();
        }
    }

    public final void k(int i, int i2) {
        if (this.l0 == null || getLayout() == null) {
            return;
        }
        if (!d() && !c()) {
            if (e()) {
                this.n0 = 0;
                if (this.s0 == 16) {
                    this.o0 = 0;
                    j(false);
                    return;
                }
                int i3 = this.m0;
                if (i3 == 0) {
                    i3 = this.l0.getIntrinsicHeight();
                }
                int textHeight = (((((i2 - getTextHeight()) - getPaddingTop()) - i3) - this.p0) - getPaddingBottom()) / 2;
                if (this.o0 != textHeight) {
                    this.o0 = textHeight;
                    j(false);
                    return;
                }
                return;
            }
            return;
        }
        this.o0 = 0;
        int i4 = this.s0;
        if (i4 != 1 && i4 != 3) {
            int i5 = this.m0;
            if (i5 == 0) {
                i5 = this.l0.getIntrinsicWidth();
            }
            int textWidth = (((((i - getTextWidth()) - ei4.I(this)) - i5) - this.p0) - ei4.J(this)) / 2;
            if (f() != (this.s0 == 4)) {
                textWidth = -textWidth;
            }
            if (this.n0 != textWidth) {
                this.n0 = textWidth;
                j(false);
                return;
            }
            return;
        }
        this.n0 = 0;
        j(false);
    }

    @Override // android.widget.TextView, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (g()) {
            p42.f(this, this.g0.f());
        }
    }

    @Override // android.widget.TextView, android.view.View
    public int[] onCreateDrawableState(int i) {
        int[] onCreateDrawableState = super.onCreateDrawableState(i + 2);
        if (b()) {
            Button.mergeDrawableStates(onCreateDrawableState, t0);
        }
        if (isChecked()) {
            Button.mergeDrawableStates(onCreateDrawableState, u0);
        }
        return onCreateDrawableState;
    }

    @Override // androidx.appcompat.widget.AppCompatButton, android.view.View
    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        accessibilityEvent.setClassName(getA11yClassName());
        accessibilityEvent.setChecked(isChecked());
    }

    @Override // androidx.appcompat.widget.AppCompatButton, android.view.View
    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        accessibilityNodeInfo.setClassName(getA11yClassName());
        accessibilityNodeInfo.setCheckable(b());
        accessibilityNodeInfo.setChecked(isChecked());
        accessibilityNodeInfo.setClickable(isClickable());
    }

    @Override // androidx.appcompat.widget.AppCompatButton, android.widget.TextView, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        j42 j42Var;
        super.onLayout(z, i, i2, i3, i4);
        if (Build.VERSION.SDK_INT != 21 || (j42Var = this.g0) == null) {
            return;
        }
        j42Var.H(i4 - i2, i3 - i);
    }

    @Override // android.widget.TextView, android.view.View
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.a());
        setChecked(savedState.g0);
    }

    @Override // android.widget.TextView, android.view.View
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.g0 = this.q0;
        return savedState;
    }

    @Override // android.view.View
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        k(i, i2);
    }

    @Override // androidx.appcompat.widget.AppCompatButton, android.widget.TextView
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        super.onTextChanged(charSequence, i, i2, i3);
        k(getMeasuredWidth(), getMeasuredHeight());
    }

    @Override // android.view.View
    public boolean performClick() {
        toggle();
        return super.performClick();
    }

    @Override // android.view.View
    public void setBackground(Drawable drawable) {
        setBackgroundDrawable(drawable);
    }

    @Override // android.view.View
    public void setBackgroundColor(int i) {
        if (g()) {
            this.g0.r(i);
        } else {
            super.setBackgroundColor(i);
        }
    }

    @Override // androidx.appcompat.widget.AppCompatButton, android.view.View
    public void setBackgroundDrawable(Drawable drawable) {
        if (g()) {
            if (drawable != getBackground()) {
                this.g0.s();
                super.setBackgroundDrawable(drawable);
                return;
            }
            getBackground().setState(drawable.getState());
            return;
        }
        super.setBackgroundDrawable(drawable);
    }

    @Override // androidx.appcompat.widget.AppCompatButton, android.view.View
    public void setBackgroundResource(int i) {
        setBackgroundDrawable(i != 0 ? mf.d(getContext(), i) : null);
    }

    @Override // android.view.View
    public void setBackgroundTintList(ColorStateList colorStateList) {
        setSupportBackgroundTintList(colorStateList);
    }

    @Override // android.view.View
    public void setBackgroundTintMode(PorterDuff.Mode mode) {
        setSupportBackgroundTintMode(mode);
    }

    public void setCheckable(boolean z) {
        if (g()) {
            this.g0.t(z);
        }
    }

    @Override // android.widget.Checkable
    public void setChecked(boolean z) {
        if (b() && isEnabled() && this.q0 != z) {
            this.q0 = z;
            refreshDrawableState();
            if (this.r0) {
                return;
            }
            this.r0 = true;
            Iterator<a> it = this.h0.iterator();
            while (it.hasNext()) {
                it.next().a(this, this.q0);
            }
            this.r0 = false;
        }
    }

    public void setCornerRadius(int i) {
        if (g()) {
            this.g0.u(i);
        }
    }

    public void setCornerRadiusResource(int i) {
        if (g()) {
            setCornerRadius(getResources().getDimensionPixelSize(i));
        }
    }

    @Override // android.view.View
    public void setElevation(float f) {
        super.setElevation(f);
        if (g()) {
            this.g0.f().Z(f);
        }
    }

    public void setIcon(Drawable drawable) {
        if (this.l0 != drawable) {
            this.l0 = drawable;
            j(true);
            k(getMeasuredWidth(), getMeasuredHeight());
        }
    }

    public void setIconGravity(int i) {
        if (this.s0 != i) {
            this.s0 = i;
            k(getMeasuredWidth(), getMeasuredHeight());
        }
    }

    public void setIconPadding(int i) {
        if (this.p0 != i) {
            this.p0 = i;
            setCompoundDrawablePadding(i);
        }
    }

    public void setIconResource(int i) {
        setIcon(i != 0 ? mf.d(getContext(), i) : null);
    }

    public void setIconSize(int i) {
        if (i >= 0) {
            if (this.m0 != i) {
                this.m0 = i;
                j(true);
                return;
            }
            return;
        }
        throw new IllegalArgumentException("iconSize cannot be less than 0");
    }

    public void setIconTint(ColorStateList colorStateList) {
        if (this.k0 != colorStateList) {
            this.k0 = colorStateList;
            j(false);
        }
    }

    public void setIconTintMode(PorterDuff.Mode mode) {
        if (this.j0 != mode) {
            this.j0 = mode;
            j(false);
        }
    }

    public void setIconTintResource(int i) {
        setIconTint(mf.c(getContext(), i));
    }

    public void setInsetBottom(int i) {
        this.g0.v(i);
    }

    public void setInsetTop(int i) {
        this.g0.w(i);
    }

    public void setInternalBackground(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
    }

    public void setOnPressedChangeListenerInternal(b bVar) {
        this.i0 = bVar;
    }

    @Override // android.view.View
    public void setPressed(boolean z) {
        b bVar = this.i0;
        if (bVar != null) {
            bVar.a(this, z);
        }
        super.setPressed(z);
    }

    public void setRippleColor(ColorStateList colorStateList) {
        if (g()) {
            this.g0.x(colorStateList);
        }
    }

    public void setRippleColorResource(int i) {
        if (g()) {
            setRippleColor(mf.c(getContext(), i));
        }
    }

    @Override // defpackage.sn3
    public void setShapeAppearanceModel(pn3 pn3Var) {
        if (g()) {
            this.g0.y(pn3Var);
            return;
        }
        throw new IllegalStateException("Attempted to set ShapeAppearanceModel on a MaterialButton which has an overwritten background.");
    }

    public void setShouldDrawSurfaceColorStroke(boolean z) {
        if (g()) {
            this.g0.z(z);
        }
    }

    public void setStrokeColor(ColorStateList colorStateList) {
        if (g()) {
            this.g0.A(colorStateList);
        }
    }

    public void setStrokeColorResource(int i) {
        if (g()) {
            setStrokeColor(mf.c(getContext(), i));
        }
    }

    public void setStrokeWidth(int i) {
        if (g()) {
            this.g0.B(i);
        }
    }

    public void setStrokeWidthResource(int i) {
        if (g()) {
            setStrokeWidth(getResources().getDimensionPixelSize(i));
        }
    }

    @Override // androidx.appcompat.widget.AppCompatButton, defpackage.m64
    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        if (g()) {
            this.g0.C(colorStateList);
        } else {
            super.setSupportBackgroundTintList(colorStateList);
        }
    }

    @Override // androidx.appcompat.widget.AppCompatButton, defpackage.m64
    public void setSupportBackgroundTintMode(PorterDuff.Mode mode) {
        if (g()) {
            this.g0.D(mode);
        } else {
            super.setSupportBackgroundTintMode(mode);
        }
    }

    @Override // android.widget.Checkable
    public void toggle() {
        setChecked(!this.q0);
    }

    public MaterialButton(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, gy2.materialButtonStyle);
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public MaterialButton(android.content.Context r9, android.util.AttributeSet r10, int r11) {
        /*
            r8 = this;
            int r6 = com.google.android.material.button.MaterialButton.v0
            android.content.Context r9 = defpackage.r42.c(r9, r10, r11, r6)
            r8.<init>(r9, r10, r11)
            java.util.LinkedHashSet r9 = new java.util.LinkedHashSet
            r9.<init>()
            r8.h0 = r9
            r9 = 0
            r8.q0 = r9
            r8.r0 = r9
            android.content.Context r7 = r8.getContext()
            int[] r2 = defpackage.o23.MaterialButton
            int[] r5 = new int[r9]
            r0 = r7
            r1 = r10
            r3 = r11
            r4 = r6
            android.content.res.TypedArray r0 = defpackage.a54.h(r0, r1, r2, r3, r4, r5)
            int r1 = defpackage.o23.MaterialButton_iconPadding
            int r1 = r0.getDimensionPixelSize(r1, r9)
            r8.p0 = r1
            int r1 = defpackage.o23.MaterialButton_iconTintMode
            r2 = -1
            int r1 = r0.getInt(r1, r2)
            android.graphics.PorterDuff$Mode r2 = android.graphics.PorterDuff.Mode.SRC_IN
            android.graphics.PorterDuff$Mode r1 = defpackage.mk4.i(r1, r2)
            r8.j0 = r1
            android.content.Context r1 = r8.getContext()
            int r2 = defpackage.o23.MaterialButton_iconTint
            android.content.res.ColorStateList r1 = defpackage.n42.b(r1, r0, r2)
            r8.k0 = r1
            android.content.Context r1 = r8.getContext()
            int r2 = defpackage.o23.MaterialButton_icon
            android.graphics.drawable.Drawable r1 = defpackage.n42.d(r1, r0, r2)
            r8.l0 = r1
            int r1 = defpackage.o23.MaterialButton_iconGravity
            r2 = 1
            int r1 = r0.getInteger(r1, r2)
            r8.s0 = r1
            int r1 = defpackage.o23.MaterialButton_iconSize
            int r1 = r0.getDimensionPixelSize(r1, r9)
            r8.m0 = r1
            pn3$b r10 = defpackage.pn3.e(r7, r10, r11, r6)
            pn3 r10 = r10.m()
            j42 r11 = new j42
            r11.<init>(r8, r10)
            r8.g0 = r11
            r11.q(r0)
            r0.recycle()
            int r10 = r8.p0
            r8.setCompoundDrawablePadding(r10)
            android.graphics.drawable.Drawable r10 = r8.l0
            if (r10 == 0) goto L84
            r9 = r2
        L84:
            r8.j(r9)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.button.MaterialButton.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }
}
