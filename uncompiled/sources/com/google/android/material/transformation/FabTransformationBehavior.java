package com.google.android.material.transformation;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Pair;
import android.util.Property;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.material.circularreveal.c;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import java.util.ArrayList;
import java.util.List;

@Deprecated
/* loaded from: classes2.dex */
public abstract class FabTransformationBehavior extends ExpandableTransformationBehavior {
    public final Rect c;
    public final RectF d;
    public final RectF e;
    public final int[] f;
    public float g;
    public float h;

    /* loaded from: classes2.dex */
    public class a extends AnimatorListenerAdapter {
        public final /* synthetic */ boolean a;
        public final /* synthetic */ View f0;
        public final /* synthetic */ View g0;

        public a(FabTransformationBehavior fabTransformationBehavior, boolean z, View view, View view2) {
            this.a = z;
            this.f0 = view;
            this.g0 = view2;
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            if (this.a) {
                return;
            }
            this.f0.setVisibility(4);
            this.g0.setAlpha(1.0f);
            this.g0.setVisibility(0);
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationStart(Animator animator) {
            if (this.a) {
                this.f0.setVisibility(0);
                this.g0.setAlpha(Utils.FLOAT_EPSILON);
                this.g0.setVisibility(4);
            }
        }
    }

    /* loaded from: classes2.dex */
    public class b implements ValueAnimator.AnimatorUpdateListener {
        public final /* synthetic */ View a;

        public b(FabTransformationBehavior fabTransformationBehavior, View view) {
            this.a = view;
        }

        @Override // android.animation.ValueAnimator.AnimatorUpdateListener
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            this.a.invalidate();
        }
    }

    /* loaded from: classes2.dex */
    public class c extends AnimatorListenerAdapter {
        public final /* synthetic */ com.google.android.material.circularreveal.c a;
        public final /* synthetic */ Drawable f0;

        public c(FabTransformationBehavior fabTransformationBehavior, com.google.android.material.circularreveal.c cVar, Drawable drawable) {
            this.a = cVar;
            this.f0 = drawable;
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            this.a.setCircularRevealOverlayDrawable(null);
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationStart(Animator animator) {
            this.a.setCircularRevealOverlayDrawable(this.f0);
        }
    }

    /* loaded from: classes2.dex */
    public class d extends AnimatorListenerAdapter {
        public final /* synthetic */ com.google.android.material.circularreveal.c a;

        public d(FabTransformationBehavior fabTransformationBehavior, com.google.android.material.circularreveal.c cVar) {
            this.a = cVar;
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            c.e revealInfo = this.a.getRevealInfo();
            revealInfo.c = Float.MAX_VALUE;
            this.a.setRevealInfo(revealInfo);
        }
    }

    /* loaded from: classes2.dex */
    public static class e {
        public z92 a;
        public pt2 b;
    }

    public FabTransformationBehavior() {
        this.c = new Rect();
        this.d = new RectF();
        this.e = new RectF();
        this.f = new int[2];
    }

    public abstract e A(Context context, boolean z);

    public final ViewGroup B(View view) {
        if (view instanceof ViewGroup) {
            return (ViewGroup) view;
        }
        return null;
    }

    @Override // com.google.android.material.transformation.ExpandableTransformationBehavior
    public AnimatorSet f(View view, View view2, boolean z, boolean z2) {
        e A = A(view2.getContext(), z);
        if (z) {
            this.g = view.getTranslationX();
            this.h = view.getTranslationY();
        }
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        if (Build.VERSION.SDK_INT >= 21) {
            t(view, view2, z, z2, A, arrayList, arrayList2);
        }
        RectF rectF = this.d;
        y(view, view2, z, z2, A, arrayList, arrayList2, rectF);
        float width = rectF.width();
        float height = rectF.height();
        s(view, view2, z, A, arrayList);
        v(view, view2, z, z2, A, arrayList, arrayList2);
        u(view, view2, z, z2, A, width, height, arrayList, arrayList2);
        r(view, view2, z, z2, A, arrayList, arrayList2);
        q(view, view2, z, z2, A, arrayList, arrayList2);
        AnimatorSet animatorSet = new AnimatorSet();
        re.a(animatorSet, arrayList);
        animatorSet.addListener(new a(this, z, view2, view));
        int size = arrayList2.size();
        for (int i = 0; i < size; i++) {
            animatorSet.addListener(arrayList2.get(i));
        }
        return animatorSet;
    }

    public final ViewGroup g(View view) {
        View findViewById = view.findViewById(b03.mtrl_child_content_container);
        if (findViewById != null) {
            return B(findViewById);
        }
        if (!(view instanceof TransformationChildLayout) && !(view instanceof TransformationChildCard)) {
            return B(view);
        }
        return B(((ViewGroup) view).getChildAt(0));
    }

    public final void h(View view, e eVar, aa2 aa2Var, aa2 aa2Var2, float f, float f2, float f3, float f4, RectF rectF) {
        float o = o(eVar, aa2Var, f, f3);
        float o2 = o(eVar, aa2Var2, f2, f4);
        Rect rect = this.c;
        view.getWindowVisibleDisplayFrame(rect);
        RectF rectF2 = this.d;
        rectF2.set(rect);
        RectF rectF3 = this.e;
        p(view, rectF3);
        rectF3.offset(o, o2);
        rectF3.intersect(rectF2);
        rectF.set(rectF3);
    }

    public final void i(View view, RectF rectF) {
        p(view, rectF);
        rectF.offset(this.g, this.h);
    }

    public final Pair<aa2, aa2> j(float f, float f2, boolean z, e eVar) {
        aa2 h;
        aa2 h2;
        int i;
        if (f == Utils.FLOAT_EPSILON || f2 == Utils.FLOAT_EPSILON) {
            h = eVar.a.h("translationXLinear");
            h2 = eVar.a.h("translationYLinear");
        } else if ((z && f2 < Utils.FLOAT_EPSILON) || (!z && i > 0)) {
            h = eVar.a.h("translationXCurveUpwards");
            h2 = eVar.a.h("translationYCurveUpwards");
        } else {
            h = eVar.a.h("translationXCurveDownwards");
            h2 = eVar.a.h("translationYCurveDownwards");
        }
        return new Pair<>(h, h2);
    }

    public final float k(View view, View view2, pt2 pt2Var) {
        RectF rectF = this.d;
        RectF rectF2 = this.e;
        i(view, rectF);
        p(view2, rectF2);
        rectF2.offset(-m(view, view2, pt2Var), Utils.FLOAT_EPSILON);
        return rectF.centerX() - rectF2.left;
    }

    public final float l(View view, View view2, pt2 pt2Var) {
        RectF rectF = this.d;
        RectF rectF2 = this.e;
        i(view, rectF);
        p(view2, rectF2);
        rectF2.offset(Utils.FLOAT_EPSILON, -n(view, view2, pt2Var));
        return rectF.centerY() - rectF2.top;
    }

    @Override // androidx.coordinatorlayout.widget.CoordinatorLayout.Behavior
    public boolean layoutDependsOn(CoordinatorLayout coordinatorLayout, View view, View view2) {
        if (view.getVisibility() != 8) {
            if (view2 instanceof FloatingActionButton) {
                int expandedComponentIdHint = ((FloatingActionButton) view2).getExpandedComponentIdHint();
                return expandedComponentIdHint == 0 || expandedComponentIdHint == view.getId();
            }
            return false;
        }
        throw new IllegalStateException("This behavior cannot be attached to a GONE view. Set the view to INVISIBLE instead.");
    }

    public final float m(View view, View view2, pt2 pt2Var) {
        float centerX;
        float centerX2;
        float f;
        RectF rectF = this.d;
        RectF rectF2 = this.e;
        i(view, rectF);
        p(view2, rectF2);
        int i = pt2Var.a & 7;
        if (i == 1) {
            centerX = rectF2.centerX();
            centerX2 = rectF.centerX();
        } else if (i == 3) {
            centerX = rectF2.left;
            centerX2 = rectF.left;
        } else if (i == 5) {
            centerX = rectF2.right;
            centerX2 = rectF.right;
        } else {
            f = Utils.FLOAT_EPSILON;
            return f + pt2Var.b;
        }
        f = centerX - centerX2;
        return f + pt2Var.b;
    }

    public final float n(View view, View view2, pt2 pt2Var) {
        float centerY;
        float centerY2;
        float f;
        RectF rectF = this.d;
        RectF rectF2 = this.e;
        i(view, rectF);
        p(view2, rectF2);
        int i = pt2Var.a & 112;
        if (i == 16) {
            centerY = rectF2.centerY();
            centerY2 = rectF.centerY();
        } else if (i == 48) {
            centerY = rectF2.top;
            centerY2 = rectF.top;
        } else if (i == 80) {
            centerY = rectF2.bottom;
            centerY2 = rectF.bottom;
        } else {
            f = Utils.FLOAT_EPSILON;
            return f + pt2Var.c;
        }
        f = centerY - centerY2;
        return f + pt2Var.c;
    }

    public final float o(e eVar, aa2 aa2Var, float f, float f2) {
        long c2 = aa2Var.c();
        long d2 = aa2Var.d();
        aa2 h = eVar.a.h("expansion");
        return ne.a(f, f2, aa2Var.e().getInterpolation(((float) (((h.c() + h.d()) + 17) - c2)) / ((float) d2)));
    }

    @Override // androidx.coordinatorlayout.widget.CoordinatorLayout.Behavior
    public void onAttachedToLayoutParams(CoordinatorLayout.e eVar) {
        if (eVar.h == 0) {
            eVar.h = 80;
        }
    }

    public final void p(View view, RectF rectF) {
        rectF.set(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, view.getWidth(), view.getHeight());
        int[] iArr = this.f;
        view.getLocationInWindow(iArr);
        rectF.offsetTo(iArr[0], iArr[1]);
        rectF.offset((int) (-view.getTranslationX()), (int) (-view.getTranslationY()));
    }

    public final void q(View view, View view2, boolean z, boolean z2, e eVar, List<Animator> list, List<Animator.AnimatorListener> list2) {
        ViewGroup g;
        ObjectAnimator ofFloat;
        if (view2 instanceof ViewGroup) {
            if (((view2 instanceof com.google.android.material.circularreveal.c) && com.google.android.material.circularreveal.b.j == 0) || (g = g(view2)) == null) {
                return;
            }
            if (z) {
                if (!z2) {
                    iy.a.set(g, Float.valueOf((float) Utils.FLOAT_EPSILON));
                }
                ofFloat = ObjectAnimator.ofFloat(g, iy.a, 1.0f);
            } else {
                ofFloat = ObjectAnimator.ofFloat(g, iy.a, Utils.FLOAT_EPSILON);
            }
            eVar.a.h("contentFade").a(ofFloat);
            list.add(ofFloat);
        }
    }

    public final void r(View view, View view2, boolean z, boolean z2, e eVar, List<Animator> list, List<Animator.AnimatorListener> list2) {
        ObjectAnimator ofInt;
        if (view2 instanceof com.google.android.material.circularreveal.c) {
            com.google.android.material.circularreveal.c cVar = (com.google.android.material.circularreveal.c) view2;
            int z3 = z(view);
            int i = 16777215 & z3;
            if (z) {
                if (!z2) {
                    cVar.setCircularRevealScrimColor(z3);
                }
                ofInt = ObjectAnimator.ofInt(cVar, c.d.a, i);
            } else {
                ofInt = ObjectAnimator.ofInt(cVar, c.d.a, z3);
            }
            ofInt.setEvaluator(ih.b());
            eVar.a.h("color").a(ofInt);
            list.add(ofInt);
        }
    }

    public final void s(View view, View view2, boolean z, e eVar, List<Animator> list) {
        float m = m(view, view2, eVar.b);
        float n = n(view, view2, eVar.b);
        Pair<aa2, aa2> j = j(m, n, z, eVar);
        aa2 aa2Var = (aa2) j.first;
        aa2 aa2Var2 = (aa2) j.second;
        Property property = View.TRANSLATION_X;
        float[] fArr = new float[1];
        if (!z) {
            m = this.g;
        }
        fArr[0] = m;
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(view, property, fArr);
        Property property2 = View.TRANSLATION_Y;
        float[] fArr2 = new float[1];
        if (!z) {
            n = this.h;
        }
        fArr2[0] = n;
        ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(view, property2, fArr2);
        aa2Var.a(ofFloat);
        aa2Var2.a(ofFloat2);
        list.add(ofFloat);
        list.add(ofFloat2);
    }

    @TargetApi(21)
    public final void t(View view, View view2, boolean z, boolean z2, e eVar, List<Animator> list, List<Animator.AnimatorListener> list2) {
        ObjectAnimator ofFloat;
        float y = ei4.y(view2) - ei4.y(view);
        if (z) {
            if (!z2) {
                view2.setTranslationZ(-y);
            }
            ofFloat = ObjectAnimator.ofFloat(view2, View.TRANSLATION_Z, Utils.FLOAT_EPSILON);
        } else {
            ofFloat = ObjectAnimator.ofFloat(view2, View.TRANSLATION_Z, -y);
        }
        eVar.a.h("elevation").a(ofFloat);
        list.add(ofFloat);
    }

    public final void u(View view, View view2, boolean z, boolean z2, e eVar, float f, float f2, List<Animator> list, List<Animator.AnimatorListener> list2) {
        Animator animator;
        if (view2 instanceof com.google.android.material.circularreveal.c) {
            com.google.android.material.circularreveal.c cVar = (com.google.android.material.circularreveal.c) view2;
            float k = k(view, view2, eVar.b);
            float l = l(view, view2, eVar.b);
            ((FloatingActionButton) view).i(this.c);
            float width = this.c.width() / 2.0f;
            aa2 h = eVar.a.h("expansion");
            if (z) {
                if (!z2) {
                    cVar.setRevealInfo(new c.e(k, l, width));
                }
                if (z2) {
                    width = cVar.getRevealInfo().c;
                }
                animator = com.google.android.material.circularreveal.a.a(cVar, k, l, w42.b(k, l, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, f, f2));
                animator.addListener(new d(this, cVar));
                x(view2, h.c(), (int) k, (int) l, width, list);
            } else {
                float f3 = cVar.getRevealInfo().c;
                Animator a2 = com.google.android.material.circularreveal.a.a(cVar, k, l, width);
                int i = (int) k;
                int i2 = (int) l;
                x(view2, h.c(), i, i2, f3, list);
                w(view2, h.c(), h.d(), eVar.a.i(), i, i2, width, list);
                animator = a2;
            }
            h.a(animator);
            list.add(animator);
            list2.add(com.google.android.material.circularreveal.a.b(cVar));
        }
    }

    public final void v(View view, View view2, boolean z, boolean z2, e eVar, List<Animator> list, List<Animator.AnimatorListener> list2) {
        ObjectAnimator ofInt;
        if ((view2 instanceof com.google.android.material.circularreveal.c) && (view instanceof ImageView)) {
            com.google.android.material.circularreveal.c cVar = (com.google.android.material.circularreveal.c) view2;
            Drawable drawable = ((ImageView) view).getDrawable();
            if (drawable == null) {
                return;
            }
            drawable.mutate();
            if (z) {
                if (!z2) {
                    drawable.setAlpha(255);
                }
                ofInt = ObjectAnimator.ofInt(drawable, qq0.b, 0);
            } else {
                ofInt = ObjectAnimator.ofInt(drawable, qq0.b, 255);
            }
            ofInt.addUpdateListener(new b(this, view2));
            eVar.a.h("iconFade").a(ofInt);
            list.add(ofInt);
            list2.add(new c(this, cVar, drawable));
        }
    }

    public final void w(View view, long j, long j2, long j3, int i, int i2, float f, List<Animator> list) {
        if (Build.VERSION.SDK_INT >= 21) {
            long j4 = j + j2;
            if (j4 < j3) {
                Animator createCircularReveal = ViewAnimationUtils.createCircularReveal(view, i, i2, f, f);
                createCircularReveal.setStartDelay(j4);
                createCircularReveal.setDuration(j3 - j4);
                list.add(createCircularReveal);
            }
        }
    }

    public final void x(View view, long j, int i, int i2, float f, List<Animator> list) {
        if (Build.VERSION.SDK_INT < 21 || j <= 0) {
            return;
        }
        Animator createCircularReveal = ViewAnimationUtils.createCircularReveal(view, i, i2, f, f);
        createCircularReveal.setStartDelay(0L);
        createCircularReveal.setDuration(j);
        list.add(createCircularReveal);
    }

    public final void y(View view, View view2, boolean z, boolean z2, e eVar, List<Animator> list, List<Animator.AnimatorListener> list2, RectF rectF) {
        ObjectAnimator ofFloat;
        ObjectAnimator ofFloat2;
        float m = m(view, view2, eVar.b);
        float n = n(view, view2, eVar.b);
        Pair<aa2, aa2> j = j(m, n, z, eVar);
        aa2 aa2Var = (aa2) j.first;
        aa2 aa2Var2 = (aa2) j.second;
        if (z) {
            if (!z2) {
                view2.setTranslationX(-m);
                view2.setTranslationY(-n);
            }
            ofFloat = ObjectAnimator.ofFloat(view2, View.TRANSLATION_X, Utils.FLOAT_EPSILON);
            ofFloat2 = ObjectAnimator.ofFloat(view2, View.TRANSLATION_Y, Utils.FLOAT_EPSILON);
            h(view2, eVar, aa2Var, aa2Var2, -m, -n, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, rectF);
        } else {
            ofFloat = ObjectAnimator.ofFloat(view2, View.TRANSLATION_X, -m);
            ofFloat2 = ObjectAnimator.ofFloat(view2, View.TRANSLATION_Y, -n);
        }
        aa2Var.a(ofFloat);
        aa2Var2.a(ofFloat2);
        list.add(ofFloat);
        list.add(ofFloat2);
    }

    public final int z(View view) {
        ColorStateList u = ei4.u(view);
        if (u != null) {
            return u.getColorForState(view.getDrawableState(), u.getDefaultColor());
        }
        return 0;
    }

    public FabTransformationBehavior(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.c = new Rect();
        this.d = new RectF();
        this.e = new RectF();
        this.f = new int[2];
    }
}
