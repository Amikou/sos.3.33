package com.google.android.material.textfield;

import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStructure;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.customview.view.AbsSavedState;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.material.internal.CheckableImageButton;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashSet;

/* loaded from: classes2.dex */
public class TextInputLayout extends LinearLayout {
    public static final int N1 = y13.Widget_Design_TextInputLayout;
    public CharSequence A0;
    public ColorStateList A1;
    public final TextView B0;
    public int B1;
    public CharSequence C0;
    public int C1;
    public final TextView D0;
    public int D1;
    public boolean E0;
    public int E1;
    public CharSequence F0;
    public int F1;
    public boolean G0;
    public boolean G1;
    public o42 H0;
    public final com.google.android.material.internal.a H1;
    public o42 I0;
    public boolean I1;
    public pn3 J0;
    public boolean J1;
    public final int K0;
    public ValueAnimator K1;
    public int L0;
    public boolean L1;
    public int M0;
    public boolean M1;
    public int N0;
    public int O0;
    public int P0;
    public int Q0;
    public int R0;
    public int S0;
    public final Rect T0;
    public final Rect U0;
    public final RectF V0;
    public Typeface W0;
    public final CheckableImageButton X0;
    public ColorStateList Y0;
    public boolean Z0;
    public final FrameLayout a;
    public PorterDuff.Mode a1;
    public boolean b1;
    public Drawable c1;
    public int d1;
    public View.OnLongClickListener e1;
    public final LinearLayout f0;
    public final LinkedHashSet<f> f1;
    public final LinearLayout g0;
    public int g1;
    public final FrameLayout h0;
    public final SparseArray<kv0> h1;
    public EditText i0;
    public final CheckableImageButton i1;
    public CharSequence j0;
    public final LinkedHashSet<g> j1;
    public int k0;
    public ColorStateList k1;
    public int l0;
    public boolean l1;
    public final nq1 m0;
    public PorterDuff.Mode m1;
    public boolean n0;
    public boolean n1;
    public int o0;
    public Drawable o1;
    public boolean p0;
    public int p1;
    public TextView q0;
    public Drawable q1;
    public int r0;
    public View.OnLongClickListener r1;
    public int s0;
    public View.OnLongClickListener s1;
    public CharSequence t0;
    public final CheckableImageButton t1;
    public boolean u0;
    public ColorStateList u1;
    public TextView v0;
    public ColorStateList v1;
    public ColorStateList w0;
    public ColorStateList w1;
    public int x0;
    public int x1;
    public ColorStateList y0;
    public int y1;
    public ColorStateList z0;
    public int z1;

    /* loaded from: classes2.dex */
    public static class SavedState extends AbsSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new a();
        public CharSequence g0;
        public boolean h0;
        public CharSequence i0;
        public CharSequence j0;
        public CharSequence k0;

        /* loaded from: classes2.dex */
        public static class a implements Parcelable.ClassLoaderCreator<SavedState> {
            @Override // android.os.Parcelable.Creator
            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel, null);
            }

            @Override // android.os.Parcelable.ClassLoaderCreator
            /* renamed from: b */
            public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new SavedState(parcel, classLoader);
            }

            @Override // android.os.Parcelable.Creator
            /* renamed from: c */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public String toString() {
            return "TextInputLayout.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " error=" + ((Object) this.g0) + " hint=" + ((Object) this.i0) + " helperText=" + ((Object) this.j0) + " placeholderText=" + ((Object) this.k0) + "}";
        }

        @Override // androidx.customview.view.AbsSavedState, android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            TextUtils.writeToParcel(this.g0, parcel, i);
            parcel.writeInt(this.h0 ? 1 : 0);
            TextUtils.writeToParcel(this.i0, parcel, i);
            TextUtils.writeToParcel(this.j0, parcel, i);
            TextUtils.writeToParcel(this.k0, parcel, i);
        }

        public SavedState(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            this.g0 = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
            this.h0 = parcel.readInt() == 1;
            this.i0 = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
            this.j0 = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
            this.k0 = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        }
    }

    /* loaded from: classes2.dex */
    public class a implements TextWatcher {
        public a() {
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            TextInputLayout textInputLayout = TextInputLayout.this;
            textInputLayout.v0(!textInputLayout.M1);
            TextInputLayout textInputLayout2 = TextInputLayout.this;
            if (textInputLayout2.n0) {
                textInputLayout2.n0(editable.length());
            }
            if (TextInputLayout.this.u0) {
                TextInputLayout.this.z0(editable.length());
            }
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    /* loaded from: classes2.dex */
    public class b implements Runnable {
        public b() {
        }

        @Override // java.lang.Runnable
        public void run() {
            TextInputLayout.this.i1.performClick();
            TextInputLayout.this.i1.jumpDrawablesToCurrentState();
        }
    }

    /* loaded from: classes2.dex */
    public class c implements Runnable {
        public c() {
        }

        @Override // java.lang.Runnable
        public void run() {
            TextInputLayout.this.i0.requestLayout();
        }
    }

    /* loaded from: classes2.dex */
    public class d implements ValueAnimator.AnimatorUpdateListener {
        public d() {
        }

        @Override // android.animation.ValueAnimator.AnimatorUpdateListener
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            TextInputLayout.this.H1.p0(((Float) valueAnimator.getAnimatedValue()).floatValue());
        }
    }

    /* loaded from: classes2.dex */
    public static class e extends z5 {
        public final TextInputLayout d;

        public e(TextInputLayout textInputLayout) {
            this.d = textInputLayout;
        }

        @Override // defpackage.z5
        public void g(View view, b6 b6Var) {
            super.g(view, b6Var);
            EditText editText = this.d.getEditText();
            CharSequence text = editText != null ? editText.getText() : null;
            CharSequence hint = this.d.getHint();
            CharSequence error = this.d.getError();
            CharSequence placeholderText = this.d.getPlaceholderText();
            int counterMaxLength = this.d.getCounterMaxLength();
            CharSequence counterOverflowDescription = this.d.getCounterOverflowDescription();
            boolean z = !TextUtils.isEmpty(text);
            boolean z2 = !TextUtils.isEmpty(hint);
            boolean z3 = !this.d.N();
            boolean z4 = !TextUtils.isEmpty(error);
            boolean z5 = z4 || !TextUtils.isEmpty(counterOverflowDescription);
            String charSequence = z2 ? hint.toString() : "";
            if (z) {
                b6Var.E0(text);
            } else if (!TextUtils.isEmpty(charSequence)) {
                b6Var.E0(charSequence);
                if (z3 && placeholderText != null) {
                    b6Var.E0(charSequence + ", " + ((Object) placeholderText));
                }
            } else if (placeholderText != null) {
                b6Var.E0(placeholderText);
            }
            if (!TextUtils.isEmpty(charSequence)) {
                if (Build.VERSION.SDK_INT >= 26) {
                    b6Var.n0(charSequence);
                } else {
                    if (z) {
                        charSequence = ((Object) text) + ", " + charSequence;
                    }
                    b6Var.E0(charSequence);
                }
                b6Var.A0(!z);
            }
            b6Var.p0((text == null || text.length() != counterMaxLength) ? -1 : -1);
            if (z5) {
                if (!z4) {
                    error = counterOverflowDescription;
                }
                b6Var.j0(error);
            }
            if (Build.VERSION.SDK_INT < 17 || editText == null) {
                return;
            }
            editText.setLabelFor(b03.textinput_helper_text);
        }
    }

    /* loaded from: classes2.dex */
    public interface f {
        void a(TextInputLayout textInputLayout);
    }

    /* loaded from: classes2.dex */
    public interface g {
        void a(TextInputLayout textInputLayout, int i);
    }

    public TextInputLayout(Context context) {
        this(context, null);
    }

    public static void U(ViewGroup viewGroup, boolean z) {
        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = viewGroup.getChildAt(i);
            childAt.setEnabled(z);
            if (childAt instanceof ViewGroup) {
                U((ViewGroup) childAt, z);
            }
        }
    }

    public static void b0(CheckableImageButton checkableImageButton, View.OnLongClickListener onLongClickListener) {
        boolean R = ei4.R(checkableImageButton);
        boolean z = false;
        boolean z2 = onLongClickListener != null;
        if (R || z2) {
            z = true;
        }
        checkableImageButton.setFocusable(z);
        checkableImageButton.setClickable(R);
        checkableImageButton.setPressable(R);
        checkableImageButton.setLongClickable(z2);
        ei4.D0(checkableImageButton, z ? 1 : 2);
    }

    public static void c0(CheckableImageButton checkableImageButton, View.OnClickListener onClickListener, View.OnLongClickListener onLongClickListener) {
        checkableImageButton.setOnClickListener(onClickListener);
        b0(checkableImageButton, onLongClickListener);
    }

    public static void d0(CheckableImageButton checkableImageButton, View.OnLongClickListener onLongClickListener) {
        checkableImageButton.setOnLongClickListener(onLongClickListener);
        b0(checkableImageButton, onLongClickListener);
    }

    private kv0 getEndIconDelegate() {
        kv0 kv0Var = this.h1.get(this.g1);
        return kv0Var != null ? kv0Var : this.h1.get(0);
    }

    private CheckableImageButton getEndIconToUpdateDummyDrawable() {
        if (this.t1.getVisibility() == 0) {
            return this.t1;
        }
        if (I() && K()) {
            return this.i1;
        }
        return null;
    }

    public static void o0(Context context, TextView textView, int i, int i2, boolean z) {
        textView.setContentDescription(context.getString(z ? r13.character_counter_overflowed_content_description : r13.character_counter_content_description, Integer.valueOf(i), Integer.valueOf(i2)));
    }

    private void setEditText(EditText editText) {
        if (this.i0 == null) {
            if (this.g1 != 3) {
                boolean z = editText instanceof TextInputEditText;
            }
            this.i0 = editText;
            setMinWidth(this.k0);
            setMaxWidth(this.l0);
            S();
            setTextInputAccessibilityDelegate(new e(this));
            this.H1.C0(this.i0.getTypeface());
            this.H1.m0(this.i0.getTextSize());
            int gravity = this.i0.getGravity();
            this.H1.c0((gravity & (-113)) | 48);
            this.H1.l0(gravity);
            this.i0.addTextChangedListener(new a());
            if (this.v1 == null) {
                this.v1 = this.i0.getHintTextColors();
            }
            if (this.E0) {
                if (TextUtils.isEmpty(this.F0)) {
                    CharSequence hint = this.i0.getHint();
                    this.j0 = hint;
                    setHint(hint);
                    this.i0.setHint((CharSequence) null);
                }
                this.G0 = true;
            }
            if (this.q0 != null) {
                n0(this.i0.getText().length());
            }
            s0();
            this.m0.e();
            this.f0.bringToFront();
            this.g0.bringToFront();
            this.h0.bringToFront();
            this.t1.bringToFront();
            B();
            A0();
            D0();
            if (!isEnabled()) {
                editText.setEnabled(false);
            }
            w0(false, true);
            return;
        }
        throw new IllegalArgumentException("We already have an EditText, can only have one");
    }

    private void setErrorIconVisible(boolean z) {
        this.t1.setVisibility(z ? 0 : 8);
        this.h0.setVisibility(z ? 8 : 0);
        D0();
        if (I()) {
            return;
        }
        r0();
    }

    private void setHintInternal(CharSequence charSequence) {
        if (TextUtils.equals(charSequence, this.F0)) {
            return;
        }
        this.F0 = charSequence;
        this.H1.A0(charSequence);
        if (this.G1) {
            return;
        }
        T();
    }

    private void setPlaceholderTextEnabled(boolean z) {
        if (this.u0 == z) {
            return;
        }
        if (z) {
            AppCompatTextView appCompatTextView = new AppCompatTextView(getContext());
            this.v0 = appCompatTextView;
            appCompatTextView.setId(b03.textinput_placeholder);
            ei4.v0(this.v0, 1);
            setPlaceholderTextAppearance(this.x0);
            setPlaceholderTextColor(this.w0);
            g();
        } else {
            Z();
            this.v0 = null;
        }
        this.u0 = z;
    }

    public final boolean A() {
        return this.E0 && !TextUtils.isEmpty(this.F0) && (this.H0 instanceof bd0);
    }

    public final void A0() {
        if (this.i0 == null) {
            return;
        }
        ei4.H0(this.B0, Q() ? 0 : ei4.J(this.i0), this.i0.getCompoundPaddingTop(), getContext().getResources().getDimensionPixelSize(jz2.material_input_text_to_prefix_suffix_padding), this.i0.getCompoundPaddingBottom());
    }

    public final void B() {
        Iterator<f> it = this.f1.iterator();
        while (it.hasNext()) {
            it.next().a(this);
        }
    }

    public final void B0() {
        this.B0.setVisibility((this.A0 == null || N()) ? 8 : 0);
        r0();
    }

    public final void C(int i) {
        Iterator<g> it = this.j1.iterator();
        while (it.hasNext()) {
            it.next().a(this, i);
        }
    }

    public final void C0(boolean z, boolean z2) {
        int defaultColor = this.A1.getDefaultColor();
        int colorForState = this.A1.getColorForState(new int[]{16843623, 16842910}, defaultColor);
        int colorForState2 = this.A1.getColorForState(new int[]{16843518, 16842910}, defaultColor);
        if (z) {
            this.R0 = colorForState2;
        } else if (z2) {
            this.R0 = colorForState;
        } else {
            this.R0 = defaultColor;
        }
    }

    public final void D(Canvas canvas) {
        o42 o42Var = this.I0;
        if (o42Var != null) {
            Rect bounds = o42Var.getBounds();
            bounds.top = bounds.bottom - this.O0;
            this.I0.draw(canvas);
        }
    }

    public final void D0() {
        if (this.i0 == null) {
            return;
        }
        ei4.H0(this.D0, getContext().getResources().getDimensionPixelSize(jz2.material_input_text_to_prefix_suffix_padding), this.i0.getPaddingTop(), (K() || L()) ? 0 : ei4.I(this.i0), this.i0.getPaddingBottom());
    }

    public final void E(Canvas canvas) {
        if (this.E0) {
            this.H1.m(canvas);
        }
    }

    public final void E0() {
        int visibility = this.D0.getVisibility();
        boolean z = (this.C0 == null || N()) ? false : true;
        this.D0.setVisibility(z ? 0 : 8);
        if (visibility != this.D0.getVisibility()) {
            getEndIconDelegate().c(z);
        }
        r0();
    }

    public final void F(boolean z) {
        ValueAnimator valueAnimator = this.K1;
        if (valueAnimator != null && valueAnimator.isRunning()) {
            this.K1.cancel();
        }
        if (z && this.J1) {
            i(Utils.FLOAT_EPSILON);
        } else {
            this.H1.p0(Utils.FLOAT_EPSILON);
        }
        if (A() && ((bd0) this.H0).q0()) {
            y();
        }
        this.G1 = true;
        J();
        B0();
        E0();
    }

    public void F0() {
        TextView textView;
        EditText editText;
        EditText editText2;
        if (this.H0 == null || this.M0 == 0) {
            return;
        }
        boolean z = false;
        boolean z2 = isFocused() || ((editText2 = this.i0) != null && editText2.hasFocus());
        boolean z3 = isHovered() || ((editText = this.i0) != null && editText.isHovered());
        if (!isEnabled()) {
            this.R0 = this.F1;
        } else if (this.m0.k()) {
            if (this.A1 != null) {
                C0(z2, z3);
            } else {
                this.R0 = this.m0.o();
            }
        } else if (!this.p0 || (textView = this.q0) == null) {
            if (z2) {
                this.R0 = this.z1;
            } else if (z3) {
                this.R0 = this.y1;
            } else {
                this.R0 = this.x1;
            }
        } else if (this.A1 != null) {
            C0(z2, z3);
        } else {
            this.R0 = textView.getCurrentTextColor();
        }
        if (getErrorIconDrawable() != null && this.m0.x() && this.m0.k()) {
            z = true;
        }
        setErrorIconVisible(z);
        W();
        Y();
        V();
        if (getEndIconDelegate().d()) {
            j0(this.m0.k());
        }
        if (z2 && isEnabled()) {
            this.O0 = this.Q0;
        } else {
            this.O0 = this.P0;
        }
        if (this.M0 == 2) {
            q0();
        }
        if (this.M0 == 1) {
            if (!isEnabled()) {
                this.S0 = this.C1;
            } else if (z3 && !z2) {
                this.S0 = this.E1;
            } else if (z2) {
                this.S0 = this.D1;
            } else {
                this.S0 = this.B1;
            }
        }
        j();
    }

    public final int G(int i, boolean z) {
        int compoundPaddingLeft = i + this.i0.getCompoundPaddingLeft();
        return (this.A0 == null || z) ? compoundPaddingLeft : (compoundPaddingLeft - this.B0.getMeasuredWidth()) + this.B0.getPaddingLeft();
    }

    public final int H(int i, boolean z) {
        int compoundPaddingRight = i - this.i0.getCompoundPaddingRight();
        return (this.A0 == null || !z) ? compoundPaddingRight : compoundPaddingRight + (this.B0.getMeasuredWidth() - this.B0.getPaddingRight());
    }

    public final boolean I() {
        return this.g1 != 0;
    }

    public final void J() {
        TextView textView = this.v0;
        if (textView == null || !this.u0) {
            return;
        }
        textView.setText((CharSequence) null);
        this.v0.setVisibility(4);
    }

    public boolean K() {
        return this.h0.getVisibility() == 0 && this.i1.getVisibility() == 0;
    }

    public final boolean L() {
        return this.t1.getVisibility() == 0;
    }

    public boolean M() {
        return this.m0.y();
    }

    public final boolean N() {
        return this.G1;
    }

    public boolean O() {
        return this.G0;
    }

    public final boolean P() {
        return this.M0 == 1 && (Build.VERSION.SDK_INT < 16 || this.i0.getMinLines() <= 1);
    }

    public boolean Q() {
        return this.X0.getVisibility() == 0;
    }

    public final int[] R(CheckableImageButton checkableImageButton) {
        int[] drawableState = getDrawableState();
        int[] drawableState2 = checkableImageButton.getDrawableState();
        int length = drawableState.length;
        int[] copyOf = Arrays.copyOf(drawableState, drawableState.length + drawableState2.length);
        System.arraycopy(drawableState2, 0, copyOf, length, drawableState2.length);
        return copyOf;
    }

    public final void S() {
        p();
        a0();
        F0();
        k0();
        h();
        if (this.M0 != 0) {
            u0();
        }
    }

    public final void T() {
        if (A()) {
            RectF rectF = this.V0;
            this.H1.p(rectF, this.i0.getWidth(), this.i0.getGravity());
            l(rectF);
            int i = this.O0;
            this.L0 = i;
            rectF.top = Utils.FLOAT_EPSILON;
            rectF.bottom = i;
            rectF.offset(-getPaddingLeft(), Utils.FLOAT_EPSILON);
            ((bd0) this.H0).w0(rectF);
        }
    }

    public void V() {
        X(this.i1, this.k1);
    }

    public void W() {
        X(this.t1, this.u1);
    }

    public final void X(CheckableImageButton checkableImageButton, ColorStateList colorStateList) {
        Drawable drawable = checkableImageButton.getDrawable();
        if (checkableImageButton.getDrawable() == null || colorStateList == null || !colorStateList.isStateful()) {
            return;
        }
        int colorForState = colorStateList.getColorForState(R(checkableImageButton), colorStateList.getDefaultColor());
        Drawable mutate = androidx.core.graphics.drawable.a.r(drawable).mutate();
        androidx.core.graphics.drawable.a.o(mutate, ColorStateList.valueOf(colorForState));
        checkableImageButton.setImageDrawable(mutate);
    }

    public void Y() {
        X(this.X0, this.Y0);
    }

    public final void Z() {
        TextView textView = this.v0;
        if (textView != null) {
            textView.setVisibility(8);
        }
    }

    public final void a0() {
        if (h0()) {
            ei4.w0(this.i0, this.H0);
        }
    }

    @Override // android.view.ViewGroup
    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        if (view instanceof EditText) {
            FrameLayout.LayoutParams layoutParams2 = new FrameLayout.LayoutParams(layoutParams);
            layoutParams2.gravity = (layoutParams2.gravity & (-113)) | 16;
            this.a.addView(view, layoutParams2);
            this.a.setLayoutParams(layoutParams);
            u0();
            setEditText((EditText) view);
            return;
        }
        super.addView(view, i, layoutParams);
    }

    @Override // android.view.ViewGroup, android.view.View
    @TargetApi(26)
    public void dispatchProvideAutofillStructure(ViewStructure viewStructure, int i) {
        EditText editText = this.i0;
        if (editText == null) {
            super.dispatchProvideAutofillStructure(viewStructure, i);
            return;
        }
        if (this.j0 != null) {
            boolean z = this.G0;
            this.G0 = false;
            CharSequence hint = editText.getHint();
            this.i0.setHint(this.j0);
            try {
                super.dispatchProvideAutofillStructure(viewStructure, i);
                return;
            } finally {
                this.i0.setHint(hint);
                this.G0 = z;
            }
        }
        viewStructure.setAutofillId(getAutofillId());
        onProvideAutofillStructure(viewStructure, i);
        onProvideAutofillVirtualStructure(viewStructure, i);
        viewStructure.setChildCount(this.a.getChildCount());
        for (int i2 = 0; i2 < this.a.getChildCount(); i2++) {
            View childAt = this.a.getChildAt(i2);
            ViewStructure newChild = viewStructure.newChild(i2);
            childAt.dispatchProvideAutofillStructure(newChild, i);
            if (childAt == this.i0) {
                newChild.setHint(getHint());
            }
        }
    }

    @Override // android.view.ViewGroup, android.view.View
    public void dispatchRestoreInstanceState(SparseArray<Parcelable> sparseArray) {
        this.M1 = true;
        super.dispatchRestoreInstanceState(sparseArray);
        this.M1 = false;
    }

    @Override // android.view.View
    public void draw(Canvas canvas) {
        super.draw(canvas);
        E(canvas);
        D(canvas);
    }

    @Override // android.view.ViewGroup, android.view.View
    public void drawableStateChanged() {
        if (this.L1) {
            return;
        }
        boolean z = true;
        this.L1 = true;
        super.drawableStateChanged();
        int[] drawableState = getDrawableState();
        com.google.android.material.internal.a aVar = this.H1;
        boolean z0 = aVar != null ? aVar.z0(drawableState) | false : false;
        if (this.i0 != null) {
            if (!ei4.W(this) || !isEnabled()) {
                z = false;
            }
            v0(z);
        }
        s0();
        F0();
        if (z0) {
            invalidate();
        }
        this.L1 = false;
    }

    public void e(f fVar) {
        this.f1.add(fVar);
        if (this.i0 != null) {
            fVar.a(this);
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:7:0x0015, code lost:
        if (r3.getTextColors().getDefaultColor() == (-65281)) goto L8;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void e0(android.widget.TextView r3, int r4) {
        /*
            r2 = this;
            r0 = 1
            defpackage.t44.q(r3, r4)     // Catch: java.lang.Exception -> L1a
            int r4 = android.os.Build.VERSION.SDK_INT     // Catch: java.lang.Exception -> L1a
            r1 = 23
            if (r4 < r1) goto L18
            android.content.res.ColorStateList r4 = r3.getTextColors()     // Catch: java.lang.Exception -> L1a
            int r4 = r4.getDefaultColor()     // Catch: java.lang.Exception -> L1a
            r1 = -65281(0xffffffffffff00ff, float:NaN)
            if (r4 != r1) goto L18
            goto L1a
        L18:
            r4 = 0
            r0 = r4
        L1a:
            if (r0 == 0) goto L2e
            int r4 = defpackage.y13.TextAppearance_AppCompat_Caption
            defpackage.t44.q(r3, r4)
            android.content.Context r4 = r2.getContext()
            int r0 = defpackage.ty2.design_error
            int r4 = defpackage.m70.d(r4, r0)
            r3.setTextColor(r4)
        L2e:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.textfield.TextInputLayout.e0(android.widget.TextView, int):void");
    }

    public void f(g gVar) {
        this.j1.add(gVar);
    }

    public final boolean f0() {
        return (this.t1.getVisibility() == 0 || ((I() && K()) || this.C0 != null)) && this.g0.getMeasuredWidth() > 0;
    }

    public final void g() {
        TextView textView = this.v0;
        if (textView != null) {
            this.a.addView(textView);
            this.v0.setVisibility(0);
        }
    }

    public final boolean g0() {
        return !(getStartIconDrawable() == null && this.A0 == null) && this.f0.getMeasuredWidth() > 0;
    }

    @Override // android.widget.LinearLayout, android.view.View
    public int getBaseline() {
        EditText editText = this.i0;
        if (editText != null) {
            return editText.getBaseline() + getPaddingTop() + v();
        }
        return super.getBaseline();
    }

    public o42 getBoxBackground() {
        int i = this.M0;
        if (i != 1 && i != 2) {
            throw new IllegalStateException();
        }
        return this.H0;
    }

    public int getBoxBackgroundColor() {
        return this.S0;
    }

    public int getBoxBackgroundMode() {
        return this.M0;
    }

    public float getBoxCornerRadiusBottomEnd() {
        return this.H0.s();
    }

    public float getBoxCornerRadiusBottomStart() {
        return this.H0.t();
    }

    public float getBoxCornerRadiusTopEnd() {
        return this.H0.J();
    }

    public float getBoxCornerRadiusTopStart() {
        return this.H0.I();
    }

    public int getBoxStrokeColor() {
        return this.z1;
    }

    public ColorStateList getBoxStrokeErrorColor() {
        return this.A1;
    }

    public int getBoxStrokeWidth() {
        return this.P0;
    }

    public int getBoxStrokeWidthFocused() {
        return this.Q0;
    }

    public int getCounterMaxLength() {
        return this.o0;
    }

    public CharSequence getCounterOverflowDescription() {
        TextView textView;
        if (this.n0 && this.p0 && (textView = this.q0) != null) {
            return textView.getContentDescription();
        }
        return null;
    }

    public ColorStateList getCounterOverflowTextColor() {
        return this.y0;
    }

    public ColorStateList getCounterTextColor() {
        return this.y0;
    }

    public ColorStateList getDefaultHintTextColor() {
        return this.v1;
    }

    public EditText getEditText() {
        return this.i0;
    }

    public CharSequence getEndIconContentDescription() {
        return this.i1.getContentDescription();
    }

    public Drawable getEndIconDrawable() {
        return this.i1.getDrawable();
    }

    public int getEndIconMode() {
        return this.g1;
    }

    public CheckableImageButton getEndIconView() {
        return this.i1;
    }

    public CharSequence getError() {
        if (this.m0.x()) {
            return this.m0.n();
        }
        return null;
    }

    public CharSequence getErrorContentDescription() {
        return this.m0.m();
    }

    public int getErrorCurrentTextColors() {
        return this.m0.o();
    }

    public Drawable getErrorIconDrawable() {
        return this.t1.getDrawable();
    }

    public final int getErrorTextCurrentColor() {
        return this.m0.o();
    }

    public CharSequence getHelperText() {
        if (this.m0.y()) {
            return this.m0.q();
        }
        return null;
    }

    public int getHelperTextCurrentTextColor() {
        return this.m0.r();
    }

    public CharSequence getHint() {
        if (this.E0) {
            return this.F0;
        }
        return null;
    }

    public final float getHintCollapsedTextHeight() {
        return this.H1.s();
    }

    public final int getHintCurrentCollapsedTextColor() {
        return this.H1.w();
    }

    public ColorStateList getHintTextColor() {
        return this.w1;
    }

    public int getMaxWidth() {
        return this.l0;
    }

    public int getMinWidth() {
        return this.k0;
    }

    @Deprecated
    public CharSequence getPasswordVisibilityToggleContentDescription() {
        return this.i1.getContentDescription();
    }

    @Deprecated
    public Drawable getPasswordVisibilityToggleDrawable() {
        return this.i1.getDrawable();
    }

    public CharSequence getPlaceholderText() {
        if (this.u0) {
            return this.t0;
        }
        return null;
    }

    public int getPlaceholderTextAppearance() {
        return this.x0;
    }

    public ColorStateList getPlaceholderTextColor() {
        return this.w0;
    }

    public CharSequence getPrefixText() {
        return this.A0;
    }

    public ColorStateList getPrefixTextColor() {
        return this.B0.getTextColors();
    }

    public TextView getPrefixTextView() {
        return this.B0;
    }

    public CharSequence getStartIconContentDescription() {
        return this.X0.getContentDescription();
    }

    public Drawable getStartIconDrawable() {
        return this.X0.getDrawable();
    }

    public CharSequence getSuffixText() {
        return this.C0;
    }

    public ColorStateList getSuffixTextColor() {
        return this.D0.getTextColors();
    }

    public TextView getSuffixTextView() {
        return this.D0;
    }

    public Typeface getTypeface() {
        return this.W0;
    }

    public final void h() {
        if (this.i0 == null || this.M0 != 1) {
            return;
        }
        if (n42.h(getContext())) {
            EditText editText = this.i0;
            ei4.H0(editText, ei4.J(editText), getResources().getDimensionPixelSize(jz2.material_filled_edittext_font_2_0_padding_top), ei4.I(this.i0), getResources().getDimensionPixelSize(jz2.material_filled_edittext_font_2_0_padding_bottom));
        } else if (n42.g(getContext())) {
            EditText editText2 = this.i0;
            ei4.H0(editText2, ei4.J(editText2), getResources().getDimensionPixelSize(jz2.material_filled_edittext_font_1_3_padding_top), ei4.I(this.i0), getResources().getDimensionPixelSize(jz2.material_filled_edittext_font_1_3_padding_bottom));
        }
    }

    public final boolean h0() {
        EditText editText = this.i0;
        return (editText == null || this.H0 == null || editText.getBackground() != null || this.M0 == 0) ? false : true;
    }

    public void i(float f2) {
        if (this.H1.D() == f2) {
            return;
        }
        if (this.K1 == null) {
            ValueAnimator valueAnimator = new ValueAnimator();
            this.K1 = valueAnimator;
            valueAnimator.setInterpolator(ne.b);
            this.K1.setDuration(167L);
            this.K1.addUpdateListener(new d());
        }
        this.K1.setFloatValues(this.H1.D(), f2);
        this.K1.start();
    }

    public final void i0() {
        TextView textView = this.v0;
        if (textView == null || !this.u0) {
            return;
        }
        textView.setText(this.t0);
        this.v0.setVisibility(0);
        this.v0.bringToFront();
    }

    public final void j() {
        o42 o42Var = this.H0;
        if (o42Var == null) {
            return;
        }
        o42Var.setShapeAppearanceModel(this.J0);
        if (w()) {
            this.H0.j0(this.O0, this.R0);
        }
        int q = q();
        this.S0 = q;
        this.H0.a0(ColorStateList.valueOf(q));
        if (this.g1 == 3) {
            this.i0.getBackground().invalidateSelf();
        }
        k();
        invalidate();
    }

    public final void j0(boolean z) {
        if (z && getEndIconDrawable() != null) {
            Drawable mutate = androidx.core.graphics.drawable.a.r(getEndIconDrawable()).mutate();
            androidx.core.graphics.drawable.a.n(mutate, this.m0.o());
            this.i1.setImageDrawable(mutate);
            return;
        }
        m();
    }

    public final void k() {
        if (this.I0 == null) {
            return;
        }
        if (x()) {
            this.I0.a0(ColorStateList.valueOf(this.R0));
        }
        invalidate();
    }

    public final void k0() {
        if (this.M0 == 1) {
            if (n42.h(getContext())) {
                this.N0 = getResources().getDimensionPixelSize(jz2.material_font_2_0_box_collapsed_padding_top);
            } else if (n42.g(getContext())) {
                this.N0 = getResources().getDimensionPixelSize(jz2.material_font_1_3_box_collapsed_padding_top);
            }
        }
    }

    public final void l(RectF rectF) {
        float f2 = rectF.left;
        int i = this.K0;
        rectF.left = f2 - i;
        rectF.right += i;
    }

    public final void l0(Rect rect) {
        o42 o42Var = this.I0;
        if (o42Var != null) {
            int i = rect.bottom;
            o42Var.setBounds(rect.left, i - this.Q0, rect.right, i);
        }
    }

    public final void m() {
        n(this.i1, this.l1, this.k1, this.n1, this.m1);
    }

    public final void m0() {
        if (this.q0 != null) {
            EditText editText = this.i0;
            n0(editText == null ? 0 : editText.getText().length());
        }
    }

    public final void n(CheckableImageButton checkableImageButton, boolean z, ColorStateList colorStateList, boolean z2, PorterDuff.Mode mode) {
        Drawable drawable = checkableImageButton.getDrawable();
        if (drawable != null && (z || z2)) {
            drawable = androidx.core.graphics.drawable.a.r(drawable).mutate();
            if (z) {
                androidx.core.graphics.drawable.a.o(drawable, colorStateList);
            }
            if (z2) {
                androidx.core.graphics.drawable.a.p(drawable, mode);
            }
        }
        if (checkableImageButton.getDrawable() != drawable) {
            checkableImageButton.setImageDrawable(drawable);
        }
    }

    public void n0(int i) {
        boolean z = this.p0;
        int i2 = this.o0;
        if (i2 == -1) {
            this.q0.setText(String.valueOf(i));
            this.q0.setContentDescription(null);
            this.p0 = false;
        } else {
            this.p0 = i > i2;
            o0(getContext(), this.q0, i, this.o0, this.p0);
            if (z != this.p0) {
                p0();
            }
            this.q0.setText(gp.c().j(getContext().getString(r13.character_counter_pattern, Integer.valueOf(i), Integer.valueOf(this.o0))));
        }
        if (this.i0 == null || z == this.p0) {
            return;
        }
        v0(false);
        F0();
        s0();
    }

    public final void o() {
        n(this.X0, this.Z0, this.Y0, this.b1, this.a1);
    }

    @Override // android.widget.LinearLayout, android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        EditText editText = this.i0;
        if (editText != null) {
            Rect rect = this.T0;
            mm0.a(this, editText, rect);
            l0(rect);
            if (this.E0) {
                this.H1.m0(this.i0.getTextSize());
                int gravity = this.i0.getGravity();
                this.H1.c0((gravity & (-113)) | 48);
                this.H1.l0(gravity);
                this.H1.Y(r(rect));
                this.H1.h0(u(rect));
                this.H1.U();
                if (!A() || this.G1) {
                    return;
                }
                T();
            }
        }
    }

    @Override // android.widget.LinearLayout, android.view.View
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        boolean t0 = t0();
        boolean r0 = r0();
        if (t0 || r0) {
            this.i0.post(new c());
        }
        x0();
        A0();
        D0();
    }

    @Override // android.view.View
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.a());
        setError(savedState.g0);
        if (savedState.h0) {
            this.i1.post(new b());
        }
        setHint(savedState.i0);
        setHelperText(savedState.j0);
        setPlaceholderText(savedState.k0);
        requestLayout();
    }

    @Override // android.view.View
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        if (this.m0.k()) {
            savedState.g0 = getError();
        }
        savedState.h0 = I() && this.i1.isChecked();
        savedState.i0 = getHint();
        savedState.j0 = getHelperText();
        savedState.k0 = getPlaceholderText();
        return savedState;
    }

    public final void p() {
        int i = this.M0;
        if (i == 0) {
            this.H0 = null;
            this.I0 = null;
        } else if (i == 1) {
            this.H0 = new o42(this.J0);
            this.I0 = new o42();
        } else if (i == 2) {
            if (this.E0 && !(this.H0 instanceof bd0)) {
                this.H0 = new bd0(this.J0);
            } else {
                this.H0 = new o42(this.J0);
            }
            this.I0 = null;
        } else {
            throw new IllegalArgumentException(this.M0 + " is illegal; only @BoxBackgroundMode constants are supported.");
        }
    }

    public final void p0() {
        ColorStateList colorStateList;
        ColorStateList colorStateList2;
        TextView textView = this.q0;
        if (textView != null) {
            e0(textView, this.p0 ? this.r0 : this.s0);
            if (!this.p0 && (colorStateList2 = this.y0) != null) {
                this.q0.setTextColor(colorStateList2);
            }
            if (!this.p0 || (colorStateList = this.z0) == null) {
                return;
            }
            this.q0.setTextColor(colorStateList);
        }
    }

    public final int q() {
        return this.M0 == 1 ? l42.g(l42.e(this, gy2.colorSurface, 0), this.S0) : this.S0;
    }

    public final void q0() {
        if (!A() || this.G1 || this.L0 == this.O0) {
            return;
        }
        y();
        T();
    }

    public final Rect r(Rect rect) {
        if (this.i0 != null) {
            Rect rect2 = this.U0;
            boolean z = ei4.E(this) == 1;
            rect2.bottom = rect.bottom;
            int i = this.M0;
            if (i == 1) {
                rect2.left = G(rect.left, z);
                rect2.top = rect.top + this.N0;
                rect2.right = H(rect.right, z);
                return rect2;
            } else if (i != 2) {
                rect2.left = G(rect.left, z);
                rect2.top = getPaddingTop();
                rect2.right = H(rect.right, z);
                return rect2;
            } else {
                rect2.left = rect.left + this.i0.getPaddingLeft();
                rect2.top = rect.top - v();
                rect2.right = rect.right - this.i0.getPaddingRight();
                return rect2;
            }
        }
        throw new IllegalStateException();
    }

    public final boolean r0() {
        boolean z;
        if (this.i0 == null) {
            return false;
        }
        boolean z2 = true;
        if (g0()) {
            int measuredWidth = this.f0.getMeasuredWidth() - this.i0.getPaddingLeft();
            if (this.c1 == null || this.d1 != measuredWidth) {
                ColorDrawable colorDrawable = new ColorDrawable();
                this.c1 = colorDrawable;
                this.d1 = measuredWidth;
                colorDrawable.setBounds(0, 0, measuredWidth, 1);
            }
            Drawable[] a2 = t44.a(this.i0);
            Drawable drawable = a2[0];
            Drawable drawable2 = this.c1;
            if (drawable != drawable2) {
                t44.l(this.i0, drawable2, a2[1], a2[2], a2[3]);
                z = true;
            }
            z = false;
        } else {
            if (this.c1 != null) {
                Drawable[] a3 = t44.a(this.i0);
                t44.l(this.i0, null, a3[1], a3[2], a3[3]);
                this.c1 = null;
                z = true;
            }
            z = false;
        }
        if (f0()) {
            int measuredWidth2 = this.D0.getMeasuredWidth() - this.i0.getPaddingRight();
            CheckableImageButton endIconToUpdateDummyDrawable = getEndIconToUpdateDummyDrawable();
            if (endIconToUpdateDummyDrawable != null) {
                measuredWidth2 = measuredWidth2 + endIconToUpdateDummyDrawable.getMeasuredWidth() + b42.b((ViewGroup.MarginLayoutParams) endIconToUpdateDummyDrawable.getLayoutParams());
            }
            Drawable[] a4 = t44.a(this.i0);
            Drawable drawable3 = this.o1;
            if (drawable3 != null && this.p1 != measuredWidth2) {
                this.p1 = measuredWidth2;
                drawable3.setBounds(0, 0, measuredWidth2, 1);
                t44.l(this.i0, a4[0], a4[1], this.o1, a4[3]);
            } else {
                if (drawable3 == null) {
                    ColorDrawable colorDrawable2 = new ColorDrawable();
                    this.o1 = colorDrawable2;
                    this.p1 = measuredWidth2;
                    colorDrawable2.setBounds(0, 0, measuredWidth2, 1);
                }
                Drawable drawable4 = a4[2];
                Drawable drawable5 = this.o1;
                if (drawable4 != drawable5) {
                    this.q1 = a4[2];
                    t44.l(this.i0, a4[0], a4[1], drawable5, a4[3]);
                } else {
                    z2 = z;
                }
            }
        } else if (this.o1 == null) {
            return z;
        } else {
            Drawable[] a5 = t44.a(this.i0);
            if (a5[2] == this.o1) {
                t44.l(this.i0, a5[0], a5[1], this.q1, a5[3]);
            } else {
                z2 = z;
            }
            this.o1 = null;
        }
        return z2;
    }

    public final int s(Rect rect, Rect rect2, float f2) {
        if (P()) {
            return (int) (rect2.top + f2);
        }
        return rect.bottom - this.i0.getCompoundPaddingBottom();
    }

    public void s0() {
        Drawable background;
        TextView textView;
        EditText editText = this.i0;
        if (editText == null || this.M0 != 0 || (background = editText.getBackground()) == null) {
            return;
        }
        if (dr0.a(background)) {
            background = background.mutate();
        }
        if (this.m0.k()) {
            background.setColorFilter(gf.e(this.m0.o(), PorterDuff.Mode.SRC_IN));
        } else if (this.p0 && (textView = this.q0) != null) {
            background.setColorFilter(gf.e(textView.getCurrentTextColor(), PorterDuff.Mode.SRC_IN));
        } else {
            androidx.core.graphics.drawable.a.c(background);
            this.i0.refreshDrawableState();
        }
    }

    public void setBoxBackgroundColor(int i) {
        if (this.S0 != i) {
            this.S0 = i;
            this.B1 = i;
            this.D1 = i;
            this.E1 = i;
            j();
        }
    }

    public void setBoxBackgroundColorResource(int i) {
        setBoxBackgroundColor(m70.d(getContext(), i));
    }

    public void setBoxBackgroundColorStateList(ColorStateList colorStateList) {
        int defaultColor = colorStateList.getDefaultColor();
        this.B1 = defaultColor;
        this.S0 = defaultColor;
        this.C1 = colorStateList.getColorForState(new int[]{-16842910}, -1);
        this.D1 = colorStateList.getColorForState(new int[]{16842908, 16842910}, -1);
        this.E1 = colorStateList.getColorForState(new int[]{16843623, 16842910}, -1);
        j();
    }

    public void setBoxBackgroundMode(int i) {
        if (i == this.M0) {
            return;
        }
        this.M0 = i;
        if (this.i0 != null) {
            S();
        }
    }

    public void setBoxCornerRadii(float f2, float f3, float f4, float f5) {
        o42 o42Var = this.H0;
        if (o42Var != null && o42Var.I() == f2 && this.H0.J() == f3 && this.H0.t() == f5 && this.H0.s() == f4) {
            return;
        }
        this.J0 = this.J0.v().E(f2).I(f3).z(f5).v(f4).m();
        j();
    }

    public void setBoxCornerRadiiResources(int i, int i2, int i3, int i4) {
        setBoxCornerRadii(getContext().getResources().getDimension(i), getContext().getResources().getDimension(i2), getContext().getResources().getDimension(i4), getContext().getResources().getDimension(i3));
    }

    public void setBoxStrokeColor(int i) {
        if (this.z1 != i) {
            this.z1 = i;
            F0();
        }
    }

    public void setBoxStrokeColorStateList(ColorStateList colorStateList) {
        if (colorStateList.isStateful()) {
            this.x1 = colorStateList.getDefaultColor();
            this.F1 = colorStateList.getColorForState(new int[]{-16842910}, -1);
            this.y1 = colorStateList.getColorForState(new int[]{16843623, 16842910}, -1);
            this.z1 = colorStateList.getColorForState(new int[]{16842908, 16842910}, -1);
        } else if (this.z1 != colorStateList.getDefaultColor()) {
            this.z1 = colorStateList.getDefaultColor();
        }
        F0();
    }

    public void setBoxStrokeErrorColor(ColorStateList colorStateList) {
        if (this.A1 != colorStateList) {
            this.A1 = colorStateList;
            F0();
        }
    }

    public void setBoxStrokeWidth(int i) {
        this.P0 = i;
        F0();
    }

    public void setBoxStrokeWidthFocused(int i) {
        this.Q0 = i;
        F0();
    }

    public void setBoxStrokeWidthFocusedResource(int i) {
        setBoxStrokeWidthFocused(getResources().getDimensionPixelSize(i));
    }

    public void setBoxStrokeWidthResource(int i) {
        setBoxStrokeWidth(getResources().getDimensionPixelSize(i));
    }

    public void setCounterEnabled(boolean z) {
        if (this.n0 != z) {
            if (z) {
                AppCompatTextView appCompatTextView = new AppCompatTextView(getContext());
                this.q0 = appCompatTextView;
                appCompatTextView.setId(b03.textinput_counter);
                Typeface typeface = this.W0;
                if (typeface != null) {
                    this.q0.setTypeface(typeface);
                }
                this.q0.setMaxLines(1);
                this.m0.d(this.q0, 2);
                b42.d((ViewGroup.MarginLayoutParams) this.q0.getLayoutParams(), getResources().getDimensionPixelOffset(jz2.mtrl_textinput_counter_margin_start));
                p0();
                m0();
            } else {
                this.m0.z(this.q0, 2);
                this.q0 = null;
            }
            this.n0 = z;
        }
    }

    public void setCounterMaxLength(int i) {
        if (this.o0 != i) {
            if (i > 0) {
                this.o0 = i;
            } else {
                this.o0 = -1;
            }
            if (this.n0) {
                m0();
            }
        }
    }

    public void setCounterOverflowTextAppearance(int i) {
        if (this.r0 != i) {
            this.r0 = i;
            p0();
        }
    }

    public void setCounterOverflowTextColor(ColorStateList colorStateList) {
        if (this.z0 != colorStateList) {
            this.z0 = colorStateList;
            p0();
        }
    }

    public void setCounterTextAppearance(int i) {
        if (this.s0 != i) {
            this.s0 = i;
            p0();
        }
    }

    public void setCounterTextColor(ColorStateList colorStateList) {
        if (this.y0 != colorStateList) {
            this.y0 = colorStateList;
            p0();
        }
    }

    public void setDefaultHintTextColor(ColorStateList colorStateList) {
        this.v1 = colorStateList;
        this.w1 = colorStateList;
        if (this.i0 != null) {
            v0(false);
        }
    }

    @Override // android.view.View
    public void setEnabled(boolean z) {
        U(this, z);
        super.setEnabled(z);
    }

    public void setEndIconActivated(boolean z) {
        this.i1.setActivated(z);
    }

    public void setEndIconCheckable(boolean z) {
        this.i1.setCheckable(z);
    }

    public void setEndIconContentDescription(int i) {
        setEndIconContentDescription(i != 0 ? getResources().getText(i) : null);
    }

    public void setEndIconDrawable(int i) {
        setEndIconDrawable(i != 0 ? mf.d(getContext(), i) : null);
    }

    public void setEndIconMode(int i) {
        int i2 = this.g1;
        this.g1 = i;
        C(i2);
        setEndIconVisible(i != 0);
        if (getEndIconDelegate().b(this.M0)) {
            getEndIconDelegate().a();
            m();
            return;
        }
        throw new IllegalStateException("The current box background mode " + this.M0 + " is not supported by the end icon mode " + i);
    }

    public void setEndIconOnClickListener(View.OnClickListener onClickListener) {
        c0(this.i1, onClickListener, this.r1);
    }

    public void setEndIconOnLongClickListener(View.OnLongClickListener onLongClickListener) {
        this.r1 = onLongClickListener;
        d0(this.i1, onLongClickListener);
    }

    public void setEndIconTintList(ColorStateList colorStateList) {
        if (this.k1 != colorStateList) {
            this.k1 = colorStateList;
            this.l1 = true;
            m();
        }
    }

    public void setEndIconTintMode(PorterDuff.Mode mode) {
        if (this.m1 != mode) {
            this.m1 = mode;
            this.n1 = true;
            m();
        }
    }

    public void setEndIconVisible(boolean z) {
        if (K() != z) {
            this.i1.setVisibility(z ? 0 : 8);
            D0();
            r0();
        }
    }

    public void setError(CharSequence charSequence) {
        if (!this.m0.x()) {
            if (TextUtils.isEmpty(charSequence)) {
                return;
            }
            setErrorEnabled(true);
        }
        if (!TextUtils.isEmpty(charSequence)) {
            this.m0.M(charSequence);
        } else {
            this.m0.t();
        }
    }

    public void setErrorContentDescription(CharSequence charSequence) {
        this.m0.B(charSequence);
    }

    public void setErrorEnabled(boolean z) {
        this.m0.C(z);
    }

    public void setErrorIconDrawable(int i) {
        setErrorIconDrawable(i != 0 ? mf.d(getContext(), i) : null);
        W();
    }

    public void setErrorIconOnClickListener(View.OnClickListener onClickListener) {
        c0(this.t1, onClickListener, this.s1);
    }

    public void setErrorIconOnLongClickListener(View.OnLongClickListener onLongClickListener) {
        this.s1 = onLongClickListener;
        d0(this.t1, onLongClickListener);
    }

    public void setErrorIconTintList(ColorStateList colorStateList) {
        this.u1 = colorStateList;
        Drawable drawable = this.t1.getDrawable();
        if (drawable != null) {
            drawable = androidx.core.graphics.drawable.a.r(drawable).mutate();
            androidx.core.graphics.drawable.a.o(drawable, colorStateList);
        }
        if (this.t1.getDrawable() != drawable) {
            this.t1.setImageDrawable(drawable);
        }
    }

    public void setErrorIconTintMode(PorterDuff.Mode mode) {
        Drawable drawable = this.t1.getDrawable();
        if (drawable != null) {
            drawable = androidx.core.graphics.drawable.a.r(drawable).mutate();
            androidx.core.graphics.drawable.a.p(drawable, mode);
        }
        if (this.t1.getDrawable() != drawable) {
            this.t1.setImageDrawable(drawable);
        }
    }

    public void setErrorTextAppearance(int i) {
        this.m0.D(i);
    }

    public void setErrorTextColor(ColorStateList colorStateList) {
        this.m0.E(colorStateList);
    }

    public void setExpandedHintEnabled(boolean z) {
        if (this.I1 != z) {
            this.I1 = z;
            v0(false);
        }
    }

    public void setHelperText(CharSequence charSequence) {
        if (TextUtils.isEmpty(charSequence)) {
            if (M()) {
                setHelperTextEnabled(false);
                return;
            }
            return;
        }
        if (!M()) {
            setHelperTextEnabled(true);
        }
        this.m0.N(charSequence);
    }

    public void setHelperTextColor(ColorStateList colorStateList) {
        this.m0.H(colorStateList);
    }

    public void setHelperTextEnabled(boolean z) {
        this.m0.G(z);
    }

    public void setHelperTextTextAppearance(int i) {
        this.m0.F(i);
    }

    public void setHint(CharSequence charSequence) {
        if (this.E0) {
            setHintInternal(charSequence);
            sendAccessibilityEvent(2048);
        }
    }

    public void setHintAnimationEnabled(boolean z) {
        this.J1 = z;
    }

    public void setHintEnabled(boolean z) {
        if (z != this.E0) {
            this.E0 = z;
            if (!z) {
                this.G0 = false;
                if (!TextUtils.isEmpty(this.F0) && TextUtils.isEmpty(this.i0.getHint())) {
                    this.i0.setHint(this.F0);
                }
                setHintInternal(null);
            } else {
                CharSequence hint = this.i0.getHint();
                if (!TextUtils.isEmpty(hint)) {
                    if (TextUtils.isEmpty(this.F0)) {
                        setHint(hint);
                    }
                    this.i0.setHint((CharSequence) null);
                }
                this.G0 = true;
            }
            if (this.i0 != null) {
                u0();
            }
        }
    }

    public void setHintTextAppearance(int i) {
        this.H1.Z(i);
        this.w1 = this.H1.q();
        if (this.i0 != null) {
            v0(false);
            u0();
        }
    }

    public void setHintTextColor(ColorStateList colorStateList) {
        if (this.w1 != colorStateList) {
            if (this.v1 == null) {
                this.H1.b0(colorStateList);
            }
            this.w1 = colorStateList;
            if (this.i0 != null) {
                v0(false);
            }
        }
    }

    public void setMaxWidth(int i) {
        this.l0 = i;
        EditText editText = this.i0;
        if (editText == null || i == -1) {
            return;
        }
        editText.setMaxWidth(i);
    }

    public void setMaxWidthResource(int i) {
        setMaxWidth(getContext().getResources().getDimensionPixelSize(i));
    }

    public void setMinWidth(int i) {
        this.k0 = i;
        EditText editText = this.i0;
        if (editText == null || i == -1) {
            return;
        }
        editText.setMinWidth(i);
    }

    public void setMinWidthResource(int i) {
        setMinWidth(getContext().getResources().getDimensionPixelSize(i));
    }

    @Deprecated
    public void setPasswordVisibilityToggleContentDescription(int i) {
        setPasswordVisibilityToggleContentDescription(i != 0 ? getResources().getText(i) : null);
    }

    @Deprecated
    public void setPasswordVisibilityToggleDrawable(int i) {
        setPasswordVisibilityToggleDrawable(i != 0 ? mf.d(getContext(), i) : null);
    }

    @Deprecated
    public void setPasswordVisibilityToggleEnabled(boolean z) {
        if (z && this.g1 != 1) {
            setEndIconMode(1);
        } else if (z) {
        } else {
            setEndIconMode(0);
        }
    }

    @Deprecated
    public void setPasswordVisibilityToggleTintList(ColorStateList colorStateList) {
        this.k1 = colorStateList;
        this.l1 = true;
        m();
    }

    @Deprecated
    public void setPasswordVisibilityToggleTintMode(PorterDuff.Mode mode) {
        this.m1 = mode;
        this.n1 = true;
        m();
    }

    public void setPlaceholderText(CharSequence charSequence) {
        if (this.u0 && TextUtils.isEmpty(charSequence)) {
            setPlaceholderTextEnabled(false);
        } else {
            if (!this.u0) {
                setPlaceholderTextEnabled(true);
            }
            this.t0 = charSequence;
        }
        y0();
    }

    public void setPlaceholderTextAppearance(int i) {
        this.x0 = i;
        TextView textView = this.v0;
        if (textView != null) {
            t44.q(textView, i);
        }
    }

    public void setPlaceholderTextColor(ColorStateList colorStateList) {
        if (this.w0 != colorStateList) {
            this.w0 = colorStateList;
            TextView textView = this.v0;
            if (textView == null || colorStateList == null) {
                return;
            }
            textView.setTextColor(colorStateList);
        }
    }

    public void setPrefixText(CharSequence charSequence) {
        this.A0 = TextUtils.isEmpty(charSequence) ? null : charSequence;
        this.B0.setText(charSequence);
        B0();
    }

    public void setPrefixTextAppearance(int i) {
        t44.q(this.B0, i);
    }

    public void setPrefixTextColor(ColorStateList colorStateList) {
        this.B0.setTextColor(colorStateList);
    }

    public void setStartIconCheckable(boolean z) {
        this.X0.setCheckable(z);
    }

    public void setStartIconContentDescription(int i) {
        setStartIconContentDescription(i != 0 ? getResources().getText(i) : null);
    }

    public void setStartIconDrawable(int i) {
        setStartIconDrawable(i != 0 ? mf.d(getContext(), i) : null);
    }

    public void setStartIconOnClickListener(View.OnClickListener onClickListener) {
        c0(this.X0, onClickListener, this.e1);
    }

    public void setStartIconOnLongClickListener(View.OnLongClickListener onLongClickListener) {
        this.e1 = onLongClickListener;
        d0(this.X0, onLongClickListener);
    }

    public void setStartIconTintList(ColorStateList colorStateList) {
        if (this.Y0 != colorStateList) {
            this.Y0 = colorStateList;
            this.Z0 = true;
            o();
        }
    }

    public void setStartIconTintMode(PorterDuff.Mode mode) {
        if (this.a1 != mode) {
            this.a1 = mode;
            this.b1 = true;
            o();
        }
    }

    public void setStartIconVisible(boolean z) {
        if (Q() != z) {
            this.X0.setVisibility(z ? 0 : 8);
            A0();
            r0();
        }
    }

    public void setSuffixText(CharSequence charSequence) {
        this.C0 = TextUtils.isEmpty(charSequence) ? null : charSequence;
        this.D0.setText(charSequence);
        E0();
    }

    public void setSuffixTextAppearance(int i) {
        t44.q(this.D0, i);
    }

    public void setSuffixTextColor(ColorStateList colorStateList) {
        this.D0.setTextColor(colorStateList);
    }

    public void setTextInputAccessibilityDelegate(e eVar) {
        EditText editText = this.i0;
        if (editText != null) {
            ei4.t0(editText, eVar);
        }
    }

    public void setTypeface(Typeface typeface) {
        if (typeface != this.W0) {
            this.W0 = typeface;
            this.H1.C0(typeface);
            this.m0.J(typeface);
            TextView textView = this.q0;
            if (textView != null) {
                textView.setTypeface(typeface);
            }
        }
    }

    public final int t(Rect rect, float f2) {
        if (P()) {
            return (int) (rect.centerY() - (f2 / 2.0f));
        }
        return rect.top + this.i0.getCompoundPaddingTop();
    }

    public final boolean t0() {
        int max;
        if (this.i0 != null && this.i0.getMeasuredHeight() < (max = Math.max(this.g0.getMeasuredHeight(), this.f0.getMeasuredHeight()))) {
            this.i0.setMinimumHeight(max);
            return true;
        }
        return false;
    }

    public final Rect u(Rect rect) {
        if (this.i0 != null) {
            Rect rect2 = this.U0;
            float B = this.H1.B();
            rect2.left = rect.left + this.i0.getCompoundPaddingLeft();
            rect2.top = t(rect, B);
            rect2.right = rect.right - this.i0.getCompoundPaddingRight();
            rect2.bottom = s(rect, rect2, B);
            return rect2;
        }
        throw new IllegalStateException();
    }

    public final void u0() {
        if (this.M0 != 1) {
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) this.a.getLayoutParams();
            int v = v();
            if (v != layoutParams.topMargin) {
                layoutParams.topMargin = v;
                this.a.requestLayout();
            }
        }
    }

    public final int v() {
        float s;
        if (this.E0) {
            int i = this.M0;
            if (i == 0 || i == 1) {
                s = this.H1.s();
            } else if (i != 2) {
                return 0;
            } else {
                s = this.H1.s() / 2.0f;
            }
            return (int) s;
        }
        return 0;
    }

    public void v0(boolean z) {
        w0(z, false);
    }

    public final boolean w() {
        return this.M0 == 2 && x();
    }

    public final void w0(boolean z, boolean z2) {
        ColorStateList colorStateList;
        TextView textView;
        boolean isEnabled = isEnabled();
        EditText editText = this.i0;
        boolean z3 = (editText == null || TextUtils.isEmpty(editText.getText())) ? false : true;
        EditText editText2 = this.i0;
        boolean z4 = editText2 != null && editText2.hasFocus();
        boolean k = this.m0.k();
        ColorStateList colorStateList2 = this.v1;
        if (colorStateList2 != null) {
            this.H1.b0(colorStateList2);
            this.H1.k0(this.v1);
        }
        if (!isEnabled) {
            ColorStateList colorStateList3 = this.v1;
            int colorForState = colorStateList3 != null ? colorStateList3.getColorForState(new int[]{-16842910}, this.F1) : this.F1;
            this.H1.b0(ColorStateList.valueOf(colorForState));
            this.H1.k0(ColorStateList.valueOf(colorForState));
        } else if (k) {
            this.H1.b0(this.m0.p());
        } else if (this.p0 && (textView = this.q0) != null) {
            this.H1.b0(textView.getTextColors());
        } else if (z4 && (colorStateList = this.w1) != null) {
            this.H1.b0(colorStateList);
        }
        if (!z3 && this.I1 && (!isEnabled() || !z4)) {
            if (z2 || !this.G1) {
                F(z);
            }
        } else if (z2 || this.G1) {
            z(z);
        }
    }

    public final boolean x() {
        return this.O0 > -1 && this.R0 != 0;
    }

    public final void x0() {
        EditText editText;
        if (this.v0 == null || (editText = this.i0) == null) {
            return;
        }
        this.v0.setGravity(editText.getGravity());
        this.v0.setPadding(this.i0.getCompoundPaddingLeft(), this.i0.getCompoundPaddingTop(), this.i0.getCompoundPaddingRight(), this.i0.getCompoundPaddingBottom());
    }

    public final void y() {
        if (A()) {
            ((bd0) this.H0).t0();
        }
    }

    public final void y0() {
        EditText editText = this.i0;
        z0(editText == null ? 0 : editText.getText().length());
    }

    public final void z(boolean z) {
        ValueAnimator valueAnimator = this.K1;
        if (valueAnimator != null && valueAnimator.isRunning()) {
            this.K1.cancel();
        }
        if (z && this.J1) {
            i(1.0f);
        } else {
            this.H1.p0(1.0f);
        }
        this.G1 = false;
        if (A()) {
            T();
        }
        y0();
        B0();
        E0();
    }

    public final void z0(int i) {
        if (i == 0 && !this.G1) {
            i0();
        } else {
            J();
        }
    }

    public TextInputLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, gy2.textInputStyle);
    }

    public void setEndIconContentDescription(CharSequence charSequence) {
        if (getEndIconContentDescription() != charSequence) {
            this.i1.setContentDescription(charSequence);
        }
    }

    public void setEndIconDrawable(Drawable drawable) {
        this.i1.setImageDrawable(drawable);
        V();
    }

    public void setStartIconContentDescription(CharSequence charSequence) {
        if (getStartIconContentDescription() != charSequence) {
            this.X0.setContentDescription(charSequence);
        }
    }

    public void setStartIconDrawable(Drawable drawable) {
        this.X0.setImageDrawable(drawable);
        if (drawable != null) {
            setStartIconVisible(true);
            Y();
            return;
        }
        setStartIconVisible(false);
        setStartIconOnClickListener(null);
        setStartIconOnLongClickListener(null);
        setStartIconContentDescription((CharSequence) null);
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v150 */
    /* JADX WARN: Type inference failed for: r2v46 */
    /* JADX WARN: Type inference failed for: r2v47, types: [int, boolean] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public TextInputLayout(android.content.Context r27, android.util.AttributeSet r28, int r29) {
        /*
            Method dump skipped, instructions count: 1568
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.textfield.TextInputLayout.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }

    public void setErrorIconDrawable(Drawable drawable) {
        this.t1.setImageDrawable(drawable);
        setErrorIconVisible(drawable != null && this.m0.x());
    }

    @Deprecated
    public void setPasswordVisibilityToggleContentDescription(CharSequence charSequence) {
        this.i1.setContentDescription(charSequence);
    }

    @Deprecated
    public void setPasswordVisibilityToggleDrawable(Drawable drawable) {
        this.i1.setImageDrawable(drawable);
    }

    public void setHint(int i) {
        setHint(i != 0 ? getResources().getText(i) : null);
    }
}
