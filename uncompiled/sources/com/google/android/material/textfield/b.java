package com.google.android.material.textfield;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.RippleDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Spinner;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.material.textfield.TextInputLayout;

/* compiled from: DropdownMenuEndIconDelegate.java */
/* loaded from: classes2.dex */
public class b extends kv0 {
    public static final boolean q;
    public final TextWatcher d;
    public final View.OnFocusChangeListener e;
    public final TextInputLayout.e f;
    public final TextInputLayout.f g;
    @SuppressLint({"ClickableViewAccessibility"})
    public final TextInputLayout.g h;
    public boolean i;
    public boolean j;
    public long k;
    public StateListDrawable l;
    public o42 m;
    public AccessibilityManager n;
    public ValueAnimator o;
    public ValueAnimator p;

    /* compiled from: DropdownMenuEndIconDelegate.java */
    /* loaded from: classes2.dex */
    public class a extends w44 {

        /* compiled from: DropdownMenuEndIconDelegate.java */
        /* renamed from: com.google.android.material.textfield.b$a$a  reason: collision with other inner class name */
        /* loaded from: classes2.dex */
        public class RunnableC0126a implements Runnable {
            public final /* synthetic */ AutoCompleteTextView a;

            public RunnableC0126a(AutoCompleteTextView autoCompleteTextView) {
                this.a = autoCompleteTextView;
            }

            @Override // java.lang.Runnable
            public void run() {
                boolean isPopupShowing = this.a.isPopupShowing();
                b.this.E(isPopupShowing);
                b.this.i = isPopupShowing;
            }
        }

        public a() {
        }

        @Override // defpackage.w44, android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            AutoCompleteTextView y = b.y(b.this.a.getEditText());
            if (b.this.n.isTouchExplorationEnabled() && b.D(y) && !b.this.c.hasFocus()) {
                y.dismissDropDown();
            }
            y.post(new RunnableC0126a(y));
        }
    }

    /* compiled from: DropdownMenuEndIconDelegate.java */
    /* renamed from: com.google.android.material.textfield.b$b  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public class C0127b implements ValueAnimator.AnimatorUpdateListener {
        public C0127b() {
        }

        @Override // android.animation.ValueAnimator.AnimatorUpdateListener
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            b.this.c.setAlpha(((Float) valueAnimator.getAnimatedValue()).floatValue());
        }
    }

    /* compiled from: DropdownMenuEndIconDelegate.java */
    /* loaded from: classes2.dex */
    public class c implements View.OnFocusChangeListener {
        public c() {
        }

        @Override // android.view.View.OnFocusChangeListener
        public void onFocusChange(View view, boolean z) {
            b.this.a.setEndIconActivated(z);
            if (z) {
                return;
            }
            b.this.E(false);
            b.this.i = false;
        }
    }

    /* compiled from: DropdownMenuEndIconDelegate.java */
    /* loaded from: classes2.dex */
    public class d extends TextInputLayout.e {
        public d(TextInputLayout textInputLayout) {
            super(textInputLayout);
        }

        @Override // com.google.android.material.textfield.TextInputLayout.e, defpackage.z5
        public void g(View view, b6 b6Var) {
            super.g(view, b6Var);
            if (!b.D(b.this.a.getEditText())) {
                b6Var.c0(Spinner.class.getName());
            }
            if (b6Var.M()) {
                b6Var.n0(null);
            }
        }

        @Override // defpackage.z5
        public void h(View view, AccessibilityEvent accessibilityEvent) {
            super.h(view, accessibilityEvent);
            AutoCompleteTextView y = b.y(b.this.a.getEditText());
            if (accessibilityEvent.getEventType() == 1 && b.this.n.isTouchExplorationEnabled() && !b.D(b.this.a.getEditText())) {
                b.this.H(y);
            }
        }
    }

    /* compiled from: DropdownMenuEndIconDelegate.java */
    /* loaded from: classes2.dex */
    public class e implements TextInputLayout.f {
        public e() {
        }

        @Override // com.google.android.material.textfield.TextInputLayout.f
        public void a(TextInputLayout textInputLayout) {
            AutoCompleteTextView y = b.y(textInputLayout.getEditText());
            b.this.F(y);
            b.this.v(y);
            b.this.G(y);
            y.setThreshold(0);
            y.removeTextChangedListener(b.this.d);
            y.addTextChangedListener(b.this.d);
            textInputLayout.setEndIconCheckable(true);
            textInputLayout.setErrorIconDrawable((Drawable) null);
            if (!b.D(y)) {
                ei4.D0(b.this.c, 2);
            }
            textInputLayout.setTextInputAccessibilityDelegate(b.this.f);
            textInputLayout.setEndIconVisible(true);
        }
    }

    /* compiled from: DropdownMenuEndIconDelegate.java */
    /* loaded from: classes2.dex */
    public class f implements TextInputLayout.g {

        /* compiled from: DropdownMenuEndIconDelegate.java */
        /* loaded from: classes2.dex */
        public class a implements Runnable {
            public final /* synthetic */ AutoCompleteTextView a;

            public a(AutoCompleteTextView autoCompleteTextView) {
                this.a = autoCompleteTextView;
            }

            @Override // java.lang.Runnable
            public void run() {
                this.a.removeTextChangedListener(b.this.d);
            }
        }

        public f() {
        }

        @Override // com.google.android.material.textfield.TextInputLayout.g
        public void a(TextInputLayout textInputLayout, int i) {
            AutoCompleteTextView autoCompleteTextView = (AutoCompleteTextView) textInputLayout.getEditText();
            if (autoCompleteTextView == null || i != 3) {
                return;
            }
            autoCompleteTextView.post(new a(autoCompleteTextView));
            if (autoCompleteTextView.getOnFocusChangeListener() == b.this.e) {
                autoCompleteTextView.setOnFocusChangeListener(null);
            }
            autoCompleteTextView.setOnTouchListener(null);
            if (b.q) {
                autoCompleteTextView.setOnDismissListener(null);
            }
        }
    }

    /* compiled from: DropdownMenuEndIconDelegate.java */
    /* loaded from: classes2.dex */
    public class g implements View.OnClickListener {
        public g() {
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            b.this.H((AutoCompleteTextView) b.this.a.getEditText());
        }
    }

    /* compiled from: DropdownMenuEndIconDelegate.java */
    /* loaded from: classes2.dex */
    public class h implements View.OnTouchListener {
        public final /* synthetic */ AutoCompleteTextView a;

        public h(AutoCompleteTextView autoCompleteTextView) {
            this.a = autoCompleteTextView;
        }

        @Override // android.view.View.OnTouchListener
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() == 1) {
                if (b.this.C()) {
                    b.this.i = false;
                }
                b.this.H(this.a);
            }
            return false;
        }
    }

    /* compiled from: DropdownMenuEndIconDelegate.java */
    /* loaded from: classes2.dex */
    public class i implements AutoCompleteTextView.OnDismissListener {
        public i() {
        }

        @Override // android.widget.AutoCompleteTextView.OnDismissListener
        public void onDismiss() {
            b.this.i = true;
            b.this.k = System.currentTimeMillis();
            b.this.E(false);
        }
    }

    /* compiled from: DropdownMenuEndIconDelegate.java */
    /* loaded from: classes2.dex */
    public class j extends AnimatorListenerAdapter {
        public j() {
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            b bVar = b.this;
            bVar.c.setChecked(bVar.j);
            b.this.p.start();
        }
    }

    static {
        q = Build.VERSION.SDK_INT >= 21;
    }

    public b(TextInputLayout textInputLayout) {
        super(textInputLayout);
        this.d = new a();
        this.e = new c();
        this.f = new d(this.a);
        this.g = new e();
        this.h = new f();
        this.i = false;
        this.j = false;
        this.k = Long.MAX_VALUE;
    }

    public static boolean D(EditText editText) {
        return editText.getKeyListener() != null;
    }

    public static AutoCompleteTextView y(EditText editText) {
        if (editText instanceof AutoCompleteTextView) {
            return (AutoCompleteTextView) editText;
        }
        throw new RuntimeException("EditText needs to be an AutoCompleteTextView if an Exposed Dropdown Menu is being used.");
    }

    public final o42 A(float f2, float f3, float f4, int i2) {
        pn3 m = pn3.a().E(f2).I(f2).v(f3).z(f3).m();
        o42 m2 = o42.m(this.b, f4);
        m2.setShapeAppearanceModel(m);
        m2.c0(0, i2, 0, i2);
        return m2;
    }

    public final void B() {
        this.p = z(67, Utils.FLOAT_EPSILON, 1.0f);
        ValueAnimator z = z(50, 1.0f, Utils.FLOAT_EPSILON);
        this.o = z;
        z.addListener(new j());
    }

    public final boolean C() {
        long currentTimeMillis = System.currentTimeMillis() - this.k;
        return currentTimeMillis < 0 || currentTimeMillis > 300;
    }

    public final void E(boolean z) {
        if (this.j != z) {
            this.j = z;
            this.p.cancel();
            this.o.start();
        }
    }

    public final void F(AutoCompleteTextView autoCompleteTextView) {
        if (q) {
            int boxBackgroundMode = this.a.getBoxBackgroundMode();
            if (boxBackgroundMode == 2) {
                autoCompleteTextView.setDropDownBackgroundDrawable(this.m);
            } else if (boxBackgroundMode == 1) {
                autoCompleteTextView.setDropDownBackgroundDrawable(this.l);
            }
        }
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public final void G(AutoCompleteTextView autoCompleteTextView) {
        autoCompleteTextView.setOnTouchListener(new h(autoCompleteTextView));
        autoCompleteTextView.setOnFocusChangeListener(this.e);
        if (q) {
            autoCompleteTextView.setOnDismissListener(new i());
        }
    }

    public final void H(AutoCompleteTextView autoCompleteTextView) {
        if (autoCompleteTextView == null) {
            return;
        }
        if (C()) {
            this.i = false;
        }
        if (!this.i) {
            if (q) {
                E(!this.j);
            } else {
                this.j = !this.j;
                this.c.toggle();
            }
            if (this.j) {
                autoCompleteTextView.requestFocus();
                autoCompleteTextView.showDropDown();
                return;
            }
            autoCompleteTextView.dismissDropDown();
            return;
        }
        this.i = false;
    }

    @Override // defpackage.kv0
    public void a() {
        float dimensionPixelOffset = this.b.getResources().getDimensionPixelOffset(jz2.mtrl_shape_corner_size_small_component);
        float dimensionPixelOffset2 = this.b.getResources().getDimensionPixelOffset(jz2.mtrl_exposed_dropdown_menu_popup_elevation);
        int dimensionPixelOffset3 = this.b.getResources().getDimensionPixelOffset(jz2.mtrl_exposed_dropdown_menu_popup_vertical_padding);
        o42 A = A(dimensionPixelOffset, dimensionPixelOffset, dimensionPixelOffset2, dimensionPixelOffset3);
        o42 A2 = A(Utils.FLOAT_EPSILON, dimensionPixelOffset, dimensionPixelOffset2, dimensionPixelOffset3);
        this.m = A;
        StateListDrawable stateListDrawable = new StateListDrawable();
        this.l = stateListDrawable;
        stateListDrawable.addState(new int[]{16842922}, A);
        this.l.addState(new int[0], A2);
        this.a.setEndIconDrawable(mf.d(this.b, q ? qz2.mtrl_dropdown_arrow : qz2.mtrl_ic_arrow_drop_down));
        TextInputLayout textInputLayout = this.a;
        textInputLayout.setEndIconContentDescription(textInputLayout.getResources().getText(r13.exposed_dropdown_menu_content_description));
        this.a.setEndIconOnClickListener(new g());
        this.a.e(this.g);
        this.a.f(this.h);
        B();
        this.n = (AccessibilityManager) this.b.getSystemService("accessibility");
    }

    @Override // defpackage.kv0
    public boolean b(int i2) {
        return i2 != 0;
    }

    @Override // defpackage.kv0
    public boolean d() {
        return true;
    }

    public final void v(AutoCompleteTextView autoCompleteTextView) {
        if (D(autoCompleteTextView)) {
            return;
        }
        int boxBackgroundMode = this.a.getBoxBackgroundMode();
        o42 boxBackground = this.a.getBoxBackground();
        int d2 = l42.d(autoCompleteTextView, gy2.colorControlHighlight);
        int[][] iArr = {new int[]{16842919}, new int[0]};
        if (boxBackgroundMode == 2) {
            x(autoCompleteTextView, d2, iArr, boxBackground);
        } else if (boxBackgroundMode == 1) {
            w(autoCompleteTextView, d2, iArr, boxBackground);
        }
    }

    public final void w(AutoCompleteTextView autoCompleteTextView, int i2, int[][] iArr, o42 o42Var) {
        int boxBackgroundColor = this.a.getBoxBackgroundColor();
        int[] iArr2 = {l42.h(i2, boxBackgroundColor, 0.1f), boxBackgroundColor};
        if (q) {
            ei4.w0(autoCompleteTextView, new RippleDrawable(new ColorStateList(iArr, iArr2), o42Var, o42Var));
            return;
        }
        o42 o42Var2 = new o42(o42Var.D());
        o42Var2.a0(new ColorStateList(iArr, iArr2));
        LayerDrawable layerDrawable = new LayerDrawable(new Drawable[]{o42Var, o42Var2});
        int J = ei4.J(autoCompleteTextView);
        int paddingTop = autoCompleteTextView.getPaddingTop();
        int I = ei4.I(autoCompleteTextView);
        int paddingBottom = autoCompleteTextView.getPaddingBottom();
        ei4.w0(autoCompleteTextView, layerDrawable);
        ei4.H0(autoCompleteTextView, J, paddingTop, I, paddingBottom);
    }

    public final void x(AutoCompleteTextView autoCompleteTextView, int i2, int[][] iArr, o42 o42Var) {
        LayerDrawable layerDrawable;
        int d2 = l42.d(autoCompleteTextView, gy2.colorSurface);
        o42 o42Var2 = new o42(o42Var.D());
        int h2 = l42.h(i2, d2, 0.1f);
        o42Var2.a0(new ColorStateList(iArr, new int[]{h2, 0}));
        if (q) {
            o42Var2.setTint(d2);
            ColorStateList colorStateList = new ColorStateList(iArr, new int[]{h2, d2});
            o42 o42Var3 = new o42(o42Var.D());
            o42Var3.setTint(-1);
            layerDrawable = new LayerDrawable(new Drawable[]{new RippleDrawable(colorStateList, o42Var2, o42Var3), o42Var});
        } else {
            layerDrawable = new LayerDrawable(new Drawable[]{o42Var2, o42Var});
        }
        ei4.w0(autoCompleteTextView, layerDrawable);
    }

    public final ValueAnimator z(int i2, float... fArr) {
        ValueAnimator ofFloat = ValueAnimator.ofFloat(fArr);
        ofFloat.setInterpolator(ne.a);
        ofFloat.setDuration(i2);
        ofFloat.addUpdateListener(new C0127b());
        return ofFloat;
    }
}
