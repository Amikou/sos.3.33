package com.google.android.material.switchmaterial;

import android.content.Context;
import android.content.res.ColorStateList;
import android.util.AttributeSet;
import androidx.appcompat.widget.SwitchCompat;

/* loaded from: classes2.dex */
public class SwitchMaterial extends SwitchCompat {
    public static final int X0 = y13.Widget_MaterialComponents_CompoundButton_Switch;
    public static final int[][] Y0 = {new int[]{16842910, 16842912}, new int[]{16842910, -16842912}, new int[]{-16842910, 16842912}, new int[]{-16842910, -16842912}};
    public final lu0 T0;
    public ColorStateList U0;
    public ColorStateList V0;
    public boolean W0;

    public SwitchMaterial(Context context) {
        this(context, null);
    }

    private ColorStateList getMaterialThemeColorsThumbTintList() {
        if (this.U0 == null) {
            int d = l42.d(this, gy2.colorSurface);
            int d2 = l42.d(this, gy2.colorControlActivated);
            float dimension = getResources().getDimension(jz2.mtrl_switch_thumb_elevation);
            if (this.T0.e()) {
                dimension += mk4.g(this);
            }
            int c = this.T0.c(d, dimension);
            int[][] iArr = Y0;
            int[] iArr2 = new int[iArr.length];
            iArr2[0] = l42.h(d, d2, 1.0f);
            iArr2[1] = c;
            iArr2[2] = l42.h(d, d2, 0.38f);
            iArr2[3] = c;
            this.U0 = new ColorStateList(iArr, iArr2);
        }
        return this.U0;
    }

    private ColorStateList getMaterialThemeColorsTrackTintList() {
        if (this.V0 == null) {
            int[][] iArr = Y0;
            int[] iArr2 = new int[iArr.length];
            int d = l42.d(this, gy2.colorSurface);
            int d2 = l42.d(this, gy2.colorControlActivated);
            int d3 = l42.d(this, gy2.colorOnSurface);
            iArr2[0] = l42.h(d, d2, 0.54f);
            iArr2[1] = l42.h(d, d3, 0.32f);
            iArr2[2] = l42.h(d, d2, 0.12f);
            iArr2[3] = l42.h(d, d3, 0.12f);
            this.V0 = new ColorStateList(iArr, iArr2);
        }
        return this.V0;
    }

    @Override // android.widget.TextView, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.W0 && getThumbTintList() == null) {
            setThumbTintList(getMaterialThemeColorsThumbTintList());
        }
        if (this.W0 && getTrackTintList() == null) {
            setTrackTintList(getMaterialThemeColorsTrackTintList());
        }
    }

    public void setUseMaterialThemeColors(boolean z) {
        this.W0 = z;
        if (z) {
            setThumbTintList(getMaterialThemeColorsThumbTintList());
            setTrackTintList(getMaterialThemeColorsTrackTintList());
            return;
        }
        setThumbTintList(null);
        setTrackTintList(null);
    }

    public SwitchMaterial(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, gy2.switchStyle);
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public SwitchMaterial(android.content.Context r7, android.util.AttributeSet r8, int r9) {
        /*
            r6 = this;
            int r4 = com.google.android.material.switchmaterial.SwitchMaterial.X0
            android.content.Context r7 = defpackage.r42.c(r7, r8, r9, r4)
            r6.<init>(r7, r8, r9)
            android.content.Context r0 = r6.getContext()
            lu0 r7 = new lu0
            r7.<init>(r0)
            r6.T0 = r7
            int[] r2 = defpackage.o23.SwitchMaterial
            r7 = 0
            int[] r5 = new int[r7]
            r1 = r8
            r3 = r9
            android.content.res.TypedArray r8 = defpackage.a54.h(r0, r1, r2, r3, r4, r5)
            int r9 = defpackage.o23.SwitchMaterial_useMaterialThemeColors
            boolean r7 = r8.getBoolean(r9, r7)
            r6.W0 = r7
            r8.recycle()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.switchmaterial.SwitchMaterial.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }
}
