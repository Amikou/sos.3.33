package com.google.android.play.core.internal;

import android.content.res.AssetManager;
import java.io.File;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

/* loaded from: classes2.dex */
public final class a {
    public XmlPullParser a;

    public a(at4 at4Var) {
    }

    public final void a(AssetManager assetManager, File file) throws IOException {
        this.a = assetManager.openXmlResourceParser(at4.b(assetManager, file), "AndroidManifest.xml");
    }

    public final long b() throws IOException, XmlPullParserException {
        if (this.a != null) {
            while (true) {
                int next = this.a.next();
                if (next != 2) {
                    if (next == 1) {
                        break;
                    }
                } else if (this.a.getName().equals("manifest")) {
                    String attributeValue = this.a.getAttributeValue("http://schemas.android.com/apk/res/android", "versionCode");
                    String attributeValue2 = this.a.getAttributeValue("http://schemas.android.com/apk/res/android", "versionCodeMajor");
                    if (attributeValue != null) {
                        try {
                            int parseInt = Integer.parseInt(attributeValue);
                            if (attributeValue2 == null) {
                                return parseInt;
                            }
                            try {
                                return (Integer.parseInt(attributeValue2) << 32) | (parseInt & 4294967295L);
                            } catch (NumberFormatException e) {
                                throw new XmlPullParserException(String.format("Couldn't parse versionCodeMajor to int: %s", e.getMessage()));
                            }
                        } catch (NumberFormatException e2) {
                            throw new XmlPullParserException(String.format("Couldn't parse versionCode to int: %s", e2.getMessage()));
                        }
                    }
                    throw new XmlPullParserException("Manifest entry doesn't contain 'versionCode' attribute.");
                }
            }
            throw new XmlPullParserException("Couldn't find manifest entry at top-level.");
        }
        throw new XmlPullParserException("Manifest file needs to be loaded before parsing.");
    }
}
