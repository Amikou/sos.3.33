package com.google.android.play.core.internal;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;

/* loaded from: classes2.dex */
public final class o extends m implements p {
    public o(IBinder iBinder) {
        super(iBinder, "com.google.android.play.core.assetpacks.protocol.IAssetModuleService");
    }

    @Override // com.google.android.play.core.internal.p
    public final void A(String str, Bundle bundle, Bundle bundle2, r rVar) throws RemoteException {
        Parcel b = b();
        b.writeString(str);
        ay4.b(b, bundle);
        ay4.b(b, bundle2);
        ay4.c(b, rVar);
        F1(9, b);
    }

    @Override // com.google.android.play.core.internal.p
    public final void U(String str, Bundle bundle, r rVar) throws RemoteException {
        Parcel b = b();
        b.writeString(str);
        ay4.b(b, bundle);
        ay4.c(b, rVar);
        F1(10, b);
    }

    @Override // com.google.android.play.core.internal.p
    public final void V(String str, Bundle bundle, Bundle bundle2, r rVar) throws RemoteException {
        Parcel b = b();
        b.writeString(str);
        ay4.b(b, bundle);
        ay4.b(b, bundle2);
        ay4.c(b, rVar);
        F1(11, b);
    }

    @Override // com.google.android.play.core.internal.p
    public final void h1(String str, Bundle bundle, r rVar) throws RemoteException {
        Parcel b = b();
        b.writeString(str);
        ay4.b(b, bundle);
        ay4.c(b, rVar);
        F1(5, b);
    }

    @Override // com.google.android.play.core.internal.p
    public final void n(String str, Bundle bundle, Bundle bundle2, r rVar) throws RemoteException {
        Parcel b = b();
        b.writeString(str);
        ay4.b(b, bundle);
        ay4.b(b, bundle2);
        ay4.c(b, rVar);
        F1(7, b);
    }

    @Override // com.google.android.play.core.internal.p
    public final void o1(String str, Bundle bundle, Bundle bundle2, r rVar) throws RemoteException {
        Parcel b = b();
        b.writeString(str);
        ay4.b(b, bundle);
        ay4.b(b, bundle2);
        ay4.c(b, rVar);
        F1(6, b);
    }

    @Override // com.google.android.play.core.internal.p
    public final void p(String str, List<Bundle> list, Bundle bundle, r rVar) throws RemoteException {
        Parcel b = b();
        b.writeString(str);
        b.writeTypedList(list);
        ay4.b(b, bundle);
        ay4.c(b, rVar);
        F1(14, b);
    }
}
