package com.google.android.play.core.internal;

import java.io.IOException;
import java.security.MessageDigest;

/* loaded from: classes2.dex */
public interface j {
    long a();

    void a(MessageDigest[] messageDigestArr, long j, int i) throws IOException;
}
