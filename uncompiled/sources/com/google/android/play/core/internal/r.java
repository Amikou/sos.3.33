package com.google.android.play.core.internal;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;
import java.util.List;

/* loaded from: classes2.dex */
public interface r extends IInterface {
    void C0() throws RemoteException;

    void C1() throws RemoteException;

    void I(Bundle bundle) throws RemoteException;

    void J(int i) throws RemoteException;

    void L0(Bundle bundle) throws RemoteException;

    void Q0(Bundle bundle, Bundle bundle2) throws RemoteException;

    void Z0(Bundle bundle) throws RemoteException;

    void a1(Bundle bundle, Bundle bundle2) throws RemoteException;

    void i1(List<Bundle> list) throws RemoteException;

    void m0(int i) throws RemoteException;

    void p1(Bundle bundle, Bundle bundle2) throws RemoteException;

    void r0(Bundle bundle) throws RemoteException;

    void u0(int i, Bundle bundle) throws RemoteException;
}
