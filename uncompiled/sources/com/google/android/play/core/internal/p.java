package com.google.android.play.core.internal;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;
import java.util.List;

/* loaded from: classes2.dex */
public interface p extends IInterface {
    void A(String str, Bundle bundle, Bundle bundle2, r rVar) throws RemoteException;

    void U(String str, Bundle bundle, r rVar) throws RemoteException;

    void V(String str, Bundle bundle, Bundle bundle2, r rVar) throws RemoteException;

    void h1(String str, Bundle bundle, r rVar) throws RemoteException;

    void n(String str, Bundle bundle, Bundle bundle2, r rVar) throws RemoteException;

    void o1(String str, Bundle bundle, Bundle bundle2, r rVar) throws RemoteException;

    void p(String str, List<Bundle> list, Bundle bundle, r rVar) throws RemoteException;
}
