package com.google.android.play.core.internal;

/* loaded from: classes2.dex */
public final class bi extends RuntimeException {
    public bi(String str) {
        super(str);
    }

    public bi(String str, Throwable th) {
        super(str, th);
    }
}
