package com.google.android.play.core.internal;

import java.io.IOException;
import java.io.InputStream;

/* loaded from: classes2.dex */
public final class i extends e {
    public final e a;
    public final long f0;
    public final long g0;

    public i(e eVar, long j, long j2) {
        this.a = eVar;
        long d = d(j);
        this.f0 = d;
        this.g0 = d(d + j2);
    }

    @Override // com.google.android.play.core.internal.e
    public final long a() {
        return this.g0 - this.f0;
    }

    @Override // com.google.android.play.core.internal.e
    public final InputStream b(long j, long j2) throws IOException {
        long d = d(this.f0);
        return this.a.b(d, d(j2 + d) - d);
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public final void close() throws IOException {
    }

    public final long d(long j) {
        if (j < 0) {
            return 0L;
        }
        return j > this.a.a() ? this.a.a() : j;
    }
}
