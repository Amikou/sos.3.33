package com.google.android.play.core.internal;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;

/* loaded from: classes2.dex */
public interface v extends IInterface {
    void B0(Bundle bundle, Bundle bundle2) throws RemoteException;

    void E0(Bundle bundle) throws RemoteException;

    void T0(Bundle bundle) throws RemoteException;
}
