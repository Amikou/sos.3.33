package com.google.android.play.core.internal;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

/* loaded from: classes2.dex */
public final class u extends m implements v {
    public u(IBinder iBinder) {
        super(iBinder, "com.google.android.play.core.assetpacks.protocol.IAssetPackExtractionServiceCallback");
    }

    @Override // com.google.android.play.core.internal.v
    public final void B0(Bundle bundle, Bundle bundle2) throws RemoteException {
        Parcel b = b();
        ay4.b(b, bundle);
        ay4.b(b, bundle2);
        F1(2, b);
    }

    @Override // com.google.android.play.core.internal.v
    public final void E0(Bundle bundle) throws RemoteException {
        Parcel b = b();
        ay4.b(b, bundle);
        F1(4, b);
    }

    @Override // com.google.android.play.core.internal.v
    public final void T0(Bundle bundle) throws RemoteException {
        Parcel b = b();
        ay4.b(b, bundle);
        F1(3, b);
    }
}
