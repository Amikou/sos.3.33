package com.google.android.play.core.internal;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.AssetManager;
import android.os.Build;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import org.xmlpull.v1.XmlPullParserException;

/* loaded from: classes2.dex */
public final class b {
    public final com.google.android.play.core.splitcompat.b a;
    public final Context b;
    public final a c;
    public PackageInfo d;

    public b(Context context, com.google.android.play.core.splitcompat.b bVar, gu4 gu4Var, byte[] bArr) {
        a aVar = new a(new at4(bVar));
        this.a = bVar;
        this.b = context;
        this.c = aVar;
    }

    public static X509Certificate e(Signature signature) {
        try {
            return (X509Certificate) CertificateFactory.getInstance("X509").generateCertificate(new ByteArrayInputStream(signature.toByteArray()));
        } catch (CertificateException unused) {
            return null;
        }
    }

    public final boolean a(File[] fileArr) {
        String absolutePath;
        StringBuilder sb;
        PackageInfo d = d();
        ArrayList<X509Certificate> arrayList = null;
        if (d != null && d.signatures != null) {
            arrayList = new ArrayList();
            for (Signature signature : d.signatures) {
                X509Certificate e = e(signature);
                if (e != null) {
                    arrayList.add(e);
                }
            }
        }
        if (arrayList == null || arrayList.isEmpty()) {
            return false;
        }
        int length = fileArr.length;
        loop1: while (true) {
            length--;
            if (length < 0) {
                return true;
            }
            try {
                absolutePath = fileArr[length].getAbsolutePath();
                try {
                    X509Certificate[][] g = l.g(absolutePath);
                    if (g == null || g.length == 0 || g[0].length == 0) {
                        break;
                    } else if (arrayList.isEmpty()) {
                        break;
                    } else {
                        for (X509Certificate x509Certificate : arrayList) {
                            for (X509Certificate[] x509CertificateArr : g) {
                                int i = x509CertificateArr[0].equals(x509Certificate) ? 0 : i + 1;
                            }
                        }
                    }
                } catch (Exception unused) {
                    sb = new StringBuilder(String.valueOf(absolutePath).length() + 32);
                    sb.append("Downloaded split ");
                }
            } catch (Exception unused2) {
            }
        }
        return false;
        sb.append(absolutePath);
        sb.append(" is not signed.");
        return false;
    }

    public final boolean b(File[] fileArr) throws IOException, XmlPullParserException {
        PackageInfo d;
        long longVersionCode = Build.VERSION.SDK_INT >= 28 ? d().getLongVersionCode() : d.versionCode;
        AssetManager assetManager = (AssetManager) d.c(AssetManager.class);
        int length = fileArr.length;
        do {
            length--;
            if (length < 0) {
                return true;
            }
            this.c.a(assetManager, fileArr[length]);
        } while (longVersionCode == this.c.b());
        return false;
    }

    public final boolean c(List<Intent> list) throws IOException {
        for (Intent intent : list) {
            if (!this.a.c(intent.getStringExtra("split_id")).exists()) {
                return false;
            }
        }
        return true;
    }

    public final PackageInfo d() {
        if (this.d == null) {
            try {
                this.d = this.b.getPackageManager().getPackageInfo(this.b.getPackageName(), 64);
            } catch (PackageManager.NameNotFoundException unused) {
                return null;
            }
        }
        return this.d;
    }
}
