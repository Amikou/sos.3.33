package com.google.android.play.core.internal;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

/* loaded from: classes2.dex */
public abstract class s extends n implements t {
    public s() {
        super("com.google.android.play.core.assetpacks.protocol.IAssetPackExtractionService");
    }

    @Override // com.google.android.play.core.internal.n
    public final boolean b(int i, Parcel parcel) throws RemoteException {
        v vVar = null;
        if (i == 2) {
            Bundle bundle = (Bundle) ay4.a(parcel, Bundle.CREATOR);
            IBinder readStrongBinder = parcel.readStrongBinder();
            if (readStrongBinder != null) {
                IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.play.core.assetpacks.protocol.IAssetPackExtractionServiceCallback");
                vVar = queryLocalInterface instanceof v ? (v) queryLocalInterface : new u(readStrongBinder);
            }
            b0(bundle, vVar);
            return true;
        } else if (i != 3) {
            return false;
        } else {
            Bundle bundle2 = (Bundle) ay4.a(parcel, Bundle.CREATOR);
            IBinder readStrongBinder2 = parcel.readStrongBinder();
            if (readStrongBinder2 != null) {
                IInterface queryLocalInterface2 = readStrongBinder2.queryLocalInterface("com.google.android.play.core.assetpacks.protocol.IAssetPackExtractionServiceCallback");
                vVar = queryLocalInterface2 instanceof v ? (v) queryLocalInterface2 : new u(readStrongBinder2);
            }
            m(vVar);
            return true;
        }
    }
}
