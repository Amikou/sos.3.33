package com.google.android.play.core.internal;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;

/* loaded from: classes2.dex */
public abstract class q extends n implements r {
    public q() {
        super("com.google.android.play.core.assetpacks.protocol.IAssetModuleServiceCallback");
    }

    @Override // com.google.android.play.core.internal.n
    public final boolean b(int i, Parcel parcel) throws RemoteException {
        switch (i) {
            case 2:
                u0(parcel.readInt(), (Bundle) ay4.a(parcel, Bundle.CREATOR));
                return true;
            case 3:
                int readInt = parcel.readInt();
                Bundle bundle = (Bundle) ay4.a(parcel, Bundle.CREATOR);
                J(readInt);
                return true;
            case 4:
                int readInt2 = parcel.readInt();
                Bundle bundle2 = (Bundle) ay4.a(parcel, Bundle.CREATOR);
                m0(readInt2);
                return true;
            case 5:
                i1(parcel.createTypedArrayList(Bundle.CREATOR));
                return true;
            case 6:
                Parcelable.Creator creator = Bundle.CREATOR;
                Bundle bundle3 = (Bundle) ay4.a(parcel, creator);
                I((Bundle) ay4.a(parcel, creator));
                return true;
            case 7:
                Z0((Bundle) ay4.a(parcel, Bundle.CREATOR));
                return true;
            case 8:
                Parcelable.Creator creator2 = Bundle.CREATOR;
                Bundle bundle4 = (Bundle) ay4.a(parcel, creator2);
                L0((Bundle) ay4.a(parcel, creator2));
                return true;
            case 9:
            default:
                return false;
            case 10:
                Parcelable.Creator creator3 = Bundle.CREATOR;
                Bundle bundle5 = (Bundle) ay4.a(parcel, creator3);
                r0((Bundle) ay4.a(parcel, creator3));
                return true;
            case 11:
                Parcelable.Creator creator4 = Bundle.CREATOR;
                Q0((Bundle) ay4.a(parcel, creator4), (Bundle) ay4.a(parcel, creator4));
                return true;
            case 12:
                Parcelable.Creator creator5 = Bundle.CREATOR;
                a1((Bundle) ay4.a(parcel, creator5), (Bundle) ay4.a(parcel, creator5));
                return true;
            case 13:
                Parcelable.Creator creator6 = Bundle.CREATOR;
                p1((Bundle) ay4.a(parcel, creator6), (Bundle) ay4.a(parcel, creator6));
                return true;
            case 14:
                Parcelable.Creator creator7 = Bundle.CREATOR;
                Bundle bundle6 = (Bundle) ay4.a(parcel, creator7);
                Bundle bundle7 = (Bundle) ay4.a(parcel, creator7);
                C0();
                return true;
            case 15:
                Bundle bundle8 = (Bundle) ay4.a(parcel, Bundle.CREATOR);
                C1();
                return true;
        }
    }
}
