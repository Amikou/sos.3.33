package com.google.android.play.core.internal;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;

/* loaded from: classes2.dex */
public interface t extends IInterface {
    void b0(Bundle bundle, v vVar) throws RemoteException;

    void m(v vVar) throws RemoteException;
}
