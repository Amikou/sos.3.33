package com.google.android.play.core.missingsplits;

import android.app.Application;

@Deprecated
/* loaded from: classes2.dex */
public class MissingSplitsDetectingApplication extends Application {
    public boolean a = false;

    @Deprecated
    public void a() {
    }

    @Override // android.app.Application
    public final void onCreate() {
        if (this.a) {
            throw new IllegalStateException("The onCreate method must be invoked at most once.");
        }
        this.a = true;
        if (c92.a(this).a()) {
            return;
        }
        super.onCreate();
        a();
    }
}
