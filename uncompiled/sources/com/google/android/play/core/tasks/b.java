package com.google.android.play.core.tasks;

import java.util.concurrent.CountDownLatch;

/* loaded from: classes2.dex */
public final class b implements tm2, mm2 {
    public final CountDownLatch a = new CountDownLatch(1);

    public /* synthetic */ b(byte[] bArr) {
    }

    @Override // defpackage.tm2
    public final void a(Object obj) {
        this.a.countDown();
    }

    @Override // defpackage.mm2
    public final void b(Exception exc) {
        this.a.countDown();
    }

    public final void c() throws InterruptedException {
        this.a.await();
    }
}
