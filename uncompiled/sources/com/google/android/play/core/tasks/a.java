package com.google.android.play.core.tasks;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;

/* loaded from: classes2.dex */
public final class a {
    public static <ResultT> l34<ResultT> a(ResultT resultt) {
        fy4 fy4Var = new fy4();
        fy4Var.g(resultt);
        return fy4Var;
    }

    public static <ResultT> ResultT b(l34<ResultT> l34Var) throws ExecutionException, InterruptedException {
        gu4.d(l34Var, "Task must not be null");
        if (l34Var.e()) {
            return (ResultT) d(l34Var);
        }
        b bVar = new b(null);
        e(l34Var, bVar);
        bVar.c();
        return (ResultT) d(l34Var);
    }

    public static <ResultT> l34<ResultT> c(Exception exc) {
        fy4 fy4Var = new fy4();
        fy4Var.i(exc);
        return fy4Var;
    }

    public static <ResultT> ResultT d(l34<ResultT> l34Var) throws ExecutionException {
        if (l34Var.f()) {
            return l34Var.d();
        }
        throw new ExecutionException(l34Var.c());
    }

    public static void e(l34<?> l34Var, b bVar) {
        Executor executor = t34.a;
        l34Var.b(executor, bVar);
        l34Var.a(executor, bVar);
    }
}
