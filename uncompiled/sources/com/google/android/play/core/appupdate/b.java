package com.google.android.play.core.appupdate;

import android.os.Bundle;
import android.os.ResultReceiver;

/* loaded from: classes2.dex */
final class b extends ResultReceiver {
    public final /* synthetic */ tx4 a;

    @Override // android.os.ResultReceiver
    public final void onReceiveResult(int i, Bundle bundle) {
        tx4 tx4Var;
        int i2 = 1;
        if (i == 1) {
            tx4Var = this.a;
            i2 = -1;
        } else if (i != 2) {
            tx4Var = this.a;
        } else {
            tx4Var = this.a;
            i2 = 0;
        }
        tx4Var.e(Integer.valueOf(i2));
    }
}
