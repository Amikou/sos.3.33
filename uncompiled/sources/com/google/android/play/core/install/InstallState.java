package com.google.android.play.core.install;

/* loaded from: classes2.dex */
public abstract class InstallState {
    public static InstallState a(int i, long j, long j2, int i2, String str) {
        return new xs4(i, j, j2, i2, str);
    }

    public abstract long b();

    public abstract int c();

    public abstract int d();

    public abstract String e();

    public abstract long f();
}
