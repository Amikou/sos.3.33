package com.google.android.play.core.splitcompat;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/* loaded from: classes2.dex */
public final class f implements h {
    public final /* synthetic */ Set a;
    public final /* synthetic */ sy4 b;
    public final /* synthetic */ ZipFile c;

    public f(Set set, sy4 sy4Var, ZipFile zipFile) {
        this.a = set;
        this.b = sy4Var;
        this.c = zipFile;
    }

    @Override // com.google.android.play.core.splitcompat.h
    public final void a(vx4 vx4Var, File file, boolean z) throws IOException {
        this.a.add(file);
        if (z) {
            return;
        }
        String.format("NativeLibraryExtractor: split '%s' has native library '%s' that does not exist; extracting from '%s!%s' to '%s'", this.b.b(), vx4Var.a, this.b.a().getAbsolutePath(), vx4Var.b.getName(), file.getAbsolutePath());
        ZipFile zipFile = this.c;
        ZipEntry zipEntry = vx4Var.b;
        int i = i.c;
        byte[] bArr = new byte[4096];
        InputStream inputStream = zipFile.getInputStream(zipEntry);
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            while (true) {
                int read = inputStream.read(bArr);
                if (read <= 0) {
                    fileOutputStream.close();
                    inputStream.close();
                    return;
                }
                fileOutputStream.write(bArr, 0, read);
            }
        } catch (Throwable th) {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (Throwable th2) {
                    yv4.a(th, th2);
                }
            }
            throw th;
        }
    }
}
