package com.google.android.play.core.splitcompat;

import java.io.IOException;
import java.util.Set;
import java.util.zip.ZipFile;

/* loaded from: classes2.dex */
public final class e implements g {
    public final /* synthetic */ Set a;
    public final /* synthetic */ sy4 b;
    public final /* synthetic */ i c;

    public e(i iVar, Set set, sy4 sy4Var) {
        this.c = iVar;
        this.a = set;
        this.b = sy4Var;
    }

    @Override // com.google.android.play.core.splitcompat.g
    public final void a(ZipFile zipFile, Set<vx4> set) throws IOException {
        this.a.addAll(i.d(this.c, set, this.b, zipFile));
    }
}
