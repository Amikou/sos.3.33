package com.google.android.play.core.splitcompat;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import com.google.android.play.core.internal.bf;
import com.google.android.play.core.splitinstall.l;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/* loaded from: classes2.dex */
public class a {
    public static final AtomicReference<a> d = new AtomicReference<>(null);
    public final b a;
    public final Set<String> b = new HashSet();
    public final at4 c;

    public a(Context context) {
        try {
            b bVar = new b(context);
            this.a = bVar;
            this.c = new at4(bVar);
        } catch (PackageManager.NameNotFoundException e) {
            throw new bf(e);
        }
    }

    public static boolean a(Context context) {
        return e(context, true);
    }

    public static boolean b() {
        return d.get() != null;
    }

    public static boolean d() {
        return Build.VERSION.SDK_INT < 21;
    }

    public static boolean e(Context context, boolean z) {
        if (d()) {
            return false;
        }
        AtomicReference<a> atomicReference = d;
        boolean compareAndSet = atomicReference.compareAndSet(null, new a(context));
        a aVar = atomicReference.get();
        if (compareAndSet) {
            l.a.b(new eu4(context, ny4.a(), new com.google.android.play.core.internal.b(context, aVar.a, new gu4(), null), aVar.a, new ny4()));
            ly4.a(new by4(aVar));
            ny4.a().execute(new ey4(context));
        }
        try {
            aVar.f(context, z);
            return true;
        } catch (Exception unused) {
            return false;
        }
    }

    public static boolean g(Context context) {
        return e(context, false);
    }

    public final synchronized void f(Context context, boolean z) throws IOException {
        ZipFile zipFile;
        if (z) {
            this.a.a();
        } else {
            ny4.a().execute(new hy4(this));
        }
        String packageName = context.getPackageName();
        try {
            String[] strArr = context.getPackageManager().getPackageInfo(packageName, 0).splitNames;
            List<String> arrayList = strArr == null ? new ArrayList() : Arrays.asList(strArr);
            Set<sy4> i = this.a.i();
            HashSet hashSet = new HashSet();
            Iterator<sy4> it = i.iterator();
            while (it.hasNext()) {
                String b = it.next().b();
                if (arrayList.contains(b)) {
                    if (z) {
                        this.a.n(b);
                    } else {
                        hashSet.add(b);
                    }
                    it.remove();
                }
            }
            if (!hashSet.isEmpty()) {
                ny4.a().execute(new ky4(this, hashSet));
            }
            HashSet hashSet2 = new HashSet();
            for (sy4 sy4Var : i) {
                String b2 = sy4Var.b();
                if (!oy4.b(b2)) {
                    hashSet2.add(b2);
                }
            }
            for (String str : arrayList) {
                if (!oy4.b(str)) {
                    hashSet2.add(str);
                }
            }
            HashSet<sy4> hashSet3 = new HashSet(i.size());
            for (sy4 sy4Var2 : i) {
                if (!oy4.a(sy4Var2.b())) {
                    String b3 = sy4Var2.b();
                    if (hashSet2.contains(oy4.a(b3) ? "" : b3.split("\\.config\\.", 2)[0])) {
                    }
                }
                hashSet3.add(sy4Var2);
            }
            i iVar = new i(this.a);
            fu4 a = gu4.a();
            ClassLoader classLoader = context.getClassLoader();
            if (z) {
                a.b(classLoader, iVar.a());
            } else {
                Iterator it2 = hashSet3.iterator();
                while (it2.hasNext()) {
                    Set<File> b4 = iVar.b((sy4) it2.next());
                    if (b4 == null) {
                        it2.remove();
                    } else {
                        a.b(classLoader, b4);
                    }
                }
            }
            HashSet hashSet4 = new HashSet();
            for (sy4 sy4Var3 : hashSet3) {
                try {
                    zipFile = new ZipFile(sy4Var3.a());
                    try {
                        ZipEntry entry = zipFile.getEntry("classes.dex");
                        zipFile.close();
                        if (entry != null && !a.a(classLoader, this.a.h(sy4Var3.b()), sy4Var3.a(), z)) {
                            String valueOf = String.valueOf(sy4Var3.a());
                            StringBuilder sb = new StringBuilder(valueOf.length() + 24);
                            sb.append("split was not installed ");
                            sb.append(valueOf);
                        }
                        hashSet4.add(sy4Var3.a());
                    } catch (IOException e) {
                        e = e;
                        if (zipFile != null) {
                            try {
                                zipFile.close();
                            } catch (IOException e2) {
                                yv4.a(e, e2);
                            }
                        }
                        throw e;
                    }
                } catch (IOException e3) {
                    e = e3;
                    zipFile = null;
                }
            }
            this.c.a(context, hashSet4);
            HashSet hashSet5 = new HashSet();
            for (sy4 sy4Var4 : hashSet3) {
                if (hashSet4.contains(sy4Var4.a())) {
                    String b5 = sy4Var4.b();
                    StringBuilder sb2 = new StringBuilder(b5.length() + 30);
                    sb2.append("Split '");
                    sb2.append(b5);
                    sb2.append("' installation emulated");
                    hashSet5.add(sy4Var4.b());
                } else {
                    String b6 = sy4Var4.b();
                    StringBuilder sb3 = new StringBuilder(b6.length() + 35);
                    sb3.append("Split '");
                    sb3.append(b6);
                    sb3.append("' installation not emulated.");
                }
            }
            synchronized (this.b) {
                this.b.addAll(hashSet5);
            }
        } catch (PackageManager.NameNotFoundException e4) {
            throw new IOException(String.format("Cannot load data for application '%s'", packageName), e4);
        }
    }
}
