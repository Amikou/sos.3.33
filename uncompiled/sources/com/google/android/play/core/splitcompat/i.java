package com.google.android.play.core.splitcompat;

import android.os.Build;
import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/* loaded from: classes2.dex */
public final class i {
    public static final Pattern b = Pattern.compile("lib/([^/]+)/(.*\\.so)$");
    public static final /* synthetic */ int c = 0;
    public final b a;

    public i(b bVar) throws IOException {
        this.a = bVar;
    }

    public static /* synthetic */ Set d(i iVar, Set set, sy4 sy4Var, ZipFile zipFile) throws IOException {
        HashSet hashSet = new HashSet();
        iVar.f(sy4Var, set, new f(hashSet, sy4Var, zipFile));
        return hashSet;
    }

    public static void e(sy4 sy4Var, g gVar) throws IOException {
        ZipFile zipFile;
        String[] strArr;
        try {
            zipFile = new ZipFile(sy4Var.a());
            try {
                String b2 = sy4Var.b();
                HashMap hashMap = new HashMap();
                Enumeration<? extends ZipEntry> entries = zipFile.entries();
                while (entries.hasMoreElements()) {
                    ZipEntry nextElement = entries.nextElement();
                    Matcher matcher = b.matcher(nextElement.getName());
                    if (matcher.matches()) {
                        String group = matcher.group(1);
                        String group2 = matcher.group(2);
                        String.format("NativeLibraryExtractor: split '%s' has native library '%s' for ABI '%s'", b2, group2, group);
                        Set set = (Set) hashMap.get(group);
                        if (set == null) {
                            set = new HashSet();
                            hashMap.put(group, set);
                        }
                        set.add(new vx4(nextElement, group2));
                    }
                }
                HashMap hashMap2 = new HashMap();
                for (String str : Build.SUPPORTED_ABIS) {
                    if (hashMap.containsKey(str)) {
                        String.format("NativeLibraryExtractor: there are native libraries for supported ABI %s; will use this ABI", str);
                        for (vx4 vx4Var : (Set) hashMap.get(str)) {
                            if (hashMap2.containsKey(vx4Var.a)) {
                                String.format("NativeLibraryExtractor: skipping library %s for ABI %s; already present for a better ABI", vx4Var.a, str);
                            } else {
                                hashMap2.put(vx4Var.a, vx4Var);
                                String.format("NativeLibraryExtractor: using library %s for ABI %s", vx4Var.a, str);
                            }
                        }
                    } else {
                        String.format("NativeLibraryExtractor: there are no native libraries for supported ABI %s", str);
                    }
                }
                gVar.a(zipFile, new HashSet(hashMap2.values()));
                zipFile.close();
            } catch (IOException e) {
                e = e;
                if (zipFile != null) {
                    try {
                        zipFile.close();
                    } catch (IOException e2) {
                        yv4.a(e, e2);
                    }
                }
                throw e;
            }
        } catch (IOException e3) {
            e = e3;
            zipFile = null;
        }
    }

    public final Set<File> a() throws IOException {
        Set<sy4> i = this.a.i();
        for (String str : this.a.j()) {
            Iterator<sy4> it = i.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (it.next().b().equals(str)) {
                        break;
                    }
                } else {
                    String.format("NativeLibraryExtractor: extracted split '%s' has no corresponding split; deleting", str);
                    this.a.k(str);
                    break;
                }
            }
        }
        HashSet hashSet = new HashSet();
        for (sy4 sy4Var : i) {
            HashSet hashSet2 = new HashSet();
            e(sy4Var, new e(this, hashSet2, sy4Var));
            for (File file : this.a.m(sy4Var.b())) {
                if (!hashSet2.contains(file)) {
                    String.format("NativeLibraryExtractor: file '%s' found in split '%s' that is not in the split file '%s'; removing", file.getAbsolutePath(), sy4Var.b(), sy4Var.a().getAbsolutePath());
                    this.a.l(file);
                }
            }
            hashSet.addAll(hashSet2);
        }
        return hashSet;
    }

    public final Set<File> b(sy4 sy4Var) throws IOException {
        AtomicBoolean atomicBoolean = new AtomicBoolean(true);
        HashSet hashSet = new HashSet();
        e(sy4Var, new d(this, sy4Var, hashSet, atomicBoolean));
        if (atomicBoolean.get()) {
            return hashSet;
        }
        return null;
    }

    public final void f(sy4 sy4Var, Set<vx4> set, h hVar) throws IOException {
        for (vx4 vx4Var : set) {
            File e = this.a.e(sy4Var.b(), vx4Var.a);
            boolean z = false;
            if (e.exists() && e.length() == vx4Var.b.getSize()) {
                z = true;
            }
            hVar.a(vx4Var, e, z);
        }
    }
}
