package com.google.android.play.core.splitcompat;

import java.io.File;
import java.io.IOException;

/* loaded from: classes2.dex */
public final class c implements h {
    public final /* synthetic */ d a;

    public c(d dVar) {
        this.a = dVar;
    }

    @Override // com.google.android.play.core.splitcompat.h
    public final void a(vx4 vx4Var, File file, boolean z) throws IOException {
        this.a.b.add(file);
        if (z) {
            return;
        }
        this.a.c.set(false);
    }
}
