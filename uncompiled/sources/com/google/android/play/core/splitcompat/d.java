package com.google.android.play.core.splitcompat;

import java.io.IOException;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.zip.ZipFile;

/* loaded from: classes2.dex */
public final class d implements g {
    public final /* synthetic */ sy4 a;
    public final /* synthetic */ Set b;
    public final /* synthetic */ AtomicBoolean c;
    public final /* synthetic */ i d;

    public d(i iVar, sy4 sy4Var, Set set, AtomicBoolean atomicBoolean) {
        this.d = iVar;
        this.a = sy4Var;
        this.b = set;
        this.c = atomicBoolean;
    }

    @Override // com.google.android.play.core.splitcompat.g
    public final void a(ZipFile zipFile, Set<vx4> set) throws IOException {
        this.d.f(this.a, set, new c(this));
    }
}
