package com.google.android.play.core.splitinstall;

import java.util.concurrent.atomic.AtomicReference;

/* JADX WARN: $VALUES field not found */
/* JADX WARN: Failed to restore enum class, 'enum' modifier and super class removed */
/* loaded from: classes2.dex */
public final class l implements ix4 {
    public static final l a = new l();
    public static final AtomicReference<lx4> f0 = new AtomicReference<>(null);

    @Override // defpackage.ix4
    public final lx4 a() {
        return f0.get();
    }

    public final void b(lx4 lx4Var) {
        f0.set(lx4Var);
    }
}
