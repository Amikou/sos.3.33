package com.google.android.play.core.assetpacks;

/* loaded from: classes2.dex */
public final class bj extends RuntimeException {
    public final int a;

    public bj(String str) {
        super(str);
        this.a = -1;
    }

    public bj(String str, int i) {
        super(str);
        this.a = i;
    }

    public bj(String str, Exception exc) {
        super(str, exc);
        this.a = -1;
    }

    public bj(String str, Exception exc, int i) {
        super(str, exc);
        this.a = i;
    }
}
