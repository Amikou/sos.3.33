package com.google.android.play.core.assetpacks;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/* loaded from: classes2.dex */
public final class i extends OutputStream {
    public final nw4 a = new nw4();
    public final File f0;
    public final m g0;
    public long h0;
    public long i0;
    public FileOutputStream j0;
    public gx4 k0;

    public i(File file, m mVar) {
        this.f0 = file;
        this.g0 = mVar;
    }

    @Override // java.io.OutputStream
    public final void write(int i) throws IOException {
        write(new byte[]{(byte) i});
    }

    @Override // java.io.OutputStream
    public final void write(byte[] bArr) throws IOException {
        write(bArr, 0, bArr.length);
    }

    @Override // java.io.OutputStream
    public final void write(byte[] bArr, int i, int i2) throws IOException {
        int min;
        while (i2 > 0) {
            if (this.h0 == 0 && this.i0 == 0) {
                int a = this.a.a(bArr, i, i2);
                if (a == -1) {
                    return;
                }
                i += a;
                i2 -= a;
                gx4 b = this.a.b();
                this.k0 = b;
                if (b.h()) {
                    this.h0 = 0L;
                    this.g0.m(this.k0.i(), this.k0.i().length);
                    this.i0 = this.k0.i().length;
                } else if (!this.k0.c() || this.k0.b()) {
                    byte[] i3 = this.k0.i();
                    this.g0.m(i3, i3.length);
                    this.h0 = this.k0.e();
                } else {
                    this.g0.g(this.k0.i());
                    File file = new File(this.f0, this.k0.d());
                    file.getParentFile().mkdirs();
                    this.h0 = this.k0.e();
                    this.j0 = new FileOutputStream(file);
                }
            }
            if (!this.k0.b()) {
                if (this.k0.h()) {
                    this.g0.i(this.i0, bArr, i, i2);
                    this.i0 += i2;
                    min = i2;
                } else if (this.k0.c()) {
                    min = (int) Math.min(i2, this.h0);
                    this.j0.write(bArr, i, min);
                    long j = this.h0 - min;
                    this.h0 = j;
                    if (j == 0) {
                        this.j0.close();
                    }
                } else {
                    min = (int) Math.min(i2, this.h0);
                    this.g0.i((this.k0.i().length + this.k0.e()) - this.h0, bArr, i, min);
                    this.h0 -= min;
                }
                i += min;
                i2 -= min;
            }
        }
    }
}
