package com.google.android.play.core.assetpacks;

import android.os.Bundle;

/* loaded from: classes2.dex */
public abstract class AssetPackState {
    public static AssetPackState b(String str, int i, int i2, long j, long j2, double d, int i3) {
        return new ju4(str, i, i2, j, j2, (int) Math.rint(100.0d * d), i3);
    }

    public static AssetPackState d(Bundle bundle, String str, fv4 fv4Var, bu4 bu4Var) {
        int a = bu4Var.a(bundle.getInt(com.google.android.play.core.internal.l.e("status", str)), str);
        int i = bundle.getInt(com.google.android.play.core.internal.l.e("error_code", str));
        long j = bundle.getLong(com.google.android.play.core.internal.l.e("bytes_downloaded", str));
        long j2 = bundle.getLong(com.google.android.play.core.internal.l.e("total_bytes_to_download", str));
        double b = fv4Var.b(str);
        long j3 = bundle.getLong(com.google.android.play.core.internal.l.e("pack_version", str));
        long j4 = bundle.getLong(com.google.android.play.core.internal.l.e("pack_base_version", str));
        int i2 = 1;
        if (a == 4 && j4 != 0 && j4 != j3) {
            i2 = 2;
        }
        return b(str, a, i, j, j2, b, i2);
    }

    public abstract int a();

    public abstract long c();

    public abstract int e();

    public abstract String f();

    public abstract int g();

    public abstract long h();

    public abstract int i();
}
