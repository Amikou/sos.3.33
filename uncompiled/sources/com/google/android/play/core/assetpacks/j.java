package com.google.android.play.core.assetpacks;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import com.google.android.play.core.assetpacks.j;
import com.google.android.play.core.common.LocalTestingException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicInteger;

/* loaded from: classes2.dex */
public final class j implements zy4 {
    public static final it4 g = new it4("FakeAssetPackService");
    public final String a;
    public final au4 b;
    public final Context c;
    public final ww4 d;
    public final cw4<Executor> e;
    public final Handler f = new Handler(Looper.getMainLooper());

    static {
        new AtomicInteger(1);
    }

    public j(File file, au4 au4Var, fv4 fv4Var, Context context, ww4 ww4Var, cw4<Executor> cw4Var) {
        this.a = file.getAbsolutePath();
        this.b = au4Var;
        this.c = context;
        this.d = ww4Var;
        this.e = cw4Var;
    }

    public static long h(int i, long j) {
        if (i != 2) {
            if (i == 3 || i == 4) {
                return j;
            }
            return 0L;
        }
        return j / 2;
    }

    public static String l(File file) throws LocalTestingException {
        try {
            return k.a(Arrays.asList(file));
        } catch (IOException e) {
            throw new LocalTestingException(String.format("Could not digest file: %s.", file), e);
        } catch (NoSuchAlgorithmException e2) {
            throw new LocalTestingException("SHA256 algorithm not supported.", e2);
        }
    }

    @Override // defpackage.zy4
    public final void a() {
        g.d("keepAlive", new Object[0]);
    }

    @Override // defpackage.zy4
    public final void b(int i) {
        g.d("notifySessionFailed", new Object[0]);
    }

    @Override // defpackage.zy4
    public final void c(int i, String str, String str2, int i2) {
        g.d("notifyChunkTransferred", new Object[0]);
    }

    @Override // defpackage.zy4
    public final void d(final int i, final String str) {
        g.d("notifyModuleCompleted", new Object[0]);
        this.e.a().execute(new Runnable(this, i, str) { // from class: kw4
            public final j a;
            public final int f0;
            public final String g0;

            {
                this.a = this;
                this.f0 = i;
                this.g0 = str;
            }

            @Override // java.lang.Runnable
            public final void run() {
                this.a.j(this.f0, this.g0);
            }
        });
    }

    @Override // defpackage.zy4
    public final l34<ParcelFileDescriptor> e(int i, String str, String str2, int i2) {
        File[] k;
        int i3;
        g.d("getChunkFileDescriptor(session=%d, %s, %s, %d)", Integer.valueOf(i), str, str2, Integer.valueOf(i2));
        tx4 tx4Var = new tx4();
        try {
        } catch (LocalTestingException e) {
            g.e("getChunkFileDescriptor failed", e);
            tx4Var.b(e);
        } catch (FileNotFoundException e2) {
            g.e("getChunkFileDescriptor failed", e2);
            tx4Var.b(new LocalTestingException("Asset Slice file not found.", e2));
        }
        for (File file : k(str)) {
            if (gu4.b(file).equals(str2)) {
                tx4Var.a(ParcelFileDescriptor.open(file, 268435456));
                return tx4Var.c();
            }
        }
        throw new LocalTestingException(String.format("Local testing slice for '%s' not found.", str2));
    }

    @Override // defpackage.zy4
    public final l34<List<String>> f(Map<String, Long> map) {
        g.d("syncPacks()", new Object[0]);
        return com.google.android.play.core.tasks.a.a(new ArrayList());
    }

    @Override // defpackage.zy4
    public final void g(List<String> list) {
        g.d("cancelDownload(%s)", list);
    }

    public final /* synthetic */ void i(Intent intent) {
        this.b.a(this.c, intent);
    }

    public final /* synthetic */ void j(int i, String str) {
        try {
            m(i, str, 4);
        } catch (LocalTestingException e) {
            g.e("notifyModuleCompleted failed", e);
        }
    }

    public final File[] k(final String str) throws LocalTestingException {
        File file = new File(this.a);
        if (file.isDirectory()) {
            File[] listFiles = file.listFiles(new FilenameFilter(str) { // from class: lw4
                public final String a;

                {
                    this.a = str;
                }

                @Override // java.io.FilenameFilter
                public final boolean accept(File file2, String str2) {
                    return str2.startsWith(String.valueOf(this.a).concat("-")) && str2.endsWith(".apk");
                }
            });
            if (listFiles != null) {
                if (listFiles.length != 0) {
                    for (File file2 : listFiles) {
                        if (gu4.b(file2).equals(str)) {
                            return listFiles;
                        }
                    }
                    throw new LocalTestingException(String.format("No master slice available for pack '%s'.", str));
                }
                throw new LocalTestingException(String.format("No APKs available for pack '%s'.", str));
            }
            throw new LocalTestingException(String.format("Failed fetching APKs for pack '%s'.", str));
        }
        throw new LocalTestingException(String.format("Local testing directory '%s' not found.", file));
    }

    public final void m(int i, String str, int i2) throws LocalTestingException {
        Bundle bundle = new Bundle();
        bundle.putInt("app_version_code", this.d.a());
        bundle.putInt("session_id", i);
        File[] k = k(str);
        ArrayList<String> arrayList = new ArrayList<>();
        long j = 0;
        for (File file : k) {
            j += file.length();
            ArrayList<? extends Parcelable> arrayList2 = new ArrayList<>();
            arrayList2.add(i2 == 3 ? new Intent().setData(Uri.EMPTY) : null);
            String b = gu4.b(file);
            bundle.putParcelableArrayList(com.google.android.play.core.internal.l.f("chunk_intents", str, b), arrayList2);
            bundle.putString(com.google.android.play.core.internal.l.f("uncompressed_hash_sha256", str, b), l(file));
            bundle.putLong(com.google.android.play.core.internal.l.f("uncompressed_size", str, b), file.length());
            arrayList.add(b);
        }
        bundle.putStringArrayList(com.google.android.play.core.internal.l.e("slice_ids", str), arrayList);
        bundle.putLong(com.google.android.play.core.internal.l.e("pack_version", str), this.d.a());
        bundle.putInt(com.google.android.play.core.internal.l.e("status", str), i2);
        bundle.putInt(com.google.android.play.core.internal.l.e("error_code", str), 0);
        bundle.putLong(com.google.android.play.core.internal.l.e("bytes_downloaded", str), h(i2, j));
        bundle.putLong(com.google.android.play.core.internal.l.e("total_bytes_to_download", str), j);
        bundle.putStringArrayList("pack_names", new ArrayList<>(Arrays.asList(str)));
        bundle.putLong("bytes_downloaded", h(i2, j));
        bundle.putLong("total_bytes_to_download", j);
        final Intent putExtra = new Intent("com.google.android.play.core.assetpacks.receiver.ACTION_SESSION_UPDATE").putExtra("com.google.android.play.core.assetpacks.receiver.EXTRA_SESSION_STATE", bundle);
        this.f.post(new Runnable(this, putExtra) { // from class: mw4
            public final j a;
            public final Intent f0;

            {
                this.a = this;
                this.f0 = putExtra;
            }

            @Override // java.lang.Runnable
            public final void run() {
                this.a.i(this.f0);
            }
        });
    }
}
