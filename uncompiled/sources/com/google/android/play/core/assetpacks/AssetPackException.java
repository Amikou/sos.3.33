package com.google.android.play.core.assetpacks;

/* loaded from: classes2.dex */
public class AssetPackException extends com.google.android.play.core.tasks.j {
    private final int a;

    public AssetPackException(int i) {
        super(String.format("Asset Pack Download Error(%d): %s", Integer.valueOf(i), vs4.a(i)));
        if (i == 0) {
            throw new IllegalArgumentException("errorCode should not be 0.");
        }
        this.a = i;
    }

    @Override // com.google.android.play.core.tasks.j
    public int getErrorCode() {
        return this.a;
    }
}
