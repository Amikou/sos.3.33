package com.google.android.play.core.assetpacks;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Objects;

/* loaded from: classes2.dex */
public final class l extends InputStream {
    public final Enumeration<File> a;
    public InputStream f0;

    public l(Enumeration<File> enumeration) throws IOException {
        this.a = enumeration;
        a();
    }

    public final void a() throws IOException {
        InputStream inputStream = this.f0;
        if (inputStream != null) {
            inputStream.close();
        }
        this.f0 = this.a.hasMoreElements() ? new FileInputStream(this.a.nextElement()) : null;
    }

    @Override // java.io.InputStream, java.io.Closeable, java.lang.AutoCloseable
    public final void close() throws IOException {
        super.close();
        InputStream inputStream = this.f0;
        if (inputStream != null) {
            inputStream.close();
            this.f0 = null;
        }
    }

    @Override // java.io.InputStream
    public final int read() throws IOException {
        while (true) {
            InputStream inputStream = this.f0;
            if (inputStream == null) {
                return -1;
            }
            int read = inputStream.read();
            if (read != -1) {
                return read;
            }
            a();
        }
    }

    @Override // java.io.InputStream
    public final int read(byte[] bArr, int i, int i2) throws IOException {
        if (this.f0 == null) {
            return -1;
        }
        Objects.requireNonNull(bArr);
        if (i < 0 || i2 < 0 || i2 > bArr.length - i) {
            throw new IndexOutOfBoundsException();
        }
        if (i2 != 0) {
            do {
                int read = this.f0.read(bArr, i, i2);
                if (read > 0) {
                    return read;
                }
                a();
            } while (this.f0 != null);
            return -1;
        }
        return 0;
    }
}
