package com.google.android.play.core.assetpacks;

import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.play.core.internal.q;
import java.util.List;

/* loaded from: classes2.dex */
public class a<T> extends q {
    public final tx4<T> a;
    public final /* synthetic */ st4 b;

    public a(st4 st4Var, tx4<T> tx4Var) {
        this.b = st4Var;
        this.a = tx4Var;
    }

    public a(st4 st4Var, tx4 tx4Var, byte[] bArr) {
        this(st4Var, tx4Var);
    }

    public a(st4 st4Var, tx4 tx4Var, char[] cArr) {
        this(st4Var, tx4Var);
    }

    public a(st4 st4Var, tx4 tx4Var, int[] iArr) {
        this(st4Var, tx4Var);
    }

    @Override // com.google.android.play.core.internal.r
    public void C0() {
        zt4 zt4Var;
        it4 it4Var;
        zt4Var = this.b.c;
        zt4Var.b();
        it4Var = st4.f;
        it4Var.d("onRemoveModule()", new Object[0]);
    }

    @Override // com.google.android.play.core.internal.r
    public void C1() {
        zt4 zt4Var;
        it4 it4Var;
        zt4Var = this.b.c;
        zt4Var.b();
        it4Var = st4.f;
        it4Var.d("onCancelDownloads()", new Object[0]);
    }

    @Override // com.google.android.play.core.internal.r
    public void I(Bundle bundle) {
        zt4 zt4Var;
        it4 it4Var;
        zt4Var = this.b.c;
        zt4Var.b();
        it4Var = st4.f;
        it4Var.d("onNotifyChunkTransferred(%s, %s, %d, session=%d)", bundle.getString("module_name"), bundle.getString("slice_id"), Integer.valueOf(bundle.getInt("chunk_number")), Integer.valueOf(bundle.getInt("session_id")));
    }

    @Override // com.google.android.play.core.internal.r
    public final void J(int i) {
        zt4 zt4Var;
        it4 it4Var;
        zt4Var = this.b.c;
        zt4Var.b();
        it4Var = st4.f;
        it4Var.d("onCancelDownload(%d)", Integer.valueOf(i));
    }

    @Override // com.google.android.play.core.internal.r
    public void L0(Bundle bundle) {
        zt4 zt4Var;
        it4 it4Var;
        zt4Var = this.b.c;
        zt4Var.b();
        it4Var = st4.f;
        it4Var.d("onNotifyModuleCompleted(%s, sessionId=%d)", bundle.getString("module_name"), Integer.valueOf(bundle.getInt("session_id")));
    }

    @Override // com.google.android.play.core.internal.r
    public void Q0(Bundle bundle, Bundle bundle2) {
        zt4 zt4Var;
        it4 it4Var;
        zt4Var = this.b.d;
        zt4Var.b();
        it4Var = st4.f;
        it4Var.d("onKeepAlive(%b)", Boolean.valueOf(bundle.getBoolean("keep_alive")));
    }

    @Override // com.google.android.play.core.internal.r
    public void Z0(Bundle bundle) {
        zt4 zt4Var;
        it4 it4Var;
        zt4Var = this.b.c;
        zt4Var.b();
        int i = bundle.getInt("error_code");
        it4Var = st4.f;
        it4Var.b("onError(%d)", Integer.valueOf(i));
        this.a.d(new AssetPackException(i));
    }

    @Override // com.google.android.play.core.internal.r
    public void a1(Bundle bundle, Bundle bundle2) throws RemoteException {
        zt4 zt4Var;
        it4 it4Var;
        zt4Var = this.b.c;
        zt4Var.b();
        it4Var = st4.f;
        it4Var.d("onGetChunkFileDescriptor", new Object[0]);
    }

    @Override // com.google.android.play.core.internal.r
    public void i1(List<Bundle> list) {
        zt4 zt4Var;
        it4 it4Var;
        zt4Var = this.b.c;
        zt4Var.b();
        it4Var = st4.f;
        it4Var.d("onGetSessionStates", new Object[0]);
    }

    @Override // com.google.android.play.core.internal.r
    public final void m0(int i) {
        zt4 zt4Var;
        it4 it4Var;
        zt4Var = this.b.c;
        zt4Var.b();
        it4Var = st4.f;
        it4Var.d("onGetSession(%d)", Integer.valueOf(i));
    }

    @Override // com.google.android.play.core.internal.r
    public void p1(Bundle bundle, Bundle bundle2) {
        zt4 zt4Var;
        it4 it4Var;
        zt4Var = this.b.c;
        zt4Var.b();
        it4Var = st4.f;
        it4Var.d("onRequestDownloadInfo()", new Object[0]);
    }

    @Override // com.google.android.play.core.internal.r
    public void r0(Bundle bundle) {
        zt4 zt4Var;
        it4 it4Var;
        zt4Var = this.b.c;
        zt4Var.b();
        it4Var = st4.f;
        it4Var.d("onNotifySessionFailed(%d)", Integer.valueOf(bundle.getInt("session_id")));
    }

    @Override // com.google.android.play.core.internal.r
    public void u0(int i, Bundle bundle) {
        zt4 zt4Var;
        it4 it4Var;
        zt4Var = this.b.c;
        zt4Var.b();
        it4Var = st4.f;
        it4Var.d("onStartDownload(%d)", Integer.valueOf(i));
    }
}
