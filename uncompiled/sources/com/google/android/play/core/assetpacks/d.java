package com.google.android.play.core.assetpacks;

import java.io.IOException;
import java.io.InputStream;

/* loaded from: classes2.dex */
public final class d extends InputStream {
    public final InputStream a;
    public long f0;

    public d(InputStream inputStream, long j) {
        this.a = inputStream;
        this.f0 = j;
    }

    @Override // java.io.InputStream, java.io.Closeable, java.lang.AutoCloseable
    public final void close() throws IOException {
        super.close();
        this.a.close();
        this.f0 = 0L;
    }

    @Override // java.io.InputStream
    public final int read() throws IOException {
        long j = this.f0;
        if (j <= 0) {
            return -1;
        }
        this.f0 = j - 1;
        return this.a.read();
    }

    @Override // java.io.InputStream
    public final int read(byte[] bArr, int i, int i2) throws IOException {
        long j = this.f0;
        if (j <= 0) {
            return -1;
        }
        int read = this.a.read(bArr, i, (int) Math.min(i2, j));
        if (read != -1) {
            this.f0 -= read;
        }
        return read;
    }
}
