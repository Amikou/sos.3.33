package com.google.android.play.core.assetpacks;

import android.content.Context;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

/* loaded from: classes2.dex */
public final class c {
    public static final it4 c = new it4("AssetPackStorage");
    public static final long d;
    public static final long e;
    public final Context a;
    public final ww4 b;

    static {
        TimeUnit timeUnit = TimeUnit.DAYS;
        d = timeUnit.toMillis(14L);
        e = timeUnit.toMillis(28L);
    }

    public c(Context context, ww4 ww4Var) {
        this.a = context;
        this.b = ww4Var;
    }

    public static void g(File file) {
        File[] listFiles;
        if (file.listFiles() == null || file.listFiles().length <= 1) {
            return;
        }
        long j = j(file);
        for (File file2 : file.listFiles()) {
            if (!file2.getName().equals(String.valueOf(j)) && !file2.getName().equals("stale.tmp")) {
                o(file2);
            }
        }
    }

    public static long h(File file) {
        return i(file, true);
    }

    public static long i(File file, boolean z) {
        File[] listFiles;
        if (file.exists()) {
            ArrayList arrayList = new ArrayList();
            if (z && file.listFiles().length > 1) {
                c.e("Multiple pack versions found, using highest version code.", new Object[0]);
            }
            try {
                for (File file2 : file.listFiles()) {
                    if (!file2.getName().equals("stale.tmp")) {
                        arrayList.add(Long.valueOf(file2.getName()));
                    }
                }
            } catch (NumberFormatException e2) {
                c.c(e2, "Corrupt asset pack directories.", new Object[0]);
            }
            if (arrayList.isEmpty()) {
                return -1L;
            }
            Collections.sort(arrayList);
            return ((Long) arrayList.get(arrayList.size() - 1)).longValue();
        }
        return -1L;
    }

    public static long j(File file) {
        return i(file, false);
    }

    public static boolean o(File file) {
        boolean z;
        File[] listFiles = file.listFiles();
        if (listFiles != null) {
            z = true;
            for (File file2 : listFiles) {
                z &= o(file2);
            }
        } else {
            z = true;
        }
        return file.delete() && true == z;
    }

    public final File A(String str, int i, long j, String str2) {
        return new File(C(str, i, j, str2), "checkpoint.dat");
    }

    public final File B(String str, int i, long j, String str2) {
        return new File(C(str, i, j, str2), "checkpoint_ext.dat");
    }

    public final File C(String str, int i, long j, String str2) {
        return new File(D(str, i, j), str2);
    }

    public final File D(String str, int i, long j) {
        return new File(new File(l(str, i, j), "_slices"), "_metadata");
    }

    public final void E(String str, int i, long j) {
        File[] listFiles;
        File[] listFiles2;
        File e2 = e(str);
        if (e2.exists()) {
            for (File file : e2.listFiles()) {
                if (!file.getName().equals(String.valueOf(i)) && !file.getName().equals("stale.tmp")) {
                    o(file);
                } else if (file.getName().equals(String.valueOf(i))) {
                    for (File file2 : file.listFiles()) {
                        if (!file2.getName().equals(String.valueOf(j))) {
                            o(file2);
                        }
                    }
                }
            }
        }
    }

    public final void F() {
        for (File file : k()) {
            if (file.listFiles() != null) {
                g(file);
                long j = j(file);
                if (this.b.a() != j) {
                    try {
                        new File(new File(file, String.valueOf(j)), "stale.tmp").createNewFile();
                    } catch (IOException unused) {
                        c.b("Could not write staleness marker.", new Object[0]);
                    }
                }
                for (File file2 : file.listFiles()) {
                    g(file2);
                }
            }
        }
    }

    public final int G(String str) {
        return (int) h(e(str));
    }

    public final long H(String str) {
        return h(d(str, G(str)));
    }

    public final void I() {
        File[] listFiles;
        for (File file : k()) {
            if (file.listFiles() != null) {
                for (File file2 : file.listFiles()) {
                    File file3 = new File(file2, "stale.tmp");
                    if (file3.exists() && System.currentTimeMillis() - file3.lastModified() > e) {
                        o(file2);
                    }
                }
            }
        }
    }

    public final void J() {
        File[] listFiles;
        if (m().exists()) {
            for (File file : m().listFiles()) {
                if (System.currentTimeMillis() - file.lastModified() > d) {
                    o(file);
                } else {
                    g(file);
                }
            }
        }
    }

    public final void K() {
        o(n());
    }

    public final void a(List<String> list) {
        int a = this.b.a();
        for (File file : k()) {
            if (!list.contains(file.getName()) && h(file) != a) {
                o(file);
            }
        }
    }

    public final void b(String str, int i, long j) {
        if (l(str, i, j).exists()) {
            o(l(str, i, j));
        }
    }

    public final void c(String str, int i, long j) {
        if (t(str, i, j).exists()) {
            o(t(str, i, j));
        }
    }

    public final File d(String str, int i) {
        return new File(e(str), String.valueOf(i));
    }

    public final File e(String str) {
        return new File(n(), str);
    }

    public final File f(String str, int i, long j) {
        return new File(x(str, i, j), "merge.tmp");
    }

    public final List<File> k() {
        File[] listFiles;
        ArrayList arrayList = new ArrayList();
        try {
        } catch (IOException e2) {
            c.b("Could not process directory while scanning installed packs. %s", e2);
        }
        if (n().exists() && n().listFiles() != null) {
            for (File file : n().listFiles()) {
                if (!file.getCanonicalPath().equals(m().getCanonicalPath())) {
                    arrayList.add(file);
                }
            }
            return arrayList;
        }
        return arrayList;
    }

    public final File l(String str, int i, long j) {
        return new File(new File(new File(m(), str), String.valueOf(i)), String.valueOf(j));
    }

    public final File m() {
        return new File(n(), "_tmp");
    }

    public final File n() {
        return new File(this.a.getFilesDir(), "assetpacks");
    }

    public final Map<String, ki> p() {
        HashMap hashMap = new HashMap();
        try {
            for (File file : k()) {
                ki r = r(file.getName());
                if (r != null) {
                    hashMap.put(file.getName(), r);
                }
            }
        } catch (IOException e2) {
            c.b("Could not process directory while scanning installed packs: %s", e2);
        }
        return hashMap;
    }

    public final Map<String, Long> q() {
        HashMap hashMap = new HashMap();
        for (String str : p().keySet()) {
            hashMap.put(str, Long.valueOf(H(str)));
        }
        return hashMap;
    }

    public final ki r(String str) throws IOException {
        String s = s(str);
        if (s == null) {
            return null;
        }
        File file = new File(s, "assets");
        if (file.isDirectory()) {
            return ki.b(s, file.getCanonicalPath());
        }
        c.b("Failed to find assets directory: %s", file);
        return null;
    }

    public final String s(String str) throws IOException {
        int length;
        File file = new File(n(), str);
        if (!file.exists()) {
            c.a("Pack not found with pack name: %s", str);
            return null;
        }
        File file2 = new File(file, String.valueOf(this.b.a()));
        if (!file2.exists()) {
            c.a("Pack not found with pack name: %s app version: %s", str, Integer.valueOf(this.b.a()));
            return null;
        }
        File[] listFiles = file2.listFiles();
        if (listFiles == null || (length = listFiles.length) == 0) {
            c.a("No pack version found for pack name: %s app version: %s", str, Integer.valueOf(this.b.a()));
            return null;
        } else if (length > 1) {
            c.b("Multiple pack versions found for pack name: %s app version: %s", str, Integer.valueOf(this.b.a()));
            return null;
        } else {
            return listFiles[0].getCanonicalPath();
        }
    }

    public final File t(String str, int i, long j) {
        return new File(d(str, i), String.valueOf(j));
    }

    public final File u(String str, int i, long j) {
        return new File(t(str, i, j), "_metadata");
    }

    public final File v(String str, int i, long j, String str2) {
        return new File(new File(new File(l(str, i, j), "_slices"), "_unverified"), str2);
    }

    public final File w(String str, int i, long j, String str2) {
        return new File(new File(new File(l(str, i, j), "_slices"), "_verified"), str2);
    }

    public final File x(String str, int i, long j) {
        return new File(l(str, i, j), "_packs");
    }

    public final int y(String str, int i, long j) throws IOException {
        File f = f(str, i, j);
        if (f.exists()) {
            Properties properties = new Properties();
            FileInputStream fileInputStream = new FileInputStream(f);
            try {
                properties.load(fileInputStream);
                fileInputStream.close();
                if (properties.getProperty("numberOfMerges") != null) {
                    try {
                        return Integer.parseInt(properties.getProperty("numberOfMerges"));
                    } catch (NumberFormatException e2) {
                        throw new bj("Merge checkpoint file corrupt.", e2);
                    }
                }
                throw new bj("Merge checkpoint file corrupt.");
            } catch (Throwable th) {
                try {
                    fileInputStream.close();
                } catch (Throwable th2) {
                    yv4.a(th, th2);
                }
                throw th;
            }
        }
        return 0;
    }

    public final void z(String str, int i, long j, int i2) throws IOException {
        File f = f(str, i, j);
        Properties properties = new Properties();
        properties.put("numberOfMerges", String.valueOf(i2));
        f.getParentFile().mkdirs();
        f.createNewFile();
        FileOutputStream fileOutputStream = new FileOutputStream(f);
        properties.store(fileOutputStream, (String) null);
        fileOutputStream.close();
    }
}
