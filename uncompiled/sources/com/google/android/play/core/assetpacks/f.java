package com.google.android.play.core.assetpacks;

import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.play.core.internal.s;
import com.google.android.play.core.internal.v;

/* loaded from: classes2.dex */
public final class f extends s {
    public final it4 a = new it4("AssetPackExtractionService");
    public final Context b;
    public final c c;

    public f(Context context, c cVar) {
        this.b = context;
        this.c = cVar;
    }

    public final synchronized void T0(Bundle bundle) {
        ComponentName componentName;
        Intent intent = new Intent(this.b, ExtractionForegroundService.class);
        int i = bundle.getInt("action_type");
        intent.putExtra("action_type", i);
        if (i == 1) {
            intent.putExtra("notification_channel_name", bundle.getString("notification_channel_name"));
            intent.putExtra("notification_title", bundle.getString("notification_title"));
            intent.putExtra("notification_subtext", bundle.getString("notification_subtext"));
            intent.putExtra("notification_timeout", bundle.getLong("notification_timeout"));
            Parcelable parcelable = bundle.getParcelable("notification_on_click_intent");
            if (parcelable instanceof PendingIntent) {
                intent.putExtra("notification_on_click_intent", parcelable);
            }
            intent.putExtra("notification_color", bundle.getInt("notification_color"));
        }
        try {
            componentName = Build.VERSION.SDK_INT >= 26 ? this.b.startForegroundService(intent) : this.b.startService(intent);
        } catch (IllegalStateException | SecurityException e) {
            this.a.c(e, "Failed starting installation service.", new Object[0]);
            componentName = null;
        }
        if (componentName == null) {
            this.a.b("Failed starting installation service.", new Object[0]);
        }
    }

    @Override // com.google.android.play.core.internal.t
    public final void b0(Bundle bundle, v vVar) throws RemoteException {
        this.a.a("updateServiceState AIDL call", new Object[0]);
        if (!hv4.a(this.b) || !hv4.b(this.b)) {
            vVar.T0(new Bundle());
            return;
        }
        T0(bundle);
        vVar.B0(new Bundle(), new Bundle());
    }

    @Override // com.google.android.play.core.internal.t
    public final void m(v vVar) throws RemoteException {
        this.a.a("clearAssetPackStorage AIDL call", new Object[0]);
        if (!hv4.a(this.b) || !hv4.b(this.b)) {
            vVar.T0(new Bundle());
            return;
        }
        this.c.K();
        vVar.E0(new Bundle());
    }
}
