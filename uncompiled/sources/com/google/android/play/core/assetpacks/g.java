package com.google.android.play.core.assetpacks;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

/* loaded from: classes2.dex */
public final class g extends FilterInputStream {
    public final nw4 a;
    public byte[] f0;
    public long g0;
    public boolean h0;
    public boolean i0;

    public g(InputStream inputStream) {
        super(inputStream);
        this.a = new nw4();
        this.f0 = new byte[4096];
        this.h0 = false;
        this.i0 = false;
    }

    public final gx4 a() throws IOException {
        byte[] bArr;
        if (this.g0 <= 0) {
            if (this.h0) {
            }
            return new gx4(null, -1L, -1, false, false, null);
        }
        do {
            bArr = this.f0;
        } while (read(bArr, 0, bArr.length) != -1);
        if (!this.h0 || this.i0) {
            return new gx4(null, -1L, -1, false, false, null);
        }
        if (!e(30)) {
            this.h0 = true;
            return this.a.b();
        }
        gx4 b = this.a.b();
        if (b.h()) {
            this.i0 = true;
            return b;
        } else if (b.e() != 4294967295L) {
            int c = this.a.c() - 30;
            long j = c;
            int length = this.f0.length;
            if (j > length) {
                do {
                    length += length;
                } while (length < j);
                this.f0 = Arrays.copyOf(this.f0, length);
            }
            if (!e(c)) {
                this.h0 = true;
                return this.a.b();
            }
            gx4 b2 = this.a.b();
            this.g0 = b2.e();
            return b2;
        } else {
            throw new bj("Files bigger than 4GiB are not supported.");
        }
    }

    public final boolean b() {
        return this.h0;
    }

    public final boolean c() {
        return this.i0;
    }

    public final long d() {
        return this.g0;
    }

    public final boolean e(int i) throws IOException {
        int f = f(this.f0, 0, i);
        if (f != i) {
            int i2 = i - f;
            if (f(this.f0, f, i2) != i2) {
                this.a.a(this.f0, 0, f);
                return false;
            }
        }
        this.a.a(this.f0, 0, i);
        return true;
    }

    public final int f(byte[] bArr, int i, int i2) throws IOException {
        return Math.max(0, super.read(bArr, i, i2));
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public final int read(byte[] bArr) throws IOException {
        return read(bArr, 0, bArr.length);
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public final int read(byte[] bArr, int i, int i2) throws IOException {
        long j = this.g0;
        if (j <= 0 || this.h0) {
            return -1;
        }
        int f = f(bArr, i, (int) Math.min(j, i2));
        this.g0 -= f;
        if (f == 0) {
            this.h0 = true;
            return 0;
        }
        return f;
    }
}
