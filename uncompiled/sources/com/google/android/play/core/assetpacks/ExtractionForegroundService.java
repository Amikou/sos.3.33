package com.google.android.play.core.assetpacks;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;

/* loaded from: classes2.dex */
public class ExtractionForegroundService extends Service {
    public final it4 a = new it4("ExtractionForegroundService");
    public Context f0;
    public ux4 g0;
    public NotificationManager h0;

    public final synchronized void a(Intent intent) {
        String stringExtra = intent.getStringExtra("notification_title");
        String stringExtra2 = intent.getStringExtra("notification_subtext");
        long longExtra = intent.getLongExtra("notification_timeout", 1L);
        PendingIntent pendingIntent = (PendingIntent) intent.getParcelableExtra("notification_on_click_intent");
        int i = Build.VERSION.SDK_INT;
        Notification.Builder timeoutAfter = i >= 26 ? new Notification.Builder(this.f0, "playcore-assetpacks-service-notification-channel").setTimeoutAfter(longExtra) : new Notification.Builder(this.f0).setPriority(-2);
        if (pendingIntent != null) {
            timeoutAfter.setContentIntent(pendingIntent);
        }
        timeoutAfter.setSmallIcon(17301633).setOngoing(false).setContentTitle(stringExtra).setSubText(stringExtra2);
        if (i >= 21) {
            timeoutAfter.setColor(intent.getIntExtra("notification_color", 0)).setVisibility(-1);
        }
        Notification build = timeoutAfter.build();
        this.a.d("Starting foreground installation service.", new Object[0]);
        this.g0.a(true);
        if (i >= 26) {
            c(intent.getStringExtra("notification_channel_name"));
        }
        startForeground(-1883842196, build);
    }

    public final synchronized void b() {
        this.a.d("Stopping service.", new Object[0]);
        this.g0.a(false);
        stopForeground(true);
        stopSelf();
    }

    @TargetApi(26)
    public final synchronized void c(String str) {
        this.h0.createNotificationChannel(new NotificationChannel("playcore-assetpacks-service-notification-channel", str, 2));
    }

    @Override // android.app.Service
    public final IBinder onBind(Intent intent) {
        return null;
    }

    @Override // android.app.Service
    public final void onCreate() {
        super.onCreate();
        k.h(getApplicationContext()).a(this);
        this.h0 = (NotificationManager) this.f0.getSystemService("notification");
    }

    @Override // android.app.Service
    public final int onStartCommand(Intent intent, int i, int i2) {
        int intExtra = intent.getIntExtra("action_type", 0);
        if (intExtra == 1) {
            a(intent);
        } else if (intExtra == 2) {
            b();
        } else {
            this.a.b("Unknown action type received: %d", Integer.valueOf(intExtra));
        }
        return 2;
    }
}
