package com.google.android.play.core.assetpacks;

import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;

/* loaded from: classes2.dex */
public final class b extends a<ParcelFileDescriptor> {
    public b(st4 st4Var, tx4<ParcelFileDescriptor> tx4Var) {
        super(st4Var, tx4Var);
    }

    @Override // com.google.android.play.core.assetpacks.a, com.google.android.play.core.internal.r
    public final void a1(Bundle bundle, Bundle bundle2) throws RemoteException {
        super.a1(bundle, bundle2);
        this.a.e((ParcelFileDescriptor) bundle.getParcelable("chunk_file_descriptor"));
    }
}
