package com.google.android.play.core.assetpacks;

import android.content.Context;
import android.util.Base64;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

/* loaded from: classes2.dex */
public final class k {
    public static us4 a;

    public static String a(List<File> list) throws NoSuchAlgorithmException, IOException {
        int read;
        MessageDigest messageDigest = MessageDigest.getInstance("SHA256");
        byte[] bArr = new byte[8192];
        for (File file : list) {
            FileInputStream fileInputStream = new FileInputStream(file);
            do {
                try {
                    read = fileInputStream.read(bArr);
                    if (read > 0) {
                        messageDigest.update(bArr, 0, read);
                    }
                } catch (Throwable th) {
                    try {
                        fileInputStream.close();
                    } catch (Throwable th2) {
                        yv4.a(th, th2);
                    }
                    throw th;
                }
            } while (read != -1);
            fileInputStream.close();
        }
        return Base64.encodeToString(messageDigest.digest(), 11);
    }

    public static long b(byte[] bArr, int i) {
        return ((c(bArr, i + 2) << 16) | c(bArr, i)) & 4294967295L;
    }

    public static int c(byte[] bArr, int i) {
        return ((bArr[i + 1] & 255) << 8) | (bArr[i] & 255);
    }

    public static boolean d(int i) {
        return i == 1 || i == 7 || i == 2 || i == 3;
    }

    public static boolean e(int i) {
        return i == 5 || i == 6 || i == 4;
    }

    public static boolean f(int i) {
        return i == 2 || i == 7 || i == 3;
    }

    public static boolean g(int i, int i2) {
        if (i == 5) {
            if (i2 != 5) {
                return true;
            }
            i = 5;
        }
        if (i != 6 || i2 == 6 || i2 == 5) {
            if (i != 4 || i2 == 4) {
                if (i == 3 && (i2 == 2 || i2 == 7 || i2 == 1 || i2 == 8)) {
                    return true;
                }
                if (i == 2) {
                    return i2 == 1 || i2 == 8;
                }
                return false;
            }
            return true;
        }
        return true;
    }

    public static synchronized us4 h(Context context) {
        us4 us4Var;
        synchronized (k.class) {
            if (a == null) {
                yu4 yu4Var = new yu4(null);
                yu4Var.b(new dy4(ny4.c(context)));
                a = yu4Var.a();
            }
            us4Var = a;
        }
        return us4Var;
    }
}
