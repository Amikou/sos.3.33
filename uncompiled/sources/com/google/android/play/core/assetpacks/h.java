package com.google.android.play.core.assetpacks;

import android.os.Bundle;
import android.os.ResultReceiver;

/* loaded from: classes2.dex */
final class h extends ResultReceiver {
    public final /* synthetic */ tx4 a;
    public final /* synthetic */ ux4 f0;

    @Override // android.os.ResultReceiver
    public final void onReceiveResult(int i, Bundle bundle) {
        vu4 vu4Var;
        if (i == 1) {
            this.a.e(-1);
            vu4Var = this.f0.d;
            vu4Var.a(null);
        } else if (i != 2) {
            this.a.d(new AssetPackException(-100));
        } else {
            this.a.e(0);
        }
    }
}
