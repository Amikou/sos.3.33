package com.google.android.play.core.assetpacks;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.SequenceInputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

/* loaded from: classes2.dex */
public final class e extends com.google.android.play.core.internal.e {
    public final File a;
    public final File f0;
    public final NavigableMap<Long, File> g0 = new TreeMap();

    public e(File file, File file2) throws IOException {
        this.a = file;
        this.f0 = file2;
        List<File> a = n.a(file, file2);
        if (a.isEmpty()) {
            throw new bj(String.format("Virtualized slice archive empty for %s, %s", file, file2));
        }
        long j = 0;
        for (File file3 : a) {
            this.g0.put(Long.valueOf(j), file3);
            j += file3.length();
        }
    }

    @Override // com.google.android.play.core.internal.e
    public final long a() {
        Map.Entry<Long, File> lastEntry = this.g0.lastEntry();
        return lastEntry.getKey().longValue() + lastEntry.getValue().length();
    }

    @Override // com.google.android.play.core.internal.e
    public final InputStream b(long j, long j2) throws IOException {
        if (j < 0 || j2 < 0) {
            throw new bj(String.format("Invalid input parameters %s, %s", Long.valueOf(j), Long.valueOf(j2)));
        }
        long j3 = j + j2;
        if (j3 <= a()) {
            Long floorKey = this.g0.floorKey(Long.valueOf(j));
            Long floorKey2 = this.g0.floorKey(Long.valueOf(j3));
            if (floorKey.equals(floorKey2)) {
                return new d(d(j, floorKey), j2);
            }
            ArrayList arrayList = new ArrayList();
            arrayList.add(d(j, floorKey));
            Collection<File> values = this.g0.subMap(floorKey, false, floorKey2, false).values();
            if (!values.isEmpty()) {
                arrayList.add(new l(Collections.enumeration(values)));
            }
            arrayList.add(new d(new FileInputStream((File) this.g0.get(floorKey2)), j2 - (floorKey2.longValue() - j)));
            return new SequenceInputStream(Collections.enumeration(arrayList));
        }
        throw new bj(String.format("Trying to access archive out of bounds. Archive ends at: %s. Tried accessing: %s", Long.valueOf(a()), Long.valueOf(j3)));
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public final void close() {
    }

    public final InputStream d(long j, Long l) throws IOException {
        FileInputStream fileInputStream = new FileInputStream((File) this.g0.get(l));
        if (fileInputStream.skip(j - l.longValue()) == j - l.longValue()) {
            return fileInputStream;
        }
        throw new bj(String.format("Virtualized slice archive corrupt, could not skip in file with key %s", l));
    }
}
