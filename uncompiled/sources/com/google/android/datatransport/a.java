package com.google.android.datatransport;

import com.google.auto.value.AutoValue;

/* compiled from: Event.java */
@AutoValue
/* loaded from: classes.dex */
public abstract class a<T> {
    public static <T> a<T> d(T t) {
        return new il(null, t, Priority.VERY_LOW);
    }

    public static <T> a<T> e(T t) {
        return new il(null, t, Priority.HIGHEST);
    }

    public abstract Integer a();

    public abstract T b();

    public abstract Priority c();
}
