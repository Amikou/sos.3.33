package com.google.android.datatransport.runtime.backends;

import com.google.android.datatransport.runtime.backends.c;
import java.util.Arrays;
import java.util.Objects;

/* compiled from: AutoValue_BackendRequest.java */
/* loaded from: classes.dex */
public final class a extends c {
    public final Iterable<wx0> a;
    public final byte[] b;

    /* compiled from: AutoValue_BackendRequest.java */
    /* loaded from: classes.dex */
    public static final class b extends c.a {
        public Iterable<wx0> a;
        public byte[] b;

        @Override // com.google.android.datatransport.runtime.backends.c.a
        public c a() {
            String str = "";
            if (this.a == null) {
                str = " events";
            }
            if (str.isEmpty()) {
                return new a(this.a, this.b);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @Override // com.google.android.datatransport.runtime.backends.c.a
        public c.a b(Iterable<wx0> iterable) {
            Objects.requireNonNull(iterable, "Null events");
            this.a = iterable;
            return this;
        }

        @Override // com.google.android.datatransport.runtime.backends.c.a
        public c.a c(byte[] bArr) {
            this.b = bArr;
            return this;
        }
    }

    @Override // com.google.android.datatransport.runtime.backends.c
    public Iterable<wx0> b() {
        return this.a;
    }

    @Override // com.google.android.datatransport.runtime.backends.c
    public byte[] c() {
        return this.b;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof c) {
            c cVar = (c) obj;
            if (this.a.equals(cVar.b())) {
                if (Arrays.equals(this.b, cVar instanceof a ? ((a) cVar).b : cVar.c())) {
                    return true;
                }
            }
            return false;
        }
        return false;
    }

    public int hashCode() {
        return ((this.a.hashCode() ^ 1000003) * 1000003) ^ Arrays.hashCode(this.b);
    }

    public String toString() {
        return "BackendRequest{events=" + this.a + ", extras=" + Arrays.toString(this.b) + "}";
    }

    public a(Iterable<wx0> iterable, byte[] bArr) {
        this.a = iterable;
        this.b = bArr;
    }
}
