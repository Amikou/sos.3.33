package com.google.android.datatransport.runtime.backends;

import android.content.Context;
import com.google.auto.value.AutoValue;

/* compiled from: CreationContext.java */
@AutoValue
/* loaded from: classes.dex */
public abstract class d {
    public static d a(Context context, qz qzVar, qz qzVar2, String str) {
        return new hl(context, qzVar, qzVar2, str);
    }

    public abstract Context b();

    public abstract String c();

    public abstract qz d();

    public abstract qz e();
}
