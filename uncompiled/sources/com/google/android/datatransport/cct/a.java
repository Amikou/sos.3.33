package com.google.android.datatransport.cct;

import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.telephony.TelephonyManager;
import com.google.android.datatransport.cct.a;
import com.google.android.datatransport.cct.internal.ClientInfo;
import com.google.android.datatransport.cct.internal.NetworkConnectionInfo;
import com.google.android.datatransport.cct.internal.QosTier;
import com.google.android.datatransport.cct.internal.h;
import com.google.android.datatransport.cct.internal.i;
import com.google.android.datatransport.cct.internal.j;
import com.google.android.datatransport.cct.internal.k;
import com.google.android.datatransport.runtime.backends.BackendResponse;
import com.google.android.datatransport.runtime.backends.c;
import com.google.firebase.encoders.EncodingException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import zendesk.core.Constants;

/* compiled from: CctTransportBackend.java */
/* loaded from: classes.dex */
public final class a implements nb4 {
    public final com.google.firebase.encoders.a a;
    public final ConnectivityManager b;
    public final Context c;
    public final URL d;
    public final qz e;
    public final qz f;
    public final int g;

    /* compiled from: CctTransportBackend.java */
    /* renamed from: com.google.android.datatransport.cct.a$a  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static final class C0098a {
        public final URL a;
        public final h b;
        public final String c;

        public C0098a(URL url, h hVar, String str) {
            this.a = url;
            this.b = hVar;
            this.c = str;
        }

        public C0098a a(URL url) {
            return new C0098a(url, this.b, this.c);
        }
    }

    /* compiled from: CctTransportBackend.java */
    /* loaded from: classes.dex */
    public static final class b {
        public final int a;
        public final URL b;
        public final long c;

        public b(int i, URL url, long j) {
            this.a = i;
            this.b = url;
            this.c = j;
        }
    }

    public a(Context context, qz qzVar, qz qzVar2, int i) {
        this.a = h.b();
        this.c = context;
        this.b = (ConnectivityManager) context.getSystemService("connectivity");
        this.d = n(ht.c);
        this.e = qzVar2;
        this.f = qzVar;
        this.g = i;
    }

    public static int f(NetworkInfo networkInfo) {
        if (networkInfo == null) {
            return NetworkConnectionInfo.MobileSubtype.UNKNOWN_MOBILE_SUBTYPE.getValue();
        }
        int subtype = networkInfo.getSubtype();
        if (subtype == -1) {
            return NetworkConnectionInfo.MobileSubtype.COMBINED.getValue();
        }
        if (NetworkConnectionInfo.MobileSubtype.forNumber(subtype) != null) {
            return subtype;
        }
        return 0;
    }

    public static int g(NetworkInfo networkInfo) {
        if (networkInfo == null) {
            return NetworkConnectionInfo.NetworkType.NONE.getValue();
        }
        return networkInfo.getType();
    }

    public static int h(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            z12.c("CctTransportBackend", "Unable to find version code for package", e);
            return -1;
        }
    }

    public static TelephonyManager j(Context context) {
        return (TelephonyManager) context.getSystemService("phone");
    }

    public static long k() {
        Calendar.getInstance();
        return TimeZone.getDefault().getOffset(Calendar.getInstance().getTimeInMillis()) / 1000;
    }

    public static /* synthetic */ C0098a l(C0098a c0098a, b bVar) {
        URL url = bVar.b;
        if (url != null) {
            z12.a("CctTransportBackend", "Following redirect to: %s", url);
            return c0098a.a(bVar.b);
        }
        return null;
    }

    public static InputStream m(InputStream inputStream, String str) throws IOException {
        return "gzip".equals(str) ? new GZIPInputStream(inputStream) : inputStream;
    }

    public static URL n(String str) {
        try {
            return new URL(str);
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException("Invalid url: " + str, e);
        }
    }

    @Override // defpackage.nb4
    public wx0 a(wx0 wx0Var) {
        NetworkInfo activeNetworkInfo = this.b.getActiveNetworkInfo();
        return wx0Var.l().a("sdk-version", Build.VERSION.SDK_INT).c("model", Build.MODEL).c("hardware", Build.HARDWARE).c("device", Build.DEVICE).c("product", Build.PRODUCT).c("os-uild", Build.ID).c("manufacturer", Build.MANUFACTURER).c("fingerprint", Build.FINGERPRINT).b("tz-offset", k()).a("net-type", g(activeNetworkInfo)).a("mobile-subtype", f(activeNetworkInfo)).c("country", Locale.getDefault().getCountry()).c("locale", Locale.getDefault().getLanguage()).c("mcc_mnc", j(this.c).getSimOperator()).c("application_build", Integer.toString(h(this.c))).d();
    }

    @Override // defpackage.nb4
    public BackendResponse b(c cVar) {
        h i = i(cVar);
        URL url = this.d;
        if (cVar.c() != null) {
            try {
                ht c = ht.c(cVar.c());
                r3 = c.d() != null ? c.d() : null;
                if (c.e() != null) {
                    url = n(c.e());
                }
            } catch (IllegalArgumentException unused) {
                return BackendResponse.a();
            }
        }
        try {
            b bVar = (b) u83.a(5, new C0098a(url, i, r3), new dd1() { // from class: mw
                @Override // defpackage.dd1
                public final Object apply(Object obj) {
                    a.b e;
                    e = a.this.e((a.C0098a) obj);
                    return e;
                }
            }, nw.a);
            int i2 = bVar.a;
            if (i2 == 200) {
                return BackendResponse.d(bVar.c);
            }
            if (i2 < 500 && i2 != 404) {
                return BackendResponse.a();
            }
            return BackendResponse.e();
        } catch (IOException e) {
            z12.c("CctTransportBackend", "Could not make request to the backend", e);
            return BackendResponse.e();
        }
    }

    public final b e(C0098a c0098a) throws IOException {
        z12.a("CctTransportBackend", "Making request to: %s", c0098a.a);
        HttpURLConnection httpURLConnection = (HttpURLConnection) c0098a.a.openConnection();
        httpURLConnection.setConnectTimeout(30000);
        httpURLConnection.setReadTimeout(this.g);
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setInstanceFollowRedirects(false);
        httpURLConnection.setRequestMethod("POST");
        httpURLConnection.setRequestProperty(Constants.USER_AGENT_HEADER_KEY, String.format("datatransport/%s android/", "3.0.0"));
        httpURLConnection.setRequestProperty("Content-Encoding", "gzip");
        httpURLConnection.setRequestProperty("Content-Type", Constants.APPLICATION_JSON);
        httpURLConnection.setRequestProperty("Accept-Encoding", "gzip");
        String str = c0098a.c;
        if (str != null) {
            httpURLConnection.setRequestProperty("X-Goog-Api-Key", str);
        }
        try {
            OutputStream outputStream = httpURLConnection.getOutputStream();
            try {
                GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(outputStream);
                this.a.a(c0098a.b, new BufferedWriter(new OutputStreamWriter(gZIPOutputStream)));
                gZIPOutputStream.close();
                if (outputStream != null) {
                    outputStream.close();
                }
                int responseCode = httpURLConnection.getResponseCode();
                z12.e("CctTransportBackend", "Status Code: " + responseCode);
                z12.e("CctTransportBackend", "Content-Type: " + httpURLConnection.getHeaderField("Content-Type"));
                z12.e("CctTransportBackend", "Content-Encoding: " + httpURLConnection.getHeaderField("Content-Encoding"));
                if (responseCode == 302 || responseCode == 301 || responseCode == 307) {
                    return new b(responseCode, new URL(httpURLConnection.getHeaderField("Location")), 0L);
                }
                if (responseCode != 200) {
                    return new b(responseCode, null, 0L);
                }
                InputStream inputStream = httpURLConnection.getInputStream();
                try {
                    InputStream m = m(inputStream, httpURLConnection.getHeaderField("Content-Encoding"));
                    b bVar = new b(responseCode, null, k.b(new BufferedReader(new InputStreamReader(m))).c());
                    if (m != null) {
                        m.close();
                    }
                    if (inputStream != null) {
                        inputStream.close();
                    }
                    return bVar;
                } catch (Throwable th) {
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (Throwable th2) {
                            th.addSuppressed(th2);
                        }
                    }
                    throw th;
                }
            } catch (Throwable th3) {
                if (outputStream != null) {
                    try {
                        outputStream.close();
                    } catch (Throwable th4) {
                        th3.addSuppressed(th4);
                    }
                }
                throw th3;
            }
        } catch (EncodingException e) {
            e = e;
            z12.c("CctTransportBackend", "Couldn't encode request, returning with 400", e);
            return new b(400, null, 0L);
        } catch (ConnectException e2) {
            e = e2;
            z12.c("CctTransportBackend", "Couldn't open connection, returning with 500", e);
            return new b(500, null, 0L);
        } catch (UnknownHostException e3) {
            e = e3;
            z12.c("CctTransportBackend", "Couldn't open connection, returning with 500", e);
            return new b(500, null, 0L);
        } catch (IOException e4) {
            e = e4;
            z12.c("CctTransportBackend", "Couldn't encode request, returning with 400", e);
            return new b(400, null, 0L);
        }
    }

    public final h i(c cVar) {
        i.a j;
        HashMap hashMap = new HashMap();
        for (wx0 wx0Var : cVar.b()) {
            String j2 = wx0Var.j();
            if (!hashMap.containsKey(j2)) {
                ArrayList arrayList = new ArrayList();
                arrayList.add(wx0Var);
                hashMap.put(j2, arrayList);
            } else {
                ((List) hashMap.get(j2)).add(wx0Var);
            }
        }
        ArrayList arrayList2 = new ArrayList();
        for (Map.Entry entry : hashMap.entrySet()) {
            wx0 wx0Var2 = (wx0) ((List) entry.getValue()).get(0);
            j.a b2 = j.a().f(QosTier.DEFAULT).g(this.f.a()).h(this.e.a()).b(ClientInfo.a().c(ClientInfo.ClientType.ANDROID_FIREBASE).b(com.google.android.datatransport.cct.internal.a.a().m(Integer.valueOf(wx0Var2.g("sdk-version"))).j(wx0Var2.b("model")).f(wx0Var2.b("hardware")).d(wx0Var2.b("device")).l(wx0Var2.b("product")).k(wx0Var2.b("os-uild")).h(wx0Var2.b("manufacturer")).e(wx0Var2.b("fingerprint")).c(wx0Var2.b("country")).g(wx0Var2.b("locale")).i(wx0Var2.b("mcc_mnc")).b(wx0Var2.b("application_build")).a()).a());
            try {
                b2.i(Integer.parseInt((String) entry.getKey()));
            } catch (NumberFormatException unused) {
                b2.j((String) entry.getKey());
            }
            ArrayList arrayList3 = new ArrayList();
            for (wx0 wx0Var3 : (List) entry.getValue()) {
                cv0 e = wx0Var3.e();
                hv0 b3 = e.b();
                if (b3.equals(hv0.b("proto"))) {
                    j = i.j(e.a());
                } else if (b3.equals(hv0.b("json"))) {
                    j = i.i(new String(e.a(), Charset.forName("UTF-8")));
                } else {
                    z12.f("CctTransportBackend", "Received event of unsupported encoding %s. Skipping...", b3);
                }
                j.c(wx0Var3.f()).d(wx0Var3.k()).h(wx0Var3.h("tz-offset")).e(NetworkConnectionInfo.a().c(NetworkConnectionInfo.NetworkType.forNumber(wx0Var3.g("net-type"))).b(NetworkConnectionInfo.MobileSubtype.forNumber(wx0Var3.g("mobile-subtype"))).a());
                if (wx0Var3.d() != null) {
                    j.b(wx0Var3.d());
                }
                arrayList3.add(j.a());
            }
            b2.c(arrayList3);
            arrayList2.add(b2.a());
        }
        return h.a(arrayList2);
    }

    public a(Context context, qz qzVar, qz qzVar2) {
        this(context, qzVar, qzVar2, 40000);
    }
}
