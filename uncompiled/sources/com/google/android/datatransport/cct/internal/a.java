package com.google.android.datatransport.cct.internal;

import com.google.android.datatransport.cct.internal.c;
import com.google.auto.value.AutoValue;

/* compiled from: AndroidClientInfo.java */
@AutoValue
/* loaded from: classes.dex */
public abstract class a {

    /* compiled from: AndroidClientInfo.java */
    @AutoValue.Builder
    /* renamed from: com.google.android.datatransport.cct.internal.a$a  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static abstract class AbstractC0099a {
        public abstract a a();

        public abstract AbstractC0099a b(String str);

        public abstract AbstractC0099a c(String str);

        public abstract AbstractC0099a d(String str);

        public abstract AbstractC0099a e(String str);

        public abstract AbstractC0099a f(String str);

        public abstract AbstractC0099a g(String str);

        public abstract AbstractC0099a h(String str);

        public abstract AbstractC0099a i(String str);

        public abstract AbstractC0099a j(String str);

        public abstract AbstractC0099a k(String str);

        public abstract AbstractC0099a l(String str);

        public abstract AbstractC0099a m(Integer num);
    }

    public static AbstractC0099a a() {
        return new c.b();
    }

    public abstract String b();

    public abstract String c();

    public abstract String d();

    public abstract String e();

    public abstract String f();

    public abstract String g();

    public abstract String h();

    public abstract String i();

    public abstract String j();

    public abstract String k();

    public abstract String l();

    public abstract Integer m();
}
