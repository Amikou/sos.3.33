package com.google.android.datatransport.cct.internal;

import com.google.android.datatransport.cct.internal.f;
import com.google.auto.value.AutoValue;
import java.util.List;

/* compiled from: LogRequest.java */
@AutoValue
/* loaded from: classes.dex */
public abstract class j {

    /* compiled from: LogRequest.java */
    @AutoValue.Builder
    /* loaded from: classes.dex */
    public static abstract class a {
        public abstract j a();

        public abstract a b(ClientInfo clientInfo);

        public abstract a c(List<i> list);

        public abstract a d(Integer num);

        public abstract a e(String str);

        public abstract a f(QosTier qosTier);

        public abstract a g(long j);

        public abstract a h(long j);

        public a i(int i) {
            return d(Integer.valueOf(i));
        }

        public a j(String str) {
            return e(str);
        }
    }

    public static a a() {
        return new f.b();
    }

    public abstract ClientInfo b();

    public abstract List<i> c();

    public abstract Integer d();

    public abstract String e();

    public abstract QosTier f();

    public abstract long g();

    public abstract long h();
}
