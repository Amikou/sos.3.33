package com.google.android.datatransport.cct.internal;

import com.google.android.datatransport.cct.internal.j;
import java.util.List;

/* compiled from: AutoValue_LogRequest.java */
/* loaded from: classes.dex */
public final class f extends j {
    public final long a;
    public final long b;
    public final ClientInfo c;
    public final Integer d;
    public final String e;
    public final List<i> f;
    public final QosTier g;

    /* compiled from: AutoValue_LogRequest.java */
    /* loaded from: classes.dex */
    public static final class b extends j.a {
        public Long a;
        public Long b;
        public ClientInfo c;
        public Integer d;
        public String e;
        public List<i> f;
        public QosTier g;

        @Override // com.google.android.datatransport.cct.internal.j.a
        public j a() {
            String str = "";
            if (this.a == null) {
                str = " requestTimeMs";
            }
            if (this.b == null) {
                str = str + " requestUptimeMs";
            }
            if (str.isEmpty()) {
                return new f(this.a.longValue(), this.b.longValue(), this.c, this.d, this.e, this.f, this.g);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @Override // com.google.android.datatransport.cct.internal.j.a
        public j.a b(ClientInfo clientInfo) {
            this.c = clientInfo;
            return this;
        }

        @Override // com.google.android.datatransport.cct.internal.j.a
        public j.a c(List<i> list) {
            this.f = list;
            return this;
        }

        @Override // com.google.android.datatransport.cct.internal.j.a
        public j.a d(Integer num) {
            this.d = num;
            return this;
        }

        @Override // com.google.android.datatransport.cct.internal.j.a
        public j.a e(String str) {
            this.e = str;
            return this;
        }

        @Override // com.google.android.datatransport.cct.internal.j.a
        public j.a f(QosTier qosTier) {
            this.g = qosTier;
            return this;
        }

        @Override // com.google.android.datatransport.cct.internal.j.a
        public j.a g(long j) {
            this.a = Long.valueOf(j);
            return this;
        }

        @Override // com.google.android.datatransport.cct.internal.j.a
        public j.a h(long j) {
            this.b = Long.valueOf(j);
            return this;
        }
    }

    @Override // com.google.android.datatransport.cct.internal.j
    public ClientInfo b() {
        return this.c;
    }

    @Override // com.google.android.datatransport.cct.internal.j
    public List<i> c() {
        return this.f;
    }

    @Override // com.google.android.datatransport.cct.internal.j
    public Integer d() {
        return this.d;
    }

    @Override // com.google.android.datatransport.cct.internal.j
    public String e() {
        return this.e;
    }

    public boolean equals(Object obj) {
        ClientInfo clientInfo;
        Integer num;
        String str;
        List<i> list;
        if (obj == this) {
            return true;
        }
        if (obj instanceof j) {
            j jVar = (j) obj;
            if (this.a == jVar.g() && this.b == jVar.h() && ((clientInfo = this.c) != null ? clientInfo.equals(jVar.b()) : jVar.b() == null) && ((num = this.d) != null ? num.equals(jVar.d()) : jVar.d() == null) && ((str = this.e) != null ? str.equals(jVar.e()) : jVar.e() == null) && ((list = this.f) != null ? list.equals(jVar.c()) : jVar.c() == null)) {
                QosTier qosTier = this.g;
                if (qosTier == null) {
                    if (jVar.f() == null) {
                        return true;
                    }
                } else if (qosTier.equals(jVar.f())) {
                    return true;
                }
            }
            return false;
        }
        return false;
    }

    @Override // com.google.android.datatransport.cct.internal.j
    public QosTier f() {
        return this.g;
    }

    @Override // com.google.android.datatransport.cct.internal.j
    public long g() {
        return this.a;
    }

    @Override // com.google.android.datatransport.cct.internal.j
    public long h() {
        return this.b;
    }

    public int hashCode() {
        long j = this.a;
        long j2 = this.b;
        int i = (((((int) (j ^ (j >>> 32))) ^ 1000003) * 1000003) ^ ((int) ((j2 >>> 32) ^ j2))) * 1000003;
        ClientInfo clientInfo = this.c;
        int hashCode = (i ^ (clientInfo == null ? 0 : clientInfo.hashCode())) * 1000003;
        Integer num = this.d;
        int hashCode2 = (hashCode ^ (num == null ? 0 : num.hashCode())) * 1000003;
        String str = this.e;
        int hashCode3 = (hashCode2 ^ (str == null ? 0 : str.hashCode())) * 1000003;
        List<i> list = this.f;
        int hashCode4 = (hashCode3 ^ (list == null ? 0 : list.hashCode())) * 1000003;
        QosTier qosTier = this.g;
        return hashCode4 ^ (qosTier != null ? qosTier.hashCode() : 0);
    }

    public String toString() {
        return "LogRequest{requestTimeMs=" + this.a + ", requestUptimeMs=" + this.b + ", clientInfo=" + this.c + ", logSource=" + this.d + ", logSourceName=" + this.e + ", logEvents=" + this.f + ", qosTier=" + this.g + "}";
    }

    public f(long j, long j2, ClientInfo clientInfo, Integer num, String str, List<i> list, QosTier qosTier) {
        this.a = j;
        this.b = j2;
        this.c = clientInfo;
        this.d = num;
        this.e = str;
        this.f = list;
        this.g = qosTier;
    }
}
