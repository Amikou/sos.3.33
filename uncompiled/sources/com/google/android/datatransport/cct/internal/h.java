package com.google.android.datatransport.cct.internal;

import com.google.auto.value.AutoValue;
import java.util.List;

/* compiled from: BatchedLogRequest.java */
@AutoValue
/* loaded from: classes.dex */
public abstract class h {
    public static h a(List<j> list) {
        return new kk(list);
    }

    public static com.google.firebase.encoders.a b() {
        return new wu1().j(b.a).k(true).i();
    }

    public abstract List<j> c();
}
