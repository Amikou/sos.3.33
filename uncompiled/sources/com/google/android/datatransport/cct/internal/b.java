package com.google.android.datatransport.cct.internal;

import java.io.IOException;

/* compiled from: AutoBatchedLogRequestEncoder.java */
/* loaded from: classes.dex */
public final class b implements b50 {
    public static final b50 a = new b();

    /* compiled from: AutoBatchedLogRequestEncoder.java */
    /* loaded from: classes.dex */
    public static final class a implements hl2<com.google.android.datatransport.cct.internal.a> {
        public static final a a = new a();
        public static final h31 b = h31.d("sdkVersion");
        public static final h31 c = h31.d("model");
        public static final h31 d = h31.d("hardware");
        public static final h31 e = h31.d("device");
        public static final h31 f = h31.d("product");
        public static final h31 g = h31.d("osBuild");
        public static final h31 h = h31.d("manufacturer");
        public static final h31 i = h31.d("fingerprint");
        public static final h31 j = h31.d("locale");
        public static final h31 k = h31.d("country");
        public static final h31 l = h31.d("mccMnc");
        public static final h31 m = h31.d("applicationBuild");

        @Override // com.google.firebase.encoders.b
        /* renamed from: b */
        public void a(com.google.android.datatransport.cct.internal.a aVar, com.google.firebase.encoders.c cVar) throws IOException {
            cVar.a(b, aVar.m());
            cVar.a(c, aVar.j());
            cVar.a(d, aVar.f());
            cVar.a(e, aVar.d());
            cVar.a(f, aVar.l());
            cVar.a(g, aVar.k());
            cVar.a(h, aVar.h());
            cVar.a(i, aVar.e());
            cVar.a(j, aVar.g());
            cVar.a(k, aVar.c());
            cVar.a(l, aVar.i());
            cVar.a(m, aVar.b());
        }
    }

    /* compiled from: AutoBatchedLogRequestEncoder.java */
    /* renamed from: com.google.android.datatransport.cct.internal.b$b  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static final class C0100b implements hl2<h> {
        public static final C0100b a = new C0100b();
        public static final h31 b = h31.d("logRequest");

        @Override // com.google.firebase.encoders.b
        /* renamed from: b */
        public void a(h hVar, com.google.firebase.encoders.c cVar) throws IOException {
            cVar.a(b, hVar.c());
        }
    }

    /* compiled from: AutoBatchedLogRequestEncoder.java */
    /* loaded from: classes.dex */
    public static final class c implements hl2<ClientInfo> {
        public static final c a = new c();
        public static final h31 b = h31.d("clientType");
        public static final h31 c = h31.d("androidClientInfo");

        @Override // com.google.firebase.encoders.b
        /* renamed from: b */
        public void a(ClientInfo clientInfo, com.google.firebase.encoders.c cVar) throws IOException {
            cVar.a(b, clientInfo.c());
            cVar.a(c, clientInfo.b());
        }
    }

    /* compiled from: AutoBatchedLogRequestEncoder.java */
    /* loaded from: classes.dex */
    public static final class d implements hl2<i> {
        public static final d a = new d();
        public static final h31 b = h31.d("eventTimeMs");
        public static final h31 c = h31.d("eventCode");
        public static final h31 d = h31.d("eventUptimeMs");
        public static final h31 e = h31.d("sourceExtension");
        public static final h31 f = h31.d("sourceExtensionJsonProto3");
        public static final h31 g = h31.d("timezoneOffsetSeconds");
        public static final h31 h = h31.d("networkConnectionInfo");

        @Override // com.google.firebase.encoders.b
        /* renamed from: b */
        public void a(i iVar, com.google.firebase.encoders.c cVar) throws IOException {
            cVar.f(b, iVar.c());
            cVar.a(c, iVar.b());
            cVar.f(d, iVar.d());
            cVar.a(e, iVar.f());
            cVar.a(f, iVar.g());
            cVar.f(g, iVar.h());
            cVar.a(h, iVar.e());
        }
    }

    /* compiled from: AutoBatchedLogRequestEncoder.java */
    /* loaded from: classes.dex */
    public static final class e implements hl2<j> {
        public static final e a = new e();
        public static final h31 b = h31.d("requestTimeMs");
        public static final h31 c = h31.d("requestUptimeMs");
        public static final h31 d = h31.d("clientInfo");
        public static final h31 e = h31.d("logSource");
        public static final h31 f = h31.d("logSourceName");
        public static final h31 g = h31.d("logEvent");
        public static final h31 h = h31.d("qosTier");

        @Override // com.google.firebase.encoders.b
        /* renamed from: b */
        public void a(j jVar, com.google.firebase.encoders.c cVar) throws IOException {
            cVar.f(b, jVar.g());
            cVar.f(c, jVar.h());
            cVar.a(d, jVar.b());
            cVar.a(e, jVar.d());
            cVar.a(f, jVar.e());
            cVar.a(g, jVar.c());
            cVar.a(h, jVar.f());
        }
    }

    /* compiled from: AutoBatchedLogRequestEncoder.java */
    /* loaded from: classes.dex */
    public static final class f implements hl2<NetworkConnectionInfo> {
        public static final f a = new f();
        public static final h31 b = h31.d("networkType");
        public static final h31 c = h31.d("mobileSubtype");

        @Override // com.google.firebase.encoders.b
        /* renamed from: b */
        public void a(NetworkConnectionInfo networkConnectionInfo, com.google.firebase.encoders.c cVar) throws IOException {
            cVar.a(b, networkConnectionInfo.c());
            cVar.a(c, networkConnectionInfo.b());
        }
    }

    @Override // defpackage.b50
    public void a(fv0<?> fv0Var) {
        C0100b c0100b = C0100b.a;
        fv0Var.a(h.class, c0100b);
        fv0Var.a(kk.class, c0100b);
        e eVar = e.a;
        fv0Var.a(j.class, eVar);
        fv0Var.a(com.google.android.datatransport.cct.internal.f.class, eVar);
        c cVar = c.a;
        fv0Var.a(ClientInfo.class, cVar);
        fv0Var.a(com.google.android.datatransport.cct.internal.d.class, cVar);
        a aVar = a.a;
        fv0Var.a(com.google.android.datatransport.cct.internal.a.class, aVar);
        fv0Var.a(com.google.android.datatransport.cct.internal.c.class, aVar);
        d dVar = d.a;
        fv0Var.a(i.class, dVar);
        fv0Var.a(com.google.android.datatransport.cct.internal.e.class, dVar);
        f fVar = f.a;
        fv0Var.a(NetworkConnectionInfo.class, fVar);
        fv0Var.a(g.class, fVar);
    }
}
