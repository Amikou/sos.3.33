package com.google.android.datatransport.cct;

import androidx.annotation.Keep;
import com.google.android.datatransport.runtime.backends.d;

@Keep
/* loaded from: classes.dex */
public class CctBackendFactory implements am {
    @Override // defpackage.am
    public nb4 create(d dVar) {
        return new a(dVar.b(), dVar.e(), dVar.d());
    }
}
