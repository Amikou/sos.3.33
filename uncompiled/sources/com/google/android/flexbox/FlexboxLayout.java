package com.google.android.flexbox;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.SparseIntArray;
import android.view.View;
import android.view.ViewGroup;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.flexbox.b;
import java.util.ArrayList;
import java.util.List;
import okhttp3.internal.http2.Http2Connection;

/* loaded from: classes.dex */
public class FlexboxLayout extends ViewGroup implements z61 {
    public int a;
    public int f0;
    public int g0;
    public int h0;
    public int i0;
    public int j0;
    public Drawable k0;
    public Drawable l0;
    public int m0;
    public int n0;
    public int o0;
    public int p0;
    public int[] q0;
    public SparseIntArray r0;
    public b s0;
    public List<a> t0;
    public b.C0103b u0;

    public FlexboxLayout(Context context) {
        this(context, null);
    }

    public final void A() {
        if (this.k0 == null && this.l0 == null) {
            setWillNotDraw(true);
        } else {
            setWillNotDraw(false);
        }
    }

    public final boolean a(int i) {
        for (int i2 = 0; i2 < i; i2++) {
            if (this.t0.get(i2).c() > 0) {
                return false;
            }
        }
        return true;
    }

    @Override // android.view.ViewGroup
    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        if (this.r0 == null) {
            this.r0 = new SparseIntArray(getChildCount());
        }
        this.q0 = this.s0.n(view, i, layoutParams, this.r0);
        super.addView(view, i, layoutParams);
    }

    @Override // defpackage.z61
    public void b(View view, int i, int i2, a aVar) {
        if (s(i, i2)) {
            if (k()) {
                int i3 = aVar.e;
                int i4 = this.p0;
                aVar.e = i3 + i4;
                aVar.f += i4;
                return;
            }
            int i5 = aVar.e;
            int i6 = this.o0;
            aVar.e = i5 + i6;
            aVar.f += i6;
        }
    }

    @Override // defpackage.z61
    public void c(a aVar) {
        if (k()) {
            if ((this.n0 & 4) > 0) {
                int i = aVar.e;
                int i2 = this.p0;
                aVar.e = i + i2;
                aVar.f += i2;
            }
        } else if ((this.m0 & 4) > 0) {
            int i3 = aVar.e;
            int i4 = this.o0;
            aVar.e = i3 + i4;
            aVar.f += i4;
        }
    }

    @Override // android.view.ViewGroup
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams;
    }

    @Override // defpackage.z61
    public View d(int i) {
        return r(i);
    }

    @Override // defpackage.z61
    public int e(int i, int i2, int i3) {
        return ViewGroup.getChildMeasureSpec(i, i2, i3);
    }

    @Override // defpackage.z61
    public void f(int i, View view) {
    }

    public final boolean g(int i, int i2) {
        for (int i3 = 1; i3 <= i2; i3++) {
            View r = r(i - i3);
            if (r != null && r.getVisibility() != 8) {
                return false;
            }
        }
        return true;
    }

    @Override // defpackage.z61
    public int getAlignContent() {
        return this.i0;
    }

    @Override // defpackage.z61
    public int getAlignItems() {
        return this.h0;
    }

    public Drawable getDividerDrawableHorizontal() {
        return this.k0;
    }

    public Drawable getDividerDrawableVertical() {
        return this.l0;
    }

    @Override // defpackage.z61
    public int getFlexDirection() {
        return this.a;
    }

    @Override // defpackage.z61
    public int getFlexItemCount() {
        return getChildCount();
    }

    public List<a> getFlexLines() {
        ArrayList arrayList = new ArrayList(this.t0.size());
        for (a aVar : this.t0) {
            if (aVar.c() != 0) {
                arrayList.add(aVar);
            }
        }
        return arrayList;
    }

    @Override // defpackage.z61
    public List<a> getFlexLinesInternal() {
        return this.t0;
    }

    @Override // defpackage.z61
    public int getFlexWrap() {
        return this.f0;
    }

    public int getJustifyContent() {
        return this.g0;
    }

    @Override // defpackage.z61
    public int getLargestMainSize() {
        int i = Integer.MIN_VALUE;
        for (a aVar : this.t0) {
            i = Math.max(i, aVar.e);
        }
        return i;
    }

    @Override // defpackage.z61
    public int getMaxLine() {
        return this.j0;
    }

    public int getShowDividerHorizontal() {
        return this.m0;
    }

    public int getShowDividerVertical() {
        return this.n0;
    }

    @Override // defpackage.z61
    public int getSumOfCrossSize() {
        int i;
        int i2;
        int size = this.t0.size();
        int i3 = 0;
        for (int i4 = 0; i4 < size; i4++) {
            a aVar = this.t0.get(i4);
            if (t(i4)) {
                if (k()) {
                    i2 = this.o0;
                } else {
                    i2 = this.p0;
                }
                i3 += i2;
            }
            if (u(i4)) {
                if (k()) {
                    i = this.o0;
                } else {
                    i = this.p0;
                }
                i3 += i;
            }
            i3 += aVar.g;
        }
        return i3;
    }

    @Override // defpackage.z61
    public View h(int i) {
        return getChildAt(i);
    }

    @Override // defpackage.z61
    public int i(View view, int i, int i2) {
        int i3;
        int i4;
        if (k()) {
            i3 = s(i, i2) ? 0 + this.p0 : 0;
            if ((this.n0 & 4) <= 0) {
                return i3;
            }
            i4 = this.p0;
        } else {
            i3 = s(i, i2) ? 0 + this.o0 : 0;
            if ((this.m0 & 4) <= 0) {
                return i3;
            }
            i4 = this.o0;
        }
        return i3 + i4;
    }

    @Override // defpackage.z61
    public int j(int i, int i2, int i3) {
        return ViewGroup.getChildMeasureSpec(i, i2, i3);
    }

    @Override // defpackage.z61
    public boolean k() {
        int i = this.a;
        return i == 0 || i == 1;
    }

    @Override // defpackage.z61
    public int l(View view) {
        return 0;
    }

    public final void m(Canvas canvas, boolean z, boolean z2) {
        int i;
        int i2;
        int right;
        int left;
        int paddingLeft = getPaddingLeft();
        int max = Math.max(0, (getWidth() - getPaddingRight()) - paddingLeft);
        int size = this.t0.size();
        for (int i3 = 0; i3 < size; i3++) {
            a aVar = this.t0.get(i3);
            for (int i4 = 0; i4 < aVar.h; i4++) {
                int i5 = aVar.o + i4;
                View r = r(i5);
                if (r != null && r.getVisibility() != 8) {
                    LayoutParams layoutParams = (LayoutParams) r.getLayoutParams();
                    if (s(i5, i4)) {
                        if (z) {
                            left = r.getRight() + ((ViewGroup.MarginLayoutParams) layoutParams).rightMargin;
                        } else {
                            left = (r.getLeft() - ((ViewGroup.MarginLayoutParams) layoutParams).leftMargin) - this.p0;
                        }
                        p(canvas, left, aVar.b, aVar.g);
                    }
                    if (i4 == aVar.h - 1 && (this.n0 & 4) > 0) {
                        if (z) {
                            right = (r.getLeft() - ((ViewGroup.MarginLayoutParams) layoutParams).leftMargin) - this.p0;
                        } else {
                            right = r.getRight() + ((ViewGroup.MarginLayoutParams) layoutParams).rightMargin;
                        }
                        p(canvas, right, aVar.b, aVar.g);
                    }
                }
            }
            if (t(i3)) {
                if (z2) {
                    i2 = aVar.d;
                } else {
                    i2 = aVar.b - this.o0;
                }
                o(canvas, paddingLeft, i2, max);
            }
            if (u(i3) && (this.m0 & 4) > 0) {
                if (z2) {
                    i = aVar.b - this.o0;
                } else {
                    i = aVar.d;
                }
                o(canvas, paddingLeft, i, max);
            }
        }
    }

    public final void n(Canvas canvas, boolean z, boolean z2) {
        int i;
        int i2;
        int bottom;
        int top;
        int paddingTop = getPaddingTop();
        int max = Math.max(0, (getHeight() - getPaddingBottom()) - paddingTop);
        int size = this.t0.size();
        for (int i3 = 0; i3 < size; i3++) {
            a aVar = this.t0.get(i3);
            for (int i4 = 0; i4 < aVar.h; i4++) {
                int i5 = aVar.o + i4;
                View r = r(i5);
                if (r != null && r.getVisibility() != 8) {
                    LayoutParams layoutParams = (LayoutParams) r.getLayoutParams();
                    if (s(i5, i4)) {
                        if (z2) {
                            top = r.getBottom() + ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin;
                        } else {
                            top = (r.getTop() - ((ViewGroup.MarginLayoutParams) layoutParams).topMargin) - this.o0;
                        }
                        o(canvas, aVar.a, top, aVar.g);
                    }
                    if (i4 == aVar.h - 1 && (this.m0 & 4) > 0) {
                        if (z2) {
                            bottom = (r.getTop() - ((ViewGroup.MarginLayoutParams) layoutParams).topMargin) - this.o0;
                        } else {
                            bottom = r.getBottom() + ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin;
                        }
                        o(canvas, aVar.a, bottom, aVar.g);
                    }
                }
            }
            if (t(i3)) {
                if (z) {
                    i2 = aVar.c;
                } else {
                    i2 = aVar.a - this.p0;
                }
                p(canvas, i2, paddingTop, max);
            }
            if (u(i3) && (this.n0 & 4) > 0) {
                if (z) {
                    i = aVar.a - this.p0;
                } else {
                    i = aVar.c;
                }
                p(canvas, i, paddingTop, max);
            }
        }
    }

    public final void o(Canvas canvas, int i, int i2, int i3) {
        Drawable drawable = this.k0;
        if (drawable == null) {
            return;
        }
        drawable.setBounds(i, i2, i3 + i, this.o0 + i2);
        this.k0.draw(canvas);
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        if (this.l0 == null && this.k0 == null) {
            return;
        }
        if (this.m0 == 0 && this.n0 == 0) {
            return;
        }
        int E = ei4.E(this);
        int i = this.a;
        if (i == 0) {
            m(canvas, E == 1, this.f0 == 2);
        } else if (i == 1) {
            m(canvas, E != 1, this.f0 == 2);
        } else if (i == 2) {
            boolean z = E == 1;
            if (this.f0 == 2) {
                z = !z;
            }
            n(canvas, z, false);
        } else if (i != 3) {
        } else {
            boolean z2 = E == 1;
            if (this.f0 == 2) {
                z2 = !z2;
            }
            n(canvas, z2, true);
        }
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        boolean z2;
        int E = ei4.E(this);
        int i5 = this.a;
        if (i5 == 0) {
            v(E == 1, i, i2, i3, i4);
        } else if (i5 == 1) {
            v(E != 1, i, i2, i3, i4);
        } else if (i5 == 2) {
            z2 = E == 1;
            w(this.f0 == 2 ? !z2 : z2, false, i, i2, i3, i4);
        } else if (i5 == 3) {
            z2 = E == 1;
            w(this.f0 == 2 ? !z2 : z2, true, i, i2, i3, i4);
        } else {
            throw new IllegalStateException("Invalid flex direction is set: " + this.a);
        }
    }

    @Override // android.view.View
    public void onMeasure(int i, int i2) {
        if (this.r0 == null) {
            this.r0 = new SparseIntArray(getChildCount());
        }
        if (this.s0.O(this.r0)) {
            this.q0 = this.s0.m(this.r0);
        }
        int i3 = this.a;
        if (i3 == 0 || i3 == 1) {
            x(i, i2);
        } else if (i3 != 2 && i3 != 3) {
            throw new IllegalStateException("Invalid value for the flex direction is set: " + this.a);
        } else {
            y(i, i2);
        }
    }

    public final void p(Canvas canvas, int i, int i2, int i3) {
        Drawable drawable = this.l0;
        if (drawable == null) {
            return;
        }
        drawable.setBounds(i, i2, this.p0 + i, i3 + i2);
        this.l0.draw(canvas);
    }

    @Override // android.view.ViewGroup
    /* renamed from: q */
    public LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(getContext(), attributeSet);
    }

    public View r(int i) {
        if (i >= 0) {
            int[] iArr = this.q0;
            if (i >= iArr.length) {
                return null;
            }
            return getChildAt(iArr[i]);
        }
        return null;
    }

    public final boolean s(int i, int i2) {
        return g(i, i2) ? k() ? (this.n0 & 1) != 0 : (this.m0 & 1) != 0 : k() ? (this.n0 & 2) != 0 : (this.m0 & 2) != 0;
    }

    public void setAlignContent(int i) {
        if (this.i0 != i) {
            this.i0 = i;
            requestLayout();
        }
    }

    public void setAlignItems(int i) {
        if (this.h0 != i) {
            this.h0 = i;
            requestLayout();
        }
    }

    public void setDividerDrawable(Drawable drawable) {
        setDividerDrawableHorizontal(drawable);
        setDividerDrawableVertical(drawable);
    }

    public void setDividerDrawableHorizontal(Drawable drawable) {
        if (drawable == this.k0) {
            return;
        }
        this.k0 = drawable;
        if (drawable != null) {
            this.o0 = drawable.getIntrinsicHeight();
        } else {
            this.o0 = 0;
        }
        A();
        requestLayout();
    }

    public void setDividerDrawableVertical(Drawable drawable) {
        if (drawable == this.l0) {
            return;
        }
        this.l0 = drawable;
        if (drawable != null) {
            this.p0 = drawable.getIntrinsicWidth();
        } else {
            this.p0 = 0;
        }
        A();
        requestLayout();
    }

    public void setFlexDirection(int i) {
        if (this.a != i) {
            this.a = i;
            requestLayout();
        }
    }

    @Override // defpackage.z61
    public void setFlexLines(List<a> list) {
        this.t0 = list;
    }

    public void setFlexWrap(int i) {
        if (this.f0 != i) {
            this.f0 = i;
            requestLayout();
        }
    }

    public void setJustifyContent(int i) {
        if (this.g0 != i) {
            this.g0 = i;
            requestLayout();
        }
    }

    public void setMaxLine(int i) {
        if (this.j0 != i) {
            this.j0 = i;
            requestLayout();
        }
    }

    public void setShowDivider(int i) {
        setShowDividerVertical(i);
        setShowDividerHorizontal(i);
    }

    public void setShowDividerHorizontal(int i) {
        if (i != this.m0) {
            this.m0 = i;
            requestLayout();
        }
    }

    public void setShowDividerVertical(int i) {
        if (i != this.n0) {
            this.n0 = i;
            requestLayout();
        }
    }

    public final boolean t(int i) {
        if (i < 0 || i >= this.t0.size()) {
            return false;
        }
        return a(i) ? k() ? (this.m0 & 1) != 0 : (this.n0 & 1) != 0 : k() ? (this.m0 & 2) != 0 : (this.n0 & 2) != 0;
    }

    public final boolean u(int i) {
        if (i < 0 || i >= this.t0.size()) {
            return false;
        }
        for (int i2 = i + 1; i2 < this.t0.size(); i2++) {
            if (this.t0.get(i2).c() > 0) {
                return false;
            }
        }
        return k() ? (this.m0 & 4) != 0 : (this.n0 & 4) != 0;
    }

    /* JADX WARN: Removed duplicated region for block: B:41:0x00d5  */
    /* JADX WARN: Removed duplicated region for block: B:59:0x0130  */
    /* JADX WARN: Removed duplicated region for block: B:62:0x018d  */
    /* JADX WARN: Removed duplicated region for block: B:68:0x01f0  */
    /* JADX WARN: Removed duplicated region for block: B:69:0x01fd  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void v(boolean r29, int r30, int r31, int r32, int r33) {
        /*
            Method dump skipped, instructions count: 557
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.flexbox.FlexboxLayout.v(boolean, int, int, int, int):void");
    }

    /* JADX WARN: Removed duplicated region for block: B:42:0x00d3  */
    /* JADX WARN: Removed duplicated region for block: B:59:0x012c  */
    /* JADX WARN: Removed duplicated region for block: B:62:0x0185  */
    /* JADX WARN: Removed duplicated region for block: B:68:0x01e8  */
    /* JADX WARN: Removed duplicated region for block: B:69:0x01f5  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void w(boolean r30, boolean r31, int r32, int r33, int r34, int r35) {
        /*
            Method dump skipped, instructions count: 541
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.flexbox.FlexboxLayout.w(boolean, boolean, int, int, int, int):void");
    }

    public final void x(int i, int i2) {
        this.t0.clear();
        this.u0.a();
        this.s0.c(this.u0, i, i2);
        this.t0 = this.u0.a;
        this.s0.p(i, i2);
        if (this.h0 == 3) {
            for (a aVar : this.t0) {
                int i3 = Integer.MIN_VALUE;
                for (int i4 = 0; i4 < aVar.h; i4++) {
                    View r = r(aVar.o + i4);
                    if (r != null && r.getVisibility() != 8) {
                        LayoutParams layoutParams = (LayoutParams) r.getLayoutParams();
                        if (this.f0 != 2) {
                            i3 = Math.max(i3, r.getMeasuredHeight() + Math.max(aVar.l - r.getBaseline(), ((ViewGroup.MarginLayoutParams) layoutParams).topMargin) + ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin);
                        } else {
                            i3 = Math.max(i3, r.getMeasuredHeight() + ((ViewGroup.MarginLayoutParams) layoutParams).topMargin + Math.max((aVar.l - r.getMeasuredHeight()) + r.getBaseline(), ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin));
                        }
                    }
                }
                aVar.g = i3;
            }
        }
        this.s0.o(i, i2, getPaddingTop() + getPaddingBottom());
        this.s0.X();
        z(this.a, i, i2, this.u0.b);
    }

    public final void y(int i, int i2) {
        this.t0.clear();
        this.u0.a();
        this.s0.f(this.u0, i, i2);
        this.t0 = this.u0.a;
        this.s0.p(i, i2);
        this.s0.o(i, i2, getPaddingLeft() + getPaddingRight());
        this.s0.X();
        z(this.a, i, i2, this.u0.b);
    }

    public final void z(int i, int i2, int i3, int i4) {
        int sumOfCrossSize;
        int largestMainSize;
        int resolveSizeAndState;
        int resolveSizeAndState2;
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        int mode2 = View.MeasureSpec.getMode(i3);
        int size2 = View.MeasureSpec.getSize(i3);
        if (i == 0 || i == 1) {
            sumOfCrossSize = getSumOfCrossSize() + getPaddingTop() + getPaddingBottom();
            largestMainSize = getLargestMainSize();
        } else if (i != 2 && i != 3) {
            throw new IllegalArgumentException("Invalid flex direction: " + i);
        } else {
            sumOfCrossSize = getLargestMainSize();
            largestMainSize = getSumOfCrossSize() + getPaddingLeft() + getPaddingRight();
        }
        if (mode == Integer.MIN_VALUE) {
            if (size < largestMainSize) {
                i4 = View.combineMeasuredStates(i4, Http2Connection.OKHTTP_CLIENT_WINDOW_SIZE);
            } else {
                size = largestMainSize;
            }
            resolveSizeAndState = View.resolveSizeAndState(size, i2, i4);
        } else if (mode == 0) {
            resolveSizeAndState = View.resolveSizeAndState(largestMainSize, i2, i4);
        } else if (mode == 1073741824) {
            if (size < largestMainSize) {
                i4 = View.combineMeasuredStates(i4, Http2Connection.OKHTTP_CLIENT_WINDOW_SIZE);
            }
            resolveSizeAndState = View.resolveSizeAndState(size, i2, i4);
        } else {
            throw new IllegalStateException("Unknown width mode is set: " + mode);
        }
        if (mode2 == Integer.MIN_VALUE) {
            if (size2 < sumOfCrossSize) {
                i4 = View.combineMeasuredStates(i4, 256);
            } else {
                size2 = sumOfCrossSize;
            }
            resolveSizeAndState2 = View.resolveSizeAndState(size2, i3, i4);
        } else if (mode2 == 0) {
            resolveSizeAndState2 = View.resolveSizeAndState(sumOfCrossSize, i3, i4);
        } else if (mode2 == 1073741824) {
            if (size2 < sumOfCrossSize) {
                i4 = View.combineMeasuredStates(i4, 256);
            }
            resolveSizeAndState2 = View.resolveSizeAndState(size2, i3, i4);
        } else {
            throw new IllegalStateException("Unknown height mode is set: " + mode2);
        }
        setMeasuredDimension(resolveSizeAndState, resolveSizeAndState2);
    }

    public FlexboxLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams instanceof LayoutParams) {
            return new LayoutParams((LayoutParams) layoutParams);
        }
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            return new LayoutParams((ViewGroup.MarginLayoutParams) layoutParams);
        }
        return new LayoutParams(layoutParams);
    }

    public FlexboxLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.j0 = -1;
        this.s0 = new b(this);
        this.t0 = new ArrayList();
        this.u0 = new b.C0103b();
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, l23.FlexboxLayout, i, 0);
        this.a = obtainStyledAttributes.getInt(l23.FlexboxLayout_flexDirection, 0);
        this.f0 = obtainStyledAttributes.getInt(l23.FlexboxLayout_flexWrap, 0);
        this.g0 = obtainStyledAttributes.getInt(l23.FlexboxLayout_justifyContent, 0);
        this.h0 = obtainStyledAttributes.getInt(l23.FlexboxLayout_alignItems, 0);
        this.i0 = obtainStyledAttributes.getInt(l23.FlexboxLayout_alignContent, 0);
        this.j0 = obtainStyledAttributes.getInt(l23.FlexboxLayout_maxLine, -1);
        Drawable drawable = obtainStyledAttributes.getDrawable(l23.FlexboxLayout_dividerDrawable);
        if (drawable != null) {
            setDividerDrawableHorizontal(drawable);
            setDividerDrawableVertical(drawable);
        }
        Drawable drawable2 = obtainStyledAttributes.getDrawable(l23.FlexboxLayout_dividerDrawableHorizontal);
        if (drawable2 != null) {
            setDividerDrawableHorizontal(drawable2);
        }
        Drawable drawable3 = obtainStyledAttributes.getDrawable(l23.FlexboxLayout_dividerDrawableVertical);
        if (drawable3 != null) {
            setDividerDrawableVertical(drawable3);
        }
        int i2 = obtainStyledAttributes.getInt(l23.FlexboxLayout_showDivider, 0);
        if (i2 != 0) {
            this.n0 = i2;
            this.m0 = i2;
        }
        int i3 = obtainStyledAttributes.getInt(l23.FlexboxLayout_showDividerVertical, 0);
        if (i3 != 0) {
            this.n0 = i3;
        }
        int i4 = obtainStyledAttributes.getInt(l23.FlexboxLayout_showDividerHorizontal, 0);
        if (i4 != 0) {
            this.m0 = i4;
        }
        obtainStyledAttributes.recycle();
    }

    /* loaded from: classes.dex */
    public static class LayoutParams extends ViewGroup.MarginLayoutParams implements FlexItem {
        public static final Parcelable.Creator<LayoutParams> CREATOR = new a();
        public int a;
        public float f0;
        public float g0;
        public int h0;
        public float i0;
        public int j0;
        public int k0;
        public int l0;
        public int m0;
        public boolean n0;

        /* loaded from: classes.dex */
        public class a implements Parcelable.Creator<LayoutParams> {
            @Override // android.os.Parcelable.Creator
            /* renamed from: a */
            public LayoutParams createFromParcel(Parcel parcel) {
                return new LayoutParams(parcel);
            }

            @Override // android.os.Parcelable.Creator
            /* renamed from: b */
            public LayoutParams[] newArray(int i) {
                return new LayoutParams[i];
            }
        }

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            this.a = 1;
            this.f0 = Utils.FLOAT_EPSILON;
            this.g0 = 1.0f;
            this.h0 = -1;
            this.i0 = -1.0f;
            this.j0 = -1;
            this.k0 = -1;
            this.l0 = 16777215;
            this.m0 = 16777215;
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, l23.FlexboxLayout_Layout);
            this.a = obtainStyledAttributes.getInt(l23.FlexboxLayout_Layout_layout_order, 1);
            this.f0 = obtainStyledAttributes.getFloat(l23.FlexboxLayout_Layout_layout_flexGrow, Utils.FLOAT_EPSILON);
            this.g0 = obtainStyledAttributes.getFloat(l23.FlexboxLayout_Layout_layout_flexShrink, 1.0f);
            this.h0 = obtainStyledAttributes.getInt(l23.FlexboxLayout_Layout_layout_alignSelf, -1);
            this.i0 = obtainStyledAttributes.getFraction(l23.FlexboxLayout_Layout_layout_flexBasisPercent, 1, 1, -1.0f);
            this.j0 = obtainStyledAttributes.getDimensionPixelSize(l23.FlexboxLayout_Layout_layout_minWidth, -1);
            this.k0 = obtainStyledAttributes.getDimensionPixelSize(l23.FlexboxLayout_Layout_layout_minHeight, -1);
            this.l0 = obtainStyledAttributes.getDimensionPixelSize(l23.FlexboxLayout_Layout_layout_maxWidth, 16777215);
            this.m0 = obtainStyledAttributes.getDimensionPixelSize(l23.FlexboxLayout_Layout_layout_maxHeight, 16777215);
            this.n0 = obtainStyledAttributes.getBoolean(l23.FlexboxLayout_Layout_layout_wrapBefore, false);
            obtainStyledAttributes.recycle();
        }

        @Override // com.google.android.flexbox.FlexItem
        public int B0() {
            return ((ViewGroup.MarginLayoutParams) this).leftMargin;
        }

        @Override // com.google.android.flexbox.FlexItem
        public int F1() {
            return this.l0;
        }

        @Override // com.google.android.flexbox.FlexItem
        public int K0() {
            return ((ViewGroup.MarginLayoutParams) this).topMargin;
        }

        @Override // com.google.android.flexbox.FlexItem
        public void O0(int i) {
            this.k0 = i;
        }

        @Override // com.google.android.flexbox.FlexItem
        public float S0() {
            return this.f0;
        }

        @Override // com.google.android.flexbox.FlexItem
        public float X0() {
            return this.i0;
        }

        @Override // com.google.android.flexbox.FlexItem
        public int a0() {
            return this.h0;
        }

        @Override // android.os.Parcelable
        public int describeContents() {
            return 0;
        }

        @Override // com.google.android.flexbox.FlexItem
        public float e0() {
            return this.g0;
        }

        @Override // com.google.android.flexbox.FlexItem
        public int getHeight() {
            return ((ViewGroup.MarginLayoutParams) this).height;
        }

        @Override // com.google.android.flexbox.FlexItem
        public int getOrder() {
            return this.a;
        }

        @Override // com.google.android.flexbox.FlexItem
        public int getWidth() {
            return ((ViewGroup.MarginLayoutParams) this).width;
        }

        @Override // com.google.android.flexbox.FlexItem
        public int m0() {
            return this.j0;
        }

        @Override // com.google.android.flexbox.FlexItem
        public int m1() {
            return ((ViewGroup.MarginLayoutParams) this).rightMargin;
        }

        @Override // com.google.android.flexbox.FlexItem
        public int p1() {
            return this.k0;
        }

        @Override // com.google.android.flexbox.FlexItem
        public boolean r1() {
            return this.n0;
        }

        @Override // com.google.android.flexbox.FlexItem
        public void w0(int i) {
            this.j0 = i;
        }

        @Override // com.google.android.flexbox.FlexItem
        public int w1() {
            return this.m0;
        }

        @Override // android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.a);
            parcel.writeFloat(this.f0);
            parcel.writeFloat(this.g0);
            parcel.writeInt(this.h0);
            parcel.writeFloat(this.i0);
            parcel.writeInt(this.j0);
            parcel.writeInt(this.k0);
            parcel.writeInt(this.l0);
            parcel.writeInt(this.m0);
            parcel.writeByte(this.n0 ? (byte) 1 : (byte) 0);
            parcel.writeInt(((ViewGroup.MarginLayoutParams) this).bottomMargin);
            parcel.writeInt(((ViewGroup.MarginLayoutParams) this).leftMargin);
            parcel.writeInt(((ViewGroup.MarginLayoutParams) this).rightMargin);
            parcel.writeInt(((ViewGroup.MarginLayoutParams) this).topMargin);
            parcel.writeInt(((ViewGroup.MarginLayoutParams) this).height);
            parcel.writeInt(((ViewGroup.MarginLayoutParams) this).width);
        }

        @Override // com.google.android.flexbox.FlexItem
        public int x0() {
            return ((ViewGroup.MarginLayoutParams) this).bottomMargin;
        }

        public LayoutParams(LayoutParams layoutParams) {
            super((ViewGroup.MarginLayoutParams) layoutParams);
            this.a = 1;
            this.f0 = Utils.FLOAT_EPSILON;
            this.g0 = 1.0f;
            this.h0 = -1;
            this.i0 = -1.0f;
            this.j0 = -1;
            this.k0 = -1;
            this.l0 = 16777215;
            this.m0 = 16777215;
            this.a = layoutParams.a;
            this.f0 = layoutParams.f0;
            this.g0 = layoutParams.g0;
            this.h0 = layoutParams.h0;
            this.i0 = layoutParams.i0;
            this.j0 = layoutParams.j0;
            this.k0 = layoutParams.k0;
            this.l0 = layoutParams.l0;
            this.m0 = layoutParams.m0;
            this.n0 = layoutParams.n0;
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
            this.a = 1;
            this.f0 = Utils.FLOAT_EPSILON;
            this.g0 = 1.0f;
            this.h0 = -1;
            this.i0 = -1.0f;
            this.j0 = -1;
            this.k0 = -1;
            this.l0 = 16777215;
            this.m0 = 16777215;
        }

        public LayoutParams(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
            this.a = 1;
            this.f0 = Utils.FLOAT_EPSILON;
            this.g0 = 1.0f;
            this.h0 = -1;
            this.i0 = -1.0f;
            this.j0 = -1;
            this.k0 = -1;
            this.l0 = 16777215;
            this.m0 = 16777215;
        }

        public LayoutParams(Parcel parcel) {
            super(0, 0);
            this.a = 1;
            this.f0 = Utils.FLOAT_EPSILON;
            this.g0 = 1.0f;
            this.h0 = -1;
            this.i0 = -1.0f;
            this.j0 = -1;
            this.k0 = -1;
            this.l0 = 16777215;
            this.m0 = 16777215;
            this.a = parcel.readInt();
            this.f0 = parcel.readFloat();
            this.g0 = parcel.readFloat();
            this.h0 = parcel.readInt();
            this.i0 = parcel.readFloat();
            this.j0 = parcel.readInt();
            this.k0 = parcel.readInt();
            this.l0 = parcel.readInt();
            this.m0 = parcel.readInt();
            this.n0 = parcel.readByte() != 0;
            ((ViewGroup.MarginLayoutParams) this).bottomMargin = parcel.readInt();
            ((ViewGroup.MarginLayoutParams) this).leftMargin = parcel.readInt();
            ((ViewGroup.MarginLayoutParams) this).rightMargin = parcel.readInt();
            ((ViewGroup.MarginLayoutParams) this).topMargin = parcel.readInt();
            ((ViewGroup.MarginLayoutParams) this).height = parcel.readInt();
            ((ViewGroup.MarginLayoutParams) this).width = parcel.readInt();
        }
    }
}
