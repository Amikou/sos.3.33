package com.google.android.flexbox;

import android.graphics.drawable.Drawable;
import android.util.SparseIntArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import com.github.mikephil.charting.utils.Utils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/* compiled from: FlexboxHelper.java */
/* loaded from: classes.dex */
public class b {
    public final z61 a;
    public boolean[] b;
    public int[] c;
    public long[] d;
    public long[] e;

    /* compiled from: FlexboxHelper.java */
    /* renamed from: com.google.android.flexbox.b$b  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static class C0103b {
        public List<com.google.android.flexbox.a> a;
        public int b;

        public void a() {
            this.a = null;
            this.b = 0;
        }
    }

    /* compiled from: FlexboxHelper.java */
    /* loaded from: classes.dex */
    public static class c implements Comparable<c> {
        public int a;
        public int f0;

        public c() {
        }

        @Override // java.lang.Comparable
        /* renamed from: a */
        public int compareTo(c cVar) {
            int i = this.f0;
            int i2 = cVar.f0;
            return i != i2 ? i - i2 : this.a - cVar.a;
        }

        public String toString() {
            return "Order{order=" + this.f0 + ", index=" + this.a + '}';
        }
    }

    public b(z61 z61Var) {
        this.a = z61Var;
    }

    public final int A(int i, FlexItem flexItem, int i2) {
        z61 z61Var = this.a;
        int e = z61Var.e(i, z61Var.getPaddingLeft() + this.a.getPaddingRight() + flexItem.B0() + flexItem.m1() + i2, flexItem.getWidth());
        int size = View.MeasureSpec.getSize(e);
        if (size > flexItem.F1()) {
            return View.MeasureSpec.makeMeasureSpec(flexItem.F1(), View.MeasureSpec.getMode(e));
        }
        return size < flexItem.m0() ? View.MeasureSpec.makeMeasureSpec(flexItem.m0(), View.MeasureSpec.getMode(e)) : e;
    }

    public final int B(FlexItem flexItem, boolean z) {
        if (z) {
            return flexItem.x0();
        }
        return flexItem.m1();
    }

    public final int C(FlexItem flexItem, boolean z) {
        if (z) {
            return flexItem.m1();
        }
        return flexItem.x0();
    }

    public final int D(FlexItem flexItem, boolean z) {
        if (z) {
            return flexItem.K0();
        }
        return flexItem.B0();
    }

    public final int E(FlexItem flexItem, boolean z) {
        if (z) {
            return flexItem.B0();
        }
        return flexItem.K0();
    }

    public final int F(FlexItem flexItem, boolean z) {
        if (z) {
            return flexItem.getHeight();
        }
        return flexItem.getWidth();
    }

    public final int G(FlexItem flexItem, boolean z) {
        if (z) {
            return flexItem.getWidth();
        }
        return flexItem.getHeight();
    }

    public final int H(boolean z) {
        if (z) {
            return this.a.getPaddingBottom();
        }
        return this.a.getPaddingEnd();
    }

    public final int I(boolean z) {
        if (z) {
            return this.a.getPaddingEnd();
        }
        return this.a.getPaddingBottom();
    }

    public final int J(boolean z) {
        if (z) {
            return this.a.getPaddingTop();
        }
        return this.a.getPaddingStart();
    }

    public final int K(boolean z) {
        if (z) {
            return this.a.getPaddingStart();
        }
        return this.a.getPaddingTop();
    }

    public final int L(View view, boolean z) {
        if (z) {
            return view.getMeasuredHeight();
        }
        return view.getMeasuredWidth();
    }

    public final int M(View view, boolean z) {
        if (z) {
            return view.getMeasuredWidth();
        }
        return view.getMeasuredHeight();
    }

    public final boolean N(int i, int i2, com.google.android.flexbox.a aVar) {
        return i == i2 - 1 && aVar.c() != 0;
    }

    public boolean O(SparseIntArray sparseIntArray) {
        int flexItemCount = this.a.getFlexItemCount();
        if (sparseIntArray.size() != flexItemCount) {
            return true;
        }
        for (int i = 0; i < flexItemCount; i++) {
            View h = this.a.h(i);
            if (h != null && ((FlexItem) h.getLayoutParams()).getOrder() != sparseIntArray.get(i)) {
                return true;
            }
        }
        return false;
    }

    public final boolean P(View view, int i, int i2, int i3, int i4, FlexItem flexItem, int i5, int i6, int i7) {
        if (this.a.getFlexWrap() == 0) {
            return false;
        }
        if (flexItem.r1()) {
            return true;
        }
        if (i == 0) {
            return false;
        }
        int maxLine = this.a.getMaxLine();
        if (maxLine == -1 || maxLine > i7 + 1) {
            int i8 = this.a.i(view, i5, i6);
            if (i8 > 0) {
                i4 += i8;
            }
            return i2 < i3 + i4;
        }
        return false;
    }

    public void Q(View view, com.google.android.flexbox.a aVar, int i, int i2, int i3, int i4) {
        FlexItem flexItem = (FlexItem) view.getLayoutParams();
        int alignItems = this.a.getAlignItems();
        if (flexItem.a0() != -1) {
            alignItems = flexItem.a0();
        }
        int i5 = aVar.g;
        if (alignItems != 0) {
            if (alignItems == 1) {
                if (this.a.getFlexWrap() != 2) {
                    int i6 = i2 + i5;
                    view.layout(i, (i6 - view.getMeasuredHeight()) - flexItem.x0(), i3, i6 - flexItem.x0());
                    return;
                }
                view.layout(i, (i2 - i5) + view.getMeasuredHeight() + flexItem.K0(), i3, (i4 - i5) + view.getMeasuredHeight() + flexItem.K0());
                return;
            } else if (alignItems == 2) {
                int measuredHeight = (((i5 - view.getMeasuredHeight()) + flexItem.K0()) - flexItem.x0()) / 2;
                if (this.a.getFlexWrap() != 2) {
                    int i7 = i2 + measuredHeight;
                    view.layout(i, i7, i3, view.getMeasuredHeight() + i7);
                    return;
                }
                int i8 = i2 - measuredHeight;
                view.layout(i, i8, i3, view.getMeasuredHeight() + i8);
                return;
            } else if (alignItems == 3) {
                if (this.a.getFlexWrap() != 2) {
                    int max = Math.max(aVar.l - view.getBaseline(), flexItem.K0());
                    view.layout(i, i2 + max, i3, i4 + max);
                    return;
                }
                int max2 = Math.max((aVar.l - view.getMeasuredHeight()) + view.getBaseline(), flexItem.x0());
                view.layout(i, i2 - max2, i3, i4 - max2);
                return;
            } else if (alignItems != 4) {
                return;
            }
        }
        if (this.a.getFlexWrap() != 2) {
            view.layout(i, i2 + flexItem.K0(), i3, i4 + flexItem.K0());
        } else {
            view.layout(i, i2 - flexItem.x0(), i3, i4 - flexItem.x0());
        }
    }

    public void R(View view, com.google.android.flexbox.a aVar, boolean z, int i, int i2, int i3, int i4) {
        FlexItem flexItem = (FlexItem) view.getLayoutParams();
        int alignItems = this.a.getAlignItems();
        if (flexItem.a0() != -1) {
            alignItems = flexItem.a0();
        }
        int i5 = aVar.g;
        if (alignItems != 0) {
            if (alignItems == 1) {
                if (!z) {
                    view.layout(((i + i5) - view.getMeasuredWidth()) - flexItem.m1(), i2, ((i3 + i5) - view.getMeasuredWidth()) - flexItem.m1(), i4);
                    return;
                } else {
                    view.layout((i - i5) + view.getMeasuredWidth() + flexItem.B0(), i2, (i3 - i5) + view.getMeasuredWidth() + flexItem.B0(), i4);
                    return;
                }
            } else if (alignItems == 2) {
                ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
                int measuredWidth = (((i5 - view.getMeasuredWidth()) + b42.b(marginLayoutParams)) - b42.a(marginLayoutParams)) / 2;
                if (!z) {
                    view.layout(i + measuredWidth, i2, i3 + measuredWidth, i4);
                    return;
                } else {
                    view.layout(i - measuredWidth, i2, i3 - measuredWidth, i4);
                    return;
                }
            } else if (alignItems != 3 && alignItems != 4) {
                return;
            }
        }
        if (!z) {
            view.layout(i + flexItem.B0(), i2, i3 + flexItem.B0(), i4);
        } else {
            view.layout(i - flexItem.m1(), i2, i3 - flexItem.m1(), i4);
        }
    }

    public long S(int i, int i2) {
        return (i & 4294967295L) | (i2 << 32);
    }

    public final void T(int i, int i2, com.google.android.flexbox.a aVar, int i3, int i4, boolean z) {
        int i5;
        int i6;
        int i7;
        int i8 = aVar.e;
        float f = aVar.k;
        float f2 = Utils.FLOAT_EPSILON;
        if (f <= Utils.FLOAT_EPSILON || i3 > i8) {
            return;
        }
        float f3 = (i8 - i3) / f;
        aVar.e = i4 + aVar.f;
        if (!z) {
            aVar.g = Integer.MIN_VALUE;
        }
        int i9 = 0;
        boolean z2 = false;
        int i10 = 0;
        float f4 = 0.0f;
        while (i9 < aVar.h) {
            int i11 = aVar.o + i9;
            View d = this.a.d(i11);
            if (d == null || d.getVisibility() == 8) {
                i5 = i8;
                i6 = i9;
            } else {
                FlexItem flexItem = (FlexItem) d.getLayoutParams();
                int flexDirection = this.a.getFlexDirection();
                if (flexDirection != 0 && flexDirection != 1) {
                    int measuredHeight = d.getMeasuredHeight();
                    long[] jArr = this.e;
                    if (jArr != null) {
                        measuredHeight = x(jArr[i11]);
                    }
                    int measuredWidth = d.getMeasuredWidth();
                    long[] jArr2 = this.e;
                    if (jArr2 != null) {
                        measuredWidth = y(jArr2[i11]);
                    }
                    if (this.b[i11] || flexItem.e0() <= f2) {
                        i5 = i8;
                        i6 = i9;
                    } else {
                        float e0 = measuredHeight - (flexItem.e0() * f3);
                        if (i9 == aVar.h - 1) {
                            e0 += f4;
                            f4 = f2;
                        }
                        int round = Math.round(e0);
                        if (round < flexItem.p1()) {
                            round = flexItem.p1();
                            this.b[i11] = true;
                            aVar.k -= flexItem.e0();
                            i5 = i8;
                            i6 = i9;
                            z2 = true;
                        } else {
                            f4 += e0 - round;
                            i5 = i8;
                            i6 = i9;
                            double d2 = f4;
                            if (d2 > 1.0d) {
                                round++;
                                f4 -= 1.0f;
                            } else if (d2 < -1.0d) {
                                round--;
                                f4 += 1.0f;
                            }
                        }
                        int A = A(i, flexItem, aVar.m);
                        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(round, 1073741824);
                        d.measure(A, makeMeasureSpec);
                        measuredWidth = d.getMeasuredWidth();
                        int measuredHeight2 = d.getMeasuredHeight();
                        Z(i11, A, makeMeasureSpec, d);
                        this.a.f(i11, d);
                        measuredHeight = measuredHeight2;
                    }
                    i7 = Math.max(i10, measuredWidth + flexItem.B0() + flexItem.m1() + this.a.l(d));
                    aVar.e += measuredHeight + flexItem.K0() + flexItem.x0();
                } else {
                    i5 = i8;
                    int i12 = i9;
                    int measuredWidth2 = d.getMeasuredWidth();
                    long[] jArr3 = this.e;
                    if (jArr3 != null) {
                        measuredWidth2 = y(jArr3[i11]);
                    }
                    int measuredHeight3 = d.getMeasuredHeight();
                    long[] jArr4 = this.e;
                    if (jArr4 != null) {
                        measuredHeight3 = x(jArr4[i11]);
                    }
                    if (this.b[i11] || flexItem.e0() <= Utils.FLOAT_EPSILON) {
                        i6 = i12;
                    } else {
                        float e02 = measuredWidth2 - (flexItem.e0() * f3);
                        i6 = i12;
                        if (i6 == aVar.h - 1) {
                            e02 += f4;
                            f4 = 0.0f;
                        }
                        int round2 = Math.round(e02);
                        if (round2 < flexItem.m0()) {
                            round2 = flexItem.m0();
                            this.b[i11] = true;
                            aVar.k -= flexItem.e0();
                            z2 = true;
                        } else {
                            f4 += e02 - round2;
                            double d3 = f4;
                            if (d3 > 1.0d) {
                                round2++;
                                f4 -= 1.0f;
                            } else if (d3 < -1.0d) {
                                round2--;
                                f4 += 1.0f;
                            }
                        }
                        int z3 = z(i2, flexItem, aVar.m);
                        int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(round2, 1073741824);
                        d.measure(makeMeasureSpec2, z3);
                        int measuredWidth3 = d.getMeasuredWidth();
                        int measuredHeight4 = d.getMeasuredHeight();
                        Z(i11, makeMeasureSpec2, z3, d);
                        this.a.f(i11, d);
                        measuredWidth2 = measuredWidth3;
                        measuredHeight3 = measuredHeight4;
                    }
                    int max = Math.max(i10, measuredHeight3 + flexItem.K0() + flexItem.x0() + this.a.l(d));
                    aVar.e += measuredWidth2 + flexItem.B0() + flexItem.m1();
                    i7 = max;
                }
                aVar.g = Math.max(aVar.g, i7);
                i10 = i7;
            }
            i9 = i6 + 1;
            i8 = i5;
            f2 = Utils.FLOAT_EPSILON;
        }
        int i13 = i8;
        if (!z2 || i13 == aVar.e) {
            return;
        }
        T(i, i2, aVar, i3, i4, true);
    }

    public final int[] U(int i, List<c> list, SparseIntArray sparseIntArray) {
        Collections.sort(list);
        sparseIntArray.clear();
        int[] iArr = new int[i];
        int i2 = 0;
        for (c cVar : list) {
            int i3 = cVar.a;
            iArr[i2] = i3;
            sparseIntArray.append(i3, cVar.f0);
            i2++;
        }
        return iArr;
    }

    public final void V(View view, int i, int i2) {
        int measuredHeight;
        FlexItem flexItem = (FlexItem) view.getLayoutParams();
        int min = Math.min(Math.max(((i - flexItem.B0()) - flexItem.m1()) - this.a.l(view), flexItem.m0()), flexItem.F1());
        long[] jArr = this.e;
        if (jArr != null) {
            measuredHeight = x(jArr[i2]);
        } else {
            measuredHeight = view.getMeasuredHeight();
        }
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(measuredHeight, 1073741824);
        int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(min, 1073741824);
        view.measure(makeMeasureSpec2, makeMeasureSpec);
        Z(i2, makeMeasureSpec2, makeMeasureSpec, view);
        this.a.f(i2, view);
    }

    public final void W(View view, int i, int i2) {
        int measuredWidth;
        FlexItem flexItem = (FlexItem) view.getLayoutParams();
        int min = Math.min(Math.max(((i - flexItem.K0()) - flexItem.x0()) - this.a.l(view), flexItem.p1()), flexItem.w1());
        long[] jArr = this.e;
        if (jArr != null) {
            measuredWidth = y(jArr[i2]);
        } else {
            measuredWidth = view.getMeasuredWidth();
        }
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(measuredWidth, 1073741824);
        int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(min, 1073741824);
        view.measure(makeMeasureSpec, makeMeasureSpec2);
        Z(i2, makeMeasureSpec, makeMeasureSpec2, view);
        this.a.f(i2, view);
    }

    public void X() {
        Y(0);
    }

    public void Y(int i) {
        View d;
        if (i >= this.a.getFlexItemCount()) {
            return;
        }
        int flexDirection = this.a.getFlexDirection();
        if (this.a.getAlignItems() == 4) {
            int[] iArr = this.c;
            List<com.google.android.flexbox.a> flexLinesInternal = this.a.getFlexLinesInternal();
            int size = flexLinesInternal.size();
            for (int i2 = iArr != null ? iArr[i] : 0; i2 < size; i2++) {
                com.google.android.flexbox.a aVar = flexLinesInternal.get(i2);
                int i3 = aVar.h;
                for (int i4 = 0; i4 < i3; i4++) {
                    int i5 = aVar.o + i4;
                    if (i4 < this.a.getFlexItemCount() && (d = this.a.d(i5)) != null && d.getVisibility() != 8) {
                        FlexItem flexItem = (FlexItem) d.getLayoutParams();
                        if (flexItem.a0() == -1 || flexItem.a0() == 4) {
                            if (flexDirection == 0 || flexDirection == 1) {
                                W(d, aVar.g, i5);
                            } else if (flexDirection != 2 && flexDirection != 3) {
                                throw new IllegalArgumentException("Invalid flex direction: " + flexDirection);
                            } else {
                                V(d, aVar.g, i5);
                            }
                        }
                    }
                }
            }
            return;
        }
        for (com.google.android.flexbox.a aVar2 : this.a.getFlexLinesInternal()) {
            for (Integer num : aVar2.n) {
                View d2 = this.a.d(num.intValue());
                if (flexDirection == 0 || flexDirection == 1) {
                    W(d2, aVar2.g, num.intValue());
                } else if (flexDirection != 2 && flexDirection != 3) {
                    throw new IllegalArgumentException("Invalid flex direction: " + flexDirection);
                } else {
                    V(d2, aVar2.g, num.intValue());
                }
            }
        }
    }

    public final void Z(int i, int i2, int i3, View view) {
        long[] jArr = this.d;
        if (jArr != null) {
            jArr[i] = S(i2, i3);
        }
        long[] jArr2 = this.e;
        if (jArr2 != null) {
            jArr2[i] = S(view.getMeasuredWidth(), view.getMeasuredHeight());
        }
    }

    public final void a(List<com.google.android.flexbox.a> list, com.google.android.flexbox.a aVar, int i, int i2) {
        aVar.m = i2;
        this.a.c(aVar);
        aVar.p = i;
        list.add(aVar);
    }

    public void b(C0103b c0103b, int i, int i2, int i3, int i4, int i5, List<com.google.android.flexbox.a> list) {
        int i6;
        C0103b c0103b2;
        int i7;
        int i8;
        int i9;
        List<com.google.android.flexbox.a> list2;
        int i10;
        View view;
        int i11;
        int i12;
        int i13;
        int i14;
        int i15;
        int i16;
        int i17 = i;
        int i18 = i2;
        int i19 = i5;
        boolean k = this.a.k();
        int mode = View.MeasureSpec.getMode(i);
        int size = View.MeasureSpec.getSize(i);
        ArrayList arrayList = list == null ? new ArrayList() : list;
        c0103b.a = arrayList;
        boolean z = i19 == -1;
        int K = K(k);
        int I = I(k);
        int J = J(k);
        int H = H(k);
        com.google.android.flexbox.a aVar = new com.google.android.flexbox.a();
        int i20 = i4;
        aVar.o = i20;
        int i21 = I + K;
        aVar.e = i21;
        int flexItemCount = this.a.getFlexItemCount();
        boolean z2 = z;
        int i22 = Integer.MIN_VALUE;
        int i23 = 0;
        int i24 = 0;
        int i25 = 0;
        while (true) {
            if (i20 >= flexItemCount) {
                i6 = i24;
                c0103b2 = c0103b;
                break;
            }
            View d = this.a.d(i20);
            if (d == null) {
                if (N(i20, flexItemCount, aVar)) {
                    a(arrayList, aVar, i20, i23);
                }
            } else if (d.getVisibility() == 8) {
                aVar.i++;
                aVar.h++;
                if (N(i20, flexItemCount, aVar)) {
                    a(arrayList, aVar, i20, i23);
                }
            } else {
                if (d instanceof CompoundButton) {
                    v((CompoundButton) d);
                }
                FlexItem flexItem = (FlexItem) d.getLayoutParams();
                int i26 = flexItemCount;
                if (flexItem.a0() == 4) {
                    aVar.n.add(Integer.valueOf(i20));
                }
                int G = G(flexItem, k);
                if (flexItem.X0() != -1.0f && mode == 1073741824) {
                    G = Math.round(size * flexItem.X0());
                }
                if (k) {
                    int e = this.a.e(i17, i21 + E(flexItem, true) + C(flexItem, true), G);
                    i7 = size;
                    i8 = mode;
                    int j = this.a.j(i18, J + H + D(flexItem, true) + B(flexItem, true) + i23, F(flexItem, true));
                    d.measure(e, j);
                    Z(i20, e, j, d);
                    i9 = e;
                } else {
                    i7 = size;
                    i8 = mode;
                    int e2 = this.a.e(i18, J + H + D(flexItem, false) + B(flexItem, false) + i23, F(flexItem, false));
                    int j2 = this.a.j(i17, E(flexItem, false) + i21 + C(flexItem, false), G);
                    d.measure(e2, j2);
                    Z(i20, e2, j2, d);
                    i9 = j2;
                }
                this.a.f(i20, d);
                i(d, i20);
                i24 = View.combineMeasuredStates(i24, d.getMeasuredState());
                int i27 = i23;
                int i28 = i21;
                com.google.android.flexbox.a aVar2 = aVar;
                int i29 = i20;
                list2 = arrayList;
                int i30 = i9;
                if (P(d, i8, i7, aVar.e, C(flexItem, k) + M(d, k) + E(flexItem, k), flexItem, i29, i25, arrayList.size())) {
                    if (aVar2.c() > 0) {
                        a(list2, aVar2, i29 > 0 ? i29 - 1 : 0, i27);
                        i23 = aVar2.g + i27;
                    } else {
                        i23 = i27;
                    }
                    if (k) {
                        if (flexItem.getHeight() == -1) {
                            z61 z61Var = this.a;
                            i10 = i2;
                            i20 = i29;
                            view = d;
                            view.measure(i30, z61Var.j(i10, z61Var.getPaddingTop() + this.a.getPaddingBottom() + flexItem.K0() + flexItem.x0() + i23, flexItem.getHeight()));
                            i(view, i20);
                        } else {
                            i10 = i2;
                            view = d;
                            i20 = i29;
                        }
                    } else {
                        i10 = i2;
                        view = d;
                        i20 = i29;
                        if (flexItem.getWidth() == -1) {
                            z61 z61Var2 = this.a;
                            view.measure(z61Var2.e(i10, z61Var2.getPaddingLeft() + this.a.getPaddingRight() + flexItem.B0() + flexItem.m1() + i23, flexItem.getWidth()), i30);
                            i(view, i20);
                        }
                    }
                    aVar = new com.google.android.flexbox.a();
                    aVar.h = 1;
                    i11 = i28;
                    aVar.e = i11;
                    aVar.o = i20;
                    i13 = Integer.MIN_VALUE;
                    i12 = 0;
                } else {
                    i10 = i2;
                    view = d;
                    i20 = i29;
                    aVar = aVar2;
                    i11 = i28;
                    aVar.h++;
                    i12 = i25 + 1;
                    i23 = i27;
                    i13 = i22;
                }
                aVar.q |= flexItem.S0() != Utils.FLOAT_EPSILON;
                aVar.r |= flexItem.e0() != Utils.FLOAT_EPSILON;
                int[] iArr = this.c;
                if (iArr != null) {
                    iArr[i20] = list2.size();
                }
                aVar.e += M(view, k) + E(flexItem, k) + C(flexItem, k);
                aVar.j += flexItem.S0();
                aVar.k += flexItem.e0();
                this.a.b(view, i20, i12, aVar);
                int max = Math.max(i13, L(view, k) + D(flexItem, k) + B(flexItem, k) + this.a.l(view));
                aVar.g = Math.max(aVar.g, max);
                if (k) {
                    if (this.a.getFlexWrap() != 2) {
                        aVar.l = Math.max(aVar.l, view.getBaseline() + flexItem.K0());
                    } else {
                        aVar.l = Math.max(aVar.l, (view.getMeasuredHeight() - view.getBaseline()) + flexItem.x0());
                    }
                }
                i14 = i26;
                if (N(i20, i14, aVar)) {
                    a(list2, aVar, i20, i23);
                    i23 += aVar.g;
                }
                i15 = i5;
                if (i15 != -1 && list2.size() > 0) {
                    if (list2.get(list2.size() - 1).p >= i15 && i20 >= i15 && !z2) {
                        i23 = -aVar.a();
                        i16 = i3;
                        z2 = true;
                        if (i23 <= i16 && z2) {
                            c0103b2 = c0103b;
                            i6 = i24;
                            break;
                        }
                        i25 = i12;
                        i22 = max;
                        i20++;
                        i17 = i;
                        flexItemCount = i14;
                        i18 = i10;
                        i21 = i11;
                        arrayList = list2;
                        mode = i8;
                        i19 = i15;
                        size = i7;
                    }
                }
                i16 = i3;
                if (i23 <= i16) {
                }
                i25 = i12;
                i22 = max;
                i20++;
                i17 = i;
                flexItemCount = i14;
                i18 = i10;
                i21 = i11;
                arrayList = list2;
                mode = i8;
                i19 = i15;
                size = i7;
            }
            i7 = size;
            i8 = mode;
            i10 = i18;
            i15 = i19;
            list2 = arrayList;
            i11 = i21;
            i14 = flexItemCount;
            i20++;
            i17 = i;
            flexItemCount = i14;
            i18 = i10;
            i21 = i11;
            arrayList = list2;
            mode = i8;
            i19 = i15;
            size = i7;
        }
        c0103b2.b = i6;
    }

    public void c(C0103b c0103b, int i, int i2) {
        b(c0103b, i, i2, Integer.MAX_VALUE, 0, -1, null);
    }

    public void d(C0103b c0103b, int i, int i2, int i3, int i4, List<com.google.android.flexbox.a> list) {
        b(c0103b, i, i2, i3, i4, -1, list);
    }

    public void e(C0103b c0103b, int i, int i2, int i3, int i4, List<com.google.android.flexbox.a> list) {
        b(c0103b, i, i2, i3, 0, i4, list);
    }

    public void f(C0103b c0103b, int i, int i2) {
        b(c0103b, i2, i, Integer.MAX_VALUE, 0, -1, null);
    }

    public void g(C0103b c0103b, int i, int i2, int i3, int i4, List<com.google.android.flexbox.a> list) {
        b(c0103b, i2, i, i3, i4, -1, list);
    }

    public void h(C0103b c0103b, int i, int i2, int i3, int i4, List<com.google.android.flexbox.a> list) {
        b(c0103b, i2, i, i3, 0, i4, list);
    }

    /* JADX WARN: Removed duplicated region for block: B:12:0x002d  */
    /* JADX WARN: Removed duplicated region for block: B:13:0x0032  */
    /* JADX WARN: Removed duplicated region for block: B:18:0x0040  */
    /* JADX WARN: Removed duplicated region for block: B:20:? A[RETURN, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void i(android.view.View r7, int r8) {
        /*
            r6 = this;
            android.view.ViewGroup$LayoutParams r0 = r7.getLayoutParams()
            com.google.android.flexbox.FlexItem r0 = (com.google.android.flexbox.FlexItem) r0
            int r1 = r7.getMeasuredWidth()
            int r2 = r7.getMeasuredHeight()
            int r3 = r0.m0()
            r4 = 1
            if (r1 >= r3) goto L1b
            int r1 = r0.m0()
        L19:
            r3 = r4
            goto L27
        L1b:
            int r3 = r0.F1()
            if (r1 <= r3) goto L26
            int r1 = r0.F1()
            goto L19
        L26:
            r3 = 0
        L27:
            int r5 = r0.p1()
            if (r2 >= r5) goto L32
            int r2 = r0.p1()
            goto L3e
        L32:
            int r5 = r0.w1()
            if (r2 <= r5) goto L3d
            int r2 = r0.w1()
            goto L3e
        L3d:
            r4 = r3
        L3e:
            if (r4 == 0) goto L55
            r0 = 1073741824(0x40000000, float:2.0)
            int r1 = android.view.View.MeasureSpec.makeMeasureSpec(r1, r0)
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r2, r0)
            r7.measure(r1, r0)
            r6.Z(r8, r1, r0, r7)
            z61 r0 = r6.a
            r0.f(r8, r7)
        L55:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.flexbox.b.i(android.view.View, int):void");
    }

    public void j(List<com.google.android.flexbox.a> list, int i) {
        int i2 = this.c[i];
        if (i2 == -1) {
            i2 = 0;
        }
        if (list.size() > i2) {
            list.subList(i2, list.size()).clear();
        }
        int[] iArr = this.c;
        int length = iArr.length - 1;
        if (i > length) {
            Arrays.fill(iArr, -1);
        } else {
            Arrays.fill(iArr, i, length, -1);
        }
        long[] jArr = this.d;
        int length2 = jArr.length - 1;
        if (i > length2) {
            Arrays.fill(jArr, 0L);
        } else {
            Arrays.fill(jArr, i, length2, 0L);
        }
    }

    public final List<com.google.android.flexbox.a> k(List<com.google.android.flexbox.a> list, int i, int i2) {
        ArrayList arrayList = new ArrayList();
        com.google.android.flexbox.a aVar = new com.google.android.flexbox.a();
        aVar.g = (i - i2) / 2;
        int size = list.size();
        for (int i3 = 0; i3 < size; i3++) {
            if (i3 == 0) {
                arrayList.add(aVar);
            }
            arrayList.add(list.get(i3));
            if (i3 == list.size() - 1) {
                arrayList.add(aVar);
            }
        }
        return arrayList;
    }

    public final List<c> l(int i) {
        ArrayList arrayList = new ArrayList(i);
        for (int i2 = 0; i2 < i; i2++) {
            c cVar = new c();
            cVar.f0 = ((FlexItem) this.a.h(i2).getLayoutParams()).getOrder();
            cVar.a = i2;
            arrayList.add(cVar);
        }
        return arrayList;
    }

    public int[] m(SparseIntArray sparseIntArray) {
        int flexItemCount = this.a.getFlexItemCount();
        return U(flexItemCount, l(flexItemCount), sparseIntArray);
    }

    public int[] n(View view, int i, ViewGroup.LayoutParams layoutParams, SparseIntArray sparseIntArray) {
        int flexItemCount = this.a.getFlexItemCount();
        List<c> l = l(flexItemCount);
        c cVar = new c();
        if (view != null && (layoutParams instanceof FlexItem)) {
            cVar.f0 = ((FlexItem) layoutParams).getOrder();
        } else {
            cVar.f0 = 1;
        }
        if (i != -1 && i != flexItemCount) {
            if (i < this.a.getFlexItemCount()) {
                cVar.a = i;
                while (i < flexItemCount) {
                    l.get(i).a++;
                    i++;
                }
            } else {
                cVar.a = flexItemCount;
            }
        } else {
            cVar.a = flexItemCount;
        }
        l.add(cVar);
        return U(flexItemCount + 1, l, sparseIntArray);
    }

    public void o(int i, int i2, int i3) {
        int i4;
        int i5;
        int flexDirection = this.a.getFlexDirection();
        if (flexDirection == 0 || flexDirection == 1) {
            int mode = View.MeasureSpec.getMode(i2);
            int size = View.MeasureSpec.getSize(i2);
            i4 = mode;
            i5 = size;
        } else if (flexDirection != 2 && flexDirection != 3) {
            throw new IllegalArgumentException("Invalid flex direction: " + flexDirection);
        } else {
            i4 = View.MeasureSpec.getMode(i);
            i5 = View.MeasureSpec.getSize(i);
        }
        List<com.google.android.flexbox.a> flexLinesInternal = this.a.getFlexLinesInternal();
        if (i4 == 1073741824) {
            int sumOfCrossSize = this.a.getSumOfCrossSize() + i3;
            int i6 = 0;
            if (flexLinesInternal.size() == 1) {
                flexLinesInternal.get(0).g = i5 - i3;
            } else if (flexLinesInternal.size() >= 2) {
                int alignContent = this.a.getAlignContent();
                if (alignContent == 1) {
                    int i7 = i5 - sumOfCrossSize;
                    com.google.android.flexbox.a aVar = new com.google.android.flexbox.a();
                    aVar.g = i7;
                    flexLinesInternal.add(0, aVar);
                } else if (alignContent == 2) {
                    this.a.setFlexLines(k(flexLinesInternal, i5, sumOfCrossSize));
                } else if (alignContent == 3) {
                    if (sumOfCrossSize >= i5) {
                        return;
                    }
                    float size2 = (i5 - sumOfCrossSize) / (flexLinesInternal.size() - 1);
                    ArrayList arrayList = new ArrayList();
                    int size3 = flexLinesInternal.size();
                    float f = 0.0f;
                    while (i6 < size3) {
                        arrayList.add(flexLinesInternal.get(i6));
                        if (i6 != flexLinesInternal.size() - 1) {
                            com.google.android.flexbox.a aVar2 = new com.google.android.flexbox.a();
                            if (i6 == flexLinesInternal.size() - 2) {
                                aVar2.g = Math.round(f + size2);
                                f = 0.0f;
                            } else {
                                aVar2.g = Math.round(size2);
                            }
                            int i8 = aVar2.g;
                            f += size2 - i8;
                            if (f > 1.0f) {
                                aVar2.g = i8 + 1;
                                f -= 1.0f;
                            } else if (f < -1.0f) {
                                aVar2.g = i8 - 1;
                                f += 1.0f;
                            }
                            arrayList.add(aVar2);
                        }
                        i6++;
                    }
                    this.a.setFlexLines(arrayList);
                } else if (alignContent == 4) {
                    if (sumOfCrossSize >= i5) {
                        this.a.setFlexLines(k(flexLinesInternal, i5, sumOfCrossSize));
                        return;
                    }
                    int size4 = (i5 - sumOfCrossSize) / (flexLinesInternal.size() * 2);
                    ArrayList arrayList2 = new ArrayList();
                    com.google.android.flexbox.a aVar3 = new com.google.android.flexbox.a();
                    aVar3.g = size4;
                    for (com.google.android.flexbox.a aVar4 : flexLinesInternal) {
                        arrayList2.add(aVar3);
                        arrayList2.add(aVar4);
                        arrayList2.add(aVar3);
                    }
                    this.a.setFlexLines(arrayList2);
                } else if (alignContent == 5 && sumOfCrossSize < i5) {
                    float size5 = (i5 - sumOfCrossSize) / flexLinesInternal.size();
                    int size6 = flexLinesInternal.size();
                    float f2 = 0.0f;
                    while (i6 < size6) {
                        com.google.android.flexbox.a aVar5 = flexLinesInternal.get(i6);
                        float f3 = aVar5.g + size5;
                        if (i6 == flexLinesInternal.size() - 1) {
                            f3 += f2;
                            f2 = 0.0f;
                        }
                        int round = Math.round(f3);
                        f2 += f3 - round;
                        if (f2 > 1.0f) {
                            round++;
                            f2 -= 1.0f;
                        } else if (f2 < -1.0f) {
                            round--;
                            f2 += 1.0f;
                        }
                        aVar5.g = round;
                        i6++;
                    }
                }
            }
        }
    }

    public void p(int i, int i2) {
        q(i, i2, 0);
    }

    public void q(int i, int i2, int i3) {
        int size;
        int paddingLeft;
        int paddingRight;
        r(this.a.getFlexItemCount());
        if (i3 >= this.a.getFlexItemCount()) {
            return;
        }
        int flexDirection = this.a.getFlexDirection();
        int flexDirection2 = this.a.getFlexDirection();
        if (flexDirection2 == 0 || flexDirection2 == 1) {
            int mode = View.MeasureSpec.getMode(i);
            size = View.MeasureSpec.getSize(i);
            int largestMainSize = this.a.getLargestMainSize();
            if (mode != 1073741824) {
                size = Math.min(largestMainSize, size);
            }
            paddingLeft = this.a.getPaddingLeft();
            paddingRight = this.a.getPaddingRight();
        } else if (flexDirection2 != 2 && flexDirection2 != 3) {
            throw new IllegalArgumentException("Invalid flex direction: " + flexDirection);
        } else {
            int mode2 = View.MeasureSpec.getMode(i2);
            size = View.MeasureSpec.getSize(i2);
            if (mode2 != 1073741824) {
                size = this.a.getLargestMainSize();
            }
            paddingLeft = this.a.getPaddingTop();
            paddingRight = this.a.getPaddingBottom();
        }
        int i4 = paddingLeft + paddingRight;
        int[] iArr = this.c;
        int i5 = iArr != null ? iArr[i3] : 0;
        List<com.google.android.flexbox.a> flexLinesInternal = this.a.getFlexLinesInternal();
        int size2 = flexLinesInternal.size();
        for (int i6 = i5; i6 < size2; i6++) {
            com.google.android.flexbox.a aVar = flexLinesInternal.get(i6);
            int i7 = aVar.e;
            if (i7 < size && aVar.q) {
                w(i, i2, aVar, size, i4, false);
            } else if (i7 > size && aVar.r) {
                T(i, i2, aVar, size, i4, false);
            }
        }
    }

    public final void r(int i) {
        boolean[] zArr = this.b;
        if (zArr == null) {
            this.b = new boolean[Math.max(i, 10)];
        } else if (zArr.length < i) {
            this.b = new boolean[Math.max(zArr.length * 2, i)];
        } else {
            Arrays.fill(zArr, false);
        }
    }

    public void s(int i) {
        int[] iArr = this.c;
        if (iArr == null) {
            this.c = new int[Math.max(i, 10)];
        } else if (iArr.length < i) {
            this.c = Arrays.copyOf(this.c, Math.max(iArr.length * 2, i));
        }
    }

    public void t(int i) {
        long[] jArr = this.d;
        if (jArr == null) {
            this.d = new long[Math.max(i, 10)];
        } else if (jArr.length < i) {
            this.d = Arrays.copyOf(this.d, Math.max(jArr.length * 2, i));
        }
    }

    public void u(int i) {
        long[] jArr = this.e;
        if (jArr == null) {
            this.e = new long[Math.max(i, 10)];
        } else if (jArr.length < i) {
            this.e = Arrays.copyOf(this.e, Math.max(jArr.length * 2, i));
        }
    }

    public final void v(CompoundButton compoundButton) {
        FlexItem flexItem = (FlexItem) compoundButton.getLayoutParams();
        int m0 = flexItem.m0();
        int p1 = flexItem.p1();
        Drawable a2 = r40.a(compoundButton);
        int minimumWidth = a2 == null ? 0 : a2.getMinimumWidth();
        int minimumHeight = a2 != null ? a2.getMinimumHeight() : 0;
        if (m0 == -1) {
            m0 = minimumWidth;
        }
        flexItem.w0(m0);
        if (p1 == -1) {
            p1 = minimumHeight;
        }
        flexItem.O0(p1);
    }

    public final void w(int i, int i2, com.google.android.flexbox.a aVar, int i3, int i4, boolean z) {
        int i5;
        int i6;
        int i7;
        double d;
        int i8;
        double d2;
        float f = aVar.j;
        float f2 = Utils.FLOAT_EPSILON;
        if (f <= Utils.FLOAT_EPSILON || i3 < (i5 = aVar.e)) {
            return;
        }
        float f3 = (i3 - i5) / f;
        aVar.e = i4 + aVar.f;
        if (!z) {
            aVar.g = Integer.MIN_VALUE;
        }
        int i9 = 0;
        boolean z2 = false;
        int i10 = 0;
        float f4 = 0.0f;
        while (i9 < aVar.h) {
            int i11 = aVar.o + i9;
            View d3 = this.a.d(i11);
            if (d3 == null || d3.getVisibility() == 8) {
                i6 = i5;
            } else {
                FlexItem flexItem = (FlexItem) d3.getLayoutParams();
                int flexDirection = this.a.getFlexDirection();
                if (flexDirection != 0 && flexDirection != 1) {
                    int measuredHeight = d3.getMeasuredHeight();
                    long[] jArr = this.e;
                    if (jArr != null) {
                        measuredHeight = x(jArr[i11]);
                    }
                    int measuredWidth = d3.getMeasuredWidth();
                    long[] jArr2 = this.e;
                    if (jArr2 != null) {
                        measuredWidth = y(jArr2[i11]);
                    }
                    if (this.b[i11] || flexItem.S0() <= f2) {
                        i8 = i5;
                    } else {
                        float S0 = measuredHeight + (flexItem.S0() * f3);
                        if (i9 == aVar.h - 1) {
                            S0 += f4;
                            f4 = f2;
                        }
                        int round = Math.round(S0);
                        if (round > flexItem.w1()) {
                            round = flexItem.w1();
                            this.b[i11] = true;
                            aVar.j -= flexItem.S0();
                            i8 = i5;
                            z2 = true;
                        } else {
                            f4 += S0 - round;
                            i8 = i5;
                            double d4 = f4;
                            if (d4 > 1.0d) {
                                round++;
                                d2 = d4 - 1.0d;
                            } else if (d4 < -1.0d) {
                                round--;
                                d2 = d4 + 1.0d;
                            }
                            f4 = (float) d2;
                        }
                        int A = A(i, flexItem, aVar.m);
                        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(round, 1073741824);
                        d3.measure(A, makeMeasureSpec);
                        measuredWidth = d3.getMeasuredWidth();
                        int measuredHeight2 = d3.getMeasuredHeight();
                        Z(i11, A, makeMeasureSpec, d3);
                        this.a.f(i11, d3);
                        measuredHeight = measuredHeight2;
                    }
                    i7 = Math.max(i10, measuredWidth + flexItem.B0() + flexItem.m1() + this.a.l(d3));
                    aVar.e += measuredHeight + flexItem.K0() + flexItem.x0();
                    i6 = i8;
                } else {
                    int i12 = i5;
                    int measuredWidth2 = d3.getMeasuredWidth();
                    long[] jArr3 = this.e;
                    if (jArr3 != null) {
                        measuredWidth2 = y(jArr3[i11]);
                    }
                    int measuredHeight3 = d3.getMeasuredHeight();
                    long[] jArr4 = this.e;
                    i6 = i12;
                    if (jArr4 != null) {
                        measuredHeight3 = x(jArr4[i11]);
                    }
                    if (!this.b[i11] && flexItem.S0() > Utils.FLOAT_EPSILON) {
                        float S02 = measuredWidth2 + (flexItem.S0() * f3);
                        if (i9 == aVar.h - 1) {
                            S02 += f4;
                            f4 = 0.0f;
                        }
                        int round2 = Math.round(S02);
                        if (round2 > flexItem.F1()) {
                            round2 = flexItem.F1();
                            this.b[i11] = true;
                            aVar.j -= flexItem.S0();
                            z2 = true;
                        } else {
                            f4 += S02 - round2;
                            double d5 = f4;
                            if (d5 > 1.0d) {
                                round2++;
                                d = d5 - 1.0d;
                            } else if (d5 < -1.0d) {
                                round2--;
                                d = d5 + 1.0d;
                            }
                            f4 = (float) d;
                        }
                        int z3 = z(i2, flexItem, aVar.m);
                        int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(round2, 1073741824);
                        d3.measure(makeMeasureSpec2, z3);
                        int measuredWidth3 = d3.getMeasuredWidth();
                        int measuredHeight4 = d3.getMeasuredHeight();
                        Z(i11, makeMeasureSpec2, z3, d3);
                        this.a.f(i11, d3);
                        measuredWidth2 = measuredWidth3;
                        measuredHeight3 = measuredHeight4;
                    }
                    int max = Math.max(i10, measuredHeight3 + flexItem.K0() + flexItem.x0() + this.a.l(d3));
                    aVar.e += measuredWidth2 + flexItem.B0() + flexItem.m1();
                    i7 = max;
                }
                aVar.g = Math.max(aVar.g, i7);
                i10 = i7;
            }
            i9++;
            i5 = i6;
            f2 = Utils.FLOAT_EPSILON;
        }
        int i13 = i5;
        if (!z2 || i13 == aVar.e) {
            return;
        }
        w(i, i2, aVar, i3, i4, true);
    }

    public int x(long j) {
        return (int) (j >> 32);
    }

    public int y(long j) {
        return (int) j;
    }

    public final int z(int i, FlexItem flexItem, int i2) {
        z61 z61Var = this.a;
        int j = z61Var.j(i, z61Var.getPaddingTop() + this.a.getPaddingBottom() + flexItem.K0() + flexItem.x0() + i2, flexItem.getHeight());
        int size = View.MeasureSpec.getSize(j);
        if (size > flexItem.w1()) {
            return View.MeasureSpec.makeMeasureSpec(flexItem.w1(), View.MeasureSpec.getMode(j));
        }
        return size < flexItem.p1() ? View.MeasureSpec.makeMeasureSpec(flexItem.p1(), View.MeasureSpec.getMode(j)) : j;
    }
}
