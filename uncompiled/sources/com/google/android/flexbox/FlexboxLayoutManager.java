package com.google.android.flexbox;

import android.content.Context;
import android.graphics.PointF;
import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.m;
import androidx.recyclerview.widget.q;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.flexbox.b;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes.dex */
public class FlexboxLayoutManager extends RecyclerView.LayoutManager implements z61, RecyclerView.w.b {
    public static final Rect W0 = new Rect();
    public int A0;
    public boolean B0;
    public boolean C0;
    public List<com.google.android.flexbox.a> D0;
    public final com.google.android.flexbox.b E0;
    public RecyclerView.t F0;
    public RecyclerView.x G0;
    public c H0;
    public b I0;
    public q J0;
    public q K0;
    public SavedState L0;
    public int M0;
    public int N0;
    public int O0;
    public int P0;
    public boolean Q0;
    public SparseArray<View> R0;
    public final Context S0;
    public View T0;
    public int U0;
    public b.C0103b V0;
    public int w0;
    public int x0;
    public int y0;
    public int z0;

    /* loaded from: classes.dex */
    public class b {
        public int a;
        public int b;
        public int c;
        public int d;
        public boolean e;
        public boolean f;
        public boolean g;

        public b() {
            this.d = 0;
        }

        public static /* synthetic */ int l(b bVar, int i) {
            int i2 = bVar.d + i;
            bVar.d = i2;
            return i2;
        }

        public final void r() {
            if (!FlexboxLayoutManager.this.k() && FlexboxLayoutManager.this.B0) {
                this.c = this.e ? FlexboxLayoutManager.this.J0.i() : FlexboxLayoutManager.this.v0() - FlexboxLayoutManager.this.J0.m();
            } else {
                this.c = this.e ? FlexboxLayoutManager.this.J0.i() : FlexboxLayoutManager.this.J0.m();
            }
        }

        public final void s(View view) {
            q qVar = FlexboxLayoutManager.this.x0 == 0 ? FlexboxLayoutManager.this.K0 : FlexboxLayoutManager.this.J0;
            if (!FlexboxLayoutManager.this.k() && FlexboxLayoutManager.this.B0) {
                if (this.e) {
                    this.c = qVar.g(view) + qVar.o();
                } else {
                    this.c = qVar.d(view);
                }
            } else if (this.e) {
                this.c = qVar.d(view) + qVar.o();
            } else {
                this.c = qVar.g(view);
            }
            this.a = FlexboxLayoutManager.this.o0(view);
            this.g = false;
            int[] iArr = FlexboxLayoutManager.this.E0.c;
            int i = this.a;
            if (i == -1) {
                i = 0;
            }
            int i2 = iArr[i];
            this.b = i2 != -1 ? i2 : 0;
            if (FlexboxLayoutManager.this.D0.size() > this.b) {
                this.a = ((com.google.android.flexbox.a) FlexboxLayoutManager.this.D0.get(this.b)).o;
            }
        }

        public final void t() {
            this.a = -1;
            this.b = -1;
            this.c = Integer.MIN_VALUE;
            this.f = false;
            this.g = false;
            if (FlexboxLayoutManager.this.k()) {
                if (FlexboxLayoutManager.this.x0 == 0) {
                    this.e = FlexboxLayoutManager.this.w0 == 1;
                } else {
                    this.e = FlexboxLayoutManager.this.x0 == 2;
                }
            } else if (FlexboxLayoutManager.this.x0 == 0) {
                this.e = FlexboxLayoutManager.this.w0 == 3;
            } else {
                this.e = FlexboxLayoutManager.this.x0 == 2;
            }
        }

        public String toString() {
            return "AnchorInfo{mPosition=" + this.a + ", mFlexLinePosition=" + this.b + ", mCoordinate=" + this.c + ", mPerpendicularCoordinate=" + this.d + ", mLayoutFromEnd=" + this.e + ", mValid=" + this.f + ", mAssignedFromSavedState=" + this.g + '}';
        }
    }

    /* loaded from: classes.dex */
    public static class c {
        public int a;
        public boolean b;
        public int c;
        public int d;
        public int e;
        public int f;
        public int g;
        public int h;
        public int i;
        public boolean j;

        public c() {
            this.h = 1;
            this.i = 1;
        }

        public static /* synthetic */ int c(c cVar, int i) {
            int i2 = cVar.e + i;
            cVar.e = i2;
            return i2;
        }

        public static /* synthetic */ int d(c cVar, int i) {
            int i2 = cVar.e - i;
            cVar.e = i2;
            return i2;
        }

        public static /* synthetic */ int i(c cVar, int i) {
            int i2 = cVar.a - i;
            cVar.a = i2;
            return i2;
        }

        public static /* synthetic */ int l(c cVar) {
            int i = cVar.c;
            cVar.c = i + 1;
            return i;
        }

        public static /* synthetic */ int m(c cVar) {
            int i = cVar.c;
            cVar.c = i - 1;
            return i;
        }

        public static /* synthetic */ int n(c cVar, int i) {
            int i2 = cVar.c + i;
            cVar.c = i2;
            return i2;
        }

        public static /* synthetic */ int q(c cVar, int i) {
            int i2 = cVar.f + i;
            cVar.f = i2;
            return i2;
        }

        public static /* synthetic */ int u(c cVar, int i) {
            int i2 = cVar.d + i;
            cVar.d = i2;
            return i2;
        }

        public static /* synthetic */ int v(c cVar, int i) {
            int i2 = cVar.d - i;
            cVar.d = i2;
            return i2;
        }

        public final boolean D(RecyclerView.x xVar, List<com.google.android.flexbox.a> list) {
            int i;
            int i2 = this.d;
            return i2 >= 0 && i2 < xVar.b() && (i = this.c) >= 0 && i < list.size();
        }

        public String toString() {
            return "LayoutState{mAvailable=" + this.a + ", mFlexLinePosition=" + this.c + ", mPosition=" + this.d + ", mOffset=" + this.e + ", mScrollingOffset=" + this.f + ", mLastScrollDelta=" + this.g + ", mItemDirection=" + this.h + ", mLayoutDirection=" + this.i + '}';
        }
    }

    public FlexboxLayoutManager(Context context, int i) {
        this(context, i, 1);
    }

    public static boolean E0(int i, int i2, int i3) {
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        if (i3 <= 0 || i == i3) {
            if (mode == Integer.MIN_VALUE) {
                return size >= i;
            } else if (mode != 0) {
                return mode == 1073741824 && size == i;
            } else {
                return true;
            }
        }
        return false;
    }

    private boolean O1(View view, int i, int i2, RecyclerView.LayoutParams layoutParams) {
        return (!view.isLayoutRequested() && D0() && E0(view.getWidth(), i, ((ViewGroup.MarginLayoutParams) layoutParams).width) && E0(view.getHeight(), i2, ((ViewGroup.MarginLayoutParams) layoutParams).height)) ? false : true;
    }

    public List<com.google.android.flexbox.a> A2() {
        ArrayList arrayList = new ArrayList(this.D0.size());
        int size = this.D0.size();
        for (int i = 0; i < size; i++) {
            com.google.android.flexbox.a aVar = this.D0.get(i);
            if (aVar.b() != 0) {
                arrayList.add(aVar);
            }
        }
        return arrayList;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public int B(RecyclerView.x xVar) {
        return f2(xVar);
    }

    public int B2(int i) {
        return this.E0.c[i];
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public int C(RecyclerView.x xVar) {
        return g2(xVar);
    }

    public final int C2(int i, RecyclerView.t tVar, RecyclerView.x xVar) {
        if (U() == 0 || i == 0) {
            return 0;
        }
        j2();
        int i2 = 1;
        this.H0.j = true;
        boolean z = !k() && this.B0;
        if (!z ? i <= 0 : i >= 0) {
            i2 = -1;
        }
        int abs = Math.abs(i);
        X2(i2, abs);
        int k2 = this.H0.f + k2(tVar, xVar, this.H0);
        if (k2 < 0) {
            return 0;
        }
        if (z) {
            if (abs > k2) {
                i = (-i2) * k2;
            }
        } else if (abs > k2) {
            i = i2 * k2;
        }
        this.J0.r(-i);
        this.H0.g = i;
        return i;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public int D(RecyclerView.x xVar) {
        return h2(xVar);
    }

    public final int D2(int i) {
        int i2;
        if (U() == 0 || i == 0) {
            return 0;
        }
        j2();
        boolean k = k();
        View view = this.T0;
        int width = k ? view.getWidth() : view.getHeight();
        int v0 = k ? v0() : h0();
        if (k0() == 1) {
            int abs = Math.abs(i);
            if (i >= 0) {
                if (this.I0.d + i <= 0) {
                    return i;
                }
                i2 = this.I0.d;
            } else {
                i2 = Math.min((v0 + this.I0.d) - width, abs);
            }
        } else if (i <= 0) {
            if (this.I0.d + i >= 0) {
                return i;
            }
            i2 = this.I0.d;
        } else {
            return Math.min((v0 - this.I0.d) - width, i);
        }
        return -i2;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public int E(RecyclerView.x xVar) {
        return f2(xVar);
    }

    public boolean E2() {
        return this.B0;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public int F(RecyclerView.x xVar) {
        return g2(xVar);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public int F1(int i, RecyclerView.t tVar, RecyclerView.x xVar) {
        if (k() && this.x0 != 0) {
            int D2 = D2(i);
            b.l(this.I0, D2);
            this.K0.r(-D2);
            return D2;
        }
        int C2 = C2(i, tVar, xVar);
        this.R0.clear();
        return C2;
    }

    public final boolean F2(View view, boolean z) {
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int v0 = v0() - getPaddingRight();
        int h0 = h0() - getPaddingBottom();
        int x2 = x2(view);
        int z2 = z2(view);
        int y2 = y2(view);
        int v2 = v2(view);
        return z ? (paddingLeft <= x2 && v0 >= y2) && (paddingTop <= z2 && h0 >= v2) : (x2 >= v0 || y2 >= paddingLeft) && (z2 >= h0 || v2 >= paddingTop);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public int G(RecyclerView.x xVar) {
        return h2(xVar);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void G1(int i) {
        this.M0 = i;
        this.N0 = Integer.MIN_VALUE;
        SavedState savedState = this.L0;
        if (savedState != null) {
            savedState.h();
        }
        C1();
    }

    public final int G2(com.google.android.flexbox.a aVar, c cVar) {
        if (k()) {
            return H2(aVar, cVar);
        }
        return I2(aVar, cVar);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public int H1(int i, RecyclerView.t tVar, RecyclerView.x xVar) {
        if (!k() && (this.x0 != 0 || k())) {
            int D2 = D2(i);
            b.l(this.I0, D2);
            this.K0.r(-D2);
            return D2;
        }
        int C2 = C2(i, tVar, xVar);
        this.R0.clear();
        return C2;
    }

    /* JADX WARN: Removed duplicated region for block: B:40:0x00ce  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final int H2(com.google.android.flexbox.a r22, com.google.android.flexbox.FlexboxLayoutManager.c r23) {
        /*
            Method dump skipped, instructions count: 423
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.flexbox.FlexboxLayoutManager.H2(com.google.android.flexbox.a, com.google.android.flexbox.FlexboxLayoutManager$c):int");
    }

    /* JADX WARN: Removed duplicated region for block: B:40:0x00d4  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final int I2(com.google.android.flexbox.a r26, com.google.android.flexbox.FlexboxLayoutManager.c r27) {
        /*
            Method dump skipped, instructions count: 541
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.flexbox.FlexboxLayoutManager.I2(com.google.android.flexbox.a, com.google.android.flexbox.FlexboxLayoutManager$c):int");
    }

    public final void J2(RecyclerView.t tVar, c cVar) {
        if (cVar.j) {
            if (cVar.i == -1) {
                L2(tVar, cVar);
            } else {
                M2(tVar, cVar);
            }
        }
    }

    public final void K2(RecyclerView.t tVar, int i, int i2) {
        while (i2 >= i) {
            w1(i2, tVar);
            i2--;
        }
    }

    public final void L2(RecyclerView.t tVar, c cVar) {
        int U;
        int i;
        View T;
        int i2;
        if (cVar.f < 0 || (U = U()) == 0 || (T = T(U - 1)) == null || (i2 = this.E0.c[o0(T)]) == -1) {
            return;
        }
        com.google.android.flexbox.a aVar = this.D0.get(i2);
        int i3 = i;
        while (true) {
            if (i3 < 0) {
                break;
            }
            View T2 = T(i3);
            if (T2 != null) {
                if (!c2(T2, cVar.f)) {
                    break;
                } else if (aVar.o != o0(T2)) {
                    continue;
                } else if (i2 <= 0) {
                    U = i3;
                    break;
                } else {
                    i2 += cVar.i;
                    aVar = this.D0.get(i2);
                    U = i3;
                }
            }
            i3--;
        }
        K2(tVar, U, i);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void M0(RecyclerView.Adapter adapter, RecyclerView.Adapter adapter2) {
        s1();
    }

    public final void M2(RecyclerView.t tVar, c cVar) {
        int U;
        View T;
        if (cVar.f < 0 || (U = U()) == 0 || (T = T(0)) == null) {
            return;
        }
        int i = this.E0.c[o0(T)];
        int i2 = -1;
        if (i == -1) {
            return;
        }
        com.google.android.flexbox.a aVar = this.D0.get(i);
        int i3 = 0;
        while (true) {
            if (i3 >= U) {
                break;
            }
            View T2 = T(i3);
            if (T2 != null) {
                if (!d2(T2, cVar.f)) {
                    break;
                } else if (aVar.p != o0(T2)) {
                    continue;
                } else if (i >= this.D0.size() - 1) {
                    i2 = i3;
                    break;
                } else {
                    i += cVar.i;
                    aVar = this.D0.get(i);
                    i2 = i3;
                }
            }
            i3++;
        }
        K2(tVar, 0, i2);
    }

    public final void N2() {
        int w0;
        if (k()) {
            w0 = i0();
        } else {
            w0 = w0();
        }
        this.H0.b = w0 == 0 || w0 == Integer.MIN_VALUE;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public RecyclerView.LayoutParams O() {
        return new LayoutParams(-2, -2);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void O0(RecyclerView recyclerView) {
        super.O0(recyclerView);
        this.T0 = (View) recyclerView.getParent();
    }

    public final void O2() {
        int k0 = k0();
        int i = this.w0;
        if (i == 0) {
            this.B0 = k0 == 1;
            this.C0 = this.x0 == 2;
        } else if (i == 1) {
            this.B0 = k0 != 1;
            this.C0 = this.x0 == 2;
        } else if (i == 2) {
            boolean z = k0 == 1;
            this.B0 = z;
            if (this.x0 == 2) {
                this.B0 = !z;
            }
            this.C0 = false;
        } else if (i != 3) {
            this.B0 = false;
            this.C0 = false;
        } else {
            boolean z2 = k0 == 1;
            this.B0 = z2;
            if (this.x0 == 2) {
                this.B0 = !z2;
            }
            this.C0 = true;
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public RecyclerView.LayoutParams P(Context context, AttributeSet attributeSet) {
        return new LayoutParams(context, attributeSet);
    }

    public void P2(int i) {
        int i2 = this.z0;
        if (i2 != i) {
            if (i2 == 4 || i == 4) {
                s1();
                e2();
            }
            this.z0 = i;
            C1();
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void Q0(RecyclerView recyclerView, RecyclerView.t tVar) {
        super.Q0(recyclerView, tVar);
        if (this.Q0) {
            t1(tVar);
            tVar.c();
        }
    }

    public void Q2(int i) {
        if (this.w0 != i) {
            s1();
            this.w0 = i;
            this.J0 = null;
            this.K0 = null;
            e2();
            C1();
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void R1(RecyclerView recyclerView, RecyclerView.x xVar, int i) {
        m mVar = new m(recyclerView.getContext());
        mVar.setTargetPosition(i);
        S1(mVar);
    }

    public void R2(int i) {
        if (i != 2) {
            int i2 = this.x0;
            if (i2 != i) {
                if (i2 == 0 || i == 0) {
                    s1();
                    e2();
                }
                this.x0 = i;
                this.J0 = null;
                this.K0 = null;
                C1();
                return;
            }
            return;
        }
        throw new UnsupportedOperationException("wrap_reverse is not supported in FlexboxLayoutManager");
    }

    public final boolean S2(RecyclerView.x xVar, b bVar) {
        View l2;
        int m;
        boolean z = false;
        if (U() == 0) {
            return false;
        }
        if (bVar.e) {
            l2 = o2(xVar.b());
        } else {
            l2 = l2(xVar.b());
        }
        if (l2 != null) {
            bVar.s(l2);
            if (!xVar.e() && U1()) {
                if (this.J0.g(l2) >= this.J0.i() || this.J0.d(l2) < this.J0.m()) {
                    z = true;
                }
                if (z) {
                    if (bVar.e) {
                        m = this.J0.i();
                    } else {
                        m = this.J0.m();
                    }
                    bVar.c = m;
                }
            }
            return true;
        }
        return false;
    }

    public final boolean T2(RecyclerView.x xVar, b bVar, SavedState savedState) {
        int i;
        View T;
        int g;
        if (!xVar.e() && (i = this.M0) != -1) {
            if (i >= 0 && i < xVar.b()) {
                bVar.a = this.M0;
                bVar.b = this.E0.c[bVar.a];
                SavedState savedState2 = this.L0;
                if (savedState2 != null && savedState2.g(xVar.b())) {
                    bVar.c = this.J0.m() + savedState.f0;
                    bVar.g = true;
                    bVar.b = -1;
                    return true;
                } else if (this.N0 == Integer.MIN_VALUE) {
                    View N = N(this.M0);
                    if (N != null) {
                        if (this.J0.e(N) > this.J0.n()) {
                            bVar.r();
                            return true;
                        } else if (this.J0.g(N) - this.J0.m() < 0) {
                            bVar.c = this.J0.m();
                            bVar.e = false;
                            return true;
                        } else if (this.J0.i() - this.J0.d(N) < 0) {
                            bVar.c = this.J0.i();
                            bVar.e = true;
                            return true;
                        } else {
                            if (bVar.e) {
                                g = this.J0.d(N) + this.J0.o();
                            } else {
                                g = this.J0.g(N);
                            }
                            bVar.c = g;
                        }
                    } else {
                        if (U() > 0 && (T = T(0)) != null) {
                            bVar.e = this.M0 < o0(T);
                        }
                        bVar.r();
                    }
                    return true;
                } else {
                    if (k() || !this.B0) {
                        bVar.c = this.J0.m() + this.N0;
                    } else {
                        bVar.c = this.N0 - this.J0.j();
                    }
                    return true;
                }
            }
            this.M0 = -1;
            this.N0 = Integer.MIN_VALUE;
        }
        return false;
    }

    public final void U2(RecyclerView.x xVar, b bVar) {
        if (T2(xVar, bVar, this.L0) || S2(xVar, bVar)) {
            return;
        }
        bVar.r();
        bVar.a = 0;
        bVar.b = 0;
    }

    public final void V2(int i) {
        if (i >= q2()) {
            return;
        }
        int U = U();
        this.E0.t(U);
        this.E0.u(U);
        this.E0.s(U);
        if (i >= this.E0.c.length) {
            return;
        }
        this.U0 = i;
        View w2 = w2();
        if (w2 == null) {
            return;
        }
        this.M0 = o0(w2);
        if (!k() && this.B0) {
            this.N0 = this.J0.d(w2) + this.J0.j();
        } else {
            this.N0 = this.J0.g(w2) - this.J0.m();
        }
    }

    public final void W2(int i) {
        int i2;
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(v0(), w0());
        int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(h0(), i0());
        int v0 = v0();
        int h0 = h0();
        boolean z = true;
        if (k()) {
            int i3 = this.O0;
            if (i3 == Integer.MIN_VALUE || i3 == v0) {
                z = false;
            }
            i2 = this.H0.b ? this.S0.getResources().getDisplayMetrics().heightPixels : this.H0.a;
        } else {
            int i4 = this.P0;
            if (i4 == Integer.MIN_VALUE || i4 == h0) {
                z = false;
            }
            if (!this.H0.b) {
                i2 = this.H0.a;
            } else {
                i2 = this.S0.getResources().getDisplayMetrics().widthPixels;
            }
        }
        int i5 = i2;
        this.O0 = v0;
        this.P0 = h0;
        int i6 = this.U0;
        if (i6 != -1 || (this.M0 == -1 && !z)) {
            int min = i6 != -1 ? Math.min(i6, this.I0.a) : this.I0.a;
            this.V0.a();
            if (k()) {
                if (this.D0.size() > 0) {
                    this.E0.j(this.D0, min);
                    this.E0.b(this.V0, makeMeasureSpec, makeMeasureSpec2, i5, min, this.I0.a, this.D0);
                } else {
                    this.E0.s(i);
                    this.E0.d(this.V0, makeMeasureSpec, makeMeasureSpec2, i5, 0, this.D0);
                }
            } else if (this.D0.size() > 0) {
                this.E0.j(this.D0, min);
                this.E0.b(this.V0, makeMeasureSpec2, makeMeasureSpec, i5, min, this.I0.a, this.D0);
            } else {
                this.E0.s(i);
                this.E0.g(this.V0, makeMeasureSpec, makeMeasureSpec2, i5, 0, this.D0);
            }
            this.D0 = this.V0.a;
            this.E0.q(makeMeasureSpec, makeMeasureSpec2, min);
            this.E0.Y(min);
        } else if (this.I0.e) {
        } else {
            this.D0.clear();
            this.V0.a();
            if (k()) {
                this.E0.e(this.V0, makeMeasureSpec, makeMeasureSpec2, i5, this.I0.a, this.D0);
            } else {
                this.E0.h(this.V0, makeMeasureSpec, makeMeasureSpec2, i5, this.I0.a, this.D0);
            }
            this.D0 = this.V0.a;
            this.E0.p(makeMeasureSpec, makeMeasureSpec2);
            this.E0.X();
            b bVar = this.I0;
            bVar.b = this.E0.c[bVar.a];
            this.H0.c = this.I0.b;
        }
    }

    public final void X2(int i, int i2) {
        this.H0.i = i;
        boolean k = k();
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(v0(), w0());
        int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(h0(), i0());
        boolean z = !k && this.B0;
        if (i == 1) {
            View T = T(U() - 1);
            if (T == null) {
                return;
            }
            this.H0.e = this.J0.d(T);
            int o0 = o0(T);
            View p2 = p2(T, this.D0.get(this.E0.c[o0]));
            this.H0.h = 1;
            c cVar = this.H0;
            cVar.d = o0 + cVar.h;
            if (this.E0.c.length <= this.H0.d) {
                this.H0.c = -1;
            } else {
                c cVar2 = this.H0;
                cVar2.c = this.E0.c[cVar2.d];
            }
            if (z) {
                this.H0.e = this.J0.g(p2);
                this.H0.f = (-this.J0.g(p2)) + this.J0.m();
                c cVar3 = this.H0;
                cVar3.f = Math.max(cVar3.f, 0);
            } else {
                this.H0.e = this.J0.d(p2);
                this.H0.f = this.J0.d(p2) - this.J0.i();
            }
            if ((this.H0.c == -1 || this.H0.c > this.D0.size() - 1) && this.H0.d <= getFlexItemCount()) {
                int i3 = i2 - this.H0.f;
                this.V0.a();
                if (i3 > 0) {
                    if (k) {
                        this.E0.d(this.V0, makeMeasureSpec, makeMeasureSpec2, i3, this.H0.d, this.D0);
                    } else {
                        this.E0.g(this.V0, makeMeasureSpec, makeMeasureSpec2, i3, this.H0.d, this.D0);
                    }
                    this.E0.q(makeMeasureSpec, makeMeasureSpec2, this.H0.d);
                    this.E0.Y(this.H0.d);
                }
            }
        } else {
            View T2 = T(0);
            if (T2 == null) {
                return;
            }
            this.H0.e = this.J0.g(T2);
            int o02 = o0(T2);
            View m2 = m2(T2, this.D0.get(this.E0.c[o02]));
            this.H0.h = 1;
            int i4 = this.E0.c[o02];
            if (i4 == -1) {
                i4 = 0;
            }
            if (i4 > 0) {
                this.H0.d = o02 - this.D0.get(i4 - 1).b();
            } else {
                this.H0.d = -1;
            }
            this.H0.c = i4 > 0 ? i4 - 1 : 0;
            if (z) {
                this.H0.e = this.J0.d(m2);
                this.H0.f = this.J0.d(m2) - this.J0.i();
                c cVar4 = this.H0;
                cVar4.f = Math.max(cVar4.f, 0);
            } else {
                this.H0.e = this.J0.g(m2);
                this.H0.f = (-this.J0.g(m2)) + this.J0.m();
            }
        }
        c cVar5 = this.H0;
        cVar5.a = i2 - cVar5.f;
    }

    public final void Y2(b bVar, boolean z, boolean z2) {
        if (z2) {
            N2();
        } else {
            this.H0.b = false;
        }
        if (!k() && this.B0) {
            this.H0.a = bVar.c - getPaddingRight();
        } else {
            this.H0.a = this.J0.i() - bVar.c;
        }
        this.H0.d = bVar.a;
        this.H0.h = 1;
        this.H0.i = 1;
        this.H0.e = bVar.c;
        this.H0.f = Integer.MIN_VALUE;
        this.H0.c = bVar.b;
        if (!z || this.D0.size() <= 1 || bVar.b < 0 || bVar.b >= this.D0.size() - 1) {
            return;
        }
        c.l(this.H0);
        c.u(this.H0, this.D0.get(bVar.b).b());
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void Z0(RecyclerView recyclerView, int i, int i2) {
        super.Z0(recyclerView, i, i2);
        V2(i);
    }

    public final void Z2(b bVar, boolean z, boolean z2) {
        if (z2) {
            N2();
        } else {
            this.H0.b = false;
        }
        if (!k() && this.B0) {
            this.H0.a = (this.T0.getWidth() - bVar.c) - this.J0.m();
        } else {
            this.H0.a = bVar.c - this.J0.m();
        }
        this.H0.d = bVar.a;
        this.H0.h = 1;
        this.H0.i = -1;
        this.H0.e = bVar.c;
        this.H0.f = Integer.MIN_VALUE;
        this.H0.c = bVar.b;
        if (!z || bVar.b <= 0 || this.D0.size() <= bVar.b) {
            return;
        }
        c.m(this.H0);
        c.v(this.H0, this.D0.get(bVar.b).b());
    }

    @Override // androidx.recyclerview.widget.RecyclerView.w.b
    public PointF a(int i) {
        View T;
        if (U() == 0 || (T = T(0)) == null) {
            return null;
        }
        int i2 = i < o0(T) ? -1 : 1;
        if (k()) {
            return new PointF(Utils.FLOAT_EPSILON, i2);
        }
        return new PointF(i2, Utils.FLOAT_EPSILON);
    }

    @Override // defpackage.z61
    public void b(View view, int i, int i2, com.google.android.flexbox.a aVar) {
        u(view, W0);
        if (k()) {
            int l0 = l0(view) + q0(view);
            aVar.e += l0;
            aVar.f += l0;
            return;
        }
        int t0 = t0(view) + S(view);
        aVar.e += t0;
        aVar.f += t0;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void b1(RecyclerView recyclerView, int i, int i2, int i3) {
        super.b1(recyclerView, i, i2, i3);
        V2(Math.min(i, i2));
    }

    @Override // defpackage.z61
    public void c(com.google.android.flexbox.a aVar) {
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void c1(RecyclerView recyclerView, int i, int i2) {
        super.c1(recyclerView, i, i2);
        V2(i);
    }

    public final boolean c2(View view, int i) {
        return (k() || !this.B0) ? this.J0.g(view) >= this.J0.h() - i : this.J0.d(view) <= i;
    }

    @Override // defpackage.z61
    public View d(int i) {
        return h(i);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void d1(RecyclerView recyclerView, int i, int i2) {
        super.d1(recyclerView, i, i2);
        V2(i);
    }

    public final boolean d2(View view, int i) {
        return (k() || !this.B0) ? this.J0.d(view) <= i : this.J0.h() - this.J0.g(view) <= i;
    }

    @Override // defpackage.z61
    public int e(int i, int i2, int i3) {
        return RecyclerView.LayoutManager.V(v0(), w0(), i2, i3, v());
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void e1(RecyclerView recyclerView, int i, int i2, Object obj) {
        super.e1(recyclerView, i, i2, obj);
        V2(i);
    }

    public final void e2() {
        this.D0.clear();
        this.I0.t();
        this.I0.d = 0;
    }

    @Override // defpackage.z61
    public void f(int i, View view) {
        this.R0.put(i, view);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void f1(RecyclerView.t tVar, RecyclerView.x xVar) {
        int i;
        int i2;
        this.F0 = tVar;
        this.G0 = xVar;
        int b2 = xVar.b();
        if (b2 == 0 && xVar.e()) {
            return;
        }
        O2();
        j2();
        i2();
        this.E0.t(b2);
        this.E0.u(b2);
        this.E0.s(b2);
        this.H0.j = false;
        SavedState savedState = this.L0;
        if (savedState != null && savedState.g(b2)) {
            this.M0 = this.L0.a;
        }
        if (!this.I0.f || this.M0 != -1 || this.L0 != null) {
            this.I0.t();
            U2(xVar, this.I0);
            this.I0.f = true;
        }
        H(tVar);
        if (this.I0.e) {
            Z2(this.I0, false, true);
        } else {
            Y2(this.I0, false, true);
        }
        W2(b2);
        k2(tVar, xVar, this.H0);
        if (this.I0.e) {
            i2 = this.H0.e;
            Y2(this.I0, true, false);
            k2(tVar, xVar, this.H0);
            i = this.H0.e;
        } else {
            i = this.H0.e;
            Z2(this.I0, true, false);
            k2(tVar, xVar, this.H0);
            i2 = this.H0.e;
        }
        if (U() > 0) {
            if (this.I0.e) {
                u2(i2 + t2(i, tVar, xVar, true), tVar, xVar, false);
            } else {
                t2(i + u2(i2, tVar, xVar, true), tVar, xVar, false);
            }
        }
    }

    public final int f2(RecyclerView.x xVar) {
        if (U() == 0) {
            return 0;
        }
        int b2 = xVar.b();
        j2();
        View l2 = l2(b2);
        View o2 = o2(b2);
        if (xVar.b() == 0 || l2 == null || o2 == null) {
            return 0;
        }
        return Math.min(this.J0.n(), this.J0.d(o2) - this.J0.g(l2));
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void g1(RecyclerView.x xVar) {
        super.g1(xVar);
        this.L0 = null;
        this.M0 = -1;
        this.N0 = Integer.MIN_VALUE;
        this.U0 = -1;
        this.I0.t();
        this.R0.clear();
    }

    public final int g2(RecyclerView.x xVar) {
        if (U() == 0) {
            return 0;
        }
        int b2 = xVar.b();
        View l2 = l2(b2);
        View o2 = o2(b2);
        if (xVar.b() != 0 && l2 != null && o2 != null) {
            int o0 = o0(l2);
            int o02 = o0(o2);
            int abs = Math.abs(this.J0.d(o2) - this.J0.g(l2));
            int[] iArr = this.E0.c;
            int i = iArr[o0];
            if (i != 0 && i != -1) {
                return Math.round((i * (abs / ((iArr[o02] - i) + 1))) + (this.J0.m() - this.J0.g(l2)));
            }
        }
        return 0;
    }

    @Override // defpackage.z61
    public int getAlignContent() {
        return 5;
    }

    @Override // defpackage.z61
    public int getAlignItems() {
        return this.z0;
    }

    @Override // defpackage.z61
    public int getFlexDirection() {
        return this.w0;
    }

    @Override // defpackage.z61
    public int getFlexItemCount() {
        return this.G0.b();
    }

    @Override // defpackage.z61
    public List<com.google.android.flexbox.a> getFlexLinesInternal() {
        return this.D0;
    }

    @Override // defpackage.z61
    public int getFlexWrap() {
        return this.x0;
    }

    @Override // defpackage.z61
    public int getLargestMainSize() {
        if (this.D0.size() == 0) {
            return 0;
        }
        int i = Integer.MIN_VALUE;
        int size = this.D0.size();
        for (int i2 = 0; i2 < size; i2++) {
            i = Math.max(i, this.D0.get(i2).e);
        }
        return i;
    }

    @Override // defpackage.z61
    public int getMaxLine() {
        return this.A0;
    }

    @Override // defpackage.z61
    public int getSumOfCrossSize() {
        int size = this.D0.size();
        int i = 0;
        for (int i2 = 0; i2 < size; i2++) {
            i += this.D0.get(i2).g;
        }
        return i;
    }

    @Override // defpackage.z61
    public View h(int i) {
        View view = this.R0.get(i);
        return view != null ? view : this.F0.o(i);
    }

    public final int h2(RecyclerView.x xVar) {
        if (U() == 0) {
            return 0;
        }
        int b2 = xVar.b();
        View l2 = l2(b2);
        View o2 = o2(b2);
        if (xVar.b() == 0 || l2 == null || o2 == null) {
            return 0;
        }
        int n2 = n2();
        return (int) ((Math.abs(this.J0.d(o2) - this.J0.g(l2)) / ((q2() - n2) + 1)) * xVar.b());
    }

    @Override // defpackage.z61
    public int i(View view, int i, int i2) {
        int t0;
        int S;
        if (k()) {
            t0 = l0(view);
            S = q0(view);
        } else {
            t0 = t0(view);
            S = S(view);
        }
        return t0 + S;
    }

    public final void i2() {
        if (this.H0 == null) {
            this.H0 = new c();
        }
    }

    @Override // defpackage.z61
    public int j(int i, int i2, int i3) {
        return RecyclerView.LayoutManager.V(h0(), i0(), i2, i3, w());
    }

    public final void j2() {
        if (this.J0 != null) {
            return;
        }
        if (k()) {
            if (this.x0 == 0) {
                this.J0 = q.a(this);
                this.K0 = q.c(this);
                return;
            }
            this.J0 = q.c(this);
            this.K0 = q.a(this);
        } else if (this.x0 == 0) {
            this.J0 = q.c(this);
            this.K0 = q.a(this);
        } else {
            this.J0 = q.a(this);
            this.K0 = q.c(this);
        }
    }

    @Override // defpackage.z61
    public boolean k() {
        int i = this.w0;
        return i == 0 || i == 1;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void k1(Parcelable parcelable) {
        if (parcelable instanceof SavedState) {
            this.L0 = (SavedState) parcelable;
            C1();
        }
    }

    public final int k2(RecyclerView.t tVar, RecyclerView.x xVar, c cVar) {
        if (cVar.f != Integer.MIN_VALUE) {
            if (cVar.a < 0) {
                c.q(cVar, cVar.a);
            }
            J2(tVar, cVar);
        }
        int i = cVar.a;
        int i2 = cVar.a;
        int i3 = 0;
        boolean k = k();
        while (true) {
            if ((i2 > 0 || this.H0.b) && cVar.D(xVar, this.D0)) {
                com.google.android.flexbox.a aVar = this.D0.get(cVar.c);
                cVar.d = aVar.o;
                i3 += G2(aVar, cVar);
                if (k || !this.B0) {
                    c.c(cVar, aVar.a() * cVar.i);
                } else {
                    c.d(cVar, aVar.a() * cVar.i);
                }
                i2 -= aVar.a();
            }
        }
        c.i(cVar, i3);
        if (cVar.f != Integer.MIN_VALUE) {
            c.q(cVar, i3);
            if (cVar.a < 0) {
                c.q(cVar, cVar.a);
            }
            J2(tVar, cVar);
        }
        return i - cVar.a;
    }

    @Override // defpackage.z61
    public int l(View view) {
        int l0;
        int q0;
        if (k()) {
            l0 = t0(view);
            q0 = S(view);
        } else {
            l0 = l0(view);
            q0 = q0(view);
        }
        return l0 + q0;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public Parcelable l1() {
        if (this.L0 != null) {
            return new SavedState(this.L0);
        }
        SavedState savedState = new SavedState();
        if (U() <= 0) {
            savedState.h();
        } else {
            View w2 = w2();
            savedState.a = o0(w2);
            savedState.f0 = this.J0.g(w2) - this.J0.m();
        }
        return savedState;
    }

    public final View l2(int i) {
        View s2 = s2(0, U(), i);
        if (s2 == null) {
            return null;
        }
        int i2 = this.E0.c[o0(s2)];
        if (i2 == -1) {
            return null;
        }
        return m2(s2, this.D0.get(i2));
    }

    public final View m2(View view, com.google.android.flexbox.a aVar) {
        boolean k = k();
        int i = aVar.h;
        for (int i2 = 1; i2 < i; i2++) {
            View T = T(i2);
            if (T != null && T.getVisibility() != 8) {
                if (this.B0 && !k) {
                    if (this.J0.d(view) >= this.J0.d(T)) {
                    }
                    view = T;
                } else {
                    if (this.J0.g(view) <= this.J0.g(T)) {
                    }
                    view = T;
                }
            }
        }
        return view;
    }

    public int n2() {
        View r2 = r2(0, U(), false);
        if (r2 == null) {
            return -1;
        }
        return o0(r2);
    }

    public final View o2(int i) {
        View s2 = s2(U() - 1, -1, i);
        if (s2 == null) {
            return null;
        }
        return p2(s2, this.D0.get(this.E0.c[o0(s2)]));
    }

    public final View p2(View view, com.google.android.flexbox.a aVar) {
        boolean k = k();
        int U = (U() - aVar.h) - 1;
        for (int U2 = U() - 2; U2 > U; U2--) {
            View T = T(U2);
            if (T != null && T.getVisibility() != 8) {
                if (this.B0 && !k) {
                    if (this.J0.g(view) <= this.J0.g(T)) {
                    }
                    view = T;
                } else {
                    if (this.J0.d(view) >= this.J0.d(T)) {
                    }
                    view = T;
                }
            }
        }
        return view;
    }

    public int q2() {
        View r2 = r2(U() - 1, -1, false);
        if (r2 == null) {
            return -1;
        }
        return o0(r2);
    }

    public final View r2(int i, int i2, boolean z) {
        int i3 = i2 > i ? 1 : -1;
        while (i != i2) {
            View T = T(i);
            if (F2(T, z)) {
                return T;
            }
            i += i3;
        }
        return null;
    }

    public final View s2(int i, int i2, int i3) {
        int o0;
        j2();
        i2();
        int m = this.J0.m();
        int i4 = this.J0.i();
        int i5 = i2 > i ? 1 : -1;
        View view = null;
        View view2 = null;
        while (i != i2) {
            View T = T(i);
            if (T != null && (o0 = o0(T)) >= 0 && o0 < i3) {
                if (((RecyclerView.LayoutParams) T.getLayoutParams()).c()) {
                    if (view2 == null) {
                        view2 = T;
                    }
                } else if (this.J0.g(T) >= m && this.J0.d(T) <= i4) {
                    return T;
                } else {
                    if (view == null) {
                        view = T;
                    }
                }
            }
            i += i5;
        }
        return view != null ? view : view2;
    }

    @Override // defpackage.z61
    public void setFlexLines(List<com.google.android.flexbox.a> list) {
        this.D0 = list;
    }

    public final int t2(int i, RecyclerView.t tVar, RecyclerView.x xVar, boolean z) {
        int i2;
        int i3;
        if (!k() && this.B0) {
            int m = i - this.J0.m();
            if (m <= 0) {
                return 0;
            }
            i2 = C2(m, tVar, xVar);
        } else {
            int i4 = this.J0.i() - i;
            if (i4 <= 0) {
                return 0;
            }
            i2 = -C2(-i4, tVar, xVar);
        }
        int i5 = i + i2;
        if (!z || (i3 = this.J0.i() - i5) <= 0) {
            return i2;
        }
        this.J0.r(i3);
        return i3 + i2;
    }

    public final int u2(int i, RecyclerView.t tVar, RecyclerView.x xVar, boolean z) {
        int i2;
        int m;
        if (!k() && this.B0) {
            int i3 = this.J0.i() - i;
            if (i3 <= 0) {
                return 0;
            }
            i2 = C2(-i3, tVar, xVar);
        } else {
            int m2 = i - this.J0.m();
            if (m2 <= 0) {
                return 0;
            }
            i2 = -C2(m2, tVar, xVar);
        }
        int i4 = i + i2;
        if (!z || (m = i4 - this.J0.m()) <= 0) {
            return i2;
        }
        this.J0.r(-m);
        return i2 - m;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public boolean v() {
        if (this.x0 == 0) {
            return k();
        }
        if (k()) {
            int v0 = v0();
            View view = this.T0;
            if (v0 <= (view != null ? view.getWidth() : 0)) {
                return false;
            }
        }
        return true;
    }

    public final int v2(View view) {
        return Z(view) + ((ViewGroup.MarginLayoutParams) ((RecyclerView.LayoutParams) view.getLayoutParams())).bottomMargin;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public boolean w() {
        if (this.x0 == 0) {
            return !k();
        }
        if (k()) {
            return true;
        }
        int h0 = h0();
        View view = this.T0;
        return h0 > (view != null ? view.getHeight() : 0);
    }

    public final View w2() {
        return T(0);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public boolean x(RecyclerView.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams;
    }

    public final int x2(View view) {
        return b0(view) - ((ViewGroup.MarginLayoutParams) ((RecyclerView.LayoutParams) view.getLayoutParams())).leftMargin;
    }

    public final int y2(View view) {
        return e0(view) + ((ViewGroup.MarginLayoutParams) ((RecyclerView.LayoutParams) view.getLayoutParams())).rightMargin;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public boolean z0() {
        return true;
    }

    public final int z2(View view) {
        return f0(view) - ((ViewGroup.MarginLayoutParams) ((RecyclerView.LayoutParams) view.getLayoutParams())).topMargin;
    }

    /* loaded from: classes.dex */
    public static class SavedState implements Parcelable {
        public static final Parcelable.Creator<SavedState> CREATOR = new a();
        public int a;
        public int f0;

        /* loaded from: classes.dex */
        public class a implements Parcelable.Creator<SavedState> {
            @Override // android.os.Parcelable.Creator
            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            @Override // android.os.Parcelable.Creator
            /* renamed from: b */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        }

        @Override // android.os.Parcelable
        public int describeContents() {
            return 0;
        }

        public final boolean g(int i) {
            int i2 = this.a;
            return i2 >= 0 && i2 < i;
        }

        public final void h() {
            this.a = -1;
        }

        public String toString() {
            return "SavedState{mAnchorPosition=" + this.a + ", mAnchorOffset=" + this.f0 + '}';
        }

        @Override // android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.a);
            parcel.writeInt(this.f0);
        }

        public SavedState() {
        }

        public SavedState(Parcel parcel) {
            this.a = parcel.readInt();
            this.f0 = parcel.readInt();
        }

        public SavedState(SavedState savedState) {
            this.a = savedState.a;
            this.f0 = savedState.f0;
        }
    }

    public FlexboxLayoutManager(Context context, int i, int i2) {
        this.A0 = -1;
        this.D0 = new ArrayList();
        this.E0 = new com.google.android.flexbox.b(this);
        this.I0 = new b();
        this.M0 = -1;
        this.N0 = Integer.MIN_VALUE;
        this.O0 = Integer.MIN_VALUE;
        this.P0 = Integer.MIN_VALUE;
        this.R0 = new SparseArray<>();
        this.U0 = -1;
        this.V0 = new b.C0103b();
        Q2(i);
        R2(i2);
        P2(4);
        this.S0 = context;
    }

    /* loaded from: classes.dex */
    public static class LayoutParams extends RecyclerView.LayoutParams implements FlexItem {
        public static final Parcelable.Creator<LayoutParams> CREATOR = new a();
        public float i0;
        public float j0;
        public int k0;
        public float l0;
        public int m0;
        public int n0;
        public int o0;
        public int p0;
        public boolean q0;

        /* loaded from: classes.dex */
        public class a implements Parcelable.Creator<LayoutParams> {
            @Override // android.os.Parcelable.Creator
            /* renamed from: a */
            public LayoutParams createFromParcel(Parcel parcel) {
                return new LayoutParams(parcel);
            }

            @Override // android.os.Parcelable.Creator
            /* renamed from: b */
            public LayoutParams[] newArray(int i) {
                return new LayoutParams[i];
            }
        }

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            this.i0 = Utils.FLOAT_EPSILON;
            this.j0 = 1.0f;
            this.k0 = -1;
            this.l0 = -1.0f;
            this.o0 = 16777215;
            this.p0 = 16777215;
        }

        @Override // com.google.android.flexbox.FlexItem
        public int B0() {
            return ((ViewGroup.MarginLayoutParams) this).leftMargin;
        }

        @Override // com.google.android.flexbox.FlexItem
        public int F1() {
            return this.o0;
        }

        @Override // com.google.android.flexbox.FlexItem
        public int K0() {
            return ((ViewGroup.MarginLayoutParams) this).topMargin;
        }

        @Override // com.google.android.flexbox.FlexItem
        public void O0(int i) {
            this.n0 = i;
        }

        @Override // com.google.android.flexbox.FlexItem
        public float S0() {
            return this.i0;
        }

        @Override // com.google.android.flexbox.FlexItem
        public float X0() {
            return this.l0;
        }

        @Override // com.google.android.flexbox.FlexItem
        public int a0() {
            return this.k0;
        }

        @Override // android.os.Parcelable
        public int describeContents() {
            return 0;
        }

        @Override // com.google.android.flexbox.FlexItem
        public float e0() {
            return this.j0;
        }

        @Override // com.google.android.flexbox.FlexItem
        public int getHeight() {
            return ((ViewGroup.MarginLayoutParams) this).height;
        }

        @Override // com.google.android.flexbox.FlexItem
        public int getOrder() {
            return 1;
        }

        @Override // com.google.android.flexbox.FlexItem
        public int getWidth() {
            return ((ViewGroup.MarginLayoutParams) this).width;
        }

        @Override // com.google.android.flexbox.FlexItem
        public int m0() {
            return this.m0;
        }

        @Override // com.google.android.flexbox.FlexItem
        public int m1() {
            return ((ViewGroup.MarginLayoutParams) this).rightMargin;
        }

        @Override // com.google.android.flexbox.FlexItem
        public int p1() {
            return this.n0;
        }

        @Override // com.google.android.flexbox.FlexItem
        public boolean r1() {
            return this.q0;
        }

        @Override // com.google.android.flexbox.FlexItem
        public void w0(int i) {
            this.m0 = i;
        }

        @Override // com.google.android.flexbox.FlexItem
        public int w1() {
            return this.p0;
        }

        @Override // android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeFloat(this.i0);
            parcel.writeFloat(this.j0);
            parcel.writeInt(this.k0);
            parcel.writeFloat(this.l0);
            parcel.writeInt(this.m0);
            parcel.writeInt(this.n0);
            parcel.writeInt(this.o0);
            parcel.writeInt(this.p0);
            parcel.writeByte(this.q0 ? (byte) 1 : (byte) 0);
            parcel.writeInt(((ViewGroup.MarginLayoutParams) this).bottomMargin);
            parcel.writeInt(((ViewGroup.MarginLayoutParams) this).leftMargin);
            parcel.writeInt(((ViewGroup.MarginLayoutParams) this).rightMargin);
            parcel.writeInt(((ViewGroup.MarginLayoutParams) this).topMargin);
            parcel.writeInt(((ViewGroup.MarginLayoutParams) this).height);
            parcel.writeInt(((ViewGroup.MarginLayoutParams) this).width);
        }

        @Override // com.google.android.flexbox.FlexItem
        public int x0() {
            return ((ViewGroup.MarginLayoutParams) this).bottomMargin;
        }

        public LayoutParams(int i, int i2) {
            super(i, i2);
            this.i0 = Utils.FLOAT_EPSILON;
            this.j0 = 1.0f;
            this.k0 = -1;
            this.l0 = -1.0f;
            this.o0 = 16777215;
            this.p0 = 16777215;
        }

        public LayoutParams(Parcel parcel) {
            super(-2, -2);
            this.i0 = Utils.FLOAT_EPSILON;
            this.j0 = 1.0f;
            this.k0 = -1;
            this.l0 = -1.0f;
            this.o0 = 16777215;
            this.p0 = 16777215;
            this.i0 = parcel.readFloat();
            this.j0 = parcel.readFloat();
            this.k0 = parcel.readInt();
            this.l0 = parcel.readFloat();
            this.m0 = parcel.readInt();
            this.n0 = parcel.readInt();
            this.o0 = parcel.readInt();
            this.p0 = parcel.readInt();
            this.q0 = parcel.readByte() != 0;
            ((ViewGroup.MarginLayoutParams) this).bottomMargin = parcel.readInt();
            ((ViewGroup.MarginLayoutParams) this).leftMargin = parcel.readInt();
            ((ViewGroup.MarginLayoutParams) this).rightMargin = parcel.readInt();
            ((ViewGroup.MarginLayoutParams) this).topMargin = parcel.readInt();
            ((ViewGroup.MarginLayoutParams) this).height = parcel.readInt();
            ((ViewGroup.MarginLayoutParams) this).width = parcel.readInt();
        }
    }

    public FlexboxLayoutManager(Context context, AttributeSet attributeSet, int i, int i2) {
        this.A0 = -1;
        this.D0 = new ArrayList();
        this.E0 = new com.google.android.flexbox.b(this);
        this.I0 = new b();
        this.M0 = -1;
        this.N0 = Integer.MIN_VALUE;
        this.O0 = Integer.MIN_VALUE;
        this.P0 = Integer.MIN_VALUE;
        this.R0 = new SparseArray<>();
        this.U0 = -1;
        this.V0 = new b.C0103b();
        RecyclerView.LayoutManager.Properties p0 = RecyclerView.LayoutManager.p0(context, attributeSet, i, i2);
        int i3 = p0.a;
        if (i3 != 0) {
            if (i3 == 1) {
                if (p0.c) {
                    Q2(3);
                } else {
                    Q2(2);
                }
            }
        } else if (p0.c) {
            Q2(1);
        } else {
            Q2(0);
        }
        R2(1);
        P2(4);
        this.S0 = context;
    }
}
