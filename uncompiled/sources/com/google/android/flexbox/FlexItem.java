package com.google.android.flexbox;

import android.os.Parcelable;

/* loaded from: classes.dex */
interface FlexItem extends Parcelable {
    int B0();

    int F1();

    int K0();

    void O0(int i);

    float S0();

    float X0();

    int a0();

    float e0();

    int getHeight();

    int getOrder();

    int getWidth();

    int m0();

    int m1();

    int p1();

    boolean r1();

    void w0(int i);

    int w1();

    int x0();
}
